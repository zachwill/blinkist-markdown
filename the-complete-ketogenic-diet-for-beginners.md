---
id: 5c06ace16cee0700076d4aa8
slug: the-complete-ketogenic-diet-for-beginners-en
published_date: 2018-12-07T00:00:00.000+00:00
author: Amy Ramos
title: The Complete Ketogenic Diet for Beginners
subtitle: Your Essential Guide to Living the Keto Lifestyle
main_color: 456435
text_color: 456435
---

# The Complete Ketogenic Diet for Beginners

_Your Essential Guide to Living the Keto Lifestyle_

**Amy Ramos**

As the title suggests, _The Complete Ketogenic Diet for Beginners_ (2016) is a helpful introduction to the low-carb, high-fat diet that can help you burn off unwanted pounds. It takes you on a step-by-step tour of what to expect as you "go keto" as well as what you can do to help make sure you stick to the new dietary lifestyle.

---
### 1. What’s in it for me? Learn about the low-carb diet that could change your life! 

There have been countless weight-loss fads and trendy diets over the years, but few of them really take advantage of the way human metabolism works. This is why the ketogenic diet has been getting results and gaining devotees; it has biology on its side.

Now, you may think that any diet that allows you to eat your favorite cheeses can't possibly work, but it can once you reset your metabolism to burn body fat in a completely natural, healthy and efficient way. And this is precisely what the ketogenic diet, or the "keto" diet for short, does. And what's more, when you go keto you'll also experience other benefits — like lowered cholesterol and a reduced risk of developing diabetes.

These blinks will help you make the transition to this new metabolic state. They'll help you navigate the pitfalls of starting a new diet, mapping out the terrain of dietary temptations and easing the difficulty of the early days.

In these blinks, you'll also learn

  * how to minimize the "keto flu";

  * the staple ingredients of a keto-friendly kitchen; and

  * the three meals and two snacks that make a perfect day for a keto dieter.

### 2. The ketogenic diet is a low-carb diet that puts your body into a metabolic process called ketosis. 

When most of us hear the word "diet," we think of measuring every gram, counting every calorie and having to eat flavorless meals while avoiding our favorite foods. But more often than not, this approach isn't just unsustainable. It's also unhealthy.

The ketogenic diet is a low-carb diet that gets its name from a natural metabolic state known as _ketosis_. When your body is in this state, it gets its energy from burning fat, rather than burning carbohydrates.

A lot of other diets are high in carbs, and they cause the body to enter a different metabolic state, known as _glycolysis_. The problem is, glucose levels in the blood tend to rise during glycolysis. This results in higher levels of insulin being produced, which, in turn, causes the body to store more fat.

On the other hand, during _ketosis_, stored fats and proteins are burned off in order to create _ketone bodies_, or _ketones_, which generate energy. And when your body burns off fat and protein, you lose weight — plain and simple.

Keeping your insulin levels down also means you'll have fewer ups and downs in your blood-glucose levels to cope with. When this happens, you'll be surprised by how much more energy you'll have during the day.

Along with cutting out high-carb and high-sodium foods, the ketogenic diet works by relying on healthy fats and proteins.

You may be familiar with the recommendations of national health organizations, which suggest that carbs are necessary to produce _glycerol_ for energy. Well, the fact of the matter is that glycerol is also produced during _gluconeogenesis_, a process that takes place in the liver and relies on dietary fats, not carbs.

Nevertheless, 5 percent of a ketogenic diet can still be carbs. How much that is precisely depends on your body type, but, in a sub-2,000-calorie diet, that would be roughly 25 grams. In order to keep an active state of ketosis, however, the majority of your calories will need to come from healthy fats and proteins.

If you're skeptical, keep in mind that ketosis is a perfectly natural state for your body and metabolism to be in. We'll explore its benefits in the next blink.

### 3. While health organizations have championed low-fat diets, the ketogenic diet is safer and more sustainable. 

If you grew up in the United States, you likely learned about the USDA Food Pyramid in school. This guide to healthy eating has long recommended a low-fat diet in which carbohydrates constitute the majority of your daily caloric intake.

But despite what national health organizations may suggest, the low-fat approach to dieting has proven both unfeasible and unhealthy.

Just take the American Diabetes Association, which encourages diabetics to eat "healthy carbohydrates" — a recommendation that has misled many dieters.

Our bodies produce insulin when glucose, or blood-sugar levels, get too high, and glucose is created when the body breaks down carbohydrates. Over time, your body can become insulin resistant, meaning that more insulin is needed to bring down glucose levels. Insulin resistance is dangerous because it can lead to _metabolic syndrome_, then _prediabetes_ and, ultimately, _type 2 diabetes_, a chronic condition of high blood sugar.

With glucose coming from carbs, it stands to reason that low-carb dieters generally have more normal levels of blood sugar and insulin than those on a low-fat diet. Moreover, studies show that low-carb dieters tend to lose more weight even when they don't reduce their calorie intake.

If you have either type 1 or type 2 diabetes, the ketogenic diet can be beneficial and even reverse your condition. That said, you should definitely consult your doctor before changing to a new diet.

While the keto diet is a perfect way to burn body fat, that's far from its only benefit.

Since the 1920s, a ketogenic diet has been recommended by doctors as a healthy lifestyle; and even as far back as 500 BCE, it was used as a treatment against epileptic seizures. Plus, there are hundreds of recent studies that show the diet to be a viable way of reducing the risk of not only epilepsy and diabetes, but also heart disease and Alzheimer's.

There are also studies that show how a keto diet can improve brain functionality and lead to healthier cholesterol and blood-pressure levels.

And since foods containing healthy fats contain twice the calories of carbs, you'll feel full after eating just half the amount of food by weight.

With all these benefits, weight loss isn't the only way the keto diet can improve your life.

### 4. Move your body into ketosis by following keto ratios and preparing for the keto flu. 

Now that you know the logic behind the ketogenic diet, it's time to get down to the nitty-gritty of what to expect once you get on board.

One of the big pluses of the keto diet is that you can reach your goals without getting bogged down by calorie counting. That said, there are some essential details you should be aware of.

The diet is built around the ratios of _macronutrients_ in the food you eat. Macronutrients are the basic building blocks that make up your food, such as fat, protein and carbs. On the keto diet, you should be eating between 65 and 70 percent fat, 20 and 25 percent protein and around 5 percent carbs.

A good example of this is the bacon, cheese and pepper "fat bomb," which is made up of bacon, black pepper and a mixture of goat cheese and cream cheese.

One thing you won't be eating much of is rice, especially as you're starting the diet. Rice is a staple in high-carb diets, with a whopping 44 net carbs per cup. Later on in the diet, after your metabolism has adjusted, it's possible to add some more carbs every once in a while and still maintain ketosis. But in the first four to eight weeks, it's important to stick to the 5-percent ratio and let your body adjust.

It's also important to understand that the exact ratios and recommended daily caloric intake will vary depending on your body type, your activity level and your weight-loss goals. To make it easy, there are online calculators you can use to identify the macro ratios that are right for you.

You should also be aware of the "keto flu." This is a common reaction people undergo when transitioning into ketosis. Essentially, your body will go through withdrawal as you cut sugar, carbs and processed foods from your diet and start using fat for fuel.

Symptoms of keto flu include coughing, headaches and fatigue, much like the common influenza virus. But in this case, the symptoms will only last a few days, and they aren't contagious or dangerous. Fortunately, you can ease your symptoms by drinking lots of water, as well as beverages containing sodium and electrolytes.

Then there's the temporary unpleasantness of "keto breath." At the beginning of the diet, ketone secretions will end up in your breath, as well as your urine, resulting in an unfortunate smell. But, again, this only lasts for the first few weeks and will stop once your body adjusts to its new metabolic processes.

### 5. Prep for your diet by making your pantry keto-friendly and keeping the people you are close with informed. 

It's never easy making a change in your lifestyle, but it can be even harder to undertake a new diet when everyone around you is eating whatever they please.

However, you _can_ overcome the early challenges and succeed in making a long-term lifestyle change. All it takes is some preparation.

The first step to avoiding temptation is to build yourself a keto-friendly kitchen.

Before you begin the diet, you're likely to have a lot of sugary foods and drinks, and carb-heavy fruits, starches, grains, legumes and processed, polyunsaturated fats and oils, like canola oil. But don't just throw them out. Instead, get rid of them by donating what you can to a local food bank, a charitable organization which distributes food to feed the hungry.

Then it's time to restock with the basics of a keto-friendly kitchen. This should include soup broths, lemon juice, low-carb condiments, butter and oils such as avocado oil and olive oil. It also helps to have healthier sweeteners like stevia.

You may feel like your pantry is too empty, but that's because the keto diet emphasizes fresh food rather than the additive- and preservative-filled foods that enable a long shelf life. Your new shopping list will include meats, dairy products, nonstarchy vegetables and low-carb fruits like berries and avocados — all of which have short shelf lives.

Now, it's very likely that you'll have friends, family or roommates who aren't going on the keto diet, so just keep them informed about your new lifestyle's requirements. This way, they'll know that you're reducing your carb intake and won't tempt you by offering you half their bagel. 

If you're living with carb-hungry people, you might want to see if you can find an arrangement where their keto-unfriendly foods are kept in a separate area, to further reduce your temptations. This will also be a clear sign to those around you of how serious you are about sticking to your diet.

Also, be prepared to encounter people who've spent their entire lives following a high-carb, low-fat diet. When these people hear about your keto diet, they may want to debate you on the benefits of a low-carb lifestyle.

A good way to steer clear of debates is to avoid the words "keto" and "low-carb" and just say you're avoiding grains and sugar. But if you find yourself facing an argumentative person, it's best not to engage. Instead, simply tell them that, if they care to be informed, there are plenty of studies out there that list all the benefits of a low-carb diet.

### 6. Stick to a meal plan and exercise to get the best results from the ketogenic diet. 

Along with getting your kitchen well prepared, there are other steps you can take to make sure you hit the ground running with your new keto diet.

First, you can start a new habit of planning out your meals for the week ahead — and not just your dinners, but also your breakfasts, lunches and the important in-between-meal snacks.

Let's say you've done your calculations and you're suited for a diet of 1600 to 1850 calories per day. With this in mind you can plan on having a berry smoothie with some greens in it for breakfast, a morning snack of homemade keto shortbread cookies, a chicken-avocado-lettuce wrap for lunch, parmesan crackers for an afternoon snack and a keto casserole with baked coconut haddock for dinner.

You'll also be doing yourself a favor by taking care of all your weekly shopping ahead of time, in one big trip. If you don't, you're likely to encounter the scenario where you're too tired to shop and decide to order a pizza instead. However, if you have all the ingredients ready to go, there's no excuse not to season the haddock, top it with some shredded coconut and ground hazelnuts, and pop it in the oven. In just twelve minutes, you'll have a delicious keto-friendly dinner!

Once you get familiar with the diet, you'll see how easy it is to tweak the recipes to suit your tastes and nutritional needs.

To take your diet to the next step, you'll want to add an exercise component to your daily routine. This will both help you shed the unwanted pounds and boost your overall health.

If you haven't been working out already, you should ease yourself into it by walking or taking short jogs. If you are already a regular at the gym, you can still take it a step further by adding more cardio or an extra component to your routine.

Another good approach is to start a new activity by joining a yoga or dance class. But no matter what your preference is, just remember that exercise should be incremental and not a competition, so be careful not to overdo it.

At the end of the day, the ketogenic diet is about cultivating a long-term, energetic lifestyle. And these tips are to help ensure you get off on the right foot, start out organized and establish a solid foundation for the years ahead.

### 7. Final summary 

The key message in these blinks:

**Many people think carbs are a necessary part of an energetic life. But science shows that a low-carb diet like the ketogenic diet is not only effective at eliminating fat; it also comes with a number of healthy benefits. By moving your body into the metabolic state of ketosis, your body will burn fat for energy and stave off diabetes, heart disease and epilepsy. To make the transition easier, drink lots of water and avoid high-carb temptations. With some planning and preparation, you can ensure that you'll experience a smooth transition and reap the long-term benefits.**

Actionable advice:

**Buy a food scale and a food processor.**

Going keto means you'll be foregoing prepared and processed foods in favor of eating from scratch, so the tools in your kitchen are going to be increasingly important. One of the best tools for hitting your keto goals is a good food scale. Likewise, a food processor is a handy way to create sauces and shakes that will keep your meals tasty and more interesting.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Grain Brain_** **by David Perlmutter**

_Grain Brain_ (2013) outlines how what we eat can cause or mitigate serious brain disorders like anxiety, ADHD and depression. Eating well is crucial to your brain's functioning well, and these blinks tell you why.
---

### Amy Ramos

Amy Ramos is a holistic health expert and a professional chef with over 25 years of experience in developing recipes and helping people with medically restricted diets. She is also an advocate for the ketogenic diet and the low-carb lifestyle, and the author of the best-selling cookbook _Easy Ketogenic Diet Slow Cooking_ (2017).

