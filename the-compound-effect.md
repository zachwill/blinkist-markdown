---
id: 54b3a2d73562330009080000
slug: the-compound-effect-en
published_date: 2015-01-12T00:00:00.000+00:00
author: Darren Hardy
title: The Compound Effect
subtitle: Jumpstart Your Income, Your Life, Your Success
main_color: C22736
text_color: C22736
---

# The Compound Effect

_Jumpstart Your Income, Your Life, Your Success_

**Darren Hardy**

_The Compound Effect_ shows us how to make lasting changes by paying attention to the smallest decisions we make — and their cumulative effect on us. It's about the slow burn, not the big gesture. Author Darren Hardy teaches us that by accepting responsibility for our own lives, we can change our habits and carve out a life that is more successful, fulfilling and happy.

---
### 1. What’s in it for me? Learn how to take control of your life, reach your goals, and push past your limits. 

Many of us wake up every day and say, "today's the day." It's the day we tackle our monsters. We're going to lose that extra weight, get that promotion, and finally realize our dreams.

And then we wake up the next day and say, "Meh, maybe next week."

But the problem isn't our goals or ourselves, or even our attitude. The problem is our approach. Why do we think we can just wake up and snap our fingers and change everything? Not only is it unlikely to happen, it's also potentially dangerous.

_The Compound Effect_ offers another path. Build up momentum, make a plan, slowly change our bad habits into good ones, and tackle our personal limits. From the publisher of _Success_ magazine, _The Compound Effect_ represents a concrete how-to guide for reaching your goals and becoming successful.

After reading these blinks, you'll learn

  * what Arnold Schwarzenegger's weight-lifting technique can teach you about success;

  * how to build up momentum like Michael Phelps; and

  * why aiming for the quickest possible successes can damage your health.

### 2. Immediate results are less rewarding than steady improvement. 

Fast money and fast promotions: we're all looking for immediate results these days. But really, it's long-term changes that produce positive differences and are far more rewarding.

In this age of "now, now, now!" we eat fast food because cooking takes too long, we go on quick-fix diet plans to drop 20 pounds in a week, and become restless if we're not promoted within a year.

However, the more quickly we reach a goal, the bigger the consequences can be.

It's not healthy to expect instantaneous results. When you don't lose those 20 pounds in seven days, you might become disillusioned and think you never deserved a better life anyway.

What you need to do is take little steps, because daily, small, positive actions are the secret to long-term success.

Take Scott, for example. Instead of trying to shed 20 pounds in a week, he cut 125 calories from his daily diet, listened to self-improvement recordings as he commuted to work, and walked a little extra each day.

The result? In 31 months, he lost 15 kilos, and even got a promotion and pay rise at work.

Using the _Compound Effect_ means knowing that every decision you make creates your destiny, and that you must work consistently before you can experience success. Then, when you reach your goals, you have to maintain discipline if you wish to build on them, or your hard work will be for nothing. Think of those restaurants that become so popular that you have to wait ages to get a table. They take their success for granted and stop doing what got them there in the first place. Then, suddenly, the service and menus become lackluster, and no one goes there anymore.

It's crucial to avoid becoming dismayed when you don't see immediate effects, and to ensure you keep generating good results.

So what are the keys to consistent success?

> _"Starting today, you can decide to make simple, positive changes and allow the Compound Effect to take you where you want to go."_

### 3. Transform your life goals into daily habits. 

Sometimes we don't want to admit it, but our choices are what shape our destiny and we _are_ responsible for what we do or don't do.

Sometimes it's the seemingly insignificant decisions that prevent us from being successful. For instance, no one wants to become obese, or declare bankruptcy, but these are the consequences of multiple poor — but small — choices.

You're probably familiar with the situation where you find yourself scoffing a bag of chips and after you've finished the bag, you realize you've ruined a day of healthy eating.

It's hard to admit that you alone are responsible for your actions. Maybe you think it's bad luck, but that's just an excuse for your poor choices. We can all be lucky. In order to "be lucky," you just need to know how to spot good opportunities.

You need to connect your decisions with the right motivation and clear goals, because when you have defined goals, you can focus on them far more effectively. Your mind will start to see the opportunities in front of you and you'll start reaching them.

Remember the _Law of Attraction_ — "like attracts like." Thinking about positive actions will draw positive things toward you.

So, what are your goals? What makes you happy?

Consider the areas of business, health, spirituality, family and lifestyle and note down all the attributes, behaviors and characteristics you need to reach those goals, and try to apply them to your everyday life.

This means cutting out old habits. List your bad habits so you can see when and where you act in a way that isn't good for you.

For example, if you must eat something sweet after a meal, have some fruit or honey nearby for a healthier treat. It also helps if you're surrounded by people with similar goals to you, and if you have patience with yourself.

### 4. Create a routine to keep your momentum going. 

So we've learned that you take the first step to kickstarting your success when you make the right decisions. But how do you maintain that habit? You use _The Big Mo_.

If you keep your healthy behavior up for a long enough time, you'll fall into a natural, consistent rhythm. As you keep that pace, you'll keep making the right decisions, and start feeling unstoppable!

That's the power of _The Big Mo_ — the big momentum. It's the most powerful driving force for success.

Take swimmer Michael Phelps. His coach only let him finish training early _once_ in 12 years, when he was allowed 15 minutes off so he could attend a school dance. But all that practice led to Big Mo, culminating in eight gold Olympic medals.

To keep momentum rolling, your healthy behavior has to be built into your daily routine, so you'll need a plan.

Remember to be realistic and don't try to do everything in a short period of time. Instead, create a routine that enables you to ease into your new life. So scrap the idea of hitting the gym for two hours every day! Going three days a week for one hour, and sticking to it, is far better. When building momentum, the length of time you work out isn't that important. What matters is that you keep showing up.

Next, eliminate situations that cause you to stray from your goals.

The media, for example, can have a negative effect on your energy without you even noticing. Commercials, news about war, and so on, can wreak havoc on your attitude and expectations. Instead of tuning in to the news, why not listen to an inspirational CD or chat with someone you love?

Finally, be aware of your environment. Sometimes you have to change it in order to fulfill your dreams. People, too, influence you significantly, so keep supportive folks around you to keep your momentum going.

> _"You get in life what you create."_

### 5. Embrace obstacles to accelerate your path to success. 

As you progress, you're bound to hit some personal limits. The question is, will you stop pushing, or will you break through the wall?

The wall of your discipline and routines represents the gap between your old self and your improved, stronger self. You'll find your new habits will compound and you'll change into a more successful person. So when you come to the limit of what you think you can achieve, push through that limit to get quicker results and multiply your success.

Remember Arnold Schwarzenegger's famous _Cheating Principle_ for weight training? When you've reached the maximum number of lifts you can manage, lean back to access other muscle groups to support the working muscles. Doing this enables you to add five or six more reps to your set.

Similarly, pushing through your personal limits will only make you stronger.

Picture a scenario in which you've come within five pounds of your weight-loss goal, then hit a wall because of work stress. Remember, you already know how to be disciplined, make good choices, and build momentum without your old self-destructive behavior. So choose to push through, and you'll find you won't just achieve your goal, you'll also be even stronger.

It's always to your own benefit to push yourself a little more, go for a little longer and prepare a little better. That's why the author invests a great deal of time researching all he can about the companies to whom he gives keynote speeches. The extra work he puts in is more than what's expected of him, and this makes his speeches extremely popular.

So if you want to put yourself ahead of others and your old self, you need to break through your limits.

> _"Don't wish it were easier, wish you were better."_ — Jim Rohn

### 6. Final summary 

The key message in this book:

**The keys to success are realizing that you're responsible for your life, forming healthy daily habits, cultivating discipline and pushing past your comfort zone. Getting to your goals at lightning speed can sometimes be detrimental and is often not possible. Instead, steady progress and patience will get you where you really want to be.**

Actionable advice:

**Prove you're the boss**

To prove to yourself that you're really in control of your behavior and decisions, for 30 days abstain from smoking, quit drinking your favorite wine or eat one healthy meal per day. Completing this goal will help convince you that you really are the boss of your own life.

**Suggested** **further** **reading:** ** _The 4-Hour Workweek_** **by Tim Ferriss**

_The 4-Hour Workweek_ advocates the idea of the New Rich. These are the people who abandon their jobs as modern desk slaves and instead live a life that is all about enjoying the moment while still achieving big goals.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Darren Hardy

Darren Hardy is a motivational speaker, bestselling author and the publisher of _Success_ magazine. Previously, he was the executive producer of two television networks, The People's Network, and The Success Training Network.

