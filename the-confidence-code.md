---
id: 53b41bd132623100072c0000
slug: the-confidence-code-en
published_date: 2014-07-01T00:00:00.000+00:00
author: Katty Kay and Claire Shipman
title: The Confidence Code
subtitle: The Science and Art of Self-Assurance: What Women Should Know
main_color: CB292F
text_color: CB292F
---

# The Confidence Code

_The Science and Art of Self-Assurance: What Women Should Know_

**Katty Kay and Claire Shipman**

_The_ _Confidence_ _Code_ explains how, in comparison with men, women lack confidence. It also explores the impact such lack of confidence has on women in various spheres of life, especially the business world, and offers advice on how women can increase their confidence.

---
### 1. What’s in it for me? Find out why women are less confident than men, and how they can increase it. 

In general, would you agree that women are less confident than men? Most people would.

However, as authors Claire Shipman and Katty Kay argue in _The_ _Confidence_ _Code_, women _can_ choose to be just as confident as men.

Shipman and Kay show, step by step, exactly _how_ women can realize and increase their confidence, which, they argue, is determined by our specific genetic makeup and environmental factors.

As these blinks show, _The_ _Confidence_ _Code_ is of particular value to those women working in male-dominated environments, as it is in these environments that low confidence is most apparent and has the greatest consequences.

In the following blinks, you will also discover:

  * how lack of confidence may have cost you the promotion you worked so hard for, 

  * why lack of confidence is typically a female problem and

  * how failing a math exam can actually make you a more confident person.

### 2. Confidence is the bridge between thoughts and actions. 

How often have you looked back on conversations or opportunities in your life and thought "I wish I had said/done that"? Perhaps it wasn't even a big deal, but something small that was within your power to achieve, yet you didn't feel confident enough to try it.

Most of us have felt this way a number of times throughout our lives.

Unfortunately, if we lack confidence, we prefer to stay inactive, and, sadly, this seems to be particularly pronounced for women.

Confidence means having enough belief in our own abilities that we become active. Lack of confidence, therefore, means being uncertain of whether our efforts will be successful — an uncertainty that makes us scared to even try.

A clear example of this can be seen in an experiment by professor Zach Estes, who had students solve complicated puzzle tests. At first, it appeared to Estes that the male students had performed better than the female students. But, on closer inspection, Estes saw that many of the women had left a lot of the questions unanswered.

So, Estes asked the students to retake the test and, this time, to make sure they answer every single question. The result? The women performed just as well as the men.

But why did the women choose to not even attempt an answer to many of the questions? The central problem was the women's lack of confidence: they preferred to leave a blank space rather than risk giving the wrong answer.

In this situation, having confidence would have made them take the leap and try.

But what if _optimism_ played a role, rather than confidence?

Well, optimism — the attitude that everything is going to be okay — is different to confidence, which refers to taking action. Being optimistic does help, though, as it can lead to action which improves confidence.

We know then, that confidence in our abilities is crucial towards becoming doers. Now we will explore how our confidence levels often differ, depending on our gender.

### 3. Women’s confidence can be different to men’s. 

There have been countless books about the difference between men and women, and most of us agree the differences _are_ there. It's no surprise, then, that when it comes to confidence, we find that women display this in a different manner to men.

Particularly in the workplace, men are dominant, so their characteristics of confident behavior, such as acting aggressively, are sought and even expected.

In fact, men are perhaps even more dominant in the workplace than most of us realize. For instance, did you know that a mere four percent of Fortune 500 companies have women as CEOs?

The way men demonstrate their confidence tends to be more aggressive than the way women display it. Men are quicker, more energetic and determined at expressing their opinions, while women tend to collaborate with others and be more humble.

Furthermore, typical male qualities like aggression are valued more highly in the workplace than typical female qualities, because — as a majority — men define the standards.

It is vital to understand, however, that women can act with their softer side and still be confident.

For example, as long as you stand behind your opinions and defend your point of view, it doesn't matter if you do it aggressively or not. Active listening and cooperating with other like-minded colleagues — often viewed as more feminine strategies — can also be a demonstration of strength.

On the other hand, some women act with _fake_ _confidence_, by acting tough. But there is a downside to this approach: the artificiality of it is easily perceived by others and doesn't benefit us in the way that real confidence does.

So, women don't have to act exactly like their male colleagues in the workplace. Instead, they can take pride in their own unique approaches.

### 4. Women’s lack of confidence plays a very negative role in the business world. 

Living in a man's world is hard, but _working_ in a man's world is even harder.

Due to a lack of confidence, women tend to accept worse work conditions than men do. Many women simply don't feel confident enough to put themselves out there: for instance, even if they have good ideas they are less likely than men are to propose them to the boss.

The reality is that the business world is very competitive and without a healthy dose of confidence, it's impossible for one to thrive there.

To merely enter the workforce, we must be able to present ourselves in the best light — which requires that we see ourselves in this light as well.

One way we see a difference in how men and women present themselves is in negotiation. An economics professor at Carnegie Mellon University concluded in a series of studies that men negotiate their salary four times more often than women. And, even when women _do_ negotiate, they still expect to get 30 percent less of a salary bump than men do.

Moreover, the business world requires a certain amount of self-promotion and self-initiative, which are impossible without confidence. In contrast to the school classroom, where doing quality work is enough to get you noticed, in the business world we cannot count on our work to speak for itself. In short, we have to get _ourselves_ noticed.

Yet, without confidence, we remain silent, which results in missed opportunities, such as promotions. This is especially the case for women.

For example, a Princeton research team discovered that women speak up to 75 percent less often than men do, when there are more men than women in the room. So, even if a woman has the best business idea, she's far less likely to propose it in a room full of male colleagues. You can see how this might hinder her progress!

By now, it should be clear that confidence is an important tool in life. But let's take a closer look at how confidence affects us in our jobs.

> _"Confidence is no longer the sideshow, it's the main event."_

### 5. Being confident is just as important for getting the job done as competence. 

Even when women are absolutely capable, they often don't feel confident. This not only interferes with their attitude, but it also prevents them from progressing.

It's crucial to distinguish between _confidence_, the belief in our abilities in general, and _competence_, the knowledge of our qualifications in a certain field.

Confidence is not always based on competence. Many women feel incompetent and unprepared, even if they are totally competent at their jobs.

One example is Christine Lagarde, Managing Director of the International Monetary Fund and, thus, one of the most influential women in the world. Nevertheless, she admits in an interview with the authors that she not only had confidence problems while working her way up the hierarchy, but even now still has moments of insecurity.

Low confidence prevents us from aiming high enough to progress. It not only makes us uncomfortable, but is a condition that makes us aim for less than we actually want because we don't think it's possible to achieve our goals.

Having low confidence means that we can't even envision what we want, and so we don't work for it. If you look in the mirror and don't see a pilot, you will never take flight lessons!

An interesting example of how self-confidence affects action can be seen in psychologist David Dunning's experiment, which questions students about their confidence. Female students showed drastically lower levels of confidence in the rating of their abilities and achievements than male students.

Then, when Dunning invited the students to participate in a contest, only 49 percent of the female students signed up, while 71 percent of the male students did.

From this we see that negative self-perception caused by lack of confidence can cause women to miss opportunities.

So now that we have established how important confidence is, can we do anything to increase it?

### 6. Like other aspects of our character, confidence is partially predetermined by our genes. 

Today we know that nature as well as nurture determines our character. We have discovered that character traits such as aggression or alcoholism are heavily determined by our genetic code.

Interestingly, the same goes for confidence. In fact, scientists found that our genetic make-up determines up to 50 percent of our confidence.

You've probably heard of the happiness-inducing hormone serotonin. Well, the degree to which serotonin influences your behavior depends on a certain gene. If you're born with a longer version of the gene, you will produce more serotonin and be less anxious than if you're born with the short version of the gene.

Research with monkeys has shown that those born with the long version of the serotonin-regulating gene are more sociable and take more risks — which, when translated into human behavior, means they're more confident. 

Interestingly, the predisposition for confidence can be so strong that sometimes even our environment can't alter it.

If someone is born with genes that suggest high levels of aggression, this doesn't mean they will necessarily turn into a violent person. But they will be more likely to do so than people who don't possess that gene.

Similarly, if someone is born with genes that suggest high levels of confidence, they will probably be confident, even if raised otherwise.

For example, in the monkey experiment mentioned above, babies born with the short version of the serotonin gene, and also raised by unsupportive mothers, were less confident as they grew up. Yet those born with the long version of the gene, who were therefore expected to become confident, developed into confident adults, even when they were raised by the same unsupportive mothers.

So, if up to 50 percent of our confidence comes from our genes, the other 50 percent must be formed from personal experience.

### 7. Confidence can also be attributed to our environment. 

Our environment, then, determines those other 50 percent of our confidence. This is partly because our environment actually influences our genes. Indeed, scientists have found that life experiences actually physically alter our genes' shapes and cause them to function differently.

Upbringing in particular can be an important factor in our gene development and is therefore crucial in determining what kind of person we become.

Let's return to the previously mentioned monkey experiment: what happens when a monkey with genes indicating low confidence is raised by a caring mother? It turns out that they are influenced by the environment. Remarkably, being raised by a good mother makes them even more confident than those born with "confident" genes!

Regarding human female and male differences, girls are often raised to be more diligent and to follow directions more often than boys, which makes them less confident.

Girls are traditionally rewarded for good behavior and, in wanting to live up to this, they can become perfectionists and less likely to take risks. And, in order to be confident, we _must_ be able to take risks.

Our expectation of girls to be diligent and "good" is heavily influenced by society's stereotypes and criticisms of women, and these can diminish their confidence. Girls are taught to be "good" from kindergarten, so this is how society views them into adulthood. As a result, when a woman tries to act more confidently, she is often confronted with opposition.

Furthermore, studies have shown that women who take an active role are disliked by males _and_ females equally. For example, the few female students at the U.S. Naval Academy in Annapolis are called DUBs — or _dumb_ _ugly_ _bitches_.

Genetics, upbringing and society's double standards are all factors in influencing our personality and our confidence level. In the end, it's a mixture of both nature and nurture that determines our traits.

> _"When women are given a fair shot at success, they do well."_

### 8. We can teach ourselves to be confident, even if our genetic predisposition suggests otherwise. 

We've seen how important confidence is, and how we're not always in control of how our confidence develops. But it is possible to break the cycle that makes women less confident.

Even as adults, we can still alter our brains, as they possess a wonderful quality called _brain_ _plasticity_. This means that, through certain thought patterns, we can actually cause a physical change in our brains.

When presented with a choice, the brain accesses memories related to that particular decision. But we have the power to change which memories our brain accesses.

In one study, for example, people with a fear of spiders received two hours of behavioral therapy for their fear. Directly after the therapy, they were able to touch a live tarantula. Whilst doing so, their brain scans showed no activity in the fear center of the brain. This result was confirmed even 6 months later!

Instead of accessing the fearful memories, they accessed the calm memories from the therapy, therefore the fear centre in the brain remained calm and the centre for thinking rationally was activated.

So it is clear that we can learn how to consciously work towards changing our brains and becoming more confident.

Specifically, we can create alternative thought patterns to avoid _automatic_ _negative_ _thoughts_. These are the negative thought patterns that occur, unconsciously, on a daily basis. Changing these into more positive patterns can be a good first step in raising our confidence level.

For example, if you often beat yourself up about not being able to perform all of your tasks perfectly, try thinking that you are a really good multi-tasker. Simply thinking about yourself in this more positive light can make you more confident.

Although it's not easy to change and it may take some practice to rid yourself of automatic negative thoughts, it's certainly possible and may even be easier than you think.

### 9. Confidence comes from failing and handling it in a constructive way. 

In the first of these blinks, we saw how being less confident causes you to remain inactive and how simply taking action boosts your confidence. Sure, sometimes you'll fail, but that can be a good thing!

Unfortunately, women tend to overthink things and not to act. But we must understand that taking action is essential to confidence.

For example, instead of worrying whether your paper is good enough for a contest, just submit it. You'll likely be more confident about the next contest, because you won't concentrate on doing things perfectly. Besides, you won't have a chance at winning it if you don't submit anything.

In truth, failing can have a positive effect on confidence. By taking more action, you're bound to fail at times. But you'll also learn that failing isn't life-threatening and, therefore, you won't be as afraid to try next time.

For example, perhaps the paper you submitted won't win you the contest. But there will be other contests, and failing at one thing doesn't mean that you're a _complete_ failure.

Really, "failures" should be seen as opportunities to better yourself. Many women are afraid of failure and see it as a lack of natural talent. Instead, it should be seen as an opportunity to improve, because with every failure we learn where we made a particular mistake and how to avoid making the same mistake in the future.

Eventually, with enough ups and downs along the road, you get better at what you're trying to master and gather confidence.

For example, if you fail at a math exam, you don't need to give up on math entirely. You can choose to persevere until you achieve the results you want.

Being confident means not letting fear of failure prevent you from trying to get the things you desire. It can be a hard lesson to learn, but it can also change your life for the better.

> _"If you choose not to act, you have little chance of success."_

### 10. Final summary 

The key message in this book:

**Confidence** **is** **more** **important** **than** **we** **might** **think** **–** **especially** **in** **the** **workplace.** **Women** **are** **often** **less** **confident** **than** **men** **because** **of** **their** **genetic** **markers,** **nurturing** **and** **society's** **stereotypes** **and** **criticisms** **of** **them.** **However,** **they** **can** **also** **teach** **themselves** **to** **be** **more** **confident** **if** **they** **wish** **to** **do** **so.**

Actionable advice:

**Take** **another** **look** **at** **your** **competence.**

Take a serious look at your competence and ask yourself: how far could you progress in your career if you were more confident?

**Think** **less,** **act** **more.**

The next time you are in an important meeting and have something to say, don't sit back wondering how or if you should share your idea. Don't overthink the situation. Instead, make the decision to act on it. Even if you're met with opposition, you can learn how to deal with it better next time. If you do it enough times, the experience of speaking up will gradually increase your confidence.
---

### Katty Kay and Claire Shipman

Katherine Kay is a journalist and anchor for _BBC_ _World_ _News_ _America_ in Washington, DC. Claire Shipman also is a journalist and correspondent for _ABC_ _News_ and _Good_ _Morning_ _America_. In addition to _The_ _Confidence_ _Code_, together they also co-authored _Womenomics_.

