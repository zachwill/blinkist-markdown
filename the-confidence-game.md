---
id: 572243675ed4340003a94ab0
slug: the-confidence-game-en
published_date: 2016-05-02T00:00:00.000+00:00
author: Maria Konnikova
title: The Confidence Game
subtitle: Why We Fall For It . . . Every Time
main_color: FFE433
text_color: 736717
---

# The Confidence Game

_Why We Fall For It . . . Every Time_

**Maria Konnikova**

_The Confidence Game_ (2016) reveals exactly how con artists can strike it rich by taking advantage of some major flaws in human nature. Find out why people believe incredibly unlikely stories and ignore incriminating evidence, and discover how basic human trust and optimism can be used to a con artist's advantage.

---
### 1. What’s in it for me? Learn to spot – or become – a master fraudster. 

Upon learning about some spectacular fraud, such as, say, the Bernie Madoff investment scandal, have you ever thought, "_I_ would never fall for that. How in the world could anyone be so naive?"

Well, that's probably just what the victims of con artists used to think, before someone came along and tricked them out of their life savings. And chances are, that same _someone_ would find _your_ weak spot in a matter of minutes.

But fortunately for you, these blinks unveil exactly how confidence tricksters proceed, step by step, from spotting their victim and winning their trust to ensuring that no one will ever learn about their grand, deceitful scheme.

Clearly, this information will come in handy, regardless of whether you want to protect yourself against master fraudsters or, perhaps, plan to join their ranks.

In these blinks, you'll also find out

  * why it's probably a good thing that you can't read your partner's mind;

  * how a young boy tricked people all over the United States into sending him money; and

  * how optimism turned a brilliant professor into a fool — and a criminal.

### 2. Most of us prefer to know little about others, but con artists thrive on knowing all they can. 

Have you ever sat in a café observing other people and imagining what their lives are like? You might think to yourself that the man sitting in a corner with the messy hair is actually a famous artist.

Most of us enjoy observing others, but we also tend to avoid finding out too much about them, as this deep knowledge can have its downsides.

For example, in one experiment, psychologist Jeffrey Simpson asked married couples to watch video footage of each other discussing a difference of opinion. While doing so, they were told to write down their own feelings and what they believed their partner was feeling.

At the end of the study, the couples were asked how they felt about their spouse and their relationship. It turned out that couples who were less successful at understanding each other's feelings reported being much happier than those who succeeded.

This is because we tend to like people less if we can sense that they might find us boring, or if they are being insincere. Therefore, by keeping a safe emotional distance from others, we can prevent ourselves from seeing something we'd rather not.

On the other hand, understanding someone well enough to spot a flaw or a weakness in them is exactly what a _confidence trickster_, or con artist, loves to do. In fact, it is what leads them to be successful in their pursuits.

Take the case of Debra Saalfield: in July of 2008, Debra went to see a clairvoyant after losing her job and her boyfriend. Before Debra said a word about her problems, the clairvoyant closely read Saalfield's body language and saw that she was very vulnerable.

This allowed the clairvoyant to expertly trick Debra into writing her a check for $27,000. If she had misread her client, the clairvoyant could have easily ended up in jail. Instead, she closely observed Debra and understood how to take advantage of her emotions.

But spotting a vulnerable person isn't everything. As we'll see in the next blink, a con artist must also be able to win their victim's trust.

> _"I can spot someone's weakness a mile away. In any room, I can pick out the best target."_ — Simon Lovell, former con artist

### 3. Con artists cunningly establish a trusting relationship with the person they intend to deceive. 

Have you come across someone who can enter a room and instantly charm every person? Such charismatic people can often become great leaders or expert deceivers.

This kind of charisma is essential to a con artist's ability to appear friendly and trustworthy.

For example, the author interviewed a young woman named Joan who was the victim of a charming con artist. Joan had fallen in love with a man named Greg, an incredibly sweet man who even helped Joan by building her a new kitchen and tending to her ailing grandmother.

But something about Greg wasn't quite right. First of all, he didn't seem to have any friends or family. And when Joan called the lab he supposedly worked at, Greg's elaborate story began to fall apart. It turned out Greg's job and past education were all lies and that he'd spent two years charming Joan into believing a totally fabricated life story.

So, if you meet someone who seems too good to be true, it doesn't hurt to double check his story. He could be a con artist trying to take advantage of you.

You might now be wondering how a con artist can gain this kind of trust. Well, a good con artist will often pretend to share common traits with their victim. After all, it is natural for human beings to trust someone similar more than someone dissimilar.

We can see this in a study by psychologist Lisa DeBruine, in which participants were asked to collaborate on a project with a virtual teammate. The study found that teams were more successful when the photograph of the virtual teammate was altered to look more like the participant.

Of course, con artists know that similarity can be faked. They can inspire trust and a false sense of similarity through techniques such as mimicking another person's facial expression and voice, mirroring their body language or pretending to share common values.

Once they've befriended and earned the trust of their victim, the next step for the con artist is to set the trap.

### 4. Con artists use some classic tricks to rope people into their schemes. 

You've probably seen enthusiastic young people on the sidewalk asking you to support a cause like Greenpeace or Amnesty International. Many people will look the other way and quickly walk by because they know that it will be hard not to give a donation if they stop.

Con artists are aware of this as well. They know that once someone has agreed to a small favor, they're more likely agree to a larger request later on.

We can see this in a 1966 Stanford University study. Researchers found that stay-at-home mothers were 30 percent more likely to agree to spend two hours answering questions at their home if they had previously agreed to answer just a few questions over the phone.

This is called the _foot-in-the-door technique_ — and for con artists, it is an essential strategy.

For example, around 1900, there was a popular newspaper ad written by someone calling himself "Bill Morrison" and claiming to be a Nigerian Prince looking for pen pals. It turned out that when people took the first step to respond to the ad, they would often grant Morrison's request to send him $4.00 in exchange for some precious gems.

Naturally, the jewels never arrived and the police eventually discovered that the "prince" was actually a 14-year-old American boy. But the ad shows that once a con artist gets an initial response, they can often take advantage in more substantial ways too.

Another effective technique for taking advantage of the kind-hearted is to start with an unreasonably big request and then scale down.

This is what happened in the 1990s when England's Lady Worcester was hosting a charity auction for ethical pig farming. She was approached by an ambitious con artist who claimed to be a nobleman and invited her to be a guest at his home in Monaco.

She refused the invitation but, during the auction, she accepted his $4,000 check for a bronze pig sculpture. She explained that she would have felt guilty refusing him a second time and on a much less extravagant offer.

As you may have guessed, the check never cleared.

> _"My pitch put the victim in a haze of ether."_ — Anonymous con man

### 5. Con artists rely on their victim’s need to feel special and superior. 

You've probably seen them on TV or in the street: men acting cool and confident while wearing what is clearly a crooked toupee or hairpiece. At the time you probably asked yourself, who do they think they're fooling?

In truth, most people's self-perception isn't very accurate and con artists are keen to take advantage of this.

People become easy prey for con artists when they become fixated on an idealized version of who they think they are.

A great example of this can be seen in the 2012 case of an otherwise intelligent 68-year-old professor who came across a gorgeous Czech model online. Despite never meeting or speaking on the phone, the virtual beauty managed to convince the professor to meet her in Bolivia.

Of course, things didn't go exactly as planned.

When the enthusiastic professor arrived, he received notification that his date had to run off to Brussels for an emergency photo shoot. But, in her hurry, she had forgotten her bag, so the professor was asked to retrieve the bag and join her in Belgium.

You might see where this is headed: In Buenos Aires, the professor was arrested after it was discovered that the bag contained two kilograms of cocaine! The poor man was so filled with confidence that he never paused to question why a 30-year-old model would fall for his charms.

This kind of deception is not reserved for vain or fragile individuals; successful con artists have been known to deceive entire families.

A famous example of such a con artist is Thierry Tilly, who managed to fleece all 11 members of an aristocratic French family. By taking advantage of the family's pride in their noble heritage, he convinced them that dark forces, such as Freemasons and Jews, were out to steal their vast secret fortune.

So he suggested a solution: for safekeeping, the family must sign over all their assets and property to his name. And that's exactly what they did; Tilly had convinced the family that they were important enough to be the center of an international conspiracy.

Obviously, con artists know a lot about human psychology and how to use it for their benefit. In the next blink we'll see how that understanding gets even deeper.

> _"Cons work so widely because, in a sense, we want them to. We want to believe the tale."_

### 6. In order to fully convince an overly optimistic victim, con artists create an illusion of success. 

Do you ever see people at a casino and wonder why they continue to place more and more losing bets? It's often due to an early bet that happened to pay off, which gives the gambler a false illusion of success and encourages him to continue even in the face of mounting losses.

Con artists are experts at creating an illusion of success and taking advantage of this false optimism.

In 1889, William Miller asked friends to invest $10 in his trading business. He told them that he had insider knowledge that would guarantee a weekly return of 10 percent on their investment. Word spread of his magical scheme and soon people were lining up take part.

Little did the investors realize that there were no investments at all. Miller was simply collecting new money and using it to pay the weekly returns to the previous investors.

But why are we so eager to fall for schemes like this?

It is because many people are naturally inclined to be optimistic about the future. This makes them eager to buy into the con artist's illusion of success.

A 1990 psychology study showed that all college students overestimated how happy they would be in the next semester by 10 to 20 percent. This included their thoughts on how high their grades would be and how their relationships would evolve.

People want to believe that things are going to turn out well for them, which means they are eager to believe dubious stories that should be met with skepticism.

For example, it's likely that many art gallery owners are hopeful that they'll one day come across a huge discovery. Gallery owner Ann Freedman was one such woman, and she was willing to trust in Glafira Rosales, an obscure art dealer who offered to sell her several supposedly undiscovered works by world-famous artists such as Rothko.

Freedman was so willing to believe in their authenticity that she accepted Rosales' refusal to reveal the origin of the paintings. But, as you may have concluded, the paintings were all masterful forgeries.

### 7. Our reluctance to part with our beliefs plays into the hands of tricksters. 

Have you ever been unfortunate enough to find yourself in conversation with a racist or a nutty conspiracy theorist? If you have, you probably know how difficult it can be to convince someone that their beliefs are wrong, even in the face of scientific facts.

This is another human trait con artists can use. They know that even when people have an experience that contradicts their beliefs, they'll still tend to hold on to those beliefs and ignore the experience.

In 1957, psychologist Leon Festinger developed the concept of _cognitive dissonance_ to explain this behavior. He suggested that when our beliefs conflict with reality, it is so highly stressful that we are willing to bend reality to ensure that the two continue to match.

Festinger first discovered this behavior when he observed a cult whose members believed that the end of the world was near. They believed that only a lucky few would be saved by an alien spaceship. Eventually, the date of this Armageddon passed without incident.

So had their beliefs been disproved by reality? Not according to the cult; instead, they averted mental stress and demonstrated cognitive dissonance by deciding that their powerful meditations had prevented the apocalypse.

This is the same behavior that allows con artists to flourish: once we decide that someone is trustworthy, we will dismiss any contrary evidence that might arise.

For example, in 1919, rancher James Norfleet stayed at a hotel where he befriended a charming man who introduced him to a lucrative investment prospect. This newfound friend then conveniently misplaced his wallet, making sure that Norfleet found it, noticed all the money he had and the respectable Masonic membership card he carried.

This was enough to win the rancher's trust and assure him of the investment's worth.

Afterward, Norfleet was easily convinced to invest his money in nonexistent stocks. And even when he was told the unlikely story that more money was needed to pay off a stock exchange secretary, he still believed that his supposed profits of $80,000 were on the way.

> _"And the burnt Fool's bandaged finger goes wabbling back to the Fire."_ — Rudyard Kipling

### 8. The fact that we place great value on a good reputation helps provide cover for con artists. 

Imagine you're a police officer who just closed an important case that brought you a big promotion. Suddenly, a young clerk discovers that you've overlooked a major detail in the case. The clerk offers an interesting solution: she will hide the evidence, as long as you promote her.

Would you accept the offer? Since our reputation is one of the most highly valued parts of our identity, it's likely that many people would.

In fact, a 1997 study by psychologist Robin Dunbar showed that 65 percent of all conversations revolve around gossip. More than anything else, we talk about how other people behave and how we behave in certain situations. Obviously, our reputation is a major concern.

In another study, Nobel laureate economist Elinor Ostrom found that when people trust each other, they collaborate more successfully. Therefore, maintaining a good reputation is important to us because it can function as a shortcut to gain people's trust, even if they don't know you personally.

So, it's not surprising that we're extremely wary of gaining a reputation for being foolish. And this is another trait that con artists can take advantage of.

One classic story shows just how much our prized reputation can protect a con artist. In 1915, a con man began a rumor that Sir Francis Drake and Queen Elizabeth had an illegitimate son. Allegedly, a descendant of this son was now waging a legal battle to retrieve the treasure that the British state had stolen from Sir Drake's ship, the _Defiance_, in the sixteenth century.

Those who were given this story were also told that anyone who invested money to cover the legal fees would be generously rewarded once the treasure was retrieved. In the end, seventy thousand investors fell for this tale; yet, after it was proven to be false, none of these victims denounced the con artist to the police.

Why? They feared they would look foolish to have been taken in by such a ridiculous story.

> _"Regard your good name as the richest jewel you can possibly be possessed of."_ — Socrates

### 9. Final summary 

The key message in this book:

**Anyone can be the victim of a confidence player; many people have likely been fooled more than once in their lives without ever noticing. Con artists are masters at seeing through people, finding their weaknesses and ruthlessly exploiting them. And, unfortunately, everyone has their weak points.**

Actionable Advice:

**Get to know your weak points.**

One of the best ways to become less vulnerable to con artists is to better know and understand yourself. Con artists specialize in finding your weak points and exploiting them. So get ahead of the game by observing yourself to find out what triggers your emotions and makes you act impulsively. This way, the next time you meet someone who keeps pushing these sensitive buttons, you'll recognize what's happening and avoid getting lured in by a con artist.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Mastermind_** **by Maria Konnikova**

_Mastermind_ explores Sherlock Holmes's unique approach of observation, imagination and deduction, and illustrates how anyone can harness his method to improve their thinking and decision-making skills. To this end, the book presents a variety of simple strategies, drawing on scientific research in psychology and neuroscience, and numerous examples from the original Sherlock Holmes stories.
---

### Maria Konnikova

Maria Konnikova is an author with a PhD in psychology. She is a frequent contributor to _The New Yorker_, writing about topics such as pop psychology and culture. She lives in New York City and is the acclaimed author of _Mastermind: How to Think Like Sherlock Holmes._

