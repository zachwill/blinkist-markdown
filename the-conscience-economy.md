---
id: 56afb2f1377c1600070000e1
slug: the-conscience-economy-en
published_date: 2016-02-02T00:00:00.000+00:00
author: Steven Overman
title: The Conscience Economy
subtitle: How a Mass Movement For Good is Great For Business
main_color: 22A947
text_color: 22A947
---

# The Conscience Economy

_How a Mass Movement For Good is Great For Business_

**Steven Overman**

_The Conscience Economy_ (2014) reveals the implications that our changing, hyperconnected world has for businesses and brands. These blinks guide you through the principles and strategies that are vital for any company hoping to win over the outspoken, discerning consumers at the heart of today's conscience culture.

---
### 1. What’s in it for me? Gain insight into the new customer generation – and win their sympathy. 

The concept of the global village may sound like a stale metaphor at first. But, as you'll learn from these blinks, it's actually becoming more and more relevant, particularly if you own a business.

Through their online presence, young people are now profoundly interconnected, much like the inhabitants of a small village. And, as it would in a village, any news about your business will spread quickly — whether you are donating to an orphanage, developing great products, mistreating your employees or poisoning a river with industrial waste. 

In contrast to traditional villagers, however, the well-informed new generation of customers is acutely aware of environmental problems and social injustices occurring well beyond their hometowns. In these blinks, you'll learn about modern-day consumers' high ethical standards, as well as how exactly you can bring these young people on board as customers: stop obsessing over bottom lines, start shouldering more responsibility for the welfare of the planet and never, ever, try to lie to them. 

In these blinks, you'll also find out

  * how brands are making your life easier;

  * why, in the near future, marketing as we know it will die; and

  * why these days, even bleach comes with an attitude attached to it.

### 2. Naughty has gone out of style – nowadays, good is sexy! 

We all know the cliché of the ruthless businessman who will go to any lengths to cut costs and maximize profits, regardless of the ethical implications. It was during the twentieth century that this stereotype reached its peak. 

Back then, it was cool to be bad if you could earn a fortune doing it. Bad behavior was admired in celebrities and entertainers. Mass media and its readers loved nothing more than to worship those that acted naughty. 

But times are changing. Young consumers today know and care about the impact of their lifestyle on the environment. Working conditions and quality of life around the world are challenges that mobilize individuals, companies and even celebrities. It seems as if we're all determined to make the world a better place; where did this change in perspective come from?

Well, this _is_ the information age, after all! Thanks to the internet, as well as regulations for honest product labelling, shoppers today have loads of information about everything they buy. Whether it's finding out how products were manufactured, where factories are located or even how energy efficient production processes are, the modern consumer buys products that are in line with their morals and values. 

So, an elegant coat produced by a prestigious brand with stunning marketing at a great price won't look so great once buyers discover that it was produced using child labor. In fact, most conscious buyers are likely to go online and share this kind of information on social networks, condemning brands and organizations as they see fit. 

In order to win the favor of this new generation of discerning consumers, companies have no choice but to start _genuinely_ caring about humanity and our planet.

> _"Business badness was more than profitable, it could even be admirable, as long as you didn't get caught."_

### 3. Technological developments make us more connected, and more empathetic too. 

How long does it take for a radical new invention to become a functional element of our daily lives? Sociologists have discovered that societies take around 40 years to wholeheartedly embrace a given innovation. 

The revolutionary electric lightbulb was invented in 1880, yet it wasn't until the 1920s that large public spaces were lit by tungsten bulbs. The world wide web was invented in 1990, but only 40 percent of the world's population uses the internet today. Even so, it has already profoundly changed the way we live and interact. 

Imagine what the world will be like when the internet has truly been integrated into society. We've already seen some radical examples of how powerful the internet can be. For example, in January 2011, it allowed young people in Egypt to organize and stage a revolution that shook the nation. Meanwhile, internet users on the other side of the world were able to follow that very revolution in real time. 

We're only two decades into using the internet and still have much to learn about the advantages and implications of living in a hyperconnected world. The intersection between new technologies and traditional morality has proven to be especially promising. 

Our conscience goes hand-in-hand with our connectedness. We aren't born with a conscience; rather, it's when we connect with others and learn of their ideas, hopes and fears that it truly emerges. 

The way we behave impacts others' lives, which is a basic lesson we learn at our church, mosque or temple, through our parents or teachers and in the playground with our kindergarten classmates. These experiences are what allows our conscience to develop and grow. 

In our information age, we can also see how our actions have implications for people all around the world. The more connections we experience, the stronger and more advanced our conscience grows. This has resulted in a new _conscience culture_ shared around the globe. In other words, a culture of empathy has taken the world by storm.

> _"I am who I am because of you and because of we."_

### 4. The more customers are aware of global challenges, the greater the pressure on companies to provide solutions. 

Whether you're browsing news websites or chatting online with friends from countries affected by natural disasters, it's hard to ignore the fact that the environment is very fragile. Gone are the days when climate change was argued to be a myth.

Dried-up riverbeds, depleted reservoirs and rapidly melting icebergs simply can't be denied. Nor can our role in these problems — today we know that each time we fly, we're doing our bit to exacerbate global warming. 

As the world around us seems to disintegrate, our internal health is also suffering. More and more people around the world suffer from illness and obesity because they depend on cheaply manufactured food products made with excessive sugar and unhealthy fats.

Global warming and human health are just two examples of pressing problems that constantly make headlines online and in mass media worldwide. As a result, we're able to learn quite a bit about these issues. The more people learn, the more they demand that solutions be found; this is something every business should keep in mind.

The modern consumer cares about how their lifestyle impacts the world on a local, national and even international level. In turn, these consumers care about whether the businesses they buy from harm the environment, manipulate customers or mistreat employees. Unless they change their ways, unscrupulous companies are facing total elimination in the new conscience economy. 

Ethically responsible companies are those that will thrive over the coming decades by ensuring that corporate culture prioritizes strong values and good causes over their own profits.

> _"Deal with change before change deals with you." - Post-it note on the computer of Louis Rossetto, co-founder of Wired magazine_

### 5. Conscience culture is superseding the culture we used to know. 

Culture is a complex thing. Our world consists of many different cultures, though the way they differ is often quite subtle. Consider the United States and the United Kingdom. Outwardly they may share several similar characteristics, including the same language. 

But the ways in which they differ are crucial. In the United States, an ostensibly upbeat manager with a cheerleading attitude may be praised for his leadership skills; conversely, in a British context, people might consider the same behavior to be excessive or strange. Different generations also have their own cultures, as illustrated by the vast cultural differences between baby boomers and their children, or even the deeply differing experiences of Generations X and Y. 

Yet another clash at the heart of society today is the one between conscience culture and established global culture. Like the established culture, conscience culture emphasizes self-actualization; but the way each culture views the self is radically different. 

In conscience culture, we're so connected to others around the world that the self is seen as part of a collective. We don't seek to self-actualize alone, but together. In conscience culture, improving the lives of others and improving your own life are seen as equally valuable. A prevailing belief has emerged, positing that what's good for other people is also good for you. 

The contrast between our established culture and the emerging conscience culture is starkest when it comes to environmental protection. While previous generations grew up believing that they could count on the environment to provide for their needs, younger generations today have already witnessed how fragile and volatile the environment has become. 

This, in turn, informs their purchasing decisions, which explains the overwhelming trend toward eco-friendly brands and businesses. Now that we know about the conscience culture, and that businesses have to adapt if they want to survive, let's take a closer look at the changes companies should start making.

### 6. Today, brands need to communicate the values of the conscience economy – or fade away. 

Connecting with other people and understanding their needs is shaping both our conscience and the way we spend our money. 

Not convinced? Say you've got the choice to buy from one of two brands. The first is famous for its stylish design, while the other is an influential supporter of sustainable technology _and_ has also won awards for its design. Which one would you go for? Now, imagine you're one of these brands. Which one would you prefer to be?

Brands help us orient ourselves in a sea of buying options. Imagine shopping in a supermarket where products are all the same shape and color, completely devoid of brand names. You'll definitely spend a lot of time reading labels! 

Brands allow us to recognize and form attachments to manufacturers that are familiar and trustworthy. This way, we can head straight for them in the supermarket and make our shopping a lot more efficient. 

And it's not always reason or rationality that lie at the heart of these brand attachments. In fact, 80 percent of our decision making is actually based on our emotions, whereas only the remaining 20 percent is rational. Great brands naturally seek to trigger our emotions by developing an image that evokes certain empathetic emotions. 

This is particularly important when it comes to conscience culture. The conscious consumer will seek out brands that resonate with her and her personal values. Likewise, people will reject any products or services associated with values that they don't support. 

Companies need to understand this new culture and attach an emotional message to their brands — even if they're just selling bleach!

Imagine a bleach brand that confidently claims to protect groundwater because it's completely biodegradable — in addition to fighting diseases by killing germs. Plus, the same company supports health-related social enterprises abroad. If a brand of bleach can transmit this information to you, will you buy it or the brand next to it on the shelf that merely says "whiter than white"?

> _"In the conscience economy, there will be no place for brands that don't sum up a set of . . . inspiring beliefs."_

### 7. In the conscience economy, a corporation’s social engagement is at the heart of its business. 

Like it or not, companies are an integral part of society. We work in companies, we buy from companies and companies use our natural resources. As it happens, companies that focus on giving back to society are often those that consistently thrive. 

The concept of _corporate social responsibility_ (CSR) has actually been around for a long time. It has its roots in the industrial revolution, when it became clear that factories were forcing inhumane working conditions on their employees. 

In the nineteenth century, William Blake's poetry famously made reference to England's "dark Satanic Mills" to raise awareness about the cruel exploitation of workers. Further writers criticized the effects of the industry's "explosive growth."

Eventually, the government responded. Businesses were at one point required to prove the social usefulness of their factories in order to receive a charter of incorporation. Much later, in the wake of World War II, businesses started to think about specific areas where they could contribute to rebuilding the country. 

But it wasn't until the 1970s that CSR became a common aspect of business strategy. Even then, it was seen as an additional tool, a nice thing to do alongside real business duties.

This has changed in today's businesses. In the conscience economy, CSR is at the heart of business operations. As a result of the growing awareness of social problems, businesses that fail to support social issues risk damaging their reputation. And, of course, without CSR initiatives ensuring that natural resources are used sustainably, most businesses will eventually run out of materials to work with!

In this way, CSR is a vital strategy that allows businesses to invest in the society that supports them, as well as their future as a company. Eventually, the very concept of CSR will be seen as outdated, as it will instead become an indelible part of all successful businesses.

> _"And what was once called social responsibility or good corporate citizenship simply becomes good business."_

### 8. Marketers must become matchmakers in order to thrive in the conscience economy. 

Conscience culture has changed a lot for today's companies. But there's one branch of business that's facing a complete revolution: marketing. 

Up until now, marketing was driven by manipulation. Customers had to be convinced to buy, and marketers used the 4 Ps to do this: designing an appealing _product_ **,** _placing_ it just right to attract as many customers as possible, naming the right _price_ and _promoting_ the product with catchy slogans or moving commercials.

Unfortunately for marketing teams, the conscience economy means things won't be quite so easy in the future. Marketing must become more interactive, more accountable and much more innovative. At the core of marketing in the conscience economy lies _matchmaking_, or the ability to create great fits between people's needs and business interests. 

An important part of this matchmaking role is identifying relationships that build business value. Whether it's between customers and employees or leaders and innovators, marketers must understand what people want from their company. This necessitates a new set of competencies, or the five Cs: _context, conversation, clarity, cohesion_ and _creating_ reasons. 

First, matchmakers need to adapt their marketing to the customer's _context_, by considering their mood, location and circumstances. For example, if it's forecasted to rain heavily, an umbrella brand may send customers a friendly alert about it, and offer to deliver umbrellas straight to their door. 

Matchmakers should also spend time _conversing_ with customers to learn more about their needs and values. This gives them helpful information that can then be analyzed and shared with companies, giving greater _clarity_ about what customers want. 

Next, matchmakers must ensure that there's _cohesion_ between prospective companies, the brand image and the company's purpose. Finally, matchmakers should encourage everyone involved in the company, whether buyer or employee, to share meaningful stories about their experiences with the product, _creating_ more and better reasons for new conscious customers to join in.

### 9. Final summary 

The key message in this book:

**Consumers today care about social and environmental challenges facing the world, and expect the brands they buy from to understand these challenges as well. In order to meet the needs and expectations of today's new generation of intelligent, inquisitive buyers, companies must demonstrate that they share their customers' values and concerns.**

Actionable advice: 

**Involve your employees in corporate changes.**

The next time you want your company to change its practices and policies to make sure that it becomes more fair or sustainable, involve your employees right from the start. Ask them how they think about the problems you want to address, such as global warming, and discuss how the company could counter such problems. This way, instead of resisting change, they'll be open to it and may even make their own suggestions or contributions. 

**Suggested further reading:** ** _Work Simply_** **by Carson Tate**

In _Work Simply_ (2015), author Carson Tate draws from her own career experience to show you how to become more productive. By understanding your own productivity style, you can make lighter work of the ever-growing pile of tasks and achieve your life goals.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Steven Overman

Steven Overman is a marketing specialist and a renowned public speaker who recently joined Kodak as Chief Marketing Officer. In addition to founding the marketing consultancy Match & Candle, Overman was one of the first employees at _Wired_ magazine.

