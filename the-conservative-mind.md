---
id: 555a05003834360007130000
slug: the-conservative-mind-en
published_date: 2015-05-18T00:00:00.000+00:00
author: Russell Kirk
title: The Conservative Mind
subtitle: From Burke to Eliot
main_color: 706B55
text_color: 706B55
---

# The Conservative Mind

_From Burke to Eliot_

**Russell Kirk**

_The Conservative Mind_ (1953) offers insights into the axioms that underpin modern conservative thought by looking at conservatism's historical roots.

---
### 1. What’s in it for me? Discover the roots of conservatism and how the movement has evolved. 

From ancient Mesopotamia to the vast Roman empire, many ancient societies believed in a divine order, with gods and rulers at the top of the power pyramid, rewarding those who are subservient and punishing those who rebel. If this order is disrupted, immorality follows and society collapses.

Crucially, not just anybody is suited to safeguard this order. It takes an extraordinary individual to lead. Whether they be tribal kings, caesars or modern-day politicians, some people are just better suited to be leaders than are others. The gist of this social order? We are not all created equal.

The conservative political movement shares many of these views. Too sudden changes can lead to societal chaos; if there is no God to keep our actions in check, how can we act morally? And if everyone is created equally, who should lead?

In these blinks you'll learn

  * why for a conservative a democratic system is misguided;

  * why scientific studies to predict human behavior are always misleading; and

  * why property for the conservative is power.

### 2. A conservative is guided by a moralistic higher power. God exists and influences everything we do. 

Conservatism as a movement didn't really come into being until 1790, with the publishing of Edmund Burke's _Reflections on the Revolution in France_.

Burke's book was the first to make people aware of the arguments and convictions of conservatives. While many other thinkers have since added their own contributions, conservative thinkers all share one thing in common: strong religious beliefs.

Conservatives believe that there is a higher power which manifests itself in us and in our actions. For most conservatives, this higher power is the Christian God. They believe that the presence of God can be felt inside each individual.

According to Burke, reason, revelation and an assurance beyond the senses all strongly signify that God both exists and is omniscient. After all, so many people believe that God is real; this must mean that God exists. If not, where could these beliefs have originated?

God's will can be seen through human history as it has developed under God's divine influence. Because God influences our lives greatly, every thought or action that endures the test of time (like societal traditions) is very likely to be an expression of God's will. Otherwise, such thoughts or actions simply could not have survived rapid and violent societal changes.

Not only does God exist, but also we need to constantly remind ourselves of God's influence. Life would lose its meaning if we ignored the influence of a higher power.

In the conservative mind-set, God's influence extends over all our actions. Say you entered into a business contract. You suddenly realize that you could break the contract without anyone ever finding out.

If this were so, then the _only_ thing that might stop you from breaking your contractual promise is the belief that the document represents an even higher form of promise, that is, something divinely sanctioned by God. Breaking the contract then would be deeply, morally wrong.

> _"Either order in the cosmos is real, or all is chaos."_

### 3. A conservative does not believe in the call of the French Revolution: “All men are created equal.” 

Edmund Burke's book on the French Revolution took to task many of that historical event's founding philosophies. For the revolutionaries, all men are created equal; yet conservatives beg to differ.

It is true that people are, in general, different from each other. There is no arguing that we have many distinguishing features, such as gender, age, strength, morality, intellect and wealth.

It's easy to see how these individual features could have a huge impact on a person's life. Somebody who is calm and has a steady hand, for example, will be a far better surgeon than one who is clumsy and easily stressed.

A conservative believes that knowing that our differences affect our everyday lives and trajectories so drastically, it is then harmful and even deceptive to say things such as, "everyone is equal." All this does is give people false hopes, and obscure reality.

Let's say you have a friend who is a mediocre actor. As you believe that all people are equal, you tell him that he will surely win an Oscar someday. After all, everyone has the same chance of success.

Your actor friend is encouraged and spends all his time and money going to auditions and trying to get work as a professional actor. Yet no matter how hard he tries, he has no chance, as he can't act!

Looking at it another way, different types of soil are well-suited for certain kinds of plants and ill-suited for others, despite being made from the same earth. To say that all soil is equal and that any plant can grow in any place is just ignoring reality!

These are the foundations of conservative thought. The following blinks will examine how these ideas guide a conservative's feelings toward democracy.

### 4. Equality is the death of progress. Ability needs to be rewarded; an equal society offers no incentives. 

Using the knowledge that each person differs in their abilities as a starting point, conservatives propose that striving for absolute equality is not only misguided, but also dangerous.

History has shown that progress stems from a desire to achieve a better-than-average life, which is only possible if there is inequality in society.

It's important to understand that the basis for such progress is not just work but _ability_, a thought first introduced by W. H. Mallock's _Labor and the Popular Welfare_.

Anybody can work, as following directions for a task without asking why or how is easy. _Ability_, on the other hand, means thinking of new and better ways to organize and do work.

While work is important, work itself doesn't lead to progress without the guiding hand of ability.

And while we all might work, engaging in work and using our abilities is linked to a desire to live _better_ than the average — which creates inequality.

Systems that impose absolute equality, in which each person is treated the same regardless of the nature or quality of her work, provide no incentive for an individual to use her abilities to the fullest.

Why should you try to invent new technologies or make improvements to the system if you'll earn the same living even without putting in all that extra effort?

People who are not rewarded when they use their abilities soon become frustrated and simply choose not to employ their talents. This is bad for "normal" workers too, as such people never get to benefit from progress either, such as new machines that would have made work easier.

### 5. A civilized society only exists when a natural aristocracy steps up and leads the masses. 

Many thinkers who praise the idea of equality have also argued for the establishment of a classless society, in which no societal group has more power or influence than others.

In contrast, a conservative believes that such a society is not only impossible but undesirable. People need leaders, and some individuals are better suited to lead than are others.

Let's say you are part of a group of 100 people, tasked with deciding what to eat for dinner. You put the decision to a vote. Everyone can vote, and all votes are valued equally. Thus everybody in the group has equal influence, and everyone is content with their status.

But what happens? During negotiations before the vote, ten people step up to shape the discussion and heavily influence other people's opinions as well as their votes. An advocate for steak and mashed potatoes, for example, is charismatic and able to influence others to join her camp. 

Conservatives believe in a _natural aristocracy_, or the tendency for a smaller, leading class to develop within societies to lead the larger group. Without such leadership, society is left with _natural equality_ — which for a conservative, translates to savagery and constant danger with no institution providing security for the masses. 

To modern conservatives, however, a member of the natural aristocracy isn't determined by birth as it once was in societies ruled by monarchs.

According to early conservative thinker and American founding father John Adams, many other factors aside from birth can play an important role in determining why people might follow a certain leader. Adams suggests popularity, eloquence, cunning, wealth or good fellowship as some examples.

In essence, an "aristocrat" is just the name a conservative would give to an individual who is (by virtue of his abilities) well suited to lead others.

### 6. A conservative isn’t a democrat, as equal voting power misrepresents what’s really good for society. 

Even though democratic systems of government are held up as an ideal in our modern times, for a conservative, democracy needs to be judged on its merits, not its aspirations.

A conservative's view on democracy, as it turns out, isn't exactly positive. Conservatives believe that democracy is based on the wrong ideas of what is important for society.

While a democratic system aims to give every individual an equal voice with the opportunity to vote, conservatives believe that equal involvement isn't what determines good government. Rather, the things that matter most are that the public is represented and that the government functions well. 

Importantly, in a democratic system, the representation of important interests is not secure when left to the majority, as the majority _may not know what's important_.

British Prime Minister George Canning summed up this idea succinctly, saying that men shouldn't seek to govern themselves, but instead, seek to be governed well.

In a democracy, different societal groups aren't represented according to their importance but instead by their voting power. As a result, groups of people that are crucial to a properly functioning society can easily be outvoted when their numbers are small.

As an example, people who depend on the government for financial support are greater in number than the few who actually finance social welfare programs through the payment of taxes.

Because of their superior numbers, this dependant group could use their voting power to pass laws that would lead to the financial ruin of the government in the long term.

### 7. Property is power, and needs to be adequately represented in modern government. 

As we've seen, conservatives consider democracy to be merely a form of "nose-counting" that doesn't take into consideration the things in society that truly matter.

One of these important things is property. Indeed, property has always been closely linked to power.

Historically, whoever has access to the most land often has the most power in society. Landowners with vast holdings can produce goods and give people space to live in exchange for services.

It is impossible to separate property from power; when property is transferred to another person, the power moves with it.

In the past, leaders concerned about the power their subjects gained through property could seize that property and bequeath it to other, more loyal subjects. But those leaders couldn't destroy the power and influence that was inherently connected to property. Soon, those new owners would too become powerful, even if they had no special abilities.

In sum: property and power are one in the same.

Accounts of ancient societies living peacefully without any notion of personal property, and claims made by early liberal thinkers such as Rousseau have simply never been proven.

As long as there has been the notion of property, it has been attached to power. And in turn, such power should be adequately represented in government.

In American democracy, densely populated cities and larger urban areas often have the most influence on a government's actions.

Yet as a result, the millions of square miles that aren't as densely populated but are still necessary for crucial activities such as food production, for example, are largely ignored.

Clearly, conservatives are unsatisfied with the current state of government and society. The final blinks will look at how a conservative would go about changing things to suit his ideology.

### 8. Abstract concepts and numbers are unable to adequately describe the human condition. 

We live in an age obsessed with measurement and numbers. But can you really capture the essence of human behavior with a number or a formula? Conservatives say no, you can't.

Humans are complicated creatures and can't be summed up with a statistic. And you certainly can't use those values to predict future behavior.

As we are all different, there can't be one formula to account for _everyone's_ behavior. A reliance on calculations requires us to oversimplify human activity, and as a result causes us to overlook important things or be surprised when people don't act according to predictions.

A conservative will say with confidence that people who claim to have made the calculations to predict future behavior will usually be wrong, a fact that history has proven time and time again.

Utilitarians, for example, argue that it's possible to calculate the best possible solution to any given situation by assigning values to the joy and pain attached to each action. One simply adds the joy-values and subtracts the pain-values, and arrives at a solution that contains the most joy.

The problem, however, is that such solutions often end up being at odds with what many conservatives feel is morally right.

The events of the French Revolution offer another example of misguided action. Liberal thinkers at the time claimed that a rebellion against the king and the aristocratic system was a necessary step toward establishing an egalitarian system based on freedom and democracy in France.

Such thinkers heavily criticized Edmund Burke, who in his crucial conservative-thinking book, was highly skeptical of the revolutionaries and their intentions. In the end, his criticisms proved correct, as the revolution devolved into mass trials and executions.

### 9. A conservative wants to change society, but only gradually and not as a result of hasty decisions. 

Conservatives aren't just political thinkers; they're interested in changing society to reflect their beliefs, too. Yet while societal change is inevitable, a conservative believes it shouldn't be rushed.

A common accusation is that conservatives are stuck in the past and work to prevent change. This is simply untrue. Even the staunchest conservatives would agree that there are situations, such as in times of war or technological advancement, in which the state needs to adapt.

Yet a conservative would remind us that any state is the result of generations of planning and building, and as such shouldn't be changed in too abrupt a fashion.

One of the basic social contracts is that the younger generation looks after the older generation, for example. A swift overhaul of services such as the social welfare system could lead to mistrust or a sense of uncertainty regarding security in old age.

Thus states should wait until leaders are certain that a change is both desired and in society's best interest before putting any plan into action.

Britain offers a classic example of how conservatism can lead to moderate yet significant change. The country's strong conservative party resisted sudden changes, and in doing so, saved its people from violent revolutions or civil war (while much of Europe was in turmoil) during the entirety of the nineteenth and twentieth centuries.

And although the Crown and the Church of England were often criticized (and as an indirect result, have less power today), their power at the time wasn't crushed violently, partly because of the support and regulation from conservative thinkers. Had such power been suddenly overthrown, it could have lead to chaos and revolution.

In the end, the most important task of any conservative politician is to guide, not halt, social movements as part of a long-term, controlled process.

> _"[...] change may not be salutary reform: hasty innovation may be a devouring conflagration, rather than a torch of progress."_

### 10. Final summary 

The key message in this book:

**Conservatism has a long, rich history that has influenced both ancient and modern times. Based on a belief in God and a natural aristocracy, as well as a deep-seated mistrust of scientific approaches to predicting human behavior, conservatives continue to play an important role in the controlled process of social progress.**

**Suggested** **further** **reading:** ** _One Nation_** **by Ben Carson**

_One Nation_ (2014) outlines what America should do to stop its current decline and to once more become the most prosperous and successful nation in the world. These blinks share recommendations that are based on faith, common sense and the values upon which the country was founded.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Russell Kirk

Russell Kirk was an American author, historian and political theorist, whose book _The Conservative Mind_ was hugely influential in shaping the conservative movement following World War II. He wrote a number of works of fiction as well as other nonfiction works, such as _The Roots of American Order_.

