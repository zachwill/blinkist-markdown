---
id: 5e64dcec6cee0700064b99a8
slug: the-courage-habit-en
published_date: 2020-03-09T00:00:00.000+00:00
author: Kate Swoboda
title: The Courage Habit
subtitle: How to Accept Your Fears, Release the Past, and Live Your Courageous Life
main_color: None
text_color: None
---

# The Courage Habit

_How to Accept Your Fears, Release the Past, and Live Your Courageous Life_

**Kate Swoboda**

_The Courage Habit_ (2018) takes a look at the role fear plays in our lives and offers a four-step program for becoming your most courageous self and following your dreams. Using inspiring examples and practical tips rooted in cognitive behavioral therapy, or CBT, and acceptance and commitment therapy, or ACT, it outlines ways that everyone can cultivate the courage they need to change their lives.

---
### 1. What’s in it for me? Learn how to confront your fears and live a more courageous life. 

Have you ever woken up and thought, "Today's the day I'll do things differently," but then fallen prey to your own habits and anxieties? Maybe you want to switch careers, improve your romantic relationship, or take up a creative pastime, but you can't seem to find your footing as you take your first steps.

You're not alone in this. In her experience as a life coach, Kate Swoboda found that fear is the main hurdle we all need to clear before we can manifest our true dreams and desires. She's also seen that _fighting_ fear is unrealistic — then you're just pretending it doesn't exist. These blinks will show you how you can live a more courageous life by investigating your deepest fears, changing your emotional routines, and reframing the stories you tell yourself.

In these blinks, you'll learn

  * why imagining your most "liberated day" can set you on the path toward change;

  * why you should focus on interrupting your _cue-routine-reward_ cycle; and

  * how to effectively engage with your inner critical voice.

### 2. Living courageously begins with pinpointing the role fear plays in your habits and routines. 

The call to transform your life may have come at you unexpectedly. The truth that prompts it — perhaps you are unhappy with your career, a relationship, or something else — may feel inconvenient. It may prompt stress or feelings of self-doubt. The whole idea of change may seem impractical or uncomfortable.

These reactions are entirely normal. It may surprise you, though, to learn that they're all rooted in the same thing: fear.

They may not look or feel like fear, but fear doesn't always look the way we expect it will. Sometimes it manifests as other feelings. When we decide to make a radical change without first acknowledging our fear, it can be easy to use the feelings that arise from it to rationalize not following through.

Let's say you'd like to change careers. But because you've already invested a lot of money and time on an advanced degree in your field, starting over seems illogical. So you keep puttering away at your job even though you increasingly dread going to work each morning.

Or maybe you have a nagging feeling that something is missing somewhere else in your life. The feeling is getting hard to ignore, but reversing course or drawing a totally new map feels incredibly intimidating. Why should taking a new direction seem so scary, though?

Well, the brain prefers comfort and predictability. To get these, it floods your body with feelings of fear and anxiety when its routines are challenged. Likewise, when you choose the predictable route, the brain rewards you with the feeling of relaxation.

This is ultimately why facing your fears and making changes are both so difficult. They require patience, as well as sitting with the feelings that make you uncomfortable. Trying to fight fear by ignoring it or relentlessly telling yourself to "think positive" are methods that simply don't work. 

So what can you do instead?

You can choose to practice courage by engaging with your fear, and giving yourself permission to ditch the status quo and figure out a better way to live. One way to do this is by reconsidering your approach to fear by challenging old habits and forming new ones. But before you're ready for this four-step process, the _Courage Habit_, you need to do a few other things first, beginning with embracing your authentic desires.

### 3. Honestly naming your true desires allows your most courageous self to emerge. 

So what is the Courage Habit? It's a technique that the author has developed to help people take their own desires seriously, confront their fears, and become their most courageous selves. It's important to realize, though, that this isn't someone you need to "turn into." You simply need to clear away the barriers that have inhibited the growth of that person.

To prepare yourself for the first step in the Courage Habit, you need to do a few things. First: figure out what you truly want.

One way to do this is by envisioning your _Liberated Day_, an ideal day in which fear doesn't hold you back. Describe the day in lots of detail. How would you feel waking up? And settling back into bed at the end of the day? Consider how your most courageous self would approach every area of your life.

Remember, the Liberated Day is all about figuring out what you really, truly want. So forget external cues, like validation from others. Pay close attention to internal cues instead, which guide you towards the things that make you feel the most authentic.

When Ellen, one of the author's clients, mapped out her Liberated Day, she realized that her idea of a good life didn't mean continuing in her career as a sales rep. Instead, she realized she was truly happiest when she was reading and discussing books with other people. Ultimately, she decided to quit her job, live more modestly, and go back to school to pursue a master's degree in literature.

Once your Liberated Day has given you permission to dream big, it's time to get practical. Narrow your focus to three _Primary Focus_ goals — three areas of your life that you'd like to change. These goals can be both tangible, things like travel to South America, and intangible, like understanding why your marriage didn't work out.

Hone in on these goals by asking yourself: What makes me feel curious or excited? Where do I want to be in six months? What patterns am I tired of? Finally, think about how your goals could benefit others. Consider how you might be a more supportive friend or family member if you could reconnect with yourself and your own happiness.

As you consider your Primary Focus, remember that stepping outside your comfort zone and trying new things is key — even if a certain outcome is not guaranteed. You'll definitely make mistakes. But building resilience is a necessary part of the process.

### 4. You can’t eradicate fear, but you can quash its power over you by claiming your fear routine. 

For most of us, fear is the greatest obstacle to following our dreams. So why can't we just decide, once and for all, that we won't be afraid?

Well, as shame researcher Dr. Brené Brown says, "you can't selectively shut down emotion." But before you can begin to learn the Courage Habit, you need to change how emotions affect you, through something you deal with every day: your habits.

According to Charles Duhigg's _The Power of Habit_ (2014), our emotional lives are hugely influenced by our habits, which can be broken into a three-part _cue-routine-reward_ cycle.

The _cue_ is your initial feeling. When a cue causes you stress, your brain is actually hardwired to trigger a behavior, or _routine_, that offers you the reward of decreased tension. For example, let's say you go to a coworking space in order to focus on your writing. But when you see other writers confidently at work, you're stricken with self-doubt. In this case, self-doubt is your cue, and your routine is leaving the situation. The _reward_ is the feeling that you get from that routine; if you leave the coworking space, you feel momentary relief.

Our example above isn't just about habitual behavior, though — it's a _fear routine_ in action. There are four predominant fear routines:

The _Perfectionist's_ fear routine leaves him __ chronically dissatisfied, irritated, and overwhelmed. He takes too much on and has trouble relinquishing control, but external validation and checking items off a to-do list give him a sort of high.

The _Saboteur_ 's routine involves taking two steps forward and one step back. The feeling of being "tied down" erodes the initial excitement she has about things, so she has a hard time sustaining her efforts and often expects big returns on small investments of energy.

The _Martyr's_ routine is about being a people-pleaser. Service to others is his main life focus; he even tells himself that he doesn't have time to follow his dreams because family or friends need him.

The _Pessimist's_ routine leads her to believe that nothing works out for her, so she doesn't see the point in trying. She refuses to ask for what she wants because in her mind, opportunities for change just don't exist. 

Most of us mix and match elements of each of these routines, but tend to fall back on one of them. Identifying yours will empower you to start interrupting the routine part of your cue-routine-reward fear pattern, and ditching the habits that don't serve you.

### 5. Once you identify the physical sensation of fear in your body, you can take steps to change your fear routine. 

Identifying what you really want and working on your habits are big tasks for anyone. But that doesn't mean that once you've done those things, it's time to rest — far from it. Instead, you're ready to finally take your first step towards the Courage Habit.

That's _accessing the body,_ which helps you identify the fears that are getting in your way. This is a type of mindfulness practice that gives you the space to pause and control your cue-routine-reward process.

Begin to access the body by performing a _body scan._ Start this at your feet. Ask how they are, phrasing this however you like — maybe asking "No pressure, just wondering — what's going on today?" or "What do you need?" Move slowly up your body this way, from your feet all the way to your head.

During this practice, notice the sensations that arise and try to translate them into emotions. A tightening in the body or a churning sensation in the stomach, for example, might be symptoms of fear.

The physical manifestations of fear can be much stronger than this, too. They can be so debilitating that we continue to take the familiar path, even if we want to change. This might mean that you struggle with accessing the body at first.

Janelle, one of the author's clients, certainly did. A mother-of-three multitasker with a Martyr's fear routine, Janelle was skeptical of mindfulness practice. Nonetheless, she identified a pit in her stomach. When the author asked what it was saying to her, Janelle realized that the sensation was saying, "You're not being a good mother." Recognizing this fear would later help Janelle enormously as she confronted her cue-routine-reward loop.

If a body scan doesn't work for you, there are many other ways to access the body. Try dancing to music, running, practicing yoga, having sex, or visualizing yourself in a happy state and noting what that looks and feels like.

If you're worried about intense feelings overwhelming you and derailing your day, try "putting a container around the experience" when you access the body — that is, plan to release the feelings in a limited amount of time and space. You could set a timer in another room to gently bring you back to reality, go for a walk afterwards, or schedule a call with a friend. Body-based practices can bring up the strongest sensations of fear in the body, but sitting with them allows us to intervene in our most detrimental cue-routine-reward cycles.

### 6. You can engage with your inner critical voices without letting them intimidate you. 

Everyone has one: a critical voice that pipes up whenever they don't feel capable. Your _Inner Critic_ confirms your worries that you aren't good enough. It stops you in your tracks before you can summon the courage to make a major change in your life.

We tend to deal with our Critic in lots of ways — by ignoring it, trying to placate it, or fighting with it — but these are all just temporary fixes. A much better approach is the second step of the Courage Habit: investigating your Critic.

Let's consider an example: Taylor bravely left salaried employment to work as a freelance photographer, but found that her Critic kept getting in the way as she tried to grow her business. Its insults and unflattering comparisons were cruel and anxiety-inducing, but it seemed to make logical points, too. Taylor worried she'd grow complacent without the Critic's feedback.

But it's possible to listen to your Inner Critic without granting it free rein over your life choices. Try writing down just what your Critic is saying, really capturing its voice. Instead of settling for something general, like "I'm afraid I'll fail," get the Critic's actual words down: "You'll never measure up to others. Why try?"

Next, think back to the collection of goals that comprise your Primary Focus. What does your Inner Critic say about your ability to make that focus a reality? Consider this, then access the body afterwards to keep clear of your fear routine.

Like Taylor, you might find it difficult to accept that your Inner Critic is undermining you. After all, listening to it is part of a familiar and comfortable cue-routine-reward cycle, while trying something new is risky and uncertain.

It's easier to engage with your Critic when you can get it to drop its defenses using a tool called _Re-do, please._

Here's how it works: Whenever your Inner Critic says something disrespectful, ask it to rephrase its statement or demand. Say: "Re-do, please. I'm listening, but only if you can say things respectfully."

Re-do allowed Taylor to interrogate her Critic's real feelings, which turned out to be fear of experiencing financial hardship. This stemmed from childhood, when Taylor's parents tended to take their financial stress out on both her and each other.

Understanding its deepest, darkest fears freed Taylor to cultivate compassion for her Critic. That's important, as our Inner Critic is part of us too. The way to heal and liberate ourselves from its control is by offering love and compassion.

### 7. Reframing limiting Stories helps you see the possibility in your life. 

Once you start having conversations with your Critic, you'll start to realize just how powerful the stories we tell ourselves are.

Everyone has these _Stories_ — they're the lens through which we see the world, and while they aren't necessarily positive or negative, some are more productive than others. _Reframing limiting Stories_, the third step in the Courage Habit, helps you prevent unhelpful Stories from dominating your life.

Carolyn, one of the author's clients, seemed happy with her lifestyle. She was free of the shackles of adulthood and responsibility, bartering her coding skills for any goods she needed. But she was also deep in debt, unable to pass a credit check and get an apartment. Normal employment was impossible, as the IRS would garnish any declared wages.

Finally, a friend offered her a job that paid over $100k — seemingly the answer to her problems. But Carolyn was adamant about not settling down, which meant, she said, never having any fun.

Before she made a final decision, though, she took time to question her Stories. Reframing them, she came to see that her anti-commitment Story was really the Saboteur fear routine. It didn't really make her happy — it just felt familiar, and less stressful than trying something new. Commitment, she saw, might mean giving up fun for some people, but that didn't have to be her truth.

Articulating her Story helped her break her pattern of self-sabotage. She took the job, stuck with it, and finally managed to pay off her debt. In that way, the job actually allowed her _more_ independence — it just came with added responsibility.

To identify your own Story, consider an area in your life where you feel stuck — perhaps as it relates to your Primary Focus. Next, finish the sentence, "I'm frustrated because…"

What does your Critic say about the progress you've made toward your Primary Focus? Write down the answer. Now, try to articulate the Limiting Stories you're telling yourself: "I'm not capable of changing my life," for example. Then you're ready to reframe the Story by stretching it in a more positive direction. The statement above, for example, can easily stretch to become "I'm willing to look at options for change," — a positive, actionable Story.

It's important to note that reframing Stories isn't the same as making positive affirmations. Affirmations are often empty — and sometimes baseless — optimism, especially if you're dealing with systematic oppression.

Reframing limiting Stories is about having empathy for your own struggle and pain, keeping your Stories from spiraling into a feedback loop with no healing or resolution.

> _"In being willing to believe that options are available, you're more likely to find them."_

### 8. To live a courageous life, surround yourself with supportive people who also honor the value of courage. 

Even if we've done plenty of work toward self-awareness, fear routines can still snag us. That's why the fourth and final step of the Courage Habit is finding our _courageous community_ — people who are working on the same things.

According to research done at Harvard University in 1994, social support is necessary in habit-formation. But where can you find a support network?

Begin by considering the people you already know. Who is also trying to live a more courageous life? Write down anyone who comes to mind, considering whether they exhibit important "reaching out" behaviors: displaying vulnerability instead of pretending things are perfect all the time; trying to solve problems rather than complaining; listening and offering empathy; or showing kindness and compassion over critique.

Pick a "practice person" to establish a courage-based relationship with and offer one of the above "reaching out" behaviors to them. As you connect with them, don't deflect when they ask about your life; get into the habit of sharing your news from a vulnerable place.

Relationships are complicated, so this may not be easy — the people who surround you may not support your decision to live a courage-based life. But challenge yourself not to skip over vulnerability. Instead, utilize the previous lessons of the Courage Habit: access the body, listen to your Critic, and reframe any unhelpful Stories.

If someone responds critically or disrespectfully, let them know how that makes you feel. Explain the "reaching out" behaviors you'd prefer to practice with them. If they remain unreceptive, you know that it's time to set appropriate boundaries and commit to finding others who share your values.

If you're feeling intimidated by forging new, "courage-based" connections, use your new Courage Habit skills to work through the feeling. For example: if your Inner Critic belittles your socializing abilities, request a "Re-do, please," responding, "Yes, I'm shy, but I'm willing to power through a few awkward moments and try my best."

Finally, don't wait for people to exhibit "reaching out" behaviors: initiate them yourself. An easy way to do this is by asking someone to share something about their lives and then listening.

It won't be easy, but once you get the hang of the Courage Habit, you can apply it to many different areas of your life, from activism to parenting to creativity. Remember, growth is an ongoing process. It's not as easy as flipping an on-off switch from "fearful" to "brave." The Courage Habit is a long game, but you'll see that the reward — living a life that feels authentic to who you are — is worth it.

### 9. Final summary 

The key message in these blinks:

**Practicing courage requires you to first pinpoint and then confront your fears. You can begin to do this by breaking down your** ** _cue-routine-reward_** **habit cycle, identifying the sensation and voice of fear, and finally, writing down and reframing the Stories you tell yourself. During this process, it's important to surround yourself with a "courageous community."**

Actionable advice:

**Celebrate your journey.**

Don't just find fear-based sensations in your body — look for the positive ones too! These can help you celebrate the time and effort you've invested and tune out any Stories that tell you it wasn't enough. Note any changes or shifts in thinking you have made during this process, too. These show that you have been present in committing to the work.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Daring Greatly_** **, by Brené Brown**

You've just learned that courage is a habit reinforced by confronting fear. But where do our fears and deep-rooted feelings of shame come from? And why is it so difficult to engage with these parts of ourselves?

To discover why you can only be brave if you embrace your vulnerability, check out the blinks to _Daring Greatly._ Drawing on the work of Dr. Brené Brown, who's spent over a decade researching shame and vulnerability, these blinks explain that while shame is endemic in our society, there is also a cure: admitting your weaknesses and imperfections.
---

### Kate Swoboda

Kate Swoboda is a life coach, director of the Courageous Living Coach Certification, and creator of YourCourageousLife.com. She has contributed to _Forbes, USA Today, Entrepreneur, Business Insider, Lifetime Moms_, and others.

