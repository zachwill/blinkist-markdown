---
id: 55c7ca8566643600076b0000
slug: the-creators-code-en
published_date: 2015-08-12T00:00:00.000+00:00
author: Amy Wilkinson
title: The Creator's Code
subtitle: The Six Essential Skills of Extraordinary Entrepreneurs
main_color: CF3429
text_color: CF3429
---

# The Creator's Code

_The Six Essential Skills of Extraordinary Entrepreneurs_

**Amy Wilkinson**

_The Creator's Code_ (2015) unlocks the essential skills needed to take a big idea and turn it into a successful company. Drawn from 200 interviews with dozens of leading entrepreneurs, including the founders of LinkedIn, eBay, Tesla Motors, Airbnb, PayPal, JetBlue and others, this book will help you better see what areas you need to address to turn your passion into tomorrow's market leader.

---
### 1. What’s in it for me? Learn the secrets of how to take your ideas to the next successful level. 

Every business, regardless of industry, size or ambition, needs to be innovative. Because of this, you'll find no shortage of publications, courses and websites from self-proclaimed "experts" who say they know the secrets to creativity.

As a business leader, how do you know which advice to trust? One way to ensure you follow the correct path is to look at what the most successful leaders have done, and then do likewise.

These blinks, based on a series of interviews by author Amy Wilkinson with today's most successful innovators, give you some tried-and-true tips to push your company to its creative heights.

In these blinks, you'll discover

  * whether you're a sunbird, an architect or an interrogator;

  * why Alfred Hitchcock shot the shower scene from Psycho 78 times; and

  * how YouTube transitioned from a dating site to a video hosting site.

### 2. Smart entrepreneurs find a gap in the market by applying one of three different approaches. 

One of the keys to becoming a great entrepreneur is to spot an opening in the marketplace. To do so, consider these three different approaches.

The first approach is called the _sunbird_ approach. Here, an entrepreneur takes a solution that has proven effective in one place and transplants it to another, often with a twist. (Just like a sunbird, a kind of hummingbird that cross-pollinates by grabbing objects in one place and dropping them somewhere else.)

During a trip to Italy, Howard Schultz (today's CEO of Starbucks) noticed how locals spent tons of time socializing in cafes, enjoying coffee and pastries and listening to opera music. At the time across the Atlantic, Americans had to go to a diner or restaurant if they wanted a cup of joe. 

Schultz saw an opportunity to transplant the Italian cafe model in America, adapting it to suit the preferences of Americans. He replaced opera music with jazz and added tables and comfortable seats, so customers wouldn't have to stand at a bar, as the Italians do, to enjoy their coffee.

This model, as we now know, was a tremendous success. In just one year, Starbucks captured 30 percent of America's premium single-serve coffee market.

A second approach is the a _rchitect_ approach. Architects design entirely new products to address unsolved problems, an approach epitomized by Spanx founder Sara Blakely. Blakely wore pantyhose to smooth her figure, and although she liked the way stockings made her look, she hated how they were uncomfortable and often fit poorly.

Blakely realized there was an opportunity to invent comfortable, figure-enhancing pantyhose. And even though she had no market experience, she went on to make Spanx a huge success.

The last approach is the _integrator_ approach. Such entrepreneurs assemble existing elements, often disparate or even opposite ones, in inventive ways to create brand new products.

Luxury SUVs are a great example of an integrated idea. These cars combine a high-end, high-comfort style in an all-wheel, off-road sports vehicle. This combination of two seemingly contradictory ideas has actually been a huge success, appealing to two very different market segments.

> _"Discovery consists in seeing what everyone else has seen and thinking what no one else has thought." — Albert Szent-Györgyi_

### 3. Don’t wait for that “perfect moment.” Start small, build momentum and focus on the future. 

Some people dream of starting their own company, but few actually live those dreams. Too many dreamers wait for that perfect moment to launch their idea, but end up waiting forever.

Don't be an idle dreamer. Focus on where you want to go and then get to it, dealing with problems as they come your way. In doing so, you can build momentum and turn your idea into a success.

Turkish immigrant Hamadi Ulakaya bought a closed-down yogurt factory from Kraft in 2005, against the advice of his family who thought he was crazy for buying an expensive factory without an established business behind him. Yet Ulakaya had a dream: to make a high-protein but low-fat, Greek-style yogurt.

On the go, he learned the basics of the yogurt business and just two years later, was delivering his first small orders. By 2008, his factory was producing 15,000 cases of yogurt per week. And by 2011, that number had jumped to 1.2 million.

When Ulakaya started his business, Greek-style yogurt was only 0.2 percent of the $78 billion yogurt market. By 2013, Greek-style yogurt accounted for almost half. And Ulakaya's company had over time emerged as the market leader!

To be successful, it doesn't help to dwell on what you've already accomplished. Instead, you should look at what's ahead.

In 2006, Facebook co-founder Mark Zuckerberg declined Yahoo's $1 billion acquisition offer. An average 20-something probably would have jumped at the chance to earn so much, especially from a one-off college experiment!

But Zuckerberg knew that if he kept at it, he could achieve so much more with Facebook. And clearly, focusing on the future was the correct choice.

> _"Never look back unless you are planning to go that way." — Henry David Thoreau_

### 4. Stay flexible by following a simple four-step process: observe, orient, decide and act. 

Things never go as smoothly as you want them to. In business, there are always unexpected shocks and changes. Flexibility is crucial in keeping your company afloat.

To tackle uncertainty, incorporate the _OODA loop_ into your processes. OODA stands for _observe, orient, decide_ and _act_. And it's a loop, because even though there are four separate actions, the sequence itself is continuous. Once you finish the final step, you start all over again.

Here's how the OODA loop breaks down.

First, you _observe_. Look at what's happening around you. How are customers using your product?

Second, you _orient_. Analyze the information you've gathered and formulate ideas about how to improve.

Then, you _decide_. Form a hypothesis about how to improve your product or service.

And finally, you _act_. Move quickly to implement your hypothesis.

This brings you back to the first step: you return to observing, this time around to see how customers are using your new, improved product.

Online video streaming company YouTube used this process early on. Did you know that at first, the company was actually a dating site that allowed users to rate each other's photos and videos?

But when company leaders _observed_ user behavior, they noticed that people were far more interested in uploading _any_ video, not just dating-related clips. With this insight, the company _oriented_ themselves, brainstorming ideas about how to improve.

Next, company executives _decided_ to simply make YouTube open to all types of videos, with the thought that at the very least, the move would improve traffic numbers. Users quickly started posting all sorts of videos, from cats to lectures and more.

To further its reach, YouTube acted quickly to make embeddable code available for MySpace users, which was then the dominant online social network.

Yet YouTube quickly outgrew even MySpace and in 2006, the founders sold YouTube to Google for $1.65 billion.

### 5. To succeed, you have to embrace failure. Own up to any mistakes you make, but don’t give up. 

Behind every success there's a list of failures. Consider that director Alfred Hitchcock shot the shower scene in his movie _Psycho_ some 78 times to get that harrowing moment _just_ right.

This story epitomizes a crucial principle. If you want to be successful, whether in the creative arts or in business, you need to _embrace failure._

To do that, you need to stop thinking of failure as a negative thing. Because every failure brings you one step closer to finding out what you need to do to succeed. Adopting this sort of mind-set can be liberating, as it leaves you free to experiment endlessly — until you reach perfection.

That's exactly why the Hitchcock scene is so iconic. The legendary director spent ages experimenting, so he could get that nail-biting suspense perfect.

Embracing failure is easier said than done, of course, because it requires being brutally honest with yourself and owning up to your failures.

Spanx founder Sara Blakely was taught to do this from an early age. Every evening, her father would ask, "What have you failed at today?"

Blakely failed at sports, singing and at taking the LSAT for law school. And as a door-to-door salesperson, she failed to make sales all the time. But she always owned up to her mistakes, which is what allowed her to improve.

Being honest about your failures won't bring you any closer to success if you don't keep striving after each setback. And to do that, you have to believe in your vision.

Netflix co-founder Reed Hastings suffered a major public backlash when the company decided to raise prices to cover the costs of its new streaming service. As a result, some 800,000 people canceled their Netflix subscriptions!

Yet Hastings knew his decision was the right one. He apologized and tweaked his strategy, but kept going. And today, Netflix has bounced back as an even bigger success.

> _"Ever tried. Ever failed. No matter. Try again. Fail again. Fail better." — Samuel Beckett_

### 6. Teams and networks help to combine skills and establish a space of interconnectivity and trust. 

The idea of a pioneering scientist or gifted artist coming up with an invention or inspiration completely independently is a thing of the past. There are no "lone geniuses."

Today, we see the development of genius as a result of a _collaborative process_. Consider that from 2004 to 2013, seven out of ten Nobel Prizes in chemistry were awarded to teams of two or three scientists.

But teams aren't just good for geniuses, they're good for all of us. Having a diverse range of skills means that a team has more resources from which to draw when encountering problems.

The famous Bletchley Park code-breaking group, assembled to crack the Nazi Enigma code during World War II, was made up of a number of specialists.

Allied forces collected British, American, Polish, Australian and other experts with diverse skills to work together. Language experts, military strategists, mathematicians, engineers, cryptographers, historians, philosophers, classicists — even crossword puzzle experts — eventually deciphered Nazi Germany's encrypted war codes.

Teams also give creatives the chance to bounce ideas off each other, offer advice and critique each other's work. This all leads to a better and more complete final product.

What's more, once you've established an environment of mutual help, you'll find that generosity is contagious. When people _start_ helping each other, they'll _keep_ helping each other!

Professional social network LinkedIn is based on this principle of interconnectivity and mutual trust. Co-founder Reid Hoffman believes that when you're focused on a small task that benefits others, people will be eager to help you in return.

It works like a chain, in that help and assistance move through the whole network, lifting everyone up.

### 7. Final summary 

The key message in this book:

**Successful entrepreneurs identify gaps in a marketplace and create new solutions to fill those gaps. The process requires being radically forward-looking and eagerly embracing failure. It's also crucial to network wisely and practice generosity.**

Actionable advice:

**Seek out people who aren't like you.**

When building a team, it's important to find the right balance to fully capitalize upon the potential of networking minds. To that end, find people with skills and a personality dissimilar to your own. It might feel uncomfortable from the start, but discomfort serves to disrupt cohesion and drives creators to move beyond routine, superficial thinking.

**Suggested further reading:** ** _Smart People Should Build Things_** **by Andrew Yang**

_Smart_ _People_ _Should_ _Build_ _Things_ explores the dangerous consequences of top students' career choices in the United States, and offers practical solutions to reset the country's course toward prosperity by encouraging students to adopt an entrepreneurial attitude. Along the way, the author provides solid advice for budding entrepreneurs on their first adventure into business.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Amy Wilkinson

Amy Wilkinson is a strategic adviser, entrepreneur and lecturer at the Stanford Graduate School of Business. She has worked at consultancy McKinsey & Company and investment company J.P. Morgan, and has served as a White House fellow and special assistant to the U.S. Trade Representative.

