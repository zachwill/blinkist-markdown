---
id: 55c0f27e34396300078b0000
slug: the-culture-engine-en
published_date: 2015-08-06T00:00:00.000+00:00
author: S. Chris Edmonds
title: The Culture Engine
subtitle: A Framework for Driving Results, Inspiring Your Employees, and Transforming Your Workplace
main_color: 288FC9
text_color: 2176A6
---

# The Culture Engine

_A Framework for Driving Results, Inspiring Your Employees, and Transforming Your Workplace_

**S. Chris Edmonds**

_The Culture Engine_ (2014) is a guide to creating an inspirational workplace by revolutionizing the culture of your organization. These blinks will walk you through the process of designing, implementing and managing an organizational constitution to guide and transform any group.

---
### 1. What’s in it for me? Transform your organization’s culture into something you can be proud of. 

Nowadays, culture has become a mystical and elusive ideal for all organizations. Fix your culture and all will turn to gold!

Of course, you can't just snap your fingers and overhaul your organization's culture from one moment to the next, but when you consider companies like Zappos, it's clearly a goal worth pursuing. After all, Amazon paid big bucks for Zappos, and it's no secret that part of what they were buying was Zappos's unique culture.

As you'll see in these blinks, research backs up the importance of organizational culture: Great company culture helps companies last longer and be more successful. It makes for more engaged employees, more loyal customers and more creative offices. So how do you start transforming your culture? Read these blinks to find out.

In these blinks, you'll learn

  * why Zappos pays new hires to quit;

  * how a mission statement can help you outlast your competitors; and

  * a five-step method for addressing people who resist the new culture.

### 2. Revolutionize your work culture with an organizational constitution. 

Want to revolutionize your work culture and transform your business? Start by writing an _organizational constitution –_ a formal document that sets out the core principles of your company or team.

Here's how it works:

Organizational constitutions lay out specific rights for employees and standards for them to follow. Think of it like the rules of the road, guidelines you use to navigate an ambiguous situation and govern your behavior.

For instance, when arguing with a colleague, it's easy to go too far and insult them. But if your organizational constitution includes a zero-tolerance policy for rude conduct between co-workers, you would likely choose your words more carefully.

But an organizational constitution should also answer important questions: What are our company goals? What performance targets can help us meet them? And what are our standards for how we treat each other?

For example, when Tony Hsieh founded the online shoe store Zappos, his intention was for the company to have a fun, familial atmosphere. To facilitate this, he defined ten values that would form the basis of the company culture.

Here's why an organizational constitution is essential for _your_ business:

Without an agreement to guide it, your company will struggle to develop a good work culture, and your organization's culture has a huge impact on its success.

Take Zappos' renowned customer service. Since Zappos' employees like their jobs, they're full of enthusiasm, which they then pass on to their customers. The customers then associate this enthusiasm with Zappos, which is a big plus for the company. In fact, Amazon cited Zappos' great office culture as a major incentive for purchasing the company in 2009.

Zappos owes their culture to their constitution, because when there's a formal agreement governing how people are treated, everyone feels safe, respected and engaged with their work. And engaged employees are better employees: a 2013 Gallup study found that engaged workers are more productive, with less turnover, fewer accidents and better customer satisfaction.

### 3. Before looking at your organization, look at yourself. 

When designing the rules that'll govern your organization, it's important to first decide the values you'll hold yourself to. Inspiring a culture as revolutionary as Zappos's also means looking at your own capabilities.

Remember, as a leader, all eyes are on you. You need to be a living example of the rules that guide your company and its culture. Everything the person in charge does affects the team culture for better or worse.

How?

Well, if your behavior varies, or if you contradict the organizational constitution, your employees might think that you're not truly invested in changing the culture, and that your shallow, short-lived attempt to do so is now over.

For instance, if you fail to reprimand people who break the rules set out in the organizational constitution, your employees won't take it seriously. And what kind of message would it send to, say, give a speech about respecting your colleagues one day, and then to honk aggressively at an employee in the parking lot the next?

So, living by your organization's constitution is essential to changing its culture. To do so, you'll need to have a _personal constitution_, that is, a personal statement comprising four values, and four behaviors that correspond to those values.

Start with your purpose. Think of it as your foundation, your personal mission statement. For example, the author's purpose is to push workplace leaders to foster inspiration and discover their own values.

Next, search for your values and the behaviors that reflect them. To find your values, think about what you're proud to be known for, things like wonder, stability, creativity and so on. Then, assign a clear and measurable behavior to each value.

For instance, if you choose "wonder" as a value, a corresponding behavior might be to read at least two books a month or to ask more questions.

Now that you've got your personal constitution, give it a leadership philosophy: something that describes how you believe people should be led and motivated!

### 4. Use the same strategy to set your company’s purpose, values and behaviors. 

Now that you've written your personal constitution, use the same techniques to write your organizational constitution. Start with your organization's _purpose statement_, then define its values and assign each value a behavior that will help put it into practice.

When writing your company's _purpose statement_, make it captivating and inspiring, but also be sure to explain what your company does, for whom and why. Remember, this will be the mission statement for your organization.

Make your purpose statement even better by learning from others' mistakes:

Avoid saying that the only purpose of your company is to make money, or simply describing what products or services you offer. For your purpose statement to work, it needs to be memorable and inspiring for your customers and employees alike.

Take this example purpose statement: To help save trees — one person and one water bottle at a time.

Why does it work?

It immediately gives you the company's reason for existing: to make water bottles while aiding the environment. Don't doubt the importance of _your_ purpose statement; a 2001 study found that purpose statements are an important factor in a company's long-term survival.

Once you have your purpose statement, it's time to decide on your values and behaviors. Now that you know how to choose _personal_ values, the next step is to figure out what your _company's_ values are and define them as specifically as possible.

For instance, if your two values are excellence and respect, you could say that excellence means going beyond the expectations of your customers, while respect means being considerate to everyone, both inside and outside the company.

Once you've defined your values, you can assign them behaviors that will make them a reality. For the value of excellence, you could be sure to embrace feedback and know everything about your business. Likewise, behaviors that reflect respect might include choosing your words carefully and encouraging employees to give each other honest praise.

But don't get too carried away. When setting your values and corresponding behaviors, try to keep the list to between three and five of each.

> _"It's not what you do; it's why you do it that is important." - Starbucks CEO Howard Schulz_

### 5. Ensure your organizational constitution is adopted by nipping resistance in the bud. 

So, you've designed your organizational constitution and are excited to revolutionize your company culture, but when you introduce your ideas they're met with some resistance. How can you handle employees who don't support your approach?

Start by understanding that resistance comes in many forms. 

The first opposition will probably come from the leaders of your organization, like managers and supervisors. After all, they're the first ones you'll ask to embrace the new company culture. They're likely to express their resistance in one of two ways:

One possibility is that they won't walk the walk, meaning that although they promise to adopt the new practices, their behavior will suggest otherwise and they won't become the ideal example you need. Or, they'll actively oppose the initiative by disregarding it or voicing complaints about the organizational constitution, claiming that it's a waste of time.

Don't panic, dealing with resistant leaders is easy:

First of all, don't take it personally. Naturally you'll be upset, but take a deep breath and remember that they're not opposing _you_, they're opposing a change to their habits.

Once you've calmed down, explain the issues with your resistant employee's behavior as non-judgmentally as possible. This will show him that you've noted the problems and that they need to change. When confronting your employee, you should only discuss their clear, observable behavior, and not the beliefs or motivations behind it.

Now that you've told the employee how you perceive his behavior, give him a chance to voice his problems with the organizational constitution. It's important that you work to understand his perspective, even if you disagree with it.

After hearing his stance, don't budge. Explain to your employee that this is non-negotiable; every leader _must_ be in line with the values of the company.

Once your employee has heard your needs, give him a chance to change his behavior. If he doesn't, he needs to be let go.

### 6. Be sure your hiring practices reflect your organizational constitution. 

Now that your organization has adopted its constitution, there's still one last hurdle to clear. It's essential that all new employees are in line with the company's stated values.

Why?

Because hiring just one person who's against the organizational culture could unravel all the hard work you've invested. It's important to attract the right people.

Say you hire a new manager with great experience and the right skills. If he rejects the organizational constitution, your employees will assume that you don't care about it either. Pretty soon, this new manager will erode the trust and engagement between your employees that you have worked hard to foster.

So, instead of looking for the most skilled people, look for the ones with both the right skills and the right commitment to your organization's mission.

How?

Ensure prospective candidates understand your organizational culture. Make this information available by including your company's purpose, values and expectations for behavior in every job posting you make! This way you can be sure to attract only people who share your values.

But hiring the right people won't matter if they don't adjust to your culture. So make sure to take the time and effort necessary to integrate new hires.

When taking new employees on board, make sure they experience _cultural exposure_, an explicit educational session on your organizational culture. Explain the expected behavior and praise new hires when they display it.

Another way to orientate new employees is through a mentor, ideally someone other than the employee's supervisor. The mentor and employee should meet every couple of weeks for the first few months to talk about whatever the new employee wants. The topic of conversation isn't limited to issues that HR can resolve, but can be about anything work-related.

The mentor-mentee relationship is productive because the mentor can answer questions the new employee might be scared to ask publicly, while showing him how to abide by the organization's purpose, values and behavior.

### 7. Final summary 

The key message in this book:

**The culture of a company is one of its most valuable attributes, but building a culture that an entire organization can embrace is no simple task. Using an organizational constitution to formally outline an entity's purpose, values and associated behaviors can provide a clear and organized format for any group to rally around.**

**Suggested** **further** **reading:** ** _Change the Culture, Change the Game_** **by Roger Connors and Tom Smith**

_Change The Culture, Change The Game_ (2012) demonstrates how to implement a culture of accountability within your organization. You'll discover how to help encourage a shift in thinking to get the game-changing results you want and explore the steps needed to sustain such changes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### S. Chris Edmonds

Chris Edmonds is the founder and CEO of an organizational culture consulting firm called The Purposeful Culture Group. He is a prolific author and an adjunct professor at the University of San Diego School of Business.

  

©[S. Chris Edmond: The Culture Engine] copyright [2014], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

