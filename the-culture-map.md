---
id: 54451e1a6161330008550000
slug: the-culture-map-en
published_date: 2014-10-21T00:00:00.000+00:00
author: Erin Meyer
title: The Culture Map
subtitle: Breaking through the Invisible Boundaries of Global Business
main_color: AAB84C
text_color: 6A732F
---

# The Culture Map

_Breaking through the Invisible Boundaries of Global Business_

**Erin Meyer**

_The Culture Map_ provides a framework for handling intercultural differences in business and illustrates how different cultures perceive the world. It helps us understand these differences, and in doing so improves our ability to react to certain behaviors that might have once seemed strange. With this knowledge, we can avoid misunderstandings and maintain conflict-free communication, regardless of where we are in the world.

---
### 1. What’s in it for me? Learn how to communicate and lead successfully in different cultures. 

The cultures we're surrounded by and grow up in have a huge impact on how we perceive the world. In every culture, we are conditioned to see particular communication styles as more desirable than others, which can lead to conflict. If you take a little time to understand a person's cultural background and his corresponding worldview, you will be able to communicate with him more effectively and avoid the risk of misunderstanding.

Erin Meyer's eight different scales will help you do exactly this, specifically in the business world.

In these blinks, you'll learn:

  * why witnessing a shouting match in Paris might not mean what you think;

  * how the Vikings' democratic style lives on in Sweden; and

  * how to safely criticize someone from any part of the world.

### 2. Being a good observer is sometimes more important than being a good speaker. 

Why do we find communicating with people from other countries so challenging sometimes? We often have to deal with different temperaments, values and senses of humor.

In order to navigate these situations, we should try to avoid being what the Japanese call _kuuki yomenai_, which translates to someone who "cannot read the air."

We can better "read the air" if we consider that communication styles can fall on a _communicating scale_, and be grouped into _low-context_ cultures and _high-context_ cultures.

Western countries, such as the USA or Australia, are known as _low-context_ cultures, where communication is precise and clear in order to avoid misinterpretation. Contrastingly, communication in _high-context_ cultures in countries such as Japan or Korea are more subtle and layered and often require reading between the lines to understand what is meant.

No country, however, is 100 percent low or high context. French managers, for instance, tend to be more high-context than German managers, but are low-context compared to Chinese managers.

Why do different countries have different contexts? The answers can be found in history. High-context cultures like Japan have had largely a homogenous population, therefore people became tuned to subtle nuances in communication and developed skills in "reading the air."

American history, on the other hand, is much shorter and has been strongly influenced by immigrants, requiring their communication to be explicit to avoid misunderstandings.

So how can you work well with other cultures? You need to strike a balance between listening and speaking. When working with high-context cultures, listen for meaning and not what is actually spoken. Pay attention to changes in body language, like head-shaking or noticing self-restraint that shrouds the meaning of a message.

The opposite is true when working with low-context cultures. You should be as specific as possible and take time to explain yourself clearly.

When multiple cultures are working together, the most effective method of communication is to use the low-context style as this causes fewer misunderstandings.

> _"It is only when you start to identify what makes your culture different from others that you can begin to open a dialogue of sharing, learning and ultimately understanding."_

### 3. Be careful with your feedback; it can be offensive. 

If you've ever worked with people from other cultures, you may have had the misfortune of accidentally offending someone when you just wanted to offer them some feedback. To gain some understanding of why this happens, let's look at the _evaluating scale_.

When we give negative feedback, it can be _direct_ or _indirect. Direct_ cultures like Russia or Israel are forthright in feedback to their colleagues. They also often use absolute descriptions known as _upgraders_, such as "totally" or "strongly" to emphasize their point. For example, they might say "I _totally_ disagree with your opinion. It is _absolutely_ unprofessional." In addition, it is not unusual to criticize in front of a group.

Japan or Indonesia , however, are _indirect_ cultures, which means they provide gentle feedback and cover negative messages with positive ones. They also use _downgraders_, like "kind of" or "maybe" to get their message across. For example "Maybe you should reconsider your opinion a little bit." And, as opposed to the style of direct cultures, criticism is given privately.

So if we take the communication scale from the previous blink and the evaluating scale we can see four groups:

Low-Context and Direct-Feedback: e.g., Germany

High-Context and Direct-Feedback: e.g., Russia

Low-Context and Indirect-Feedback: e.g., USA

High-Context and Indirect-Feedback: e.g., Japan

To work better with diverse cultures, bear these styles in mind and adapt your feedback to reduce the likelihood of offending others.

When meeting with people from high-context and indirect cultures such as Japan, avoid delivering feedback in front of others and adjust your message while retaining its meaning.

Sometimes it's even appropriate _not_ to mention negative aspects of an idea or suggestion and just highlight the positive ones. By doing so, it will become clear to the person you're dealing with that you would prefer the other aspects to fall to the wayside.

By adapting your behavior and observing others' feedback style, you should reach your desired outcome of being perceived as polite and supportive.

### 4. Paying attention to how others convey ideas will help you be more convincing. 

How do different cultural backgrounds affect the way we perceive persuasion?

Let's use the _persuading scale,_ which consists of _principles-first reasoning_ and _applications-first reasoning_.

Principles-first reasoning is deductive and uses general principles to draw conclusions. Take learning a new language, for example: first you learn the grammatical principles, then you start speaking the language.

Applications-first reasoning, on the other hand, is inductive. You present a theory first, then you state the facts that support it. This method is typically used in math class: the student first gets a formula and then practices applying it. After practicing applying it, the students starts to understand its principles.

Cultures which use principles-first reasoning, like France or Italy, are more inclined to draw attention to _why_ something is requested, before they decide to carry it out. In contrast, application-first cultures such as the US or Canada center on the _how_ more than the _why_.

Therefore, it's not unusual for a French employee to become frustrated with an American boss when instructed on how to do something without clarifying the reason why.

The best way to deal with these differences, then, is to switch back and forth between explaining the principles behind your point and showing its practical application.

Let's say you're giving a presentation at an international conference on why your product is superior to your competition's. You could begin by presenting how your product is made to satisfy the principles-first audience. Then offer some practical examples, like videos of people using your product, to draw in your application-first audience.

### 5. To be a successful leader, you must learn to adapt your style. 

How can acknowledging cultural styles help your workflow and leadership? To find out, let's examine the _leading scale_ which involves _egalitarian_ and _hierarchical_ cultures.

_Egalitarian_ cultures, like Denmark or the Netherlands, have a narrow gap between employees and managers, where managerial roles often focus on mediation between equals. Their organizational structures are flat, so a marketing director may work directly with a copywriter, and both parties are comfortable using each other's first names.

However, countries like China and Nigeria have distinct _hierarchical_ structures, with noticeable gaps between boss and employees. In these structures, bosses lead and make decisions. Communication also follows a firm hierarchy: to reach the person three positions superior to you, you need to first talk to the person directly above you. They will then go directly above them to reach the person you need.

Note that how countries are grouped geographically does not necessarily mean they fall in the same place on the leading scale. For example, France and Sweden are both European, but fall on different ends of the scale, with France being more hierarchical.

As Professor André Lauren found, this difference stems from historical differences in leadership.

France was influenced by the hierarchical and centralized political system of the Roman Empire, while Swedish history was impacted by the Vikings, one of the world's first democracies, in which its people's opinions were considered equal.

Coping with these differences means understanding how different cultures react to your requests.

Employees in egalitarian cultures must be included when making decisions. Don't supervise, but rather facilitate. Provide room for autonomy and increase your involvement only when things are not progressing well.

When you're leading hierarchical cultures, encourage employees to offer their opinion; without an invitation, they won't. Most importantly, make clear that you are in a position which requires people to look up to you. This might mean using your surname, instead of your first, for example.

### 6. Understanding different decision-making processes is vital to implementing ideas. 

Being egalitarian at work doesn't mean that decisions are automatically made by consensus. The same applies to hierarchical cultures. Actually, decision-making has its own cultural scale.

The _deciding scale_ consists of _consensual_ at one end and _top-down_ at the other.

_Consensual_ countries such as Sweden or the Netherlands take their time in group discussions until a consensus is reached. Once a decision is made, the time it takes to implement it is short, as it's already been approved by all involved.

Decisions in _top-down_ countries like China or India, are made individually and usually by the boss. They are made more quickly than consensual countries, but are often revisited and altered later on. Because of this continuous revision, changes can take more time to implement.

For example, in the Japanese _ringi-system,_ a proposal document ( _ringisho_ ) is offered to mid-management. Each person here can edit this document until a consensus is reached. The document is then handed to the next management level where the process is repeated until the document reaches the top level. Therefore, the ringi system is both highly consensual and hierarchical.

It's wise not to make snap judgements on how decisions are made if you're only aware of the organizational structure. As with the ringi-system, some structures appear top-down but are actually consensual as well.

When working in a multicultural environment, it's best to stick with one decision-making method. Early on, clarify whether decisions will be consensual or made by a boss. Also, decide how vital a total consensus is and how flexible the decision-making process should be. Will it be fixed for a few years, or will it be something that can be changed every few weeks? Finally, when it comes to major decisions, review the method and make sure all involved understood and accepted it.

### 7. Trust is built differently in different cultures, but it's important to all of us. 

As the Russian proverb goes, "Trust, but verify." Many cultures have established rules in place to make sure businesses don't just lean on trust alone. Even so, trust is central to business negotiations and can be split into _cognitive trust_ and _affective trust_.

Cognitive trust occurs when we have worked with someone for a long time and have established them to be reliable.

Affective trust involves our friends and relatives and comes more from emotions, rather than experiences.

On the _trusting scale_, cultures may be grouped as high _task-based_ to high _relationship-based._

In regards to business, _task-based_ cultures like the US and the Netherlands create trust by means of business-related achievements. Depending on how good it is for business or profits, a relationship can be forged or dissolved easily. For example, Americans are unlikely to be bothered if a longstanding colleague is fired for performing poorly.

But _relationship-based_ countries like Brazil and China form their partnerships according to shared personal experiences. In China, an agreement between executives cannot be reached until they have developed what is known as _guanxi._ So you should take the time, energy and effort to establish an emotional connection with people from these cultures.

The bottom line though, anywhere you go or regardless of the culture with which you're working, is that creating affective trust is useful. Relationship-based trust hinges on affective trust, while task-based trust relies on cognitive trust. Relationship-based trust proves more advantageous, because you can use it anywhere, whereas task-based only applies in certain cultures.

One way you can create trust with cultures you're unfamiliar with is by identifying commonalities.

For example, you discuss more personal topics such as family or music. Sharing these commonalities shows you are interested in the other person, not just the business, and that care will help you gradually build a personal relationship with your business colleague.

In relationship-based cultures, it's best to show your true self so don't fret about saying or doing something wrong.

> _"Awareness and open communication can help working in a multicultural environment."_

### 8. There’s a proper way to disagree. 

In some cultures, disagreements are welcome. Bear in mind, though, are you disagreeing with somebody personally or with her idea? The _disagreeing scale_ allows us to see how confrontation is appreciated or evaded.

Cultures with _confrontational_ disagreeing patterns, like Israel or France, differentiate between the person and the idea they disagree with and open confrontations don't negatively impact relationships. So in Paris, for example, if you see two people arguing openly with one another, this doesn't necessarily mean that they aren't good friends.

In cultures which _avoid confrontation_ such as Indonesia or Japan, open confrontation is seen as inappropriate and disruptive to the harmony of the group. In addition, a person and the idea they offer are strongly connected, so disputing someone's idea is seen as an attack on the person.

Another factor to consider is emotional expression. Cultures can be either emotionally _expressive_ or _inexpressive_. For instance, Germany and France are both confrontational cultures, but their disagreement styles differ. Germans tend to disagree objectively, without involving personal emotions. This way they distinguish between the person and their idea. So you likely won't encounter as many open emotional disputes in Berlin as in Paris.

France can be described as emotionally expressive, which makes it difficult for non-French people to tell if they themselves are being criticized or just their idea.

In order to maintain relationships with different cultures, you will need to understand how disagreement is handled within them. In cultures that avoid disagreement, holding pre-meetings without the boss can help to prepare for criticism. This way employees can come together and offer their thoughts as a group. This makes for a consensus critique, as opposed to a critique of an individual, whether it's the boss or an employee.

When you work with cultures that are more confrontational than your own, approach disagreement with caution. However, you needn't totally convert to the other culture's confrontational style if it doesn't feel natural to you, instead it's better to take part in a relaxed debate.

> _"'To engage in conflict, one does not need to bring a knife that cuts, but a needle that sews.' — Bahamian proverb."_

### 9. Schedules should be made according to a culture’s perception of time. 

How is it that being late in Germany could indicate punctuality in France? Well, time perception differs between cultures and can range from _linear_ to _flexible_ on the final scale we will look at, the _scheduling scale._

Countries like Germany and Switzerland approach tasks in a linear way, finishing one task before they begin the next. More value is placed on adhering to deadlines and schedules than being adaptable or flexible in business. It's also considered impolite to behave in a way that's not related to the task at hand. So answering your cell phone or chatting to your colleague during a meeting would give a negative impression.

However, flexible-time cultures like Saudi-Arabia or Kenya deal with tasks as they arise and many things may be addressed at the same time. Adaptability is ranked higher than organization and meetings aren't conducted in a linear way. Even though there may be a predefined schedule, it's common for discussions to stray, for subgroups to form and for discussions to turn into topics that weren't clearly stated in the schedule.

How can you work among varying scheduling cultures?

You can learn how the people you'll be working with deal with tasks and then emulate this method yourself. For example, since linear-cultures adhere to schedules, try to do this when you present a schedule for them. Plan your meetings in detail, emphasize punctuality and stick to your planned theme.

When working with flexible cultures, plan your meetings without predetermined limits. In a managerial position, you should adapt and cope with needs as they come up.

Also, building a team culture with its own scheduling rules can help when dealing with different cultures. You could try an exercise, for example, in which the people in your group imagine they are all from Switzerland, where punctuality is of high importance. You could agree that each time somebody arrives late, they have to contribute five Euros to the end-of-year party.

> _"Flexibility is the key to success."_

### 10. Final summary 

The key message in this book:

**With some understanding, education and flexibility, you can learn to adapt your communication and leadership styles to different cultures. By doing so, you can avoid conflict and conduct business successfully all over the world.**

Actionable advice:

**Understand there are differences between countries, even if they are geographically close to one another.**

France and Germany may border each other, for example, but it doesn't mean they conduct business in the same way. Although both tend toward open confrontation, Germans are far less likely to display emotion when doing so.

**When dealing with Chinese culture, ask for input if you would like it.**

Because of China's hierarchical system, a Chinese business person is unlikely to speak up during a meeting. That tendency is not due to shyness, but to respect. If you would like his input, you'll need to ask for it directly.

**Suggested** **further** **reading:** ** _Tribal_** **_Leadership_** **by Dave** **Logan,** **John** **King** **and** **Halee** **Fischer-Wright**

_Tribal_ _Leadership_ explains how members of the same workplace function together as a tribe. Each tribe has a culture that determines its productivity, and there are five distinct stages of tribal culture. These blinks will show how you, as the "tribal leader," can guide _your_ tribe to higher levels, resulting in a healthier and more productive work environment.
---

### Erin Meyer

Erin Meyer is a professor at INSEAD The Business School for the World and specializes in cross-cultural communication. Her work has been published in _Harvard Business Review_, _Singapore Business Times_ and on _Forbes.com._

