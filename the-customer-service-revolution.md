---
id: 56d5b96394333d000700009c
slug: the-customer-service-revolution-en
published_date: 2016-03-04T00:00:00.000+00:00
author: John R. DiJulius III
title: The Customer Service Revolution
subtitle: Overthrow Conventional Business, Inspire Employees, and Change the World
main_color: F4C731
text_color: 806819
---

# The Customer Service Revolution

_Overthrow Conventional Business, Inspire Employees, and Change the World_

**John R. DiJulius III**

_The Customer Service Revolution_ (2015) reveals the real secrets of brilliant customer service. These blinks provide a practical guide for taking your customer service to the next level, helping to create an extraordinary experience for your customers and forge an enthusiastic vision-driven workforce.

---
### 1. What’s in it for me? Be the best of the best customer service providers. 

Doing business means taking care of your customers. But how? One way is to join the revolution, that is, the customer service revolution.

Haven't heard of it? Well, read on!

_The Customer Service Revolution_ is packed with tips and tricks for treating your customers right. Drawing on many years of experience in running both his own spa business and a customer service consultancy, the author provides you with sound advice on how to make a good customer experience into an extraordinary one.

In these blinks, you'll learn

  * why the way you instruct your employees is an important part of your customer service;

  * why it's essential to create a customer service vision statement; and

  * why you should make a distinction between on and off stage.

### 2. Great customer service is giving the customer what they need before they ask. 

Got those friends that seem to know what you're going to say before you say it? Chances are they'd be customer service naturals. _Service Aptitude_ is the ability to anticipate your customer's every wish before they recognize it themselves. This allows you to exceed customer expectations: a mark of top-quality service. 

Service Aptitude is a key skill, no doubt. But we're not born with it — it comes from experience. Some of us were lucky enough to have had hospitality experience growing up in a family business. Otherwise, we gain our Service Aptitude through work experience. 

Every employee has had different experiences with previous employers, which means different expectations of what good customer service entails. So, the owner of a restaurant shouldn't get angry with a new waitress for failing to deliver the service he expects. 

Rather, he should remember that she simply might not know any better. This new waitress would do well to work alongside an experienced coworker, so she can learn what the restaurant's expectations are and build her Service Aptitude.

Great customer service isn't the sole responsibility of your employees. You have to outline the features of good customer service that you expect. To put it in catchier terms: you need to know how to rock a customer's world. This is what we'll learn in the following blinks.

### 3. Aim to give your customers a revolutionary experience, then let them rate it! 

Could you imagine life without your smart phone? If not, you're having the _epiphany experience_ Steve Jobs wanted his customers to have. 

An epiphany experience occurs when a need that the customer never knew existed is fulfilled by a service or product. By seeking out a gap in the worlds of supplying, retailing and purchasing, you can discover hidden needs for which you can create a product. Steve Jobs did just that with the iPhone: he created a new way of using a phone and new technological expectations consumers had never dreamed of.

An epiphany experience will gain you customers, sure. But after that you've got to make sure you maintain them. To do this, you need to work out whether they're satisfied with your service or not. Why not invite them to rate your performance? Many companies have incorporated customer ratings into their services — to great success. 

Take Zappos, an online megastore for shoes. Buying shoes is a tricky enough task on its own: factors like size, price, material and brand make it pretty hard to find the right ones without trying them on. But Zappos customers can talk to a customer service agent who will try to help them pick the shoe that fits.

Then, the customers can evaluate the agents who helped them out on the website based on a 100-point scale comprised of questions like "Did the agent try twice to make a personal emotional connection?" Agents who get fewer than 50 points in one month receive extra training.

### 4. Spare your customers and employees negative cues to ensure you always make a great impression. 

Body language is everything. If you're telling a friend a joke and they cross their arms and start gazing at something behind you, would you feel encouraged to continue to the punchline? Not likely. In the same way, the subtlest employee behaviors can make or break a customer experience. Be wary of this and boost your communication simply by avoiding _negative cues._

Negative cues are the messages you give to customers that leave negative impressions on them. For instance, a receptionist at a medical practice once told the author he needed to _verify_ his information. It seemed that the receptionist was suspicious of the author, thus giving a _negative cue_. The receptionist, of course, was just trying to do her job. By rephrasing her request, using the word "update" or "confirm," she could have gotten the details she wanted while making the author feel welcome and trusted, too!

But negative cues aren't just received by customers. You can also give your employees negative cues if you're not careful. If you're condescending, patronizing or dismissive towards your employees, you can bet it won't leave a good impression on your customers, either.

Consider a notice in the bathroom of your restaurant that informs your employees to wash their hands. This is a very bad policy because it makes customers question why such a sign was needed in the first place: surely all employees are conscientious enough to be hygienic!

Instead, the sign could say something like: "We love cleanliness, that's why we always wash our hands before going back to work." This sign has the same effect on your employees, but leaves a much better impression with customers.

### 5. Create a vision statement for your customer service to motivate your team. 

Many companies print their mission statements all over their products for customers to see. Yet when it comes to mission statements that concern employees, it's a little harder to work out what a company stands for. You can boost your customer service with a _vision statement._

A vision statement outlines _what_ your company aims for in its customer service. It should be easy to understand, memorable and empowering. 

Starbucks once approached the author to create a new customer service vision statement. After some attempts, they found the one: "We create inspired moments in each customer's day. ANTICIPATE - CONNECT - PERSONALIZE - OWN." This statement is printed on the inside of every employee's apron. 

Define your vision statement in this way and you'll give your whole workforce something in common. From managers to secretaries to cleaners to the CFO, the whole organization is working with a powerful vision statement in mind. 

However, a vision statement needs supporting pillars that show employees _how_ they can realize the company's vision. To be exact, you'll need three supporting pillars, each of which has differing functions: 

Pillar 1 describes the quality of the service your workforce is providing. Pillar 2 defines the treatment of the customer. Pillar 3 provides the key to exceeding the customer's expectations.

The author's business supports companies in designing extraordinary customer service in their organization. Its vision statement is: "To be the best decision our clients make. Expertise - World-Class - Whatever/Whenever."

Pillar 1 is the delivery of great service with innovative solutions; pillar 2 is building long-lasting relationships with customers; and pillar 3 is being flexible and global enough to allow collaboration at any place and time.

### 6. Keep your great customer service consistent by giving employees clear guidelines and a time and place to unwind. 

Imagine a Ritz-Carlton employee responding to a customer's question with "How would I know? I'm housekeeping!" You'd be pretty taken aback, wouldn't you? And rightly so! A high-performance team member always has a solution for the customer, no matter what their role is. To make this easier for your employees, provide them with a _never and always list_. 

A never and always list lays the ground rules for employee conduct, so that your team members know how to proceed at all times. For example, if customers ask for the restroom, your staff would know that they shouldn't just point in the general direction. Having read your never and always list, they'd remember the following rule: "Never point. Lead!"

Likewise, your employees will also know that they should always answer requests with an "I would be happy to" or "Certainly," rather than just "All right" or "Okay, sure." This, in turn, means customers will _always_ feel like their needs are taken seriously. 

At the same time, employees shouldn't feel like they're carrying out some exhausting charade. Let your team members know when they're on or off stage. Everybody needs a break sometimes, just don't let your customers see. An employee slouching around outside on a cigarette break still in his uniform will not give off a professional image. 

So, give your employees the designated space and time they need to unwind away from customers. This will allow them to return refreshed and ready to continue providing stellar customer service.

### 7. Final Summary 

The key message in this book:

**Brilliant customer service comes down to a handful of key elements. Surprise your customers by fulfilling their needs they didn't know they had, leave great impressions by making the most of customer feedback, and instruct and inspire your employees to give the customer service you want to see.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Be Our Guest_** **by Disney Institute and Theodore Kinni**

_Be Our Guest_ (2011) reveals Disney's key tenets and principles of outstanding customer service and how following these has helped the company become the successful business empire it is today.
---

### John R. DiJulius III

John DiJulius is the founder and owner of John Robert's Spa, one of the top 20 spa salons in America. He is also the president of The DiJulius Group, a customer service consulting group whose clients include successful companies like Starbucks, Lexus and Nestlé.

