---
id: 570a22478b5d6e0003000145
slug: the-da-vinci-curse-en
published_date: 2016-04-15T00:00:00.000+00:00
author: Leonardo Lospennato
title: The Da Vinci Curse
subtitle: Life Design for People With Too Many Interests and Talents
main_color: AD2627
text_color: AD2627
---

# The Da Vinci Curse

_Life Design for People With Too Many Interests and Talents_

**Leonardo Lospennato**

The _Da Vinci Curse_ (2012) plagues people who have too many talents and interests: they are always learning, but never invest enough time and energy into one thing. They are always swapping their job, their hobbies or even home and never become fully engaged in the many domains to which they're drawn. These blinks describe their particular problems, analyzes the causes and provides a powerful systematic approach to combat the curse.

---
### 1. What’s in it for me? Learn what it takes to overcome your Da Vinci curse! 

You know Leonardo Da Vinci already. He was a universal genius: a painter, a sculptor, an architect, an anatomist. He never specialized in one particular field, and this wasn't a big issue back in those days.

Today, however, things have changed quite a bit. In fact, society doesn't seem to have much time for _Da Vinci people_ — that is, the Renaissance men and women of our time. These individuals never really get around to mastering one field of knowledge fully and thus become almost obsolete in today's highly complex knowledge society.

But don't worry, these blinks will provide you with an understanding of why some people (including you?) cannot get around to specializing in one field, and maybe more importantly, teach you how to overcome the curse.

In these blinks, you'll discover

  * why competition is likely to paralyze any desire for specialization;

  * how to identify an activity that'll lift the Da Vinci curse; and

  * why you should distinguish between cow, dog and star activities.

### 2. Multi-talented people don’t fit into a world focused on specialization. 

Doctors who specialize in spinal surgery, psychologists with expertise in adolescent schizophrenia, physicists who study gravitational waves: The list of specialists in today's society is long — and for good reason.

In fact, specialists have never been more important than today.

This is because knowledge has been rapidly growing since Da Vinci's era in the sixteenth century. And as knowledge accumulates, expertise becomes more and more important.

Take the spinal surgeon. The intricacies of the spine demand absolute precision during surgery. That's why it requires years of specialized training to acquire the expertise necessary to not make a single mistake.

This means that someone who is interested in spinal surgery, but also wants to be a maestro on the violin _and_ become a top chef, will have to make a tough decision: if you want to master a highly complex skill, you can only dedicate yourself to one thing.

Indeed, this is the struggle of all _multi-talented people_.

Multi-talented people feel a strong drive to realize all their talents, but fear that they don't have enough time. Furthermore, they are typically highly curious, but struggle to commit to the practice that mastery requires. They get fascinated by a new field and dive into it with a passion. But once they've mastered the basics, the initial appeal fades away.

Take the author as an example. He fell in love with classical music when he was 18 and began learning the violin. But only a few months later, his interest vanished, he stopped going to classes, and he moved on to something else.

> _"The less specialized you are, the farther you are from the top of a given field or market."_

### 3. Da Vinci people fear competition and struggle to find the field in which they can thrive. 

A clear direction in life gives us a purpose. But _Da Vinci people_ with their multiple talents lack this — they repeatedly change direction and switch jobs and hobbies.

Why?

One main reason is that they fear competition.

For many people, having someone to compete with motivates them to push their skills further. But that doesn't work with Da Vinci people.

Their fear of competition leads them to leave a field just when things start getting interesting. They will start learning a new skill, — and stop when they've learned enough to convince themselves that if they wanted to, they could master it.

For example, if they tried out basketball, they would only practice until they could say to themselves "this isn't too difficult — I could become a great basketball player if I wanted to." By leaving the field at that point, they can maintain their high self-esteem without having to confront other practitioners.

This is linked to Da Vinci people's fear of criticism. They refuse to acknowledge that every master starts as a student, and fear the feedback necessary to grow. So when the going gets tough, instead of pushing through to the next level, they will often change fields instead.

But this lack of direction makes Da Vinci people unhappy.

Their inability to dedicate themselves to one particular field limits them to a superficial understanding. And while they may know a bit about many fields, they end up feeling like "Jacks of all trades, masters of none."

Also, after many years of jumping from field to field, Da Vinci people often feel that they've wasted time. They've acquired a lot of knowledge, but not a lot of depth, and most of all, they still don't know what their true vocation is. And as they get closer to middle age, their despair increases, because they know time is running out, and they still can't see a way of bringing together all their talents.

> _"We are under the same curse that affected Da Vinci: we have many talents and interests, but only one life."_

### 4. To lift the Da Vinci curse, Da Vinci people need to find an activity that involves many of their talents. 

So if you, like the author, consider yourself as part of the Da Vinci community, what can you do? How can you lift the Da Vinci curse?

In a nutshell, you need to find a _single_ activity that is _complex_ enough to _integrate_ many of your talents.

Da Vinci people have complex personalities that can only be satisfied by a complex task. And that task has to bring together a variety of their gifts, or they will soon feel the itch to concentrate on the one that feels left out.

Let's take the author as an example. He is a typical Da Vinci person who tried out everything imaginable. He worked for IBM, learned how to code and engaged in a variety of hobbies — yet, he could never figure out his _one particular_ thing.

One day he decided to dedicate himself to building electric guitars and basses — and that was how he lifted his personal curse. Building musical instruments was complex enough to involve his many talents and interests: it tapped into his knowledge of acoustics, physics, electrical engineering and design. And by doing his best to build great instruments, he also satisfied his passion for music and his need to help other people be creative.

So that's the author's story of success — but what about yours? In the blinks that follow, we'll see how anyone can take a rational approach to finding their own calling.

> _"So that's the Answer to life: Life is not about finding meaning in it, it's about giving meaning to it."_

### 5. Finding our personal calling starts with creating a long wish list, and then drawing up a short list. 

Now that we know we need a calling to lift the Da Vinci curse, how exactly do we find it? The author proposes a three-step approach that starts with _preselection_.  

  

In the preselection stage we write out a _creative inventory_ of every activity we would like to do.

Imagine for a moment that you have infinite time and money. How would you decide to spend your life? Write down all the activities that come to mind, including jobs, hobbies and even one-time experiences — for example, getting a degree in psychology, scuba-diving in Thailand, learning the piano, etc. And make sure to include your wildest dreams, like becoming a pop star or walking on the moon.

Once you've got a list of all of your wishes and dreams, you can start the preselection. To pass through this stage, the dream has to fulfill three criteria: is it fun, do we have a talent for it, and can we earn money with it?

Read through your creative inventory and circle the activities that fulfill all three criteria, and don't be tempted to hold on to those that don't. The ones you can't earn money with are only hobbies, while the ones that'll make you rich, but aren't fun, won't make you happy. And if you don't have a talent for it, why bother?

We've now taken a major step forward with our preselection. Let's move on to the next stage of finding our calling in the following blink.

### 6. The second step in the quest for our calling is a systematic evaluation of our creative inventory. 

Now that we have a basic assessment of our creative inventory, it's time to move to the next step: a more pragmatic and systematic evaluation of our activities.

Our previous list pinpointed our intersection of talent, money and fun. We now need to refine our list by asking two crucial questions: First, how much income potential does each activity involve? Second, how fulfilling will each activity be?

To answer these questions systematically, we can use the _BCG matrix_.

The BCG matrix, developed by the famous Boston Consulting Group, helps companies evaluate which products are worth investing in by dividing them into _Cows_, _Dogs, Stars and Question Marks_. We can use it to figure out which activities can be our calling.

_Cows_ are activities that will make us rich — think of a cash cow — but are not at all fulfilling. They can be put aside, just like the _Dogs_, which are the activities that provide neither profit nor fulfillment. But we should keep an eye out for the Stars: the dream jobs that will bring both wealth and meaning to our lives.

Then there are the Question Marks. These are the activities we love to do, but which are unlikely to generate money. But we shouldn't throw them out straight away. We should continue to do them for our own fulfillment, while looking for ways of making them profitable. If we're lucky, they might become our future Stars.

Have you identified your Cows, Dogs and Stars? Then get ready for the third and final step!

### 7. We need to balance fear, overcome procrastination, surmount blocks and manage our narcissism. 

The first two steps allowed us to determine and evaluate the activities we like. Now, step three ensures that our newly minted plan won't fail. Let's see what we need to know about the common traps and obstacles of following a new calling.

First of all, we need some fear — but not too much.

If your brand new plan doesn't make you at all afraid, it's probably not ambitious enough. And if it's not ambitious enough, it won't make you happy.

But if you feel too much fear, then you've gone too far the other way. You'll need to revise the plan until you feel just the right amount: the sweet spot of fear between perceptible and intense.

Second, we have to avoid procrastination by all means possible. If we procrastinate, we'll never get our dream plan off the ground, and while occasional lazy days are OK, we can't allow them to become a habit.

Third, we need to surmount creative blocks. We've all had the experience of working on a creative project and suddenly finding our inspiration has dried up. But contrary to common belief, these blocks aren't caused by laziness. In fact, they happen because we've forgotten why we are creating what we are creating. And to free up the blockage, we need to get back in touch with the deepest reasons that drove us to create in the first place.

Finally, we need to carefully manage our narcissism. We do need a tiny amount of narcissism to maintain a healthy self-esteem, but if we allow it to get out of hand, we will cycle between states resembling mania and depression. We spin into manic mode when we think too highly of ourselves and our capabilities, and our mind gets obsessed with all the incredible things we're going to create. But when our dreams inevitably clash with reality, we get depressed as we realize that our goals were set too high.

> _"Fear is kind of a good friend; just don't let it get_ too _close."_

### 8. Final summary 

The main message in the book is:  

  

 **Da Vinci people suffer from being unable to focus on one activity. They simply have too many talents and keep jumping from one to the other, which makes them unhappy and frustrated. But by finding a path that is both complex and financially viable, they can find their true calling.**

**Got feedback?**

**We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!**

**Suggested further reading:** ** _Talent is Overrated_** **by Geoff Colvin**

_Talent is Overrated_ explores the top performers in a number of fields to get at the bottom of just what makes them great. Contrary to what most of us intuitively think about skill, this book offers enticing evidence that top performance in any field are not determined by their inborn talent, but by deliberate efforts over many years. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Leonardo Lospennato

Leonardo Lospennato is a Renaissance spirit who has studied engineering, worked for eBay and IBM, and written articles as a journalist. He now follows his calling as a modern-day luthier making custom electric guitars and basses.

