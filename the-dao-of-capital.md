---
id: 54579b7c3365660008980000
slug: the-dao-of-capital-en
published_date: 2014-11-05T00:00:00.000+00:00
author: Mark Spitznagel
title: The Dao of Capital
subtitle: Austrian Investing in a Distorted World
main_color: 773018
text_color: 773018
---

# The Dao of Capital

_Austrian Investing in a Distorted World_

**Mark Spitznagel**

_The Dao of Capital_ takes the reader on a journey from ancient China to nineteenth and twentieth century Vienna, to modern, globalized markets. Using a multitude of examples, it outlines the approach of "roundabout investing" or "Austrian investing," which is based on ancient Daoist wisdom of "gaining by losing." See how strategic investing, rather than rapid and rushed investing, can lead you to great financial success.

---
### 1. What’s in it for me? Learn what Daoism can teach you about wise investing. 

In the late 19th century, The Austrian School of Economics established a revolutionary new way of thinking about capital as a roundabout means to more productive ends. This idea didn't originate in Austria however; its roots are found in Ancient China, in the wisdom of Daoism.

Daoists saw everything as a result of its opposite: hardness from softness, strength from withdrawal, advancement from retreatment. Though it seems counterintuitive on the surface, this wisdom can also be applied to markets: profiting by _not_ investing. In these blinks, you'll learn how withholding your investments until the right moment is better than directly pursuing profits, like most people would do. You'll learn how you can approach the market like a Daoist, to gain more money in the long-run.

You'll also learn:

  * why Robinson Crusoe was a great investor;

  * what a marshmallow has to do with unwise, hasty investments;

  * how the market functions like a natural forest;

  * why central banks should allow small market crashes from time to time;

  * how Daoism made China's military so strong; and

  * how Ancient Chinese wisdom was relevant to nineteenth century Vienna and is relevant today.

### 2. "Austrian investing" means applying the Chinese philosophy of Daoism to markets. 

The concept of _Austrian investing_ starts with a paradox: you've got to love to lose money, and hate to make it. The wisdom of this paradox reaches back more than two and a half millennia to the Chinese philosophy of Daoism.

In Daoism, the best path to anything is through its opposite. So you gain by losing and lose by gaining.

Daoism emerged in ancient China during a time of heavy conflict. One of its central concepts is _wei wuwei_, which literally means "doing by not doing." In warfare, that meant not attacking until you could secure a position of advantage, and turn the opponent's own force against him.

You can see this concept at play in the Daoist martial art _tuishou_, or "push hands." In tuishou, two opponents try to throw each other to the ground by going through a sequence of very subtle alternating feints and attacks.

The _tuishou_ exercise is a physical manifestation of the idea of waiting and exploiting your opponent's urgency. The real force in _tuishou_ isn't in pushing; it's in yielding.

Austrian investing follows the Daoist model of pursuing a roundabout route to market success. Instead of following a direct route towards immediate gain, Austrian investing takes the more _roundabout_ path towards immediate _loss_. This pursuit of loss is similar to the yielding in _tuishou_ : you pull back and experience short-term loss, so you can gain a more advantageous position in the future.

Like the Daoist concept of _wei wuwei_, the idea is to profit from other investor's impatience and intolerance of small losses, as well as their urgency to get immediate profits. Patience is key.

> _"The whole point of my approach to investing is that we must be willing to adopt the indirect route to achieve our goals."_

### 3. Robinson Crusoe and Henry Ford are prime examples of how Austrian investing can lead to success. 

You've got to tolerate initial setbacks to make gains using the strategy of Austrian investing. Let's look a bit more at what this means.

The _Robinson Crusoe_ parable illustrates this principle well. When Crusoe gets stranded on his remote island, his first priorities are the basic necessities of life. To feed himself, he first tries to catch fish using his bare hands, which is clumsy and rarely works well.

Crusoe manages to reduce the time he has to spend fishing by constructing more efficient fishing tools. He does this at the risk of starving.

Initially, he catches fewer fish, because he's spending more time building tools than actually fishing. After he's finished, however, he's able to catch _more_ fish in a shorter time period. Sometimes, "losing" something short-term can mean gaining future advantages that make the loss worth it. In Crusoe's case, his cause for short-term losses may have saved his life.

Henry Ford, founder of the _Ford Motor Company_, was also a quintessential roundabout investor. He created a new method of production that improved efficiency: the assembly line, a process by which parts are added as the semi-finished product moves from one workstation to another.

The company's transition to success followed an indirect path. Ford invested a tremendous amount of time, and all his earnings from his first few cars into research and development of the assembly line. There was very little to show for his sacrifice in the beginning.

Eventually, however, his investment paid off tremendously. After the new assembly line process was finalized, Ford was able to produce a new car every _twenty-four seconds_. The Ford Motor Company could suddenly and efficiently produce cars for the masses, at unprecedented rates.

### 4. Daoist concepts can be found in nature, especially in the forest system. 

Nature is our greatest teacher. It's also an important part of Daoism: a major Daoist theme is to observe and learn from nature, from things like the growth of _conifers_.

Conifers (also called pines, spruces and firs) illustrate the Daoist indirect path that leads to a position of advantage. They're also the oldest tree species on the planet, having first appeared roughly 300 million years ago.

Conifers have to compete with _angiosperm_, a flowering plant, for space in the forest. They do this by falling behind, because angiosperms have the advantage of growing quickly, which makes it easier for them to compete in the short-run.

However, conifers can overtake the angiosperm by slowly developing very strong roots and thick bark. Because conifers live much longer, they can eventually overtake the angiosperm in biomass and height.

After securing their area, conifers also become more productive than angiosperm. In their slower and earlier growth stages, they create a structure that can then lead to fast and efficient development.

Conifers teach us that it's much better to avoid direct competition for scarce resources. Instead, it's better to pursue the Daoist route that will lead to greater gain in the future.

Conifers also exemplify _wei wuwei_ (doing by not doing). They grow on rocks, and other areas where competing plants can't live. However, if conditions suddenly change and an opportunity such as a wildfire arises, we can see the _wei wuwei_ — conifers "seeding by not seeding."

The rocks on which the conifers grow give them a defensive position against other plants, but they also have an _offensive_ advantage after a wildfire. When new forest areas have been cleared by the fire, conifers can freely disperse their seeds in the wind.

All in all, conifers are soft, fragile and highly flammable, but they can flourish and thrive by strategically pulling back, growing slowly and seeding at just the right moment.

> _"Nature takes a roundabout, intertemporal approach."_

### 5. Daoist concepts were used in the military strategies of ancient China and 19th century Prussia. 

Ancient China and nineteenth century Prussia might seem worlds apart, but they have an important commonality: in both regions, wars gave rise to canonical texts on war strategy. In Ancient China, it was _Sunzi_, by the general Sun Wu. In Prussia, it was _Vom Kriege_ (On War), by the general Carl von Clausewitz.

_Sunzi_ applies the Daoist indirect approach to military strategy. It's one of the most important works on the topic, and has greatly influenced both Eastern and Western military thinking.

The military strategy outlined in _Sunzi_ can be summed up in a single word: _shi_. _Shi_ has no direct translation in English. It's more defined by a cluster of meanings, two of the most important of which are _strategic value_ and _positional advantage_.

_Shi_ conveys the significance of gaining influence through nonintervention so that you can secure an advantageous position in battle. _Shi_ is to the _Sunzi_ what _wei wuwei_ is to Daoism.

The Sunzi states that the highest mark of excellence in battle is to subdue the enemy's army without fighting at all, or in other words, "marching without appearing to move." Brute strength isn't always the best way to win a fight.

A similar principle can be found in _Vom Kriege_, which has also been influential, more so in the Western world.

In _Vom Kriege_, Clausewitz uses the German words _Ziel_, _Mittel_ and _Zweck_ to describe a Daoist-like approach to war. In Clausewitz's approach, the immediate aims ( _Ziel_ ) are to weaken the enemy at strategic focal points to gain a positional advantage. After their defenses are down, this position can be used as a means ( _Mittel_ ) to reach the ultimate end ( _Zweck_ ) of winning the war.

### 6. The market is an ongoing process and cannot be considered empirical. 

Austrian investing originated from a new school of economic thought, the _Austrian School_, in late nineteenth and early twentieth century Vienna. The economist Ludwig von Mises, one of the school's pioneers, famously proclaimed, "The market is a process!" which has become a tenet in Austrian investing.

His statement is true; the market can only be understood as a process. In Daoism, this process or path is called the _Dao_.

Because markets are driven by the actions of countless people, economics is largely the study of human interaction. However, there are no constants on human behavior like there are in the natural sciences, such as the charge on an electron. Human actions are highly subjective.

Because of human action, the market can only accurately be viewed as _Dao._ It's constant and ongoing; its a series of causes and effects towards the various goals of people participating in it.

Naturally, it follows that the market can't be considered empirical either. It's impossible to observe the behavior of an individual market participant, isolated from the other participants. This means that we can never conduct proper experiments on any markets. Moreover, any attempts to predict market movements using empirical data will always be a bit nebulous.

Economists can't rely much making sense of historical patterns either. Scientists can make predictions based on prior research, but economists aren't able to do this because of the lack of laws regulating human action.

If investors could use history to predict market movements, they'd never be surprised by them or lose huge amounts of money. Yet this is precisely what happened in the 2008 global financial crisis, which took virtually the entire world by surprise. The market is always elusive, and must be understood as the unpredictable and chaotic entity that it is.

> _"Indeed, capital is a process, or a method or path — what the ancient Chinese called the Dao."_

### 7. The market is naturally self-correcting, and intervention from outside weakens its balancing forces. 

A system should naturally achieve balance through internal guidance. Any attempts at intervention are typically counterproductive. The Austrian School teaches this in regard to markets: government intervention doesn't balance markets — it distorts them.

The market is like a forest: it has natural regulators that keep it balanced. A forest is kept in balance by the constant battle for resources among the creatures that live there.

For example, when areas of the forest become too dominated by the overgrowth of angiosperm, they become prone to small wildfires. When fire breaks out the land is cleared, which allows the conifers to reseed.

Thus, the fire is not merely destructive. It's a cleaning process, and it's another mechanism for keeping the forest in balance.

Markets are like our financial forests. Malinvestments can thrive for a short time, but they'll eventually end in bankruptcies. Small "fires" like this free up and redistribute resources by releasing capital to new areas.

Intervention weakens both the forest's and the market's balancing forces. Forest fires can become deadly when smaller blazes are suppressed by any kind of human forest management. When smaller fires are suppressed, trees have no opportunity to replace each other, and the forest becomes feeble and prone to even more destructive fires.

A similar phenomenon occurs in markets, when central banks make artificial changes by printing more money to counteract small crashes (or "fires"). The central bank can print money, but it can't print its underlying value such as real estate or gold. Printing more money than there is value inevitably distorts the market's "natural" state and makes it prone to malinvestment.

Forests and markets are both self-regulating, and intervention from outside exposes them to unnaturally occurring damage.

### 8. Austrian investing is difficult to implement because it goes against our instinct to seek immediate gratification. 

We can only succeed with Austrian investing if we can stop being so focused on the moment. This is extremely challenging, however. Why exactly is this so hard for us?

Well, humans are simply designed to prefer things that are immediate and direct, rather than intermediate or indirect. We want things that benefit us _now_.

Walter Mischel, a famous psychologist, researched this phenomenon extensively in the 1960s, through the _Marshmallow Test_. In the Marshmallow Test, children were allowed to choose a treat (often a marshmallow). They were then told that they could either eat it immediately, or wait fifteen minutes and have two.

The children were then left alone in a room with their treats, while the researchers secretly observed their behavior. Only a few of them could resist eating it, even though they knew they could have more in the future if they did so.

As our brains mature, we develop the ability to wait for longer periods of time, but it's still our natural tendency to focus on the immediate. This is part of our evolution: our ancestors needed to focus on any immediate threats in order to survive.

This tendency is exacerbated by our culture, which teaches us that the moment is all that matters. We focus on what we can see and experience in the short-run.

Signs of this are all around us. Savings rates are very low, and we pillage the finite natural resources of our earth for our immediate use.

Austrian investing is difficult to follow because it requires us to overcome this human tendency. Although it's counterintuitive, there are many ways to implement it, as the following blink discusses.

### 9. You have to be patient and seek out highly productive capital to benefit from Austrian investing. 

So how can we go about implementing Austrian investing? How do we set up for the moment when our patience can pay off? How do we know in which firms to invest?

The first step in Austrian investing is to stay out of the market when _distortion_ is high.

Distortion occurs when central banks print too much money, and thus create artificially low interest rates. This only leads investors into malinvestment, which in turn can lead to stock market crashes.

A distorted market is prone to crashes, just like an overgrown forest is prone to fires. So stay out of it, to avoid the crash that will inevitably result. Keep your capital in reserve on the sidelines, and wait until the distortion passes for you to invest.

The next step in Austrian investing is to seek out highly productive capital. Remember that the most productive capital is the most _roundabout_ capital.

For example, you can find technological ways of producing more output with different inputs. Remember this means that you'll have to wait longer and reinvest in research and development. If you spend time to improve the right technologies, you can see great gain, as Crusoe learned on his island.

The first criterion for investing in a company is that it should show a high ratio of _reinvested profits_ that can eventually make it more efficient. As we saw earlier, the Ford Motor Company exhibited this reinvestment criteria in its early stages; Ford invested the profits from his first cars into the assembly line development.

Second, look for firms with a low market value, those which other investors don't appreciate because they grow too slowly. These firms are likely to come through with great advantages at the end of an indirect route.

> _"I earn my living from the hungriness of investors, from their decisiveness, their forcefulness, from their great urge for immediacy."_

### 10. Final summary 

The key message in this book:

**Austrian investing is an approach that focuses on long-term profits rather than immediate gains. The conceptual ideas of this investment approach trace back to ancient Chinese Daoist concepts. They were fully developed in the Austrian School of Economics and can also be found in nature. You can apply Austrian investing techniques by following a roundabout route of accepting small, initial losses to build a position of advantage for greater gains in the future.**

Actionable advice:

**Stay out of a distorted market.**

Before entering the market, consider if distortion is high. Are central banks creating artificially high or low interest rates? If so, keep waiting to invest. Don't be too hasty; distorted markets are prone to crashes, meaning investors are likely to lose money. If you have the patience to wait out the distortion, you'll benefit a great deal later.

**Suggested** **further** **reading:** ** _Antifragile_** **by Nassim Nicholas Taleb**

Some things seem to improve if they are placed in environments of volatility and unpredictability. _Antifragile_ analyzes why this is the case. It suggests that this quality has been vital for the progress of human civilization since ancient times. Nassim Nicholas Taleb takes a critical look at modern society and its aim to smooth out life by interfering in systems like the economy. Far from making society a better place, this interfering nature is destroying the volatile environment essential for antifragility to take place.
---

### Mark Spitznagel

Mark Spitznagel is the founder and President of _Universa Investments_, an investment advisor that specializes in profiting from extreme stock market losses as a means of enhancing investment returns. In addition to hedge fund investing, Spitznagel's roles in his twenty-year investment career have ranged from independent pit trader at the Chicago Board of Trade to the proprietary trading head of _Morgan Stanley_.

Mark Spitznagel: The Dao of Capital copyright 2013, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

