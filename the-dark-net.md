---
id: 588f630c1a8b8f00045e1332
slug: the-dark-net-en
published_date: 2017-02-03T00:00:00.000+00:00
author: Jamie Bartlett
title: The Dark Net
subtitle: Inside the Digital Underworld
main_color: 2BAAD7
text_color: 1F7999
---

# The Dark Net

_Inside the Digital Underworld_

**Jamie Bartlett**

_The Dark Net_ (2014) is a window into the internet's nefarious underbelly. These blinks detail a trove of hidden online activity, from drug deals to illegal pornography to troubling discussions among suicidal teenagers.

---
### 1. What’s in it for me? Discover how the internet reflects the darker sides of humanity. 

The internet has revolutionized our lives and must surely be one of humanity's greatest inventions — but it hasn't only brought along positive changes. There is also a hidden, more sinister side of the internet, where our more shadowy sides live a life sheltered from the prying eyes of the rest of the world: the Dark Net.

Sometimes this darkness appears in plain sight, like when people leave nasty comments with the express purpose of insulting or angering other people. But most of what takes place on the Dark Net remains unseen by the vast majority of internet users.

So, let's grab a flashlight and take a trip into the internet's heart of darkness.

In these blinks, you'll learn

  * how the dark side of the internet inspired the 2011 terrorist attack in Norway;

  * what potentially harmful type of website has been seen by one in ten teenagers; and

  * how your next-door neighbor might well be on his way to becoming a porn star.

### 2. The internet enables online threats to public figures and an insidious game called trolling. 

Have you ever been insulted by a total stranger on an anonymous chat service or social network? If so, you're not alone. It's actually a common phenomenon, and one that can be especially troubling for high-profile people who are fighting for a controversial cause.

For instance, the success of the 2013 campaign to feature Jane Austen on the new ten-pound note in the UK was in part due to the work of feminist journalist Caroline Criado-Perez. But not everyone agreed with her message. She got thousands of troubling anti-feminist Twitter messages, some of which even contained threats of rape, violence and murder.

Criado-Perez was forced into hiding while the police arrested two people who had posted some of the most egregious threats. And Criado-Perez isn't alone in suffering anonymous online abuse; this vile pastime is called _trolling_ and it's only getting worse.

Trolling is the act of making comments via the internet with the intention of upsetting other users and goading them into a reaction. The word stems from the verb "to troll," which means to slowly drag a baited fishing line through the water.

Just how bad is it?

Well, there were 498 convictions in England and Wales related to aggressive, indecent or offensive online behavior in 2007. By 2012, that number had spiked to 1,423.

But trolling isn't only about threatening other people's lives. In reality, it's often much more subtle and playful, with lots of trolls simply motivated by a desire to stir up trouble.

Just take Zach, a troll interviewed by the author. He joined a popular right-wing website and posted a poorly written message that complained about how uneducated conservatives were. He received loads of indignant messages in reply, to which he responded with a wave of messages containing pictures of his penis, literary quotations and various insults.

His only motivation was entertainment.

> About one-third of the British youth have a friend who has been the victim of anonymous online trolling.

### 3. Political extremists and lone terrorists use the internet to share their unsavory beliefs. 

You've probably never been face-to-face with someone who would say something racist and insulting like, "The world is a mess because black people are taking over." But people are a lot less shy about voicing these kinds of views online.

As a result, various social media platforms offer a suitably discreet outlet for political extremists. In fact, websites and social media platforms like Facebook, Twitter and YouTube have become the preferred meeting place for people with ideologies that aren't accepted by mainstream culture.

For instance, the right-wing extremist British National Party's website has been the most popular political party site in the UK in recent years, according to website ranking company _Alexa_. It attracts more visits than the Labour or Conservative Party websites.

When it comes to Twitter, neo-nazi groups are particularly active. These users can often be picked out by usernames that contain the numbers 14 and 88. 14 stands for the "14 words" they subscribe to: "we must secure the future of our people and a future for white children." And 88 refers to the eighth letter of the alphabet "H," which, when doubled as "HH," stands for "Heil Hitler."

So, the web has become a popular place for white supremacists to meet and greet, but it's also the preferred platform for dangerous terrorists who act alone. One example is Anders Behring Breivik, the man responsible for the 2011 terrorist attack in Norway. He acted alone and his only source of contact with other extremists was online.

As a young computer programmer, Breivik was obsessed with blogs and various posts regarding the threat to the white race as well as the rise of "cultural Marxism." Before his violent attack, he wrote a 1,516-page manifesto titled _2083: A European Declaration of Independence_.

Then, on the 22nd of July, he traveled to Utøya, an island where he gunned down 69 youth members of the Norwegian Labour party who had gathered on the island for a reunion. While Breivik is now in prison, his _2083_ manifesto is still being circulated online by his supporters.

> "The internet has given the ordinary person access to far-right groups in a way that was impossible a decade ago." - Nick Lowles, campaign director of Hope Not Hate

### 4. Legal pornography can quickly lead users to off-limits content that encourages illegal sexual activities. 

Can you imagine how embarrassing it would be to receive a message saying your computer will be confiscated by Interpol because you watched illegal online pornography?

Well, it's actually not uncommon for people to get messages like these. Although they're usually just caused by viruses or malware, they do point to a major problem: links on the internet easily guide people from naughty but legal porn to illegal content.

For instance, when you visit a legal porn site, it will bombard you with links to, or pop-ups for, other sites. Users who follow these links move in a downward spiral toward increasingly nefarious content that can culminate in child or animal pornography.

This topic was researched in 2013 by the _Lucy Faithfull Foundation_, an organization committed to preventing sexual abuse of children. They found that nine out of ten adults who ended up on such sites did so unintentionally.

However, it's difficult for the police to determine whether or not that's true. It could just as easily be that real sex offenders are intentionally pretending to stumble upon illegal material.

Beyond that, the internet has the power to stimulate or encourage illegal sexual tendencies. For instance, the author interviewed a man who went by the pseudonym "Michael" and who was arrested for viewing illegal porn. He began with more run-of-the-mill content, but eventually stumbled upon some videos that featured 15- and 16-year-old girls, and watched those. At some point, he transitioned from these young teens to pornography that clearly featured children.

Why did this happen?

Well, according to Professor Richard Wortley, who teaches at the Crime Science Institute of London, porn can stimulate an increased desire for young adults and, eventually, children. The appeal here lies in both the youth of those depicted and the clearly forbidden nature of the content. In other words, the taboo triggers an adrenaline reaction which increases the viewer's excitement.

> "It happened in tiny increments. I really don't remember when I moved from teens to children. But I did." - Michael, anonymous internet user

### 5. The internet hosts controversial communities that promote anorexia and suicide. 

Everyone knows at least one or two people who struggled with serious depression. Even so, very few people are willing to actually listen to the dark thoughts and emotional struggles of others.

So, where do these people go to openly discuss their problems?

As you might have guessed, they turn to the internet. In fact, some of the most controversial online communities are those devoted to self-harm.

One of the first incarnations of these groups was found on a social platform from the 90s called _Usenet_. The group was called _alt.suicide.holiday_ and was created in 1991 by a California man named Andrew Beals to support people who wanted to commit suicide during the holidays. It was a place to discuss reasons for suicide and possible methods of killing oneself.

The group was controversial to say the least, but forum like it can be productive. For instance, Gerard, a 30-year-old depressive and suicide forum participant, says that such groups saved his life by providing a space for him to talk about his problems — problems that nobody in the "normal" world wanted to hear.

Around the same time, in the late 90s, the internet also gave rise to pro-anorexia or _pro-ana_ websites. These sites present anorexia as a lifestyle choice instead of as a life-threatening illness, and encourage their members to be goal-oriented in their weight loss plans.

Dr. Emma Bond of the University of Suffolk conducted research on the pro-ana community in 2012, and found a whopping 400 to 500 official sites and blogs devoted to the topic, which in turn link to thousands of smaller blogs. Not only that, but a 2012 study conducted by the European Union found that at least ten percent of young teens have visited a pro-ana site.

As you can imagine, the results of such sites can be devastating. Just take Amelia, who joined a pro-ana site at the age of 13. Three years later, she was so weak she could hardly walk. For her to begin her recovery, she was taken to the hospital — against her will.

### 6. The internet is a haven for drug dealers. 

Skulking figures exchanging illicit drugs in dark alleys is a common image when people think of the drug trade. While these kinds of transactions certainly take place around the world, drug deals can happen in a much simpler way in the information age — without either of the two parties leaving the comfort of their own homes.

That's right, the internet even provides hidden spaces for dealing drugs. One such place was called _Silk Road_, a website that began spreading through the underground internet community in 2011. This encrypted site was a marketplace from which you could purchase any number of illegal goods.

But that's not what made Silk Road stand out. Unlike prior illegal trading sites, this one was highly professional. It had well-organized product categories, vendor contact details and even user reviews. More importantly, the site was extremely secure.

How?

First off, it was only accessible through a _Tor browser_, a free program that lets people surf the web with anonymity; second, products could only be purchased with the digital currency known as _Bitcoins_ ; and finally, visitors were encouraged to use pseudonyms. Even if a user did provide their real name, all their communication was encrypted and automatically deleted once the messages were read.

With such a complex system at play, it's no wonder that, despite the best efforts of law enforcement, the internet was and is still is one of the safest places to sell drugs.

By July 2013, police were carefully monitoring everything that happened on Silk Road. At that point, at least 150,000 customers had done business through this online black market. FBI agents had been purchasing items from the site since 2011 in the hopes of tracking down vendors who could lead them back to the site's administrators.

Then, on the October 1st, 2013, the FBI arrested Silk Road's creator: Ross Ulbricht, a 29-year-old with a suspiciously high Bitcoin balance. Several other arrests followed shortly thereafter and, eventually, Silk Road was shut down.

Well, at least for a month. Not long after the site got busted, a better and safer version was up and running: Silk Road 2.0.

### 7. Webcams have made pornstars out of anybody who wants be one. 

How would you describe a porn star?

In the past, the answer to this question would have revolved around fake breasts, botoxed lips, platinum blonde hair and a spray-on tan. But nowadays, a popular pornstar might not look any different than the person sitting next to you on the train.

Webcams have transformed the porn industry into a free-for-all. It used to be that making porn required contacts, studio space and plenty of professional equipment. But now, high-quality webcams have made it easy for anyone to capture and broadcast their naughty performances for money.

A good example is _Chaturbate_. One of the most popular places online for amateur porn, this site offers users access to upwards of 600 amateur models who perform online shows throughout the day. These so-called "camgirls" and "camboys" earn money by soliciting tips from their viewers in exchange for "seeing more."

The site facilitates these transactions by selling tokens — and retains 60 percent of the tokens purchased. In exchange, the performers can develop a strong brand and establish a customer base.

So, amateur porn is extremely popular, and this might well be because the models look like everyday people. Just consider a 2013 study of females in the professional porn business, which found that actresses need to meet specific measurements, including a height of 5.5 feet and a shoulder-waist-hip ratio of 34-24-34.

But cam models don't have to fit a particular body type. They just need to be willing to show themselves off online. As a result, models include couples having casual sex, naked men playing guitars, bored women staring into space and men just masturbating.

The most important factor is that, unlike professional porn, the amateur porn industry has tremendous variety. That's why, according to a _New York Times_ article from 2013, _camming_, as it's called, is a billion-dollar-a-year industry that has cut a 20 percent slice out of the pornography market. It's so lucrative that lots of cam models support themselves working just an hour a day.

### 8. Alternative digital currencies are independent of any government. 

Nerds, geeks and otherwise socially ostracized people are encouraged by the biblical promise that the meek will inherit the world — but perhaps this has already come to pass.

After all, the nerds of the twenty-first century are _cypherpunks_, advocates of computer code as a means of social change, who are determined to build an alternative economy. This work began in the 90s, when a computer cryptographer and cypherpunk named David Chaum tried to set up a company to produce a digital currency that he called _DigiCash_.

In Chaum's system, each unit of money was given a unique number that could be readily transferred by users online. But it had a major flaw: such a string of numbers could easily be copied, devaluing the currency in the process.

To protect against this, Chaum built a central ledger that tracked all transactions, thereby ensuring that no unit of currency was duplicated. The problem was that the system was built around one central control system. It was just too risky, and the project fizzled out.

Then, in 2008, the first functional digital currency was established: _Bitcoin_. Its development began in that same year when an anonymous cypherpunk, using the alias Satoshi, introduced what's called a _blockchain_ to secure Chaum's model.

Through this system, each piece of the data string is made up of ten minutes worth of transactions, which are ordered chronologically and end with a hashtag. This blockchain, a record of every transaction ever made using Bitcoins, is installed and constantly updated on the computers of all users who exchange the currency.

As a result, every computer involved in trading Bitcoins is perpetually testing whether duplication has occurred, making this independent currency, which is beyond the control of any government, totally secure.

### 9. Final summary 

The key message in this book:

**The internet is so much more than just Google and e-mail. There are far reaches of the web that provide an anonymous safe haven for anyone with reason to seek out more privacy, from drug dealers to anorexic teens.**

Actionable Advice

**Learn computer code and save the world.**

Being able to penetrate and navigate the Dark Net could provide you with the tools you need to unmask lone terrorists before they can attack innocent citizens, shut down websites that protect political extremists and even build an alternative economy that's free of the crimes of fraudulent bankers. So, consider learning to code — it could be the most powerful weapon there is.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Future Crimes_** **by Marc Goodman**

_Future Crimes_ (2015) lucidly explores the dangers inherent in using today's highly interconnected web of technologies. Through carelessness or ignorance, we make huge amounts of personal information available to criminals who would love nothing more than to exploit us.
---

### Jamie Bartlett

Jamie Bartlett is Director of the Center for the Analysis of Social Media and a frequent contributor to the _Daily Telegraph_. _The Dark Net_ is his first book.

