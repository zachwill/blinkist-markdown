---
id: 53d779383661640007000000
slug: the-death-of-money-en
published_date: 2014-07-29T00:00:00.000+00:00
author: James Rickards
title: The Death of Money
subtitle: The Coming Collapse of the International Monetary System
main_color: 939049
text_color: 666433
---

# The Death of Money

_The Coming Collapse of the International Monetary System_

**James Rickards**

_Death_ _of_ _Money_ examines the current global monetary system, centered around the dollar. If current policies continue, a total collapse is imminent. You should prepare for the worst.

---
### 1. What’s in it for me? Find out how to prevent your savings from being obliterated by the impending economic collapse. 

Remember the financial crisis of 2008? Are you happy the worst is over now and the world can look forward to a period of growth, prosperity and happiness?

If so, prepare to be shocked.

The current monetary system, by which the money in your pocket has no intrinsic value, is in fact incredibly fragile and the next crisis is already looming on the horizon.

In these blinks you'll discover:

  * why governments and central banks are deliberately making the money in your pocket less valuable,

  * where the next crisis will come from and how you can protect your life savings from being worth less than a loaf of bread, and

  * how American locomotives were recently traded for frozen turkeys, and why this may be the norm in the future.

### 2. No modern currencies have any intrinsic worth – only the state guarantees they are worth something. 

As recently as a few centuries ago, if you wanted to buy a loaf of bread from a baker, you had to pay with a piece of gold or silver, equal in value to the bread. These days, you can just hand over a piece of paper, a banknote, that isn't particularly valuable in itself. So why does the baker accept this scrap of paper as payment for his labors?

Simply because the government gives the paper value. This phenomenon applies for all modern currencies and is called _fiat_ _money_, _fiat_ is Latin for _let_ _it_ _be_ _done_.

Fiat money is basically a contract between its users and the state. The state has effectively promised the baker that he can exchange the banknote for something else of value, like flour or salt. As long as everyone trusts in the state to give banknotes this so-called _extrinsic_ _value_, they don't need to have _intrinsic_ _value_ like gold coins once had.

Severing the connection between currency and matter of intrinsic value is as new as 1972. Until then, the global economy relied on the _gold_ _standard_, where the American government had pledged to fix the value of the dollar to the price of gold at a fixed rate of $35 per ounce of gold. Other countries then fixed their exchange rates to the dollar, so actually all currencies were linked to the price of gold.

But after the financial crisis of 1973, governments around the world lost their confidence in the ability of the US to maintain the fixed rate between dollars and gold. The US concluded that it had to sever the connection to gold, and let the currency — and therefore every other currency — fluctuate freely.

After this point, every currency has truly been devoid of material value, and can only be influenced through government monetary policy.

But today, the modern global economy that is based on freely floating currencies is under attack. In the next blinks we will find out why.

### 3. Financial warfare could wreak havoc on the fragile global financial system. 

When terrorists committed the 9/11 attacks, they didn't just attack people in New York and Washington. They also targeted the American economy.

How?

In the months preceding the attacks, they made huge bets on the share prices of airlines plummeting after the attack. When the share prices did in fact plummet, the terrorists profited greatly. This is an example of _financial_ _warfare,_ a strategy increasingly used by criminals to profit and by states to attack their enemies.

Financial warfare can be either offensive or defensive in nature.

Offensive financial warfare could for example mean hacking into the stock market of a foreign country to manipulate prices and thereby cause economic damage.

A real life example occured in 2012, when the Unites States attacked Iran by excluding it from a system for relaying global payments. As a result, Iran could no longer swap its currency for dollars or euros, making the Iranian currency depreciate as it became undesirable for use in trade.

Defensive financial warfare, on the other hand, are used to protect capital markets or to retaliate against states who have attacked.

Iran, for example, defended itself by buying up gold in bulk in the months before the US offensive. That way, it could still trade despite being cut off from the international payment system.

The potential seriousness of financial warfare was demonstrated by the financial crisis of 2008. If bad mortgages could accidentally lead to economic losses of $60 trillion, just imagine how much damage could be done by experts who understand the financial system and deliberately set out to do harm. And the global economic system is very fragile and vulnerable to such attacks, so even small terrorist or criminal groups could wield huge damage on all of society.

Having come to this realization after the financial crisis, the US and China have now developed procedures for financial warfare alongside their traditional military forces.

### 4. Contained within the financial system are the seeds of future crisis. 

It's been over five years since the financial crisis, so you may be thinking that the worst is now over and the coming years will be pretty breezy.

Unfortunately, the future might not be quite so rosy: a few potential crises may still lurk within the financial system.

One such potential crisis stems from the fact that although the Chinese economy has expanded dramatically over the last decade, this growth has been poorly planned. Far too much investment has been directed into the wrong areas of society due to massive corruption in the ruling Communist Party.

One example is the many ghost cities all over the country: huge deserted urban areas with buildings, parks and roads that no one can afford to use. They have been built due to the government's policy of starting major projects and encouraging investment into them, without caring about whether there is demand for the end result.

Unfortunately, this policy has created an investment bubble, which could soon burst as investors find they aren't getting any return on their investments. A burst in the Chinese market could put the whole global economy in danger of collapse, as investors from around the word have poured money into China, and thereby into these pointless projects.

Another potentially looming crisis can be found in the US. Even though the mortgage crisis is over, there is another bubble rapidly forming, this time in the realm of student loans.

Why?

Because whenever politicians want to insert money into the economy to stimulate consumption, they love to do so by giving students more generous loans. The politicians know they can rely on students to spend the money in the economy.

Currently, the US government and several financial institutions have given out over $1 trillion in student loans. Unfortunately, because the US economy is still in a slump, students aren't finding jobs after graduating. The result is that they can't pay back their student loans and will likely soon default in droves. Such a default en masse could push US households and the financial markets back into turmoil.

> _"China is a fragile construct that could easily descend into chaos, as it has many times before."_

### 5. The global dominance of the US dollar as the reserve currency is coming to an end. 

Have you ever travelled to a country in, say, Africa or Asia with a weak domestic currency? If so, you may have noticed that locals tend to happily accept US dollars in shops and stalls.

They're happy to because the US dollar is the world's _reserve_ _currency:_ virtually all states hold huge reserves of it to use for international transactions like trade. As a result, it is seen as a reliable and stable currency, so people in some countries prefer it over their own. In fact, the whole global financial system is based on the dollar being the main currency — for trading.

Yet developments over the past few years have led to some states questioning their reliance on the dollar. One reason is that since most trade takes place in US dollars, emerging economies like the BRICS (Brazil, Russia, India, China and South Africa) countries are unhappy that their economies are so directly affected by the monetary policy of a foreign power — the US.

Another group promoting change is the OPEC states (Organization of the Petroleum Exporting Countries). Traditionally they, especially Saudi Arabia, have been happy to ply their oil trade in dollars in return for US promises of security. But recently the US has slowly began to rebuild a relationship with the Saudis' sworn enemy Iran, and this has spooked the Saudis into probing for alternatives to US friendship and their over-reliance on the dollar.

Besides these groups, the strengthening euro is also putting pressure on the global dollar-based monetary system. It may seem counterintuitive, since the Eurozone has just lived through a currency crisis, but in fact the crisis and the strong German economy pushed the Eurozone into restructuring itself into a stronger, more competitive economy, far more stable than that of the US. It may be time to contemplate a greater role of the euro in the global monetary system of the future.

> _"The coming collapse of the dollar and the international monetary system is entirely foreseeable."_

### 6. So far, the US has tried to tackle its massive national debt through inflation. 

If you've watched the news lately, you've probably noticed a lot of talk about the immense size of the US national debt. And for good reason: for the first time since the Second World War ended, the national debt has grown to larger than the entire US economy. Consequently, political debate has focused on how to bring this debt down.

There are two main methods for doing this.

The prudent and sensible way is to strengthen and grow the economy. This can be done by, for example, investing in the development of infrastructure, like transport and communication systems that foster economic growth. Unfortunately, it is a hard pitch to sell to the electorate, and it's also hard to know what investments to make.

Politicians often favor the politically easier but economically more problematic method: encouraging inflation so it eats away the debt by making the dollar less valuable. To understand what this means, consider the following example:

If the US government borrows $100 and there's no inflation, in one year that debt will still be worth $100. But if inflation is running at 10 percent, then after one year the debt will only be worth $90.

In order to increase inflation, the US government has resorted to printing more money and injecting it into the economy. This method is known as _quantitative_ _easing_, and it results in people having more money to spend, which drives up prices. To understand the scope of this printing operation, consider that it has reached the record-breaking speed of one trillion dollars a year.

So what's the problem?

So far the strategy is not working, because the dollar is also subject to deflationary forces that work to increase its value.

For example, the price of energy in the US has been dropping thanks to the tapping of shale gas. Also, labor costs are dropping as cheap labor enters the market from many developing countries.

These deflationary pressures are hindering the US from reaching the desired inflation, and hence the debt continues to grow.

> _"Today the central banks, especially the US Federal Reserve, are repeating the blunders of Lenin, Stalin, and Mao."_

### 7. The US’ efforts to increase inflation have lead people to look for alternative currencies. 

In the previous blink, you saw how the US government is trying to tackle its enormous debt by printing more money.

Unfortunately, this tactic is eroding people's confidence in the US dollar.

Why?

Because printing more money just creates a _money_ _illusion:_ You may think that growth is picking up, but it doesn't actually expand the economy or make it any healthier in the short term. And in the long term the currency is just becoming less and less valuable, eroding the savings of people and investors, thereby decreasing their confidence in the dollar.

This loss of confidence applies to the dollar-backed global monetary system as well, and it is already noticeable in the newly emerging payment alternatives to cash.

One prominent example is the current trend toward electronic cash such as _the_ _bitcoin_. This digital currency is completely independent of any government or central bank. By late 2013, over $7 billion worth of bitcoins were already in circulation. While the currency's viability in the long-term remains unclear, its popularity does indicate that people are desperate for alternatives to cash.

Another interesting new development is _electronic_ _bartering:_ trading goods online without using cash as an intermediary. For example, one time China Railway received a load of frozen turkeys from one of its customers, and subsequently traded them to General Electric (GE) for locomotives. GE then sold the turkeys to Tyson Food China for cash, but in the original deal no cash changed hands.

### 8. The global monetary system needs a truly global currency instead of the dollar. 

By now we've seen that the current dollar-backed global financial system is in trouble. So what can be done about it?

One option would be to turn to the International Monetary Fund (IMF), whose role is to lend funds to countries who desperately need financial support. To lend, the IMF uses its own quasi-currency, the _Special_ _Drawing_ _Rights_ _(SDR)_.

Although it's not officially a currency, the SDR functions in a similar way: it holds a value that is derived from the world's major currencies, can be swapped between countries and exchanged for other currencies.

Because the SDR functions like a currency and is more stable than the dollar, it is a candidate to replace the dollar as the global reserve currency. In fact the IMF and the US are discussing slowly moving in this direction.

The shift would bring definite benefits, the major one being that monetary policy regarding the SDR would be determined by the IMF, an international entity that would make its decisions based on what's good for the entire world. Currently, the US determines its monetary policy for the dollar based on its own national interests, which has on occasion hurt other states economically.

It seems that the whole world, including the US, is interested in seeing the IMF and its SDR take on a larger role in the global monetary system. After all, a weak monetary system is unstable and unpredictable, and therefore not in anyone's interests.

### 9. Reintroducing the gold standard would stabilize the global monetary system. 

"Gold is for losers," said the legendary investor guru Warren Buffett. Indeed, in the past few decades, the global monetary system has increasingly distanced itself from the value of gold.

And yet, if the goal is to build a stable monetary system, we should consider bringing gold back into the equation.

Why?

Because linking currencies to gold would provide a stable reference point — an anchor — for their value. Currently, the dollar, for example, is not backed up by anything solid, merely the promise of the US government and central bank that it has value.

One financial writer called this system the "PhD standard:" The dollar's value is tied only to the actions taken by an elite of Ivy League graduates who determine the policy.

Of course, the gold standard can't be reintroduced on a whim — it needs careful planning and consideration.

One crucial concern will be how to correctly determine the price of gold. The current price of about $1500 per ounce can't be used, since at this price all gold in the world wouldn't be enough to back up the worth of the world's $2.5 trillion money supply. So the central banks would need to determine a new price closer to $17,500 per ounce in order to cover the money supply of the US, EU, Japan and China.

It seems like far from being just the material for necklaces and rings, gold may in fact be the savior of our global monetary system.

> _"In markets today, the dead hands of the academic have replaced the invisible hand of the merchant or the entrepreneur."_

### 10. Spread your wealth out into many assets to protect it should hyperinflation strike. 

If you had been alive in 1920s Germany, you would have perhaps seen a strange sight: someone buying a loaf of bread, and paying for it with a _wheelbarrow_ full of money.

This bizarre circumstance was due to out-of-control inflation or _hyperinflation_ : prices were rising so quickly that money was basically worthless. Of course, this kind of situation is untenable, and in this case, it lead to social disorder and eventually the population turning to Adolf Hitler.

Unfortunately, the current situation looks like it is only a matter of time until the dollar also tips into hyperinflation. Faced with this dire circumstance, you should take precautions to ensure you will survive and thrive even if the worst happens.

How can you do this?

The simplest precaution is to ensure that your wealth is invested in the broad mix of assets and areas. If you've put all your wealth into just one asset, like a savings account at your local bank, it could all be wiped out by hyperinflation.

Instead, you should have a diverse investment portfolio:

  * Keep 20 percent of your assets in gold bars, for their value is relatively stable.

  * Invest 20 percent in property in locations where there is an influx of people. This kind of property will increase in value no matter how the economy fares.

  * Invest 20 percent in alternative financial instruments like hedge funds. These investment funds specialize in profiting when the economy crashes or fares poorly.

  * Use 10 percent to buy fine art like paintings and sculptures. This way you'll have something of value that's tangible and small enough to carry with you if need be.

  * Finally, keep 30 percent of your wealth in cash in the bank. Cash is the most flexible instrument to hold, so if a crisis should strike you can allocate it to whatever investment seems most prudent at the time.

This kind of diverse portfolio will serve you well even if hyperinflation and social unrest strike.

### 11. Final summary 

The key message in this book:

**The** **dollar** **will** **not** **be** **the** **world's** **reserve** **currency** **for** **much** **longer.** **US** **monetary** **policy** **is** **eroding** **people's** **confidence** **in** **the** **dollar** **and** **eventually** **it** **will** **suffer** **from** **hyperinflation.** **You** **should** **prepare** **yourself** **for** **this** **scenario.**

Actionable advice:

**Spread** **your** **wealth.**

Do you keep all your savings in the bank? If so, you may be exposing yourself to much more risk than you think, because hyperinflation could wipe out the value of your savings in no time. You'll be far better off distributing your savings over a variety of investments, like gold, real estate and so forth. Your local bank can probably even help you decide where and how.
---

### James Rickards

James Rickards is an economist, lawyer and investment banker, especially well-known for his 2011 _New_ _York_ _Times_ bestseller _Currency_ _Wars_.

