---
id: 5b98d03f862c430007dc4c79
slug: the-death-of-truth-en
published_date: 2018-09-13T00:00:00.000+00:00
author: Michiko Kakutani
title: The Death of Truth
subtitle: Notes on Falsehood in the Age of Trump
main_color: 8DD0C9
text_color: 456663
---

# The Death of Truth

_Notes on Falsehood in the Age of Trump_

**Michiko Kakutani**

_The Death of Truth_ (2018) offers an informative look at the current political climate in the United States, and the many developments in the past that have brought us to this divisive time. With over 30 years' experience as a respected literary critic, Michiko Kakutani uses her expertise in modern literature to show how authors of the past worried about many of the same concerns we're facing today.

---
### 1. What’s in it for me? Get a unique, literary-minded view of the modern political landscape. 

Do you feel like world affairs are too chaotic and surreal these days? Well, firstly, you're not alone, but secondly, these aren't new feelings. "Chaos" and "surreal" were the same words a distressed Philip Roth used in 1961 when he wrote a still highly relevant essay about how truth was becoming stranger than fiction.

This is just a sample of the kind of insights that veteran _New York Times_ book critic, Michiko Kakutani, has compiled to illustrate how we ended up in the "post-truth era" of "fake news." As Kakutani sees it, writers like Roth, George Orwell, Aldous Huxley, Victor Klemperer, Robert Heinlein and Neil Postman all left very prescient warnings about the dangers that await when proven facts and established truths are denied or twisted by those in power.

In these blinks, you'll find out

  * what goes on in Russia's Internet Research Agency;

  * which Twitter comment President Trump accepted as a great compliment; and

  * how everything started with the Know Nothing Party.

### 2. There has always been opposition to reason and progress, but it went mainstream in 2017. 

Some of the guiding principles of the founding of the United States of America were drawn from the Enlightenment of seventeenth- and eighteenth-century Europe, with its emphasis on reason, liberty, progress and religious tolerance. But throughout the nation's history, there have been people who opposed these principles.

In 1839, Abraham Lincoln gave a speech known as the Lyceum Address, which emphasized the importance of reason in keeping tyranny at bay and preserving the rule of law in the United States. But even then, there was a separate ongoing narrative being kept alive by people working to undermine reason and progress. In 1855, this counter-narrative was perhaps best represented in the political arena by the right-wing Know Nothing Party, staunchly anti-immigrant and anti-Catholic, and counting 43 members of Congress at the time.

Those who supported the Know Nothing candidates weren't much different to those who push back against progress and reason today: they're part of a working class that feels marginalized and dispossessed of any agency, a fact that they tend to blame on progress made due to globalization and technology.

Writers have given this opposing movement different names over the years. Philip Roth called it "the indigenous American berserk," and Richard Hofstadter called it "the paranoid style." This counter-narrative has always been alive and survives just under the surface of the nation, not unlike a dormant virus waiting to reactivate. It wasn't until recently, in 2017, that it went mainstream.

Conspiracy theories and extremist views around religious and racial intolerance, which had largely been kept out of the national spotlight, started being written about by Breitbart bloggers and getting supported with retweets by President Trump that year.

We can see the actions Trump has taken against reason and progress by his choice of leaders for government institutions. Rick Perry once called for an end to the Department of Energy, and against all reason, he is now running it. Likewise, Scott Pruitt is now the head of the Environmental Protection Agency, which he has sued multiple times in the past. These important agencies have been rolling back their efforts and programs concerning renewable energy and the environment.

### 3. Although there’s been a rise in information options, the internet has created dangerous filters, silos and tribes. 

The way we get information has changed greatly over the past couple of decades. This "kaleidoscope of information options," as the author David Foster Wallace once called it, has resulted in outlets like Fox News that uphold a certain ideology rather than being impartial, and a media landscape where "the truth is wholly a matter of perspective and agenda."

Wallace's remarks are from 2005. That was before social media became as omnipresent as it is today and made it even more likely that information would be filtered and skewed before it reaches an individual. On top of this, since 2005, the right-wing news media has grown beyond Republican mouthpiece Rush Limbaugh and Fox News to include Breitbart and the Sinclair Broadcast Group, which controls the local news programs that reach around 38 percent of the United States.

These outlets respond to the news in a knee-jerk fashion that tends to ignore facts, data and evidence — especially on issues like gun violence, global warming and healthcare. Fans of these outlets respond in kind, either ignoring real evidence or dismissing it as information that was created with a liberal bias or originated from a vaguely defined "establishment" to which Trump supporters sinisterly refer as "the deep state."

In other words, people are only letting in the information with which they're predisposed to agree — a development known as _confirmation bias_. This creates what the author calls _partisan silos_ or _content silos_ — structures people build around themselves to contain only the information that fits their preferred story.

People are also sorting themselves into like-minded _tribes_ like never before, and the resulting group dynamic has only made confirmation bias worse. In fact, the ideological tribes with which we position ourselves operate not unlike terrorist networks. Recruits are cut off from any outside influence so they can develop an extremely polarized worldview.

How divided has the media landscape become? In a 2016 Pew survey, 45 percent of Republicans believed that Democratic policies were a danger to the nation's well-being, while 41 percent of Democrats felt the same about Republicans. Meanwhile, 70 percent of Democrats called Republicans "close-minded," and 47 percent of Republicans called Democrats "immoral." Hardly a landscape encouraging open, reasonable conversation.

### 4. Postmodern theory and subjectivism are being used to bolster division and a self-serving agenda. 

The postmodern movement led to some game-changing work by authors such as David Foster Wallace and Thomas Pynchon, as well as filmmakers like Paul Thomas Anderson, Quentin Tarantino and the Coen Brothers. However, postmodern thought has also entered the world of politics, which has produced some less-than-ideal results.

One of the primary tenets of postmodernism is that your reality or experience may not be the same as the next person's, due to any number of differences, including gender or class. In the United States, postmodernism gained traction in the wake of the Vietnam war and the Watergate scandal, when the public was hungry for a narrative other than the one the government had been trying to force-feed them.

So, postmodern thought helped popularize the idea that reality can, in some circumstances, be subjective. When the government was caught lying to the public about the war and the Watergate break-in, alternative narratives were seen as enlightening. But this concept can, and has, been taken too far — to the point where everything is subjective, and simple truths, facts and reality suddenly become debatable.

Even the verifiable fact of how many people showed up at the 2016 presidential inauguration was treated as subjective by the Trump administration. But as Kakutani explains, undercutting facts is not only ridiculous, it's also downright harmful.

Take Trump's willful disregard for the well-researched facts on the benefits of alternative energy sources and the dangers of climate change. These seem clearly designed to appeal to his followers and thereby widen ideological divide in the nation, as well as appeal to his donors in the fossil fuel industry.

Perhaps even more disturbing is the way he has used lies to stir up anger and fear in white working-class voters and provide them with scapegoats in the form of immigrants, women, black people and Muslims. He claimed that crime rates were soaring, when in fact they were reaching historic lows. Trump also called immigrants dangerous and a burden on the economy, although they are less likely than US-born citizens to commit crimes. What's more, immigrants and the children of immigrants have founded 60 percent of the nation's tech companies.

Tactics like these are straight out of the totalitarian script. In her 1951 book, _The Origins of Totalitarianism_, Hannah Arendt wrote that the ideal foundation for totalitarian rule was people who could no longer tell the difference between fact and fiction. Can _you_?

### 5. Fake news is being used as a propaganda tool, especially by Russian agents seeking to disrupt. 

The political manipulation toolkit involves more than just using subjectivity to raise doubts about basic facts. Another way to chip away at democracy is through propaganda, which is what a lot of today's "fake news" actually is.

When World-Wide-Web inventor Tim Berners-Lee was considering what the internet could be used for back in 1989, he was excited about its potential for collaboration and creating a huge digital information database. What he didn't imagine was how good the internet would be at spreading misinformation at lightning fast speeds.

Prior to the 2016 US election, the lines between news and entertainment were already blurry thanks to so many outlets putting a premium on attention-grabbing headlines and viral content. What was already understood was that the best viral content had certain properties: it tapped into feelings like awe, anxiety and outrage.

By 2016, many people saw sites like Facebook and Twitter as their primary source of news, so the stage was set for the massive propaganda campaign that mostly originated in Russia.

Between June 2015 and August 2017, Russian agents posted around 80,000 posts on Facebook alone, reaching an estimated 126 million Americans. From December 2015 onward, many originated from an organization in St Petersburg called the Internet Research Agency. People on one floor of the agency would write fake blog posts, often pro-Trump or anti-Clinton, while those on another floor would set up fake American accounts to comment on and share the posts.

How successful were they? In the three months before the election, the most popular fake news articles had more readers than the top stories from mainstream outlets such as the _New York Times_, the _Washington Post_ and NBC News.

Aside from the pro-Trump articles, fake online groups were also created to increase tensions and divides in the United States. One called "South United" featured Confederate flags and promoted a new Southern uprising, while another was called "Blacktivist" and celebrated the Black Panthers.

There's a long global history of using misinformation against democracy, especially in Russia, and these new Russian propagandists aren't limiting themselves to the US. In recent years, they've interfered in at least 19 European elections as part of their efforts to destabilize both the EU and NATO.

### 6. Trump’s nihilistic, troll-like behavior, as well as his lies and twisting of language, have dangerous repercussions. 

Anonymous Russian internet trolls are one thing, but Kakutani finds it even more shocking that the president regularly uses his Twitter account to lie, bully, taunt and generally engage in trollish behavior. In fact, in 2013, one user on Twitter called Trump "the most superior troll," to which he replied, "A great compliment!"

One thing we know from Trump's own books is that he has a zero-sum worldview in which there must be a loser for there to be a winner, and the prime driving force is _nihilism_, which, in philosophical terms, is the denial of objective truth and an embrace of destructive negativity.

These words, from his book, _Think Big_, are a good example: "The world is a horrible place. The same burning greed that makes people loot, kill, and steal in emergencies like fires and floods, operates daily in normal everyday people . . . People will annihilate you just for the fun of it or to show off to their friends."

In his policies, Trump has been driven by negativity, which has repercussions that could affect millions. In particular, he has attacked every aspect of Obama's legacy, from health care to the environment, and he seems intent on rolling civil liberties back to the pre-1960s era.

But there have been other dangerous repercussions from the casual and constant lies Trump has told, and the lack of consequences for his behavior. For example, other politicians in the United States and leaders around the world are following his lead.

In their efforts to get a tax bill passed in late 2017, some Republicans, seemingly inspired by Trump's blatant lies, said the bill would help the budget deficit and benefit the middle class when it was obviously about giving a tax cut to the rich.

Meanwhile, some in Congress appeared to be inspired by Trump's cynicism when they were oddly open about their true motivations in passing the bill. Representative Chris Collins admitted that it was his big money donors who wanted the bill passed and that he was ready to make them happy.

Elsewhere in the world, leaders appear to be inspired by Trump's attacks on the media, such as the way he trolled CNN, which he considers "fake news," by tweeting an image of something labeled "CNN" squashed under his shoe. As if taking a page from Trump's playbook, the leaders of Syria and Myanmar both shrugged off accusations of mass murder and human rights violations by calling them "fake news."

### 7. A new kind of nihilism is breaking out across the United States. 

Following the election of Donald Trump, there was a noticeable uptick in the sales of two books: George Orwell's _1984_ and Hannah Arendt's _The Origins of Totalitarianism_. But equally relevant is _Brave New World_, by Aldous Huxley, which imagines a population so sated by drugs and "undisguised trivialities" that they are no longer responsible citizens.

Back in 1985, Neil Postman, the author of the book, _Amusing Ourselves to Death_, already saw Huxley's vision reflected in an America that was so narcotized by mindless entertainment and bitterly disillusioned by politics in general that citizens were all too willing to disengage from politics. Thirty years later, is our new age of distractions and discontent playing a similar role in letting the president trample over the Constitution?

What's also disturbing is all the signs of a new brand of nihilism running rampant in the United States.

You can see this nihilism in the way the surviving victims of the Sandy Hook and Parkland shootings are continually being trolled online. You can see it in a business based in California called Disinfomedia, which operates a number of fake news sites, one of which ran a phony story with the headline "FBI Agent Suspected in Hillary Email Links Found Dead in Apparent Murder Suicide." Founder Jestin Coler says the company's efforts to fool liberals into sharing fake news stories haven't gone as well as the stories they've targeted at Trump supporters.

A new kind of nihilism can also be seen in Richard Spencer, a white supremacist who has led alt-right crowds in cheers of "Hail Trump! Hail our people!" When questioned about giving Nazi salutes to these cheers, Spencer shrugged the accusations off, claiming he was obviously being ironic.

But even if we concede that Spencer, as well as many users on Reddit or 4chan comment boards, _are_ being ironic in their racist or fascist remarks, this can still lead to very real hate. Researchers Alice Marwick and Rebecca Lewis conducted a study about online media manipulation in which they concluded that after two or three months of using racial slurs "ironically," an online troll "may be more receptive to serious white supremacist claims."

Even Trump's aides have told people that they shouldn't take every word the president says seriously.

What's become of our world? The founders of the United States, including George Washington and Thomas Jefferson, urged citizens to remain diligent in upholding the principles of truth, liberty and reason, lest they put democracy at risk. It's time we all started heeding these warnings.

### 8. Final summary 

The key message in these blinks:

**While it may seem like we've entered an unprecedented era of tumultuous tensions and political chaos, history shows that our current predicament is firmly rooted in earlier events. In fact, as Kakutani puts it, the aftermath of the 2016 US election shouldn't be all that surprising given that many writers have predicted what happens when leaders continually lie, twist language to their own purposes and distort facts and verifiable truths. As many authors, scholars and historians have pointed out over the decades, the actions of Donald Trump place the basic tenets of democracy under attack.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Death of Expertise_** **by Tom Nichols**

_The Death of Expertise_ (2017) examines the current attacks on science and knowledge that seem to be on the rise in our current technological and political environment. What has happened to objective truths being the truth and facts being indisputable? Why is science now a matter of political partisanship? Find out what's really going on and why this is one of the most important issues of our day.
---

### Michiko Kakutani

Michiko Kakutani received her bachelor's degree in English from Yale University in 1976, after which she became a reporter for _Time_ magazine and the _Washington Post_. In 1983, she became a book critic for the _New York Times_ and held that job until her retirement in 2017. _Vanity Fair_ once called her "the most powerful book critic in the English-speaking world."

