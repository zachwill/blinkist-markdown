---
id: 553509fc6638630007280100
slug: the-demon-haunted-world-en
published_date: 2015-04-24T00:00:00.000+00:00
author: Carl Sagan
title: The Demon-Haunted World
subtitle: Science as a Candle in the Dark
main_color: B47528
text_color: 996322
---

# The Demon-Haunted World

_Science as a Candle in the Dark_

**Carl Sagan**

_The Demon-Haunted World_ (1995) helps the reader distinguish between dangerous pseudoscience and real, hard science by exploring the critical-thinking tools scientists use to make their discoveries. The author argues for science's place in education and popular culture, and offers his advice on how we can incorporate more critical thought into our society.

---
### 1. What’s in it for me? Discover why science, not superstition, will lead us to a better future. 

Science has brought us modern medicine and a deep understanding of the universe, not to mention much of the technological gadgetry we rely on every day. In short, science and scientists are largely to thank for our health, happiness and prosperity.

And yet, in the modern world, science is under heavy attack from the proponents of superstition. From astrologers to religious fundamentalists, many people are beginning to question the merits of the scientific approach. They suggest it is too arrogant, too narrow, too boring.

These blinks, based on Carl Sagan's groundbreaking work, show you why such criticism is pure hogwash. They delineate why our development as a species depends on our giving up our beliefs in the paranormal and replacing them with an understanding of science.

In these blinks, you'll discover

  * why astrology is total bushwa;

  * why there never seems to be any witnesses to alien abductions; and

  * why scientists need to work on their PR.

### 2. Scientists rely on skepticism and critical thinking to understand how the universe works. 

When you look at the moon, what do you see? Throughout human history, millions of people believed they saw a face. Today, however, we're quite sure that the "man in the moon" isn't _really_ looking down on us. Rather, it's _we_ who are looking at nothing more than 4.5 billion-year-old craters.

But how do we know there's no man in the moon? Well, because scientists disproved that hypothesis by using powerful telescopes to carefully examine the moon's surface.

Science is generally open to all ideas — even that of there being a man in the moon! — but it tests them rigorously. Scientists look for explanations of how the world works, and try to consider every single idea that might explain any given phenomenon.

Then, through the process of critical questioning, careful observation and repeated experimentation, they try to discern the explanation that is most probable.

For example, if you were to explain your burnt finger to a scientist by saying it had been roasted by a dragon's fiery breath, you would have to demonstrate that there were no other more probable, more valid explanations for your burn.

Otherwise, your theory simply doesn't hold up to the scientific method, and will remain just another unsubstantiated claim.

The scientific method is fortified by a constant skepticism that jettisons bad ideas and singles out good ones. And because scientists know better than anyone that human thought is fallible, they constantly challenge one another's explanatory models.

It is this skepticism that helps them find and correct errors even in well-established scientific theories. Copernicus did exactly that when he flouted centuries of scientific thought by proposing a heliocentric, as opposed to a geocentric, astronomical model — that is, that the earth orbits the sun, not the other way around.

By maintaining a stolidly scientific skepticism, only the best explanations survive, which ensures a deeper understanding of the universe.

### 3. Many people are uninterested in science and don’t understand its methods. 

Here's a startling fact: 95 percent of Americans have trouble explaining how a scientific theory, such as the Big Bang theory, differs from statements like, "Ghosts exist." In other words, they are "scientifically illiterate."

Such illiterates don't really understand _what_ science is or _why_ it's different from religious and superstitious beliefs. Most importantly, they don't understand how the scientific method is able to differentiate between truth and myth.

Consequently, most people can't distinguish real, well-grounded scientific findings from the unsupported claims of pseudoscience. Perhaps this is why about a quarter of Americans believe in the oracular power of horoscopes, despite the dearth of evidence demonstrating that astrological predictions are anything more than fortuitously correct.

Scientists speculate that people identify with horoscopes because they are written in such general language. To prove this, a French scientist sent the same horoscope to hundreds of Parisians. (It had originally been written for a French serial killer.)

He then asked these Parisians if they thought the horoscope reflected their personality. Ninety-four percent of them said it did — even those whose astrological sign didn't match the killer's!

So why the popularity of pseudoscience and superstition? For one, they appeal to people's emotions, rather than to their rationality. There are many passionate believers in astrology, for example, who love the mystery and excitement of prophesying their love lives by reading tarot cards. By comparison, the hard facts of science are a bit of a letdown.

From high school onward, the prevailing belief is that science is "too hard" or "boring." Parents and teachers are often unfamiliar with the intricacies of the scientific method, and therefore do little to change the minds of their children and students. As a result, subjects like mathematics, biology, chemistry and physics are increasingly unpopular.

This lack of interest in science is a great threat, not only to individual enlightenment, but to the moral fiber of our society as a whole.

### 4. Pseudoscientific beliefs are increasingly popular, despite a lack of evidential support. 

In the Middle Ages, people believed that evil demons called _incubi_ visited and violated young women in their sleep, thus turning them into witches. It's easy to laugh at history's pseudoscientific blunders, but things aren't much different today. For instance, each year thousands of Americans report having been abducted, examined and sexually assaulted by aliens.

And yet, there has never been any hard evidence supporting any claim of alien abduction. No abductee has ever brought back an unusual alien instrument from the UFO, nor have any microchips ever been found implanted in the victims. Of those who claim to have had sex with aliens, none gave birth to any half-alien offspring.

Often, the "evidence" meant to irrefutably prove alien visitations could just as easily have had a human origin.

Consider the case of Doug Bower and Dave Chorley, two friends from Southampton who, in 1991, announced that they had secretly been making crop circles all over England for 15 years. Many people had taken these signs, which the pair had pressed into fields of wheat or corn under the cover of darkness, to be definitive proof of extraterrestrial visits.

Furthermore, many reports of alien abductions can be explained more plausibly (and scientifically) as hallucinations.

Indeed, any "normal" person can experience unusual visions on occasion, caused by things like sensory deprivation, high fever, a lack of sleep or changes in brain chemistry, to name only a few causes.

Interestingly, these "abductions" usually take place right before the "abductee" falls asleep or wakes up, when they're likely to be in a dream-like state.

Moreover, people can even _share_ their hallucinations. This was the case for husband and wife Barney and Betty Hill, who insisted that they were both regularly abducted and examined by aliens in their sleep.

Yet, when doctors looked more closely at this strange case, they eventually discovered that the scenarios they described closely resembled the 1953 movie, _Invaders from Mars_.

### 5. Scientists are stereotyped as nerdy, arrogant and sometimes even dangerous. 

Think about the cartoons you used to watch as a kid or even the ones your own kids enjoy watching today. Do any of these shows feature scientists? If so, are they loveable and curious, enjoyable people overall? Probably not! More likely than not, they're portrayed as exceedingly evil, either hopelessly nerdy or dangerously insane.

Very often, the media misrepresents science and scientists, focusing mainly on the negative aspects of scientific inquiry. They propagate stereotypes such as the "mad scientist" — think Dr. Frankenstein or Dr. Jekyll — and portray the pursuit of science as an undesirable, even dangerous occupation.

The media also disseminates the idea that scientists are narrow-minded and arrogant people who categorically dismiss reports of paranormal activity without affording them due consideration.

But, in fact, scientists are open to _all_ ideas; they just approach every idea with skepticism, because skepticism is a crucial part of the scientific method, the part that's made possible our deep understanding of the world.

With that in mind, it's also important to know that public distrust of science isn't totally baseless. Science, like all other human enterprises, is morally ambiguous. It's findings, therefore, can just as easily be used for evil as for good.

Take Edward Teller, for example, who developed the hydrogen bomb. For decades, he was chief adviser on nuclear weapons to the US government, and yet he purposely downplayed the dangers of nuclear weapons simply so he could continue his research.

So it's imperative that the scientific community address the public's fear that science will be misused. The best way to do that is by supporting high ethical standards for the research and development of potentially dangerous technologies.

In part, this means not being secretive with their findings, and instead sharing them openly with the public. This way, the potential danger of misuse can be addressed early on.

With a solid understanding of what science is and what its goals are, our following blinks will demonstrate exactly _why_ science is so important.

### 6. Science guides humanity’s social and technological development. 

Imagine your life without real science. You'd live in a frightening world, dominated by religion and superstition. There'd be no cars, no TVs, no toasters — and certainly no computers or tablets on which you could read this! 

To be sure, scientific innovations have been put to unspeakably evil ends. But if physics, biology, chemistry and other scientific approaches are applied carefully and with ethical intentions, the benefits of the technologies they give rise to will far outweigh the potential hazards.

These benefits are, generally speaking, threefold: they steer us away from danger, improve our material conditions and help us grapple with life's eternal questions.

Let's start with how science helps us avoid danger.

Even if a scientific discovery (like, for example, nuclear fission) gives rise to a potentially dangerous technology (such as a nuclear warhead) it's science that often enables us to see and prevent those dangers from getting out of hand. By illuminating a technology's underlying mechanisms, science can help us predict that technology's possible applications.

For example, scientists often blow the whistle on other scientists. Remember Edward Teller, the guy who imperiled the world by lying about the dangers of nuclear weapons? Well, another scientist alerted President Franklin Roosevelt to the very real possibility of a nuclear war between the United States. and the Soviet Union. That scientist was Albert Einstein.

Technological advancements, while sometimes threatening, can also boost production efficiency, improve the use of sustainable materials and energy, and draw in money from foreign investors. Those are exactly the effects industrialization had on the United States in the late eighteenth and early nineteenth centuries. 

And for developing countries, such increases in efficiency and investment often improve the population's quality of life.

Finally, science helps us answer some of life's big questions. Using scientific inquiry, we are able to learn more about our species, our planet and our universe, and thus gain insight into the role we humans play in the context of the cosmos.

> _"Science can be the golden road out of poverty and backwardness for emerging nations."_

### 7. The values of science are similar to, and help sustain, the values of democracy. 

When people think of Thomas Jefferson, they often remember his role in the formation of the United States. But he was also a passionate scientist! He collected books and scientific instruments, and firmly believed in the power of education. So it should come as no surprise that it was Jefferson who inserted many of the revolutionary ideas to be found in the Constitution and Bill of Rights.

Indeed, the concepts of freedom of speech, skepticism, pragmatism and objectivity are central to both scientific inquiry and a functioning democracy.

Like science, democracy depends on the diversity and free exchange of ideas. This means that everyone's opinion both counts and is subject to critical scrutiny, by the voting public on the one hand and by the scientific community on the other.

For example, in most democracies, the State's executive, legislative and judicial powers are separated. This way, each branch can investigate the mistakes of the other branches. No one is above the law, and each branch must justify its actions to the people.

Similarly, scientists must constantly defend their work against the thoughtful criticisms of their skeptical peers.

This system of error-correction allows for constant improvement, both in democracy and in science.

In a democracy, political leaders who fail to live up to their promises find themselves out of a job when reelection time rolls around. In essence, their political gambits are experiments the outcomes of which they can only hypothesize.

Similarly, science depends on subjecting theories to experiments. And like the washed-up politician, the flawed theory must be discarded.

In light of the close parallels between scientific inquiry and a functioning democracy, it makes sense that tyrants, who have a great interest in oppressing the free exchange of opinions in order to maintain their monopoly on power, often have little to no interest in scientific research.

> _"Freedom is a prerequisite for continuing the delicate experiment of science."_

### 8. Science needn’t be incompatible with religious belief, faith in humanity or a sense of wonder. 

People often complain that science is utterly unromantic, devoid of all dignifying mystery, a constraint on the human imagination and spirit. Religion and pseudoscience, with their miracles and paranormal activity, seem at first glance more accommodative of people's emotional needs than any scientific theory ever could be.

But in reality, the world of science is just as passion-filled and awe-inspiring as the world of the spirit.

Science is full of phenomena that, even after being explained to the best of our scientific ability, still sound like magic.

For example, whenever you think you're touching something, you're not really touching it at all. What's really happening is the electrical charges of your hand are influencing the electrical charges of the object you're "touching" and vice versa. No matter how it feels, there is always an infinitesimally small gap between your fingers and every object you "touch."

It gets weirder: at certain speeds, time can flow backward. And our bodies? Just like all other organisms on earth, they're made up of billion-year-old stardust.

More than any other human enterprise, science appreciates the magnificence of our universe. It celebrates our sense of wonder and our amazing intellectual achievements.

Because of this, science can actually be a deep source of spirituality — a beacon in the darkness of superstition, ignorance and narrow-mindedness.

Science, therefore, doesn't stand in opposition to religion. Rather, the two go hand in hand.

The main appeal of many religions is that they imbue our short lives with a sense of meaning. But as we've seen in the examples above, the way science explains the miracle of existence is equally magnificent, and doesn't necessarily rule out the existence of God.

While historically treated as polar opposites, science and religion can be understood as two expressions of one very human concern: the desire to understand our existence.

### 9. Employing the tools of science, we can learn to critically test the quality of arguments and theories. 

The benefits of a skeptical mind hit much closer to home than lofty ideas like democracy and finding the meaning of life. In fact, the art of critical thinking can help you in any area of your life — and you don't have to _be_ a scientist to _think_ like one!

Fundamental to this process of critical thinking is the ability to form a good hypothesis. It must be precisely formulated, and in a way that it can be falsified by observation or experimentation.

Let's compare this strict standard with claims by pseudoscience.

Imagine that, after reading the tea leaves, a fortune-teller informs you that "Tomorrow you will have an eventful day." Is this a good hypothesis?

No, because we can't objectively measure the "eventfulness" of a day. If the fortune-teller had instead told you tomorrow's lottery numbers, then the accuracy of their prediction could easily be tested.

Moreover, the facts used to support a hypothesis must be confirmed independently. It's not enough, as is nearly always the case with "alien abduction," for a strange event to be confirmed by a lone source, no matter how strong their conviction. To have real scientific weight, facts must be checked continually by many people.

Furthermore, when we're developing our hypothesis based on facts, we must be careful to consider _all_ alternative explanations to the phenomena. Every link in our chain of argumentation must withstand scrutiny; there can be no half-truths or inconsistencies.

Finally, we have to be careful not to mistake correlation for causation. For example, imagine that you're presented with a survey that shows that there are more homosexuals among college graduates than among the less well educated. There's a clear correlation between being gay and graduating from college. But that doesn't mean that getting a college education results in homosexuality.

Use these critical thinking tools whenever you're confronted with a strange argument. That will make it easier to separate valid ideas from pure nonsense.

### 10. We must provide better science education and encourage kids to think critically. 

One of the most important steps toward the abolition of slavery in the US was the uptick in literacy among African-American slaves. For them, literacy meant greater access to knowledge, an increased understanding of the laws used to justify their mistreatment and an ability to politically organize. Indeed, in many cases, knowledge is power.

Education and critical thinking offer an escape from poverty and oppression. With better education, oppressed peoples can better understand their surroundings and critically question existing hierarchies of race, gender, wealth and so on.

In our modern world, the basis for this kind of critical thinking _should_ be laid in school. Unfortunately, however, the quality of science education, and the quality of grade school in general, has suffered a sharp decline in recent decades.

We see this in American high school students' poor rankings in cross-cultural science tests. On one algebra test, for example, Japanese 17-year-olds managed to answer 78 percent of the questions correctly; American students of the same age only managed 43 percent.

This is especially tragic considering that it's in our very nature to question our surroundings and experiment. In our evolutionary past, curiosity and inventiveness were likely to be rewarded. Hunter-gatherer groups that experimented with medicinal herbs and thereby learned to treat disease, for example, were more likely to thrive and procreate than less creative groups.

It's the job of parents and teachers to encourage this natural curiosity in children by inviting them to ask questions and conduct their own experiments.

This will require educators to stop teaching by rote. Rather, they should explain _how_ we've come to know these facts. They should explore questions like: How do we know that the earth revolves around the sun and not the other way around?

Of course, education continues well beyond school, and critical thinking is beneficial during every stage of our lives. It's therefore important that adults continue to think critically, too.

> Fact: "63% of American adults are unaware that the last dinosaur died before the first human arose."

### 11. Government and mass media should support science and the public’s understanding of science. 

So far, you've seen how important science and critical thinking are to us, both personally and as societies, as well as how science and scientists are tragically mischaracterized in today's media. To correct these misrepresentations, it's not enough to improve science education in school. Government and media organizations must also do their part to help science thrive.

For starters, governments should do more to fund purely scientific research projects.

Consider, for example, that the US Department of Defense spends over 300 billion dollars per year on weapons and military projects. Meanwhile, research funding for the pure pursuit of knowledge dwindles a little more every year.

And yet, some of the best discoveries — the ones that, for example, led to inventions like the microwave and the radio — happened purely by accident during the course of extensive research where scientists had no specific goal in mind.

In order to justify more research grants from the government, scientists must learn to publicly advertise their projects. By explaining what they want to do and why it could be beneficial for humanity, they can garner the public support that will help them to secure research funds.

In addition, media should produce more programs that deal with real science.

Instead of programs that celebrate pseudoscience, mystery and superstition (think _X-Files_, for example), we should be exposed to shows that popularize _real_ science. These shows should help drive out the pervasive and damaging scientist stereotype — that pale, socially awkward egghead — as well as train people to think on their own.

That was the author's goal when he launched, in 1980, the widely popular television show, _Cosmos_. In it he explains different kinds of scientific research to an audience of non-scientists in order to expose them to science and scientific thinking.

If we continue to deprive the public of an opportunity to engage in scientific thinking, we will eventually have to reckon with countless lost opportunities.

### 12. Final summary 

The key message in this book:

**Our best shot at understanding and getting the most from the world we live in is through real science and critical thinking. The constant, critical skepticism that characterizes scientific thought doesn't have to be cold, nor does have to limit our sense of spiritual wonder.**

**Suggested** **further** **reading:** ** _Bad Science_** **by Ben Goldacre**

We often swallow scientific-sounding language used in advertisements or on the news without any further thought. But if we analyze it a little, we often find that it's merely pseudoscience. _Bad_ _Science_ shows us that this bogus science can lead to serious misunderstandings, injustice and even death.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Carl Sagan

Carl Sagan was an American scientist who mainly specialized in astronomy and the search for extraterrestrial life. He also wrote a number of popular science books, including _Broca's Brain_ and _Pale Blue Dot._ Sagan achieved worldwide recognition as an advocate for science education with his television series, _Cosmos_.

