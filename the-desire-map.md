---
id: 571a7f9e8fe3320003ed448f
slug: the-desire-map-en
published_date: 2016-04-25T00:00:00.000+00:00
author: Danielle LaPorte
title: The Desire Map
subtitle: A Guide to Creating Goals with Soul
main_color: 5D4893
text_color: 5D4893
---

# The Desire Map

_A Guide to Creating Goals with Soul_

**Danielle LaPorte**

_The Desire Map_ (2014) is your guide to harnessing the positive power of desire. From fostering creativity to providing motivation for you to chase your goals, desire with a healthy dose of self-awareness can help you on your way to fulfillment.

---
### 1. What’s in it for me? Meet your desires and let them guide you. 

Sometimes, desires can be painful and lead to destructive outcomes. After all, if we weren't haunted by desires, there'd be no bitter wars and no lovesickness. 

But at the same time, they are vitally important for our existence. It's not just that your life would be bland without them — a life without desires would be close to impossible, the human race would simply stop reproducing! 

That's one of the things you'll learn from these blinks. What's more, you'll glean some deep insights into the character of your desires. You'll discover what to watch out for if you want to recognize your deepest desires and learn how to let them guide you to a more fulfilling and authentic life. 

These blinks will explain

  * why it's a good thing to hanker after that adorable girl or boy in your science class — even if you never get to kiss them;

  * about the special way feelings can be true even though they're not facts;

  * why you'd better think twice before you dismiss a big-nosed guy (or woman).

### 2. Desire fuels our lives. 

Rarely do we think of _desire_ as something more than a sexual impulse. But desire doesn't just drive our animal instincts! So what does it do? Let's investigate what desire really is, and what we use it for. 

Desire is the power of _wanting_. It's an engine of creativity and a purposeful force. When we desire something, we're willing to plot and plan to get it. Desire is something we act upon, and it's something for which we make sacrifices. It can make us angry, it can thrill us, or it can fill us with despair. In all cases, desiring something forces us to get creative in order to work out _how_ we're going to get what we want. 

Can you remember a high school crush you had? If there was someone you sat behind in science whom you really wanted to kiss, perhaps you thought up all sorts of creative ways to make them notice — dropping hints, writing notes, and so on. 

Desire is also a force that pushes your evolution as a person. Imagine if every time you met someone new, they gave you a written list of all their desires. You'd certainly learn a lot about them very fast! Our desires reflect things we face in our current realities and reveal our dreams for the future. Often, fears get in the way of our desires for the future. By tackling our fears, we'll also tackle our dissatisfaction with life as we become brave enough to achieve all the things we desire. This is precisely what helps us grow as individuals. 

Finally, desire is the spark that keeps our will to live burning. Could you imagine a life where you stopped wanting things altogether? Without the desire for food, love, sex, companionship or achievement we'd have nothing to get us out of bed in the morning. In other words, our desire _fuels_ us.

> _"You've got to be hungry. You've gotta want it."_

### 3. Follow your feelings to navigate those tricky life decisions. 

We're often advised to make decisions based on facts, not feelings. Why? Because facts are considered a measure of the truth, while feelings seem to mislead us. Unlike facts, feelings aren't true to everyone. But your subjective feelings are "true" too! They're simply true to you, and nobody else. 

If you've ever called someone annoying, you haven't stated a fact. But you have shared some sort of truth — that the person makes you _feel_ annoyed. 

What about more powerful feelings, like sadness? A messy breakup might give us feelings that seem a little too real, which definitely makes it hard to concentrate on our work for a while. Intense feelings can be distracting enough to make us unproductive, which is why most businesses don't tell their employees to "follow their heart." In this way, being told to focus often equates to _ignoring our feelings_. 

But no matter what people tell you, remember that your feelings are absolutely crucial. We all have _core desired feelings._ These aren't just passing emotions such as annoyance or anticipation. Whether they're a constant desire for a community where you belong, freedom to be creative, or companionship with someone you love, core desired feelings stick with you for life. These feelings are the signposts we need to guide ourselves.

Imagine being stuck doing work that's well paid, but incredibly boring. If the first thing you feel upon waking is dread about going to work, then this is a clear sign that your life needs something new. Of course, rational, logical you would never think of giving up your income. But following your feelings, not money, is what will make you feel fulfilled.

> _"Feelings are magnetic. Each feeling is a beacon that attracts a reality."_

### 4. Free your desires from the pressure of others, and welcome the unexpected! 

Has all this talk about core desires got you thinking? Well, good! Working out what your desires are isn't just enlightening — it's liberating too. Many of us waste our time chasing what we _think_ are our desires. But unfortunately, we're simply chasing things that others desire for us. 

If you're striving to get married before you hit a certain age, or are pushing yourself to earn a certain amount of money this year, have a think about whether you're subject to pressure from others. What are your _real_ desires? By figuring this out, you'll be able to discern when you're simply trying to prove yourself to your peers, and free yourself from this behavior. 

With this awareness firmly in place, you'll finally be able to start making better choices. Knowing your real desires will help you chase the opportunities that really matter to you. Say you desire to feel loved, or make a contribution to the lives of others. In this case, it might not be that six-figure salary that'll help you get there, but a volunteer job in a refugee shelter will. 

Sometimes, a long hard think about your desires isn't enough to reach complete awareness. Chance encounters also play a vital role in helping you discover what you really want out of life — so, if something unexpected comes your way, don't ignore it!

You might, for instance, be desiring true romantic connection, with a little adventurousness in the mix. Then you run into a guy who makes you feel just that. But the thing is, he's not in great shape, doesn't have a fancy degree and has quite a large nose. Ready to run for the hills? Hold it! We've got to be open to all the different ways our desires manifest. Treasure every moment when objects of your desire fall into your life by chance, and you'll reach new levels of awareness, while gaining way more new experiences.

> _"Knowing how you actually want to feel is the most potent form of clarity that you can have."_

### 5. Goals that you dread aren’t worth chasing. 

Ever chased a goal and achieved it, but still felt empty? These are the goals that aren't in accordance with your true desires. So, drop them! 

The author used to set strict, quantifiable goals for her life, whether it was a figure she wanted to earn that year or a date she wanted to publish a book by. But these hard numbers only succeeded in stressing her out. Goal-setting can be daunting and it can make us nervous, but it should always make us feel excited and happy too. 

A goal that you dread chasing isn't a goal worth achieving. So, create goals that seem achievable to you, and capture your core values and desires. The author came up with new goals to raise awareness for her book among audiences she cared about, and to make her publications as beautiful and insightful as possible. 

When goals become inspirational like this, you'll have a better time chasing them.

But what about when you reach one of these goals, and still feel hungry for more? This is natural. Your creativity has been sparked, and your mind is already looking for further opportunities to grow and achieve. 

Perhaps you now feel capable of things you never thought you could do before you achieved that first goal. Publishing your first poem, for example, might have made you eager to enter a short story into a competition. Getting a runner-up position in that competition might, in turn, make you eager to start running your own publication. Our desires grow and grow in this way, so keep setting goals to match them.

But remember to ensure that these goals also feel completely _right_. Fulfilling goals might feel great, but they shouldn't mean you have to sacrifice other things that matter to you, such as relationships you care about or a lifestyle you love. There's also no need to push other people out of the way. In fact, you'll have an even better experience if you help other people to follow their desires while chasing your own goals.

> _"Desire hurts so good. It is an ache that we will never escape . . . if we want to be fully alive."_

### 6. Make peace with your weaknesses and rise above them. 

Ever dreamt of becoming an acclaimed writer or a successful designer, but promptly nipped those plans in the bud? Chances are that a voice in your head told you that those aspirations were ridiculous. This is exactly the voice that you _must_ ignore.

Make peace with the goals you want to achieve. Whether they're fantastical or humble, if your goals make you feel good, then don't criticize yourself for having them. By the same measure, you should also be careful not to make fun of the goals of your friends and family. Each of us should feel free to do what we desire, whether it's opening our own restaurant or rescuing injured animals.

But what if your desires are at odds with each other? This happens quite often. Someone who loves alone time and dreams of living in a log cabin in peaceful woodland might also be the mother of two noisy children with a jam-packed family life. Sound conflicting?

Well, it is. In these cases, a choice must be made. But this can be a blessing in disguise. By opting out of one long-held desire, you make room for new desires and new goals, which can allow you to find happiness in places you'd never expect. 

We might also find obstacles in our own limitations. Nobody is invincible, after all. You might have a limited budget, family responsibilities to uphold or health issues to manage. Whatever they are, these limitations aren't a reason to give up on your dreams. On the other hand, you shouldn't chase your goals while pretending these limitations don't exist. Simply admit them and learn to work with them. This will allow you to live your dreams _and_ do so sustainably, perhaps even discovering new ways to overcome problems and grow.

### 7. Final summary 

**The key message in this book:**

Desire fuels our existence, fosters our creativity and helps us grow. Free yourself from goals forced upon you by others and make peace with your own weaknesses. Keep your heart and mind open for opportunities that surprise you and you'll make the most of desire's positive power. If you want some help creating your desire map, the book itself features useful exercises and diagrams.

**Actionable advice: Focus on what you already trust.**

Know that nervous rush you get before an important event? Whether you're facing a test, a decisive match or a daunting speech, this rush can disadvantage you if you don't learn to work with it. Silence your inner pessimist by concentrating on what you know for certain. A list of things that are already working out great for you is enough to comfort you, calm your nerves and boost your confidence. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Fire Starter Sessions_** **by Danielle LaPorte**

_The_ _Fire_ _Starter_ _Sessions_ is a spiritual guide to how you can achieve success on your own terms. It will show you how to become successful — without sacrificing your true self — through practical exercises that will set your full creative potential free.
---

### Danielle LaPorte

Danielle LaPorte is a writer and entrepreneur who decided to skip school and instead learn everything by herself. She is deeply engaged with the concept of success and has authored _The Fire Starter Sessions_.

