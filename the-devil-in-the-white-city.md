---
id: 583d6bfce536360004b64eef
slug: the-devil-in-the-white-city-en
published_date: 2016-12-02T00:00:00.000+00:00
author: Erik Larson
title: The Devil in the White City
subtitle: Murder, Magic and Madness at the Fair that Changed America
main_color: 729AC7
text_color: 496380
---

# The Devil in the White City

_Murder, Magic and Madness at the Fair that Changed America_

**Erik Larson**

_The Devil in the White City_ (2003) takes you to Chicago in the 1890s, when the growing city was the host of the World's Fair amid a time of social upheaval and serious crime. These blinks blend a story of exciting American innovation with the unspeakable acts of one of the world's first serial killers.

---
### 1. What’s in it for me? Travel back in time to turn-of-the-century Chicago. 

The third-largest city in the United States, Chicago has been the subject of many songs, its people and nightlife immortalized in movies and books. How did this initially sleepy midwest city rise to such fame?

In the nineteenth century, the Windy City played host to the World's Columbian Exposition, a huge fair that attracted people from all over the world. The fair thrust the city into the limelight, and its aftereffects lingered for generations, from its skyline to demographics and even its language.

Yet the fair was not all fun and games. The event also inspired some of the most devilish sides of humanity, revealed here in these blinks.

In these blinks, you'll learn

  * why the wind has nothing to do with Chicago's nickname, the Windy City;

  * which famous brands launched at the fair still exist today;

  * how Chicago became the home of the first-known serial killer.

### 2. At the close of the nineteenth century, the streets of Chicago were rife with vice and violence. 

Are you ready for a thrilling story of national pride, high society and murder? Before we dive in, let's take a look at life in Chicago circa 1890.

As the nineteenth century drew to a close, Chicago, an American city on the shores of Lake Michigan in Illinois, was a city rife with vice and violence.

Death along the city's many railroad crossings were commonplace; two people on average were crushed under the wheels of trains daily. Chicago residents weren't fazed, and some even had the task of collecting severed heads and limbs found along the tracks.

Fire was another common killer, with wooden shanty homes often going up in flames, killing dozens daily. The city's water supply was also deadly, as sewage that spilled into the Chicago river was teeming with unhealthy bacteria. Cholera, typhus and other potentially fatal diseases were just a fact of life.

Those in poor neighborhoods lived with trash-lined streets and suffered from infestations of rats and flies. The corpses of animals were also a common sight; no matter if a dog, cat or horse, the city had no organized groups to collect them. These dead animals would freeze under the ice of winter only to warm, bloat and rupture in the sweltering heat of summer.

Murder rates in Chicago were some of the highest in North America. The city's police didn't have the manpower or even the training to manage this violent city. In the first half of 1892, some 800 gruesome deaths were counted in Chicago — that's about four deaths every day.

Amid all this chaos, Chicago was also witness to serious social change. Women were entering the workforce in droves, allowing many young, single women to build new lives for themselves in Chicago.

Chicago's working women worked as seamstresses, weavers, typists and stenographers, among many other jobs. As urban reformer Jane Addams wrote, there had never been a time in the history of civilization in which so many young girls could travel and live so freely.

Indeed, Chicago's industry was booming, with the city's meatpacking district the largest in the nation. Demand for housing was high; the growth in real estate construction saw modern skyscrapers rapidly shaping the Chicago skyline.

> Sociologist Max Weber likened Chicago to a "human being with his skin removed."

### 3. In 1890, Chicago won the votes of Congress to host the World’s Columbian Exposition. 

At the turn of the century, the United States was eager to establish itself as a world leader in culture, yet it clearly lagged many of its European rivals. France, with its internationally chic capital of Paris, was the standard many countries looked to emulate.

In 1889, France hosted the Exposition Universelle, a colossal, glamorous world's fair. While not common today, a world's fair is a gigantic public exhibition that takes the form of a mini-city.

Countries of the world are invited to take part, hosting exhibits and putting modern innovations on display. Gustave Eiffel, for example, erected his now-famous tower of iron and steel as the centerpiece of the Exposition Universelle.

America had a section at the fair, but it wasn't too impressive, and many felt the country's jumble of booths an embarrassment. Officials afterward were determined to repair the country's reputation by hosting a world's fair that would outdo France.

And so the World's Columbian Exposition was born, its official purpose being a celebration of the 400th anniversary of Columbus discovering America.

American cities competed for the chance to host the fair, and New York, Washington and Chicago were the favorites. Chicago, however, prevailed.

This might seem surprising, given the corrupt, violent nature of Chicago in the 1890s. Despite everything, Chicago residents were boastfully proud of their city — the true reason why it was nicknamed the Windy City in the first place!

Chicago's big talk seemed to do the trick. On February 14, 1890, Congress named Chicago the host of the World's Columbian Exposition.

While residents rejoiced, officials knew the project wasn't going to be an easy one. Chicago was now under pressure to build what was essentially an entire city within the city — and they needed the right person to lead this massive project.

### 4. Daniel Burnham headed the fair’s construction, which from the start was plagued with problems. 

Chicago chose architect Daniel Burnham to head the fair's construction. This decision was a no-brainer, as Burnham was a charming, successful and wealthy professional — people told the story of Burnham shipping a barrel of fine Madeira wine around the world twice to age it properly!

The handsome architect had the charisma of a born leader, but this didn't make the project any easier.

From the outset, Burnham had to deal with a host of complications. The US economy was struggling, and the budget for the fair was tight.

As thousands of unemployed men made the trip to Chicago to find work at the fair, Burnham at the same time was laying off workers because of budget constraints.

Those who did work on the construction of the fair's many buildings toiled amid dangerous conditions, and many workers lost their lives in the process. Many critics said the project acted as a mirror of the negative aspects of modern industrialization and the class conflict it created.

Burnham, against all the odds, was able to open the fair on time in 1893. The temporary yet grand buildings of the fair, with inspiring neoclassical facades, were painted a brilliant white — giving it the name The White City. Chicago was ready to welcome the world.

> _"Call it no more the Windy City on the lake; it is Dreamland."_ — Daniel Burnham

### 5. The fair was a runaway success, with new inventions and star-studded attendees. 

The fair was a huge success, running for six months and receiving some 27.5 million visitors, with upwards of 700,000 people showing up on the fair's biggest day.

A spectacle of glamor and technological innovation, the World's Columbian Exposition awed visitors, with some even weeping at its inspiring beauty!

The fairgrounds covered just under one square mile, with models of over 200 buildings. One exhibition hall was large enough to house large replicas of the US Capitol Building, the Great Pyramid of Giza, Madison Square Garden, Winchester Cathedral and St. Paul's Cathedral!

The fair also showcased new inventions and innovative products, such as the first clothing zipper, the first all-electric kitchen with an automatic dishwasher, Juicy Fruit gum, caramel popcorn, shredded wheat cereal, Pabst Blue Ribbon beer and even a 22,000-pound wheel of cheese.

George Washington Gale Ferris Jr. presented his Ferris Wheel, an engineering achievement that was considered at the time to be equivalent if not surpassing in genius France's Eiffel Tower.

Inventors Thomas Edison and Nikola Tesla, as well as author Theodore Dreiser, were honored guests at the fair. American journalist Richard Harding Davis called the exposition "the greatest event in the country since the Civil War."

On the fair's dedication day, American schoolchildren were brought together to recite a certain pledge for the first time, one which nearly all Americans know by heart today: the Pledge of Allegiance.

But behind the glare of the White City, all was not well. Chicago was still a city of violence and crime, and no fancy world's fair could change that.

> _"Chicago has disappointed her enemies and astonished the world."_ — New York editor Charles T. Root.

### 6. The fair did nothing to curb the brutal violence of Chicago, as Henry Howard Holmes was to prove. 

What happens when you pour thousands of visitors into a dangerous city? In addition to the world's first zipper and Ferris Wheel, Chicago played host to the world's first-known serial killer.

Henry Howard Holmes appeared a charismatic, handsome physician to people who met him. In reality, he was a conman and psychopath who lured young women to their deaths, later cashing in on their life insurance.

Holmes and his associate Benjamin Pitezel ran a hotel for guests attending the world's fair. The hotel was popular thanks to its convenient location and charming innkeeper.

What guests didn't know was that the hotel had been specifically designed as a death trap.

Holmes installed gas pipes in rooms so he could silently asphyxiate his guests. Others suffocated in his soundproof basement vault or were hanged in a custom-made "hanging room."

Holmes then dissected the corpses, stripping them of skin and organs. He sold the skeletons to hospitals and universities.

Nearly a decade later, Holmes was finally apprehended. Police after entering his apartment found a vat of acid that contained eight ribs and part of a skull. They also discovered a large kiln for burning bodies and a dissection table, in addition to a collection of bones, such as 18 ribs from a child's torso.

All in all, police believed Holmes killed between 20 and 200 people.

In 1895, Holmes was tried for murder, found guilty and sentenced to death, the case a media sensation around the world. In 1896, to the relief of Chicago residents, Holmes was hanged.

> _"I was born with the devil in me. I could not help that I was born a murderer, no more than the poet can help the inspiration to sing."_ \- Henry Howard Holmes

### 7. Final summary 

The key message in this book:

**At the end of the nineteenth century, Chicago was a booming industrial center, rife with social change and rampant violence. As the United States made its international cultural debut at the 1893 World's Fair, one man's obsession with murder introduced the world to its first-known serial killer.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Poisoner's Handbook_** **by Deborah Blum**

_The Poisoner's Handbook_ details the work of New York City's first scientifically trained medical examiner, Charles Norris, and his partner, Alexander Gettler, the city's first toxicologist. It offers an insider's view of how forensic science really works by walking readers through their investigations into notorious and mysterious poisonings.
---

### Erik Larson

Erik Larson has written for the _Wall Street Journal_, _Time_ and other publications. He's also the author of multiple books, including _In the Garden of Beasts, Dead Wake, Thunderstruck_ and _Isaac's Storm_.

