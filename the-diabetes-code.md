---
id: 5b73d82db238e100073cda1d
slug: the-diabetes-code-en
published_date: 2018-08-16T00:00:00.000+00:00
author: Jason Fung
title: The Diabetes Code
subtitle: Prevent and Reverse Type 2 Diabetes Naturally
main_color: 288EC9
text_color: 1F6C99
---

# The Diabetes Code

_Prevent and Reverse Type 2 Diabetes Naturally_

**Jason Fung**

_The Diabetes Code_ (2018) addresses one of the western world's most alarming health epidemics: the rise of type 2 diabetes — a disorder closely related to poor diet and obesity. But as Jason Fung shows, it can be reversed. The important thing is to figure out what works and what doesn't. Drawing on his professional experience as well as the latest scientific data, Fung makes a powerful case for a dietary strategy to prevent and reverse type 2 diabetes that you can start implementing today.

---
### 1. What’s in it for me? A practical plan for tackling type 2 diabetes. 

Type 2 diabetes is a modern epidemic. Nearly 400 million people have the disorder around the world, with 28 million of them in the US alone — and it's on the rise.

The cause? Carb- and sugar-heavy diets and a sedentary lifestyle our bodies just can't keep up with. That, in turn, leads to insulin resistance and all sorts of serious health issues.

Unfortunately, the standard treatment for type 1 diabetes just doesn't work for type 2 diabetes. In fact, there's plenty of evidence that insulin shots actually _increase_ the risk of cardiovascular diseases, strokes and heart attacks if you have type 2 diabetes.

But there's a solution. As Jason Fung shows in _The Diabetes Code_, dietary changes and intermittent fasting can help put type 2 diabetes patients on the road to health, while sparing themselves expensive and invasive surgery.

In the following blinks, you'll learn

  * why drugs might not be the answer to type 2 diabetes;

  * about the risks of weight loss surgery; and

  * how to prevent and reverse type 2 diabetes by changing your diet.

### 2. Diabetes is related to high blood sugar, but different forms of the disorder have differing roots. 

Diabetes — and type 2 diabetes in particular — is on the rise. The figures make for a worrying read: one hospital in New York recently reported that it was treating ten times as many diabetes patients in 2000 than it had been in 1990 and that the majority of new cases were type 2.

Diabetes is a disorder related to high blood sugar and consists of four basic types. These include types 1 and 2, as well as gestational diabetes caused by high blood sugar levels during pregnancy. Other types of the disorder are related to genetic problems and a dysfunctional pancreas.

Its symptoms include thirst and needing to urinate frequently. Both reflect the fact that the kidney isn't processing blood sugar or, to give it its proper name, _glucose,_ in the patient's body. The body responds by trying to eliminate excess glucose through frequent urination.

Let's take a look at the differences between type 1 and type 2 diabetes.

The former is an _autoimmune disease_. That means that the body's immune system is attacking insulin-producing cells. Insulin is the hormone responsible for regulating blood sugar levels.

When the body stops creating enough insulin, blood sugar levels spike. That's why type 1 diabetes patients need insulin shots to live safely.

Type 2 diabetes is different. It's typically the result of a poor, sugar-rich diet. The body responds by producing large amounts of insulin in an attempt to regulate all the ingested sugar.

Eventually, the body's cells become insulin-resistant. That means they no longer react to insulin because there's just too much of it the body.

As a result, Insulin shots aren't particularly well suited to treating type 2 diabetes. The problem, after all, isn't a lack of insulin — as it is with type 1 diabetes — but _too much_ insulin.

> _"Fundamentally, type 1 and type 2 diabetes are polar opposites, one characterized by very low insulin levels and the other by very high ones."_

### 3. Like obesity, type 2 diabetes can’t be treated by simply limiting calorie intake. 

The way we describe the world changes all the time. We look for new words to describe new phenomena. Just think of "bromance" — a popular term used to describe close, non-sexual relationships between two men.

But have you ever heard of _diabesity_? It's another recent coinage and is a useful way of highlighting a novel trend in western society: the epidemic of people suffering from a combination of obesity and type 2 diabetes.

The close link between obesity and type 2 diabetes was conclusively demonstrated back in 1990 by Walter Willett, a nutritional expert at Harvard University.

His research showed that post-puberty weight gain is the most significant factor in increasing the risk of type 2 diabetes. Putting on 44-77 pounds of weight, for example, means that we're an astounding _11,300 percent_ more likely to develop the disorder.

Willett and his colleagues carried out follow-up studies in 1995. These showed that even small weight gains dramatically increased the chance of type 2 diabetes. An extra 10 to 20 pounds increases the chance of type 2 diabetes by 90 percent.

Unfortunately, it took a while before these breakthroughs became widely accepted in the general medical community. But today there's no room for doubt: type 2 diabetes is intimately connected to weight gain and obesity.

That said, neither obesity nor type 2 diabetes can be cured by simply consuming fewer calories. When we consume fewer calories, our bodies reduce their metabolic rates — the amount of energy needed to keep our hearts pumping and brains functioning.

Although this was the solution first proposed by doctors and nutritionists, a sharp reduction in daily calorie intake just doesn't work.

Ultimately, our sense of hunger and calorie intake depends on our hormones. More specifically, it's our insulin levels that are responsible. That means losing weight is all about reducing our insulin levels.

Reducing overall food consumption doesn't help achieve that. What we really need to do is avoid specific _types of food_.

> _"Obesity is a hormonal imbalance, not a caloric one. The hormonal problem in undesired weight gain is mainly excessive insulin."_

### 4. Insulin resistance is caused by fat deposits in the liver, and it can develop very rapidly. 

Most people know that excessive alcohol consumption is hard on the liver. But alcohol isn't the only culprit when it comes to liver damage.

Let's start with _glycogen_ : it's a substance which stores carbohydrates in our bodies. Too much of it creates fatty deposits in the liver, which over time leads to insulin resistance.

Insulin resistance is the first step on the road to type 2 diabetes. It's the result of excessive carbohydrate and protein consumption.

Unlike other dietary fats which can be stored throughout the body, glucose from protein and carbs is transported straight to the liver. Once it arrives there, it's converted into a reserve of glycogen which can be used when blood sugar levels sink.

But once this reserve is fully stocked, the body starts converting new glycogens into fat, which is then exported to other parts of the body.

The problems start when the liver can't keep pace with our protein and carb intake. When that happens, the fat is no longer exported but stays in the liver. And as the liver becomes fattier, it stops accepting further glucose.

When our blood sugar levels rise, insulin is released. That, in turn, encourages the liver to accept more glucose. Once the liver starts struggling to process that glucose, even more insulin is released to fix the problem.

This creates a vicious cycle. The more insulin there is in our bodies, the less the liver reacts to it. That's what doctors mean when they talk about insulin resistance.

And it doesn't take much to develop a fatty, insulin-resistant liver.

Take a study carried out in 2008 by neurosurgeon Suzanne De La Monte: she found that people can develop insulin resistance in as little as three weeks just by eating 1000 calories of sugary snacks a day.

During those three unhealthy weeks, the participant's body weight only increased by two percent, but the amount of fat stored in their liver increased by a massive 27 percent!

Luckily, this fatty liver effect can be reversed by returning to a normal diet with fewer carbs and less fructose.

### 5. Rising fructose consumption is a contributor to widespread fatty liver disease. 

In 2009, the endocrinologist Robert Lustig uploaded a video to YouTube that quickly went viral. In it, he confirmed what many had long suspected: that sugar is toxic for the body.

One type of sugar is particularly bad for our health. It's called _fructose_ and is a major contributor to the development of type 2 diabetes.

Like glucose, fructose is unhealthy and practically devoid of nutritional value, especially in its refined forms. But it also has an added sting in its tail: the liver can't break it down.

Around 80 percent of glucose is metabolized outside the liver, meaning the organ only has to deal with a fifth of all ingested glucose. Fructose, on the other hand, goes straight to the liver where it can quickly lead to fatty liver disease and, eventually, diabetes.

That's because the liver simply can't metabolize large quantities of fructose on top the all the glucose it's already getting from proteins and carbs.

The bad news is that fructose now plays a larger role in our diets than ever before.

And that's the problem: fructose isn't inherently bad for us — what really damages our health is consuming _too much_ of it.

In the nineteenth century, people ate around 15-20 grams of fructose a day, mostly in the form of fresh fruits containing relatively small amounts of the sugar.

But people's diets started changing after the Second World War. This was largely a result of increased sugar cane and sugar beet production. By the 1970s, daily fructose consumption per person had increased to 37 grams.

The most devastating development, however, was the emergence of fructose-rich corn syrup. It was a cheap source of the sugar, and soon enough was being added to all sorts of processed foods. Eventually, it could be found in everything from sauces to ready meals, breads and sweets.

By 2000, per capita fructose consumption in the US had climbed to 78 grams a day. There's no doubt that this is part of the problem.

The author's own research shows that countries in which corn syrup is popular have 20 percent more cases of diabetes than countries in which less fructose is consumed.

### 6. Insulin shots aren’t the answer to type 2 diabetes, because too much insulin is bad for the body. 

There's no question that the ability to produce insulin in laboratories and treat type 1 diabetes was a major medical breakthrough. But insulin shots aren't a silver bullet in the fight against type 2 diabetes.

Type 2 diabetes and obesity aren't just fitness issues. Left untreated, they can lead to cardiovascular diseases, including heart attacks.

Insulin shots aren't particularly effective in that context. While they can help type 2 diabetes patients regulate their blood sugar levels in the short term, long-term use can actually damage their health. In some cases, they can even cause death at a younger age.

That was shown by G.L. Duff and G.C. MacMillan back in 1949. Their studies on animals revealed that high insulin levels can lead to atherosclerosis — a hardening of the arteries linked to heart attacks and strokes.

Modern research also highlights the fact that insulin shots aren't suited to the treatment of type 2 diabetes. They regularly show that lowering blood sugar levels actually _increases_ the risk of heart disease.

Consider the American National Institute of Health's massive ACCORD study from 1999: the scientists behind it investigated whether insulin treatment could reduce cardiovascular fatalities among type 2 diabetes patients.

One group of patients received normal doses of insulin as well as heart medication. The other group received higher doses of both insulin and heart medication. The aim was to bring the second group's blood sugar levels down quicker.

The study was a spectacular failure. The patients receiving higher doses of insulin and medication died 22 percent more quickly than those who'd been given normal dosages. In the end, the whole study had to be called off.

The Canadian scientist J.M. Gamble carried out another study in 2010. He found that type 2 diabetes patients receiving insulin treatments were 279 percent more likely to develop coronary disease than other patients.

### 7. Bariatric surgery can be an effective cure for type 2 diabetes, but it’s not the best solution overall. 

Obesity is a serious problem. Take one of the author's patients: Adrian weighs 208 kilograms and lost his job because of his disastrous health issues.

Many patients in a similar situation consider resorting to a drastic solution: elective weight loss surgery.

Doctors call it _bariatric surgery_, and it involves removing a large portion of the stomach.

It's an effective cure for type 2 diabetes, and in most cases, the disorder simply disappears after patients have the operation.

So how does it work?

Well, the operation dramatically reduces the number of calories that can be ingested. That gives the liver a chance to use up its glycogen reserves and burn up the fatty deposits which had made it insulin resistant.

Just how effective the surgery can be was shown by P.R. Schauer and his colleagues in a study carried out at the Cleveland Clinic in 2012.

Type 2 diabetes patients who underwent bariatric surgery were in much better health than their counterparts who received insulin treatments. After three months, the former patients could be taken off their diabetes medication entirely — they were cured!

That applies to 95 percent of type 2 diabetes patients who have the operation. Additional benefits of bariatric surgery include long-term weight loss as well as lower blood pressure for more than 70 percent of all patients.

So _that_ must be the cure all we were looking for, right?

Well, no. The problem is the procedure is incredibly expensive, highly invasive and can lead to all sorts of complications further down the road. These include internal bleeding, infection and reduced nutrient absorption.

But there is a silver lining. The good news is that the positive effects of bariatric surgery can be achieved by much simpler means. In the next blink, we'll take a look at them.

### 8. You can prevent and reverse type 2 diabetes by avoiding fructose and refined carbohydrates. 

In 2015, a hospital in Texas was confronted with a three-year-old type 2 diabetes patient — the youngest diabetes patient on record.

That's a pretty good indicator that the disorder is out of control and that knowing how to prevent and reverse type 2 diabetes is more important than ever before.

So, how can you reverse and prevent it? There are two highly effective strategies you can adopt today.

The first is to avoid fructose.

The obvious place to start is banishing sugars from your kitchen and dining table. That goes for sucrose — a sugar composed of one part glucose and one part fructose — as well as high-fructose corn syrup.

Cutting fructose from your diet means being wary of the products in which it often hides. These include sweet drinks like cocktails, smoothies and flavored waters.

Candy, cakes and pastries are also obviously off-limits. But remember, bread and pasta also often contain added sugar. Your safest bet is to check the ingredients list and leave anything containing sugar on the supermarket shelf.

Caution is also advisable when it comes to sauces, condiments and even meats. Sugar is an easy way to make all kinds of food tastier. That's something vendors and producers know, which is why they add it to their products.

Eating out, especially if it's something you do often, can also be a minefield. Don't be shy about asking your waiter about the fructose content of various dishes before placing your order.

The second strategy you can adopt to prevent and reverse type 2 diabetes is to avoid refined carbohydrates.

Refined carbs belong to the worst food group of all because of their role in sending your insulin levels through the roof. So steer clear of refined wheat-based products like bread, pasta, corn-based tortillas, popcorn, fries, chips and white rice.

That doesn't mean you have to give up on old favorites. Not all carbohydrates are unhealthy — switch to unrefined carbs like brown rice and whole-wheat pasta and you can still enjoy some of your favorite dishes.

These alternatives don't stimulate insulin production nearly as much as their refined counterparts and can be part of a healthy diet.

Turfing out refined carbohydrates means there's a vacancy in your nutritional plan. Fill it with nourishing fatty foods like high-quality oils, fish, avocados and nuts.

### 9. Intermittent fasting is a much more effective cure for type 2 diabetes than daily portion control. 

Fasting has long been a known cure for diabetes. Elliott Joslin, an early American diabetes specialist, advocated it as a treatment more than a century ago in 1916.

However, a lot has changed in the medical profession since then. Today, the focus has increasingly shifted to using drugs to treat diabetes. But it's time to rediscover more traditional cures.

So what exactly does fasting involve?

One possibility is daily portion control, but that's probably not the best answer. Curing diabetes and encouraging weight loss aren't easy things to achieve.

Consider a British study carried out in 2015: it analyzed the effectiveness of normal nutritional counseling that focused on portion control and concluded that this approach failed for 99.5 percent of all participants. They just didn't lose very much weight.

The reason it doesn't work is that lowering daily calorie intake merely slows down your metabolic rate while increasing your sensation of hunger. That's tough to endure and sooner or later most dieters cave in and are quickly back to their original weight.

A much bettser approach is _intermittent fasting_.

That's basically about abstaining from all foods for a set amount of time, and could be anything from a day to a week. After that, people can return to their normal diets.

The effort required to follow this kind of plan is much more concentrated, which makes it easier to implement than the daily grind of portion control.

And, most importantly, it works! Fasting causes a dip in the body's insulin production, meaning it stays insulin-sensitive rather than developing a resistance to the hormone.

Another British study conducted in 2011 by N.M. Harvie underscores the effectiveness of this approach. Harvie compared two groups of dieters. The first ate a Mediterranean diet with a restricted calorie intake, while the second ate normally for five days a week and fasted for the other two.

Both groups experienced some weight loss after six months, but the second group also had much lower insulin levels than the first.

This suggests that intermittent fasting might be the best cure for type 2 diabetes. After all, it's high insulin levels and insulin resistance which are responsible for the disorder and fasting helps reduce these.

### 10. Final summary 

The key message in these blinks:

**Type 2 diabetes differs significantly from type 1 diabetes. Whereas the latter is characterized by low insulin levels, the former is a product of dangerously high insulin levels which, in turn, leads to insulin resistance and a range of serious health issues. The good news is that type 2 diabetes can be reversed. Combine carb avoidance and a change in diet with intermittent fasting and you'll be well on your way to recovery.**

Actionable advice

**Fasting is effective, but you need to find a regimen that works for you.**

Intermittent fasting is a great way of lowering your insulin levels, but it takes a bit of fine-tuning to get it right. If you're new to the idea, start by consulting a medical or nutritional expert. The next step is to find a regimen that suits your metabolism. Some people prefer to fast for longer periods less frequently, while others find more frequent but shorter fasts more effective. The key is experimenting. Try fasting for 3-4 days every two months, or simply skipping dinner and fasting for eighteen hours before breakfast. And remember to keep yourself well hydrated during whatever regimen you go with and stop if you feel sick!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkists.com with the title of the book as the subject line and share your thoughts!

**Suggested further reading:** ** _Super Immunity_** **by Joel Fuhrmann, MD**

_Super Immunity_ (2011) reveals the secret to a better, stronger immune system and healthier body: superfoods. These blinks shed light on the shortcomings of modern medicine and teach you how to take advantage of the healing powers of plant foods rich in nutrients and phytochemicals.
---

### Jason Fung

Jason Fung is a doctor and leading medical expert. He specializes in type 2 diabetes, nutrition and obesity and is a well-known advocate of fasting as a cure for the disorder. Fung has written numerous articles on health issues for newspapers and magazines including the _Atlantic_ and the _Daily Mail_. A regular guest on Fox News, he's also the author of the bestselling book _The Obesity Code_ (2016).

