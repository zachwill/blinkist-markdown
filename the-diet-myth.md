---
id: 55b628473464390007020000
slug: the-diet-myth-en
published_date: 2015-07-27T00:00:00.000+00:00
author: Tim Spector
title: The Diet Myth
subtitle: The Real Science Behind What We Eat
main_color: None
text_color: None
---

# The Diet Myth

_The Real Science Behind What We Eat_

**Tim Spector**

_The Diet Myth_ (2015) teaches you how to revolutionize your eating habits using the latest scientific research on _microbes_ , the tiny microorganisms that live in your gut and help you break down food. Most diets and healthy eating tips rarely work because they fail to take these microbes into account – but these blinks show you how to understand and work with own body, with great results.

---
### 1. What’s in it for me? Debunk the diet myths and learn to eat more healthily. 

Dieting is great for your health, right? Actually, dieting can be fine at its best and downright unhealthy at its worst. This is because diets don't take into account one of the most essential things impacting our health: the trillions of bacteria that inhabit our bodies and help us benefit from the nutrients in the foods we eat.

These blinks combine traditional wisdom and some of the latest research to improve our eating habits. The most popular diet myths will be debunked, and replaced with actual scientific knowledge.

You'll also learn

  * that eating with chopsticks can make you lose weight;

  * how your microbes can help you benefit from seaweed; and

  * why Sprite Zero is way worse for you than Sprite.

### 2. Microbes, the microscopic organisms that live in our bodies, are a critical part of our health. 

Did you know that your body is home to trillions of microorganisms? Luckily, most of them don't make you sick — and some are even immensely beneficial.

Microorganisms, or _microbes_, are the microscopic organisms living inside us. They're an essential part of our bodies, brains and immune systems.

We need microbes to help our bodies break down food and absorb its nutrients. We're actually born without them but our microbe-free state only lasts a few milliseconds. And it's no coincidence that microbes flock to the human body so quickly — we need each other to live.

Experiments with mice have illustrated how critical microbes are. If mice are birthed through sterile C-sections, kept in isolation away from all microbes and fed sterile food, they don't develop normal brains, guts or immune systems. If the mice are exposed to microbes after a few weeks, their development starts to improve but they'll never reach the same level as mice exposed to microbes from birth.

The species of microbes you have in your body also reveal a lot about how healthy you are. The author experimented by having his 22-year-old son eat only fast food for ten days.

After just ten days, his healthy microbes had decreased by 50 percent and he lost more than 40 percent of his microbes overall. His remaining microbes were mostly aggressive and inflammatory. Some new and rarer species, which are usually only found in people with deficient immune systems, also flourished.

After a week on a regular diet, his _microbiome_ — the full array of his microbes — had started to normalize and become healthier.

> _"Our microbiome is likely to be responsible for much of our obesity epidemic."_

### 3. No nutritional advice is universal because each person’s microbiome is so different. 

Although roughly 20 percent of the British population is on some form of a diet at any given time, the United Kingdom's weight gain epidemic rages on. The United Kingdom isn't an exception, either — it's the norm. Obesity is on the rise and diets don't seem to be helping much.

There are hundreds of diets, and all of them blame the wrong factors for weight gain or general unhealthiness. Veganism blames animal protein, the Atkins diet blames carbohydrates and the Paleo diet blames grains.

Everyone has access to nutritional information these days, so anyone can pretend to be an expert and misattribute our health problems to protein or grains. Unfortunately, there's no diet that works for everyone. In fact, our bodies are all so different that there isn't really _any_ nutritional advice that can be generalized.

The spread of the obesity epidemic is actually related to our microbes and trendy diets aren't a cure. A diet only works if you have the right microbiome for it!

About seven percent of the United Kingdom's population was obese in 1980. Today, that figure is 24 percent. And current research suggests that it's microbes — not genes — that are contributing to this problem. Microbes can change and adapt very quickly in a way that genes can't.

This would explain why one person can eat a certain meal on a regular basis without gaining weight, while another person who does the same _does_ gain weight. It's because they have different gut microbes that process their food differently. Some people digest carbohydrates more effectively, while others benefit more from protein, for instance.

So your microbiome determines whether a given diet will work for you or not. You can't just unthinkingly follow a diet that restricts certain categories of food and assume it will work for you.

### 4. Don’t blindly follow food labels – they’re often wrong and there’s a lot more to nutrition than just calories. 

A _calorie_ is defined as the amount of energy released when a standard unit of food is burned off. But calories don't tell you everything about healthy eating.

Whether you gain or lose weight depends on several factors — not just the amount of calories you consume. The energy your body extracts from food also depends on the food's source, how much you chew it and how easily you digest it, among other things.

For example, studies have shown that white rice eaten with chopsticks instead of a spoon reduces rising blood sugar — and lower blood sugar is positively correlated with losing weight.

Another study fed two groups of monkeys food with the same calorific content. The first group got 17 percent of their intake from vegetable oils, however, while the second group got theirs from artificial and unhealthy trans fats. The trans-fats group gained weight, put on more _visceral_ fat (belly fat) and had unhealthier insulin profiles.

So you can't rely on calorie counting alone if you want to slim down. Moreover, the estimates on food labels are often incorrect, misleading you when you _do_ try to count calories.

Food manufacturers are legally allowed to have error rates of up to 20 percent on their labels. And studies have shown that labels on natural foods like almonds sometimes overestimate the calorie amount by up to 30 percent. Frozen processed foods are _under_ estimated by up to 70 percent!

Food manufacturers can make these errors because the formulas for estimating calorie content are over a hundred years old. They disregard the food's age, the effects of different cooking methods and the variations in each person's digestive system.

> _"Relying on counting calories is often misleading."_

### 5. Our microbiome adapts to the food we eat, allowing us to extract nutrients from new kinds of food. 

The human race has survived for millions of years because of our ability to adapt to new environments. And today's research shows that it's not only us who adapts — our individual microbes adapt as well.

In fact, your microbes adjust when you incorporate new kinds of food into your diet, allowing your body to make use of new sources of healthy nutrients.

Seaweed is a great example of a food your body adapts to. Europeans usually lack the enzymes needed to fully extract the nutrients in seaweed during the digestive process, but Japanese people, who have been eating seaweed for centuries, are used to it. And when your gut is adapted to seaweed, it can break down and absorb all the anti-inflammatory, antioxidant and anti-cancer agents seaweed contains.

Further studies suggest that if you introduce enough seaweed into your diet, your microbes eventually adapt.

And microbes adapt much faster than you'd think! The author learned this when he conducted an experiment about cheese on himself.

He wanted to see if his microbes would change if he ate large amounts of cheese for three days. So he consumed 180 grams of three different kinds of cheeses — much more than the recommended daily amount of 30 grams per day.

After just three days, a test revealed that his microbes had already lost some of their diversity. A few days later, after he stopped eating so much cheese, the variety in his microbes had already started to increase. Our microbes can change very quickly when the circumstances call for it.

### 6. Not all fat is bad for you – some is very healthy. 

Fat is the unhealthiest part of food, right? Most people assume that because of an "anti-fat" movement that started in the 1970s. The movement blamed fat for nearly all of society's major diseases, including obesity. But fat is much more complicated than that.

The main proponent of the anti-fat movement was an epidemiologist named Ancel Keys. In 1970, Keys published a study arguing that a high fat intake correlated with higher risks of heart disease. It had a major impact on the press, the public, the medical world and even politics, as many began calling for policies to reduce the fat in food.

Keys's study was flawed, however. He originally surveyed 22 countries, but cherry-picked his data, leaving out the contradictory information from countries like Chile, Mexico and France. His ideas were spurious and unfounded, but that didn't stop their spread.

Thanks to Keys' misinformation, we're often told that high fat foods are unhealthy. That's untrue — they can be healthy as long as they're natural and contain living microbes.

Foods high in fat are often over-processed and loaded with sugar or other artificial additives and sweeteners, which is what really makes them unhealthy. But some fats are quite good for you, particularly yoghurt and natural cheese — though not the cheese substitute you find on pizza! Yoghurt and cheese contain a number of microbes that are very beneficial to your body.

Olive oil, another healthy fat, is a major component of the Mediterranean diet, which is widely considered one of the healthiest diets there is. It's the only diet that's been proven to reduce the risk of heart disease.

Avoid trans fats, however. Certain fats have many positive aspects, but trans fats are created by industrial processes and they increase your risk of cancer and other diseases. Natural fats are the best — and trans fat is anything but natural.

### 7. Sugar and processed foods are bad for your microbes and your health. 

Many people want to reduce the fat in their diet, yet are surrounded by food products that contain a much more harmful substance: sugar. Nearly 80 percent of the products in your local supermarket contain added sugar, which is a fairly new development.

In recent decades, lots of highly unhealthy food items have become popular, such as sugary drinks and high-fructose corn syrup. These foods have addictive qualities, lead to tooth decay and alter our microbes in a negative way.

Your digestive system normally signals to your brain to stop eating when you're full, but a large soda doesn't trigger this process because your body doesn't have time to realize that it's getting so many calories — we drink much faster than we eat. We finish our soda without realizing how many calories we're pumping into our bodies.

Processed foods also tend to contain chemical additives and artificial sweeteners, and although they come in many forms, they're generally made up of just a handful of ingredients: corn, soy, wheat or meat.

People once thought it was healthier to replace sugar with artificial sweeteners. However, recent studies have proved that artificial sweeteners cause both weight gain and diabetes.

In fact, one study of students drinking either Sprite or Sprite Zero found that those who had the Sprite Zero actually consumed _more_ calories than those who drank regular Sprite.

Furthermore, most of those additives and sweeteners alter our hormones, reduce the variety in our microbiome and negatively impact our immune system, potentially leading us to develop allergies.

All in all, it's best to avoid consuming sugar and processed foods as much as you can. They have very little nutritional value and do far more harm than good.

> _"Too much sugar is not good for us, particularly if it comes in liquid or unnatural form."_

### 8. It’s healthy to have as much variety in your diet as possible. 

In recent decades, people have started to consume much more processed and fast food — and we're constantly hearing about new, trendy diets that have been developed. It might seem that we have more food options than ever, but the rise of processed foods has actually _decreased_ the variety of the foods we eat.

A lot of wellness plans advise people to reduce the variety in their diet, which isn't healthy. Reducing the variety of foods you consume is a big mistake, because it also reduces the variety of microorganisms in your microbiome and limits your sources of nutrients.

It's a good idea to limit your intake of sugar and processed foods, but limiting your intake of natural foods is counterproductive. The paleo diet, for instance, stipulates that you shouldn't eat tomatoes because they come from a family of harmful, toxic nightshade plants that humans supposedly haven't had time to adapt to.

This kind of pseudo science is misleading because it focuses on the effects of just one or two chemicals within the food, not the food as a whole. In fact, no real scientific study has ever found that tomatoes are harmful — on the contrary, they've been proven to reduce the risk of heart disease.

Avoiding absolutely all animal products also presents risks. Vegans have a much higher chance of developing Vitamin B12 deficiencies, which are harmful to your health.

Fifteen thousand years ago, our ancestors typically ingested about 150 different ingredients per week. Today, most people's diets only consist of about 20 separate food items and most of them are artificially refined or modified.

> _"Trying new foods is part of my new philosophy, as is eating as diverse a diet as I can."_

### 9. Fasting is healthy if you do it right. 

Fasting might seem like a new idea, but it's actually millions of years old. Hunter-gatherers inadvertently fasted during food shortages and all major religions incorporate some form of fasting into their practices. The Greeks, Romans and Persians would only eat one meal each day, in the evening.

Fasting provides of number of health benefits. It's been proven to trigger anti-aging and anti-inflammatory hormones and to increase the variety in your gut microbes. You can fast in many ways, but it generally means restricting your eating and drinking below 30 percent of your usual daily intake. Studies have shown that the microbes in your gut produce their own serotonin when you fast, which lifts your mood. It also strengthens your willpower, as you prove to yourself that you have the self-discipline to skip a meal.

Intermittent fasting, where you fast for shorter periods in a single day, is a kind of midway between complete fasting and eating regularly. Lab mice become healthier, leaner and more resistant when they are fed only in six to eight hour periods between stretches of fasting 18 hours long.

There's a common misconception, propagated mostly by cereal companies, that breakfast is the most important meal of the day. But recent research has actually shown that it's not harmful to skip breakfast and it can even help with weight loss.

Intermittent fasting can also promote healthy microbe growth, but only as long as your diet is highly diverse.

The author suggests that each person experiment with different kinds of fasting to see what's right for them. He's personally made a habit of skipping breakfast on busy days. You'll have to figure out what works best for your body.

### 10. Final summary 

The key message in this book:

**You can't see or feel your microbes, but they have a tremendous impact on your health. After all, they're the agents that extract the nutrients from your food. So adjust your eating habits to increase the diversity of your microbiome, which will help you get more value out of what you** ** _do_** **eat. Steer clear of sugar, eat as much natural food as possible and practice intermittent fasting now and then. Adapting your diet to your microbes is much more effective than following any nutrition fad.**

Actionable advice:

**Get your microbes checked.**

Check out the British Gut Project (britishgut.org), an organization launched by the author. It's similar to the American Gut Project (americangut.org) — it allows anyone to check their own microbes by sending a sample through a post.

**Suggested** **further** **reading:** ** _In Defense of Food_** **by Michael Pollan**

_In Defense of Food_ is a close examination of the rise of nutritionism in our culture, and a historical account of the industrialization of food. An expert in food ecology, author Michael Pollan takes a look at the way in which the food industry shifted our dietary focus from "food" to "nutrients," and thus narrowed the objective of eating to one of maintaining physical health — a goal it did not accomplish.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tim Spector

Tim Spector is a consulting physician and a professor of genetic epidemiology at King's College in London. He's written over 700 academic papers and in 2011 started the largest microbiome project in the United Kingdom.

