---
id: 54d88dcb383963000a010000
slug: the-discomfort-zone-en
published_date: 2015-02-09T00:00:00.000+00:00
author: Marcia Reynolds
title: The Discomfort Zone
subtitle: How Leaders Turn Difficult Conversations into Breakthroughs
main_color: 28BAC8
text_color: 0B7580
---

# The Discomfort Zone

_How Leaders Turn Difficult Conversations into Breakthroughs_

**Marcia Reynolds**

_The Discomfort Zone_ outlines a method for dealing with the tough, unpleasant conversations that all of us encounter from time to time. The key to successfully navigating them is truly listening to what other person has to say.

---
### 1. What’s in it for me? Learn how to create a safe conversational space to have those tough talks. 

We all avoid tough conversations. As a manager, you can't dodge them for long. But how do you have a constructive conversation that addresses sensitive issues without being seen as the bully?

That's where the discomfort zone comes in. Learning how to create a safe space for an employee or friend in a conversation gives you the freedom and flexibility to then address the main problem that needs solving, and makes everyone feel involved and valued in the process.

These blinks give you a step-by-step approach to having that conversation and getting the results you want every time.

In the following blinks, you'll discover

  * how listening involves not only your ears, but your head, heart and gut;

  * what a "safety bubble" is, and how it can help you earn trust; and

  * how if you let yourself DREAM you'll successfully change your partner's thinking.

### 2. If you want to change the conversation, you have to disrupt your partner’s thinking patterns. 

Imagine the captivated look on a young child's face when she sees snow for the first time. Mouth agape in astonishment, her first thoughts might be, "What _is_ this? Where did it come from?"

This response is called the _baby stare_ and it's not just for children — adults too experience the same feeling of awe.

There's a leadership technique based loosely on the baby stare which aims to inspire employees by disrupting entrenched thinking patterns. It's called the _discomfort zone_.

A _discomfort zone conversation_ is designed to encourage people to adopt a new perspective, leading them to imagine new solutions to old problems. It works by triggering negative emotions, ultimately forcing people to think more clearly.

It might seem counterintuitive that negative emotions can produce positive results. After all, we usually try to avoid discomfort! But negative emotions can actually help us by forcing us to confront reality and break down whatever biases exist in our thinking.

For instance, a colleague might feel angry about his performance at work. You try to comfort him and replace his negative outlook with a positive one, by telling him how good his work really is.

But this isn't actually the best approach, as it might give your colleague the wrong idea about acceptable work standards. It might also make him feel embarrassed, for getting emotional for no reason.

So instead of placating him, you need to encourage your colleague to face reality and confront his negative feelings. He needs to understand why he thinks his work is subpar, and why this is making him angry. This will allow him to see the situation from a different perspective — the only way he'll be able to solve the problem.

> _"You choose the discomfort zone when you want to assist others in thinking differently."_

### 3. To truly listen to another person, you need to use your head, your heart and your gut. 

How can you incorporate the _discomfort zone_ in your own life?

First, you need to truly listen to the people around you. And this isn't easy, as listening isn't just about using your brain. It also requires intuition.

Think about this way. When you listen to a friend tell a story, you tend to trust your _head brain_ (where you logically analyze what they're saying). That's a good start, but you should also pay attention to your _heart brain_ and your _gut brain_.

Your heart brain gives you a deeper sense of what the other person is feeling. In other words, it allows you to read between the lines. For instance, does your friend seem overwhelmed, even though he claims that he's not overextending himself?

Once you've heard what your heart has to say, it's time to listen to your gut, as it will tell you what's inspiring your friend's emotions.

For example, if his story keeps going around in circles, your gut brain might make you realize that he's scared of taking a big leap, or maybe trying to come up with something new.

And once you've gathered the information from your three brains, you should use it to create a discomfort zone conversation.

If you realize that your friend doesn't seem passionate about what he's saying, let him know! Confronting his own emotions directly might make him feel uncomfortable, but it will allow him to deal with what he's actually feeling and think about what's motivating him.

Eventually, this could lead him to understand what he truly wants and what it would take to achieve it.

### 4. To successfully have a difficult conversation, you need to create an atmosphere of trust. 

People won't open up to you if they don't trust you, which is why you need to approach discomfort zone conversations carefully.

This means that you shouldn't start a difficult conversation in the discomfort zone right away, as that will only make your conversational counterpart feel immediately put on the defensive.

To better understand this, imagine you are talking to an employee. If you start by telling him that you've noticed some problems with his work, it's totally normal that he might feel bullied, and therefore be less receptive to constructive criticism.

In other words, he won't be in the right state of mind to work with you to solve the problem.

To prevent this, you need to create a _safety bubble._ That is, try to establish an environment of trust, one which will allow your employee to willingly listen to your criticism and work with you to make a change.

So what can you do to create this kind of atmosphere and to let your employee know that you only want the very best for him?

Go into the conversation completely focused on the present moment. Relax your body and free yourself from prejudices, worries or thoughts. Arrange to meet in a quiet place where there won't be any distractions from the phone or email.

In this relaxed, open environment, you'll be able to focus all of your attention on the conversation, which will make you a better, more trustworthy listener and ultimately create a safety bubble.

Your employee will also naturally pick up on the fact that you've made him your sole concern, which will then make him more receptive to the discomfort zone conversation.

### 5. To start a difficult conversation, determine the goal and reflect why it hasn’t already been met. 

Now that we've learned the basics, let's break our discomfort zone technique down into five concrete steps, represented by the acronym, _DREAM_.

The first step is to _determine_ the goal of the conversation. This is crucial; without a clear goal in mind, you can't have a successful conversation. The way to discover this goal is to start by asking specific questions to get the other person thinking about what he truly needs to solve his problem.

A good question to start could be, "What will be easier for you to do once we've talked about this problem?"

The second step is to _reflect_ on the past to understand why the goal hasn't already been achieved.

This is important because, to get a sense of what a person is thinking and feeling about the problem, you need to know his perspective on relevant past events.

For instance, if a manager can't decide whether to let an employee go, you might not understand the problem at first. But in a discussion of the past, you'll get better insight into whatever emotions and reasoning are affecting his decision. Perhaps the employee in question performed well in the past, and that's why the manager is reluctant to let her go.

And once you've gotten your conversational partner to reflect, you can work with this new knowledge using a technique called _summarizing_.

Say something like, "So I understand that the reason you're thinking like that is because..." This sentence will not only establish that you're listening closely (promoting trust), but it will also allow your conversational partner to hear his own views again, and reflect on them.

For the following steps in DREAM, read on!

### 6. To help a person change their perspective, explore the blind spots in their thinking. 

The third step in DREAM has to do with breaking down barriers in the other person's thinking.

During this part of the conversation, you'll need to _explore the blind spots_ to change your partner's perspective. 

Now that the whole story is out in the open and you understand the other person's reasoning, try to determine what assumptions are guiding his thinking. The idea is to draw out the biases that prevent your partner from finding a solution to the problem at hand.

Imagine this scenario. An owner of a small company decides to sell. However, when the time came to make a deal, the owner kept rescheduling, finding new problems he "needed" to solve before he could sell the company.

In this case, you could ask: "What would make you feel better: selling the company or not selling it?"

If he responds by saying that the thought of keeping the company makes him feel better because he values his employees, you would then ask, "Why don't you just keep the company?"

He might respond by saying that he's afraid of what others would think if he suddenly changed his mind after months of preparation to sell.

Here's the valuable insight: the owner is motivated by a conflict he's not even fully aware of.

Armed with this information, you could carefully direct him (don't tell him flat out, as that may make him uncomfortable) to realize this conflict himself.

You could ask whether he'd rather avoid embarrassment, sell the company and lose his employees, _or_ admit he made the wrong decision, keep the company and be happy?

### 7. End the conversation by acknowledging what you’ve learned and creating a follow-up plan. 

Is the discomfort zone conversation over once the other person becomes aware of their biases and finds a solution for the problem at hand?

Well, no. There's still two more steps to go!

The fourth step is to direct your partner to _acknowledge_ what he's learned. Although it might seem like a good sign if your partner says, "Ah, I get it now," at the end of the conversation, that's vague. It doesn't actually indicate whether your partner truly understood the conversation's implications.

When a student says they understand a difficult math problem, the teacher wouldn't just take her at her word, right? Of course not! The student would be asked to demonstrate her understanding by completing the problem on her own.

So back to our conversation. The best way to make sure your partner still remembers what you discussed is to actually let him verbalize it. Prompt him by asking, "What would you say was the most important part of our conversation?"

And once your partner has satisfactorily acknowledged what he's learned, you can move on to the fifth and final step: _make sure to commit to a follow-up plan_ to ensure your results last.

This step should address the initial goal of the conversation. For instance, if your goal was to make a decision, make sure there's a date for putting the decision into action.

Though it's great to have a follow-up plan as soon as possible, your partner might need more time to think. In that case, it isn't necessary to develop a full-scale action plan.

However, you should still agree on some kind of commitment, even if that means scheduling a time to have another conversation.

### 8. Final summary 

The key message in this book:

**If you want someone to change their perspective, you have to create a safe space. This way, the other person will be comfortable exploring the negative emotions that prevent them from finding solutions to difficult problems.**

Actionable advice:

**Improve your listening by visualizing your three brains.**

Many people find it difficult to use their head brain, their heart brain and their gut brain all at the same time. That's why it's important to practice this kind of balanced listening. To do so, take a few moments to empty your mind. Breath in and out, visualize a glowing ball floating through your brain, to your heart and then into your abdomen.

**Suggested further reading:** ** _Crucial_** **_Conversations_** **by Kerry Patterson, Joseph Grenny, Ron McMillan and Al Switzler**

We've all been in situations where rational conversations get quickly out of hand, and _Crucial_ _Conversations_ investigates the root causes of this problem. You'll learn techniques to handle such conversations and shape them into becoming positive and solutions-oriented, while preventing your high-stakes conversations from turning into shouting matches.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Marcia Reynolds

Marcia Reynolds is a leadership coach and author, and holds a doctoral degree in organizational psychology.

