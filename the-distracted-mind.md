---
id: 5af945d7b238e1000773ca54
slug: the-distracted-mind-en
published_date: 2018-05-18T00:00:00.000+00:00
author: Adam Gazzaley and Larry D. Rosen
title: The Distracted Mind
subtitle: Ancient Brains in a High-Tech World
main_color: F3425E
text_color: CC374F
---

# The Distracted Mind

_Ancient Brains in a High-Tech World_

**Adam Gazzaley and Larry D. Rosen**

_The Distracted Mind_ (2016) explains the basic structures of the brain and questions how well it can function in a world stuffed full of high-tech gadgets and constant distractions. Drawing on the latest research in neuroscience, it offers practical solutions for how to resist all these distractions and regain focus.

---
### 1. What’s in it for me? Stay focused and keep your eyes on the prize. 

What's the first thing you do when you need to find the solution to a problem or find some information? Most likely, you reach for your smartphone. With a world of information constantly at the ready in your pocket, there's hardly any information you can't find within seconds. And do you even remember what it was like trying to keep up contact with all your friends before Facebook?

Let's make no mistake. There are definite pluses to living in an interconnected high-tech world. But there are downsides too. Our brains are the product of millions of years' worth of evolution. They have developed to operate in a certain way. Unfortunately, we're hard-wired to get distracted, and new technology is hardly helping.

These blinks explain just how the brain works and what strategies can be deployed so that you don't use your smartphone unless you have to. Don't worry; this isn't a guide to returning to the Stone Age! It's a pointer to enjoying life and getting the most out of it.

Along the way, you'll learn

  * how your brain responds when you see a terrifying lion;

  * how often people reach for their smartphones each day; and

  * what to do to change your brain for the better.

### 2. The brain’s limitations mean we easily succumb to interference and distraction. 

The human brain is unquestionably one of the wonders of the universe. With it, we can achieve incredible feats — from solving complicated math problems to learning languages and designing cars and jets.

In short, it's one of the most complex systems in the known universe. In fact, it's this complexity that helps us set goals and perform an incalculable number of tasks. From gossiping with friends to presenting the next big project for colleagues at work, our brain is equipped with decision-making, planning and evaluation abilities. More formally, these are known as _executive functions_.

After this planning function, another faculty of the brain is necessary to actually accomplish tasks. Specifically, we need _cognitive control_. These are cognitive abilities like _attention_, _goal management_ and _working_ _memory_.

If we didn't have cognitive control, there'd be no way to make conscious decisions that inform and have an impact upon our lives. Instead, we'd just respond unthinkingly and mechanically to the world around us.

Imagine you're wandering the aisles of your local supermarket because you need food and drinks for friends who are coming over that night. If you were to suddenly lose all cognitive control, your ability to pay attention to and remember that goal would simply vanish. Instead of tracking down beer and snacks, you'd be walking through the aisles without aim.

Unfortunately, nowadays, our cognitive control is under more stress and strain than ever before. Consequently, we are distracted from our goals even more.

This has a lot to do with complexity and the complicated nature of the brain in particular. More complex systems are more vulnerable to interference.

If we'd evolved so that we could cognitively control our minds despite constant interference, that would be great. But we haven't.

In fact, our cognitive control seems to have evolved less than our executive functions. In other words, we're collectively great at setting goals but far worse at seeing them through.

This explains why we all tend to forget tasks like calling on a friend's birthday or turning off the lights.

> _"... our impressive goal-setting abilities have resulted in the conditions necessary for goal interference to exist in the first place."_

### 3. No matter how hard we try, we’re primed by evolution to get distracted by certain stimuli. 

We might like to imagine that we're in full control of our behavior, but that's far from the case.

We can't help the way environmental stimuli cause us to respond automatically. These external stimuli are called _bottom-up influences._ Typically, they're characterized by _saliency_, which means they are immediately noticeable to us, like, for example, when you hear someone shouting your name. Additionally, these stimuli have _novelty_ — a dramatic and unexpected quality such as a car backfiring.

There's nothing we can do to stop our involuntary responses to bottom-up influences _._ And for good reason. They are the mark of the survival instinct we've inherited from our ancestors.

Thankfully, we are more than just passive creatures, primed only to respond to sudden stimuli for immediate survival.

In general, our actions are guided by the _perception-action cycle_ : we perceive the world in a certain way and act accordingly. We have also evolved the ability to pause in the moment and evaluate our perception of a given situation. That means we can decide upon the best response at the time. These internal decision-making mechanisms are called _top-down influences_.

Let's look at an example. Imagine a baby pinches your arm. You pull away quickly in pain, but you aren't going to hit back blindly in response, as you're aware you're in no danger. No action is taken because of the "pause" in the cycle when you evaluate and act upon possible responses.

Now that we've glanced into the brain's workings, let's try to understand why it's so difficult for us to achieve self-imposed goals.

### 4. Modern technology hampers performance, but we let that happen because we naturally strive for information. 

Although we can't hold buzzing smartphones or flashing TV screens entirely responsible for distracting us, it's nonetheless clear that modern technology _has_ put considerable strain on our cognitive control.

More precisely, modern technologies exploit our brain's intrinsic susceptibility to interference. They impede our performance and sidetrack us from our goals. You know the feeling: even though you're in an important meeting you just can't help glancing at your phone. Or maybe you totally zone out at the dinner table because the game is playing on the TV next door, and you can hear the crowd reaching fever pitch.

The obvious solution might be to keep clear of cafés, TVs and the internet when you've got an objective you want to achieve. But it's not that simple.

The truth is, we _let_ ourselves be distracted because these interferences are a basic facet of what it is to be human.

Our primitive primate brains were always on the lookout for food, as that's what we needed to survive. Nowadays, this instinct remains, but it expresses itself as a search for information as well as food.

Consequently, we get the same hit of satisfaction from activities like googling, Twitter browsing, or TV watching as we did when we were primates foraging for food.

Amazingly, even when this behavior hampers performance and interferes with our goals, we let it continue.

It's actually just part of being human and, interestingly, it's a drive that's linked to our internal reward systems. For instance, a 2009 study of macaque monkeys showed that when primates receive information, their dopamine systems are activated just as they are when food is found.

That may explain why we end up juggling smartphones, TVs and tablets. We crave that hit of dopamine.

### 5. Three technologies have changed societal behavioral patterns. 

We've seen that we have hardwired responses to information. But this means we have to recognize how the current high-tech world makes it all the more easy for us to get distracted.

Specifically, there are three technologies which deliberately accentuate interference and have significantly changed societal behavior. These are _the internet_, _smartphones_ and _social media_.

First off, the internet provides us with an endless supply of ready information at all times. It's also thanks to the internet that we have email, a form of free and instant communication.

Mobile computing has meant that the internet is now everywhere too. This is most visible in the omnipresence of smartphones. A single device that can browse the web, stream video and music, take pictures and share them can now be carried everywhere.

This probably explains why 860 million Europeans and 7 in 10 people in the United States have a smartphone. And these objects are put through their paces too. On average a smartphone is picked up 27 times a day. And that's just the average: some people pick them up as many as 150 times a day!

These three technologies make it extremely difficult to focus on one task at a time or to sustain focus over a long period.

We might like to tell ourselves that we're "multitasking," but in reality, we're just jumping between different tasks without giving sufficient attention to any.

A 2013 study conducted by one of the authors showed just this. Students could only keep their attention on one task for three to five minutes at a time before they directed their focus elsewhere.

This occurs in non-work environments too. The next time you're at a café, look at all the couples and friends who can't help playing with their smartphones instead of engaging with each other. That impulse is observable under test conditions too. A 2012 study showed that younger adults switch tasks, on average, 27 times every hour. Older adults do it a little less, but 17 times every hour is still quite high.

> _As many as 12 percent of all smartphone owners use their phone in the shower, while 9 percent use them while having sex._

### 6. Modern technology impacts our daily lives and affects our safety – and we let it. 

Brace yourself for this next tidbit. In 2004 in the United States, an estimated 559 people had to seek medical help after smashing into stationary objects while simultaneously texting and walking. Since then the number has surged. In 2010, the number had climbed to a whopping 1,500. And these were only the incidents serious enough to require admission to medical centers.

Why do people put themselves in constant danger because of this behavior? You would think that no phone is that important.

It turns out that there are four factors at play which keep our eyes fixed on these screens: _boredom, anxiety, accessibility_ and _lack of_ _metacognition_.

Boredom decreases when people switch between tasks and information sources. That's particularly true if you go from a work-related information stream to an "entertainment-related" one, like Facebook or YouTube.

Professor Leo Yeykelis proved this with his colleagues in a 2014 study at Stanford University. They equipped students with wrist sensors that used galvanic skin response (GSR) to monitor their arousal level while working on their computers at home. The researchers found that there was an observable increase in their arousal level just before they switched between tasks. It was most noticeable when they switched from work to entertainment-related tasks.

Anxiety levels also decrease when we change information feeds. We feel anxiety if we're unable to check our smartphones or social media. This has been termed _FOMO_ or "_fear of missing out_, _"_ and it can be observed in young adults if they go 15 minutes without looking at their smart devices. It's this self-derived anxiety which causes us to interrupt ourselves.

Access to technology is nowadays more or less a given in most parts of the world. It's like constantly having access to a never-ending box of chocolates — easy to overdo it. Smartphones, for example, are always connected to the internet, so email and the temptations of social media are just a finger tap away.

Finally, there's metacognition or being aware of what your brain is up to. If you're self-aware that you're switching tasks, you're less likely to become distracted. However, if you lack metacognition, then you'll find it pretty hard to resist looking at your smartphone.

All this shows that we just don't have the tools to confront the ever-expanding cornucopia of temptation that modern technology represents. We can't seem to stop ourselves getting distracted. So what are we going to do about it?

### 7. Physical and mental training changes your brain and improves cognitive control. 

There are two ways to improve your cognitive control. You can either alter your brain, or you can change your behavior. In this blink, we'll explore the former.

Don't worry: modifying your brain does not involve using surgical tools! The brain is _plastic_, which means it's able to constantly change its own structure, physiology and chemistry. Consequently, many scientists think the brain's cognitive control can be enhanced.

There are several ways to change your brain. The most effective method for both children and adults is also the simplest: physical exercise.

It's well known that exercise will improve physical and mental health, but it's also been shown to induce neural changes that boost cognitive control.

A 2009 study published in _Pediatrics_ showed that physically-fit children exhibit better cognitive control than those who are less fit. The children were connected to a virtual reality environment and placed on a treadmill. They were instructed to cross a virtual street within the environment either while chatting on the phone or listening to music. The results showed that the fitter children were less distracted.

Another way to modify the brain is through _cognitive exercises_, otherwise known as _brain training._

There are many types. Just like toning your body at the gym, if there's going to be a noticeable effect, you'll have to repeat the exercises regularly and adapt as you improve. As you get better, the exercises have to get harder.

In 1998, an experiment called the ACTIVE trial was conducted. The idea was to examine if attention — one of the brain's major cognitive-control abilities — would improve in older adults when compared to a control group if they underwent cognitive training. The results were astounding. Even ten years later in follow-up tests, the participants reported that they had fewer difficulties in their daily lives compared to the control group. Incredibly, they even caused 50 percent fewer traffic collisions.

### 8. A few hacks will reduce interference while driving. 

Cognitive-control improvements can also be achieved by modifying our behavior.

However, there's no need to do a full-scale "digital detox" and delete Facebook or Twitter, or even cut yourself off from the internet altogether unless you feel you're addicted.

Just a little less should do it. The trick is to depress interference by increasing metacognition while decreasing boredom, anxiety and access to technology. That's to say, the four factors we looked at earlier.

The most noticeable effects occur when we look at the impact of reducing interference for car drivers.

It's possible to increase _metacognition_ if you familiarize yourself with the well-documented research which demonstrates the danger of texting and driving simultaneously. This awareness will discourage you from doing it yourself.

For instance, a study that might help you increase your metacognition was published by the US National Safety Council. This found that using mobile phones while driving caused 23 percent of all car crashes nationwide. That also explains why most states have banned the practice.

You might begin by reducing _access_ to technology. Sure, you could lock your phone in the trunk, but there are also plenty of apps like DriveOFF and DriveMode that will block incoming texts, emails and phone calls while you drive.

If you start getting _bored_ while driving, there's no need to reach for your phone. Chat with your passengers, fire up an audiobook, podcast or some music.

According to a recent study by David Strayer, a professor at the University of Utah, each of these activities is far less distracting for drivers than talking on the phone.

Finally, you can lower your _anxiety_ by forewarning colleagues, friends and family of your regular commute time. That way they won't call you, and you'll have no reason to fear you're missing a phone call or a text. Alternatively, an app like Live2Txt will reply automatically to people trying to get hold of you and let them know you're driving and will get back to them shortly.

Now that we know how to deal with interference while driving, let's look at doing the same while socializing.

### 9. You too can resist interference from digital devices while socializing. 

If you ever pop your head into a social environment like a café or restaurant, then you're sure to see people fiddling with their phones when they're meant to be chatting with the people right in front of them.

Even in these social situations, interference seemingly can't be avoided.

However, with a little bit of effort, you can combat that. Once more, you have to target metacognition, boredom, anxiety and access to technology.

Let's start with metacognition. You should be aware that mobile devices ruin relationships.

A 2013 study at the University of Essex showed that the mere fact that a phone was in the room was enough for two people to feel less close and less empathetic during conversation. The phone didn't even have to be picked up or checked. Its presence reduced mutual trust and understanding.

Now, do you feel like you should put your phone away for a bit?

The most effective way to reduce accessibility to mobile devices is to simply put them down and not use them. Less drastically, you could make your bedroom or dining area "_technology-free zones._ "

It's oh-so-easy to reach for your phone when you're bored. This means you should reduce your boredom — try to actively make sure that everyone is engaged in the conversation you're having. That way, you won't miss out on anything interesting or titillating that gets brought up — even at family dinner time!

Finally, just as when you're commuting, telling people your schedule will reduce your anxiety that you might be missing out. If they know that you eat between 6 p.m. and 7 p.m., they'll just call you later or earlier. There's really nothing to fear.

There's no doubt that modern technology can help us in our lives. But too much can be detrimental. Use its power in moderation and, as the old saying goes, "nothing in excess!"

### 10. Final summary 

The key message in this book:

**The human brain is capable of some astonishing feats. But its evolutionary history has left it susceptible to distractions and interruptions. Such interference stops us reaching our goals as quickly and efficiently as we would like. Luckily, there are ways to train your brain and improve your cognitive control.**

Actionable advice:

**Meditate!**

It's not only physical and cognitive exercises that help you mitigate the brain's cognitive limitations. Meditation has been proven to improve cognitive control. You might want to consider making time for mediation on a regular basis. Don't know what to do? There are loads of easily accessible guides on the internet, but the first step might be to find a local class.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _You Are Not Your Brain_** **by Jeffrey M. Schwartz and Rebecca Gladding**

_You Are Not Your Brain_ explores our deceptive brain messages which program us to have harmful thoughts such as "I'm not good enough." And it tells us how we can change this detrimental wiring by challenging these brain messages and focusing our attention elsewhere. In doing so, we can rewire our brain to make it work _for_ us, not against us.
---

### Adam Gazzaley and Larry D. Rosen

Adam Gazzaley is an American professor and neuroscientist at the University of California, San Francisco. He is also the founder of Neuroscape, a neuroscience center devoted to the understanding and optimization of human brain functions.

Larry D. Rosen is an American researcher and professor emeritus of psychology at California State University. He is the author of _Me, MySpace, and I_ (2012) and _IDisorder_ (2007). He is also an in-demand keynote speaker and a recognized expert on the effect of technology on human lives and psychology.

