---
id: 53d8c9ad3930340007390100
slug: the-divide-en
published_date: 2014-07-29T00:00:00.000+00:00
author: Matt Taibbi
title: The Divide
subtitle: American Injustice in the Age of the Wealth Gap
main_color: B03D23
text_color: B03D23
---

# The Divide

_American Injustice in the Age of the Wealth Gap_

**Matt Taibbi**

_The_ _Divide_ looks at income inequality in the US, explaining how it impacts society and the justice system. Sadly, poverty is effectively criminnalized while the rich enjoy preferential treatment.

---
### 1. What’s in it for me? Learn why justice is not equal for all. 

Have you ever wondered what would happen if one day you lost everything and were reduced to desperate poverty? Would there be anyone to help you get back on your feet?

As these blinks will explain, you'd find yourself in deep trouble, and the government certainly would not be there to lend you a helping hand. You'll learn how the US justice system protects the rich and criminalizes the poor. The machinery of the current system will become shockingly clear: Your legal rights are determined by your income. Today, being poor is essentially considered a crime.

In these blinks, you'll find out:

  * why you shouldn't smoke a hand-rolled cigarette outside a New York subway station,

  * why an additional toothbrush could land you in prison, and

  * how some people turn illegal immigrants into a highly profitable business.

### 2. Even though the financial sector is responsible for many serious crimes, prosecution is extremely rare. 

If you thought that US jails would be full of bankers after the 2008 global economic crash, you'd have been wrong. Despite their obvious responsibility for ruining the economy, very few bankers were brought to justice by the government.

Perhaps this seems strange. It's even stranger when you consider the phenomenal cost of criminal activities in the financial sector, a.k.a. _white-collar_ _crime_.

Roughly $4 trillion — 40 percent of the global economy — has been lost as a result of the recent financial crisis, and while we can't know exactly how much of the deficit was a result of illegal activities, there's no question that fraud played a huge role, especially in the mortgage industry.

For years, many profiteering banks like Washington Mutual simply gave out as much credit as possible without ever ensuring that their customers could actually afford the mortgages they were signing up for. These banks disregarded any moral or legal issues, leading to an enormous build up of unsustainable debt and eventually economic devastation.

So why haven't these bankers been locked up? Because rather than prosecute anyone, the government has sought to control these crimes through expensive fines and agreements.

For example, in the 21 biggest mortgage-fraud cases of the crisis, record-breaking sums of $26 billion in fines have been shelled out by the government, but not a single banker involved was charged for his or her misconduct. Taking bankers to trial is not the government's favored disciplinary strategy. Instead, the government prefers "deferred prosecution agreements," where companies merely agree to future restrictions on their business activities, like not being allowed to gamble on the housing market anymore.

With no fear of prosecution, bankers and other financiers are left to take careless risks.

### 3. The US government is unwilling to prosecute banks for financial fraud. 

In the last blink, we learned that the government doesn't put much effort into punishing financial criminals, but why not? Wouldn't it be better to live in a society where serious crimes had serious consequences?

The US government is simply ill-equipped to bring crooked bankers to justice.

Government prosecutors are intimately linked with the world of finance. For example, US Attorney General Eric Holder and his assistant Lanny Breuer, both former white-collar defense lawyers, should've been in charge of prosecuting banks while they were in public office. But when Breuer stepped down from his position in 2013, he returned to his earlier private-sector job at a major law firm, where he defended financial companies. He had gone full circle in the _revolving_ _door_ between private companies and public office.

The prosecution of financiers is also hampered by the government's _don't_ _lose_ _doctrine._ What does this mean exactly?

After Obama became president, a large number of former defense attorneys from the lobbying industry were hired by the Department of Justice. As a result, these attorneys came to their new government positions with the economic rationale of always maximizing results while minimizing risk. Unfortunately, this logic causes prosecutors to settle outside court even when a charge might have succeeded in front of a jury. The norm among government prosecutors is: Don't go to trial if there's _any_ possibility of losing.

A final factor hampering the prosecution of banks is that the government has also put into practice a _collateral_ _damage_ _doctrine_. This ideology maintains that when reviewing a charge against a company, a prosecutor needs to consider the larger consequences a sentence might have on the economy and job development. Because of this, many prosecutors in the Department of Justice often see banks and other financial companies as simply _too_ _big_ _to_ _jail._

> _"The attorney general of the US has just admitted, in a room full of reporters, that he asks Wall Street for advice before he prosecutes Wall Street."_

### 4. It's very difficult to punish top executives for their crimes. 

When it comes to white-collar crime, convictions aren't straightforward to obtain. You might have plenty of evidence or even a confession but that doesn't always lead to a sentence.

One explanation for this is that people who accuse banks are mistrusted by the courtroom. Simply put, it's an attitude problem: Unlike a murder trial, in the case of financial crime the judge and jury are often sensitive to the defendant, whereas those who have lost everything due to a bank's negligence are viewed as spiteful and greedy. Incredibly, it's the victims who are often seen as careless risk takers. They should have checked the small print, apparently.

It's also difficult to maneuver around huge institutions and companies to bring a single individual to trial.

This is partially because of differing laws between states and nations. For example, while the Racketeer Influenced and Corrupt Organization Act (RICO) was created to hold executives accountable for company wrongdoings, it is difficult to implement because it doesn't apply in all US states.

So if a German employee working for a British subsidiary engages in fraud for the New York mother company, and the only evidence of the crime comes from New Jersey, would RICO even be helpful?

It's tough to say.

The fact that RICO only counts in certain areas makes it easy for defense attorneys to claim the law doesn't pertain to their clients.

In addition, it's hard to collect evidence that can hold up against powerful executives in court. For example, when the insurance company Fairfax sued multiple hedgefund managers for deliberately damaging its reputation in order to artificially lower the company's stock price, the suit was dismissed despite the existence of emails in which the accused boasted about their crimes. These emailed confessions were not judged to be legitimate evidence.

While the police are quick to lend assistance after a bank has been robbed, don't expect the white-collar friendly justice system to help when it's the bank which robs you.

### 5. The Democrats have not challenged banks to change their financial behavior. 

Many people believe the financial crisis was a result of George W. Bush's policies. However, both the crisis and its lack of management can be linked to the Democratic Party.

Bill Clinton's elimination of financial regulations in the 1990s set the scene for the crisis. Clinton repealed the Glass–Steagall Act, a piece of legislation which required banks to keep their investment operations separate from their commercial operations, which handle money from regular citizens.

As a result, banks could gamble with the savings of private citizens, thereby exposing people to increased financial risk.

In addition, the loosening of restrictions enabled bankers to create risky new financial products, such as Collateralized Debt Obligations (CDOs). CDOs are complicated financial instruments in which individual loans like mortgages are combined into a single debt which can be resold to big investors for higher profit. The trading of unstable CDOs was a major contributor to the financial crisis.

Another Democrat who has been very lenient on white-collar crime is Barack Obama. While Bush attempted to fight financial crime with tough prosecution techniques like imposing jail time on fraudsters, Obama has sought to curb the problem through much milder settlements.

It was also Obama who initiated the hiring of many new Justice Department staff directly from Wall Street, so it seems that he's never been very eager to prosecute the financial sector. It's not hard to believe how small a budget Obama gave his financial crisis investigation team: a mere fraction of the cash behind Oliver Stone's second _Wall_ _Street_ movie, which dealt with financial fraud.

Coincidentally or not, investment banks provided huge donations to the election campaigns of both Clinton and Obama. Perhaps this explains why the Democrats have been so soft on white-collar criminals.

### 6. The US criminal justice system is unfairly biased against the poor. 

In the past few blinks, we've come to understand how the rich easily avoid the justice system. Unfortunately, the opposite is true for society's poorest members.

Poor people frequently find themselves targeted by the police. For example, though police departments deny it, officers are commonly required to meet certain arrest quotas. As a result, many charge innocent people with all sorts of ridiculous crimes, including jaywalking.

Police officers search for criminals to fill their quotas in poor neighborhoods, where statistics suggest they might be found. Only after a "suspicious" person has been arrested and detained will an officer attempt to blame that person for a crime. Often people are stopped for something as simple as driving a car that the police think is too expensive for them.

In addition, officers can subject the poor to "Stop and Frisk," a controversial policy allowing police to publicly search citizens at any time and for any reason. Especially notorious for its use among New York City police, Stop and Frisk was first introduced in the 1990s to dissuade criminals from committing illegal acts, though poor black males, immigrants and homeless people are actually the ones who end up being singled out.

Furthermore, Stop and Frisk produces more crimes than it prevents.

Consider the case of Tory Marone, a homeless man found sleeping in a park. When police searched Marone, they discovered a concealed joint. Marone could've possessed the joint legally as long as he didn't display it in public. However, after discovering it, police placed the joint in Marone's visible hand, thereby causing him to commit an illegal act. Marone couldn't afford to pay the fine and later went to jail.

Unlike rich, white-collar criminals, poor people who commit _poor_ _people_ _crime_ must cope with police that hassle them constantly and a justice system that already views them as hopeless cases.

> _"The basic principle is volume arresting. It's fishing for crime with dynamite."_

### 7. Money talks in immigration too: poor illegals are deported while foreign white-collar criminals can stay. 

By now you're probably beginning to see that when it comes to justice, those who have the fewest rights suffer the harshest consequences. In the United States, those people are illegal immigrants.

Why is that?

The truth is that both the police and the private detention-center industry profit by exploiting illegals.

The police can take advantage of illegals who have no rights. For example, Natividad is a single mother of two whose job requires her to drive every day. However, Natividad cannot obtain a license because she immigrated from Mexico to the United States illegally. When Natividad was pulled over, she discovered there are special rules for illegals: Her car was impounded until she paid $1,000, which is more than the car's value. Incredibly, this has happened to Natividad three times over the past six years.

Because illegals can be deported if they make even the tiniest mistake (like fishing without a permit), there's also money to be made in immigrant deportation centers. The government has contracted out these centers to private companies, which make roughly $160 per immigrant per day.

What's more, these correction companies are suggested to investors as reliable sources of growth, further solidifying illegals as the industry's best source of profit. As a result, many industry insiders lobby for stricter deportation laws so that they can make even more money.

Now compare this to foreign bankers who commit white-collar crimes.

Wealthy foreigners are shielded by their riches. Not a single executive at Deutsche Bank or British bank HSBC was deported even though their banking fraud was far more damaging than driving without a license.

The difference in how authorities deal with white-collar crime versus poor crime also holds true for the treatment of immigrants: You can stay if you have money, but if you're poor then either pay or get out.

### 8. For some reason, welfare fraud is one of the most zealously pursued crimes in the United States. 

Thinking about applying for welfare? You'd better verify that the number of toothbrushes in your home matches the number of inhabitants, otherwise you could be committing fraud. Believe it or not, officials seem more interested in catching welfare "thieves" than murderers and rapists.

In fact, the social security system actually operates in a way that results in _more_ cases of perceived fraud. For example, in California social security caseworkers are constantly being reshuffled, with an increasing number of employees assigned to pinpoint fraud instead of managing cases.

This makes it very unlikely that a welfare applicant would ever meet the same caseworker twice. Just imagine meeting a new doctor every time you visit the hospital to treat a chronic disease. Clearly this constant change makes it easier for the claimant to make mistakes during the claiming process.

On top of this, many California counties reward people who report welfare fraudsters. For example, Riverside County pays out $100 for each fraud conviction, and even prints the names of welfare criminals in the newspaper — an archaic public shaming technique that isn't even used against rapists or murderers anymore!

Moreover, people receiving welfare lose some of their rights in return.

For example, before being approved for welfare you must consent to home inspections to prove you're not trying to claim social security fraudulently. Officials don't need a warrant to search your home for evidence of this so-called fraud, and something as simple as having more toothbrushes than family members makes them assume fraud is being committed.

So while welfare is not the only type of government support available, it _is_ the only one that comes with reduced rights.

Although preventing fraud seems to be a major concern to US policy makers and the justice system, authorities appear less conscientious when they handle crooked mortgage schemes and other large-scale financial fraud.

### 9. US authorities do not have an accurate understanding of the wider consequences of crime. 

What behavior does society criminalize and what does it tolerate? Even if it's hard to believe, by now it should be apparent that US authorities would rather allow mortgage fraud to happen than let a black man drive around unmolested in a nice Range Rover.

This attitude can be traced to the _collateral_ _damage_ _doctrine_ adopted by policy makers. This doctrine requires prosecutors to consider the financial repercussions of charging corporations and their executives: In other words, if there's a chance other jobs are at risk, _not_ prosecuting is believed to be the best option.

But is that really the case?

Unfortunately not. What this doctrine doesn't recognize is that it's ultimately _more_ harmful to the economy to ignore white-collar crime than to prosecute.

For one, it's unlikely a company will fold as a result of a fraud conviction, because there will always be more executives to replace those removed. Additionally, failing to take these criminals out of their positions will result in greater economic problems in the future: Not only will companies believe they're above the law, but they'll also think there aren't any consequences to cheating and will continue taking financial risks and committing crimes.

Meanwhile, the collateral damage caused by pursuing poor criminals is never taken into account.

For example, after they thought they smelled marijuana, police detained and searched an innocent black man under the Stop and Frisk policy and the man later lost his job. Each year over 600,000 people in New York City alone experience similar outcomes after encounters with the law. They're fired, their records are ruined, their families get into trouble, etc. The only difference is that _these_ collateral consequences are accepted.

It's obvious that collateral damage is a good enough reason to curb the prosecution of white-collar crime, but not poor people crime. Is justice really being served?

### 10. The justice system responds to money, not people. 

Many of us have heard the phrase, "All men are equal before the law." In reality, the US justice system is more concerned with money and power than people.

Take a look at bureaucracy: the rich face increasingly lax rules while the poor must jump increasingly difficult hurdles.

Consider financial regulation again: The rules are constantly cut back so investors can pretty much do what they please because they have money to influence the process. Many of the richest corporations also have enough money to lobby Washington directly and change financial regulation so that it suits them better.

Meanwhile, poor people live with a massive bureaucracy that increasingly dehumanizes them. Not only must they tolerate reduced rights and welfare fraud investigators who can search their homes at any time, but it's also becoming more difficult for the poor to get the government assistance they need. Nowadays, applying for welfare is almost as hard as buying Rolling Stones tickets was in the 1970s: The only way to get an appointment is by standing in line for days.

But perhaps the biggest takeaway here is that the justice system just doesn't respect _people_, regardless of whether they're rich or poor.

In fact, rich people only survive and benefit from the system because it answers to their power, which they buy. However, should a rich person fall into a financial hole — maybe lose his or her retirement savings due to fraud — they will end up with the worst of the system too.

Likewise, the fact that most of the system's victims are black, single parents, immigrants or other marginalized people also comes down to money (or lack thereof): If any of these people suddenly came into riches, then they'd probably escape it.

To change the system, you'll need plenty of time and money. With both, you can invest in world-class lawyers and lobbyists who will combat the inhumane justice system at your request.

> _"In the Orwellian dystopia the secret crime was thought crime, but now in our new corporate dystopia the secret inner crime is need, particularly financial need."_

### 11. Final summary 

The key message in this book:

**The** **full** **extent** **of** **the** **rights** **conferred** **by** **US** **citizenship** **are** **not** **universal:** **The** **extent** **of** **your** **rights** **in** **the** **US** **justice** **system** **depends** **on** **the** **amount** **of** **dollars** **in** **your** **bank** **account.** **By** **treating** **the** **poor** **as** **criminals** **and** **the** **rich** **as** **special** **cases,** **the** **justice** **system** **further** **reinforces** **these** **structural** **wealth** **inequalities.**
---

### Matt Taibbi

Matt Taibbi is an award-winning American author and journalist. He became well-known as a financial journalist for his controversial reporting during the economic crisis. He is currently a contributing editor at _Rolling_ _Stone_.

