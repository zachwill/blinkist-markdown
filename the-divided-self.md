---
id: 557fcbcd6232320007030000
slug: the-divided-self-en
published_date: 2015-06-18T00:00:00.000+00:00
author: Ronald D. Laing
title: The Divided Self
subtitle: An Existential Study in Sanity and Madness
main_color: AA9347
text_color: 736330
---

# The Divided Self

_An Existential Study in Sanity and Madness_

**Ronald D. Laing**

Most people never question the "realness" of their body. _The Divided Self_ (1960) offers unique insights into the minds of those who do, and examines the practical and psychological consequences of their detachment from their own bodies.

---
### 1. What’s in it for me? Discover the work of one of the 20th century’s most influential psychologists. 

How do you think it would feel if you were trapped inside your body, unable to act or sense? Each time you did anything in the "real world," from talking to people to driving a car, it wouldn't be you acting, but a false sense — an imposter.

This terrifying situation is what influential psychologist R. D. Laing witnessed in some of his patients. His work in this area led him to develop ideas that would come to change the way we think about mental illness and psychology forever. These blinks provide you with an insight into his groundbreaking ideas.

In these blinks, you'll learn

  * why some people seem to think they can't lift a piece of paper;

  * why some people think they're in danger of being absorbed by others; and

  * why some people don't even care if their physical bodies are harmed.

### 2. Most of us perceive our distinct personalities in early childhood, leading to a safe sense of self. 

Would you say you know yourself? Where does your sense of identity come from? Did you always have it, or did it develop over time?

As it turns out, babies aren't born with a sense of identity, as they've never experienced life as themselves or encountered anyone else. They aren't yet aware that they are distinct beings, separate from the others around them.

They haven't discovered that they alone have direct access to their own experiences — that if they feel pain, for instance, that pain is theirs alone, and doesn't pervade the world.

Nor are babies aware of the fact that other people are likewise distinct beings with their own separate consciousness. They don't know, for example, that the breast or bottle that feeds them belongs to someone else. They don't even know that they can be seen or perceived by other people!

Typically, children begin developing a stable sense of identity during infancy. This process occurs as young children interact with their parents or guardians. But how exactly?

Typically, if a young child expresses a need — if he cries when hungry, for example — his parents will respond to that need. As this process is replicated over time, the baby begins to understand several important things: first, that his behaviors, like crying or giggling, elicit specific reactions; second, that he is a discrete entity, separate from the others around him; and finally, that the other beings around him, like his mother and father, are aware of his existence.

During this process of self-discovery and the development of self-consciousness, parents treat the baby as a complete and self-conscious person. Almost instinctively, parents interpret their children's behaviors as expressions of personality, even though their personalities haven't yet developed. Their projections nonetheless influence the child's emerging sense of self.

But as you'll see, not all children develop the same understanding of themselves and the world.

### 3. If a child is “too good,” it’s possible that she has an emerging psychological problem. 

Parents of a demanding baby or wild child may find themselves wishing they had a child who never cried or shouted, who obeyed their orders like a perfect little soldier. But the truth is that this ideal child may need help.

Babies who fail to express their needs — who never cry when they're hungry, for example — are prone to develop a weak sense of self as they grow older. These children may simply lack the strong instinct to express their feelings and seek out gratification, be it through excited crying or energetic flailing.

However, it could also be the case that the parents are either unable or unwilling to look out for and respond to the child's exclamations. This is typical among parents of future schizophrenics, who ignore the cries of their children and leave them in a puppet-like state of helplessness and confusion.

If the child doesn't express himself, or his guardians don't respond, he misses out on many of the interactions that strengthen his sense of self.

Moreover, preternaturally obedient or overly honest children might not be naturally "good" kids. It's possible that they're simply showing the early signs of a personality disorder.

If a school-age child never lies, this doesn't necessarily suggest a superior sense of morality. Rather, this may point to a serious problem: The child simply isn't sure where she ends and another person begins.

A child who is _too_ honest may fear that others can read her mind and, as a result, believe that there would really be no point in lying.

Also, a child that only does what he's told may simply lack the initiative to do something on his own. He may be unaware of his own wishes, or may not be able to distinguish his own wishes from his parents'.

Unfortunately, this "good" child may never attain a safe sense of identity or self.

### 4. Some people are fundamentally unsure of themselves; they need other people to feel real. 

Most people are afraid that they are inadequate in some way. Maybe they aren't attractive enough, or maybe they aren't the social butterfly they'd like to be. A fundamentally insecure person, on the other hand, experiences a whole other class of fears.

There are some people who are unsure about their own personality, and can't say definitively that they are real, whole and alive. The author refers to this condition as _ontological insecurity_, and many of those who suffer from uncertainty regarding their personality will become schizophrenic.

As an example, one schizophrenic woman would sometimes refer to herself in the second or third person, believing that she was "not a real person" and referring to herself instead as the "ghost of the weed garden."

One way for ontologically insecure people to feel more real is to meet other people. When we encounter someone else, we often regard and treat that person as real, irrespective of how the individual experiences himself or herself.

This can be very reassuring for ontologically insecure people. If they can be perceived and recognized by others, they can see that they are indeed both real and alive, and not merely apparitions.

In contrast, isolation can have the opposite effect. For instance, one of the author's patients recounted becoming extremely panicked when alone on the street, out of fear that she would dissolve whenever there was no one around who knew her.

Similarly, before an infant has developed a firm sense of self, he'll cry whenever his mother leaves the room, afraid that he too will disappear.

Indeed, some people are insecure in ways most of us couldn't possibly imagine. Social interaction is one way for them to feel real and grounded. Yet, all too often, ontologically insecure people shy away from contact. Why?

> _"The sense of identity requires the existence of another by whom one is known."_

### 5. Social interactions can be scary for someone with a weak sense of self. 

Imagine you're a therapist and have just had a "eureka moment" about one of your patients. You excitedly share your analysis, and expect them to be ecstatic that someone finally understands them. Instead, they simply look at you aghast and refuse to meet with you again. What happened?

When another person gets too close, ontologically insecure people fear they'll be engulfed by them. Their insecurities are so severe that they can only feel like an autonomous personality if there are significant differences between themselves and others.

When others get too close, ontologically insecure people feel that the distinctions between themselves and others dissolve to nothing. Consider your hypothetical patient: If you are able to understand them, then they can't be sure that they're actually a discrete person, separate from you. For them, it feels as if you have both merged into one, like adjacent puddles of water.

Similarly, approaching an ontologically insecure person with love or concern can threaten their sense of self.

Ontologically insecure people are typically unaware of many of their own feelings and sensations. If we show love or concern for them, then we might be expressing stronger feelings for them than they feel for themselves. 

In these cases, ontologically insecure people might wonder whether they belong to someone else. Maybe they're the other person's property, or perhaps their identity and agency have been stolen.

As an example, imagine you've found your insecure neighbor outside covered in blood. Strangely, they appear unconcerned about their wounds. You, on the other hand, are shocked and do your best to aid them.

Your insecure neighbor will note how much _more_ worried you are about their body than they are. This gets them thinking, "Maybe my body doesn't belong to me! Maybe it belongs to my neighbor, which is why he's panicking."

So, ontologically insecure people are stuck in a serious dilemma: On the one hand, they need others to feel real, but on the other hand, every contact threatens their sense of identity.

### 6. In an effort to preserve their identity, ontologically insecure people tend to split their personality. 

As we've seen, contact with other people can endanger an ontologically insecure person's sense of self. But total isolation is hard to achieve in our world. As a result, these people must find their own way to preserve at least some sense of self.

This starts with a retreat from human contact, either through isolation or by other means. For instance, since ontologically insecure people see being understood and liked by others as a threat, they will try to ensure that what they show the people around them is unintelligible. This way, they can hide their true feelings and thoughts.

While a neurotic person might hide unpleasant feelings out of shame, an ontologically insecure person hides their innermost feelings to protect them from others and to stay distant and safe.

However, even ontologically insecure people have to deal with the world in some way. To cope, they split up inside and develop a false self.

Think back to the dilemma described in the previous blink: Ontologically insecure people will feel unreal if they lack human contact, while also feeling threatened by that same contact. To manage this paradox, they develop a new persona — the _false self_ — that interacts with the world and displays all the appropriate feelings while keeping the true self, whose thoughts and feelings deviate from the false self's, hidden inside.

For instance, if you're at a party, you might have the impression that you're talking to a pleasant, normal young woman. Yet all the while, her inner self will disapprove of the idle small talk that her false self is participating in.

While these false selves initially begin as a protective shield for the real self, they eventually become more and more autonomous over time.

> _"For the schizoid individual...participation 'in' life is felt as being at the constant risk of being destroyed."_

### 7. Some people have a precarious separation with their bodies that further separates them from the world. 

Some people go through life feeling like a ghost. They observe the world undetected as if they're invisible and powerless, unable even to move something as light as a sheet of paper.

These people feel detached from their bodies, believing that these bodies themselves belong to the false self. And since they don't identify with their false self, their bodies must not really belong to them, either. This sense of detachment can stretch back as far as childhood, and can become so strong that the afflicted person won't feel any concern if their body is hurt or maimed.

Moreover, this detachment from the body can cause insecure people to become detached from the world at large. For example, whenever an ontologically insecure person perceives something, from a cold breeze to the sun's rays, they'll have the impression that they are only experiencing things indirectly; they will feel as if they are simply witnessing their false self's perception of the world. Their true self has become completely disembodied and invisible to others, just like a ghost.

Their true self has no opportunity to enjoy any direct contact with the world; it has no chance to interact with the world. Anything that requires the body must therefore be mediated through the false self (which is becoming more and more stubborn).

Some people who suffer from disembodiment will shut themselves off from the world. In other cases the false self lives a fairly normal life: It goes to college, chats with fellow students, has a job and so on, while the inner self remains hidden.

### 8. Being detached from the world comes with certain temporary advantages. 

So why would anyone prefer to live a life of isolation? As we've seen, ontologically insecure people choose isolation out of necessity, as a means to protect their fragile sense of identity. It should be noted, however, that a life of isolation nevertheless offers them certain advantages.

For one, since all interactions conducted through the body are carried out by the false self, the true self may feel completely self-contained. Since the inner self does not identify with the body, it seems to be uninfluenced by bodily needs like food, warmth, sexual gratification and so on.

Furthermore, disassociation from the body can allow ontologically insecure people to feel as if they have great talents and the freedom to do anything. While the person's inner self may be imprisoned and powerless, it is nonetheless free to imagine anything, envisioning any and all possibilities.

Being detached from reality, an insecure person's fantasies may feel just as real as reality itself. For example, if the detached person believes that they have an exceptional talent, such as singing, channeling spirits or martial arts, their talents are never actually put to the test, and their assumptions of themselves never verified or corrected.

Some detached people come to believe in the superiority of their inner selves due to the inner self's ethical consistency and honesty towards itself.

For all of us, managing the stresses and expectations of the outside world requires a certain amount of deception — both towards ourselves and towards others. And since most of us don't split our identity into a "false" and "true" self, we're forced to identify with all of our shortcomings.

An ontologically insecure person's true self, on the other hand, never interacts with anyone, and can thus afford to be completely honest. After all, it's the false self who does all the lying!

> _"The self is able to enjoy a sense of freedom which it fears it will lose if it abandons itself to the real."_

### 9. A divided identity can cross the line of (in)sanity and become schizophrenic. 

Not everyone with a split personality is insane. Sometimes, however, people's personalities are destroyed, sending them tumbling into a schizophrenic state. But how does this happen?

It starts when the inner self begins developing delusions about the false self. Since no one perceives an ontologically insecure person's inner self and because it has no opportunity to interact with the outside world, the detached inner self begins feeling insubstantial and unreal.

At the same time, the false self does interact with the world, and as it continues to do so it appears increasingly real, independent and powerful to the inner self.

The inner self may eventually develop the delusion that there's a stranger living in the same body who commandeers their senses.

While this is going on, the inner self goes for years without being able to interact with the outside world, thus strengthening their delusional thoughts.

We all make strange assumptions about the world around us from time to time. For example, if you notice that a railway crossing gate lowers every time you approach, you might think that there is some cosmic conspiracy against you to make you late for work.

But most people are able to talk about these delusions with others, who then challenge unrealistic ideas about the world. When this happens, we often adjust our perceptions accordingly.

For instance, as you approach that gate (which, as always, is lowering for a passing train), you might say, "Man, the universe must really have it in for me!" Your friend with whom you carpool might respond, "Yeah, it sure feels that way. But we usually arrive here around 7:20 each day, and there's a long-distance train that passes through at 7:21. So it makes sense that we always get stuck!"

An ontologically insecure person has virtually no contact with anyone but herself, so there's no way for her to confront her delusional thoughts. As a result, an entirely delusional worldview can easily develop.

### 10. Final summary 

The key message in this book:

**Many people assume that an acute schizophrenic's words are nothing but a meaningless salad of letters. However, with the right understanding of schizophrenics' experiences, their words and behavior can make a lot more sense. Learning from the unique experience of schizophrenics, we confront the fact that many of our assumptions about how the world works aren't as universal as we might think.**

**Suggested further reading:** ** _Cracked_** **by James Davies**

_Cracked_ gives a clear and detailed overview of the current crisis in psychiatric science: malfunctioning scientific standards and the powerful influence of pharmaceutical companies have caused the overdiagnosis and overmedication of people all over the world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ronald D. Laing

Ronald D. Laing (1927-1988) was one of the world's best-known modern psychiatrists, as well as a major proponent of the anti-psychiatry movement. He wrote numerous books during his lifetime, ranging from a collection of sonnets to sociological and psychological texts.

