---
id: 5649f66666343100070f0000
slug: the-dorito-effect-en
published_date: 2015-11-16T00:00:00.000+00:00
author: Mark Schatzker
title: The Dorito Effect
subtitle: The Surprising New Truth About Food and Flavor
main_color: EB6C2F
text_color: B25224
---

# The Dorito Effect

_The Surprising New Truth About Food and Flavor_

**Mark Schatzker**

_The Dorito Effect_ explains how the quality of our food has diminished over the past century. Modern agricultural practices and the emergence of a huge synthetic flavor industry have radically altered the food we eat. The following blinks examine how these significant changes have led to unhealthy eating habits and soaring obesity rates.

---
### 1. What’s in it for me? Learn why synthetic flavorings are turning us into nutritional idiots. 

When you tuck into that juicy chicken fajita at your neighborhood Mexican restaurant, you probably think it's the blend of chicken, vegetables and chillies you're tasting. But chances are you're wrong. In fact, what you're most likely tasting is a complex set of chemical flavorings designed to imitate the taste of chicken, paprika and chilli.

Unfortunately, eating food enhanced with artificial flavoring is dangerous to our health. In fact, the modern epidemic of obesity can be connected to it.

In these blinks you'll discover

  * why we use our nose to taste;

  * why it is perfectly natural to have orange juice and liver for breakfast; and

  * why flavoring makes unhealthy food taste healthy.

### 2. Despite decades of popular diet plans and weight-loss programs, obesity rates continue to rise. 

Have you ever run into an old friend at the supermarket and excitedly said, "You look great! When's the baby due?" at which point she bursts into tears and you realize she's not pregnant but has just put on weight? This is what once happened to a woman named Jean Nidetch in the fall of 1961, and it ended up changing the lives of millions.

The U.S. has seen a dramatic increase in obesity rates since the 1960s. Back then only 0.9 percent of the population suffered extreme obesity, whereas that number has since skyrocketed to 6.4 percent. Perhaps even more troubling is that a whopping 69 percent of Americans now fall into the clinical category of overweight or obese.

These statistics seem to defy the fact that Americans have spent trillions of dollars on dietary plans and products between 1989 and 2012: a period in which obesity rates doubled. One such plan is Weight Watchers, started by Nidetch, who wanted to help people lose weight by going to a support group with other overweight people. And yet, despite popular programs such as that, the life-threatening trend continues.

Because, let's face it, obesity isn't just an issue of aesthetics. It's a serious illness that requires a cure. Obesity brings with it an increased risk of asthma, cancer, heart failure, high blood pressure, diabetes and arthritis: all of which can contribute to disease and disability. And, the issue of aesthetics is also a factor as obese people, especially women, find themselves socially marginalized and unable to find well-paid work.

So what's happened in the past 50 years to cause this? Why has weight control been such a difficult thing to achieve?

### 3. Over the last decades, food quality has diminished due to intensive production practices. 

Here's a familiar scenario: You bring home a delicious looking chicken filet from the supermarket, but after seasoning and cooking it, you end up with a piece of flavorless shoe rubber. Why does it end up tasting, as beloved chef Julia Child puts it, "like the stuffing of a teddy bear"? What is making our food bland?

Quite simply, ever since the Second World War, all developments in farming practices have been aimed at achieving maximum yield at the expense of taste.

For example, in the late 1940s the government sponsored regional "Chicken of Tomorrow" contests throughout America. Awards were given out based on breast size, feed efficiency and weight. What's missing? That's right: taste!

This wasn't always the case. There used to be different kinds of chicken — such as broilers, fryers, roasters and fowl, each with their own distinct flavor. Much like the difference between beef and veal, these chickens would vary depending on their size and age. These days though, all chickens headed to the supermarket are slaughtered after 35 days or so because it's more efficient: that way they achieve maximum size and need one-third less feed to get to full weight.

If your only concern is efficiency, you might see this as an improvement. But in terms of nutrition and taste, food quality has actually deteriorated. 

The problem isn't only with chicken, either. Modern farming practices are affecting our produce as well. In the 1980s, a study in the _British Food Journal_ took a range of current produce, from bananas to parsnips, and compared it to the same food from the 1930s. The results proved that even in the '80s produce contained less nutrients than it did 50 years prior.

This is what happens when farmers begin using modern fertilizers and intensive irrigation practices. These methods already remove much of the food's natural mineral nutrients. And that's compounded by breeding practices that opt for foods that grow fast and large. This is what's referred to as the _dilution effect_.

So if this is what's been going on for so long, why haven't we taken notice and demanded the return of high-quality food?

### 4. The increasing use of synthetic flavorings has dramatically changed our food. 

Have you ever been frustrated after picking up a loaf of bread in the supermarket and noticing that the list of ingredients goes into the double digits? Isn't bread just water, flour, yeast and salt? It can be equally as frustrating to find that simple foods like ice cream and corn chips can list up to 30 different ingredients nowadays.

This increase in ingredients is due to technological advancements made in the food industry in recent years. For instance, the process of gas chromatography has made it possible to take any substance or food, and to separate, analyze and reproduce its aroma. This is how flavor scientists were able to extract the signature flavor of tacos and add it to tortilla chips. But it doesn't stop there.

Industrial chemistry has advanced to the point that labs can imitate natural flavors. These chemical compounds are trademarked secrets but they're what make, for example, McCormick's Imitation Vanilla Extract a best-selling item. The chemical that gives vanilla that special resinous note is one of 30 compounds that McCormick uses to make an inexpensive imitation of the stuff that consumers now prefer to the real thing.

These synthetic flavoring practices have increased dramatically over the past 30 years. Something has to spice up the bland, poor-quality, pre-packaged food the industry is manufacturing — and synthetic flavoring has become the answer. Take chicken nuggets: before they're ready for the dipping sauce the meat is ground up, mixed with water and seasoning and doused in a batter full of synthetic flavorings. 

Sadly, restaurants can be just as guilty of the pre-flavoring crime. Most fast-food eateries consider "cooking" a matter of assembling and reheating a packaged product that was manufactured and pre-seasoned in a food industry factory.

### 5. Our natural sense of taste guides us to the nutrient-rich foods we need. 

Do you ever find yourself craving a particular food, like olives, fresh fruit or a rare steak? When this happens, your body is probably telling you it needs specific nutrients which can be found in that food. The guiding force behind this dynamic is your powerful sense of flavor.

When you think of what's responsible for your sense of taste, you probably think of your tongue and mouth. But when it comes to appreciating food, your nose is the star of the show. The process of chewing and swallowing releases aroma vapors from the food, which reach the nasal cavity through a passageway in your throat. This important process is called _retronasal olfaction_.

The tongue certainly plays a role but it only picks up basic tastes like sweet, salty and sour. The tiny receptors in our nasal cavity, on the other hand, can pick up and differentiate over a trillion unique aromas.

With these tools at its disposal, our body then trains our sense of flavor by sending positive feedback when we eat foods containing the nutrients we need, making sure we'll continue to seek them out. This is perhaps best represented in a famous experiment from the 1920s when a pediatrician allowed 15 recently weaned babies to choose their own diet from a buffet of 34 different nutritious items. 

As you might expect, letting babies choose their own food led to some bizarre combinations like orange juice and liver for breakfast. But the results were conclusive: the children were listening to their bodies, choosing foods that provided the nutrients they needed and their health thrived because of it. Their sense of taste helped them become their own expert nutritionists.

### 6. Synthetic flavors trick us into eating unhealthy foods. 

So, if our bodies have this perfectly designed system intended to keep us healthy, why do so many people gorge themselves on junk food?

Many products containing synthetic flavoring can fool our bodies into believing we're eating something nutritious when we're not. Take Grapeade, a drink that actually contains 0 percent grapes. In reality, it's made of high-fructose corn syrup and pear juice. It's a classic example of what's called _bait and switch_ : by combining synthetic grape flavoring with a powerful flavor additive — in this case high-fructose corn syrup — Grapeade tricks our bodies into thinking we're actually ingesting healthy grapes, but instead the beverage is only feeding us refined carbohydrates and sugar.

Adding to our body's confusion are fortified foods and newly developed ways to supplement the nutrients we're missing. 

Normally, if we were on a diet comprised of only carbohydrates and sugar — say, in the form of frozen pizza and soda — our bodies would naturally revolt after no more than a week. Scurvy — which plagued pirates out on the high seas where they lacked fruit and vegetables — should eventually set in and we should start craving the micronutrients we need to survive. To prevent that from happening to us despite bad nutrition, the food industry has come up with things like fortified flour and enriched vitamin water, and puts these micronutrients in places they aren't normally found. 

So as long as we're popping multivitamins and eating enriched foods, our biological instincts aren't going to kick in when they should and the junk food binging can continue. In this way, additives and palatants contribute significantly to the growing obesity problem.

### 7. New breeding practices could lead to healthy food that also tastes great. 

There's a good chance that at some point or another you've been invited to a dinner party hosted by a health-food nut. If you've suffered through plates of whole grain, sugar-free dishes of bland raw vegetables, you can probably understand why people prefer tasty yet unhealthy foods like fried chicken covered in BBQ sauce. 

But healthy doesn't have to mean flavorless.

You might think the solution is to return to the small farming practices of the past, before all these problems started. But even though a home garden can provide you with a fine-tasting meal, this method isn't going to meet the demands of the modern world. After all, even though organic chicken and heirloom tomatoes taste a lot better than average industrially raised chickens and hothouse tomatoes, not everyone can afford them.

A better solution would be to develop new animal and vegetable breeds that strike a balance between flavor and yield. 

Originally, the food industry bred animals and vegetables to maximize efficiency and resistance to disease at the expense of flavor. But, thankfully, there are plenty of suitable breeds out there that could help fix the flavor problem.

New varieties of chicken can be bred that would cross the current big, plump variety with the more flavorful varieties of the past. As with fruits and vegetables, genetic crossing between the tasty heirloom variety and the bland commercial brand can result in a flavorful variety that still meets the food industry's requirements and is capable of producing enough food for everyone. 

As an added bonus, an increase in flavor also means an increase in nutrients where produce is concerned.

With these changes in place, traditional supermarket foods would improve in both taste and nutritional value. This would result in less desire for people to seek out satisfying flavor in junk food and artificially flavored carbohydrates.

Just think, this would mean a future where a healthy diet also tastes great!

### 8. Final summary 

The key message in this book:

**Synthetic flavorings are influencing our sense of taste to the extent that we no longer know what is good for us. When faced with natural foods, our bodies have the amazing ability to select the ones containing the nutrients we need. But when faced with an artificially flavored raspberry doughnut, the body is fooled into thinking it is eating a raspberry. The consequences are dire, as we see mounting cases of obesity and related diseases, such as diabetes, arthritis and heart failure.**

Actionable advice:

**Eat naturally flavorful foods and avoid synthetic flavors.**

When shopping, seek out and enjoy the best-tasting food you can find. Take the time to find real fresh bread made without additives. Eat peaches that are sweet, juicy and aromatic. Use olive oil that tingles the back of your throat. Also, make sure to read the ingredients listed on the packaging and avoid products that include the following: natural flavors, artificial flavors or flavoring, monosodium glutamate, saccharin, stevia.

**Suggested** **further** **reading:** ** _A Bone to Pick_** **by Mark Bittman**

_A Bone to Pick_ (2015), a compilation of articles originally published in the _New York Times_, outlines the systemic problems in the American food industry. It lays out the governmental and agricultural problems that are holding the industry back — and harming us and our planet in the process.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mark Schatzker

Mark Schatzker is an award-winning journalist, travel writer and food lover. He is also the author of _Steak: One Man's Search for the World's Tastiest Piece of Beef_ as well as a frequent contributor to _The New York Times_ and _The Wall Street Journal_.

