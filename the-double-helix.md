---
id: 58fc8ce3a6d7cf0004aeb8cf
slug: the-double-helix-en
published_date: 2017-04-24T00:00:00.000+00:00
author: James D. Watson
title: The Double Helix
subtitle: A Personal Account of the Discovery of the Structure of DNA
main_color: 665D2A
text_color: E7D25F
---

# The Double Helix

_A Personal Account of the Discovery of the Structure of DNA_

**James D. Watson**

_The Double Helix_ (1968) is a firsthand look at what is arguably one of the greatest scientific discoveries in human history — that of the function and double-helix design of DNA. Follow James Watson as he navigates the bitter rivalries and oversized egos of the scientific community in 1950s England and, in the end, assists in one of the most unlikely of scientific breakthroughs. This is the story, not of an extraordinary genius, but of an ambitious student trying to make a name for himself.

---
### 1. What’s in it for me? Take the thrilling ride on the way to discovering DNA. 

Charles Darwin, in _On the Origin of Species_, proposed that species are related and that different traits are passed on within a species. Though he noticed this process in the natural world, he didn't explain _how_ it happened.

All that changed with the discovery of the structure of DNA. The information contained within DNA finally proved that all living beings on earth are related, just as Darwin had postulated in the nineteenth century.

So how was such an important discovery made?

This is the story — told by Nobel Prize-winner James Watson — of how a number of different scientists approached the problem of DNA. Despite petty academic rivalries, each of these scientists persevered on the quest to prove how life is related and how it came to be.

In these blinks, you'll learn

  * why a general lack of interest in how genes were passed on opened new doors for the author;

  * how meeting Francis Crick would change the author's future; and

  * why academic conventions of the time almost clocked the discovery of the double helix.

### 2. The double helix is one of history’s greatest scientific discoveries because it explains the true nature of DNA. 

To understand the amazing significance of James Watson's story, it's important to understand what makes DNA so important.

Deoxyribonucleic acid, or DNA, is a molecule containing our fundamental genetic information, and it's made up of two strands of sugar-phosphate that twist around each other like the railings of a spiral staircase.

This structure is called a _double helix_. Since a single helix would resemble a coiled spring, a double helix is basically two springs curling in synchronicity with one another. These two strands are connected to bases, much like the railings on a spiral staircase are connected to steps. And each base is made of two out of four genetic building blocks, which are abbreviated with the letters A, C, T or G.

Whenever our cells replicate, the information in our DNA is copied and passed on. But for a long time, scientists wondered how this process could repeat itself millions of times, with very few errors.

The answer became clear when the double helix was discovered: when a cell splits, the staircase divides right down the middle. It works perfectly since the bases that connect the two strands can only bind with other specific bases. So when the two strands are split down the middle, each one becomes an inverted copy of the other. Once separated, the two strands act like a template for making two new double helixes.

It's a beautifully simple and dependable process, and it even helped confirm Darwin's theory that all life has a common ancestor.

This happened in 1961, eight years after the discovery of the double helix, when Francis Crick, Watson's DNA research partner, led an experiment that revealed the nature of the genetic code. It showed how the letters in three of the DNA bases serve as the code for one amino acid, which in turn can be linked to form a genetic code that describes the form and function of an individual's body.

And not just human bodies. The genetic code showed us that all organisms, including plants and animals, share the same primordial beginnings.

But, we are getting ahead of ourselves. Let's go back and look at how the double helix structure of DNA was discovered.

### 3. Since DNA wasn’t connected to genetics in the 1950s, it took chemists to start unlocking the secrets. 

Nowadays, the study of DNA and genetics is a central field in biology. But back in the 1950s, biologists didn't seem that interested in the properties of DNA. As the author James Watson saw it, the biologists, zoologists and botanists of the time were a "muddled lot," wasting their time speculating about such things as the origin of life.

Even those with an understanding of genetics mostly failed to address the important questions raised by the discovery of genes. They were asking about _which_ properties were hereditary while ignoring the more important question, which is _how_ that hereditary information is stored and passed on.

For these scientists, it didn't seem possible that the DNA molecule could be the carrier for genetic information. After all, DNA only consisted of four separate subunits; how could it contain such complex information? Some scientists even referred to DNA as the "stupid molecule," because of its lack of complexity.

The only people interested in DNA molecules were physicists and chemists, yet even these people weren't considering its link to genetics.

At the time, the best method for studying molecules, such as those found in DNA, was _X-ray crystallography_. This technique bends X-ray beams to get a 3-D picture of electron density, which gives scientists the ability to deduce certain information, such as which elements are present in a molecule, their configuration and how they're being held together.

It was with these tools and prevailing attitudes that James Watson found himself placed on the path to discovery.

At Indiana University, professor and microbiologist Salvador Luria was studying viruses, and he needed to use X-ray crystallography to find out what they were made of. However, he was unwilling to learn biochemistry by himself, so he turned to one of his PhD students, James Watson, and sent him to a lab in Europe, where the technique was most established.

### 4. Once in Europe, Watson got hooked on the DNA puzzle and teamed up with Francis Crick at Cambridge. 

With little expertise in chemistry, James Watson felt somewhat at sea in Europe. He had a lot to learn and, making matters more complicated, his mentor was Herman Kalckar, a man whose work was unrelated to genetics.

However, things changed during a conference in Naples, where Maurice Wilkins, of King's College in London, gave a lecture on DNA. At one point, Wilkins showed the audience an X-ray photograph of a DNA molecule. The picture revealed that the structure of DNA was likely to be very regular.

At this point, Watson realized that DNA was the kind of puzzle he could probably solve, and he began to take an interest in interpreting X-ray crystallography images.

This interest led him to apply for a transfer to the University of Cambridge, where Sir Lawrence Bragg, the founding father of X-ray crystallography, worked.

However, this process proved to be more complicated than he'd expected. His first request was rejected because Watson was considered under qualified. But, after establishing a phony partnership with another virus scientist at Cambridge, he was able to convince the admissions committee.

Of course, it was all worth it. Once Watson was officially enrolled at Cambridge, he met Francis Crick and immediately knew he was in the right place. Crick was a 34-year-old physicist with a sharp mind and a quick mouth. If you happened to be nearby when he became excited about a new idea, chances are you'd feel trapped as he loudly talked at you for a small eternity.

He also often upset colleagues and students by telling them how to interpret the results of their experiments. And he never hesitated to point out an error.

However, Crick was convinced that DNA was an important molecule, and he was happy when Watson showed up. Finally, there was another scientist who agreed with him. Before long, the two were working side by side, trying to figure out how genes fit together.

> _"I have never seen Francis Crick in a modest mood." - James D. Watson_

### 5. Since more prestigious parties were already investigating DNA, there were some clashing egos. 

Crick and Watson weren't the only ones studying DNA.

In England, Maurice Wilkins, whose talk had inspired Watson to get involved in the first place, was still at King's College in London, hard at work using X-ray crystallography to study the mysterious DNA molecule.

But Wilkins was a novice in this technique, and he relied on the help of a trained crystallographer named Rosalind Franklin, a woman he regarded as his assistant.

Franklin, however, had her own interest in solving the DNA puzzle, and she considered herself Wilkins's colleague. Indeed, she was smart and had her own multi-year contract with King's College, so, though Wilkins's working relationship with her was strained, he couldn't fire her.

Crick, on the other hand, had his own reason to be frustrated with Wilkins.

Wilkins never seemed excited to be working on DNA. But, according to academic ethics and the rules of fair play, Wilkins was considered to be the lead scientist in charge of the DNA case and all others had to keep their distance. To make matters worse, everybody in the English scientific community knew everybody else, making secret experiments impossible.

Furthermore, there was pressure to get to work. Crick and Watson figured it was only a matter of time before the chemist Linus Pauling, working at the California Institute of Technology, turned to DNA.

Pauling was already famous in the world of chemistry for the three-dimensional models of proteins that he'd built. Both Watson and Crick assumed that the 50-year-old Pauling was aiming for a Nobel Prize, and that it was only a matter of time before he'd realize that DNA and its link to genetics were the next big thing.

In fact, Pauling had already asked Wilkins for a copy of his DNA photograph — and Wilkins was purposely putting off giving it to him.

### 6. Despite some new insights, Watson and Crick’s research was temporarily shut down. 

With Wilkins and the British establishment on one side, and Pauling, the world's greatest chemist, on the other, the chances of success for Watson and Crick didn't seem so hot.

But they didn't give up. Instead, they began putting together their rivals' various methods to make their own model of DNA.

Pauling's method of constructing big 3-D models of molecules was frowned upon in London. To the old guard, these models looked like unscientific Tinkertoys. Yet Watson and Crick thought such an approach might be helpful.

Crick had seen the measurements that Rosalind Franklin had made with her X-rays, and he used them to narrow down the number of possible structures that DNA might have. In the end, he deduced that the structure had to be spiral — that is, helical — in nature.

Yet, when they made their first 3-D models, and invited Franklin and Wilkins to have a look, Watson and Crick were dismissed and heavily criticized by their colleagues as they'd recorded Franklin's X-Ray measurements incorrectly.

Sir Lawrence Bragg, the head of their research department at Cambridge University's Cavendish Laboratory, was also a harsh critic of their method of model-building, calling it unsophisticated and childish.

So Watson and Crick went back to the drawing board, and came up with a second model, this time with the right measurements, and taking into account new evidence from Austrian biochemist Erwin Chargaff that suggested the molecule's two bases — A & T and C & G — always appear at the same rate. Yet this new model was also dismissed without serious consideration.

Even Chargaff wasn't pleased. Prejudiced against Watson's American accent and haircut, he wanted nothing to do with the duo and scolded them when they couldn't immediately recall the chemical difference between the four bases.

At this point, Sir Bragg decided to finally shut down Watson and Crick's research.

Crick was told to focus on his thesis paper, and he knew that he would likely have to seek employment elsewhere once it was finished. Watson had to return to his work on viruses. However, he secretly continued to study X-ray crystallography, so that he wouldn't have to rely on others.

Still, Watson and Crick's work on DNA had hit a major roadblock. It would be a year before they would pick up where they left off.

### 7. A surprising error by Linus Pauling gave Watson and Crick a new opportunity. 

The year-long hiatus on DNA work did allow things to calm down at the Cavendish Laboratory. It also gave Crick the opportunity to publish some work and prove that he could be an asset to the department rather than a loudmouth, and Watson was also able to produce his own successful work — all of which put Sir Lawrence Bragg at ease.

It was during this calm period that an advanced copy of a manuscript by Linus Pauling — in which he attempted to identify the structure of DNA — arrived at Cavendish.

When Sir Bragg received it, he tried to keep it away from Watson and Crick, assuming they'd just waste their time on DNA if they saw it.

However, at this time, Linus Pauling's son, Peter, was also working at Cavendish, and he received his own copy of the manuscript, which he quickly shared with Watson and Crick.

They read it at once, and immediately spotted a big mistake.

In his calculations, Linus Pauling had failed to correctly ionize some of the molecules' subgroups, a college-level error that would cause the structure to be unstable and fall apart.

Watson discussed the error with Rosalind Franklin and Maurice Wilkins. This discussion resulted in an argument with Franklin, but Wilkins and Watson managed to bond over the error and their mutual dislike of Franklin.

Despite the animosity from her colleagues, however, Franklin made a crucial contribution. One of her photographs, which Wilkins showed to Watson, indicated that the backbone of the molecule must be located on the outside.

In the end, Pauling's error proved to be a major windfall. Sir Bragg, after being presented with Watson's findings, allowed Watson and Crick to continue their work on DNA in the Cavendish lab.

Pauling's paper was due to be published in six weeks, and Watson knew that the fallout would be so embarrassing for him that he'd begin working twice as hard to redeem himself. In other words, it wouldn't be long before Pauling fixed his error and, potentially, claimed all the glory.

### 8. The beauty of the solution obviated the animosities between the researchers. 

Sometimes, important advancements are based on a hunch — and such was the case with DNA: Watson made an educated guess that DNA's structure would have two helical strands, since in nature, bilateralism is the rule, rather than the exception.

Likewise, sometimes a lucky coincidence plays an important role.

As others had done before him, Watson began work on the new model by copying the chemical composition of the four bases from a standard chemistry textbook. However, this textbook contained a crucial error, which wasn't uncommon at the time. Luckily for Watson and Crick, they shared an office with one of the world's foremost chemistry experts, Jerry Donohue, who just happened to notice the error.

With the corrections made, things began falling into place. Watson realized that, if each strand has two bases, with A connecting to T and C to G, the structure could have stable strands of identical length. Thus the famous double helix began to take shape, with the base pairs forming the steps of the spiral staircase.

Watson felt confident about this structure because it also aligned with Chargaff's data, which indicated that DNA had an equal number of bases.

In fact, when presented with the new model, all of Watson and Crick's colleagues and former critics were won over by the beautiful simplicity of the molecule's design.

When Sir Bragg saw it, he immediately noticed how the strands of the double helix could assist in passing on genetic information. He was also proud that his advancements in X-ray crystallography had helped produce this remarkable insight into the nature of life.

Even Wilkins showed no trace of bitterness or resentment over the results. In fact, two days later, he and Franklin had results from their own experiments that supported the double helix.

Remarkably, even Linus Pauling was thrilled and quick to support the solution when the news finally crossed the Atlantic.

Eventually, Watson, Crick and Wilkins shared a Nobel Prize for their accomplishments. But, before having that honor bestowed on them, the group mended their relationship, especially when it came to Rosalind Franklin.

They began to understand and acknowledge how extraordinary her research was, and what a difficult position she was in, having to work within a patriarchal system that saw women as incapable of serious thinking.

Each of these people played a part in giving birth to modern genetics — and each continued to produce groundbreaking work.

### 9. Final summary 

The key message in this book:

**At just 21 years of age, James Watson arrived in Europe with a vague intention to learn chemistry. He may have been a novice, but he did have a burning desire to understand what a gene was. His curious spirit drew him to like-minded colleagues. No one would have thought that James Watson and Francis Crick would be the ones to finally solve the mystery of DNA, but the paper that they went on to write, proposing DNA's double-helix structure, would launch the field of modern genetics.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Immortal Life Of Henrietta Lacks_** **by Rebecca Skloot**

_The Immortal Life of Henrietta Lacks_ tells the story of a poor tobacco farmer who died from cervical cancer, and her cell strand, HeLa, which scientists used to develop a cure for polio and other diseases. In a fascinating and revealing investigation, author Rebecca Skloot uncovers the history of Henrietta and her family, of the exploitation of black Americans by the medical industry, and of Henrietta's immortal cells.
---

### James D. Watson

James D. Watson helped discover the structure and true nature of DNA. He would later help establish the Human Genome Project and work as director of the Cold Spring Harbor Laboratory, one of the world's leading cancer-research centers.

