---
id: 53b4246b63373200073b0000
slug: the-dragonfly-effect-en
published_date: 2014-07-01T00:00:00.000+00:00
author: Jennifer Aaker, Andy Smith with Carlye Adler
title: The Dragonfly Effect
subtitle: Quick, Effective, and Powerful Ways To Use Social Media to Drive Social Change
main_color: 30BAEF
text_color: 1F7799
---

# The Dragonfly Effect

_Quick, Effective, and Powerful Ways To Use Social Media to Drive Social Change_

**Jennifer Aaker, Andy Smith with Carlye Adler**

_The_ _Dragonfly_ _Effect_ explains how anyone can harness the power of social media to get people behind meaningful social change.

---
### 1. What’s in it for me? Learn to promote your cause by unleashing the power of social media. 

Love it or hate it, social media has become an inherent part of daily life. The likes of Twitter and Facebook have transformed the way we communicate and collaborate with others, by allowing us to reach more people faster and more efficiently than ever before.

One consequence of this is that we find ourselves overwhelmed with new information. Consider that each day millions of people log in to Facebook, posting new status updates, comments and photographs for others to see. Meanwhile, every single minute, twenty hours of video are uploaded to YouTube, while every second, hundreds of tweets are sent out to a global audience. What's more, this hectic pace is only likely to speed up in the future.

What does this deluge of information mean?

Simply, if you want to harness the power of social media to create a social impact — say to campaign for your favorite presidential candidate — you need to craft your messages carefully, or else you'll be just another drop in the ocean. It's not that people don't care about what you're saying, it's just that attracting people's attention is extremely difficult today!

So what's the right social media strategy?

These blinks will show how you can leverage the social media to your advantage to create social change.

### 2. Enacting social change via social media is possible through the Dragonfly Effect. 

In nature, the dragonfly is the only insect that can deftly and quickly maneuver in any direction in mid-air. It achieves this feat by using all of its four wings beautifully in concert.

Similarly, if you want to enact social change through social media, there are four "wings" that can produce astounding results, but only if used in concert as meticulously as the dragonfly's wings.

To illustrate the point, let's examine how these four wings are used by Samasource, a Kenyan charity organization that finds digital work for unemployed women and youths in impoverished countries.

First, there is the so-called _Focus_ wing: you must identify a simple concrete goal that you want to motivate others to achieve. For example, Samasource's focus is simply to provide women, youths and refugees with dignified work so they can earn a living.

Second, there's the _Grab_ _Attention_ wing: if you want to reach people, you first need to get their attention. In its communications, Samasource emphasizes that unemployment can lead to social ills, such as gangs and terrorism — definite attention-grabbers!

Third, there's the _Engage_ wing: if you want your audience to take action on behalf of your cause, you need to make them care about it by creating a personal connection to them. Samasource does this by sharing personal stories about the people it helps, but also about the donors who provide work for them.

Finally, there's the _Take_ _Action_ wing: you must enable and empower others to actually take action to further your cause. Here, Samasource offers three levels of action, the choice of which depends on the donor's resources. The most desirable action would be for the donor to outsource work to Samasource. After that comes donating money. And, finally, the most basic supporting action is to promote the organization. This range of options helps and encourages all kinds of supporters to take part.

### 3. The Focus wing: Look at your audience, and come up with a goal that is actionable and clear. 

The first wing of the dragonfly approach to social media requires you to focus intently on the one single outcome you wish to achieve.

So, how can you find yours? Simple: the goal you focus on must adhere to the so-called _HATCH_ _rule_.

First of all, your goal must be _humanistic_ : it must be based on understanding your target group. Start by getting in touch with the people you want to affect and find out what drives them and how they are connected to your cause.

An example of this approach can be seen in the way companies like Procter & Gamble develop their products: increasingly, they are working to understand the needs of end users and using them as a jumping-off point for development.

Second, your goal must be _actionable._ So while you want a visionary long-term goal to strive for, you also need to break it down into smaller, actionable and easy-to-understand goals.

Thirdly and fourthly, your goal has to be _testable_ and _clear_, meaning that you can set unambiguous milestones and measure your progress. This motivates your audience as they can celebrate the small wins and feel the momentum picking up.

Finally, your goal must be such that your audience is _happy_ to work for it. In other words, it should not be meaningful just for you, but for them too, so you need to explain to them why they should support your cause.

> _"Use short-term tactical micro goals to achieve long-term macro goals."_

### 4. The Grab Attention wing: be personal, unexpected and visual. 

Today, we're inundated with a never-ending stream of information: emails, text messages, tweets, Facebook status updates and banner ads on your favorite websites.

With all these messages vying for our attention, it is little wonder that it's very hard to actually get anyone to listen and to take action.

So, what's the solution? You have to get creative.

First of all, you need to be personal: if you're inviting people to an event on Facebook, for example, you'll surely evoke more interest if you add a personal message to each recipient instead of just sending a generic invite.

Similarly, if you customize the message about your cause so it feels personal to your audience, they will be more likely to pay attention.

Second, your communication should be somehow unexpected and surprising. This is because people tend to spend most of their time in a kind of autopilot mode, without really actively paying attention to any of the vast amount of messages that cross their screen. To shock them out of this trance, you need to surprise them. As marketing guru Seth Godin said, you need to create "a purple cow" that will stand out and grab people's attention.

Third, you should always "show rather than tell." Instead of long-winded explanations, synthesize your ideas into simple imagery, like photos and videos. Studies have shown that people are far better at remembering what they see than what they hear.

For example, consider one Japanese charity that fights obesity and hunger. It was successful largely because it simply had a compelling logo — two lunch tables: one full, the other empty — and a creative name: "Table For Two."

Finally, if you really want your campaign to be remembered, include an emotional connection by stimulating the other senses too, like hearing or taste. For example, choosing the right background music for a video can evoke powerful emotions.

### 5. The Engage wing: Get your audience involved by telling a story. 

Ever since humans developed the capacity for language, we have used it to tell stories. Historically, telling stories has helped communicate information from person to person and from generation to generation, and even though we don't typically sit around bonfires every night exchanging stories anymore, they are still a very powerful means of communicating.

Why is this?

Because stories inspire and touch people in a way that cold hard facts just can't.

As an example, consider KIVA, the non-profit organization that facilitates microlending to low-income entrepreneurs. It does not ask you to donate to some vague charity goal, but rather tells you the individual stories of each entrepreneur, thereby building a personal connection with them. The success of this approach can be seen in the fact that KIVA has already helped hundreds of thousands of people around the globe.

So, how else can you engage your audience?

First of all, craft your messages so they address the needs and feelings of your audience members. This, of course, requires that you understand them.

Second, you can't expect your audience to get passionate about your cause unless you yourself are authentically passionate about it. As the poet Robert Frost said: "No tears in the writer, no tears in the reader." You need to understand what you genuinely care about, for it is the only thing that you can get the audience to care about too.

Third, you must find the right media for your messages: for some personal requests, an email or text message may be best, whereas other messages are better broadcast on, for example, Twitter, where they can spawn more discussion.

> _"How we say something can be as important as what we say."_

### 6. The Take Action wing: Make it easy and fun for participants to take action. 

The last wing of the dragonfly is where the magic truly happens: people take action thanks to your message. Effecting this leap is far from easy, but you can make it happen by keeping in mind a few simple rules:

First, always make it easy for your audience to take action. An open-ended request that they should "save the world" will never be as successful as a campaign explaining to participants exactly how to buy and install energy-saving light bulbs.

Second, you need to show your audience that you value their contribution and are making efficient use of their time. People are more prone to participate if you can show them that they are in a unique position to have a disproportionate impact on your cause, so you should ensure that the call to action you make matches the interest and skills of your audience.

Third, you should also try to make your campaign fun. Consider including game-elements and humor to attract more people. For example, you could use a referral competition to entice people to get their friends to join the campaign as well.

Finally, make your program transparent and open, sharing information and allowing others to contribute ideas as well. This way you'll not only get more ideas, but your entire audience can follow your program's progress and be inspired by the milestones you hit.

> _"No one should ask for permission to act."_

### 7. Case Study: Alex’s Lemonade Stand 

We have now learned all about the four wings of the dragonfly. Next, let's look at a case study to examine how they made one little girl's social media impact soar.

In 1996, Alexandra Scott was born in Manchester, Connecticut. Sadly, before she had even turned one, she was diagnosed with neuroblastoma, a type of childhood cancer.

At the age of four, Alex started her own lemonade stand to raise money to find a cure for her disease. The very first summer, she and her brother raised an astonishing $2000.

In the years that followed, Alex and her family opened the lemonade stand each summer, and as word spread around the globe, she inspired others to open similar stands and donate their earnings.

Alex passed away at the age of eight, but she died knowing that she had helped raise over one million dollars for pediatric cancer research.

Today, _Alex's_ _Lemonade_ _Stand_ _Foundation_ _(ALSF)_ continues to encourage children to raise money for cancer research by setting up their own lemonade stands, and has so far raised over 80 million dollars.

Part of ALSF's success lies in its ability to use the four wings of the dragonfly in concert:

  * _Focus_ : In all of their messages, ALSF focuses on Alex and Alex's cause: eradicating childhood cancer.

  * _Grab_ _attention_ : ALSF uses an old American tradition, lemonade stands, in a wholly unexpected context — raising money for cancer research.

  * _Engage_ : ALSF connects to people emotionally by telling the moving story of little Alex who was so selfless.

  * _Take_ _action_ : ALSF provides individuals, schools and companies with a simple and fun way to contribute: by setting up a lemonade stand.

### 8. Final summary 

The key message in this book:

**It's** **not** **necessary** **to** **be** **rich** **or** **highly** **influential** **to** **bring** **about** **meaningful** **social** **change.** **Today's** **social** **media** **technologies** **enable** **virtually** **anyone** **to** **share** **stories** **in** **ways** **that** **can** **mobilize** **people** **all** **over** **the** **world** **to** **take** **action.**

Actionable advice:

**When** **you** **present,** **tell** **a** **story** **and** **use** **visuals.**

Next time you're preparing a written document to explain some idea or plan, consider whether you could instead make a brief video where you present the same information in story form. The images and narrative will make your presentation more engaging and more memorable.
---

### Jennifer Aaker, Andy Smith with Carlye Adler

Jennifer Aaker is a social psychologist and marketer who is currently a marketing professor at Stanford University's Graduate School of Business. Her work has been featured in several prominent magazines, including _The_ _Economist_ and _The_ _New_ _York_ _Times_.

Andy Smith is a seasoned technology marketing executive, and Aaker's husband.

Jennifer Aaker and Andy Smith: The Dragonfly Effect copyright, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

