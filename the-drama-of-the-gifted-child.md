---
id: 5694ae312f68270007000006
slug: the-drama-of-the-gifted-child-en
published_date: 2016-01-13T00:00:00.000+00:00
author: Alice Miller
title: The Drama of the Gifted Child
subtitle: The Search for the True Self
main_color: 327643
text_color: 327643
---

# The Drama of the Gifted Child

_The Search for the True Self_

**Alice Miller**

International bestseller and classic _The Drama of the Gifted Child_ (1979) is about the ways in which our unhappy, repressed childhood memories come back to haunt us as adults. Everyone deals with negativity in their childhood, and if adults don't confront these memories, they risk living unfulfilled lives or even passing their problems on to their children. Overcoming these suppressed emotions will set you free.

---
### 1. What’s in it for me? Overcome the drama of your childhood and find your true self. 

Have you ever experienced that weird chemistry that can arise when you meet your parents or siblings for a holiday get-together? When old conflicts and feelings from your childhood seem to resurface and create tension? You might think that your childhood is a thing of the past. Wrong.

Whatever their relationship to their parents and siblings, many adults carry unresolved — and painful — memories from their childhood. If left undealt with, these memories can have far worse consequences than heated arguments over Christmas dinner. Not only can childhood trauma lead to emotional problems and addiction, it can also prevent people from becoming who they truly are.

Still, many of us overlook the bearing of our childhoods on our adult life. So how do we deal with it?

In these blinks you'll learn

  * why people deny the importance of their childhood;

  * what the consequences of being abandoned as a child are; and

  * why parental pressure is so dangerous.

### 2. Many adults are unknowingly burdened by suppressed childhood memories and emotions. 

Have you ever felt like you were missing something, even though life was going well? We all feel this way sometimes. Why is that?

As adults, we often lose touch with our own emotions. That's part of why so many therapy patients say they can't feel anything, whether it's shame, jealousy, happiness, sorrow or joy. Life is empty without emotion; emotion is what makes life worth living.

Sadly, these therapy patients struggle to feel anything real. They've lost their ability to connect with themselves — a problem often rooted in their childhoods. 

A lot of people have rose-tinted memories of being a kid. They're nostalgic for those easier days when their parents took care of them and all they had to do was play. 

But childhood isn't just fun and games — it's also the time when most of us learn to suppress our emotions. As children grow up, they learn they have to quell their emotions so their parent will love and accept them. 

Consider a child with an over-controlling father, for example. From an early age, that child would learn to obey their parent rather than following their own wishes. And if they were beaten, they'd learn to suppress their tears and pain so as not to provoke their father even further.

A child with an overbearing mother might have problems with their sexual development. Alice Miller, the author, knew of a case of one unhealthily attached mother who went so far as to massage her son's genitals when he was almost in puberty. It had a terrible impact on his sexuality.

Children with parents like these often lose touch with their feelings as they get older. Repressing your emotions and memories can have serious, long-term consequences.

### 3. Repressed childhood emotions can resurface in adulthood in the form of destructive behavior. 

Sexual fetishism, womanizing and drug use are often viewed as lifestyle choices, but the mental reality behind them is usually much darker. Rather than being free, the people who engage in these activities are actually caged in the mental prison of their childhoods.

Repressed emotions can resurface in adulthood in a number of ways. Unlike children, adults try to keep their emotions in check to live a socially acceptable life. However, if they actively avoid dealing with dark childhood memories, their emotions get expressed in other ways.

When adults feel the onset of unwanted feelings from the past, they often look for an outlet that can provide a rational explanation for them. Some people might acquire complicated sexual fetishes, for instance, or they'll start seeking out a high number of casual sexual encounters instead of looking for a meaningful emotional connection. Others might drown their sorrow in alcohol or drugs.

The good news is that you can change your harmful behavior by dealing with your childhood memories. This may take a while, but patients can experience a big change when they finally confront their own emotions, often with the help of therapy. When such patients connect with themselves as children, they're finally able to connect with themselves as adults too.

Miller knew one such man, Peter, who had been a heavy womanizer for years. He'd hurt a number of people on his quest for more and more sexual partners. But when he finally confronted the fact that he'd had a lonely childhood because his mother spent very little time with him, he managed to build a loving and long-lasting relationship with a woman for the first time. 

When people like Peter confront their dark pasts, they can finally become whom they truly want to be.

### 4. Many gifted children have high expectations of themselves and grow up to suffer from depression. 

We've all heard of great composers, writers or artists who were unbelievably creative but suffered from depression throughout their lives. How could people who are so successful be so unhappy?

Brilliant people often have feelings of grandiosity. Their sense of their own importance and value is what propels them to create. However, grandiosity is the first sign of a repressed childhood — and it's usually coupled with depression. 

That's because such children typically have parents who demand a lot from them. So they demand more from themselves too, and thus go on to do more than the average person.

Such children strive for greatness and earn their self-worth from it. However, they inevitably reach a point where they can't climb any higher and there's only one direction they can go: down. And when they fall, they tend to fall hard — straight into a dark depression.

In 1954, researchers examined the backgrounds of a group of people who suffered from manic depression. They found that manic depression patients had generally been under great pressure as children and were often neglected by their parents if they failed. But failure is unavoidable, especially in adulthood, so their emotions spiraled out of control. 

Depression is ultimately another result of repressed emotion. Depression sets in when the inner child, who was never accepted in her own right, realizes she can't buy love with achievements anymore. 

If people with repressed emotions don't work through their childhood issues, they often spiral further and further into depression as they get older. Some even attempt suicide.

And if adults with unresolved childhood problems have children of their own, they pass their trauma on to the next generation and the cycle continues.

### 5. Unaddressed childhood trauma can be passed down through generations. 

Adults who are emotionally trapped in their childhoods subconsciously want their children to have the idealistic childhoods they never had, because their own parents didn't give them the chance. In their quest to be the perfect parents, they put stress on their children in new ways.

Parents usually want the best for their children, of course. They want them to be happier and have more fulfilling lives than they had themselves. And if a parent didn't get enough love from their own parents, they'll try to foster that love with their children.

This can have negative consequences, however. The child will notice their parent wants them to be happy all the time, so they'll suppress negative emotions like fear or pain in order to please them. They'll end up repressing and denying important feelings — just as their own parents did!

This inevitably causes the cycle of damaged childhoods to keep spinning on and on. If a person doesn't address their own damaged psyche before they have children themselves, their children will grow up to become just as damaged as they are. And how could that child, in turn, grow up to become an effective parent?

A mother of two contacted Miller when she was struggling with these issues, because she feared she couldn't love her children properly. After working through her own childhood memories, she realized her mother had never loved her for who she was. Achieving this revelation meant that by the time she'd had her own third child, she was finally able to accept him for who he was and give him the love he truly needed. She didn't try to relive and correct her own childhood through him.

### 6. People who succeed in becoming their true selves will change the world for the better. 

We've looked at the negative aspects and consequences of repressed emotions, but what happens when a person finally overcomes them? The good news is that it's possible to get past your dark memories. An adult can undergo miraculous changes when they confront the unhappy emotions of their past.

People who effectively confront their repressed emotions are finally able to live life to the fullest. They're not bogged down by their own contempt, hatred or self-destructive behavior. Instead, they come to accept their own emotions for the first time. They allow themselves to love, hate, cry or laugh — instead of ignoring those feelings and trying to live without them.

When therapy patients face their childhood trauma and mourn their own loss of self, they often say they're finally able to be _themselves._ And of course that has positive effects on their family — and even society too!

When a person can finally be themselves, they'll allow their partner or children to be themselves too. A woman who was ashamed of her submissive mother, for example, would no longer feel the need to dominate her husband to compensate for her. Political leaders who spread hate for minorities out of their own self-contempt would also learn to accept others for who they are.

All in all, embracing your true, inner self is the most important thing you can do. When you confront your childhood trauma and learn to accept the person you truly are, you don't just liberate yourself. You'll also help your family, and have a positive impact on the world at large too!

### 7. Final summary 

The key message in this book:

**Many people suffer from depression, addiction or a general dissatisfaction with life. Though few are aware of it, these problems usually stem from childhood. As we grow up, we learn to stifle certain emotions to please our parents, and these emotions can resurface in other ways in adulthood. If you don't confront these dark memories, you can never achieve your full potential.**

Actionable advice:

**Talk to your parents while you still can!**

Have you held back something you wanted to tell your parents? That emotional pressure might be causing you strain. So talk to them; let your emotions and frustrations out. Chances are, you'll be much better off if you do. 

**Suggested further reading:** ** _The Talent Code_** **by Daniel Coyle**

_The Talent Code_ uses recent neurological findings to explain how talent can be trained through deep practice. It shows how nurturing our cellular insulation — called _myelin_ — influences the development of our skills, and explains why certain methods of practice and coaching have been used in "talent hotbeds" around the world to great success.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alice Miller

Psychologist and psychoanalyst Alice Miller was the author of a number of highly acclaimed books such as _The Truth Will Set You Free, Banished Knowledge, For Your Own Good, Thou Shalt Not Be Anymore_ and _Breaking Down the Wall of Silence_.

