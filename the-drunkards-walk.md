---
id: 540f2e353934640008330000
slug: the-drunkards-walk-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Leonard Mlodinow
title: The Drunkard's Walk
subtitle: How Randomness Rules Our Lives
main_color: 6C5EA7
text_color: 6C5EA7
---

# The Drunkard's Walk

_How Randomness Rules Our Lives_

**Leonard Mlodinow**

This book is about the role randomness plays in our lives. It explores the historical roots of modern statistics and delves into fundamental mathematical principles to explain how — like a drunk person struggling to walk — much of our lives is dictated by pure chance.

---
### 1. What’s in it for me? Learn how much of your life is random. 

How much of our life is really in our control? To what extent can we attribute our success to our skills? Is it possible for us to predict future events? And if so, how?

This book explores these questions and more by examining the crucial role that _randomness_ plays in our lives, reviewing the history of modern statistical research and outlining some fundamental mathematical concepts that will equip you with a better understanding of statistics. In short, you'll learn about how much of your life might be based on pure chance.

You'll also learn

  * how a man with no expertise in the stock market correctly predicted the stock market for 18 years;

  * why receiving a positive HIV test might not be as terrifying as you think;

  * how Galileo revolutionized scientific study by rolling dice; and

  * how Bruce Willis's Hollywood success might be due to a vacation he took in 1984.

### 2. The likelihood that an event will occur depends on the number of ways it can occur. 

Do you feel like winning a dice game has to do with your talent or your luck? Probably luck — after all, you know that dice is based on chance. However, if you'd won a dice game in the sixteenth century, people would've commended you on your excellent throwing or on having received God's good fortune.

Why? Because back then people didn't know about probability. It wasn't until Galileo began introducing _experiments_ and _observations_ into scientific study that this changed. He soon realized that random acts, like throwing dice, could be analyzed.

Galileo explored the following question: Why is it that when someone throws three dice, the dice will more often have a total value of ten than nine?

And, after researching, he came up with a scientific explanation. Ten comes up more often than nine because there are more possible combinations that add up to it. He thus discovered an important mathematical principle: the chances of an event occurring depend on the number of ways it can occur.

Other scientists, such as Blaise Pascal, would later expand on Galileo's insights. Pascal dealt with another dice situation and discovered something called the expectancy value. Imagine two people playing a dice game where the first person to win ten rounds takes home the winnings. But if the game has to stop early when player one has eight wins and player two has seven, how should the winnings be divided?

First, determine the possible scenarios left in the game (in this case, 16). Then, see how many of those scenarios would result in player one winning (11), and how many would result in player two winning (5). It then becomes simple — player one should get 11/16 of the winnings. That's the _expectancy_ _value_.

So, to determine the likelihood that any future event will happen, you have to know how many different possibilities lead to it. This is a fundamental idea in mathematics.

### 3. You can calculate the probability of certain outcomes using the law of large numbers. 

Imagine you start rolling a dice and recording the numbers. Would you expect the results to be _perfectly_ random? If they were, each number would appear exactly once every six rolls. In reality, that's quite unlikely. So what does that say about randomness?

Well, in nature, there's no such thing as _perfect_ randomness.

One gambler named Joseph Jagger understood this in 1873. While playing Russian roulette, he recorded all the winning results on six different wheels. He found that, on one wheel, nine numbers came up more often. So he and his friends started betting on those numbers, and won the equivalent of about $5 million in today's money.

This poses an interesting question: If some numbers appear again and again, what's the probability they'll continue to appear in the future?

At the end of the seventeenth century, one of the first mathematicians to approach this question was Jakob Bernoulli . After twenty years of work, he developed the _golden_ _theorem_, also called the _law_ _of_ _large_ _numbers_.

To understand it, imagine a jar of 5,000 pebbles, of which 60 percent are white and 40 percent are black. If you drew 100 pebbles, you _might_ get 60 white and 40 back, but it's also possible to draw 50 white and 50 black, or another combination that's not too far off.

However, as you draw more pebbles — say, 1,000 or 2,000 — you'll gradually get closer to a perfect 60/40 divide between white and black. This fact — that the percentage will become more precise as the numbers increase — is the _law_ _of_ _large_ _numbers_.

Using the law of large numbers, Bernoulli was able to calculate the probability of drawing between 58 and 62 white pebbles when drawing a certain total number of pebbles.

Imagine Jacob Jagger had known about Bernoulli's theory — he could've earned even _more_ money.

### 4. Probability can differ greatly depending on the specific problem you look at. 

If you took an HIV test and it came back positive, what would you do? Understandably, most people would probably panic. But mathematics can offer us some interesting insight here.

Unfortunately, the likelihood of your HIV test result being positive is probably higher than you think. Even the author, Leonard Mlodinow, had an HIV test come back positive once. Mlodinow then asked his doctor what the chance was of a test showing a positive result if the person was truly negative. He said it happens only once every 1,000 HIV tests.

However, that probability wasn't really relevant. Mlodinow had asked about the probability of a test coming back positive _if_ the person is negative. This is very different from the probability of a person being HIV negative _if_ the test comes back positive.

The answer here is shocking: in ten out of eleven cases of positive HIV tests, the person is actually negative. Only one out of eleven tests correctly identifies a person with HIV. Thus, the likelihood of not having HIV, despite receiving a positive test, is over 90 percent.

This important concept is called _conditional_ _probability_. It examines the likelihood that an event will occur, given that another event has _already_ _occurred_. In our situation, the HIV test had _already_ produced a positive result. So we needed to see the probability that, given that, the person might be negative.

Here's another example: if you have ebola, it's likely that you'll have a headache. But if you have a headache, it's not necessarily likely that you have ebola. The situation changes greatly depending on which condition is first.

So the next time you read a statistic, look carefully at the conditions of it. They could make the difference between a probability of .1 percent and one of more than 90 percent!

### 5. Random errors and variations can influence statistical calculations. 

Have you ever encountered two statistics measuring the same thing with different results? Statistics can be twisted to promote certain agendas, but they can also be subject to random human error.

Sometimes small mistakes in calculations or data recording can have huge consequences.

In 2006, the American Bureau of Labor Statistics reported the national unemployment rate to be 4.7 percent, which was an improvement from the previous year's 4.8 percent. In response, _The_ _New_ _York_ _Times_ wrote an article entitled "Jobs and Wages Increased Modestly Last Month." However, that wasn't exactly the case.

An increase so small — 4.7 to 4.8 percent — might have just been the result of an error. And, unfortunately for _The_ _New_ _York_ _Times_, it was: this phenomenon is known as _random_ _error_.

In this case, someone might've simply reported something wrong. Even if only a few cities were reported incorrectly, that could've impacted the overall figure for the country.

Random error can be even stronger when measuring something that's subjective, like wine ratings. Wine critics typically rate wines from 1 to 100. But how reliable are these ratings? If three critics rate one wine as an 80, that wine is probably pretty good. But what if the ratings are 60, 80 and 100? Those numbers also average 80, but the critics didn't favor the wine equally.

To illustrate the variety in such statistics, mathematicians developed the concept of _sample_ _standard_ _deviation_, which measures the variance between results in a given sample.

In the first example, where the critics all gave the wine an 80, the standard deviation would be _zero_ as there isn't any variation. In the second case, the standard deviation would be higher (20), which would demonstrate the variety and help the wine drinkers understand the ratings better.

So, seemingly small variations or errors can have profound effects. Learn to keep an eye out for them and you'll have a deeper understanding of statistics.

> _"From a theoretical viewpoint, there are many reasons to question the significance of wine ratings."_

### 6. Accurate data sets often produce a bell curve. 

If you were to graph the heights of everyone you know, how would the distribution look? Many data sets, such as human height, have what's called a _normal_ _distribution_ or _bell_ _curve_.

A bell curve is literally shaped like a bell and it has two fundamental characteristics:

First, the position of its _mean_. The mean is the average (60, 80 and 100 have a mean of 80). In a normal distribution, the mean is the most represented value. So with male height, the mean might be 1.8 meters. The highest (or most represented) value on the bell curve will be 1.8, and 1.79 or 1.81 will be lower.

The second characteristic is the spread of their data. The further you get from the mean, the fewer the representations. So in our height example, there are more men who are 1.8 meters tall than 1.81 meters, but also more who are 1.81 meters tall than 2.01 meters.

So if we can identify the characteristics of a bell curve, it indicates that the data is probably accurate.

Let's say the average American male height is 1.8 meters. If you measure the heights of males at different American universities, they might not all average 1.8 meters — one school might have 1.78 meters and another might have 1.81 meters — but it'd be unlikely to see a data set with an average of 1.7 meters because that's quite far from the national mean. So a sample, such as male height at a certain university, may not be perfectly representative in the case of the national average, but it will often be quite close.

Many data sets are normally distributed in this way, which is useful for making predictions. If you have a smaller data set, you can often predict the outcome of a larger data set. Predictions for upcoming elections, for instance, are done this way, and they're usually close to the actual result.

### 7. We can find meaningful patterns between different sets of data. 

How tall are you? How tall are your parents? Do you think there's a relationship between those two variables? If so, how can that relationship be expressed?

Mathematician Francis Galton, who was a cousin of Charles Darwin, investigated this phenomenon when he developed the _coefficient_ _of_ _correlation_.

Galton was fascinated with measurements. He measured people's heads and noses, and even the attractiveness of girls he passed on the street (to him, London girls were the most attractive, and Aberdeen girls the least).

More importantly, he began to predict certain measurements of children's bodies based on measurements of their parents. He plotted a graph of the children's height on top of a graph of their parents' height and observed that the two graphs seemed to be related. By doing so, he discovered an important statistical concept: _correlation_.

The _correlation_ _coefficient_ describes the degree of correlation between two variables. So here, our variables are the parents' height and their children's height and we have a _positive_ _correlation_, which means that the variables follow a specific pattern: the taller the parents, the taller the children.

The highest possible correlation of these variables would be if a person's height _always_ followed a formula depending on his or her parents' height. So a person's height might be exactly the mean of his or her parents' height, for instance. Height doesn't work that perfectly, but there's certainly a positive correlation to be seen.

Another example of positive correlation might be a connection between eating at McDonald's and gaining weight. If, in the unlikely event that eating McDonald's somehow correlated with _losing_ weight, there would be a negative correlation: eating more would mean weighing less.

So, the correlation coefficient describes how different variables are connected with each other. It's a vital way of mathematically proving the relationship between variables, and it's one of the most important concepts in statistics.

### 8. Randomness explains some people’s success. 

Most of us would like to think that people are successful for a reason. However, this isn't necessarily true.

Humans always look for patterns even when there aren't any. We can see this in the case of Leonard Koppet, who in 1978 invented a system that predicted whether the stock market would go up or down.

And the system worked: from 1979 to 1989, his predictions were right every year. After being wrong in 1990, his predictions were correct again until 1998. In 19 years, he was wrong only once.

Was he an incredibly gifted marketing analyst? Not at all! He was a columnist for a sports magazine, and his predictions came from looking at the results of the Super Bowl. His results were coincidental, and there was no proof that his prediction system was valid.

Just as these correct predictions weren't based on a pattern, it might also be the case that a person's success is just luck. Bill Miller, a mutual fund manager who invested very wisely in stocks from 1990 to 2005, is another example. One analyst calculated the odds of his success to be 1 out of 372,529.

This probability, however, is not the important one. There are almost 6,000 mutual fund managers in the United States, making the likelihood that one of them performed as successfully as him go up to 3 percent.

Moreover, if the analyst had looked at more years than only 1990 to 2005, he would've found that that level of success had a 75 percent chance of occurring. A success story like Bill Miller's was actually quite likely.

So the odds that _someone_ was going to be that successful were very high; the odds that it would be Bill Miller, _specifically_, were very low. Thus, Miller's success can hardly be attributed to his skill: if it hadn't been him, one of the other 6,000 mutual fund managers probably would've succeeded just as well. And that shows how our lives are affected by randomness. Just as a drunk person walking in random patterns on a sidewalk, our lives also follow random patterns. Sometimes we end up being successful and sometimes it's our competitor.

### 9. Personal success has more to do with chance than individual qualities. 

Many people think of Steve Jobs as the ultimate visionary. They attribute Apple's massive popularity to his genius. But is this the best way to understand his success?

Numerous factors determine a person's success — and luck is an important one of them. One study on people's taste in music illustrated this particularly well.

In the study, people were asked to rate songs by bands they'd never heard of. They were divided into eight groups. Some groups saw previous ratings that other people had given and others didn't.

If the songs' popularity was based only on their quality, then each group should've had roughly the same ratings. But this wasn't the case. When people could see that certain songs had been rated higher, they were more likely to rate them higher as well. Their opinion was influenced by what they thought was the majority opinion.

This means that if something becomes popular early or quickly, it's likely to become _more_ popular. How much credit can we give Steve Jobs himself for the popularity of his products? It's possible that they were simply released at just the right time for their popularity to be contagious.

Hollywood actors are another good example. Are the famous ones really the best? Or are there some talented actors who never get discovered?

Just look at Bruce Willis: he was working as a bartender while trying make it as an actor when, in 1984, he went to LA for a holiday. While there, he decided to go to a few auditions. Luckily, he landed a role in _Moonlighting_, which ended up launching his career.

What if Bruce Willis had never taken that trip to LA? Would he still have been discovered? Perhaps he would still be working as a bartender if it weren't for that one audition.

So, if you want to be successful like Steve Jobs or Bruce Willis, you don't only need talent. You also need luck.

> _"The random motion of molecules in a fluid can be viewed as a metaphor for our own paths through life."_

### 10. Final summary 

The key message in this book:

**Randomness** **is** **a** **much** **more** **important** **force** **than** **most** **of** **us** **think** **(or** **would** **like** **to** **think).** **Instead** **of** **trying** **to** **find** **a** **pattern** **in** **everything** **you** **see,** **be** **aware** **that** **many** **things** **in** **life** **are** **simply** **unpredictable.** **Some** **things** **are** **far** **beyond** **anyone's** **control** **and** **happen** **just** **by** **chance.**

Actionable advice:

**Don't** **take** **statistics** **at** **face** **value.**

Sometimes people tweak statistics to promote a certain message or agenda, so closely examine any statistics you encounter. Are the conditions appropriate? Is there room for random error? Is correlation accurately expressed? When you examine statistics more closely, you'll be more resistant to other people's attempts to dupe you with their data.

**Intimidated** **by** **someone's** **success?** **Remember** **randomness!**

Though successful people are often quite skilled, part of their success is also due to luck. So don't let yourself feel so inferior to those who are more successful than you — it might just be that they were in the right place at the right time!

**Suggested** **further** **reading:** **_Fooled_** **_by_** **_Randomness_** **by** **Nassim** **Nicholas** **Taleb**

_Fooled_ _by_ _Randomness_ is a collection of essays on the impact of randomness on financial markets and life itself. Through a mixture of statistics, psychology and philosophical reflection, the author outlines how randomness dominates the world.
---

### Leonard Mlodinow

Leonard Mlodinow has a PhD in physics from the University of California at Berkeley and is the author of many successful scientific books, such as _A_ _Briefer_ _History_ _of_ _Time_ and _Feynman's_ _Rainbow:_ _A_ _Search_ _for_ _Beauty_ _in_ _Physics_ _and_ _in_ _Life_. He has also written for the television series _MacGyver_ and _Star_ _Trek:_ _The_ _Next_ _Generation_.

