---
id: 53cf7bc43630650007ac0000
slug: the-e-myth-revisited-en
published_date: 2014-07-22T00:00:00.000+00:00
author: Michael E. Gerber
title: The E-Myth Revisited
subtitle: Why Most Small Businesses Don't Work and What to Do About It
main_color: 2288A9
text_color: 1C718C
---

# The E-Myth Revisited

_Why Most Small Businesses Don't Work and What to Do About It_

**Michael E. Gerber**

In a revised and updated version of his famous book _The_ _E-Myth_, author Michael Gerber cuts through various myths about what's involved in starting a small business and how to make a business successful. Walking you through every stage of how to build a business, _The_ _E-Myth_ _Revisited_ highlights the important difference between working _in_ your business and working _on_ your business.

---
### 1. What’s in it for me? Learn the secrets for small-business success. 

Did you ever realize that the vast majority of small businesses fail without ever becoming successful? Did you ever wonder what's so special about businesses that survive past the five-year mark and then run smoothly ever after?

In these blinks, you'll find an easy-to-follow guide to making sure your business is a success and not just a sad statistic.

You'll also find out:

  * why 80 percent of small businesses fail in the first five years,

  * why founder Ray Kroc's strategy for McDonald's is one you should follow, and

  * why your business will be better off without you.

### 2. A heroic entrepreneur with a great idea and technical know-how succeeding in business quickly? That’s a myth. 

Did you know that one million small businesses are founded in America each year, but 40 percent of them fail in the first year and 80 percent in the first five years? That's 800,000 failed businesses, and most of these failures are due to the E-Myth.

The E-Myth, or entrepreneurial myth, is a fundamental misunderstanding in American business. It's the notion that skillful technical work and a good idea form a sufficient basis for business success.

People often start their own business merely because they excel at work in a certain field, such as a machinist, barber or computer programmer.

Then one day the _entrepreneurial_ _seizure_ strikes. They realize they don't want to do technical work for someone else. They want to work for themselves as per their own ideas for their own business.

Let's say you work as a barista. You've mastered coffee roasting, brewing and latte art and you have lots of ideas of how to run a cafe. Suddenly, you realize you'd rather open your own cafe.

Such a realization is the reason behind the one million new businesses each year.

But if you start a business from the basis that you have technical expertise and new ideas, you've already started on the wrong foot. Your business will probably fail.

You've made the _fatal_ _assumption_, the mistaken belief that knowing how to do technical work means you know how to run a business.

In fact, technical work and the work required to run a business are two completely different things.

Here's an example. A barista opens her own cafe, and soon realizes that her coffee skills are not enough to make her business successful. She has to know how to hire more employees, organize tasks and grow her business.

This is why so many small businesses fail!

### 3. Usually an entrepreneur’s business won’t survive past adolescence. 

Have you ever thought about the stages of a business like those of a person? Interestingly enough, businesses go through infant, adolescent and mature stages, just like we do. The difference is that most businesses won't survive adolescence.

In the first stage, infancy, the owner and the business are one and the same.

At first, infancy is romantic. The business owner finally gets to do all the work herself! For example, the barista opens her own cafe and is now roasting and brewing her own coffee — great!

But success at this stage means more customers and more production. Eventually, the work becomes too much to handle.

At the barista's cafe, customers notice the space isn't as orderly anymore, because the owner doesn't have time to clean every day.

Suddenly, by starting her own business, the owner too finds herself buried under technical tasks. She's become the boss she wanted to avoid!

When she hires someone to help her, the business enters its adolescence phase.

Adolescence also starts out great, as the owner doesn't have to do everything herself anymore.

But most adolescent business owners enjoy freedom too much and _manage_ _by_ _abdication_ instead of _managing_ _by_ _delegation._ They leave their tasks to others and assume they're taken care of, instead of ensuring everything is done properly.

Back at the cafe, customers start to complain about the lackluster lattes the new employees make.

During this part of the adolescent stage, the owner must be pulled past her _comfort_ _zone_, where she controlled everything in the business herself. The business will fail unless it can grow beyond the owner's ability to do and control everything herself.

What can the former barista, now business owner, do?

She could get small again, fire her employees and return to her _comfort_ _zone_, where she's instead overwhelmed with work.

Alternatively, she could go for broke and let the growth of her business accelerate until it got out of control, hiring more employees and accepting an inevitable decline in quality.

Or finally, as you will see in the following blinks, she can accept that her business has to grow and plan for this opportunity from day one.

### 4. To nurture your business beyond the adolescent stage, you have to plan ahead from the start. 

Even if you're prepared to get out of your _comfort_ _zone_ and relinquish some control to grow your business, where do you start? You have to start from the very beginning, back before you even opened your business.

That's because businesses that make it past adolescence and into maturity are founded on a broader perspective than most, and have planned their structures accordingly.

Successful businesses consider the future, focusing on building a business that works without being dependent on the owner always being there. That way, when it's time to grow past adolescence, they'll be able to handle the growth.

To launch a business that will make it to maturity, you need _entrepreneurial_ _perspective_. This means that you plan from the very beginning how your business will look, feel and work toward its goals.

Instead of asking "What work is necessary in the business?" ask "How will the business work as a whole?"

For example, the barista knows the technical work her cafe requires. She'll roast Guatemalan beans and serve lattes. But what will set her business apart from competitors? How will she attract customers? What sort of customer does she want? The answers to these questions demand _entrepreneurial_ _perspective_.

Then, to implement your _entrepreneurial_ _perspective_, you'll need an _entrepreneurial_ _model_.

The _entrepreneurial_ _model_ is a plan for your business that satisfies potential customers' needs in an innovative way.

Your _entrepreneurial_ _model_ will include your business's market opportunities, a clear idea of your ideal customer and exactly how your product is to be delivered.

To save her business, the barista might have to close the cafe for a few days to ponder her _entrepreneurial_ _perspective_ and _entrepreneurial_ _model_. She could decide, then, that her target customers are eco-conscious students and she'll satisfy their needs by being the first cafe to offer locally sourced milk and reading cubicles.

> _"The problem isn't your business; it never has been. The problem is you!"_

### 5. Inside, everyone has many different business personalities. 

Do you think of yourself as just one single, predictable personality? Then your business is probably going to struggle. The truth is that we're made up of a number of battling personalities. Specifically, we're each some part _entrepreneur,_ _manager_ and _technician._

One moment we're entrepreneurs creating a new product, and the next we're a technician, frustrated with the new idea we just came up with a moment ago!

Of our battling personalities, the _entrepreneur_ is the innovator, looking around and seeing a world of opportunity.

She's a high-energy dreamer and visionary. She sees all the angles, all the possibilities toward success and is intently focused on the future.

Sometimes that energy and constant opportunity-chasing creates havoc and chaos. She tries to pull people along and gets frustrated when things slow down or lag behind.

Without the _entrepreneur_, there'd be no innovation.

The _manager_ in you is pragmatic and craves order. More than opportunities, she sees problems to fix.

As the _entrepreneur_ innovates and creates new things, the _manager_ arranges things into rows, organized and orderly.

Without the _manager_, the business could never function.

Lastly, there's the _technician_, the doer and the tinkerer.

The _technician_ in you loves controlling the work flow and getting things done.

She's frustrated by the _entrepreneur's_ flakiness and need to constantly change ideas, and irked by the _manager_ 's meddling in her work flow. But she's happy when the _entrepreneur_ and the _manager_ create more work for her to do.

Without the _technician_, nothing in the business would ever get done.

Although the three personalities inside us seem to be totally at odds with each other, we must utilize the strengths of each to run a successful business. That's why the average small business owner is approximately 10 percent _entrepreneur_, 20 percent _manager_ and 70 percent _technician_.

So now you know the daunting odds stacked against you if you decide to start a company. But how can you avoid becoming one of the 800,000 failed businesses? Well, there's a revolution going on in small business, and within lies the secret of success.

### 6. There’s an ongoing revolution in small business that provides a path to success. 

Did you realize that we're in the middle of a historic revolution that will change business forever?

It's called the _turn-key_ _revolution_, because more and more, businesses are being built so that an owner could in principle give the key to their business to anyone, and that person would be able to run the business successfully.

Businesses in the _turn-key_ _revolution_ create a model that works perfectly, provides a predictable product to the customer with every purchase and can be replicated without the owner's presence.

In other words, they're franchises.

For your business to be a turn-key business, you need to have a _business_ _format_ _franchise_ : the model you give to the franchisee, the person who will run your franchise. It contains your business's processes, organizations and systems.

The success rate for franchises is amazing. Whereas 80 percent of small businesses fail in the first five years, 75 percent of _business_ _format_ _franchises_ succeed.

The _turn-key_ _revolution_ is so successful because it focuses on building businesses that anyone would want to buy.

For example, if someone wanted to buy your business, their first question would probably be, "Does it work?" If your business's systems are designed to work in the simplest and most efficient way, anyone can run the business, and thus it is appealing to buy.

In the _turn-key_ _revolution_, you're not just selling the products you make to customers. You're working to sell the whole business, including its processes and systems, to franchisees.

Ray Kroc started the _turn-key_ _revolution_ in 1952 when he became obsessed with creating a hamburger stand that would produce a precisely replicable hamburger to each customer. He reengineered the way hamburger stands worked, making everything so exact that, for example, every hamburger was flipped at precisely the same time. Kroc defined processes that anybody could follow because he saw the eventual franchisee, the person who would run the stand, as his real customer.

Kroc then sold the system of McDonald's businesses as a franchise, thousands of times over.

> _"Pretend that the business you own… is the prototype, or will be the prototype, for 5,000 more just like it!"_

### 7. Imagine your small business will one day be a national chain; now, build the very first store. 

So how do you go about making a franchise? The first thing you have to do is build a _franchise_ _prototype_, the original model of your business that will be replicated.

Your _franchise_ _prototype_ has to give people value and be so simple that it can operated by anyone.

The value your prototype gives customers is whatever they perceive it to be. Sound confusing? What it means is that the value can be anywhere: in your reasonable prices, in your amazing customer service, in a gift your customers receive in the mail, and so on.

For example, the barista's cafe value could be her impeccable lattes that come with free cookies.

Next, the way the value is delivered has to be designed in a way that is _systems-dependent_, not _expert-dependent_.

This means that you should design your systems to be so simple and efficient that your business will no longer rely on you or on technical experts.

For example, if the barista designs a flawless training program that ensures every barista in her cafe makes perfect lattes, she or other latte experts will not have to do it themselves.

In addition, the _franchise_ _prototype_ should document everything in an operations manual.

Why?

If you don't document how your business works, how will someone be able to run it without you? Therefore, you must write down every single process as part of your company's how-to guide.

The barista's cafe, then, should have manuals not just on how to make a latte, but on how to train people to make lattes.

Lastly, the _franchise_ _prototype_ also should provide predictable service, 100 percent of the time.

If people don't know what kind of product or service they're going to receive, they probably won't become regular customers. For example, people who come to the barista's cafe shouldn't get a delicious latte one day and a rancid one the next or they'll never come back again. And of course, a franchisee won't want to run a business with unpredictable results.

### 8. Start a business to satisfy your personal aim in life. 

Why did you want to start a business in the first place? Hopefully it's at least partially because you wanted something more than a 9-to-5 job in your life. So as you set up your _franchise_ _prototype_, your top priority is to ensure that your business will give you what you want!

The most important step in building your business is knowing your _primary_ _aim_, or what kind of life you want to live.

After all, how can you know what kind of business to build if you don't know what it'll help you achieve?

To know your _primary_ _aim_, ask yourself questions such as, "What do I care about most?" "How do I want to live?" "How much money do I want?" and "How much do I want to travel?"

After you know your _primary_ _aim_, you have to make a _strategic_ _objective_. That's a list of objectives that your business will have to fulfill to help you reach your _primary_ _aim_.

The _strategic_ _objective_ is also a tool for measuring progress, implementing plans and franchising your business. It's a list of standards that anyone should be able to understand.

It should include financial projections, including how much money you expect to make in gross revenue and profit.

It should also define why your business is an _opportunity_ _worth_ _pursuing_, meaning that it involves a big enough market opportunity to fulfill your financial goals and satisfy your _primary_ _aim_.

And it should define what kind of business you're in, including a description of your ideal customer.

Let's say the barista's _primary_ _aim_ is to make $500,000 a year and travel for one month every year. That means that her _strategic_ _objective_ should explain how her three cafes will each make her $167,000 a year, as well as a plan on how she can close the cafes and wind down operations for one month each year.

> _"With no clear picture of how you wish your life to be, how on earth can you begin to live it?"_

### 9. Organizational charts are crucial for your business’s growth and establishing accountability. 

If you're like most people, you hate drafting organizational charts. Boring, right? But without a clear way for everyone to know their responsibilities, how will your business succeed?

You need an _organizational_ _strategy_ to lay out exactly who in your company will do what work.

Even if you're still a one-person business, you have to plan your _organizational_ _strategy_ to know how your business will grow.

So start out by considering how many employees you'll need, and what work each one will do.

Then, for each and every position, write out a _position_ _contract_ explaining who the employee reports to, what work has to be done and by what standards the employee's work will be judged.

For example, the barista knows that her cafes will need three baristas and one baker in each, a manager to oversee them, a marketing manager, an accountant and a general manager to oversee everyone.

At the beginning, the barista will fulfill all those jobs. She'll make coffee, bake cookies, design ads for local newspapers and keep the books. But as the business grows, she'll need to know exactly how many people and for which positions she has to hire for her business to run successfully.

Plus, the barista will learn the best approaches as she works in each position. She should document them in a specific manual for each position that can be passed down to future employees.

Another advantage of a clear _organizational_ _strategy_ is establishing accountability.

Each employee will be responsible for the work his position requires, which will be clearly laid out in each position's manual and _position_ _contract_.

Each employee also must sign his _position_ _contract_, which shows he agrees to accept responsibility in accomplishing his assigned work.

At the end, when each position is filled, with employees fulfilling the standards for their positions, your business will be on its way toward satisfying its _strategic_ _objective_ and your _primary_ _aim_.

### 10. To manage your employees, don’t rely on great people – rely on a great people-management system. 

Do you think the secret to a successful management strategy is finding as many incredibly talented people as possible? Then you're wrong!

The secret to a great management strategy is implementing a _management_ _system_ that approaches your management of people as a marketing tool.

Why a marketing tool? Well, the way you treat your employees and the way you push them to do satisfactory work will end up having the biggest impact on the product your customer receives.

For example, one way the barista could manage her baker would be by telling him that he must stay in the kitchen and bake a certain amount of cookies and cakes every day. Or, alternatively, she could put him on display in the shop, front and center, and even let him choose his own ingredients. The latter option would probably result in the baker being more enthusiastic about his job and customers consequently enjoying better cookies.

But the most important part of your _management_ _system_ is a _people_ _strategy_, where you ensure your people understand the idea behind the work they're doing.

If your employees understand the meaning of the work they're doing, they're more likely to want to work to help the business reach its goals.

A third important component is that employees should always be tested against the standards you set for each position.

Let's say that one of the barista's business objectives is to prioritize creativity. When the barista hires a baker, she makes sure to connect the objective of creativity with his work. She tells him that personal creativity is a cornerstone of her cafes, so he has to push his own creative limits by designing each week's cake schedule. He will be held up against the standard of creativity: no two weeks' cake schedules should be the same.

The result? The baker will be pushed to reach his baking potential, while your customers will have more choice of cakes!

### 11. Next, forget everything and think only about the customer. 

What about marketing? What's the best way to approach your marketing strategy?

It's simple. Focusing on the customer is so vital that you should forget about everything else and only think about the customer.

First, consider your customer _demographics_. How old are your customers? Where do they live?

Then think about why your customer makes purchases. These are your _psychographics_. Why should your demographic group buy from you and not from somebody else?

For example, while the barista might not have enough money to compile a huge customer research report, she could ask each of her customers to fill out a small survey, rewarded with a free cookie at the cafe. To know her customer better, she could ask demographic questions such as age and address, and psychographic questions, such as which leisure-time activities a customer enjoys.

With this information, the barista can present her products according to her customers' profiles, and they'll be more likely to buy.

Once you understand your customers as best as you can, make your marketing as appealing as possible to them.

For example, research done by technology company IBM found that a particular shade of blue implied dependability to its customers. It then ensured that all its products were packaged or highlighted somehow by what is now known as "IBM Blue."

For your _franchise_ _prototype_ to be predictable to a potential franchisee, you have to consistently market to your customers as scientifically as possible. Scientifically, in this case, means according to data and tests that you run.

That means that once your research shows your customers have become younger, or that your marketing approach by advertising in newspapers isn't as effective anymore, you must change it. For example, you can consider buying online ads to reach younger customers.

> _"If your customer doesn't hear you, he'll pass you by."_

### 12. In the end, you’ll have a business made up of fully functional systems. 

What will your business look like after plotting out your entire _franchise_ _prototype_? It will be a complex, yet easy to run, interdependent series of all the systems and processes that make up the business, from marketing to management to organizational structure, all the way back to your _primary_ _aim_.

That's because you'll have a _systems_ _strategy_ in which everything interacts with each other, and through those interactions, everything will develop and change. Generally, your _systems_ _strategy_ will be divided into a few categories:

You'll have _hard_ _systems_, the inanimate objects that make up your business, such as computers and colors.

You'll have _soft_ _systems_, the ideas and the living things in your business, like yourself!

And you'll have _information_ _systems_, which will tell you all the data on your business so you know what's working, what's not and when it's time to change.

For example, in the barista's cafe, hard systems will include her espresso machine; soft systems will include employee attitudes; and information systems will include data on what exactly customers purchase.

To succeed, all these systems have to work together.

You can't work on any one part of the business without considering all the other parts.

At the barista's cafe, she might want to exchange the espresso machine for a newer model, a change to the hard system. To make that decision, she'll have to consider how the other systems will be affected. The soft system could be compromised if employees absolutely adore the current machine and don't want to learn how to operate a new one. Then the information system will have to monitor customer behavior to ensure the new machine isn't making subpar lattes and hurting sales.

If the systems can't run smoothly together, the business doesn't have a chance!

### 13. This process of planning and implementing never stops. 

If you want to ensure your business is successful, you can never rest on your laurels. You have to constantly be working on your prototype, tweaking its systems and confirming that it's running as best as it can. That continual tweaking, testing and energy is called the _business_ _development_ _process_.

The first step in the _business_ _development_ _process_ is _innovation_.

_Innovation_ is simply doing new things. The key to successful business innovation, though, isn't innovating your product so much as it is innovating your business.

Ask yourself the question, "What's the best way to do this?"

Remember, Ray Kroc didn't innovate hamburgers — he innovated how hamburgers were made and sold.

The second step in the _business_ _development_ _process_ is _quantification_.

_Quantification_ is simply measuring everything. Every single thing.

How can you know what's working or what's not without measuring your innovation's impact?

For example, how will the barista know that her _management_ _system_ of fostering creativity in the baker is working unless she tracks how many pieces of cake she sells?

The final step of the _business_ _development_ _process_ is _orchestration._

_Orchestration_ is putting innovation into practice. It's taking your idea of how your business should work and having it play out between employees and customers, then watching what happens.

It's a constant process, based on your efforts of _innovation_ and _quantification_.

That means, if wearing a blue suit increases your sales, keep wearing a blue suit! But if _quantification_ finds that losing the suit and wearing shirtsleeves helps performance, then switch to shirtsleeves.

The key point is that _innovation_, _quantification_, and _orchestration_ aren't necessarily chronological. They don't happen one after the other, but rather all together, all the time.

The _business_ _development_ _process_ never ends, because your business will constantly be creating new innovations, orchestrating them into real life and measuring the results. It's a process that will drive your small business to success.

> _"If you haven't orchestrated it, you don't own it!"_

### 14. Final summary 

The key message in this book:

**Most** **small** **businesses** **fail,** **but** **if** **from** **day** **one** **you** **build** **your** **business** **as** **a** **franchise** **prototype** **so** **that** **anybody** **can** **take** **the** **reins,** **your** **chances** **of** **success** **increase** **dramatically.** **The** **key** **is** **to** **go** **to** **work** **_on_** **your** **business,** **not** **_in_** **your** **business.**

Actionable advice:

**Define** **your** **primary** **aim.**

Before starting your business, ask yourself, "What kind of life do I want to live?" How much money do you want, and how much do you want to work? These are key questions for which you need the answers before you start, as you should set up your business in a way to help you attain your goals.
---

### Michael E. Gerber

Michael E. Gerber is an entrepreneurial legend. He is the co-founder and chairman of Michael E. Gerber Companies, an organization that helps businesses and entrepreneurs succeed regardless of industry. He has written more than a dozen books on entrepreneurship.

