---
id: 597bce5eb238e100050843f7
slug: the-economic-singularity-en
published_date: 2017-08-02T00:00:00.000+00:00
author: Calum Chace
title: The Economic Singularity
subtitle: Artificial Intelligence and the Death of Capitalism
main_color: CF2946
text_color: CF2946
---

# The Economic Singularity

_Artificial Intelligence and the Death of Capitalism_

**Calum Chace**

_The Economic Singularity_ (2016) takes a long, hard look at what the future has in store for us based on the technological progress we've made so far. It's clear that we're moving toward the kind of artificial intelligence that will automate most of our jobs — but how do we plan to deal with this scenario? Find out the challenges we'll face and what we need to do to prepare ourselves for the inevitable.

---
### 1. What’s in it for me? Learn what AI has in store for you. 

For decades, human beings have been worried that machines might take over the world — and these worries are by no means unfounded. It's entirely imaginable today that robots with all manner of human traits could be developed, capable of displaying empathy, speech cognition and a high level of intelligence.

If the robots are compliant, this could be very practical, such as for the many single people in the world who need a steady and reliable partner to live with. But the robots could also decide to take over and make humans into slaves.

Leaving aside science fiction, technology and machines have thus far not wreaked havoc on Earth; on the contrary, mechanization has meant that human beings have to work less and less, leaving them to be creative, productive or just lazy. This tendency is bound to continue as technology progresses, so let's consider the positive and negative aspects that this increase in AI has in store for us.

In these blinks, you will learn

  * why there are so many more consultants than carpenters these days;

  * how technology is evolving to master complex human skills; and

  * why you may be getting a paycheck without working in the near future.

### 2. The Industrial Revolution set the stage for the Information Revolution. 

When machines and factories began to appear in nineteenth-century Victorian England, many people were critical of this new development. Charles Dickens, in particular, worried about the effect machinery would have on England's working-class laborers, and expressed these concerns in his books.

Even then, people were worried about losing their jobs to new technology. But as mass production took over during the Industrial Revolution, there was no stopping the progress made.

Actually, it wasn't the machines that brought about this change so much as the steam engines that were powering them.

People were already familiar with machines powered by wind and water, as these had been around since medieval times. What really fueled the Industrial Revolution was the fact that new machines along assembly lines were harnessing the power of Thomas Newcomen's steam engine.

Invented in 1712, the steam engine accelerated the onset of the industrial age and was far more powerful than any of the water- or air-powered contraptions that preceded it.

With machines taking over a large portion of the labor, a significant percentage of the population could now focus on other things — and it was this extra time that would lead to the Information Revolution.

As time went on, machines became increasingly efficient and less manual work was needed on the assembly line. As a result, workers turned their attention to the service industry.

Prior to 1940, only half of America's gross domestic product (GDP) came from the service industry, but by 1950, this industry was employing over half of the nation's workforce.

This shift, from industry and agriculture to service, marked the start of the Information Revolution, an era that would focus on producing knowledge and information rather than raw materials.

Since the Information Revolution is still ongoing, we can't be certain of its overall consequences; in fact, it remains difficult to speculate as to what our workforce will look like in the future.

But before looking ahead, let's first take a closer look at how we got to where we are today.

### 3. History shows that machines increase the wealth of nations and do not threaten human employment. 

Are you familiar with the term Luddite? You might be if you prefer to live a life free of technology. But do you know the origins of the name?

As the story goes, in 1779, a textile worker by the name of Ned Ludd revolted after being whipped by his employer. For Ned, it was the final straw, and he smashed his new mechanical knitting machine in protest.

The story lingered in the public imagination, and when textile workers in nineteenth-century England faced further mechanization of their profession, they united in protest over the threat to their livelihood, calling themselves Luddites. And much like the Ned Ludd story, the workers smashed the new weaving machines in defiance.

But is technology really to blame? When we look at the evidence, we can see that machinery generally increases a nation's wealth while the resilient job market adapts.

Since machines don't need to sleep and don't require wages or benefits, they make production cheaper and faster. And with more products being made, a higher demand can be met, which results in more wealth and more jobs in the long run.

So it's shortsighted to assume that automation automatically leads to unemployment. After all, there isn't a fixed amount of jobs to be had or work to do.

Thus far, the general rule has been that when societies and companies innovate and become more prosperous, new jobs are created.

Today, more machines than ever are being used. And at the same time, humans are working as much as we ever have, because machines allow us to be more productive and accomplish new and exciting tasks.

In 2015, the financial consulting firm, Deloitte, studied employment trends dating back to 1871 and found that machines have indeed generated more jobs — primarily due to the automation of the agriculture industry and laundry-related professions.

Workers in these sectors were suddenly freed up to fill the jobs of the expanding service industry, including teachers and nurses. Between 1992 and 2014, the number of teaching and nursing jobs in the United States increased by six and nine percent, respectively.

### 4. Since machines are learning to make cognitive decisions, service industry jobs are now at risk. 

So far, machines have done more good than harm to the job market, so you might think that improving machines through artificial intelligence (AI) would make things even better, right? Not so fast.

As machines get smarter and smarter, in large part thanks to AI developments such as _deep learning_, it means they'll eventually be able to replace the very jobs in the service sector that we've come to rely on.

Deep learning is a process wherein a machine is fed different data sets and uses algorithms to make predictions and draw future conclusions based on the results of those predictions. So, in a sense, the machine is learning. And the more data the machine is given, the more comparisons the machine can make and the more intelligent it becomes.

This is how machines are learning to perfect a range of human skills like speech and image recognition.

In 2012, computer scientist and cognitive psychologist Geoff Hinton led a team to victory in an AI image recognition contest. Through deep learning, their computer recognized the most images, including clouds, cats and dogs.

This is the kind of knowledge that will enable machines to make informed decisions and perform human tasks, making them much cheaper and efficient workers than humans. And some jobs in the service sector are already at risk of becoming automated.

The software program, Quill, is automating some of the more basic jobs in the journalism sector by writing Associated Press articles on sports and financial news.

Developed in 2010 by Narrative Science, Quill can analyze data and pick out the most relevant details for an article. It can then quickly generate a structure for the article and use its language-generation software to compose the necessary sentences.

This shows us that even complex jobs requiring cognitive analysis can either be partly or completely performed by machines. And once one company starts using them, other companies will be forced to follow their lead or else fall behind their competition.

### 5. In the near future, intelligent machines and robots will make many service jobs obsolete. 

Once a new technology becomes readily available, rapid change inevitably follows. Think back to how quickly people switched to digital cameras, or how typewriters became obsolete virtually overnight.

With constant advancements being made in AI, we're guaranteed to see more rapid change happen in the near future, such as the proliferation of self-driving cars.

A renowned competition for self-driving cars in the United States is the DARPA Grand Challenge, and in 2004, the winner was Sandstorm, a converted Humvee that only made it seven miles before getting stuck on a rock.

But a lot has changed since then: the self-driving cars that Google has designed for Lexus and Chrysler have already logged over a million miles on California roads — and there hasn't been a significant setback yet.

Experts are predicting that public roads will be taken over by self-driving cars by the year 2041.

This means that driving tests will become obsolete, as will most chauffeur and commercial driving jobs. With fewer accidents on the road, there will be a drastically reduced demand for mechanics and hardly any need for car insurance.

So when can you expect to get your first self-driving car? Chris Urmson, the head of Google's automated driving project predicts that the general public should have the option of driving hands-free by 2020.

Another industry that's especially vulnerable to automation is the healthcare sector.

In fact, the first caregiving robot is already being used in Japan. Both the robot and its parent company are called PARO, and the robot itself looks like a small seal, complete with fur and big eyes.

Animal therapy has become quite popular recently, as patients exposed to cuddly creatures get some much-needed cheer to reduce stress and lower blood pressure. And PARO allows more patients to experience these benefits by reaching those who don't have access to a real animal.

The author believes that by 2041, we'll all have access to devices that will constantly monitor our vital statistics. As a result, we'll see a dramatic decline in the demand for doctors and nurses, as their basic functions will be automated.

> _"The future generally turns out to be not only different to what we expect, but also much stranger."_

### 6. Technology will lead to economic contraction and may widen the divide between social classes. 

There are going to be a number of challenges facing our society in the future, not the least of which will be the economic impact that will accompany a surge in joblessness.

When large numbers of people suddenly become unemployed, there will be a subsequent period of economic contraction that could bring society to a grinding halt.

When we put the AI technology to use, productivity will increase and the prices of goods and services will drop so that people continue buying.

But the initial positive effect of these changes will eventually level off, and will be replaced by an economic contraction caused by so many unemployed people.

We've seen this happen in Greece, where 46.5 percent of the country's young people are unemployed.

While family members are able to provide a temporary safety net for relatives in situations like Greece's, there's no precedent for the sprawling unemployment that will happen in the future. It will cut across all ages, classes and demographics, so there's no safety net big enough to prevent the economy from deteriorating under such stress.

Another issue is the disparity between classes, which could lead to some people not being able to afford access to certain technologies and getting left behind.

While technology changes quickly, there's still a lengthy period before new products become affordable to the general public.

So, let's say implants emerge that allow wealthy people to enhance their bodies and minds far beyond what's possible today. This could make the social divide even worse than it is today by making the lives of the wealthy so drastically different that their access to technology makes them "superior" beings.

We'll need to work together if we're going to overcome future challenges, and in the final blink we'll look at how we might just persevere.

### 7. Universal basic income could be the solution, although joblessness will come with psychological consequences as well. 

How would you feel about making money and not having to work? It might sound like a dream, but it could soon be a reality for many of us.

This concept is known as _universal basic income_ and it will be a necessity in a future where most jobs can be automated.

After all, we'll still need an income, even when sophisticated AI is taking care of the tasks that keep services functioning.

A universal basic income allows a fixed amount to be distributed to all citizens, either unconditionally or through what's known as a _negative income tax_, a method that works by providing a certain amount of money to help you reach a set figure.

This model of basic income welfare has already been tested in some areas, with the biggest experiment taking place during the 1970s, in the Canadian town of Dauphin.

The Canadian government determined that every family would receive a minimum amount of money each year, with no strings attached. Adjusted for inflation, they decided this amount would be $16,000 Canadian. So if a family's normal income dropped below that amount, the government would cover the difference.

Between 1974 and 1976, 10,000 citizens benefitted from the program, and despite worries that it would lead to voluntary unemployment, only mothers with newborns and teenagers quit working altogether.

Universal basic income should allow for more artistic endeavors, but it could also lead to depression.

A job is what gives meaning to many people's lives, and we can safely predict that around ten percent of the population will suffer psychologically from being unemployed. They'll either sink into a depression or escape into the world of virtual reality entertainment.

But the majority of the population will find healthy endeavors to bring joy to their lives, such as writing, playing sports or discovering other creative and artistic pursuits.

If we're going to make this happen, we'll need to be prepared, with strategies and structures in place to allow for a smooth economic transition and to avoid some of the biggest dangers we're sure to face.

### 8. Final summary 

The key message in this book:

**Unlike previous changes in society, the information revolution will end up having a major impact on employment, making many of the jobs we perform today largely unnecessary. In order to prevent a major catastrophe, we need to plan ahead for this inevitability and find a way to organize a successful society that will continue to function in the absence of work as we know it today.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Singularity Is Near_** **by Ray Kurzweil**

_The Singularity Is Near_ (2005) shows how evolution is drawing ever closer to a dramatic new phase, in that by 2029, computers will be smarter than humans, and not just in terms of logic and math. This event will not only profoundly change how we live but also pose serious questions about humanity's future.
---

### Calum Chace

Calum Chase is a futurist and a speaker, with a lot to say on the state of artificial intelligence (AI) in modern society. After three decades of working as a successful businessman, he's written multiple books on AI, including the novel _Pandora's Brain_ and the nonfiction book _Surviving AI_.

