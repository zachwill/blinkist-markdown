---
id: 56e0315e26ea840007000054
slug: the-economy-of-you-en
published_date: 2016-03-10T00:00:00.000+00:00
author: Kimberly Palmer
title: The Economy of You
subtitle: Discover Your Inner Entrepreneur and Recession-Proof Your Life
main_color: FED633
text_color: 806B19
---

# The Economy of You

_Discover Your Inner Entrepreneur and Recession-Proof Your Life_

**Kimberly Palmer**

_The Economy Of You_ (2014) is your guide to establishing and maintaining additional sources of income. From seeking out opportunities and developing networks to finding your audience and facing new challenges, these blinks provide you with the tools you need to do what you love without leaving your full-time job.

---
### 1. What’s in it for me? Awaken your inner entrepreneur and earn some extra money. 

The 2007-2008 financial crisis had a huge impact on the US economy, leaving many Americans in dire straits and struggling to make ends meet. Although the worst part of the crisis might have passed, the economy is still shaky. 

Luckily, there are measures you can take to protect yourself and secure an income if the going gets tough. With a little creativity, you can raise your chances of finding a second job or turn your skills into a thriving side-gig. There's nothing wrong with a little moonlighting — it may even change your life for the better.

In these blinks, you'll learn

  * how to turn your skills into money;

  * how to surf your way to a new job; and

  * how (and why) you can still pick your friends.

### 2. Going through financial struggles isn’t the only incentive for finding a second job. 

It seems like all we ever hear about is how volatile the global economy is. Since the 2007 financial crisis, many individuals have taken extra steps to protect themselves. Once thought of as a last resort, working two jobs is becoming a norm today.

A 2012 study conducted by the University of Michigan revealed that as many as 50 percent of participants described themselves as _financially worse off_ compared to their situation five years ago. According to the US Bureau of Labor Statistics, 5 percent of the working population in the United States already has at least two jobs. What's the relationship between these two pieces of information?

Well, it's easy enough to assume that people get a second job _because_ of financial struggles, be it a growing debt, increasing rent prices or mounting medical bills. But while this is logical, it's not the whole story. 

Take Joe Cain, for instance. A former cop, Cain built the website sidegig.com, a platform where police officers and firefighters could offer additional services, such as home repair work or legal consultation. He didn't take on his website project out of financial desperation. Rather, Cain was determined to gain new skills and opportunities.

Cain was able to become a certified tax specialist by taking classes while still working on the police force. He had success offering his accounting services to other people, and decided to continue focusing on tax consultation. Not only did this job pay better, it also afforded Cain more time with his two children. But not wanting to stop there, Cain also invested time into sidegig.com to secure yet another stream of income. 

Cain's story shows how motivations for finding additional streams of income can be manifold. A need to earn more money often goes alongside a desire for changes in your professional and personal life.

### 3. Start by gathering inspiration for your new job and a new weekly schedule. 

If you remember how tough it was starting your current job, finding a new one might seem pretty daunting! That's because we often have no clue where to start. So at this stage, it's best to keep things simple and ask yourself _what you're good at_. 

Take the time to reflect on yourself as an employee by listing the skills you've gained in your current job and how they might be useful in other professions. For example, if you've learned great management skills at your hospitality job, you might enjoy taking on coaching as a second job, where you assist clients in organization and planning. 

With this list nearby, it's time to hit the web. The internet is full of creative, resourceful individuals selling goods and services independently with the aid of online platforms. Take Etsy.com, which allows crafters to buy and sell their handmade items. If you can see yourself designing your own jewelry, Etsy should be your first stop. 

Elance.com is another site where you can put your graphic design, marketing or copywriting skills up for offer. If you've got an eye for detail or a way with words, this platform might suit you best. No matter which route you take, there's one thing you have to keep in mind: time management!

Because time is money. And if you're already working full-time, you have don't have much of it to spend generating additional income. So you've got to make the time. But how?

Early mornings are a great window of time to get stuff done before your workday begins. If you have downtime, such as waiting for a bus, prepare yourself so that you can use it productively. Smartphones make this very easy: you can, e.g., read through job ads the next time you've got some waiting to do. 

We can also find time for our second job by cutting back on unnecessary things. By reducing time you spend on Facebook or watching TV, you'll free up whole hours in your week. If you're feeling inspired about potential second jobs and motivated to start using your week more productively, it's time to progress to the next step.

> _"One of the richest sources of side-gig ideas, in fact, might be your day job."_

### 4. Seek out a strong network of friends online to help grow your business. 

A great network of friends is something we should all aspire to have. Their support often comes in handy when facing challenges, such as relationship and family troubles. It's also great for starting new business endeavors. 

As you start to find your footing in your second job, you'll need a network to show you the ropes, and connect you with customers and other businesses. So if you don't have any friends in your new field, it's time to make some!

The internet is a great place to find like-minded people doing the work you'd like to do to. Start by seeking out individuals blogging on topics that interest you. If you're planning on developing a side job as a writer, you could seek out people blogging about creative writing. Then, start actively engaging with their content: share your thoughts in the comments section and respond to their Facebook and Twitter posts. 

After doing this for a while, get in touch with the author. Don't just think about what their friendship could offer you; consider how your skills could benefit him or her, too, and you'll be off to a great start. At the same time, think about how you can make it possible for other people to find you. Having an online presence, such as a blog or an online shop, is a great way to put yourself out there for others to find. 

With a network established, it's time to win the attention of your target audience. Making guest appearances on different blogs is one great way to reach out to new potential clients. If you want to start selling your handcrafted jewelry on Etsy, you could contact bloggers writing about design and art, sure, but you could also get in touch with bloggers writing about the materials you use or even about entrepreneurship in general! Don't be afraid to ask.

### 5. Failure is natural, so focus on bouncing back well. 

Whether it's a disappointed customer, a disastrous workshop or a declined application, failure can make you question your ability to maintain additional streams of income. Failure is a perfectly normal part of life — it happens to the best of us! If you find yourself at a dead end, don't despair. What matters most is how you bounce back. 

There are three steps to making the most out of failure. First, _learn_ from your mistakes. The internet is your friend here. Seek out stories of failure similar to yours and see how other people have grown from them. This will console you and remind you that you're not the only one who's gotten lost along the way! 

Next, put your plan B into action. If your current project doesn't seem to be working out, what else could you do? Get your creative juices going and start brainstorming alternative endeavors. This will reassure you that your project isn't the be-all and end-all. If you came up with one great idea, you can definitely think of another. 

Finally, keep your failure in perspective. We tend to overestimate the impact of our negative experiences. But, in reality, we often forget about traumatic experiences before we know it.

### 6. Find fulfillment in your second job by keeping karma in mind and avoiding too much pressure. 

Do you believe that "what goes around, comes around"? While ideas of karma may seem a little superstitious, any successful businessperson can tell you that they do apply. 

In previous blinks, we found out that having a network of supportive friends can be great for getting your second job up and running. If you want to maintain these friendships, you'll need to be a good friend. Sounds obvious enough, but it's easy to forget in the early stages of your business when you have a lot to lose. That's why you should be willing to make sacrifices and help others out when they ask. It's worth it, because they'll be more likely to get your back in the future. 

Say you're crowdfunding for a new project. You can't hope to promote the project alone: you need people to share it with their friends. The people who are going to help you out here will without a doubt be those you've shown kindness to in the past. 

Aside from karma, there's one last thing to keep in mind: _there's no pressure_. Most of us aren't planning to leave our full-time jobs to dedicate our lives to our second source of income. You don't have to turn your second job into your dream career. Thinking this way will just turn your second jobs into sources of stress.

Placing too much emphasis on earning money will also suck the fun out of your second job. Financial security is important, of course. But so is making a difference in the lives of others, and doing something that you find fulfilling. 

Businesswoman Febe Hernandez started a jewelry brand to create a second source of income. Her products were a hit, so she decided to hire young people from the Bronx to help her expand. Though she could have scaled up her business more effectively with skilled workers, giving this opportunity to young people in need was motivating and fulfilling for Hernandez. 

By being kind to others and to yourself, you'll find it far easier to maintain your additional sources of income, and they'll become sources of meaning and happiness in your life, too.

> _"The secret to excelling in both a full-time job and a side-gig is often to find a way to combine them, in a completely above-board, ethical way."_

### 7. Final summary 

The key message in this book:

**If you're seeking greater financial security or just eager for new opportunities, having a second job could be the right move for you. While you maintain your full-time job, you can use online resources and networks to grow your own side business. With some clever time management and a good attitude, you'll be on your way to a more fulfilling work life.**

Actionable advice: 

**Make your day work for you.**

Next time you feel frustrated at yourself for not completing the work you had planned to do in the afternoon, remember that energy levels fluctuate over the day. If you've noticed that you're quite tired at certain times, but have boundless energy earlier or later in the day, then adjust your daily schedule to take advantage of this changing rhythm. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Startup of You_** **by Reid Hoffman and Ben Casnocha**

_The Start-Up of You_ is a guide to how you can leverage strategies used by start-ups in your own career: being adaptable, building relationships and pursuing breakout opportunities.

In a world where entire industries are being ravaged by global competition and traditional career paths are fast becoming dead-ends, everyone needs to hustle like an entrepreneur.
---

### Kimberly Palmer

Kimberly Palmer is a blogger for _U.S. News & World Report_ whose expertise in money management has made her a sought-after guest on NBC, CNBC, CNN and other networks. Palmer has written articles for newspapers including _The Washington Post_ and _The Wall Street Journal_.

