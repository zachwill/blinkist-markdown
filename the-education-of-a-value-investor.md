---
id: 546325c46634660008000000
slug: the-education-of-a-value-investor-en
published_date: 2014-11-13T00:00:00.000+00:00
author: Guy Spier
title: The Education of a Value Investor
subtitle: My Transformative Quest for Wealth, Wisdom and Enlightenment
main_color: D52B33
text_color: D52B33
---

# The Education of a Value Investor

_My Transformative Quest for Wealth, Wisdom and Enlightenment_

**Guy Spier**

In _The Education of a Value Investor_ (2014), Guy Spier recounts his transformation from greedy hedge-fund manager on Wall Street to a successful _value investor_. Sharing the incredible story of his career and the wisdom he acquired along the way, Spier has some surprising insights concerning, what he sees as a false choice between leading an ethical life and a financially successful one. With great admiration, Spier also names the people who were most influential to his professional life, explaining the specific effect each of them had on his mindset and career.

---
### 1. What’s in it for me? Find out how it’s possible to work in high finance without compromising your ethics. 

If you had to put a price on it, how much would you pay to change your outlook, and your life, forever?

Guy Spier's price was $650,100. And that was just to have _lunch_ with business magnate and philanthropist Warren Buffett, a man he believed could provide him with the tools to transform himself from a ruthless and dissatisfied hedge-fund investor into a happy, grounded _value investor_.

Luckily for Spier, his instincts were right, and he left what must've been the most expensive meal of his life as a changed man.

Educated at Oxford University, Spier became a smart and ambitious hustler in the world of finance, and set off on what appeared to be a successful though unremarkable career.

The only problem was that he was intensely unhappy. He'd ended up spending most of this time in a working environment he absolutely loathed, leading him to compromise his ethics and become — what he calls — an inauthentic person.

What he did next amazed and surprised his colleagues and former school mates: he discovered a completely different business mentality for himself, which involved him breaking with many of the existing conventions of the finance world.

In these blinks, you'll find out

  * why following your own business ethics always leads to happiness;

  * how sending thank-you cards to colleagues can transform your career;

  * why elite students often lack an essential ingredient for success; and

  * why it's better for your life and career to stop being a taker and start being a giver.

### 2. An elite education can be an obstacle to dealing with problems in the real world. 

For all the prestige of an elite education, this kind of schooling often teaches skills that simply cannot be applied in the real world.

For example, highly educated people from elite business schools didn't see the recent 2007-2008 financial crisis coming. Why? Because there are some fatal flaws in the way that these institutions teach their students.

Elite business schools and universities help students to develop specific technical skills based on certain theories. Those theories, however, neglect to take the real world into account, and therefore do not apply to the actual business world.

Theories and economic models are created under the assumption that one will have the perfect amount of information. In reality, things aren't nearly as neat.

Take the price of ham. In theory, it's possible to work out how much ham should cost. You could look at how many stores sell it, how many pigs are slaughtered, what the demand is and then work out the ideal price.

But in no way will this price be matched in reality. There are just too many other factors that must be taken into consideration. For instance, stores in certain locations, such as train stations, will make their ham more expensive; and other stores will often have offers, making their ham cheaper.

Since it's _impossible_ to have all of this info at hand in the real world, the theories taught in elite schools never quite match the reality.

Elite education focuses too narrowly on rationality, neglecting the power of human instinct and critical thinking. Rational thinking is revered, while unconventional ways of thinking are condemned.

So if in your studies you come up with an idea or argument that differs radically from the norm, it's likely you'll keep it to yourself, as sharing it may result in you being accused of lunacy.

Additionally, if you happen to be a very rational thinker, an elite institution can make you feel sublimely intelligent. Many people with a first-class education think they know it all, and when they enter the world of work they ignore the sage advice of more experienced people, because they consider them to be inferior.

> _"...One thing I came to realize was that my ivory tower education had left me dangerously exposed and vulnerable."_

### 3. Some businesses can force their employees to challenge their ethics and morals. 

You're just out of college and have recently joined a big investment bank. Naturally, you want to make a good impression.

Yet, in the first week, you discover that the bank often makes money by cheating clients out of theirs.

What should you do? Should you stay and continue to profit from their dishonest methods, or leave with a clear conscience? This dilemma is one that many employees are forced to confront, as many companies — especially those on Wall Street — use dubious strategies to maximize their profits. In order to attain more clients and wealth, some investing firms have low moral standards and aim to exploit clients with their aggressive salesmanship.

Take the brokerage firm D. H. Blair & Co. In order to dupe their clients, this company made investment deals appear more profitable than they actually were.

Moreover, the managers of such companies expect all of their employees to play along with the shenanigans. For new employees in particular, this can present a tough ethical dilemma. But it's not only the expectations of others that can cause you to forget about your own ethics. It's also your intrinsic motivation to succeed. The pressure of competing with others and the will to succeed might give you a reason to stretch your moral boundaries.

Imagine you're the only one in your investment firm who hasn't made a deal yet, but you suspect that every one of your colleagues is using dubious methods to accomplish their goals. If you're like most people, you won't want to be left behind. So, it's likely that you'll wonder whether you should copy the behavior of your dishonest colleagues and deliberately mislead your clients.

Many fall into this trap and end up making the mistake of compromising their own standards.

Yet, for those who want to overcome them, such mistakes aren't the end of the world. Once you recognize that you've made a mistake, it's an opportunity for you to learn from it and begin anew.

It's not easy to recognize moral fraud when it's happening around you, and it's even harder to get away from it. Nevertheless, it's imperative that you do. In the following blinks, you'll learn how to build a career based on sound morals.

> _"We like to think that we change our environment, but the truth is that it changes us."_

### 4. Warren Buffett’s value-investing philosophy can help you avoid unethical behavior when investing. 

Have you ever read a book that changed your life?

When the author became disillusioned in his job at D. H. Blair, he read Ben Graham's _The Intelligent Investor_ and Roger Lowenstein's _Buffett: The Making of an American Capitalist_. What he read in those pages forever changed the way he would approach his job.

Warren Buffett's _value-investing philosophy_ was an especially strong influence, as this philosophy can help one to succeed in financial business without compromising one's ethics.

How?

Investors are often driven by the whims of the market. Thus, they focus their sights on short-term, risky investments in companies that are trending. The value-investing philosophy, in contrast, focuses on the _long-term_ development and potential of a firm.

The philosophy views a company share not as a piece of paper to be traded for a quick buck, but as a stake in a real, flesh-and-bones company. In other words, it takes a detailed look at the business itself, looking at not only its current value, but its future potential too.

The ethics of such an approach are sound: because one is always looking at the long-term potential, there is no pressure to deceive buyers to make a quick sale. This also happens to be the far safer approach, as there is less risk involved.

As the author's use of Buffett's philosophy demonstrates, finding a good role model is important in business. It can be very hard to change your approach if you don't know how to achieve that goal. You might end up feeling quite lost and helpless. That's when a role model can help lead the way.

Simply imitating the behavior of your role model — known as _matching and mirroring_ — and trying to understand the motivation behind it, can make you more successful.

For example, from Buffett, the author learned the best way to achieve success, using ethics, personal experience and hard work. Because he observed that this was the very opposite of what was happening at D. H. Blair, he was empowered to leave the company.

> _"This might sound peculiar, but the ability to mimic is one of the most powerful ways in which humans advance."_

### 5. If you don’t want to be constrained by traditional practices, develop a positive attitude and go your own way. 

Why is it that when we start a new project, it doesn't take long before we're tempted to give up? The simple answer is that we often have the wrong attitude towards it. The only way to succeed when we're embarking on a new endeavor is to develop a _positive attitude_.

The most effective way to achieve this is to develop a more practical way of thinking: instead of just sitting back and waiting for your new project to magically grow from a seed into a mighty oak, you simply have to go for it and try out your idea.

Moreover, you have to be aware of your emotions and personal shortcomings.

For example, after experiencing initial success as a value-investor, the author noticed that he was still driven by envy. So he changed his intrinsic motivation and stopped comparing himself to others who used short-term-oriented methods, or even dubious ones.

Once you've developed the right mindset, you need to follow your own path, not those established by traditional business practices. Often, the conventional, and very common practices might not be ideal for _your_ business.

It's certainly not easy to find your own way of doing business, but one effective approach is to prioritize ethics over profit when deciding how to represent yourself.

Investors usually have multiple portfolios, one for each type of client, so they can hide losses and make their businesses look better than they are.

In contrast, the author decided to focus on a single portfolio. He wanted to emphasize his credibility, establish trust and keep an overview of his investments.

Although others thought this was a foolish idea, it actually worked, and helped the author to develop long-lasting relationships with customers and investors.

> _"...When you begin to change yourself internally, the world around you responds."_

### 6. Growing a social network can help to establish a strong foundation for a new business. 

Which of the following approaches would you think is more effective? Building a social network made up of only strictly professional business contacts, or one that includes friendship, care and mutual favors?

If you answered the latter, you're right. In fact, being authentic to each person you interact with and expressing your gratitude to those you work with will make others interested in your work. That means viewing and treating each contact as someone you can learn from, and expressing that relationship to him or her. By doing so, you will rapidly grow your social network.

When the author started as a value investor, he wrote many "thank you" cards to people who'd helped him to develop professionally — to those who gave an inspiring speech, for example, or who did him a personal favor. After a while, people began to write back, inviting him to attend networking events and conferences.

Eventually, the people you treat in this way will remember your name, become part of your social network and be there for you when you need their help.

Being authentic to your clients through honesty and helpfulness will make them interested in you and your business. In his book, _Power vs. Force: The Hidden Determinants of Human Behavior_, David Hawkins explains that this kind of upfront honesty makes it easier for you to influence others, simply because, as a result, they'll trust in you and your decisions.

The upshot of all this is that you'll develop _personal goodwill_.

You'll begin to put relationships before money, start helping others and want to give something back to society. And, as you change internally, so will your business mentality — a precondition to becoming a successful value investor.

In fact, once you start doing small favors for people, you'll start to notice other small things you can improve in your life and business, and thus gradually develop at a micro level.

And these small acts can gather momentum, building to the point that you can do big favors for others, with little regard for your own personal reward.

Warren Buffett, for example, pledged most of his shares to the _Bill and Melinda Gates Foundation_ and is now working virtually for free, demonstrating that he cares very little about his personal wealth, and more about his social responsibility.

> _"As I looked for more opportunities to thank people, I found that I truly did become more thankful."_

### 7. Even a financial crisis can be a once-in-a-lifetime chance for a value investor. 

Remember the financial crisis of 2007-2008, when panic and desperation spread through the financial world?

The unpredictability of financial crises is, of course, rather terrifying. But value investors can depend on their business philosophy when everyone else is panicking, and even generate future profits.

Even in a financial crisis, value investors stay calmer than others. That's because they situate themselves in an intellectual environment that doesn't rattle their nerves, or lead them to compromise their ethical standards. Being surrounded by panicking traditional investors, on the other hand, can cause one to act irrationally and unethically, and to distrust one's investments.

But, because value investors are long-term oriented and are quicker to avoid risky businesses than others, their hedge funds — enterprises for investing huge amounts of money — are more stable.

The author, for example, avoided investing in any risky businesses connected to the financial market or the "tech bubble," so when the financial crisis hit in 2008, even though he lost money, his portfolio wasn't completely devastated like those of most other investors.

In fact, keeping calm during such a crisis can help you to make the most promising investments for the future.

During a crisis, many businesses are sold for much less than their intrinsic value because fearful investors sometimes lose their nerve and sell them lock, stock and barrel, for an incredibly low price.

This is a great opportunity for a value investor to swoop in and buy shares of a company promising high profits in the future for a very low price.

However, being a value investor also requires that you maintain a belief and commitment in your own investments, and not immediately choose to sell them if their value begins to drop.

### 8. To become a successful value investor, you have to create the most efficient working environment for yourself. 

How many times during your working day do you stop to check your email, or browse a favorite website?

Of course, getting distracted while at work is very common. But why is this especially true of those working in finance?

Our brains simply aren't made for dealing with the intensity of the financial world. When we deal with money, the most instinctive part of our brain — the neocortex — is activated. For us, money is like food or sex — an _essential_.

So, when we make financial decisions, we're likely to be overwhelmed by mood swings or irrational optimism or pessimism, which causes us to act quickly and, often, unwisely.

At the height of the recent financial crisis, for example, the author's employee decided to withdraw all his money from the author's hedge fund, even though he followed the value-investing philosophy and knew that his money was comparably safe.

Moreover, our willpower is a limited resource, so we can't resist distractions indefinitely. If we constantly have to use our willpower to avoid doing instinctive things, its power will diminish until we take the time to "recharge."

This was demonstrated in a study by psychologist Roy Baumeister, in which he showed that the effort required to resist eating chocolate chip cookies wore out people's willpower, making them susceptible to mistakes.

Of course, in order to better handle uncertainties, you have to become aware of your own shortcomings and irrationalities. But the easiest way to deal with such temptations is to get a distance from them.

This necessitates creating a stable working environment: choosing an appropriate city, designing your office in a way that suits your working habits, and developing daily routines to keep you focused on doing what you should.

Take the author, for example. To get away from all the hustle of New York, he moved to Zürich and rented an office far from the attractive and distracting city center. That lifestyle helped him focus on his job, enabling him to make consistently sound financial decisions.

> _"The truth is that it's hard to invest well if your non-investing life is out of whack, in chaos, or stunted."_

### 9. Create the right tools to deal with complexity and uncertainty. 

As we've already seen, the human mind is often irrational. It's good practice, therefore, to develop some tools to help you manage your mind.

Once you've created the ideal work environment for yourself, you have to develop _investing tools_ — a set of rules and routines. These will help you to bring order and predictability to your working life.

In fact, these rules and routines will reduce the complexity of your decision-making process, and prevent you from being tempted by irrational behavior.

The author recognized that when people wanted to sell him shares, or when he talked to the management of a company, he was easily influenced and manipulated by their reasoning. So he developed the habit of not buying anything that was advertised to him. Following this code enabled him to focus on only using his own, strong business instincts when making such purchasing decisions. He would only buy something when he sensed it was really worth it.

One example of such a tool is the checklist. Making a checklist can help you to avoid making obvious and otherwise predictable mistakes.

Especially when dealing with money, the human brain can become intoxicated. This is what the author calls _cocaine brain_ : the brain gets stimulated and its reward circuits are activated. The result is that we often make small errors chasing immediate rewards, no matter how many rules and regulations we've set ourselves.

Thus, reflect on your previous mistakes in business, and write a personal checklist of things you should control before making a decision.

The author's checklist includes more than 80 questions that he asks himself every time he's considering investing in a new company. He questions, for example, the intrinsic value of a company, how stable its value chain is or if he wants to buy it just to satisfy a personal motivation.

### 10. After adopting the value-investing philosophy, you’ll become not only a better investor but also a better person. 

After learning all about the value-investing philosophy, you'll probably be wondering why the author is sharing these valuable "secrets" with us. Well, it's because he wants to act in the spirit of the value-investing philosophy, and _give something back_.

So, how will the philosophy make you a better person?

First, by developing a network of mutual friendship, you'll transform from a _taker_ into a _giver_. As a taker you would always have a hidden agenda when approaching someone else; as a giver you'll try to sense _their_ needs.

This will expand your goodwill and establish a benevolent cycle: if you do favors for people, they'll do the same for you.

As a result, you'll find yourself within a community in which all members have a positive influence on each other, and negative elements in your network are excluded.

The author believes that, due to the principles of the value-investing philosophy, he's now able to value true friendship, is much calmer, and became "addicted" to positive emotion. In short, he is a much happier person.

Second, by understanding your _inner fears_, including your desires and biases, you take the first step towards inner growth. Identifying your shortcomings and learning about the deepest, darkest parts of yourself, will make you a stronger person, better equipped to deal with the adversity and uncertainty that are inherent to the financial world.

Achieving such self-knowledge on your own is no easy task. If you feel you need assistance here, you could use career coaches, psychotherapy or religion — _anything_ that will help you increase your self-knowledge.

Moreover, by knowing your emotions and habits, you'll develop a more playful attitude towards your working life.

Once you know what motivates and scares you, you will see business decisions more objectively, and be able to make rational decisions without being emotionally troubled by them.

To that end, the author took up playing Bridge, a game in which uncertainty and strategy play a central part. His experience with the game gave him many skills he could transfer into the world of finance.

> _"So instead of seeing everything — including my work — as a form of mortal combat, I began to approach it in a different spirit, as if it were a game."_

### 11. Final summary 

The key message in this book:

**Becoming a value investor means breaking with business conventions, creating a valuable social network and taking yourself on an inner journey. By surrounding yourself with positive people and working on growing your self-awareness, you'll inevitably become not just a better investor, but also a better person.**

Actionable advice:

**If you have misgivings about your current career, ask yourself if you're doing what you** ** _really_** **want to do.**

If the morals and business practices of your job don't reflect your own working ethics, you might have to invest some time and effort in establishing a new career. But don't be afraid to take the other option and change your business from the inside: breaking business norms can be part of professional success as long as you stay true to yourself and your business network.

**Suggested** **further** **reading:** ** _The Little Book of Common Sense Investing_** **by John C. Bogle**

_The Little Book of Common Sense Investing_ provides a detailed overview of two different investment options: actively managed funds and index funds. These blinks explain why it's better to your money in a low-cost index fund instead of making risky, high-cost investments in wheeling-and-dealing mutual funds.
---

### Guy Spier

Guy Spier is an investor based in Zürich who ran the _Aquamarine Fund_ for 17 years and became a leading figure in the value investors network. His work was inspired by Warren Buffet and Mohnish Pabrai, and he's now a regular commentator for the financial media.

