---
id: 58d1345fed3a570004158196
slug: the-ego-trick-en
published_date: 2017-03-24T00:00:00.000+00:00
author: Julian Baggini
title: The Ego Trick
subtitle: What Does it Mean To Be You?
main_color: FED833
text_color: 806C19
---

# The Ego Trick

_What Does it Mean To Be You?_

**Julian Baggini**

_The Ego Trick_ (2011) explores the slippery topic of what we call "I" or "me." These blinks give insight into the many factors that shape our sense of self, including brain function and dysfunction, society, culture and technological changes, and introduce the key philosophical questions behind our ideas about identity, souls and free will.

---
### 1. What’s in it for me? Challenge your understanding of your “self.” 

The person who's reading this text at this moment is . . . you, right? But who _are_ you, really? Are you the schools you went to, the parents you had and the job you hold? Are you the same person now that you were at the age of three? What if you hear multiple voices inside your own head — whether it's your conscience, or you have multiple personality disorder and literally experience six distinct voices and personalities? How do you know who is _really_ you, or what makes you "really" an individual?

Philosophers, scientists and religious leaders have been trying to answer these questions for centuries. These blinks don't pretend to have all the answers, but as you'll learn, no one has them all anyway!

Jump in to learn about

  * the likelihood that you have a soul;

  * the common cause of multiple personality disorder; and

  * why you might not really have free will.

### 2. Do spiritual awakenings reveal the truth of our existence – or are they the result of brain dysfunctions? 

In 1982, a woman named Suzanne Segal was waiting for a bus in Paris. Without warning, she forgot everything about her life, including her conception of who she was.

To Segal, the body and brain that she'd had since birth no longer seemed to be hers. Instead, her thoughts of herself encompassed everything and everyone around her. "I" was nothing more than a vast open space, and Segal became certain that she didn't really exist at all.

For the following ten years, Segal resisted this loss of self and sought help from several therapists. But as her attempts to reconnect with her old self failed, Segal began to wonder if she'd experienced an awakening. Was it possible that losing her identity was a form of transcendence?

Having found similarities between her experience and the Buddhist conception of _anatta_, a transcendental state of non-being, Segal began to work as a spiritual leader. Yet by 1996, Segal's sense of _anatta_ began to fall apart at the seams. At times, she even felt like her old self again, and her spiritual messages became confused.

Neuroscience research suggests that some spiritual experiences may, in fact, be the result of brain dysfunctions. Unfortunately, this seemed to be the case for Segal, who could no longer hold a pen or remember people's names. In February 1997, she was diagnosed with a large brain tumor. She died a few months later, after a brief coma.

Doctors believed Segal's shift in consciousness back in 1982 was caused by the pressure the tumor was exerting on her brain. However, Segal's followers disagreed. They believed it was the tumor that caused her to lose her connection to the transcendent, universal consciousness.

> _"We will never know for sure what the true causal factors for Segal's experiences were."_

### 3. The notion of something like a soul is intuitive, but not logical. 

The idea that humans have an immortal soul exists in nearly every religion. The idea of the soul, eternal and separate from the mortal body, fascinated ancient philosophers too.

Persian philosopher Avicenna tackled the question of whether the soul is real by creating a thought experiment called _the floating man_. Avicenna asks us to imagine ourselves without our bodies, perceiving nothing of the world around us and floating in the air without feeling any sensations at all. In this state, would we still be conscious of the fact that we exist?

If your intuitive response is "yes," the thought experiment seems to suggest that there's something at the core of our existence, something distinct from the sensations of the body and the responses of our brain. But is this conclusion a logical one?

Not entirely. Although Avicenna's thought experiment seems to lead us in the right direction, there are a few snags. This exercise would have us believe that we're imagining ourselves without bodies, but we're probably imagining what it'd be like to have no _awareness_ of our bodies. In our imaginations, our bodies are hidden, but still present — which doesn't allow us to imagine our souls separately.

In addition, our sensations, or lack thereof, can be misleading. After all, people are capable of imagining all kinds of sensations under the influence of psychedelic drugs. The ability to conjure up the "feeling" of a soul as separate from the body doesn't make it a tangible truth. Descartes famous claim — "I think, therefore I am" — contains a similar logical mistake. Just because human beings _can_ think, doesn't mean that human beings are only thinking minds.

### 4. Multiple personality disorder illustrates the subjectivity of our experience. 

Once upon a time, there was a mysterious castle where eleven people lived together. This might sound like the scene for a fairytale, but it is also an apt analogue of the mind of a person with multiple personality disorder.

Multiple personality disorder highlights how subjective our experience of the world is. In 1990, Robert B. Oxnam was diagnosed with _dissociative identity disorder_ (the scientific term for multiple personality disorder). It was as if there were different personalities using his mind at different times.

Sometimes, the personality present in Oxnam would be a mischievous character named Bobby, who was fond of practical jokes and roller skating. But a moment later, Oxnam's brain would switch to the personality of Tommy, who had a short temper and was prone to tantrums. Altogether, Oxnam's brain hosted eleven different characters.

Before he was diagnosed, Oxnam got on fairly well and had a successful academic career. But in 1990, he began to experience longer blackouts. Eventually, Oxnam began therapy.

In his sessions, his therapist witnessed how one personality after another would assert itself. For a quarter of an hour, the angry and childish personality of Tommy would present itself, only to be followed by another, different personality. It soon became clear that each of the personalities contained within Oxnam was unaware that the others existed. Whenever he switched personalities, he'd forget all that he'd just said and done.

Cases of multiple personality disorder like Oxnam's often stem from early childhood abuse. When children experiences profound trauma, whether it's physical, sexual or emotional, they struggle to make sense of what's happening to them.

To cope, their brains create an alternate universe where someone else experiences the trauma, not them. In Oxnam's case, the trauma was so intense that he created eleven characters to help him cope with the aftermath.

Slowly but surely, Oxnam learned to acknowledge and work through his childhood trauma in therapy, and in turn was able to reduce the number of his personalities to just three.

### 5. Our sense of self, which varies from culture to culture, is shaped by how society perceives us. 

Have you ever been so consumed with a personal drama of your own that you completely missed an important event in the world around you? Just as it's not enough to reflect on your own life to learn about the world, you also need to look outside of yourself to learn about the self.

Our sense of self is shaped by the way we're perceived in society. This is why it's vital that the peers of transgender people perceive them the way they perceive themselves — as belonging to whichever gender category they identify with. Society's validation and acceptance are essential to a strong, stable sense of self.

Consider the story of Dru Marland, who transitioned from male to female when she was 43. She emphasizes how important it is for her to pass as female. Otherwise, those around her will treat her like a man, which is at odds with how she feels inside.

Marland, after transitioning in middle age, had an experience common to many transwomen. She found that, as soon as she no longer presented as a man, she was often subtly disregarded or ignored completely. This caused her — as it has caused countless others — to lose confidence.

Different cultures shape our sense of self in diverse ways. In the West, the sense of self centers on the individual.

This isn't necessarily the case in other cultures. According to philosopher Rom Harré, the Inuit have a less individualistic sense of self. This is reflected in the way their emotions are tied up with those around them. If the people in their community are sad, they tend to be sad too. If their community is rejoicing, they rejoice. Similarly, decisions are made from the perspective of the family or clan, not of the individual.

### 6. The existence of the ego is a trick of the mind. 

As human beings, we're terrified of our own mortality. Death marks the end of the complex identities that we feel belong to us. But what if these identities are ultimately illusions? The truth is that the ego is a trick of the mind.

In the eighteenth century, philosopher David Hume developed the idea that the ego is an unstable entity. Our minds, Hume argued, aren't constant. Instead, they're an amalgamation of passing ideas, thoughts and emotions.

To test out his theory, Hume practiced introspection, searching for evidence that a constant subject or self existed independently of his emotions. He found that his sense of self always emerged in relation to a sensation or emotion, such as coldness, heat, light, darkness, fear, joy, love and hate. Hume realized that he was incapable of experiencing himself in any other way.

As a result, Hume concluded that there is no self. The illusion arises from a sequence of experiences and emotions that seem connected in our consciousness, though they're essentially fragmentary. The ego's trick involves constructing a sense of identity out of these fleeting, haphazard perceptions.

Beyond the mind, the self may always elude us. Just as an individual cannot simultaneously be both the hunter and the prey, it's impossible for an individual consciousness to observe itself. It'd be like trying to look oneself in the eyes.

### 7. The idea of the self as an illusion isn’t just found in Western philosophy, but in Buddhist teachings too. 

Though the idea of the self as a construct is relatively new to Western philosophy, it's been central to many other world religions for centuries. Ancient Buddhist wisdom teaches that the self is an illusion.

In the first blink, you were introduced to the concept of _anatta_, or "no-self." Recent re-examinations of ancient Buddhist texts suggest that "non-self" might be a more accurate translation. The no-self interpretation implies that there is no self at all and that the whole universe is an illusion.

The non-self interpretation, on the other hand, merely indicates that the common notion of self should be negated. Buddhist thought takes inspiration from the Hindu concept of an ultimate, all-encompassing reality or deity called _brahman_, which contains the true self. The non-self is the body, the mind and all the temporary thoughts and experiences that individuals take ownership of when constructing their identities.

There are different interpretations of the non-self concept in Buddhism. Certain Buddhist schools of thought argue that when Buddha spoke of non-self, he was rejecting the existence of this divine consciousness called brahman. Instead, the bundle of impermanent and oft-painful thoughts and experiences is all that exists. This pragmatic perspective argues that the self is simply who we consider ourselves to be.

At first glance, this interpretation seems simplistic, even a little depressing. But it also communicates that the self isn't something pre-defined that we're given and then stuck with for the rest of our time on Earth. Instead, the self is something that we _become_ through living, acting and making choices.

### 8. If we have no soul, we might not have free will either. 

The idea that we actually have no hand in the decisions we make, whether we're deciding between ice-cream flavors or career choices, is as relieving as it is devastating. If none of our "choices" matter, we can all stop worrying about making the right ones. On the other hand, this makes life itself seem pointless and empty.

Whether you believe in the empowering idea of human free will or not depends on which theory of the soul you find most compelling. If you accept the argument that the self is no more than a bundle of fragmentary thoughts and experiences, with your decisions and actions simply the mechanical outcome of the body and brain interacting with environmental changes in alignment with physical laws, you must also deny the existence of free will.

The idea of life without free will is something that we instinctively disagree with. When we order a cup of tea, we think that we could have just as easily chosen a cappuccino instead. But this might not be true. It could be that because of your life experiences, the circumstances at the time and the particular feelings involved, choosing tea was inevitable.

This doesn't necessarily mean that the choice is predictable. With so many conscious and unconscious factors shaping our decisions, we don't know which choice we'll make until the very last moment. It's rather like a complex weather system that makes it hard to predict whether it'll rain or shine in three weeks. The fact that we can't predict this doesn't mean that clouds have free will and are doing as they please though.

### 9. Technological and cultural changes are beginning to transform our sense of self. 

In many ways, science fiction authors don't try to illustrate the future. Their real aim is to make sense of what's going on in the present as changes leave our world looking less and less like the world of the past. Therefore, perhaps some science fiction exploring the present nature of the self is in order.

Today, the self is very different from what it once was. Society is moving away from the sense of a united, cohesive identity toward sets of multiple identities. Children's lives used to be comprised of a series of relatively constant and uniform experiences, which led them to develop conventional personalities with a handful of specific cultural eccentricities.

Over the past two decades, this cohesive world experience has been all but rewritten by the internet and globalization. Children come into contact with diverse people and ideas from hundreds of different cultures on a daily basis. As they interact with and internalize new information, their personalities gain new facets.

And it doesn't stop there — changes to our perception of self could become even more radical in coming years. As illustrated by neuroscientist Susan Greenfield, media has a powerful impact on young, developing minds.

According to Greenfield, humans have the ability to perceive themselves as individuals, as part of groups, but also as "nobody." Being "nobody" is, for instance, what we experience when we go out dancing and lose ourselves in the music, or when we immerse ourselves in an addictive TV series. The danger is that if we spend more time engaging with multimedia that lets us be nobody, we'll lose our sense of individual and group identity.

### 10. Final summary 

The key message in this book:

**Though you might feel like you know "who you are," insights from neuroscience, psychology, sociology, religion and philosophy all make a strong case for the idea that the self is an illusion that connects our ever-changing thoughts, emotions and experiences.**

Actionable advice:

**Reflect on yourself.**

Whether through meditation or philosophical introspection, take some time to test the ideas in these blinks on your own mind. Learning to recognize the ways in which we subconsciously construct our own identities leads us to greater self-awareness and can even free us from restrictive ideas about who we are — and who we can be.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Who's in Charge_** **by Michael S. Gazzaniga**

_Who's in Charge_ (2011) explains science's latest discoveries in how the human brain works and what such insight means for civil society at large. These blinks examine the concept of "free will" and how advances in neuroscience are also changing how we approach law and order.
---

### Julian Baggini

Julian Baggini is a British philosopher and founder of _The Philosopher's Magazine_. He is also the author of the bestselling philosophical texts _The Pig That Wants to be Eaten_ and _Do They Think You're Stupid?_

