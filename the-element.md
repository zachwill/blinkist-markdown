---
id: 596b2ec7b238e10005430beb
slug: the-element-en
published_date: 2017-07-19T00:00:00.000+00:00
author: Ken Robinson, with Lou Aronica
title: The Element
subtitle: How Finding Your Passion Changes Everything
main_color: 54ADCF
text_color: 39758C
---

# The Element

_How Finding Your Passion Changes Everything_

**Ken Robinson, with Lou Aronica**

_The Element_ (2009) is about getting in the zone and unearthing exactly what drives you. That driver is your _element_ and these blinks explain precisely what it is, how to identify it and what it will mean for your life.

---
### 1. What’s in it for me? Find your element. 

Americans tend to overuse the word "love." They _love_ macaroni and cheese, they _love_ Katy Perry, they _love_ the color pink.

But Americans are not the only ones who use this weighty word so lightly. Perhaps you yourself have said, "I love to write," or "I love playing the piano." But unless you've truly dedicated yourself and put yourself on the line for your so-called love, "love" might not be the right descriptor for what you're feeling. Perhaps you admire it or wish you could do it. To truly love something, that thing must be your _element_.

In these blinks, you'll learn

  * why it's not too late to start creating or learning something totally new;

  * how aptitude is not the same thing as passion; and

  * why finding your "tribe" involves more than a clever Instagram caption.

### 2. Your element has two important features: aptitude and passion. 

You've probably heard people talk about "being in their element" when referring to a sense of fulfillment and connection with their true identity and purpose. It sounds great, but how can you find that place for yourself?

Well, the truth is there's no single route to get there. But by learning the main characteristics that define the element, you can begin to determine what it means for you. The first of these characteristics is _aptitude_.

Aptitude is what most people call "talent." It's the natural and intuitive ability to accomplish something. However, there are limits to how far aptitude can get you.

For instance, you could have an aptitude for anything from software development to poetry to playing the violin. Maybe you have a voice perfectly suited for opera, or for radio — that's aptitude.

But aptitude isn't enough. You also need _passion_. After all, you can do something with extreme proficiency and still not feel moved by it. Being in your element requires feeling a certain pleasure and delight in what you're doing. This kind of passion will keep you practicing for hours and loving every minute of it.

Just take Charles, the keyboard player in the author's brother's former band. One night after a show, the author told Charles how well he had played and how much he himself loved playing keyboard. Charles responded by saying that the author _didn't_ actually love it, because, if he did, he would be doing it.

Charles practiced several hours a day. Not because anyone was forcing him to, but because that's what he wanted to do. In other words, playing the keyboard was his passion.

### 3. Finding your element requires the right attitude and appropriate opportunities. 

People love to classify themselves as lucky or unlucky, but when it comes to motivation, luck won't get you very far. In fact, you'll have a difficult time ever finding your element, if you're not driven by the proper _attitude_.

Just consider all the high achievers with attitudes that drive their ambition and perseverance every step of the way. John Wilson is a good example. At the age of twelve, he was blinded during a mishap in chemistry class. But the experience didn't defeat him.

He was glad to have the rest of his life in front him and jumped right into learning Braille, the writing system used by the visually impaired. He excelled at a college for the blind and later at Oxford, where he studied law alongside sighted students.

From there he went on to play a role in the formation of Sight Savers International, a group that works to treat and prevent blindness among the populations of developing countries. He served as director of the organization for decades, traveling all over the world and doing incredible work.

That's the power of attitude.

Of equal importance is _opportunity_. To understand why, just imagine being the most talented pearl diver in the world, yet you happen to live in the Sahara Desert. You'd never notice your talent, much less apply it successfully. That's why you need the right opportunities.

Opportunity can come in a variety of forms. In the author's case, for instance, a mentor paved the way.

As a child, the author contracted polio, and, due to the partial paralysis he suffered, attended a school for children with disabilities. He didn't have the opportunity to excel until a public official visited the school and noticed how bright the author was. The official, named Mr. Stafford, had the author take a special test, which ensured him acceptance to a good college and, from then on, Mr. Stafford became a mentor and good friend.

### 4. Intelligence is diverse, dynamic and distinctive. 

Most people have a relatively limited perception of intelligence. They think of it as a score on a test, a grade in school or a knack for words and numbers. But, actually, a totally different perspective is required.

If you fail to recognize the nuances by which people navigate the world, you'll dramatically limit your chances of finding your element. More specifically, when seeking your element, it's essential to recognize the manifold forms that intelligence takes.

Just consider Gordon Parks. He barely got any high school education and probably would have bombed the standardized tests of today. But he taught himself to play piano, take photos, write and master a variety of other skills, molding himself into a renowned American photographer, filmmaker, author and composer.

Intelligence needn't be limited by standard definitions. It's a dynamic force. Like the human brain, it never settles into a single fixed state, and it can be developed by forming connections in a variety of ways.

Consider Albert Einstein, widely known as an incredible scientist and brilliant mathematician. He is a great example of the benefits different forms of thinking can offer. For instance, when Einstein faced difficult mathematical problems, he'd play his violin. This change of pace would foster new connections in his mind and produce solutions that would have otherwise been inaccessible.

And finally, it's important to remember that every single person has a unique intelligence, like a fingerprint. That means there are no two people who use their intelligence in precisely the same way.

For example, some people study math and engineering at top universities to become amazing architects, while others travel the world, observing structures and shapes to help them reach the exact same goal.

### 5. Finding the people you naturally gravitate toward will help you discover your element. 

Have you ever passionately described your dreams to another person and been met with nothing but an uncomprehending stare? Well, you're not the only one, and this poses problems since finding your element requires other people. Indeed, finding your element is much easier if you first find your _tribe_.

This group could consist of just about anybody. It might be people you work with or even your competitors. What makes your tribe unique is a commitment to doing what feels most natural to you.

Consider the American actress Meg Ryan. She did excellently in school, was a brilliant writer and a talented academic. But despite all these other talents, it was on film sets that she met her tribe. These people — actors, cameramen and directors — shared her view of the world. And that's exactly what you need a tribe to do.

So your tribe can help you find your element, but keep in mind that, once you _do_ find your element, it might fully absorb you. You might have already experienced that certain state of mind where you totally lose track of time and space. Well, that's a common way to experience your element.

You'll find yourself entirely absorbed, even "lost" in what you're doing. For instance, when the great Swedish-American pool player Ewa Laurance, is shooting pool, she sometimes feels as though everything around her disappears. She can't keep track of time and will play for nine hours straight, experiencing it like 30 minutes.

But of course there are limitations to this. You can't expect to be entirely absorbed in what you're doing every second of your life. Sometimes the situation just isn't right or you find your mood isn't appropriate. You're bound to get distracted from time to time and it's important to accept that fact.

### 6. Personal and social barriers are par for the course on your way to your element. 

Have you ever felt like you couldn't do something? Whether it was running a marathon, finishing college or painting a beautiful picture, there was probably some goal that you felt you could never achieve. This is normal. Everybody is faced with self-doubt _some_ of the time, especially when searching for your element; you're bound to run into certain barriers, some of which will be particularly personal.

A good example is the celebrated American artist Chuck Close. Facing physical problems and a learning disorder, he did poorly in school, as well as in sports. His life at home wasn't easy, either; his dad died young and his mother had a serious illness.

Nonetheless, his art kept him motivated and caused him to blossom. Even when he became paralyzed by a blood clot and lost the ability to hold a paintbrush, he refused to give up his art; he simply found a way to hold a brush with his teeth and forged ahead.

So whether you suffer from a physical disability or not, a strong will is essential. Remember, even a perfectly healthy person won't be able to complete a marathon without a good attitude.

However, in other cases these barriers won't be personal, but social. For instance, what would you do if your closest friends, or your family, disdained the life path you chose?

Well, that's precisely what happened to the world-renowned Brazilian author Paulo Coelho. He had a difficult life growing up and his parents were desperate for him to become a lawyer. As a result, when Paulo pushed ahead with his art, they had him put into a psychiatric asylum, not just once — but three times!

That's why it's important to remember that, regardless of what the people in your life think is best for you, only _you_ can find your element.

> _"...it is difficult to feel accomplished when you're not accomplishing something that matters to you."_

### 7. It’s rarely too late to find your element, and you don’t need to be a professional to do it. 

Lots of people believe that, upon reaching a certain age, there are particular things they cannot do. But while it might _feel_ too late, it rarely actually is.

Naturally, there are limits to this and you're probably not going to become an Olympic figure skater at the age of 90, but that doesn't mean life is strictly linear when it comes to capacity and accomplishments. People might age in a linear fashion, but you can still reach for your dreams later in life.

Just take the successful author Harriet Doerr. She wrote a bit while raising her children, but it wasn't until the age of 65 that she finally returned to college. The writing courses she took at this late age helped her get into the creative writing program at Stanford University and she was 72 when her first novel, _Stones for Ibarra_, finally came out.

And even if you don't become a world-renowned author, or anything else, you can still be in your element as an amateur. In fact, to find and enjoy your element, it's completely unnecessary to be a professional. After all, your element isn't about getting rich or famous; it's about living according to your talent and your passion.

The academic Gabriel Trop is a good example. While pursuing his PhD in German literature at the University of California at Berkeley, he began playing cello. After just a year, he had risen to the title of lead cellist in the university orchestra.

But when he had to make a choice between music and literature, he chose the latter as his profession. Both pursuits put him in his element, but a career in literature would let him pursue the cello without financial stress. If he had taken the other route, and become a professional cellist, he would have struggled to meet his basic needs, much less find time for literature.

> Actually, the word amateur derives from the Latin word amator, referring to a lover, a great friend, or a person pursuing some specific objective.

### 8. Final summary 

The key message in this book:

**Every person can find her element — that place where she feels totally absorbed and fulfilled. Age, career status and personal barriers will be no match for you if you can find the confluence of your talent and passion and commit yourself to sticking with it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Unlocking Potential_** **by Michael K. Simpson**

_Unlocking Potential_ (2014) outlines practical coaching tools to help leaders, managers or supervisors better engage their teams and transform their organizations. It's simply the most comprehensive guide to becoming a great coach!
---

### Ken Robinson, with Lou Aronica

Ken Robinson is a renowned British thinker on education, creativity and innovation. He is Professor Emeritus at the University of Warwick, a frequent speaker and award-winning writer of, among other books, _Creative Schools_ and _Out of Our Minds: Learning to be Creative._

Lou Aronica is a writer with a background in publishing and co-author of the best seller, _The Culture Code_.

