---
id: 5681b4aa704a880007000041
slug: the-elements-of-scrum-en
published_date: 2015-12-30T00:00:00.000+00:00
author: Chris Sims and Hillary Louise Johnson
title: The Elements of Scrum
subtitle: None
main_color: FD3334
text_color: CC292A
---

# The Elements of Scrum

_None_

**Chris Sims and Hillary Louise Johnson**

_The Elements of Scrum_ (2011) explains how outmoded software development processes are holding companies back in an ever-changing market. Today's successful teams need to be agile and flexible; and the best companies do this by adopting a methodology called _scrum_. This book gives you everything you need to know to start a scrum-based process in your own organization.

---
### 1. What’s in it for me? Get agile and flexible, and learn how scrum will help you work better. 

Do you think you have the best method for organizing a project? It's a classic mistake, especially in software development. Each team has its own preferences and conceits: the designers want it one way, the coders another — and in the chaos, the client is left out in the cold.

The bottom line is that there is no set procedure, no perfect path from A to Z in developing great software products. But there are a couple of tricks that will get you as close to perfection as possible.

Today's teams have to be flexible and agile, ready to bend with the whims and challenges that come their way. You have to make a plan then toss it aside, building another on the fly while keeping your eye on the ultimate goal.

How do you do this? Ditch those old waterfall methods and embrace scrum — an agile, flexible system that will change the way you think and revolutionize your development process.

In these blinks, you'll learn

  * why "waterfalls" today will only get you wet; 

  * how a 15-minute daily meeting is your ticket to success; and 

  * why lots of sprints are far more efficient than a long, slow run.

### 2. The traditional methods of software development are inefficient and lead to cost overruns. 

Tradition may have its charm, but it has few fans when it comes to technology development. Technology needs to be constantly updated to remain relevant; in fact, being current is essential not only to technology but also to the processes used to create it.

One system of development is called the _waterfall method_, which is a _finish-to-start_ _process_ for producing software. In such a system, a software team would typically compile requirements, make a design, write code, test and then deliver a finished product. 

The "finish-to-start" aspect of the process is crucial. Activity A must be completed before work on activity B can start. For instance, you can't begin testing a design until you've finished coding. 

Why do people and organizations like this method?

By separating each development step, scheduling and planning is made easier. Managers often prefer a waterfall approach as they believe it allows them greater accuracy in scheduling and allocating budgets.

But the waterfall method isn't very reliable. Software is often too complicated a product to be fully designed before production starts. Therefore, if you demand a perfectly designed product, you'll be left with little wiggle room for change during production. 

So while it's possible for a perfect design to seamlessly transition to a perfect production process, this usually only occurs when you're producing a static object, like a car. In this example, you'd design every element in the car, then follow those design directions to the letter during manufacturing. 

But software production, in contrast, is just too complex. 

So, while designers may come up with what they think is a perfect product, complications are sure to arise when designs are applied. The numbers tell the tale: only 16 percent of waterfall-method projects meet completion deadlines, while 31 percent are cancelled and 53 percent go over budget! 

How do you avoid such a pitfall in your own projects? Read on to learn more.

### 3. An agile process has the same components as a waterfall process, but gives you more flexibility. 

In the fast-paced and often unpredictable technology market, you need to be flexible to adapt your development process quickly. This means preventing one project element from holding up another. 

But how can you keep the development train running? 

You choose an _agile process_ — a development strategy that embraces change as an opportunity for growth. 

Agile teams work largely in the same fashion as do those using a waterfall process: they gather requirements, make designs, write code, test and deliver a product. The thing that they _don't_ do is to wait for each step to be completely finished before starting on the next. 

Instead, agile teams work on each step _a little bit at a time_, delivering pieces of a product to the client. The team repeats this process, over and over, until the product is complete. This cycle is called _iteration_. 

Let's think about a photo-editing software development team. Using an agile process, the team might first finish a feature that changes a color photo to black and white. The team would send this small feature to the client, who would offer feedback. The team then could work toward improving the feature based on the feedback in the next iteration. 

But don't think that agile processes are just "mini" waterfalls; they're _fully integrated processes._ This means that agile teams treat each project like a complete unit and not as separate parts, as do teams working with a finish-to-start process.

Thus an agile team ensures that its designers and coders think of their contributions as complementing one another — instead of thinking only of "their part" and ignoring the rest.

There's another crucial difference between waterfall and agile processes. In a waterfall process, a client offers his software requirements when the project begins, and isn't consulted again until the product is delivered. Yet with an agile process, a team constantly interfaces with the client during the process, allowing requirements to be tweaked or changed throughout.

> _"The main idea behind the agile approach is to deliver business value immediately."_

### 4. The four core values of agility pave the way to a streamlined, efficient and successful process. 

You're driving along your usual route to work when you see that an accident is blocking the intersection ahead. What do you do? Would you wait for the accident to clear, or turn down an unfamiliar road to explore a way around? 

If you said turn and explore, you've already got one of the four key values of agility. Each value represents a _priority_, or a choice between different approaches.

The first value of agility is to prioritize _individuals and interactions_ over processes and tools. This means that the people involved in a project are more important than the process guiding the project. 

Basically, tools and processes should serve people, not the other way around.

The second value is to prioritize _functional software_ over in-depth documentation. Sure, documentation is important, but it shouldn't supercede the product itself. So if you're pressed for time, focus your energies on turning out a great product and don't sweat taking dozens of documentary photos. 

The third value is to prioritize _customer collaboration_ over contract negotiation. It's essential for the contractual agreement between a development team and a client to remain open. Only in doing so can you be sure of producing and delivering what the customer is really after. 

Agile teams hold regular meetings and maintain open communication at every iteration. While contracts are important, they don't ensure that a client's standards of quality will be adhered to — but open, fluid communication will. 

And finally, the fourth value prioritizes _responding to change_ over sticking to a plan. A plan shows that you know where you want to go and how to get there; but if there's a sudden change (like that traffic accident), you'll need to be both flexible and agile to adapt and reach your goals in stride. 

When you hold true to the four values of agility, you'll be able to set project priorities both adaptive and efficient.

### 5. Scrum is a system that embodies agility through a focused development process. 

Now that you know the agile values, you'll need a system in which to implement them ‒ and that's where _scrum_ will help you. 

But what exactly is a "scrum?"

A _scrum_ is a software development process that applies the four values of agility to build new products through a series of _sprints_ — with each series containing every step of the development process, from researching requirements to delivering the product to the client. 

Within each sprint you only tackle a small piece of your larger project, which can mean that a sprint period can run from as short as a week to as long as a month. 

So how do you start a sprint? 

Each sprint comes alive at a _sprint planning meeting_, which is composed of two parts: the first determines what the sprint's deliverables are, and the second figures out how to achieve them.

First discover what your team needs to deliver at the end of the sprint. This part is led by a _product owner_, the team member who decides what to build and when, based on the wishes of your client. 

In our photo-editing software example, the product owner might say that the team will do a sprint on the product's black and white feature this week, then focus on another feature the next week. 

The product owner is also responsible for presenting an ideal user experience, called a _story_ (you'll learn more about this in the next blink). 

Once the first part is agreed upon, the second part of the sprint planning meeting is devoted to deciding _how_ the team will reach its goals. 

To do this, the team sets certain tasks, such as conducting user interviews or testing. It's important that no task takes longer than half a day, so big tasks should be split into many smaller ones.

### 6. Sprints revolve around the experience and stories of your users. 

A crucial aspect of every sprint planning meeting is coming up with a great user experience. But how do scrums incorporate this element in the process? Through _user stories._

Stories are a simple way to show how people actually use a product. They're called _user_ stories to ensure a team keeps its attention on the end customer and not the product owner, designer or coder. 

Every user story has three components: who is the target user, what is the user requirement and why the user requirement is necessary. 

Here's an example story template: As a (type of user), I want to (do something) so that (some value) is created. Filling in the blanks, a team might come up with: As a user of a smartphone with a camera, I want the photos I take to be automatically edited by the phone software to save me time. 

To plan a successful sprint, a team has to estimate how complicated — or how "big" — a user story is. The bigger the story is, the more development time is needed. For example, tracking a user's entire purchasing process is a big story, requiring a month-long sprint, while developing new headers is a small one, needing a week.

So how do you figure out the size of a user story? 

One way is to focus on the story's relative size — that is, how "big" it is in relation to other stories in your project. A story is, after all, an abstract concept; there's no way to measure exactly how "big" it is.

So take a look at your projects and compare user stories. Find what you think would be the "smallest" story, and then measure the other stories against the smallest one. 

You can even come up with a special numbering system as a reference tool for your team, to quickly identify big stories from smaller ones. For example, if a sprint to redesign headers is the smallest on your list of projects, you could measure the purchasing process sprint as "four header redesigns."

### 7. Incorporate the three types of scrum meetings to ensure the responsiveness and agility of the sprint. 

So your team just launched a new project, and everyone says they're clear on scope and expectations and is hard at work. A month passes, and you gather to share notes — and then the trouble starts.

Three teammates have finished their tasks, two are stuck mulling a problem and can't finish and one guy took it upon himself to change his task in a way that messes up everyone else's work. 

Does this sound familiar?

The scrum method uses three types of meetings to avoid this sort of situation. 

First, a _daily scrum_ identifies changes as they occur in "real-time." It's a very short, daily meeting designed to keep your team on track by identifying obstacles so you can address them straight away. 

A daily scrum should be held at the same time every day — for 15 minutes maximum — and only team members should be present. 

In the meeting, each team member shares what they accomplished the previous day, what they'll tackle today and any roadblocks that might be slowing progress. 

Another type of meeting is the s _print review_, held at the end of each sprint for clients to offer feedback. Here your team can show its work to the client; be sure that the product owner and other team members take note of the client's reactions. 

The client's response lets the product owner adjust the _product backlog_ — a list of to-do items that are ranked according to priority. 

The last type is called the _retrospective_ meeting. Here you and your team can look back on the finished sprint and discuss what everyone has learned. This meeting should be held after the sprint review, on the same day that the sprint is completed. 

During this meeting, your team should also reflect on how to apply new knowledge to the next sprint. For instance, the team might find that it's necessary to change the testing process to make it more efficient.

### 8. Final summary 

The key message in this book:

**Traditional methods for managing software development are ineffective and inflexible. Instead of defining a project, getting to work and not stopping until you're finished, it's essential to bring your customers into the process early on and seek advice frequently. To produce the customer satisfaction you want, you need a process with the agility and flexibility to adapt to any change. That process is scrum!**

Actionable advice:

**Put the project first.**

Developing software is about building a system that solves a problem; and solving a problem is all about teamwork. That's because no software project is ever completed by a lone individual, no matter how central his role. Rather, development processes rely on teamwork to find solutions and overcome roadblocks. In other words, put the project first, _not_ your ego.

**Suggested** **further** **reading:** ** _Scrum_** **by Jeff Sutherland**

Learn all about Scrum, the project management system that has revolutionized the technology industry. This is a uniquely adaptive, flexible system that allows teams to plan realistically and adjust their goals through constant feedback. By using Scrum, your team can improve their productivity (and the final result) without creating needless stress or working longer hours.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Chris Sims and Hillary Louise Johnson

Chris Sims is a certified scrum trainer, agile coach and the founder of _Agile Learning Labs_.

Hillary Louise Johnson is a writer and journalist. She has contributed to publications such as _Inc._ and the _Los Angeles Times_.

