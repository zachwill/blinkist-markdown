---
id: 5e7e1f6d6cee07000723f87f
slug: the-emotion-code-en
published_date: 2020-04-01T00:00:00.000+00:00
author: Dr. Bradley Nelson
title: The Emotion Code
subtitle: How to Release Your Trapped Emotions for Abundant Health, Love, and Happiness
main_color: 4275B7
text_color: 3263A1
---

# The Emotion Code

_How to Release Your Trapped Emotions for Abundant Health, Love, and Happiness_

**Dr. Bradley Nelson**

_The Emotion Code_ (2007) offers a new and empowering approach to healing ourselves and others. Through scientific evidence, plenty of firsthand experience, and a great respect for the energetic and spiritual forces that transcend and unite us all, _The Emotion Code_ lays out a revolutionary method for finding and releasing trapped emotions. By doing so, we give ourselves the opportunity to move beyond our limitations and flourish.

---
### 1. What’s in it for me? Learn a new and powerful method for clearing emotional baggage. 

Today, perhaps more than ever before, people are beginning to understand just how much our emotions impact our experience, thoughts, and reactions. Many of us even recognize that emotional patterns can have a direct impact on our physical health and well-being. 

With this realization, there's been an explosion of interest in therapy and self-help books, some of which can make a big difference. The problem is that a lot of the methods out there don't deal with the problem at its root. 

That's why the Emotion Code is such a powerful tool. By learning how to locate and release trapped emotions, we can entirely reconfigure how emotions flow through or get stuck within us and also replace negative emotions with positive ones. The Emotion Code is the ultimate life hack in healing; a simple and profoundly effective method, it allows us to release old emotional baggage and clear the path for a healthier and more positive future. 

In these blinks, you'll learn

  * what trapped emotions are;

  * how to locate and release negative energy; and

  * why learning to listen to your subconscious is the key to well-being.

### 2. We don’t just feel emotions, they can actually get trapped within us. 

A common way to think about emotions is that they're momentary reactions. For instance, if someone cuts you off in traffic, you feel anger. If your child scores the winning basket, you feel proud. 

However, emotions can also get trapped inside of us and continue to impact our thoughts, feelings, and health without our awareness.

To understand emotions at a deeper level, let's break down exactly what happens when we experience an emotion. First off, our body creates an emotional vibration — for example, the vibration of happiness, sadness, anxiety, or excitement. Then we begin to feel the emotion, as well as the thoughts and physical sensations that accompany it. 

This process can last for a single second or a much longer period of time. Eventually, in a healthy emotional life cycle, we choose to let the emotion go and move on. 

_Trapped emotions_ happen when this process gets interrupted or isn't allowed to evolve fully through the three steps. Sometimes the problem is that we haven't truly let go of the emotion. Other times, it's that we didn't allow ourselves to actually experience the emotion in the first place, but instead bottled it up, storing it away in our body. 

Unfortunately, while the average person has over a hundred trapped emotions, we often aren't aware of them. And if you've ever experienced relationship problems, rejection, negative self-talk, loss, or long-term stress, you most likely have trapped emotions. 

To illustrate how trapped emotions can impact us without our realizing it, let's take the story of Julia, a former client of the author's. Julia was a hard-working, enthusiastic student. In class, she did fine, but whenever she took her qualifying exam, she failed. She'd taken the test three times, and was afraid that if she failed again she might not be able to proceed further in her career path.

Julia had no prior experience working with trapped emotions, but she was so desperate to get around her testing problems that she was willing to give anything a shot. Through a single session, Julia was able to discover that she had a trapped emotion around _discouragement_ from the age of fifteen, when her parents got divorced. 

Under pressure, this trapped emotion took over and sabotaged her. After releasing the trapped emotion, she aced the next test!

### 3. Everything is energy. 

Recent research is proving what healers have known for thousands of years: even though our bodies seem very solid, we're actually built out of energy. The Emotion Code, a therapeutic technique developed by the author, is based on the ancient wisdom that achieving a healthy mind and body requires healthy energy. 

Over the last one hundred years, quantum physics has shown that, at a subatomic level, atoms are actually 99.99% empty space. Think about that for a minute: 99.99% of everything that you see and feel, and which appears to be solid, is actually just vibrating energy! This isn't just an unbelievable and interesting thought — it has a big impact on how we treat our bodies and minds, because energy is stored, released, and interconnected in complex ways. 

For instance, in a recent study, scientists split apart an atom and sent the particles in opposite directions, traveling almost at the speed of light. At a certain point, one of the particles passed through a strong magnetic field, causing the particle to shift direction. At the exact same moment, its sister particle shifted to match the change in direction, even though it didn't go through a magnetic field.

When we apply this understanding of how energy is interconnected at the particle level to the human realm, the common experience of one person's extreme energy infecting a room makes more sense. When someone walks into a space with very irritable energy, the reason you begin to feel irritated is because that energy is literally radiating out and impacting everyone in its vicinity. 

The Japanese scientist Masaru Emoto is famous for showing how the energetic vibrations we send out have a direct impact on the world around us. In a series of experiments, he showed how water droplets formed into completely different crystalline shapes when told, "I love you" versus "I hate you." The "I love you" droplets formed symmetrical, beautiful shapes, while the "I hate you" droplets were asymmetrical and ugly. 

So negative energy can literally change the world around it. But what if that energy is stuck inside of us? This is why we need to understand trapped emotions.

### 4. Trapped emotions impact how we think, feel, and react, often without our knowing it. 

One way to think about trapped emotions is as balls of energy lodged in our system. Like electricity, they're colorless and odorless — but anyone who's ever been shocked knows that just because electricity is invisible doesn't mean it isn't real! Similarly, trapped emotions are powerful, invisible forces, and they impact how we think, feel, and react. 

Have you ever realized in retrospect that you overreacted to an innocent remark, were inexplicably afraid of something, or misinterpreted someone's behavior, leading to a lot of negative emotions? 

If so, it's quite possible that you did so because of a trapped emotion. You may have thought that your feeling or reaction was perfectly rational, but actually, your present was being ruled by a trapped emotion. 

Let's take the story of Debbie, another of the author's clients. Debbie went in for an Emotion Code session, terrified that she was experiencing symptoms of a heart attack. She had chest pain, difficulty breathing, and her left arm and the left side of her face were numb. 

After ensuring she wasn't actually having a heart attack, the author tested her to determine whether she had a trapped emotion. She tested positive for _heartache_. After further questioning, they found that the trapped emotion was stored three years before, when her husband had an affair. 

Debbie was shocked, because she thought she had fully processed that pain in all the right ways. She'd gone to therapy, cried and expressed her feelings, leaned on a strong support network, and made peace with the divorce. Despite all of that, the trapped emotion was still there. Once they released it, all of her symptoms vanished, and she felt freer and lighter than ever before. 

Trapped emotions are also often the root cause of phobias. One client of the author's, who had for years been terrified of flying, was completely cured after a trapped emotion of _helplessness_ — which had been created years before, when she read about a crash — was located and released. 

Unfortunately, much of what governs how we feel and act in the present comes from energetic blockages in the past. The good news is that the Emotion Code allows us to release these blocks. What's more, it's critical that we do so; the longer these blocks stay in us, the more detrimental they can be — even to the point of causing physical ailments.

### 5. Left long enough, trapped emotions can have a physical impact on us. 

In the last blink, we learned about how Debbie's _heartache_ impacted her so profoundly that it ultimately caused a physical reaction mimicking a heart attack. In an ideal world, trapped emotions would be caught and released before ever getting to such an extreme place, but the reality is that they're often neglected so long as to cause physical pain. 

The reason trapped emotions cause physical pain is that these balls of energy are located in the body, where their energy influences the tissue around them. 

Interestingly, ancient physicians observed a correlation between certain emotions and accompanying physical issues. For instance, people who lived a life full of anger often suffered from liver or gallbladder trouble, while people whose lives were overloaded with grief had lung or colon problems. 

While modern medicine has proven that the brain gets activated in certain ways when we experience emotion, the method underlying the Emotion Code shows that the ancient physicians were also right — we do indeed store certain emotions in specific areas. In fact, the Emotion Code uses a special chart to help you locate the trapped emotion and the corresponding area of the body; the chart is free and can be found online by searching for "emotion code chart."

Over the years, Emotion Code practitioners have treated thousands of clients whose excruciating physical pain was immediately alleviated by releasing a trapped emotion. One such client had constant problems with his legs, knees, and back, and was quickly developing arthritis. After working to release _resentment_, _anger_, __ and _fear_, __ the pain disappeared and his arthritis vanished. Another client had _ankylosing spondylitis_, which turns spine ligaments to bone and affects every joint in the body, often causing debilitating pain. After a single Emotion Code session, this client's pain went from an eight to a one. 

A problem with modern Western medicine is that pain is frequently treated with medication, which aims to numb or drown out the problem, but which doesn't treat it at its root. When we experience pain, it's our body sending us a signal that something is wrong. By paying attention to our pain and finding the true source of it, we're able to cure ourselves in a much more profound way.

> _"If the sum total of all your experiences makes up the tapestry of your life, it is the emotions you have experienced that give that tapestry its color."_

### 6. Lucky for us, there’s a simple method to identify and release our trapped emotions. 

So if our bodies and our emotions are made out of energy, and that energy can get stuck in us and have a negative impact on our well-being, what can we do about it? The answer lies with an incredibly simple process, called _muscle testing_. __

Muscle testing was developed by Dr. George Goodheart in the 1960s, initially as a way to correct structural imbalance in the skeleton. However, it was quickly discovered that muscle testing could be used for far more than structural misalignment; it offered us the opportunity to get information directly from the subconscious mind. 

So what is muscle testing? Well, the idea behind it is that the subconscious mind knows the truth, even when the conscious mind may not. By asking a subject questions and then testing their body's responses, we can gain insight into their deep truths. 

While all muscle testing is based on the same basic principles, there are a variety of methods; let's look at a common one to help us better understand the mechanics. 

First, the practitioner asks permission to work on a client. If the client consents, the practitioner needs to ensure that the client is _testable_, which means that their subconscious is willing and able to cooperate. This is most easily achieved by getting the client to first state something true, like "My name is x," and then to state something false, like "My name is y." If the client is testable, they will test strong when they state the truth and weak when they state something false. 

What it means to test strong or weak depends upon what method you are using. The most common method is the arm test. In this method, the client stands with their arm out, parallel to the floor. The practitioner asks a question, and then gently and smoothly applies a small amount of pressure to the client's wrist. If the client tests strong, their arm won't move. If weak, their arm will drop. 

It may sound too good to be true, but this simple method has accurately located thousands of clients' trapped emotions! 

While the arm test can be successfully used both on other people and on yourself, in the next blink we will explore an even easier method you can use at home to begin your Emotion Code journey!

### 7. Your body’s sway can unlock insight into your subconscious mind. 

One of the best explanations for how muscle testing works is that our subconscious brains are used to processing and judging only what is directly in front of us at any given moment. Over the course of human evolution, moving towards what is good and true has protected us, while moving away from what is bad or false has also protected us. 

By applying this instinct to something called a _sway test,_ we can unlock subconscious truths.

To do the sway test, stand upright with your feet shoulder-width apart. Try to relax completely, and then state your name. Your body should sway forward within ten seconds. Then try lying about your name. Within ten seconds, your body will sway backward. 

You can also test out the way your body reacts to positive versus negative statements. If you say "unconditional love," which way do you think your body will sway? What about if you say "hate?" You guessed it! Your subconscious is primed to make you move towards the positive and away from the negative. 

Once you've established a reliable baseline for testing, you're ready to dive into your subconscious. The first question to ask is, "Do I have a trapped emotion?" Most likely, you'll sway forward. If you don't, it's probably because of a block in your system. We'll explore potential blocks in Blink 8, so don't worry about that for now. 

Assuming you sway forward, you can now follow the Emotion Code chart and begin to locate trapped emotions by asking where they're stored and what they are. 

For instance, you might discover that you have a trapped emotion in your spleen and that the emotion is _lack of control._ Using yes or no questions, you might then try to figure out when the emotion was trapped. Let's say you find that it happened when you were six, when you witnessed a horrible car accident. Maybe you don't even remember much about the accident, but with muscle testing you're able to discover that a trapped emotion has been stuck in you ever since, radiating out a negative energy. 

Now that you've located a trapped emotion, it's time to release it. In the next blink we'll explore the simplest do-it-at-home method to do just that.

### 8. One of the most powerful tools for releasing a trapped emotion is a simple refrigerator magnet. 

If you thought that the complicated part of the Emotion Code must come with the release of emotions, you were wrong! In fact, releasing trapped emotions is just as easy as locating them.

Once you've located a trapped emotion and discovered when and where it was stored, the next step is to release it. Remember: trapped emotions are built of energy. To release a trapped emotion, an equal and opposite energy wave must collide with it so that it can be leveled out and washed away. A useful analogy for this is noise-canceling headphones, which sense the frequency of external sound waves and produce the opposite frequency so as to level the sound waves out. 

When it comes to releasing emotions, magnets are the perfect tools to help create this counter-energy.

Magnets are so effective because they magnify intention. When we're able to locate a trapped emotion and approach it with positive intentions and the goal of clearing out this unhelpful baggage, we can counteract its energy and get it to dissipate out through our energetic channels, specifically along something called the _grand meridian_. __

The grand meridian is a term used in the Chinese practice of acupuncture. It refers to our main flow of energy, which moves up our spine and over the center of our head, down through our forehead. When we use magnets to release trapped emotions, we pass them along the grand meridian three times. We then ask, "Did I release the trapped emotion?" If we test strong, then we can move on to the next trapped emotion. If we test weak, then we pass the magnet along the meridian another three times and repeat the process until we test strong. 

In a recent study, researcher Jean-Claure Darras injected radioactive isotopes into acupoints and non-acupoints, to test whether there really is such a thing as the grand meridian, and if so, exactly how it works. To his amazement, he found that the isotopes that were injected into acupoints dispersed precisely along the energy channels charted by acupuncture, while the isotopes injected into non-acupoints dispersed in no particular pattern.

Darras' study confirms what practitioners of the Emotion Code experience constantly with clients — in order to increase well-being, energy must be flowing through and out of our natural energetic pathways. Magnets are the perfect tool for facilitating this.

### 9. Should problems arise while locating and releasing trapped emotions, some useful tips can help. 

The Emotion Code is beautifully simple. However, it's possible that you'll run into some roadblocks along the way. Here are some things to look out for as you begin your practice. 

First off, it's important to remember that getting used to muscle testing will take some time. We tend to be very stuck in our conscious brains, which can sometimes interfere with our subconscious mind's ability to lead. To counteract this, take a deep breath and try to clear your mind. 

Another common mistake when you begin muscle testing is to apply too much force. The right amount of force to apply is about the same amount you would use holding a ladybug safely between your fingertips without hurting it. Remember, the importance is in focusing on the question, not in forcing the physical reaction. 

If you or the person you're muscle testing keeps coming up weak, the most likely cause is a temporary state called _overload_. Overload just means that too many trapped emotions have been released already and the body needs to recalibrate. Overload usually only lasts for about thirty seconds but can persist for up to a day. 

Conversely, if you or the client test consistently strong, there are two likely causes. The first is dehydration; try drinking a glass of water, then retest. The second likely cause is bone misalignment in the neck, which creates interference in the nervous system. If the problem persists, some chiropractic realignment may be necessary. 

Finally, when working on others, it's important to do regular sessions on yourself so as to avoid projecting a trapped emotion onto a client _._ If you notice the same trapped emotion coming up repeatedly, it's a good indicator that you may be projecting. While it's possible that everyone is experiencing that trapped emotion, it's more likely your own energy clouding your readings. 

Another version of this is when you experience something called _resonance_. Resonance occurs when we come across a trapped emotion within a client that also exists in us. By resonating with the client's energy, we can amplify our own trapped emotional energy, ultimately interfering with our client's process. 

Whether you're working on yourself or others, remember to clear the conscious mind, and let the subconscious be the guide.

### 10. The Emotion Code allows us to reroute our current emotional experience. 

Often when we're caught up in a feeling, especially a negative one, we believe it's occurring because of external forces. However, the truth is that no one can make us feel anything — we choose our feelings. That's why the Emotion Code is so useful. Not only does it help us let go of old emotions, but it can also help us choose emotions in the present that better serve us. 

Imagine you spent a week preparing ideas for an important meeting, and you were really proud of the work you did. When the meeting came around, your boss dismissed your ideas, which brought up a negative reaction in you. 

In such a situation, you might let those negative emotions take you over and quite possibly get stored as trapped emotions. But there's another option.

First, breathe into the sensation. If you don't allow yourself to honor the feeling, it'll never be processed in a healthy way, so accept that it's occurring for a reason. Then try to figure out exactly what it is that you're feeling. Maybe, in this case, _rejection_ or _low self-esteem_ will resonate. If consulting the Emotion Code chart doesn't help you clearly identify the feeling, try muscle testing yourself, either with the arm method or the sway test.

Once you've figured out what the feeling is, analyze it. What triggered it? Are there trapped emotions that are amplifying it? Maybe you had a very judgemental father who always told you that your work wasn't good enough, and trapped emotions around that are greatly amplifying your reaction to your boss. 

Next, locate the trapped emotion. Release it with a magnet, breathe deeply, and then test yourself on whether there are still feelings occurring. If so, repeat the process until you have cleared all associated emotions. After releasing the trapped emotions, thank your subconscious mind for guiding you.

Now it's time to decide on a positive emotional resonance to replace your feeling with. Maybe in this case you could replace _rejection_ with _curiosity_, asking how you could present your great ideas in a way that allows your boss to appreciate them like you do.

Rerouting emotions is no easy feat, but with practice, it can make a huge impact on your happiness and well-being.

### 11. Final summary 

The key message in these blinks:

**While reflecting on and talking about your feelings and experiences can serve a useful purpose, it doesn't always treat the issue at its core. The Emotion Code, by contrast, is a simple and amazingly straightforward way to figure out which trapped emotions are holding you back and release them once and for all. By releasing trapped emotions, you will improve your emotional, psychological, spiritual, and physical health and well-being.**

Actionable advice: 

**Practice spreading positive energy to those around you.**

Try building up a positive energy of love and gratitude inside yourself before entering a new space. Once you're in the space, observe how your energy spreads to everyone around you, building into an even larger and stronger energetic field of positivity. 

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next: How Emotions Are Made, by Lisa Feldman Bar** **rett**

Now that you've learned about the Emotion Code, you understand that emotions are far more interesting and complex than automatic reactions in the present moment, and that there's a simple and effective way to work with your emotions to improve your life. 

In _How Emotions Are Made_, psychologist Lisa Feldman Barrett takes an academic approach to the same topic. By overturning old ways of thinking about how the brain creates and processes emotion, she breaks down how emotion is constructed moment by moment, through core systems interacting across the whole brain. If you want to learn more about the intricate inner workings of our minds, bodies, and feelings, head over to our blinks for _How Emotions Are Made_.
---

### Dr. Bradley Nelson

Dr. Bradley Nelson is a renowned holistic practitioner and lecturer who has worked with thousands of clients. His methods are a unique blend of energy healing, applied kinesiology, and magnet therapy. Nelson is widely considered the foremost expert in the emerging fields of bioenergetic medicine and energy psychology.

