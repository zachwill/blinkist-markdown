---
id: 545798d43365660008850000
slug: the-emperor-of-all-maladies-en
published_date: 2014-11-04T00:00:00.000+00:00
author: Siddhartha Mukherjee
title: The Emperor of All Maladies
subtitle: A Biography of Cancer
main_color: BE503E
text_color: A64636
---

# The Emperor of All Maladies

_A Biography of Cancer_

**Siddhartha Mukherjee**

_The Emperor of All Maladies_ is an informative look at the deadly disease that has affected millions. Cancer is one of the greatest challenges facing medical science today, and this book gives us a rare opportunity to learn about every aspect of it — its causes, its astonishing biological processes and our ever-changing fight against it, from the past to the present day.

---
### 1. What’s in it for me? Gain a fuller understanding of the greatest medical challenge of human history. 

It's highly likely that you or someone you know has been touched by cancer in some way. Though cancer and its many forms are more prevalent in our lives than ever, few of us have a solid understanding of the disease. Cancer is not a new phenomenon — descriptions of the illness date from as far back as Egypt in 2500 BCE. Since then, numerous theories have altered the way we look at cancer, ultimately leading us to what we know of it today. This book explains the two biological factors that make cancer cells so deadly. It reveals the internal processes and external agents that induce cancer. Moreover, it guides us through the milestone events in cancer treatment and research that point to the future of our battle with the disease.

In the following blinks, you'll also learn

  * how eternal youth is actually a bad thing for our cells;

  * why young women's jaws began to crumble after painting watches; and

  * how the unlikely team of a pathologist and a New York socialite changed the face of cancer research.

### 2. We’ve known about cancer since ancient times – but our understanding of it is very different today. 

Ever heard the expression "balanced personality?" Today it might be a way to describe one of your level-headed friends, but around 400 BCE it was closely linked to the ideas of Hippocrates, the "Father of Medicine." He was convinced that the human body was composed of four cardinal fluids or _humors_ : Blood, phlegm, yellow bile, and black bile. When one of these fluids was out of balance with the other, then an illness or personality problem would result. For example, a short-tempered person would be diagnosed by Hippocrates as having an excess of yellow bile.

How does cancer fit into this four-part physical system? The first known theory of cancer held that tumors were caused by an entrapment of black bile. This understanding, first developed by Greco-Roman physician Galen in CE 160, informed mainstream theory about cancer for centuries.

But, because autopsies were forbidden for religious reasons, there was no opportunity to prove Galen's theory until the sixteenth century. At this time, the physician Vesalius autopsied cancer-riddled corpses, and was surprised to find that neither the tumors nor the bodies contained black bile. The late eighteenth-century physician Baillie was equally unsuccessful in his investigation.

With Galen's black bile theory refuted, many scientists turned to a substance that was both external to the body, and invisible. Until 1850, scientists suspected that parasitic and inscrutable poisonous vapors called _miasmas_ led to tumors. Worms, fungal spores and protozoa were also thought to cause cancer. Scientists falsely believed they had found them after examining "cancerous tissues" under microscopes, and in 1926 physician Johannes Fibiger was even awarded the Nobel Prize for "proving" that roundworms cause stomach cancer (he was wrong!)

So humanity first thought cancer's cause was located in the body's own substance. Our second theory was concerned with external agents. But what do we think of cancer today?

> _"It is hard to look at the tumor and not come away with the feeling that one has encountered a powerful monster in its infancy."_

### 3. Cancer develops from our own cells, but unlike normal cells, cancerous cells multiply endlessly and never die. 

Today, the idea that cancer is caused by invisible miasmas that emerge out of nowhere seems a little absurd. That's what pathologist Rudolf Virchow may have thought in 1840, when he decided to investigate cancer _only_ using what he could view under a microscope. This approach laid the foundations of our modern understanding of cancer.

Virchow's _cellular theory_ explained that every cell arises from another existing cell. By investigating tumor tissue under a microscope, he discovered that it was in fact composed of a vast number of the body's own cells **.** This is how he concluded that cancer tissue arises from and is made up of our own cells.

So what makes cancer cells so deadly? They are unique in two ways: cancer cells don't die, and they never stop replicating.

Normally, tissues regulate cell replication. The average cell only divides if it receives growth signals from its environment, and stops replication in response to growth inhibitors. This process is crucial. Just imagine if all the cells in your brain replicated endlessly. It wouldn't sound too bad if it made you endlessly smarter, but what would actually happen is that your brain would grow to a skull-cracking size!

Cancer cells do precisely this: they have mutated growth genes, and so they replicate without any signal, and will keep replicating despite the presence of growth inhibitors. This is one aspect that makes cancer incredibly difficult to combat.

The second dangerous characteristic of cancer cells is that they never age or self-destruct, whereas normal cells age and self-destruct if they become damaged.

Again, ageless cells sound rather like something that'd be good to bottle up and market as facial treatment. However, the combination of incessant replication with immortality makes cancer a formidable and all but indestructible enemy. It's no wonder the disease is so lethal.

How does our knowledge of cancer today sit with the two theories of the past? Like Galen, we conceive of cancer as something arising from within our bodies, a _perversion_ of our own cells' nature. But, like the supporters of the second, parasitic theory of cancer, we understand that external agents can induce cancer. ****  

****

> _"Down to their innate molecular core, cancer cells are hyperactive, survival-endowed, scrappy, fecund, inventive copies of ourselves."_

### 4. Certain chemicals not only cause cancer, but also prevent our body from fighting it. 

In eighteenth-century Georgian England, scores of young boys were dying from an otherwise rare scrotal cancer. What exactly was going on?

The surgeon Percival Pott investigated the mysterious case of the disease-stricken boys and found that they were all chimney sweeps. From as young as four years old, these boys were forced to climb naked into narrow, sooty chimneys. As they sweated, the soot ran down to their scrotums, coating the skin and ultimately causing their sickness.

Pott was one of the first scientists to hypothesize that something as mundane as soot could induce cancer. Although data backed up this assertion, scientists were still reluctant to accept it, as it did not align with the cancer theories they'd learned.

However, since Pott's discovery, many other everyday substances have been revealed to be cancer-inducing, including asbestos, benzene and heavy metals.

How exactly can these external substances induce the growth of cancerous cells? It happens in two steps.

Firstly, some toxins can directly alter your DNA. These are called _mutagens._ If mutagens alter the genes for cell behaviors such as growth, self-repair, self-destruction and tissue invasion, a normal cell can transform into a cancer cell.

Benzene, for example, is a substance with a high mutagenic potential, and we encounter it nearly every day. It can be found in cigarette smoke, gasoline, furniture wax, and sometimes even in soft drinks.

So right now, inside your body, there might be a mutated cell, ready to replicate itself endlessly. Normally, your immune system will eliminate this deviant cell right away.

However, certain toxins found in heavy metals and benzene may disrupt your immune system, so that it is no longer able to destroy a potentially malignant cell. This is the second step in the development of cancerous cells, as this renegade cell may now multiply as it pleases, eventually developing into cancerous tissue.

Now that we're aware of these chemicals, it's clear that we need to avoid them. However, we're not safe yet — cancer can also arise from infections. We'll learn about these in the following blinks.

### 5. Infections increase the risk of cancerous mutations as our tissue attempts to recover itself. 

What comes to mind when you think about infections? A runny nose, or that cough you always get at the start of winter?

In fact, not all infections are so benign — some of them can lead to cancer. This connection was first discovered in poultry, when chicken virologist Peyton Rous experimented with a rare chicken carcinoma. Transplanting these carcinoma cells into a healthy chicken, he found that they kickstarted tumors. Rous then prepared another piece of the tumor, filtering out all its cancerous cells and injecting it into healthy hens. Once again, these hens developed cancer.

Rous concluded that the cancer must have been transmitted by an agent small enough to pass through his filters. Only one kind of organism fit this description: a _virus._

You might not feel that you've got a lot in common with chickens, but the link between cancer and infections is something we share. In humans, infections induce cancer in two ways.

Firstly, germs may indirectly give rise to cancerous cells. Some viruses cause a chronic inflammation — this increases the cancer risk dramatically. How? Inflammations damage the cells of infected tissue, while the intact cells divide furiously in order to repair the tissue. But every cell division bears the risk of a _copy error_ — an accidental change in the cell's DNA — that could turn it into an endlessly multiplying cancer cell.

Another such germ is the bacterium _Helicobacter pylori_. It resides in the stomach and is responsible for peptic ulcers, and a lot of damaged stomach tissue. When cells attempt to repair the tissue by replicating, DNA mutations may occur, and in turn, cause stomach cancer.

Moreover, some viruses induce cancer by _directly_ altering a cell's DNA. For example, the hepatitis-B virus is capable of inserting its own genetic code into ours, activating cancer-related genes.

However, most cancers don't arise from infections, and most infections won't result in cancer, so you don't need to worry about getting cancer from a handshake!

> _"All cancers are alike, but they are alike in a unique way."_

### 6. Radiation, hormones and hereditary influences all increase your cancer risk. 

Have you ever heard of the _Radium Girls_? No, they're not a new pop band, but a group of young women in the 1910s who were employed to paint glow-in-the-dark watch dials using highly radioactive paint infused with _radium._ Unfortunately, this work proved lethal a few years later, when their jaws began to disintegrate and they suffered cancerous lesions of the mouth, neck and bones — worse, they developed leukemia.

_Radiation_ was later scientifically proven to cause mutations that lead to cancer.

How? In the 1920s, Nobel laureate Hermann Muller demonstrated the process by bombarding fruit flies with x-rays. The rate of mutated flies increased multifold as a result. In humans, radiation damages the DNA of our cells, which then mutate and may ultimately become cancerous.

In addition to radiation, your body's own _hormones_ can increase your cancer risk. Remember we learned that cancer cells respond abnormally to growth signals? Well, this isn't true when it comes to sex hormones, which work as growth signals for both normal _and_ cancerous cells. For example, any breast tissue will grow faster in the presence of estrogen, whether cancerous or not. Some tumors will even _thrive_ under the influence of estrogen as a result.

The third factor that increases cancer risk is something you're born with — genes _._ This is why some cancers run in families. A notable example of this is the BRCA1 gene, mutations of which strongly predispose whole families of women to breast and ovarian cancer. A healthy BRCA1 gene helps repair damaged DNA in breast tissue, while a mutated gene won't. While most damaged cells die, a few will live on, accumulate more damage and become cancerous. As many as one in a hundred women possess these mutated BRCA1 genes.

In a worst-case scenario, these three diverse factors can come together to cause cancer: a woman could have mutated BRCA1 genes, and be exposed to heavy metals that hinder her immune system's ability to eliminate early cancer cells, while her own estrogen fosters the growth of a tumor.

Having learned all about the factors that increase your risk of cancer, could you believe that some of the very same factors can be used to fight the disease? Well, it's true! But before we find out why, we should first explore the radical changes in the history of cancer therapy.

### 7. Since antiquity, cancer has been fought by surgical means, often with terrible consequences. 

If cancer treatment today seems a complicated process, imagine trying to treat it back in 500 BCE! It was at this time that the proud Persian queen Atossa discovered a lump in her breast. Horrified, she locked herself away in her chambers, isolating herself from everyone but her beloved slave Democedes. He eventually convinced her to let him cut out the lump, thereby healing her.

In theory, what Democedes did matches the first of three approaches to fighting cancer with surgery. The first goal is to remove the primary tumor, and ideally before the cancer spreads to other areas of the body. If those cells have already spread and new tumors are forming, surgery can be used to hinder the cancer by removing those new tumors. Finally, surgery can also prevent cancer by removing tissues such as colon polyps and certain moles, before they become malignant.

In practice, however, Democedes lacked two things that we take for granted in surgery today: anesthesia and sufficient hygiene! In fact, effective anesthesia wasn't discovered until as late as 1846, when dentist William Morton demonstrated the use of ether to induce narcosis.

Prior to this, all surgeons had to numb their patients were alcohol and opium, which were unreliable. Modern reliable anesthetics allow surgeons to conduct complex operations over several hours.

Even if nineteenth-century patients did survive their excruciatingly painful surgery, many of them died afterward due to infections. It wasn't until 1860 that John Lister discovered how to fight infections with carbolic acid, one of the first antiseptics.

Some surgeons fought cancer with increasingly radical means: around 1890, surgeon William Halsted believed in treating breast cancer by destroying every single cancerous cell. This didn't just mean removing the entire breast of a patient, but also the breast muscles necessary to move the hand and shoulder, as well as the lymph nodes. Though this crippling procedure helped prevent local recurrences of cancer, it was useless if the cancer had spread to other organs.

### 8. Chemotherapy curbs the rapid replication of cancer cells. 

Surgery is a vital tool in fighting cancer, but its use is still limited. It's simply not possible to cut out blood cancers like leukemia or to eliminate all rapidly spreading tumor cells. In order to eliminate fast-growing cells that are elusive to the knife, we need _chemotherapy._

The first thing to understand about chemotherapy is that it damages the parts of DNA that govern cell multiplication. It cuts off the growth of every cell in the affected population, but especially cancer cells, as they multiply the most and can't repair DNA damage. In this way, chemotherapy attacks all cells, but normal cells will regenerate while cancer cells die.

One substance used in chemotherapy is actually based on a World War I chemical weapon: mustard gas. Today, its derivatives create nitrogen mustard, which is used to treat leukemia and lymphomas by reducing cancer cells in lymph nodes, bone marrow and blood.

Other kinds of chemotherapy affect not the DNA of cancer cells, but their _metabolism_. These drugs are _antimetabolites_ and can cleverly mimic nutrients required by our body cells. But instead of feeding cells, they are rather like disruptive employees who refuse to do the important job they've been hired to do.

Basically, they mimic substances vital for cell division without actually performing their function. For example, the vitamin _folate_ plays a central role in cell replication. However, if a cancer cell is tricked into "hiring" an _antifolate_, the antifolate won't replicate the DNA, thus halting cell division and stopping the cancer from growing. In fact, these antifolates were the first drugs used to successfully treat leukemia.

These are just a few examples from a wide and diverse range of chemotherapeutic drugs. However, these drugs are all successful in the same way: by putting a stop to the endless replication of cancer cells.

### 9. When surgery and chemotherapy don’t work, radiation is the best option. 

Remember the Radium Girls and their crumbling jaws, and how we found out that radiation can cause cancer? Well, surprisingly enough it can fight cancer too, for the same reason — radiation damages DNA.

So how exactly can we make use of radiation's destructiveness? Radiation treatment uses highly controlled and intense rays to eradicate cancer cells that have spread over a limited area.

For example, the most common blood cancer suffered by children is called _acute lymphoblastic leukemia,_ and while it responds well to chemotherapy, some cancer cells hide in the brain, thereby eluding the chemotherapy. Since these cells can spread all over the brain, we can't just surgically remove the brain to combat the disease!

What we _can_ do is radiate the patient's brain after chemotherapy. The treatment involves the firing of high energy beams into the patient's head several times a week for a few weeks. The beams themselves are painless but may cause sickness, fatigue and hair loss. However, this treatment greatly reduces the likelihood of a relapse.  

Radiation treatment is also effective in eliminating _localized_ tumors that are inoperable, as it is able to reach areas that a scalpel simply cannot without threatening the patient's life. This is why radiation is so useful when faced with tumors located in critical regions of the brain — cutting into these is out of the question, but radiation is a viable option, because its highly controlled beams won't cause as much damage as a scalpel.

So, radiotherapy is a crucial part of cancer treatment for tumors where other treatments have failed. But it's not always just a last resort. Rather, it's combined with surgery in lieu of a more drastic operation.

### 10. In the twentieth century, an unlikely couple joined forces to fight cancer. 

In the 1940s, a pathologist named Sidney Farber was spending his days shut away in a small subterranean laboratory in Boston. Meanwhile, a woman named Mary Lasker lived the glittering life of a New York socialite and businesswoman. An unlikely couple to lead the fight against cancer, wouldn't you say?

In 1947, Farber discovered that antifolates (which we heard about earlier) could be used to treat leukemia. Indeed, he is considered the father of modern chemotherapy.

He is also famous for his compassionate approach to oncological care in the children's ward. He recognized that life with cancer can be crippling, painful and traumatizing, so he insisted on "total care" and established the support systems of social workers and counsellors for patients.

However, the medical and personal needs of cancer patients could not be met by Farber on his own. In 1948, he founded the _Children's Cancer Research Foundation_ and through it raised impressive amounts of money, but still not enough. He needed financial support and a veritable advertising whiz to promote the cause.

Enter Mary Lasker, who just three years earlier had revived the _American Cancer Society_, which campaigned for Congressional funding. Lasker had advertising expertise but required a sympathetic and knowledgeable scientific authority to strengthen her platform.

So, naturally, when Lasker and Farber met, the two immediately hit it off — each had just what the other needed, leading to two decades of brilliant cooperation. The culmination of their work was the _National Cancer Act_, signed by President Nixon in 1971, granting them a vital $1.5 billion in research funds. Today, we owe much of our understanding of cancer to them.

Unfortunately, Farber and Lasker focused mainly on testing various cancer treatments and drugs, instead of performing basic research on the nature of the disease. This meant that it wasn't until 1990 that doctors understood that certain altered genes cause cancer, allowing for a new therapeutic approach to emerge: _gene therapy_, centered around returning these deviant genes to normal or at least muting their growth signals.

> _"History repeats, but science reverberates."_

### 11. Final summary 

The key message in this book:

**Despite the complexity of cancer, thanks to all the research and breakthroughs of the past, we now have a firm understanding of the dynamics of cancer cells. We have at our disposal a diverse range of innovative approaches that allow us to eliminate, treat and prevent cancer while supporting patients.**

**Suggested** **further** **reading:** ** _Parasite_** **_Rex_** **by Carl Zimmer**

_Parasite_ _Rex_ offers an up-close-and-personal look at the fascinating and often misunderstood world of parasites. By introducing you to some of the great discoveries in parasitology, you'll discover that parasites aren't only important parts of our delicate ecosystem but also responsible for our own evolutionary complexity.
---

### Siddhartha Mukherjee

Siddhartha Mukherjee is an Indian-born cancer physician and pioneering researcher. A Rhodes scholar, he graduated from the University of Oxford, Stanford University and Harvard Medical School, has published numerous scientific papers and is the recipient of the prestigious NIH Challenge Grant for his research on stem cells. _The Emperor of All Maladies_ is his first book, and won several major awards in 2011, including the Pulitzer Prize for Nonfiction and the _Guardian_ First Book Award _._ Recently, Mukherjee was awarded the Padma Shri — one of the highest civilian awards given the Indian government.

