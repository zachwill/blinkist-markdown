---
id: 5813015ff70d8c000309427c
slug: the-end-of-average-en
published_date: 2016-11-01T00:00:00.000+00:00
author: Todd Rose
title: The End of Average
subtitle: How to Succeed in a World that Values Sameness
main_color: A93923
text_color: A93923
---

# The End of Average

_How to Succeed in a World that Values Sameness_

**Todd Rose**

_The End of Average_ (2016) reveals how people are measured against an abstract and misguided conception of the average human being, and how their individuality is more or less ignored. Learn about the first misapplications of averages to human nature, and how your company or school can lead the way in recognizing and embracing individuality. And reap the rewards!

---
### 1. What’s in it for me? Learn why we should stop evaluating people through averages. 

Not a day goes by without news about statistics measuring our behavior, our abilities and opinions, our character and body traits. Every day we are measured against an "average," and are so used to statements like, "on average, humans will have 4.7 partners over the course of their lives," that we don't even question the value of such statements.

We should, however!

Evaluating people by means of average measures is misguided at best. It's based on a fallacy — and, what's worse, it disregards what makes us human: our individuality. 

In these blinks, you'll learn how the idea of average measurements was brought into the human realm. You'll learn where and why it fails to describe human characteristics, and why educational institutions, companies and we as individuals are missing out on an opportunity to develop our full potential as long as we adhere to "averagarianism."

You'll also learn

  * how measuring celestial bodies differs from measuring human traits;

  * why, in the 1950s, no US Air Force pilot fit into the aircraft's cockpits; and

  * that companies don't need résumés to evaluate their applicants.

### 2. The mathematical concept of averages has its purposes, but it’s irrelevant when applied to human nature. 

Chances are you spent a good portion of your school life getting grades that placed you on a scale of "above average" or "below average." And as this carries over into job assessments and questionnaires, it invites the question: Who is this average person and how did this system get started?

Well, it began back in the nineteenth century, when astronomer Adolphe Quetelet first used the mathematical concept of averages to explain human traits.

At this time, using a system of averages proved useful for measuring astronomical characteristics, like charting a planet's movements. A number of observers would take turns keeping track of the same celestial body and afterward, the average of their measurements would be used to get an accurate calculation.

Quetelet then applied this system to human beings. He measured thousands of people, both psychologically and physically, and averaged out the results in order to find what he considered the perfect "Average Man."

However, while this approach might be suitable for astronomy, it's not necessarily appropriate for measuring people.

For example, you might have heard the statistic of the average American family having 2.5 children. But, of course, no family actually has 2.5 children.

In fact, the majority of human beings almost always has different characteristics than what is considered the average, which makes the whole concept irrelevant. 

We can see this folly when it comes to human anatomy as well.

In 1945, a competition was held in the _Cleveland Plain Dealer_ newspaper. Women were asked to submit nine of their own physical dimensions; the winner would be the woman whose measurements came closest to a statue called "Norma," created by sculptor Abram Belskie and gynaecologist Dr. Robert L. Dickinson. Using the average proportions of 15,000 women as reference, "Norma," was sculpted to represent the ideal female figure.

Three thousand women entered the contest, yet no one came close to hitting all nine measurements. The winner only managed to meet five of the nine averages.

### 3. There is no single body type and averages do not apply to human anatomy. 

Let's say you're walking outside one day and you encounter two people: one of them is about seven feet tall and weighs 200 pounds; the other is five feet tall and weighs 300 pounds. You could easily use the word "big" to describe both of these people, but how can one word describe two very different individuals?

The lack of a ready vocabulary for accurately describing people shows just how varied human body characteristics actually are.

There is a wide variety of measurements we have to take in order to get an accurate picture of an individual's body size, including head, neck, chest, shoulders, arms, legs, waist, height and weight.

As the statue of Norma demonstrates, many people believe that the average person should have the average of each of these measurements. But, in reality, there is no single body type, and each of these different characteristics is mostly unrelated to any other.

For example, if you were only given someone's weight, you'd have no idea how tall he might be.

And this is why there isn't one single word or measurement that can accurately describe a person's body; there are simply too many variables.

Words like small, medium and large are fine for T-shirts, but when it comes to wedding gowns and vehicles, designers know that averages simply don't work.

In 1950, the US Air Force measured 140 dimensions of over 4,000 different pilots and used the average of these measurements to design their first-ever standard airplane cockpit.

Amazingly, not _one_ pilot fit the dimensions of the cockpit. Research showed that when they used just three dimensions — neck, thighs and wrists — only 3.5 percent of pilots fit these averages. So, by using 140 dimensions, they were essentially ensuring their cockpit would fit no one. 

Nowadays, cockpits and vehicles are adjustable, allowing each individual to comfortably use the controls. Similarly, every wedding gown is adjusted according to the measurements of each bride.

### 4. Character traits are unrelated to how we learn and there is no average path to gaining skills and knowledge. 

The misuse of averages on human characteristics doesn't stop with the body; it has also been applied to the mind and given people the wrong-headed idea that human character traits are related to intelligence.

These are the kinds of ideas that have led to stereotypes like "nerds are bad at sports" and "jocks aren't intelligent."

But the truth is this: the human characteristics that make us individuals have little to do with how we learn.

For instance, take the common misconception that people who are quick to acquire knowledge are also of "above average" intelligence, meaning they can retain and use that knowledge.

Experiments conducted by educational expert Benjamin Bloom in the 1980s showed that there is no relationship between the speed of learning and knowledge retention.

Bloom came to the conclusion that slow and fast learners were both equally knowledgeable about any subject, whether it was philosophy or molecular biology.

Another misguided notion is that there is an "average" path that will better facilitate learning and the acquisition of knowledge. This is another idea that research has disproved.

The thought that there is one "right way" to do things starts early. Scientists once believed that a child's development, from crawling to reading, had to be learned through a strict series of specific stages, and that any deviation from this path could result in a developmental disorder.

But according to psychologist Kurt Fischer, who has done extensive testing in this field, there is no one path that must be followed.

By observing the various methods by which children learn to read, Fischer discovered that 30 percent of children follow a different path than the "standard" one — and yet their skills develop normally.

Studies like this show that there are many different ways to achieve the same outcome, and that the effectiveness of any given way depends on the individual. Therefore, any deviation from what is considered "average" should not be thought of as abnormal.

### 5. Human character traits are fluid, and workplaces should encourage their employees’ individuality. 

Have you ever had a coworker who was strictly business at the office but would cut loose and become a completely different person outside of work?

This isn't all that unusual. Indeed, people tend to exhibit different character traits in different environments. And the only way to truly understand how someone behaves in every situation is to observe him around the clock.

You might believe that the boss at your job is a total disciplinarian, but if you had the chance to visit him at home, you might see that he's extremely easy-going with his children.

The reality is, people don't have fixed character traits for every situation. Our behavior is fluid; it shifts according to context.

For this reason, many modern companies are trying to create flexible working environments that allow their employees' individual skill sets to shine.

Companies like Google and Microsoft spend millions of dollars studying people's individual character traits and range of behavior in an effort to create the ideal workplace that will suit everyone.

These companies use different attributes to their advantage by putting together teams of people who have unique but complementary character traits. For example, a marketing team can have an extrovert to pitch ideas and an introvert to handle research.

By paying attention to what someone's good at, companies can encourage unique skills even when those skills aren't what that person was hired for.

For instance, a worker at the tomato processing company Morning Star was noticed to enjoy fixing and tinkering with machinery. To take advantage of this unique skill, the company made him an expert mechanic of factory equipment, a position that he never would have been hired for based on his résumé.

When companies limit their search for potential employees by having strict requirements, they're missing out on valuable talent. To take advantage of people's unique abilities, you need to look beyond the standard and "average" signifiers like grades and degrees.

### 6. Companies should look beyond the average and seek out individuality in their applicants. 

So, if grades and degrees aren't the best way to determine who should be hired for a job, what is?

To start with, companies need to get specific. They need to look for individuality in their applicants, rather than just looking for someone smart.

After all, a person's intelligence is usually suited for specific tasks, such as problem-solving or retaining facts and figures. So, if a company is simply looking for an "intelligent person," it means they haven't asked themselves: What specific qualities and skills are best suited to the position?

When companies don't know the answer to this question, they're likely to fill the position with someone who has the wrong kind of intelligence and miss out on hiring someone who could be a perfect fit. Therefore, their chances improve when they get specific and look for "a self-motivating person with an eye for detail," for example.

At Zoho Corporation, a tech company in India, they've found another way to hire people with the exact skills they're after — they started the Zoho University.

This allows the company to draw people from impoverished areas and pay them to study programming and learn specific skills, making them prime candidates upon graduation.

In fact, if companies know exactly what they're looking for and push aside the standard model of hiring requirements, they can capitalize on an employee's unique combination of skills. 

This is what the media company IGN did. They knew they couldn't compete with their giant competitors when it came to hiring, so they tossed out the standard system of looking at an applicant's grades and college degrees.

Instead, they began a "no-résumé" rule for applications and, by using their own unique methods, they were able to find the kind of uniquely talented people they were looking for.

One method that proved useful was to pay applicants to work for them on a six-week trial period, after which they could assess their passions and identify their skill set.

### 7. We need to dispose of outdated models based on averages, including our education programs. 

Now that we've seen all the misguided practices of applying averages to human nature, let's take a closer look at how the new techniques of the future can make our world a better place.

The US military has already seen the benefits of disposing with the notion of an average fighter pilot.

Fifty years ago, a female pilot like Kim N. Campbell, who is five feet four, wouldn't be flying for the military. For one, she wouldn't have met the standard "average" height requirements.

Luckily, the military got rid of such notions and, in 2003, Campbell was piloting a fighter jet as part of a counterattack to help US ground troops that were being bombarded by enemy soldiers.

Campbell completed the mission, but in the process her plane sustained damage that put her skills to the test: Campbell grabbed the controls, flew back to the military base and manually landed the jet herself, an extraordinary achievement that had only been attempted a handful of times before.

Another way to promote more extraordinary achievements, and to embrace individualism, is for educational institutions to start offering credentials for specific skills, rather than broadly designed degree programs.

These would be relatively shorter programs focused on one subject, but they could be combined with others to create even better credentials. This way, students would have greater flexibility and freedom to design their own unique path.

For example, you could get credentials in design, animation, mobile applications and software programming in order to qualify for an overall credential in mobile game software development.

While a college degree might have required 24 useful courses, it's unlikely that a company will be looking for someone who is highly skilled in 24 different topics. By focusing on more targeted credentials, job seekers and companies can save time and money and get the skilled employment they both desire.

This would be a big step toward embracing our inherent individualism, stepping away from the fallacy of "averagarianism," and allowing ourselves to reach our full potential.

### 8. Final summary 

The key message in this book:

**We need to acknowledge and embrace our individuality while putting an end to the notion that there is an ideal and "average" human nature. To do this, we must stop comparing ourselves to one another and start paying attention to what makes us individuals. This way, we can focus on our unique abilities and maximize our potential.**

Actionable advice:

**Consider your characteristics.**

What makes you different from those around you? When you discover what is unique about yourself, investigate this further and find out ways to foster this skill set and make the best use of it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Grit_** **by Angela Duckworth**

_Grit_ (2016) is about the elusive virtue that allows people to do what they love, find a purpose in life and, most importantly, stick with it long enough for them to truly flourish. Find out how you can discover your grit and use it to follow your calling in life — and to hang in there, even when the going gets tough.
---

### Todd Rose

Todd Rose dropped out of high school, but eventually earned his doctorate in Human Development from the Harvard Graduate School, where he now works as the director of the Mind, Brain, and Education Program. He is also the author of the book _Square Peg_.

