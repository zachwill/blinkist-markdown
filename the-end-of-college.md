---
id: 592c22a9b238e1000507d15a
slug: the-end-of-college-en
published_date: 2017-06-02T00:00:00.000+00:00
author: Kevin Carey
title: The End of College
subtitle: Creating the Future of Learning and the University of Everywhere
main_color: D6C92B
text_color: 666014
---

# The End of College

_Creating the Future of Learning and the University of Everywhere_

**Kevin Carey**

The End of College (2015) is about the American higher education system. These blinks give a historical overview of how the author sees the development of the American university and its evolution from European models. He evaluates its current status and advocates for the _University of Everywhere_ — a remotely accessible university of the future.

---
### 1. What’s in it for me? Find the university of your dreams. 

Have you ever dreamed of attending a university where you could have conversations with other people during class or enjoy your lunch while learning? How about taking your biology course down by the beach or learning about law in a café instead of spending lonely hours in dusty libraries?

If any of this appeals to you, you might be the perfect student for the university of the future; the _University of Everywhere_.

Online resources already make it possible to access more knowledge through a computer screen than can be found even in the largest libraries and universities in the world. And institutions are beginning to offer more and more online classes. As this body of knowledge grows, education will also become increasingly accessible at a low cost, which will be a considerable contrast to the current elitist education system in the United States.

In these blinks you will learn

  * where the first university was founded;

  * why it wouldn't be a great loss if many current universities close; and

  * how knowledge will become much more accessible to everyone in the future.

### 2. The future of higher education is online, and it’s already here. 

Let's face facts. The American higher education system isn't fit for its purpose. It produces too many dropouts, lets degrees drag on and limits the scope of their subject matter.

There are studies to back this up. It's been shown that not even two-fifths of enrolled students meet the four-year deadline for graduating from college. What's more, two-thirds of them still haven't graduated after six years.

The US census confirms this. According to the 2014 census, an incredible 35 million people over the age of 25 have dropped out of college.

And what about the skills the students were meant to learn? Sociologists Richard Arum and Josipa Roksa conducted a survey of students from a diverse range of colleges in the United States. The study found that after two years of college, 45 percent of students had not advanced in fundamental disciplines. These included critical thinking, communication and analytical reasoning. Even after the full four years of college, 46 percent of the students had made no statistically significant progress.

But there's a solution. It's what the author calls the _University of Everywhere_. It's a new way to get educated. Free online courses will be available to everyone wherever or whenever they want them, 24/7.

It'll be much more egalitarian too. It's not just a finishing school for the wealthy or for those burdened with big loans. There will simply be no debts because there are no overheads online.

This isn't some crazy vision of the future. The University of Everywhere is already here.

The author himself attended an introductory series on biology by a professor at the Massachusetts Institute of Technology (MIT). The class had been set up by edX, an online educational organization established by MIT and Harvard University.

So if online courses are the future, what went wrong in the first place?

### 3. The first universities were all about the students, but this soon changed. 

Have you ever considered when and where the first universities emerged? Or even what their ethos was? Well, they were originally student focused.

The first university was established in Bologna in 1088 — by students! They hired teachers on contracts that today's professors would balk at, to say the least. There were strict conditions and rules. Teachers couldn't just cancel class if they felt like it and punctuality was a must. What's more, they were fined if student attendance was too low, because it was a sure sign that the teacher wasn't doing his job properly by keeping the students engaged in earlier lectures.

But during the Renaissance and the Enlightenment, things began to change. Knowledge became capital to be traded. Before long, universities began to exploit students' desire for advancement.

To become educated, you either had to learn from a professor or buy the books yourself. This effectively created a market for learning structured around the principle of supply and demand. Consequently, universities began popping up everywhere and market conditions meant that they were centered on professors, not students.

Consider the University of Paris, the world's second oldest university. Professors there even organized themselves into faculties based around disciplines like canon law, theology and medicine.

All this time, universities were in high demand. And professors, as owners of intellectual capital, became the gatekeepers. They could decide who could enroll and who couldn't.

Over time, these universities would sell themselves as even better products, partly because they stockpiled volumes of books. In those days, books were hard to come by and very expensive as it was so laborious to copy them out by hand. These books were highly sought after by eager students, who craved the knowledge contained within.

And so it was that students lost control over their learning.

### 4. The printing press influenced the university model that was brought to the United States. 

From the fifteenth century, Gutenberg's printing press spread knowledge throughout Europe. In the wake of its impact you might expect the power balance in the knowledge economy to shift. After all, books were cheaper, in wider circulation and sometimes even affordable!

But did things change? Absolutely not.

Power structures within academia didn't shift. Quite the contrary. The printing press actually set universities' business models more firmly in place.

Books may have become cheaper, but they were still out of reach for most. And even then, students still needed instruction from educated people. Now that the university model was established, all the brains were concentrated within. There was no source of guidance outside the ivory towers!

Consequently, this still meant enrolling at a university. There you met professors who told you what to read and other students with whom you quarreled. What's more, the university libraries had all the books.

Eventually, this university model was exported to the United States. A group of British immigrants established the first college there in 1636 in Cambridge, Massachusetts. It was later renamed Harvard College after its wealthy alumnus John Harvard, who left it a sizable bequest.

Harvard was not the last. By 1765, there were nine colonial colleges in America. These early colleges were organized much like those in England. But the subsequent American approach to founding colleges was quite different.

Whereas universities in England fell under national law and were controlled by English authorities, in the United States, individual states were responsible for the colleges, and they took a looser approach. States happily provided the charters to establish colleges but stopped short of paying for them or helping them find funds. Left to their own devices, American universities developed as private institutions.

By the beginning of the Civil War, there were nearly 250 private universities or colleges in the United States.

### 5. The American University has three founding principles. 

The fundamentals behind the very concept of a university have never been resolved: What should it be? At whom should it aim its teachings? What subjects should be taught?

In the United States, three principles guided universities from the start.

These ideas led to the foundation of three different types of university: the land grant university, the research university and the liberal arts university.

Land grant universities got their name from the 1862 Morrill Land-Grant Act. This act guaranteed that each state could use federal lands, and the income generated from them, to found universities. The principle behind these institutions was to train the working classes in agriculture and the mechanical arts. They were job-focused institutions for the new industrialized age.

The second principle is associated with early nineteenth-century Germany in general and Wilhelm von Humboldt in particular. This was the idea of the research institution. Von Humboldt wanted institutions focused on scholars. They would fuel the knowledge and progress of humankind, while students would hang onto their coattails, picking up what knowledge they could. This became the prevalent model in continental Europe.

The third principle led to the foundation of the liberal arts universities. These were based in part on the ideas of Cardinal Newman of Britain, who was a contemporary of von Humboldt. Newman believed that universities should disseminate universal knowledge, that is, knowledge that already exists. After all, students themselves aren't going to advance science and philosophy!

For Newman, universities existed to explain how the world was constructed and interconnected. That's why these universities trained students in many different disciplines. The students developed broad-based knowledge rather than a specialization in one particular field. In the English-speaking world, Newman's model found the most favor.

No particular model won out in the United States. That's why universities to this day exhibit qualities from all three models.

### 6. Charles William Eliot created the hybrid university – and with it the market for education. 

Today's universities are hybrid institutions. This means they don't exist solely as research, practical training or liberal arts institutions. The hybrid model was devised by the former president of Harvard University, Charles William Eliot (1834–1926).

Eliot believed that before students became world-class scholars in, say, chemistry, medicine or law, they should first be taught to observe and contemplate. His general bachelor's degree was designed to nurture these skills.

Eliot thought that only mature students could compare and reason, so a bachelor's degree became the prerequisite for Harvard's professional or graduate research schools. Therefore the subjects covered in a bachelor's degree were much broader and included English, foreign languages, mathematics, philosophy and the sciences.

Consequently, liberal arts universities and research institutes became perfect fits for one another. The former were basically training intake for the latter. The bachelor's degree taught a more general syllabus. This meant students who progressed to graduate school could delve deeper into specific fields and were taught by specialized professors. This graduate school model looks a lot like von Humboldt's vision for the research university.

But the bachelor's degree was flexible: there was no set curriculum _per se_. Eliot wanted the students to decide for themselves.

Eliot's system spread and was copied by other universities, thus creating a flourishing market for the education of undergraduates. Particularly popular was his idea that students could select their interests. Universities therefore set about hiring more professors, building enviable libraries and world-class departments. It was all about attracting students.

But there was a consequence to this expansion; not all universities could afford it. The largest and richest universities effectively became gatekeepers that could restrict access to only the most affluent students.

The hybrid concept was broken and a two-tier system had been created. We'll explore these flaws in the next blink.

### 7. American universities levy big fees from undergraduates, but they don’t really care about students. 

Even the briefest examination of the American education system reveals its deep flaws. And there are some pretty big elephants in the room.

For starters, it's clear that the focus of the hybrid university is skewed. It focuses primarily on research at the expense of undergraduate teaching. The three principles aren't evenly weighted.

Consider the fact that teaching faculty in colleges, who are mostly doctoral candidates or PhDs, never receive proper training in how to teach undergraduates. That's been the case for several hundreds of years and it's still true today.

This occurred because universities were, and still are, centered on researchers and their fields of specialization. If they do offer training, it's to help them focus on research rather than teaching.

Economically, this makes sense. It's not the teaching of undergraduates or positive teaching evaluations that gets them coin. Instead, academic staff cash in paychecks based on publication of papers or results from their laboratories.

And the crazy thing is this: while universities aren't interested in teaching, attending college has become incredibly expensive for most students.

Look at MIT. A student pays $42,000 in tuition annually. An additional $15,000 has to be set aside to cover books, accommodation and other living costs.

You might think it's expensive because MIT sits in a prestigious bracket of universities like Harvard and Yale. But the high costs are also a reality for students who attend less prestigious schools. Elsewhere a student might pay no more than $100,000 in total for a bachelor's degree. It's still a princely sum. Especially when you consider that because of the poor teaching standards, students will leave college with learning that is restricted or even absent.

The faults of the current system are plain to see. But the University of Everywhere could potentially provide a solution to this arcane and ossified model.

### 8. What the University of Everywhere lacks in close teacher–student relationships, it makes up for in flexibility. 

Just imagine earning credit for your degree while you're at home with your snug comforter around you. The University of Everywhere lets you do it. Perhaps you can forgive its few faults.

Let's begin with the downsides. When compared to the current hybrid-model university, the University of Everywhere lacks the face-to-face interaction that comes from a student–teacher relationship. Direct contact with a teacher is a plus, but let's not make a big deal of it. Teachers at hybrid universities still aren't trained properly.

At the University of Everywhere, interaction takes place through the computer screen. You could even be on a different continent from your teacher! You can contact the teacher electronically, but the teacher still won't be able to customize the learning experience in real time to suit your preferred method or abilities.

But the University of Everywhere _does_ shine when it comes to flexibility. You can study at your own pace and at a time and location to suit.

Long gone are the days of sitting bored or tired in a lecture hall where the teacher is speaking too quietly. At the University of Everywhere, you can turn up the volume or come back when you're more in the mood.

Consider the author's experience. He took a biology course on edX. He paused the lecture whenever he wanted, rewound if he didn't get the gist, or sometimes he fast-forwarded if he already knew the information.

There's also a benefit for students who need jobs to pay for tuition. This model means it's much easier to catch up on missed lectures or seminars.

The University of Everywhere isn't perfect yet. But the advantages it has over the outdated American university are obvious. There is a clear path to the future.

### 9. Final summary 

The key message in this book:

**The American university needs to change. It has come a long way to become what it is today, but now it's failing in its core duty: it doesn't teach students properly. The solution can be found in information technology. A new consumer market is being created and a new type of university will emerge: the University of Everywhere.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Job U_** **by Nicholas Wyman**

_Job U_ (2015) reveals how the idea that college is for everyone will disadvantage both the individual and the workforce as a whole — and even the economy itself. These blinks explore alternative approaches to education that will help us find fulfilling _and_ well-paying careers, proving that college isn't all it's cracked up to be.
---

### Kevin Carey

Kevin Carey directs the Education Policy Program at the nonprofit research organization New America. He has also taught education policy at Johns Hopkins University, Baltimore. He is a regular contributor to the _New York Times_ and has written for publications such as _Wired_ and _Slate_.

