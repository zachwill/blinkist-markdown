---
id: 5654301663626400078e0000
slug: the-end-of-jobs-en
published_date: 2015-11-27T00:00:00.000+00:00
author: Taylor Pearson
title: The End of Jobs
subtitle: Money, Meaning and Freedom Without the 9-to-5
main_color: C52729
text_color: C52729
---

# The End of Jobs

_Money, Meaning and Freedom Without the 9-to-5_

**Taylor Pearson**

Drawing from our history over the last few centuries and from hundreds of interviews with entrepreneurs, Taylor Pearson reveals in _The End of Jobs_ (2015) why being an entrepreneur is safer and more profitable than ever. He explains why so many graduates have such a hard time getting work, and why becoming an entrepreneur in our increasingly globalized world gives us more meaning and freedom than working a conventional 9-to-5.

---
### 1. What’s in it for me? Understand why now is the time to become an entrepreneur. 

Ever since our childhood we've been told to go to school so we can get a good job. A sentiment that still holds strong. But does it hold water? Given that more people than ever before have a college degree and the whole world is just a few keyboard clicks away, it might be time to reconsider this sentiment.

Something has indeed changed. In our globalized modern world, a job is no longer the safest route to financial security. From outsourcing to unemployed college graduates, it looks like we are in a recession, but we could also be witnessing the limits of a decaying system and the beginning of a new one: a paradigm shift and the end of jobs as we know it.

In these blinks, you'll find out

  * how the English King Henry VIII made capital more important than land;

  * why great entrepreneurs think like poker players; and

  * how population growth in the U.S. has increased unemployment.

### 2. Every system has a limit that can be surpassed. 

There are many variables that determine how society progresses. One variable particularly worth examining is the limit of the systems we live and work in. Software entrepreneur Eli Goldratt came up with this _theory of constraints_ in the 1980s. 

According to Goldratt, every system that has a goal also has a _limit_ : a part of the system that restricts growth. So if your factory has two assembly lines that produce 100 units of a product and one that produces only 50, that third line would be the limit.

In order to progress, then, we need to work on this limit. For example, if you have a great product but nobody's heard of it, you won't boost sales by improving your product. Here, the limit you need to improve on would be your marketing strategy.

According to systems thinker Ron Davidson, the last three major changes in the Western economy occurred when the limits changed.

Back in fourteenth-century England, the _agricultural econom_ y was based on land, and since the Catholic Church owned the lion's share of it, they controlled the economy. Therefore, the limit was land until the sixteenth century, when Henry VIII appointed himself head of the Anglican Church and revoked Rome's control over it. 

Then, at the dawn of the _industrial age_ in the 1700s, _bankers_ became more powerful than kings, shifting the limit from land to capital, and forcing land-owner and Prussian King Friedrich Wilhelm III to ask bankers for a loan to finance his wars.

Later, in the 1900s, we shifted from a capital economy into a _knowledge econom_ y. Knowledge, proliferated by technology, became the new limit, making companies like IBM extremely powerful. This was evident in 1975, when IBM successfully issued a billion dollars in bonds, despite their bankers at Morgan Stanley refusing to cooperate.

Since the early 2000s, though, we've seen a weakening of the knowledge economy as a burgeoning _entrepreneurial economy_ gains momentum. Read on to find out why.

### 3. While baby boomers only needed credentials to succeed, today a degree doesn’t guarantee financial security. 

Jobs are becoming increasingly hard to come by and, believe it or not, those in their fifties today are partly responsible for it. 

After WWII, the economic boom pushed the knowledge economy to its limit, and it began morphing into a new system.

People born in the U.S. in the first two decades after WWII — the baby boomers — enjoyed a period of economic prosperity and an abundance of career options. According to the U.S. Census Bureau, jobs grew 1.7 times faster than the population between the years 1948 and 2000.

This growth meant that baby boomers simply had to get a college education and the right credentials, and they'd walk straight into a job. The next generations followed suit, believing it would remain this way. But there was never any guarantee that the economy would stay the same. And it hasn't.

In fact, since 2000, the population has grown 2.4 times faster than have positions on the job market. This explains the plethora of well-educated, jobless graduates. In the U.S., over half the recent college graduates are either unemployed or employed in a position that doesn't require a degree. From this we see that knowledge in the form of credentials is no longer the limit. We've reached the peak of 9-to-5 jobs and we are now witnessing the beginning of entrepreneurship.

Interestingly, the only jobs on the rise since the 1980s are _non-routine cognitive jobs_ : creative, unpredictable work rather than work involving completing the same task every day.

Our economy is expanding based on creativity and innovation. In other words, creativity is the new limit and the new power players are entrepreneurs who set up businesses and pursue innovative ideas. The apartment rental website AirBnB is a great example: they started out by posting apartments on Craigslist, an external site that lists apartments, as a creative way to market their own business.

> _"Entrepreneurship is connecting, creating and inventing systems — be they businesses, people, ideas or processes."_

### 4. Today, knowledge-based jobs can be outsourced or mechanized, which can actually be advantageous. 

Aside from the dearth of jobs in the U.S., there's another reason finding work has become so difficult: the possibility of outsourcing.

Modern communication technology and increased global education has released us from our office cubicles and the number of people with university degrees has dramatically increased, making degrees less valuable. In fact, the number of college graduates in the world increased from 90 million in 2000 to 130 million in 2010. But what does this mean? Well, when we add technology like the video calls of Skype into the mix, this means that qualified talent can be hired and work from anywhere in the world.

This also means companies can employ workers from countries with lower wage expectations.

An English-speaking web designer in the Philippines, for example, can be employed for an annual salary ranging from $700 to $1,400. A shocking contrast to the expected salary of around $82,000 for a web designer in the U.S.

Machines are increasingly replacing humans in industry, especially factory workers whose jobs are now carried out by hardware and software. But these machines have moved beyond factory tasks and can be used in knowledge-based jobs. Take Eventbrite, a company that sells tickets online which are booked and paid for entirely by software.

So what can you do? Don't wait around to be replaced by software — _own_ it.

Modern technology has made running a business financially viable for more and more people, and an entrepreneur can run a small company using an international talent pool at a relatively low cost. Such startups are often called _micro-multinationals_.

Jesse Lawler, for example, runs Evil Genius Technologies, a Vietnam-based software company which employs workers from England, India, the Philippines, and other countries. Lawler has sales and customer service people in the U.S., while his programmers live in Vietnam.

This way he cuts down drastically on his expenses: living costs alone would make it impossible for Lawler to hire more than two developers in the U.S.

For an entrepreneur, modern technology presents an abundance of opportunities.

> _"We've over-invested in traditional credentials at a time when many jobs are being moved overseas or replaced by machines."_

### 5. Being an entrepreneur now is actually more profitable and safer than having a job. 

The benefits of having a steady job have been drilled into our heads since we were children, but what if we were misinformed?

A steady job today means exposing yourself to an insidious risk that is out of your control. In our current economy, your steady income can disappear at the drop of a hat, leaving you clueless as to how to recover.

Your regular paycheck gives you a false sense of security, and you may easily be replaced by an equally talented employee in India who is willing to work for less. It's a harsh reality but, if you've followed orders your entire career, you won't have the skill set to create options for yourself. Think of this as the _Turkey Problem_ : you're like a turkey, relying on being fed everyday until one day you're sent to the ax!

Entrepreneurs don't have this problem, as they deal directly with visible risks. They may not make an immediate profit, but they acquire skills and create systems and can change them if they don't produce the desired results. 

In other words, they enjoy unlimited control and unlimited variables, and the possibility for growth is vast. Most of the entrepreneurs the author talked with would actually be _disappointed_ with a growth rate of 20 percent per annum.

Much like poker players, entrepreneurs understand the concept of _expected value_. This is the average value in a series of repetitions of a random variable.

Say you're playing a hand in a poker game and it'll cost you $1,000 to view the final card. You know there's a 20 percent chance you'll win $20,000 for the whole hand. So you're coughing up $1,000 for an expected value of $4,000, which is 20 percent of $20,000. If you place this bet enough times, you're guaranteed to come out on top. It's no surprise, then, that many poker players end up being entrepreneurs.

### 6. Modern technology has made entrepreneurial investment easier and safer than ever before. 

Starting a business is for highly intelligent business types with piles of cash, right? Actually, thanks to modern technology, anyone has the possibility to do anything from designing furniture to publishing a novel. Apple mogul Steve Jobs even said that entrepreneurs aren't necessarily smarter than the average person.

One thing that has changed the game of business is, of course, the internet. One way it has done so is by dramatically reducing production costs.

The tools needed to create a product are more affordable and more widely available than ever before. Take Software as a Service, i.e., businesses like Adobe Creative Cloud that sell software on a subscription basis. Whereas in the past you needed to invest in pricey equipment and sign long-term contracts, now quality services can be accessed on a monthly basis. So, instead of spending hundreds of dollars on accounting software, you can now pay $9 a month for an accounting program like Xero.

The internet has also lowered distribution costs. Along with accessing manufacturers in lower-cost countries, you can also reach and distribute to customers directly. The author, for example, used to work for a business that made and sold portable bars. By manufacturing in China and distributing directly to consumers, they were able to cut their costs by about 25 percent _and_ improve the product's quality.

The internet is also constantly creating new markets, largely because location is now irrelevant. For instance, in the past you would use a law firm because it was near your office. Now, you can use a website like UpCounsel to find a vetted lawyer who specializes in your needs, regardless of your location.

If you're an entrepreneur, you can also more easily serve niche markets on the net. Firegang Dental Marketing, for example, offers digital marketing services exclusively in the niche of dentists who want to reach new customers.

> _"The ability individuals have now to deliberately design their lives and realities is greater than at any time in history."_

### 7. Unlike the obligation of a job, being an entrepreneur allows you to work on what is meaningful to you. 

Prehistoric hunter-gatherers didn't work like we do today: they worked to survive. But this changed at the beginning of the agricultural revolution. After that, work became an obligation.

When we started creating farming communities, we worked to collect foods we could store or trade. Tilling the land was arduous, constant work to amass wealth rather than to survive.

After the industrial revolution, factory workers viewed work and wages as a means to improve their quality of life. So they bore the obligation of routine factory tasks as they believed it would lead to a better life. 

This idea probably seems very familiar to you. That's because we still hold onto it today.

However, today it's possible to find more meaningful work. But how do we really weigh meaning versus money? Economist Dan Ariely conducted a psychological study in India to find out how powerful money was as an incentive.

In Ariely's study, a group of workers were assigned a complex task that was impossible to solve by using a formula, much like entrepreneurial work. One of the tasks, for example, was to unscramble an anagram. The first subgroup was offered a day's wages for the task, the second two weeks' wages, and the third five months' wages. Surprisingly, the higher the reward, the weaker the performance. This is because meaningful work motivates us more than monetary incentives.

We also saw this in 2006, when Mark Zuckerberg was offered $1 billion by Yahoo to buy out Facebook. Zuckerberg didn't even consider selling. Why? His work meant too much to him. As of 2015, Facebook is worth $230 billion. In the end, Zuckerberg got to continue doing the work he loved and made a staggering profit to boot.

This is all the more proof that starting a business that fulfills us is far more viable than we have traditionally been taught.

### 8. The Stair Step Method is one way to become an entrepreneur and start your own business. 

Being an entrepreneur isn't always a walk in the park, but it's not as treacherous as you may think. Rob Walling, leader of three software companies, used his experience to create a useful entrepreneurial method known as the _Stair Step Method_.

The first step involves creating and launching a single product, selling it for a one-time fee, and using just one marketing channel.

By keeping things simple this way, you'll learn some business basics without the pressure of a highly competitive environment, how to build and market a product, how to network, and how to hire and manage a small number of employees. 

The next step is to launch enough one-time products to be able to quit your job, as Walling did before becoming a computer programming consultant. In 2005, he launched a few products and acquired some invoicing software still in its development stage, but he was only bringing in a few hundred dollars a month.

At this point, your focus should be on generating enough income to buy back the time you put into your day job. This will also allow you to gather business experience, a network of entrepreneurs and a stable workforce.

Walling was able to teach himself marketing just by building on the SEO that the previous owners of the software used. He then used the capital he gained from consulting and the SEO skills he learned to purchase and run an e-commerce website selling bath towels.

After you've launched some products and gained some experience, it's time to expand your business. You could launch bigger products or create a membership site, for example. After buying the towel website, Walling increased the company's profit from zero to $2,500 per month.

One key thing to remember is that no college course can make you an entrepreneur. While education used to be the ticket to financial stability, now practical experience is the way to acquire the skills you need to get meaning _and_ income from your work.

### 9. Final summary 

The key message in this book:

**The promise of a stable 9-to-5 job at the end of college has been unsustainable since the turn of the century. Our society is transitioning into an entrepreneurial economy and now, more than ever, is the time to take advantage of technology to create meaningful work that embraces risk and creativity, and offers more wealth and freedom than the broken tradition of the 9-to-5 grind.**

Actionable advice:

**Make decisions like a billionaire.**

When investing, diversifying is the key, right? Wrong! If you want to see real gains, look at the strategy of Warren Buffett and Charlie Munger of the $300 billion Berkshire Hathaway holding company. Instead of diversifying, day trading or trying to beat the market year by year, these billionaires focus on 40-year time frames. The result? They've raked in $50 billion over 50 years by sticking with only four to five investments.

**Suggested** **further** **reading:** ** _The 10 Laws of Career Reinvention_** **by Pamela Mitchell**

_The 10 Laws of Career Reinvention_ (2010) teaches you the career survival skills you need for the twenty-first century economy. No matter what industry you're trying to break into, this book will help you create a successful roadmap to your goal.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Taylor Pearson

An entrepreneur, marketer and consultant, Taylor Pearson advises authors, entrepreneurs and CEOs on how to expand, clarify and market their businesses.

