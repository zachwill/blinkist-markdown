---
id: 548f060d6532350009140100
slug: the-end-of-stress-en
published_date: 2014-12-30T00:00:00.000+00:00
author: Don Joseph Goewey
title: The End of Stress
subtitle: Four Steps to Rewire your Brain
main_color: 2FBAED
text_color: 1F7899
---

# The End of Stress

_Four Steps to Rewire your Brain_

**Don Joseph Goewey**

_The End of Stress_ (2014) offers a unique look into the severe damage caused by stress on both your health and happiness, and offers simple tips and tricks that you can start using today to undo the damage. Ultimately, it reveals how adopting a peaceful mindset will set you on the path to increased productivity, creativity and intelligence.

---
### 1. What’s in it for me? Find out how stress is killing you, and fight back. 

There's a new epidemic sweeping the world, and it's not a virus. It's stress. Almost everyone has experienced stress at work or at home. But without knowing how to deal with it, it's a lethal affliction, one of the leading causes of death!

Maybe most damaging, though, is that stress keeps us from reaching our potential. You worry about a presentation so much that you make mistakes, instead of being the charming speaker you normally are. Or you're so fed up with the traffic that you don't go out and it affects your relationships with your friends.

_The End of Stress_ includes numerous ways to deal with stress. Then the best part is that once you know how to handle your stress, you can focus on getting the most out of your brain. That means getting back to your creative, intelligent, kind and high-performing self!

In these blinks, you'll find out

  * why stress is lethal;

  * how research indicates your worries are probably groundless; and

  * how loneliness is as bad as cigarettes.

### 2. Stress can be life-threatening. 

Ask yourself, _am I among the three out of four workers who is stressed out by something each workweek?_

Indeed, stress is everywhere. It's become so ubiquitous that most of us consider it to be both normal and harmless. In reality, stress is serious, so serious that it has major health consequences.

When we're stressed, our brain releases toxic hormones that damage our higher brain functions and consequently hamper our mental performance.

Our higher brain, or the _prefrontal cortex_, is where we derive our human intelligence. Think of it as the executive office of the brain, where the big decisions are made.

It's responsible for the invention of airplanes and the _Mona Lisa_. It's also where the plans are drawn for achieving our goals.

Furthermore, the prefrontal cortex enables social intelligence, helping you to discern other people's emotions and evoking compassion.

Your stress hormones, however, disable these high-order functions. As a consequence, your brain functionality is dumbed down, thus making you predisposed to making poor decisions or mental missteps.

Furthermore, stress hormones sever the connections between your brain cells and shrink your brain's neural networks. As a consequence, stress prevents your brain from making the new connections that are necessary for learning and creativity.

But stress doesn't just harm your mental performance; it also harms your physical health. In fact, research shows that stress hormones weaken your immune system as well as damage your heart and even the chromosomes within your cells' nuclei!

Consequently, we find that a myriad of illnesses are directly related to stress, such as heart diseases, strokes, cancer and diabetes, to name only a few of the thousands that exist. In fact, once you tally up all the stress-related diseases, you could consider stress to be the number one cause of death in the United States!

It's clear that stress is severely damaging for both your body and mind. Luckily, as you'll find out in our following blinks, reducing stress is as simple as tweaking your attitude.

### 3. Rewire your brain by changing your attitude and letting fear go. 

When it comes to handling stress, most people feel hopeless. In fact, 83 percent of Americans admit that they've given up doing anything about it at all.

Yet, you _can_ do something about it!

Mitigating stress boils down to your attitude: the right attitude adjustment will literally rewire your brain and mollify your stressful thoughts, thanks to your brain's _neuroplasticity_.

Your brain has the uncanny ability to reorganize itself, creating new neural pathways and expanding its neural networks. You can create these new pathways simply by changing the way you think and the way you view your own thoughts.

It's important that you make a habit of not believing your own negative and stressful thoughts, and instead opt to experience peace and tranquility.

Imagine, for example, that you're stuck in a traffic jam. If you're like most, this is a pretty stressful experience. You might become angry and aggressive, flipping off other drivers and punching your steering wheel, but none of these things actually do anything to change your situation.

But what if you simply chose to be calm instead? Rather than thinking of the traffic jam as eating into your time, you could instead think about it as _extra time_ — an opportunity to think about the things in your life you didn't have time to think about before.

You might even come up with a new idea that will improve your life!

In addition to altering your perspective, changing your attitude is also about letting go of fear.

From an evolutionary perspective, stress is merely an expression of fear. In our collective past, fear of danger was a useful thing: it kept us alert and prevented us from being gobbled up by our predators.

But today's world is much safer, and most of our fears aren't even real. Rather, we conjure them up in our head, and, as you'll discover, the vast majority don't even come true!

> _"Nothing can stop the man with the right mental attitude. . . nothing on earth can help the man with the wrong mental attitude." — Thomas Jefferson_

### 4. Ditch the thoughts that provoke stress. 

We all experience stress. Maybe it manifests itself as fear or anxiety, but we all experience it. And Usually, we blame these negative feelings on external factors: our circumstances or environment.

But once you've cultivated a greater awareness of your stress, you realize that you create most of it yourself.

When we worry, we essentially fear that bad things will happen, and fear means stress. But this sort of stress is unnecessary.

This was verified in a study at Cornell University, in which subjects were told to write down their worries for two weeks and then track which actually came true. They found that the vast majority of their worries — 85 percent — never ended up happening! Of the 15 percent that _did_ actually happen, it often wasn't that bad: in fact, 79 percent of the time things went better than expected.

All in all, the study suggests that around 97 percent of our worries are either exaggerated or complete fabrications.

Looking at these numbers, you have to ask yourself: are my worries rational? Or are they simply exacerbating the situation?

In addition to re-evaluating your worries, you should think about how to better manage them.

Whenever you find yourself having stress-provoking thoughts, make an effort to impartially observe them. Don't interfere with your negative thoughts; just be aware that they are there — in your head, but not in reality.

Imagine, for example, that you're the CEO of a company that just lost an important client. It's a serious loss, and you don't know why it happened. All you know is that you feel somehow responsible.

You become obsessed with the mistakes you might have made, growing so anxious about making any other mistakes that you no longer take _any_ action, which only makes the situation worse.

But wait a minute. Aren't you a CEO precisely because you're capable of dealing with these kinds of situations? Upon realizing this, it becomes much easier to come up with a good plan and save the day.

### 5. Good decisions help you deal with impossible situations. 

Sometimes we're met by horrible yet unavoidable situations. So what do we do in instances where, for example, we lose our job because of an economic downturn or the company we work for cheats us out of our retirement fund? What then?

In any difficult stressful situation, you face three choices:

  * _Change_ it. Identify where you have influence and then put it to use;

  * _Leave_ it. Sometimes it's best to simply walk away and close that stressful chapter of your life. While this process can be painful, it's also sometimes necessary; or

  * _Accept_ it exactly as it is. Sometimes there's just nothing that can be done about your situation. Acceptance means choosing not to complain or judge, and not to demand change.

Take the example of the wife who is troubled with her husband's neglectful behavior. Away from him all day, she wants to spend time together once they both get home from work. Yet, as soon as he walks in the door, he cracks open a beer, plops himself down on the couch and turns on the baseball game.

She could try to change her husband by forbidding him from watching TV, but this could end in arguments and even more stress.

She could also leave him, walk out the door and file for divorce. However, this might be too extreme.

She could also just accept her husband as he is. She could also grab a beer and join him on the couch and learn a thing or two about baseball. Once she gets into it too, they can _both_ spend that time _together_.

Acceptance is the hardest choice, requiring a lot of mental effort and a powerful determination. In order to accept your situation, you'll have to embrace the fact that there are things in life that you can't completely control.

In such situations, when you feel totally powerless, remember that there's always one thing you can influence: your attitude.

### 6. A peaceful mindset comes with practice. 

Developing a peaceful mindset isn't something that happens overnight. You'll have to practice, but luckily there are some exercises that help:

First, start your day off quietly. By beginning your day with peace and quiet rather than frantically rushing out the door, you can ensure that you have the calm mindset necessary to manage the day's challenges.

Try waking up about ten minutes early and find a quiet place where you won't be disturbed. Close your eyes and listen to your breathing. Think about the things that you're grateful for, like the people you love or the opportunities you have.

Commit yourself to having a positive and productive day, adhering to a peaceful attitude that won't be easily disrupted.

If you re-adjust your morning routine in this way, the day will go by much more smoothly, minimizing stress.

Another complementary strategy is to take a thirty-second "_time-out_ " a few times throughout your day. Think of it like a "peace switch" that you can easily flip whenever you need it. Here's how it works:

Disengage from whatever you're doing or from whatever is occupying your thoughts, and allow yourself to relax. Clear your mind, letting go of all your thoughts just for a moment. Take a slow and easy breath and relish in the feeling of peace that washes over you.

There are countless opportunities to do this exercise: you can do it in the shower, while waiting for a friend, or standing in the elevator, just to name a few opportune moments.

Despite its simplicity, this exercise was used and elaborated by the cardiologist Meyer Friedman for highly stressed people who are at greater risk of developing heart disease. He told his patients that they should, for example, intentionally take the longest line at the supermarket as a means of practicing inner peace.

Moreover, he told them they should take notice of the thinking patterns that inevitably arise in opposition to the exercise — thoughts that, for example, tell you that you simply don't have time.

Don't believe them!

The longer you employ these tools, the better the outcomes will be. As you'll discover in our final blinks, ending your stress will allow you to realize the full potential of your brain.

### 7. Your new-found inner peace will spark your creativity. 

Having tamed your stress, you can now get the most out of your brain's amazing capabilities, starting with increased creativity. Everyone's brain is capable of magnificent creativity, as long as they know how to trigger it.

These creative insights require both time and a calm atmosphere. Knowing this, make sure that you stop what you're doing and go on a walk every once in a while.

While you're taking a break, your brain is better able to connect the dots and memorize what you've just done. Counterintuitively, it does this work during times of relaxation. In fact, some of humankind's greatest breakthroughs were made during a peaceful stroll!

Just look at the Scottish inventor James Watt, for example. While working on perfecting the steam engine, which would later trigger the Industrial Revolution, he encountered a roadblock. He just couldn't find the right solution!

So, he went on a walk, during which a light bulb went off — a sudden insight appeared to him that enabled him to perfect the steam engine and revolutionize industrial production.

In addition, make sure not to focus too much on a single thing, and instead allow your mind to wander. Doing so facilitates creativity and problem solving.

Giving your mind the freedom to wander allows it to discover new paths, thus fostering the out-of-the-box thinking that allows you to find solutions to your problem more easily. Indeed, relaxation is the key to innovation and breakthroughs in thinking.

Take the corporation 3M, for example, which is famous for its prolific innovation. For decades they have encouraged their employees to follow the 15-Percent rule, through which employees "waste time" daydreaming for up to 15 percent of their total work time.

Following this rule is what led their employees to come up with these exciting new inventions, such as Art Fry's breakthrough invention of Post-It notes.

### 8. Transcend shame and let go of your ideal self. 

Think back on a time when you made a big mistake in front of others. Remember the heat in your cheeks, as well as the panic and shame you felt.

This feeling of shame is another path to stress.

Imagine for example, that you're an accountant and you've just worked out your budget for the next six months. You're quite pleased to see a surplus, so you quickly send the budget to your boss and proudly proclaim that no cuts are necessary!

The next day, however, you realize that you've made a mistake, and feel the shame of having to admit this to your boss.

Shame is essentially the fear of being judged and rejected by those whose opinions matter to you, and, as you already know, fear means stress.

Shame also causes you to give up control based on what others think and say about you. Rather than expressing yourself as you truly are, shame makes you the person who you think others want you to be.

And conforming to others' expectations is constant work, and that means constant anxiety and thus constant stress.

In order to curtail your shame, you'll first need to be able to recognize it for what it really is: a fabrication.

Think about last time shameful thoughts commandeered your thought process. Why did you feel shame? Was there any real reason for feeling ashamed, or were things not so bad after all?

Looking back to your fictional budget mistake: of course you made a mistake, but mistakes happen to everyone once in a while! And what do you think your boss will do? Fire you? Your boss understands your value to the company, she knows your skills; of course she won't fire you!

In fact, she'll probably just be glad that you caught your mistake at all, so there's really no need to stress out.

### 9. Long, harmonious relationships benefit your health. 

For many, their relationships with other people can be a great source of stress, and as you've learned, stress harms our health. Simply put, the better your relationships are with other people, the better your health and the longer you will live.

In part, this is because the love you experience in strong relationships slows down the production of toxic stress hormones. In fact, numerous studies have demonstrated that people who have strong relationships are 50 percent more likely to live for longer.

Here's an impressive example: there was a Greek immigrant named Stamatis, who left his home town in Greece to live the American Dream. Unfortunately, a few decades after his arrival he was diagnosed with lung cancer and given only nine months to live.

With such a grim prognosis, he went back to his hometown and reconnected with his childhood friends and family. Rekindling these relationships had a major impact on his health, and he lived much longer than only nine months — in fact, he lived to be just about 100 years old! (His exact age was undetermined. According to his calculations, he was 102-years old at the time of his passing; according to his documentation, he was 98-years old.)

Conversely, loneliness damages your health. In fact, current science suggests that experiencing loneliness is as harmful as smoking 15 cigarettes per day.

Unfortunately, we sometimes ruin our chances at forming deep connections by judging others. Indeed, our judgements — whether about how people look, behave, talk, etc. — always erect a barrier between ourselves and others.

When we feel judged, we tend to project our shame onto others by forming our own unfair judgements. For example, if you ever got a miserable grade on an exam as a kid, you might have blamed your teacher for being "stupid" because you yourself felt the shame of poor performance.

We often think that others can't sense the judgements we make about them. In reality, you unconsciously send thousands of signals, through facial expressions, body language, etc., that reveal your judgement.

As social animals, we're adept at reading those signals, so your unkind judgements can damage your relationships and thus your health.

### 10. Harness the power of suggestion to improve your health. 

Have you ever wished upon a star, or crossed your fingers and hoped or prayed that your dreams would come true? Although it might sound surprising, these superstitious beliefs actually influence whether your dreams come to fruition!

Indeed, science shows that the mere thought of a desirable outcome can set into motion the inner resources and actions that will ultimately fulfill your desire.

For example, if you know that you have to take a test, you can increase your score by simply convincing yourself that you are well prepared for the exam.

This effect is also known as the _Placebo effect_, i.e., when a medication or medical procedure produces health benefits in a patient who believes that he has received useful medication, but which was in fact inert.

The medication itself does nothing (often, placebos are just sugar pills), but the patient's mind generates a beneficial outcome nonetheless.

In a classic study, for example, severely injured soldiers were given placebos that they were told were actually powerful opiates. The soldiers, despite receiving medication with no actual medical benefit, felt immediate relief from their physical pain.

Additionally, science suggests that the placebo effects works in areas outside of the medical field.

For example, while it might seem unbelievable at first, the power of suggestion can even help you lose weight!

During a study on hotel cleaning staff, some of the employees were told that their work constituted heavy physical exercise comparable to going to the gym. After only four shifts, these workers lost both weight and body fat without _any_ diet or increase in activity!

The _only_ thing that changed was their mind-set.

As you can surely see, your attitude has a huge impact on your life: your beliefs are capable of ending your stress and improving your life.

### 11. Final summary 

The key message in this book:

**Stress inflicts major harm on your health, intelligence and performance. Luckily, you can overcome the negative influence of stress by incorporating certain exercises into your life and adopting the right mind-set.**

Actionable advice:

**_Hawaiianize_** **your mind when faced with stressful situations**

Think back on a memory that brings you contentment and peace: maybe your honeymoon or the smell of grandma's freshly baked cookies. Feel how the memory calms you, and relish the experience. Now that you feel calm, you're better equipped to fully utilize your brain's higher functions and increase your performance.
---

### Don Joseph Goewey

Don Joseph Goewey formerly managed the Department of Psychiatry at Stanford Medical School as well as a pioneering research institute focusing on methods to cope with catastrophic life events. He is now a managing partner of ProAttitude, a firm dedicated to ending stress in the workplace.

