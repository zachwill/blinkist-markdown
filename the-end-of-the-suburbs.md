---
id: 53d2a6883063370007010000
slug: the-end-of-the-suburbs-en
published_date: 2014-07-29T00:00:00.000+00:00
author: Leigh Gallagher
title: The End of the Suburbs
subtitle: Where the American Dream is Moving
main_color: E22D34
text_color: BF262C
---

# The End of the Suburbs

_Where the American Dream is Moving_

**Leigh Gallagher**

_The_ _End_ _of_ _the_ _Suburbs_ tells the story of how what used to be the textbook example of achieving the American Dream is in deep trouble today. The rising cost of living and an increase in poverty and crime have made suburbs less desirable places to live. The silver lining in the death of the suburb, however, can be found in the renaissance of once-neglected urban areas.

---
### 1. What’s in it for me? Learn how the American Dream became the American nightmare. 

A home for every American. A chicken on every table. A mother, a father and two children who play on a well manicured lawn enclosed by a white picket fence.

This is the image of the twentieth-century American family: home-owning, family-oriented suburbanites.

This kind of quiet, suburban lifestyle is seriously appealing to many people. In fact, you may very well be one of them. For most of the last century, the American Dream was embodied in the suburban lifestyle, as it offered people the independence associated with homeownership and the privacy and serenity of a life outside the dirty, smog-ridden city.

However, these blinks will show you how this idyllic image of the good life doesn't represent the reality of suburban life. Far from it: suburbanites languish in a life of boredom, unhealthiness and uniformity.

Not only that, you'll be shocked to learn how crime and poverty have made their way into these "perfect" American communities. In fact, changing demographics and social values have left many people wondering whether the suburban life is really for them.

In these blinks, you'll learn:

  * why the suburbs are "turning grey,"

  * why your life is in greater danger on a suburban road than any other road in America,

  * why, when making an omelette, it's better to actually _mix_ the ingredients together, and

  * why it seems that the age of the suburbs has come to an end.

### 2. In post-war America, a cultural and financial obsession with homeownership drove the growth of suburbs. 

What's the first thing that comes to mind when you imagine the _American_ _Dream_? From the early 1930s onward, this dream often involved striving to own a car and — more importantly — a home in the _suburbs_ : a serene, green place far beyond the hustle and bustle of the crowded city streets.

What was it that drove this near-universal dream? Actually, it was in large part the result of actions taken by banks and the US government.

The government actively encouraged homeownership as a way of representing wealth and prosperity and inspiring patriotism and "good citizenship." In 1934, it created the Federal Housing Administration, which encouraged private lenders to provide mortgages to eager home buyers by insuring them if the loans went unpaid.

Banks, too, encouraged mortgage borrowing as a means to increase their own profits. One way of doing so was turning debt into bonds, which investors could then trade. In order to maximize this profit opportunity, they encouraged more and more people to buy their own homes by taking out mortgages.

These plans were an enormous success, leading to millions of new homeowners.

As demand continued to rise, builders and developers started building in more and more undeveloped areas. Because the value of housing climbs with the number of homeowners, builders would often build in areas further away from city centers to secure cheaper land and thus more homeowners.

Those desperate to own a home aren't put off by the distances: their only concern was finding a house they could afford to buy, a phenomenon known as "_drive_ _till_ _you_ _qualify_."

As a result of all this outward development, 3 million Americans lived in the suburbs simply because of their families' desire to have their own little piece of land by 2009.

Over the years, however, the desire to live the American Dream, or at least this version of it, has severely declined. In the following blinks, you'll learn all about why.

> _"Suburbs evoke a way of life, one of tranquil, curving streets and cul-de-sacs; marching bands and soccer leagues."_

### 3. Living in the suburbs brings with it a uniformity that not everyone finds appealing. 

For many Americans, living in the suburbs has long been their dream. Who wouldn't want to live in a big house with a big yard, miles away from the claustrophobic, polluted cities? Yet, hidden behind this idyllic facade, suburban life still has its problems.

For starters, life in the suburbs confines people to social routines that plunge many into depression.

There's simply not much room for creativity in the suburbs: most families live in similar houses, eat the same types of food, shop at the same shops and have the same hopes and aspirations.

This uniformity can lead to depression and anxiety, especially among those without jobs or who stay at home all day and lack other stimulation.

We see this reflected in popular culture today, where the media plays upon the dullness and uniformity of traditional suburban life. Just think about films like _Blue_ _Velvet_ and _American_ _Beauty_ or the TV series _Desperate_ _Housewives_.

And this is no new phenomenon. The lack of character of suburban life has long provoked comments from intellectuals.

Sociologist and urban historian Lewis Mumford, for example, believed that the homogeneity of suburban life placed limits on creativity, fearing that suburban life limited people to consuming the same processed, tasteless foods, socializing only within the same social class, and enjoying the same mass-produced culture, especially from watching television.

In addition, writer Jane Jacobs saw the destruction of "real" nature in all this new suburban development. She lamented that chasing the dream of a serene life in open, green, natural spaces outside the city walls actually leads to the destruction of natural rural diversity. All the new houses, roads and malls are built upon what was once a natural landscape and ecosystem.

Which leads us to our next question: What, exactly, makes the suburbs more uniform than the city?

### 4. The segregation of suburbs into different zones of activity fosters a lack of variety and diversity. 

If you live in a city, how far do you have to walk before you get to the nearest Chinese restaurant or thrift store? Probably not too far. It's just not the same in the suburbs, where proximity to shops and culture is limited.

That's because, unlike urban environments, where houses, apartments, factories and shops are all jumbled together, suburban space tends to be more strictly segregated. This separation based on function is known as _single-use_ _zoning_, first introduced by the Supreme Court in 1926, and ruling that areas earmarked for residential buildings could not be developed for anything else.

However, in spite of its attempt to create quieter community space, this zoning strategy has many critics. One architect has even likened single-use zoning to an unmade omelette. Rather than combining all the ingredients, the individual ingredients are left separate — eggs over here, cheese over there, and salt in its own corner. If you eat these ingredients on their own, your meal wouldn't be nearly as satisfying as if they were all mixed together.

The result of single-use zoning has been the creation of boring, impersonal communities. Sure, life in the city certainly has its drawbacks, but it's also full of possibilities and excitement. You never know who you'll bump into next or what new experiences you may enjoy.

Life in the suburbs is essentially the opposite. Since everything is separated, having new experiences means hopping into the car and taking a drive.

For example, the author describes how one woman felt after moving to a suburb after living in New York City. She found that she was almost permanently bored, and even began to miss things about the city that used to annoy her, such as the loquacious doorman from her old apartment, and even found herself becoming more talkative due to the lack of stimulation.

### 5. Living in the suburbs creates a reliance on cars, which in turn causes financial and health problems. 

The car is, hands down, the most important product for existence in the suburbs.

Since the suburbs are built so far away from commercial and industrial centers, suburbanites need a car — or two or three — to get around.

In fact, the design of suburbs themselves reflects drivers' desire to be comfortable. For example, cul-de-sacs and looping streets allow for easy navigation. And the streets between residential areas, called _arterial_ _roads_, tend to be wider so as to allow more cars to move quickly between communities.

Unfortunately, however, this dependence on cars results in health problems.

For starters, spending so much time in the car severely limits the opportunities for physical exercise. In addition, the car-centric suburban design makes it difficult for those who would prefer to walk to get around.

Indeed, an over-reliance on cars has resulted in serious consequences for US citizens' health in general, increasing the rates of heart disease, diabetes and obesity. In fact, more than a third of all American adults are considered overweight, the number of overweight children has doubled since 1980 and, among teens, the number has tripled.

Furthermore, wide roads encourage drivers to go faster, which consequently makes it more dangerous for pedestrians. In fact, more people are killed on the arterial roads between suburbs than _any_ _other_ type of road.

This dependence on cars is even more problematic in an age of rising fuel prices. During most of the history of the suburbs, fuel was cheap. However, since the turn of the millenium, the cost of fuel has steadily risen. In fact, from 2000 to 2008 oil prices increased by an astounding 80 percent.

For people living in the suburbs, the cost of fuel is becoming unbearable. This was demonstrated in a study suggesting that lower-income suburban families spend more money on transportation than on housing, i.e., 29 percent versus 28 percent of their income, respectively.

> _"The average American walks a little over 5,100 steps a day, compared with 9,700 for Australians."_

### 6. Rising suburban crime and poverty is making people think about returning to cities. 

For much of the twentieth century, people associated life in the suburbs with the ultimate sign of peace and prosperity, whereas cities were considered to be a place of vice and crime. In recent years, however, this trend has actually reversed.

Indeed, crime and poverty in the suburbs is climbing at an alarming rate. In part, this is because the financial crisis has forced many middle-class Americans into poverty.

Another cause has to do with the demographic make-up of today's suburbs. In the 1990s and early 2000s, for instance, a wealth of construction and service jobs in the suburbs led many immigrant laborers to flock there for work.

However, after the financial collapse, these jobs were hit the hardest, leaving most of the suburban newcomers to face serious hardship. In fact, between 2000 and 2010, the number of poor people in the suburbs rose 53 percent to 15.3 million, a growth rate double the one found in the cities.

And with poverty comes crime.

While the average number of homicides in the United States has fallen in recent years, it's actually climbing in the suburbs — and climbing fast. Between the years 2001 and 2010, the number of homicides in US suburbs rose by 17 percent.

Consequently, with the rise of these problems in the suburbs comes the exodus to the cities to live and work. For example, in 2010, Philadelphia saw a population increase for the first time in sixty years. New York City, too, saw its urban population rise by 40,000 in only ten years — despite the 9/11 attacks.

In fact, while the suburbs are racked with crime and poverty, many cities have made serious positive adjustments. Where there were once derelict factories and dilapidated homes there are now waterfront marinas, multiplexes, loft apartments, convention centers, amusement parks, etc. And the list goes on.

We've seen now how and why the suburbs have declined in recent years. These final blinks will deal with the future of the suburbs.

### 7. The popularity of suburbs has declined as young people’s aspirations no longer revolve around raising a family. 

When you imaged the prototypical American Dream in the first blink, what sort of family did you see? Probably two parents and two kids: the _nuclear_ _family_.

Although this lifestyle was long held as the American ideal, changing demographics in society have caused the nuclear family to become less present.

Over the last decade, getting married and raising a family is no longer the central focus for many young people. We can easily see this lack of interest reflected in statistics.

For example, in 1957, the birth rate in the USA reached a high of 122.7 births per thousand people. In 2011, this number was nearly halved at 63.2 births per thousand people. Additionally, "married households" comprised 83 percent of the population in 1950. Today, that number is little over 48 percent.

As a result of young people's waning interest in starting and raising a family, the suburbs have lost their appeal.

Young people today demand vibrancy and opportunity in their communities rather than peace and quiet. They'd rather live in the cities, where their professional and entertainment opportunities are more plentiful.

This, in turn, leaves the suburbs with an increasingly "greying" population. Unsurprisingly, as young people flock to the cities, the average age of suburban residents increases, which has a direct effect on suburban services.

Without the young families to support them, facilities like schools and youth groups have to shut their doors or are repurposed for an aging population. In one suburb, a local school shut down and became a senior center.

These changes are great for seniors, but they make suburbs even more unappealing to younger people.

> _**"** The traditional family structure is now really a minority."_

### 8. The suburbs can be revived with the right planning. 

We've now seen the rise of the suburb as an American ideal and its steep decline in recent years. But is it destined to die out altogether? Or does the fate of the suburb lie elsewhere?

It depends. On the one hand, suburbs provide a way of life that is still desirable, yet they also suffer from bad design, which makes realizing this way of life difficult.

The suburbs still appeal to Americans' desire for privacy. In fact, the chance to live in relative peace — versus the confinement and chaos of cities — is what drove many to flock to the suburbs in the first place. Once suburbanites make it behind their closed doors, there's very little chance for others to bother them.

Some suburbanites have even described the suburbs as providing them with their own "private realm."

So, at least part of the appeal is still there. If the suburbs are to be saved, however, they need to be planned better for the twenty-first century. If they could overcome their dull uniformity and over-reliance on cars, the suburbs could once again become a desirable place to live.

Luckily, there are _New_ _Urbanists_ — or architects who want to build neighborhoods that encourage walking — traditional planners, academics and real estate developers all looking at novel ways of building and developing.

One idea is to build communities on the basis of traditional planning principles: building a main street with rows of houses close together. This development style, more like a nineteenth-century town or village, aims to create a sense of community and vibrancy that was otherwise lacking in suburban development.

Another idea is to improve public transport as a way of encouraging less driving. One such step in this direction is the approval for a $1.6 billion light rail line in Maryland linking the suburbs in between New Carrollton and Bethesda.

The will is there; developers just need to find the way.

### 9. Final summary 

The key message in this book:

**The** **traditional** **suburb** **is** **now** **a** **thing** **of** **the** **past.** **Whereas** **cheap** **gas,** **land** **and** **easy** **access** **to** **credit** **made** **it** **the** **most** **desirable** **place** **to** **live,** **today's** **changing** **economic** **and** **social** **conditions** **have** **caused** **suburbs** **to** **become** **less** **attractive.** **Unless** **developers** **can** **improve** **the** **suburbs'** **image,** **people** **are** **likely** **to** **flock** **back** **to** **the** **cities** **in** **ever** **higher** **numbers.**
---

### Leigh Gallagher

Leigh Gallagher is an assistant managing editor at _Fortune_ magazine and makes regular appearances in the news media where she comments on economic issues. In addition, she co-chairs _Fortune_ 's Most Powerful Women Summit.

