---
id: 5a00e036b238e100067eaf44
slug: the-energy-bus-en
published_date: 2017-11-10T00:00:00.000+00:00
author: Jon Gordon
title: The Energy Bus
subtitle: 10 Rules to Fuel Your Life, Work, and Team with Positive Energy
main_color: E8942E
text_color: 99621F
---

# The Energy Bus

_10 Rules to Fuel Your Life, Work, and Team with Positive Energy_

**Jon Gordon**

_The Energy Bus_ (2007) is a handbook to positive energy and long-term success in your personal and professional life. These blinks are full of practical advice on how to cut out negative feelings, replace them with positive thoughts and fuel your life journey with committed people and a can-do outlook.

© Jon Gordon: The Energy Bus copyright 2007, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

---
### 1. What’s in it for me? Take a ride on the bus to a better, more positive life. 

Most of us have both personal and professional slumps in our lives. Sometimes those slumps can feel so deep that you wonder if you'll ever get out of them. So imagine if you went on the bus one morning and the bus driver gave you a set of rules for escaping your slump.

That's what these blinks are about: the energy bus. Take a ride on an allegorical bus that gives you new ways to think positively and you'll find yourself transforming both your personal and work life. Here you'll find ten rules for giving your life an energy boost.

In these blinks, you'll find out

  * what the formula E + P = O means and why it's important;

  * why you want to get rid of negative energy on your bus ride; and

  * how CEO has a different meaning on the bus, and why you should become that CEO.

### 2. A lack of control over your life can breed unhappiness. 

For most people, "positive energy" means walking around with a pasted-on smile. However, truly positive energy runs much deeper; it's a state of real optimism and enthusiasm that cultivates a lifestyle of trust, passion and love.

It sounds great, but how can you reach that state?

Well, take our fictional leading man in this story, George. His life seemed perfect; he had a house, a wife, two children, a puppy and a team to lead at the NRG Company, which manufactured light bulbs. Nevertheless, George was miserable. Every little thing seemed to go wrong for him, and he was desperate to find meaning in his life.

Lots of people feel like George, trapped in the passenger seat of their own lives. After all, life is loaded with stress and impossible expectations, both personal and professional.

One Monday morning, George discovered that his car had a flat tire. He was late to a meeting about the launch of a new light bulb, and his team descended into chaos. To make matters worse, that morning he'd had a vicious argument with his wife. Just one day in and he was already certain that it was going to be another terrible week.

From there, it seemed like things only went downhill; George's car also had a brake problem, and he had to take the bus to work for the next two weeks. However, when he climbed on board for the first time, he was immediately captivated by Joy the bus driver and her brimming smile. As it turned out, she had met many people like George and knew exactly what he needed.

George learned from another passenger that, according to a study, Monday morning at 9 a.m. is the most common time for people to commit suicide. It's the time when people feel they simply can't go on with another week of their lives. George was dangerously close to this point and Joy knew she needed to intervene. So, she invited him on a once-in-a-lifetime journey: a trip to a happier life on the _Energy Bus._

### 3. Take control of your life and focus on the vision you desire. 

Joy offered George an incredible opportunity, but it wasn't until his wife and boss told him he was poised to lose everything that he was ready to accept it. Once he did, he committed to learning Joy's ten rules for a new life. The first rule was: _become the driver of your own bus and take back control_.

Here's how his journey began.

As George boarded the bus for another day at the office, Joy told him that people are often unhappy because they have no say in their lives. George immediately understood what she meant. After all, he felt trapped between conflicting expectations, like those of his boss and his wife. Not just that, but his life was full of events totally beyond his control, like the breakdown of his car.

Joy went on to say that, according to the philosophy of the energy bus, to truly change your life, you need to take back control, decide who you want to be and how. But before you do that, you need to know where you're going.

That's where the second rule comes in: with _vision and focus, you can drive the bus in the direction you want._

To demonstrate this, Joy asked George to write down a vision for his personal life, one for his job and one for his family. George decided that he wanted to be happy again; he wanted to have a positive influence on his kids and for his wife to be happy. His primary goal at work was to have a successful product launch with his team.

Now all that was left was to achieve these goals. To help, Joy told George about the _law of attraction_.

According to this concept, all thoughts are magnetic, which means the things you think about will begin showing up in your life. Just take Olympic athletes, many of whom use the law of attraction by visualizing their best performances before entering a competition. Often this results in gold-medal wins. Realizing this powerful potential, George resolved to spend ten minutes a day thinking about his goals.

### 4. You can choose how you perceive events, transforming energy from negative to positive. 

Everybody knows that it can be difficult to focus on the positive, but working on your attitude and outlook on the world can make all the difference. Your perception of events is entirely within your control.

Just take the formula _E + P = O_. It says that _Events_ \+ _Perception/Positive Energy_ = _Outcome_. In other words, there are lots of things you've got no control over, but what you _do_ control is how you perceive those events. So, by choosing to have positive thoughts, rather than negative ones, you can achieve better results.

This is key since positive energy will keep your bus moving forward. This is rule number three: _positive energy is the fuel for your journey_. But how can you keep that fuel tank topped up?

Sometimes it just requires approaching things from a different perspective. If you have a lot of work to do, it can help to feel grateful for having a job in the first place, knowing that lots of people struggle to get work at all.

Such a simple act of gratitude will release endorphins and make you feel better before you know it. George decided to try this practice out for himself by spending ten minutes walking around the office, considering all that he has to be grateful for.

He realized that he's healthy, has a loving family, a home and a job — much more than most people. After this walk he felt a newfound sense of energy and could get back to work, ready to face all the challenges of the day.

You can follow George's example. Just make these little pieces of practice part of your daily routine, offering yourself opportunities to recharge and reenergize as you do.

### 5. Tell people about your vision and ask them to join you on your journey. 

Whether it's at work with your colleagues or at home with your partner, happiness and success often rely on teamwork. After all, a happy family life and a successful work environment can only be achieved when everyone does their part.

So, as George began to realize his vision in life, he started to get worried; if he was the only one approaching his work with passion and energy, it was unlikely that he'd be able to pull off a successful product launch or improve his marriage. Luckily, Joy was on hand with rule number four: _tell others about your vision and ask them to join you_.

It's simple: the more people you get on your bus, the more positive energy you'll have to fuel your ride and the more successful your results will be.

To make this a reality, Joy told George about an easy-to-use website that could help him tell other passengers about his plans. It's called theenergybus.com, and by using it, George could print out bus tickets to give to his wife and everyone on his team at work.

But a ticket isn't much good if passengers don't first understand where they're going. While George was excited about the idea, he also knew from speaking with Joy that it was important to personally hand each potential passenger a ticket, but only after discussing the upcoming journey. That's because, if people don't know where you're headed, they won't be so keen to hop on board.

With this knowledge in mind, George met with each member of his team individually, handing out invitations to share his vision. In those meetings, he laid out his expectations for the launch and asked everyone to commit to the vision.

His employees had all weekend to consider their conversations and let him know on Monday if they were going to come along for the ride. George felt energized by the prospect of adding new travel companions. But he was also nervous that they might disappoint him. To avoid this, his vision would need to have enough power to inspire others, something you'll learn all about in the next blink.

> _"No one creates success in a vacuum and the people we surround ourselves with have a big influence on the life and success we create."_

### 6. Don’t waste energy on people who don’t share your vision; remove negative people whenever you can. 

When George met with his team again, he was shocked to hear that one team member, José, didn't want a seat on the bus. George was surprised because José was one of his best employees. This unexpected development made George question his vision; how could he succeed if others didn't want to help him?

Well, it's actually less important than George might think. If people don't want to get on the bus, you shouldn't try to convince them.

Just take a Gallup poll which found that there are around 22 million workers in the United States who hold a negative attitude toward work. These employees are responsible for an annual loss in productivity of around $300 billion. Naturally, being surrounded by people with such negative attitudes can fuel self-doubt and prevent you from realizing your goals.

So, to handle his situation, Joy told George about rule number five: _save your energy and don't try to convince people who aren't ready to get on board_. Just remember, if they're full of negativity, they'll only slow you down.

And if negative people _do_ get on your bus, it's essential to remove them as soon as you can. You might notice passengers who complain along the way. You can think of these people as vampires who suck out your positivity and vision.

That's why rule number six is to _get Energy Vampires off of your bus_. To put it another way, if you have negative people on your team, sit them down and have a talk. Try to determine where their negative attitude comes from and how you can work together. If they're unwilling to change, you have to let them go.

Or, in situations where you can't get rid of problematic people, say your boss or supervisor, find ways to boost your own positive energy. If you can, you'll outweigh the negativity they bring into your life.

### 7. Other people can feel your emotions and be inspired by your enthusiasm. 

Have you ever known someone who gets ridiculously excited about new albums or movies? Have you noticed that seeing such an energetic person describe their passion fills you with excitement and happiness as well?

It's only a natural response. After all, the human body can sense the emotions of others. Just take a study from the Institute of HeartMath, an American research and education institution that specializes in the way the heart influences well-being. Their research found that the heart can transmit emotions through its own electromagnetic field. Not just that, but such a field can be sensed from a distance of up to ten feet!

In other words, people around us can perceive the way we feel and respond to it. That's why people can tell when someone is insincere. But it also means that when people are truly passionate about something, they share that enthusiasm and those positive emotions with others.

Because of this, it's key to energize your whole bus, but when you do so, it's also important it's done with true enthusiasm. This is where rule number seven comes in: _enthusiasm will make more people join you_ and motivate them along the ride. In fact, the word "enthusiasm" comes from the Greek word _entheos_, meaning "inspired" or "filled with the divine."

As long as you're excited about your work and the tasks you're doing, everyone around you will feel the same. This is the kind of positive energy you need to fuel your ride for the long haul.

Joy even has a name for it: your _CEO_ or _Chief Energy Officer_. People who fill this role infect their team members, employees and even customers with powerful positive energy that inspires and propels them.

### 8. Make your passengers feel valued by giving them your time and recognition. 

You've probably noticed that you work harder when others recognize your efforts and care about you. It seems obvious, but it points to a deeper human need for appreciation.

In other words, by loving your team members, you can make them feel good, but also motivate them to do their best work and follow you wherever you go. That's why rule number eight is to _love the passengers on your bus_.

Here's how.

First, be sure to take your time and listen. Remember, love and relationships need a while to develop and blossom. So, just as you need to spend time with your partner to build your relationship, you've got to spend time with your employees. Regular individual meetings are essential; they offer an opportunity for you to get to know your team personally.

In these meetings, you should listen carefully to what the other person says and show empathy for their experience. Your employees should know that you care both about their needs and their ideas.

From there, it's also important to recognize your employees as individuals and show them how much you appreciate their work. This one is a no-brainer since people want to be seen as individuals, not robots. So, show your workers that they're more than a cog in a machine.

For instance, you should never send an electronic birthday message. Rather, write out real birthday cards for each person on your team.

And finally, be sure to recognize the individual accomplishments of every employee you manage. Just take George and his employee José, who didn't want to get on the bus. José never felt valued and, even though he regularly worked late, George never recognized it. Adding insult to injury, when José asked for a raise, George said he would think about it but never brought it up again.

But that was the old George. The first thing the new George did after learning about rule number eight was to thank José for all his great work and promise him a raise. As a result, José was happy to join George on the energy bus.

### 9. Purpose and fun will propel you to new heights. 

By implementing all the rules you've learned so far, George managed to get his marriage back on track and spend more time than ever with his kids. However, as his product launch approached, George wondered how he could inject another shot of energy into his team.

To do so, he needed to understand that purpose makes his work easier. A story about President Johnson and a janitor at NASA offers a great example. The president was visiting the space program when he encountered a custodian enthusiastically cleaning the floors. The president told the man that he was probably the best janitor he had ever met, to which the janitor responded, "Sir, I'm not a janitor. I just helped put a man on the moon."

It just goes to show that, with a higher purpose, you can easily accomplish everyday tasks, and that's why rule number nine is to _let yourself be driven by a higher purpose_.

George got his team to see themselves as creators of not just light bulbs, but light — the kind of people who can illuminate the first book a child reads. Once they made this decision, they all happily worked late nights alongside George. Suddenly, the whole team was united behind their shared mission.

But most importantly, purpose and fun can enable stellar performances. Another passenger on George's morning commute told him about a study involving two teams, each of which was tasked with designing a new airplane. One of them was told that they were chosen to design the best airplane that ever existed, while the other was told to simply design components and denied any information about the mission behind the project.

Unsurprisingly, the team with the mission had a better time and worked harder, completing their job in half the time.

This example points to the last rule, number ten: _you have got to have fun on your ride_. After all, success is much easier to attain when you're enjoying yourself, rather than stressing out.

### 10. Final summary 

The key message in this book:

**The modern world is overloaded with competing pressures from the stress of work to the demands of family life and the general difficulty of paying bills, maintaining your house and holding everything together. But there's a solution: positive energy can help you take back control and find true happiness.**

Actionable advice:

**Learn from your setbacks.**

Making sure your bus arrives at its destination can be hard work. To get where you want to go, you'll need to be able to handle all the setbacks and challenges along the way. To do so, make a habit of asking yourself what you can learn from such difficulties. What opportunities do they present to you and your team? By thinking productively about all the roadblocks, the road to your vision will be all the smoother.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Positivity_** **by Barbara L. Fredrickson**

_Positivity_ presents the latest research into the positive emotions that are the foundation of our happiness. By presenting different strategies to increase the amount of positive emotions you experience, this book will help you adopt a positive general attitude toward life.
---

### Jon Gordon

Jon Gordon is a leadership and teamwork expert as well as bestselling author. He regularly shares his knowledge at conferences and has worked with a number of organizations, including The Los Angeles Dodgers, Dell, Southwest Airlines and BB&T Bank.

