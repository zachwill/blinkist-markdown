---
id: 5575979e3935610007420000
slug: the-entrepreneur-roller-coaster-en
published_date: 2015-06-10T00:00:00.000+00:00
author: Darren Hardy
title: The Entrepreneur Roller Coaster
subtitle: Why Now Is the Time to #JoinTheRide
main_color: CD2940
text_color: CD2940
---

# The Entrepreneur Roller Coaster

_Why Now Is the Time to #JoinTheRide_

**Darren Hardy**

_The Entrepreneur Roller Coaster_ (2015) is the essential guide to building a successful business. These blinks will walk you through the most important aspects of entrepreneurship by taking a close look at the four skills crucial to success: sales, recruiting, leadership and productivity.

---
### 1. What’s in it for me? Discover how to be a great entrepreneur through the good times and the bad. 

Whatever we do in life, there will be ups and downs, times when we are on top and times when we simply want to give up; this is especially the case for entrepreneurs.

When trying to start a business from scratch, there might be times when you will win big, and others when you will be close to bankruptcy.

When these bad times hit, many people give up — but not those with the right mind-set. They ride the entrepreneurial roller coaster right to the end, until they have made their company a success. So what is the right mind-set? Read on and you will find out!

In these blinks, you'll learn

  * why every entrepreneur needs something to fight for;

  * why Barack Obama doesn't care that half of the American public dislikes him; and

  * why every CEO should aim to be the dumbest person in the company.

### 2. Ignite your passion, the key to business success. 

Plenty of people go into business for the money. But the truth is, people who become entrepreneurs just to get rich rarely end up with the fortunes they seek.

Want a better motivator than money? Try following your passion, and here's why: 95 percent of your job will be repetitive and boring, and only 5 percent will be fun and engaging. So you had better love that 5 percent.

Take Bono, for example, the lead singer of the Irish band U2. His life may seem amazing, but he's actually only performing and doing what he loves about 5 percent of the time. He spends the other 95 percent taking care of business: rehearsing, traveling and reviewing contracts. But the 5 percent he loves gets him through the 95 percent he doesn't.

Passion is the key to getting through the hard parts, but how do you find what _you're_ passionate about?

Actually, your passion is already inside you — you just need to stir it up. You can start by thinking about things you want to change or fight against. Some of the world's most revolutionary ideas originated with people confronting what made them angry.

Just think where we'd be if India's Mahatma Gandhi, South Africa's Nelson Mandela or US civil rights activist Martin Luther King, Jr. didn't say "enough" and stand up to injustice.

Try using your enemies to ignite your inner passion! But remember, your passion resides within your strengths, and you can't get anything done without the right knowledge.

Take the example of business magnate Warren Buffet. In the 1990s, when the worldwide trend was to invest in internet-based companies, Buffet refrained. When the dot-com bubble burst, he was applauded and was asked why he stayed away from these companies. He explained that he only invests in his areas of expertise; he simply didn't know enough about the internet.

Follow Buffet's example and find your passion by knowing your strengths and sticking to them. Passion is important because people aren't born successful. Rather, success takes hard work, determination and a commitment to improving. Your passion will get you through the difficult times you'll experience on the road to success.

### 3. Overcome the self-conscious naysayers to follow your dreams. 

Have you ever been fired up by a stellar idea, only to have people throw cold water on your passion instead of offering their encouragement and support?

It's a common response from insecure people to great ideas, especially when you distinguish yourself from everyone else. Your colleagues will respond by trying to shoot down your ideas and drag you down to their level. The best way to handle these reactions is to just ignore them and stay focused on your own project.

When following your passion it's important not to worry about everyone liking you. There will always be people putting you down; the more success you experience, the more they'll dislike you.

Just consider Barack Obama. The second time he was elected president of the United States, he won with only 51 percent of the popular vote. So even though 49 percent of voters voted against him, he's still one of the most powerful people in the world.

It's unfortunate that the more successful you are, the more disapproval you'll receive. But you can overcome it by redefining what disapproval means to you, and taking it as a sign that you're doing well!

People won't just respond to your success with anger, however; they'll respond with ridicule as well. As such, it's also important not to worry about being laughed at. Just remember who'll be laughing when you're a success.

We wouldn't believe it now, but when a young Arnold Schwarzenegger declared he would be the world's biggest movie star, he was laughed at. As Schwarzenegger's success with _The Terminator_ launched him to become the highest-paid actor of his day, he laughed back plenty.

When Schwarzenegger entered the California gubernatorial race, people said he was crazy. But he rose to the challenge again and, following his dream, won the election.

It just goes to show that you can accomplish anything you choose as long as you disregard the approval of others and step beyond their mockery to follow your dreams.

### 4. Make sales your number one job. 

As a first-time entrepreneur, it's easy to feel overwhelmed by your innumerable duties. You're faced with a mountain of tasks — from picking the best vendors to ensuring quality to staying on top of finances — and every job can seem urgent.

Faced with this situation, it's essential to prioritize; if you have to concentrate on one thing only, that thing should be _sales_.

Sales should be your number one priority, because the popularity of a product is based 10 percent on its quality and 90 percent on how it's marketed and sold.

Just think of the best-selling products in any industry — rarely are they the ones of the best quality. For instance, the most frequented restaurant in the world is not a five-star Michelin-rated one. It's McDonald's.

It's clear that the quality of its food isn't what took McDonald's to the top. Rather, it's the company's constant attention to marketing and sales.

But maybe you're uncomfortable using the word "sell." Not a problem. Try replacing it with the word "help" and consider how your product aids people and addresses their needs. Knowing how your product helps customers is key, because a salesman's most important quality is empathy.

Just consider the great Miami real estate agent John Lennon, who once sold more than $3 billion of real estate within six identical buildings. What was Lennon's key to success? Even though all his units were the same, he never sold the _same thing twice._

What this means is that Lennon personalizes his sales pitch for every prospective buyer. If the client is a vintage car collector, Lennon talks up how amazing and safe the building's garage is, thereby showing his client how the property addresses his needs.

However you do it, you'll need to start selling to be in business. The success of your company depends on sales, so get out there and start making them!

### 5. Your employees are the key to your business – so hire the best, even if they’re better than you. 

What's the biggest cost of doing business? The average company spends 65 to 80 percent of its operating budget on salaries and wages.

With numbers like that, it's no secret that hiring the wrong people can cost you a fortune. But saving money isn't the only reason to prioritize finding the right people for the job.

People make a company what it is, and to build a high-performing business, you need high-performing employees. Even among companies that sell a generic product, there are average brands and excellent ones.

Airlines are a great example. Say you buy a transatlantic flight with United Airlines. You're sitting in a plane that's nearly identical to the one you'd be sitting in if you had instead purchased a ticket with British Airways or Air Canada. The difference, however, is the people working on the ground and in the air to make your flight the best.

Employees can make or break a company, so when building your business, make sure to select the right people for the right jobs.

But wait a second — as leader of the company, you should be the smartest person around, right?

Actually, your team should always be better than you. Take the advice of the CEO of a multibillion-dollar telecommunications company: make it your goal to be the dumbest person in the room.

Why? Because becoming successful doesn't require you to have extraordinary intelligence or ability, as long as you have the discipline to hire people who do.

If you're thinking of better marketing ideas than your marketing director, or better financial solutions than your CFO, you know it's time to make a change.

Building a business with a sound future depends on the ability of the team you recruit, as well as your ability to recruit them. Make your business a success by hiring the best of the best.

### 6. As leader you set the standards, so take responsibility and be a good example. 

Say you're the founder of a successful business. One of your employees, in an office across the country, snaps at a customer.

You should discipline the employee and his manager for their mistakes, right? Wrong!

As head of the company, you're responsible for everything, even the mistakes of your employees.

It's essential to take responsibility for your company because you're setting an example for everyone else. Your employees take cues from you, and you can't expect them to be any more disciplined, dedicated and focused than you are.

For instance, if the CEO of a company wears a suit every day, his employees will adopt his habit and wear suits too. Likewise, if the CEO is late to meetings, employees won't bother being on time either.

As the boss, you define the standards, so make sure the example you set is the correct one.

However, being responsible for the actions of an entire company can be difficult; sometimes you'll need to make decisions that aren't popular. To do what's right for your business, you'll need to be comfortable making decisions that your employees don't like.

Take Howard Schultz, CEO of Starbucks. In 2008, the company hit hard times and Schultz decided to close 600 locations, resulting in thousands of layoffs. He says that day was one of the most trying in his life. After all, he had worked with some of those people for over 15 years!

As leader of a company, you'll face emotionally challenging decisions. Success means doing what's right for the business, even if it's unpopular. Imagine if Schultz hadn't made the cutbacks he did. The company would have gone under and _everyone_ would have lost their jobs!

Even though it may seem like your employees aren't always listening to you, they _are_ always watching you. Make sure the example they see is a good one.

### 7. Productivity means prioritization. Get the most done by finding what’s important to you. 

Have you ever wondered how hyper-successful people do it? After all, everyone gets the same 24 hours each day and starts with about the same opportunities.

Yet there are entrepreneurs working harder and longer than even US business magnate Donald Trump without any great success. Why?

Being busy and working long, hard hours doesn't necessarily equal success. The missing pieces are clarity and focus.

To boost your productivity, you'll need to clarify your priorities. Start by concentrating your time, energy and resources on the things most important to you and avoiding the distraction of trivial matters.

After all, stopping ourselves from doing what we _shouldn't_ is as essential to productivity as doing the things we _should_.

Take Warren Buffet again as an example. When posed the question, "What's the single greatest key to your success?" his response was simple. He said that the key to success is the ability to say "no." For every hundred amazing opportunities he's presented, he has to say "no" to 99 and carefully pick the one and only "yes. _"_

Priorities are clearly essential, but how many is too many? A good rule of thumb is that if you have more than three priorities, you have no priorities. Luckily, naming your priorities is simple using Buffet's technique: Start by writing down all your priorities, narrow the list to three and throw the rest away.

Everyone's priorities are different. Although there are no _right_ priorities, using this technique allows you to decide which ones are right for _you_.

On the road to success, your resources are precious and it's vital to only spend them on the things that really matter.

So to make the most of your time, avoid getting distracted by things that _could_ be important. Instead, name your priorities and focus on the things you _know_ are important. Just delegate the rest to other people with the proper expertise!

### 8. Achieve your full potential by conquering your fear. 

As an entrepreneur, two things you'll experience every day are fear of the unknown and uncertainty. To run a successful business, you'll need to face these fears head-on.

Too many promising entrepreneurs have let fear prevent them from living up to their potential. Avoid their fate by learning how to overcome your fears _._

Start by understanding your fear. For instance, the things we're afraid of usually aren't that bad. It's the anticipation of our fear that really hurts, because it's what makes us blow our fears out of proportion.

This anticipation is a vestige of our native instincts to be on the defensive against predators. The problem is, even though no such literal threats exist today, our brains still tell us they do. Instead of fearing an encounter with a hungry lion, we fear giving presentations or making unpleasant phone calls.

Luckily, overcoming these fears is easy once you understand them. Just remember, when facing a customer or speaking to a large group, there's no mortal threat. The only danger is letting your fear get the best of you.

But sometimes conquering your fear is easier said than done. Give yourself an advantage by focusing on the task at hand and not the outcome it will produce.

Think of US basketball legend Michael Jordan. When the score is tied and he's taking the game's deciding shot, he's focused on that shot and nothing else. He's not thinking about the effect it will have on his career or the impact on the game. He's focused on doing the thing he's done a million times before: shooting a basket.

You can do the same. By focusing on one task and putting everything else out of your mind, you can conquer your fear and excel at your work. Overcoming fear is key to making it as an entrepreneur because it enables you to take bigger risks — and bigger risks mean bigger returns!

### 9. Become the person you want to be by following your dreams and trusting your judgment. 

Say you followed your dream and opened a business. You made the rules and did things your way. But as time passed, you drifted from your path; you saw other people's success and, doubting your own methods, started copying theirs.

Before you knew it, your business was struggling and your passion was gone. Put simply, you can't live somebody else's dream.

Consider Olympic gold medalist and Grand Slam-winning tennis player, Andre Agassi. Despite being one of the best tennis players of all time and ranking number one in the world, Agassi wasn't happy. When he got to the top, he realized he never wanted to be there. As his unhappiness continued, his ranking steadily fell to 141, and he tested positive for drugs.

The problem was that he wasn't the best because _he_ wanted to be, but because other people wanted him to be the best. He was living _their_ dreams, not _his_. He turned his life and game around by finding a new motivation — founding a school for disadvantaged children. And soon he climbed his way back up to first place again.

The trick? Agassi was doing it for himself and nobody else. You too can start living your own dreams today, beginning by trusting your own judgment.

Take Jeff Bezos, who years ago was a successful New Yorker with a high-paying Wall Street job. One day Bezos had the idea to open an online bookstore. He took the idea to his boss, who shot it down, deeming it too risky. But Bezos followed his own judgment. He quit his job and started the bookstore, which became a company you may have heard of: Amazon.

When faced with a difficult call, like whether to open your own business, ask yourself if you will regret not taking the plunge 30 years down the road.

By following your dreams and trusting your judgment, you put your future in your own hands and can become, today, the person you always thought you could have been.

### 10. Final summary 

The key message in this book:

**Most people opening their own business know it will be challenging. Between creating the best product, maximizing efficiency and serving customers, being an entrepreneur is a full-time job. What most people** ** _don't_** **know is that the real challenges lie beneath your main business responsibilities: becoming a successful entrepreneur also means conquering your fears, defining your priorities and following your dreams.**

Actionable advice:

**Define your number one priority and stick to it.**

At the start of every day, take a moment to consider the tasks you expect to accomplish. From these, choose one top priority for the day and put everything else aside. By concentrating on your goal and not starting anything else until you've attained it, you will accomplish more and feel better.

**Suggested** **further** **reading:** ** _The Hard Thing About Hard Things_** **by Ben Horowitz**

These blinks explain why the job of a CEO is among the toughest and loneliest in the world, and how you can survive all the stress and heartache involved.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Darren Hardy

Darren Hardy is an entrepreneurial guru. He is the publisher of Success Magazine, works as a motivational speaker and is the author of _The Compound Effect_, a motivational book about shaping your destiny through everyday decisions.

