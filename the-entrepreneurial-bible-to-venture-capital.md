---
id: 572c65f4d77ba6000351704a
slug: the-entrepreneurial-bible-to-venture-capital-en
published_date: 2016-05-11T00:00:00.000+00:00
author: Andrew Romans
title: The Entrepreneurial Bible to Venture Capital
subtitle: Inside Secrets from the Leaders in the Start-up Game
main_color: B5953E
text_color: 735E27
---

# The Entrepreneurial Bible to Venture Capital

_Inside Secrets from the Leaders in the Start-up Game_

**Andrew Romans**

_The Entrepreneurial Bible to Venture Capital_ (2013) is a must-read for any entrepreneur or business leader looking to fund their next great idea. Venture capital firms seek start-ups that show potential and often commit to the tune of millions of dollars. If you want to make it in today's competitive start-up world, you need to understand how venture capital works.

---
### 1. What’s in it for me? Understand the world of venture capital to secure start-up funding. 

Behind every successful start-up is a risk-taking investor who dared to jump on board at an early stage, when the challenges and potential for loss were greatest. A venture capitalist is by nature willing to risk it all, with the hopes of being part of the next Facebook or Twitter.

In these blinks, you'll get a behind-the-scenes look at the life of a venture capitalist to understand better how your start-up can secure the funding it needs to succeed. The goal is clear: you need to master the venture capital game if you want to win big.

In these blinks, you'll also discover

  * how to build a great management team to attract investors;

  * why success means having a great exit strategy; and

  * why you should seek an "angel" before knocking on a VC's door.

### 2. The growth in technology start-ups has inspired a boom in venture capital investing. 

If you've worked in or even founded a start-up, you probably know the importance of venture capital in realizing your business goals.

But what exactly is venture capital, and how does it work?

Venture capital is a form of private sector finance dedicated to helping new companies establish themselves and grow. "VC" firms usually seek out investment opportunities in growing markets such as information technology (IT) or biotechnology. In exchange for a cash infusion, the start-up gives the VC firm a significant share of company equity.

The market for venture capital has boomed in recent years, as ever more start-up companies are founded and require investment to grow. Today it's never been easier to launch a new company.

In the 1990s, things were different. Start-up costs were stiff, with fledgling companies having to put together tens of thousands of dollars to cover the costs of servers and software licenses.

As technological innovation has skyrocketed, the cost of starting a new company has fallen considerably. Cloud computing has slashed the cost of storage, as just one example. Today it can cost less than $5,000 to launch a beta version of a website or mobile app!

VC firms know that while there's plenty at stake, there is also serious potential in start-up ideas. While a traditional bank won't typically grant a loan to a start-up without a marketable product, VC firms are willing to put up the needed cash early, betting on a serious future payoff.

The term "venture," in fact, refers to the risks that VC firms knowingly take when they invest in a new company. Some 60 percent of start-ups backed by venture capitalists, however, go bankrupt before the start-up can pay back the investment.

In fact, just one out of ten venture capital investments turn out to be successful — but that one start-up just might be the next Facebook or Twitter!

### 3. Venture capital firms are built on partnerships with one single goal: to make a profitable exit. 

With the start-up boom, VC deals are being made all over the world. Yet how exactly does a VC firm function?

A venture capital firm is usually structured as a _limited partnership._ Most investment capital comes from limited partners (LPs) while the fund's general partners (GPs) invest that capital in various projects on a limited partner's behalf.

General partners also invest money in projects. Typically, if a general partner raises a total of $100 million from limited partners for a particular project, the general partner would probably throw in an additional $1 million to $5 million as a sign of the VC firm's confidence in the project.

VC firms ultimately have one goal, and that is to make a profitable exit when a funded start-up is either sold or goes public. A venture capital group will aim to pay back its investors a decent margin on their initial investments. Of course, the VC firm would like to earn some money itself if possible, usually around 20 percent of the final sale or public valuation.

A venture capital firm pays for its expenses by retaining a _management fee_ on the whole amount of its original investment, to the tune of about two percent annually _._

So who do you talk to when you want to reach out to a venture capital firm?

At the top of the hierarchy are general partners, managing directors and then partners. These are the people who make the big decisions and determine investment strategies. When you're ready to pitch your great idea, these are the individuals you want to talk to.

### 4. A cash infusion from an angel investor is often the first step to then securing more from a VC. 

Every start-up needs cash to get off the ground. One way to secure needed investment capital, especially when you're first starting out, is to find an angel investor.

Angel investors are individuals who personally invest in companies to help them grow. The first "angels" appeared in Los Angeles in the 1920s. At that time, wealthy individuals financed the first Hollywood films. The term "angel" later spread to other business fields, and wealthy donors willing to part with cash to benefit young companies were dubbed _business angels_.

Business angels have been growing in significance and power ever since. In 2011, for example, the total investment made by angel investors surpassed that made by venture capital firms.

Start-ups can gain a lot from angel investors. Angel investments provide needed financial support without demanding much in return. This stands in stark contrast to venture capitalists, who usually demand enough shares in a start-up to allow them to influence company decision-making.

Crucially, an investment from an angel can also provide lucrative networking opportunities, putting a start-up in a better position to secure further funding from a venture capital group.

Let's say your biotech start-up is working on a cure for cancer. Your costs are high, and you're looking for $1 billion in investment. But no single entity could provide such a sum. An angel investor, however, could provide a small investment to finance your first lab trials; and if those trials prove successful, your next step might be to pitch a VC firm.

Don't ask for too much when seeking funding from angel investors, however. It's unrealistic to expect $10 million from an angel. Angel investors usually invest from $500,000 to $1 million in a single project, just enough capital to push a start-up through its first year.

### 5. Venture capitalists look for start-ups with strong, well-balanced management teams. 

The golden rule of real estate is location, location, location. For venture capitalists, however, it's management, management, management.

A start-up's success depends on having a flexible yet skilled management team. It doesn't matter if the start-up has a great business plan. Realistically, the management team will have to revise that plan, changing directions to respond to market needs and pressures. They will always need to be ready to face unexpected challenges.

That's why VC firms rarely invest in ideas alone. It's a much safer bet to invest in a great team.

Most successful companies are founded on strong, balanced teams. In fact, when you look at Silicon Valley success stories, a pattern emerges: a successful company often springs from the combination of a visionary leader with a global perspective, a technician with the skills to turn the vision into reality, and a salesperson who can tailor a product to market expectations.

Computer graphic card pioneer 3Dfx was one such company. In the 1990s, the company's founding trio included a visionary expert on polygonal mathematics, a professor from the Massachusetts Institute of Technology specializing in 3D mathematics, and a senior sales vice president with years of experience.

This is the sort of team a venture capitalist wants. If a company is missing a key position, it's an indication that the management team might not be well-balanced. What's more, venture capitalists also question a start-up that can't attract at least one founder with a solid technical background.

In sum, if the management team doesn't impress, a VC firm might feel the start-up isn't yet ready for investment.

### 6. A start-up needs to find ways to grow value and be innovative to attract VC investment. 

Sometimes entrepreneurs can lose perspective and go on a spending spree after they raise needed capital. Let's review common pitfalls so you can avoid making this mistake.

Don't pour too much money into marketing early on. Start-ups should grow by creating a compelling product that fills a market niche. If you've done your job well, you shouldn't have to spend a lot of time or effort explaining your product — your product should speak for itself.

For example, Facebook, Uber and Paypal have never spent a lot on advertising. Instead, these companies focused resources on building value into their products.

True innovation is found when your start-up can identify a need that the market doesn't even realize yet. Steve Jobs of Apple is the classic example of this sort of thinking: he felt that most people didn't know what they wanted until it was shown to them. Market research will only get you so far; your idea needs to address not what people think they want today but anticipate what they'll need tomorrow.

An innovative concept alone won't guarantee success, however. You also need a plan for getting your concept to the right customers. Venture capitalists want to know how a start-up intends to build a community around a product or service.

Skype, for example, was particularly successful in this. When it was first developed, Skype was one of 200 start-ups offering a similar sort of telephony service. Yet the company set itself apart by placing a banner on Kazaa, a popular file-sharing service at the time.

The banner said, "Don't pay for your music, why pay for telecom?" This playful jab at "free" services appealed to the Kazaa community, and Skype's user base quickly grew.

Remember that your product has to have some viral potential; you can't "make" it viral after it's already on the market. YouTube, for example, allows anyone to upload a video and then embed it on another website. Thus YouTube-branded videos pop up everywhere online, a great viral concept.

### 7. Not only does a start-up need to build a great business, but it also needs a good exit strategy. 

A venture capitalist's core strategy is to invest in a start-up with the goal of turning a profit once that company is either sold or goes public. As an entrepreneur, you have to show potential investors that you're aware of their goals by presenting a good exit strategy.

The challenge for a start-up is to develop a sustainable business with steady revenue and loyal customers so that eventually, it can be sold. Not an easy task, and even Google struggled with it.

Excite, one of the first internet portals, in the early 1990s had the chance to buy Google for $1 million. Excite turned down the offer, as Google had no revenue, and nobody knew how to monetize search engines at the time.

You need to be aware of the ways potential buyers will view your start-up. The biggest deals are made when a buyer sees a target company as a strategic asset. Google acquiring YouTube was a strategic move, for example, as it sought to expand further into internet services.

As you put together an exit strategy, remember that the best deals probably won't come from number-crunching financiers. _Emotional buyers_, or those who have an urgent need to incorporate your company into their business, are the buyers you want.

An emotional buyer might be an established company losing market share, for instance. If your start-up offers a chance for the company to stave off decline, they'll buy no matter the price.

And if you're already on your way out of the market, an additional round of venture capital funding could help you better negotiate a sale. Twitter, for example, was interested in purchasing Instagram when negotiations began to stall. In response, Instagram raised additional funds, which boosted the company's overall market valuation. This inspired Twitter to finally make an offer.

Twitter might be sorry Instagram didn't take their offer, but Instagram was later sold to Facebook for twice that amount!

Sometimes investors are reluctant to buy a company until competitors show interest in doing the same. Your competition could help you seal better exit deals, so use this strategy to your advantage.

### 8. Venture capitalists don’t have the time to sit through lengthy presentations. Make it snappy! 

Venture capitalists are busy people. To secure needed financing, don't waste the precious time they grant you. Have all your materials ready before you meet to make a good impression.

Know that the days of lengthy, multiple-page business plans are long gone. Investors now want to see snappy, concise plans with clear goals. Entrepreneurs need to show that they can launch their ideas in the market and analyze results quickly — as well as be prepared to make big changes if needed.

Today's VCs deal with a ton of communication, whether email or otherwise, so they prefer bullet points that address big issues in few words.

You should be clear about your start-up's financial requirements, the talents of your management team, your current stage of development and future goals. Your business plan's _executive summary_ — just one or two pages, tops — should cover these key topics.

Once you're in the door, you can offer other supporting documents, such as the so-called _investor slide deck_. This is a set of ten slides that address your start-up's most important issues, such as your competition or company value proposition.

You also need to have a _financial model_ to illustrate the viability of your project. Your model should offer three to five years' worth of financial data, covering issues such as forecasted revenue, major costs and net results. All this should be clearly organized on a spreadsheet.

A good model will accurately describe the factors that will determine your company's profitability. Don't skimp on the details! If you're opening a restaurant, for example, you need to consider how many customers you'll serve, fluctuations in the price of raw materials, potential raises in the cost of rent, and so on.

### 9. Master a couple of different versions of your pitch to deliver it to the appropriate audience. 

One important detail that can play a huge role in your quest for financing is your _pitch_, or the brief summary of your project. You'll need to prepare a few different versions of your pitch to use on different audiences.

Use a quick, 30-second version at networking events, but have ready an in depth, two-minute version for people who want to know more. Prepare a 20-minute version for potential investors who want to know everything.

Don't skimp on your 30-second pitch, as it's the most effective way to grab people's attention. The founder of Half.com, an online marketplace for secondhand books, had a killer micro-pitch.

At a networking conference, Half.com's founder asked his audience which of them had read the latest bestseller. Nearly everyone raised their hands. Then, he asked how many people intended to read the book again — and nobody raised their hands. He then pointed out that everyone in the audience was a potential Half.com seller.

Don't confuse potential investors by mincing words. They want to know what makes you uniquely _you_, so don't rely on generalities to describe your idea. Avoid things like, "We're a mix between Facebook and Pinterest." That doesn't explain who you are or what your idea is.

Avoid buzzwords, such as "disruptive lean start-up." Which start-up isn't? Such buzzwords offer little insight into your actual concept.

Finally, learn how to tell a good story. Your investors want to be persuaded, so hone your speaking skills and bring your idea to life.

Don't be defensive when answering questions, either. It's normal for investors to ask for more details about an idea or plan. If you bristle at questions, you might come off as nervous or unprofessional.

### 10. Final summary 

The key message in this book:

**Venture capitalists have become increasingly influential in the start-up scene, as nearly every major technology company launched in recent years was funded by a VC firm. Aspiring entrepreneurs need to know what VC firms are looking for and how to best present themselves to them. So hire a great management team, learn how to pitch and show that you understand a VC's goals regarding exit strategies. Securing funding is a crucial part of bringing your start-up dream to life.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Venture Deals_** **by Brad Feld and Jason Mendelson**

_Venture Deals_ offers insider insights into the mechanisms that govern venture capital deals as well as tricks that will help you get the most out of negotiations with investors. It lays out the nuts and bolts of venture capital deals in a way that is both easily understood and will give you an edge at the negotiations table.
---

### Andrew Romans

Andrew Romans is the co-founder of Rubicon Venture Capital, a venture capital firm that specializes in early-stage investment opportunities for start-ups in Silicon Valley.

