---
id: 56a5d555ce4ca90007000002
slug: the-entrepreneurial-state-en
published_date: 2016-01-25T00:00:00.000+00:00
author: Mariana Mazzucato
title: The Entrepreneurial State
subtitle: Debunking Public vs. Private Sector Myths
main_color: 921F1D
text_color: 921F1D
---

# The Entrepreneurial State

_Debunking Public vs. Private Sector Myths_

**Mariana Mazzucato**

_The Entrepreneurial State_ (2014) sheds light on the state's role as a daring investor in emerging technology. From the iPhone to sustainable energy, these blinks reveal the central role governments have played in creating the world we know today, and how they can help to secure our planet's future.

---
### 1. What’s in it for me? Learn why we need an entrepreneurial state. 

Never before have we faced such daunting global challenges. Climate change alone will require international cooperation and political ingenuity on a scale we have not seen before. It will also require innovation. Lots of innovation. But who should be doing the innovating? And what should the state's role be in all this? To leave it to corporations and the market to sort out? Or to take a more active role?

As these blinks will show you, there has never been, nor should there be, a clear-cut line between the state and entrepreneurship. They go together like the proverbial peanut butter and jelly. 

In these blinks you'll learn

  * who really invented Google's search algorithm;

  * why we can't leave it to corporations to develop green technology; and

  * what Apple owes the American taxpayers.

### 2. Believe it or not, the state has proven itself to be a savvy investor. 

It seems like market leaders Google, Facebook and Apple announce stunning new innovations every week. It's hard not to be impressed by these top-performing companies. But should we really give them all the credit?

Believe it or not, those companies and even technology itself wouldn't be where they are today without the help of one key player: _the state._

Take Google, for example. For the past decade, Google has been one of the most influential companies on earth. It's an innovative force that controls the way we use the internet. Google's domination began with one innovation in particular: its search algorithm, an invention that Google alone was responsible for . . . right?

Wrong! The research and development for Google's winning algorithm was made possible through _state funding_. The private sector didn't have a hand in it — investors were simply too scared to back Google's revolutionary project. 

This isn't the only case where the public sector has displayed remarkable foresight. And yet, why are public opinion and even economists so strongly against public sector involvement in the market?

The truth is that history has been warped. While the achievements of the state have been celebrated as the victories of an innovative and daring private sector, the failures of the private sector are portrayed as fatal mistakes made by the state. 

Take the financial crisis of 2007, for example. Most of us would blame that devastating market collapse on overwhelming public sector debts. In 2011, British Prime Minister David Cameron even vowed to give the private sector more freedom to combat the ill-advised government investments and convoluted bureaucracy that seemed to be the root of the crisis. 

And yet, we should know by now that the greatest contributor to the crisis was in fact the _private sector_. Companies desperately trying to increase their profit, most notably in the US real estate market, spiraled out of control. This created a massive financial bubble, which caused the crisis. 

With this in mind, it's about time we reconsidered the role of the state in business.

### 3. The entrepreneurial state is a risk-taking, pioneering investor. 

Nobody ever found success without taking some risks. If we want our economy to succeed, we can't just play it safe. Without taking the plunge and investing in innovative and unknown technologies, we'll never see the progress we dream of. So who should take on these risks?

Many young and bright companies turn to venture capital for funding and support. But is this the right way to go? Sure, venture capitalists are more than happy to pump money into projects that seem profitable, but when it comes to groundbreaking innovation and research, venture capitalists are just too scared of taking a hit. 

After all, true innovation doesn't happen overnight. It takes years of development, and it may not be clear if profit is possible at the end of the journey. Rather than taking that risk, venture capitalists prefer to wait for somebody else to step up to the plate and fund these radically new ideas. 

Luckily for us, the state has proven itself to be the daring risk-taker we need to drive technological advancement. In the previous blink, we learned how the state made it possible for Google to develop their search algorithm. But did you know that the US government also helped bring the internet into existence?

The US Department of Defense saw the enormous potential in being able to exchange information "online" and spent years supporting the development of the internet technology that is now a central part of our everyday lives. 

The US government then realized that it was time to embrace its potential as an innovator, even if this meant taking huge risks.

In 1982, the Small Business Innovation Research program (SBIR) was launched. SBIR allocated annual funds to small firms seeking research funding to allow the development of new technologies. Today, SBIR directs $2 billion to pioneering tech companies and start-ups. 

Even technology giant Apple has the state to thank for some of its success. The iPhone, seen as the culmination of Steve Jobs' genius, offers many different services. And yet many of the iPhone's capabilities, such as the internet, GPS, touch screen and other communication technologies are innovations made possible by state funding. 

As we can see, the state is one of the most vital players in technological development. This is something to recognize as a new wave of technology appears on the horizon. It's not just communication technology that will continue to benefit from state investment, but _green technology_ too.

### 4. Entrepreneurial states will play a crucial role in advancing green technology. 

Global warming is real, and has already begun to have radical impacts on our planet. Although fossil fuels are running out and the Earth's climate grows more volatile, our technologically advanced world demands more energy than ever. In other words, we've got a huge problem on our hands. 

Green technology is our only hope for fulfilling growing global energy needs and keeping climate change in check. By converting solar, wind and water power into electricity, we need no longer rely on finite resources such as coal and fossil fuels that damage our environment. 

Sounds good, but there's one hurdle still to jump: green technologies need to be developed if they're to keep up with our overpopulated, globalized world. Developers need further research, and further research requires more funding. 

Yet venture capitalists are unwilling to lend a hand. They fear — more than climate change itself! — that they won't see a return on their investment in green technology. Enter the entrepreneurial state. Governments in several nations are all stepping up to help green technology grow. 

Green technology also requires continuous legal and economic support so that it has a shot at success on the energy market. Until green technology is cheaper than other more damaging energy sources, it won't take off. 

Governments in Germany and China recognize this, and are leading the world in their entrepreneurial support of green technology, investing millions each year toward a more sustainable future. And it works: in Germany between 2000 and 2014, the energy from renewable sources, like wind, solar and biogas, increased from 6.3 percent to 30 percent.

Whether we like it or not, green technology is the future. States must get on board before it's too late.

### 5. Citizens and the economy should reap the benefits of state-funded successes. 

We've learned that the iPhone as we know it today wouldn't be here without state-funded technologies. If you think Apple doesn't give the state credit for this, you'd be right. 

The problem with Apple is that, although it makes huge profits, it doesn't give back much to the American economy. Many companies are granted funding under the assumption that they will then go on to create new, well-paying jobs and offer workers skill-development opportunities.

Yet Apple creates very few jobs in the United States. Most assembly takes place overseas, while Apple store workers rarely earn more than a Wal-Mart sales clerk. Having achieved huge financial success, Apple can certainly afford to pay their US workers more. But rather than raising wages, they recently chose to distribute $45 billion among their shareholders. 

Rather than letting private sector companies present themselves as the genius risk-takers, governments must emphasize how taxpayer money is being put toward the development of technologies that will make our world more efficient, convenient and sustainable. 

To do this, the state must take steps toward claiming part of the rewards earned through their savvy investments, for example by increasing taxation on companies benefiting from them or claiming royalties on innovative technologies. Governments should also prevent companies from shifting their profits to other countries with little taxation, while also raising public awareness about the role of the state in technology and business today. 

The entrepreneurial state will be an important part of tomorrow's economy. Research and risk-taking investments will continue to push innovation in groundbreaking new technologies that will connect our world and protect our planet.

### 6. Final summary 

The key message in this book:

**Game-changing technologies that have transformed the way we communicate today would not have been possible without state investment. By playing the role of the risk-taking, entrepreneurial investor and ensuring that their citizens see a return on government investments, the state can drive the advancement of renewable energy technology.**

**Suggested** **further** **reading:** ** _The Zero Marginal Cost Society_** **by Jeremy Rifkin**

_The Zero Marginal Cost Society_ (2014) lays out a strong case for the self-destructive nature of capitalism, demonstrating how it is sowing the seeds of its own destruction. But in its wake, a new, collaborative, democratized economy will materialize — one made possible by the internet.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mariana Mazzucato

Mariana Mazzucato is a renowned professor of Economics of Innovation who teaches at the University of Sussex. _The Entrepreneurial State_ was the _Financial Times_ and _Huffington Post_ Book of the Year.

