---
id: 573465f7e2369000032901ff
slug: the-essential-hr-handbook-en
published_date: 2016-05-18T00:00:00.000+00:00
author: Sharon Armstrong & Barbara Mitchell
title: The Essential HR Handbook
subtitle: A Quick and Handy Resource for Any Manager or HR Professional
main_color: 2EC1E4
text_color: 1C778C
---

# The Essential HR Handbook

_A Quick and Handy Resource for Any Manager or HR Professional_

**Sharon Armstrong & Barbara Mitchell**

_The Essential HR Handbook_ (2008) is a guide to human resources management. These blinks are full of useful tools and important insights on how to manage your organization's most important resource: human capital.

---
### 1. What’s in it for me? Become a better talent manager. 

In any business context, regardless of the products or services you offer, one rule applies across the board: people matter. When it comes to corporate success, your employees are your single most important resource — and one of the costliest, as well.

So how do you make sure that you find the most talented people for each position, and also stoke their passion to do their best and keep improving? Those are only some of the questions these blinks will answer for you.

You'll also learn

  * why you can forget about running newspaper ads if you're fishing for real talent;

  * how to find out whether an expensive training program is any good; and

  * how to give truly worthwhile performance reviews.

### 2. Successful organizations align human resources planning with their overall strategy. 

When you think of a human resources department, do you imagine an isolated, hidden office with a set of duties limited to simply hiring and firing?

Well, think again; _human resources_ or _HR_ is much more than recruitment and layoffs. In fact, HR is responsible not only for the acquisition, training, appraisal and compensation of an organization's employees, but also for dealing with issues like health and safety.

So, if an organization truly wants to be successful, it needs to link its HR planning to its greater organizational strategy. For instance, it's important for every organization to build a strategy that lays out exactly how its goals will be achieved. To make these goals a reality, the managers then allocate available resources.

And here's where HR comes in, because the people working in the organization are one such resource — and a very important one at that.

After all, a typical human resources department will generally receive a hefty chunk of an organization's budget to spend on things like salaries and benefits. So, if the organization fails to put its HR planning in line with its overall strategy, broad success will be hard to achieve.

For example, if an organization hasn't put together a strong benefits package for its current and future employees, it might struggle to attract the high-quality talent it needs to succeed.

It's important to keep HR updated on all organizational targets; doing so will help the department understand the role employees are meant to play in achieving these goals, which is the first step in determining whether the right people are on staff, and who still needs to be hired.

> _"If you don't know where you're going, how will you know when you get there?"_

### 3. Optimal staffing means outpacing the competition and focusing on an applicant’s prior experience. 

It used to be that talent hunts could be conducted simply by running an ad in the newspaper — but times have changed. These days, you have to keep your competition in mind when attracting job applicants.

Job seekers of today have much wider access to open positions through the internet. So, while your website is helpful, remember that applicants will be accessing those of your competitors as well, not to mention general job sites like _monster.com_.

You'll have to deal with the competition one way or another, and to do so you'll need to let applicants know why they should apply to work for you and not someone else. This means clearly stating the benefits you offer and outlining exactly why working at your organization is so great.

Once you've done that, you need to decide who you want to invite for an interview by reviewing resumes, comparing them to the specifics of the position and conducting brief screening interviews over the phone.

You should begin by understanding the position you're hiring for, which means determining what will be expected of applicants, what goals they need to achieve and what skills will be essential to their job. After those bases are covered, you can screen for basic requirements; for instance, is the applicant qualified and what are their salary expectations?

After all, if they don't meet these basic conditions, it's not worth your time to interview them in person.

Finally, you've gotten to the fun part: conducting a _behavioral interview_. This type of interview assumes that an applicant's past performance is the best indicator of how they'll do in the future. For such an interview, you'll want to formulate questions that bring past performance to the fore and show you whether the candidate is a good match for the position.

So, if you need someone who is flexible and adaptable, you might ask, "In previous jobs, did you ever have to change rapidly between different tasks? If so, how did you handle it?"

### 4. For a seamless transition, provide new hires with proper orientation and onboarding. 

So, you've found the perfect person for an open position and it's his first day on the job. What should you do?

Well, once you've brought a new employee on staff, it's crucial to get him oriented at the organization. If he doesn't know the basic facts of what _you_ do, it'll be difficult for him to know what _he_ was hired to do.

And remember, an orientation isn't just a rapid overview of the organization; rather, it should accomplish several key objectives. One critical facet of the orientation is to make sure the new employee understands the organization broadly — that is, in terms of its history, corporate culture and vision — as well as in a detailed sense, such as how specific procedures work and how certain departments operate.

Once you've done that, you should incorporate the new hire into the company and make sure he's brought on board as efficiently as possible. If an organization wants to keep the employees it hires, it needs to make sure they're onboarded properly.

For instance, using Laurie Friedman's _Strategic Business Consulting_ as a benchmark, the authors explain that organizations have plenty of great opportunities to win over a new employee. In fact, one of them presents itself right away.

It's extremely beneficial if the organization makes a good _first impression_ — after all, first impressions tend to be the ones that last.

Early on in an interview, the prospective employee will make up his mind about the organization, determining whether he feels welcome there, among other things. So, if the organization makes a good impression here, it stands a much greater chance of holding on to the employee if and when he is hired.

For example, the space in which the interview is conducted should be pleasant and comfortable, and careful attention should be given to the environment the prospective hire is walking into.

### 5. Training is essential for new hires. 

Once you've hired, oriented and onboarded the right people, your work is done, right? Actually, there's still a fundamental step left in the hiring process: training.

Training is essential for both an organization and its employees. However, there are lots of different ways to do it, since different needs call for different forms of training. It's in everyone's best interests that employees are constantly developing their skills.

Any organization will want competent, well-trained employees, and for them to be invested in developing their professional skills in order to stay competitive. So, while there are lots of different trainings out there, here are two particularly useful types:

The first type is used to ensure employees, usually new ones, gain the skills necessary for the organization at the present moment. For instance, a new customer service representative will need to learn how to deal with customers in a polite, effective manner.

The second type is for people who have been with the organization for a long time, but need additional training that adapts their behavior to organizational needs. For example, maybe one employee is not a great team player, but their current job requires much better teamwork skills.

Then, once you've conducted a training program, you should take stock of how effective it was. Luckily, there's an easy system for doing so: the four-level model developed by Professor Emeritus Donald Kirkpatrick in 1994.

Here's how it works:

Level 1 is _reactions_. Here you want to identify how the employee responded to the training; did they like the material? Was it relevant?

Level 2 is _learning_. Put simply, did the employee's skill level increase? To determine this, you can conduct pre- and post-training tests to ascertain the degree of change.

Level 3 is _transfer_. The aim here is to determine how much of the newly acquired skill gets transferred to an employee's actual work.

And finally, in level 4 you look at _results_. For this step, you measure the extent to which the training resulted in increased quality, sales or any other measure you choose.

If you determine the training program was ineffective, you can change it the next time.

### 6. Feedback, both positive and corrective, is essential. 

For plenty of people in HR and management, monthly employee evaluations are a source of real stress and anxiety. But don't worry — if done correctly, _performance evaluations_ are a huge benefit to your organization, your employees and yourself.

How?

First of all, evaluations are a chance for you to tell an employee about the organization's goals, and doing so will definitely increase their productivity. Knowing exactly what she is working toward will sharpen an employee's focus and provide her with a motivating sense of purpose.

But these evaluations will also help you hone your managerial skills and build a good rapport with your employees. After all, a good performance evaluation is a dialogue, not a lecture. Engaging in one is a chance to make your employees appreciate you and your listening skills.

Finally, the evaluation process is a chance for employees to learn how they're doing and identify specific changes they can make to improve.

So, there are plenty of good reasons to conduct performance evaluations and to do so often. But to make these evaluations truly worthwhile, you'll have to give both _positive_ and _corrective feedback_ in the right way. Here's how:

Positive feedback is used to encourage and reward employees. You can use the acronym _FAST_ to guide you. It stands for _frequent, accurate, specific_ and _timely._

Positive feedback shouldn't be rare or random; instead, it should be given out _frequently_, with _accuracy_ and using _specific_ information to avoid generalizations. But it's also important for positive feedback to be _timely_, which means you shouldn't offer it while having a casual lunch or on the way to a meeting.

The second form, corrective feedback, can be a bit more challenging. Here you can use the acronym _BEER_, standing for b _ehavior, effect, expectation_ and _results._

You'll want to start by addressing the exact _behavior_ that's problematic, explaining the negative _effect_ said behavior has on the organization and what you _expect_ instead. And to close this constructive criticism on a positive note, you can tell the employee what positive _results_ will come from altering their behavior.

> _"Employees need to know where their organization is headed, and how the work they do fits into the plan."_

### 7. Be prepared for change: an increasingly diverse workforce and technological innovation are transforming management. 

Does every one of your employees have the same attitude, nationality and gender? Of course not, and the workforce is growing more diverse by the day. A 2004 study done by the consulting company Hewitt Associates predicted the following for future US workforce trends:

By 2008, 78 percent of the workforce was expected to be female and/or belong to a minority group. And by 2014, 75 percent of the United States' new workforce was expected to come from Asia, making it increasingly likely for western corporations to employ people of diverse ethnic backgrounds.

So, while we have obviously passed 2014 by now and these estimates should be checked for accuracy, the trend is apparent: the American workforce is growing in diversity, and is shifting in both conspicuous and subtle ways, such as its thinking patterns and religious beliefs.

So what does this have to do with you?

Well, it's your job to manage diverse employees by ensuring an inclusive environment, one that accommodates all employees regardless of their knowledge, skills and abilities.

For example, when you communicate with your employees, it's essential that everyone understands you even if they might not be fluent in the language you're using. After all, it's hugely important that a new Chinese engineer on your team understand the procedure manual just as well as everyone else.

Diversity is on the rise and, simultaneously, technological innovation is transforming the workplace. However, just because technology exists, this doesn't necessarily mean it should be used. Your job is to determine how any given technology might help your company operate and adapt your investments as well as your policies to suit it.

Technology has, for example, transformed the way we communicate. In many cases, face-to-face meetings have grown obsolete since you can just have a quick Skype call, leave a voicemail or shoot over an e-mail.

But in some cases, technology is no substitute for meeting in person. As a manager, it's your responsibility to know when that's the case and have clear communication policies.

### 8. Final summary 

The key message in this book:

**Human resource management is often underappreciated and left to fall by the wayside — but it's actually a core aspect of any organization. As such, for any organization to be successful, HR should be well integrated into its daily work.**

Actionable advice:

**Act fairly and according to procedure when firing people**.

As an HR manager, it's inevitable that you'll have to fire people. Here are a few tips on how to do it in the best possible way:

  * Always make sure the employee knows the reason for the termination of his or her employment; if the reason is poor performance, let them know so they have the chance to improve at their next job and into the future.

  * Remember, if your organization has a standard procedure, you should follow it to avoid any possible claims of violation of contract.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Investing In People_** **by Wayne Cascio and John Boudreau**

_Investing in People_ (2011) shines a light on human resources, a crucial department in a successful company that too often is undervalued and underappreciated. A savvy personnel strategy can fine tune a company's performance and boost employee well-being. The book provides an easy-to-follow, four-step process to improving your human resources strategies.
---

### Sharon Armstrong & Barbara Mitchell

Sharon Armstrong is the founder of the consulting business Sharon Armstrong and Associates, and works as a human resources consultant and trainer. She is the co-author of two successful business books, as well as the light-hearted _Healing the Canine Within: The Dog's Self-Help Companion._

Barbara Mitchell advises businesses on human resources and organizational development. Before becoming a managing partner of the consulting practice The Mitchell Group, much of her career was spent as an HR specialist at Marriott International.

