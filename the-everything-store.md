---
id: 5304807a34393700083d0000
slug: the-everything-store-en
published_date: 2014-02-19T10:28:44.000+00:00
author: Brad Stone
title: The Everything Store
subtitle: Jeff Bezos and the Age of Amazon
main_color: C8A781
text_color: 806A52
---

# The Everything Store

_Jeff Bezos and the Age of Amazon_

**Brad Stone**

Despite being a billion-dollar company today, Amazon was built on humble beginnings in Jeff Bezos' garage. From the get-go, Bezos was driven by the grand vision of creating an _Everything Store_ — which has, in the meantime, virtually come true. Focusing equally on the company and its founder, this book shows how he turned his dream into a reality.

_The Washington Post_ and _Forbes_ both dubbed _The Everything Store_ the best book of 2013.

---
### 1. What’s in it for me? Learn from the maker of The Everything Store 

In 1994 only a handful of people understood the possibilities the internet would hold in the future. Back then, Bezos was working for the hedge fund D.E. Shaw & Co., where he and several colleagues dreamt of an online company that would one day become the central interface between producer and consumer, and sell just about every product under the sun.

Impressed by the rapid spread of the internet, he quit his well-paid job to devote himself entirely to realizing his vision of an online company that would sell everything. He picked up and moved to Seattle, and launched Amazon in his garage.

However, he quickly realized that he wouldn't be able to pull his _Everything_ _Store_ out of a hat. He would have to limit himself to just a few product categories in order to establish his product range — at least in the beginning. After a thorough analysis, he decided on books.

What followed was an unequaled success story — one that brilliantly reflects Jeff Bezos' idiosyncratic way of thinking. And that's what you'll read about in _The_ _Everything_ _Store_. In addition to the secrets to Amazon's success and the sorts of ideas that influenced its founder Jeff Bezos, you'll also learn about some of the negative aspects of the company's story.

### 2. Everything for the customer: an unlimited focus on service and comfort. 

From the very beginning, Amazon has championed taking customer orientation to a higher level in every industry worldwide.

The online store has, step by step, added new features and functions that people initially considered unfounded, but were always conceived to benefit the customer.

For example, despite resistance from publishers, Amazon introduced the customer review feature that provided users with independent and often critical information in addition to publishers' blurbs. Similarly, the decision to give sellers and individuals the option to sell used products also met with internal opposition at first, but ended up being just what the customers wanted.

What's more, Amazon is constantly working on optimizing its logistics and delivery systems in order to meet customers' desire to receive the products they've ordered as quickly as possible. The complexity of Amazon's fulfillment centers is hidden from the customers' view.

Jeff Bezos quickly recognized one huge advantage e-commerce had over traditional business: that it was extremely easy to analyze customer behavior, which Amazon now does almost obsessively. Every time customers access the site, product recommendations pop up based on their previous site behavior. This feature has consistently yielded increased sales as customers discovered relevant products that they wouldn't have stumbled upon otherwise.

Clearly, Jeff Bezos and Amazon are driven by an almost compulsive customer orientation. And Amazon — rather immodestly — formulates this in its business model as follows: "Our goal is to be Earth's most customer-centric company."

In fact, at the company's office nothing could be more frightening than when Jeff Bezos forwards an e-mail with a customer complaint to which he's only reacted with a "?".

### 3. Less is more: excessive frugality all around. 

Perhaps this slogan doesn't seem appropriate for the billion-dollar company that Amazon is today, but it fits a company that started in a garage and has had to struggle on the commercial battlefield with minimal profit margins. To this day, Amazon is characterized by a frugality that some might perceive as excessive. However, Jeff Bezos is convinced that limitations provoke innovation and frugality helps the company concentrate on the most important things, e.g., customer satisfaction.

Amazon's employees have to pay for their parking permits themselves; there are no free snacks at work; on business trips, they have to sleep in double rooms and managers even have to pay for their own flights. In general, the corporate culture at Amazon is harsh and has a fiercely competitive structure. The motto is: "You can work long, you can work hard, you can work smart, but at Amazon you can't choose two out of three."

Amazon's fulfillment centers display this quite clearly: poorly paid employees have to walk distances of up to 30 kilometers per day to retrieve goods. Despite all the movement within these colossal halls, they are extremely silent — employees can be fired for talking with each another.

Finally, since its inception, Amazon has repeatedly hired tens of thousands of temporary workers — especially during high seasons, such as Christmas time — only to let the majority of them go afterwards. Amazon's approach is simple: it establishes fulfillment centers in economically weak areas, which initially meet with approval, since many believe that they will improve the local economic situation. But Amazon is merely exploiting the opportunity to hire cheap labor and fires the workers later when the high season is over — knowing well that the region will have more temporary workers on standby the next time the company needs them.

### 4. Unusual, yet effective: a unique corporate culture 

Amazon's corporate culture is unique: for example, nobody gives presentations at internal meetings. Instead, employees have to write a six-page paper explaining their ideas, which all the meeting participants, including Bezos himself, have to read in complete silence — a task that can last up to 30 minutes. Bezos believes that his employees are thus forced to contemplate their ideas critically, which helps them present the ideas more persuasively.

Another Amazon quirk is the _two-pizza_ _rule_. No team in the company should have so many members that it can't be fed with two pizzas. Bezos considers meetings of big groups unproductive, which is why the whole company is organized in autonomous units with fewer than ten employees per unit. And these groups compete with one another for resources. They all have the task of solving the biggest problems that stop consumers from being a tad happier.

Bezos was apparently once heard saying, "Communication is terrible!" He wants a decentralized organization in which small groups can develop innovative ideas rather than wasting time in brainstorming sessions with too many participants. He believes that agility is the main advantage of small groups. They can implement ideas more quickly so they end up where they belong more quickly — benefitting the client.

The bulk of Amazon's meetings are extremely data driven: all employees need to substantiate their arguments and theses with hard data. In other words, these meetings don't revolve around customer anecdotes, but big Excel sheets that contain key performance indicators relevant to the whole company. Everything is evaluated using numbers — from customer behavior to the effectiveness of marketing measures — nothing if not true to the maxim that "numbers don't lie."

### 5. Not tomorrow, but in 20 years: the importance of long-term thinking. 

In Bezos' words, Amazon's greatest strength is its willingness to not be understood by others.

To a certain extent, this also applies to its business model. Many outsiders talk about the fact that Amazon makes frequent short-term losses. What they don't take into account is that the company is thinking in the long term and is happy to accept short-term losses if they end up helping the company achieve its future goals.

For years, Amazon put a lot of money into developing its infrastructure, and many investors lost sleep over the company being in the red. But today it's clear that these enormous investments made it possible for Amazon to solidify its position as a universal online retailer — which is practically a license to print money later.

Bezos preaches that just about anything you have to do to proactively make customers happy is fair game. Even if it costs money in the short term, it creates a loyal clientele that will ultimately bring money into the company.

The e-book makes for an interesting example. When the e-book started getting popular, Bezos decided to sell them for $9.99 per book. Since Amazon bought copies of the e-books for the same price as their printed equivalents, the company lost around $5 per book sold. Bezos accepted this because he was convinced that publishers would have to lower their prices sooner or later and he wanted to establish Amazon as the go-to marketplace for e-books. This was ultimately how the business hit the billion-dollar mark and made room for the sensational success of the Kindle.

The fundamental difference between Amazon and other companies is its extreme focus on the long term. From the start, Bezos was driven by an immense vision and he knew that it would take decades to come to fruition.

### 6. Beyond Amazon: Bezos’ private projects 

Jeff Bezos' private and non-Amazon projects are also a testament to his long-term thinking.

He's currently financing the construction of an underground clock in Texas that is designed to run for 10,000 years with hardly any maintenance. Danny Hills, the inventor of the clock, says that the Clock of the Long Now will only tick once per year. The "century hand" moves once every 100 years and, after every millennium, a cuckoo comes out of the clock and sings.

If all goes as planned, this iconic creation will become a major attraction. The idea behind the clock is to give people a feeling for wider time horizons and to defend long-term thinking. Similarly, it wants to change our perspective of time — in the same way that photographs of the earth taken from outer space gave us a new understanding of space.

At the moment, Bezos' most well-known project is the _Blue_ _Origin_ space program, which focuses on the development of technologies that will make it possible for people to fly into space for far less money and with less difficulty than ever before. Step by step, Blue Origin's technologies will build on each other and enable more and more flights. The project's long-term goal is a permanent presence of humans in outer space.

Ever since childhood, Jeff Bezos' dream has been to fly to outer space — his motivation for the rest of his later efforts to get rich.

### 7. Learning from mistakes: rewards for the doers and the brave 

During the dot-com boom in the late 1990s, Amazon lost hundreds of millions of dollars buying start-ups on the verge of bursting. Amazon took a rough fall, but the company learned its lesson — it became much more careful when acquiring companies and developed a do-it-yourself culture. It started to make its own products rather than acquiring them from others.

The company has a very bold approach: it's not afraid to jump into things before analyzing them down to the last detail. Even if that approach results in mistakes, Bezos prefers it to overanalyzing everything first, which often means losing lots of opportunities to try something new that might work.

Bezos is a bona fide _doer_. His decision to found Amazon in 1994 speaks to that. Most other people would have kept their well-paid job as a hedge fund manager rather than packing up camp and starting an online bookstore at the other end of the country — financed entirely by their own savings and those of their parents.

Since Amazon's founding, Bezos has always encouraged his employees to risk failure and try new things, which has led to huge setbacks on occasion. One such flop was Amazon Auctions, established in 1999 and unable to hold its ground against eBay. It was discontinued within months. In many other cases, though, taking risks has yielded brilliant innovations such as Amazon's 1-Click ordering option.

Bezos even went so far as to institute a "Just Do It" award for employees who implemented something remarkable on their own initiative, preferably outside of their own area of activity. The award can also go to employees whose attempt ended in failure, as long as they had acted with resolve and courage in the process.

In line with its thriftiness, Amazon does not award cash prizes, instead giving the winners a pair of huge Nike sneakers that belonged to basketball players.

### 8. More than you think: offers in unexpected areas 

Once Bezos managed to establish Amazon as an online book retailer, it started to sell music, films, electronic devices and toys. Then he offered third parties an opportunity to sell their own goods, both used and new, on Amazon. Finally, thanks to the Kindle, Amazon also became a central figure in the world of e-books.

What most people don't know is that Amazon offers a number of additional services, such as the cloud computing service Amazon Web Services (AWS), which turned Amazon from being a purely commercial enterprise into a mix of a store and a tech company. Many businesses, the US government, NASA and the CIA buy storage space and computational power via AWS. The service is the backbone of many online start-ups and provides servers for companies like Instagram and Netflix.

Amazon also changed its own image with AWS: all of a sudden, its customers weren't only avid readers, but also start-up developers buying terabytes worth of space to solve some of the world's most exciting problems.

The Kindle also underlines Amazon's ability to continually recognize and satisfy new customer needs — even before the competition catches wind of them. Bezos realized very early on that his customers were going to need an e-reader in order to read e-books, which were bound to become more popular with time.

Amazon got to work on this issue and developed the Kindle. In November 2007 the first generation of Kindles was launched — and within six hours they sold out (and stayed that way for five months). The Kindle is still a bestseller today. In 2011, Amazon reported that it had sold well over a million devices — and had been doing so every week.

### 9. The path to the Everything Store: always good for a surprise 

After almost 20 years of constant growth since its launch in Jeff Bezos' garage, Amazon is coming closer to achieving its original vision of creating an _Everything_ _Store_.

Still, Bezos doesn't see that as a justification to sit back and rest on his laurels. As far as he's concerned, there are still many challenges that need to be addressed before his long-term vision can be fulfilled. He would like to see Amazon provide same-day delivery, and have its own vehicle fleet (with trucks) and grocery business (Amazon Fresh); he also wants to turn Amazon itself into a publisher and media company, to build an Amazon film studio, to produce Amazon smartphones and televisions and, finally, to expand to new countries and maybe even offer a 3D printing service.

Bezos believes nothing is impossible; there's nothing Amazon can't do, no product it can't sell online. He thinks that there's simply too much left to invent, too much waiting to be discovered in the future — and most people still haven't the slightest idea of what the internet will make possible. For him, the journey has just begun.

Jeff Bezos' unique way of thinking made Amazon what it is today: a company that is constantly evolving, one that rewrites the rules day after day and will never stand still. After many grueling years, it has essentially become the _Everything_ _Store_. Even if its founder thinks he's just taking off on his journey, right now the company's turnover is already at $75 billion per year.

### 10. Final Summary 

The main message of this book:

**Strong customer orientation, long-term thinking and the drive to evolve and improve are the qualities that make Amazon what it is. The company's unequaled success can undoubtedly be traced back to the way of thinking promoted by its founder, Jeff Bezos. He stands out in particular for his willingness to take risks and try new things, as well as for his future-oriented thinking, which is also exemplified by his other projects, such as a private space program and a 10,000-year clock.**

Actionable ideas from this book in blinks:

**Rethinking the meeting.** As Bezos once said, "Communication is terrible!" — Why don't you try running your business in line with his philosophy? For example, by only making decisions based on hard facts or by forcing your employees to put their ideas in writing so they think them through before presenting them? If speakers think through their ideas so thoroughly that they can fill six pages of text and all other participants read them in complete silence and take these ideas to heart — will it steer the discussion in a much more sensible direction than in most companies?

**Two-pizza teams.** Jeff Bezos believes that no team should be so big that its members can't be fed with two pizzas. According to him, a company should be made up of lots of small, autonomous units that are able to organize themselves and compete with one another. As described above, they should be fearless about developing ideas and motivated to solve customer problems. Why not arrange your company into small, independent units that are gauged by the frequency with which they produce something new that pushes the company forward?
---

### Brad Stone

Brad Stone is an American author and journalist who writes for _The New York Times_ and _Bloomberg Business Week,_ among other publications.

