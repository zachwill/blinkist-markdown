---
id: 56faa209df8142000700006f
slug: the-evolution-of-everything-en
published_date: 2016-03-31T00:00:00.000+00:00
author: Matt Ridley
title: The Evolution of Everything
subtitle: How New Ideas Emerge
main_color: E52E3B
text_color: CC2934
---

# The Evolution of Everything

_How New Ideas Emerge_

**Matt Ridley**

_The Evolution of Everything_ (2015) argues that the phenomenon of evolution — gradual change without goal or end — reaches far beyond genetics. Evolution happens all around us in economic markets, our language, technology and customs, and is what's behind nearly all changes that occur in these fields.

---
### 1. What’s in it for me? Unfold the evolution of everything around you. 

When we think of evolution, we usually think of the monkey-to-man picture and the mind-boggling research being done on the human genome. But have you ever considered the evolution of everything beyond biology? 

These blinks explore evolution outside the realm of genetics, be it the evolution of morality, economy, language or technology. Typically, we're taught that these things are created in a top-down manner, by leaders, scientists, priests, businesspeople, and so on. But actually, when evolution is understood in its broadest sense — i.e., as the gradual development of something — it is far more common for things to evolve from the bottom up. 

The author argues that evolution, in the bottom-up sense, can explain virtually all human cultural changes.

You'll also learn

  * what DNA and language have in common;

  * why the Wild West wasn't so wild after all; and

  * how money got monopolized.

### 2. With few exceptions, the history of Western thought is shaped by a creationist perspective. 

When we think of evolution, we often think of biology, Charles Darwin and his evolutionary theory. But the word "evolution" doesn't just denote genetics. Originally, "evolution" meant "unfolding" and described how things gradually changed without a plan.

And yet, the history of Western thought has been dominated by a creationist mode of thinking. That is, explaining the world through design and planning.

Consider a few examples: 

The ancient Greek philosopher Plato thought that society functioned by mimicking a designed cosmic order; in _The Iliad_, Homer had gods deciding the outcome of battles; much later, the Christian reformer Martin Luther stated that our fate lay in the hands of God; the nineteenth-century German philosopher Friedrich Nietzsche believed that healthy societies were made by the plans of powerful leaders; and Karl Marx claimed that a planned state was the best means to encourage economic and social progress.

The list goes on. Again and again, we see top-down descriptions of how the world is designed or should be organized. However, there are a few exceptions to this creationist mode of thinking. Just take the ancient Greek philosopher Epicurus.

Epicurus believed that the physical world, including society and morality, emerged spontaneously, needing no divinity or royal power to explain it. Everything, he said, was made of invisible atoms which followed the laws of nature rather than the laws of God.

The Roman poet Lucretius adopted Epicurus's stance, stating that the world was made of invisible particles. He believed the world had no creator and life had no end or purpose.

Epicurus and Lucretius were thus precursors to Darwin. We'll explore this in detail in the blink.

> "_The way in which these streams of human culture flow is gradual, incremental, undirected, emergent and driven by natural selection among competing ideas_."

### 3. Darwinian evolution removed the notion of creationism from biology. 

It was the ideas of philosophers and poets such as Epicurus and Lucretius and, later, Enlightenment thinkers like David Hume, which gave rise to the question: "If God is the designer of humans and the world, who designed the designer?" 

However, it was Charles Darwin who eliminated creationism from biology and replaced it with a theory of evolution through natural selection. Darwin came up with his theory of biological evolution after his sailing expedition to the Galápagos Islands, where he collected various animals and plants, and meticulously observed and reflected on his findings.

Darwinian evolution argued for a mechanism that explained the emergence of complex organisms from simple cells. This mechanism was natural selection, or the process by which beings with specific characteristics that make them better suited to their environment have increased chances of survival.

Today, Darwinian evolution is widely accepted as truth. Yet, since we have been able to decode genes, people now ask what individual genes are for.

It has been argued that genes aren't designed to serve the body. Rather, they use the body to serve themselves. As Richard Dawkins stated in his book _The Selfish Gene_, the only reasonable way to understand organisms is to view them as temporary vehicles used by genes to secure their continued survival.

This idea is supported by the finding that many of the genes that humans and other organisms have appear to have no function. They don't exist for the needs of the body: they're just hitching a ride in a vessel. Dawkins's description of the selfish gene provides a strong argument against creationism as it seems unlikely that an intelligent, divine designer would create "useless" genes.

> "_It makes more sense to see the body as serving the needs of the genes than vice versa_."

### 4. Evolution exists in culture, the economy and technology. 

Although we associate it with biology and genetics, evolution's reach is much longer than this. Evolution is inevitable in any system where information is transmitted with a degree of randomness, such as the genetic information in our DNA that we pass on to our children or the cultural information we impart to them when we raise them.

In other words, evolution applies to human culture, too. The best example can be seen in the evolution of language. Actually, there is a strong parallel between the evolution of DNA and the evolution of language. Both are systems capable of combining a small number of basic building blocks to form virtually infinite permutations.

The building blocks of DNA consist of four nucleobases; the building blocks of many languages are the letters of the alphabet. Moreover, DNA evolves through natural selection and, in a sense, so do languages. For instance, words and phrases that are popularly used in written and spoken language are likely to endure, while words that nobody uses will die out. 

The concept of evolution also applies to economy and technology. It was Adam Smith who first realized that markets could regulate themselves without an overseer such as a government. In his book _The Wealth of Nations_, Smith argues that general prosperity is produced by the free exchange of goods and services.

Later, in the twentieth century, economist Joseph Schumpeter understood markets as evolutionary systems that were constantly testing new products and services to meet human needs. The "fittest" products on the market were bought and sold, i.e., they "survived," whereas the unfit products didn't.

Technological progress, too, can be seen as an evolutionary phenomenon. Like biological evolution, technology moves from tool to tool through trial and error. In communication technology, for instance, the telegraph evolved into the traditional telephone, which evolved into the cell phone, which will inevitably evolve into another device one day.

> "_But if life needs no intelligent designer, then why should the market need a central planner?_ "

### 5. Morality and religion are not God-given, but have evolved with society. 

Most religions teach that morality is prescribed by God. For instance, the Judeo-Christian God had Moses climb Mount Sinai to receive the Ten Commandments to pass on to his people. In Islam, it was Muhammad who received the word of God and imparted this to his people.

However, God isn't necessary for morality. Rather, morality evolves through social interaction in which people seek out ways to communicate successfully and happily.

While Adam Smith is famous for _The Wealth of Nations_, he also spent a lot of time exploring morality. Smith observed that morality was not so much taught as evolved - an attribute that develops as we mature in our societal framework. As children, we discover by trial and error which actions evoke positive responses and sympathy from others, and it's through these discoveries that we have evolved toward a common moral code.

So morality isn't God-given. But since teaching morality is central to religion, what does this say about religion itself? 

It means that not even religion is passed on to us by God. Instead, it's an evolving, man-made invention. Moreover, some examples make clear that the concept of God has an evolutionary history, too.

For example, the ancient Greeks and Romans believed in several gods, while Christianity dictates the existence of only one God. There was also a shift from gods who were temperamental, foolish and desirous, to gods who were disembodied and virtuous. Just compare the jealous and moody Zeus to the holy and perfect Allah. 

Religions are also the result of selection among various people and beliefs. Christianity, for example, arose in the first century AD from a number of different competing religions in the Roman Empire. So in that sense, religions have evolved, too.

> "_All religions look man-made to me_."

### 6. Creationist thinking shapes concepts of personality development and education. 

Did you know that twins raised apart have very similar personalities? This is a fascinating discovery, because until the 1990s, psychologists widely assumed that personality depended only on a child's upbringing. 

Even today, our idea of personality development is shaped by the creationist belief that culture creates a child's personality when, in actuality it evolves from within. Consider the following example: toy shops are split into girls' and boys' aisles where parents are encouraged to buy trucks for boys and dolls for girls. This angers people who believe that behavioral differences between the sexes are culturally forced upon children. But while it's true that culture evolves, this doesn't necessarily mean that personality evolves along with culture, or that culture creates personality.

In fact, there's evidence against the theory that behavioral differences between sexes were generated by culture. Experiments show that, when offered a truck and a doll, girls will choose to play with the doll and boys with the truck, regardless of their previous experience. And, interestingly, the same is true of male and female monkeys. Therefore, personality evolves from within, and isn't just influenced by culture.

Education, too, is shaped by creationist thinking. However, evolving and experimental educational systems seem to be more successful. Education is governed by the belief that the best way to impart knowledge to children is through adult instruction, the idea being that teachers "create" educated children. 

But one educational system does better than many traditional schools — Montessori system. The Montessori method of collaborative, self-directed learning techniques have given the schools an impressive track record of producing entrepreneurs. Interestingly, the founders of Amazon, Google and Wikipedia all attended Montessori schools, demonstrating that learning thrives in evolving, bottom-up educational systems.

> "_Banish the magic of cultural determinism; look to evolution for the causes of behaviour_."

### 7. Innovation evolves on its own – there’s no need for leaders or governments to “create” it. 

In its first edition, the _Encyclopédie_ — the encyclopedia of the French Enlightenment published between 1751 and 1772 — contained nearly no entries on people. Why? The authors wanted to remind readers that innovation evolves on its own, rather than being created by a handful of powerful people.

These days, though, leaders are viewed as the creators of innovation. But really, innovation can flourish without top-down creation. Take leadership in business. Hugely influential CEOs like Bill Gates, Steve Jobs, Jeff Bezos and Mark Zuckerberg are granted nearly godlike powers for their innovation and success. So much so that, when Steve Jobs died in 2011, many people assumed that Apple would simply go under without him.

And yet, companies can create success without any leadership at all. The Californian company Morning Star Tomatoes understands this and has been experimenting with self-management for two decades. And it's working. It is a highly innovative company and its profits have increased rapidly, despite the fact that it has no managers or CEOs. Moreover, its success came about without any central planning or leadership.

Laws can also evolve from the bottom up. In most political science classes, students are taught that governments create laws and that it's the state's job to enforce them. But this is only partly true.

Consider the Wild West. Many people assume that the nineteenth century American West was lawless and violent because it had no government. But the Wild West wasn't as wild as we've been made to believe. With few formal law enforcement systems in place, the people regulated themselves. Customs and laws evolved from the people and weren't dictated by a governing body.

### 8. Money and the internet are evolutionary phenomena. 

Most of us take for granted that the internet belongs to nobody in particular. It is neither controlled by monopolizing companies nor governments. So when it comes to money, which today is created and issued solely by governments through central banks, why do so many of us assume that it's always been a government monopoly? In fact, money was once free to develop much in the way the internet does today.

Money gradually emerged among traders, and wasn't always controlled by rulers. Take Sweden, which established a free banking system in the nineteenth century. During this time, several banks — the central bank among them — competed to issue their own banknotes. This system worked remarkably well and not a single bill-issuing bank went out of business.

Canada, too, took a similar approach. In the 1930s, it had no central bank and its banking system came out unscathed after the Great Depression. 

Today, thanks to the internet, we're seeing new forms of self-organizing money systems. For example, air miles, mobile phone credit and the internet currency Bitcoin.

And so, the internet itself is an evolutionary phenomenon. The internet emerged in an unplanned, unpredictable way; no one had anticipated blogs, social networks or search engines. The internet has no core or hierarchy. Nobody controls it, yet it isn't chaotic. Its inception came about through decentralized programming and hobbyist groups. 

Unfortunately, however, the internet is in danger of becoming controlled and centralized, as the number of governments that have opted to censor it has steadily risen. So, if we'd prefer the internet to avoid suffering the same fate as money, we must advocate for an internet that can continue to evolve freely.

> "_Let's give a bit less credit to creationists, while we encourage and celebrate the evolution of everything_."

### 9. Final summary 

The key message in this book:

**Evolution isn't limited to biology. It's in our culture, our economy, our technology, our morality and even our religions. Rather than falling into creationist ways of thinking, we would do better to try to see the ways in which our culture evolves. We stand to reap great benefits from doing so.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Where Good Ideas Come From_** **by Steven Johnson**

_Where Good Ideas Come From_ examines the evolution of life on earth and the history of science. This _New York Times_ bestseller highlights many parallels between the two, ranging from carbon atoms forming the very first building blocks of life to cities and the World Wide Web fostering great innovations and discoveries.

In addition to presenting this extensive analysis, replete with anecdotes and scientific evidence, Johnson also considers how individual and organizational creativity can be cultivated.
---

### Matt Ridley

Matt Ridley is the author of several bestselling books including _The Rational Optimist: How Prosperity Evolves_, _Genome: The Autobiography of a Species in 23 Chapters_ and _The Red Queen: Sex and the Evolution of Human Nature_. Ridley writes for _The Times_ and the _Wall Street Journal_ and is a member of the UK House of Lords.

