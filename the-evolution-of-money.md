---
id: 59353b13b238e10008fda248
slug: the-evolution-of-money-en
published_date: 2017-06-08T00:00:00.000+00:00
author: David Orrell and Roman Chlupatý
title: The Evolution of Money
subtitle: None
main_color: 156A3C
text_color: 156A3C
---

# The Evolution of Money

_None_

**David Orrell and Roman Chlupatý**

_The Evolution of Money_ (2016) offers an insightful look at the history of currency in civilized society, from shells and coins to the digital ones and zeroes of an online bank account. Find out how monetary systems have always functioned much like religion — without faith and belief, they'd collapse — and learn what the future may have in store.

---
### 1. What’s in it for me? Explore the history and character of money. 

As a child, did you ever wonder why someone would gladly exchange precious things — a car, a teddy bear, a piano — for little green pieces of paper? Did this not strike you as strange?

Money really is an odd thing. Humanity has been using it for a long time, but even economists are still divided over its exact character. Also, it comes in many different forms, both tangible and not, from shells to bills to Bitcoins.

These blinks offer an in-depth look at money's turbulent history, its uncertain future and the many ways in which the economy has tried to make sense of it.

As we move through the centuries, you'll also discover

  * one more reason to envy Australians;

  * how money is related to quantum theory; and

  * why two pizzas once sold for what would be worth millions in today's money.

### 2. Contrary to popular belief, money wasn’t invented to replace the barter system. 

Have you noticed how bartering comes very naturally to children? Perhaps you remember exchanging a juice box for some cookies, or your best set of marbles for a Matchbox car.

One old and popular theory holds that money was "invented" as societies outgrew the barter system. In fact, this theory dates back to Aristotle.

Though it was essentially pure speculation, the theory gained traction with many great thinkers who followed, including the influential eighteenth-century economist, Adam Smith.

They all believed that money was an outgrowth of _commercial trading_ — where, for example, some valuable bit of property such as cattle could be traded for a certain number of slaves.

But this isn't very efficient; things like cattle aren't very easy to transport, whereas coins are.

Plus, the precious metals in coins were seen as being valuable just about anywhere, while other goods may not be in demand in some areas and are therefore less valuable.

This idea of money evolving from bartering may sound plausible, but it's actually been debunked. In 1913, Alfred Mitchell-Innes, a British economist, published his own findings, noting that there was no evidence in commercial history to suggest that a barter-only system ever existed.

And Mitchell-Innes has yet to be proven wrong — in fact, historians have only gone on to find more evidence of ancient civilizations using old forms of money _in addition_ to bartering.

Around 5,000 years ago, in Sumer, one of Mesopotamia's earliest urban civilizations, commercial transactions were recorded on clay tablets, which show us that salt, beads and bars of precious metals were all used as early versions of money.

The truth is, we don't know exactly how or when money came to be used, but we do know that the first coins began appearing in the seventh century BC, in the Mediterranean kingdom of Lydia.

And by the sixth century BC, Greek city-states were minting their own coins as a demonstration of power and independence.

### 3. Determining the value of money reveals its complex nature. 

You probably know that Sir Isaac Newton is one of world history's most influential physicists, but did you know he also had a huge impact on our currency?

Indeed, Newton is responsible for developing the relationship between money and its weight.

This happened in 1649 after Newton suffered a nervous breakdown and took a job as warden of London's Royal Mint.

It was here that he put England on _the gold standard_, a system that uses a fixed rate between the currency's weight, such as England's silver coins, and the value of gold.

This is how we got the name for a British "pound" — one of these silver coins used to be worth one pound of gold.

But when it comes to money, it's important to consider both its tangible and intangible properties.

Money is, of course, a real and tangible object, like the coins and bills in your pocket. But money can also represent intangible things, such as the number denoting its value.

Some people consider the dual nature of money — what it physically represents versus what it theoretically represents — to be similar to a quantum object, like a photon that has the characteristics of both a particle and a light wave.

And like some quantum objects, money can change from one moment to the next.

A one-dollar bill's worth is determined by a trusted authority, such as the Federal Reserve. But once we begin using it to buy goods or services, that value can change due to market rates. So today a dollar might get you a bottle of water, but tomorrow conditions could change, and you might be able to sell that same bottle of water for two dollars.

This complex nature has been confounding and enchanting economists for centuries.

### 4. Banking and international trade flourished after the invention of debt. 

No one enjoys being in debt, but debt itself is a necessary part of a functioning economy.

Debt can't exist without negative numbers. And the first person to demonstrate the use of negative numbers was Brahmagupta, an Indian mathematician who explained their purpose in his seventh-century book, _The Opening of the Universe_.

From this point on, businesses could use bookkeeping and the double-entry system, which notates two kinds of transactions, negative debits and positive credits. This system made it a whole lot easier to find out when a transaction error may have occurred and also to evaluate how profitable a business was.

Once transactions were recorded in a ledger, ideas of money lending began to emerge, introducing intangible concepts, such as interest.

In seventh-century Mesopotamia, promissory notes known as _sakk_ were introduced. Around the same time, Islam officially forbade _usury_, the practice of lending money at high interest rates, but it did allow for fees to be accepted in exchange for loans.

Loans proved to be useful in the Middle Ages, as European towns used them to build churches, which was seen as justifiable since it was in service of God.

With economies becoming more complex in the Middle Ages, an international banking system began to emerge.

It began with tradesmen forming associations that led to companies, prompting moneylenders to require a more formal system of finance.

Port cities like Venice and Florence began trading with Asia, which led to their becoming major financial centers.

Moneychangers then formed their own guild called _Arte del Cambio_ at the turn of the thirteenth century, making them the earliest version of modern bankers.

Eventually, international traders realized that heavy coins weren't ideal, which is how bills became popular. At first they were simply letters, instructing a banker or a foreign agent to make certain payments on the writer's behalf.

This dramatically boosted the efficiency of international trade. Now a merchant in Venice could purchase goods from a French supplier using a bill valued at an agreed exchange rate.

### 5. The gold and silver riches of the New World significantly changed the world economy. 

The discovery of the Americas was, of course, a big deal for folks in the Old World, and part of that was due to the economic impact it had.

After Hernán Cortés conquered Mexico in 1521, Spain soon learned about the financial chaos that too much of a good thing can suddenly cause.

When Cortés landed in Mexico, he found that the Aztecs were rich in gold and silver, which they used for jewelry and decoration. For money, they used other things, such as cacao beans.

And even though the Aztec emperor, Moctezuma II, offered the Spaniards gifts of silver and gold, they instead chose to conquer and take as much of the Aztec riches as they could.

As a result, Spain was flooded with more precious metals than they could have imagined. Between 1500 and 1800, around 150,000 tons of silver and 2,800 tons of gold was produced. This influx led to a new problem: _inflation_, as the value of the precious metals declined.

Prices now had to be adjusted, and as Spanish goods became more expensive, massive debt caused Spain to default on its loans a whopping 14 times between 1500 and 1700.

However, this surplus of gold and silver allowed more European nations to mint coins. Even the lower classes now had access to them.

The newfound riches also led to the development of _mercantilist_ nations, such as Great Britain, which extended its military reach to secure as much of these precious metals as possible.

Such nations operated under the _mercantilist theory_, which assumes that there's a fixed amount of resources in the world, and a nation's wealth depended on how much precious metal it possessed. So, for one to gain, another must lose.

With very few gold and silver mines, Great Britain became eagerly expansive by granting a royal charter to the East India Trading Company at the beginning of the seventeenth century. This charter allowed the company to mint its own coins and spread England's power to India, where the silver rupee became the Indian standard.

### 6. The ability to print money led to certain problems, but a stable economy eventually emerged. 

Paper money has come a long way. It's now a complex mix of numbers, watermarks and even holograms.

The history of banknotes goes back to the early eighteenth century when France was facing some tough economic times.

To help straighten things out, economist John Law convinced France to let him start his own bank and to use banknotes as currency. This led to the nationalized Bank Royale in 1718 and a similar bank in the settlement of New France, in what is now Mississippi.

Banknotes were attractive since they could be produced cheaply and in huge amounts without using expensive metals. But, once again, people soon discovered the dangers of having too much currency.

Coins were in short supply in the New World, so people relied on old-fashioned commodity trading and foreign coins. But this wasn't ideal for the ongoing military campaigns in the area, and colonial governments were forced to issue bills, which again led to inflation.

To fix this problem, and ensure that the supply of physical money didn't exceed the economy's needs, Pennsylvania came up with a brilliant solution in 1723.

Supported by Benjamin Franklin, the state tied its supply of bills to measurable assets, like land and future taxes, meaning that more bills were only issued in relation to growth in these assets. And, sure enough, the economy stabilized and grew.

But a stable system needs a stable relationship between banks, which is a concern that Abraham Lincoln had to deal with. He wasn't happy with the power struggle that was going on between the private and the federal banks, both of which could issue money.

Eventually, the Federal Reserve in the United States ended up providing reliable supervision and regulation of private banks, which has enabled a relatively stable and robust money system, even when the economy has faltered.

It's worth noting that when the economic crisis of 2007 unfolded, it was mostly the private banks that were abusing their power, not the federal ones.

### 7. Economic theory has changed over the last few centuries, and it’s come to include psychological aspects. 

If you've taken a class in economics, one of the first names you'll have encountered is that of the eighteenth-century philosopher, Adam Smith. In his desire to create a universal theory of finance, he gave birth to the science of economics.

Smith laid a very solid economic foundation, but our understanding of the relationship between money and its value has certainly evolved since then.

Smith typically determined the value of something by considering the labor required to obtain it. So, for instance, the value of gold should reflect the amount of work it takes to unearth it.

But this relationship isn't always clear-cut. For instance, what's the value of the labor when a company uses unpaid slaves to do the work?

This is why, two centuries later, economist Irving Fisher developed the _quantity theory of money_. This became the prevailing philosophy for the twentieth century.

Fisher, arguing that an active economy is the healthiest, felt that monetary value isn't as important to an economy as the momentum or flow of money. So an ideal economy is one where people are constantly investing and buying, not hiding their money under mattresses or in piggy banks.

Another important factor that is now being reexamined is the psychology of consumerism.

Most economists up until the latter half of the twentieth century assumed that our economic decisions were rational. More recently, economists like Daniel Kahneman and Amos Tversky have shown that that's not the case, and we are in fact profoundly irrational when it comes to money.

They created a new field called _behavioral economics_ to help explain why we make biased, irrational and emotionally charged decisions about how we spend and save money.

For example, behavioral economics illustrated that we place a higher value on money that we can have now rather than in the future.

### 8. Economists and politicians have discussed and tried various methods to deal with monetary crises. 

Here's a funny thought: one day, a government employee shows up at your door with an envelope filled with cash and says, "Enjoy." Pretty unrealistic, right?

Actually, a number of economists believe that providing people with extra spending money is a very realistic way of helping economies bounce back from recessions, like the one that followed the 2007 crisis.

In December of 2008, Australia did just that, giving every taxpayer $900 to encourage spending. And unlike many other countries, Australia did not experience a recession after the crash.

Another strategy is called _quantitative easing_ (QE), which involves a central bank providing extra money and boosting reserves by buying assets from private banks.

While some think this could stimulate the economy by making loans more readily available, critics think this is too close to printing money and could lead to inflation. Yet a QE plan has recently been put in place in Iceland after the nation experienced a banking collapse, and it has since proved successful.

A third option for resolving an economic crisis is to change the currency altogether.

Ever since the gold standard ended in 1971, the International Monetary Fund has reported around ten systemic financial crises every year. Economists think that one solution to this recurring problem would be to simplify economies by using a universal currency.

And solving a problem by changing a nation's currency does have a history: In 1922, after Russia faced a crisis with the ruble, it reintroduced gold chervonets, a move that did help stabilize the monetary system.

However, if currency is in short supply, introducing negative interest rates can help stimulate spending.

For instance, during the Great Depression, _stamp scrips_ were introduced. These were notes that would lose their value unless every week, you bought a one-cent stamp and stuck it onto the scrip. This incentivized people to spend them quickly.

Clearly, when times are tough and money is tight, creative monetary solutions abound.

### 9. Bitcoin has changed the present status of monetary systems – and the future has many challenges. 

Things have been changing fast in the new millennium, and it's not over yet. Before long, things like piggy banks and coin jars might be a thing of the past.

So far, the _Bitcoin_ is the closest we've come to a truly universal currency, and it's also a threat to the traditional banking system.

Bitcoin was created in 2008 as an electronic currency unconnected to any banking system. It can also be seen as a response to rising distrust of the global financial system after the 2007 banking crisis.

Unlike traditional money, new Bitcoins aren't issued by any central bank based on a government order; they're created as a kind of reward whenever someone with a powerful computer, or a network of powerful computers, solves a difficult math equation.

It's part of a process called _mining_, and as more Bitcoins get put into circulation, the more difficult the equations become.

At first, Bitcoins were seen as being part of a game, but once people began buying real things with them, they quickly became legitimate. One of those first purchases with the new currency was for two pizzas that were bought by a computer programmer in Florida for 10,000 Bitcoins. In 2016, that amount of Bitcoins was worth millions of dollars.

This kind of innovation may actually be good for the economy since it is currently facing some big problems.

Right now we're in a largely deregulated capitalist system that aims for unlimited growth by exploiting more and more of nature's very limited resources. Every level of the environment is being damaged; we release massive amounts of carbon dioxide into the atmosphere, we exploit the land in search of metals and minerals and we overfish the already polluted seas.

Meanwhile, income inequality is at an all-time high. An average CEO in the United States earns 354 times more than an unskilled worker. It's no wonder that we're facing immense tensions due to social conflict.

While it's difficult to predict how all this will ultimately resolve itself, it's not unreasonable to think that an economic revolution might play a role.

> _"Like a toxic algal bloom on a lake, the economy is doing fine, but it is asphyxiating everything around it."_

### 10. Final summary 

The key message in this book:

**It's trite but true: money makes the world go round. Throughout history, money has taken on a number of different forms, but some things never change — those with money wield immense power, and the strength of a civilization's economy may spell its success or demise.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Coined_** **by Kabir Sehgal**

_Coined_ (2015) offers an in-depth explanation of money, a powerful and complex force that many of us take for granted. It examines money's historical roots and explains the relationship between it and our emotions, while offering theories on the future evolution of money.
---

### David Orrell and Roman Chlupatý

David Orrell is a Canadian author with a doctorate in mathematics from the University of Oxford. His writing has been featured in several economic publications and his other books include _Truth or Beauty: Science_ and _The Quest for Order_.

Roman Chlupatý is a journalist and consultant who specializes in economics and politics. He is also the author of the book _(S)OUR EARTH: Fourteen Interviews About Things That Are Changing Our World._

