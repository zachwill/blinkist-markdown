---
id: 544e02223630630008670000
slug: the-facebook-effect-en
published_date: 2014-10-29T00:00:00.000+00:00
author: David Kirkpatrick
title: The Facebook Effect
subtitle: The Real Inside Story of Mark Zuckerberg and the World's Fastest-Growing Company
main_color: 3B9AD0
text_color: 2B7199
---

# The Facebook Effect

_The Real Inside Story of Mark Zuckerberg and the World's Fastest-Growing Company_

**David Kirkpatrick**

_The Facebook Effect_ reveals the inside story of social media site Facebook: its modest origins, its meteoric rise and its continued dominance in social networking. Author David Kirkpatrick shows how Facebook has not only changed how we communicate with each other, but also how we think about politics and the media — not to mention our attitudes toward privacy.

---
### 1. What’s in it for me? Learn how Facebook has changed how we communicate with each other. 

While Facebook wasn't the first online social network, it's by far the first that comes to mind when you think about social networking.

From its early beginnings as a pet project realized in a Harvard dorm room to its status as one of the most popular websites in the world, Facebook has achieved both technological and business success.

Facebook has also spurred radical societal change. From how we consume media to how we communicate with each other and the world, Facebook's influence is vast — and these blinks will show you how the social media site got so powerful, and what it means for you.

In the following blinks, you'll also discover:

  * how Facebook helped bring police brutality to light in South Africa;

  * how U.S. President Obama used Facebook to help win the 2008 election; and

  * how a new form of journalism is being created when you "like" and "share."

### 2. Before social sites Facebook and Friendster, there was the WELL and AOL. 

Facebook didn't invent social networks; in fact, the concept has been around since 1985.

Early social networks took the form of electronic bulletin boards. If you were a member, you could "chat" with other members online about common interests. One of the earliest online communities is the _The Whole Earth 'Lectronic Link,_ or the _WELL,_ which is still active today.  

These early communities evolved rapidly as technology developed. Members could create personal profiles, establish private groups or even chat in _real time_ in "chat rooms," somewhat akin to today's instant messaging. _America Online_ (AOL) and _Geocities_ were the first to offer such services.   

_Classmates.com_ was another site that helped you find and contact that good-looking guy from your graduating class: a feature still integral to modern social networks.

Strictly speaking, however, such virtual communities weren't really social networks, according to the sociological definition of the term. In a true social network, a user can construct a _profile,_ create a _friend list_, and use others' profiles and friend lists to make _new connections._

By this definition, the first real social network was start-up _Sixdegrees.com._ Its purpose was to map the relationships between people via member profiles that also listed friends and interests. The site's draw was that you could " find" someone in seconds through the site's search engine.   

Yet we all know the most satisfying part of finding a "friend" online is seeing what they really look like! It wasn't until 2002 when _Friendster_ was launched, a social network that allowed a user to include a profile photo. With this feature, the site was an instant success.

The following year, scores of new social networks popped up, including _LinkedIn, Myspace, Spoke_ and _Tribe._  

So by the time Facebook arrived, the market was flooded with competition. Yet from day one it clicked and its success has been meteoric since.

What was its secret? Let's go back to Facebook's beginnings, starting in a college dorm room.

### 3. An idea dreamed up in a college dorm room quickly became one of the world’s most popular sites. 

Mark Zuckerberg was a tinkerer. As he was always messing around with coding projects, his Harvard classmates didn't pay much attention to yet another project called _Thefacebook_ in early 2004.

Zuckerberg named his project after college "face books," booklets that listed students' names, class years and profile photos. Initially, _Thefacebook_ was accessible only to Harvard students, although Zuckerberg secretly suspected that the site could be hugely popular.

He launched _Thefacebook_ on February 4, 2004. Just four days later, 650 students had joined; three weeks later, the number had grown to 6,000.

Soon, the site's popularity spread beyond Harvard, and by the end of May that same year, some 100,000 users from 34 schools had joined.

Impressed? So was a financier who offered Zuckerberg $10 million for the site in June 2004. Zuckerberg, however, refused to sell. In September 2005, _Thefacebook_ was renamed _Facebook_, following the advice of _Napster_ founder Sean Parker.

By October 2005, Facebook's popularity had spread to schools around the world, and one year later, the network was opened to non-student users. 

Over the following three years, the number of Facebook users skyrocketed to 145 million. By the end of 2008, nearly a third of worldwide internet users were on Facebook.

Today Facebook is the second most-visited website after Google. As of June 2010, some 500 million people had profiles on Facebook, and an average of 25 million new users sign up each month.

All this success — sparked not by a seasoned professional but by a young college student!

### 4. Socially savvy co-eds and a boom in broadband use helped push Facebook ahead of competitors. 

Facebook's meteoric success may seem unbelievable. Yet the site's quick growth and popularity had a lot to do with being in the right place at the right time.

Importantly, broadband usage rose dramatically around the time when Facebook and Myspace were launched. With faster loading times, social networking online has a lot more appeal.

Higher connection speeds also were crucial for uploading and sharing photos, an important feature of the later Facebook.   

While launching Facebook amid a broadband boom may have been a lucky stroke, Zuckerberg also proved himself to be a savvy opportunist, timing the release when many Ivy League universities were seeking to put campus networks online.

The campus setting was also crucial to Facebook's success. As one of the leading universities in America, if not the world, Harvard had best and brightest minds, people with whom Zuckerberg could share ideas and eventually build a company.

Facebook's co-founders included Chris Hughes, who became the de facto spokesperson for the social networking site, and went on to spearhead online organizing for President Barack Obama's 2008 campaign.   

Not that students necessarily needed much convincing to join Facebook. Socializing is practically its own subject in a university setting, and a young audience keen to meet peers in any way possible was exactly the demographic that would make a social networking site immensely popular.

As such, Facebook was a bit of a honey pot dropped into a swarm of bees, giving it a serious edge on other competing social networking sites at the time.

> _"Having genius and ambition alone isn't going to get you there. It's really important to be lucky. But Mark had all three in spades." –Dustin Moskovitz_

### 5. Facebook stayed steps ahead of the competition by constantly evolving and improving its services. 

Though Facebook started as a social website for college students, today its popularity has spread around the globe. Yet how did it evolve?

The site's first significant milestone came in the fall of 2005. Early Facebook members liked that they could post a photo as part of their online profile, but as there was only room for one uploaded photo, users would frequently change their profile picture, even several times a day.   

Facebook in response developed a _photo-hosting service_ so multiple photos could be uploaded and _tagged_ with key information or other Facebook members' names. With search, you could find all the photos of a particular friend, regardless of the profile on which they were displayed. Users quickly took to the new service and uploaded an uncountable number of images.

The feature soon became one of Facebook's most popular applications and what's more, one of the most popular photo apps on the internet.

Keeping tabs on how members used the service helped the company continue to evolve. In 2006, for example, the site introduced the _news feed_, a scrolling list of updates and information that keeps members informed of the status updates and activities of Facebook friends.

In 2007, the company rolled out a less visible but nonetheless significant change with the launch of the _Facebook Platform,_ a software environment for third-party developers to develop services or applications that interact with core Facebook features.   

One of these services is _Marketplace,_ which allows you not only to post advertisements but also to purchase games and virtual gifts. _Facebook Connect_ is another app that allows members to log onto third-party websites using their Facebook login and password.

In 2009, Facebook introduced the _like button,_ the virtual "thumbs-up" that has come to be synonymous with the social networking site. Members could now "like" photos, links and comments on Facebook.

### 6. Famous mentors such as Marc Andreessen and Steve Jobs helped Zuckerberg fulfil his vision. 

Though Zuckerberg is famous for his online networking site, he was in fact also skilled in creating offline networks. 

Zuckerberg recruited some of his closest friends as supporters and early collaborators. While his college roommates were initially skeptical of the project, Zuckerberg eventually won them over.

Eduardo Saverin would become Facebook chief financial officer, while Dustin Moskovitz would be vice president. Andrew McCollum crafted Facebook's iconic logo and wrote the code for its search engine, and Chris Hughes was Facebook's first spokesperson.   

Schoolmates and math whiz friends from Zuckerberg's prep school days at the prestigious _Phillips Exeter Academy_ were also recruited to join the company.

Notable among these was Adam D'Angelo, the company's first chief technology officer. Another former classmate went on to become the chair of the _Dartmouth Student Assembly_, a valuable connection for Zuckerberg as Facebook expanded to other Ivy League universities.

Zuckerberg also sought out the assistance of prominent, well-connected mentors. Sean Parker, the founder of peer-to-peer music sharing software _Napster,_ helped Zuckerberg in learning how to deal with financiers, and even became actively involved in Facebook himself. Cofounder of _Netscape_ and entrepreneur Marc Andreessen was another helpful mentor to Zuckerberg.   

Zuckerberg used his new contacts to make others. One year after the launch of Facebook, Zuckerberg approached Bill Gates for business advice. Steve Jobs was another vital mentor and friend to Zuckerberg during Facebook's early years, and the two were often seen taking walks in Palo Alto together.   

From virtual networks to real-life networks, it's clear that Zuckerberg knew the value of both. Yet many of us still don't fully understand the power of Facebook itself. The next blinks will show you what exactly the _Facebook effect_ is, and how it works.

### 7. Facebook has evolved to play a powerful role in politics, from online activism to national elections. 

Considering that Facebook connects more than 500 million active members in just a few clicks, it's not a stretch to say the service has changed how communities communicate and political issues are addressed on a global scale.

We call this change the _Facebook effect_.

One significant Facebook effect is _political activism_. In countries where organizing a protest march might land you in jail, building a Facebook group to address a wrong is almost as effective, and certainly safer.

Such a virtual community is also better positioned to stay updated on abuses, injustice and corruption, news of which can spread quickly through Facebook. Moreover, Facebook makes political activism cheaper. You don't need to spend cash on flyers for a demonstration; instead, you can invite people and inform everyone about the issues online.

In Stellenbosch, South Africa, activists formed a Facebook group in 2008 to protest a series of brutal drug raids. In just two days, the group collected 3,000 members. Witnesses posted photo and video evidence of the raids online, while other members used the group to organise a protest march and start a petition. The Facebook group also mobilised victims of the raid to file complaints.

The result? The police raid was critically investigated by the country's _Independent Complaints Directorate_.   

Facebook has played a large role in political elections, too. In his 2008 campaign for the Democratic Party's presidential nomination, Barack Obama used Facebook more skillfully than did his rival, Hillary Clinton. Obama's comprehensive, personal Facebook page garnered millions of fans. He shared information about the bands he likes and impressed young voters with his hip shorthand.  

Interestingly, Chris Hughes, Facebook's first spokesperson, at the age of 25 went on to take a senior role in the campaign's online strategy team. The team created other Facebook pages to specifically address crucial demographics, such as "Veterans for Obama," "African Americans for Obama" and "Women for Obama."

> _"Facebook is one of the most organic tools for democracy promotion the world has ever seen." –Jared Cohen_

### 8. Quicker and closer: Facebook has changed how we communicate with friends and strangers. 

When we want to contact friends, we'll often turn to Facebook without a second thought. However, the instinctive way we now use social media reflects a radical change in communications that Facebook has inspired.

Facebook has already begun to supplant communication channels that were previously the norm. For many young people, writing an email seems an archaic exercise. 

Immediacy and convenience are some of the key attractions of Facebook. We can share a status update with all our friends without the tedium of compiling a mailing list; if friends are online, we can chat with them in real-time. And if we're curious to know the latest news, we can simply check a friend's profile.

Curiously, despite its 500 million users, interacting on Facebook is akin to living in a small town. Facebook's ability to create a sense of familiarity among strangers overcomes geographical distance; members can easily keep in touch, no matter where they are in the world.

And also like in a small town, we're constantly surrounded by people who know us and care (or are at least curious) about us, and even the minor events in our lives.   

This mentality is reflected in the diverse and often unexpected range of topics that Facebook members share online. For example, imagine you'd just bumped into an acquaintance you don't know well — would the first thing you share be a picture of your lunch? Probably not, but on Facebook, this is common and accepted behavior.   

When we use Facebook, we're able to turn groups of former strangers into a tightly knit community. Moreover, as a platform that combines several online methods of getting in touch with people, Facebook is slowly but surely eroding other methods of communication.

### 9. Facebook users are changing traditional media by becoming their own editors and journalists. 

Do you want to be a journalist? With Facebook, you can report, write and publish as if you were one. And if your story's interesting, your post just might go viral, reaching thousands of people.

Facebook users have come to exercise more influence over media content and how it is received. Traditionally, a newspaper editor would choose which stories were the most important; today, every time you "like" or share an article, it gets pushed to more and more readers.

Citizen journalism has also become a more serious pursuit, with people posting information about breaking news from the source. Sometimes, "real" journalists learn about the news from Facebook! For example, in 2010 _CNN_ newscasters first learned about the Haiti earthquake via Facebook posts.

Facebook too is posing a financial threat to traditional media, by eating up a significant share of brand advertising previously reserved for print and television. As a result, media ad revenue continues to fall, while the number of advertisers on Facebook tripled between 2008 and 2009. Some of the largest U.S. brands advertise through Facebook, including _PepsiCo_, _Procter & Gamble_ and _Sears_.

So as they say, if you can't beat them, join them. Media companies are now beginning to work with Facebook, rather than compete against it. 

For example, the _Huffington Post_ as well as other news outlets have integrated Facebook into their own websites, enabling members to share and discuss news pieces. CNN teamed up with Facebook for a live messaging event during President Obama's inauguration, in which millions of users could watch the ceremony live and simultaneously chat or update their Facebook status.

> _"We want to give everyone that same power that mass media has had to beam out a message." –Chris Cox_

### 10. Our idea of what’s fit for public eyes and what stays private has changed with Facebook. 

What does your best friend know about you? What about your boss? What we share with others and what others share about us has changed considerably with the advent of Facebook. 

For many, Facebook has made separating the professional and the personal a challenge.

Usually we adjust how we present ourselves depending on the situation. A bank clerk, for example, may present herself as staid and respectable at work, only to slip into a biker jacket and play in a punk band with friends after hours. The two personas in "real life" seldom mix.   

Yet on Facebook you can only have one identity, and your profile is visible to friends and acquaintances from every corner of your life. So if you post something personal, chances are that events and details from one "part" of your life will spill over to others.

Our bank clerk's drummer may post pictures of a crazy gig on Facebook, images the clerk might not want her boss to see. But the photos might nonetheless reach her boss, even if he's not among the clerk's Facebook friends.

Crucially, we can't control what others post about us on Facebook. Our friends may say embarrassing things or post embarrassing pictures, in which we're tagged and also can't remove.   

Thus, our sense of what is private has changed radically with Facebook. Yet Zuckerberg, as well as others, have said that increased transparency in all situations will minimise what we have previously considered private.

While we might hide information that we think could discredit us, once enough people disclose the same or similar information — hard drug use, health issues or whatever — then this information becomes less discrediting and more normal. And perhaps then, what was private will become public and sharing any information is no longer a concern.   

Or, the converse could become true. We might try even harder to control information we deem private, to the extent where we refuse to disclose personal matters in any setting, online or off.

> _"A more transparent world creates a better-governed world and a fairer world." –Mark Zuckerberg_

### 11. Final summary 

The key message in this book:

**Through timing, effective networking and nimble business positioning, the social networking site Facebook has become an astonishing success story. The site connects friends and strangers from across the globe, facilitating activism and changing the face of media. Facebook too has changed how we talk to each other and radically altered what we see as private or public.** **  

**

Actionable advice:

**Take care of what you share!**

The next time you think about share something private on Facebook, be sure that the information will reach only the people with whom you really want to share it. Once it's out, you can't take it back! In addition, ensure that you understand Facebook's privacy policy before you post anything to the site.

**Suggested** **further** **reading:** ** _How Google Works_** **by Eric Schmidt and Jonathan Rosenberg**

_How Google Works_ shares business insights from one of the most successful technology start-ups in history. Written by the former top executives at the company, the book lays out, step by step, _Google's_ path to success; a roadmap that your company can follow, too.
---

### David Kirkpatrick

Journalist, author and conference organizer David Kirkpatrick is the developer of _Fortune_ magazine's _Brainstorm Conference_. His work has appeared in _Fortune_ as well as in _Forbes_ and _Vanity Fair_.

