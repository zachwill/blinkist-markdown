---
id: 56249ccd6361310007350000
slug: the-fast-diet-en
published_date: 2015-10-23T00:00:00.000+00:00
author: Michael Mosley and Mimi Spencer
title: The Fast Diet
subtitle: Lose Weight, Stay Healthy and Live Longer with the Simple Secret of Intermittent Fasting
main_color: C5D92D
text_color: 5D6615
---

# The Fast Diet

_Lose Weight, Stay Healthy and Live Longer with the Simple Secret of Intermittent Fasting_

**Michael Mosley and Mimi Spencer**

_The Fast Diet_ (2013) is a guide to intermittent fasting and its health benefits. The 2015 edition includes a revised and expanded theory behind intermittent fasting, along with advice for physical exercise.

---
### 1. What’s in it for me? Discover how fasting can make you not only healthier, but also happier. 

When you're young, you need to eat generous, healthy meals to grow properly. Yet as soon as you reach adulthood, you just don't need that much food. 

Still, we as a society eat far too much. This affects not only our collective waistlines but also our overall health. Yet the solution to this is not another fussy diet plan. 

What we need to do is simply eat less. The connection between fasting regularly and living longer has been long proven. Yet in these blinks, you'll learn about new evidence that shows the benefits of fasting in keeping diseases such as diabetes and Alzheimer's — as well as depression — at bay.

In these blinks, you'll discover

  * why your body isn't made for the sort of foods you eat today; 

  * how fasting on occasion can drastically lengthen your life; and

  * how you can prevent diabetes by learning how to fast.

### 2. Fasting is evolutionarily more “normal” and healthier for us than is eating three meals a day. 

The current medical advice for a healthy and happy life tells us that we should eat low-fat foods and exercise more. But does this sort of prescription work? Not really. 

Today, worldwide obesity rates are soaring. We are fatter and less healthy than we ever were, and cardiovascular diseases are among the top killers around the globe. 

How can we fix this? The solution is fasting, in particular _intermittent fasting_, which means you alternate periods of eating normally and not eating at all. 

We've actually been switching between a state of "feast or famine" for millennia. Thus, our bodies respond extraordinarily well to intermittent fasting, as it mimics the environment in which we lived as we evolved as humans. 

So what are the benefits of intermittent fasting?

Medical articles have shown that fasting can reduce oxidative damage and inflammation, can optimize energy metabolism and can also increase cellular protection in the body. This means that fasting intermittently can help you reduce obesity, hypertension, asthma and rheumatoid arthritis.

Other research suggests however that frequently eating smaller portions of food increases the body's metabolic rate, and therefore can help you lose weight. 

But is this really accurate? Researchers from the Institute for Clinical and Experimental Medicine in Prague, Czech Republic, found out that it isn't.

In a study, two groups of type 2 diabetics consumed 1,700 calories, divided daily into either two meals or six meals. 

Although the amount of calories was the same for each group, the two-meal-a-day group lost an average of three pounds and about 1.5 inches more from their waists than did the group that ate six meals.

> _"I discovered that I often eat when I don't need to. I do it because that food is there, because I am afraid that I will get hungry later, or simply from habit."_

### 3. Fasting decreases your probability of getting cancer and can potentially prolong your life. 

Fasting intermittently can be a useful way to keep disease at bay, and scientific evidence to support this claim is growing.

An article in the scientific journal _Nature_ states that while much research points to the benefits of fasting, the fundamental mechanisms behind why fasting works are not yet clear.

One potential mechanism supporting the benefits of fasting may be the reduction in the amount of the hormone IGF-1, or insulin-like growth factor 1, in your body when you fast. A reduction in this hormone can potentially lead to a decreased likelihood of developing cancer, as well as an extended life span.

The cells in your body naturally grow and reproduce as you take in energy in the form of food. Yet when you starve or fast, levels of IGF-1, which helps stimulate this growth, drop off, signalling a sort of "warning" to your body.

When this happens, your body abandons its normal grow-mode, instead going into repair-mode, fixing cell damage and maintaining the status quo.

Dr. Valter Longo of the University of Southern California's Longevity Institute explored this with mice that were genetically engineered to not produce IGF-1. He found that mice without IGF-1 lived almost twice as long as mice with IGF-1, and also didn't develop diabetes or cancer.

But what about humans? Longo expanded his research to study Ecuadorians with Laron syndrome, a genetic mutation that results in extremely low IGF-1 levels in the body.

Interestingly, the individuals with Laron syndrome never developed diabetes or cancer. Longo ruled out environmental factors as an influential variable, as the test cases' relatives who didn't have Laron syndrome lived in the same village, and some of them did develop cancer.

Longo's research suggests that while we may need increased levels of IGF-1 when we're young and developing, elevated levels later in life may lead to accelerated aging or even cancer.

### 4. Fasting affects your brain chemistry, decreasing the chances of developing a disease like Alzheimer’s. 

Does becoming smarter and happier, enjoying a ripe old age and escaping degenerative illnesses like Alzheimer's sound like the sort of life you want to live? 

If so, you might want to consider incorporating fasting into your routine!

The human brain is a complex machine. Its adaptability and flexibility are remarkable, yet it's not invincible. Neurodegenerative diseases such as Alzheimer's seriously affect the life of not only the patient but the patient's family, too. 

Dr. Mark Mattson, a professor of neuroscience at the National Institute of Aging, conducted a study to demonstrate how fasting affects the brain by genetically engineering mice to make them more vulnerable to Alzheimer's.

When the mice consumed a normal diet, by the age of one (equivalent to a middle-aged person) they started having problems with memory and learning — early signs of Alzheimer's. Yet when mice were put on a an intermittent fasting diet, the onset of Alzheimer's was delayed for some 20 months, or until the mice were essentially as "old" as an 80-year-old person.

The mice that were fasting showed an increase in a protein known as _brain-derived neurotrophic factor_. This protein stimulates stem cells to grow into new nerve cells in the brain's hippocampus — an area that is responsible for learning and memory.

From an evolutionary standpoint, this chain of events is crucial. When an organism is starving, it needs increased brain power to learn quickly how it can find food to continue to survive.

Interestingly, this same protein has been shown to produce antidepressant effects in rodents. In one experiment, researchers injected the protein into the brains of rats, and found that it had an effect similar to the repeated use of standard antidepressant medication.

So fasting can potentially not only make us smarter and delay the onset of diseases such as Alzheimer's but also potentially make us happier!

> _"I thought that fasting would make me distractible, unable to concentrate. What I've discovered is that it sharpens my senses and my brain."_

### 5. Fasting intermittently helps the body regulate insulin levels, curbing diseases like diabetes. 

Worldwide, every seven seconds someone dies from diabetes. In 2014, a staggering 4.9 million people succumbed to the disease. 

An effective way to reduce the number of deaths from diabetes is to adopt intermittent fasting into our lives! But first, let's take a look at how diabetes develops, and how fasting can decrease the chances of developing this disease.

When you consume a lot of carbohydrates, your blood glucose levels rise. To regulate your blood glucose, the body needs the hormone insulin. Insulin helps to extract glucose from your blood, transform it into glycogen, and store it in your liver or muscles as energy. 

Insulin also forces fat cells to absorb and store energy too, in the form of fat. Yet if your insulin levels become too high, your body holds on to fat more readily, and you gain weight.

Over time, if your body produces more glucose and therefore more insulin, your cells simply stop responding to the insulin. When this happens, your blood glucose levels remain high, as your insulin regulator has just quit working!

At this point, you become a diabetic, joining the ranks of some 371 million diabetes sufferers in the world.

What's even more troubling is that when you develop diabetes, your chances of going blind, getting cancer or having a stroke or heart attack rise, too.

Fasting can help your body regulate insulin as well as encourage it to work more efficiently.

In a study conducted in 2005, eight healthy men were asked to fast every other day for 20 hours a day over a two-week period. On fasting days, they could only eat between 6 p.m. and 10 p.m.

After the two weeks, the men's weight and fat levels remained the same, but their bodies' insulin sensitivity increased substantially. This means that the same amount of insulin worked much more efficiently in the body than it did before the period of fasting.

Doctors have ascertained that when we fast, our body breaks down fat cells for energy. It's these fat cells that distract insulin from its job in regulating blood glucose levels. So fasting helps reduce fat cells and gives insulin the opportunity to work more efficiently.

### 6. Fasting can help you better cope with inflammatory diseases such as asthma and eczema. 

Whether natural or synthetic, allergens are everywhere and often cause chronic inflammatory diseases such as asthma and eczema. Crucially, fasting may also help allergy sufferers. 

One in every 12 people in the United States struggles with asthma, for a total of almost 25 million people. While fasting hasn't been scientifically shown to be directly related to curbing inflammatory diseases like asthma, many asthma sufferers who have also fasted have reported an alleviation of symptoms as a result of their lifestyle change. 

One 44-year-old woman, for example, decided to fast to lose weight. Over the course of her fasting she not only managed to shed 14 pounds but her breathing also improved. 

Medical studies have also shown similar results. One study focused on the effects of fasting on obese asthmatics. In the experiment, ten individuals were placed on a fasting diet for eight weeks. On one day the participants could eat what they pleased, yet on the following day, they had to reduce their caloric intake to 20 percent of normal.

After two months, participants dropped an average of 18 pounds. Even more surprisingly, within just two weeks many of the participants' asthma symptoms improved. 

The researchers suggested that the improvement was related to a significant drop during the period of proteins that cause an inflammatory response in the body, such as tumor necrosis factor. As asthma is an inflammatory disease of the lungs, such a result from fasting is likely to be of benefit.

Another inflammatory condition, eczema, affects some 10 percent of the US population. Again, while there is not yet scientific proof, fasting has been cited as helping to relieve the eczema symptoms of chronic sufferers.

One woman had eczema patches all over her body, with especially irritating patches between her fingers. After starting to fast, she reported that her eczema was somewhat improved, and several patches had even disappeared.

### 7. The experience of author Michael Mosley shows that you too can fast and be healthier. 

In these blinks, we've examined the benefits of fasting. Let's now take a look at how fasting benefited the author of this book! 

Mosley's fasting journey began with a blood analysis and a weigh-in. He decided to follow a three-month, five-to-two intermittent fasting diet. This meant he would consume 600 calories on two days per week, and would eat "normally" for the other five.

After the three months, Mosley lost 19 pounds and his body mass index dropped from 26.4 to 24 (the recommended norm is between 19 and 25). In essence, he was no longer overweight. He also was able to shrink his waist size, from 36 to 33 inches.

A blood analysis showed that his glucose levels had returned to within normal range. Before fasting, Mosley's blood glucose level was 7.3 millimoles per liter, and post-fast it was 5 millimoles per liter. The recommended amount ranges from 3.9 to 5.8 millimoles per liter. With the fast, Mosley was on his way to avoiding diabetes.

Mosley's IGF-1 level also plummeted, from the upper limits of the recommended range to the bottom, thereby decreasing his chances of developing cancer and other age-related diseases.

All great news, right? But how difficult is fasting? It turns out it's not tough at all.

Eating just 600 calories on a fasting day means that you can still enjoy a breakfast and a light dinner. People who fast also report that after the first few weeks of a five-to-two program, it becomes easier.

It also helps to know that your brain sometimes persuades you that you're hungry when you're not, such as during a social event, when you smell or see something tasty or when you're emotional. Thus staying calm helps banish hunger pangs.

Fasters also say that walking, drinking tea or taking a shower can help alleviate hunger pangs. And of course, having a fasting buddy can make your experience a lot smoother.

### 8. Final summary 

The key message in this book:

**Fasting is far more natural than you might think, as humans have either feasted or fasted as our species has evolved. Not only does fasting help you lose weight, but also it can potentially delay the onset of dementia and helps ward off common diseases like cancer and diabetes.**

Actionable advice:

**Save your brain: drop the junk food!**

An experiment showed that mice on a high-fat and high-fructose diet developed Alzheimer's disease earlier than did mice on a normal diet. All the more reason to opt for health food over junk food, even when you're rushing around during the week!

**Suggested further reading:** ** _Bulletproof Diet_** **by Dave Asprey**

_Bulletproof Diet_ (2014) shows you how to hack your body to optimize your health, lose weight and increase your mental functioning. How? Maintain a diet filled with the right proteins and fats, and learn to exercise and sleep in the most effective way possible.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael Mosley and Mimi Spencer

Michael Mosley holds a medical degree from the Royal Free Hospital Medical School. Also a television journalist and producer, Mosley in 1995 was named Medical Journalist of the Year by the British Medical Association.

Mimi Spencer is a journalist and author. Along with Michael Mosley, she is a co-author of _101 Things To Do Before You Diet_ and _The Fast Diet_. She also writes for _The Guardian_, _Marie Claire_ and _Harper's Bazaar_.

