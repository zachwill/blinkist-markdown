---
id: 5e48a3f96cee070006209341
slug: the-fearless-organization-en
published_date: 2020-02-17T00:00:00.000+00:00
author: Amy C. Edmondson
title: The Fearless Organization
subtitle: Creating Psychological Safety in the Workplace for Learning, Innovation, and Growth
main_color: None
text_color: None
---

# The Fearless Organization

_Creating Psychological Safety in the Workplace for Learning, Innovation, and Growth_

**Amy C. Edmondson**

_The Fearless Organization_ (2018) delves into psychological safety and how the workplace can become an environment in which everyone feels confident enough to pitch in and do their best. These blinks explain why people hold back on sharing their ideas at work, how this harms businesses, and how leaders can encourage a culture of openness, questioning, and experimentation that leads to learning and innovation.

---
### 1. What’s in it for me? Find out what it takes to inspire confidence in the workplace. 

We live in a knowledge economy where success is a matter of solving problems and coming up with the next big idea. It's not enough to be smart or hard-working, either. Organizations need their employees to collaborate, experiment, and respond to business needs that are constantly changing. 

But in many offices and boardrooms, people lack the confidence to do exactly this, silenced by the fear of failure, judgmental colleagues, or unapproachable bosses. 

These blinks introduce the concept of _psychological safety_ and why it should be a feature of every work environment. Through research and examples from prominent businesses, the blinks break down the ways in which a culture of openness and support enables success, and what leaders can do to develop this in their own organizations.

In these blinks, you'll discover

  * the playground habit that follows us to work;

  * why having a clueless boss helps; and

  * which company throws parties to celebrate flops.

### 2. Worrying about how we’re perceived prevents us from doing our best work. 

Imagine you're sitting in a strategy meeting. Your boss has shared some of the challenges that need to be addressed, and now she's asking the team to come up with suggestions. You've got an idea, but you're worried that others will think it's no good. So, rather than risk it, you keep your thoughts to yourself.

Whether it happens in a meeting, a classroom, or even around a dinner table, most of us have experience of having something to say but holding back in case it made other people think less of us. We learn to do this early; as children, we start caring what our peers think and avoid saying or doing anything that could make us look silly, weak, or not as cool as everyone else. 

By the time we're adults, the habit of silencing and restricting ourselves is almost unconscious, and it prevents us from speaking up when we have ideas, questions, or concerns at work. 

In a 2003 study into people speaking up in the workplace, academics Frances J. Milliken, Elizabeth W. Morrison, and Patricia F. Hewlin found that 85 percent of study participants felt unable to approach their bosses with concerns about work. The most common reason for this? The participants didn't want their bosses to see them in a negative light. 

Even seemingly confident people experience this. Take business innovator Nilofer Merchant; she was labeled a visionary by CNBC, and in 2013, she was awarded the Future Thinker Award by Thinkers50. But in a 2011 _Harvard Business Review_ article, Nilofer shared that while working at Apple, she would keep quiet about problems she noticed because she didn't want to be wrong. She's quoted as saying, "I would rather keep my job by staying within the lines than say something and risk looking stupid." 

When fear gets in the way of people speaking up at work, it's not only the individuals keeping silent who miss out. Companies also lose opportunities to generate new ideas, and this is especially dangerous in a world where businesses need to innovate if they want to succeed.

### 3. Psychological safety leads to better performance by both individuals and teams. 

Let's revisit that meeting from the previous blink. But this time, instead of worrying that your idea won't be received well, you know for sure that your boss and colleagues will respond positively. If they like the idea, they'll tell you so. And if it doesn't hit the mark, they'll give you constructive feedback. 

In this ideal scenario, you and your colleagues have _psychological safety_ — the shared feeling that you can freely express your thoughts and ideas, or make mistakes and ask for help without receiving negative reactions. 

The author came across the concept of psychological safety in the 1990s while studying medical errors in hospitals. At first, she was surprised to find that the best medical teams seemed to make more mistakes than lower-skilled teams. But a closer look revealed that they weren't making more mistakes; they were just more open and willing to report them, which led to discussions about better ways of working. 

Refined work processes aren't the only benefit of psychological safety — it also helps unleash creativity and innovation. A 2012 study by Taiwanese researchers Chi-Cheng Huang and Pin-Chen Jiang demonstrated this. They surveyed 60 research and development teams whose work demands innovative, outside-the-box thinking, and learned that teams with psychological safety performed better, while members of the other teams were too scared of rejection to share their ideas. 

For further proof, consider one of the world's most innovative companies — Google. In 2016, a _New York Times_ article shared the tech giant's research on what factors made the best teams. After studying over 180 Google teams, researchers found that the most important characteristic of a good team was, in fact, psychological safety. 

Now, innovating is difficult no matter what; when you then add different personalities who often have to work across distances and cultures, it becomes significantly harder. But where there is psychological safety, these challenges are easier to navigate. 

Why? It comes down to communication. In 2006, Professor Christina Gibson of the University of Australia and Professor Jennifer Gibbs of Rutgers University studied innovation teams with members scattered across the world and found that psychological safety helped teams communicate more openly. When teams can not only share their thoughts openly but work through them together, they're much better prepared to tackle whatever challenges come their way.

In the following blink, we'll look at what can happen when psychological safety is missing from the workplace.

### 4. The absence of psychological safety can have terrible consequences for a company, its employees, and its customers. 

Have you ever noticed something not right at work? Maybe your supervisor has made a mistake in a presentation. But she has a reputation for being harsh, so instead of pointing anything out, you go along quietly, afraid to rock the boat. 

Unfortunately, many bosses think fear is a good leadership strategy. While the atmosphere that this creates is no picnic, there can be far worse consequences. 

When leaders use fear to motivate, people can turn to extreme and sometimes dangerous methods to get the job done. Take the employees at Wells Fargo. In 2015, Wells Fargo was the leading bank in America thanks to its community banking division's impressive sales. On average, every customer was signed-up to about six banking products, around double the industry average.

But as it turned out, these impressive figures were the result of sketchy sales tactics. Employees were under pressure to hit an incredibly ambitious target of eight products per customer, and those who failed were publicly ridiculed or even fired. Scared of speaking up about the unrealistic targets, they instead opened accounts for customers without permission or lied about certain products being package deals. Two million accounts and credit cards were set up this way, and when the practice was discovered, the scandal cost Wells Fargo $185 million in settlements. 

Fear in the workplace doesn't always lead to unethical practices, but it can prevent staff from being upfront about a company's challenges, stopping them from finding solutions before it's too late.

Nokia learned this the hard way. During the 1990s, it was the top cell-phone manufacturer globally, but by 2012 it had lost this spot, along with over $2 billion and 75 percent of its market value. 

How did this happen? Well, in 2015, graduate business school INSEAD published a study of the company's fall, revealing that Nokia's executives didn't communicate openly about the threat from emerging competitors Apple and Google. At the same time, managers and engineers were afraid to tell their bosses that the company's technology couldn't compete in an evolving market. As a result, Nokia missed the opportunity to innovate and soon became irrelevant.

Businesses like Nokia and Wells Fargo are cautionary tales for any leader who thinks fear is the best way to get the most out of their teams. So, if fear has taken root in your workplace, the first step in creating psychological safety is to root it out — for good.

### 5. A fearless workplace starts with reframing failure and redefining the boss’s role. 

How many times have you been told to do your best? We get this advice throughout our lives, from parents, teachers, coaches, friends, and even anonymous quotes on the internet. Rarely are we ever told to fail.

But being OK with failure at work is the first step in creating a fearless environment. When team leaders and bosses start talking about failure as something that happens often and as a learning opportunity, people become comfortable with taking risks, trying out new things, and openly discussing their mistakes.

While failure seems like the opposite of what any company wants to do, some of the most successful ones have made the belief that it's OK to fail a key part of their work practices.

Animation studio Pixar is behind 15 of the 50 highest-grossing animated films of all time, and co-founder Ed Catmull makes a point of telling staff that every movie is bad in the early stages. This reduces their fear of failure and makes them more open to feedback. And in a completely different industry, Christa Quarles, CEO of restaurant reservation company OpenTable, encourages her team to fail often and early so that they can quickly find new strategies. 

In fact, being comfortable with failure is so important that Smith College and other schools in the United States now offer courses to help students understand failure not as a setback but as a step toward learning.

Failure isn't the only thing we need to redefine. In many workplaces, leaders are seen as authorities who know best, giving instructions, and judging how well they're carried out. But in a fearless workplace, leaders instead set the direction and goals, then encourage people to contribute their own ideas and insights.

A great example of this approach is Cynthia Carroll, a former CEO of the mining company Anglo American, who wanted to lower the number of mining injuries and deaths. But instead of just sending an order down to workers, Cynthia chose to organize meetings with thousands of the mine's employees and find out what they felt was needed to improve safety. This input shaped new safety guidelines, and after these were implemented, mining deaths reduced by an impressive 62 percent between 2006 and 2011.

> _"Those who are not the boss are seen as valued contributors, as people with crucial knowledge and insight."_

### 6. When leaders are curious and admit that they don’t know everything, people are encouraged to speak up. 

We've all heard the phrase, "No one likes a know-it-all," and it can definitely be annoying to be around someone who thinks they have all the answers. 

However, when the know-it-all is your boss, they're not so much annoying as they are intimidating. Leaders who think they know everything intimidate people out of expressing ideas and opinions. And so a key component of a fearless workplace is a boss who openly says that they don't have all the information or all the ideas. This makes it clear that they're open to learning and hearing from other people.

Former chairperson and CEO of Xerox Anne Mulcahy was so comfortable with saying she didn't know the answers that people nicknamed her the "Master of I Don't Know." This gave Xerox employees the confidence to engage fully in tackling the company's challenges, and under Mulcahy's leadership, Xerox came back from the brink of bankruptcy.

However, even when the boss admits they need help, getting people to share their thoughts and ideas isn't as simple as asking them. 

To encourage participation, leaders should ask questions in a way that shows genuine interest in what others have to say. This can be accomplished by avoiding questions that only have "yes" or "no" answers, and asking thought-provoking ones that motivate people to reflect and think creatively. 

The art of asking questions also involves knowing that different situations call for different kinds of questions. If you want to widen your understanding of an issue, ask people what they think is missing or invite those with different perspectives to chime in. If your aim is to get a deeper understanding, ask people to share the reasons behind their thinking or to give examples. 

Another way leaders can create a culture of participation is by setting up structures specifically for sharing information. These can be regular workshops, focus groups, or meetings. 

When the food company Groupe Danone started holding conferences to encourage information sharing between different departments, management noticed that people not only started generating new ideas, they were also more comfortable with speaking up and asking for help.

Once people start taking action and providing input, the feedback they receive is key to maintaining psychological safety.

### 7. When people take risks and speak up, it’s important for leaders to respond productively. 

Picture a group of five-year-olds learning about shapes. Their teacher asks if anyone can name the shape on the chalkboard, and when a child shouts out the wrong shape, he's instantly and bluntly dismissed. Do you think that child will try again? Probably not. 

In the same way, people in the workplace can be discouraged if leaders don't respond to input in the right way. 

A good place to start is by showing appreciation. Speaking up or taking action takes courage, so whether the outcome is good or bad, first thanking people for their effort helps maintain the feeling of psychological safety.

For instance, imagine a nurse who isn't sure that the doctor is giving a patient the right treatment. The nurse is nervous about speaking up because he's heard the doctor respond negatively to questions from other colleagues. But if he were to say something despite his reservations, and the doctor thanked him for the input before going on to explain her decision, the nurse would feel more confident and continue sharing his thoughts.

In the same way that input requires the right response, so does failure. But it's important to keep in mind that failure comes in different forms, and there are appropriate ways to respond to each one. 

When people fail because they tried something new and didn't get the results they hoped for, they should be encouraged, and their experiences discussed and learned from. At the pharmaceutical company Eli Lilly, they go as far as throwing parties to celebrate and share failed experiments. This may seem extreme, but cementing the idea that failure is a positive thing ensures that people don't continue to waste time and resources on experiments that aren't going anywhere. 

On the other hand, there are preventable failures, and learning from these means trying to make sure they don't happen again. This can be through training or putting new systems in place. But if the failure happens because set processes weren't followed or company values and boundaries were ignored, then the response should be a fair consequence like a sanction, suspension, or even firing, if necessary. When employees know that they always face fair feedback and consequences from their leaders, the feeling of psychological safety is strengthened.

### 8. You don’t have to be a leader to help create a fearless work environment. 

What would you do if you ruled the globe for a day? You probably have lots of great ideas for how to improve things if you were in charge but can't imagine making a difference in the real world because you're not in a high enough position of authority. 

This frustrating feeling happens in the workplace, too. But the good news is that if you want to work in a less fearful environment, there are small steps you can take to help make this happen, even if you're not the boss. 

For one, you can show your colleagues that you're curious about what they have to say and slowly create safe spaces for them to speak up. Make a point of regularly asking them for their input and expertise; this works especially well when you direct your questions to specific individuals. The next time you speak up in a meeting, hand the baton over to someone else by asking them what they think.

Now, sharing an idea or voicing an opinion doesn't mean much if no one is listening. And so another way you can contribute to psychological safety is by actively listening to people, whether or not you're the one asking the questions. When your colleagues speak up, listen attentively and respectfully, even if you don't necessarily agree with them. This shows interest and appreciation for the effort they made, as does providing feedback and building on their ideas.

Finally, you can improve the feeling of safety and support in your work environment by allowing yourself to ask for help when you need it, and letting others know that you're available to help them. Start using phrases like, "I need help," "I don't know," "I made a mistake," "What challenges are you facing?" and "What can I do to help you?" 

When people realize that others around them can be vulnerable and that there's always help at hand, they'll eventually start sharing ideas and being bold enough to reach their full potential.

### 9. Final summary 

The key message in these blinks:

**Today, success at work means being able to take risks and have conversations that lead to innovation, but this is impossible to do when people feel unsupported and afraid. When leaders and colleagues alike start inviting other voices to the table and encouraging people to learn from failure, the result is a workplace in which people and ideas thrive.**

Actionable advice:

**Play to win.**

A lot of the time, we play it safe and avoid being vocal or trying new approaches because we don't want to risk failing or being judged harshly. This mindset is called _playing not to lose_, and it's the reason we miss out on opportunities that come our way. Start adopting a _play to win_ mindset by focusing not on what you stand to gain if you step up to the plate, rather than what could go wrong. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Workplace Wellness that Works,_** **by Laura Putnam**

In these blinks, you've discovered that with the right approach from leaders, the workplace can become an environment where people feel encouraged and supported enough to meet every challenge and bring their best selves to work. But it's no secret that work and life take their toll on the mind and body, and so more and more organizations are realizing how important it is to focus on wellness if they want to get the most out of their employees.

For a step-by-step guide to creating a healthier workplace, check out the blinks to _Workplace Wellness that Works_. You'll understand why it's not enough to offer healthier snacks in the office and why your current attempts at encouraging wellness aren't getting the buy-in you hoped for. By the end of these blinks, you'll have a set of tools for designing impactful and sustainable wellness programs.
---

### Amy C. Edmondson

Amy C. Edmondson is a professor of leadership and management at the Harvard Business School. She's been repeatedly ranked as one of the world's most influential management thinkers by Thinkers50. She's spent 20 years researching psychological safety, organizational learning, and leadership, and shared her expertise in publications such as the _Harvard Business Review_ and the _California Management Review_. Edmondson is the author of _Teaming_ and _Teaming to Innovate,_ as well as the co-author of _Building the Future_ and _Extreme Teaming_. 

© Amy C. Edmondson: The Fearless Organization copyright 2018, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

