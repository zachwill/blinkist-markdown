---
id: 59009a93b238e100061eee40
slug: the-female-eunuch-en
published_date: 2017-04-28T00:00:00.000+00:00
author: Germaine Greer
title: The Female Eunuch
subtitle: The landmark book in the history of the womens rights movement
main_color: FE336B
text_color: CC2956
---

# The Female Eunuch

_The landmark book in the history of the womens rights movement_

**Germaine Greer**

_The Female Eunuch_ (1970) is an explosive feminist classic that confronts the societal expectations holding women back. These blinks argue that it's womankind's responsibility to create new definitions of femininity and take ownership of their bodies, sex and lives.

---
### 1. What’s in it for me? Learn what women have in common with eunuchs. 

Feminism made great strides in the twentieth century. Almost everywhere in the world, feminists have won the battle for women's right to vote and own property.

But there is still a long way to go toward achieving true social equality between men and women. Most importantly, women's reproductive rights and ownership of both their bodies and choices are still being debated. The fight to be respected goes on.

Germaine Greer, a major voice of the feminist movement, addressed these issues in her seminal text,

_The Female Eunuch_. It was published in 1970, but many of her ideas remain as relevant as ever.

These blinks explain her theories.

You'll also learn

  * why poetry and paintings that celebrate female beauty are anything but flattering;

  * why even female leads in comic books are not all that feminist; and

  * why vaginal orgasms are so rare.

### 2. Society has castrated women. 

The qualities that society values in a woman match those of a eunuch — subservience and sexlessness.

Women are expected to be attentive, agreeable and patient. Ambition, confidence and any other signs of a strong personality aren't tolerated. Rather than actively seeking and taking what they want from the world, women should wait until they're told what to do. They should show mindless joy when given a gift, demonstrate unfailing devotion to their families and a passionless desire for their husbands.

In pop culture, dominant women typically appear as one of two stereotypes — the cunning and sexy woman or the athletic and arrogant woman.

Whether it's a blockbuster, a soap opera or a comic book, these dominant female characters are inevitably tamed or overpowered by the hero at the story's end. Audiences are taught that "dominant" qualities in women, while enticing, will only be detrimental to a man unless he defeats them.

It often seems that the entire concept of "woman" is all aesthetics and no substance. Even the woman's body should be soft and non-threatening, devoid of signs of power or maturity.

The true essence of the woman is lost as media and brands push women to change their appearances to fit this artificial norm.

By requiring women to behave and appear as if they were castrated, societal norms also communicate the fact that women cannot and should not play a truly equal role in public — or in private.

### 3. Our cultural definition of heterosexual sex limits the sexual potential of women. 

The way we talk about sex promotes the idea that women are passive receptacles. Though a greater awareness of her own desires makes sex more enjoyable for a woman — as well as her partner — many women don't prioritize their sexual pleasure at all.

Instead, women are taught to treat sex as something to give in exchange for commitment.

Just think of the heroines of romantic novels, who make their male love interests wait for sex to ensure that these men are devoted to them and won't abandon them as soon as they have their wicked way. The woman's enjoyment of sex is excluded from this narrative entirely.

As a result, women have limited knowledge of their personal experience of orgasm. The clitoral orgasm, which is analogous to a man's orgasm, is typically considered the height of female pleasure. The vaginal orgasm, however, is far more euphoric. It's also harder to achieve, requiring both physical and mental stimulation, as well as deep emotional bonds.

Sex isn't something that women owe to men who treat them kindly — it's a pleasure that women owe to themselves as human beings. But if society continues to portray sex as a purely physical act in which women must be obedient and generous and nothing more, women will struggle to experience the full potential of their sexuality.

> _"Unhappily we have accepted . . . a notion of utter passivity and even irrelevance of the vagina."_

### 4. Women have more opportunities than ever, but still aren’t being taught to take advantage of them. 

In the nineteenth and early twentieth centuries, a women's movement called the _suffragettes_ fought to give wives and daughters the right to vote and get out of the house. They succeeded, and the subsequent generations of feminists secured many other victories.

Despite this, girls today are still conditioned from a young age to live cautiously, not ambitiously. While young boys are given the freedom to run wild and grow strong, young girls are taught to play with dolls, sit still and stay close to their mothers. Indeed, girls grow up dependent on their mothers, which prepares them for dependency on their husbands in their adult life.

Girls who have been lucky enough to escape societal norms and run around with the boys will be looked down upon as unladylike or strange during puberty. As their bodies change, they are expected to start changing their behavior too.

In early adulthood, societal pressures are still shaping the opportunities open to young women. The author argues that although more young women were attending university at the time she was writing, they weren't motivated by learning, but by the prospect of finding a husband of a higher social class.

When they begin school, girls are often far ahead of boys academically. But as learning becomes increasingly driven by independent thought, boys take the lead. Girls fall behind because they are rarely encouraged to develop these skills themselves. Instead, they're taught not to take risks — this deeply impedes young girls' creative and critical thinking abilities.

Girls often make it to university with no clue what to do with the opportunities they receive there. Rather than challenging themselves and their peers intellectually, or passionately pursuing their new interests, young women are most focused on gaining professors' approval, demonstrating the same kind of obedience that they'll readily show in their marriages.

### 5. Women are required to hide their own bodies and perform the male conception of gentle femininity. 

Men have long been the arbiters of femininity. It is up to women to reject these restrictive definitions of femininity and bring new ones into the world.

Women must start by eliminating the stereotype of the _eternal feminine_. Poets and painters throughout history have depicted women as fragile and vulnerable, likening them to delicate flowers or capturing them in images of repose and various states of undress. The eternal feminine makes women nothing more than alluring objects that can be owned by men and aspired to as a model by other women.

Women have little power beyond that of the eternal feminine because they aren't encouraged to develop the virtues that men are told to live by. Women are expected to demonstrate their morality through their benign outward appearance and a gentle, inoffensive demeanor.

Women are, of course, powerful enough to bear a child for nine months and give birth. Unfortunately, the womb, menstruation and other aspects of the woman's fertile body terrify men. Women are told to feel shame about completely natural processes in their bodies, viewing them as something ugly that must be hidden at all costs.

The success of tampons hinges on their ability to keep the ugliness of menstrual blood contained. Many major religions still deem women unclean and unfit to sleep with their husbands while on their period.

As a result, women know little about their own bodies and how to care for them. Rather than living in fear and shame, women must take ownership of their bodies and let their own gynecology empower them. This is the first step toward a new definition of femininity that goes beyond the batting of eyelashes.

### 6. Women must reject their current role in society to revolutionize it. 

Why do women find men in uniforms, from cops to military men, so alluring? Women have learned to tacitly or explicitly condone violence.

If women stopped thinking of violence as heroic, men would have far less incentive to risk their own lives and those of others in dangerous acts. Just as women are under pressure to play the damsel in distress, swept off their feet by male machismo, men are forced into the role of the brave and aggressive hero. Women should instead only let themselves be impressed by intellectual bravery.

Women must also rethink marriage. Many women live their lives with the goal of finding a man to depend on as fast as possible. Even career-driven women find themselves pressured to quit their jobs once they get married and have kids, leaving them financially reliant on their husbands.

By rejecting the dependency of marriage and taking ownership of their lives, women have a shot at achieving true independence. Through experimentation and trial and error, women can discover what it is they really want, both from their careers and from partners.

Just as women shouldn't be afraid to take risks professionally and fight for new opportunities, nor should they be afraid to have sex with more men in order to get to know their own bodies, minds and partners better. The task now is to create and normalize a new, liberated definition of a woman. Women must show the world that they have far more to offer than the castrated slaves that society pressures them to be.

### 7. Final summary 

The key message in this book:

**Societal pressures encourage women to behave as if they were eunuchs, to look entirely non-threatening and to view the natural processes of their bodies as shameful. By rejecting ideals of female dependence, women can create and demonstrate a new and empowering definition of femininity.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Second Sex_** **by Simone de Beauvoir**

_The Second Sex_ (1949), an 800-page feminist classic, explains how woman has been shaped into the "Other," second sex — the negative counterpart to man. By examining history, myths, biology and life experience, de Beauvoir paints a clear picture of why woman is subjugated to man, and how womankind should respond.
---

### Germaine Greer

Germaine Greer is an Australian writer and academic. She has held teaching positions at the University of Warwick and Newnham College in Cambridge. Greer is also the author of _The Whole Woman_ and _Shakespeare's Wife_, among other titles.

