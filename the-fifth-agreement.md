---
id: 55c123923439630007ff0000
slug: the-fifth-agreement-en
published_date: 2015-08-07T00:00:00.000+00:00
author: Don Miguel Ruiz and Don Jose Ruiz
title: The Fifth Agreement
subtitle: A Practical Guide to Self-Mastery
main_color: 8A91BE
text_color: 666B8C
---

# The Fifth Agreement

_A Practical Guide to Self-Mastery_

**Don Miguel Ruiz and Don Jose Ruiz**

Our perceptions of our true selves are clouded by the society we live in and the people around us. _The Fifth Agreement_ (2010) introduces five pacts that will help you strip away those misconceptions and uncover the truth about who you really are — without running off to a mountain top to be a hermit.

---
### 1. What’s in it for me? Discover your true self by committing to five agreements. 

Do you live your life freely and in accordance with the _real_ truth about yourself and your surroundings? Or as you grew up did you gradually lose the ability to explore and create without judging yourself and others? If you agree to let go of the self-limiting habits that prevent you from seeing the real truth, you can recover this power.

In these blinks you'll learn how to regain your normal human tendencies by committing to the five simple agreements that should guide your everyday life.

You'll also learn

  * how _should_ can be used to judge and punish;

  * that opinions are only relative truths; and

  * why assumptions can lead to unnecessary dramas.

### 2. We lose our natural human tendencies thanks to the process of domestication and the symbology we’re taught. 

Every day our mind is influenced by society and the people around us. Our parents and our environment teach us what they've learned and what they believe, and when we take on these beliefs, we become _domesticated_.

But prior to domestication, we go about our normal tendencies without self-consciousness or self-judgment. That is, from the moment we are born, we have tendencies to explore, create, eat, and so on, and we act on these tendencies without question.

When we reach toddlerhood, we frolick around naked and carefree without thinking _I'm too fat_, and express our creativity without thinking _I painted with the wrong colors_. We seek to fulfill our needs and instincts, simply because it makes us happy.

Gradually, though, we're introduced to symbology, and use the symbols that we're taught during our upbringing to judge and punish ourselves and each other.

But we do need symbology — it's how we express ourselves. That means that the words we use are graphical symbols that we've given a certain meaning in order to communicate through sound and writing.

However, the symbology we're exposed to and taught when growing up is loaded with cultural and social values and norms.

In this way, we also learn how we _ought to_ be, in accordance with particular values and norms, and we use abstract notions of _wrong_ and _right_, _fat_ and _skinny_, _beautiful_ and _ugly_ and so forth to gauge these norms.

For instance, we might learn that we _should_ go to church every Sunday to be a good Christian, or we _should_ be skinny, smart and beautiful in order to experience a happy life.

Unfortunately, many of us judge and punish ourselves and others for not fulfilling the different _shoulds_ of symbology, and over time, we lose the ability to behave in line with our normal human tendencies — the unselfconscious toddler in all of us.

### 3. Our knowledge and truth become relative through symbology. 

As we grow up we make _agreements_ with one another and with ourselves. One agreement is to share certain symbologies through which we can communicate, like language, as we learned in the last blink. You're only able to comprehend what you are reading or hearing now because you agree with the writer on the meaning of the words.

But here, the key word is "agree"– it's not about universal truth. The symbology we're taught feels true because we _agree_ that the symbols hold a certain value or meaning, not because it's the absolute truth.

A _tree_, for instance, is true. But the word _tree_ would have no meaning or value to a Chinese person who doesn't understand the English language.

So if we don't agree on a particular symbology, we cannot extract meaning from it. Thus, symbology is a relative truth rather than a real truth, since its meaning is solely dependent on people and context.

Furthermore, as our wider knowledge is based on shared symbology, it can be understood as relative truth. When we perceive our environment and those around us, this world becomes our _truth_. And through the symbology that is passed on to us, we form entire belief systems about these objects and people, which become our _knowledge_.

In addition, the various religions, philosophies, and ways of thinking create our knowledge, and come about through agreements we make with those around us about shared symbology.

For example, in some regions of the world, people have _agreed_ that there is one almighty god, while in other areas, people have _agreed_ on multiple gods.

Everything we _know_ is therefore a symbology shared among certain people, and since this symbology isn't the same for all of us, it can only be a relative truth.

So if the majority of what you perceive about yourself is a relative truth created through symbology, how do you know who you _really_ are? By following the _five agreements_.

### 4. The first agreement: be impeccable in your use of words. 

Words are a powerful creation tool — we use them all the time. But many of us don't fully fathom just how much words matter.

When you use words charged with self-judgment and self-rejection, you're using words against yourself.

We actually create our life stories through words. But when we speak with self-judgment and self-rejection, the words we use are destructive. For instance, by telling yourself, "I'm not beautiful, skinny or smart," you are using words to judge yourself.

But what if you became aware of how you used words against yourself? You could replace these words and create a new and better story about yourself. You could even take it further and, by using words impeccably, narrate a beautiful story about yourself that brings you joy.

"Using words impeccably" means using them to your benefit and not contaminating the story of your life with destructive self-judgments.

If you can train yourself to do this, your fears of being judged or rejected will fade, because you'll know they're just symbology relating to norms, and that symbology is only relative truth.

So the next time someone uses the word against you, or when you use the word against yourself by talking about your inadequacies, you'll be able to pinpoint and see beyond the symbology, and perceive the negativity as mere words.

Eventually, when fear of rejection and judgment dissipates, the desires and intentions in your life will unfold.

### 5. The second agreement: stop taking everything personally. 

Remember a time when a family member or friend didn't understand your way of thinking, and how hurt or angry you felt about it? We've all experienced this. But there's really no use in taking misunderstandings or negative opinions to heart.

It's vital to remember that the opinions that people hold about you only concern their personal _image_ of you. What they think about you isn't the real _you_.

If you were watching a movie about yourself, which you'd written and directed, and later watched a movie about your mother, which she'd written and directed herself, they wouldn't share many similarities. Even though all the characters would be the same, including you and your mother, they would be portrayed in the way the movie-maker saw them. This is because you view yourself in a certain way, and this would come through in your movie. But as you watch your mother's movie, you'd realize that she sees you differently.

Everyone lives the story that they've created about themselves. We form opinions based on the story we've created, but this is simply an _image_, not the truth. It's important to remember that opinions are grounded in personal perceptions that often don't correspond with your own.

To free yourself, you must agree to stop taking other people's opinions personally. Whatever your parents, friends or co-workers think or say about you, isn't about the real _you._ Therefore it makes no sense to take these opinions personally.

As you no longer have to worry about the opinions other people have about you, you'll be freer to do whatever you desire, because you'll feel immune to judgment.

### 6. The third agreement: stop making assumptions and you can focus your energy on the actual truth. 

Have you ever made a false assumption about why someone was late or didn't show up to your meeting or date? Most people make assumptions that turn out to be false, and this can create unnecessary worry and drama.

As humans, we have a need to predict, explain and justify events that happen in our lives, and when we're uncertain about something, we fill in the gaps by imagining what people are thinking, doing, and feeling.

But all these assumptions are lies that we tell ourselves, and when we buy into them, we can create hurt and worry.

Imagine that you have a daughter who is out partying with her friends, and one night she doesn't make her curfew. You immediately jump to conclusions and think "What if something terrible happened?", and a whole host of scenarios start streaming through your mind. Then, a few minutes later, your daughter arrives home safely with a big smile on her face, proving that the drama in your mind was created by your assumptions.

There is a far better way to spend your time and energy than by making assumptions, and that is to focus on the actual truth.

When we do this, the energy that we would have otherwise invested into making assumptions is used more wisely.

For instance, if you assume that a man is evil due to some beliefs he has that differ from your own, then you've distorted the truth by creating an assumption, and all of your energy is drained by this distortion.

### 7. The fourth agreement: do your best and incorporate the agreements into your life. 

You've now learned about three agreements: _be impeccable with words_, _don't take anything personally_, and _don't make assumptions_. But how can you integrate these into your life? By dedicating yourself to the fourth agreement: _always doing your best_.

When you do your best, you'll find that the first three agreements will manifest far more easily in your life.

The first three agreements will spark positive change in your life, but mastering them won't be possible if you don't practice them to the best of your ability.

By doing your best, your old ingrained habits — of being lazy or unscrupulous with words, of always taking what people say to heart, and making assumptions about other people's behavior — will slowly die out. And gradually, you'll begin to regain control over your life.

So how do you go about doing this? You must practice them as much as you can.

The person you are today wasn't created overnight; who you are and how you behave is a result of lifelong practice.

Imagine all of the skills you've acquired throughout your life. Whether it's speaking, reading or riding your bike, you'd be clueless about these things if you hadn't practiced them frequently. Therefore repetitive practice is essential if you want to incorporate the three agreements into your life.

Practice is the only way, but it requires patience, so don't fret if you fail to immediately master all of them at the same time.

### 8. The fifth agreement: be skeptical, but learn to listen. 

Everyone wants to express their point of view by telling stories about their experience of the world. But how do you know which of the stories are true? By committing to the fifth agreement of _being skeptical_.

To thoroughly explore what symbology people are using in their storytelling, you have to be skeptical of what is being communicated. This means agreeing with yourself to not naively buy into everything you hear, and to look _behind_ the symbols.

When someone communicates a message to you, ask yourself if this is the _real_ truth, or the _relative_ truth. By doing so you'll become open to the truth that lies _behind_ the symbols and words. For instance, when you hear a personal story, don't blindly agree with the person's point of view, but do abstain from judgment, too.

Remember, though, that being skeptical isn't about discarding everything that everyone says. To fully understand what people are communicating, their intentions and their point of view, you also have to learn to listen. So listen to how they express symbols, while acknowledging that their point of view will be colored by their beliefs.

By listening in this way, it's easier to choose how to react. For instance, you'll probably want to ignore someone who's telling you you're not smart enough or good-looking enough. By being a little skeptical, you'll be better able to discern that what they are communicating is only _their_ relative truth and not a _real_ truth that you have to react to.

### 9. Final summary 

The key message in this book:

**If you commit to the five agreements:** ** _be impeccable with the word_** **;** ** _don't take anything personally_** **;** ** _don't make assumptions_** **;** ** _always do your best_** **; and** ** _be skeptical, but learn to listen_** **, you'll be able to uncover who you really are and live an easier, happier life.**

Actionable advice:

**Take a look in the mirror.**

Next time you look into the mirror, remind yourself that what you see is what your mind perceives. It's not absolute reality, because what you see in the reflection is not the same as what your mother or a dog or a bird sees. By bearing this in mind, you'll be one step closer to knowing the difference between reality and relative reality.

**Suggested** **further** **reading:** ** _The Power of Now_** **by Eckhart Tolle**

_The_ _Power_ _of_ _Now_ offers a specific method for putting an end to suffering and achieving inner peace: living fully in the present and separating yourself from your mind. The book also teaches you to detach yourself from your "ego" — a part of the mind that seeks control over your thinking and behavior. It argues that by doing so you can learn to accept the present, reduce the amount of pain you experience, improve your relationships and enjoy a better life in general.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Don Miguel Ruiz and Don Jose Ruiz

Don Miguel Ruiz is the author of _The Four Agreements_, which remained a _New York Times_ bestseller for over seven years. His other books include _The Mastery of Love_ and _The Voice of Knowledge_. He draws on the philosophies of the Toltec culture, which flourished in Mexico over a thousand years ago.

Don Jose Ruiz is the son of Don Miguel Ruiz. Don Jose Ruiz teaches and lectures both in the United States and internationally, helping to spread the wisdom and teachings of Toltec culture.

