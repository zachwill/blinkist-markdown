---
id: 5a3a398ab238e10007128aed
slug: the-fifth-trimester-en
published_date: 2017-12-29T00:00:00.000+00:00
author: Lauren Smith Brody
title: The Fifth Trimester
subtitle: The Working Mom's Guide To Style, Sanity, and Big Success After Baby
main_color: FFEA4F
text_color: 736924
---

# The Fifth Trimester

_The Working Mom's Guide To Style, Sanity, and Big Success After Baby_

**Lauren Smith Brody**

_The Fifth Trimester_ (2017) is packed with advice and tips for new mothers on how to successfully return to work after their maternity leave. Covering important details like what clothes to wear, how to pick the best day care and how to feed your baby even when you're not with it, the book draws on the author's own experiences as well as interviews with other new moms. This is a practical and inspirational self-help guide for new mothers who may feel overwhelmed by the prospect of returning to their job.

---
### 1. What’s in it for me? Enter the workplace with postpartum clout. 

The working mom is the new Renaissance person: she seems capable of everything from managing teams and issuing concise orders at work to keeping a small human being happy and well-fed at home. It's an awful lot, so how do you take on this type of superwoman lifestyle?

You prepare for a fifth trimester!

Covering everything from breast pumping hacks to how to handle workplace jealousy, these blinks will give you hands-on, practical and inspirational advice on how to stay happily working while bearing the new title of "mother."

You'll also find out

  * how to handle your postpartum pooch;

  * why you need to relinquish responsibility to your partner; and

  * how not to act "mom-perior."

### 2. Minimize postpartum depression by making efforts to take care of yourself and your appearance. 

When the author, Lauren Smith Brody, returned to her job after having her first child, some things were going surprisingly well. She was getting her work done, earning enough money to pay the nanny, arriving home on time and getting rather efficient at using the breast pump.

But when she stopped to look at herself in the mirror, it was a different story.

The author found it difficult to cope with her "postpartum pooch" and the desire to get back to looking "normal." But she also found that her worries were alleviated when she put extra effort into taking care of herself and her appearance.

Research has shown that self-compassion is an important part of reducing the toxic self-critical view that new mothers often have. So the more you make sure to nurture yourself, the more confident you'll start to feel.

Perhaps, like the author's friend, you might benefit from putting a confidence-boosting message on the top corner of your mirror so that you can read it to yourself in the morning.

The right clothes can also help. Nitzia Logothetis is a psychotherapist and the founder of the Seleni Institute for Maternal Mental Health. She recommends avoiding ill-fitting outfits and wearing clothes that reflect how you _want_ to feel. New moms will start to feel more confident and put-together when they look in the mirror and see someone who's well dressed.

You might be thinking: If only I had time to spend on choosing outfits and applying makeup! Well, believe it or not, there are ways to improve your appearance that don't take up too much time.

First of all, stay hydrated. Drinking lots of water will help diminish the dark circles under your eyes caused by lack of sleep.

There are also eye creams containing algae that work wonders for those dark circles, and with a little practice you can get a quick makeup routine down to around a minute!

> _Studies show that a little makeup can make women appear four years younger while improving first impressions by 37 percent!_

### 3. Trust your partner to rise to the challenge and do their share of the parenting. 

Most new moms would love to have a partner that they can depend on — someone who'll both be there for them _and_ take on half of the childcare duties. However, many first-time mothers don't trust their partner to be able to care for the new baby.

In a survey of new moms, the author found that 49 percent believed their partner would need at least seven weeks before being up to the task. But she also found that these moms were underestimating their partner's abilities.

As it turned out, those partners were ready, willing and able, without delay, 76 percent of the time. So before you underestimate your mate, know that many dads are ready to quickly step up to the challenge.

According to Carolyn Pirak, who studies marriage longevity as director of the Bringing Baby Home Program, many mothers doubt their partner's abilities due to unresolved guilt about not being able to take care of everything themselves. So instead of falling into this trap, remind yourself that it's okay to let go of some of the responsibility and trust that your partner will pick up the slack.

Trusting your partner will be especially important when you're ready to get back to work, so keep these helpful tips in mind:

When learning new childcare skills, such as baby CPR, take classes with your partner. Research suggests that doing new things together strengthens the bonds of a relationship. And being next to him as the two of you master new skills together will be reassuring when the time comes for you to head back to work.

It's easy to doubt your partner's abilities when you're observing him and second guessing his every decision. But when you're not around, all you can do is trust. So you might find it helpful to think of returning to work as an emergency situation, which often isn't far from the truth. In emergencies, it's often easier to let go and accept the fact that you won't be in total control.

> _In the author's survey, 71 percent of partnered respondents said they fought more with their partner during their first three months back at work._

### 4. Make sure you find the right day-care service and recognize natural feelings of jealousy. 

When the end of your maternity leave arrives, your precious little bundle of joy will have spent much of the past months snuggled up against your chest. Separating yourself and putting your child into day care can be downright terrifying, and the only way you'll be able to think about work is to make sure you're dealing with a good facility.

The National Institute of Child Health and Human Development has a helpful summary of the top three concerns for making sure a potential day-care facility will have a positive impact on your child.

When a baby is between six and 18 months old, its day care should have no more than three children per adult staff member, with a maximum of six kids per group.

All staff members should have a certificate in child development or a university degree in a similar field. No one's education should be limited to a high school diploma.

The staff should also demonstrate "positive caregiving," which means having a cheerful attitude toward children and day care in general. This should involve activities like singing and reading and the use of positive words to praise and encourage good behavior.

Once day care begins, it's important to remember that feelings of jealousy about someone looking after your child are absolutely normal. You might even hear your child call the caregiver "mommy," but there's no need to worry. All young children have a strong tendency to seek close bonds with those caring for them and it doesn't mean you're being replaced.

In fact, it's important that the child have a day-care worker who's consistently there, as opposed to a new one every week or month. Research shows that a steady presence allows children to feel calm and secure — so it'll be best for your child, and for you, if he or she can develop a bond with a steady caregiver.

### 5. Being an expert with a breast pump requires the right preparation. 

If you're anything like the author, one of your primary concerns about returning to work is breastfeeding. Statistics show that around 83 percent of working moms return to their jobs while still breastfeeding.

A breast pump can solve the problem, but you'll first have to consider what kind of equipment you'll need and how much time and milk you'll need to put aside.

In taking a poll of new mothers who were determined to breastfeed and work at the same time, the author found that the average mom could comfortably pump milk for the first eleven months. But in order to go down this route, you'll need the right equipment and clothes.

The best setup is to get two breast pumps — one for work and one for home. You may think it's a waste of money now, but after the first few times you forget to bring it to work, you'll realize that two breast pumps will bring peace of mind and save you a lot of needless running around.

As for extra equipment, you'll need ice packs to keep things fresh and a good supply of backup storage bags so you're not suddenly stuck without a place to put your milk.

When you're at work, the right clothes are important as well. Button down shirts and wrap dresses are the two easiest ways to allow for quick and easy access. The clothes to avoid are dresses that zip at the back and anything made out of silk, which is an unforgiving material for the sometimes messy business of pumping.

You'll also need a good amount of room in your freezer for your supply of breast milk, as it's recommended to try and keep a two-day supply in there at all times. With a two-day supply always being kept frozen you won't have to worry about common mishaps, like your daytime caregiver accidentally using up next day's supply and giving your baby too much milk. With a two day supply in the freezer you'll still have enough, even if this were to happen.

It's also smart to start using the bottle before you return to work, so that your child has time to get accustomed to it.

### 6. To avoid quitting your job, focus on its benefits and the reasons you chose it in the first place. 

Many first-time moms don't look forward to their first day back at work. If you're one of these, here's a statistic you might find comforting: In 2015, a Harvard Business School study showed that children of working mothers grow up to be kinder and more ambitious.

Still, even this thought might not drown out the voice in your head that's saying how much you'd rather be with your child than stuck at work Monday through Friday. So it's a good idea to keep in mind the benefits your job provides.

The largest predictor of whether a new mom will return to her previous job after maternity leave isn't her occupation, her seniority or how much her husband earns. It's how rewarding she finds her job.

If you're feeling resentful toward your job, think about the reasons you started your job in the first place. Was it the work? The people? The confidence or self-esteem it provides? Whatever the reason, see if you can't draw on it when you get the urge to quit.

Another helpful piece of advice is to return to work slowly. Instead of jumping back in full-time, see if you can't extend your maternity leave by returning part-time and easing your way back to a normal schedule over a number of weeks.

This can greatly reduce your chances of quitting. Indeed, studies show that women who take shorter maternity leaves experience more "daily reentry regrets." And the more regrets women have, the more likely they are to quit.

A gradual return, on the other had, has shown to be a great help in getting new moms to refocus their attention back on their job.

### 7. Make a smooth return to your job by being sympathetic and honest with your colleagues. 

So once you've made it back to work, you can expect to pick up right where you left off, right? If only it were that simple.

Even when the author polled working mothers, 30 percent admitted that, before they had their child, they resented coworkers who returned to work after having a baby.

You might think it best to ignore your colleagues' opinions of your situation, but this isn't wise. Instead, it's best to understand why this resentment is common and practice some give and take.

New moms are often resented at work because their coworkers think they receive special treatment and can pop off whenever a baby-related situation arises. No one wants to come across as being "the needy one" on a team, so it's best to establish your trust and strong work ethic with colleagues.

One of the best ways to do this is to make sure they know you'll be eager to repay any favors you may need to ask due to sudden family emergencies. The wrong way is to start acting superior, or "mom-perior," by suggesting that all other jobs are child's play compared to the job of motherhood. This will only ensure that your childless colleagues resent you.

Instead, remember that others have responsibilities that are just as important to them as your new baby is to you. If you get time off due to your new child, don't encourage any jealousy or animosity by making it seem especially important. Be transparent and honest about it, and remind them that they will get the same treatment if they need to tend to a sick family member or spend some time in the hospital.

Emotional transparency is a good practice as well.

Pretending like motherhood is all cute photos and giggling babies can encourage jealousy or resentment. So be sure to share the stories of sleepless nights, filthy changing stations and stained clothes, too.

Equipped with these tips, you should be on your way to re-entering the workforce as smoothly as can be expected. ****

### 8. Final summary 

The key message in this book:

**It is absolutely normal to feel overwhelmed by the prospect of returning to work after having a baby. Many new moms find it an extremely emotional and challenging time; however, there are practical steps you can take to reduce stress and make your return as smooth as possible. Among these concerns are preparing your breast pumping routine, finding the right day care and managing your colleagues' expectations. In addition, it's important to give yourself time and attention to reduce postpartum depression and maintain a healthy relationship with your body, your baby and your partner.**

Actionable advice:

**Use the 60-second escape plan.**

With a new baby in tow, you might have to spend a lot of time in the morning, before work, looking after it. Luckily, it's possible to get your makeup on in just 60 seconds:

  1. Seconds 1-10: dab on under eye concealer.

  2. Seconds 11-20: dab tinted moisturizer onto nose, forehead, cheeks and chin and blend out from the center with finger.

  3. Seconds 21-40: brush mascara onto your upper lashes only.

  4. Seconds 41-50: apply cream blusher to the apples of the cheeks.

  5. Seconds 51-60: apply large dot of lip gloss to the bottom lip and rub in. Voila!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Moms Mean Business_** **by Erin Baebler and Lara Galloway**

_Moms Mean Business_ is a guide to time management for mom entrepreneurs. These blinks help you discover where your true priorities lie, and provide you with planning techniques that will make it possible for you to dedicate more time to your ambitions and yourself.
---

### Lauren Smith Brody

Lauren Smith Brody is the former executive editor of _Glamour_ magazine and founder of the Fifth Trimester Movement, which brings together new parents and businesses and helps them to create family-friendly workplaces.

