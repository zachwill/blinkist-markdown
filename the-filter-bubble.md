---
id: 58c6b3ad8d90030004072610
slug: the-filter-bubble-en
published_date: 2017-03-16T00:00:00.000+00:00
author: Eli Pariser
title: The Filter Bubble
subtitle: What the Internet is Hiding from You
main_color: 4490A8
text_color: 346D80
---

# The Filter Bubble

_What the Internet is Hiding from You_

**Eli Pariser**

_The Filter Bubble_ (2011) offers an insightful and critical look at the internet. Specifically, it puts under the microscope the dangerous consequences of data collection and the way it is used to personalize the internet. Discover just how many things are being hidden from you every time you click the search button, and why you shouldn't always take internet search results at face value.

---
### 1. What’s in it for me? Step into your private filter bubble. 

We all know the internet is vast. Indeed, it's a veritable universe of information — an exciting frontier open to all. Or at least that's what we're led to believe. In reality, though, our access to this digital cosmos is closely monitored, our every click subtly guided. We think we're surveying the heavens, but, more often than not, we're never able to see much farther than our own backyard.

How did our view become so limited? Well, internet giants like Google, Facebook and YouTube have created an individualized web for us. By using personalization and filters, such companies ensure that we never get the full picture — that we're always confined to our own digital bubble.

In these blinks, you'll discover how we get stuck in a digital bubble to begin with. You'll learn how we shape these bubbles and how they shape us, too.

You'll also learn

  * how much data there is;

  * what Google knows about you; and

  * why the internet affirms your beliefs.

### 2. Due to the overwhelming vastness of the internet, many people have embraced personalization. 

It's not unusual to feel overwhelmed by the seeming infiniteness of the internet.

After all, there is a mind-boggling amount of data out there.

Here are some numbers that'll give you a sense of what's going on: Over the course of a typical day, 900,000 blog posts are created, 50 million tweets are sent, 60 million updates are logged on Facebook and 210 billion (that's right, billion!) emails are sent. And this is just the tip of the internet iceberg.

If that's still difficult to comprehend, here's another way of looking at it. As the former CEO of Google, Eric Schmidt, puts it: To record and store the two thousand years of human communication prior to 2003, one would need five billion gigabytes of space. Yet this amount of storage would only hold _two days_ of communication in 2011.

This overwhelming amount of data is the main reason people have embraced the personalization of the internet. Filters such as the recommendation features that many sites offer simply make the internet a more navigable place.

Media analyst Steve Rubel even has a term for what people experience when faced with the unfiltered vastness of the internet: _the attention crash_.

Today, it's so easy and cheap for people all around the world to communicate and create content that there's no way to catch it all. People jump from an email to a YouTube clip to a news site and so on without any focus or capacity to identify what's relevant.

This is why the giants of the internet — companies such as Google, Facebook, Yahoo and Amazon — started offering personalized filters that uniquely match results with a person's individual tastes and preferences. With these filters in place, scrolling through the data becomes less overwhelming and finding and identifying relevant information is a whole lot simpler and faster.

Imagine if Netflix, Amazon or iTunes offered no personalization features. You'd have to scroll through hundreds of thousands of titles, categorized alphabetically or by genre alone, making it the type of daunting task that you'd be loath to undertake at all.

### 3. Striving to produce increasingly relevant results, internet companies collect more and more personal data. 

So, obviously, personalization has some major advantages. But there's also a dark side that involves an ongoing battle for individual information.

The distressing problem with the personalization of the internet is this: relevant results rely on personal information — the more personal the better. This means that corporations like Google are out to learn everything about _you_.

Google's founders, Larry Page and Sergey Brin, want to provide people with the best, most tailored search results, which means using "smart" algorithms than don't rely on keywords alone.

An important part of Google's algorithms are links. A site is considered more relevant when other sites link to it. And if these "relevant" sites link to other sites, those sites will also be considered more important, and so on.

But to provide accurate and personalized results, Google needs as much data as it can get its hands on.

So the company stores and monitors each and every datum. After every Google search, they track which results the user is offered as well as the "click signals" — that is, which websites the user ended up clicking through to.

Google's ability to collect information received a significant boost in 2004, when they began offering personal services such as Gmail, services that require the user to log in. This provided Google with even more detailed knowledge about people's personal preferences, because it could now cross-reference personal data, such as age and location, in combination with each person's usual link-clicking behavior.

With this new info in hand, Google's algorithms became more refined and its search engine results provided more personalized and tailored results.

Both Google and Facebook are leading the way in collecting our personal data.

It's believed that for approximately 96 percent of all US households, Google has an average of 1,500 pieces of information. This information could range from the names of your relatives and friends to whether you are left- or right-handed.

Facebook also continues to collect vast amounts of personal and detailed information, such as who you date, where you go to eat, where you work and the things you "like." All the information that is freely deposited on news feeds is ultimately added to Facebook's growing treasure trove of data.

### 4. The internet democratized the news, but it’s also mechanizing and personalizing what we read. 

The _New York Times_ once ruled the advertising game. If you wanted to reach the best and brightest minds, you would pay whatever the _New York Times_ was charging for ad space. That's because the best and brightest relied on the _Times_ for their news.

Of course, this is no longer the case. Most of us know that the internet has democratized the way we get our news — and we'd probably be tempted to say that this development is a good thing.

Take the scandal surrounding former CBS anchorman Dan Rather, for instance. In 2004, during George W. Bush's presidential campaign, CBS News announced that they possessed a document which allegedly proved that Bush had lied about his military record. Dan Rather, who was covering the campaign, defended the document. Shortly before the election, however, a conservative activist posted in a right-wing forum that the document was inauthentic. The discussion quickly spread to other online communities, and the verdict came down a few days later: the document was indeed inauthentic. CBS issued an apology — but Dan Rather retired soon thereafter.

Today, internet users and blog communities have a major voice in journalism, a voice that is getting more and more audible. In the best cases, this voice serves as check on the most prominent publishers and news broadcasters.

But the democratization of the news has gone hand in hand with its mechanization and personalization.

The internet provides access to massive amounts of news, so whether you're interested in sports or politics, you can fire up your browser and find more information than could possibly fit into one newspaper.

This is both terrific and troublesome because the overwhelming amount of news requires some level of filtration.

Since human curators are expensive, it's left up to some kind of software code to recommend news for our consumption. And since your personal information will reveal you to be either a liberal or a conservative thinker, this software code will filter out articles that might challenge your point of view.

This is where we see how the personalization of the internet becomes less about recommending a movie you might like and more about monitoring what news you are exposed to.

And it was this shift that created the filter bubble. As we'll find out in the next blink, this bubble can even affect how we learn.

### 5. Due to the filter bubble, we’ve become overconfident in our beliefs and are learning less. 

The filter bubble is what we can call our own personalized internet environment, and it impacts our relationship with the world in two important ways.

First of all, since the filter bubble does its best to present you with information that aligns with your personal tastes and beliefs, you'll likely become very confident that those beliefs are right.

This is known as _confirmation bias_, and it happens when you believe something — that the earth is flat, for instance, or that climate change is a government conspiracy — and then deny everything that doesn't reinforce that belief.

As you can imagine, confirmation bias proliferates when everyone is living in their own filter bubble. It's natural, of course, for people to want to reinforce their personal beliefs; however, an issue shouldn't be considered settled until both sides of the argument have been heard, which happens less and less when the content people see is always tailored to support their beliefs.

This bubble also affects our curiosity, which is vital to how we grow and learn.

The psychologist George Loewenstein points to the "information gap" as the reason humans become curious. An information gap is any barrier that stands between us and a piece of information, like the wrapping paper on a present. It makes us curious and excited — ready to tear off the paper and find out what's hidden beneath.

But the filter bubble removes the information gap by hiding the fact that anything is being filtered to begin with. Since each search turns up an abundance of information, you think you're getting an accurate picture of what's going on in the world, even though each search makes an encounter with anything unfamiliar or challenging less likely.

And, of course, it's challenging information — things we don't fully understand — that spark our curiosity. So how can we learn new things if we're living in a filter bubble?

> _"…personalized filters limit what we are exposed to and therefore affect the way we think and learn."_

### 6. You shape the internet, but the internet also shapes you. 

You might have always thought that the internet is a one-way street — information flows into your mind from the web. But, as you've already learned, things are much more complex than that.

To fully understand the relationship between you and the internet, you should first know that you help shape the way the internet works.

The internet wants to provide a personalized experience, so it needs to know who you are.

It does this through the algorithms that are programmed within it, which are designed to figure out what you like based on information such as the sites you visit, the links you follow and the things you share.

This is the data that will determine what content you are then exposed to. As you provide companies like Google and Facebook with more data, they in turn refine the content that is provided to you. But since you're the one providing the data, you're also the one shaping the internet you interact with. 

And remember: the internet also shapes you.

As the content you're exposed to becomes more personalized, the array of topics and ideas you're exposed to becomes more limited, thereby influencing your perception and your identity.

Self-fulfilling prophecies occur by creating a reinforcing loop between belief and behavior. The internet, by creating a _you loop_, essentially creates self-fulfilling identities.

The "you loop" starts when Facebook and Google get their initial idea of who you are from limited data removed from any context. This is Google's theory of you, and it might be based on your general interest in science and a couple movies you liked. Of course, it is nothing like the real you, since your identity is much more than a taste for science and a handful of films.

Nevertheless, they'll use this information to suggest specific internet communities as well as news and ads that align with these highly specific aspects of your identity. And there's a good chance that you'll end up clicking on one of these things and confirming Google's theory, thereby prompting more science-related content. In this way, the internet begins subtly molding your personality.

### 7. As technology advances, personalization will be employed more and more. 

If you are on Facebook, you're probably familiar with the upsetting experience of being tagged in an embarrassing photo that was taken during one of your less-than-shining moments.

In these cases, the best you can do is untag the photo, cross your fingers and hope that it never resurfaces.

But face recognition technology is on the rise right alongside personalization, and it will only add to the difficulty of protecting your privacy.

As uneasy as it might make you feel, it is possible that anyone with a computer will one day be able to search the internet for a face just as easily as they can search for a name.

Once face recognition becomes mainstream, there's little hope of protecting your privacy. You can say no to photos and do all the untagging you want, but your face can still pop up in a search, even if you were just walking along and got caught in the background of someone else's selfie.

For companies and advertisers, your location is just one more important piece of data, and in some places face recognition is already transforming the ads in public spaces.

The idea of walking along and watching as all the ads on billboards target _you_ might sound more suited to some dystopian science fiction movie, but, in Japan, this is already a reality.

There's a very special, and very personalized, billboard already being used in Tokyo. As you walk past it, it scans your face, looking for a match in its database of 10,000 pictures. As soon as it determines your gender and age, it'll show ads tailored to that specific group.

Indeed, we're already living in a brave new world, and while there might be things that aren't in our control, we should, at the very least, remember that the filter bubble is there — and try to step outside it whenever we can.

### 8. Final summary 

The key message in this book:

**Despite appearances, the internet is far more diverse than you could ever imagine. But due to personalization and filtration, we miss out on valuable information that could inform us, spark our curiosity and help us grow as intelligent human beings. Instead, companies are focused on advertising and appealing to our personal tastes, so we end up trapped in a bubble that simply reinforces our previously established ideas of the world.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _You May Also Like_** **by Tom Vanderbilt**

_You May Also Like_ (2016) dives into the ever-changing world of taste, or what you like and why you like it. Trying to guess whether a consumer will enjoy a movie or buy a product is both tricky science and big business, as a myriad of different factors influences the decisions you make daily.
---

### Eli Pariser

Eli Pariser is a political activist and board president of the advocacy group MoveOn.org. He is also the cofounder and chief executive of Upworthy, a viral content website. Pariser's writing has appeared in many respected publications, including the _Washington Post_ and the _Wall Street Journal_.

