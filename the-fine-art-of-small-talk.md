---
id: 5cf784f16cee070008ddca64
slug: the-fine-art-of-small-talk-en
published_date: 2019-06-06T00:00:00.000+00:00
author: Debra Fine
title: The Fine Art Of Small Talk
subtitle: How To Start a Conversation, Keep It Going, Build Networking Skills – and Leave a Positive Impression!
main_color: 803D9A
text_color: 803D9A
---

# The Fine Art Of Small Talk

_How To Start a Conversation, Keep It Going, Build Networking Skills – and Leave a Positive Impression!_

**Debra Fine**

_The Fine Art of Small Talk_ (2005) offers practical advice for cultivating conversation skills. Drawing on anecdotes from the author's own journey to becoming a confident small-talker, these blinks will teach you how to initiate, sustain and exit conversations with ease and grace.

---
### 1. What’s in it for me? Gain the skills you need to become a smooth-talking conversationalist. 

From elevator rides with coworkers to parent-teacher conferences, small talk pervades many aspects of our lives. Though we may regard it as meaningless conversation, small talk actually lays the groundwork for establishing the rapport integral to cultivating friendships, romances and business relationships.

Its importance aside, the thought of engaging in small talk sends many of us into spirals of anxiety; rather than taking advantage of conversational opportunities, these insecurities often lead us to hover in the corner of networking events or avoid them altogether. Just think of all the opportunities you might have missed out on because you fear making small talk.

Whether you're an introvert afraid of approaching strangers or a smooth-talker looking to fine-tune your ability to charm, these blinks offer tips, skills and techniques for mastering the fine art of small talk.

In these blinks you'll learn

  * surefire icebreakers for getting a conversation going;

  * ways to exit a conversation with grace; and

  * the reasons why small talk can position you as a leader.

### 2. Small talk is a skill that can be learned. 

Shy people often think that since they weren't born with naturally good communication skills, they'll never have conversational clout. But small talk isn't a biological trait or something we intuitively know. It's time to put that idea aside and adopt a new understanding of small talk.

The bottom line is that small talk is a skill that can be learned. Sure, some people are naturally able to navigate social situations better than others. But most of us have to work to develop our conversation skills.

As a shy and overweight bookworm, the author eventually sought out a career in engineering, which involved little communication with others. When her job did entail attending meetings or conferences, she was filled with anxiety. In these situations, she would go into autopilot, attempting to converse with people by asking what their jobs were. This inevitably meant that every conversation dried up within a few minutes.

Around the time she was turning 40, she realized that her weight and negative self-image were holding her back. Then she and her husband divorced. Realizing that she needed to make some changes if she wanted to meet new people, she committed to taking care of her health and lost 65 pounds. Then she set off to cultivate social skills by observing and imitating successful conversationalists around her.

At a bar one night, her friend convinced her to approach a man who had exchanged glances with her but hadn't approached her. The man, named Rex, was delighted when she introduced herself. Their conversation that night led to a close friendship. And in time, Rex revealed something surprising to her: he hadn't approached her that night at the bar because he had been too shy!

Discovering both someone else's insecurity and the fact that her friendship with Rex wouldn't have existed if she hadn't made the uncharacteristic effort to approach him showed the author the power of small talk. From then on, she dedicated herself to mastering this skill and helping others do the same. Since then, her business, The Fine Art of Small Talk, has helped thousands of people learn conversation skills. By following the practical advice in the following blinks, you too can become an adept conversationalist.

### 3. It’s up to you to initiate a conversation with someone; in fact, it’s a mistake not to. 

Many people avoid approaching others to talk because they fear rejection. In fact, after public speaking, the biggest social fear in the Western world is initiating conversation with strangers. So if you want to widen your network, it's up to you to take the risk and approach people you'd like to get to know.

To gain the confidence required to initiate a conversation, you first have to let go of your fear of rejection. It helps to realize that, in most cases, people appreciate it when you make the effort to speak with them. This is especially true for shy people. When determining who to approach, try seeking out people who are sitting alone or have already made eye contact with you.

Once you've worked up the confidence to approach someone, initiating a conversation is simple. In most cases, when you smile at someone, they'll smile back. So, the first step is flashing a friendly smile and establishing eye contact. Next, be the first to introduce yourself. Maintaining eye contact, offer your hand for a handshake and say something along the lines of "Hi, my name is Tracy. Nice to meet you."

Sometimes there will be situations when you want to approach a group of people. This can be more intimidating, but not impossible. First, demonstrate your interest to the group from a distance, paying attention to the speaker. In most cases, the group will notice and make room to include you. Once you're in, try to let the group warm to you before you offer any strong opinions.

In some cases, _not_ talking to someone can make you come off as arrogant or aloof. The author had noticed Bob, the senior vice president of a reputable company, at various corporate events that she had attended with a friend. At the time, she had been too intimidated by Bob's self-confidence to introduce herself. Then, after starting a new job as an engineering salesperson, she called Bob to pitch her employer's products. To her horror, Bob refused her sales offer, accusing her of ignoring him whenever their paths had crossed. That day she learned a valuable lesson: it's always in your favor to introduce yourself.

### 4. Assume the burden of a conversation by learning people’s names and preparing icebreakers. 

Ever found yourself chatting to a stranger who put you completely at ease? Chances are, said stranger didn't achieve this by accident. Most of us hope that others will take on the responsibility to drive conversations forward. But expert communicators know that guiding a conversation evokes the positive feelings that make people want to work or socialize with them.

One easy way of assuming the responsibility for guiding a conversation is to act like you're a host. Just like any host should, focus on learning the name of each person you speak to. After you introduce yourself, ask your conversation partner "What's _your_ name?" Put the emphasis on "your" to make them feel valued.

You can continue hosting the conversation when new arrivals join, introducing the people you have just met by their names. The author once did this at an event, introducing herself to the first three attendees seated at her table. When the remaining guests arrived, she introduced first herself and then the other attendees. In doing so, she established a friendly atmosphere and was perceived as the leader of the group.

Following introductions, it's then up to you to establish conversation topics. You can prepare yourself for this by formulating icebreakers beforehand. Try to come up with something more thoughtful than asking people what they do for a living, as this rarely leads to a long-lasting or in-depth conversation. Instead, you might ask how they got started in the industry if you're at a business event, or about their hobbies if it's a social function.

You could also think of more creative questions, which may lead to a more interesting discussion. The author once watched a TV segment in which a reporter was sent undercover to a party. After introducing herself to a random guest, he asked her what her star sign was, initiating a conversation about astrology. Your icebreakers don't need to be this bold, but one of your goals should be to make your conversation partners feel comfortable by demonstrating your interest in them.

### 5. Asking open-ended questions and digging deeper improves conversations. 

"How was your weekend?" "How have you been?" "How was work today?" The problem with these everyday inquiries is that each of them evokes more or less the same response: "Good. How was yours?" Exchanges like these lack the sincerity that might lead to deeper responses. If you want to be a better conversationalist, you need to dig deeper and break out of the mundanity of everyday inquiries.

The best way to improve your conversations is to ask open-ended questions. Open-ended questions demonstrate to your conversation partners that you genuinely care about what they have to say. They enable you to dig deeper into a speaker's response without being invasive or demanding. Questions like "What did you think about that movie?" allow respondents to determine how much information they're willing to share with you.

Say you greet your child at the end of the day by asking the standard question "How was school?" A typical response to this is a terse "Fine." But rather than allow the conversation to fall flat, you can instead follow up with another open-ended question. You might ask her what class she liked that day, and then ask why she liked it. By framing your conversation with open-ended questions and showing real interest in what your child has to say, you might find that she imparts more vivid information about her interests or her friends. In other words, you can get to know your own child better through small talk.

In the case of a colleague asking you how your weekend was, rather than give a short answer, tell him an anecdote about your weekend before asking him about his. This way, you set an example for the kinds of conversations you want to have, even though you're not the one starting the conversation.

It's up to you to know what is appropriate to ask in a given context. You can treat your open-ended questions like an interview and have questions prepared for situations with strangers. But it's also important to pay attention to how your conversation partner reacts. If you ask your colleague something personal and he changes the topic back to business, it's likely a sign that he doesn't want to engage in small talk at that time. Respect his choice and shift back to his preferred topic of conversation.

### 6. Use varied questions and environmental clues to keep conversations alive. 

When engaging in small talk, there will inevitably be times when a conversation dips into an awkward silence. If you wait for someone else to think of something to say, though, you'll risk letting the conversation die entirely. Instead, when awkward silence strikes, take the reins and get the conversation back to a comfortable flow.

One way to fill a conversational pause is to ask a new open-ended question that changes the direction of the discussion. If you have trouble thinking of open-ended questions on the spot, you can trigger your memory with the acronym _FORM: family, occupation, recreation_ and _miscellaneous_.

While the first three categories are self-explanatory, the miscellaneous category is an opportunity to be more imaginative. You could ask a new acquaintance whether she's enjoyed any books recently. If you're joining a group conversation, you could ask the others how they met.

In some cases, you might draw a blank when thinking of questions using FORM, or find that a question about the person's history might not feel like the right way forward in that particular moment. In these situations, another way to get the conversation going again is to look out for clues by paying attention to your surroundings and the person you're speaking to. What the person is wearing, the venue you're in or details about the event you're attending are all easy topics that you can draw upon when the conversation slows down, or when you're looking to start a new conversation. For example, you can ask guests at a wedding about their connections to the bride or groom. In any situation, it's important to be genuine about what you're saying.

But beware! There are certain topics that you should be careful to avoid. Bringing up gossip, controversial issues or personal misfortunes will in most cases leave your conversation partner with a negative impression of you. What's more, if you're talking to a casual acquaintance, try to avoid asking about specific things related to her job or family members that you may remember from a previous conversation. That's because it's possible that things have changed in her life since you last spoke. If she lost her job or had a death in the family, asking about that job or family member could easily lead to an awkward downturn in the conversation. Instead, ask more general questions, and allow her to bring you up to date on her life on her own terms.

### 7. Paying careful attention to body language and verbal cues will make you a better listener. 

Have you ever told a story and felt like your conversation partner wasn't listening? If so, this most likely left you feeling frustrated and possibly even offended; you might even have chosen not to speak to that person again. For a conversation to go well, both parties need to feel understood, appreciated and, above all, listened to. That means that in addition to taking on the burden of a conversation, you also need to be an active listener.

Even if you naturally listen carefully, you'll want to make sure that your conversation partner _feels_ listened to. One way of indicating that you're genuinely paying attention is through body language. Avoid crossing your arms, hunching your shoulders or fiddling with your clothes, hair or jewelry. Instead, lean forward, nod, smile and maintain eye contact — these are all examples of approachable body language which signal that you are engaged.

Equally important are the verbal cues you give a speaker to show that you're actively listening. The author illustrates this with an anecdote about an eight-year-old named Nicholas. One day after school, Nicholas was telling his father a story about his day. He had painted a picture of mountains in art class, scored a touchdown while playing football and eaten pizza. But Nicholas got upset when he noticed that his father was reading the newspaper. His father stopped reading and assured him that he _was_ listening, and then repeated word for word what his son had shared. But Nicholas wasn't interested in hearing his story repeated back to him: he was looking for a real connection.

There are many ways that you can engage with speakers without interrupting them. You can ask a follow-up question about specific details of a story. You can respond enthusiastically, expressing that you find the story interesting. In some situations, paraphrasing what the speaker has said can be useful to clarify information and prevent misunderstandings.

When appropriate, you can even use your listening skills to make connections between what your conversation partner is saying and other things that you're reminded of. By doing so, you can effortlessly transition the dialogue to a different topic.

### 8. Exiting a conversation gracefully and honestly can enhance the connection you’ve made. 

No matter how well a conversation is going, a poorly executed conclusion can stain the entire interaction. Whether you're charmed or bored by your conversation partner, you should make an honest effort to exit every encounter with grace.

Luckily, there are a few easy ways to conclude any conversation skillfully. The first thing to do is to circle back to the highlight of your discussion. If you've been talking to someone about issues that are affecting the healthcare industry, you could address the person by name and comment on how fantastic it was to discuss this particular topic.

If you genuinely want to continue the discussion at a later date or simply exchange emails, it's up to you to assume the burden of politely requesting a follow-up. Then, once you're finally ready to end the interaction, state what you're going to do next, shake your acquaintance's hand and say goodbye.

It's important that follow through with whatever it is you say you're doing next. If you declare that you're going to see the exhibits at a conference, and your conversation partner then sees you somewhere else having another conversation, he may think that you simply weren't enjoying your time with him. This may hurt his feelings and damage the connection you just made.

Another courteous way to end a conversation is to introduce your conversation partner to a new person who joins your group before excusing yourself. This will help your initial acquaintance widen his own network, and should ensure that he doesn't feel that you're abandoning him.

You can also use your exit to facilitate your own networking agenda. Say, for example, that you're trying to find a job as an engineer. You could ask your acquaintance if they know anyone who might be able to help you with your job search. In the best-case scenario, he may point you in the right direction and make an introduction. And if not, you can simply say that you enjoyed speaking with him, but that you really need to find someone who can help you. People will understand that you need to move on and remember you for your courtesy and charm.

> "_Done properly, an authentic farewell will actually enhance your relationship._ "

### 9. Final summary 

**Final summary**

The key message in these blinks:

**You can benefit from the power of small talk by taking charge of initiating and guiding your conversations. Accommodate your conversation partners by making a warm introduction, asking open-ended questions and actively listening to their responses.**

Actionable advice:

**Use a person's name in conversation.**

A great way to remember a conversation partner's name is to use it in conversation shortly after learning it. What's more, addressing new acquaintances by name is an easy way to make them feel special. Of course, we can't always remember the names of everyone we meet. If you run into an acquaintance whose name you've forgotten, don't be shy about politely asking if they could remind you of it. It will prevent a potentially embarrassing moment down the line.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Reclaiming Conversation_** **, by Sherry Turkle**

As we've just learned, small talk is the foundation for building social and professional relationships. But thanks to our high-tech world, face-to-face communication is becoming increasingly rare. Now more than ever, it's important to look at the benefits of real conversations.

_Reclaiming Conversation_ examines human interaction in our digital age. The ubiquity of mobile devices and computer screens has led to a time of constant interruption, unanswered messages and a general lack of interest. To discover ways to use conversation to cut through the tech and make real connections with people, head over to the blinks for _Reclaiming Conversation._
---

### Debra Fine

Debra Fine is a best-selling author, keynote speaker and communications expert. As a member of the National Speakers Association, she has delivered coaching programs to companies including Google, Credit Suisse and Procter & Gamble for over two decades. Debra has appeared on CNN, _The Today Show_ and NPR's _Morning Edition_, and she contributes regularly to _HuffPost_.

