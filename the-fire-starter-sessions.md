---
id: 53e0de4f3230610007300000
slug: the-fire-starter-sessions-en
published_date: 2014-08-05T00:00:00.000+00:00
author: Danielle LaPorte
title: The Fire Starter Sessions
subtitle: A Soulful and Practical Guide to Creating Success on Your Own Terms
main_color: D34E2B
text_color: B24224
---

# The Fire Starter Sessions

_A Soulful and Practical Guide to Creating Success on Your Own Terms_

**Danielle LaPorte**

_The_ _Fire_ _Starter_ _Sessions_ is a spiritual guide to how you can achieve success on your own terms. It will show you how to become successful — without sacrificing your true self — through practical exercises that will set your full creative potential free.

---
### 1. What’s in it for me? Set your creativity aflame without burning your true self. 

Do you want to make it big, but are afraid of sacrificing your integrity? Do you want to create a business or a creative enterprise, but don't want to lose sight of your true self? Many of us are blocked by these kinds of dilemmas, but the truth is, we are blocking ourselves — because success and our values _are_ compatible.

But all great things come at a price. What you'll need is the self-knowledge to stick to your true self _and_ the marketing skills to make it in the big business world. You'll need to be prepared to dive into the depths of your being, into places you're scared to go — and you'll need to learn how to take control of your life to achieve your peak potential.

In these blinks, you will:

  * discover the tools for success and how you can get results the fastest: by being true to yourself,

  * learn how you can discover your true self, and how to lever your innate potential to your advantage,

  * find practical exercises that will help you discover who you really are and how to set your creativity on fire, and

  * find out why it's sometimes better to ask for _less_ money for your hard work.

### 2. Instead of trying to be good at everything, discover what makes you feel alive. 

For many of us our day to day work is a draining necessity. Mentally, a lot of us are _limping_ through our lives. But don't despair; there are ways to reignite your spark.

Start by discovering what lights you up — because this is usually where your true strengths lie.

Think of Michael Jordan playing basketball: people just couldn't figure out how he could be one of the best players in the world and make it look so easy and fun. Yet, although he was a brilliant sportsman, Jordan failed miserably when he decided to retire from professional basketball and play professional baseball instead.

Why?

Simply because baseball was not his main talent, and he didn't particularly enjoy it. When he returned to basketball, he excelled once again, showing that when you choose the right job, it lights you up.

If you want to discover your true talent, try thinking of all the things that you are good at. For example, what activities do you truly enjoy? When do you feel truly at ease with yourself? Write the answers down — and if you're at a loss for words, try asking your best friend.

You might end up with a long list of things you are good at. But if you want to be successful, you need to start picking your battles, instead of trying to be good at everything.

Let's say you are a pretty good cook, but get really stressed when preparing a meal. You might think that you can make it as a professional cook if you just practiced some more, but in fact, it's actually more likely you're not made for the job. Instead of mobilizing all the energy to get better at something which doesn't flow, try cultivating another of your strengths until you find one that feels right.

### 3. Do what comes easily to you and brings you enthusiasm. 

Most of us learn early on that we have to work our asses off to meet other peoples' expectations. But in fact that is the _Myth_ _of_ _Endurance_ that keeps us from choosing the easy, efficient and self-compassionate life.

Truth is, choosing an easy route in life is more effective.

Creativity coach Dyana Valentine says: "If it doesn't feel easy and juicy, then you're automatically dropping your operation percentage down to about 80 to 70 percent." However, Valentine's reasoning does not mean you should take the "_cheap_ _easy_ " route.

Cheap easy is for example staying in a stifling relationship because it seems easier than facing the heartbreak. Instead, the easy route is about examining your best assets: which topics do you already know a lot about? For example, if you are already doing taxes for all your friends and neighbours — and you actually like doing it — why not try to incorporate your love for numbers into your everyday life?

To learn more about your easy route, take out a piece of paper and write out your To-Do List for the next week. Which of these points are you good at? Which of these fill you with enthusiasm?

Your easy route will also involve _enthusiasm_ and _Bright_ _Faith._

Enthusiasm means that you feel a deep enjoyment when you do what you do. When you're enthusiastic, you're fully present with the task at hand — anything less is not worth doing. This means choosing your projects carefully: "If it isn't a 'Hell yes!,' then it's a no!"

And if you are truly enthusiastic about something, you can experience _Bright_ _Faith._

When you have Bright Faith in an idea, you are certain that you are at the right place at the right time, and that life will come through for you. You'll feel that you're on the right track, and you're not wasting your time on unimportant and time-consuming tasks.

> _"The moment you say yes to acting on your desire is the real beginning."_

### 4. Instead of picking goals, figure out how you want to feel – and don’t be ashamed of it. 

Are you one of those super-productive people who have to-do lists, quarterly objectives and five-year goals? If so, do these lists actually make you happy? More often than not, they don't.

Why?

Because often our goal planning procedures are upside down. Instead of thinking of the results, we should try and figure out how we want to _feel_.

While you are busy picking goals — e.g. chasing the house, the boat and the mate — you might lose sight of what you are really going for: what you hope these things will make you feel. Consider this: what would change if you first decided what you want to feel like, and _then_ wrote your to-do-list?

For example, if you want to feel affluent, then do the things that make you _feel_ affluent: you will naturally attract more experiences that make you feel that way. So ask yourself: what can you do today, right now, to feel the way you want to feel?

Say you want to feel more affluent, you could, for example, make your monthly donation for a good cause a week earlier or treat a friend to a glass of wine.

However, many of us already know how we want to feel, but we think we don't deserve to feel that way. To overcome this inner block, you'll need to stop _judging_ your feelings.

Stop feeling guilty about your wants and needs. Instead of overanalyzing your emotions, listen to what they are telling you! For example, one of Laporte's clients wanted to feel important, but always asked herself: "Isn't it wrong to want to feel that way? Isn't it arrogant?" She was judging her feelings, when instead her focus should be on what would _make_ her feel important. In the end she followed her heart, and stopped taking on clients who made her feel small — and her business took off at the same time as her happiness.

In the following blinks, you'll find out how to get on your way to success.

> _"Being your true self is the most effective formula for success there is."_

### 5. Come up with a “cocktail line” that makes you feel good about yourself. 

What do you say when you get asked the most notorious party question: "So, what do you do?" More importantly, how does your answer make you _feel_?

In fact, a lot of people have cocktail lines that make them feel bad.

Think of all the people you've met who answered with a monotone voice and a bored look when you asked about their work. Having a good answer to this question will not only make you feel good; it'll also help your business: anyone you talk to will notice if you're truly excited about your job, or if you're just in it for the money. So instead of sounding like you're bored with your life, your cocktail line should express what you do in a confident manner.

But what does a good cocktail line sound like?

Answer: be _genuine,_ _but_ _positive._

Actually it doesn't matter what you do: if you run an ice cream truck or if you're a primetime TV producer; when you state what you do with a sparkle in your eyes, you'll seem authentic and at ease with yourself.

Take LaPorte's client for example, who started out by simply introducing herself as "an athletic coach."But she wasn't _just_ an athletic coach! Consider her cocktail line after she had worked on it: "I have an athletic performance company called Power Racing. My coaching team and I design training programs for athletes [...] but we do take on other types of cross athletes [...]. It's part science, part motivation." Can you see, how she manages to convey her enthusiasm? And what's more, she's not exaggerating, but simply stating the facts.

To create your own awesome cocktail line, here are simple questions you can ask yourself:

  * What about your work makes you feel powerful, passionate, free and excited?

  * How does your work make a difference to people's lives?

  * And how would you like to be seen, recognized, acknowledged, awarded and praised?

### 6. Let go of your past failures and visualize the future. 

When you've truly messed something up, it is easy to dwell on what you did wrong. In fact, some of us are still living our worst failures, defeats and break-ups over and over again.

But how does that mindset help you?

The truth is, the mishaps of the past have only one useful feature: what you can learn from them.

When people are stuck in the past, they usually have a hard time envisioning a positive future. Take Jack, for example, who, after being asked to describe his "ideal relationship" went on and on about his ex wife and other failed relationships! That pattern is a sure path to remaining negative — and single.

But you don't have to stay stuck in the past: you can learn from your biggest mistakes by thinking about them and writing them down. Ask yourself what you've truly learned from them. Then vow to purge yourself of your negative ruminations — maybe even burn the sheet of paper you've written on!

Purging the past in this way can also help you open yourself to new dreams. You can do this by hosting a dream funeral. Consider Alexandra's tradition: every New Year's Eve her family wrote down and buried beliefs, feelings, and dreams that they needed to let go, because they weren't useful anymore!

Once you're rid of your old dreams, you're ready to write down a new dream and visualize it.

Start by getting yourself into an excited, thrilled state and write your dream list! Go for outrageous, beyond reason dreams. When you've got your vision, start visualizing your success and the results. Visualize how the world would feel like if you had already accomplished your dream.

### 7. Prioritize your feelings and don’t be afraid of criticism. 

Have you ever had a vision of where you wanted to go, but were too afraid to make it a reality? Don't worry, you're not alone — and this blink will help you overcome your fear.

Try and prioritize your positive feelings by imagining you are a team coach giving your emotions a pep talk before the game: "So, how is everyone feeling?" Enthusiasm will be right there: "I can't wait to get playing." But fear will stand up: "Are you crazy? I will never play again if I lose this game!"

This is your time to step in, so the fear doesn't take over: "All right, you have some good points and we appreciate your point of view. But let's listen to the others: Enthusiasm, you are in front, Confidence will have your back. Fear, you will be playing from the bench."

One major fuel for fear is criticism — but there are ways to handle criticism in a constructive way:

First of all, simply breathe. In order to avoid reacting rashly, it is important that your brain gets enough oxygen. Then admit that criticism _hurts_. When you're honest about it — "Wow, that's hard to hear. But I'm up for it." — you level the playing field between yourself and the critic, and make yourself seem very mature.

Next, don't react immediately. It's better to let it sink in and get back to the critic tomorrow. This way you can avoid saying things you don't mean, and concentrate on the things you want to say the most.

Finally, don't take any nonsense. LaPorte once got a very bad performance review from a manager at a retail job. But she stood her ground and complained to her manager's boss. As it turned out, she wasn't the only one who had problems with the manager. In the end, her complaint led to a promotion — and the manager leaving the company.

> _"It's often best to do the opposite of what your fear is telling you to do."_

### 8. When dealing with people, trust your gut and know when to walk away. 

Other people are a blessing and a burden to us all: while some encounters can be very rewarding, others can be very stressful — especially when people try to play you.

So how can we distinguish the good from the bad?

More often than not, your first impression of a person is correct, which means that when you meet someone new, you should trust your gut.

There's even an old Buddhist saying that sums up this idea for LaPorte: "As in the beginning, so in the middle, so in the end."

For example, LaPorte once hired a consultant who stood her up on the first meeting. Two days later, he called to reschedule and neither apologized nor explained — but LaPorte hired him anyway, because she thought he was the only good consultant in town. However, he kept standing her up in one way or the other, and soon enough, the relationship fell apart. LaPorte knew something was wrong from the beginning, but forgot to listen to her gut.

To deal with people well, we also need to know when to walk away. Sometimes we don't walk away soon enough because we blindly stick to our principles, which can cost us money, time — and most importantly — inner peace.

For example, LaPorte was tangled in a messy contract with some TV producers, which led to a lot of conflict. The disputes escalated, and because of her principles, she didn't want to let it all go, and decided to take legal action. She didn't care about the money — what bothered her was that they were intentionally trying to rip her off.

However, her lawyer kindly reminded her that she had seen clients drag this kind of case on for months, making themselves sick, wrecking their marriages or draining their finances — just for their principles. So she gave LaPorte some great advice: "Get on with your life!"

In the following blinks, you'll learn more about creating real results.

### 9. Revitalize your inner muse by pricking up your ears and sharpening your brand. 

All of us have that voice hidden inside of us: the voice of our _inner_ _muse_ that comes alive when you listen to its call. Next time you hear it whispering, try paying close attention — and watch your creativity flow.

For example, you can try doing what the author Richard Bach does, and give your muse a name: he calls his "the idea fairy." She visits him either when he is gardening or when he is flying in a plane. That's why he learned how to pilot his own plane — and soon he found ideas for his books amongst the clouds. You too should pay attention to when and where your inner muse strikes you the most. You can then recreate the optimal working conditions your muse needs to visit you often.

Another way of helping your inner muse shine is by _sharpening_ _your_ _brand_.

Sharpening your brand means making your key attributes clear, and identifying which of those attributes are most vital to your success. You can then focus on what is important to boost your creativity.

For example, let's imagine you're a carpenter who has recently lost his or her sense of purpose. If you were to examine the driving force behind your craft, you might discover that your purpose is to make people feel comfortable in their homes. Now that you've sharpened the purpose behind your brand, you can use that energy to revitalize your muse and your creativity. And when your muse is shining, you'll probably notice it by the amount of new clients you get!

If the character of your inner muse and brand does not come easy to you, try interviewing a close friend and ask the following questions:

  * What do you think is my greatest strength?

  * What do you feel my purpose is?

  * When have you seen me really shine?

### 10. Find out your time personality to deal with your time management issues. 

Like many others, you might have tried a plethora of time management systems over the years without finding one that fits you right.

This trend is common because, although systems can be useful for running our lives, they can also enslave us.

For example, obsessive time management can be like an overly strict diet that helps us lose unwanted weight, but in the long run doesn't keep the weight off.

Why?

Because at first you might feel the system makes you more productive, but after a while things start falling apart, and you feel even worse than before. Or you want to rebel against your system because you've started to feel that your system is the boss of you — and not the other way around!

There is a way, however, to become better time managers without a system. What we need is to understand our _time_ _personality_ — and learn how to change it.

You might be a _Time_ _Cop_, the kind who always counts the minutes, and is always on time. This tendency makes you very dependable, but on the other hand, it can make you feel stressed all the time.

Then there's the _Time_ _Slacker_, the kind who says: "I'm only five minutes late; what's the problem?" This tendency can make you feel less pressured, but can quickly annoy others because they are always waiting for you.

To get the best of both worlds you need to learn how to change aspects of your time personality. For example, if you are a Time Cop, you can refuse to rush and still be on time: try scheduling periods of free time, which you can use to unwind; or get rid of appointments that aren't really important and focus on those that generate value for you.

### 11. Know why you need money and make sure you feel comfortable with what you’re charging. 

What truly matters to you? Wealth, reputation, happiness? The answers vary wildly from person to person, and drive us down all kinds of paths — but when you don't even know yourself, you can feel like you're running in circles.

To find out what truly matters, you should get an idea of what you actually _need_ _money_ _for_.

If you have a clear idea of what you want from life, you will have a clear purpose for your money. For example, do you want to have a house and car? Do you crave stability? Do you want a long holiday, or do you want to be free? Try to get as clear a purpose as possible by asking yourself the following questions:

  * What do you consider luxurious?

  * What do you love spending money on?

  * What do you wildly resent paying for?

  * When, where, and how are you cheap?

  * When, where, and how are you generous?

Even though money is important to live a good life, sometimes it's better to charge less — if that's what feels right.

Consider Violet, a friend of LaPorte, who got hired at a nonprofit company with a comfortable starting salary. Shortly afterwards, the company hired an older expert for his asking price. Although Violet was his boss, she now made 35 percent less than him — which mean the NGO felt obligated to pay Violet more.

But Violet now felt she was earning _too_ _much_ money: she would rather have earned her raise because of her own performance, and not from something that she didn't do herself. So she asked to go back to her old wage — and she felt a lot better for it.

### 12. Find your tribe and strengthen your relationships by being a giver. 

Do you have a group of friends or co-workers that make you feel totally at ease? Then you have already found your tribe — and you'll know how valuable it is.

If you don't have a tribe already, try to get an idea of which kind of people inspire you. Who do you really want to work with? What do they need to believe in?

For example, LaPorte once worked with a woman who believed that trust had to be earned. She wouldn't trust people unless they'd proven themselves worthy of her trust, and therefore withheld information as a negotiation tool. This personality trait made LaPorte realize that she wanted to work with happy, trusting people instead. So she made a new rule for her friends and teammates: if you want to work with me, you've got to believe that a trusting heart is a strong heart.

But your tribe isn't everything in life: you also need _strong_ _personal_ _relationships_. And one time-tested way of strengthening your relationships is to start _giving_ _more_ _freely_.

Most of us have already experienced how generosity can strengthen our relationships: an unexpected moment of connection with a stranger who did us a kind deed, or the smile we saw on our partner's face when they came home to find a beautiful meal all prepared for them. Try harnessing this power to deepen your relationships. For example, next time you see your partner, bring him or her an unexpected gift. Or buy the next round of drinks next time you're out with your pals — you'll be surprised how grateful they'll be!

### 13. Final summary 

The key message in this book:

**You** **don't** **have** **to** **comply** **with** **other** **people's** **definition** **of** **success.** **Instead,** **stick** **to** **your** **true** **self** **_and_** **learn** **how** **to** **make** **it** **in** **the** **world** **of** **business.** **Define** **yourself** **through** **your** **strengths;** **embrace** **the** **future** **by** **letting** **go** **of** **the** **past;** **and** **learn** **how** **to** **boost** **your** **creativity** **by** **listening** **to** **your** **inner** **muse.**

Actionable advice:

**Your** **gut** **knows** **best.**

The next time you have a bad feeling about a business partner, listen to that feeling. Your gut is trying to tell you something — and more often than not, your gut knows best.

**Listen** **to** **your** **fear** **–** **then** **tell** **it** **you** **don't** **care.**

Next time you are scared of a big project, listen to your fear and what it's telling you. After making sure your fear has been heard, make sure you do not let it stop you. If you are really enthusiastic about something, concentrate on that project. And if you let your enthusiasm guide you, your fear will eventually lose its grip.
---

### Danielle LaPorte

Danielle Laporte is a best-selling Canadian author, motivational speaker, entrepreneur and blogger. She's written several self-help books and was a regular contributor on the show "Connect with Mark Kelley" on Canadian television.

