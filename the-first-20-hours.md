---
id: 5a88cfd7b238e1000705417a
slug: the-first-20-hours-en
published_date: 2018-02-21T00:00:00.000+00:00
author: Josh Kaufman
title: The First 20 Hours
subtitle: How to Learn Anything… Fast
main_color: FFF033
text_color: 666014
---

# The First 20 Hours

_How to Learn Anything… Fast_

**Josh Kaufman**

_The First 20 Hours_ (2013) is about learning new skills quickly. These blinks will help you shake off the notion that it's too late to expand your horizons. Whether you want to master a foreign language or learn how to fly a helicopter, here are ten simple steps for getting started.

---
### 1. What’s in it for me? Learn a new skill in no time. 

Over the course of your life, you've probably had many interests, and perhaps you regret not pursuing some of them. Maybe you wish you'd started studying the violin ten years ago when you had more time, or kick yourself for not mastering a foreign language at a young age when it was easier.

But what if, instead of regretting missed opportunities, you simply started working toward your goals today? This is where the author's technique comes into play.

In these blinks, you'll learn how, in just 20 hours, you can master the basics of your desired skill. From scheduling practice time to acquiring all the tools you need to succeed, you'll learn how to lay a solid foundation for obtaining any skill.

In these blinks, you'll find out

  * what a difference 20 hours can make;

  * why you'll never just find time to practice; and

  * that quantity sometimes is better than quality.

### 2. Learn the basics of anything in 20 hours by using rapid skill acquisition. 

Is there some skill you've always wanted to learn but never had the time for? Many people wish they could play the piano, dominate on the tennis court or converse in fluent French, and may even undertake to learn these skills. But often they give up before they've even acquainted themselves with the basics. The reason is that they believe it's too late for them to acquire a new skill — but that's simply not true!

This is where _rapid skill acquisition_ comes into play. By employing this technique, you can become fairly good at whatever skill you choose by putting in a mere 20 hours of practice.

Obviously, if your aim is to become the next Serena Williams, you'll need many thousands of hours of work. Rapid skill acquisition is more about covering the basics, rather than transforming yourself into an instant expert. But even those first 20 hours of practice on the court will already enable you to play against friends and maybe even partake in local tournaments.

Let's continue with the example of tennis. If you practice for 20 hours, breaking that time up into daily doses of 60-90 minutes, you'll soon find yourself rapidly improving. And once you've completed the initial 20 hours of practice, it'll become considerably easier to continue developing your chosen skill.

If you've ever tried to learn something new, you'll know that the first few hours of practice are often the hardest. At this stage, you'll be more likely to get confused and run into problems, which is what causes people to give up early on. But it's essential to persist for at least 20 hours. That way, you'll have already reached a substantial level of skill, which will make further practice less problematic.

So how can you make those 20 hours really count? Let's dive into the ten principles underlying rapid skill acquisition.

> _"There's no magic to it — just smart, strategic effort invested in something you care about."_

### 3. Focus all your energy on learning your favorite chosen skill. 

There are no doubt many skills you'd like to acquire, but an important part of skill acquisition is to choose the one you care about the most.

So make a list of all the skills you want to work on and pick the one that you feel most excited about at that present moment. This will help keep up your motivation to practice.

The author did just this when faced with the choice of which skill to pursue next. Windsurfing was on his list, but he hadn't been in the sea for years, even though he had previously been a water-sports teacher at a Boy Scout's camp. But since he'd had an innate passion for aquatic activities since childhood, he quickly got excited about windsurfing helping bring him back to the element he missed.

The second principle of rapid skill acquisition claims that you should focus exclusively on one skill. It's very tempting to try and learn several skills simultaneously; for instance, you may want to combine learning windsurfing with studying Spanish and how to play the ukulele.

But this is not a good idea. You need to use your time wisely when learning something new because you likely don't have much more than an hour or two a day. So don't try to learn multiple skills simultaneously. If you do attempt to study multiple things at once, you'll progress very slowly, which will only demotivate you.

### 4. Determine the skill level you want to attain and break it down into smaller parts. 

The third principle of rapid skill acquisition is all about deciding how good you want to become at your chosen endeavor. This is referred to as your _target performance level_, and it's important because if you can imagine what you want your performance to look like, you'll have an easier time getting there.

So ask yourself what level of performance constitutes "good enough" for you. For instance, if you're learning to play the banjo, do you want to play three tunes by heart, or to seamlessly join in a jam session?

When the author started to learn how to play the ukulele, his target performance level was that he wanted to play at a conference his friend was organizing. As he had been asked to give a talk about rapid skill acquisition, he thought a perfect way to illustrate the concept was to demonstrate how much progress he'd made on the ukulele in a mere ten days.

The fourth principle of rapid skill acquisition is to break up your desired skill up into smaller parts that you can tackle individually. You don't try to down a whole meal in one bite, and you shouldn't try to learn a whole skill all at once either. By dividing it up into sub-skills, progress will be easier and faster.

In the author's case, when he began to study the ukulele, he first analyzed the anatomy of the instrument. He then learned how to tune it and only then began to learn the chords for the song he planned to play at the conference.

By the time it was time for his performance, he was able to play the song flawlessly.

### 5. Acquire the tools you need and limit distractions. 

So you've decided on your chosen skill — but what tools do you need to pursue it?

Ensuring you have the tools you need is the fifth principle of rapid skill acquisition. This step is rather straightforward — if you want to learn how to play tennis, you need a racket, and if you want to learn how to operate a helicopter, you need access to one.

Though simple, it is essential to figure out which components, environments and tools you'll need in order to practice and learn. This is what the author did when he started learning how to windsurf. Obviously, he needed a board but also a helmet and a wetsuit. And later, he discovered that before he could start using a sail, he had to get more comfortable simply standing on a board, meaning he needed a paddleboard and a paddle.

Principle number six is about identifying the barriers that might interfere with acquiring your desired skill. Are there any emotional roadblocks, like fear or self-doubt that could hold you back? And what about distractions that might prevent you from practicing, such as a ringing phone?

If there are any hindrances, try to eliminate them. For example, you can create a peaceful environment by shutting out any distractions while identifying and working through your fears.

When the author started windsurfing, he was concerned that it would be dangerous — the risk of drowning and hypothermia had to be considered **.** In order to rid himself of those mental barriers, he familiarized himself with the dangers. He learned about dangerously low water temperatures and bought himself a suitable wetsuit. He also decided to always have someone else present when he was windsurfing — even if the person just sat on the shore. This way, there would always be help available if he was in danger.

### 6. Make time to practice and give yourself feedback. 

Nowadays, people lead busier lives than ever, which is why it's important not just to try to _find_ the time to practice but to _make_ it. This is the seventh principle of rapid skill acquisition.

In order to make time to practice the skill you wish to acquire, identify other activities in your life that you're not particularly fond of, or during which you tend to stall or feel restless, and then work on cutting those out of your day. This will free up time with the goal of dedicating 60-90 minutes every day to working on your desired skill.

The eighth principle of rapid skill acquisition is that you need to ensure you get feedback on your progress.

Say you're trying to learn Chinese. You need a way of getting fast feedback to be able to assess how you're progressing and so you can adjust if you've gone wrong somewhere.

If you can, hire a coach, as they can give you feedback almost instantly. Or, if you're learning a new language, you could also use equipment such as a voice recorder, which will make it easier for you to spot your own pronunciation and grammatical mistakes.

In the case of the author, he received feedback while learning how to play the game Go. He downloaded a piece of software called SmartGo, which provides feedback every time a move is made. This helped him quickly learn how he was doing and made it easier for him to identify his weak spots.

### 7. Practice in short bursts and prioritize quantity and speed. 

Everyone dreads Mondays because they seem never-ending. After the pleasures of the weekend, it's torture to have to endure an entire day of work. Indeed, engaging in any difficult or tedious activity for an extended period of time is both draining and inefficient — and that's why you should only practice in short intervals.

The ninth principle states that practicing in short spurts is the best way to make progress. When you start learning a new skill, the time you spend practicing can feel like it's going very slowly. In fact, at the very beginning, people tend to overestimate how much time they've spent practicing because the task often feels so arduous.

To get around this, use a timer to set 20-minute time frames for yourself. That way, you'll know exactly how long you've spent practicing, and you'll feel more motivated to do your best for that period of time. Aim to do three to five of these 20-minute blocks throughout the day, on a daily basis, and you'll be astounded at how quickly you progress.

The tenth and final principle of rapid skill acquisition is to focus on practicing a lot and quickly, not on attaining perfection.

As a beginner, you're not going to be an expert from the very start, so you should focus on practicing a whole lot, quickly. If you prioritize quantity and speed, you'll find that you're less likely to become irritated and demotivated due to the inevitable setbacks you'll encounter.

The author took this approach when he first started to learn how to windsurf. When he initially got on the board, he was far from perfect — in fact, as soon as he pulled up the sail, he fell into the water. He lost his glasses, nearly gave himself a concussion and swallowed an unpleasantly large gulp of water. If he had expected perfection from the very beginning, he probably would have given up right then and there. Instead, he focused on getting as much practice in as possible before the season was over and as a result, rapidly picked up this new and rewarding hobby that had seemed so daunting before.

By keeping the ten principles of rapid skill acquisition in mind, you can successfully undertake a new skill in no time. Find your equivalent of windsurfing, and embark on your learning journey today!

### 8. Final summary 

The key message in this book:

**It's never too late to learn a new skill. All you need to do is keep the ten principles of rapid skill acquisition in mind. Whether you aim to learn French or play the ukulele, all you have to do is practice for an initial 20 hours, and you'll be well on your way to proficiency.**

Actionable advice:

**Use a logbook to find time to practice.**

When trying to clear your schedule for practice time, it's a good idea to log how you spend your time for a few days. You'll be able to identify patterns and hopefully see slots in your routine that could be filled with practice.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Art of Learning_** **by Josh Waitzkin**

_The Art of Learning_ (2007) offers a crash course in improving your mental performance. In these blinks, the author draws on experiences from his chess career and martial arts practice to present a range of methods and techniques to make your brain work harder, faster and more effectively.
---

### Josh Kaufman

Josh Kaufman is an independent researcher on topics such as business, entrepreneurship, productivity and behavioral psychology. He's been ranked as one of the best business authors by Amazon. His first book, _The Personal MBA: Master the Art of Business_, was a best seller.

