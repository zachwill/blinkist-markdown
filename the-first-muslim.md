---
id: 593553aeb238e100051fda9f
slug: the-first-muslim-en
published_date: 2017-06-09T00:00:00.000+00:00
author: Lesley Hazleton
title: The First Muslim
subtitle: The Story of Muhammad
main_color: 135F53
text_color: 135F53
---

# The First Muslim

_The Story of Muhammad_

**Lesley Hazleton**

_The First Muslim_ (2013) details the incredible story of the prophet Muhammad, the first member of the Islamic faith. These blinks take you back to before Muhammad's birth and tell the complete story of God's revelation to this prophet, how he spread Islam and what his contemporaries thought of his ideas.

---
### 1. What’s in it for me? Humanize an important historical and religious figure. 

Icons of history often remain just that — one-dimensional names or symbols whose humanism is utterly lost on us. But these figures were, of course, individuals, people who often faced seemingly impossible trials and travails. Muhammad was one of these individuals, facing hardship throughout his life both before and after he began receiving God's revelations.

No matter what your religious beliefs are, these blinks give a timely overview of how an outsider orphan became Muhammad, the prophet of Islam, as recounted in the Qu'ran, Islam's central religious text.

In these blinks, you'll learn

  * that Muhammad suffered a mid-life crisis;

  * the importance of patience; and

  * how intertwined religion has always been with politics.

### 2. Muhammad’s story begins with a vow made by his grandfather. 

The story of Muhammad starts well before his birth — and it was unusual from the very beginning.

Muhammad's grandfather was chief of the Hashim clan, a prominent grouping within the Quraysh tribe who, at the time, were a rich community that controlled the city of Mecca. His name was Abd al-Muttalib, and as a young man he found a freshwater spring known as Zamzam, which many people believed to be sacred.

Other members of the tribe, envying Abd al-Muttalib's discovery, challenged his right to the spring. So, to convince them that he was the rightful owner, Abd al-Muttalib swore that if the gods gave him ten healthy sons, he would sacrifice one of them on the banks of the water source.

As fate would have it, Abd al-Muttalib _was_ blessed with ten wonderful sons and was forced to keep his word. To make the decision, he threw ten arrows into the air near the stone of Hubal, a monolith that was sacred to the Quraysh. Each arrow bore the name of one son and the arrow to fall closest to the stone would seal the fate of the son whose name was written upon it. When the decision was made, Abd al-Muttalib's favorite son, and Muhammad's future father, Abdullah, was chosen.

With a heavy heart, Abd al-Muttalib prepared to sacrifice Abdullah. But tribal members stepped in, saying that there could be another way. Eventually, it was decided that Abd al-Muttalib should speak to a priestess, known as a _kahina_, from the city of Medina.

The kahina said that Abdullah's life could be spared if an equivalent offering was sacrificed instead. Abd al-Muttalib was instructed to throw two arrows, one representing his son, the other ten camels. Every time it fell against his son, they'd have to increase the offering by an additional ten camels. Finally, after ten throws, Abd al-Muttalib got lucky. His son would be saved. And so the hundred camels were sacrificed. Afterward, Abdullah married a woman named Amina and the couple conceived Muhammad on the same night.

But fate would take another turn. The following day, Muhammad's father, Abdullah, left on a trade expedition and would later die of unknown causes.

> _"Abdullah was buried in an unmarked grave, leaving his bride a widow, and his only child an orphan in the womb."_

### 3. Muhammad was raised in the desert, but a miracle brought him back to the city of Mecca. 

In many traditions, mysticism and spirituality are deeply connected to nature. So it's only appropriate that Muhammad spent his early years in the vast and empty desert wilderness.

Muhammad was born in the city of Mecca and, although his mother was still alive, society at the time dictated that, without a father, Muhammad was considered an orphan.

In 570 AD, a woman named Halima came to Mecca from a bedouin tribe, a nomadic desert-dwelling people. She had come to offer her services as a wet nurse for wealthy city families and, as was customary, to take children back to the villages where they would be raised.

Although Muhammad's widowed mother had nothing to pay her with, Halima took Muhammad with her as a foster child. In the end, this act of generosity would be a blessing for the bedouin wet nurse. Even though it was a time of drought and Halima was therefore emaciated, her breasts became engorged with milk and her family's cattle produced huge quantities of rich dairy.

Then, at the age of five, after years in the desert, Muhammad returned to the city of Mecca. A miracle precipitated this return. He recounted that two angels, descending from the sky, opened his chest and took out his heart. They washed it and removed a black speck, the mark of the devil, before returning the organ and sewing Muhammad up.

Remarkably, he saw all of this as if he were standing outside of his body. Halima, fearing that Muhammad had been taken by an evil _jinn_, a supernatural creature, brought him back to his mother's side in Mecca.

From there, as a fatherless child, Muhammad lived an unexceptional life. He grew up as an outsider in the patriarchal society of Mecca. But that all changed when he turned 40.

### 4. God revealed himself to Muhammad, naming him his messenger. 

In Western society, turning 40 tends to spark a mid-life crisis, replete with a sports car and a brand new take on life. And Muhammad was no different. Well, except for the part about the sports car.

After his fortieth birthday, Muhammad was visited by God while on Mount Hira, just outside of Mecca. It was the year 610, during the month of _Ramadan_.

Muhammad had climbed Mount Hira to pray and meditate, as was his custom. He was standing in a small cave on the mountainside when the angel Gabriel came to him. Frightened, Muhammad attempted to run, and even contemplated throwing himself off the mountaintop. He thought he had gone crazy.

But the angel stood in his way, telling him that he, Muhammad, was God's messenger and that he must recite the holy word. He said that God had created man from a clot of blood, reflecting the tremendous power and generosity of the Lord. He also told Muhammad that the Lord's word would educate man about the one, true God. After Muhammad repeated what Gabriel had said, the angel vanished.

Muhammad rushed home and sought refuge with his wife, Khadija. As you can imagine, Muhammad was shaken up by what he had seen, but, thanks to Khadija, his fears were assuaged and his faith strengthened.

He gradually told her of what he had experienced and, as he spoke, Khadija saw her terse, quiet husband bloom into a profound orator. The words coming from his mouth, the words of the angel Gabriel, would compose the first words written in the Qur'an, the holy text of Islam. Khadija understood at that moment that their lives would change forever and that her husband was to be the prophet of their people.

### 5. Muhammad’s faith was sorely tested but he met the challenge. 

Many people think of God as an entity that is nothing but kind and soothing. But Muhammad worried that God was both demanding and temperamental. You can't really blame Muhammad for feeling this way since, following the first revelation, his faith was put to an extreme test.

Here's the story:

Muhammad was crippled with doubt about the experience he had had on the mountain. He didn't know what to believe and was desperately awaiting any sign at all that might confirm what had happened.

For two years, he waited. God was silent and Muhammad, an orphan who'd been left by his father, felt abandoned. Those two years were the darkest period in his life. He experienced abject loneliness, depression, suffering and doubt.

Many hundreds of years later, in the sixteenth century, the Christian Saint John of the Cross would refer to such a period of doubt as the "dark night of the soul," a rite of passage that every saint must endure on the path to true enlightenment.

And that's exactly what happened to Muhammad. God, finally convinced of Muhammad's faith as he stood strong despite God's silence, passed on further revelations of profound beauty and meaning.

This second revelation contained the _Sura of the Morning_, which promised a way for Muhammad to teach his people this new faith. From then on, revelations of the Lord continued to be bestowed upon Muhammad. These revelations paid tribute to nature and instructed humanity to care for and preserve the natural world.

For instance, in Sura 91, God instructs Muhammad that any person who strives to maintain Earth's purity will be blessed — but that anyone who corrupts the Earth will meet his end.

These brief, almost haiku-like profundities were vouchsafed to Muhammad at irregular intervals. God was teaching the prophet patience and forcing him to allow each revelation to take its full shape before attempting to repeat it.

Such a lesson in patience could have been geared toward preparing Muhammad for the trials that lay ahead.

### 6. Muhammad’s divine message created division within his clan. 

Muhammad may have been a prophet to his people but he was also something of an introvert. He was reserved, not one to jump at opportunities to speak in public. But since God had asked him to spread the divine word, he didn't have much choice in the matter.

He began by simply reciting his revelations to his wife, Khadija. But this all changed when the angel Gabriel came back to Muhammad with precise instructions on how he was to share these divine ideas with his friends.

Muhammad was to make a meal of bread, mutton and milk to which he would invite members of his Hashim clan, a branch of the Quraysh. When all had dined, he was to tell them of the revelations.

He did as he was told, but when he began the recital portion of his first dinner, his uncle abu-Lahab rose from his seat and left. Such a rude refusal of hospitality was an egregious offense at the time and it completely disrupted the gathering, forcing Muhammad to stop his recitation.

However, he remained steadfast and held another dinner the next day. At this meal, which his uncle did not attend, Muhammad recited the revelations without interruption.

In so doing, he took the first step to spreading the word. But these revelations were not well received on all sides. In fact, they divided his friends and family.

Here's what happened:

Upon finishing his recitation, Muhammad asked those in attendance to help him tell others of this new faith. Just one person, Muhammad's cousin, Ali, the son of the Hashim clan chief, abu-Talib, was prepared to serve Muhammad. Seeing this, Muhammad made Ali his representative, telling all others that they should obey him.

The problem was that such a decree was contrary to tribal rules. Tribal edict stated that a son was to always obey his father, and so the older clan members, along with the majority of the Hashim clan, rejected Muhammad's command. So, at first, only a few younger members converted.

### 7. Muhammad’s message became political, leading to the partial exclusion of his clan from Meccan society. 

You might imagine that getting messages from God would be an enjoyable experience that would give you power in life, but, for Muhammad, the duty of spreading God's word created plenty of difficulties.

For instance, when a Quranic revelation instructed Muhammad to tell people about God's word, he began preaching at the Kaaba, Mecca's most sacred site. This choice of location upset many powerful people in society.

But the real problems began when things got political. Many of the new revelations touched on politics, criticizing corruption and wealth and siding with the poor and oppressed. Because of this, the prophet's message was extremely popular with young people and those who the system had marginalized or ostracized. But it also alienated those who held power in the city.

In the end, Muhammad's revelations resulted in his clan being partially pushed out of Meccan society. It happened when the chiefs of Mecca's two most influential clans, the Makhzum and the Umayyads, began pressuring the chief of Muhammad's clan, abu-Talib, to cut his own nephew out of the clan. While abu-Talib wasn't ready to side with Muhammad, he also didn't want to cast him out.

So the two major clans decided that abu-Talib's entire clan would be excluded. With this edict passed, all other tribal members were barred from doing business with the Hashims or from arranging marriages between members of the Hashim clan and others.

Luckily for the Hashims, not all the clans participated in this boycott. The Hashim tribal members had already been intermarried with people from other clans for hundreds of years, forging deep bonds of family loyalty that trumped mere orders, even those from the highest Meccan leaders.

### 8. Muhammad found the backing he needed in Medina, and his supporters followed him there. 

Muhammad continued preaching in the streets of Mecca, to the displeasure of many; soon, people were hurling garbage at him and cursing his name. It was then that he realized he would not succeed in converting his own people and must seek support elsewhere.

He eventually found this support in Medina. It happened during the traditional _hajj_, or pilgrimage to Mecca, when six pilgrims from Medina found Muhammad preaching in the streets. They were convinced by his ideas and invited him to Medina to be a conflict mediator.

In the following year, 72 clan leaders from Medina swore allegiance to Muhammad and, in 662 AD, the Medina clans formed a military bond with Muhammad's clan, promising to come to each other's aid in the case of enemy aggression. Through this pact, Muhammad and his followers essentially joined the Medinan tribes.

In the summer of 622, Muhammad and his followers moved to Medina, but the process wasn't easy. For a tribal people to move away from their tribal land was major, since these people were strongly linked to their clans and the places in which they were born.

Furthermore, when the Meccans discovered that some of their family members were leaving, they came after them, tracking them down and even forcing some of them to return to Mecca, since such desertion was unheard of.

For example, Umm Salama, who would in the future become Muhammad's fourth wife, was on her way out of Mecca when her family intercepted her, taking her and her child back to Mecca. It wasn't until much later that her family finally let her rejoin Muhammad in Medina.

Clearly, some deep rifts formed between the tribes — rifts that would need healing. In the next blink, you'll learn how that healing occurred.

### 9. Muhammad reunited Medina and Mecca through a pilgrimage and a battle. 

The tribes of Mecca and Medina fought several battles following Muhammad's move. But neither side could definitively best the other. So Muhammad tried a different approach:

He began to reunite the cities through a pilgrimage to Mecca. The goal of this journey was to force the issue of his embattled relationship with the city and, together with a group of unarmed followers, he set out toward Mecca.

When Muhammad and his supporters arrived, the Meccans had no idea how to respond as it was forbidden by their laws to impede the path of a pilgrim. In the end, Mecca and Medina agreed to a truce for the next ten years.

Additionally, Muhammad was allowed to enter Mecca in one year on his next pilgrimage and the tribes were now allowed to choose whether to follow Muhammad or the Quraysh leaders of Mecca. For many people, this pilgrimage marks the decisive shift toward Muhammad's eventual victory in building support for his faith.

But it would take one more short battle for Mecca and Medina to finally be united under Muhammad's rule. Muhammad built more and more support over the year following the pilgrimage and worked steadfastly at the task bestowed upon him by God — to spread Islam as far as he could, even through war.

So when a bedouin tribe, protected by Mecca, found itself in a skirmish with a bedouin tribe protected by Medina, Muhammad took the opportunity to finally settle the issue. He marched on Mecca with his army on January 11, 630, taking the city by storm from all four directions.

The Medinan army encountered no real resistance and Mecca was finally Muhammad's. With the recognition of all the people in the region, he became the leader of a new religion, Islam.

### 10. Final summary 

The key message in this book:

**The story of Muhammad is not a straightforward tale of an ascetic saint. Rather, Muhammad's journey is one of profound humanity. It is the story of a man who was orphaned as a child and exiled from his own people because of his message — a man who, despite these difficulties, stuck to and defended his beliefs and, in the end, founded a new faith.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Muqaddimah_** **by Ibn Khaldūn**

_The Muqaddimah_ (fourteenth century, first English edition 1958), a classic text on the Islamic history of the world, focuses on the rise and fall of civilizations. It offers a unique glimpse into the world of the fourteenth-century Arab Muslim, and is regarded as a foundational text in several academic disciplines.
---

### Lesley Hazleton

Lesley Hazleton is a renowned Middle East specialist. Her other books include _After the Prophet_, which was selected as a finalist for the 2010 PEN nonfiction award.

