---
id: 53d8c7913930340007110100
slug: the-first-word-en
published_date: 2014-07-29T00:00:00.000+00:00
author: Christine Kenneally
title: The First Word
subtitle: The Search for the Origins of Language
main_color: F8A197
text_color: 734A46
---

# The First Word

_The Search for the Origins of Language_

**Christine Kenneally**

_The_ _First_ _Word_ examines the different theories and viewpoints that have been put forward in the past half a century on the complex topic of the origin of language.

---
### 1. What’s in it for me? Find out where the tool that defines your identity comes from. 

Unless you're a hermit or disabled in some way, chances are high you spend every day speaking, hearing, reading, writing and/or thinking in a language. Language is, quite simply, a fundamental part of the human experience. What's more, it's one of the main reasons why humanity has been able to progress and flourish: it enables us to cooperate with one another, resolve disputes and pass on our knowledge to future generations.

But how much do you know about where this magnificent tool came from?

In these blinks, you'll read about various theories that have been put forth on how language has developed and why it seems unique to humans.

You'll also find out:

  * what happens when part of a chicken's brain is transplanted into the head of a quail,

  * what George H. W. Bush and a baboon have in common, and

  * why language isn't unlike a virus.

### 2. Noam Chomsky claimed that human beings inherit the ability to acquire languages. 

In 1959, a relatively unknown academic by the name of Noam Chomsky shocked the world of psychology by radically criticizing a book by psychologist B. F. Skinner.

At the time, Skinner had enjoyed almost God-like status for his theory of _behaviorism_, but Chomsky's criticism of Skinner's views on language-learning effectively "dethroned" him.

What made Chomsky's review so revolutionary?

Well, the key idea in Skinner's behaviorism was the _stimulus-response_ _model_. According to this idea, learning occurs when learners encounter a stimulus and give a response that then gets positive feedback. For example, a child who hears the sentence "The cat is black" and then uses it correctly later may get an enthusiastic applause from her mother.

But Chomsky pointed out that this model doesn't explain language acquisition in children. They not only learn a huge number of words extremely quickly but also the rules for how words can be combined and recombined to create new sentences — all without being explicitly told how this should happen.

For example, a child who knows the sentences "The cat is black" and "The cat is small" will also know that these two sentences can be combined as "The cat is black and small" but not "The cat is black is small."

And that's where the stimulus-response model runs into trouble due to what Chomsky referred to as _poverty_ _of_ _stimulus_. In the example above, the child has never heard the combined sentence or the explicit grammatical rules for how sentences can be combined, so there's not enough stimuli to explain why she can nevertheless produce the correct response.

Chomsky's explanation was that language can be seen as a perfect, inherited system that pre-exists in the human brain. It's the reason why children don't need to learn the structures of language: they have them automatically from this inherited component. What's more, he argued that only humans have this component, or else other species would also develop languages of their own.

### 3. Language is not a uniquely human phenomenon: it can be taught to animals, too. 

As stated in the previous blink, Chomsky thought language was based on a component specific to the human brain. Which means that his theory could be rendered implausible if human language could also be taught to apes and dolphins.

And that has, in fact, been done.

To research the language capabilities of primates, scientists started with images. For example, they gave apes keyboards with images printed on the keys, so that they could communicate once the apes learned the significance of the images.

In this field, the most impressive results were obtained by researcher Sue Savage-Rumbaugh, who taught an ape named Kanzi to be able to produce and even understand some aspects of language.

In the 1970s, Savage-Rumbaugh had spent years training a bonobo named Matata, Kanzi's mother. For a long time, Kanzi merely observed his mother's lessons. But then one day when Savage-Rumbaugh turned her attention to Kanzi, he began to use the picture keyboard proficiently. Based only on what he had seen, he was able to combine multiple symbols to communicate, for example, what he wanted to do next. Later, he also learned to understand spoken English, and was able to understand hundreds of words and longer expressions.

Now doesn't that sound an awful lot like how Chomsky described children in the previous blink?

### 4. Just like the human eye, language is a product of evolution. 

Today, most sensible people understand that our species descended from apes through the process of evolution. And because human language acquisition and the language capabilities of apes share certain similarities, it seems to suggest that language itself is the product of evolution.

This idea was made popular in the scientific community by two psychologists — Paul Bloom and Steven Pinker — and it was actually a far bigger innovation than you might assume. Keep in mind that Chomsky and his followers, such as Stephen Jay Gould, had dominated the field for decades with their view that language was a perfect, preconfigured system in our brains. And for this to be plausible, they had to assume that language appeared out of nowhere because such a perfect system could not have evolved over time as a result of natural selection.

But Pinker and Bloom made the compelling argument that there is no fundamental difference between language and other complex abilities, such as echolocation, which bats use to track their prey, or stereopsis, the mechanism that gives humans depth perception: these are all the result of natural selection.

Pinker and Bloom combined linguistics with evolutionary biology in their argument, which makes sense since language provides a substantial evolutionary advantage to any species by allowing individuals to communicate information to one another.

After Pinker and Bloom's revolutionary argument, people increasingly stopped wondering whether language evolved and started focusing on the question of how it evolved.

### 5. There is no specific, independent language organ in the brain: it’s connected to motor control. 

In the previous blink, we examined how Pinker and Bloom proposed that language capabilities had evolved. But they were not the first to challenge Chomsky's view.

In 1984, more than five years before Pinker and Bloom's suggestion, Philip Lieberman already argued radically against Chomsky's idealized opinion of language. According to Lieberman, the starting point of understanding language should be through evolution.

He developed this position while studying the biological constraints of human language, writing his Ph.D. thesis on how the physiology of breathing affects our speech. He quickly came to the conclusion that if you want to truly understand language, you must start with biology, and biology only makes sense if looked at through the lens of evolution.

In fact, Lieberman's view conflicted even more radically with Chomsky's idea than Pinker and Bloom's did.

Like Chomsky, Pinker and Bloom believed that there was a language-specific structure in the brain. Their ideas only diverged in that they believed this structure had emerged as a result of Darwinian evolution. But Lieberman disputed even this, saying that no such "organ" could have evolved. His research indicated that, in fact, the language system was connected to the motor system.

This finding was based on a study where Lieberman compared patients with Parkinson's disease to neurologically healthy patients. He found that the patients whose brains had degenerated due to the disease not only suffered from motor problems like tremors but also had trouble understanding and producing syntactically correct language.

He therefore deduced that the language and motor functions were connected, meaning there could be no specific, independent language center in the brain.

### 6. What constitutes human language? Talking about a subject and the presence of meaningful words. 

Before we dive into examining how language evolved, let's use the next blinks to try to understand what human language actually is and how it's different from animal language.

First of all, human language requires the ability to talk about something.

But it seems this isn't a uniquely human trait. Just to name one example, there's a thirty-year-old parrot named Alex who can name fifty different objects, seven colors and five shapes. He can also answer questions when asked, i. e., he's able to talk about something.

A second trait of human language is that it requires words.

Though a lot of animals seem like they might have words, too. African vervet monkeys make different alarm calls depending on whether they see an eagle, leopard or a snake. Chickens and several other animals also have similarly meaningful sounds.

However, these alarm calls can't be plausibly construed as some primitive form of language because they're genetically anchored to each species. In human language, we see how new terms, such as "computer" and "internet," emerge and spread rapidly, which wouldn't be possible through mere evolution. Obviously, then, there's a fundamental gap between human and animal language.

Moreover, the alarm calls we see in animals aren't really words but sounds that have a certain reference. Words, on the other hand, contain information about sound, grammar and meaning. For example, when you hear the word "dog," you know that it means a four-legged animal, but also that it's a noun and can be used as the subject of a sentence.

### 7. What constitutes human language? Animals also use gestures and speech. 

Do you know what a "muzzle wipe" is? It's when baboons quickly swipe across the bridge of their nose with their hand, a gesture they usually use in situations where they feel conflicted or nervous about something.

Such gestures are a key component in human language, too. And, believe it or not, the muzzle wipe isn't unique to baboons.

One time, at a press conference, former US president George H. W. Bush was speaking about his son, then US president George W. Bush, and how he'd been arrested for drunk-driving in his youth. When defending his son, Bush senior raised his hand to the bridge of his nose and scratched it: the same muzzle wipe baboons use.

In fact, gestures like pointing are usually the very first sign of language that human infants learn, occurring around ten months of age. Later on, words and gestures will appear together to form sentences: a child might point at a banana and say the word "eat," and speech eventually emerges as the primary form of communication.

Which leads us to one of the most important aspects of human language: _speech_.

Infants begin babbling at around five months, thereby producing their first speech-like sounds. Surprisingly, this isn't unique to humans: dolphin infants also seem to go through a babbling phase. The purpose for both seems to be to learn the sounds native to their species before going on to structuring those sounds more meaningfully.

### 8. What constitutes human language? Structure is key, but it doesn’t appear to be uniquely human. 

As anyone who has ever studied the grammar of a foreign language will tell you, _structure_ is a major component of human language.

Linguists have identified two types of structure in language:

The first has to do with _morphemes_, which are studied in the linguistic discipline of phonology. Morphemes are the smallest meaningful units of a language that can't be broken down into smaller parts.

For example, "lady" is a morpheme, but the individual sounds "la-" and "-dy" are meaningless.

The second type of structure is called _syntax_, which has to do with how words and morphemes are combined into phrases, for example: "She's a lady."

Interestingly, neither of these structures is unique to humans.

Certain elements of phonology can be seen in the way birds and whales sing: they repeat "phrases."

What's more, studies with vervet monkeys show they use a distinctive fall in pitch to signify that they're at the end of an utterance, which others interpret to mean that it's their turn to "talk." This is strikingly similar to humans.

The even bigger question concerns whether syntax is exclusively human. Could it poke a hole in the idea that language has evolved?

Some claim that animals have no syntax in their language, but this is clearly wrong: several primates have been observed using syntax.

Others maintain that, while animals may have their own syntax, only humans are capable of understanding and using the complicated nature of human syntax. However, the example of Kanzi has shown that this isn't true.

So it seems that neither morphemes nor syntax are unique to humans.

This begs the question: What makes human language human?

### 9. Human language does have a biological basis: the human brain. 

For a long time, linguists assumed that language abilities could be attributed to distinct areas of the brain. But this has been disproven by experiences with patients who have suffered brain damage: it turns out that if an area of the brain is damaged, language capacity can move to another area. If language capacity really were restricted to one area, damage to it would be catastrophic.

However, experiments with animals have suggested that some language-related behaviors seem to be completely reliant on how the brain is constructed. For example, in one study, scientists implanted a piece of a Japanese quail's brain into the brain of a domestic chicken and vice versa.

The result?

The birds continued to produce the calls of their own species, but started responding to the maternal calls of the other.

So it seems that the basis for language is at least in part innate and biological, and this applies to humans, too. It explains why babies have a natural instinct to learn the language of their parents.

Still, it would be an oversimplification to suggest that there is some "language gene" that produces this instinct.

### 10. Language is like a virus that has evolved in symbiosis with humans. 

In the sixteenth century, Polish astronomer Nicolaus Copernicus put forth a rather revolutionary model of the universe: What if the sun, not the earth, was at its center?

This idea is an example of the kind of remarkable and profound shifts in perspective that have occurred in the history of science and propelled our progress.

Perhaps it is time for a similar shift in language evolution: instead of thinking of language as survival tool for humanity, we could consider the possibility that humanity is a survival tool for language. In other words, we could ask ourselves: What if language is like a virus that survives by "infecting" people — children, in particular?

This idea has been put forth by linguist Simon Kirby, whose research is founded on the idea that evolution has taken two separate forms: biological and linguistic. As life evolved from bacteria to humans, language evolved along its own path.

To examine this theory, Kirby has simulated the evolution of language through computer models. Basically, he creates populations of individuals and then studies how language would spread among them. One of his more surprising findings was that structure in language evolves when an individual doesn't have access to the whole wealth of language at once, e.g., all the words. This means he or she has to come up with rules to combine words and so structure, or grammar, emerges.

Language that evolves in this way could be considered a co-evolving virus or even parasite that resides in the human brain. But we modern humans need this language parasite to thrive and reproduce just as much as language needs humans to spread.

### 11. We can understand human language by examining how robots are developing theirs. 

As you've seen in the course of the previous blinks, the academic world is quite divided when it comes to theories about the origins of language. Unfortunately, we can't know who, if anyone, is right.

But wouldn't it be enlightening if we could observe a language developing in real time? That's what Luc Steels, a Belgian scientist, tried to achieve by building robots that develop a language of their own.

Steels conducted his study by placing remote-controlled robots that could see, hear and produce sounds in laboratories around the world, then allowing real human agents to "teleport" into the individual bodies of the robots. Once agents were inside a robot, they could see and hear what the robot could see and hear, and could communicate with any other robot in the same physical space.

Steel allowed the agents to explore an object in the lab, e.g., a painting. Agents could then produce arbitrary "words" for what they saw and the others tried to understand what the words meant. No predetermined word list was given.

Over time, the robots began to develop their own language as the agents began to agree upon what each word meant. The language then spread and became more uniform as agents jumped into the bodies of robot bodies in other laboratories.

Steel also made an interesting discovery in the process. In order to enable the language to develop, he had to enable the robots to point at things. When they couldn't point, the two agents couldn't verify that they were indeed talking about the same object. Thus, it turned out that pointing was crucial to forming word-object relations.

Of course, the primitive language developed in this way is still a far cry from what we humans use, but it is a meaningful step on the continuing journey to understanding language.

### 12. Final summary 

The key message in this book:

**One** **of** **most** **fascinating** **questions** **science** **can** **ask** **is:** **Where** **does** **language** **come** **from?** **While** **human** **language** **is** **only** **used** **by** **humans,** **elements** **of** **language** **and** **the** **abilities** **required** **to** **use** **it** **can** **be** **seen** **in** **animals,** **too.**

Actionable advice:

**Read** **Robert** **Wright's** **_Nonzero_** **in** **blinks**

Did you enjoy these blinks? If so, we recommend you check out the key insights from Robert Wright's book _Nonzero_, also available in blinks. It examines the evolutionary and cultural history of humanity, concluding that we are purposefully pushing towards goodness. Even though it is not directly related to language, the blinks can help you better understand evolutionary research in general.
---

### Christine Kenneally

Christine Kenneally has a Ph.D. in linguistics from the University of Cambridge, and she has written for publications such as _Scientific_ _American,_ _The_ _New_ _Yorker_ and _The_ _New_ _York_ _Times_.

