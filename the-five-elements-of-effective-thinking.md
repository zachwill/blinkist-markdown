---
id: 540477793663330008160000
slug: the-five-elements-of-effective-thinking-en
published_date: 2014-09-02T00:00:00.000+00:00
author: Edward B. Burger and Michael Starbird
title: The Five Elements of Effective Thinking
subtitle: None
main_color: 30BBF1
text_color: 1F7799
---

# The Five Elements of Effective Thinking

_None_

**Edward B. Burger and Michael Starbird**

With _The_ _Five_ _Elements_ _of_ _Effective_ _Thinking,_ you'll learn how to think effectively and realize your full potential. Using as an organizing principle the four elements — earth, fire, air and water — the authors explain many techniques for improving the way in which we think. With the addition of a fifth element, change, they demonstrate how adopting the right attitude helps to bring about lasting, positive change.

---
### 1. What’s in it for me? Become a bold, inquisitive, outside-the-box thinker. 

After reading these blinks, you'll

  * **Lose your fear of failure.** You'll be able to take on challenging endeavors fearlessly. In fact, you'll embrace failure as a way of learning new things that you can then apply elsewhere too.

  * **Become a more inquisitive and critical thinker.** You'll ignite your inner curiosity and learn to ask more questions - even ones that you already know the answer to - and this will deepen your understanding of the subject.

  * **Be able to find outside-the-box solutions to problems.** You'll learn how to look beyond the obvious, generally-accepted solutions and find completely new approaches to problems.

### 2. Earth: True mastery isn’t about doing difficult things, but about mastering the basics. 

What's the best way to develop a true understanding of something? Should you learn as much as possible about a subject, for instance?

Actually, no. The key to developing a true understanding of an issue is to master the _basics_. The basics make up the foundation of any skill or talent, the core of any expertise — just like the element _Earth_ represents the solid ground underneath our feet.

Often a person who wants to become an expert — such as a student cramming for an important exam — will attempt to master as many complex theories or facts as she possibly can at once.

This isn't the best strategy, however. _True_ experts are instead concerned with continually and constantly perfecting the basics.

Virtuoso trumpet player Tony Plog once gave a masterclass for accomplished soloists, in which he requested that they play their most challenging, virtuosic piece. As you'd expect, they all played incredibly well.

In response, rather than offering advice and tips about how the performances could be improved, Plog asked the soloists to then perform a simple beginner's exercise.

While they played the exercise well, none played impressively. Once they were finished, Plog himself performed the exercise, astonishing the group as to how virtuosic this "basic" piece was played.

What happened? Plog knew that mastery requires constant attention to and understanding of the basics, as it's the basics that provide the foundation on which we can improve.

So when you're faced with a challenging task, don't tackle it headlong immediately. First, consider the basic elements of the task, and through this, you can attack each simpler element successfully.

Consider how the U.S. National Aeronautics and Space Administration (NASA) met the challenge of landing a man on the moon in the 1960s. The agency didn't accomplish this goal by immediately shooting people into space; rather, they started with sending an _unmanned_ rocket to the moon first.

Only once NASA had accomplished this basic step did they pursue and succeed in their goal of sending a man to the moon.

### 3. Earth: To find a problem’s true essence, look only at what you can see, and identify what is missing. 

For years, people looked at birds, bats and insects and concluded, "All these animals flap their wings; flapping must be the secret to flight!"

If you've flown in an airplane, you know this conclusion is clearly wrong. But when trying to understand a phenomenon, we often reach for what seems the most obvious answer.

To get to the bottom of a problem, we need to uncover its _essence_, which is often hidden behind other, irrelevant details. Again, we are reminded of the element _Earth_, as a problem's essence is its core or foundation.

Let's think about flying. Understandably, the mechanics of flapping wings distracted observers from the real reason behind how a body can fly. It was only when people studied the mechanisms of flight more closely that they were able to better understand what enabled flight: the particular curvature of a wing.

Thus to find the essence of an issue or problem, we need to look at only _what_ _we_ _can_ _see_. This means actively ignoring what we might expect to see, or have been taught to see.

Often our expectations get in the way of discovering the essence of an issue or problem, as we don't question enough what others have told us that we _should_ be observing.

For centuries, people believed Aristotle's theory that heavier objects fall faster than lighter objects. It wasn't until much later, during the seventeenth century, when curious individuals actively ignored Aristotle's claim and examined the situation with fresh eyes — and discovered that Aristotle was wrong.

There's also another way to find the essence of problem: to focus on what's _missing_.

Today when we look at old photographs, we describe them as "black and white," as those are the only colors present. Yet before the invention of color film, such monochrome photographs were simply known as photographs.

At the time, people didn't think such photos as being limited in any way. But had they added the adjective "black and white" to "photography"_before_ colored film was invented, people would have had a better understanding of the _essence_ of the photographs in front of them.

> _"Just by describing what is there, he was led to see the invisible."_

### 4. Fire: You have to fail before you can progress and truly succeed. 

We all have bad days. Yet what distinguishes great thinkers from the average is not the absence of bad days, it's the way a great thinker reacts to them when they occur.

This is a crucial lesson: whenever you make a mistake, make sure you take time to reflect on it.

Your accomplishments are built on the lessons you've learned, and mistakes are the basis of your most effective lessons. Sure, we don't like making mistakes, just like we don't enjoy being burned. The element _Fire_ embodies this uncomfortable truth; we must embrace our mistakes to learn better.

So when you make a mistake, understand what you did wrong and then question _why_ it was wrong. With each mistake you identify and investigate, you'll have better insight into how to tackle future problems.

Thomas Edison was someone who understood the fire element, believing that invention was "1% inspiration and 99% perspiration." His approach was to experiment, see what went wrong, learn from the mistake, then try again.

After he successfully invented the lightbulb, Edison was asked about his failed attempts. His response?

"I have not failed, I have just found 10,000 ways that won't work."

Indeed, an ineffective solution to one problem could be the perfect solution to another. In 1970, for example, a scientist working for 3M Laboratories tried to create a strong adhesive. What he came up with instead was a failure: an adhesive so weak it could be peeled off any surface, leaving no trace. The project was subsequently abandoned.

Three years later, another scientist at 3M was seeking to create a bookmark that wouldn't slip out between pages easily or damage them.

The solution? That same "failed" weak adhesive. The scientist's invention went on to become one of 3M's most successful and ubiquitous products: the Post-it note.

So before you consider an idea useless or a failure, take a moment to consider whether your idea might be used in a way that you haven't yet imagined.

### 5. Fire: Embrace an attitude of being willing to intentionally fail. 

Many people are deathly afraid of failure. They believe that in failing, they won't ever succeed — a perspective that stops many people from ever trying new things.

To be innovative, you cannot be afraid to make mistakes. In fact, because failing can provide you with such useful information, it shouldn't be avoided but instead welcomed. The element _Fire_ reminds us of this; we have to make the uncomfortable comfortable to truly succeed.

Although thinking like this is fairly unconventional, there are several ways to encourage it in yourself.

First, commit to the idea that you'll fail at least nine times before getting something right.

Whether you're producing a new gadget or artistic work, don't believe that you'll succeed on your very first attempt. By thinking in this way, you'll feel free to simply go ahead and try new ideas without fear.

So when your first attempt inevitably turns out to be a failure, you can say, "Well, that just means I'm 10 percent closer to succeeding! Let's try again."

Second, attack your problem in a way that excludes the thought that your idea is somehow "correct."

When you write down your ideas without caring whether they're right or wrong, your thoughts will tend to drift more easily, and they'll be clearer too. Leave the task of distinguishing right from wrong, or good from bad, for later.

Finally, exaggerate potential problems on purpose to "force" mistakes, and then fix those mistakes.

Taking your ideas to the extreme can reveal inherent flaws. This is an approach that manufacturers often take: they perform "stress tests" designed to push a product to its breaking point. In doing so, they gain valuable information about the product's strengths and weaknesses.

Similarly, certain companies hire skilled hackers to attempt to break into their computer systems, so they can locate even the most difficult-to-find security holes.

So the next time you're facing a challenging task, don't allow yourself to be paralyzed with fear. Fail! And then try again.

> _"A person who is willing to fail is someone who is willing to step outside of the box."_

### 6. Air: Force yourself to ask questions, even when you might already know the answer. 

Children ask a _lot_ of questions. Yet as adults, we're often distracted by our own problems and thus annoyed by this overeager inquisitiveness — especially the persistent, "why?"

This response is wrongheaded. Indeed, constantly questioning is crucial to developing the most effective mind-set.

But what steps should you take to achieve this ideal frame of mind? Think about the element _Air_ — asking questions acts like a breath of fresh air, clearing the way to deeper knowledge.

First, put yourself in the position of a teacher. When you have adequately grasped a specific subject or solution to a problem, consider how you would then teach the subject to someone else.

For instance, you could prepare a lecture — perhaps for a colleague or friend — and, as you assemble your thoughts, try to make the subject as clear as possible.

In this process, you may find yourself considering questions you might not have had before. Moreover, having to explain a subject in detail to another person can help you to identify gaps in your own knowledge.

Once you've created your lecture, evaluate your work by making up an exam on your subject. Which questions would you ask? And more importantly, would you be able to then answer those questions accurately, based on your lecture's contents?

Second, embrace an attitude of being constantly inquisitive and critical.

When you are curious about a subject, don't bog yourself down with trying to absorb every fact you can at once. Instead, _ask_ _questions_. Regularly ask yourself, "what if…?" and find an answer that satisfies your curiosity.

This approach works. The book's authors, as they gave lectures, would randomly select one student in the hall to be the "questioner." The questioner then had to ask at least two questions during the lecture.

Over the years, the authors noticed that the questioner usually would gain and retain a better understanding of a subject — because the questioner had to be inquisitive — than any other student present at the lecture.

### 7. Air: To find the right solutions, ask the right questions. 

Two men are walking in the forest when suddenly an angry bear starts to chase them. Both break into a sprint. While they're running, one asks the other if he thinks they can outrun the bear and survive. The other says, "I don't need to outrun the bear. The question is: Can I outrun _you_?"

Coming up with good questions is an invaluable skill. But what exactly makes a good question?

Questions should be effective; they should engage your mind in ways that lead to new insight and solutions.

An ineffective question is one that might be obvious or vaguely formulated, one that doesn't lead to action. A question such as, "How can I get better grades?" is too general to generate a response that would actually lead you down the path to better grades.

Like the element _Air_, an effective question should provide clarity and focus. "How can I manage my time better?" or "How can I understand this subject more deeply?" are questions that get to the heart of the matter, where "How can I get better grades?" does not.

Yet sometimes, asking the right questions means _questioning_ _your_ _own_ _questions_.

Think of the last time you were stuck in traffic. You were probably stressed, and perhaps came up with a half-dozen ways to fix the "traffic problem" in your city — by adding more lanes, as one example.

The reason your solutions aren't helpful? You're asking the wrong question.

In this situation, the _right_ question begins with accepting the traffic you're stuck in. So, by questioning your original question, a better question can then be formulated. For example, "Given the likelihood that I'm stuck here, is there a way for me to use this time effectively?"

The final type of question is more philosophical in scope: "What's my reason for even doing this task in the first place?" A very good question. Before you begin a project, question why you're interested in doing it, and determine what it is that you hope to gain from it.

### 8. Water: Ideas don’t flow from a void, but are generated from previous ideas. 

In cartoons, a character has an idea and a light bulb switches on over their head. Although this is a popular and visual metaphor, it's far from accurate.

That's because every idea has its own history. "New" ideas do not come from nowhere; rather, they are simply variations on existing ideas. Like the element _Water_, ideas flow from the past to the present.

In the seventeenth century, for example, Isaac Newton and Gottfried von Leibniz independently formulated a branch of mathematics that would change the world: calculus.

Yet, a closer examination of history reveals that all of the necessary elements to come up with calculus had already been conceived by other mathematicians.

Even Newton himself admitted that he was able to see further than others only because he'd "stood on the shoulders of giants." In short, Newton and Leibniz took just a small, albeit crucial step forward based on the ideas of the past.

And after them, even more ideas marched on. While Leibniz's published theory was just six pages long, current theoretical writings on calculus weigh in at 1,300 pages. In other words, after the two men came up with their theory, generations of mathematicians added their own small contributions.

So what does this teach us about developing our own ideas?

Whenever you're trying to develop an idea, you should look to the past and explore the rich history of ideas. How did past thinkers develop _their_ ideas? What do you think they were thinking at the time?

In doing this, you'll discover that great ideas seldom arrive like that cartoon light bulb but make themselves known after a long process of trial and error. Once you accept this, you'll let your thoughts flow like water, and you'll reach your goals sooner than you know.

> "I begin with an idea, then it becomes something else." — Pablo Picasso

### 9. Water: New ideas and solutions should be viewed as a beginning, not as an end. 

The American mathematician R. H. Bing once said that the time to work on a problem is after you have solved it. In other words, innovation should never come to a halt.

As we saw in the previous blink, every new idea is the source of another new idea. Thus you shouldn't stop thinking, just because you've reached one solution to a particular problem.

Indeed, the most effective thinkers are those who continually exploit new ideas for further inspiration.

The invention of the light bulb was just one solution for bringing artificial light to dark rooms. Yet this one idea sparked dozens of others: electric heaters, television, even computers.

By thinking of the light bulb not as a final product but as a point of departure for new ideas, one small drip of inspiration turned into a flood of new ideas and discoveries.

The element _Water_ can be an inspiration to let your ideas flow. Never allow yourself to dam up this creative process! Instead, maintain your flow of ideas by seeing each new solution as just a waypoint to other destinations down the river, which itself flows endlessly forward.

The question is not whether an idea will lead to further ideas, as all ideas do. The real question is how to find those new ideas. So keep asking yourself, "What's next?" and then take that first step toward tackling your next challenge.

### 10. To implement the four elements of thinking in your life, you must be willing to change. 

What's the definition of insanity? Someone once said it's doing the same thing again and again, and expecting different results each time.

So, if you're not happy with how your life is progressing, _change_ _it_. Earlier blinks have shown you what you can do to make a positive change. Now it's time for you to actually do it!

The first thing you must do is be willing to change. People often think of change as being too difficult, or too complicated. They think it's better to continue along the same path, saying, "Surely if I keep at it, things will improve."

But this isn't true. The only way to improve your life is to change it. If that sounds daunting, don't worry. Now that you're armed with the elements of effective thinking, you have the tools to change.

All you need to bring to the table however is a _willingness_ to change. Sure, it's not easy to go out into the world and make mistakes on purpose; but you must accept that this is the only way to generate better ideas.

So, don't concern yourself with the risks involved — just do it!

However, it's not enough to do this just one time. To transform yourself, you must be prepared to change and evolve _constantly._ There is always room for improvement.

This process is somewhat like renovating a city. While people usually tend to think of an ideal city as complete and perfect, by thinking effectively you will see that an entire city cannot be renovated all at once, but in incremental steps.

Fixing one area takes time. And once that area is ready, the next area will also take time. This process will continue until the moment you believe you're finished — at which point, you should begin working on the first part again!

The five elements can help you on your path: the _Earth_ centering your ideas, _Fire_ inspiring your confidence, _Air_ clearing your thoughts, _Water_ helping your ideas flow from one inspiration to the next, and the final element — _Change_ — leading you to success.

> _"In a leaking boat, energy expended to changing vessels is more productive than energy devoted to patching leaks."_

### 11. Final summary 

The key message in this book:

**Extraordinary** **people** **are** **just** **ordinary** **people** **that** **think** **differently.** **By** **using** **the** **proven** **methods** **of** **effective** **thinking,** **that** **ordinary** **person** **that** **thinks** **differently** **could** **be** **you.**

Actionable advice:

**Fail** **on** **purpose** **and** **learn** **from** **your** **mistakes.**

Next time you're facing a problem and you don't know what to do, rather than allowing yourself to be paralyzed with fear, try an approach you know will fail, and then learn from your mistake.

**Locate** **your** **weaknesses** **to** **test** **your** **mastery.**

To see exactly how well you've mastered a subject's basics, take a blank sheet of paper and write down everything you know about it. Later, compare what you've written with other, more authoritative sources. In doing so, you'll discover the weaknesses you have in your understanding of the basics.

**Suggested** **further** **reading:** **_Six_** **_Thinking_** **_Hats_** **by** **Edward** **de** **Bono**

_Six_ _Thinking_ _Hats_ offers you valuable tools for group discussions and individual decision making. The book shows ways to compartmentalize different ways of thinking to help you and your group use your brains in a more detailed, cohesive and effective way.
---

### Edward B. Burger and Michael Starbird

Edward B. Burger is an educational and business consultant and president of Southwestern University in Texas. His teaching and scholarly works have earned him many honors in the United States as well as the biggest teaching award in the English-speaking world.

Michael Starbird is a distinguished teaching professor at the University of Texas at Austin and a business and educational consultant. He has been awarded with the highest American teaching award in his field, and his many books, lectures and workshops have reached large national audiences.

