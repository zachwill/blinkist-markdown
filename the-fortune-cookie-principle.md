---
id: 58eb977fa54bbb000464bb08
slug: the-fortune-cookie-principle-en
published_date: 2017-04-11T00:00:00.000+00:00
author: Bernadette Jiwa
title: The Fortune Cookie Principle
subtitle: The 20 Keys to a Great Brand Story and Why Your Business Needs One
main_color: A92C3B
text_color: A92C3B
---

# The Fortune Cookie Principle

_The 20 Keys to a Great Brand Story and Why Your Business Needs One_

**Bernadette Jiwa**

_The Fortune Cookie Principle_ (2013) is a practical guide to building a successful brand through powerful storytelling, a compelling vision and a clear purpose. These blinks explain how to tie your product to the meaning that potential customers seek.

---
### 1. What’s in it for me? Create an alluring story about your company. 

Every year, hundreds of companies enter the market with amazing products. Many fail — and only a handful make it to the very top. So why do these happy few stand out from the competition? Did they get lucky? Did they start out with a whopping marketing budget? Neither.

These companies succeeded because they had a great story to tell, a story which resonated with potential customers. The good news is that you can create such a story for your own company, step by step. These blinks focus on the most important of the 20 keys to building an amazing brand story.

In these blinks, you'll also find out

  * What connects Apple to a fortune cookie;

  * why the Lego brand was harmed by creating cool things like the Legoland parks; and

  * why the outdoor company Patagonia told its clients not to buy its gear.

### 2. Think of your brand as a fortune cookie. 

Imagine you've invented a totally awesome product like a convenient online subscription service for razors. How can you get the attention of potential customers and sustain your fledgling business?

Well, marketing is no longer a simple matter of selling bigger and better commodities. You can bet that there will always be someone waiting to compete with you. This means that if you just aim to sell a product, like razors, your business is likely to fail. You need a whole lot more than a glitzy product to succeed.

After all, it's extremely likely that someone else will come out with an even cooler razor the next day. If your business is entirely based on having the coolest product, you'll be overtaken just like that.

That's why you need something to set yourself apart from the pack, which means creating a brand based on a story. You can think of such a narrative as a _fortune cookie_ that contains two parts: the cookie and the fortune.

In this metaphor, the cookie is your product or service. It's a tangible thing with a constant value.

The fortune, on the other hand, is something intangible. It's a story that captures your values, purpose and vision while touching customers in a way that makes them adopt your story as a part of themselves. Simply put, the fortune is a narrative that fosters a connection between your customers and your product.

Just take Apple, a successful fortune cookie company. Apple doesn't only give its customers a device that lets them listen to music. It puts thousands of hits right in their pockets, just a click away.

But how can you make your company a fortune cookie business? By adhering to the key points necessary to forge a brand story that'll create a bond between your customers and your products. Together, these points form the _Fortune Cookie Principle_, and you'll learn all about them in the following blinks.

> _"Marketers spend most of their time selling the cookie, when what they should be doing is finding a way to create a better fortune."_

### 3. Define a clear purpose and stick to it. 

Making loads of money is a pleasant side effect of running a successful business, but money alone is rarely the reason for a company's existence. So, ask yourself, why are you in business?

For your company to thrive, you'll need to define a clear _purpose_ that's central to all your operations. A purpose will guide you in everything from decisions about product design and hiring to the details of your overall business model. If you fail to set and follow a purpose, you'll lose sight of why your company exists and start making poor decisions.

For instance, Jim Stengel, executive of the consumer goods company Procter and Gamble, along with the market researchers Millward Brown, identified 50 businesses that had experienced tremendous growth over the past decade. These companies, including Google and Jimmy's Iced Coffee, had grown on average three times faster than their competition.

How come?

Because each has a great mission. Just take Google. They aren't simply offering a search engine; they're satisfying people's curiosity. In the case of Jimmy's Iced Coffee, the company isn't just selling java; they're offering instant joy.

But what about a company like Lego? This brand used to thrive by selling construction-themed toys but eventually went south. The company found itself in a budget deficit that had grown to $300 million by 2003.

Then, in 2004, a new CEO took over and managed to turn the ship around. His name is Jørgen Vig Knudstorp and, within a year, he transformed this colossal deficit into a $110 million profit. He made this incredible change by reminding people of the brand's purpose: to inspire creativity in children across the world.

Lego had lost sight of this purpose, and this caused the downturn. The company had been overly focused on diversifying into projects like a Lego theme park and computer games, which caused them to neglect their supply chain, leading to poor customer service and irregular availability of their products.

So, asking why your business exists is crucial. But it's not enough. You also need to see where you're going. To put it differently, carve out a vision for what your business _could_ be, which is exactly what you'll learn about next.

### 4. Define a clear vision but be open to growth and change. 

Have you heard of the Californian non-profit, _Room to Read_? This organization helps communities in the developing world construct schools and libraries. Their purpose is guided by a compelling _vision_ of helping build a world in which all children have access to a decent education that enables them to reach their full potential and contribute to their communities.

The purpose of any good company is backed up by a bigger vision that details the impact the company wants to have on the world. That's why defining a strong vision is so important in successfully marketing your business. To find your vision, ask yourself these questions:

First, how will your company affect the future? Is it going to alleviate poverty? Protect the environment?

Second, how will you make sure that your day-to-day work supports this vision? Will your office go paperless to protect forests?

And finally, how will the changes your business promotes make your customers feel and act? Will they make people healthier? Happier? More successful?

By answering these questions, you'll get a sense of what your business is focused on and know how to make decisions in support of this vision. But remember, your vision can always change and grow over time. So, while your vision should be clear from the get-go and inform your daily operations, it can transform as you and your business develop.

Just take Airbnb. The founders of this online vacation rental site, Brian Chesky and Joe Gebbia, had an initial vision of providing people with an easy way to advertise and book accommodation when traveling to conferences in the United States. Pretty soon, their website was so popular that they had to change their vision to providing users with a platform for listing and booking everything from a bunk bed in Brooklyn to a castle in France.

### 5. Make your values clear and stick with them to attract loyal customers. 

Wouldn't it be wonderful if your customers proudly showed themselves off on YouTube while featuring your product? You can take steps to make customers identify strongly — even showing-off-on-YouTube strongly — with your product. It all begins with demonstrating how you stand for something that your customers believe in.

In this way, your values are fundamental to building your brand story and, if you truly believe in something that shapes the way you do business, make sure people know it. After all, if your brand can stand for a value that consumers share, they'll use your product to express these common beliefs.

For example, when the outdoor gear manufacturers Patagonia began a huge "don't buy this jacket" campaign, it reminded customers of the ecological impacts their purchase decisions had. In the process, the company demonstrated how it stands for sustainability, a value shared by many of its customers.

But that doesn't mean you should lie. If you're dishonest, your customers will find out.

And most importantly, if you don't stick to your values, you'll start _losing_ customers. This is pretty straightforward, since making people believe you're one thing while acting another is a surefire way to kill your integrity.

Just imagine you love an authentic little café around the corner from your apartment. It's always packed with loyal customers eager for their morning coffee, a homemade brownie or even a friendly chat with the barista. You don't mind the long line because you know that the café values making residents happy.

Then, the café's owner decides to put profit over this value and starts expanding the business. He rearranges the interior, starts outsourcing the baked goods and speeds everything up.

All of a sudden you're no longer getting that cozy feeling you used to have when you walked in the door. The values that made the café special are all lost and it's no longer the community-minded spot it once was. As a result, you find a new place to spend your mornings.

### 6. Your location and content should align with your brand story. 

Everybody knows the saying "location, location, location." Location matters when buying and selling property and the same goes for running a business.

That means the space you choose must align with your brand story. If it doesn't, the story will be lost.

Whether it's that little café around the corner that serves its community, or the high-end luxury shoe store that's in the most expensive and sought-after neighborhood in the city, your store's location has a huge impact on your brand story.

It wouldn't make any sense to put a local coffee place smack dab in the middle of high-end shoe stores like Manolo Blahnik and Jimmy Choo. While it could certainly sell coffee in such a location, it would lose its story.

It would no longer be the local coffee place that's core to a small community. It would become a random spot that sells lattes to the occasional high-end shopper.

Another essential factor when creating a strong brand is the content surrounding you and your business. All the content you produce should be designed to bring in customers by telling them the story behind your brand and should, therefore, be in line with that story.

To be clear, content is anything you use to communicate about your brand, from images to video, text and audio. If this content is out of sync with your brand story, customers won't understand your vision and probably won't feel compelled to buy your products.

Say you want to advertise your new bakery, a hometown spot that will dish out fresh-baked favorites to the locals. You take out an ad, but since you're so eager to appear like a business pro, you stuff it with industry jargon.

In the process, you end up alienating your customer base — the residents of your local neighborhood. A better idea would have been to talk to locals directly, using the personal touch that you'd expect from a brand with your story.

### 7. Final summary 

The key message in this book:

**Having a stellar product is no longer enough to build a successful business. You also need a story that infuses whatever you sell with meaning. To create and communicate such a convincing story you need to have a clear purpose for your business and a vision of where it will go.**

Actionable advice:

**Align your company's name with its story.**

A company's name is one of the first things that a prospective client notices and is therefore crucial to shaping the public image of any business. To figure out how you should name your business, ask yourself these three questions:

First, what do you want the name to communicate? Second, does it communicate this to both your staff and customers? And finally, how does the name make your business stand out?

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Meaningful_** **by Bernadette Jiwa**

_Meaningful_ (2015) is a guide to making customers central to your business. These blinks, by teaching you how to produce a product that truly matters to and empowers your customers, will perfectly align your brand with the demands of the twenty-first century.
---

### Bernadette Jiwa

Bernadette Jiwa is an Australian brand storytelling and marketing expert. She has written a number of bestselling books including _Make Your Idea Matter_ and _Marketing: A Love Story_ and blogs at thestoryoftelling.com.

