---
id: 5547721c3666320007290000
slug: the-founders-dilemmas-en
published_date: 2015-05-05T00:00:00.000+00:00
author: Noam Wasserman
title: The Founder's Dilemmas
subtitle: Anticipating and Avoiding the Pitfalls that Can Sink a Start-Up
main_color: 1A6283
text_color: 1A6283
---

# The Founder's Dilemmas

_Anticipating and Avoiding the Pitfalls that Can Sink a Start-Up_

**Noam Wasserman**

_The Founder's Dilemmas_ (2013) reveals exactly what it takes to become the founder of a start-up company. Drawn from the author's research and case studies, this step-by-step guide will help you navigate the rough waters of your company's early stages.

---
### 1. What’s in it for me? Discover the most challenging dilemmas a start-up founder faces and how you can tackle those problems head on. 

Are entrepreneurs and business leaders really so different from regular employees at a large corporation? If so, what exactly makes them different?

With insight based on more than 10 years of research and some 10,000 interviews with business founders in technology and life sciences, these blinks outline just what it is that sets today's entrepreneurs apart from the pack.

While founders may have particular characteristics that make them tick, they too face a daily set of unique dilemmas, conflicts and decisions that threaten their business's success.

In these blinks, you'll learn how you can deal with challenges just like the pros so when you're ready to launch your start-up, you'll handle the worst without breaking a sweat.

After reading these blinks, you'll know

  * what Evan Williams learned about hiring family before founding Twitter;

  * how female entrepreneurs are different than male entrepreneurs; and

  * what Pandora Radio did to ensure it had enough cash to start.

### 2. Entrepreneurs are motivated to strike out on their own to achieve power, influence and autonomy. 

Different things motivate different people. For some it's prestige, while for others, it's financial gain or autonomy.

So what's the difference between someone who wants a career and someone who wants to be an entrepreneur? While employees or career-oriented people find it challenging to give up a steady job and start a business, entrepreneurs find it hard to conform to desk life, and thrive on the risk and challenge of doing their own thing.

A difference of _motivation_ is really what separates these two worlds. For career people, security, prestige, financial gain and affiliation are the top four motivations. For male entrepreneurs, financial gain and control — in the form of power, influence, autonomy and managing people — are what moves them.

Such motivations are expressed in interesting ways. The male founder of Blogger, Evan Williams, knew that his main motivations were power and autonomy. So much so that he turned down a buy-out offer worth millions of dollars, just so he could retain control of his company.

Similarly, the female founder of Sittercity, Genevieve Thiers, was working at IBM in her 20s when she realized she felt stifled, like a cog in a machine. Realizing that autonomy and influence were what she craved, she quit her job to start her own company.

Interestingly, the top motivations for female entrepreneurs are mostly the same as they are for male entrepreneurs: autonomy, power, influence, managing people and altruism. Note, though, the addition of altruism and the fact that financial gain has left the list. For female career people, in contrast, the top four motivations are recognition, affiliation, security and lifestyle.

The first step toward becoming a successful entrepreneur is looking at yourself and defining your motivations. Have you figured out whether you have an entrepreneurial character? If so, read on.

### 3. Entrepreneurs need human capital to start a company. Find talent to shore up your weak spots. 

So you think you're an entrepreneur? Does this mean you should drop everything and start your own business?

Hold up for just a moment. Here's one question to consider. Do you know how the product you want to make is manufactured? If you don't, then maybe you need more _human capital_ before you start.

Human capital refers to the skills, knowledge and expertise in your business and in your intended industry. Barry Nalls, the founder of telecom service provider Masergy, was an employee at GTE, a large telecommunications company, for over 25 years, giving him plenty of relevant experience!

The skills that Nalls gained at GTE guided him through several other small companies before he started Masergy on the strong foundation of his acquired knowledge.

Understanding your product's industry will help you avoid potentially fatal problems. It's no surprise that founders who launch start-ups without applicable human capital have higher failure rates than founders who have previous experience in their start-up industry.

When baseball star Curt Schilling launched his massively multiplayer online gaming (MMOG) start-up 38 Studios, he relied on skills gained from his sports career, such as a strong work ethic and the ability to lead. But he lacked experience in one key area: managing people.

Schilling didn't understand why employees needed or even wanted to take weekends off. Coming from a completely different industry, he needed to learn instead how people were motivated in the business world. So to save his start-up from failure, he had to learn fast.

### 4. New companies need social capital. Make your connections, but don’t linger too long at that old job. 

Human capital is vital, but it won't be enough unless you've got another kind of capital up your sleeve: _social capital_.

Your social capital is the social and professional network you bring to your company as a founder. It should allow you to access established resources, as well as discover new ones.

The more connections you have before starting your business, the less time needed to build your business!

Before starting Masergy, Barry Nalls had established professional connections with potential employees, customers, advisors and investors. That's how he was able to make Masergy viable six months from launching the company.

You have to find a balance, however. While it's true that the more time you spend as an employee, the more social capital you can build, be careful not to find yourself handcuffed to a career while your dreams of becoming a start-up founder slip away.

Potential founders that linger too long at companies spend too much time working on specific things and become too specialized in their knowledge. This makes them less likely to be successful founders, as founders need diverse skills to be truly effective.

Nalls avoided getting stuck in a certain field while at GTE by making sure he spent less than two years in any one position before moving on to the next.

> _"Employees of small companies are more likely than employees of large companies to leave and become founders of their own companies."_

### 5. Having sufficient human, social and financial capital is vital. If all else fails, seek out co-founders. 

Do you have enough cash to start and run your company? Similar to social and human capital, you will need _financial capital_ to succeed.

Barry Nalls founded Masergy on his own, as in addition to his experience and professional connections, he also had enough cash to start the business and cover living expenses until he made a profit.

But many of us aren't as fortunate as Nalls. So what should you do?

Two heads, the old saying goes, are better than one. If you're struggling to make ends meet while going solo, consider finding a co-founder.

Before you do, take stock of your capital: social, human and financial. Do you have enough of each? In which area are you lacking? Identifying these holes will help you decide what your potential co-founder should bring to the table.

Pandora Radio founder Tim Westergren came up with his idea for the business in 1999. Tim had knowledge about the music industry, had the appropriate connections and had sufficient funds to start.

The trouble was, he had never worked in a business before. What's more, his idea for an advanced music database would require highly technical skills that he didn't have. He decided to wait until he could identify appropriate co-founders to help him realize his idea — a decision that was crucial to Pandora's eventual success.

Even if you have all the right capital, you may still want a co-founder, as the tasks of your new business might be too much or too many to handle alone. Or perhaps you'd rather focus on raising money and not human resources, for example. 

Whatever your reason, when you bring on a co-founder, it's essential that you balance your company's roles — the reasons for which you'll discover in the next blink.

### 6. Don’t be arbitrary about handing out titles. Co-founder roles should be crystal clear from the start. 

There's no question that the coveted title of chief executive officer is one every entrepreneur wants. Who gets to be a CEO when there's more than one start-up founder, however, is less clear-cut.

The title of CEO usually goes to either the "most committed" co-founder or the person who came up with the idea for the company in the first place.

So if you're the person who has quit your day job to work full-time on your start-up, or the person who has invested the most seed capital, or the brainchild behind the original idea — then you're usually CEO, regardless of your actual strategic or leadership abilities.

But does this actually work? A better strategy for the long-term health of your company is to delegate roles according to your co-founders's actual _skills_.

Take Apple, for example. The technology company's success was in part based on the early interplay between Steve Jobs's sales skills and Steve Wozniak's technical skills. In this situation, Jobs was the natural CEO while Wozniak headed up research and development.

Of course, if you and your co-founders share similar skills, roles may overlap. At electronic ticketing start-up Smartix, the skills of its three founders overlapped, thus giving each the flexibility to delegate and better manage daily tasks.

Preferable to this situation, however, is when you and your co-founders bring different skills to the table and thus can assume _clearly separated roles_. Why is this ideal? A proper division of labor lets you create greater accountability within your organization.

At Pandora Radio, for example, one co-founder assumed responsibility for technical issues, another for administration and business development, and a third managed contacts with the music industry.

This division of labor helped the founders to develop their own distinct roles and responsibilities. Each success or failure could be traced to its source, and no one could be blamed for someone else's mistake. And most importantly, all tasks were completed when they needed to be.

### 7. Tread lightly when distributing equity. What seems fair today may not be the case tomorrow. 

If you have two co-founders in your start-up, how should you distribute company equity? You may think that a 50/50 split is the best, if not fairest, option. But rarely is this so straightforward.

The experiences of entrepreneur Evan Williams offer an interesting lesson in equity distribution.

When building podcast publishing start-up Odeo, Williams decided to offer co-founder Noah Glass a 70-percent share in the company. Williams's reasoning was that while Glass was working full-time on the project, he was working just part-time.

Yet as the company grew and Williams started working full-time for Odeo himself, tension between the two co-founders over who should be CEO started to affect their working relationship. The dilemma was resolved, however, when Glass decided to leave the start-up.

The moral of all this? Distributing equity early in the life of a start-up is not easy, as you just can't predict what your co-founders will do down the road.

The founders of technology start-up UpDown distributed company equity among themselves before they really knew each other's abilities well. After a few months, it was clear that the division had underestimated the contributions of one founder and overestimated those of another. Attempts to redistribute, however, caused a lot of tension within the team.

To avoid conflict, remember that it's often better to postpone splitting equity until co-founders know each others' skills and abilities, and most importantly, their actual commitment to the start-up.

> _"Many of our natural inclinations about equity splits are wrong or counterproductive."_

### 8. Hiring from your own personal network is easy but also risky. Could you lay off your best friend? 

Who wouldn't want to make it big with their best buddy? Working with people you know well can be easy from the outset, but potentially heartbreaking too.

Founders often find that by hiring friends or relatives, they can build a more dedicated, driven organization. For example, Pandora Radio's founders chose to hire people they already knew with the idea that friends and family would be willing to make more sacrifices and be more productive.

Curiously, investors seem to agree with this strategy as well. Research has shown that companies that hire employees from their own personal networks received valuations that were 37 percent higher than companies that didn't.

Yet hiring friends has its disadvantages, too. It might be tough to talk about salary raises with a cousin or best friend; performance reviews too can become sensitive issues.

Additionally, making tough decisions about team members is even tougher when a friendship is at stake. Pandora Radio founders had to juggle the well-being of the company with the feelings of friends and family when it came to company layoffs.

The key thing to remember is that your hiring decisions should be based on your objectives for your start-up. Though Blogger's Evan Williams hired from his own network to create a loyal, passion-driven environment, when he decided to hire for Odeo, he changed his tack.

With Odeo, Williams was focused on gaining the maximum value for his start-up as quickly as possible, and so decided to hire employees based on their professional experience.

> _"It is extremely difficult to keep the new co-founding relationship from affecting the underlying social or family relationship."_

### 9. Generalists are comfortable wearing many hats, and can adapt easily to early start-up challenges. 

Just as not everyone has what it takes to be an entrepreneur, not everyone is fit to be a start-up employee. Which sort of person then will make the cut?

Surprisingly, it's the generalists, not the specialists, who are worth your while. Start-ups in the early stages require a fluid strategy to cope with the ups and downs of a new business, thus employee roles should be fluid enough to manage these rises and falls.

In fact, your very first employees may move quickly through radically different positions at the start. With that in mind, think how a specialist may struggle with this constant shifting. Imagine asking a life-long accountant to start handling Facebook posts, for instance!

Your ideal start-up employee should also come with experience working in small companies as opposed to large corporations. Frank Addante of StrongMail learned this lesson the hard way.

He hired a qualified vice president of sales who had previously worked for IBM and Oracle. Though the candidate looked great on paper, he was near useless for the first three months. The reason? He wasn't comfortable with the responsibility of building a system from scratch.

Being able to start small and work from the ground up requires a whole host of skills that a person often doesn't gain from working in established systems. This also applies to managing people.

Masergy's Barry Nalls experienced this firsthand when he hired a sales manager who had a background working with larger corporations. Though the manager was capable when leading large teams, he was simply unable to work on his own.

Teamwork is crucial in start-ups, as everybody has to contribute! You might find that you just need people who can work independently, and can do without managers at the start.

### 10. Raising money might get your product built faster, but then you’re on the hook to your investors. 

Spend or save? This seemingly simple decision becomes a crucial one as start-up founders begin to grow their ventures.

Should you look for outside support for your start-up, or tighten your belt and try and make as much as you can from what little money you have?

The decision between _bootstrapping_ — starting a company with little money — and raising capital depends ultimately on a founder's overall goals and market competition.

Some founders, however, choose to go it alone, based simply on gut feeling.

When Jim Triandiflou and Mike Meisenheimer founded Ockham Technologies, they attracted investors who offered them some $2 million to get their project off the ground. The founders, however, turned the money down, seeding their start-up instead with $150,000 from their own pockets.

Why did they do this? "We just felt that we should go sell something [first]," Jim said at the time.

Evan Williams felt the same when he founded Blogger, having friends and family fund his start-up in exchange for equity. However, when he began working on Odeo, Williams's approach changed.

Realizing that the market was highly competitive and that other start-ups were edging in, Williams knew he needed to raise money quickly to enter the market first. So he courted top-tier venture capitalists to raise $5 million in return for 30 percent of the company.

Venture capitalists, or VCs, raise capital from institutions and invest in high-potential start-ups. They often seek to protect their investments by improving a company's organization and discipline.

However, VCs are also known to introduce irksome bureaucracy and reporting into the mix, putting additional pressure on founders. Masergy's Barry Nalls calculated the time he spends preparing for board meetings, and discovered that it totalled as much as a quarter of his time each month!

### 11. Final summary 

The key message in this book:

**Entrepreneurship requires a particular blend of motivation and skill that is quickly put to the test in the early stages of a start-up. With a considered approach to capital needs, company organization and decision making, a savvy founder can overcome any dilemma that he or she faces.**

Actionable advice:

**Get inside your co-founder's head before starting your business.**

If your start-up is run by two founders who are both motivated by control and want to be called CEO, it's a recipe for disaster! To avoid this, make sure your management team is stable and each founder is aware of and understands the skills, abilities and motivations of the rest of the team before starting your business.

**Suggested** **further** **reading:** ** _The Hard Thing About Hard Things_** **by Ben Horowitz**

These blinks explain why the job of a CEO is among the toughest and loneliest in the world, and how you can survive all the stress and heartache involved.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Noam Wasserman

Noam Wasserman is a professor at Harvard Business School. He won the Academy of Management's Innovation in Pedagogy award in 2010 for the course he taught based on _The Founder's Dilemmas_. It was also named one of the top entrepreneurship courses in the country by _Inc. Magazine_ in 2011.

