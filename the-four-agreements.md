---
id: 593e88f4b238e10005ae419a
slug: the-four-agreements-en
published_date: 2017-06-16T00:00:00.000+00:00
author: Don Miguel Ruiz and Janet Mills
title: The Four Agreements
subtitle: A Practical Guide to Personal Freedom. A Toltec Wisdom Book
main_color: A46527
text_color: 804F1E
---

# The Four Agreements

_A Practical Guide to Personal Freedom. A Toltec Wisdom Book_

**Don Miguel Ruiz and Janet Mills**

_The Four Agreements_ (1997) is your guide to breaking free from negative patterns and fully realizing your true self. It was a _New York Times_ best seller for over eight years. These blinks explain how society raises people to conform to a strict set of rules and how, with a little effort and commitment, you can set your own guidelines for life.

---
### 1. What’s in it for me? See how ancient Mesoamerican wisdom can help you today. 

The _Toltec_ were an ancient society of artists and scientists who explored and saved spiritual knowledge from ancestors who'd lived in central Mexico before the Aztec became the dominant group. They had a deep tradition of education and passing on knowledge from generation to generation.

Like most ancient civilizations, they had a system of beliefs about the workings of the world and the humans who populated it. But what can these teachings tell us today? 

That's exactly what we're going to explore. You'll discover how Toltec ideas and concepts can help us to become who we really are and cast aside the chains that keep us from realizing our full potential, enjoying a life of freedom and making peace with ourselves.

In these blinks, you'll find out

  * why you should rebel against the collective dream of society;

  * what's wrong with taking both critique and praise personally; and

  * why most assumptions about yourself and others are wrong.

### 2. We’re taught strict rules as children, which we force ourselves to abide by as adults. 

Nobody gets to choose their native tongue, but the language we grow up speaking isn't the only thing society imposes upon us. Social norms even prescribe the content and form of our dreams.

For instance, everyone has individual dreams, but there's also a collective dream. This is _the dream of the planet_. The rules that define this collective dream are taught to us by parents, schools, religions and other influential forces. It's through this education that we learn what proper behavior is, what we should believe and the difference between good and bad.

However, these rules and agreements weren't chosen by any of us. Our acceptance of such rules without question amounts to our domestication.

If we rebelled as children, adults were still more powerful than us. They could suppress our dissent and punish us if we disobeyed them. Not just that, but we were also rewarded for obeying their beliefs and following their rules. As a result, the vast majority of us surrendered.

Just consider how many parents tell their children that they're "good" when they follow the rules and "bad" when they don't. In this system, the primary reward for proper behavior is attention from parents, teachers and friends.

Naturally, getting such a reward feels great, and we learn to abide by these rules to reap this benefit. We fear rejection and often pretend to be something that we're not.

As a result, at a certain point, it's not necessary for anyone to control us because all of these beliefs are lodged deep within us. In other words, we domesticate ourselves. We devise a perfect self-image and, when we fail to act in accordance with it, punish, judge and blame ourselves.

But there's a different way. We can break free from this structure by establishing new agreements for ourselves, which we'll learn all about in the following blinks.

> _"We don't need to dream a nightmare. It is possible to enjoy a pleasant dream."_

### 3. The first agreement is to be impeccable with your word and never use it against yourself or others. 

Did you know that the word "impeccable" stems from Latin, meaning "without sin"? In that sense, being impeccable with your language means never using it against yourself, as any harm done to yourself is considered sinful.

Such harm might come in the form of self-judgment or blame. You might think that you're too fat or too stupid. Simply by harboring these thoughts, you're using your words against yourself and passing judgement. Instead of doing this, you could insist on reaffirming how great you are and how much you love yourself.

That's why it's key to remember that the words you use are powerful and can either liberate or enslave you — and other people. So, while a word might seem like a random jumble of sounds, it's actually much more.

Your word lets you communicate and express yourself but also transforms the way you think. In this regard, your words can create ideas in your mind, shape your sense of reality and affect the opinions of others.

Just consider this story.

A smart and good-natured woman had a daughter whom she loved with all her heart. One day, the woman arrived home, wishing only to have some quiet time alone. However, her daughter was in a happy mood, boisterously singing.

Losing control over herself for a moment, the woman yelled at her daughter, "You have a stupid voice! Shut up!"

Because of this experience, the daughter believed what her mother had said, making an agreement with herself that her voice was ugly and bothersome. She didn't sing for a long time and even had difficulty speaking to people.

It just goes to show how words can enslave us. Just think of how often you've told yourself you're not good enough. Each time you do this you're making an agreement and choosing to abide by it.

That's why the first agreement is not to use the word against yourself. Next up you'll learn about the second: to take nothing personally.

### 4. You won’t need to take things personally if you have a strong sense of yourself. 

Can you remember the last time somebody was rude to you and you took it personally? It likely wasn't too long ago and that's a problem.

After all, as soon as something is construed as personal, you begin inadvertently agreeing with it and taking it upon yourself, even if it has nothing to do with you. Beyond that, by taking things personally, people get trapped in what's called _personal importance_, a state in which they believe that everything is about them.

This is actually a result of the process of domestication you learned about earlier. It teaches people to take literally _everything_ personally.

But in reality, absolutely nothing people say or do to you is about you. All of it is actually about them.

People have their own dreams and live according to their own agreements. When someone calls you fat, it's not about your own body, but rather the issues, opinions and beliefs with which they're dealing.

That means when a person is happy they might tell you that you're the best, but call you the devil when they're angry.

To overcome this issue, know who you are. Because then you won't need to take other people's comments personally. When you know yourself, you don't need to seek this information from others or get their acceptance. As a result, nothing they say will affect you.

This is also helpful in realizing that all people view the world from a different perspective and that you too should recognize your own viewpoint.

Just imagine somebody telling you that your words are hurtful. It's not actually your words that are causing them pain, but the wounds they experience as a result of their own agreements. In the same way, when you get mad about something someone else says, it's actually because of your own fear, which makes it essential to deal with such emotions.

### 5. Instead of making assumptions, ask questions. 

Have you ever assumed something was seriously wrong with one of your relationships simply because the other person failed to say "hi" one time? Well, similar things happen to everyone and this is just one example of how assumptions create problems.

After all, when you make assumptions, you think that your thoughts must be true and feel duped when they turn out not to be. This is a major issue as most assumptions have just about no basis in reality. They only appear real in the imagination.

So, when you have a hard time understanding something, you assume you know what it means. Then, when the truth becomes clear, you see it actually has an entirely different meaning.

Imagine you're walking through a city when a beautiful person catches your eye and shoots you a beaming smile. You might immediately assume that the person likes you and drift away on a daydream that ends with the two of you getting married.

Such out-of-control assumptions can also cause serious problems in relationships, as people often assume that their partners know what they're thinking. As a result, people believe that their partners will do what they want and get disappointed and angry when they don't.

But you also likely make assumptions about yourself. You might think you can do something and, when you fail, feel bad for overestimating yourself, while in reality, you might have just needed to ask more questions to succeed.

So, assumptions are damaging. You should replace them with courageous questions instead. While asking questions can be difficult, it's the only way to beat back against assumptions.

If a friend doesn't greet you, rather than assuming something's wrong, you might ask, "What's going on?" The most important thing to keep in mind here is clear communication. That means asking as many questions as you need to get total clarity on the issue. By asking questions, you'll get closer to the truth and not feel the desire to assume.

Those are the first three agreements. Coming up you'll learn about the fourth, which will help you put the first three into action.

> _"We make all sorts of assumptions because we don't have the courage to ask questions."_

### 6. Always do your best even if it constantly changes. 

Do you remember your parents and teachers telling you to just do your best when you were a kid? Well, it's pretty sound advice and you should always do your best, whatever it might mean in a given context.

That's because doing your best always depends on the situation you're in, and as long as you give it all you've got, you won't need to blame or judge yourself. Doing your best changes from day to day. On certain days, your best will be totally stellar, while on others it might not be so great.

Just consider how different you are in the morning when you're full of energy compared to late at night when you're exhausted after a long day.

You must keep this in mind because if you try to go beyond your best at any given moment, you'll just end up depleted and worn out. Naturally, wearing yourself out will mean it takes you longer to reach your goal, but doing less than your best will make you prone to self-judgment, criticism, guilt and frustration.

So, to do your best, do things for their own sake. You should work hard because it makes you happy, not because of some external motivation.

A great example here is salary. You'll never do your best if your only motivation is your paycheck. Most people only do their job for the money and not because they enjoy it. As a result, most people have jobs that feel difficult, joyless and imposed upon them. This lack of fulfillment means that people need to distract themselves at the weekend through partying, drinking and other less than beneficial activities.

On the other hand, if doing your best means working hard because you love the task at hand, you'll perform better and your work will begin to feel effortless. In this way, the final agreement — to always do your best — will increase the power of all the other agreements, while helping you free yourself.

### 7. Break with your old agreements and find your freedom. 

Now that you have a sense of how your old agreements cause you suffering, here are three ways to break with them and free yourself.

The first relates to the dream you're living right now, which is called _the dream of the first attention_, since people used your earliest childhood attention to forge it. But you're not a child anymore and today you can start changing your dream and believing whatever you want.

You also have the benefit of beginning to notice that the things you learned as a child aren't the sole truth. Because of this, you can alter your dream, decide on your beliefs and create what's called _the dream of the second attention_.

To do so, begin by noticing the fear-based beliefs that cause you unhappiness. Then, break them apart bit by bit, replacing them with new beliefs like the Four Agreements.

The second way to become free corresponds to the Toltec belief that there's a parasitic organism in control of our minds. To break free from this burden you must practice forgiveness, which cuts off the parasite's food supply.

Imagine waking up in the morning, loaded with energy. Then you get into an argument with your partner and suddenly feel totally depleted, trapped in negative emotions. These feelings sap your energy and make it difficult to change your life or those of others.

Not just that, but your resentment feeds the parasite, causing more negativity. To stop this vicious cycle, you need to forgive people who hurt you, including yourself.

And finally, the third way to free yourself is to live each day like it was your last. Doing so will let you see that you have nothing that doesn't exist in the present. It gives you a clearer vision of how you want to live. This is called the _initiation of the dead_ and it asks, "Do you really want to spend this amazing moment worrying what other people think of you?"

### 8. Final summary 

The key message in this book:

**From the moment we're born, society makes us conform to its rules, thereby preventing us from realizing our true selves. But it's possible to break free by replacing these ingrained rules with the Four Agreements from ancient Toltec wisdom.**

Actionable advice:

**Start to break your agreements**

Most people live by hundreds, if not thousands, of agreements. Take a moment to break one of them right now. Maybe you tell yourself that you can't sing. To break this agreement just sing your heart out and imagine someone applauding your performance. By following this process, you can chip away at all your agreements, replacing them with new ones that set you free.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Fifth Agreement_** **by Don Miguel Ruiz and Don Jose Ruiz**

Our perceptions of our true selves are clouded by the society we live in and the people around us. _The Fifth Agreement_ (2010) introduces five pacts that will help you strip away those misconceptions and uncover the truth about who you really are — without running off to a mountain top to be a hermit.
---

### Don Miguel Ruiz and Janet Mills

Don Miguel Ruiz was expected to follow in his family's footsteps, carrying on ancient Toltec wisdom as a spiritual teacher. Instead, he became a surgeon. It was only after a near-death experience that he rediscovered the traditional wisdom of his people and devoted himself to becoming a spiritual master, or _nagual._

Janet Mills is the founder and president of Amber-Allen Publishing and the co-author of the Toltec Wisdom series.

