---
id: 53d8cefa3930340007840100
slug: the-four-steps-to-the-epiphany-en
published_date: 2014-07-29T00:00:00.000+00:00
author: Steve Blank
title: The Four Steps to the Epiphany
subtitle: Successful Strategies for Products that Win
main_color: 2A5E76
text_color: 2A5E76
---

# The Four Steps to the Epiphany

_Successful Strategies for Products that Win_

**Steve Blank**

By looking at examples of companies that failed, _The_ _Four_ _Steps_ _to_ _the_ _Epiphany_ explains the key insights startups need to achieve and sustain success, and to steer clear of the path to failure.

---
### 1. What’s in it for me? Learn how to avoid the costly mistakes many startups make. 

If you've ever been involved with a startup, you know the operation can be chaotic. Typically, there will be anywhere between a dozen and a hundred projects, tasks and focus areas that are all "absolutely vital" and all in some degree of incompletion.

In this storm, it would be useful to have a compass that clearly indicates priorities. These blinks are that compass. With them, you'll know how to prioritize in the beginning, and what approaches simply can't work. This will help you avoid the mistakes other startups have made, that have bankrupted them.

In these blinks, you'll learn:

  * who your ideal first customers are,

  * to identify what kind of market you're operating in, and

  * why it's crucial to launch your product as early as possible instead of tinkering with your product till its perfect.

### 2. Unlike large companies, startups need to find their customers and prove that their vision is viable. 

Many business people labor under a fundamental flaw in thinking: they believe that startups are just tiny versions of big companies, and therefore to succeed they just need to use the same methods as the larger businesses. Not so.

First, unlike large businesses, startups can't usually introduce a new product to the mainstream market.

Established companies have a pre-existing customer base and know their competitors well, so to come up with new products they use a _product_ _development_ process: first design a product and then find suitable customers.

Startups on the other hand don't understand their market environment, and need to first get to know their potential customers before developing a new product. They use a _customer_ _development_ process: first build a customer base and then create a suitable product.

For example, _Webvan_, an innovative start-up that founded the first online grocery business in 1996, shows this customer development approach is crucial. Instead of researching its customers and their needs, the company focused on the product development process, and failed miserably.

Second, unlike big companies that know their market and know how to sell to it profitably, the founders of startups have no idea whether or not their business idea will actually fly. It's up to them to prove that their vision is viable.

In this sense, entrepreneurs are like classical heroes, who are called upon to undertake a quest, and must journey into the unknown along uncertain paths, facing obstacles and difficulties. But thanks to their powerful initial calling, they persist and become heroes when they reach their goal.

Similarly, entrepreneurs must also find a path to making their vision into a reality. They learn as they go, discovering who their best potential customers are and how they should conduct business.

### 3. To keep themselves following the right path, every startup needs a written mission statement and a set of core values. 

As stated in the previous blink, when a startup sets out to fulfill its visions, it may not yet know what path it will follow. This is why it needs to make some decisions right from the get go to help keep it on the right path.

The first thing a startup must define is a set of fundamental and long-lasting _core_ _values_ to guide it on its journey.

For example, a pharmaceutical company may decide one of its core values is "First and foremost, we make drugs that help people." This value means that whenever it needs to decide about profits versus helping people, it'll know what to do.

Whatever core values a company chooses, it is important that they are authentic and the company stands behind them.

And besides the core values, a startup also has to have a written _mission_ _statement_.

Why?

You've probably also noticed in your personal life that if you want to achieve something, it's easier if you write down a plan first. The same is true of a startup. To achieve its aims and objectives, it needs to write them down in a mission statement.

Almost every startup goes through a period of turmoil in its early stages, and it is during this time that the mission statement will be especially vital in showing the way forward.

Note that unlike the core values, the mission statement can change over time as the company's business situation develops through, for example, launching new products and entering new markets.

So how do you define these key parts of your company?

The mission statement should answer questions like: why does our company's staff come to work? What are our goals for growth and profit? How do we know when we're doing good work?

With authentic core values and a good mission statement, a startup has a much better chance of staying on the path to success.

> _"Every traveler starting a journey must decide what road to take."_

### 4. Startups must choose their strategy based on the market type they find themselves in. 

Each startup is different, and so it follows that the right strategy for each startup is, too. Mostly, the right strategy depends on the environment, or _market_ _type_.

The crucial question is whether the startup faces an _existing_ or _new_ market.

An _existing_ _market_ is one where it's clear who the customers and competitors are. The upside is that you won't have to spend time gathering information on potential customers, but the downside is that you'll be facing established competitors, whom you'll have to outperform to successfully enter the market.

One company that faced an existing market was the microprocessor startup Transmeta, which wanted to challenge Intel's dominance with a new, Intel-compatible chip that had far superior energy efficiency. Transmeta saved time and money because it already knew its potential customers — anyone with an Intel-processor — and whom they were competing against — Intel. But in the end, the established giant developed its own low-power chip that put Transmeta out of business. 

The second type of market is the _new_ _market_, which the startup creates by finding users. Though this is not easy, the upside is that there are no competitors yet.

One startup that failed to create a new market was _PhotosToYou_, the first company to print high-quality photos from digital cameras in the late 1990s. It focused most of its resources on branding instead of researching who its potential customers were, and consequently struggled to reach prospective customers.

Finally, startups also have a third option: _resegmenting_ an existing market by offering a cheaper version of an existing product or a niche product. Resegmenting can open up a whole new customer base of people who could not afford the product before or who didn't find a product that was right for their needs.

For example, the fast food company _In-N-Out_ _Burger_ managed to capture some of the fast food market despite fierce competition from McDonald's and other established players because it focused simply on offering higher quality hamburgers for the same price.

### 5. Mistakes happen and market conditions change: startups need to learn from errors and respond rapidly. 

Sooner or later, every startup will make a mistake.

While it's impossible to avoid any and all mistakes, it is better to catch them early. Startups can reduce mistakes and their impact by collecting lots of feedback from their users as soon as possible — even before there's an actual product to sell!

The goal is to find out if there's market for your product: people who will actually buy it. If not, you'll just have saved yourself a costly mistake, and by reviewing the feedback you'll be able to change your product to better suit customer needs. Once this is done, you start the process again: collecting feedback and using it to optimize your product. At this stage, the goal is not to sell anything, but rather to collect as much information as possible.

Let's say your new product is a sheet of plastic that protects cell phone screens from cracks and scratches. You would probably want to ask how inconvenient people find broken screens and how much money they spend on replacing them, as well as what they feel the ideal solution would be.

Just as startups can hardly afford mistakes, they also can't afford to be slow and unresponsive, since they operate in an environment that is ever-changing.

Stiff hierarchies and organizational structures that hinder fluidity are out of the question: if feedback indicates that a product should be changed or a new opportunity pursued, the startup can't wait for someone up the chain of command to make a decision — competitors won't.

Every team member needs to have the authority to make urgent decisions.

Now you know some of the keys to a startup's success. But even if all these are adhered to, a startup may still fail. What is the final key to startup success?

> _"It's OK to screw up if you plan to learn from it."_

### 6. The customer development process can lead startups to success; relying on product development can lead to disaster. 

In the first blink, you learned that startups cannot follow the product development process used by big companies. Doing so can lead to a disaster.

Startups who try this approach believe they can just build and launch a great product, after which customers simply appear. It rarely works.

In fact, product development is just an internal product process, and to create products that people buy, it needs to be synchronized with findings from the customer development process, which focuses on external influences for the product's success — customers.

Consider the online-furniture retailer Furniture.com. It focused primarily on building an expensive website, a recognized brand and a large-scale shipping system before researching market demand. Unfortunately, it turned out customers weren't ready for online furniture shopping yet, so all the effort had been for nothing.

Rather than making this mistake, your startup should focus on the customer development process: build your customer base and ensure your products suit their needs. How exactly your startup carries out the customer development process depends on your core values and mission statement, but primarily on which market type you operate in and the feedback your customers provide on your product.

The designer product retailer _Design_ _Within_ _Reach_ honed in on this principle, adjusting every catalog it published according to customer feedback received from the previous catalog. The retailer's attention to customer feedback resulted in increasing numbers of customers and a larger average order size.

> _"'Build it and they will come' is not a strategy; it's a prayer."_

### 7. Startups should first develop a product for eager early adopters, not the mainstream market. 

Have you ever had a problem so pressing that you'd be willing to pay anything for a solution, even if it wasn't perfect?

The ideal first customers of startups are in this position.

In the beginning, startups have to sell their product to a small set of enthusiastic _early_ _adopters_ who have a pressing problem.

For example, let's say your product is a piece of software that helps banks cash checks, but it hasn't been perfected yet. An ideal first adopter for this product would be a bank that loses something like half a million dollars every year because they have to do their check cashing manually. This problem means they'll be happy to pay you that same half a million for your software. What's more, by installing the software and seeing it in action, you'll get information for developing it further.

But many startups make the mistake of not looking for early adopters and, instead, spend a lot of time and money building a perfectly engineered product for the mainstream market. These startups fall into various traps — of then not having enough money to alter their product in response to customer feedback, or of offering a "perfect" product that is outdated by the time it hits the market.

_FastOffice_ fell into this trap in 1994. They offered a new home office device with email, fax and phone functionalities all in one. They raised $8 million in funding and focused on engineering the product just right. Still, sales were very disappointing, and unfortunately it was too late to tailor the product according to customer feedback, since most of the money had already been spent on engineering.

Don't make this mistake. Get the product out of the door as soon as possible to get real customer feedback and then tailor the product accordingly.

### 8. As startups grow, they need to define a strategy for reaching mainstream customers. 

No startup can remain a startup forever. To grow, it has to continuously build a customer base.

This goal is achieved by targeting different customers over time:

First comes the _customer_ _creation_ phase, during which startups have to decide whether to continue targeting customers similar to the early adopters, to enter a specific niche market, or to approach a broader range of customers. The answer depends on whether they find themselves in a new, existing or resegmentable market.

By studying its customers, a startup might find out, for example that the early adopters are all under 35 years old and from big cities. Depending on the market type, the startup may then decide to either continue to pursue them or to shift focus.

The main point is that startups can't target mainstream customers before they understand how and why these customers would buy the product, and have refined their product accordingly. Once the product is refined, the startup finds itself in the _company_ _building_ phase, in which its sales grow.

No matter how you decide to go after mainstream customers, you'll have to adapt your strategy: you can't simply repeat what you did for early adopters.

Basically, you have two options:

First, you can use your early adopters as cheerleaders. They're enthusiastic about the product anyway since it solved a problem for them, and they can promote it to mainstream customers through word-of-mouth or by reviewing it in public. You can also use industry experts, bloggers and other thought leaders to spread the word.

The second option is to utilize _positioning_ : defining and describing your product so that your target customers recognize it, know its characteristics and what appeals to them. For example, Starbucks positioned its product with the slogan: "Starbucks is the No. 1 place for coffee."

### 9. Startups need to define what messages they want to send to their target customers and choose the right media to do so. 

These days, companies bombard consumers with advertisements and messages every waking hour.

For a startup, it is crucial to send the right messages, because they can strongly influence the consumers' perception of the company. A company's messages range from how it names its products to the advertisements it uses.

For example, Santa Clara County in California in 1981 had a rampant fruit fly problem, that led them to spray a pest-control chemical, called Malathion. Had it been called something like Bugs Away or Summer Dew, it may have gone unnoticed, or even appreciated. But it turned out residents were furious, since the product name sounds dangerous and harmful.

Besides crafting the right message, it's also important that a startup knows how to convey that message.

As mentioned before, "unpaid messengers" like early adopters, industry experts and other thought leaders are effective. Nevertheless, paid media like advertisements in magazines, on billboards or websites should also be a part of the marketing communications strategy.

To find out which media your prospective customers use, ask your early adopters what sources they used to inform themselves before making their purchase. You can also look at market research on what kinds of media potential mainstream customers, experts and opinion leaders use. Then target the sources of media that get the most attention from your customers.

For example, if you find your target customers read only prestigious newspapers like _The_ _Wall_ _Street_ _Journal_, don't kid yourself into thinking you'll reach them with ads in obscure trade magazines just because advertising there is cheaper.

### 10. Final summary 

The key message in this book:

**Startups** **aren't** **miniature** **versions** **of** **big** **companies.** **While** **big** **companies** **just** **implement** **their** **strategy,** **startups** **still** **have** **to** **figure** **out** **what** **their** **market** **and** **strategy** **are.** **To** **succeed,** **startups** **need** **to** **listen** **to** **their** **customers** **and** **have** **a** **flexible** **strategy.**

Actionable advice:

**Define** **the** **soul** **of** **your** **company:** **the** **core** **values** **and** **the** **mission** **statement.**

No matter what industry your startup is in, one thing's for sure: you'll have to handle plenty of uncertain situations, in which you don't know the answers. Your core values and mission statement will guide you and keep you on the right path.

So how do you define these key parts of your company?

Your core values should somehow embody your morals, as well as include how you intend to differentiate your company from others, for example, in the way you treat your customers.

Your mission statement, on the other hand, should clarify your goals, the tasks to fulfill them and why your employees come to work every day.
---

### Steve Blank

Steve Blank is a founder of eight startups and a teacher at the University of California at Berkeley, Stanford University and Columbia University. He has written several books on entrepreneurship.

