---
id: 59da758ab238e10005d6a179
slug: the-four-tendencies-en
published_date: 2017-10-09T00:00:00.000+00:00
author: Gretchen Rubin
title: The Four Tendencies
subtitle: The Indispensable Personality Profiles That Reveal How to Make Your Life Better (and Other People's Lives Better, Too)
main_color: 50ADD8
text_color: 2F6680
---

# The Four Tendencies

_The Indispensable Personality Profiles That Reveal How to Make Your Life Better (and Other People's Lives Better, Too)_

**Gretchen Rubin**

_The Four Tendencies_ (2017) reveals the four personality types that dictate how people react to the expectations in their daily lives. Expanding on the first chapter to her book on habit _Better Than Before_, Rubin provides insight and advice to help you understand your own tendencies, as well as those of the people you live and work with.

---
### 1. What’s in it for me? See how the Four Tendencies are reflected in the people around you. 

How do you respond to expectations? While this might seem like a straightforward question, we can gain tremendous knowledge if we venture to explore it in greater depth. In fact, this question can be rewarding not only in terms of the self-insight it can offer, but also by deepening our understanding of other people.

In these blinks, you'll discover that the way people respond to expectations, both their own and those posed by others, fits into _Four Tendencie_ s: _Upholders_, _Questioners_, _Obligers_ and _Rebels_. You'll learn what exactly it means to fit into each of these categories and how better understanding our own tendency can help us improve our productivity, and how we relate to others.

These blinks aren't about pigeonholing people into limiting categories, rather it's about providing the insight that allows us to reach our full potential.

You'll also learn

  * which tendency is most likely to be a snitch;

  * why you shouldn't let a Questioner choose your washing machine; and

  * what can be the only way to make an Obliger exercise.

### 2. The Four Tendencies tell us about our nature and how we respond to expectations. 

One day, the author Gretchen Rubin was chatting with a friend over lunch and the conversation turned to exercise. Her friend was reflecting on the fact that when she was on the track and field team at school, she had no trouble finding the time to run, but now, forget about it!

"Not having the time" is a common excuse for why we can't get something done, but if you really want to get to the bottom of why some tasks are so hard to accomplish, you need to understand _The Four Tendencies_.

The Four Tendencies are all about how we respond to the two kinds of expectations we face every day: the _outer expectations_ that come from work and society, and the _inner expectations_ we set for ourselves.

The first of the Four Tendencies are _Upholders_. These are people who excel at meeting both inner and outer expectations.

The second is _Questioners_, who are people who meet inner expectations but question and struggle with outer expectations.

The third is _Obligers_, people who deal well with outside expectations but struggle with their own.

And the fourth is the _Rebels_, who push against both inner and outer expectations.

In the case of Rubin's friend, who excelled at running when she had a coach and a team, but doesn't find the time on her own, this is a typical scenario for an Obliger, someone who needs outside accountability.

As we'll see in the blinks ahead, none of these categories are inherently bad or meant to make you feel boxed in or doomed to lifelong problems. They're meant as tools for gaining insight into your nature, and the nature of those you work and live with, so that you can be a more confident and productive individual.

> _You can find out which of the Four Tendencies you belong to by taking the test at_ happiercast.com/quiz

### 3. Upholders are reliable people who take care of themselves, but they do face challenges. 

First of all, you should know that the author, Gretchen Rubin, slots right into the Upholder category, so she might be a little biased toward this one!

At first glance, Upholders can appear to be an ideal category since they are generally very reliable and productive people. Since they respond well to both the expectations placed on them by other people and the expectations they place on themselves, they get their work done efficiently and make time for themselves.

In other words, an Upholder is the kind of person who has no trouble getting to work on time _and_ getting a full eight hours of sleep.

To make sure they take care of the work given to them, as well as their personal tasks, Upholders are fond of schedules, to-do lists and having a clear understanding of what's expected of them.

Upholders find great satisfaction in meeting expectations and following rules, which means that these people are the kind that find it gratifying and liberating to have a disciplined life. In fact, a good motto for Upholders is "discipline brings freedom."

This Tendency doesn't come without its challenges though, as anyone who's lived with an Upholder will attest.

They can, for example, be so ready to follow the rules that they neglect to ask questions and end up blindly adhering to rules and directions that are harmful or just plain wrong. These are also the kind of people who are most likely to be a snitch and the least likely to embrace change.

Upholders can also experience what's known as _tightening_, which is the opposite of how others experience a new habit. When you begin a new healthier diet, you might stick to it for a few days before it slowly falls by the wayside as you revert back to your usual comfort foods. An Upholder, however, can casually start a new habit, but over time it'll _tighten_ and become stronger and more controlling over their life.

### 4. Upholders should receive clear instructions, and their frustrations are best met with understanding. 

Upholders often have easy interactions with doctors and managers, since they'll readily take orders and aim to please.

If you're trying to manage an Upholder, the best piece of advice is to make sure you give clear and precise instructions. Once they have a clear understanding of the priorities and what's expected of them, Upholders can then be left to take care of business. They're generally self-starters, so they don't require a lot of micromanagement.

However, they can have trouble delegating duties and adjusting to changes in routines. So if you see work piling up, or if there's been an adjustment to procedures, you might want to check in to see if they need help.

Upholders make great bosses and managers, but there are some things that can really get them frustrated.

People in all of the Four Tendencies can have problems understanding why everyone else isn't like them. So Upholders can get frustrated and impatient with people who fail to meet expectations — they don't understand why some people can't just put their mind to a task and get it done.

This also means they can get upset and hostile when a mistake is made, which includes becoming defensive or beating themselves up for a mistake they make themselves. Making an error is such a potent fear that Upholders will avoid accepting a good opportunity, like a promotion, if they sense there's a chance they might fail.

For the spouse or partner of an Upholder, dealing with these characteristics is a matter of understanding and tolerance.

Remember that all Upholders want to meet expectations, so you can help by, for example, avoiding making spontaneous changes or casually suggesting a plan that hasn't been thought out. This is because Upholders are eager to please, so they might latch onto a bad idea and think they need to see it through to the bitter end.

### 5. Questioners are resistant to meeting the needs of others, and their nature can be both a benefit and a hindrance. 

"I'm willing to help out, but you have to convince me why it's worth my time." Does this sound familiar? It's a typical attitude of the second of the Four Tendencies, the _Questioner_.

Questioners do a fine job of setting and meeting their own expectations, but they resist the expectations of others and feel a strong desire to question everything.

The statement that best defines a Questioner is, "I do what makes sense, even if it means ignoring rules or other people's expectations." Unlike Upholders, a Questioner isn't going to follow your instructions just because you're the boss, or even if it's a procedure people have been following for years. They want to know why you made this rule and whether or not it's fair.

As you can imagine, Questioners can be exhausting to deal with at times, but their nature can make them tremendously valuable.

Since Questioners are skeptical of rules and procedures, they're great at spotting the ways in which a procedure can be improved. Questioners are always thinking there's a better way to do something, which makes them a perfect fit for an organization that wants to stay on the cutting edge.

"Why are we using this software?" "What's the benefit of this policy?" "Why do we need weekly meetings?" These are the kinds of challenging questions they ask, which can be of great benefit to any company — as long as the management is looking for novel ideas and doesn't consider questions to be an act of insubordination. As such, some organizations may see a Questioner as not being a "team player."

Furthermore, the nature of a Questioner can be a hindrance when it results in _analysis paralysis_.

The main reason for questions is to make sure the right decisions are being made, and even something as simple as buying a washing machine can result in days of research to identify the best machine. But sometimes this can result in the Questioner being overwhelmed and unable to make any decision at all — in other words, they will experience analysis paralysis.

### 6. Questioners need clear justification, they don’t like being questioned and should avoid certain complicated jobs. 

Have you ever been driven nuts by a kid who constantly follows up every answer with, "but why?" Then you already have some experience in dealing with a Questioner.

The key to dealing with a Questioner is to be precise with your reasoning and justification when you want to give them a task.

If you want your Questioner partner to buy bread on the way home, don't just send a text that says, "Pls pick up bread, thx!" While it might take some extra effort on your part, you'll be preventing a lot of hassle by being exact and asking him to pick up crusty sourdough bread because your mom is coming over tonight and it's her favorite, and also because it will go perfectly with the soup you're making. With this justification, you'll be sure to avoid any follow-up questions.

Another thing to keep in mind is that Questioners don't like being questioned!

If this sounds like a perfect irony, you're right — but the fact remains that Questioners tend to feel insulted when someone questions their motives or reasoning. This is likely because they're so thorough in their decision-making that they feel their choices should be seen as unquestionably logical.

However, Questioners do like to share their knowledge, so you can avoid hurting their feelings by phrasing your question appropriately. So, instead of "Why are you doing that?" ask something like, "How did you come to this conclusion?"

While Questioners can be great at any job, there are some scenarios they'd do best to avoid.

Many Questioners find success in research-heavy roles and auditing jobs that suit their inquisitive nature and their knack for improving efficiency. But jobs that require a lot of decisions, like designing a home, are best left to people who don't have a tendency to fall into analysis paralysis. These are jobs that are best left to people who excel at making quick decisions.

### 7. Obligers struggle to meet their own expectations, but this can be solved by manufacturing outside accountability. 

Do you know someone who always puts others ahead of themselves? Someone who has no problem working overtime to help his boss but feels uncomfortable asking for any time off?

These are the signs of an Obliger, a person who excels at meeting outside expectations but struggles with meeting inner expectations.

Some of the everyday tasks an Obliger will have trouble with are exercising, taking an online course or doing much of anything that requires self-motivation.

Conversely, they are quite effective at meeting the demands of others. In fact, since Obligers are the largest of the four groups, they are the dependable rocks of society. Even so, they struggle mightily to give themselves the same respect they give others.

However, there is a reliable way to fix this unhealthy imbalance, namely by turning internal expectations into external ones. By doing this, Obligers can create the kind of outside accountability they need to take action.

Let's say you live alone and have trouble keeping the place clean unless you're expecting company. For some people, all it takes is to imagine there are people on the way over and they can start cleaning; for others, they'll invite people over for the very purpose of creating the accountability they need to spruce up the place.

Another method is the threat of being charged a fee. Some Obligers find the motivation they need to exercise regularly because their gym will charge them if they don't show up to their appointments.

But money isn't always enough; the necessary accountability is only achieved when there's also the threat of letting a real person down. Obligers can get pretty creative in this regard, such as by exchanging their exercise gear with another person. This way, if you don't show up with the gear, the other person won't be able to exercise. This is exactly the kind of accountability many Obligers need to do something good for themselves.

> The author has an app called the Better App that allows users to create accountability groups to help each other meet inner expectations.

### 8. Obligers may be the toughest category to be in, so they may get the most out of understanding the Four Tendencies. 

If the last blink sounded very familiar to you, you may also be familiar with people telling you that you simply need to be "more selfish." You may also feel a sense of shame at needing to come up with these outside sources of accountability in order to take care of yourself.

Indeed, the Obliger category may be the most difficult of the Four Tendencies.

Over two-thirds of Obligers report feelings of frustration over their inability to devote any time to themselves. Feelings of low self-esteem are also common, which can be made worse by ignorant Upholders who call Obligers lazy for not exercising.

This frustration can erupt into _Obliger-rebellion_, which is when an Obliger snaps after one too many times of being taken for granted, treated unfairly or shamed with accusations of being lazy or pathetic. In these moments, they might quit their job or break up with a partner.

Other Obligers might get through their difficult times through small acts of defiance, like being deliberately late to work or refusing to prepare for a presentation. But these often only serve to self-sabotage the Obliger more than anything else, since they're fireable offenses.

Since they often have the most problems, it makes sense that knowing about the Four Tendencies might also benefit the Obligers the most.

Many people benefit from simply discovering these categories and seeing how they all make sense. With this information, they now know that it's part of human nature and not some personal defect. They can start setting up external accountability and begin making life better.

Many Obligers have had to deal with bosses and therapists who don't listen to their requests for accountability and tell them to "grow up and learn how to be accountable for yourself." If only it were that easy!

As more people who fall into different categories discover the Four Tendencies, they'll also be able to make life easier for Obligers. They'll see that they aren't, in fact, lazy, and that they need a certain amount of oversight.

### 9. Rebels resist meeting any expectations or being bossed around. 

Last but not least is the fourth tendency: the Rebel, whose motto could be "You can't make me, and neither can I." Because not only does the Rebel resist meeting outside expectations, they resist their own expectations as well.

Rebels are all about individuality, and they want everything they do to be a reflection of their unique self. As such, anything that could be considered an expectation gets rejected.

If this sounds rather extreme, keep in mind that Rebels are the smallest group of all four tendencies; it's also true that this group loves to defy assumptions and prove people wrong.

But even though Rebels hate being bossed around, they're willing to work hard. It's all a matter of framing things in a way that makes Rebels feel like they are the ones making the decision.

If you're the parent, teacher or boss of a Rebel, you know it can be a challenge dealing with their attitude. If you send an email to a Rebel with the title "Please read!" you shouldn't be surprised if it's immediately deleted.

You don't want to give a Rebel any sort of direct order, but if you give them the necessary information, explain the potential consequences and offer them the freedom to make their own choice, you stand a good chance of getting your desired outcome.

So, provide them with the details of the current situation, the consequences of the different choices available and then walk away; this will give them the space to make their own decision. If the Rebel has an audience staring at them and waiting for the decision, this will still feel like they're being bossed around.

Instead of "Let's meet up next week," try something like, "Let's meet up when you feel like it. I have next week off so that works for me."

### 10. Rebels can frustrate themselves, but there are ways they can meet their own expectations. 

Just like Obligers, Rebels can have trouble taking care of themselves and meeting their inner expectations. While it might be for different reasons, such as not wanting to be a conformist, the result can be the same.

Even if a Rebel wants to exercise and eat a healthy diet, they can be frustrated with their inability to stick to routines and do what's good for them.

Being an authentic individual is at the top of a Rebel's priorities, so the secret to meeting expectations is for them to align their identity with their goals.

For example, if you're a Rebel who has trouble staying in shape and eating healthily, make being a great chef and fitness geek a central part of your identity. Once this takes hold, a well-made meal and yoga will become part of who you are.

Other Rebels have used play-acting to get around these Rebel barriers. So, if you need to pay the bills, even though every fiber of your being says that doing so is the worst kind of conformist nonsense, just put on the accountant hat and pretend you're someone else for the time it takes to get it over with.

Another way to get in the healthy frame of mind is to get your Rebel mind going against the corporations that want you to eat junk food, smoke cigarettes and drink booze. So, if you're trying to set inner expectations related to quitting these vices, you can always use the anti-corporation angle.

One method that has proven quite reliable to setting inner expectations in a Rebel is the bet.

This has worked well for the loved ones of Rebels, and all it takes is saying something like, "I bet there's no way you can quit smoking." Rebels love a challenge and they love to prove someone else wrong, so this bit of reverse psychology is surprisingly reliable.

### 11. Whatever tendency you may be, knowing more about yourself can help you succeed in work and life. 

You may think that the only job for an Upholder would be one that enforces rules, since they're so eager to meet expectations, but this is a shortsighted look at what the tendencies tell us.

It's true that our tendency can help us better understand our strengths and weaknesses, but you shouldn't see these categories as limiting anyone to certain roles.

The author was at a dinner with a few Upholders who all agreed that their type were the ones who would make the best business leaders, since they would put the interests of the company ahead of their own. But if it were a group of Questioners, they'd probably agree that _their_ group makes the best CEOs.

The truth is, any type can produce a great leader, and if you want to be successful, it only helps to know more about yourself, including your strengths and weaknesses. And this is precisely how the Four Tendencies can be useful to each and every one of us.

No one type is better than any other, since they all have strengths and weaknesses, and being familiar with these will allow you to understand yourself, your partner, coworkers, boss and family members.

If you're an Upholder who's dating a Questioner, the Four Tendencies can help you understand your attraction. There's a good chance you like that they ask the questions you don't feel comfortable asking yourself.

In work and in life, knowing the characteristics of the Four Tendencies can be extremely helpful. Here's a little cheat-sheet to keep in mind:

Upholders want you to tell them what needs to be done; they value self-reliance and performance.

Questioners want you to justify what needs to be done; they value justification and purpose.

Obligers want you to hold them accountable for what needs to be done; they value teamwork and duty.

Rebels want you to let them decide what needs to be done; they value freedom and individuality.

Just as no one type is more successful, no one type is happier than another, either. What does bring more happiness is understanding ourselves better.

### 12. Final summary 

The key message in this book:

**The key to happiness is to understand your own strengths and weaknesses, collectively known as your Tendency. Once you can recognize these aspects of yourself, you can start to adjust to and compensate for them. No one is perfect, and we can make life much easier on ourselves by avoiding pitfalls and playing to our strengths.**

Actionable advice:

**Make your signage work for all four types.**

Is the kitchen or bathroom at your workplace a mess? Don't make a sign that bosses people around, since it's going to make the Rebels in your office want to be deliberately messy. Instead, make a sign that gives them information, consequences and choices. It could read: "A messy workplace is a sign of disrespect and can negatively affect morale. Please be professional and help us keep the area clean." This allows a Rebel to make their own choice.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Better than Before,_** ** __****by Gretchen Rubin**

So if habits are a key to change, then what we really need to know is: How do we change our habits?  

  

Better than Before answers that question. It presents a practical, concrete framework to allow readers to understand their habits—and to change them for good. Infused with Rubin's compelling voice, rigorous research, and easy humor, and packed with vivid stories of lives transformed, Better than Before explains the (sometimes counter-intuitive) core principles of habit formation.
---

### Gretchen Rubin

Gretchen Rubin is an influential writer whose books have sold millions of copies around the world. Her writing strives to examine human nature in a way that can help anyone understand themselves better and therefore live a happier life. Her best-selling books include _The Happiness Project_ and _Better Than Before_.

