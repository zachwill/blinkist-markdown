---
id: 5bf4178c6cee070007e15066
slug: the-fourth-age-en
published_date: 2018-11-22T00:00:00.000+00:00
author: Byron Reese
title: The Fourth Age
subtitle: Smart Robots, Conscious Computers, and the Future of Humanity
main_color: 499BB3
text_color: 346E80
---

# The Fourth Age

_Smart Robots, Conscious Computers, and the Future of Humanity_

**Byron Reese**

In _The Fourth Age_ (2018), author Byron Reese provides essential context for the subject of artificial intelligence and the kinds of changes we can expect from its proliferation. By taking into consideration the major innovations of the past, Reese provides a valuable framework that will help anyone better understand the potential impact AI will have on the modern workforce.

---
### 1. What’s in it for me? Find out which theories are influencing debates about AI. 

Recent years have been full of books, movies, television shows, and newspaper editorials that all try to predict what a future with artificial intelligence might be like for humans. Not long ago, AI was largely considered the stuff of sci-fi, but it's now clear that we're not far from innovations like self-driving cars being the norm.

As author Byron Reese describes, the world of automation will be a seismic shift for human history, on par with the invention of the wheel. As he puts it, we'll be entering the Fourth Age. This comes with significant concerns about how things will change for humans.

Reese also describes the different schools of thought on how disastrous or beneficial AI will be for us. While no one can claim to be a soothsayer with visions of what's to come, we can gain a better understanding of the philosophies that influence the speculation, cautioning, and panicky editorials of the present.

In these blinks you'll find out

  * which inventions have marked the first three ages in human history;

  * how Plato is still influencing current debates; and

  * the difference between "strong" and "weak" AI.

### 2. Human history can be divided into three key periods, and the first features the invention of fire and language. 

You could reflect on human history, divide it up and categorize certain eras in any number of ways. One way would be with an eye toward innovation and technology. In this light, we can divide history into three key periods, or ages, with the first two going back to prehistoric times.

The First Age is defined by the inventions of fire and language. This age is believed to have started around one hundred thousand years ago, when we were living as hunter-gatherers.

The mastery of fire was a game-changer for humans. It not only provided us with light and warmth, it also increased our safety. But perhaps even more important, fire allowed us to cook food.

Why was cooking so important? Well, since we remarkably use 20 percent of our caloric intake on brain functioning alone, cooking gave the evolution of our brain a huge boost. Cooking made it possible to eat a wider array of foods, and therefore consume many more calories.

Thanks to cooking with fire, we could break down the starches and cellulose that had made many vegetables previously indigestible. Fire also broke down tough proteins, making them easier to chew, not to mention a lot tastier!

Before fire, we had around the same amount of neurons as a gorilla or chimpanzee. Afterward, that number multiplied threefold.

The second key technological advance of the First Age was language, since it allowed us to communicate more complex ideas and abstract concepts. Once we began to exchange information like this, we could cooperate more and expand our imaginations. With this, our ability to tell stories was born.

In telling stories, we could finally express our inner world through the use of both words and symbols, as well as mix multiple ideas in order to suggest new and exciting futures. We can't know for certain what the earliest languages sounded like, but we can use the languages that followed to guess what may have come before. Some theorize that today's 445 languages, including English, Russian, German, Hindi and Punjabi, suggest that the earliest humans spoke a Proto-Indo-European language.

> It wasn't easy to survive during the First Age. The total human population was around 200,000 and the threat of extinction was quite real.

### 3. The Second Age is marked by the development of agriculture and cities. 

Our days as hunter-gatherers ended around ten thousand years ago, when the Second Age began.

This lifestyle shift was made possible by the invention of agriculture, which is the first key technology of the Second Age. Subsequently, the formation of cities and towns — where people could settle while they worked the land — became possible.

This marked a considerable change from the hunter-gatherer lifestyle, which was nomadic and required people to travel vast distances in order to find food. Since fertile farmland requires a steady supply of water, early cities were often located near a freshwater source. In addition to homes, the settlements would usually expand to include markets and temples. The early city of Jericho, for example, formed alongside the river Jordan in modern day Palestine.

Another important innovation that was born out of cities and agriculture was the division of labor. This was made possible once people joined cooperative communities, as they could now specialize in one or two specific tasks with the common goals of efficiency and economic prosperity.

In modern day economics lingo, it's considered one of the very rare "free lunches," which means it allows people to work less yet still increase the society's overall wealth. Imagine you're responsible for everything you need to stay clothed, dry and fed: You must sew your own clothes, make your own tools, build shelter and get water and food. In his famous 1958 essay "I, Pencil," economist Leonard Read describes a scenario wherein, even if a single person doesn't have all the skills it takes to manufacture a pencil, hundreds of people can work together to bring the pencil into the world. Indeed, this is the beauty of the division of labor.

There were, however, downsides: People became fiercely territorial about their land. After the advent of private property and the accumulation of wealth, inequality wasn't far behind. Before long, it wasn't just property and farmland that were privately owned, but livestock and stored grain as well.

This eventually created what is known as an _owning class_. As wealth was passed down from one generation to the next, it led to the emergence of aristocracy and royalty. For a long time after this, there were two types of people in the world: those who ruled, and their subjects.

### 4. We’re currently in the Third Age, and the Fourth Age is on its way. 

The Third Age is now upon us. It all started around five thousand years ago when three new inventions came about: writing, wheels and money.

Exactly where writing began is a topic of some debate. There are those who believe the honor belongs to the Sumerians, a civilization located in modern day Iraq. Others believe it was simultaneously developed by civilizations located in modern day Egypt and China.

No matter where the first letter was written, the fact remains that it changed the course of human history by making it possible to record knowledge and disseminate it around the world. This shared knowledge was a catalyst for new ideas and allowed laws to be enacted.

Alongside writing came the wheel, another transformative innovation that helped spread information, travel and commerce across the globe. Then there's money, made possible in the Third Age by the development of metallurgy. This involved mixing tin and copper to make bronze. Together, the wheel, money and writing were just the ingredients humanity needed to create expansive empires and nation-states. With laws, money and better ground transportation, empires could gain and control widespread populations.

Thus, the Third Age saw the rise of the first mega-civilizations, particularly in China, Mesopotamia and Central America.

We can now fast forward five thousand years or so to the present and to the last big invention of the Third Age: the computer.

From a philosophical standpoint, some believe the computer to be hugely important because it's a computation machine and, so the theory goes, computation is at the heart of every mystery in life and the universe. Whether it's consciousness, how the brain works, or how space and time function, some believe it all comes down to data processing. This brings us to the development of the ultimate computer and the rise of artificial intelligence, which, as we'll see in the next blink, could very well bring us into the Fourth Age.

### 5. The two main types of artificial intelligence are narrow AI and general AI. 

We could define all artificial intelligence (AI) as a technology that reacts to its environment or the data it receives. But not all artificial intelligence is built the same. More precisely, there is a difference between _narrow AI_ and _general AI_.

_Narrow AI_, also referred to as _weak AI_, is the only AI we currently know how to create. It propels everything from your Roomba vacuum cleaner to Apple's Siri or a self-driving car. There are three ways narrow AI can work. The first is known as _classic AI_, since it was seen as the best method when AI was first developed.

Let's say we want to develop an AI machine to tell farmers the best time to plant their seeds. Under the classic AI model, this would involve a machine that takes into account every variable, such as soil type, weather conditions and crop variety, then weighs all the options and makes a suggestion.

The second type of narrow AI is called an _expert system_. This machine would take into consideration the data from a hundred of the best farmers, including all the rules they use to grow the best crops. The farmer could then enter her own set of unique variables, which would be weighed against the expert rules in order to recommend the best option.

The third variation of narrow AI is called _machine learning_. This would require the farmer to consistently give the machine all the data about everything she's planted and what the conditions and results have been. Over time, the machine would use this data to build rules that would result in the maximum yield.

Recently, the greatest advances have been made in the category of machine learning, thanks to the availability of large amounts of data and powerful computer algorithms.

Finally, we have _General AI_, also known as _strong AI_ or _artificial general intelligence_ (AGI). General AI is potentially more versatile, adaptable and smarter than a human being. An AGI machine could face a completely new set of circumstances and know precisely how to respond.

This might sound like some frightening sci-fi scenario, but at the moment this kind of technology doesn't exist, nor is there any consensus on whether it's even possible. So for now, we'll stick to narrow AI and the implications it may have for the near future.

### 6. Our understanding of the implications of the Fourth Age depends on key philosophical debates. 

Seeing how revolutionary the changes were following inventions like the wheel and the printing press, it's safe to assume that Fourth Age technology will have a similar impact. But before we can determine how significant these changes surrounding AI machinery will be, we need to ask ourselves some fundamental philosophical questions. The first is whether we understand the universe in terms of _monism_ or _dualism_.

_Monism_ is sometimes referred to as materialism or physicalism. It suggests that since _everything_ in the universe is composed of atoms, everything is subject to the same set of physical laws. Even our dreams, feelings and identities are the result of molecular reactions and synapses in the brain. Under monism, everything can be broken down into the same basic building blocks, whether it's an iPhone, a turnip or a human being.

_Dualism_, on the other hand, sees the universe as composed of two interacting materials: physical and mental. According to noted dualist Plato, there is a world of ideas that contains things the physical world doesn't, such as a perfect circle. As the theory goes, a truly perfect circle will never exist in the physical world, but such a thing can exist in the realm of thoughts and imagination.

The second key question to ask yourself is whether you believe homo sapiens to be machines, animals or humans. Those who take the mechanistic view see people as driven by chemical reactions, and the mind and body as sustained by fuel in the form of food. According to this view, we're machines, no different than a computer.

Those who take the animalistic view see people inhabiting a biological world that is separate from machines. While machines can take on lifelike qualities, they're fundamentally lifeless and lack the mysterious force that animates humans and other animals.

Finally, those who take a humanistic view see consciousness and the soul as the major difference separating man from machine. As the Dalai Lama said, "Humans are not machines. We are something more. We have feeling and experience."

As we'll see in the next blink, these philosophical questions are essential to the debate around AI and robots.

### 7. There are three possible scenarios illustrating how robots will impact our jobs. 

With these three philosophical positions in mind, let's take a look at some predictions about how robots and AI might affect our world -- especially in relation to our jobs.

According to the first school of thought on the matter, based on mechanistic and monistic theory, AI will leave us with no jobs at all. If we're no different than machines, and if we believe we're made of entirely the same material as everything else in the universe, it stands to reason that a machine could do everything a human does. In fact, according to this line of thought, machines will undoubtedly become so sophisticated that they'll do everything better than humans, whether it's painting, writing a TV show or being president. In this scenario, if there are any jobs available to humans at all, it will likely be for sympathetic or nostalgic reasons.

The second school of thought, associated with animalism and dualism theory, suggests that not all but _most_ of our jobs will be taken by robots and AI. Since animals are fundamentally different than machines, there will be certain human jobs that machines can't do. And since dualism accounts for a mental world that machines can't access, this area will always be untouchable by machines and a safe haven for humans.

Nevertheless, in this view, a far greater amount of jobs will be taken over by AI than will be created, especially in areas like retail, service and delivery. Even professional jobs requiring education, like doctors, paralegals and accountants, will be handled by AI. The only remaining jobs for humans will be ones that require emotional sensitivity and social skills.

This brings us to the third and final school of thought, rooted in humanism and dualism, which suggests that we'll still have plenty of jobs in a world of artificial intelligence. If humans are indeed different from both animals _and_ machines, then logic would have it that we'll always be more capable of doing certain jobs than robots. In this scenario, _more_ jobs will be created as automation takes over many of the old ones, and the focus of these new jobs will be on tasks that are specific to human sensitivities.

All of these scenarios are possible. However, as we'll see in the final blink, there are reasons to be hopeful.

### 8. In the end, there are no clear answers for what life in the Fourth Age will look like. 

Unfortunately, there are no guarantees about what the future has in store. But it's important for us all to understand the different biases and perspectives that are shaping conversations about the Fourth Age.

While some may be fearful about the future, there are also plenty of reasons to be hopeful. For starters, the conditions that bring about wars are decreasing, which suggests that large-scale wars could eventually be a thing of the past.

The world is more connected than ever, with global computer networks, interdependent trade agreements and a financial system that is dependent upon a healthy global economy. All of this discourages war since it would be tantamount to the death of the economy. These days, leaders prefer to fight their battles with money, rather than might.

And despite some recent trends, there is still less nationalism worldwide than there once was. What's more, with increased access to information from around the world, it's more difficult for governments to monopolize information and control a narrative through propaganda.

In the Fourth Age, poverty and hunger could also be things of the past. While today too many people still suffer from poverty, that number has greatly decreased in recent years. In 1980, half of the people in the world lived on less than $2 a day. By 1990, that number had fallen to 35 percent, and over the next five years it dropped by half, so that by 1995 it stood at 17.5 percent. Today, 12 percent of the world lives in poverty, and there's little reason to think that the number won't continue falling.

Even though the average per capita income globally is around $30 a day, there are a billion people who get by on just $2 a day. However, as history has shown, technology multiplies human labor and can bring about a continual increase in productivity and prosperity.

While there is still plenty of wealth disparity, as innovation continues apace, the billion who reside at the bottom income level could very well gain the tools they need to rise out of poverty and tap into the wealth of the modern world. AI could make automated doctors, teachers and technicians accessible to anyone with a smartphone.

So there are plenty of reasons to be optimistic about the new world that lies ahead.

### 9. Final summary 

The key message in these blinks:

**We stand at the brink of a new era, one that will be determined by the development of technologies such as robots and artificial intelligence. As we consider what lies ahead, it's important to be aware of the different beliefs and biases that shape the conversations and predictions about the kind of impact this technology will have. The bottom line is, no one knows for certain what the future holds, but there are reasons to be optimistic about it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Future of Work_** **by Darrell M. West**

_The Future of Work_ (2018) offers keen insights about what to expect when automation and artificial intelligence change the face of the global workforce. Author Darrell M. West gathers a wealth of expert opinions to provide a thorough look at the challenges we'll face when the industrial economy is replaced by a digital one.
---

### Byron Reese

Byron Reese is the publisher and CEO of technology research company Gigaom. He also founded several high-tech companies, and is interested in the interplay between technology and human history. He's the author of the book _Infinite Progress: How the Internet and Technology Will End Ignorance, Disease, Poverty, Hunger, and War._

