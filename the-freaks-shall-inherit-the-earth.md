---
id: 561c3dc23635640007c30000
slug: the-freaks-shall-inherit-the-earth-en
published_date: 2015-10-16T00:00:00.000+00:00
author: Chris Brogan
title: The Freaks Shall Inherit the Earth
subtitle: Entrepreneurship For Weirdos, Misfits and World Dominators
main_color: B1A265
text_color: 736941
---

# The Freaks Shall Inherit the Earth

_Entrepreneurship For Weirdos, Misfits and World Dominators_

**Chris Brogan**

In _The Freaks Shall Inherit the Earth_ (2014), you'll come face to face with today's "freaks," the new generation of way-out-of-the-box thinkers who are transforming modern business culture. Entrepreneurs who are a little crazy, who aren't afraid to be freaky, can show you how you too can build a successful business by channeling your inner freak.

---
### 1. What’s in it for me? Discover your inner “freak” and channel it to achieve business success. 

Who wants to be called a freak, a weirdo or a misfit? These are labels we often shy away from. Instead, being "normal" is preferred, blending in with the pack so as to not stand out.

Why is this?  What if being a freak isn't seen as a negative, but a positive; something to embrace and even champion? What if we're all freaks on the inside? And importantly, how can being a freak actually help you succeed in business?

Being a freak isn't about trying on wacky clothes or hair colors, but about choosing your own path in your thoughts and in your life — truly living "outside the box."

Steve Jobs went through a period where he only ate apples, a quirk that led him toward the seed of one of the most successful computer companies in history. Tony Hawk did nothing but skateboard as a teen, his obsession leading him to international business success.

Do you want to find your inner freak? These blinks will help you. Once you do, you'll be well on your way to creating something freakishly good!

In these blinks you'll discover

  * how street graffiti became the foundation of a successful fashion brand;

  * how happy people in Okinawa "chose" their families; and

  * why Thomas Edison invented 1,000 dim bulbs, and one light bulb.

### 2. Embrace your inner freakishness and do business the way you want to. 

Have you ever felt out of place at school or work? If so, don't sweat it. Being a _freak_ can be a good thing!

What's a freak, exactly? Freaks are people who think differently than others. They don't just want to conform and survive. They want to _live_. 

Being a freak isn't about looking a certain way. In business, freaks are people who leave a career to pursue a dream, even if that dream seems like a long shot. Freaks want to build their success upon their individuality — so of course they don't play by the rules!

Fortunately for freaks, the business world has seen enormous changes in recent years. It's now possible for freaks to dominate.

Previously if you wanted to make it in the business world, you had to stay in line and do as you were told. But as the internet and globalization have changed the way we do business, it's much easier now for people to embrace their inner freakishness. Today, you can build a business the way _you_ want to. 

Marie Forleo did this when she left a job on the New York Stock Exchange to work in magazines. Not too long afterward, however, she read an article about life coaches and was inspired, and immediately started a training course to become one. She's now a life coach and earns a seven-figure salary! 

Societal attitudes about alternate career paths such as Forleo's have changed, and they're still changing today. It's now more and more acceptable to start a career in a field you've never worked in previously. 

R. J. Diaz, for example, was a construction worker before he decided to make his own line of bags and other goods. His friends were initially skeptical; yet in a short time his business took off.

### 3. Define what success means for you, then work toward it. Cultivate self-discipline. 

You don't want to end up being one of those people who has great ideas but never turns them into reality. Let's look at some strategies for acting on your inspirations. 

The first thing you need to do is outline your definition of success. Once you have a clear understanding of what you want, you can start working toward it.

Ask yourself: What do you want out of life? Maybe you want to work from home and spend more time with your family. Maybe you want to get promoted; maybe you want to travel the world. Set clear goals. Goals tell you where you're going!

It's important for freaks to stay goal-oriented. Freaks often have great ideas but struggle with making a living, as they don't understand how business works. You can't sell your ideas if you don't have a basic understanding of finance, marketing, or even sales! 

Mark Ecko, the designer of _Ecko Unltd_, promoted his ideas cleverly when he was first starting out. He airbrushed clothing with graffiti, and painted people's nails. He stayed devoted to his art and often tailored his work to the interests of his customers' communities, which helped him build a loyal customer base.

But achieving your goals isn't just about understanding business concepts, it's also about self-discipline. You might be motivated to go to the gym occasionally, but motivation alone won't push you to work out three times a week, every week. That's where self-discipline comes in. 

Self-discipline is what guides you to achieve your goals. You can't write a book if you're sitting in front of the television! You need to learn the self-discipline to sit at your desk and write.

### 4. Have plans? Write them down. Create a schedule and organize what you do, every day. 

Have you ever made a five-year plan? Or even a one-year plan? Many people think about it, but end up procrastinating. Here are some strategies to get you to start planning your life today. 

Start out by making a weekly schedule. You want to feel like you've achieved something when you look back at the week, right? This means you need a clear plan for what you want to achieve!

So organize your time. Each night, write out what you want to do the next day and start right when you wake up. Written plans motivate you to do more, so you can feel accomplished and then relax once the day's work is done!

Once you adjust to your daily routine, you can start upgrading. Develop a system for achieving bigger and bigger goals. 

If you want to be fluent in Spanish within two years, for example, set that as your highest goal. Then work toward your big goal by completing smaller goals, such as learning vocabulary or grammar rules.

Developing a system for achieving your goals is also the key to building a functional business. A business will fail if it doesn't have an organized structure. So develop tools for keeping your business together, like publishing a newsletter. 

Distributing a regular newsletter to employees is a good way to keep everyone on the same page. Newsletters allow you to spread information quickly and make sure everyone is working toward the company's goals. 

Even the biggest freaks need plans and organizational systems to succeed. A great idea isn't enough on its own — you need a good strategy to bring it to life.

### 5. Learn the necessary skills of your trade, but recognize that you don’t need to know everything. 

Business skills are important, but it's not possible for one person to have mastered them all. You can't know _everything_ about how a business works, so start with the most important concepts. 

There are certain skills you simply _must_ have when starting a business. A lot of business freaks falter here. You won't succeed if you can't grasp the reality and importance of earning a profit.

You also need some basic finance and legal knowledge, even if you're uncomfortable with the idea of doing math or negotiating contracts. 

It's normal to be a bit overwhelmed at first, but don't give up! Design a system: aim to learn one important skill per month, and you'll eventually have everything you need. 

Remember that you don't need to know it all, however. If a concept isn't critical to your business, consider skipping it and seeking help from someone who understands it better. 

The author uses this advice himself when counselling people on how to start their own websites. He explains that you don't need to know everything about CSS programming, for example, as you can always hire a professional web designer, or even use WordPress templates instead. 

When you outsource something you don't know well, like web design, you then have more time to focus on your website's content and other aspects of your business. It would be a waste of time to spend several months developing programming skills! 

Remember too that it's not the end of the world if you don't understand every aspect of your business. Don't ever stop learning, though. Even though you'll never know everything, you should always work to acquire new knowledge.

> _"The unknowable is an opportunity."_

### 6. Learn to love challenges as a learning opportunity. Quitting can be constructive, too. 

Problems are a natural part of life. You'll face problems as you build your business, just like you do in your personal life. If you can overcome personal problems, you can overcome business problems too!

What matters most is how you deal with these problems. Instead of just giving up, change your mind-set by learning to enjoy challenges because of the opportunities they present. 

Every new challenge is a chance to improve. Obstacles can make you a better person, so learn to love them!

If you're running low on money, for example, look for alternative ways to gain credit. If the bank doesn't believe in you, find people who do. Seek out investors who share your vision.

Naturally, you won't always succeed, but that's okay. Most people are afraid of failure, but every successful product in the market came about through a series of failures. As long as you don't make the same mistakes over and over, it's possible to fail in a constructive way. 

That's what Thomas Edison meant when he said he never failed when trying to invent the lightbulb. He just discovered hundreds of ways to make light bulbs that didn't work!

Don't be afraid of quitting, either. We tend to think of quitting as a sign of weakness, but sometimes it's the best thing you can do. Don't stick to an idea if it isn't working out. Move on to another freakish idea instead!

The author wanted to become a fiction writer when he was a child. He pursued his dream into his early adulthood, but his career simply never took off. So he let go of this particular dream to cultivate his goal of becoming a nonfiction writer instead!

### 7. Every successful freak has her own community. Cultivate networks to support your new business. 

So you've got a great idea for a product or service. How do you build a business around it? 

The key is finding the right community of people. Communities are particularly important for freaks; even if you don't fit into mainstream society, there are still people out there who work and think like you.

Freaks often feel isolated, but really, a freak is never alone. Even if your passion is collecting Star Wars figurines or building Dr. Who replicas out of rice grains, you can always find someone who shares a similar interest. 

Finding fellow freaks is an important part of developing your business. Think of them as a tribe in which everyone works together and helps each other. 

Once you've found your tribe, build a community. It'll take a lot of effort, but it's worth it! Networking and connecting with other freaks is a crucial part of making a business flourish.

People in Okinawa, Japan, understand this well. Okinawa is widely considered to have one of the happiest populations in the world, largely because of the region's concept of _monchu_, which means "the family you choose." 

Once you've found your "family," you remain committed to helping them find success, and they help you in return. 

If you write a book that the people in your monchu enjoy, for example, they'll tell their friends and family about it. Give your monchu something meaningful, and they help you in return. This sort of networking partnership is very positive when you start building your business. 

So you're ready to find your tribe, but how? Read on to find out.

### 8. Use media to find your tribe and get your story out. Think about which channel is right for you. 

If you have a strong idea, chances are that other people have the same or similar ideas, too. Your competitors will certainly try to hone in on the same market niche. 

How can you set yourself apart from the rest? In a word, media. 

Use the media to tell your story. Good media is like a campfire that your tribe can gather around to share ideas and experiences. You can reach out to people interested in your work through social media, blogs or videos.

Effective media has three key components: content, community and a marketplace. Interesting content brings a community together, then the community establishes its marketplace. 

The Ultimate Fighting Championship (UFC), a mixed martial arts league, is a good example of a business reaching out to its tribe effectively. The UFC hosts a reality show and several live fighting events; it serves a community of aspiring fighters and fans who come together in the UFC marketplace, buying UFC products. 

Different media outlets serve different goals. You need to find your own channel that's best suited to your future customers. 

So think about which methods would be best for getting your message out. Should you start a blog, or buy keywords on Google? Should you print T-shirts or hand out flyers? Think about the time and money you want to invest in promoting yourself. 

If you decide to create a blog or newsletter, for example, you'll have to decide how to write your story. One possible method is called the _mirror approach_, where you tell a personal story to which people can relate. 

If you're selling fitness equipment, for example, talk about your personal experience with it and not just about the equipment itself. This helps you build a relationship with your customers and fellow freaks. It also makes you more approachable.

### 9. Final summary 

The key message in this book:

**If you're a freak, you can conquer the business world by setting clear goals, sticking to them and finding the right people to help you along the way. So don't be afraid to be different, and be sure to embrace failure as a learning opportunity! Take action now instead of waiting for the right moment. Be brave and face the world head on!**

Actionable advice:

**Be an** ** _employeepreneur_** **.**

An "employeepreneur" is someone who always tries to help their company move forward, even if they're just a regular employee. So think like a boss: stay on the lookout for innovative solutions to problems and take on the projects that other people shy away from. That's the kind of mind-set that leads you to achieving more!

**Suggested** **further** **reading:** ** _Misfit Economy_** **by Alexa Clay and Kyra Maya Phillips**

_Misfit Economy_ (2015) sings the praises of people who break society's rules. You'll learn the strategies of such freethinkers — often working on the periphery of the mainstream economy — who succeed amid tough circumstances. Instead of shunning such "misfits," you'll instead be inspired to adopt such strategies in your own life and career.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Chris Brogan

Chris Brogan is the CEO of _Owner Media Group_, a bestselling author of eight books and a professional speaker.

© Chris Brogan: The Freaks Shall Inherit the Earth copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

