---
id: 5385ff1f6162380007600100
slug: the-future-of-the-mind-en
published_date: 2014-05-27T16:18:54.000+00:00
author: Michio Kaku
title: The Future of the Mind
subtitle: The Scientific Quest to Understand, Enhance, and Empower the Mind
main_color: 1A567E
text_color: 1A567E
---

# The Future of the Mind

_The Scientific Quest to Understand, Enhance, and Empower the Mind_

**Michio Kaku**

_The_ _Future_ _of_ _the_ _Mind_ looks at our current understanding of the human brain, as well as the varied research that is currently being conducted to expand the potential of the mind to areas which sound like science fiction, but could soon be reality.

---
### 1. What’s in it for me? Find out why telepathy and telekinesis may not be as far away as you think. 

Life holds many mysteries. No doubt, one of the most tantalizing is the vast universe around us: countless galaxies, stars and black holes hurtling through space light years away. Happily, some four hundred years ago the telescope was invented, and this allowed humanity to begin charting and understanding the vast reaches of space.

But when it comes to an equally fascinating and complex mystery — the mind — it took much longer for even rudimentary tools of enquiry to emerge. In fact, the brain imaging technologies that one might say constitute the first, crude telescopes for the mind were only invented a few decades ago.

This means that we're still only taking our very first strides in understanding the functioning of our brain. Yet even after these brief glimpses, it's clear that new neurological research and technological applications offer great promise in curing ailments as well as enhancing our already formidable brains.

In these blinks, you'll find out

  * why one half of your brain might be religious while the other is atheist;

  * how you may soon be able to upload and share your memories online; and

  * how genetic modifications have made some mice into veritable geniuses.

### 2. The human brain comprises the brains of species that came before us in evolution. 

Scientists have long been fascinated by the human brain and the precise function of each part. Before the advent of modern methods, they used fairly crude approaches to gain knowledge about the brain. For instance, they would dissect the brains of deceased people with brain damage and, based on the symptoms the patients had exhibited while alive, guess what the damaged part of the brain was responsible for.

Despite these primitive methods, scientists were eventually able to understand how the brain had evolved into its current state.

Basically, they found out that, because species on earth evolved from reptiles into mammals and then into humans, new brain structures were added on top of the old ones.

This means that our brain was built on the following three clear stages of evolution visible even today:

First, there is the _reptilian brain_, located at the back and center of the brain, and so named because it is almost identical to the brain of reptiles. This 500 million-year-old structure controls the most elementary mental functions necessary for survival, like breathing and the heartbeat, as well as basic behaviors like fighting and mating.

Second, on top of this ancient part is the _mammalian brain_ consisting of the _limbic system_ and the _cerebral cortex_, i.e., the outer layer of the brain. All mammals have developed these systems, which allow for higher-order thinking skills and more complex social interactions.

Third, our human brain is most clearly separated from other mammals due to the fact that our _prefrontal cortex_, the outer layer of the brain located directly behind our foreheads, is so big and complex. This is where rational thought is processed and grandiose human plans for the future are made. It is like the CEO of the brain.

As you can see, the human brain is, in fact, a bit like a museum of evolution, comprising remnants from the species that came before us.

### 3. The brain is split into two hemispheres that have their own functions – and even personalities. 

It's fairly common knowledge that the brain is split into a left and right hemisphere, and that each hemisphere has specific functions: in muscle control, for example, the left side of the brain controls the muscles on the right side of the body, and vice versa.

Another difference is that the left hemisphere houses the brain's language capabilities, whereas the right side contains areas responsible for spatial awareness.

What's more, the left side is better at analytically examining the details of a given situation, whereas the right side is better at intuitively and imaginatively integrating many pieces of information into a bigger picture. This is where the common idea that more rational people are "left-brained" and more artistic are "right-brained" comes from.

Though this is something of a simplification, research indicates that the two hemispheres may indeed have different personalities.

This is best seen in experiments with people whose two hemispheres have been severed — a procedure often performed on people with epilepsy. By selectively presenting information to only the left or right side of a subject's field of vision, scientists could actually communicate with only one half of the brain, asking each hemisphere questions.

For example, one neuroscientist asked a split-brain patient's left hemisphere what he would do after graduation, and the patient said he wanted to become a draftsman. However, when the right hemisphere was asked, it gave a markedly different response: "automobile racer."

Then there was a study where a split-brain patient was asked whether he was religious or not. The left hemisphere said that he was an atheist; the right side, however, claimed to be a believer!

### 4. The brain consists of billions of neurons, with specific areas responsible for specific functions. 

In the previous blink, we learned that the left hemisphere of the brain controls the right side of the body and vice versa. This is hardly a new discovery: one German doctor already figured that out in 1864 while treating soldiers with wounds to the brain; whenever he touched one side of the brain, the opposite side of the body would move.

What's more, since the 1930s, scientists have also had a fairly accurate idea of which part of the human cortex controls which part of the body. In fact, Dr Wilder Penfield, a brain surgeon, produced a remarkably accurate "brain-to-body" map by stimulating his patients' brains with an electrode and observing which muscles twitched.

Penfield's diagrams also show that more brain area is allocated to more important body parts: for example our mouths and hands are crucial for survival, so larger swathes of the cortex are devoted to controlling them than, for example, the skin on our back.

Other specific functions that have been pinpointed in the brain are the two language areas in the left hemisphere responsible for producing and understanding speech. These areas are called _Broca's area_ and _Wernicke's area_ after their discoverers, who found that damage to these areas produced certain language disabilities. For example, damage to Broca's area tends to stop the patient from articulating words, whereas damage to Wernicke's area results in the patient not being able to understand language.

By the 1990s, scientists had begun to glean an even better view of the brain, including its basic building blocks: _neurons_ which are connected in an immensely complex network. Estimates state that there are as many neurons in the average human brain as there are stars in the Milky Way galaxy — roughly 100 billion.

> _"The brain weighs only three pounds, yet it is the most complex object in the solar system."_

### 5. Technologies for brain imaging, probing and therapy have greatly advanced. 

Since the 1990s, the technologies scientists use to explore and understand the brain have taken great leaps forward.

One major area of technological development is _brain imaging_, which has allowed researchers to actually look inside the brain. Perhaps the most influential of these advancements is _functional magnetic resonance imaging_ (fMRI), where powerful magnetic fields are used to measure the flow of blood to various parts of the brain. Because neurons require more blood when they're active, increased blood flow indicates which neurons are active when.

Many other brain imaging technologies have also been discovered and are being developed, but a common trait to all of them is that there is a trade-off between _spatial_ and _temporal_ accuracy: they are either good at measuring where something happens or when something happens, but not both. Thus, none of the other technologies really represent a substantial improvement over fMRI.

Besides advances in brain imaging, there is also development in _brain probing,_ where parts of the brain are stimulated or disturbed to study the effects. One technique for this is _transcranial electromagnetic scanning_ (TES) where a magnetic pulse is directed through the skull at a particular spot on the surface of the brain, thus dampening its activity and demonstrating its role. For example, using TES on the left hemisphere of the brain can inhibit a person's ability to talk.

Another promising advance in brain probing comes from _optogenetics._ Here a light-sensitive gene is inserted directly into a neuron, resulting in that neuron firing whenever a light is shone on it. Effectively, this allows individual neurons, as well as the behaviors they result in, to be flicked on and off like a light switch.

Finally, there have also been advances in _brain therapy_ technologies like _deep brain stimulation_ (DBS). In DBS, hair-thin electrodes are inserted deep into the brain so that they can stimulate the desired area. This technique has already proved valuable in treating ailments, such as depression and Parkinson's disease, and holds promise for other brain-related problems as well.

### 6. Advances in brain imaging are enabling futuristic technologies like telepathy and telekinesis. 

In the previous blink, we looked at some of the technological advances in studying and manipulating the brain. These advances have opened the door to possible applications that may sound like science fiction.

First of all: _telepathy_. Scientists have found that individual words produce distinguishable neuronal activation patterns in the brain. This means that by monitoring a person's brain as words are read aloud to them, scientists can compile a "dictionary" that shows which neuronal patterns are related to which words.

At the moment, this application is still rather crude, but as the dictionary becomes more precise, this could, for example, allow people who are unable to speak to merely think words and have a voice synthesizer say them out loud.

What's more, a similar "dictionary" has been created for images, and even dreams: subjects are first shown hours of images to understand which images light up which parts of the brain and later, by monitoring the brain, scientists can guess what kind of images the subject sees or dreams.

Another futuristic-sounding development is _telekinesis_, or using thoughts to control objects such as computers.

The process is similar to the development of telepathy as described above. A patient's brain is observed as they perform various tasks on the computer, like moving the cursor left and right; the computer then compiles a dictionary capable of translating brain activity patterns into actions.

This technology was already tested in 2004, when it enabled paralyzed patients to communicate via laptop. But, in the future, this same technology may be used to allow humans to remotely operate robots to perform tasks too arduous or dangerous for people. It's even conceivable that, one day, a single individual could orchestrate an entire construction yard full of cranes and bulldozers and other equipment to construct a building using only the power of his or her mind.

### 7. It’s already possible to erase, record and download memories, as well as to artificially boost cognitive capacity. 

Another area of the brain's functioning that has been illuminated by advances in brain imaging technology is _memory_. In this area, too, recent progress has been very promising.

First of all, neuroscientists have found a way to _erase_ specific memories in mice using chemicals. When this method is perfected for humans, it could well be used in therapy to erase painful memories.

Second, scientists have also made their first strides in "uploading" memories onto a computer. This has been achieved by inserting electrodes into the brain of a mouse and recording the neuronal activation pattern when it learned to perform a simple task, effectively storing the memory on a computer.

Hold on though, it gets even more impressive: next, the scientists deleted the memory in the mouse's brain using chemicals, so it was no longer capable of performing the task. Then, using electrodes inserted into the mouse's brain again, the scientists stimulated the same neurons as they did before to artificially download the memory back into the mouse's brain. And, lo and behold, it worked — the mouse could once again recall the task!

The next step will be to upload a memory onto a computer from one creature and download it onto another. If this happens, it may soon be possible for people to record their memories and share them online like we do with photos today.

Third, we may soon all have super-memories: scientists working with fruit flies have found that they can give certain flies _photographic_ _memory_ through very simple genetic manipulation. Though much research still needs to be done, similar methods could theoretically boost human memory as well.

And it may not only be our memory that can be boosted, but also our general intelligence. Scientists have gotten promising results in creating "genius-mice" through genetic modifications.

Clearly, with many fascinating avenues of research being pursued, there is still much potential to be unlocked in the human brain.

### 8. Scientists are working fervently to both decipher and emulate the connections of the human brain. 

Though advances in brain-imaging technology have opened the door to many applications, there is still a lot of fine-tuning to be done. As this happens, there's no doubt that our understanding of the brain will greatly increase.

One such ambitious project on this front is the _BRAIN initiative_, intended to map all the neurons and their connections in the human brain. Having and deciphering such a map would be of great help in many of the applications already discussed, but also the one we have yet to touch on: _artificial intelligence_ (AI).

Of course, some degree of AI already exists: in 1997, for instance, IBM's computer Deep Blue defeated chess grandmaster Garry Kasparov in chess.

However, in many ways, today's AI is still far from being human: for example, whereas a human will easily recognize a chair even if seeing that particular chair for the first time, a computer will probably not decipher its "chairness," seeing instead a jumble of angles and lines.

Part of the problem is that usually AI has been designed around rigid rules, like a programmable digital computer. But sophisticated brains like the human one do not function in this way: they're a network of neurons that are constantly rewiring themselves as they learn new things.

Today, many AI researchers are looking into such a _neural network_ approach to AI. At the Massachusetts Institute of Technology (MIT) Computer Science and Artificial Intelligence Laboratory, for instance, one researcher has created tiny, insect-like robots that learn only through trial and error, i.e., by scurrying around and bumping into things.

Though this approach is still fairly new, it shows great promise as a future avenue toward more sophisticated AI, especially as our understanding of the human brain is also increasing and thus providing a better "role model" for AI.

### 9. To fully emulate human intelligence, robots should have values, emotions and self-awareness. 

In the previous blink, it was mentioned that current AI technology falls short of human intelligence in certain ways. Indeed, in order for AI to truly be human-like, it will need to emulate many fundamentally human traits.

For one, it would need to be able to properly behave in the extremely wide range of situations we encounter in everyday life. This requires a _value_ _system_ : robots with AI would need to be told how everything in life is ranked in importance, like an ethical guideline. This is, in effect, what we humans do, too. But while it may take us a lifetime to refine our ethical system, the poor robot would probably need to have its own system completely figured out before leaving the factory due to safety concerns.

What's more, to truly react appropriately to humans, robots with AI would also need to have some capacity for _emotions_, as they are a fundamental part of the human experience. In fact, scientists from the University of Hertfordshire have already made some headway here by building the robot _Nao_, which is capable of interpreting and displaying a wide range of human emotions from fear and sadness to pride and happiness.

Finally, robots with AI must also be capable of _self-awareness,_ for they need to make decisions about future courses of action, and this demands considering their own role in those scenarios as an independent actor. Happily, scientists have already made progress here, too. One team at Yale University created the robot Nico, capable of recognizing itself in a mirror — the first sign of basic self-awareness.

With so many promising strides being taken toward genuine artificial intelligence, it looks like we can one day expect the world to be full of robots who will act as our helpers, and possibly even our friends and companions.

### 10. Final Summary 

The key message in this book:

**Advances** **in** **brain** **imaging** **technology** **are** **opening** **or** **have** **opened** **the** **door** **to** **many** **futuristic-sounding** **technologies,** **like** **controlling** **computers** **with** **thoughts,** **artificial** **memory** **boosts** **and** **downloading** **memories.**

Thought experiment:

**What** **kind** **of** **technologies** **and** **advancements** **would** **you** **feel** **comfortable** **with** **when** **it** **comes** **to** **your** **brain?**

Many of the possibilities mentioned in these blinks are tantalizing but might also arouse some apprehension. After all, most people feel that the brain is pretty much the epitome of who we are. As a thought experiment, think about what kind of technologies you would happily adopt. For instance, would you happily accept intelligence-boosting gene therapy or would you worry about it changing "who you are"? Or would you accept electrodes to be surgically inserted into your brain if it meant you could directly experience the memories and emotions of your friends and loved ones?
---

### Michio Kaku

Michio Kaku is a professor of theoretical physics at the City College of New York and the author of a number of best-selling books, such as _Physics of the Future_, which is also available in blinks.

