---
id: 56c060244f33cc000700001b
slug: the-future-of-the-professions-en
published_date: 2016-02-16T00:00:00.000+00:00
author: Richard Susskind and Daniel Susskind
title: The Future of the Professions
subtitle: How Technology Will Transform the Work of Human Experts
main_color: 207532
text_color: 207532
---

# The Future of the Professions

_How Technology Will Transform the Work of Human Experts_

**Richard Susskind and Daniel Susskind**

_The Future of the Professions_ (2015) examines how modern technology and the internet have revolutionized our society. These blinks in particular address how technology has changed the way society views the work of experts, the so-called professionals. The role of such experts is evolving quickly; here you'll discover just what the future of professions will look like.

---
### 1. What’s in it for me? Understand how technology is changing the role of professional experts. 

What do you think: With advanced technology, have traditional professional positions such as lawyers, auditors and bookkeepers become obsolete?

Perhaps you rely on a tax advisor, as you couldn't imagine filing your taxes on your own. Or perhaps you're the type who instead, with a few Google searches, is changing the oil on your car, diagnosing a friend's persistent cough and managing the books of your own personal business.

These scenarios give rise to a debate that is relevant in today's internet-based society. Where exactly is the line between what we need experts to do and what we can do ourselves? Perhaps more importantly, where _should_ that line be — or should it exist at all?

Drawing on years of research, these blinks introduce you to the changes currently taking place in the professional world. You'll see why these changes are necessary, and what it means for our future. 

In these blinks, you'll discover

  * how a data-crunching computer handily beat two Jeopardy super-contestants; 

  * why if technology threatens a professional, he's doing "professional" wrong; and

  * why tomorrow's automated world will have more jobs, not less.

### 2. Professionals are granted autonomy over a particular field in exchange for trained expertise. 

How skilled are you at doing your own taxes, diagnosing your own illnesses or educating your children? 

These kinds of skills are certainly complicated; no person can manage all of life's challenges completely independently. People need guidance on topics that they may not know much or even anything about. 

This is why we turn to others who are more knowledgeable and skilled than we are in particular fields of expertise: _professionals_. 

Professionals educate our children, help run our businesses, fight for our legal rights and care for us when we're sick or injured. We trust professionals because of their knowledge, expertise, experience and character. 

Professionals don't simply memorize knowledge from textbooks and journals; they also know how to tailor that knowledge to each person with whom they work. They stay current with developments in their field, and aim to maintain a high standard of quality in their practice. 

Importantly, we trust professionals, expecting them to be honest and selfless, keeping our best interests at heart. In turn, society in general grants professionals the autonomy to manage their fields of expertise.

We can think of professions like clubs. Members decide who gets in, and set the standards for training, examinations and required credentials. Professionals often also manage their own schools, universities and institutes.

For example, if you want to be a credentialed teacher, you'll have to attend either a certified school or special classes, and eventually pass a state examination.

Some professions also often maintain full legal authority over certain fields, such as health. This is why only a licensed doctor can prescribe certain medicines, those that could be dangerous if not administered correctly. In short, we need professionals to keep us safe.

### 3. Expert knowledge is now online and easily accessible, yet professionals aren’t as open-minded. 

If you need to seek the advice of a lawyer, do you worry first about the cost? It's not an unreasonable concern. Many people simply can't afford the services of a professional. 

Many countries as a result find it difficult to maintain a high level of quality in professional services, as funds are often lacking to support schools and hospitals, for example. 

The financial crisis and resulting public funding cuts have further exacerbated this situation. Today few people can truly afford access to professional expertise — whether tax advisors, bookkeepers or consultants. 

Yet the internet has created new ways for the average person to access and share knowledge and information. Previously, only a professional was privy to high-level information relating to a specific field. For example detailed medical information was accessible only to students or doctors in academic libraries or professional facilities.

Now nearly anyone can diagnose a cough or learn how to file taxes online. Online communities, video tutorials and even the rise of online universities have opened the floodgates of knowledge for countless people around the globe. 

Professionals, however, tend to resist changes that dilute the exclusive power of their chosen professional fields. 

By definition, a professional knows more about her field than the person to whom she's offering services. Often, an expert might unconsciously (or even consciously!) try to keep an intellectual distance from a client to maintain her profession's exclusivity, discouraging self-help or self-discovery. She may even use complicated jargon in an attempt to justify high fees. 

When professionals are disingenuous, however, they fail to hold up their end of the professional bargain. So here's the question: should the public trust professionals to reform themselves?

> _"The professional man, as it is said, does not work in order to be paid; he is paid in order that he may work." — T. H. Marshall, 1939_

### 4. Advanced technology will drive automatization and innovation within professions. 

The last time you felt ill, how long did you wait before you saw a doctor? Plenty of people put off seeking the help of a professional. Yet soon, such concerns might not be a problem anymore. 

Thanks to technology, we're entering into an age of easier access to professional services. 

Technology makes professional work more efficient by automating routine tasks. Automatization can take care of manual or administrative work, allowing a professional to focus on more complex or rewarding tasks. 

_Teleprofessionalism_ allows a patient to have a video consultation with a doctor, for example, to offer medical advice when the doctor or patient can't be there in person. An instance of _telesurgery_ enabled a team of surgeons in the United States to "remove" the gallbladder of a patient some 4,700 miles away in France!

Advanced technology can assume other aspects of professional work, through the creation of protocols, standardized documents or online services. This should result in enhanced productivity and faster turnaround and delivery for services. 

Automation will also become more important as certain tasks become too complex to be carried out by professionals alone. There are now over 13,000 known diseases, 6,000 drugs and 4,000 medical procedures — far too much information for one professional to know and master.

Technology also makes professional expertise more readily available to the layman, threatening certain professional positions. In 2014, for example, 48 million US citizens prepared their taxes with online software instead of with the help of a tax professional. 

Increased internet access now allows expert knowledge to reach people no matter where they are. The Khan Academy, for example, offers educational videos online for free. In 2014, it registered 10 million visitors per month!

> Ebay's online dispute resolution system resolves three times more disputes in one year than there are legal complaints filed with courts in the entire United States.

### 5. The business of tax filing has been revolutionized through technology, much to professionals’ chagrin. 

Few people enjoy the process of filing taxes. Each year, laws change and it becomes harder and harder for the average citizen to keep up with new, ever more complicated regulations. 

In the United States alone, tax regulations are updated an average of once per day. Each year, it's estimated that some 6.1 billion hours are spent filing taxes. That's equivalent to three million people working full time. 

Individuals, as well as small and big companies, already have technological solutions for filing taxes. In the United Kingdom, for example, small businesses that previously depended on accountants for monitoring their cash flow now use online software. The software keeps track of a company's finances and updates tax documents as the year progresses, so that the return is set to go once the fiscal year has ended. 

Tax advisor Deloitte, for example, distilled the knowledge of some 250 tax specialists into one digitized system. This system is now used by companies all over the world. 

Tax specialists also use technology to organize accounting work, predicting tax that is due and making changes to accounts to minimize the tax that needs to be paid. Technology too can use its data-crunching power to offer advice on managing cash flow, determining the pros and cons of mergers and acquisitions and even helping to figure out the best location for corporate headquarters. 

Deloitte has even created a mobile app geared for expat professionals to offer advice on where to travel with the goal of minimizing the professional's tax burden. 

The same set of laws and regulations that defines tax accounting is also applicable to general business accounting, so in the near future, we'll see specialized systems assuming more accounting work as well.

> _Only about 1 percent of tax preparation work is estimated to be safe from computerization._

### 6. Increasingly complex machines will take over more and more areas of professional work. 

Modern technological systems are powerful, thanks the growth in computational power over the years. This has led to groundbreaking advances in the crunching of enormous data sets, a process known as _big data_. 

Big data techniques can identify and predict patterns in very large data sets. This sort of processing power is highly valuable in today's data-rich world. 

With the growth of video and digital images, society now generates as much data every two days as was generated from the beginning of time up until 2003. And from these gigantic data sets, we can extrapolate interesting, useful things about society and the world. 

Google Flu Trends, for instance, was a project that analyzed Google searches to help track and potentially predict the spread of the flu on a global level. By comparing current search terms to search data from past flu outbreaks, the project sought to predict which areas of the world might be likely to experience a flu outbreak at a particular time. 

Through harnessing the power of big data, this type of digital research could potentially be more efficient in predicting disease outbreaks than simply counting people with flu symptoms once they visit a doctor. 

Supercomputers with extraordinary computing power can even perform increasingly human-like tasks. IBM built a computer, called Watson that can play the television trivia game Jeopardy.

To play Jeopardy requires a wide range of general knowledge, as well as an understanding of human speech. In a live TV game against the two best candidates in Jeopardy's history, Watson won by a landslide!

Now Watson helps diagnose cancer and develops treatment plans, partly by comparing a patient's symptoms to a database of over 10 million patient health records.

Watson also helps conduct research, as it can read and process information much faster than can any human. Considering that the medical field publishes a new professional paper every 41 seconds, no doctor can possibly keep up with such a mass of information — but Watson can.

### 7. Knowledge spreads naturally and becomes more valuable as it reaches more people. 

You don't lose what you know when you share your knowledge with someone else. Unlike physical goods, knowledge is _nonrival_, meaning its value doesn't decrease when it's shared. 

In fact, the opposite occurs. Knowledge actually _grows_ when it's shared. 

A teacher, for instance, improves with each school year. The more a teacher practices sharing her knowledge and engaging her students, the better she gets at conveying information and making it stick. 

After teaching for several years, a teacher might even distill the insight she's gained into a book about pedagogy and the psychology of learning, passing her knowledge along even further. 

Knowledge is also _non-excludable_, meaning it's not possible to prevent someone from using it. When a doctor advises a patient on how to deal with an illness, the patient can then pass that knowledge on to whomever they choose. 

The shift from a print-based to internet-based information society has also further facilitated the creation, access and spread of knowledge. 

Information from any field of expertise can now be digitized and stored on a computer. In 2000, only 20 percent of the world's information was stored digitally. Today, that figure is around 98 percent. 

Technology also shapes the way we create and share knowledge. With the internet and using social media platforms, a single person can send a message to thousands of people with just a few clicks. Thus professionals in turn can get their work into the world much more easily, if they choose to do so.

### 8. Standardization in work will decrease costs and make expertise more readily available and reliable. 

As technology progresses, more and more tasks will be performed by non-specialists with the assistance of digitized processes and systems. 

Computer-aided design (CAD) software, for instance, enables hobby architects to design more detailed, complex projects. Resulting designs can be more easily shared and reused than can traditional hand-drawn plans. Digitization also helps to cut costs.

_Standardization_ helps to prevent errors and encourages the reuse of templates and information, by establishing and utilizing norms and protocols, thus making the process more efficient each time. 

Standardization is also important because the more information we accumulate, the more challenging it becomes for a professional to stay on top of it all. It's here where standardized routines come into play, as they ensure certain processes are carried out correctly, allowing a professional to focus on more intellectually straining or unique tasks.

Such tools will become more versatile as they're shared more frequently online. New users can and will improve on them, making tools more valuable and widely applicable. 

Yet the advent of expert knowledge becoming a common online good raises questions about the kind of society we are building and want to be part of. 

If we distill the knowledge of hundreds of specialists into a single system and make that system readily available to anyone, who is liable if the system fails? Who is responsible for maintaining it and building new software? Who owns the intellectual property rights to it?

Making more expertise available online could potentially solve a lot of problems. Our decisions on how accessible that information should be will shape our future society.

### 9. As technology advances, traditional professions will change and new jobs will be created. 

The traditional role of a professional as gatekeeper to exclusive information in a certain field is long over. So what does the future hold?

We should first remember that as a society, we shouldn't be afraid of new technology. Instead focusing on the potential of technology should be our common goal.

Many people are afraid that technology will "dehumanize" society, damaging personal interactions. While this is theoretically possible, the opposite is equally possible. The social media platform Twitter, for instance, actually brought journalists and their readers closer together. 

When professionals feel threatened by technology, they are in essence misunderstanding their role. Their reason for being shouldn't be to maintain their privileged status, but to help solve society's problems by providing even more access to their expert knowledge. 

Contrary to the fears of many professionals, technology won't lead to a decrease in the number of professional jobs. 

Economists once falsely thought that a society offers a fixed quantity of reasonably paid work. If that were the case, machines would be harmful to our jobs and well-being. The opposite is true, however. 

While technology has partly automated traditional ways of working, it has also given rise to completely new forms of employment. Process analysts, for instance, are important in allocating tasks between humans and machines. 

Professionals need to be flexible, adapting to new technology as it becomes available. Technology can dissolve the boundaries between professions, bringing people together. Thus professionals need to be prepared to go with the flow to ensure they stay relevant in our modern world. Technology is a tool to make knowledge more accessible; it helps us all.

> _"The professions must remain a means to an end, and not an end in themselves."_

### 10. Final summary 

The key message in this book:

**Society is on the brink of a major change when it comes to the concept of professionalism. Thanks to technology, expert knowledge, now digitized and disseminated online, is far more readily available to the layperson. That doesn't render professionals obsolete, it just means their roles are changing. Professional expertise will always be important, as today's mass of knowledge cannot be mastered by a single individual alone; technology is the tool that will help us all get ahead.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Richard Susskind and Daniel Susskind

Richard Susskind holds a chair on the Advisory Board of the Oxford Internet Institute and is the president of the Society for Computers and Law. An international speaker and expert advisor on information technology and the law, he is also the author of _The End of Lawyers? Rethinking the Nature of Legal Services._

Daniel Susskind is Richard Susskind's son and a lecturer in Economics at Oxford University. He has worked for the British Government, in the Prime Minister's Strategy Unit and as Senior Policy Advisor at the Cabinet Office.

