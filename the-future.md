---
id: 537c5d063465300007000000
slug: the-future-en
published_date: 2014-05-20T08:05:57.000+00:00
author: Al Gore
title: The Future
subtitle: The Six Drivers of Global Change
main_color: 2A9CD2
text_color: 1F7299
---

# The Future

_The Six Drivers of Global Change_

**Al Gore**

_The Future_ is a look at six key forces that will shape the future of humanity, and why they not only hold promise, but also danger.

---
### 1. What’s in it for me? Understand why democracies are not that democratic anymore. 

Someone once said that it's hard to make predictions, especially about the future. Today this is truer than ever, as this increasingly complex and interconnected world is changing at an unprecedented pace.

But out of this blizzard of moving pieces it is possible to identify six larger themes that will no doubt play a big part in shaping the future of humanity. If you want to prepare for the future, you need some kind of road map, and in these blinks you'll find the makings of one.

In the following blinks, you'll discover:

  * why, whoever you are, you may soon find yourself out of a job,

  * why the internet can spur revolutions as well as quell them, and 

  * why the rising global population may mean we'll soon run out of _everything._

### 2. Outsourcing and robosourcing are resulting in lost jobs and increasing inequality. 

The first major theme to be examined is the new _global_ _economy_.

Thanks to increasing free trade and advancements in communication and transportation technology, the simple truth is that the world as a whole is rapidly becoming a single integrated economy: _Earth_ _Inc._

This development has two major impacts for workers.

First, jobs are being increasingly _outsourced_ to developing countries, where labor costs are cheaper. While this has improved the economic health of those developing countries and grown their middle class, it is also increasing unemployment in the developed world.

Second, jobs are also being lost to _robosourcing_, when machines and computers take over jobs formerly performed by people. And these machines are increasingly autonomous, so there are no new jobs to be had in supervising them either.

In fact, even service jobs — where one might have considered human employees essential — are being robosourced. For example, bank tellers and travel agents are already being replaced, and soon professional drivers may also face the same fate, given that self-driving cars are being developed.

It is true that outsourcing and robosourcing have created _some_ new jobs as well — for example, in the financial industry, and in fact these new jobs pay very well. But as there's not enough of them to go around, the high pay merely contributes to the growing gap between the rich and the poor.

As the middle class disappears, there will be fewer affluent customers to buy goods, and this in turn will be bad for the economy as a whole. There are indications of this trend in developed countries already, and eventually the now soaring middle class in developing countries will face the same fate.

The only solution is to redistribute the wealth of the richest few to compensate for the lost income of the people who become unemployed. Unfortunately, so far the rich have been resistant to the idea, and have used their political sway to prevent any government action.

### 3. Global power is increasingly shifting to corporations and away from the United States. 

As you might have guessed, the rise of Earth Inc. has not been bad for everyone: in this new dynamic, private corporations are becoming increasingly powerful economic entities. In fact, more than half of the world's largest 100 economies are now corporations.

With this economic power corporations have gained political power too.

Why?

In many Western democracies, especially the United States, politicians depend on corporations to fund their campaigns. Elections can be won or lost based on who invests the most into expensive television advertising. This means corporate backers become essential, and politicians then feel indebted to those backers and will feel pressure to push their political agendas.

What's more, corporations also invest in advertising campaigns that can sway public opinion on their side too.

The result is of course that the interests of private citizens are constantly being trampled in favor of those of corporations. For example, consider that 90 percent of Americans would like to see genetically modified food products clearly labeled, but the agribusiness lobby has still managed to prevent this from actually happening.

The fact that the United States is pandering to corporate interests in this way is the likely cause of another major global shift in power occurring at the moment: one that takes influence away from the United States.

For a long time, America played an important role in maintaining peace and stability throughout the world. But now, its reputation as a corporate lackey is undermining the United States' moral standing and credibility. Sadly, this is depriving the world of an important and responsible leader at a time when it sorely needs one.

At the same time, China is emerging as a new superpower to rival the United States. Indeed, China is expected to become the biggest economy on the planet within the next decade.

In this arena too the solution is clear: the democratic governments of the world need to limit the ability of private corporations to affect policy, and this can only realistically happen if citizens actively call for it.

### 4. The internet is helping us improve our lives and encourages democracy. 

Of course, one of the biggest global shifts that has occurred and is occurring is the rise of the internet. People today are connected to huge global network. In 2012, internet users numbered 2.4 billion, and as connectivity spreads to every corner of the world, we can expect virtually everyone to be online soon.

As the internet expands, it is also being connected to many new devices and systems, ranging from refrigerators in your home to packages sent in the mail. The sheer size of this network is producing vast amounts of data — so called _Big_ _Data_ — and it presents a fantastic opportunity to improve many facets of our lives.

For example, the pace at which people top up their mobile phone accounts can be used to predict unemployment — the more worried they are about job insecurity, the less money they put on their phones. Also, spikes in online searches for terms like "flu" can be used to predict the spread of diseases, as sufferers head online to find relief for their symptoms.

But perhaps the greatest benefit of the internet lies in the way it can allow citizens to influence governments. Just as the Print Revolution that occurred in fifteenth century Europe allowed new ideas and knowledge to be spread, thus spurring people to challenge feudalism, so can the internet help spur democratic debate in today's dictatorial regimes.

Just by looking at recent revolutionary political movements like the Tahrir Square protesters in Egypt or the Occupy Wall Street movement in the United States, it is clear how powerful a tool the internet can be for citizens to organize themselves and give resonance to their collective voice.

In the United States, the internet can also help curb the rising political power of corporations, by providing political candidates with a cheaper platform to present themselves than television, thus decreasing their need for corporate funding.

At the same time, journalism is also migrating online, away from the influence of corporations, to better reach people and spur healthy democratic debate.

### 5. Criminals and autocratic governments can also use the internet for nefarious purposes. 

The previous blink outlined the benefits of the internet, but clearly there are also drawbacks.

First of all, as anyone with an internet connection can attest, it is easy to focus on the entertainment value of online content, remaining blissfully oblivious to serious political issues.

But more importantly, there is an inherent danger in the fact that everything we do online, be it browsing websites, sending emails, uploading photos or making purchases, is recorded. Companies can use all this data to create very precise profiles of individuals and then target advertisements at them.

What's more, though the internet can spark political reform, it can also be used to stifle it. As an example, consider that during Iran's Green Revolution in 2009, the government was able to track digital communications to identify and hunt down dissenters.

Though this may seem like an extreme case, one must remember that Western democracies too have claimed that in order to better protect their citizens, they need a greater mandate to conduct surveillance of them online. It is easy to see that this is a slippery slope that could endanger liberty itself if governments gain the ability to monitor the very thoughts and ideas of their citizens.

Of course, there's also the danger of _cybercrime_. After all, anything that is connected to the internet can also be hacked into for criminal purposes like identity theft. This includes your personal accounts, the systems of businesses, as well as those of government agencies.

Consider that the internet security company Norton estimated the annual global cost of cybercrime to be a staggering $388 billion. In the case of government agencies, hacking could even be used as a way of waging war, with catastrophic results.

All these threats make it clear that while we take advantage of the many benefits of the internet, we must also pass new laws to ensure the protection of online information, while simultaneously guarding our privacy.

### 6. Biotechnology in agriculture and medicine holds much promise, but also many threats. 

Another theme that is likely to have an impact on every area of our lives is _biotechnology_.

In agriculture, for example, food products made from _genetically_ _modified_ _organisms_ or GMOs have spread far and wide. These are crops like corn or tomatoes that have had new DNA spliced into them in order to give them desirable traits like resistance to pests.

GMOs have in fact become very widespread: for example, in 2011 eleven percent of the world's farmland was planted with genetically modified crops.

Clearly there are dangers and issues related to this practice. Perhaps the most obvious of which is that it is hard to predict the long-term ecological impact of manipulating the genes of organisms and then releasing them into the wild.

This risk is further emphasized by the fact that genetically modified _animals_ are just around the corner: in 2012 US scientists applied for approval to introduce genetically modified salmon for human consumption.

Another risk arises from the fact that GM crops are genetically uniform by design, so if a suitable pest or disease were to arise, it could wipe out huge swathes of crops at once, endangering the world's food supply.

Even more worrisome, the GM industry is dominated by a few large players like the massive Monsanto. The political sway these behemoths hold makes one wonder whether GMOs are being regulated as closely as they should be.

Medicine is another prominent domain of biotechnology. Advances in genetics can, for example, help doctors diagnose patients more precisely and craft more effective personalized treatments to match genetic backgrounds.

What's more, many devastating diseases like cancer, diabetes and Alzheimer's may soon be eradicated as new medical methods like gene-based therapies are developed.

But if genes can cure diseases, could they also "cure" undesirable behavioral patterns, such as propensity for violence? Or can they make people more intelligent or attractive?

Questions like these highlight the serious ethical issues medical biotechnology brings to light. Clearly the only way to resolve them is through healthy, open democratic debate.

### 7. Runaway population growth is putting immense strain on our natural resources. 

As citizens of Planet Earth today, one issue we simply must confront is the rapid population growth that's underway.

Consider that the current global population stands at ca. seven billion people, and is expected increase by two billion in the next quarter century. The total population is expected to stabilize at just above ten billion near the end of the century.

The bad news is that even at current population size we're beginning to see serious negative effects.

For example, since most of the population growth is happening in developing countries, people are increasingly migrating to developed ones, and this is spurring a resurgence of racist and xenophobic attitudes.

At the same time, people all over the world are flocking from the countryside to cities, and this hyperurbanization is putting tremendous pressure on municipal governments to provide adequate housing, sanitation, etc., to these incomers. If development continues along these lines, the number of people living in slums in projected to double over the next few decades.

But even these concerns are far overshadowed by the environmental impact of the rising global population. After all, there is a limit to the amount of natural resources we have at our disposal, and as global consumption skyrockets, we are likely to hit those limits.

These key resources include things like topsoil and freshwater, on which our very survival depends, but it seems we are also bound to hit the limits of many other commodities too, like iron, coal, corn, soybeans and palm oil. In fact, over the past decade, their market prices have soared due to increased demand. As one observer noted soberingly, "we may soon reach 'peak everything.'"

### 8. Technology alone cannot save us; we must encourage policies to limit the consumption of resources. 

As discussed in the previous blink, population growth is placing a tremendous strain on our natural resources.

But not everyone is worried about this.

Some _techno-optimists_ point to the fact that the depletion of natural resources has been a worry countless times before, but new technologies have always come to the rescue and allowed us to do more with less, thus stopping us from hitting the limit. For example, advancements in material sciences and 3D printing could help us produce and distribute goods far more efficiently than before, thus reducing demand for commodities.

_Techno-pessimists,_ on the other hand, believe that even new technologies won't be enough to make up for the current unsustainable rate of consumption, especially when it comes to the most indispensable ones: topsoil for farming and freshwater for drinking. For example, while there are advances in technologies for turning seawater into freshwater, they are still not cost-effective on a large scale, and should not be relied upon.

Thus it seems that despite technological advances, we simply cannot keep mindlessly consuming as much as we are now, and the long-term solution must come from elsewhere.

But where?

At the moment, a big part of the problem is that when countries make political decisions, they're not even taking into account the depletion of natural resources. This is because their sights are set solely on maximizing their _Gross_ _Domestic_ _Product_ _(GDP)_, as this is considered to be a good measure of economic success.

But in fact this is absurd, since it completely ignores the future cost of recklessly depleted resources and an utterly polluted planet. It encourages short-sightedness and recklessness, and also ignores social ills like increasing inequality.

Clearly a good way to move forward is to redefine GDP so it includes environmental goods as well as economic ones.

### 9. The climate crisis threatens our food and freshwater sources, and renewable energy sources are the only solution. 

Of all the issues the world will face in the future, perhaps the most all-encompassing is the _climate_ _crisis_.

The climate crisis is caused by CO2 and other greenhouse gases accumulating in the earth's atmosphere and thereby raising planetary temperatures at an alarming rate: nine of the ten hottest ever recorded years since 1880 have occurred in the past decade.

If this development continues, we can expect severe negative effects on our food and freshwater sources:

The amount of arable land will decrease as water evaporates from soil more quickly and dust storms become more commonplace. What's more, the increased temperature will melt polar ice caps and raise sea levels, thereby flooding coastal farmlands with salt water.

At the same time, pests and diseases will proliferate in the hot climate, further endangering the crops on the little arable land that remains.

Simultaneously, freshwater supplies will be depleted by the uneven rain patterns caused by global warming, and many aquifers will be contaminated by rising seawaters, thus leaving us not only hungry, but thirsty too.

Happily, it is still possible to reverse this development, but only if drastic measures are taken: this late in the game, the only solution is to cut greenhouse gas emissions by _80_ _to_ _90_ _percent_.

Because the main emission culprit is the burning of fossil fuels like coal and oil, energy production that depends on them must be eliminated in favor of renewable sources.

This must be done through government policy. For example, CO2 emissions should be taxed and cap-and-trade schemes enacted to encourage polluters to cut back on their emissions. What's more, subsidies to fossil fuel companies should be eliminated and instead directed at renewable energy companies.

The biggest obstacle in this, as you might have guessed, is the massive political influence of the world's fossil fuel corporations, who are actively misinforming governments and the public about the severity of the situation. The only solution is to put these corporations in their place.

### 10. Final Summary 

The key message in this book:

**The** **six** **major** **forces** **that** **will** **shape** **the** **future** **hold** **both** **threats** **and** **opportunities.** **The** **key** **to** **mitigating** **the** **threats** **and** **leveraging** **the** **opportunities** **lies** **in** **curbing** **the** **increasing** **political** **power** **that** **corporations** **are** **wielding** **in** **the** **Western** **world,** **particularly** **in** **the** **United** **States.**
---

### Al Gore

Al Gore is the former vice president of the United States as well as a prominent environmentalist and author.

