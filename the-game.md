---
id: 568b8cc2de78c0000700004d
slug: the-game-en
published_date: 2016-01-08T00:00:00.000+00:00
author: Neil Strauss
title: The Game
subtitle: Penetrating the Secret Society of Pickup Artists
main_color: B58E41
text_color: 80642E
---

# The Game

_Penetrating the Secret Society of Pickup Artists_

**Neil Strauss**

_The Game_ (2005) gives readers an inside look into the "pickup community" frequented by men desperate to convince women to sleep with them. These blinks share the advice of a leading seduction guru, and the less than great consequences of his successes.

---
### 1. What’s in it for me? Learn the controversial techniques pick-up artists use to get laid. 

You've always been a very bright man. And a very helpful one. The nicest one, in fact. Yet, you never got laid (and you still never get laid?). And you wonder why that is. You wonder how to turn things around and seduce all those pretty women around you. Well, how about learning from people with experience? How about learning the pick-up tactics that thousands of men have used successfully for years?

These blinks provide you with (a sometimes disturbing) insight into the rise of a seduction community in the United States in the 1980s — a community the author knew well. He knew its members, its gurus and their controversial seduction methods. 

In these blinks, you'll discover

  * the problem with pickup lines;

  * how one guru managed to get paid for having sex; and

  * how another leader of the seduction community convinced women that it was a privilege for them to have sex with him.

### 2. A community for exchanging pickup advice has been growing since the 1980s. 

Everybody who went to high school can tell you that some kids are popular and others, well, just aren't. While things improve for most people as they mature, some people find themselves bound to a life of unpopularity, incapable of ever getting any action. 

Well, luckily that can all change, but it helps to understand that men have always been focused on the mechanics of seduction. That's because not everyone is born looking drop-dead handsome like James Dean, and many must rely on their wits to attract women. That means seducing women who wouldn't ordinarily consider them attractive. For instance, think of Casanova, the guy who conquered one woman's heart — and bed — after another, using nothing but pure charm. 

The first person to openly discuss modern tactics was Eric Weber, who published a book called _How to Pick Up Girls_ in 1970 — and the race was on.

The decades that followed saw the birth of an entire online community focused on one thing: helping men around the world to trade tips on becoming _pickup artists_ or _PUA_ s. This community used blogs, websites and chatrooms to exchange advice about seducing women. 

Just how much information has been passed on?

Well, when the author was new to the community it already boasted over 3,000 online posts, amounting to 2,500 written pages of seduction advice — and it's grown exponentially ever since!

But while the internet was just the beginning, the web-based seduction community would soon become a physical reality. That's because while the online community allowed people to share their experiences and advice, some gurus held more respect and admiration than others, and taught their personal methods of seduction. 

It was for this reason that the seduction community branched out from its virtual roots and entered the real world. In the early 2000s, gurus began holding workshops designed to teach men how to pick women up in the field. 

Don't have the time to attend a workshop?

No sweat, just read on to learn about the wide range of pickup tips and tricks.

> _"Women, you eventually realize, are just as bad as men — they're just better at hiding it."_

### 3. Hypnosis is a powerful seduction technique. 

As you can imagine, the seduction community, with over 30 years of growth and experience under its belt, has developed a variety of methods for attracting women. One of these techniques is hypnosis — a practice that Ross Jeffries used frequently himself and often taught to others. 

But who is Ross Jeffries?

He's essentially the founder of the pickup community — the author of countless books and seminars during the 1980s. His special technique, _Speed Seduction_, uses a combination of hypnosis and psychology. 

Here's how it works:

The basic idea is that emotions can be manipulated through gestures and word patterns. That means a man can arouse a woman through speech alone — so much so that she'll want to go to bed with him. For instance, Jeffries once demonstrated his technique for the author by asking a waitress to recall the last time she was attracted to someone and to find that emotion in her body. 

He pointed out to her how the emotion grew with time and lifted his hand to show this. His logic was that the waitress would be prompted to feel that same emotion whenever he lifted his hand. Well, it worked and Jeffries left with the waitress' number. 

But Jeffries isn't the only expert on using hypnosis to seduce women. The PUA duo Steve P. and "Rasputin" are also adept practitioners. So, while Jeffries was the _first_ person to seduce partners with hypnosis, Steve P. and Rasputin took the method to new heights by using sophisticated and creative tactics to get what they desired. 

In fact, Steve P. was eventually getting paid to sleep with women because he could convince them that his use of hypnosis would make them orgasm on command and even make their breasts bigger. 

And Rasputin?

He focused on increasing his self-confidence to the point where women were convinced it would be a privilege to go home with him.

### 4. Social dynamics can be manipulated to aid seduction. 

Hypnosis isn't for everyone and luckily for us there are plenty more methods to go around. One is the use of _social dynamics_ to transform yourself into an object of female desire. Just take a tip from the Canadian pickup artist, Mystery, who used this technique with great results. 

How?

By manipulating social situations so much that women couldn't help but fall for him. His method was actually pretty simple: _Find, Meet, Attract and Close_, or _FMAC_. 

The first step is for the PUA to _find_ a woman he is attracted to. 

Then he _meets_ her by joining the group she's in and presenting himself as an "alpha male," by, for instance, uttering a memorable line with a smile that focuses the attention of the whole group on him. 

Then, to _attract_ the woman, he ignores and _negs_ her — subtly insults her — to build her insecurity and make her desire the attention she's used to getting. Simultaneously he wins the favor of the group through jokes and entertainment, becoming so well-liked that the woman sees him as the center of attention. 

At this point, the pickup artist takes the woman away from the group, maybe under the guise of apologizing for his poor manners. He then entertains her as he did the group until she gives at least three indications that she's interested, like a squeeze of the hand or asking for his number. 

Then finally the PUA _closes_ by casually asking the woman if she would like to kiss. Either she does and the deal is sealed, or he says he's got to go and leaves with her number.

> _"Pickup is a linear process: Capture the imagination first and the heart next."_

### 5. The author learned from every guru out there before becoming one himself. 

When a guy who's spent his entire life failing with women suddenly discovers a way to easily seduce them, success can rapidly become all-consuming, and that's exactly what happened to the author: he became obsessed with mastering every tip and trick the pickup community could offer, practically forgetting that the world existed outside of his new-found passion. 

Here's what happened:

After learning about the community, he signed up for a workshop organized by Mystery. The event cost $500 for four nights full of pickup advice at various clubs throughout Los Angeles, and limo service was included. After this first taste, the author was hooked. He endeavored to meet every pickup artist he could and attend loads more seminars and workshops. 

Then, after the author had studied under every guru, he synthesized their methods to develop his personal routine. It's composed of an _opening_, a _demonstration of value_, _building an emotional connection_ and finally _building a physical connection._

More specifically, an _opening_ is designed to quickly get a woman's attention. For instance, asking her how she feels about speaking with an ex. 

A _demonstration of value_ convinces the woman that the PUA is an interesting, energetic person. For example, he might do a rune reading or magic trick, something that most guys wouldn't be able to. 

Then, to _build an emotional connection_, the PUA listens to the woman and learns more about her, before moving on to _building a physical connection_.

That means getting to your place or hers, for instance, by asking for a lift home. Once you're there, it's wise to talk more until she is comfortable. From there you can get closer, for example, by complimenting her scent and sniffing at her neck. 

But before you go hog wild with these strategies, keep reading to uncover what other knowledge the pickup community has to share.

### 6. The author’s attempt to create a shared PUA living space resulted in failure. 

So, men had shared their pickup expertise online and in intensive workshops, so the next logical step was to build a more permanent pickup community, and that's how _Project Hollywood_ was born. It was an attempt by the author and Mystery to pick up girls without actually picking them up. 

It didn't pan out as expected.

The author had recently had a birthday celebration where every woman in sight had practically thrown herself at him. He realized that the real goal was to develop a lifestyle that made women come to him without him doing any of the work. 

So, he and Mystery, who were good friends at this point, reached out to two other PUAs named Papa and Herbal, both of whom joined the project. They founded Project Hollywood, a shared villa in Los Angeles where pickup artists would live together. The idea was that women would come to the house of their own volition because of its position as a lifestyle hub. 

Unfortunately, Project Hollywood was a total failure because of the clashing egos involved. The _idea_ behind Project Hollywood was a good one, even inspiring similar projects in San Francisco, Perth and Sydney, but the participants soon found that having more than one alpha male around — much less a house full of them — was a recipe for disaster. 

As a result, Project Hollywood rapidly degenerated into a disgusting frat house that actually scared women off instead of drawing them in. Nobody followed the house rules, like asking approval for guest stays longer than two months and not hooking up with another person's date. 

Instead, Papa was packing as many people as he could fit into his room, Herbal started dating Mystery's girlfriend, Katya, which caused Mystery to move out, and the author himself even left when he discovered that Papa had been turning houseguests against him to cut out his competition.

> _"The less you appear to be trying, the better you do."_

### 7. The seduction community paradoxically became more about men than women. 

One might assume that a group of men united by their determination to seduce women would be focused on the female sex. However, the pickup community was built entirely around men and their egos. 

After all, men who need seminars to attract women aren't exactly what you'd call "the social type." So, while the tricks of their trade helped them bring home women, they didn't adapt their social circle to include more females. In fact, exactly the opposite was true. 

For instance, while Papa joined Project Hollywood intending to build a harem of women in his bedroom, he ended up sharing the space with five men. The author himself had to face the fact that the pickup lifestyle meant spending most of his time with horny men rather than members of the opposite sex. 

A further problem lay in the fact that practically every man in the community was striving to be an alpha male — every one of them vying for recognition as head guru and for followers to inflate their egos. Each guru worked hard to promote his technique as the only true method, and this battle was directly tied to the fact that the pickup community had become a lucrative business. 

As a result, the community became much darker as time went on. So, while in the beginning it was about picking up women and doing so with class, things slowly changed. Many people got sucked so far into the lifestyle that they were dropping out of school and quitting their jobs to focus fulltime on the community. 

Not just that, but violence and disrespect toward women started to become a norm. That meant pickups were more about getting drunk and into fights and less about seduction. A pickup artist named Jlaix began posting stories about being thrown out of clubs and blacking out from drinking. 

So, do you still want to join the pickup community? Well, if you do, don't count on it as a path to real love. Moving forward, we'll learn why seduction techniques can't deliver actual romance.

> _"The point was women; the result was men."_

### 8. Using routines to pick up women doesn’t work in the long run. 

The techniques of PUAs might seem like magic tricks for getting laid. But relying solely on these techniques is a sure path to disappointment. Like all tricks, seduction is unsustainable, and when midnight strikes don't be surprised to find your carriage turning back into a pumpkin. 

That's because every routine has its limit. So, mastering lines to build your confidence and join social groups is fine, but you run the risk of repeating yourself or another PUA — an incredibly awkward experience. 

For instance, the author once tried to enter a few groups in a club on Sunset Boulevard with pickup lines. The only problem was that every group he targeted had heard his lines already, because the masses of men taking classes at Project Hollywood had already been there. 

But this issue pales in comparison with the fact that using a routine to pick up women will never help you find love. Routines are exactly that — routines. That means they're inauthentic and people can feel it. 

Not just that, but routines rely on the assumption that all people are the same and will react identically. But what about those special somebodies who respond differently?

You actually run the risk of scaring off the love of your life by treating them just like any other mark. The author almost lost his chance with Lisa, a woman he was incredibly attracted to. He tried his routines on her, only for them to throw his sincerity into question. 

It was at this moment, after two years of picking up women and going so far as to get Britney Spears' number, that the author encountered someone who responded differently. There was only one thing to do:

To get Lisa, the author had to revert to his socially awkward, pre-pickup self. And guess what?

It worked!

### 9. Final summary 

The key message in this book:

**The 1980s witnessed the emergence of a seduction community in which men exchanged advice about how to pick up women. This community has produced a wide variety of techniques, some of which are highly effective, but using these methods might be a terrible idea in the long run.**

**Suggested further reading:** ** _Why Men Love Bitches_** **by Sherry Argov**

_Why Men Love Bitches_ (2000) debunks the myths of male-female relationships and gives you real, insider tips on how to keep the man you want. These blinks reveal why it's vital that you regain your independence, and why doing this is advantageous to both you and your relationship.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Neil Strauss

Neil Strauss is a journalist, author and pickup artist who has co-authored such bestsellers as Marilyn Manson's _The Long Hard Road Out of Hell_ and Jenna Jameson's _How to Make Love Like a Porn Star_.

