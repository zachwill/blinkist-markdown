---
id: 54c656303732390009530000
slug: the-gamification-revolution-en
published_date: 2015-01-29T00:00:00.000+00:00
author: Gabe Zichermann and Joselin Linder
title: The Gamification Revolution
subtitle: How Leaders Leverage Game Mechanics to Crush the Competition
main_color: 2052A0
text_color: 2052A0
---

# The Gamification Revolution

_How Leaders Leverage Game Mechanics to Crush the Competition_

**Gabe Zichermann and Joselin Linder**

_The Gamification Revolution_ shows us why games are the answer to many of contemporary business's major problems. From engaging customers in the age of social media, to inspiring employees to be more innovative, every business needs to add a little game play to its arsenal.

---
### 1. What’s in it for me? Learn why games are a MUST for your organization or company. 

What does the word "game" conjure up in your mind? Chess, football, maybe tag, and playing cards? A set of rules, points to be scored and opponents to challenge? Structure and mental and emotional engagement? Couldn't you use those qualities for more than a family Monopoly session?

Gamification takes winning to another level. Companies use game designs to increase customer loyalty or staff engagement, engineering game play to transform the way they do business.

_The Gamification Revolution_ explains many different kinds of games, and how they can be used not just to engage with customers, but also to invigorate new employees or strategy meetings _inside_ your organization.

After reading these blinks, you'll know

  * why the architecture industry is so obsessed with games;

  * how Nike uses games to train new employees; and

  * how to introduce gamification into your company or organization.

### 2. Games are more important than you think. 

What's your favorite game? Is it a board game or a computer game? Have you ever stopped to think about how many games there are, and how often we play them?

You're probably familiar with games like chess or Angry Birds, but what about _gamification_ games?

Gamification means using games to get people together or solve problems. It's widespread, and it can be very useful.

The annual McDonald's Monopoly game is an example of gamification. Customers are rewarded with Monopoly pieces every time they buy food or drinks. To get more obscure pieces and complete a set, they have to order bigger meals or visit more branches.

Gamification works by leveraging behavioral economics and design. Just think about all the situations where we collect _points_, where _badges_ represent achievements, _levels_ give us a rank, and _leaderboards_ and _scoreboards_ show how we compare with others.

Then there are _grand challenges_, which aim to solve specific problems. They're usually highly publicized and offer prizes to the winners. Sometimes they can lead to great discoveries or inventions. In fact, a grand challenge led to the establishment of our system of longitude in 1714.

Gamification provides great opportunities for businesses and other organizations. It can even be critical for them.

Games can help you invigorate your brand, engage with customers and raise profits — just like the McDonald's Monopoly. They can also inspire innovation and creativity _within_ your company.

The main thing that makes games essential, however, is their ubiquity. A study by TNS Global found that over 60 percent of people in the Western world regularly play computer or video games. This is even more true for young people: about 70 percent of toddlers and young kids play them!

So if you want to connect with people in our modern world, you've got to do it through means that you know will hook them in: games.

> _"Gamification presents the best tools humanity has ever invented to create and sustain engagement in people."_

### 3. Gamification lets you reach customers in new ways. 

Gamification isn't just about designing fun games. It's also about relating to your customers in new ways, and ensuring they use whatever you're putting out there.

This is the biggest advantage of gamification: it engages people. So what does that mean for a company?

Well, gamification allows you to respond directly to your customers' desires and needs. It lets you connect with them on a new level.

For example, young people today are driving less and less, as they prefer to take public transit (where they can play games on their phones!) Ford and Nissan have addressed this issue by gamifying their cars.

Ford created a virtual plant for the dashboard display that acts like a digital pet — it turns green when the driver conserves gas. Nissan lets their drivers connect to Facebook, so they can compete with each other over who's the safest and most eco-friendly driver. These strategies allow Ford and Nissan to engage with young people, especially those who don't want to drive for environmental reasons.

Games let you give customers what they want, so they provide for you in return. The rapper Chamillionaire did this by creating a gamified community called the "Chamillitary."

Fans in the Chamillitary compete to be Chamillionaire's number one fan. They can collect points to redeem for merchandise, special events or things they can show off to their friends, like a personal voice mail from Chamillionaire.

The Chamillitary is essentially a loyalty program where you buy something then get points that encourage you to buy something else. It has a more important function for Chamillionaire, however: it allows him to release music and merchandise, and reach out to his fans without having to rely on a record label's marketing department. He and his fans use games to help each other.

> _"Give the customers what they want...as long as they do something for me." Chamillionaire_

### 4. Games can help people develop strategies for real life situations. 

So how can gamification make a difference _inside_ your company?

The first step is to gamify your strategy. Games can engage your employees just like they engage your customers.

Games can help you think about what might happen in the future, or "_end game._ "_Alternative Reality Games_ (ARG) are great for this. ARGs encourage participants to anticipate future problems or situations.

One ARG, for instance, was a 32-day simulation that explored what would happen if there was no oil for 32 weeks (each day represented a week). 18,000 people participated. In the simulation, gas rose to $7 per gallon, two million people lost jobs, and cities suffered major damage from riots and migrants.

Games can also help you plot for future eventualities, as in _scenario planning_.

In scenario planning, people go through a step-by-step process outlining what the other side would do in certain situations. It allows you to assess possible outcomes, and it's especially useful for negotiations. In fact, since 2007, every US Army officer has taken game-based training for negotiation.

Through scenario planning, the bond company PIMCO was able to predict that Lehman Brothers would collapse. Standard quantitative models didn't predict it, but PIMCO's models did.

You can also use _gamestorming_, which is gamified brainstorming. It fosters more enthusiasm and engagement.

Use the _3-12-2_ game for gamestorming. First, you introduce a problem, and players have three minutes to write down aspects of the problem on index cards.

Next, assemble the players in pairs, each of which gets 12 minutes to go through the index cards and brainstorm solutions.

Finally, each pair gets two minutes to pitch their idea.

The 3-12-2 game inspires players to be creative and work together under clear time constraints — to the benefit of your organization.

### 5. Use games to motivate your staff. 

Games come in many different shapes and sizes, and we've seen how they help people work. If you gamify your employees' experiences, you can motivate them and improve their performance.

Even dull or dreary tasks can be spiced up with games. Nike tech reps, for example, go through a nine day camp where they _live_ the history of Nike. It's called "Ekin," which is "Nike" backward.

Ekin participants run on the original tracks the Nike founders used, and they hold the waffle rubber shoe soles that were Nike's first product. This increases the camaraderie between staff members, and teaches them the company's history. It also lets them interact with their own products in a meaningful way.

The real goal of Ekin is to make the employees excited about the brand, so they can excite the consumers in turn. In the end, they transfer their enthusiasm and joy of running to the customers.

Target also motivated their employees by creating a "checkout game" for cashiers who were bored by their monotonous work. They made a simple system that rates how fast the cashiers are checking people out, and told them to aim for 82 percent. The screen notifies the cashier if they're swiping too slowly, or at the desired rate.

The purpose of the game is to give the cashiers a sense of control as they do their repetitive task. The low-stakes competitive element also gives them a feeling of accomplishment.

This is especially important because of _agency_ — the feeling that you control your own destiny. The World Health Organization has reported that a lack of agency is one of the most common stressors at work.

Instead of creating flashy graphics or rewards, Target offered its employees instant feedback and a goal to reach. It motivates cashiers, and helps them work better without having an annoying manager peering over their shoulder.

### 6. Games can trigger innovation. 

Nothing works better than games when it comes to innovation. But how?

There are three main ways to gamify corporate innovation.

First, you can use _marketplaces_ and _competitions_ to generate feedback or ideas. The United Kingdom's Department for Work and Pensions (DWP), for example, has "Idea Street"_–_ a game that's like a stock market. Employees suggest ideas, which work like stocks. They use virtual currency to "buy" or support them.

Idea Street allows the management to see what proposals the employees value most. It saved DWP $16 million in its first nine months alone, at virtually no cost to the organization.

You can also hold competitions for designs or other ideas, and award prizes to the winners. This will bring in much more creativity than coming up with ideas internally.

The architecture industry utilizes this strategy often. The designs of many global landmarks, such as London's Olympic Legacy Park and the Barcelona Museum of Contemporary Art, were submitted in competitions.

The next way to gamify corporate innovation is through _simulations_. Marketplaces seldom inspire innovation because they're too complex, but simulations can.

The IT consultancy NTT Data, for instance, has a simulation game called "Go Leadership." It tests consultants by giving them difficult challenges and hypothetical scenarios to work through.

Go Leadership led to another innovation simulation called "Go Platform." In Go Platform, specific client needs are simulated, allowing consultants to train by responding to them. You load a client into the game, then play out the possible situations you'll have to deal with — it could be a system meltdown, cash flow pressure, or something else.

Finally, just _playing_ can inspire innovation and creativity. Playtime isn't just for kids!

Playing lowers our stress, which hurts innovation and leads to burnout. It creates space in your head where you can be creative. This is why you can find Xboxes, pool tables and other games at almost every successful company in Silicon Valley these days. In fact, Google employees famously use 20 percent of their work time to work on — or play with — their own creative projects. This is how Google developed Gmail.

> _"Often it's not the most game-like businesses that have the most success with gamification but rather those that need it the most."_

### 7. Games can improve recruitment and training. 

Games can help save money and time even in the nitty gritty aspects of corporate life, like recruitment and training.

Gamified recruitment saves money and attracts ideal candidates. Google did this when they put a billboard in Silicon Valley that read, "{first 10-digit prime found in consecutive digits of e}.com." The ad didn't even mention Google.

The answer to the puzzle was a website: 74727466291.com. On the site, there was another math problem, and if you solved that one, you could apply for a job at Google.

Google didn't have to hire headhunters or spend months going through resumés when they used this game. It naturally attracted the people they wanted, and filtered out those who were unfit for the job.

Games can also speed up training and improve people's performance. Training is necessary, but it's usually costly. It can also be dull, depending on the topic and presenter.

Trainees won't benefit much if their training is uninteresting. They're much less likely to remember what they've learned.

Daiichi Sankyo, a Japanese pharmaceutical company, addressed this problem by creating a "critter-killer game" to teach its young sales force the complicated, scientific details of a new diabetes treatment — details that would probably be boring otherwise.

In the game, employees used avatars to chase pesky animals, and they were presented with facts on the new drug every time they shot an offender. After memorizing the facts, they could trade them for ammunition or points.

This was much more effective than giving stacks of boring documents to the employees, and asking them to memorize the information. Games can help both the company and its employees.

### 8. In the age of social media, gamification offers a bridge between companies and customers. 

Social media outlets like Twitter and Facebook have massively impacted our society, and they provide great opportunities for businesses to engage with customers — especially through games. Games can separate you from your competitors, and help promote your brand.

Games encourage customers to share their experiences with your product. This made the difference between Foursquare and another location check-in company that preceded it, Dodgeball. Dodgeball allowed you to check in at places and show your friends. Foursquare, however, lets its users play games with their check-ins.

Foursquare gamified its check-ins by dubbing people who check in more at particular places "mayors." Users can also earn badges. This way, people don't get tired of telling others where they are, and sharing their check-ins on Facebook or Twitter.

Some great badges include "The Last Degree Badge" for the first people to check in at the North Pole, and the "Player Please" badge for checking into a place with three or more members of the opposite sex. Users are motivated to keep using the service to get more badges.

Foursquare has enjoyed great success. Dodgeball, on the other hand, shut down after just a few years.

Games that use social media also allow you to tell your brand's story. Nike's "Nike+" is an example of this. It has a chip you can put into your shoes to measure your running. Nike+ then records the details, like distance, time and route. Users can track their accomplishments with it.

Nike+ users can also publish their stats online through social media sites. They can challenge their friends, see their work and congratulate each other.

Nike+ isn't just a fun game either — it also expresses Nike's branding goal of achieving joy through running.

### 9. Final summary 

The key message in this book:

**People are motivated by games, and games have far more to offer than a few minutes' fun. Companies need to implement gamification to keep ahead in our modern world. They can help you in every area from strategy building to recruitment and customer engagement.**

Actionable advice:

**Let your employees play.**

Games aren't just for your customers — they're equally important inside the company. There's a reason that Google keeps Xboxes around for their staff to play. When your employees can relax and play a bit, they'll not only be happier, but more creative and productive too.

**Suggested further reading: _Reality is Broken_** ** __****by Jane McGonigal**

_Reality is Broken_ explains how games work, how they influence our everyday lives and what potential they have to improve our lived reality. Full of examples of different game styles and their effects on gamers' dispositions, it not only offers a broad perspective on what games are but also shows how game designers can use them to solve some of the world's most pressing problems.
---

### Gabe Zichermann and Joselin Linder

Gabe Zichermann is the CEO of Gamification Co and Dopamine _._ He's a leading figure in the gamification industry, and has written several successful books on business, including _Gamification by Design_.

