---
id: 55d0fb2c688f670009000068
slug: the-gen-z-effect-en
published_date: 2015-08-19T00:00:00.000+00:00
author: Thomas Koulopoulus and Dan Keldsen
title: The Gen Z Effect
subtitle: The Six Forces Shaping the Future of Business
main_color: FF515A
text_color: B2393F
---

# The Gen Z Effect

_The Six Forces Shaping the Future of Business_

**Thomas Koulopoulus and Dan Keldsen**

_The Gen Z Effect_ (2014) shines a light on the changes that we face in a hyperconnected world, both as individuals and in business. By embracing the mind-set and innovations of Generation Z, we can manage these changes and unite to create a brighter future for a world that no longer divides itself into generations.

---
### 1. What’s in it for me? Be part of the technology-driven Generation Z. 

The world is moving faster and faster, technology is rapidly evolving and an ever-larger section of the population needs to catch up with the latest developments. So how can we deal with this brave new world? And how will our businesses thrive on the effects of future developments?

_The Gen Z Effect_ is what happens when technology is used to unite rather than divide generations, and so Generation Z isn't a demographic construct, but a set of generation-crossing behaviors shaped by the newest technology. You will learn about the mind-set, behaviors and forces that are Gen Z, and discover how they can affect the future of business. 

In these blinks you'll also find

  * that 15 generations could be living alongside one another by 2080;

  * that offices might soon be a thing of the past; and

  * why young tech-savvies can be a company's most valuable asset.

### 2. Generational differences have shaped our world and our workplaces. 

We had the baby boomers and Generation X. So what's next? A generation truly unlike anything that came before it: Generation Z. But before diving into what today's kids mean for our future, let's take a closer look at how generations have shaped our recent history. 

Every generation has its own character, defined by the conditions they faced growing up. While a young person coming of age in the 1940s faced the terrors of the Second World War, the following generation — the Boomers — grew up in a society that strove for peace. Some issues, such as terrorism and economic crisis, may bridge generations. 

Yet until now, _generational differences_ have created well-known divides between demographics. And it's not just a matter of you disagreeing with your parents. Research by the authors reveals that innovative thinking in companies is often hindered by the tension between old and new. 

However, workplaces that are divided by generational differences simply won't survive in the years to come. Why? Because the next generation is a different one altogether, with six driving forces that give it the potential to reach incredible success — if we can learn to adapt.

### 3. The Gen Z mindset is one of openness to change and progress. 

Generation Z. It's been described as the last real generation, but why? Well, because generational differences such as those that separated Generations X and Y may soon cease to exist altogether. 

For some, using objects made of metal, plastic and glass to order groceries or meet people is quite novel. But for members of Generation Z, they're just mundane activities that don't require a second thought. 

Generation Z is characterized by the ever-increasing use of _technology_ in everyday life. And these conditions have given rise to a new attitude that we can describe as openness to progression, and a willingness to adapt. This is what we call the Gen Z effect. So why does it matter?

Well, technology isn't the only thing that's changing our world. Life expectancy is also on the rise, which means it's predicted that by the year 2080, we'll have as many as _15_ different generational bands living alongside one another. 

Now, imagine that all 15 coexisting generations were as alienated by each other as a pre-teen might be from his grandfather today. It's a recipe for social disaster! So how can we avoid it? 

By embracing the Gen Z effect. This means using technology as a bridging force to strengthen the bonds between generations, allowing all of us to become part of Generation Z and unite to build a better future together. 

In this way, we'll need to learn not only how to become part of Gen Z as individuals, but as organizations. Let's consider the six different forces that will shape how future businesses are built — forces that we need to take into account if we want to embrace the Gen Z effect.

> _"We are all — if we care to be — builders of the future."_

### 4. A Gen Z attitude will help us cope with challenging demographic changes. 

Six different forces will influence business in the future. The first is demographic change, with its radical impacts on society and the way we do business. So what does demographic change look like?

In recent decades, mortality has largely decreased. More and more people are gaining access to clean water, food, education and medicine. At the same time, increased access to contraception means people are having fewer children. Ageing populations are emerging all over the world as a result. 

This will have big consequences for business. Today, you might have two or maybe even three generations working alongside each other. But future workplaces may feature five to seven different generations, each with varying experiences, attitudes and technological knowledge. 

Enter the Gen Z effect: a powerful solution for positive workplace dynamics. If you want your business to thrive in the future, then your employees must first stop seeing each other as different generations. But how?

Over the last decade, remote work has been introduced in many companies with great success. In fact, The Future Laboratory, a consultant for future trends, went so far as to claim that offices will soon be obsolete, and that more flexible work patterns will be introduced and embraced by workers. 

Not only does this reduce the chance of people making tiresome assumptions about their coworkers because of their age, it also has financial benefits. IBM started their remote program in 1995 and saw a 50 percent increase in productivity as well as $700 million in real estate savings. Now 39 percent of their workers work remotely.

### 5. Our world is hyperconnected and your employees should be too. 

You're probably using a tablet or smartphone to read these blinks right now. Only a few years ago, this would have been unbelievable. Today we embrace our new, ever-online world. Our state of _hyperconnection_ is force number two.

Some aspects of technology are really quite astonishing. Take our rapidly increasing interactions with devices, for example. Can you guess how many times you connect each day? It's about 243 times on average, whether it's your car's GPS, your phone or your laptop. 

Today there are around 70 billion devices connected to the internet. And, it's estimated that the number of intelligent sensors, such as tracking devices in cars, will exceed three trillion in 2020. These incredible figures reflect that we are no longer content to own just one device. Younger people may own phones, laptops and tablets, and two of each! 

It's crucial that your employees are able to thrive in this hyperconnected world. This means that technology education should be one of your organization's priorities. Many companies have already used mentoring schemes to help new employees learn from veterans that have decades of experience. 

But in light of the Gen Z effect, let's consider this pattern of learning in reverse. As older employees aren't familiar with the technological tools they need to connect with customers and coworkers, they'll benefit from learning tech skills from someone younger. 

Reverse mentoring is in fact the very tactic that the authors employed in their company, Delphi Group. In 1994, they hired a young musician to work on their IT system. He didn't have any qualifications other than knowing Macintosh well and loving cutting-edge technology. Over the next decade he was the person everybody went to with questions about technology. This became a simple and effective way to keep everyone up-to-date.

> _"There are more cellular mobile devices in use today than there are people in the world."_

### 6. Products that feature simplicity, availability and adaptability will lead the market. 

There was a time when smartphones were cutting edge and reserved for a select few. But in an astoundingly short time they became affordable and accessible. Today, owning a smartphone is the norm, and excellent example of _slingshotting technology_. Slingshotting is force number three.

Three things determine whether a tech product will slingshot, or become rapidly and widely available. First, it needs to be simple and easy to use. Secondly, it should increase the availability of the user, allowing them to be online more often. And finally, the technology must be able to collect data from the users in order to adapt to their needs. 

The iPad was another recent slingshotting success. This tablet was targeted at those who wanted to increase their availability, while also enjoying a tool that could do more than a smartphone could. 

With streamlined design featuring just one button to return to the home page, functions that allowed people to connect to their family and friends anytime, and a memory for the unique preferences and interests of the user, the iPad was able to slingshot and meet needs we didn't even know we had. 

Tools like smartphones and iPads aren't just a pleasant addition to everyday life — they can also be great for business, and boost productivity in new ways. But simply buying iPads for all your employees or upgrading to cutting-edge software will do more harm than good if nobody knows how to use them! 

Of course, no employee enjoys sitting through training sessions. So how can you make the most of new technology while keeping your workplace engaged? With a healthy dose of fun and games to help the slingshot process!

_Gamification_ is the process of using data to motivate people by adopting game mechanics to allow employees to interact with technology in a playful way. For instance, a sales company might use the data collected by their employees' devices to track their sales progress and present it in a fun way that lets them compare their numbers with coworkers. It creates a bit of friendly rivalry that's good for motivation.

> _"Gamification is about motivating people through data. That's it."_

### 7. Companies don’t just need funds to make an impact: they need a loyal community of supporters. 

Through TV, newspapers, radio, the internet or on the streets, we are bombarded with ads every day. In the last century, a memorable ad needed a big budget. But in our hyperconnective world, expensive production just won't cut it. You need force four: the shift from affluence to influence, or _loyalty_.

Say a company like Coca-Cola wants to launch a new line of products. Previously they might have taken a large chunk out of their ad funds to launch a campaign. But with the rise of the internet, consumers have countless opportunities to express their opinions. So the challenge of maintaining a good reputation is far greater today. 

In this way, social media can be a public relations curse. But it can also be a blessing. Take Dove, whose Campaign for Beauty utilized social media to spread a powerful message questioning beauty standards. This message resonated with many, leading the Dove campaign to be liked and shared virally over several platforms. 

Companies who understand social media in this way have hit the jackpot. They create an online presence that provides supporters with constant, always accessible interaction through special offers, competitions and access to exclusive information.

One project that earned their advertising success through loyal online supporters is Free the Children. This not-for-profit organization aims to improve children's education around the world. In just two decades, it gained 2.3 million members and built 650 schools and classrooms without spending a single cent on marketing. How did they do it? By being there for their supporters and earning a good reputation that spread and grew with each positive impression.

### 8. The future of education will no longer be bound by age or location. 

Wouldn't it be great to have a new skill or qualification? Many of us would love the opportunity to learn but worry that we won't have the time, or that it's far too late anyway. If that's the case, then you'd better think again! 

Force five: Education is no longer a matter of age or location. The traditional process of completing school, perhaps moving on to college or university, then ending our education when we choose a job, may soon be a thing of the past. 

Technology has given a whole new face to education. With endless possibilities for connecting, its possible for 70-year-old college students to learn a new discipline after spending their entire life in the same job — and they don't have to move to a city that offers their new course of study, either. 

Generation Z has introduced two highly influential aspects of education. The first is MOOC, which stands for Massive Open Online Course. A MOOC enables everybody to attend classes, no matter their age or location. One MOOC was led by Stanford University, with over 160,000 registrants and 23,000 completions of the course. 

These classes are largely enabled by slingshotting technology and hyperconnectivity. While higher education might still be seen as a largely upper or middle class privilege, this is rapidly changing. Courses taught by elite professors are accessible to anybody who can reach a device with an internet connection.

Another significant aspect of Gen Z education is its gamification _._ While traditional education has focused on learning through lectures, MOOCs and other modern pedagogical initiatives are designed to engage their students and stop them losing attention or missing parts of the course.

For instance, if you want to learn the guitar, you can get the game Rocksmith, which teaches you real guitar playing and only allows you to move further after you complete tasks to test your learning.

### 9. Lifehacking is Gen Z’s bold new approach to problem-solving. 

The sixth and final force that defines Gen Z is _lifehacking_. For those who haven't heard the new buzzword already, lifehacking refers to innovative and creative problem solving that saves time and money by departing from accepted methods. 

Lifehacking means using the opportunities that hyperconnectivity presents to deal with different problems. Take fundraising. A few years back, the City of Honolulu needed a new website. The first estimate they were given by web designers amounted to $9,300,300 of tax payer's money. 

When the citizens heard about this, they were outraged and decided to lifehack the problem by finding a creative solution. They set up an online competition inviting contestants to design the best website at the lowest price, and shortly after, a new website was built for only $9,300.

To lifehack the future for creative solutions, Gen Z will use three important developments. The first of these is crowdfunding for start-ups and projects. This entails raising funding contributions from a large number of people via the internet.

Another key player in lifehacking is 3D printing, a technology that enables people to produce things for themselves, independently of manufacturers. 

Finally, the rethinking of intellectual property and patents will push the limits of lifehacking. Consider Open Source innovations as an example of lifehacking. These are free apps that can be downloaded by anyone, and change the way that traditional intellectual property is handled. Before, patents were usually owned by wealthy organizations and needed to be paid for, but Open Source allows everyone to use and contribute to the newest developments. This kind of lifehacking allows simple solutions to be found without adhering to the old system of paying for patents.

### 10. Final summary 

The key message in this book:

**By embracing new technology and being open to change, everybody will be able to become part of Generation Z. If we unite under the six forces that drive the Gen Z Effect, we'll be able to create a better future for ourselves.**

Actionable advice:

**Ask for help!**

Next time you feel lost and out-of-touch with the new technology you're required to learn, instead of panicking and giving up, try to ask a younger person at your workplace if they have any know-how. This will probably both flatter your colleague and provide a simple entrance into the foreign land of new technology. 

**Suggested** **further** **reading:** ** _Millennial Money_** **by Patrick O'Shaughnessy**

Although technological advances have made it easier than ever to invest in the stock market, today's Millennials (young adults born between 1980 and 2000) tend to be risk-averse. But this kind of thinking is misguided. Given that benefits like Social Security and retirement pensions are imperiled, it has never been more important for young people to start investing in their future financial security.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Thomas Koulopoulus and Dan Keldsen

Thomas Koulopoulus is the founder of the Delphi Group. InformationWeek called him one of the most influential information management consultants and has written nine books about major shifts in social media, business and technology.

Dan Keldsen is a senior business strategist at NFP Health with more than 20 years of working experience as consultant, technologist and analyst. He co-led groundbreaking research on attitudinal differences between baby boomers and millennials and was noted as one of the Most Influential Enterprise 2.0 Writers of 2009 by SeekOmega.

