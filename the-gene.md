---
id: 58262b347029760004d5ace8
slug: the-gene-en
published_date: 2016-11-16T00:00:00.000+00:00
author: Siddhartha Mukherjee
title: The Gene
subtitle: An Intimate History
main_color: AD233E
text_color: AD233E
---

# The Gene

_An Intimate History_

**Siddhartha Mukherjee**

_The Gene_ (2016) offers an in-depth look at the history of genetics. These blinks take you on a journey from the field's humble beginnings to its modern day applications in diagnosing illnesses, debunking racist claims and creating genetically modified life.

---
### 1. What’s in it for me? Dive into the history of genetics and learn how genes made you who you are. 

With the rise of supercomputing, the field of genetics has made huge strides. We still have a lot to learn about genes and how they function, but one thing is certain — as our knowledge grows and we push deeper into ways to modify genes, such discoveries will have significant consequences for human life.

But to better understand the future, we need to turn back the clock. How did scientists learn about genes in the first place?

These blinks take you first to the early days of gene research, when questions of heredity sparked the interest of an Austrian botanist. You'll learn how early gene exploration led to the horrors of eugenics during World War II. And finally, you'll wonder at the challenges of sequencing the entire human genome and consider how society should cope with modern-day genomics and its ethical implications. 

In these blinks, you'll also discover

  * how scientists came to discover the properties of genes;

  * how Hitler distorted genetic science to justify the extermination of Jews and Gypsies; and

  * how the environment affects a person's genome.

### 2. An exploration of heredity led to the discovery of genes and how they help pass on information. 

The story of the gene begins in 1864 with Austrian botanist Gregor Johann Mendel. As part of an experiment in breeding pea plants, Mendel noticed that parent plants passed on specific traits to the next generation of pea plants _intact_ — that is, with the traits unaltered.

A tall plant, for instance, when crossed with a dwarf plant, would produce only tall offspring — not mid-size offspring, which might indicate a blending of parental traits. In pea plants, tallness is a dominant trait, which means it overrides the trait of dwarfism.

In other words, what Mendel had discovered is that hereditary information — the trait of tallness, for example — is passed down from generation to generation in indivisible units.

In identifying these indivisible units, Mendel had unveiled the smallest building block of heredity, the gene.

Some years later Dutch botanist Hugo De Vries revived Mendel's earlier work and was able to merge his ideas on genetics with Charles Darwin's theory of evolution, published when Mendel was still in school.

Darwin proposed that all animal species, rather than being a direct gift from God, had descended from earlier forms of animals through a process of slow, continuous change.

Mendel's heredity work perfectly complemented Darwin's theory. If a species evolved as Darwin suggested, it would make sense that an animal transferred physical traits to its offspring through genes, or messengers that contained genetic information.

De Vries pushed Mendel's theories further, explaining why genetic differences, or _variants_, occur in the first place. He discovered that such variants are accidental — freaks of nature, essentially — or as he called them, _mutants_.

The work of these three scientists combined to form a complete picture of species evolution. Nature produces random variations in traits that are then passed to offspring, and naturally selected over time as some offspring survive, and others die.

### 3. DNA is the building blocks of genes; and when genes work together, traits are expressed. 

The discovery of the gene answered an important question regarding heredity in a species. Yet it also sparked new questions. Researchers knew that genes existed, but did not know what a gene looked like — or how it worked on a biological level.

In the 1940s, biochemists began examining the workings of cells. In the nucleus of the cell, they discovered certain molecules — DNA (deoxyribonucleic acid) and its close relative, RNA (ribonucleic acid). These molecules were grouped as _nucleic acids_, as they were found in the cell's nucleus.

Both acids consist of four components, known as _bases_. DNA's bases include adenine (A), cytosine (C), guanine (G) and thymine (T). RNA's bases are identical, except instead of thymine, RNA has uracil (U).

With the discovery of DNA, scientists had finally found the building blocks of genes.

The traits Mendel had observed so long ago essentially live on an organism's many DNA strands. Interestingly, genes work together to express a trait in an organism. Observable traits, such as height, aren't the result of the expression of a single gene but rather many genes working together.

You can think of gene expression like the interaction of pixels on your smartphone's screen. While each pixel is independent, together they form a complete picture. Similarly, genes act independently; but when they combine, they produce a complete "picture" of an observable trait in an organism.

Yet you can't just automatically connect gene expression to visible traits, such as the size and shape of a person's nose, as external factors can influence these traits as well.

A boxer's nose, for example, isn't shaped purely based on genetics. Naturally, the boxer's environment — in this case, multiple encounters with hard-punching opponents — has also helped to influence the shape of his nose. The connection between genes and visible traits isn't so straightforward.

This hazy connection is essential to the study of genetics. As you'll discover in the next blink, this link was often ignored by those looking to abuse the authority of science.

### 4. Revelations in genetic research were abused to justify wholesale killings by Nazi Germany. 

As researchers further explored the workings of genetic material, how such discoveries could be used to "improve" humanity was taking form in a field called _eugenics_.

Supporters of eugenics felt that through science, they could purify the gene pool by encouraging people with desirable traits to reproduce, while preventing those with undesirable qualities from doing so.

Francis Galton, Charles Darwin's cousin, coined the term eugenics in 1883. He thought human attributes like intelligence, strength and beauty could be increased in society through selective breeding.

This idea led to the first court-ordered sterilization in the state of Virginia, on October 19, 1927.

Carrie Buck was ordered sterilized as she was deemed "feeble-minded" by the court. Her sentence was based on the logic that sterilization would prevent her from producing "feeble-minded" children, and thus protect the overall population from the damaging effects of undesirable genetic traits.

Not so long afterward, leaders in Nazi Germany took this concept to terrifying extremes.

One of Adolf Hitler's desires was to create a perfect, defect-free race of people. His administration would use the ideas behind eugenics to justify the elimination of "traits" that Hitler believed undesirable — and groups such as Jews, Gypsies and disabled people were targeted.

The Nazis thus distorted genetics to enact a policy of systematic sterilization. "Jewishness" and "Gypsyness" were argued to be the result of hereditary traits, although of course no science existed to support such a claim. By 1934, nearly 5,000 adults were being sterilized monthly.

From there, it wasn't long before the Nazis began wholesale exterminations of undesirable groups.

They started with the disabled. In 1939, Gerhard Kretschmar, born blind and with physical deformities, was euthanized at the request of his parents, who desired to strengthen the national gene pool. Gerhard was 11 months old.

By the end of the war, the Nazis had exterminated some 11 million people. This regime showed the world how genetics could be twisted; the field of eugenics became taboo and remained so for generations.

### 5. The nucleus of a cell is like a lending library, sharing DNA information with the rest of the body. 

How does DNA behave within a cell? One of DNA's key relationships is with proteins, as proteins act as middlemen, shuttling messages between DNA and the rest of the body's systems.

First, what's a protein? Proteins are large molecules, composed of amino acids. They perform many tasks in an organism, from aiding the process of digestion to battling harmful viruses. Your genes, which "live" on DNA strands, support such tasks by instructing cells to build proteins.

The process of building a protein requires a series of steps. To start, DNA makes a copy of itself within the nucleus of a cell. This is called _transcription._ The DNA copy is RNA; it's not an exact copy, but it is very close.

The RNA then moves outside the nucleus, and its information, or sequence of genes, is used to create a protein.

Think of this process like students borrowing ancient manuscripts from a library and copying them for other people to study from. 

DNA — the ancient manuscript — is always stored safely within a cell's nucleus. When information that the DNA holds is required by the body, that information is copied as RNA and brought out of the nucleus to be translated into proteins.

Remember that DNA is composed of four bases: adenine (A), cytosine (C), guanine (G) and thymine (T). The process of protein creation requires that bases act in sequence. No single base can carry enough information to produce a protein; a particular series of bases, however, provides a complete recipe.

In this way, bases act as a language. The letter "A" on its own conveys little meaning, but when combined with other letters, a variety of words with specific meanings can be created.

### 6. Genes regulate protein production, replicate to keep cells functioning, and recombine to create life. 

So now we're familiar with how DNA, through transcription, works to create proteins. DNA also gives _itself_ instructions to replicate constantly, yet it needs help to do so. Here's how this process works.

When a cell splits in two, each newly created cell must contain the same genetic information as the original cell. Without this information, the cell would not know how to function; it would be like an actor without a script, wandering silently on an empty stage.

So to make sure a cell has the proper script, DNA also _replicates_, or copies, itself.

Yet DNA can't replicate on its own, because if it could, it would do so all the time, causing havoc and confusion in a cell. DNA replication is thus regulated by an enzyme called DNA polymerase. Only in the presence of this enzyme is DNA able to make a copy of itself.

DNA also "knows" which proteins to synthesize and when, depending on the body's needs. For instance, when you eat something sweet, your DNA works to build proteins to help you digest sugar.

And, if the activities of genes weren't amazing enough, DNA can also recombine to create entirely new genes. In fact, the process of gene _recombination_ is the jumping-off point for species evolution.

Recombination occurs, for example, when a sperm and an egg meet to eventually create an embryo — the process of reproduction.

In reproduction, genetic information is exchanged between the maternal and paternal chromosomes of an organism. Essentially, "chunks" of genes from one chromosome swap places with "chunks" in the other — and a newly combined set of genes are formed to create a new organism.

> _"The caterpillar and the butterfly carry precisely the same genome — but gene regulation enables the metamorphosis of one into the other."_

### 7. DNA tells each cell in a body what it will become and exactly when to do so. 

Your body is made up of different types of cells that serve a specific function, such as skin cells, muscle cells, liver cells and so on. But how does each cell know exactly which function it should fulfill?

From its DNA, of course! Another of DNA's many jobs is to tell each cell what to become. Here's how this fascinating process works.

Each organism begins life as a single cell that contains its entire genetic code. When the first cell begins to divide, DNA tells each new cell which role it should play in building the new organism. This process continues as cells divide and divide again, taking on new functions.

Thus some cells become liver cells and others become skin cells, until from an initial mass of cells an embryo begins to take shape. 

An embryo evolves in three stages. First, the embryo's main axis, or its "head" and "tail," are defined. Then specific genes called mapmaker genes activate to form body parts, from left to right and from front to back. Finally, specific genes are turned on or off to form organs or other specific elements unique to the particular species.

In addition to telling cells what to become in a developing organism, DNA also tells cells _when_ to assume their roles.

In the 1970s, biologists at Cambridge University decided to map every cell in a male earthworm — all 1,031 of them. The team found that they could predict not only which role a cell would play but exactly _when_ it would do so. They found, for example, that a particular cell would divide once every 12 hours and that 60 hours later, the cell would move to the worm's nervous system.

The team also made a surprising discovery. They observed that specific cells would even disappear at certain times in the worm's development. In other words, because DNA said so, a cell knew when it was supposed to die.

### 8. Researchers in the 1970s made great strides in recombinant DNA and gene sequencing. 

In the 1970s, geneticists were looking for ways to manipulate DNA. They knew that through recombination, DNA could change — but the natural process itself is slow, requiring multiple generations.

Scientists thus decided to explore whether they could speed this "evolutionary" process up a bit. The question they asked themselves was this: Can science create new genetic combinations in a lab?

Stanford University biochemists Paul Berg and David Jackson found in 1970 that the answer to that question was "yes."

The two men successfully inserted the entire genome of a virus called SV40, together with three genes from the bacterium E. coli, into a Lambda bacteriophage. They called their new creation _recombinant DNA_. The process they developed would later become known as _gene cloning_.

Through gene cloning, the biochemists had created a new life form, one that didn't exist in the natural world.

Berg and Jackson, through their cloning experiment, proved that science could "write" new DNA to create new organisms. Yet a new question was posed: could we also "read" DNA instructions? 

We know that it's not the bases on DNA per se but rather the order in which the bases appear on a DNA strand that determines genetic information. To "read" a gene, then, we have to know its _sequence_ — the precise order in which its bases are positioned.

Reading genes is precisely what _gene sequencing_ seeks to do.

The first person to successfully sequence a _genome_, or complete set of genes, was Cambridge-based biochemist Frederick Sanger. In 1977, he mapped all 5,386 base pairs of virus Phi X174.

After this discovery, gene sequencing continued to develop, illuminating new aspects of the language of DNA.

Scientists found that in animal DNA, a string of base pairs are often separated by long stretches of what are called _stuffer bases_. These are bases that don't appear to code for anything, but act as a space or pause between useful base "sentences."

> _"With DNA, as with words, the sequence carries the meaning. Dissolve DNA into its constituent bases, and it turns into a primordial four-letter alphabet soup."_

### 9. DNA sequencing can help doctors diagnose some genetic diseases, but other illnesses are still elusive. 

So we can read DNA, but what exactly does such genetic information tell us?

A lot, in fact. DNA sequencing can help diagnose certain diseases. By reading a person's genetic information, we can see where improperly functioning genes are located. These problem points can indicate the potential for disease.

By the 1960s, for example, doctors could diagnose if a baby had a certain genetic syndrome _in utero_ ; that is, during gestation (before a child is born).

For instance, individuals with Down syndrome are born with an extra copy of chromosome 21. Since the marker for this disease is an entire chromosome, it's easy to spot when examining fetal cells.

However, other genetic diseases aren't so simple to identify. In certain cases, the cause of a disease isn't a single gene or extra chromosome.

Cancer is one disease that is difficult to pinpoint through genetic analysis. Cancer is the result of the cumulative malfunction of dozens of genes in a cell. It's also genetically diverse; if you examine two cases of breast cancer, for example, each might be caused by the mutation of entirely different genes.

For diseases such as cancer, a genetic diagnosis is only possible by reviewing a patient's complete genome. It's for this reason, among others, that the Human Genome Project began in 1990.

The goal of this project was to sequence the entire human genome — essentially drawing a map of the more than 20,000 genes that make up human DNA.

While early geneticists could never have imagined such a project, technological advances in gene sequencing had advanced to such a stage by the 1990s that a project like this was possible.

And by 2000, the project had published a first draft of a complete human genome. In 2003, the project was announced completed, with every gene mapped and the map made available online.

> If you published a book, printed in standard font, with the whole sequence of DNA bases in the human genome, the book would have more than 1.5 million pages.

### 10. The genome project has revealed our common ancestry and disproved racist genetic claims. 

Now we have access to a complete map of the human genome. But what can this information tell us?

For starters, the human genome has shown us the common ancestry of our species.

With the successful completion of the Human Genome Project, new doors opened in the field of genetic research. Scientists now could research every single human gene, and by comparing the genomes of people from around the world, trace the origins of the species.

But how does our genetic code point to where we came from?

Two closely related people share many variations in their genomes. Conversely, distantly related people also share variations, but not as many. From this simple principle, scientists have measured the degree to which people from opposite sides of the world are related.

In doing so, researchers have found that the oldest human populations are the San Tribes of South Africa, Namibia and Botswana and the Mbuti Pygmies of the Congolese Ituri forest.

Scientists have even traced human ancestry to a single woman who would likely resemble the women today of the San Tribes.

Mapping the human genome has helped scientists trace humanity's roots. Importantly, this information has also shown that racist genetic claims have zero basis in scientific truth.

Many people claim that some races are genetically "inferior" to others. The history of mankind, however, is literally too young for such a thing to be true.

Research has shown that less than 100,000 years ago, all humans lived on the African continent. Certain groups then migrated, with some tribes eventually becoming white Europeans.

So if someone says that a European is born more intelligent than an African, this statement is simply not true. Such a genetic variation would take millions of years to occur; and what's more, some 85 to 95 percent of genetic diversity in the human species is contained _within_ racial groups.

Thus a man from Namibia and a man from Ghana are actually so genetically _different_ that it doesn't even make sense to put these two individuals in the same racial category!

### 11. Genes affect a person’s sex, but not necessarily a person’s gender identity. 

There are many ways for you as an individual to identify yourself, such as in relation to nationality, religion or class.

But despite changing gender norms, the majority of people still identify as either male or female. This distinction is influenced by genetics — specifically, by one gene in particular.

Your sex, regardless of whether you're born anatomically male or female, is controlled by your twenty-third chromosome pair.

For females, the two chromosomes match perfectly and are called "XX." For males, however, one of the two chromosomes is shortened, and we call the pair "XY."

Sex determination, however, has been narrowed down to a single gene, called the SRY gene. Peter Goodfellow discovered this marker in 1989. Simply put, if you have an active SRY gene, you're more likely to be born anatomically male.

Determining gender identity, however, is a different issue. In fact, many people self-identify as somewhere between the two normative genders; some identify as neither, and some identify as both.

In this way, gender identity is non-binary — and that makes perfect genetic sense.

For instance, if your SRY gene is turned on, you'll be born anatomically male, but the SRY gene doesn't affect your gender identity in any direct way. Rather, the SRY gene acts on dozens of secondary genes that respond to environmental inputs and eventually work to determine your gender identity.

Since these genetic factors are so varied, it's no wonder that people hold such a variety of gender identities.

### 12. We’re born with genetic tendencies, but environmental cues are necessary to turn them into traits. 

It's an age-old question: what makes a person, genetics or the environment?

The nature-versus-nurture debate is a bit more nuanced than proponents of either extreme will admit. The bottom line is that people are born with tendencies and not traits; and it's only when these tendencies interact with the environment that they become visible traits.

In a 1979 study, behavioral psychologist Thomas Bouchard followed identical twins who were separated at birth and raised in entirely different environments.

While the siblings had an identical "nature," they had nowhere close to the same "nurture."

The experiment found that twins raised in different environments exhibit the same behavioral tendencies, although their _exact_ behavior is ultimately different.

In a particular case, one male twin was raised as a Nazi youth while his brother spent summers on a kibbutz. Both twins defended their beliefs rigidly and with passion, despite the belief systems being opposed.

Similarly, since tendencies need to interact with the environment to become observable, a person with a predisposition toward violence will only act violently when exposed to violence, for example in the home.

But the environment does more than just solidify tendencies. It physically etches itself into our genome, which is the subject of a field called _epigenetics_.

When a gene is activated or deactivated by an environmental cue, like a traumatic experience or captivating smell, small molecules called _methyl tags_ attach themselves to a gene. These tags act like annotations to a cell's DNA, somewhat like comments in the margin of a text.

As tags accumulate over time, they begin to influence the functions of the cell.

In a 2006 experiment, stem cell biologist Shinya Yamanaka erased epigenetic marks on a mouse's skin cells. The process caused the skin cells to transform, reverting into stem cells. Effectively, Yamanaka had sent the cells back in time.

### 13. Gene therapy and gene manipulation have some promising health applications. 

So how can scientists use advanced knowledge of genetics to improve human life?

One promising field is to find ways to cure diseases through gene therapy. Basically, scientists are exploring ways to insert genes into a sick patient to alleviate the symptoms of a disease.

OTC deficiency, for example, is caused by an improperly functioning enzyme called ornithine transcarbamylase (OTC). Such a deficiency can lead to excessive levels of ammonia in the blood, putting a person in a coma.

Yet doctors now can insert a healthy version of the malfunctioning gene that works to create OTC in a patient, effectively curing the disease.

Conceptually, gene therapy was already being practiced in the 1980s. The idea then was to use a virus to "smuggle" genes into a living host. The target gene could be placed in a virus, and then the virus could be inserted into the host, where it could enter cells and copy the gene it carried there.

Yet inserting a virus into a person is extremely dangerous. Viruses can be deadly; a patient's safety is still a huge obstacle in exploring gene therapy methods.

But gene therapy isn't the only promising application of advanced genetics. Stem cells are also full of genetic potential. These amazing cells can regenerate or be transformed into any other type of cell in the body. Scientists can use stem cells even to manipulate genes to build an organism from scratch.

The process involves extracting stem cells from a living organism, isolating the cells' DNA and then manipulating the genes. Through this, scientists can produce genetically modified animals.

For instance, scientists have created a mouse that glows under blue light. They extracted stem cells from the rodent and placed a jellyfish gene into the cells. The stem cells were then mixed with embryonic cells and inserted into the womb of a female mouse; the mouse then gave birth to an iridescent baby mouse.

### 14. Genetic manipulation seems limitless, from advanced disease diagnosis to the creation of life itself. 

With great knowledge comes tremendous power. How could genetics alter the course of humanity?

Advances in genetics will make it possible to diagnose more diseases. In fact, as genome sequencing progresses, even the most complicated illnesses could become diagnosable and even curable.

Schizophrenia is one candidate. This mental illness, in which patients hear inner voices, is difficult to diagnose. Its cause has been traced to a series of genes scattered throughout the genome. Yet as sequencing techniques improve, doctors may eventually be able to diagnose schizophrenia in utero.

As science finds ever greater ways to diagnose genetic disease, however, we have to learn how to respond to these diagnoses — an ethical matter.

Many people who suffer from mental illnesses also exhibit extraordinary creativity. In fact, the two traits are often closely related. Consider the likes of painter Vincent Van Gogh, composer Wolfgang Amadeus Mozart or author Virginia Woolf, all of whom exhibited signs of mental illness.

So it begs the question: should a parent abort a fetus that's likely to develop a mental illness if that future person could potentially live an inspiring, creative life?

Or is it even acceptable to ask this question? To do so is to question the value of a potential life based on genetic "fitness," much like eugenicists of Nazi Germany did.

And how about _creating_ life? This isn't the stuff of science fiction, as scientists are already well on their way to producing the first modified human.

The process is simple. First, human stem cells are obtained. These cells are then genetically modified before being converted into sperm and egg cells. These cells are used to produce a human embryo through _in vitro fertilization_, or reproduction in a test tube.

The result could be a human resistant to all known blood diseases — or something even more incredible.

### 15. Final summary 

The key message in this book:

**From its modest beginnings, the field of genetics has shaped the study of medicine and biology, allowing researchers to make extraordinary leaps in understanding. Yet ultimately, genetics has also shaped the way we view ourselves. The limits of genetic exploration appear boundless, and it's clear that understanding the gene is the key to understanding life itself.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Emperor of All Maladies_** **by Siddhartha Mukherjee**

_The Emperor of All Maladies_ is an informative look at the deadly disease that has affected millions. Cancer is one of the greatest challenges facing medical science today, and this book gives us a rare opportunity to learn about every aspect of it — its causes, its astonishing biological processes and our ever-changing fight against it, from the past to the present day.
---

### Siddhartha Mukherjee

Siddhartha Mukherjee is a physician, geneticist, researcher, stem cell biologist and cancer specialist. He is also the author of _The Emperor of All Maladies: A Biography of Cancer_, winner of the Guardian First Book Award and the 2011 Pulitzer Prize for general nonfiction.

