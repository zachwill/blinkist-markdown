---
id: 56143cba3632330007360000
slug: the-genius-of-dogs-en
published_date: 2015-10-09T00:00:00.000+00:00
author: Brian Hare and Vanessa Woods
title: The Genius of Dogs
subtitle: Discovering the Unique Intelligence of Man's Best Friend
main_color: CC292E
text_color: CC292E
---

# The Genius of Dogs

_Discovering the Unique Intelligence of Man's Best Friend_

**Brian Hare and Vanessa Woods**

_The Genius of Dogs_ (2013) uncovers the remarkable intelligence of man's best four-legged friend. By first examining human intelligence, the authors go on to explain exactly what makes dogs so smart, which talents they have in common with humans and other animals, and what sets them apart.

---
### 1. What’s in it for me? Discover the secret behind the genius of dogs. 

Every year, proud owners travel the world with their furry friends to compete in dog shows to display a wide range of agility and tricks. It's hard to deny that dogs have an impressive ability to learn! In fact, research has shown that few other animals can learn as well as dogs can.

So how exactly did dogs get so clever? Domestication and training has surely played a part, but as these blinks will show you, the story is more complex. 

How did wild, lone wolves become man's best friend and develop a special bond that makes them more suited to a pack of two-legged people? Read on to discover the genius of dogs. 

In these blinks, you'll learn

  * why dogs chose to domesticate themselves;

  * how dogs and toddlers share the same cognitive abilities; and

  * why the bulldog's nose is so flat and its jaws so strong.

### 2. A dog’s intelligence is grounded in its survivalist skills. Picking humans as companions was genius. 

If you've ever seen a dog perform a series of complicated tricks, you know that these four-legged creatures are clever. But just how clever are they, and how can we measure dog smarts? 

Animal intelligence is different than human intelligence, and is better measured in how well an animal has managed to survive and reproduce over time. 

Interestingly, many mammals today are in decline, endangered or teetering on the edge of extinction — but not dogs.

So how do we evaluate the genius of dogs? A genius can excel in one area of cognition, but remain average or lacking in another. While dogs may not be all-round geniuses, they can be considered specialized geniuses when compared to other closely related species. 

Another signifier of genius is the ability to make spontaneous inferences, which humans do all the time. 

For example, if you're driving toward an intersection and can't see the traffic light, yet notice a car entering the intersection from a side street, you infer that your traffic light is red. Dogs too have the ability to infer information from their surroundings, as there are few allowances for trial and error in nature.

The lasting relationship between humans and dogs throughout history is based on our shared intelligence.

Initially, people thought early humans raised wolf cubs as companions, animals that evolved into what we know today as dogs. Yet why would our ancestors take cubs away from wolves, wild animals that were intensely feared? 

Humans didn't need the help of wolves to hunt; in fact, keeping wolves as part of the human "pack" would mean we'd have to hunt more, as a wolf needs up to five kilograms of meat per day to survive. What's more, wolves are incredibly possessive of their food, and a hungry wolf is a dangerous wolf!

So the wolf cub-to-dog theory didn't make sense. How then can we explain why ancient human graves, from some 10,000 to 12,000 years ago, also included buried puppies?

### 3. Dogs domesticated themselves to eat better, and along the way, bonded closely with humans. 

So how did the wolf become the domesticated dog? Rather than humans seeking to bond with wolves, it was the wolves that chose to bond with humans.

Early human settlements offered new sources of food, such as discarded bones and rotting meat. Only the more audacious wolves could use this discovery to their advantage, and slowly, these wolves became more skilled at responding to cues in human behavior. 

Even when the wolves were chased off, the draw of food outweighed the threat of danger. Within a few generations, differences could be observed between the wilder wolves and the proto-dogs. These animals showed a preference for being around humans rather than sticking with the pack. 

In this extraordinary way, dogs essentially domesticated themselves.

Experiments with foxes in Siberia support this argument. Foxes were selected according to how friendly they were toward people, and only those foxes were allowed to breed. These selected foxes preferred toys that humans had handled, for example, whereas other foxes preferred toys untouched by humans. The breeding selection gradually resulted in foxes that instead of fearing humans, sought to interact with them.

We also know that if dogs have a choice to be with other dogs or with a person, they choose the person. The mere presence of a person causes a dog's stress levels to drop dramatically.

A dog displays an alliance with people unlike any other animal, and even behave like a human infant toward its owner. As nineteenth century writer Josh Billings said, "A dog is the only thing on earth that loves you more than he loves himself."

And even the dogs that are closest to the original proto-dogs, Australian dingoes and New Guinea singing dogs, are able to read and understand human gestures.

But really — to what extent do dogs truly _understand_ us?

### 4. Dogs watch us and learn from us, much like an infant does. Yet dogs develop much quicker. 

As humans, we aren't born with fully formed cognitive abilities. It takes time for small children to develop the skills needed to survive. Dogs, however, learn much more rapidly.

At around nine months, human babies begin to focus on what others are looking at and how others react to the environment. This ability to read intention is the cognitive basis for culture and communication. Impressively, dogs share this ability, too.

Most animals only learn specific gestures. You can train an animal to understand a pointed finger, but if you then point with your foot, the message is lost. Interestingly, studies have shown that contrary to trained wolves, dogs can comprehend human gestures even without having been trained to do so.

Experiments have also shown that dogs can read human gestures much like a human infant can. They can even do so selectively. For example, if you direct your gaze above a cup with food in it, the dog will not understand it as a signal; yet they will understand the signal if you look directly at the cup. 

In this way, dogs pay attention to what we pay attention to! 

Dogs also make similar errors as human infants do. If you hide a toy behind one object and then move it in full view behind another, both infants and dogs will first search the original hiding place. A trained wolf, however, wouldn't make the same mistake.

Yet unlike infants that take months to develop such cognitive abilities, puppies and street dogs exhibit these abilities early on. This demonstrates that such abilities are a product of evolution, not rearing.

So while dogs have much in common with human infants, what role did this play in their survival?

### 5. For dogs, survival of the fittest means survival of the friendliest. Wagging tails are more than cute! 

You're probably familiar with the concept of "survival of the fittest." But what does it mean to be fit?

Let's take two of Homo sapiens' closest relatives, the chimpanzee and the bonobo. In doing so, you'll witness two different types of "fitness" at work.

Chimpanzees are hostile toward strangers, hunt other groups and kill males and infants. In the wild, inter-group aggression is one of the leading causes of mortality. The leader of a chimpanzee group is the alpha male, asserting sexual dominance over all females in the group. For chimpanzees, being fit is to be strong, as it is the best chance for a male to reproduce.

Bonobos are a different story. Although they're nearly genetically identical to chimpanzees, they greet neighboring groups and don't kill other bonobos. The group leader is always female, and all females are bonded. If a male tries to harass a female, the females will defend her. Bonobos are also selective over mates, usually opting for the more peaceful, gentle males. 

With bonobos, being fit is being friendly. So how does this relate to the fitness of dogs?

Just like humans and bonobos, early on, dogs display spontaneous cooperation skills and seek to help others. To do so, tolerance is extremely important. From an evolutionary standpoint, being tolerant and approachable was advantageous for humans, enabling us to form larger groups and share food.

The same can be said for dogs. The dogs that bonded with humans were more cooperative and more communicative. For dogs, then, developed social skills — in particular friendliness — meant and still means being fit. 

Interestingly, both bonobos and dogs have a cranium that is 15 percent smaller than that of chimpanzees or wolves. Yet bonobos have greater intelligence, as shown by their superior ability to cooperate, than chimpanzees; dogs are more intelligent than wolves. Thus brain size is not a good indicator of intelligence!

We now know that dogs are naturally social animals. But what else are they adept at?

### 6. Dogs show understanding and can communicate, but there are limitations to their “language.” 

Plenty of dog owners attest to sharing a mutual understanding with their pet. Is this an exaggeration? Well, there's actually more truth to this claim than you might think.

So what exactly do dogs understand? A word? A concept? When we're young, and learn a word such as "shoe," we understand that "shoe" refers to a category. The same goes for our canine companions.

Experiments have shown that dogs can understand the symbols behind words. In a test, dogs were taught the words for objects, like "frisbee" or "toy," and then trained to fetch the objects from another room. Later, when the dogs were shown a picture of a frisbee instead, they were able to fetch the same object. 

Dogs also understand when their owner can or can't hear their actions, such as when Fido might drag a bit of garbage into another room to enjoy it where he can't get caught!

As clever as dogs can be, though, there are certain limitations to what they can do. 

In experiments where a direct route to a goal is obstructed, a wolf will search for and find an alternative path. A dog, in contrast, will just sit in front of the obstacle. This is likely because the survival dilemmas of many domesticated animals have been solved by humans. 

Understanding basic physical principles is another limitation that dogs exhibit. Early on, human children learn that a toy can't be passed through a solid object, for example. Dogs have a hard time with this; when a dog is tied to a tree, it will keep moving around, unaware that the tree is restricting its movement. 

Dogs also show little sense of self. Like humans, apes can use a mirror to see parts of their body they normally can't. Dogs, on the other hand, often get bored after failing to find the "other dog" behind the mirror. 

Still, when in company, dogs are undeniably remarkable.

> _"Relative to other animals, it is the ability of dogs to understand human communication that is truly remarkable."_

### 7. Dogs are social animals. They learn and act best with others, and even better when with humans. 

Dogs are pack animals. Street dogs work together, and pets stick to their human "pack."

This social way of living and learning gives dogs an advantage. When they're alone, dogs can get stuck with a problem, but when they observe others solving the problem, they learn quickly.

Dogs are extremely skilled at learning by observation. This doesn't mean they actually comprehend the reasoning behind the solution or the problem, granted, but they learn quickly from a success, even if it's by chance.

The value of social learning and living for dogs is best understood when looking at feral dogs. Feral dogs are animals which have been domesticated but not purposely bred by humans, sometimes for generations and have thus returned to a wilder existence.

Feral dog packs are similar to wolf packs in size and tend to reconcile after conflict. However, wolf packs are generally closely related, having only one "privileged" pair that breeds, with the rest caring for the pups. This is different in feral dog packs, which are unrelated and bear more pups, with more offspring perishing due to lack of help. Wolf packs guard themselves by killing other wolves, whereas dog packs defend themselves against other dogs by barking.

Another way in which dogs display sociability is how they seek out and become attached to one or more cooperative partners. Dogs can even remember an owner years later. Charles Darwin, for example, reported that upon his return from a three-year trip abroad, his dog Czar greeted him as he always did, as if he had been gone for just a moment.

Dogs can live in groups, learn from others and are proficient cooperators. They show that they know when they need to cooperate, and can identify potential cooperative allies.

### 8. There is no “best” dog breed. Curiously, a Chihuahua and a St. Bernard have lots of genes in common. 

Even if you don't own a dog, you've probably heard different opinions about what breed is best.

But dog breeds are not as different as you might think. In fact, there is no international consensus on the number of dog breeds, as different countries recognize different breeds.

Moreover, up until a few hundred years ago, dogs were classified according to function. Any chasing dog was a harrier, any lapdog a spaniel. It was only much later that appearance became the signifier of breeds.

Part of the transition from function to form is exemplified by bulldogs. In nineteenth century England, dogs were used to kill bulls, as it was believed this made the meat more tender than if the bull was slaughtered traditionally. These dogs required a strong jaw in order to not get thrown, broad noses so that they could breathe while biting, and a small build to avoid getting hit. So to accommodate all this, the "bulldog" came to be.

While there are many dog breeds, genetically there are only two groups of dogs: those closer to wolves, such as Afghans, and those classified as dogs of "European origin."

The latter group has only about 150 years of genetic distance and are in general closely related, apart from appearance. Surprisingly, a Chihuahua and a St. Bernard are genetically very similar!

Dogs can also be distinguished by personality tests. In general, there are two basic groups: dogs that are sociable and bold, and dogs that are shy. Yet all these qualities can be found in each dog breed. Aggressiveness, however, is an independent trait, and one that distinguishes the two groups.

Because of this, most research concentrates on dog aggression — crucially, as there are some 4.7 million dog bites reported in the United States every year!

Yet even if you have a dog with an aggressive personality, you can still train it. But how?

### 9. Dog training doesn’t follow behavioral concepts, but uses cognitive encounters as a guide. 

Established by Burrhus Frederic Skinner, behaviorism was a leading learning concept in the second half of the twentieth century. 

Behaviorists believe that the cognitive mechanism behind behavior is irrelevant, but that it is the ingraining of the "right" behavior that matters. This approach, however, has its limitations.

In behaviorism, learning is centered on how deprivation makes reward possible.

In his experiments, Skinner used rats and deliberately underfed them. To receive food, the rats had to perform certain activities. It was through this that Skinner molded the rats' "correct" behavior.

Though these techniques have been criticized as unethical, they have formed the basis of many learning programs for animals as well as humans, such as in weight loss and quit-smoking programs.

Behaviorism has its limitations, though, as not all animals or all humans for that matter are uniformly created. That is, we can't always predict an individual's behavior.

Focusing on cognitive processes rather than behavior makes room for different concepts of intelligence. As we now know, the intelligence of dogs lies in their ability to understand human communication and their desire to cooperate with us. 

Many dog training programs are based on the notion of a person as the "alpha" giving directives to a dog. Yet in dog packs, there is no hard and fast hierarchy.

Therefore, to develop superior animal training programs, the cognitive abilities as well as the limitations of dogs we've seen in earlier blinks should be considered. This would give us a more realistic view about what dogs can do and how they can learn to do it.

### 10. Final summary 

The key message in this book:

**What makes dogs intelligent lies in their ability to understand human communication and their willingness to cooperate with us. However, there are also limitations, and by taking this into account we can treat dogs better and train them more effectively.**

Actionable advice:

**You won't always get the dog you expected!**

You've probably heard about "aggressive" bulldogs and "intelligent" border collies. But genetically, all dogs are practically identical, so even if your dog is picture perfect on the outside, it might have a nasty temperament inside. Breed tells us little about behavior; let your interactions with a dog be your guide when choosing your new life companion.

**Suggested further reading:** ** _Zoobiquity_** **by Barbara Natterson-Horowitz and Kathryn Bowers**

_Zoobiquity_ describes the intimate similarities between humans and other animals by examining topics such as sexuality, health and psychological development. It illustrates how we, as humans, could have much to gain by increasing our understanding of the animals we share our planet with.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Brian Hare and Vanessa Woods

Brian Hare obtained his PhD from Harvard University and is an associate professor of evolutionary anthropology at Duke University. His research has been cited in articles published in _The_ _New York Times_, _TIME_, _Wired_ and _National Geographic_.

Specialising in animal intelligence and behavior, Vanessa Woods is an Australian scientist, author and award-winning journalist. She has written for _The Wall Street Journal_, _BBC Wildlife_ and _The Huffington Post_.

