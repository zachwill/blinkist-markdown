---
id: 56afa5d7377c160007000052
slug: the-genius-of-opposites-en
published_date: 2016-02-03T00:00:00.000+00:00
author: Jennifer B. Kahnweiler
title: The Genius of Opposites
subtitle: How Introverts and Extroverts Achieve Extraordinary Results Together
main_color: FD6633
text_color: 993E1F
---

# The Genius of Opposites

_How Introverts and Extroverts Achieve Extraordinary Results Together_

**Jennifer B. Kahnweiler**

_The Genius of Opposites_ (2015) sheds light on the potential for partnerships between seemingly incompatible personality types: extroverts and introverts. These blinks lay out five basic steps that can turn the most unlikely business partnerships into the most successful.

---
### 1. What’s in it for me? Learn why partnerships between opposites are so successful. 

Would you say you're an introvert or an extrovert? If you're unsure, it might be high time to find out, because, as these blinks make clear, being successful is often a matter of partnering up with your opposite.

Although it might be easier to work with someone who is just like you, such partnerships rarely lead to new insights. Working relationships between extroverts and introverts, on the other hand, often result in true innovation. 

In these blinks, you'll learn

  * why there is no John Lennon without Paul McCartney;

  * the foolproof way to tell whether you're an introvert or an extrovert; and

  * how Steve Jobs and Steve Wozniak overcame their differences and changed an entire industry.

### 2. When extroverts and introverts overcome their differences, they can achieve great things. 

What would the introverted John Lennon have done without his extroverted counterpart, Paul McCartney? As their relationship proves, opposed personality types can create wonderful things when they work together. 

You've probably heard time and time again that people are either introverts or extroverts. But can you explain the difference? It's more than just a preference for solitude or company. Rather, it's the way you _recharge your batteries_, so to speak, that makes all the difference. 

If, after spending time in a large group of people, you feel the need to curl up alone for a while, then you're probably an introvert. If you feel totally energized after hanging out with a crowd, but start to feel edgy and tired after spending too much time on your own, then you might be an extrovert. In short: introverts extract their energy from within, while extroverts absorb it from the outside world. 

It's perhaps not surprising, then, that introverts and extroverts don't always see eye to eye. While introverts might regard extroverts as shallow, extroverts might perceive introverts as snobbish or cold. And so, though many high-performing extrovert-introvert duos seem to have a great collaborative relationship, these relationships are often torn apart by underlying differences. 

Nobel Prize-winners James Watson and Francis Crick, for example, though utterly unlike one another, joined forces and made the twentieth century's greatest scientific discovery: DNA. And yet they ceased to work together following this. Why? Because they no longer had a common goal that helped them overcome their differences. 

Still, it is possible to create a great working dynamic between opposites. That's what we'll be exploring in the following blinks!

### 3. Discover your partnership by evaluating your differences and defining a common goal. 

Bringing together introverts and extroverts is like mixing oil and water. If you know the difference between them and have a few tricks up your sleeve, it is indeed possible. 

After establishing who's the extrovert and who's the introvert, it's time to assess the dynamics of your partnership. How? By comparing how each of you respond to certain statements.

These statements will be akin to those you'd find on a standard personality test. Things like: "I can tell how a person is feeling from their body language" or "I am open to working with clients whose values conflict with mine."

The degree to which these statements hold true for each of you should be rated from "never" to "almost always." The areas where your answers contrast the most are _problem areas_ that the pair of you should work on.

And how do you work on these personality clashes? By following the five steps that lead you to the genius of opposites, using the acronym ABCDE as a guide: Accept the alien; Bring on the battle; Cast the character; Destroy the dislike; and Each can't offer everything. 

We'll work our way through each of these steps in the following blinks, but before we begin there's one thing crucial to your partnership: sharing a concrete, long-term goal. 

Legendary opposites Steve Jobs (an extrovert) and Steve Wozniak (an introvert) were bound by the long-term goal of bringing computers to the public. This goal gave the partnership the common ground they needed to overcome their clashing personalities, paving the way for revolutionary collaborative work. 

So, once you've got your common goal, it'll be time to get started with the five steps mentioned above.

### 4. Get your partnership off the ground by accepting your differences and embracing conflict. 

We're always told to accept others for who they are. Seemingly simple advice. But it's not so easy to follow when your partner starts attacking your ideas or starts being passive aggressive. 

_Accepting the alien_ is a major challenge for members of collaborative partnerships. We often wish that our partner functioned more like we function, sometimes pushing them to adjust to our approach. This leads to animosity and resentment. Meanwhile, the pressure of deadlines often brings out our less fortunate traits, making it difficult to calmly resolve conflict. 

But trying to get your partner to be more like you is simply misguided. Personality types are deeply ingrained, so it's best to accept your partner for the introvert or extrovert that they are. If you and your partner have a disagreement, try to understand why they're acting or reacting the way they are, even if it's completely alien to you. If you can do that, you'll have cut your stress in half. 

After you've accepted the alien in your partner, it's time to _bring on the battle_. While having conflicts can be unpleasant, it's also the fastest path to creative problem solving. Two heads are better than one, and having another voice to criticize your assumptions is one of the best things about having a partner. 

Luxury automobile firm Chrysler, for instance, made huge financial gains during a period when it was lead by a great partnership. Opposites Robert Lutz, an extroverted auto executive, and Robert Eaton, the introverted Chrysler CEO, welcomed productive disagreement with open arms. 

Unfortunately, this all changed when Jürgen Schrempp became CEO after the Chrysler-Mercedes Benz merger. Schrempp saw himself as a better leader than Robert Eaton, who in turn kept his concerns to himself. The partnership was rocky, with Eaton ultimately leaving the company. Without accepting different personalities and welcoming conflict, partnerships simply can't flourish. 

Accepting differences and welcoming conflict are two key parts of a successful partnership. In the next blink, you'll learn about three more.

> _"Disciplining yourself to learn how and why your opposite sees the world differently is work, but the work carries rewards that far outweigh your investment."_

### 5. Strengthen your partnership with clearly defined roles, mutual respect and a team-oriented approach. 

Each person has strengths that align perfectly with certain roles in a team. What are yours? Are you the gal who loves to organize it all or the guy who knows how to keep morale high? If you don't know, it's time to find out!

The third step toward creating a brilliant partnership is _casting the character_. Every partnership benefits from clear roles and responsibilities. If nobody knows what their duties are, your partnership won't reach its full potential. 

Chinese online marketplace Alibaba has achieved massive success under a partnership with clearly defined roles. Jack Ma, Alibaba's founder, and his corporate equivalent, Jonathan Lu, have contrasting responsibilities that bring out the best in the company. 

Ma is the _visionary_ — the creative one who sees opportunities for expansion and future financial success. Lu is the _planner,_ turning these visions into reality by delving into details, structure and organization. Together they created a thriving marketplace that not only provides a platform for online auctions, but offers other services, such as a means of transferring funds. 

The next step in your partnership is _destroying the dislike_. Great partnerships need not be built upon a close friendship, but they should at least have a foundation of firm mutual respect. 

Let's return to the partnership of Jobs and Wozniak. By no means were they great friends. In fact, they were often at loggerheads. But they had mutual interests and passions and, because of this, respected each other. Because of that, they were able to radically alter the course of computer technology while also encountering new sides of their own personalities, broadening their worldviews in the process.

Now, there's just one step left that's necessary for your partnership, and it's all about staying realistic: _each can't offer everything_. We can't hope to do things all by ourselves. For all our strengths, some of our weaknesses mean we aren't equipped to solve certain problems. We need our partners, and our partners need us.

### 6. Final summary 

The key message in this book:

**The clash between introverts and extroverts isn't something to avoid in business partnerships. Rather, it's the very thing you should foster! By putting extra effort into embracing dialogue, supporting strengths and balancing out each other's weaknesses, you'll take your collaboration to its full creative and productive potential.**

**Suggested** **further** **reading:** ** _The Introverted Leader_** **by Jennifer Kahnweiler**

Kahnweiler explores the specific challenges introverts face in an extroverted business world. She then sets out to show how introverted executives can push their limits, employ their characteristic strengths and still become great leaders.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jennifer B. Kahnweiler

Jennifer B. Kahnweiler, a learning and development professional, is an expert on introverts. She has advised organizations such as General Electric and NASA. Kahnweiler has a Ph.D. in counseling and organizational development and is a Certified Speaking Professional (CSP). _The Genius of Opposites_ is her third bestselling book.

