---
id: 5a1b4328b238e10006ebb48e
slug: the-geography-of-bliss-en
published_date: 2017-11-29T00:00:00.000+00:00
author: Eric Weiner
title: The Geography of Bliss
subtitle: One Grump's Search for the Happiest Places in the World
main_color: 3B6DAC
text_color: 3B6DAC
---

# The Geography of Bliss

_One Grump's Search for the Happiest Places in the World_

**Eric Weiner**

_The Geography of Bliss_ (2008) asks which nations are the happiest on Earth, and what it is about these countries that makes their citizens so joyful. The answers to these questions reveal some fundamental truths about our many cultural differences, as well as the many similarities and contradictions we share.

---
### 1. What’s in it for me? Take a happy trip around the world in just ten blinks! 

For the founding fathers of the United States, the freedom to pursue happiness was written into the country's Declaration of Independence. But according to the latest data in happiness research, there are many other countries who are doing a far better job at keeping their citizens joyful and content with their lives.

But what can a country do to make its people happy? Should it make education and health care free, or perhaps eliminate taxes?

These are just some of the tactics we'll look at in the blinks ahead, while also finding out the kinds of conditions that affect the lives of people who rank lowest on the happiness scale. 

In these blinks, you'll learn

  * which country keeps track of its Gross National Happiness;

  * that a good artistic and cultural scene is more important than wealth;

  * which Eastern European country ranks dead last in happiness.

### 2. The Netherlands is the headquarters of happiness research and one of the world’s happiest nations. 

The idea of happiness research may strike you as strange. Is it even possible to scientifically measure happiness? Or is it a subjective and elusive quality?

Well, if you're looking for the latest research on happiness, the best place to start is the Netherlands, where annual happiness conferences are held for researchers to compare their notes and methodologies.

Recently, the Netherlands has become something of a Mecca for happiness, and the Dutch professor Ruut Veenhoven has become its prophet. Veenhoven's _Journal of Happiness Studies_ is a highly influential publication and his _World Database of Happiness_ is a vital resource for many of his peers, including the author himself.

The Database is a collection of statistics and results from research conducted around the world, and by bringing this information together, a great deal of insight can be obtained.

For example, the database makes it quite clear that married people are happier than singles, Democrats are less happy than Republicans, the rich are happier than the poor and women are just as happy as men.

But not all of the data is quite so clear-cut. In fact, some of it is contradictory: in many of the world's happiest countries, for instance, suicide rates are higher than average. And while religious people are generally happier than the nonreligious, it's the secular countries that are ranked the happiest.

It's no surprise that the database is kept in the Netherlands, since many studies and articles in it show the Dutch to be among the happiest people in the world.

The question then becomes, why? Which conditions contribute the most to happiness? For the Netherlands, relevant factors could include being a democratic and wealthy European nation with a functioning welfare system. Tolerance is another potential reason for their happiness; the Dutch are famed for their tolerant attitude toward prostitution, drugs and immigration.

But how does one measure happiness anyway?

Well, there are many methods, some of which are highly dubious — to put it nicely. While counting smiles might get you mixed results, asking people to rank their own happiness is a surprisingly accurate way to go.

> _"Americans buy self-help books as if their lives depend on them."_

### 3. Swiss happiness is the result of precision, avoiding class conflicts and enjoying nature. 

Another country consistently scoring high on the happiness charts is Switzerland.

You might think of Switzerland as being a rather sleepy place, and the legacy of "Swiss precision" is one of the only reputations that can make the punctual Germans look lazy.

But that precision has effectively removed any source of unhappiness.

Everything operates like clockwork in Switzerland, with trains running on a strict schedule, roads clear of potholes and bathrooms and sidewalks alike kept clean and spotless.

Methods like this aren't going to be the cause of happy celebrations, but they do succeed in removing any cause for being unhappy.

The Swiss can keep clean thanks to their nation's wealth, but they're smart about avoiding class divisions by making it a cultural taboo to talk about one's wealth. The Swiss know that discussions about money trigger envy, one of the biggest roadblocks to happiness.

The Swiss avoid dressing or acting in any way that might be perceived as flaunting wealth, which is a philosophy very much at odds with the United States, where the motto "If you've got it, flaunt it!" seems to be far more popular.

Switzerland also has one of the most liberal euthanasia laws in the world. So, they can always rest easily, and happily, in the knowledge that in the worst-case scenario, death is always a safe and legal option.

In addition, the geography of Switzerland is a good example of how nature can likewise be a deep source of happiness.

The picturesque beauty of the Alps has served as a constant contributor to Swiss happiness, and it's a relationship that was once put into words by the Pulitzer Prize-winning biologist and naturalist, E. O. Wilson. In his 1984 work, _The Biophilia Hypothesis_, he describes our enjoyment of nature's beauty as a powerful genetic trait.

Others have noted how nature can affect our physiology. In 1984, psychologist Roger Ulrich studied patients recovering from gallbladder surgery, and he noted how patients with a view of nature from their bed would heal at a faster rate than patients with no such view.

> _"The Swiss have done for boredom what the French did for wine and the Germans for beer: perfected it, mass-produced it."_

### 4. In Bhutan, happiness shapes the national policy, while spirituality and Buddhism contribute. 

The next stop on our tour of the world's happiest places is Bhutan.

If you're not familiar with Bhutan, it's a small and remote Himalayan country located between its big neighbors, India and China. If you've ever dreamed of living like a monk and spending your days devoted to the peace and quiet of meditation, Bhutan is the place for you.

Bhutan is another country with policies fundamentally at odds with American values. The United States and other Western nations place an emphasis on _Gross National Product_ and the pursuit of economic growth, but Bhutan's government is more interested in _Gross National Happiness_ (yes this is an actual policy!).

Many capitalist nations pursue money not as a means to an end, but as an end unto itself. However, something entirely different is going on in Bhutan, where people are happy not to be so concerned with money.

Bhutan didn't have a single paved road until 1962. Not only that, the nation didn't have schools, hospitals or even a national currency. Nearby Nepal has looked to tourism as a reliable source of revenue, but the King of Bhutan sees overall amounts of happiness as a much more valuable form of wealth. As a result, the king has made sure his people have free education and health care, and are not exposed to corporate advertising. Bhutan is also the only nation to outlaw smoking.

Another factor in Bhutan's happiness is its Buddhist spirituality, which imparts significance to every rock and tree branch.

The World Database of Happiness reveals many places where spirituality increases happiness, and it's definitely the case in Bhutan, where the lines between reality and fantasy happily blur. Part of Buddhism is a belief in reincarnation, which the author saw in a woman who proudly refers to her husband as being a brother of Dalai Lama — in a previous life.

It's also important to recognize the perspective the Bhutanese have on life. They don't stress about their achievements and failures because they recognize that the efforts of one life are quite insignificant in the grand scheme of things.

> Bhutan is an upside-down country. Bhutanese greet with "bye-bye," 13 is a lucky number, and the king wants to abolish the monarchy.

### 5. Qatar is rich, but due to a lack of culture and an unforgiving environment, there’s very little happiness. 

Our next stop is a detour on the happiness tour, to a country that stands in great contrast to the penniless Bhutanese. It's Qatar, an example of the old lesson that happiness can't be bought.

Until the relatively recent oil boom of the 1980s, Qatar was just a deserted stretch of land, with one road and a few houses. While it has since been discovered that it is home to some of the world's greatest reserves of oil and natural gas, the country is still 98 percent desert, an environment generally associated with unhappiness.

But unlike other wealthy nations, Qatar has used this money to create the ultimate welfare state. Everything from water, electricity, health care and education are free and no one pays taxes. Also, upon getting married, every Qatari husband gets a plot of land and some $7,000 per month.

But despite all these efforts, Qataris complain a lot about being unhappy. The nation's wealthy citizens believe the government doesn't do enough to make things easy for them. And if there's ever talk of creating a new tax, it's always met with stern disapproval.

Wealth has been a burden for Qatar, much like the stories of lottery winners who grow saddened by the loss of friends and the lifestyle change that comes with so much money. Going shopping is no longer the special treat it once was. And since most Qataris don't have jobs, they're also missing the satisfaction that comes after a hard day's work.

Then there's the lack of culture as well: among the displays in Qatar's National Museum are the toenail clippings of a camel, which sums up the interest the city has in art.

Most disturbing, however, is the trouble Qatari people have with the very concept of happiness.

When asked, "Are you happy?" one Qatari man was irritated and confused about why someone would ask such a question. From the Qatari perspective, whether you're happy or miserable, it's God's will and out of the control of humans.

But the real lesson we can take from Qatar is that happiness can't exist unless there's also the occasional discomfort to compare it with.

> In Arabic script, the word "Qatar" resembles a smiley face.

### 6. Icelanders have a vibrant culture and a beautiful landscape to fuel their happiness. 

When you close your eyes and think of happiness, what kind of landscape comes to mind? Is it sunny blue skies with a grassy meadow or sandy beach?

These typical happy images are far from the daily life of Icelanders, yet their country is often near the top of the list when it comes to happiness.

But what Iceland may lack in pleasant weather, it makes up for in creativity. Every other building in the capital city of Reykjavík is either a bookstore, a record shop, an art gallery or a café filled with poets and painters.

There's even a local joke that one day, a statue will be built in central Reykjavík to honor the one Icelander who never wrote a poem. But for now, they'll continue to wait for that day to arrive. It's no surprise really, since writing is one of the best ways for a person to stay busy during the endless stretches of dark winter. It's certainly a big reason why Iceland's taxi drivers, fishermen and hotel clerks are all writers.

Iceland is another country with an inspirational landscape, thanks to its dramatic geysers, hot springs, glaciers and volcanic rock. Looking around, it becomes perfectly clear why this land gave birth to the people who first imagined the otherworldly creatures of dwarves and elves.

Part of the reason Icelanders are so willing to work in the arts is due to their culture's willingness to embrace naiveté and failure. No one in Iceland will discourage you from trying your hand at painting, sculpting or writing; instead, they'll encourage you to try and see what happens.

Icelanders are happy to admit that a lot of bad art comes from their country, but in their mind, it was made with good intentions, which is what really counts. So what if it fails? Tomorrow's another day to start all over again.

> Icelanders love their language. Unlike other languages, they do not adopt English words for new inventions. So their word for "computer" is "tolva" — or "prophet of numbers."

### 7. Many factors lead to Moldova being the least happy place on Earth. 

If Iceland such a happy place, it would be common sense to think a hot and oppressive country might be the world's unhappiest place. Perhaps somewhere in Africa? Alas, no. According to Veenhoven's World Database of Happiness, the world's true hub of misery is a small strip of Eastern European land located between Romania and the Ukraine, known as Moldova.

There are a number of factors making Moldovans so unhappy, but at the top is economics.

For at least one Moldovan woman, the root cause of unhappiness is the simple fact that there's no money, which is supported by data that shows the average per capita annual income is only $880.

Moldova isn't the poorest nation — other countries like Nigeria or Bangladesh fare worse — but since the nation is surrounded by wealthy neighbors, their world standing stings a bit more. It's like living in poverty on a street full of rich people.

Making matters worse is Moldova's rampant corruption, where even college professors are routinely bribed by students. Moldovans have learned to avoid young doctors, since they're likely to have "bought" their degree.

Moldova is especially poor in natural resources, with no oil or minerals to trade or use. Their small amount of revenue comes from a small range of fruit and vegetables they manage to grow.

Like unhappy Qatar, Moldova is a barren desert when it comes to culture. This is likely due in part to the country's lack of a strong national identity. Russia only created the region after the collapse of the Soviet Union, before which there was no such thing as Moldova.

Adding to the confusion is Russia, which says Moldovans are essentially Romanian, and Romania, which says Moldovans are definitely Russian.

With two neighbors pushing them in opposite directions, it makes sense that the closest thing Moldovans have to culture is a pervasive pessimism that is filled with both resignation and envy.

This attitude is perhaps best displayed in the many Moldovans who enjoy the misfortunes of their neighbors more than they care about their own advancements.

> McDonald's is prohibitively expensive for most Moldovans. Only wealthy oligarchs and the Russian mafia can afford eating there.

### 8. Thais have found happiness in not taking life too seriously. 

No exploration of the world's happiest countries would be complete without a look at Thailand, known to some as the Land of Smiles.

You may know the saying about the Inuit people of the Arctic having multiple words for snow. Well, the Thais have a similar variety, but in this case it's for different kinds of smiles. There's "yim cheun chom," the smile that expresses admiration; there's "yim thak thaan," the smile that says "I dislike your idea but go ahead;" and let's not forget "yim sao," the sad smile.

The Thais are so familiar with the many reasons to smile because they've discovered their own secret to happiness: don't think about being happy.

Two common phrases in Thailand are, "Don't think too much," and "Don't be so serious." In Thai society, ignorance really is bliss and too much thinking or questioning only leads to problems, not happiness.

Thai people aren't the only ones making this connection, either. Psychologists Tim Wilson and Jonathan Schooler conducted a study that involved participants listening to classical music after being told to either think about their happiness levels, or have no particular thoughts in mind. The decisive results showed that those with a clear mind were consistently happier than the analytic participants.

The other reason for Thai happiness is that they treat everything with the same lack of seriousness.

It's common for business meetings in Thailand to be filled with jokes and laughter, and even unpleasant activities like mowing the lawn are treated like jolly good fun. Happiness springs up on a lot of their products, such as "Happy Toilets," "Happy Pubs" and a dish called "Double Happiness."

Thais generally accept their fortunes, for better or for worse, and if it's not so great, well, then there's always the next lifetime. The Thai motto might as well be "when nothing matters, life is easy."

### 9. Britain has conducted some experiments on happiness, but it’s not considered as important as other qualities. 

There's a good chance you've experienced contagious laughter. But what about contagious happiness? Can a happy person enter an unhappy place and spread joy?

A group of TV producers in England were curious about these kinds of questions and applied them to the town of Slough, located just outside London and widely considered the dreariest place in the United Kingdom.

In 2005, the BBC aired a reality television series called _Make Slough Happy_, and its basic idea was that if Slough can become happy, any place can.

The show followed six "happiness experts" giving a 12-week "happiness training" course to 50 volunteers. These 50 people then went on to be a positive influence in their part of Slough and viewers would see if their happiness could be passed on to others, who would then continue passing it on, until Slough became a changed place.

The volunteers would hold people's hands, hug trees, perform yoga and tai chi, dance in the aisles of supermarkets and, of course, engage in some reliable, uncontrollable laughter.

The 50 volunteers concluded by reporting that Slough's happiness increased by 33 percent following the experiment, but it's hard to say how accurate or long term those results were.

It should also be recognized that the British have a different attitude toward happiness than others.

Unlike Americans, who consider the pursuit of happiness as part of their national identity, British folks look at life as something one must get through with a stiff upper lip and some determination.

For example, imagine you're in an airplane and the engine has just burst into flames. Who would you rather have flying the plane? A happy optimist just out of pilot school or a grumpy pilot who has a decade of experience.

In other words, the British don't view happiness as a prerequisite for a meaningful life.

> _"Brits don't merely enjoy misery, they get off on it."_

### 10. In India, happiness is big business, but there’s still some genuine wisdom to be found in it. 

India is famous for its gurus, miracles and spirituality, and these characteristics attract thousands of Western tourists every year. Many of these travellers are people who are chasing bliss and feel the need to leave their comfortable environment for more exotic and happier lands of India.

When the author went to India, he went to hear the guru Sri Sri Ravi Shankar speak to an audience about eternal bliss, but found that his words rang hollow.

In fact, the author found Shankar's words to be a lot like the popcorn that much of the people in the audience were eating; like a low-calorie snack, Shankar's words were tasty and easy to digest, but they lacked substance and nutrition.

For example, when asked about what happens after death, Shankar's cheeky reply was that he could provide answers, but he doesn't want to ruin the suspense.

Shankar's talk is more like entertainment than enlightenment, as the most popular Indian gurus are treated like pop stars and even get sponsored by big corporations. But while Shankar may lack in philosophical depth, his overall message is clear and positive: happiness shouldn't be anyone's main goal in life, and love will always trump happiness.

This isn't a controversial message in India, since the country's predominant religion is Hinduism, which regards the pursuit of happiness, or any sort of striving, as an act of self-defeat.

According to Hinduism, striving to be happy is futile since unhappiness is a result of powerful forces such as fate and karma, and trying to fight this is basically like trying to defy the will of the gods.

So, while the author and other researchers and scientists are determining whether or not happiness can be measured, many Indians see these efforts as a prime example of human folly.

> In the 1980s, guru Baghwan Shree Rajneesh, or Osho, famously had not one but some 93 Rolls Royces.

### 11. The United States has been getting less happy as it has grown wealthier. 

The United States ranks high on most lists dealing with economics and spending. But happiness isn't a currency, and there are a lot of researchers who place the United States relatively low on the joy scale.

Adrian White is one such researcher. He works at the University of Leicester in England and he ranks the United States as twenty-third happiest in the world, below considerably less affluent countries as Costa Rica, Malta and Malaysia.

For Americans, their levels of happiness have not been able to match their levels of wealth.

Wealth has tripled in the United States since 1950, but research shows that their happiness has not matched this progress. In fact, happiness levels have been on a steady decline.

Psychologist David Myers points to a number of troubling trends that suggest Americans are currently at their lowest levels of happiness ever. Depression, anxiety and other mental health problems are rising, while divorce rates have doubled, violent crime rates have quadrupled and teen suicide rates have tripled.

One explanation might be expectations. With more money, there's an expectation to experience more happiness. But as the wealth increases, it only becomes harder to reach these levels. So, as more people buy more luxury items, everyone eventually finds out that such purchases do nothing to increase emotional happiness. Instead, confusion and disappointment follow.

Americans also spend more time at work and less time with family and friends, which is a factor that the World Database of Health suggests is directly related to happiness. Another one is the amount of time it takes to commute to work, which also tends to be higher for Americans.

The United States is also a traditionally restless nation, with people who are never satisfied with what they have and are always striving for more. And when nothing is good enough, there's never going to be happiness.

Even so, one of the other defining characteristics of the United States is optimism. Even if they're not happy now, two-thirds of Americans still believe that happiness awaits them in the future.

### 12. Final summary 

The key message in this book:

**The world is home to happy people and unhappy people, and everything in between — although the factors contributing to happiness may not be what we expect. Some populations find happiness by not focusing on it at all, while certain nations try too hard to make their citizens happy and end up failing miserably. While there's no perfect recipe for a happy nation, there are certain trends that lead us to believe happiness is not something that can be bought.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Geography of Genius_** **by Eric Weiner**

_The Geography of Genius_ (2016) takes you on a journey around the world to places that have been at the epicenter of golden ages of creativity. You'll discover what made these places so rich in human genius.
---

### Eric Weiner

Eric Weiner was a long-time foreign correspondent for National Public Radio, having been stationed in New Delhi, Jerusalem and Tokyo. His work covering Islamic news in Asia won him the Angel Award for outstanding journalism. His writing has appeared in the _Los Angeles Times_ and the _Washington Post_, and his other best-selling books include _The Geography of Genius_.

