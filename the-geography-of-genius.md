---
id: 57344353e2369000032901a8
slug: the-geography-of-genius-en
published_date: 2016-05-16T00:00:00.000+00:00
author: Eric Weiner
title: The Geography of Genius
subtitle: A Search for the World's Most Creative Places from Ancient Athens to Silicon Valley
main_color: 72B3B4
text_color: 416566
---

# The Geography of Genius

_A Search for the World's Most Creative Places from Ancient Athens to Silicon Valley_

**Eric Weiner**

_The Geography of Genius_ (2016) takes you on a journey around the world to places that have been at the epicenter of golden ages of creativity. You'll discover what made these places so rich in human genius.

---
### 1. What’s in it for me? Take a trip to the fertile lands of genius! 

Ancient Athens and twenty-first-century Silicon Valley are thousands of years and thousands of miles apart, but they still have something in common. Both places bear a legacy of innovation, creativity and genius. Why? What made them such fertile soil for genius?

To answer this question, let's embark on a trip through both the historical and modern world to examine the places that have been conducive to genius, innovation and productivity. This journey reveal the backdrop to some of humanity's greatest creative eras.

In these blinks, you will discover

  * why Hangzhou, China, saw a golden age spanning hundreds of years;

  * what part practicality played in the Scottish Enlightenment in Edinburgh; and

  * how the invention of purgatory financed some of the greatest works in Renaissance Florence.

### 2. Genius is not inherited; it is bound to place. 

A genius can be defined as someone who has the potential to come up with new, surprising and valuable ideas. But is there a way to predict whether an individual will possess this potential?

In the past, some thinkers believed that genius could be predicted by genetic inheritance. But modern science has proved the contrary.

One of these thinkers was Francis Galton, the father of inheritance theory. In the nineteenth century, he came up with the idea that civilizations flourished or failed because of their genes. For him, a regular influx of immigrants and refugees brought a valuable new line of blood into societies, boosting creativity.

However, Galton didn't believe all immigrants and bloodlines were equal. Typically for the nineteenth century, he was plagued by racist assumptions. For instance, he believed that ancient Greek civilization declined because its members began to intermarry with non-Greek people of lesser blood, like people from Asia Minor.

Furthermore, Galton's theory does not fit the empirical evidence: modern psychologists believe that genes play only a minor role in the development of creative genius. The truth is that creativity is too complex to be explained by genes alone: it's a relationship that develops at the meeting point between person and place.

This is what Dean Simonton, a leading researcher in human creativity, discovered when he studied the history of creativity. He found that at certain times certain places have been the cradles of numerous brilliant minds. These epicenters of extreme creative activity are commonly referred to as Golden Ages, a concept that comes from ancient Greece, where one of the great Golden Ages took place. Not one but many world-changing geniuses lived at the same time during these eras. If we look carefully at examples of such creative cities, we can determine the circumstances that favored the emergence of genius — and try to pinpoint where the next Golden Age will happen.

> _"Talent hits the target no one else can hit, genius hits the target no one else can see."_ — German philosopher Arthur Schopenhauer.

### 3. Athens is the mother city of creative genius. 

Ancient Athens has an impressive résumé: It's the birthplace of great philosophers like Socrates, statesmen like Pericles and playwrights like Euripides. So how did Athens become such a hotbed for genius?

First of all, Athenians loved their city. They had a sense of civic duty, or we could even say civic joy. This led citizens to compete with one another to make the greatest contributions to their society. They spent a significant part of their time debating public affairs, often meeting in spaces like the ancient marketplace Agora. The great philosopher Socrates was a pioneer and vigorous advocate of this model.

What's more, Athenians invested large quantities of their own wealth into art, entertainment and festivities to further the cultural advancement of their city. For example, classic Greek tragedies from playwrights like Euripides were financed by rich Athenians.

Another reason Athenians were so creative was their enthusiasm for foreign ideas. While most Greeks went as far as adopting Babylonian mathematics, deriving their alphabet from the Phoenicians, and imitating the architecture of Egypt, the Athenians progressed even further. Not only did they incorporate a variety of foreign words into their dialect, but some were influenced by foreign fashion and embraced new clothes and styles.

A perhaps smaller, but revealing, component that contributed to the Golden Age of this city was the rich tradition of walking. Everyone from common citizens to philosophers walked everywhere, with Aristotle himself delivering his lectures while strolling through the grounds of his academy, the _Lyceum_. The creative value of walking has been proven in the studies of contemporary psychologists Marily Oppezzo and Daniel Schwartz, who found increased cognitive activity in habitual walkers.

Athens arguably represents the foundation of Western culture; it's the root of human creativity and inspiration from which so much later genius grew.

> _"We are all a little bit Greek, whether we know it or not."_

### 4. Hangzhou sustained an unprecedentedly long Golden Age of genius. 

While European countries were stuck in the Dark Ages, Hangzhou in China was the Song Dynasty's most important city, fostering a flourishing of genius and creativity that lasted from 969 to 1276. Why was this?

Hangzhou was the meeting point of the Buddhist and Confucian cultures which brought together innovative ideas and traditional values. For instance, the Buddhist monasteries perfected _woodblock printing_ that made it easier to reproduce manuscripts. This soon led to the production of the first nautical and astronomical maps, and to pioneering developments in the field of archeology.

Confucian culture generally discouraged people from quickly embracing novel ideas and "strange doctrines" but this didn't prevent creativity. Instead, new ideas like the woodblock prints were adopted in the context of tradition — in this case China's monasteries — which allowed them to combine the past with the present.

This synthesis of the new and the old is one reason why the Chinese didn't develop in one burst but over the course of a long, fertile era. During this time, they produced the finest textiles and porcelain in the world, and were the first to introduce paper money. The traveler Marco Polo was so astonished by Hangzhou's development that he wrote tens of pages in his diary describing it.

This development was all the more enhanced by another Confucian tradition: "less is more." Chinese art was restricted by traditional values, but this actually led to more creativity instead of less. For example, Song Dynasty painting contained obligatory elements, like the way in which the brush was used or objects depicted, while other aspects were left up to the artist. These constraints made a framework within which artists could create a wide range of variations on the same theme. The power of these creative constraints has been validated by modern psychologists like Robert Sternberg and Todd Lubart, who concluded that having limited options can spur creativity because too many options can make choosing impossible.

Unfortunately, in today's China creativity is undermined. High-school students compete in memorization and cramming rather than in creativity.

> _"Nothing is new but the arranging"_ — Will Durant, author of _The Story of Civilization._

### 5. In Renaissance Florence, creativity was fostered by entrepreneurs, competition and collaboration. 

In the late fourteenth to sixteenth centuries, Florence was neither the largest nor the best-defended city in Italy. Yet, creatively it surpassed all others, producing geniuses like da Vinci, Michelangelo, Donatello and Botticelli.

Why?

First of all, wealthy Florentines believed in the value of art. Rich families of entrepreneurs like the Medici believed that commissioning great artists would ensure them fame and posterity. Some of the masterworks of da Vinci and the others were created under their patronage.

Then there was the other powerful patron in Florence: the Catholic Church. The church had become extremely wealthy thanks to purgatory. After death, every sinner had to suffer in purgatory before they got to go to heaven, and the more you sinned, the longer you had to wait.

But there was a way out: if you bought expensive _indulgences,_ you could shorten your time in purgatory. The popularity of these indulgences allowed the church to accumulate great riches, which were then used to commision talented artists. For example, the _Duomo,_ the spectacular cathedral of Florence, was partly financed with such money.

Another driver of Florence's creativity was its lively commerce. Florentine merchants were exposed to very diverse ideas as they traveled all across the known world to trade their wares. For instance, a Tuscan named Leonardo Fibonacci traveled to Arabia where he came across Arabic numerals. He immediately saw their use for improving commercial bookkeeping, and introduced the Arabic system in Florence. From there it gradually spread to other cities and is now used all over the world.

The prevailing spirit of creative competition and collaboration was the final source of Florentine creativity. For example, the architects Ghiberti and Brunelleschi battled each other for fame, pushing themselves to create ever greater masterpieces. Meanwhile, Leonardo Da Vinci received help from his master Andrea del Verrocchio, who in turn harnessed young Leonardo's creativity to benefit his workshop.

### 6. In Edinburgh, practical creativity flourished. 

At the turn of the eighteenth century, the Scottish Enlightenment produced unprecedented developments in science, medicine, engineering and the humanities. What caused this Golden Age?

Firstly, in Scotland invention and scientific evolution were born out of necessity. The existing agricultural techniques were inefficient, with a mere 10 percent of the landscape being farmable. Carpenter James Small responded by inventing a new plow that allowed farmers to cultivate their land more efficiently. Small's invention stimulated further debate among landowners, who regularly met to talk about new developments in production.

This culture of discussion and debate was well developed in Scotland, playing a fundamental role in the country's cultural advancement. Like the Athenians, the Scots assembled in public places to address contemporary issues and provoke new, creative ideas. Adam Smith, author of _The Wealth of Nations_, often took to a public platform to dispute ideas with his fellow philosopher, economist and friend David Hume.

Moreover, this interchange of thought was inherent to the Scottish universities. In the academic world, emphasis was placed on the duality of learning, with information flowing both from professor to student and vice versa. The students were often the first port of call for professors to develop their ideas. For example, Adam Smith first presented his magnum opus to his students in a series of lectures in the late eighteenth century. The feedback Smith received was invaluable to him, prompting fresh thought and improvements.

Not only did the discourse of the Scottish Enlightenment advance creative and intellectual conditions for Scots, but it continued to spread far into Europe and the Americas. Indeed, many of the major scientific and humanitarian fields explored in today's universities, including politics, economics and morality, were conceived by these pioneers of the Scottish Enlightenment.

### 7. Western and Eastern genius met in Calcutta. 

Known today as the _Bengali Renaissance_, the period between 1840 and 1930 saw a remarkable flowering in the humanities, science and religion of Calcutta. With the exception of London, Calcutta published more books than any other city in the world.

So, what caused this creative boom?

Calcutta was the place where British Western ideals collided with the Indian subcontinent's cultural heritage. However, this influx of foreign culture was neither wholly accepted nor rejected by the indigenous citizens. Instead, British values were subjected to a glorious type of _Indianization._ As a result, the two traditions evolved together, and a new artistic synthesis emerged.

The author Rabindranath Tagore is one example. Partly educated in Britain, Tagore was able to intertwine elements from both cultures in his writing, creating a unique Indo-Anglian voice. He was the first to introduce British colloquialisms to Indian literature, something that had previously been restricted due to the country's Sanskrit heritage. Tagore was also the first Asian Nobel Prize winner.

The chaotic nature of Calcutta was another reason for its productivity. Contemporary studies in psychology suggest that complex environments stimulate the brain, which in turn can make individuals more creative. For example, recent studies on rats have shown that those kept in stimulus-rich environments experienced enhanced thought and perception. Humans also benefit from such complex environments — like chaotic Calcutta.

In fact, Calcuttans have to cultivate their creativity just to be able to function in the daily mayhem of the city. For example, the complicated structure of Indian bureaucracy requires creative leaps that prepare the mind for the invention of new ideas.

Another reason Calcutta is so culturally productive is their cherished tradition of discussion, called _Adda_. As in Athens and Scotland, Calcuttans discuss everything from the universal to the mundane to elevate the discussion for all.

> _"Calcutta taught me that the best state to be poised in is in-between two cultures and their productive discourse."_ — Sunreta Gupta

### 8. Silicon Valley’s innovation is driven by successful failures and weak ties. 

Today, Silicon Valley is at the forefront of innovation. But a hundred years ago, it was nothing more than a fruit orchard. Its transformation began with the vision of Fred Terman, the dean of Stanford's Engineering School.

Back in the 1930s, Terman helped start the computer revolution by investing in _Hewlett and Packard_, founded by his former students Bill Hewlett and Dave Packard. He later went on to establish a technological center called Stanford Industrial Park, which was the first of its kind in the world. Terman wasn't afraid of failure — on the contrary, he embraced it, and made plans for the park to be made into a school if things didn't go as planned.

This use of failure is one major driver of Silicon Valley's success _._ Entrepreneurs there even came up with a new term: _successful failure,_ which means failing intelligently until something works. Instead of giving up, you learn from your mistakes and try another way.

Silicon Valley's courage to face failure manifests itself in other ways too. People who work there aren't afraid of taking big risks because they know they can easily find a new job. These high-stake gambles create an incredible flux of successful and failing business. In fact, every year the ranking of the top ten firms in the valley is drastically different.

Another factor that makes Silicon Valley so creative is the way it leverages the value of _weak ties_. Weak ties are the connections people have with each other through common friends and acquaintances. And seeing that Silicon Valley attracts a lot of young talent from the same Ivy League schools, these weak ties are plentiful.

But why are they so useful?

First, weakly connected people come from different disciplines and states. This means that people share ideas from many fields of interest.

Second, people connected by weak ties are less invested in their relationships, which makes it easier for them to challenge bad ideas. Instead of avoiding conflict, they tend to be frank about an idea's worth — which allows everyone to move on faster.

> "If you fail in Silicon Valley, your family will not know and your neighbors will not care."– William Foster

### 9. Final summary 

The key message in this book:

**Genius is not a consequence of genes or personality. Instead it flourishes in places and times that embrace debate, invest in the arts, combine ideas and embrace failure.**

Actionable advice:

**Develop healthy drinking habits.**

Although large quantities of alcohol may lead to addiction and severe health problems, the Mediterranean tradition of drinking small quantities of wine seems to incite creativity. Ancient Athenians often accompanied discussion with wine. In fact, they even diluted the wine with water to increase the positive effect of alcohol and keep its toxicity in check.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Triumph Of The City_** **by Edward Glaeser**

_Triumph of the City_ extolls the virtues of the city as one of civilization's greatest inventions. Cities not only connect people but also help them accomplish great things. And although many of today's urban metropolises face real challenges in a new economic order, there are many ways for cities to succeed.
---

### Eric Weiner

Eric Weiner is a writer, columnist and public speaker. He is also the author of the _New York Times_ bestseller _The Geography of Bliss_.

