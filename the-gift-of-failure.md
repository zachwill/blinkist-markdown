---
id: 56dd900e24b39b000700001a
slug: the-gift-of-failure-en
published_date: 2016-03-07T00:00:00.000+00:00
author: Jessica Lahey
title: The Gift of Failure
subtitle: How the Best Parents Learn to Let Go So Their Children Can Succeed
main_color: D28D2A
text_color: 805619
---

# The Gift of Failure

_How the Best Parents Learn to Let Go So Their Children Can Succeed_

**Jessica Lahey**

In _The Gift of Failure_ (2015), Lahey offers compelling reasons for caregivers to relinquish control over their children and let them fail. By taking this approach, Lahey argues, it will give children an important opportunity to learn about their values and skills, while strengthening their confidence, autonomy and sense of responsibility.

---
### 1. What’s in it for me? Give yourself and your kids the greatest gift of all: failure! 

What just about always happens when we do something for the first time? We fail. Nothing wrong with that. In fact, failure is an important experience we need to help us improve. But, over the last decades, we have started to deny our children the right to fail. 

Turns out, this has even worse implications than we might think. By not letting our kids figure things out on their own, we effectively deny them the skills they will need later on in life. So let's explore how the overly controlling way we raise and teach our kids is doing exactly the opposite of what we'd hoped it would do.

In these blinks, you'll discover

  * what monkeys can show us about demotivation;

  * why saying a child is good can be bad; and

  * how cleaning the house will help your kids.

### 2. Our view on children and their education has changed throughout history. 

Today the predominant parenting style involves protecting and sheltering our children until they leave the family nest. But it wasn't always like this.

In the past, children's education was geared toward early autonomy. In fact, in seventeenth century New England, work took precedence over children's education. Due to poor health and mass poverty, many children died, and those who survived had to help their parents with the household or on the farm as soon as they were able.

Around this time, philosopher John Locke advised parents to let their children make mistakes and face the consequences. Failing to do this would only weaken their minds and prevent them from getting back up and trying again when they fail.

Over the next few hundred years, children's life continued to be tough. In nineteenth century America, nearly one out of six children between the ages of 10 and 15 were employed, mostly in factories. Teenagers weren't seen as sensitive, developing children, but as practical, cheap labor.

Later, a change in working circumstances and family structures allowed parents to focus more on their child. Dangerous child labor practices in the early twentieth century gave rise to regulations which prohibited children under a certain age to work.

From then on, children went from being "useful" to "useless" in their families. Combined with growing divorce rates and couples having fewer children later in life, children's education became more centered on caring for children as developing adults.

Psychological books about children's education also became popular, like Nathaniel Branden's 1969 bestseller _The Psychology of Self-Esteem_. Branden argued that self-esteem played the most central part in a child's behavior. According to psychologist Jean Twenge, the self-esteem movement reinforced self-esteem in American society but, at the same time, turned people into narcissists.

### 3. Children’s enthusiasm must come from their own motivation instead of rewards. 

The fight for survival that children experienced hundreds of years ago has given way to a fight for perfect grades in school. Although grades were invented to measure progress and future success, they have a considerable downside: they fail to instill intrinsic motivation in children.

From the very first day of school, children are pushed to deliver good grades. They are taught that without them they won't live the life that they want or become a respected member of society. This forces all pupils to go the same route and strive for consistent A's. Failure has been removed as an option.

To motivate their children to bring home perfect grades, many parents turn to external rewards, such as a new phone. But this type of external reward dampens intrinsic or self-driven motivation. 

We can see this in one experiment psychologist Harry Harlow carried out with monkeys. Harlow showed that the monkey's interest in resolving a task stopped when they were rewarded regularly, as opposed to monkeys that received no reward.

In humans, we also know that, when a child is enthusiastic about a task, they will persevere, even when the going gets tough.

To foster enthusiasm, children must find their own way of solving problems. But how do you make a student excited about school? Well, the key rule for caregivers is to take a step back. 

Children should decide themselves how, when and where they accomplish a task, and not expect money or gifts for it. However, this doesn't mean that parents shouldn't express an opinion in school activities. Rather, parents should establish nonnegotiable expectations, such as homework deadlines and, if their children don't do it, they should let them fail. This is the only way for them to learn how to cope under pressure.

> _"Put simply, if you'd like your child to stop doing his schoolwork, pay him for good grades."_

### 4. Education should center on guiding children to make their own decisions. 

Parental involvement is crucial in a child's education, but there is a difference between being involved and trying to rule your child's school life.

You should therefore opt for _autonomy-supportive_ parenting, rather than _controlling_ parenting.

Psychologist Wendy Grolnick studied autonomy-supportive and controlling parents, and found that children who were controlled by their mothers gave up faster than children who weren't when faced with frustrating situations while playing by themselves. 

An autonomy-supportive approach shouldn't be mistaken for permissive parenting, however. Being autonomy supportive means setting limits for your children so that they can test their standards against yours. Moreover, evidence shows that children prefer parents who hold them responsible for not meeting expectations, rather than controlling parents who monitor their children extensively.

Switching from controlling to autonomy-supportive mode takes some time and effort, but the goal should be to support your child, rather than direct him or her. The difference between the two styles isn't always clear, though, and it's important to keep this in mind. 

One example of an autonomy-supportive approach would be to value your child's successes in addition to his or her mistakes. This style also involves helping children discover different ways of dealing with problems.

On the contrary, controlling parents provide solutions before children have had the chance to solve it by themselves.

If you allow your children to struggle with problems, they'll learn the necessary patience for coming up with a solution. Remember that not all parenting styles are clear-cut and, often, controlled parenting seems supportive because it uses rewards. 

Remember: in the same way that you make it okay for your children to fail, make it okay for yourself to fail as a parent, too! But as long as you show your children that your love isn't dependent on their success, you'll always be on the right path.

### 5. The right type of praise encourages your child. 

A lot of parents praise their children every possible way they can, but praise can actually demotivate your child. It's all about _how_ you praise them. So how should you give praise?

First and foremost, you should praise a child's behavior, not the child. Praise has different functions: 

you can praise someone for who they are, such as saying "You're so funny!" or you can praise for behavior, like when you say "You did this so well!"

When you praise someone for who they are, you pass judgement and create a _fixed mindset_. This means you perceive their particular skills or ability as fixed throughout their life. Praising behavior, on the other hand, establishes a _growth mindset_. This is based on the idea that everyone can develop skills in different areas, as long as they practice them.

To praise in the best way, you must focus on effort. Psychologist Carol Dweck researched the influence that different praising styles had on student behavior. Dweck's results showed that a group of students who were labeled as smart before the study started were far less likely to persevere with more challenging tasks. They also gave up faster than the students who were valued for how hard they worked.

Japan is an interesting counterexample to typical Western styles of schooling. In Japan, children aren't grouped according to their ability. Instead, there is the belief that all children can achieve a high degree of skill in any area. Moreover, no subjects are only reserved for talented children.

So instead of saying how wonderful your child is after he or she gets a good grade, try valuing the effort he or she put into preparing for the test. The more children believe that their talent can develop with effort and perseverance, the less they will fear failure.

### 6. Household duties give children an early sense of purpose. 

Household duties are a classic breeding ground for parent-child conflict. But they're also the first opportunity for children to learn competence and responsibility.

Children who take part in housework feel responsible and purposeful, and this kind of participation is a crucial step toward an autonomous, self-driven life. To understand just how important a sense of purpose is, we need only to look to research on suicide: lack of purpose is one of the most reported problems of suicidal people.

To feel a sense of purpose, children should be encouraged to take responsibility for household work. When parents take over if their children are making more of a mess than what they started with, this interruption has a detrimental effect. Constantly saving your child from mistakes by taking over their activities teaches them that independent efforts aren't important. 

Instead, assist your children in establishing their own system of doing things, and they'll learn initiative and how to get things done. You might also want to refer to household duties as "family contributions" instead of chores, as this will show children the value of their actions and that they play a key role in the family.

One important thing to consider is the age of the child when you assign them tasks. Even young children can be responsible for household chores. As they get older, they can be given more complex tasks and need less supervision. For example, children between six and 11 can set and clear the dinner table, write grocery lists or vacuum. Children 12 years and older can cook meals, care for younger siblings or start doing minor repairs like changing light bulbs.

Remember that assigning duties isn't about having an immaculate house, it's about sharing the effort to form a family bond.

### 7. Regular, free communication with other children helps children develop. 

Our social life begins in infancy when looking at adult faces and mimicking their expressions. And learning how to interact with others will continue throughout our lives. Therefore, the sooner children recognize that social skills aren't fixed, the better.

Though caregivers play a critical role in communication, children develop largely through communication with other children. They learn a great deal from free play, and parents should try to be as hands-off as possible during these times.

Psychologist Hara Estroff Marano has shown that free play in kindergarten is a 40 percent better predictor for academic success than standardized achievement tests, such as reading or math tests. 

Children playing with other children doesn't just pave the way for academic success, it also fosters empathy through immediate peer feedback. 

When parents intervene when children push each other, they prevent them from seeing the consequences of their actions. But if children see other children crying after being pushed, they learn how their actions affect others, which makes them better equipped to resolve issues with other children. 

Studies have also shown that children are less upset when they see emotional healing after their parents have argued, than when the conflict remains unresolved.

It also helps children to have relationships with children who have different interests and social backgrounds. This makes it easier to interact with new people later in life. In the world of sales, for example, those who have been exposed to a variety of people early in life have a higher level of mimicry, meaning they're better able to communicate and adapt to others.

There are times, though, when parents do need to intervene. If you know some children are having a dangerous influence on your children, for example with drugs or violence, talk to your children about it. If you're unsure, seek professional help. But remember to be honest with yourself: Are you worried about your child or do you simply not like the child they're hanging around with? If it's the latter, try to take a step back.

### 8. Grades can limit children, whereas children who set their own goals gain a sense of purpose. 

When poor grades start coming in, even the most supportive parents can get nervous and try to use controlling techniques to get their children back on track. These moments, however, are ideal opportunities for children to decide what's important to them.

Grades aren't the be-all and end-all: they have their limitations. One study in Japan researched the performance of students in math quizzes with two groups of students. One group was told that their results would impact their final grade and the other group was told the purpose of the quiz was to monitor their progress. The result? The second group performed far better and learned more from the quizzes compared to the first.

Even early on in the history of grading there were doubts about how useful it really was. Former Yale University president Timothy Dwight V criticized them in 1898. He didn't agree with their impersonality and the lack of personal communication between teacher and the student.

But even though grades have limited value, the reality is they're not going away any time soon. So what can parents do to help their children stay motivated, independently of grades?

They can help them set educational goals. To put grades in perspective, help your children set self-determined goals instead of goals dictated by the school curriculum. Take your time and talk with your kids, support them, and focus on their effort, _not_ the end result. When your child reaches their goal, it will give them a sense of self-affirmation and success.

Choice is another important part of autonomy. As soon as students can select their own courses, let them do so. This gives them a sense of ownership over their education. If instead _you_ decide what your children "need," they will become disengaged, feeling as if everything has been planned out for them in advance.

> _"Teach your children to face failure and accept it as valuable feedback."_

### 9. Final summary 

The key message in this book:

**When parents protect their children from making mistakes, they teach them that they're incompetent and that failure is bad. Instead, parents should let their children fail. This approach equips children with the tools to enjoy a self-sufficient, driven life.**

Actionable advice:

**Hands off the Legos!**

When giving your child a new task, don't interrupt and take over yourself if they're being slow or not doing it precisely according to your instructions. Instead, support them by asking if they're having problems. After some trial and error, they'll master it and reap the benefits of learning a new skill, which will also boost their self-esteem.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Raise an Adult_** **by Julie Lythcott-Haims**

_How to Raise an Adul_ t (2015) reveals the ways in which the most common parenting method today, helicopter parenting, is doing more harm than good, both for parents and kids. These blinks outline a better way to parent — one that actually raises children to become truly independent adults.
---

### Jessica Lahey

Jessica Lahey is an author, journalist and speaker. She writes a biweekly column for _The New York Times_ and is a contributing writer at _The Atlantic_ magazine.

