---
id: 578768bac6cd380003a5c5a3
slug: the-gift-of-fear-en
published_date: 2016-07-21T00:00:00.000+00:00
author: Gavin de Becker
title: The Gift of Fear
subtitle: Survival Signals That Protect Us From Violence
main_color: 1F2A9B
text_color: 1F2A9B
---

# The Gift of Fear

_Survival Signals That Protect Us From Violence_

**Gavin de Becker**

_The Gift of Fear_ (1997) provides insight into the mechanisms of fear, explaining how our instincts protect us from criminals by attuning us to universal signals and warning signs. Violence rarely comes out of the blue, and by recognizing some telltale signs, you will be better equipped to keep yourself out of harm's way.

---
### 1. What’s in it for me? Learn how to use fear to save yourself from harm. 

Have you ever had a bad feeling while walking home at night? Or maybe you've been made uneasy by a stranger on the subway? Maybe you stopped dating someone because of that person's refusal to take no for an answer.

The point is, we've all been in situations that didn't feel completely safe, and there's only one thing that keeps us from actually getting hurt — the instinct we call fear.

Most acts of violence aren't unexpected. The signs are usually manifest, and our gut helps us escape by sending a queasy feeling through our bodies. To really use the gift of fear, however, we need to learn how to identify these signals with even more accuracy, putting our intuition to full use. So let's look at these signals and discover how we can empower our natural instincts.

In these blinks, you'll find out

  * that people offering help may be using a predatory technique;

  * why victims of domestic violence stay in the relationship; and

  * that identifying a jutting chin could help you escape violence.

### 2. Your intuition is the best safeguard against danger. 

Have you ever woken up from a dream feeling like you've just had a premonition? Well, it's best not to ignore such feelings, since dreams are a vital part of human intuition.

Your dreams can even provide you with an intuitive sense of looming danger.

Michael Cantrell, a police officer, has observed this on the job and he takes it very seriously. One day, Cantrell's patrol partner, David Patrick, told him that he'd had a dream in which he got shot. Cantrell advised Patrick to pay attention to this dream and consider it a warning of possible future danger.

This was advice Patrick needed to take to heart; he often ignored intuition, a tendency that had put him in harm's way before.

For example, the two policemen had once pulled over a car with three men inside. While Cantrell had sensed danger in this situation, Patrick hadn't noticed anything unusual.

Cantrell noticed that the two men in the back seat refused to make eye contact and kept looking straight ahead, which he intuitively understood as a sign of danger even before his rational mind could react. Fortunately, when Cantrell asked the driver to step out, he noticed a gun on the floor of the car before any of the men could pick it up.

Meanwhile, Patrick remained oblivious to these signals, standing calmly beside the car and smoking his pipe.

Patrick's behavior demonstrates how ignoring your intuition, and thus the warning signs it picks up on, can have life-threatening consequences. Despite this incident, however, and his unsettling dream, Patrick didn't change his ways.

Later, Patrick was on patrol alone, and he noticed a pair of men driving by who resembled armed robbers that the police were looking for.

Instead of calling for backup, he confronted the men on his own. He pulled the car over and asked the driver to step out. While he was patting him down, the other man pulled out a gun and shot Patrick.

Luckily, he survived. The whole incident may have been avoided, however, had he heeded his intuition.

> _"Technology is not going to save us. We have to rely on our intuition, our true being."_ \- Joseph Campbell

### 3. Don’t be deceived by strangers who try to seduce you with charm and friendly behavior. 

Have you ever dropped your guard and placed trust in someone you thought was a friend, only to be double-crossed by that person later? Well, criminals use a similar tactic — befriending before betraying — as a means of getting what they're after.

This common trick is called _forced teaming_. The criminal pretends to be a friend to the victim, a form of deception that can only be seen through with highly developed intuition.

Such tactics are often very subtle. For example, one day a woman named Kelly was carrying a bag of groceries upstairs to her apartment when the bag ripped; she had bought a bunch of cat food, and the cans rolled down the stairs.

A man in the flight of stairs below collected the stray cans and offered to help bring them to her apartment. Intuitively, Kelly felt afraid and tried to decline his offer. But the man insisted, saying, "we have a cat to feed up there!"

At this point, Kelly should have gotten more suspicious; the stranger used the pronoun "we," a technique criminals use to gain trust and build a false sense of common purpose or togetherness.

Despite the man's use of forced teaming, however, Kelly's first intuition was spot-on: the stranger turned out to be a rapist and murderer and Kelly just barely managed to get away.

Another signal to look out for is a person's use of seductive charm.

Most of the time, charming people don't have sinister intentions, but, in the hands of a criminal, charm can be a powerful tool for disabling your intuition.

So, when you encounter charming person, try to determine if it's natural, or if the person is trying to use charm to manipulate you.

In Kelly's case, the man in her hallway appeared to be very friendly and courteous by offering to help carry her groceries. But her intuition told her that he was using his charm to deflect her natural resistance and get into her apartment.

> _"Just as a wise man keeps away from mad dogs, so one should not make friends with evil men."_ \- Buddha

### 4. Protect yourself from criminals by reading body language and using empathy to predict their behavior. 

You've probably met someone who can enter a room and have their presence felt by everyone. Maybe they strike an intimidating pose by standing tall or trying to take up as much space as possible. Whether they know it or not, these people use body language to give off signals, and it is something criminals do as well.

Potentially violent people give universal signs that you can pick up on.

Since most human body language is universal and performed on an unconscious level, such behavior is pretty easy to recognize. And so by consciously keeping an eye out for signs of aggression, you can prevent yourself from being caught off guard.

One of the most common signs of aggression is a jutting chin. Men tend to have larger chins, and they are also the main perpetrators of violent crimes.

Flaring nostrils are another sign indicative of imminent violence. Also, look out for people who have staring, unblinking eyes; this is another universal sign — a common prelude to a violent outburst.

It also helps to be empathetic, since being in tune with people's emotions allows you to assess the risk of being attacked.

If you're worried about someone turning violent, a good way to figure out the likelihood is to put yourself in their shoes.

For example, let's say you recently fired an employee and he's started send you threatening emails. If you look at things from his perspective, you can try to figure out what his goal might be.

If he wants his job back, he probably won't become violent since that would ruin his chances. On the other hand, if he's given up all hope and only wants revenge, there may be cause for concern.

Now that you know how to read some common danger signals, let's explore the signals you're giving off.

### 5. Avoid becoming a victim by recognizing a false threat and refusing to empower the perpetrator. 

If you were ever bullied as a child and sought advice on how to make it stop, you may have been told to stand up to the bully and show resistance. It's tough advice — but it actually is one of the best solutions.

Bullies and aggressively violent people feed on the fear of their victims.

We can also see this in cases of bomb threats. When a business or government agency receives a threatening phone call telling them a bomb is set to explode on their property, there's a tendency to respond with panic by quickly evacuating the premises. But this fear-based reaction is often exactly what the caller was looking for.

By reacting to a threat with blind fear, we give criminals the power to create chaos by making a simple phone call. Therefore, it's crucial to know when the threat is empty, and to be able to call a bluff.

Luckily, there are signs we can read in order to distinguish real threats from fake ones.

For example, if a person calls in a bomb threat by using an aggressively mean voice, this is a sign that the main purpose is to create fear and anxiety in the listener rather than to warn them of a serious threat.

Likewise, if the caller shows signs of being overly emotional, or yells and uses violent imagery, this might simply be an outlet for pent-up anger.

Real threats, on the other hand, come from criminals who are rarely dramatic. These people are patient and able to bide their time; otherwise, they would never have been able to plan something as delicate as a bomb attack.

So, when someone overexcited calls in a bomb threat, don't panic. After all, if doing actual harm was the main goal, a call probably wouldn't be made in the first place.

> _"Man is a coward, plain and simple. He loves life too much. He fears others too much."_ \- Jack Henry Abbott, American crime novelist.

### 6. Understanding signals is critical to identifying what is dangerous and what is not. 

As the saying goes, things are not always as they seem.

Take stalking, for example: sometimes stalkers are dangerous; other times, they're just harmless weirdos.

For instance, in 1980, John Searing, a middle-aged man who sold art supplies, sent the host of _The Tonight Show_, Johnny Carson, a letter asking if he could introduce him on air using the announcer's famous catch phrase, "Here's Johnny!"

When his first letter only earned him an autographed photo, Searing wrote again — and again and again — eventually sending off 800 letters. But none of these letters ever contained a threatening remark, and the tone never changed; he just kept on asking.

Ultimately, the producers became convinced of Searing's genuineness and harmlessness, and invited him on the show. Of course, Carson asked Searing if he would finally stop writing the letters now, and Searing agreed, happily returning to his day job.

But every case isn't as clear-cut as this, and real signs of potential danger can't be ignored.

A tragic example can be seen in school shootings, such as the one at Bard College at Simon's Rock, in 1992, which could have been prevented had troubling signs been properly handled.

The first sign was when a suspicious package for student Wayne Lo was delivered to the school. The receptionist was cautious when she noticed the words "Classic Arms" on it. But the school's dean decided not to open the package and risk violating student privacy. Instead, he sent dormitory supervisor Trinka Robinson to talk with Wayne.

Robinson asked Wayne if she could see the package's contents, but Wayne refused. And by the time Robinson returned with her husband, the package was empty and Wayne told them it had only contained harmless gun parts.

Then, that very night, an anonymous call came in, warning school officials that there would be a shooting. Despite all of these warning signs, however, the police were never alerted.

In the end, Wayne Lo shot six people, killing two of them.

### 7. Victims of domestic violence can lose their sense of fear by becoming addicted to the cycle of abuse. 

Drugs, alcohol and food aren't the only things that people can get addicted to. You might be surprised to learn that abuse can also become an addiction.

Oscillating between extremes is addictive, and a battered partner can come to rely on the feeling of well-being that follows whenever the abuse stops.

The relief that comes when the violence finally stops is so great that it creates an addictive emotional high that is more intense than normal happiness. So, in order to achieve that high, victims become reliant on abusive partners, giving them the power to control whether or not they are happy. This is why abused partners often remain in unhealthy relationships.

Ultimately, this kind of relationship dulls the battered person's natural fear instinct.

We see this in a case where a woman called a support line for victims of domestic violence. When the counselor asked if she was in immediate danger, the woman said no. But as the conversation continued, it became clear that the woman had locked herself in her bedroom, and the husband was on the other side of the door holding a loaded gun.

By continuing to live with repeated abuse, the woman's sense of fear had become so dulled that she didn't recognize the situation as dangerous. In order to feel genuine fear, she needed to be looking down the barrel of that gun.

By constantly living through violent and potentially life-threatening situations, victims of abuse no longer see their partner as a real threat. They lose the instinct to protect themselves or seek help from others.

On top of all of this is the urge to stay in the kind of secure environment that a family provides, even if a member of that family is perpetrating the abuse.

### 8. All too often, negligent school officials create an environment where children can sexually abuse other children. 

Sadly, there are constantly news stories about children being sexually abused by adults. But less reported are the stories of children being sexually abused by other children — and the fact that many of these cases could have been prevented.

Prevention can happen once school officials realize that they often have all the information necessary to take precautions.

For example, in one grammar school, a student by the name of Joey was caught sodomizing a seven-year-old boy in the bathroom.

This wasn't his first offense. School officials were warned by Joey's previous school that he had been disciplined for committing arson, wielding a knife, threatening to take his own life and the lives of others and for inappropriate sexual conduct with younger children.

But here, as is often the case with schools, the officials ignored the warning signs and failed to take appropriate measures to protect their students.

Even after the incident with the seven-year-old student, the school's principal failed to inform teachers or custodians about what Joey had done. And to make matters worse, when Joey showed signs of learning difficulties, the principal agreed to have him transferred to a class of younger boys.

They did take one measure to keep an eye on Joey, however: he was no longer allowed to use the bathroom unsupervised. Shockingly, though, it wasn't an adult who was told to monitor Joey; the duty fell to one of his younger classmates. And, sure enough, one month later, Joey raped another younger classmate in the same bathroom.

Incidentally, even if a school is well monitored by guards, having them around doesn't necessarily lead to safety.

It can actually make everyone feel that the problem of violence in the school has been addressed and that there's no need to remain vigilant. And if the school officials fail to inform the guards about potentially dangerous students, they can be of little help in preventing violence and abuse.

### 9. Final summary 

The key message in this book:

**Violence can be avoided. Every human being has a unique mechanism designed to warn them of impending danger and allow them to get to safety. That mechanism is called fear, and it is a gift. In order to stay safe, learn to listen to your fear and intuition without hesitation.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Better Angels of Our Nature_** **by Steven Pinker**

_The_ _Better_ _Angels_ _of_ _Our_ _Nature_ takes a close look at the history of violence in human society, explaining both our motivations to use violence on certain occasions and the factors that increasingly restrain us from using it — and how these factors have resulted in massive reductions in violence.
---

### Gavin de Becker

Gavin de Becker is the founder of a security firm that provides services to some of the top government officials in the United States. A pioneer of crime-prevention law, he is a sought-after expert in criminal cases, including the O.J. Simpson trial.

