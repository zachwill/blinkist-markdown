---
id: 54cf907e323335000a5e0000
slug: the-gifts-of-imperfection-en
published_date: 2015-02-05T00:00:00.000+00:00
author: Brené Brown
title: The Gifts of Imperfection
subtitle: Let Go of Who You Think You're Supposed To Be and Embrace Who You Are
main_color: B02326
text_color: B02326
---

# The Gifts of Imperfection

_Let Go of Who You Think You're Supposed To Be and Embrace Who You Are_

**Brené Brown**

_The Gifts of Imperfection_ offers an accessible and engaging walk through the ten principles that you can follow to live a more fulfilling life, defined by courage, connection and compassion towards others. Filled with relatable anecdotes and actionable advice, the book is a useful resource for readers both young and old.

---
### 1. What’s in it for me? Learn how to live a more fulfilling life by being true to yourself. 

What would you say is your biggest flaw? Perhaps you're an inveterate procrastinator. Maybe you feel like you've failed as a parent. Whatever it might be, you're not alone. We all have our flaws and foibles. It's how we deal with them that matters.

In her research on shame and its effects on the human mind, author Brené Brown has gathered thousands of stories from people around the US. From these stories, a powerful pattern has emerged: Those who find ways to embrace their imperfections, lead happier, more fulfilled lives than those who don't. While being aware of your flaws is an important first step, it's not enough. The true transfomative power lies in learning to _love yourself._

That's easier said than done. So how do you do it? Let's find out.

Along the way, you'll learn

  * why everything looks better when you zoom out;

  * why you should devote a decent portion of your life to play; and

  * why you should define yourself with slashes.

### 2. Authenticity is a choice that requires courage, compassion and connection. 

Most people would like to live a life that is true to who they are; in other words, we'd like to be as _authentic_ as possible.

Unfortunately, a handful of factors stand in the way: for example, a lack of self-confidence or pressure to conform. As a result, we feel we are _inauthentic_ people, too weak to live honestly. But this is simply untrue!

Authenticity isn't a quality that you either have or don't. Rather, it's a choice, one that reflects how we want to live. It's the daily decision to be honest, embrace our vulnerability and to not care what others think.

And because it's a choice, we thus have the option to be authentic on some days and less authentic on those other days when we're too tired.

If you do, however, choose to act with more authenticity, then you'll need to practice courage and compassion.

You'll need the courage to speak your mind and allow yourself to be vulnerable in front of others. To look at this in practice, think about the next time you really want something to happen, like winning a contest or nailing an interview. Try not to play down your hopes in these situations. Acting like failure is no big deal won't make the pain of failure any easier. In contrast, being honest about your hopes makes it possible for you to find support when you need it.

Furthermore, exercising compassion allows you to recognize that you aren't alone, and that, in fact, everyone around you struggles with the exact same issues as you.

Compassion, in contrast with sympathy, is a relationship between equals: in order to relate to the struggles of others, you have to acknowledge your own, as well. By understanding that everyone around you has likely gone through what you're going through now, you'll have an easier time opening up to them and finding support.

> _"Authenticity is a collection of choices that we have to make every day."_

### 3. Fear of shame hides behind perfectionism. 

Are you a perfectionist? If you are, do you consider it to be a positive quality?

Perfectionism, despite maybe sounding positive, isn't worth pursuing. It's different than striving to be your best, and is unrelated to self-improvement. Rather, it revolves around the fundamental fear of shame.

_Perfectionism_, in short, is the belief that, if we look perfect and live and act perfectly, then we'll be able to shield ourselves from criticism, judgment or blame. This shield, in other words, is supposed to protect us against shame.

However, life as a perfectionist is emotionally unhealthy, because it makes our own self-worth dependent on approval or acceptance from others. Not only is perfectionism unhealthy, but it's also addictive and self-destructive. In fact, perfectionism is futile, as perfection itself is illusory!

The perfectionists' mindset, however, doesn't recognize these traps. Instead, whenever they inevitably fail to achieve perfection, perfectionists blame themselves for their inability, and tell themselves to "do better," regardless of whether that's actually possible.

They become, in effect, addicted to improvement.

Perfectionism can also lead to _life paralysis_, that is, the inability to put oneself out into the world, due to fear of imperfection. People suffering from life paralysis might, for instance, be unable to send that email to someone they admire out of fear it won't be well received, or might leave their writings unpublished out of fear of criticism.

Luckily, we can avoid the constraints of perfectionism by simply being honest about our fear of shame and by reminding ourselves to do things _for ourselves_ rather than for others.

So the next time you want to get fit, for example, don't let others' opinion of you and your body be your motivation. Instead, tell yourself that exercise and a healthy diet will make you feel better and healthier, and that your success or failure in getting fit won't affect your worth as a person.

### 4. Cultivate purpose and perspective so you are resilient in the face of adversity. 

How many of us have tried to lose weight, but instead give up at the first sign of trouble? So many of us lack the _resilience_ necessary to achieve our goals. Luckily, we can change that tendency. Let's start by looking at where resilience originates:

Resilience comes from practicing _hope_. While hope is often considered an emotion based on circumstances outside of our control, researcher C.R. Snyder argues instead that hope is actually a cognitive process that can be both learned and practiced.

Hope comes from telling yourself _where_ you want to go, recognizing _how_ you can get there and telling yourself that you have what it takes to succeed. You can make the light at the end of the tunnel appear closer or brighter by dividing larger goals into smaller, more manageable ones.

Next time you face a daunting challenge, like giving up nicotine, make a conscious choice to take it one day at a time. Thinking about your efforts for a day is easier than thinking about it for a year or the rest of your life. And once the habit of not smoking sinks in, your resilience will build on itself.

Resilience can also be developed by adopting a critical, broadened perspective towards the adversity you face. It's easy to feel terrible when the camera is zoomed in on you, and all you see are your "imperfections." But if you pan out a bit, you'll start to see that you are surrounded by people who share your struggle.

Many people who have issues with body image as a result of media pressure, for instance, could benefit from adopting a broader perspective.

They should ask themselves: are the images that I'm seeing real or fantasy? Am I the only person who feels dissatisfied with my body after seeing these images?

Answering these questions can help people remain critical, see that they aren't alone in their struggle, and resist societal expectations, instead refocusing on self-worth.

### 5. Practice being grateful for the ordinary moments in life. 

It's common sense that you're happier when you're grateful for the things you have, rather than when you're lamenting that you don't have enough. This gratitude, much like hope, is not an accidental emotion, but is a mindset that can be consciously trained.

While most people think of gratitude as the feeling that follows positive experiences, in reality it is a _practice_ that fosters happiness. This has major implications for how we live our lives: it means that joy isn't merely the accidental result of external conditions over which we have no control. Rather, we actively _choose_ joy by practicing gratitude.

One way to actively choose gratitude is by telling yourself that what you have is enough — or more than enough — rather than seeing everything in terms of scarcity.

We often fall into the bad habit of faulting ourselves for not having enough: we're not rich enough, not thin enough, we don't have enough time and so on. Instead, we should focus on the things we _already_ have, and understand that we could have less. With the grateful perspective, you'll soon find yourself feeling more grateful no matter how prosperous you are.

But above all else, the key to gratitude is to find value in the ordinary moments that make up your life — things like tucking your child into bed, sharing a good meal or walking home on a sunny day.

That it is a choice to have a grateful perspective helps people who have experienced severe trauma or sadness. People who have undergone intense, traumatic experiences, such as the loss of a child, violence or genocide, attest that they tend to remember fondly the mundane aspects of daily life before the traumatic experience.

### 6. To be a better decision-maker, let go of the need for certainty and trust your intuition. 

Your intuition is one of your greatest resources in decision-making. Unfortunately, however, many people find it difficult to trust their intuition.

In part, it's because most people don't understand what intuition is. They think of it as a "gut" feeling that has nothing to do with rationality or reason. In reality, intuition and reason are not mutually exclusive.

Rather, intuition works like a rapid-fire series of associations that happens unconsciously. How so?

When your brain makes an observation, it goes through your catalogues of memories in order to find relevant information. This information is compiled into the unconscious "gut feeling" that informs your actions.

It is this exact process of unconsciously drawing on previous experiences that allows athletes, like basketball players for example, to know the precise angle and force they need to shoot a three-pointer without having to sit down first to do the math.

So, we can't think of intuition as the _opposite_ of reason. Rather, intuition is simply a way of reasoning that leaves room for uncertainty when making decisions.

By embracing your intuition, you put trust in both yourself and the experiences that have contributed to your knowledge. This enables you to act with a degree of confidence despite not knowing how a situation will play out. The basketball player, for example, can't be _certain_ that the ball will swish through the hoop, but he _can_ make an educated guess based on his intuition.

But why should you want to trust your intuition? Simply put: doing so can help you overcome your fear of risk.

Most people avoid risk and uncertainty, leading them to act hesitantly and make poor decisions. By learning to embrace intuition, you'll become accustomed to taking action in the face of uncertainty, and thus persevere through the fear of making the wrong decision.

> _"Intuition is not a single way of knowing."_

### 7. Embrace your own creative potential to do away with the need for comparison. 

Comparing ourselves to others is totally natural, and something that we all do. However, in our attempts to measure ourselves against our peers, we often end up actually ridding ourselves of the very qualities that make us interesting individuals.

Indeed, comparison is the very root of conformity. While competition and conformity might at first sound like polar opposites, they're actually inextricably related.

Whenever we compete, we necessarily compare ourselves to others by means of very narrow criteria. For this reason, we won't bother to compete with people from entirely different traditions and backgrounds, yet get riled up about the very people who live next door.

While we might not compare our homes to the mansions across town, we're likely to compete over who has the best kept lawn on the block.

However, because we only compete with those who are similar to us already, we ensure that we will follow the path of conformity.

If we want to transcend these arbitrary comparisons, we must start by embracing our own individuality. When we focus on our own unique gifts, it reminds us that the world consists of individuals, each of whom make unique and incomparable contributions.

But to let your individuality shine, you'll first need to cultivate your creativity.

What if you aren't creative?

Hogwash! There's no such things as "creative types" and "non-creative types." But there is a clear distinction between those who make use of their creativity and those who don't.

So don't get hung up on whether you're creative enough. Just get out there and create! It doesn't matter if you paint, cook, write, make music or whatever else. As long as you're creating, you're also cultivating your individuality.

> _"The comparison mandate…[is] not cultivate self-acceptance, belonging, and authenticity; it's be just like everyone else, but better."_

### 8. When it comes to your well-being, play and rest are just as important as work. 

If someone told you to drop everything and go play — right now! — could you do it? Or would your other obligations keep you from leaving your desk? Making time to play isn't as easy as it might seem.

Our society has the bad habit of tying self-worth to productivity. As a result, we end up sacrificing things like rest, play and our general well-being if those things appear to get in the way of our work.

When was the last time you told yourself to stay up just one hour more to get more work done, despite barely being able to keep your eyes open?

People think of work and play as being polar opposites. They think that, by getting rid of one (usually play), they'll have more of the other.

The fact is, however, that the opposite of play is _not_ work, but depression. Humans, as has been verified by the author's research, are biologically programmed to engage in "purposeless" activity, meaning play. To deprive ourselves of play is to do ourselves a great disservice.

In fact, cranking up the amount of play and rest you give yourself can actually make you more productive by bringing back excitement and novelty to your job, while also fostering empathy and creativity.

One way to do so is by playing with your coworkers. For example, you could make it a habit to go out with colleagues to shoot some hoops after work every Friday. Not only will the exercise and fun make you happier and healthier, you'll develop a connection with your coworkers in your professional context and improve your ability to work within your team.

The same goes for rest. If you always push yourself to exhaustion, then your work — and your well-being — will inevitably suffer. So pay attention to your body and its needs!

### 9. Learn to manage your anxiety, rather than trying to get rid of it. 

Life in our fast-paced, stress-filled society makes for a lot of anxiety. For many people, perhaps even you, the nervousness and restlessness that come with anxiety can be practically paralyzing.

We are most affected by anxiety when we inadvertently allow it to become an integral part of our lifestyle. This often occurs when we try to balance too many things at once without allowing ourselves to take a step back and put everything into perspective.

Think about those days when everything seems to come tumbling down around you: you have to plan for an upcoming deadline at work while also thinking about what to cook for dinner, when to pick up your child from daycare and how you'll finish writing all those holiday greeting cards in time — all while trying to stick to your daily fitness regimen.

When we're constantly reminded of everything we need to take care of in the short time that we have, anxiety can become an ever-present aspect of life.

Dealing with anxiety, however, doesn't mean ridding ourselves of it, or even trying to avoid it. Rather, we must simply be aware of it in order to prevent it from becoming habitual.

The next time you feel anxious, try approaching your anxiety with a broader perspective. Breathe slowly and focus on the moment rather than on an unknowable future — besides, who's going to care if those holiday cards arrive a few days later?

By simply taking the time to think and acknowledge your anxiety, its source and its ultimate importance, your anxiety will transform into something that is manageable, rather than something that defines your life.

### 10. Identify your own gifts and talents that you can share with the world. 

How many times did your parents or teachers tell you to quit spending all your time drawing, singing or playing and to instead do some real work? It's high time you ignored their advice!

We all have unique gifts and talents that belong to us alone, and we should embrace them, rather than ignore them for the sake of getting "real work" done. Some of us, for example, might be artistically gifted, while others might be particularly good at conversation. Others might have a unique talent for remembering sports statistics.

These gifts and talents are not always necessarily the ones that pay the bills. Nevertheless, by identifying the unique things that we can share with the world and incorporating them into our lives — even if only marginally — we make our lives that much more meaningful.

So don't be afraid to pursue that hobby or activity that you've always wanted to pick up but thought might distract from what you "really need to do." The truth is: you can have your cake and eat it, too!

You can incorporate your talents into your everyday life by seeing your career in terms of "_slashes_."

For example, if you work as an insurance broker by day but also like to write novels in your spare time, you don't have to undersell your literary talents by introducing yourself as an "insurance broker who dabbles in writing on the side."

Just because your passion isn't what you spend most of your time working on doesn't mean that it's any less a part of your identity. So instead, tell people that you're an "insurance broker _slash_ writer."

### 11. Don’t be afraid of being uncool. 

In this age of social media, the pressure to present ourselves as cool, blasé types with adventurous lives and without a care in the world has never been stronger. But it's precisely this desire to be "cool" that keeps us isolated from others.

We need to be connected to others, and the best way to do so is through laughter, song and dance. These three activities create emotional and spiritual connection with those around us, and allow us to feel we're not alone.

In her book _Dancing in the Streets: A History of Collective Joy_, Barbara Ehrenreich explains that, throughout human history we have demonstrated an urge to share joy with others by engaging in "collective ecstasy."

So, when we laugh, sing or dance, we're engaging in the same primal activity that affirms our place within the larger human community.

All three activities, though, require us to let go in some way.

Admit it: we've all had that awkward experience when we've laughed a bit too hard, sang a bit too enthusiastically or danced a bit too intensely for the tastes of those around us — and then felt the immediate pangs of humiliation when told to "bring it down a notch."

Laughter, song and dance all require a full-body vulnerability that few of us want to risk, thus leading us to confine those activities to the privacy of our own homes or amongst trusted friends and family.

We have to tell ourselves that it's okay to be uncool; it's part of the opportunity to cultivate connection with others.

The only way to maintain the façade of coolness is by putting down those who are perceived to be "not as cool," and to do so at the expense of genuine connection. But by throwing caution to the wind and allowing yourself to wholeheartedly enjoy activities like laughter, song and dance without reservation, you lose the need to criticize others and gain an opportunity for genuine connection.

### 12. Final summary 

The key message in this book:

**Living a happier, more fulfilling life is easier to accomplish than you might think. It takes practice, not miracles. Ultimately, it boils down to cultivating your courage so that you can approach others, as well as yourself, from a place of sincere compassion.**

Actionable advice:

**Make some time for yourself.**

No matter what, take some time _today_ to do something that you love. It doesn't matter what your obligations are, if you don't make time for yourself and your hobbies, then you will suffer in the long run.

**Check "take a nap" off your list.**

Look at your to do list and cross off one item, instead replacing it with "take a nap." Rest is something we all neglect, and if you spend all day every day in high gear, you'll soon find yourself too worn out to do much of anything. So get some rest!

**Suggested** **further** **reading:** ** _Daring_** **_Greatly_** **by Brené Brown**

_Daring_ _Greatly_ explores how embracing one's vulnerability and imperfection is necessary for achieving real engagement and social connection. Through explaining our deep-seated reasons for shame, and showing how to embrace our vulnerability, the author aims to provide guidance for a better private and professional life, and to initiate a fundamental transformation in our shame-based society which, according to the author, needs to adapt a new culture of vulnerability.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Brené Brown

Brené Brown is an author and professor at the University of Houston Graduate College of Social Work, where she specializes in research on shame and its effects on the human mind. Her work has been featured by a diverse selection of media, including PBS, NPR, CNN, OWN, and TED.

