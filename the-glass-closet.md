---
id: 58581067d1a2f30004796853
slug: the-glass-closet-en
published_date: 2016-12-22T00:00:00.000+00:00
author: John Browne
title: The Glass Closet
subtitle: Why Coming Out Is Good Business
main_color: F08B83
text_color: 995954
---

# The Glass Closet

_Why Coming Out Is Good Business_

**John Browne**

_The Glass Closet_ (2014) details the struggles that lesbian, gay, bisexual and transgender people face in the workplace and how these struggles can be overcome. Importantly, these blinks will show how coming out can change people's lives for the better.

---
### 1. What’s in it for me? Learn the truth about being gay in the modern workplace. 

Do you have any employees in your company who are gay? Have you ever wondered if any of your coworkers are afraid to truly be themselves — and reveal their sexual orientation openly at work?

Sharing his experiences as a gay executive, the author offers valuable insight into today's environment for gay and transgender employees in the workplace. In these blinks, you'll learn how employees and companies can thrive when they embrace people's differences in sexual orientation.

In these blinks, you'll discover

  * which countries continue to discriminate against gay and transgender people;

  * how Apple's CEO feels about gay marriage; and

  * why companies that support gay and transgender rights are successful.

### 2. People throughout history have been ostracized, imprisoned and even killed because of their sexual orientation. 

Despite many developments toward expanded equal rights, homophobia is still a real issue in society.

In some countries such as Uganda, gay people have no legal protection. If authorities discover that someone is gay, that person could be sent to prison, or worse, even killed.

Yet persecution based on sexual orientation wasn't always a fear. In ancient Rome and Greece, gay people were treated equally. Some societies even considered gay people sacred.

The Roman emperor Hadrian lived as an openly gay man. Greek gods such as Zeus were depicted in fables as taking male lovers; believers saw such acts as normal.

The spread of Christianity, however, changed the public's perception of homosexuality. Many believers have used their interpretation of the book of Leviticus in the Bible to justify the repression and persecution of people based on sexual orientation.

Such religious condemnation has played a major role in branding gay people across the world as "sinners," making them the targets of Christian violence.

But such horrors aren't limited to ancient history. Since the seventeenth century, gay people have been used as scapegoats for many of society's troubles. Deadly earthquakes, the bubonic plague and more recently AIDS have all been blamed on the influence of gay people in society.

What's more, in Western nations such as Germany and the United Kingdom, being gay was a crime until very recently.

During the Nazi regime in Germany, thousands of gays and lesbians were sent to concentration camps; they were given a pink triangle to wear, identifying them to others and signifying their low status in society.

Even after World War II, being gay in Germany remained a crime for decades. Homosexual relations between men in England was made legal only in 1967.

### 3. While “coming out” at work is mostly accepted, many gay professionals still worry about their careers. 

Take a moment to consider this question honestly: can you truly be yourself at work? Do you feel you have to hide any part of your personality from your coworkers?

Most people feel comfortable at work, just as they do at home. But the same often isn't true for gay people. The feeling that one has to hide or disguise one's true identity is a situation with which many gay people around the world are familiar.

People are working to change this situation, however. Many Fortune 500 companies are actively trying to improve their business culture to create an environment in which employees feel comfortable coming out.

Some of these companies have established support groups or hang posters in the workplace that inform employees that they are accepted members of the corporate team, whatever their sexual orientation.

Apple CEO Tim Cook, for example, has publicly stated that his company fully supports marriage equality. Such a clear defense of equal rights for gay people allows employees more freedom to express their opinions and not feel they have to hide their identity from others.

Yet many gay people still fear that coming out at work will ruin their careers. One reason for this is that there are few openly gay role models higher up on the corporate ladder. Overall, there is a distinct lack of gay, bisexual or transgender executives to whom employees can look for inspiration and strength.

If employees do happen to find a highly successful gay executive, that executive is likely to have been attacked at some point in his or her career. The author resigned his position at energy giant BP, for example, after an ex-boyfriend leaked a story about the author's sexual orientation to the British newspaper _Daily Mail_.

This exposé not only hurt the author personally and professionally but also gave other gay people the impression that they too might face a similar situation if they dared to come out at work.

> **_"_** _As I know from decades of hiding my sexual orientation, closeted people usually assume that coming out will have dire consequences."_

### 4. Coming out at work can be a plus for gay coworkers, yet not every experience is positive. 

The number of people coming out at work grows daily. What's more, these openly gay, bisexual and transgender employees through the process of coming out are living more fulfilled lives overall.

Coming out has tremendous benefits, whether at work or in general. Living life "in the closet" requires inhuman energy, as a person lives in constant fear of being "outed."

For instance, LGBT activist Louise Young has developed a productivity index that suggests closeted gay employees are some 10 percent less productive than heterosexual coworkers. After coming out, gay or transgender employees can channel more creative energy into the work at hand.

Coming out could even help an employee improve the productivity of his or her entire team. Kirk Snyder, an associate professor at the University of Southern California, through his research, suggests that gay managers might treat employees more inclusively and better motivate colleagues than heterosexual counterparts. As gay managers have experienced being part of a minority group, Snyder contends, they can be more open and fair with others, especially other minorities.

Interestingly, employees working for openly gay male managers report significantly higher satisfaction than employees working under heterosexual male managers.

But while coming out has its benefits, not all gay or transgender people have had a positive experience when revealing their sexual orientation to coworkers. Some are mocked; some find they earn less than heterosexual peers; some are victimized by homophobic bosses and even fired.

Gay men in particular often are ridiculed by peers who irrationally believe that they are "by nature" prone to acting flirtatious — and in some instances, they are seen as predatory.

Since this prejudice still predominates in society, it influences working environments too, and can determine whether gay people are hired or promoted within a company.

> Transgender people still suffer discrimination. In 2013, the rate of unemployment for transgender people was at least twice as high as that of heterosexual people.

### 5. Espousing a culture that supports employees of different sexual orientations is a win for companies. 

Would you want to work in a company that doesn't support you? Would you buy a product from a company that you know has no interest in supporting equal rights?

Probably not. Unsurprisingly, gay, bisexual and transgender people are far more likely to work for and buy from businesses that respect their sexual orientation and equal rights overall.

In fact, gay, bisexual and transgender people not only pay attention to the progressive values of the companies for which they work but also often know which companies are active in the fight for equal rights for gay people.

Some 58 percent of gay adults say they're more likely to buy from companies which actively support equal rights for gay, bisexual and transgender individuals and market specifically to these communities.

To help them make informed decisions, many gay consumers consult the Buying for Workplace Equality Guide _,_ which is published by the Human Rights Campaign. This guide details which companies have the best records of not only promising to support equal rights for gay people but also living their commitments.

Creating a more gay-friendly environment isn't just good for a company's bottom line. It also helps companies attract other minorities, such as employees from different racial backgrounds, therefore diversifying a company's workforce for the better.

Here's an example of this policy in action. Claudia Brind-Woody is the co-chair of IBM's LGBT diversity task force. At a job fair, she noticed that an unusually high number of young Asian women were visiting the IBM booth.

When she asked some of them why they were interested in working at IBM, they said that if the company was open in its support of gay rights, it was probably also welcoming of minority groups, too.

### 6. For employees to feel comfortable coming out, they need to know that their leaders support them. 

Do you know whether your company supports equal rights for gay, bisexual and transgender people?

If you don't know, then it's time your company makes more vocal its message to not only its employees but also the market at large.

If employees are unaware that their employer is working to improve conditions for gay, bisexual and transgender people, many will choose to remain in the closet.

Companies need to communicate that coming out is not only safe to do, but also will be celebrated. Even if there is some pushback from employees who may harbor homophobic feelings, a company needs to stress that the corporate culture is both opposed to and will address hate in the workplace.

When a company is actively working to improve conditions at the office, employees will know that efforts at diversity and inclusion are real. One way to do this is to share stories of other employees who have come out and been received positively by coworkers.

Support of equal rights for gay people also needs to be an active topic of discussion among company leaders, so that correct policy trickles down to all levels of an organization.

Many business leaders already support gay, bisexual or transgender employees or have already revealed their sexual orientation publicly. While such information is crucial in making employees feel comfortable coming out at work, it often doesn't reach the rank and file of an organization.

Thus leaders need to speak out to show the whole company that they will stand up for every employee regardless of sexual orientation. Vocal support from gay and straight allies alike is powerful. And even when company executives are heterosexual, their open support of equal rights for gay people is crucial toward creating a more inclusive, welcoming workforce for everyone.

Leaders at Google, for example, are vocal about their commitment to diversity within the workforce. Every year the company officially participates in gay pride parades.

> _"In order to compel change, leaders must cast diversity and inclusion as a business issue, not as an ancillary function."_

### 7. Gay public figures have historically been ostracized after coming out, but times are finally changing. 

Thomas Hitzlsperger, a famous German soccer player, came out as gay in 2013. The announcement was met with overwhelming support, a sign that society is finally becoming more accepting of gay people.

This marks a significant change from just a few decades ago when athletes, politicians and other public figures would face severe criticism for coming out. Many as a result chose to remained closeted for fear of destroying a career.

Billie Jean King, a former professional tennis player, had won several Grand Slam titles by the time she came out in 1981. That same day she lost all her professional sponsorships, worth over $2 million.

Today even the world of sports has embraced gay athletes. What's more, the public's response to prominent athletes coming out has been largely positive.

Certain professional fields tend to be much more supportive of equal rights for gay people than others. While gay people have long occupied executive positions in the world of publishing, for example, coming out as a professional athlete was just not done until recently.

Many sports teams and even sports brands such as Nike now are proud to support gay athletes, standing up against discrimination and hate. This sort of support is reflected in society, as people come to accept openly gay athletes more and more.

In 2013, British Olympic diver Tom Daley came out publically. Rather than receiving a stream of hate mail as he feared he would, Daley was embraced with positive messages and support. Former athletes even congratulated him on his extraordinary bravery.

### 8. Final summary 

The key message in this book:

**Coming out can be one of the most powerful experiences in a gay person's life, but it can also be one of the most dangerous. While many countries have enacted laws to ensure equal rights for gay people, there is still much work to be done in society, and especially in the workplace. Employees and employers alike should strive to make the office a safe space for everyone, regardless of sexual orientation.**

Actionable Advice

**When a person comes out, be considerate and thoughtful.**

If someone trusts you sufficiently to come out to you, it's important to be kind and considerate, and above all, to honor that person's trust. Tell the person that you support them and ask how best you can be there for them.

If you don't know how to immediately respond, don't panic. If faced with feelings that are conflicting, instead of speaking rashly, calmly explain that you need time to reflect. Doing so will give you needed space so you can respond to your colleague rationally and positively.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Everyday Sexism_** **by Laura Bates**

_Everyday Sexism_ (2014), explores why sexism is so deeply entrenched in society, from sexual assaults against women to the stark differences in the ways boys and girls are raised. These blinks show how sexism is harmful not only for women but also for men; and you'll learn how to combat sexism to create a more peaceful, equitable world.
---

### John Browne

John Browne is the former CEO of British Petroleum (BP), a company he served successfully for 40 years. In 2007, he resigned his position following revelations by a British newspaper that he is gay. This incident, and the expressed relief he felt after publicly coming out, inspired Browne to encourage others to be more open about their sexuality.

