---
id: 580b7073d4d2ce0003dd2493
slug: the-global-code-en
published_date: 2016-10-27T00:00:00.000+00:00
author: Clotaire Rapaille
title: The Global Code
subtitle: How a New Culture of Universal Values Is Reshaping Business and Marketing
main_color: B5C1E7
text_color: 646B80
---

# The Global Code

_How a New Culture of Universal Values Is Reshaping Business and Marketing_

**Clotaire Rapaille**

_The Global Code_ (2015) is about a recent worldwide phenomenon: a global unconsciousness, or code, that contains a new system of values, beliefs and principles. This code is formed and shared by the Global Tribe, a highly mobile and cross-cultural group of people who are setting trends, shaking up the old status quo and becoming the target demographic for global luxury brands.

---
### 1. What’s in it for me? Learn about the Global Tribe that influences our world. 

As you know, a tribe is a societal group that has evolved outside a state. It is a group of people who live with no specific connection or attachment to nationality, who have formed their own mode of self-sustained living.

Now, you might think of the Apache or the Cherokee, envisioning a shaman beating a drum beside a bonfire. But there are other, less obvious tribes out there.

For example, in recent decades a new tribe has come into existence. It's called the Global Tribe and it has a major influence on our everyday life, creating a shared, global code.

You didn't know? Well, that's what these blinks are all about.

In these blinks, you'll also discover

  * how Bill Gates and Mark Zuckerberg are related to the Global Tribe;

  * if you are an unwitting member of the Global Tribe; and

  * why, according to the tribe, "hand-made" is better than "Made in China."

### 2. These days, there is a Global Tribe of trendsetters following a new cross-cultural code. 

In recent years, new technologies and changes to the way we travel have led to an unprecedented level of international interconnectedness. Smartphones provide information to people around the world at the tap of a finger, and inexpensive airlines shuttle people to far-flung destinations.

With this new interconnectedness, a new phenomenon has emerged called the _global unconscious_, or the _Global Code_.

Of course, every culture still has its own unique elements; people across the globe maintain their own way of speaking, working and cooking. Yet there are also cross-cultural experiences that we share: people go to school, find a job, get married, raise a family. And no matter where you are, most people share a similar dream of finding a better place to live, a place that will offer a better career, a nicer house and an ideal environment for their children.

These shared dreams and beliefs form a Global Code that has emerged from today's interconnected world. Media and travel have given rise to a multicultural _Global Tribe_ that follows and finds inspiration in the goals and aspirations of others.

The Global Tribe is a small group of highly connected and highly mobile influencers who aren't bound to one specific society or culture. Instead, the Global Tribe adheres to a truly Global Code that takes its ideas and inspirations from worldwide sources.

Since this tribe is made up of influential individuals, the pieces of culture that are absorbed into their code become highly popular throughout the world; the tribe is an international trendsetter.

For example, if the tribe decides that a certain brand of clothing or social network is superior, people across the world will follow suit and rush to buy or use that product — and, in no time, these choices will become the hottest items on the market.

The power of the tribe has not gone unnoticed. Indeed, companies now create and market their products and services with the Global Code in mind.

> _"What the members of the tribe have in common is that they live in hubs, travel all the time, benchmark the world, and in doing so, create the Global Code."_

### 3. The hierarchy of the Global Tribe has six levels, starting with the court, the courtesans and the suppliers. 

Most organizations have some sort of hierarchy, and the Global Tribe is no exception. It has a distinct hierarchy that's not unlike the kind you might find in a kingdom or royal empire.

This hierarchy is made up of six layers. The first three are: _the court_, _the_ _courtesans_ and _the suppliers_.

As with any royal structure, we find _the court_ at the very top.

Those in the tribe that belong to the court are the ones who symbolize success, money and fame. While everyone knows who these people are, only a select few actually know them personally.

The members of the court are the real tastemakers: they decide what music and fashion is "in" and they define the "cool" ways to act and behave.

Well-known members of the court include Mark Zuckerberg, Bill Gates, the Lauder family and the Koch brothers.

On the second tier of the hierarchy are the courtesans. They aspire to be members of the court and, like Kate Middleton, they might marry a prince to get in. Others strive to reach this status by following in court members' footsteps — by attending the same universities, say, or going to charity events.

Unlike the members of the court, courtesans are eager for attention and good press; they, like all up-and-comers, crave the spotlight.

The third tier is where we find the suppliers. These are the ones who provide luxurious items to the court, whether they're watches or cars. They throw parties for the court and try their best to please them and predict what they'll want to buy next.

Famous suppliers for the Global Tribe are high-end brands like Cartier, Rolex, Chanel, Rolls-Royce and Bentley.

At the top of this category are the realtors who are eager to help court members buy and sell properties. They also like to predict the court's needs by showing them ads for homes in Newport Beach while they're vacationing in Aspen.

### 4. At the bottom half of the hierarchy are the symbolic creators, the third culture individuals and the aspirants. 

As we continue to work our way down the hierarchy of the Global Tribe, we hit the fourth tier, which is made up of the _symbolic creators_.

These are artists, thinkers and philosophers — the ones who create symbolic and artistic value for the Global Tribe.

Just as Louis XIV and the court at Versaille had creative people to help provide style and elegance, the court of the Global Tribe also benefits from the help of symbolic creators.

Oscar Niemeyer, a Brazilian architect, is a good example of a more recent symbolic creator. He influenced modern architecture by working for the country's elite and designing their capital.

Coco Chanel is another. She changed the face of fashion and was a go-to designer for the rich and famous.

After the artists, we have the fifth tier, which contains _the third culture individuals_.

This category refers to the children of the Global Tribe, people who were born to parents that originated from one culture but had children after moving to another. Third culture individuals tend to spend a lot of time moving around and living in many different cultures.

Since these people might come from a military family or from parents who had to move around for business reasons, they may have less money than the others in the tribe. Third culture individuals also tend to be more tolerant and open-minded than people from a monoculture family.

Finally, we have _the aspirants_, or the social climbers. 

Aspirants live outside the tribe but are desperate to become members. While they have the money to try to become a member, they hold back out of fear that they don't have permission or understand the rules of the Global Tribe.

Aspirants still have things to learn — like how to speak about topics such as art, opera and polo matches or how to mingle properly at a charity event.

> _"Of course these are more symbolic than real, but these different levels still have the capacity to inspire the masses."_

### 5. Members of the Global Tribe follow the rule of three, which makes them international people with a big network. 

After the first few blinks, you might be thinking about how you can become a member of the Global Tribe.

Well, one of the first things you have to do is adhere to _the rule of three_.

The rule of three means that every member of the tribe must have at least three places, be it a city or a hub, that they call home. And if they don't own property in these places, then the rule says that the member must have lived in three different countries, speak three different languages and be familiar with three different cultures.

For example, let's look at Jean-Paul, a prime Global Tribe member. His mother is French and his father is American, which gives him two nationalities. As a result, he's lived in both France and the United States, but he also lived in Toronto, Canada, during his university years, and worked in Madrid for four years.

Given his background and his job in Madrid, Jean-Paul speaks English, French, and Spanish, but he's also familiar with Italian thanks to his Italian girlfriend. All of this has given him a great network of friends, classmates and colleagues from all around the world and makes Jean-Paul a certified member of the Global Tribe.

And people like Jean-Paul, who have a large global network, tend to stay with friends when they travel, instead of in hotels. This is generally true of Global Tribe members. With mobile phones, email and platforms like Facebook and Twitter, it's easy for them to stay connected and maintain their international contacts.

It also helps that members of the Global Tribe often live in conveniently located and internationally connected places.

These places are large cities or hubs that offer big airports which provide direct flights to other well-connected hubs in the world. They're also business-friendly cities, the kind that support free trade and are strategically placed to connect people with goods and services.

Big cities that meet these qualifications are Hong Kong, Singapore, Dubai, Monaco and Luxembourg.

### 6. The Global Tribe sets society’s standard of luxury as something that is unique and handmade. 

As we saw in the previous blinks, it's the top members of the tribe within the court that set the trends. But it's not just other members who follow their lead; everyone follows the standards of luxury that the court sets.

Since the world is so interconnected these days, everybody can carefully follow the Global Tribe to find out what they're buying — and make it their goal to do the same.

For years it was fashion magazines that offered a peek into their world, but now you can go online any day and check a few Instagram accounts to see what famous members of the court are wearing and buying.

Since their cross-cultural tastes are seen as being the best, and since they have sufficient funds to spare no expense, the court is now perfectly positioned to set the world's standard for luxury.

And for the Global Tribe, luxury doesn't just mean expensive — it also refers to something that is unique and handmade.

So, rather than just looking for a clothing label that says "Made in Italy" or "Made in France," the Global Code will define luxury with labels like "bespoke," "sur-mesure" or "tailor-made."

In the tribe, items that are unique and handmade are valued above all others.

In recent times there has been a backlash against disposable products or things that are machine-made through mass production. People want one-of-a-kind products that provide the uniqueness and artistry that comes with being handmade. These are products that are created with patience, passion and commitment, which adds a whole extra layer of luxury to the item.

For example, there are plenty of fine machine-made watches, but a handmade watch from Patek Philippe is a highly coveted item, due to the skill and craftsmanship that goes into making it.

In the end, it's the Global Code for luxury that sets the highest standards and creates a new system of reference that the rest of society follows.

### 7. Successful worldwide brands follow the Global Code for luxury and use it to their advantage. 

So, if being a member of the Global Tribe isn't within your reach, maybe you want to create a global brand that will appeal to them and meet the conditions of the Global Code.

One of the first steps to creating a successful global brand is to market your product in a way that resonates with people around the world.

After all, if you start out with a brand that people can relate to, it could eventually develop into a luxury product.

Take Jeep, for example. They used a marketing campaign that told people they didn't need roads because, with a Jeep, they could create their own. Men could relate to this message because, subconsciously, every guy wants to be his own boss and create his own rules.

The success of Jeep's campaign shows us that if you know what the Global Tribe desires, you can use that knowledge to create a luxurious and enduring product or brand.

Other brands have found success by leveraging the knowledge that the Global Tribe enjoys being a special and exclusive group that can travel easily, and they've used this knowledge to their advantage.

Laphroaig is a successful brand of Scotch whiskey that adapted its marketing plan to make its customers feel special.

They did this by offering their customers a membership to the club, Friends of Laphroaig. And once they joined the club, the offer got even more special: members were then given the opportunity to buy a one-square-foot piece of land in Scotland.

And when it came to traveling, American Express knew that the Global Tribe likes to get around simply and quickly.

So they launched a new and easy one-step international arrival system for their customers. This allowed flyers to arrive at an airport and immediately get assigned a personal guide to lead them through the terminal and assist with procedures.

With this, American Express hit two key elements of the Global Code: they made customers feel special _and_ they made traveling easier. And that is a win-win.

### 8. Final summary 

The key message in this book:

**The Global Code is a new set of values and beliefs that are shaped and structured by the Global Tribe. People look up to the Global Tribe due to their position as cross-cultural tastemakers and can follow them closely thanks to the growing interconnectedness of the world. These international icons of style set today's standards of luxury, thereby dictating which products people around the world want to buy.**

Actionable advice:

**Speak culture when you do business abroad.**

When you travel abroad, even if you don't speak the country's language, try to speak its culture! This is particularly important if you're conducting business while traveling abroad. Before meeting with your business partner, prepare yourself by learning about the country's customs, such as how to address superiors, how deals are made and how to dress appropriately.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Culture Map_** **by Erin Meyer**

_The Culture Map_ provides a framework for handling intercultural differences in business and illustrates how different cultures perceive the world. It helps us understand these differences, and in doing so improves our ability to react to certain behaviors that might have once seemed strange. With this knowledge, we can avoid misunderstandings and maintain conflict-free communication, regardless of where we are in the world.
---

### Clotaire Rapaille

Dr. Clotaire Rapaille is an international marketing guru and founder of Archetype Discoveries Worldwide, which has provided global marketing insight into some of the world's biggest brands. He is also the author of numerous books on marketing, sociology, psychology and cultural anthropology, including _The Culture Code: An Ingenious Way to Understand Why People Around the World Live and Buy as They Do._

