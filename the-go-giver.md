---
id: 549131833633390009420000
slug: the-go-giver-en
published_date: 2014-12-25T00:00:00.000+00:00
author: Bob Burg and John David Mann
title: The Go-Giver
subtitle: A Little Story About a Powerful Business Idea
main_color: 2372AF
text_color: 2372AF
---

# The Go-Giver

_A Little Story About a Powerful Business Idea_

**Bob Burg and John David Mann**

_The Go-Giver_ shares five simple principles that are the secret to achieving realistic success in life — while becoming a better person. These blinks use examples from real businesses that will help us achieve our goals and dreams by harnessing generosity and authenticity.

---
### 1. What’s in it for me? Discover how to use the power of giving. 

How do you make it big in business? Most people would say you need to ruthlessly grab any opportunities that come your way. According to them, the key to business is to solely concentrate on getting what you can for yourself.

Although popular, this view is completely wrong: Your business will succeed when you find out what others want and try to satisfy their demands. Put concisely, you need to stop being a go-getter and start being a go-giver.

These blinks show you the benefits of putting others first, because the more you give, the more you will receive in return.

In these blinks you will discover

  * the five laws of stratospheric success;

  * why a 50/50 relationship isn't worth it; and

  * how one go-giver went from a hotdog stand to a restaurant empire.

### 2. The best way to succeed is not to try and take everything from life for yourself, but to share it with others. 

What would you look like as a success? Sometimes, the more you work to become the rich, admired entrepreneur you'd like to be, the further away it all seems. If nothing seems to be working in your favor, then you may need to change your mind-set.

Of course, we all want to attract success in our lives. But the best approach isn't to focus on gaining things for ourselves.

Let's take the example of Joe, a friend of the author and, by all accounts, a real _go-getter._ Joe was ambitious, hard-working and dedicated to his job. Yet the harder he struggled to succeed, the further away his goals seemed. What was happening?

Joe threw himself blindly at the goal of becoming successful and in doing so, stopped striving to do high-quality work he'd done in the past. Quite simply, he pushed himself to succeed so much that he lost sight of what was important.

There was one key difference between Joe and the people who _were_ achieving the success he desired. A go-getter thinks only of what they can gain. But successful people think in terms of what they can _give_ to others.

This is the _go-giver mentality_. It's based on both the pleasure of giving and the idea that you get out of life what you are looking for. So if you worry that people are taking advantage of you, eventually they will. If you are looking for conflict, it will find you. But if you are looking for the best in people, and go into business with the express wish of giving good things to other people, you'll be surprised at how much talent, empathy and good you'll find.

In the following blinks, we'll learn the rules that will help you unleash the power of giving. This way, we'll see that success isn't measured by how much you get, but by what you manage to _give._

> _"Trying to be successful with making money as your goal is like trying to travel a superhighway at 70 miles an hour with your eyes glued to the rearview mirror."_

### 3. Make sure that you add value to the experience of every customer who comes to your business. 

We've been introduced to the idea that giving to others will put you on the path to success. But what about the nitty-gritty?

Well, there are five rules you need to follow to become a go-giver: _the five laws of stratospheric success._ We'll discuss the first one in this blink.

If you want to be a successful go-giver, then start by creating your _customer experience,_ making it something people will savor and remember.

Many think that the best way to keep customers coming back is by concentrating on making the _product_ awesome. Sure, this can work, but not everyone can do it. For example, if you sell hotdogs at a stand, how much better can you make your hotdogs than everyone else's? Not much.

Instead, learn to stand out by following the _law of value_, and giving your customers a positive feeling they will remember.

Consider the story of Ernesto, who ran a hotdog stand that stood out among the others, because he ensured his customers had the best possible experience. How? By remembering their names, their children's birthdays, what they were up to in their lives and of course, their favorite order. In short, he made every customer feel special. Ernesto's extra effort paid off. His business thrived and he went on to own half a dozen full-service restaurants!

Ernesto's story illustrates that when you give positive things to people, you'll start to receive positive things in return. Though a customer's experience with you may only be a small part of their day, it can nonetheless be incredibly valuable to them.   

How else can we touch the lives of others with our business endeavors? Read on to find out in the next blink.

> _"Your true worth is determined by how much more you give in value than you take in payment."_

### 4. Your success depends on how many lives you touch with what you do. 

How do you know if a business will be successful? You look at how many people it serves. This is the _law of compensation_, where the number of people who are touched by your enterprise determines how successful it can become. How exactly does this work? Let's learn from an example of the law of compensation in action.

Nicole Martin was a former grade school teacher who developed a series of games that encouraged children to be curious and creative. After developing these games, Nicole's life was transformed. She is now CEO of the Learning System For Children software company, which is worth as much as two hundred million dollars. The reason for her success? The almost unlimited number of children around the world whose lives could be touched by her games.

Of course, this kind of success is easier said than done. In order to serve people with your business, you'll first need the support of others. But how can we convince people to support our ideas before we've had any success with them? Well, it's often just a matter of showing conviction yourself.

For example, when Nicole decided to help as many children as she could, she tried her ideas out on family and friends. Not only did they support her business, they also put her in touch with a big investor who was willing to fund the educational software company. When you invest your energy in one direction, you'll soon notice how many people are willing to help you get things going.

> _"Your income is determined by how many people you serve and how well you serve them."_

### 5. Place other people’s interests first to achieve your own goals. 

We've begun to see that thinking in terms of giving to others rather than gaining for yourself will bring you more success. "Is this really true?" you might be wondering. Yes, it really is!

The _law of influence_ shows us just how profitable it is to put others first, by highlighting a relatively simple idea. Place someone's interest above your own and you build a positive relationship with them. Later down the road, they'll go out of their way to make sure _your_ interests are also met.

The story of businessman Sam Rosen reflects this. In his early career, he struggled to further his own interests as an insurance salesman. But then he had a change of heart. Sam decided to start putting others first and became a financial advisor, sharing his insights to help others use their money in a smarter way.

The people who were grateful for Sam's help boosted his career in turn. He now brings in a massive three-quarters of the revenue for his company.

Still, it's tempting to only enter into 50/50 relationships with people, in which you get back exactly what you put in. But, by giving 100 percent of your effort, both parties will be happier.

The author shares a touching example of this principle in action. Joe, the friend mentioned earlier, had a deal with his wife. They promised that they would never complain to each other about their job for longer than 30 minutes.

But one day his wife came home completely exhausted and he decided to let her speak unconditionally about her stressful day. The next day, he found a note from her thanking him for listening, which made him happy. Giving your all is far better than simply keeping score.

> _"Your influence is determined by how abundantly you place other people's interests first."_

### 6. Your business will get nowhere without authenticity – it is the salt and pepper of success. 

Nobody likes it when they go into a store and get accosted by a salesperson going on and on about the statistical superiority of their product. It's irritating, and their bold statements aren't exactly believable, either. There is a better way to make sales, and it's another go-giver principle.

The _law of authenticity_ tells us that we should treat other people as human beings, not just potential customers. Take the example of realtor Debra Davenport. She struggled during her early days in the industry. It seemed that every time she had to make a sale, she couldn't, no matter how much she waxed lyrical about the property.

Then one day, it all changed. She began a conversation with the client about his day and suddenly, the formal meeting was transformed into something much more relaxed, personalized and fun. The result? The house was sold. And that was just the beginning! She went on to become a successful realtor, simply by engaging with the clients as people.

We can learn another crucial aspect of the law of authenticity from Debra's story. Whenever you interact with others, just _be you_. You could be the most technically gifted person in your profession, but you won't be truly successful until you find a way to _show_ your uniqueness.

This is just what Debra did. She had her skills and training as a salesperson, but she didn't rely on this alone to sell houses. As soon as she started the conversation, she wasn't just Debra the Realtor, but Debra the Person, with her own charms and quirks. By allowing herself to be authentic, she could easily strike up relationships with clients. So pluck up the courage to be yourself, and you'll reap the benefits.

> _"The most valuable gift you have to offer is yourself."_

### 7. Learn how to give in order to receive. 

We've heard a lot about giving, but not a lot about what to expect in return. You may still be a little unsure exactly where your own success will come from. Well, let's put this fear to bed now!

Every time you give something, you can expect to get something in return. This is the fifth and final principle of go-giving — the _law of receptivity_. Theories and belief systems all around their world have their own version of this law, from karma, yin and yang, to every action having an equal and opposite reaction. Just as your heart must relax after it contracts, the natural movement from giving is receiving.

But if you want to take advantage of giving, you've got to accept this flow. All too often, we close ourselves off to this process by ignoring it or considering ourselves undeserving. This has to stop! Instead, we should actively open ourselves up by dreaming big, being curious, and believing in ourselves. Not only will this give us the energy to give to others, but it will make us more receptive to the good things we receive in return.

It's vital to remember that no matter what profession you have, how skilled you are, or how much you work, the most appreciated and successful way of approaching things is by thinking what you can do for others. Use these laws to guide you in your business strategies. Not only will you become more successful, you'll also see your success touch all those around you. In this way, your business will grow while you too grow as a person.

> _"The key to effective giving is to stay open to receiving."_

### 8. Final summary 

The key message in this book:

**If you want to be successful in life you have to give, to serve and to put the interests of others before your own. Remember the five laws of stratospheric success: value, compensation, influence, authenticity and receptivity. Together, they add up to the strongest building block for business — generosity.**

Actionable advice:

**It's not all about the money!**

The next time you find yourself complaining that you work hard at your job and still don't get as much money as you'd like, think about all the other things you gain. These could be positive interactions with strangers, new skills or simply growing as a person. If you're not gaining any of these things, then it's time to start _giving_ more positive things — then you'll see the results.

**Suggested further reading:** ** _Give and Take_** **by Adam Grant**

_Give and Take_ offers a breath of fresh air to traditional theories of what it takes to be successful. Backed by ground-breaking research, _Give and Take_ demonstrates how giving more to others, rather than competing against them, may be the secret to profound success and fulfillment.
---

### Bob Burg and John David Mann

Bob Burg is the author of _Endless Referrals: Network Your Everyday Contacts into Sales._ He is also on the board of directors for the Florida pet rescue, Furry Friends.

John David Mann is the _New York Times_ bestselling coauthor of _Flash Foresight_ with Daniel Burrus. He won the Nautilus Award for the _Axiom Business Book_, and Taiwan's Golden Book Award for _Innovation_. He is also a concert cellist and a composer.

Mann and Burg also partnered up to write _Go-Givers Sell More_ and _It's Not About You._

