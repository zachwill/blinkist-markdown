---
id: 519de7b5e4b0a17389a8ac0f
slug: the-god-delusion-en
published_date: 2015-10-28T00:00:00.000+00:00
author: Richard Dawkins
title: The God Delusion
subtitle: The Science behind Atheism
main_color: 60A0C4
text_color: 3E6880
---

# The God Delusion

_The Science behind Atheism_

**Richard Dawkins**

_The God Delusion_ (2006) deconstructs the most popular arguments and reasoning for the existence of God to show the statistical and logical improbability of a higher being's actual existence. These blinks explain why religion _shouldn't_ be the foundation for society's morals and how it can actually be harmful to our ethical standards.

---
### 1. What’s in it for me? Look at religion from a more critical perspective. 

Is there a higher power? Regardless of whether we call it God, Allah or Vishnu, the question of whether a divine being truly exists has been with humankind for a very long time — and we continue to have no clear answer. 

But, of course, all the valuable lessons that we can take from the Bible and other scripture are essential in guiding us when it comes to morality and how to live our lives. Or are they? While the moral benefits of religion and religious scriptures might be clear at first glance, taking a closer look can provide you with a whole different perspective on life. 

These blinks take a closer look at the idea of God, primarily from a Christian perspective, to see if any of the ideas usually associated with religion can really hold water.

In these blinks, you'll discover

  * how the Bible's gospels don't agree on where Jesus was born;

  * why religion could be an evolutionary byproduct; and

  * how the story of Lot in the Bible shows the amorality of scripture.

### 2. The most widely known and accepted arguments for God’s existence are simply not persuasive. 

It's a task that humanity has struggled with throughout the ages: proving the existence of God. In the past, people have tried to do so through logical reasoning and cosmological proofs, which assume that God was the _First Cause_ — the force that made everything else. 

But what does this kind of proof look like?

Well, cosmological proofs of God start by saying that an external force must have produced the universe. The most famous one was postulated by the medieval theologian and philosopher Thomas Aquinas, who took First Cause as the fundamental premise of _all_ his proofs. 

In what the author calls the _Cosmological Argument_, Aquinas says that there must have once been a time when nothing physical existed. The fact that physical things now exist proves the existence of God — the _Unmoved Mover_ who created them. 

So, cosmological proofs assume that everything, the existence of humans and the universe included, must have a cause, and that this cause must be God. 

What they don't say is how God, the so-called First Cause, could have come into existence without a cause himself. 

Another common argument for God's existence are ontological proofs, but these justifications are mere wordplay. 

Unlike cosmological arguments, ontological proofs construct reason with words. The first and most famous proof comes from Anselm of Canterbury in 1078. He argued that we can _imagine_ a perfect being, but that this being could then only exist in our minds. To be truly perfect it would need to exist in the physical world. 

However, he says that since we can picture this perfect being, it must also exist; if it didn't, there would be a logical error. He uses this assertion to prove that God exists as a perfect being. 

But there are some problems with his proof: first, it doesn't prove the existence of God; and second, it's logically flawed. Anselm assumes that existing is more perfect than not existing. But according to philosophers like David Hume and Immanuel Kant, existence isn't a quality. Therefore, a perfect being doesn't necessarily need to exist.

### 3. The scriptures can’t prove that God exists because they’re full of gaps and contradictions. 

Did you know that the Bible is the best-selling and most distributed book in the world? Surely this widely read piece of literature _must_ contain evidence of God's existence. But are the scriptures a trustworthy source on this subject?

Not necessarily. The Bible has changed over time and is loaded with contradictions. 

In fact, every single gospel in the Bible was written long after Jesus died. To preserve them, the scriptures were copied over and over again by scribes who, like anyone, were prone to human error. As a result, very little actually remains of the original text. 

And if that doesn't prove the fallibility of the Bible, the contradictions that occur among the gospels certainly do. For instance, in his gospel, John says that Jesus's followers were surprised to hear that the Messiah was not born in Bethlehem, because a prophecy from the Old Testament foresaw the city as Jesus's birthplace. 

However, Matthew and Luke wrote that Jesus _was_ born in Bethlehem — and there are also inconsistencies regarding Mary and Joseph's arrival there. Luke says that Joseph was forced to Bethlehem when Augustus ordered a census, and while a census did indeed occur, it happened in AD 9, long after Jesus was born. 

Even biblical scholars don't consider the book of their study to be a reliable record. For example, not one person knows who the four evangelists were, and the four gospels that secured places in the official canon were basically chosen at random from a larger pool. In fact, there were at least a dozen gospels, among them the gospels of Thomas and of Mary Magdalen. 

Historians agree that the gospels aren't an attempt to record history, but rather a form of storytelling. Because of the contradictions it contains, reputable biblical scholars don't consider the New Testament an accurate account. 

For instance, in _Misquoting Jesus_, American scholar Bart Ehrmann explains that the Bible's stories are a result of both accidental and intentional alterations made by various scribes.

> _"There is little historical evidence that Jesus claimed to be divine at all."_

### 4. Natural selection and evolution offer a more probable explanation for life on earth than the existence of God does. 

OK, so the Bible is fallible. But something as complex as all the species on earth must have had a creator, right? 

Well, actually, the opposite is true. 

However statistically unlikely the existence of complex life forms like humans might be, the existence of a higher being capable of creating such complicated creatures is even more improbable. 

Then how did the world as we know it come to exist?

It can all be explained through Charles Darwin's _Theory of Evolution._

Complex species developed over centuries through _natural selection_ — a process by which organisms adapt to their environments; those that adapt with the most success are most likely to survive. So, while it was statistically unlikely for humans to come into existence, what _is_ likely is the existence of a lengthy process full of tiny evolutionary steps, from single-celled organisms to us humans. 

That means it took thousands of years for a species in existence today to develop, because natural selection is a long, cumulative process in which every little step is slightly improbable, but not impossible. 

In fact, many of the slightly improbable steps in this process led to highly improbable results. The very existence of Earth itself, and all of the complex life forms it contains, is a prime example of just how complicated the universe, including all the species in it, can be. 

But evolution isn't just a _probable_ explanation for our existence — it actually makes the existence of God even more unlikely. 

The existence of any force capable of creating something as improbable as human life is itself even more unlikely than human life. And God couldn't have been produced by evolution because something would have had to come before him, which is impossible given that he is supposedly the First Cause. 

So, while evolution might appear unlikely, God is, statistically speaking, even less likely. Shouldn't we always stick to the most plausible explanations?

### 5. Religion is an evolutionary by-product and serves no purpose in and of itself. 

Maybe you're wondering, "If there's no God, then where did religion come from?" Since worshipping God doesn't offer a direct evolutionary benefit, how can religion have been created during the process of evolution?

Well, evolution sometimes leads to undesired and unhelpful by-products. While the behavior of living beings doesn't always appear logical at first glance, evolution can explain such irrational actions. 

For instance, the way moths fly straight into candle flames can appear suicidal. However, this compulsion is an unwanted result of evolution that used to help moths survive. 

Insects use celestial objects like the moon to orient themselves — historically with great results. But when humans started to produce artificial light, insects' internal compasses were thrown off and the consequences were fatal: moths fly toward light as they always have and are now often burned up. 

However, their behavior is still rational, because moths see the moon much more often than they see a candle. Their fatally dive-bombing flames is merely an unintended consequence of an otherwise helpful evolutionary adaptation. 

But what does this have to do with religion? Well, just like the moth's compass, religion can be viewed as an unnecessary by-product of a useful evolutionary trait. 

Consider the task of raising children. Until they can make choices for themselves, kids must trust adults to decide everything for them. As a result, natural selection favors children who believe what adults tell them. When kids don't know which foods are poisonous or that crocodiles are dangerous, it can be life-saving for them to trust warnings from grown-ups. 

However, the by-product of this tendency is that children can't discern between correct and incorrect information from an adult source. That makes it likely for arbitrary beliefs to be handed down from parent to child, generation after generation. 

One such arbitrary belief caused by the blind faith of kids?

That's right, religion itself.

### 6. Being good is an evolutionary by-product with self-serving motivations, not divine origins. 

Now you know how religion can be chalked up to evolution; but did you know that our predisposition to do good is also a product of natural selection? 

While the compulsion to survive can easily explain hunger or lust, uncovering the evolutionary roots of sympathy and altruism actually requires a bit of digging. 

For starters, being nice to our own kin is key to our survival. Here's how:

The building blocks of natural selection are genes, and to survive, they push their bearers to be selfish and reproduce. But when it comes to our own flesh and blood, it makes solid biological sense for our genes to tell us to behave sympathetically. That's because our family members share a similar gene pool and their survival is also key to reproducing our genetic codes. 

Therefore, a gene that tells an organism to protect its kin is statistically likely to help itself reproduce. 

But that's not all. Being nice to others also means direct benefits for ourselves. Being nice produces the expectation that we'll receive something in return. As a result, two people both benefit from being nice to one another. 

For instance, a bee needs nectar and a flower needs pollinating. Bees take nectar from flowers and, in exchange, they spread the flower's pollen. But this kind of symbiosis goes way beyond bumblebees and wildflowers — it occurs all over the animal kingdom, helping various species survive. 

In fact, this concept can even be applied to humans. For example, a hunter needs a spear and a blacksmith needs meat. Every profession has a different benefit for society and, in exchange for the work each individual does, they receive different products or services from other people.

And what about those who don't do their share?

They are quickly seen as unreliable and others start refusing to help them. So, being nice to others is essential to benefiting ourselves.

### 7. Our morals shouldn’t come from the Bible, as its values contradict those of our modern society. 

Would you consider it ethical to hand a woman over to an angry mob so they can rape and kill her? Obviously not. But this act, committed by a Levite religious man, is portrayed in the Bible as an commendable deed. 

If the "good book" praises behavior like this, what else does its moral code justify?

The Old Testament is actually full of cruel tales that contradict our morality. In fact, it paints a picture of a quick-tempered, jealous and vengeful God, while promoting values that are completely inconsistent with modern standards of behavior. 

For instance, Genesis 19 contains the story of Lot. In the tale, God wants to destroy the city of Sodom because it is a den of sin. Lot, a righteous man who lives in the city, is spared by God, who sends two male angels to warn him of the impending destruction. But when the angels arrive at Lot's house, every other man in Sodom gathers to demand the angels be turned over to be sodomized. 

Rather than let that happen, the supposedly morally upstanding Lot offers the virginity of his two daughters to spare the angels, an action that the story portrays as just. While we would never consider this to be moral today, it's literally in the Old Testament. 

And the New Testament?

It teaches us that we're all condemned as soon as we're born. So, while Jesus's teachings may be a huge improvement on the views proffered in the Old Testament, there are some that no good person should stand by. 

For instance, the central doctrine of the New Testament is that every human is a sinner because we all descended from Adam and Eve — the original sinners. It was for these alleged sins that Jesus died. But if God is truly omnipresent and omnipotent, why couldn't he just have forgiven our sins and spared Jesus's life, without such a dramatic sacrifice?

Today, such an idea seems obvious, given that modern societies know and value forgiveness.

### 8. Morals are a product of the times, and shift as society does. 

If our morals can't come from the Bible, then how do we decide what's right and wrong? Well, every society actually maintains a broad consensus about morality that has developed over decades. 

In fact, our morals shift all the time, but these changes are only apparent with the hindsight of history. People living at different times have viewed morality differently, and changes in morals have been influenced by the shifting _zeitgeist_.

Zeitgeist is the shared thought and spirit that influences any given culture during a particular period of time. A zeitgeist changes slowly and imperceptibly for individuals who are living through it; but in retrospect, the changes are clear. 

For instance, women's suffrage was an achievement of the twentieth century. In the United States, women were first given the right to vote in 1920, yet in Switzerland, it was not granted until 1971. 

This shows that the disenfranchisement of women was a common and accepted practice less than a century ago.

Or consider the abolition of slavery in the United States, which occurred after a violent civil war only 150 years ago. Just think: the most liberal person in the nineteenth century held views that seem absolutely antiquated and backward to liberals today. 

But how does the zeitgeist shift?

The process is influenced by lots of factors — but religious scriptures are not one of them. 

For instance, new ideas spread from person to person through discussions, books and the media. In addition, changes in the moral climate can be seen in speeches, polls and elections that lead to political decisions and laws. As a result, leaders like Martin Luther King, Jr. play a tremendous role in shifting the zeitgeist, because they are instrumental in persuading the masses to move in unison. 

So, not only is it impossible to derive our morals from religion, but religion actually inhibits moral progression! Read on to learn how.

### 9. When taken literally, the scriptures have a negative effect on society’s moral values. 

Turn on the news on any given day and you'll hear about religious fundamentalists in one country or another. There are still many people who take the scriptures literally, leading to morally questionable ideas and actions.

Just think of attitudes toward homosexuality, something that, as a direct result of scriptural decrees, is still not fully accepted. When Afghanistan was under Taliban rule, for example, the country's official punishment for homosexuality was to be buried alive. The justification for this extreme sentence was the fact that God and the scriptures condemn homosexuality.

But such views exist even in countries that are seen as liberal and diverse. In Britain, for example, homosexuality was criminalized until 1967, based on the logic that only sexual relations between members of the opposite sex are divinely ordained.

In fact, gay people are still denied equal rights in many Western countries. For instance, in the United States, fundamentalist Christians dream of eradicating homosexuality, while popular politicians compare homosexuality to bestiality and insist that the Bible condemns it. As a result, political demonstrations bearing slogans like "Thank God for AIDS" are not uncommon in the United States.

But homosexuals aren't the only victim of religious extremism; the battle against abortion is another example of a fight in which religious activists don't hesitate to spill blood. Absolutist Christians believe that since life begins at conception, embryos are human life. Therefore, to them, abortion isn't just wrong, it's indistinguishable from murder, and they'll go to extreme measures to prevent it.

For instance, former Republican presidential candidate nominee Randall Terry expressed the urge to "find and execute" those who facilitate abortions. And in 1994, an American named Paul Hill killed an abortion doctor, John Britton, along with his bodyguard.

The murderer justified his crime by claiming he was preventing the deaths of future innocent babies. While abortion may be an ethically controversial issue, it certainly doesn't justify murder — not to mention the hypocrisy of killing someone while claiming to be pro-life!

### 10. Children involuntarily assigned a religion often suffer mental and physical abuse. 

Would it be reasonable for someone to register their infant as a member of a political party? The question seems silly, but why then does it seem justifiable to enlist newborns into a belief system as domineering as religion?

It's a fact that parents choose religions for their children before the kids are old enough to make such decisions themselves. And it's easy for parents to do so.

For instance, in the Christian faith, a simple sprinkling of water can change a child's life forever. In the case of male Jews or Muslims, committing a child to the religion even means indelibly altering the young person's body through circumcision.

On the other hand, we protect children with laws that say they are not legally competent and responsible for their actions until a certain age. So why don't we also wait until a person reaches adulthood for them to make religious choices that will alter their lives?

But in addition to religion being chosen for them, it can also cause children physical and mental abuse. While cases of child abuse by priests are well known, the mental abuse that occurs through religion can be equally damaging.

For instance, many Christian parents bring their kids to _hell houses_ — attractions built like haunted houses — where kids experience the sinners' version of the afterlife. Like haunted houses, these hell houses use actors and guides to ensure the visitors are frightened by what awaits them in hell. Violent, vivid and horrifying depictions are even accompanied with the smell of burning brimstone and the agonized screams of those damned to hell for eternity.

Imagine how a child who experiences this must feel when thinking that they are doomed to hell, a plausible thought for a Christian person who believes in original sin. It comes as no surprise that there are many reported cases of trauma resulting from people having this very thought. And this is just one example of the mental suffering religion is capable of inflicting.

### 11. Religious beliefs are more heavily protected than other ones, even if they are discriminatory. 

Everyone should be free to practice their religion, and many countries have entrenched this freedom in the law. But how does society view religious beliefs relative to others? 

Generally, religious ideas are given more respect and protection than other beliefs, since many people consider religion particularly vulnerable to attack and worthy of protection. A great example of the weight given to religious freedoms are the exemptions offered for military service.

You can be a brilliant philosopher who has written innumerable essays on the evils of war, yet find it hard to avoid the draft. But if you protest enlistment in the army based on religious grounds, your exemption is practically preordained. 

In fact, the privileges afforded to religion even apply to drug consumption. For example, in 2006, the US Supreme Court ruled that a church in New Mexico was exempt from laws prohibiting the consumption of hallucinogenic drugs because members of the church claimed that the drugs helped them understand God. You can contrast this example with the Supreme Court's 2005 ruling that cannabis use is illegal, even for medicinal purposes. 

So, while protecting religious freedoms is important, this respect has at times gone too far. As a result, people can point to their religious beliefs to justify discriminating against and hurting others. Take the case of a 12-year-old Ohio boy who, in 2004, won the legal right to wear a T-shirt that read, "Homosexuality is a sin, Islam is a lie, abortion is murder."

The basis of his lawyers' argument? The boy's fashion choice was a matter of religious freedom. 

Another example comes from Denmark, where, in 2005, a newspaper published 12 cartoons that depicted the prophet Muhammad. As a result, protesters in Pakistan and Indonesia burned Danish flags, destroyed embassies and boycotted Danish products. 

While the media and politicians denounced the violent response, they also expressed sympathy for the offense that the cartoons had caused to Muslims. You can only imagine the full-on outrage the media would've directed at the protesters if they hadn't been representing religious views — but they didn't, because religious groups are unjustifiably protected.

### 12. Science is a better source of creative inspiration than religion, and finding consolation in religion is hypocritical. 

Regardless of whether you believe in God, religion at least gives inspiration for great works of art and offers a respite from our harsh world, right?

Well, science has actually proven to be more inspirational than religion. For instance, while many of the great works of art and architecture were inspired by God or religion, the Church was a major employer and often represented the only means by which artists could pursue their passions while making a living. As a result, much of the religiously themed art that exists today is nothing more than commissioned work.

When you remove God from the equation, it's clear that there's a far better source of inspiration: science. Just think of how many things there are in our universe and even on our planet that we don't yet understand. 

We're constantly surrounded by the wonders and riddles of the natural world, which is an amazing source of inspiration. The best part is that, without God, our thinking about these topics is totally unrestricted!

Furthermore, the comfort offered by religious belief is hypocritical. How so?

Well, believing in an afterlife is a tempting proposal, and it's no shock that many people rediscover religion on their deathbeds. Even people who admit to doubting God's existence find a psychological and emotional need for God in the most difficult moments. In fact, surveys have suggested that roughly 95 percent of the US population believe in some form of heavenly afterlife. 

But then, why are so few people happy to hear that their days are numbered? Shouldn't the chance to ascend to heaven come as good news? This discrepancy suggests that the faith of religious people might not end up being so deeply entrenched after all. 

And, of course, there is the possibility that people believe in an afterlife but fear dying nonetheless. This is certainly understandable, at least for Christians whose doctrine tells them that death is followed by eternal purgatory — a less than ideal post-life experience.

### 13. Final summary 

The key message in this book:

**It's extremely unlikely that God exists, and investing in religion is not worth our while. In addition to its failure to deliver anything of value, religion actually impedes progress while perpetuating outdated and inhumane values.**

Actionable advice:

**Think twice before you baptize your children.**

The decision to adopt a religion has lasting consequences and can even determine the friends a person keeps. Since this choice can affect a person's entire life, it's essential that it be made by each individual for and by themselves, just like choosing a career or a spouse. Do your children a favor and let them make this important decision when they're old enough to understand their options!

**Suggested** **further** **reading:** ** _God Is Not Great_** **by Christopher Hitchens**

_God is Not Great_ traces the development of religious belief from the earliest, most primitive ages of humankind through to today. It attempts to explain the dangerous implications of religious thought and the reasons why faith still exists today. It also helps explain why scientific theory and religious belief can never be reconciled.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Richard Dawkins

Born in Kenya in 1941, Richard Dawkins is an English ethologist, evolutionary biologist and writer. In addition to being a fellow of both the Royal Society and the Royal Society of Literature, he has received numerous awards and honors, including the Royal Society of Literature Award and the Michael Faraday Award of the Royal Society.

