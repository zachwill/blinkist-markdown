---
id: 56e6f1289854a50007000141
slug: the-great-escape-en
published_date: 2016-03-17T00:00:00.000+00:00
author: Angus Deaton
title: The Great Escape
subtitle: Health, Wealth and the Origin of Inequality
main_color: 736B37
text_color: 736B37
---

# The Great Escape

_Health, Wealth and the Origin of Inequality_

**Angus Deaton**

_The Great Escape_ (2013) clearly explains that humanity is doing better than ever before. But not everyone has benefited from the technological and political developments that have made our prosperity possible. By examining both historical and modern inequality, this book offers solid advice on how to close the gap.

---
### 1. What’s in it for me? Get insight into global inequality and what we can do about it. 

During the last years of the eighteenth century, when the life expectancy for men was about 35 years, over 400,000 people died in Europe from smallpox, a disease that, today, we don't even have to worry about. 

Needless to say, a lot has changed since then. Thanks to scientific discoveries, political ideas and new technology, life has vastly improved for much of the world's population. But not for all of it. By and large, richer countries benefit and poorer ones suffer on. 

So what can we do about this? That's exactly what these blinks are all about.

You'll also learn

  * why our earliest ancestors' way of life might not have been so bad;

  * how the United States became an aristocracy; and

  * why foreign aid is a blunt instrument in the fight against poverty.

### 2. There’s never been a better time to be alive. 

When listening to the news these days, one gets the impression things are just getting worse and worse. But the news never gives the whole story. In fact, rather than getting worse, our well-being has, on average, never been better than it is today.

Until about 250 years ago, most people throughout the world lived in poverty. And today, although a lot has happened since then, more than a billion people live in extreme poverty, suffering the same terrible living conditions as their forebears.

But overall well-being — which includes things like access to health services, higher pay, longer lifespans, happiness, opportunities for education and progress as well as general quality of life — has increased considerably. 

For example, a white middle-class girl born in the Unites States today has a life expectancy of over 80 years (with a 50 percent chance of living to 100). She also has opportunities for education and better economic prospects than her parents.

Despite all this, however, there is still extreme inequality in well-being worldwide. Though people today earn more money and enjoy better living standards than ever before, there are still massive disparities between rich and poor countries.

The health standards in Sierra Leone, for instance, are actually worse than health standards were in the United States in 1910, when 25 percent of children died before the age of five. And over half of the population of the Democratic Republic of Congo lives on less than a dollar per day.

The good news is that these inequalities can lead to progress — if they're used in the right way. For example, if the difference between, say, the rate of child mortality in rich and poor countries is well known, poor countries will likely try to adopt the innovations that allowed wealthier countries to increase life expectancy, and thus diminish these inequalities.

> _"The great failure of poverty reduction has been in sub-Saharan Africa."_

### 3. Our ancestors lived significantly shorter and unhealthier lives than we do today. 

The first humans, members of hunter-gatherer communities, lived healthy, albeit short, lives. Members of these nomadic tribes lived until about the age of 40, and spent their days looking for food and shelter. 

This might seem like a hard way of life, but that's not entirely true. For instance, these tribes enjoyed a very balanced diet, and shared what they found with one another. Instead of getting stuck in dirty villages, they were able to move on before a lack of sanitation caused disease to spread, and their diet consisted of highly nutritious wild plants and meat that was probably healthier than what we consume today.

Then, millennia ago during the Neolithic Revolution, these hunter-gatherers began settling down into an agricultural lifestyle alongside their newly domesticated animals. But this agricultural revolution actually led to a _decrease_ in well-being and a massive increase in mortality brought on by disease.

One would expect that settling in villages would have increased our ancestors' well-being. After all, a settled life meant less dependence on the seasons and climate, less travel and less competition with other tribes for food. 

But it turned out that the opposite was true: settling down actually decreased well-being. These early villages and cities were dirty — people kept their food next to the feces of domesticated animals, and disease spread fast as new epidemics arrived through trade.

As a result, both quality of life and life expectancy drastically decreased during the Neolithic Revolution. People lived shorter lives, and often died young due to diseases or catastrophic famines after droughts.

### 4. Social, political, economic and scientific changes have rapidly decreased mortality in the last 250 years. 

In the last century alone, life expectancy in wealthy countries like the United Kingdom has increased by about 30 years! How did this happen? The biggest reason for this amazing increase in life expectancy is a decrease in child mortality.

Until very recently, life was short and child mortality was high. Yet with improvements in health care and disease prevention, many countries in the world have brought child mortality down to a rate of only 0.5 percent! If you consider that, a few centuries ago, almost a third of all children wouldn't have made it to age five, these are massive improvements. 

In addition, better nutrition, health care and education mean that most babies born in wealthy countries can expect to live long enough to see their grandchildren and maybe even great-grandchildren — something that might not have been possible a few centuries ago.

Another important factor in improving health and mortality rates was the spread of scientific knowledge and other developments. Advances like the germ theory, governmental stability, improved sanitation and increased research into disease prevention and treatment have improved health and decreased mortality in many countries.

When the city of London improved their sanitation in the early nineteenth century, for example, disease rates quickly dropped as the government could more effectively fight the cholera epidemic of 1854.

That's not to say that the progress has been entirely smooth. For example, the Great Famine of China, between 1959 and 1961, resulted in the deaths of around 30 million people. Then there's the 34 million people that have died of HIV/AIDS. And let's not forget that there are children in the world today who still suffer and die from preventable diseases like cholera, measles or diarrhea.

### 5. Child mortality is still high in poorer countries. 

In the previous blink, you learned that there is a pretty easy way to manage the majority of diseases that have plagued humanity for thousands of years: proper sanitation. So why do children in poor countries continue to suffer from such high mortality rates?

The knowledge that improvements in sanitation or the use of cheap, yet effective vaccines can help prevent diseases and infections is accessible all over the world. And yet, in many countries, this knowledge isn't actually put to use. But why?

Often, the government simply lacks the motivation to implement these measures, and the local population is still too uneducated to know that such simple improvements could save the lives of their children.

As a result, children die of treatable or easily preventable illnesses like diarrhea, or suffer from malnutrition for years before dying when their weakened immune system is attacked by a virus.

Compounding this problem is the fact that poor countries often have little or no health care provided by the government. Consequently, access to advanced medicine is sorely lacking for many people. For instance, Zambia and Senegal allocate a much smaller percentage of their budget to improving their citizens' health than countries like the United Kingdom.

Unfortunately, there are plenty of undemocratic countries that don't act in the population's best interest. All too often, quality health care is available only for those who can afford it, while the majority continues to suffer and watch their children die from preventable diseases.

To make matters worse, many people don't even realize that their government could and should help improve their health, having never been educated about their rights as citizens. The Gallup World Poll, for instance, regularly surveys citizens on what they consider to be the primary issues that their government should focus on. Health care ranks very low in these surveys, far behind things like job creation.

### 6. In richer countries, caring for the elderly is a huge problem. 

As a result of our technological innovation and scientific breakthroughs, life expectancy in wealthier countries has risen to unprecedented heights. However, this growth is beginning to level out. We've made amazing progress in extending the lives of children, but we seem to have hit a wall when it comes to ways of lengthening the lives of the elderly. 

Indeed, diseases such as cancer remain the leading cause of death among the elderly. The biggest existential threats that an elderly person in a rich country faces are cardiovascular disease, cancer and pneumonia. 

Rather than dying of diseases and infections, today's elderly die as a result of chronic diseases that begin to manifest at 50 years of age and older. For this reason, billions of dollars are being invested in research on cancer treatment, cardiovascular disease and Alzheimer's prevention.

But increasing healthcare spending isn't necessarily the solution to these problems. In the United States, for example, around 18 percent of taxed income is spent on health care — twice as much as in any other country. And yet, Americans don't live as long as many people in other rich countries.

One of the most important medical and social developments of our time has to do with lifestyle decisions. For example, people today smoke less and live healthier lifestyles than those that lived in the '70s and '80s. People today know that they could potentially live long, fulfilling lives, and they're starting to work hard to get there.

Instead of only throwing money at healthcare initiatives, this money should also be spent on better education and improvements to living standards, which would help prevent illness instead of simply treating symptoms as they develop.

### 7. The very nature of inequality has changed over time. 

When thinking about inequality, we often only consider the disparity of wealth between the rich and poor within our _own_ society. Today, however, we have to start thinking globally.

It's certainly true that inequality can be seen in all countries. But the greatest gaps are found between nations.

Before the Enlightenment and the Industrial Revolution, most countries were on a similar footing, and the largest disparity of wealth was found between poor farmers and the rich aristocrats who owned the land the farmers worked. 

However, the Enlightenment essentially abolished aristocracy and, with the growth of the middle class, these massive disparities began to shrink.

For some nations, these intellectual and technological revolutions brought great wealth; others were left behind. While the United States and many European countries enjoy plenty of wealth and high standards of living, many African and East Asian countries still struggle to get by and develop.

In Africa, for instance, the number of poor people actually doubled between 1981 and 2008, from 169 million to 303 million.

Nor has the disparity of wealth within nations been wiped out entirely. The United States, for example, is quite a rich country, but it's also plagued by inequality.

In the United States the _one-percenters_ — the top one percent of income earners — control the vast majority of wealth, while the rest of the population lives only slightly better than their parents did, or live in poverty. 

And the consequences of poverty include much more than just struggling to afford housing and food. Poverty also entails an inability to participate in social and political processes, as well as a much slimmer chance of ever getting a higher education.

On the other hand, the super rich (that is, the top 0.01 percent of income earners, who hold 4.5 percent of total US income) are becoming even richer due to lenient tax regulations and benefits. In a certain light, one could argue that the United States is building a modern-day aristocracy.

> _"Inequality can spur on progress or it can inhibit progress."_

### 8. Globalization won’t lift poorer countries out of poverty. 

Globalization has enabled us to buy more things, for less money, from more areas of the world than ever before. Not only has globalization made consumer life easier; it's also had a wonderful impact on poverty.

Globalization makes it possible for innovations to reach all corners of the globe. Global telecommunications infrastructure, for example, makes information and scientific innovations easily accessible via the internet. Today, it's easier to travel, transact and communicate globally than ever before, all thanks to global technological innovation.

One would think that poor countries today should be able to easily use the information and innovation that rich countries have accumulated during the last 250 years, and quickly catch up with them. Unfortunately, however, access to information isn't the only ingredient necessary for escaping poverty.

In many countries around the world a lack of fundamental institutions impedes growth and progress. While the knowledge of how to treat preventable diseases or establish a functioning democracy might be available, the institutions required to implement these innovations are not.

Some countries, including Hong Kong, Japan and Singapore, as well as China and India, are indeed growing at a rapid pace. But their growth leaves countries like Liberia and Afghanistan behind. While some poor countries have developed into middle-income ones, others have become even poorer than they were decades earlier.

The Democratic Republic of Congo is one such example. Thanks to massive and continual political and economic problems, its population is now worse off than it was after World War II.

### 9. Giving aid to poor countries can actually make things worse. 

Charity is all about giving to those in need. And that's a good thing, right? Well, that depends on what you give, and how you give it.

Rich countries often suffer from the _aid illusion_. Indeed, development aid in the form of cash injections from a rich country to a poor one happens everywhere in the world. In fact, in 2011 alone, governments around the world provided over $133.5 billion in development aid, and charities and NGOs raised another $30 billion. This should have been enough money to finally end poverty once and for all!

Yet, the opposite is happening. While rich countries remain under the illusion that simply throwing money at poor countries will end people's misery, a lack of money is not the _only_ cause of poverty. Bad governance, malfunctioning institutions and unprotected human and civil rights are the more prominent problems. 

Cash aid can easily land in the hands of corrupt regimes that have absolutely no incentive to bring about an end to poverty and suffering, because that would stop the flow of aid. Zimbabwe, for example, is under the autocratic regime of Robert Mugabe, and receives development aid that amounts to around ten percent of its national income.

Luckily, there are other ways to fight poverty more effectively. For instance, dissemination of scientific knowledge, information about democratic processes, private remittance sent back by those who have immigrated to other countries or capital funding by private investors are all better than lump-sum development aid.

Most importantly, rich countries can lift trade restrictions that effectively lock farmers in poorer countries out of the international market. The World Bank, for example, could provide diplomatic help to countries in need to level the playing field during trade negotiations, and migrants from Africa could be allowed to temporarily enter rich countries in pursuit of higher education.

Indeed, there are many ways to help poor countries escape poverty. Simply throwing money at their problems, however, doesn't seem to be the most effective one.

### 10. Final summary 

The key message in this book:

**Times have never been better for human beings. In rich countries, scientific and political developments have made life quite comfortable. However, many countries have been left behind in the process, and they need help to catch up to modern standards of living.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Poor Economics_** **by Abhijit V. Banerjee and Esther Duflo**

Investigating some of the biggest challenges poor people face, this book provides the reader with an understanding of why there still is so much poverty in the world, and why many of the measures usually implemented do not help. Based on these insights, the authors offer a number of concrete suggestions to demonstrate how global poverty might be overcome.
---

### Angus Deaton

Angus Deaton, a professor at both Princeton University and the Woodrow Wilson School of Public and International Affairs, won the 2015 Nobel Prize in Economics. He is also the author of _The Analysis of Household Surveys_ and _Economics and Consumer Behaviour_.

