---
id: 5e959f986cee0700060a0d59
slug: the-great-mental-models-en
published_date: 2020-04-16T00:00:00.000+00:00
author: Shane Parrish, Rhiannon Beaubien
title: The Great Mental Models
subtitle: General Thinking Concepts
main_color: B9303F
text_color: B9303F
---

# The Great Mental Models

_General Thinking Concepts_

**Shane Parrish, Rhiannon Beaubien**

_The Great Mental Models_ (2019) provides a crash course on how to upgrade your thinking and decision making. Drawing from a wide variety of disciplines, it will equip you with nine of the most essential tools for understanding and navigating the complicated world around you.

---
### 1. What’s in it for me? Upgrade your mental toolkit and make better decisions. 

In most areas of life, you need the right tools to get the job done. Want to fix a clogged toilet? Get a plunger. Want to write a novel? Grab a pen. The more complicated the job, the more tools you'll tend to need. Just look at all the gear inside the workshop of a car mechanic.

The same is true of decision-making — only here, the tools you need are mental devices. To make good decisions in life, you need to think through your options carefully. Certain ideas, techniques and knowledge can help you out with this. How? By enabling you to create a better working model of reality in your mind. We can therefore call them _mental models._

The better your models, the better your chances of success. And the more varied your models, the more prepared you'll be to handle the many challenges life throws your way. Ideally, you'd have a whole toolbox full of mental models — and that's exactly what you're about to receive: not just one, not just two, but nine of the most useful mental models for making better decisions! 

In these blinks, you'll learn about

  * a zany thought experiment that helped Einstein understand gravity; 

  * the dark secrets of a 1920s propaganda pioneer; and

  * the difference between Occam's razor and Hanlon's razor.

### 2. A map is a simplified representation of a complicated reality. 

Mental models are tools that help us navigate reality. The most obvious example is a map. As familiar as it might seem, this classic navigational aid illustrates some of the basic features, benefits, and limits of mental models in general. So it's a good place to start.

The purpose of a map is to represent the world in a way that's useful to us. To accomplish this purpose, a map needs to focus on certain aspects of reality while ignoring everything else. For instance, if you're trying to navigate the London Underground, all you need is a map that shows the overall layout of the subway's rails and stations — the information that will help you get to your destination. A simple array of lines and circles will do the trick.

**The key message here is: A map is a simplified representation of a complicated reality.**

Of course, the resulting map — that network of lines and circles — leaves out a lot of details. But that's unavoidable. To see why, imagine trying to create a map that shows every little detail of the London Underground, right down to the nuts and bolts of the railroad tracks. Such a map would be completely impractical — way too big to fit inside your pocket, and way too complicated to help you get from point A to point B!

So we don't want a map to include every aspect of the reality it represents. But we also don't want to forget that it leaves out a lot of details. Sure, many of them are irrelevant to us, like the nuts and bolts of the railroad tracks. But some of them are important to keep in mind. If you've ever been so busy looking down at your smartphone map that you walked straight into a lamppost, then you know this firsthand. Or if your GPS device has ever led you to a road that's closed, then you know the importance of having a map that's fully up-to-date.

These lessons apply not just to literal maps, but to more metaphorical maps as well. Financial statements, policy papers, parenting manuals, even news articles — they all offer map-like simplifications of reality. A company's financial statement, for instance, condenses thousands of transactions into a single, easily digestible document.

These simplifications are meant to guide us through the complicated world around us. But if we forget about what they're leaving out of the picture, and if we fail to update them as the world changes, they can also lead us astray and get us into trouble.

### 3. Recognize the limits of your circle of competence. 

Imagine all of our practical human knowledge as forming a vast and varied landscape, encompassing everything from knowing how to cook to knowing how to invest in the stock market to knowing how to climb Mount Everest. To navigate most of this terrain, you'll need a map — whether it takes the form of a cookbook, an investment manual, or a mountaineering guide.

Other places, though, will be so familiar to you that a map becomes unnecessary. These places are the skills you've mastered through years of study, practice, and hard work — and they all fall within your _circle of competence_.

**The key message here is: Recognize the limits of your circle of competence.**

Inside your circle of competence, you can move around with well-deserved confidence. You know what to expect, and you can handle the challenges life throws your way. Outside of this circle, though, you're more like a fish out of water. If you're a mechanic, you'll feel right at home when looking under the hood of a car. If there's a problem with the engine, you'll be able to get to the root of it. But if you're not a mechanic, the task might seem as baffling as brain surgery.

Knowing where your circle of competence begins and ends is crucial to success. None of us can know everything, and all of us have areas of ignorance. That's okay. We just need to be honest with ourselves about our limits. That way, we can focus on our strengths, while seeking out help with our weaknesses.

Let's say you want to start a business, but you're terrible with numbers. Why not hire a financial advisor? You could also read up on the subject of financial literacy. Maybe you'll never be an expert, but at least you'll know the basics. And that way, you won't feel totally disoriented, even if you're outside your circle of competence.

But you need to know when you _are_ outside of it. Unfortunately, our egos can lead us to inflate our conception of how big our circles of competence are. As a result, we might venture outside of them with unwarranted confidence, unaware that we're out of our depth. For a vivid reminder of the problem with this, just look at the 200 frozen bodies littering the slopes of Mount Everest. All of them belonged to people who felt confident about their abilities to conquer the mightiest of mountains.

### 4. Solve problems creatively by reasoning from first principles. 

When you focus on your circle of competence, you're playing to your strengths. But there's more to success than mere competency. You also need creativity. It might sound like a cliché, but it's true: you have to think outside the box. Otherwise, you'll end up doing the same old thing that everyone else is doing. But how do you break away from the pack?

The trick lies in digging deep. For any subject or body of knowledge, if you drill down deep enough, you'll eventually find what you're looking for.

**The key message here is: Solve problems creatively by reasoning from first principles.**

First principles are the foundational facts on which knowledge in a certain field gets built. If you're an engineer and you want to build an energy-efficient refrigerator, your first principle would be the laws of thermodynamics. These are the most basic aspects of reality your refrigerator needs to contend with.

But first principles can also be found in less obvious domains, and identifying them can lead to creative solutions. **** Let's say you're a scientist dealing with the problem of overconsumption of meat. You could approach this problem by trying to mitigate the consequences of it. For instance, you could come up with ways of minimizing the environmental impact of livestock farming.

But there's another way you could approach the problem — by asking yourself, "What are the first principles of meat consumption?" That's what scientists did starting in the 1970s. They realized that, for consumers, the taste and smell of meat are the most important things about it. These aspects of meat ultimately depend on chemical properties and reactions, such as the one that takes place between sugars and amino acids when meat is cooked. But the fact that it comes from an animal isn't essential.

After realizing this, the scientists looked for ways of creating artificial meat — lab-grown food that replicates that meaty taste and smell, and mostly obviates the need to raise and kill animals. Fast forward to the present, and there are about 30 labs in the world currently developing artificial meat. Eventually, this meat could make livestock farming a thing of the past.

When you approach a problem from first principles, you're essentially trying to prevent it from turning into a problem in the first place. That's because you're starting with the underlying causes of the problem, rather than just trying to address the effects of it. And this way, you can come up with a creative solution that addresses it at the root.

But this isn't the only way. In the next blink, we'll look at another mental model that can help you get your creative juices flowing.

### 5. Develop your creative problem-solving skills even further by practicing inversion. 

Imagine you're an advertising executive working for the American Tobacco Company in the 1920s. The company has noticed that most smokers are men, and you've been tasked with the following objective: sell more Lucky Strike cigarettes to women. Putting aside any moral scruples, how would you achieve this goal?

That was the question once faced by Edward Bernays — the Austrian-American pioneer of the fields of public relations and propaganda. He answered it by implementing the next mental model we're going to look at. It's a nifty little trick called _inversion,_ which involves taking the normal approach to problem-solving and turning it upside down.

**The key message here is: Develop your creative problem-solving skills even further by practicing inversion.**

There are two basic ways of going about inversion. The first is to assume something is true, and then work backward to show what else would have to be true for that to be the case.

Let's return to Edward Bernays. He started by thinking along the following lines. "Alright, let's assume we live in a world where women do smoke as much as men. What else would have to be true?" He then realized that women would need to feel it was socially acceptable and desirable to smoke. And that, for this to happen, smoking would need to be linked to other things that are socially acceptable and desirable.

Bernays then set about forging this link. Through his advertising campaigns, he presented Lucky Strike cigarettes as an after-dinner treat to replace desserts — in other words, an elegant means to maintaining a slim figure. He also marketed them as "torches of freedom." The women's rights movement was on the march, and Bernays presented smoking as a way for women to show their independence.

So that's one way of approaching inversion. The other one works the same way, but in reverse. This time, you assume the opposite of what you want to accomplish, and then you see what else would need to be true for this scenario to happen.

Let's say you want to be rich. To practice this form of inversion, you'd assume you're poor and then work backward to think about the actions that would lead you to poverty. These would include things like spending more money than you make, and taking out high-interest loans. By identifying all of these behaviors, you can come up with a list of things you should be avoiding if you want to get rich!

### 6. Use thought experiments to test out your ideas and clarify your thinking. 

Now that you've learned first-principle reasoning and the technique of inversion, there's no limit to how many creative ideas you can come up with. Hopefully they'll be more like growing artificial meat than getting people to smoke! But before you try them out in the real world, you'll probably want to simulate them in your imagination first.

**The key message here is: Use thought experiments to test out your ideas and clarify your thinking.**

Thought experiments — experiments you do in your head — have some obvious advantages over actual experiments. To begin with, you can imagine taking risks without forcing anyone to suffer the potential consequences. You can also conduct thought experiments as many times as you want without having to invest time, money, or other resources. And this means you can conduct experiments that would otherwise be impossible or impractical.

For example, imagine if you put someone in a closed elevator, glued his feet to the floor, somehow transported the elevator into outer space without his noticing, and then pulled it upward at an accelerating rate. Would he be able to notice he was in space? Or would he think he was still on Earth, being pulled down by gravity?

This thought experiment might seem ridiculous, but it helped Albert Einstein come up with the general theory of relativity. He realized the answer was no — the two experiences would feel the same. And this helped him work out his ideas about gravity.

By allowing us to think through the consequences of impractical or impossible scenarios, thought experiments can also help us clarify our thinking. 

For example, consider the classic question, What would you do if money were no object? Now, as far as our lifetimes are concerned, money will likely always be an object, so the question might seem frivolous — maybe it's just a fun thing to ask on a date or at a cocktail party. But it can also be a serious thought experiment. That's because in our imagination, it removes the variable of money from the equation of our decision-making. This permits us to think about what we'd want to do for its own sake, rather than the sake of money.

Maybe you'd quit your job. Maybe you'd spend more time with friends. Maybe you'd learn how to play the guitar. Or maybe all of the above. Whatever your answer might be, the question allows you to gain a better idea of what you really value in life.

### 7. Engage in second-order thinking to scrutinize your decisions and bolster your arguments. 

Imagine you've just won the lottery! What's the first thing you'd do? Let's say you buy a bigger house. Great — but now you have to consider how this decision will change your life. For example, you might have to spend more time cleaning. Either that, or have to hire a cleaner.

Either way, you've now gone from thinking about the consequences of something, as you do when conducting a thought experiment, to thinking about the consequences of the consequences. This is called _second-order thinking_.

**The key message here is: Engage in second-order thinking to scrutinize your decisions and bolster your arguments.**

To see the importance of second-order thinking, we just have to look at the problems that can arise in its absence. Consider the overuse of antibiotics in the livestock industry. When farmers first started giving antibiotics to their cattle, they were fixated on the first-order consequences. They simply wanted bigger cattle, which meant bigger profits. But then came the second-order consequences. Some strains of bacteria survived the onslaught of antibiotics, and these were the only ones that were left to reproduce. The result? Drug-resistant bacteria flooding our food chain.

Of course, those consequences were unintended — but that's part of the point. We haven't really thought our decisions through until we've considered their potential second-order consequences. As with the overuse of antibiotics, those consequences can be negative. In that case, we should reassess our choices.

Perhaps the long-term costs outweigh the short-term benefits. For instance, sure, that daily candy bar tastes good now, thanks to the first-order effects of sugar. But ten years from now, do you really want to experience the second-order health effects of indulging your sweet tooth on a regular basis?

It's not all bad news, though. Second-order consequences can also be positive. In that case, we can use them to bolster our arguments in favor of doing something — a pretty useful thing to do. After all, if you want to make good decisions in life, you often need to get other people to sign off on them. Perhaps you need to persuade a boss to greenlight a new project, or convince a partner to agree to a new parenting technique. Whatever the case, you can point out positive second-order consequences to bring other people to your side.

That's what the philosopher Mary Wollstonecraft did when writing _A Vindication of the Rights of Woman_ — one of the earliest works of feminist thought. She argued that giving women the same rights as men wasn't just the morally correct thing to do, it would also have beneficial second-order consequences for society as a whole. A formal education, for example, would enable women to become better citizens.

So don't just look for the dark clouds on the horizon — keep an eye out for the bright spots as well!

### 8. Use probabilistic thinking to weigh your decisions more precisely. 

Second-order consequences can often come as an unpleasant surprise. But that doesn't mean you should get too carried away guarding against them. With any given action, you can imagine all sorts of horrible things happening. If you fixate on them, though, you may become paralyzed with indecision, or overreact by drawing extreme conclusions.

Call it the slippery slope effect. One moment, you're thinking about all the potential negative consequences of drinking alcohol, such as alcoholism. Next thing you know, you're advocating for prohibition! **** That would be an overreaction. Sure, drinking alcohol can lead to alcoholism, but it doesn't always. To temper our fears and assess the potential consequences of an action rationally, we need to think about their _probabilities_.

**The key message here is: Use probabilistic thinking to weigh your decisions more precisely.**

One of the keys to becoming better at probabilistic thinking is to understand an approach called _Bayesian updating_. The basic idea here is that we already possess information about the world. Yes, that information is limited, but it's also useful, and we should make the most of it. That means that when we encounter new information, we should assess it in light of our prior information, before we reach a conclusion.

For instance, let's say you open a newspaper and read a headline that says "Violent Crime Skyrocketing." What do you do? Freak out and never leave the house again? ****

Not if you're a Bayesian thinker. In that case, you'll remember that violent crimes have been declining for decades. As a result, even if the crime rate doubled this past year, it could have just moved from 0.01 percent to 0.02 percent. This would mean that 2 in every 10,000 people were now victims of violent crime. That would certainly be unfortunate for those two people, but it shouldn't cause you to panic. The chances of you yourself being a victim are still extremely slim.

However, this doesn't mean we should dismiss new information while clinging to our old information. The world changes, and we need to update our understanding of it accordingly.

By itself, the headline about violent crime "skyrocketing" should only slightly diminish your belief that the crime rate is low. If true, it's basically telling you that the rate is just a tiny bit higher than you thought it was. So you should only adjust your belief a tiny bit as well. But if the rate continues to rise and you read that same headline year after year, you'd eventually need to replace your prior belief with a new one: the crime rate is now high. And in that case, maybe you should be careful when you go outside!

### 9. According to Occam’s razor, the simplest of two or more equally compelling explanations is the most likely to be true. 

Imagine you wake up one morning feeling sick. Naturally, you go online and look up your symptoms — which, you discover, could either be caused by the flu or by Ebola. Which explanation should you believe? The answer is important. It's the difference between going back to bed or going into quarantine.

In a situation like this, you're no longer just weighing an individual piece of evidence, as you do with probabilistic thinking. Now you need to decide between two overall explanations for the evidence you have at hand.

To cut through a problem like this, you can use a tool called _Occam's razor_.

**The key message here is: According to Occam's razor, the simplest of two or more equally compelling explanations is the most likely to be true.**

The basic idea behind Occam's razor is this: given two explanations that seem to account for the facts equally well, the simpler explanation is more likely to be true. That's because the more complicated explanation has more variables that need to be true than the simpler one. And with each additional variable, the explanation becomes less likely to be true.

For example, imagine your friend hasn't shown up to your party, and you haven't heard from him. Maybe he got into a car accident — but that would require many variables to be true. Did he leave his house? Did he drive a car instead of walking or taking a bus? Did he or another driver make an error? Did that error lead to an accident? The answer to all of these questions needs to be yes for the explanation to be true.

The simpler and more likely explanation is that he's just running late. For that to be true, he only needs to have done something simple like forgotten to look at his watch.

Of course, sometimes many variables do line up, and unlikely things like car accidents happen as a result. So none of this is to say that your friend crashing his car is impossible. The point is merely that simpler explanations tend to be true more often than complicated ones. The simpler explanation is therefore the safer bet.

So now let's circle back to our original problem. Unless you live in — or recently traveled to — a place experiencing an Ebola outbreak, you don't need to freak out about your fever. Is it theoretically possible that you contracted Ebola virus through some complicated series of events? Sure — but the flu is the simpler and more probable explanation.

### 10. According to Hanlon’s razor, the simplest and most likely explanation for misbehavior is stupidity, rather than malice. 

From deadly illnesses to mere tardiness, Occam's razor provides a great way to decide between competing explanations of things. But the vagaries of human behavior are varied, and sometimes we'll need another type of blade. This brings us to our final mental model: _Hanlon's razor._

You can think of Hanlon's razor as a close cousin to Occam's razor. Like its more famous relative, Hanlon's razor starts by reminding us that the simpler explanation — all other things being equal — tends to be the safest bet. But then it adds a special twist.

**The key message here is: According to Hanlon's razor, the simplest and most likely explanation for misbehavior is stupidity, rather than malice.**

Imagine you're driving a car. You're cruising along, minding your own business, when all of a sudden, another driver cuts you off. Before you get carried away by road rage, ask yourself, What's the explanation for her behavior?

Well, one is that she acted with malice. In other words, you could assume there's some sort of evil intention behind her action. But for that to be true, a whole series of conditions would also have to be true. She would have to have seen your car, had a desire to cut you off, decided to act on that desire, come up with a plan to get in front of you and then maneuvered her own car accordingly.

The simpler explanation? It was just a mistake. She didn't even see you.

The underlying idea here is that intentional acts of wrongdoing tend to require a lot of time, energy and thought on the part of their supposed perpetrators. Mistakes are a lot easier to make; they just require ignorance, laziness, oversight or a lapse of judgment.

That's not to say that people never do evil on purpose. It's just a healthy reminder that this is the exception to the rule. It might not always feel this way, but the truth is that people usually aren't out to get us, even when they do us wrong.

Instead, like us, they're often just stumbling through life, stepping on other people's toes in the process. Sometimes those toes happen to belong to us, and the result can hurt — but we shouldn't take the pain so personally, and we shouldn't assume the worst of intentions.

So in this case, rather than succumbing to road rage, we should deal with the situation as best we can and keep a cool head. And then we should move on with our lives.

### 11. Final summary 

The key message in these blinks:

**Reality is a complicated place, but mental models can help us to navigate it. Each model has its specific uses, strengths, and limitations. If we wield them correctly, we can sharpen our thinking, develop our knowledge, increase our understanding of the world and make better decisions in our lives.**

Actionable advice:

**Turn theory into practice.**

The point of using a mental model isn't just to arrive at better insights into the nature of reality, it's to use those insights to make better decisions in your life. What use will your newfound knowledge be if you don't do anything with it? Imagine realizing that you interrupt people too much — and then continuing to interrupt people anyway. The realization only gains value if it leads you to adjust your behavior.

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Super Thinking,_** **by By Gabriel Weinberg and Lauren McCann**

You've just learned nine of the most essential mental models — but there are plenty of other tools to put in your cognitive toolbox. Hungry for more? Check out our blinks to _Super Thinking_ by Gabriel Weinberg and Lauren McCann.

Here, you'll learn about other mental models, such as the veil of ignorance, which can help you make fairer decisions as a leader. You'll also learn about pitfalls to avoid, such as the conjunction fallacy and the fallacy of confusing correlation for causation.
---

### Shane Parrish, Rhiannon Beaubien

Shane Parrish is a former cybersecurity expert who worked for the Communications Security Establishment (CSE) — an agency of Canada's Department of National Defence. He is host of _The Knowledge Project_ podcast and the founder of Farnam Street — an online learning community and blog. Rhiannon Beaubien is also a former member of the CSE and writes for Farnam Street's blog.

