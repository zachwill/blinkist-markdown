---
id: 57bc6801003a4b0003f1a911
slug: the-great-race-en
published_date: 2016-08-26T00:00:00.000+00:00
author: Levi Tillemann
title: The Great Race
subtitle: The Global Quest for the Car of the Future
main_color: 2D5DE0
text_color: 2D5DE0
---

# The Great Race

_The Global Quest for the Car of the Future_

**Levi Tillemann**

_The Great Race_ (2016) is a comprehensive history of the competition to produce electric vehicles. These blinks detail the various roadblocks that emerged in the effort to build electric vehicles, and how different countries and companies sought — and sometimes managed — to overcome them.

---
### 1. What’s in it for me? Join the race to develop a desirable electric car. 

In 1886, German engineer Carl Benz developed the world's first combustion-engine vehicle and revolutionized personal transport. A few decades later, during the 1930s in the United States, the visionary Henry Ford revolutionized the car again, making it affordable for all sectors of society.

Today, we are on the cusp of yet another automobile revolution.

Ever since the 1960s and 1970s, when major cities around the world began to suffocate in smog, calls for everyday transportation with less polluting engines have become progressively louder. Now, with electric vehicles on the rise, we are witnessing a pivotal moment in the history of automobiles.

In these blinks, you'll delve into the history of the electric car, take a detailed look at its pioneers and visionaries, the competition to develop an electric vehicle, and the winners and losers in the race to an electric future.

You'll also learn

  * what the term "Fordism" means;

  * why you shouldn't promote nuclear energy in Japan; and

  * why Chinese people don't need a Tesla.

### 2. After Henry Ford transformed automobile production, US car manufacturers dominated the early auto industry. 

Cars have changed the world like no other technological advancement, and that's largely due to one of the automobile's early innovators: Henry Ford of the Ford Motor Company. While Ford didn't invent the car, he did transform the way it was built.

In 1903, he founded the Ford Motor Company, which began producing its first car — the Model T — shortly after. By 1910, the company was building 20,000 Model Ts every year. From then on, production continued to skyrocket, and by 1927, Ford was building one car for every five Americans.

But such incredible productivity was no coincidence — it was a result of Ford's groundbreaking production philosophy, which we now know as _Fordism_. The idea was simple: Ford only built one model of car, in one color. His goal was for the finished product to be durable and to have mass appeal.

By sticking to one model, Ford was able to utilize an _assembly line_ system, wherein a car moved along a row of workers, each of them responsible for just one step, until it was completed. This ingenious system allowed Ford to keep his prices so low that even poor Americans could afford his company's cars. It allowed him to export them to Tokyo, Shanghai and other cities around the world.

So, by the time the 1930s rolled around, the three US car companies, Ford, General Motors (or GM) and Chrysler, were dominating the international auto market. For instance, in 1938, while Germany, Italy and Japan were producing a combined total of 437,000 vehicles per year, the United States was producing about 3.5 million!

And while Ford was focused on a single model, GM — a collection of many small manufacturers — was diversifying its mass production. Similar to GM, Chrysler combined several marques such as Plymouth or Dodge.

However, while the auto market was dominated by US companies early on, you'll soon see how Japanese car companies quickly took the lead as people started to become increasingly concerned about emissions.

### 3. Japanese companies were the first to tackle the issue of vehicle emissions. 

It's no secret that the Japanese car manufacturers of today outcompete their American counterparts. But do you ever wonder why? It's primarily due to the close attention they paid to the problem of car emissions.

In the second half of the twentieth century, smog became an increasingly serious issue and the auto industry found itself slapped with more stringent environmental regulations.

It was simple math, really: since cars emit pollution that causes smog, the more cars on the roads, the more smog in the air. So, it was just a matter of time before environmental regulations were introduced to slow down the booming auto industry. When the first states introduced them in the 1970s, automakers in America claimed the regulations were so strict as to be "impossible" to follow.

At the same time, their competitors across the Pacific Ocean were also experiencing an auto boom, and smog was similarly becoming a major issue in big Japanese cities like Tokyo. As a result, environmental regulations in Japan forced companies to strive for lower emissions.

Although Toyota and Nissan dominated the Japanese auto industry at the time, it was the then-underdog company Honda which was the quickest to seek out innovative solutions to the emission rules. At first, the Honda team, under the guidance of founder Soichiro Honda, experimented with alternative fuel sources like hydrogen. But they quickly found that such new technology was hard to implement, and instead changed their focus to redesigning the combustion engine.

Their work resulted in a redesigned cylinder that was much more fuel-efficient and, therefore, less polluting. In fact, the design was so effective that, in 1971, Mr. Honda announced that his cars would meet the 1975 Japanese environmental standards by 1973.

It was at this point that Japanese automakers really seized control of the global market. To meet the new, strict environmental standards at home, US companies had to license this technology from Honda.

But the United States wasn't the only country paying attention to Japan's rise in the auto industry. Their success also caught the attention of China, and one Chinese engineer in particular jumped at the chance to get his country started on its way to becoming an automotive superpower.

> _"Small innovative companies could sometimes achieve standards the big boys had said were impossible."_

### 4. A Chinese engineer at Audi brought China into the great race to build an electric car. 

Once the auto industry had reduced its emissions, the next frontier was _electric vehicles_, also known as _EV_. Such cars would produce no emissions at all and China was the first country to really attempt to make them a success. How did China find itself leading the way?

In the 1990s, when California was again poised to tighten environmental regulations, automakers rallied against any new laws. It was then that Chinese engineer Wan Gang, of the German automaker Audi, took notice. He had studied in Germany and had been working for Audi for a few years — but his dream was to transform the Chinese auto industry.

To do so, instead of borrowing, buying or stealing technology from established auto manufacturers, he wanted to breathe new life into China's car companies. However, China's huge population and corresponding demand for cars made the pollution from auto exhaust a huge risk.

As a result, Wan's vision was to make the Chinese auto industry the most environmentally friendly in the world. He began by presenting this vision for a new Chinese car industry and a technologically advanced, clean car to a Chinese delegation of high-ranking government officials, who quickly brought him back into the country.

He was appointed professor at Tongji University and was put in charge of a secret government project to develop electric vehicles. The project was extremely well funded since the government was keen to beat Western carmakers to the punch.

And because of China's growing position in the world economy, nothing seemed out of reach; the project raced out of the gates. But, as you'll see, it, unfortunately, didn't come to fruition.

At the same time, however, a nuclear engineer in Japan was teaming up with Mitsubishi to build an electric vehicle of their own.

### 5. In the early 2000s, a Japanese nuclear engineer set out to build an all-electric vehicle. 

At the turn of the twenty-first century, with Honda having the best Japanese combustion engine on the market and Toyota content working on hybrids, there were still no truly electric vehicles on the Japanese market. But that all changed thanks to Takafumi Anegawa, a Japanese nuclear engineer.

While everyone in the Japanese auto industry was rushing to catch up with Toyota's hybrid technology, Anegawa was setting out to build a real electric car. After all, hybrids — the best the industry had to offer at the time — used both combustion and electrical power, so they still burned fuel.

So, Anegawa saw the need for a truly electric vehicle, and his vision was to build one that ran on nuclear generated electrical power, thus allowing Japan to break free from foreign oil. However, the Japanese government wasn't especially keen to promote nuclear energy: the island is regularly struck by earthquakes and typhoons, both of which pose a catastrophic threat to nuclear power plants.

As a result, the Japanese public is largely skeptical of nuclear power. People soon started referring to Anegawa as "crazy Anegawa."

Undeterred, Anegawa still took his idea to Toyota and Honda, but the auto giants declined as they were so focused on their respective work with hybrids and combustion engines.

Luckily, there were plenty of other eager auto manufacturers out there, and Anegawa launched his electric vehicle program in partnership with Mitsubishi, Subaru and his own employer, the Tokyo Electric Power Company or TEPCO. This collaboration was of particular interest to Mitsubishi, since the company had been struggling for several years.

The project received government funding, primarily from the Ministry of Economy, Trade and Industry, or METI, to develop an electric car that was affordable for the general public, which meant lowering the target price from $48,000 to $35,000. They had soon produced their first electric vehicle, the Mitsubishi iMiev, one of the first Japanese electric cars intended for mass production.

But while Anegawa was hard at work in Japan, Elon Musk was transforming the US auto industry. Let's look at his story next.

### 6. With Tesla, Elon Musk revolutionized the US auto industry and joined the battle for the best electric vehicle. 

Have you seen the movie _Iron Man_, with the main character Tony Stark? Well, did you know that Stark's character is based on Elon Musk?

It's true. The film follows the billionaire engineer Stark as he transforms into the action hero Iron Man, and Musk, a real-life billionaire, engineer and visionary, was the perfect template.

So what was Musk's claim to fame?

He founded the electric car company Tesla, and in the process transformed the American auto industry. Musk began with an expensive sports car, aimed at wealthy customers and powered by 6,831 lithium-ion batteries. He then released the Model S, a luxury sedan built to outperform any fossil fuel-powered competitor. After that, he turned his attention to building a smaller electric car that would be accessible to the general public.

So how did he do?

Well, despite some initial technical hang-ups and lack of confidence from financial markets, Tesla cars are now on the road and the company has a growing fan base, largely propelled by the launch of the Model S.

This should come as no surprise because the sedan is a magnificent car and was in fact developed with the help of the US government.

Here's what happened:

When the 2008 financial crisis struck, two of the three major US car manufacturers were bailed out by the federal government. At the same time, Tesla was in trouble and needed a $465 million loan from Washington. Getting this money let the company realize its vision and launch the Model S.

This was significant because the car is nothing short of remarkable. It can go 300 miles on a single charge, can accelerate in an instant and has the best crash test ratings ever. In fact, _Consumer Reports_ has even said that the Model S is the best car they've ever driven.

Soon after Tesla revolutionized the electric vehicle industry, and in the process joined the race with Japan, a catastrophic natural disaster would threaten Japan's electric vehicle projects.

### 7. A major catastrophe and diplomatic tensions with China put Japanese electric vehicle projects in danger. 

Japan is no stranger to natural disasters and, in March of 2011, a massive tsunami once again threw the country into turmoil. The colossal wave struck off the Japanese coast and nearly caused a nuclear catastrophe.

The 2011 tsunami was the biggest ever to reach Japanese shores, causing waves of over 100 feet. The results were devastating, with many cities destroyed and thousands of lives lost — but another major issue was that Japanese nuclear plants tend to cluster along the country's coastline. So, while some of them are built at high elevations and are protected by huge walls, the plant at Fukushima wasn't so fortunate.

While a major explosion at the plant was averted, the reactor leaked huge quantities of toxic water and, due to safety issues, every one of Japan's nuclear reactors was shut down.

This fact drove TEPCO, the operator of the Fukushima reactor, to bankruptcy. If that name sounds familiar it's because TEPCO was also the sponsor of Japanese engineer Takafumi Anegawa's electric vehicle project, which now found itself on thin ice.

Soon, the project was faced with yet another threat, because diplomatic tensions with China were endangering _all_ Japanese electric vehicle projects.

So-called _rare earths_, a type of chemical element found in the Earth's crust, are an essential component for electric vehicle batteries. China essentially had a monopoly on these substances, producing about 97 percent of the world's supply in 2010.

This had made Japan particularly dependent on China, and China suddenly halted deliveries to Japan when, in 2010, the Japanese coast guard imprisoned a Chinese captain due to a conflict over territorial rights in the Pinnacle Islands off the East China Sea.

The Japanese electric vehicle industry was only saved in 2013, when Japan discovered a great source of rare earths at home.

But did the Chinese really withhold their supply of rare earths over a single sea captain? The truth is, what they really wanted was to take control of the global electric vehicle industry.

> _"Anegawa's atomic dream had transformed Japan's nuclear nightmare and his beloved reactors were in dire straits."_

### 8. Chinese attempts at an electric vehicle failed because of weak demand and poor technology. 

China has the largest population of any country in the world and one would assume that they have a market for everything. Well, as it turns out, they don't have one for electric vehicles.

But to learn why, we've got to go back to Wan Gang, the Chinese engineer who wanted to get his country started on the road to an electric vehicle. In 2013, he still held this aspiration; but while the Chinese auto market is the largest in the world, people there weren't showing much interest in electric cars.

So, Wan secured subsidies for research and development, and even ones for customers — but his vision failed nonetheless. Besides the relatively poor quality of his product and a lack of electrical infrastructure, the main reason for the failure was simply low demand.

Lots of Chinese people don't need a car at all, much less a high-performance one like a Tesla. After all, the majority of them live in urban apartment buildings that don't even have parking spaces. What they really need is an electrically powered public transportation system that can accommodate a billion people.

But another issue was that Chinese companies simply didn't have the technology to compete in the electric car industry. A great example is the battery manufacturer BYD, in which even Warren Buffett bought a 10 percent share.

The company got their start making batteries that were not exactly up to environmental standards before going on to produce lithium-ion batteries in partnership with Motorola. Later on, they would switch their focus to sustainability, producing things like solar panels, electric bikes and power storage devices. However, their shot at an electric vehicle failed miserably, because their technology was poor and was priced far too high.

And this wasn't an isolated incident; the company was representative of the situation across the board in China, which just didn't have the technology to develop its own electric car.

So, China couldn't cut it — but, as we'll see, California and Japan continue to soar ahead.

> _"But China scarcely had the technology to design, assemble, and produce one viable EV model, not to speak of dozens."_

### 9. California and Japan are at the front of the pack in the race for the best electric vehicle. 

From the passage of the first environmental law in California to the launch of the Tesla Roadster, it's been a wild ride. But in the end, California's concerted efforts have paid off and the state is now the world leader in the electric vehicle industry.

So, while the race for the best electric vehicle is still in full swing, California has a substantial lead. While the US auto industry is under the control of Washington and most cars are still produced in Detroit, California is picking up momentum.

And it wasn't just federal dollars that got them there. After all, China invested the most money of anyone into its national electric car projects, while California barely invested any. Instead, it was the fact that California saw electric vehicles as a long-term investment in air quality, reduced dependence on foreign oil and better environmental health in general.

But it's also important to say that without Elon Musk, the founder of Tesla, California would never be where it is today. After the successful launch of the Model S, Musk announced the creation of a _gigafactory_ that will produce batteries on a huge scale, thereby reducing the cost of the third model by about 30 percent.

However, Japan has become the clear leader in terms of electric cars in general. By 2011, the majority of the world's electric vehicles were produced in Japan, and the country was also making motors, batteries and other technological features for most electric vehicles around the world.

So, despite frequent natural disasters, the country is still managing to hold onto its position at the top of the industry. Not only that, but Takafumi Anegawa, the Japanese visionary, managed to convince his country that electric vehicles _are_ their ride to the future.

And this isn't just the case for Japan, it's true the world over. Thanks to Japan and the state of California, we've gotten a good start at integrating this technology into our lives — for a cleaner environment and a safe planet.

> _"In 2012, little California deployed more electric vehicles than mighty China."_

### 10. Final summary 

The key message in this book:

**Electric vehicles are our ride to a cleaner future, but the road to that future has been riddled with obstacles. While many companies and countries have taken a shot at overcoming these hurdles, California and Japan are currently leading the pack with innovative ideas and high-quality products.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Elon Musk_** **by Ashlee Vance**

Elon Musk (2015) gives us an insight into the brilliant and difficult character of today's most innovative entrepreneur. Interwoven with details of his turbulent private life, these blinks reveal why Elon Musk is so determined to save the human race, how he's worked towards this goal so far, as well as what's on the horizon for potentially the richest and most powerful man of our future.
---

### Levi Tillemann

Levi Tillemann is an American businessman and author. He is a former special advisor of policy and international affairs at the US Department of Energy and the former CEO of Iris Engines, where he worked with his father to develop a smaller, more powerful and energy-efficient engine.

