---
id: 5e70e5b16cee070006244024
slug: the-greater-freedom-en
published_date: 2020-03-21T00:00:00.000+00:00
author: Alya Mooro
title: The Greater Freedom
subtitle: Life as a Middle Eastern Woman Outside the Stereotypes
main_color: FF82B1
text_color: CC2B68
---

# The Greater Freedom

_Life as a Middle Eastern Woman Outside the Stereotypes_

**Alya Mooro**

_The Greater Freedom_ (2019) chronicles one British-Egyptian woman's struggle to forge her own identity from the two cultures that raised her. Using stories from her own background, detailed research, and interviews with fellow women of the Arab diaspora, author Alya Mooro examines issues including sexuality, Islam, beauty standards, and immigration. She ultimately finds that there is freedom in choosing to exist in-between established tropes of culture, nationality, and identity.

---
### 1. What’s in it for me? Learn the unique perspective of a woman growing up between two cultures. 

What happens to women who grow up in a tug-of-war between two cultures? What happens when one of those cultures is subject to painful stereotypes by the other? 

For an increasingly large Arab diaspora living in Europe and the United States, this question has defined their lives. For Alya Mooro and the dozens of women of Arab descent she interviews, forging an identity in the midst of the mixed messages has been a difficult journey. 

In these blinks, we'll follow along as Mooro recounts her own struggles with the lack of media representation of and for women like her, the pressure to conform to unrealistic Western beauty standards, and her journey toward a healthy relationship with both sex and Islam. Ultimately, Mooro's challenges will lead her to catharsis, discovery, and self-actualization.

In these blinks, you'll learn

  * why sex is such a fraught issue for many women in the Arab diaspora;

  * why media representation is crucially important for kids in immigrant communities; and

  * why Alya stopped telling people she's "technically" a Muslim.

### 2. As an Arab girl growing up in Britain, Alya felt caught between two cultures. 

Growing up, Alya often felt defined by her otherness, rather than her own unique personality traits. 

In predominantly white Britain, people of color are often subject to lazy caricature. That's why people who aren't white are often confused with other people from the same background. For example, the British media has mistakenly used a photo of the Belgian footballer Lukaku in stories about grime artist Stormzy. 

Alya also personally experienced this. At school, she was often confused with other brown girls in her class: another Egyptian girl, as well as a girl who was half Pakistani and half Italian. 

But Alya isn't the only one who has struggled with proactively defining her identity. What "Arab" means has always been unclear: even UNESCO and Wikipedia list different numbers of Arab states. Many nationalities in the Middle East traditionally don't consider themselves Arabs at all, a feeling shared by Egyptians too.

Diaspora Arabs have had to define their identity in a new way, though. In the melting-pot of London, surrounded by so many other nationalities, Arab identity has crystallized. Alya's Middle Eastern neighbors and her own family bonded over their similarities, in contrast to the white, British norm.

But even if Alya and her friends identify as Arab, that doesn't mean there's a place for them. Arab girls growing up in the UK are often forced to choose between identifying as white or black. This happens in highly segregated schools, as well as on official forms, where "Arab" often isn't an option.

Looking for role models, she observed that mainstream media doesn't portray realistic versions of Arab characters. For instance, while representation has gotten better since 9/11, it's only because there are more stories about terrorism — and Arabs are relegated to playing terrorists.

A study that analyzed television shows in 2015 and 2016 showed that 92 percent of scripted shows had no season regulars of Middle Eastern origins. Of the ones that did, 78 percent appear as terrorists, agents, soldiers, or tyrants, and 67 percent spoke with an accent. 

This is especially harmful for children, who see only distorted versions of themselves reflected in the media. Without good role models, kids can become listless, and potentially even perform the stereotype. If society expects Arab children to be angry, it's a lot easier to actually become that way.

### 3. Arab women are under tremendous pressure to look good; an aesthetic which is generally defined by European physical traits. 

All women face pressure to be physically attractive, and all women are judged on their appearance. Being attractive opens doors and creates opportunities: this is called the Halo Effect.

But for Arab women, the pressure is more intense. Arab women are held to a higher standard, by society, their families, and themselves. One Egyptian-British friend of Alya's told her that in London, she frequently does the shopping with her hair tied up, but in Egypt, she wouldn't dream of it. Worse, she would judge anyone in Egypt who went out with their hair up as slovenly.

As a young woman, Alya felt alienated by the images of beauty around her. European physical traits are considered more attractive, for reasons spanning colonialism to the proliferation of the internet. One of the first things Alya learned was how to conform to the standards of beauty around her. But for someone in her body, conforming to European standards of beauty is difficult and painful.

For most Arab women, their typically curly, thick, and black hair is viewed as a problem to be dealt with. For example, when she was 13, as a means of fitting this standard, Alya started chemically straightening her hair with poisonous ointments that burned her scalp and eventually even caused hair loss. To this day, she has never done anything important, personally or professionally, with her hair in its natural state.

When it comes to body hair, the typical routine for Arab women is to remove any and all body hair below the eyelashes with hot wax on a biweekly basis. It's a painful rite of passage for girls as young as nine, according to Alya's aesthetician. 

Though this is slowly changing, it's still considered newsworthy when a prominent woman chooses _not_ to remove her body hair, and dares to be seen in public in her natural state.

Another challenge Alya faced was with her body, which wasn't as slender as the images of beauty around her. It wasn't until the ubiquitous popularity of the half-Armenian Kim Kardashian that the culture of beauty started to shift away from the European fashion model look that compelled Alya to squeeze herself into uncomfortable jeans that clearly didn't fit. Kim and her sisters helped her find beauty in the color and shape she was born in. 

On top of that, for much of her adolescence, Alya not only felt unhappy in her own skin, she was also uncomfortable with her own sexuality. We'll find out next how this discomfort led her to make some unhealthy choices.

### 4. Alya went through puberty in both Cairo and London, attuning her to the unique contradictions faced by young Arab women. 

When her family moved back to Cairo for a year when she was 13, Alya was devastated. She had to leave the friends it had taken her so long to make. On the first day of her new school, though, she felt welcomed. Many kids were members of her extended family, or had grandparents that had known her grandparents and so she felt like she was part of a community.

There's a cocoon of safety in everyone knowing everyone, as she learned that first day at school in Cairo. But that's also why Arab societies can be so stifling, especially for women. 

In Middle Eastern societies, nothing is more policed than a woman's sexuality. Behavior is regulated by gossip, and everyone is the jury. In Cairo, a woman can't walk down the street without being cat-called, regardless of how old she is or what she's wearing. The burden of abstaining from sexuality — while looking sexy at all times — is placed firmly on girls' shoulders. 

The social regulation on sex means that natural rites of passage are furtive and hasty, which makes them feel sordid. Alya's first kiss was in a filthy elevator that she and her boyfriend rode up and down, trying to gather up the courage. It was the only place they could be alone.

When Alya's family moved back to the UK, she brought the internalized jury of Middle Eastern society back with her. She started acting out at home, chafing against the relatively strict boundaries set by her parents, especially her 9pm weekend curfew. She stayed out late, drank Bacardi Breezers, smoked joints, and lied to her parents constantly. Although they didn't always believe her lies, she got into trouble a lot.

Added to these behaviors, sex started taking up an increasingly large part of her mental energy — as it does for many teenagers. For one thing, casual sex was happening all around her. The house parties in London were very different to those in Cairo. There, she'd walk into a room to see a couple kissing, only to have the girl deny it later, while in London, she'd see a couple engaged in enthusiastic fingering in the middle of a crowded dance floor. 

As she and her friends started exploring their own sexualities, her two cultural identities came into opposition. For Arab girls, she was taught, sex is dirty and shameful. But for Western girls, it's not a big deal to sleep with someone you like. 

Where did that leave Alya, with her omnipresent invisible jury?

### 5. The shame that Alya had internalized about sex set her up for years of traumatic sexual experiences. 

When she had sex for the first time, it was under what Alya describes as "really shitty circumstances." She'd been taught to be conscious, and ashamed, of her body at all times. For her, guilt was inextricable from sexuality.

When she was 15 she met a guy she calls Satan. They exchanged numbers, and he started leaving her long, sappy voicemails. The trouble was, he was her friend's boyfriend. Alya felt guilty, but thought that this was how you're supposed to feel when you like someone. 

When she first slept with him, it was under conditions of questionable consent. But because he'd made her promise not to tell anyone about their relationship, she didn't share her concern around the questionable circumstances with anybody. Moreover, she'd been raised to think that _all_ sex was wrong, not just the kind they'd had. 

Eventually, word of their liaison got out, and in response, the girls at school turned on her, calling her a "whore" in the hallways. Worse still, her mother found out too. A familiar practice with her when it came to the relationship with her parents at the time, Alya lied like her life depended on it. Albeit it grudgingly, her mother believed her.

Some women are murdered by their families when news like this comes out; as many as 5,000 per year worldwide. Fortunately, Alya's mother's response was to give her the "sex talk." Less fortunately though, not only was it over a year too late for them to be having this conversation, it also completely ignored the role of women's desire in sexual relationships. 

In hindsight, her mother admits that this approach wasn't optimal. After all, her mother is just as susceptible to the invisible jury as Alya is, and was really just relating what her own mother had told her.

The women in her family were certainly not alone in facing Arab society's impossible expectations. In one example, Alya recalls the sister of one of her friend's being dumped by her fiancé for not being a virgin — after she'd lost her virginity to him!

Alya, and other women in her community that she knows, had to learn how to enjoy casual sex after years of being taught that it was dirty and wrong. She eventually found the liberation she sought through hypnotherapy. 

Since then, she's been able to build a satisfying, lasting "friends with benefits" relationship. Sometimes, though, in her newfound sexual liberation, the fact that she _could_ hook up with anyone would override the _should_. Sex is the easy part — the difficulty is finding someone worthy of sharing that moment with.

### 6. In the Arab world, marriage is another minefield of stifling expectations for women. 

When it comes to marriage, Arab women face impossible expectations that completely disregard whatever their individual wishes might be. Not only is the expectation that they get married in the first place, women must marry young, marry the right person, and shoulder the burden for sustaining the marriage for the rest of their lives. 

This perception extends to include, for instance, situations like when a man has extra-marital affairs. In such scenarios, it's widely seen as the wife's fault for failing to maintain his attention. The same goes for divorce. In a survey of over two thousand respondents across the Arab world, divorced women were usually blamed for failing to keep their husbands happy.

For the majority of Arab families, the expectation is that young people will marry someone who looks like them, and share the same beliefs. 

When Alya was dating her ex-boyfriend, who is black, the couple faced a great deal of racism. Her friends gossiped about their interracial relationship, and even her mother had said "You know he's black, right?" when she first saw his photo. After they broke up, a family member confided that she had secretly wished for the relationship to end.

Marrying someone of the same religion is also a common cultural expectation. In many Arab countries, this expectation is backed up by laws that don't recognize inter-faith marriages. If, in theory, Alya were to marry a non-Muslim and have children with him, their marriage wouldn't be recognized in Egypt. This means their children wouldn't be able to get Egyptian birth certificates, and she wouldn't be able to legally share a hotel room with her husband. 

After much soul-searching, Alya realizes now that she's most comfortable when she's single. This reflects a growing trend in England, where the age of the average bride is now 35, which for Alya at the time of writing, is three years away. 

In the Middle East, it's very different. According to UNICEF, one out of five girls in the MENA (Middle East and North Africa) region is married by the time they're 18.

What's more, Arab girls and women are expected to adapt themselves to fit the requirements of their husband — even if they don't have one yet. As Alya gets older, she's noticed that her family and community have begun to see her as a pitiful case. Now, she's often told that if she wants to find a husband she should be less outspoken. 

It's taken her many years, and a lot of heartache, but Alya has finally realized that the longest relationship she'll ever have is the one with herself. So, it's worth putting in the effort to make it a good one.

### 7. Caught between rising Islamophobia in Britain and her Muslim cultural identity, Alya struggled to find her place. 

For years, Alya would tell people she was "technically" a Muslim. She did this because the 9/11 attacks, and the resulting Islamophobia, made her want to de-emphasize her Muslim identity in her British communities.

Even scarier than this, though, is the judgement from practicing Muslims. She constantly lives in the expectation that she'll be called out as a "bad Muslim."

But she realized that the need to identify as a moderate Muslim is unique to the Arab diaspora. After taking a poll on her Instagram stories, she realized that many diaspora Arabs feel the need to qualify their Muslim identity as "moderate" because they're living as minorities in Western countries. For Muslims in the Middle East, though, religious identity is much more fluid.

Assuming everyone from the Middle East is a pious Muslim is the same as assuming everyone in America or Europe is devoutly Christian. It's not based in reality. In fact, frequently restrictive ideologies are as alienating to Muslims as they are to non-Muslims. For example, in famously devout Saudi Arabia, six out of the ten most-watched YouTube channels are satirical shows produced by carefree young people. As with everything else, religion is what each individual brings to it.

Indeed, stereotyping Muslims and its associated Islamophobia has had tragic consequences, including the murder of many innocent people. In the UK alone, there was a 500 percent surge in Islamophobic attacks in the period following the Manchester Arena bombing, in which a man of Libyan descent detonated an explosive, killing 23 people. 

Even Alya herself is subject to Islamophobic paranoia. Once, on a flight from Cairo to London, she panicked when a group of men who she thought looked like terrorists boarded her plane. When the plane landed safely, she felt horrified that she had succumbed to the lazy, racist generalization. 

The problem, again, is lack of representation. So-called "moderate Muslims" aren't prominent in mainstream culture, and so when people see anyone who is Muslim it's assumed that they are an extremist. There's no equivalent, popular notion of what a "secular Muslim" looks like. 

The reductive lens through which Islam is currently viewed is damaging for both Muslims and non-Muslims. Non-Muslims are left feeling fearful of an entire group of people, and cultural Muslims like Alya are afraid to identify as Muslim at all. 

One thing she's learned, though, is that her voice is valid too. Things will only change if she and others like her take their seats at the table.

### 8. Alya’s embrace of feminism stems from troubling experiences she had in both the West and the Middle East. 

For many years, Alya didn't identify as a feminist. She bought into the gendered stereotypes that men are chill and women are crazy. She thought she was an exception to her gender because she didn't care about makeup and liked football. 

Eventually, though, she saw that sexism is all around us, and we internalize certain narratives. For example, a study asked participants to watch a startup pitch and decide whether the company deserved investment. When the pitch was voiced by a man, 68 percent thought the startup should get funding, but when a woman delivered the exact same pitch, only 32 percent said the same.

As she became an adult, Alya was increasingly offended by the way men saw her. Once at a club in Miami, she was talking to a rapper who kept forgetting her name. When she called him out, he responded by saying it didn't matter. There were so many sexually available women there he didn't need to put in any effort to get to know her. She felt, as she puts it, like a "disposable vagina."

It's not so different, Alya realized, from the way girls and women in the Middle East are kept out of sight by their families for fear they will drive men wild with desire. Both approaches reduce women to a sum of their body parts. 

Sexism is certainly a problem in the West, but in the Middle East it's particularly acute. In the 2017 Global Gender Gap Report, six of the ten worst countries for gender inequality were in the Arab region.

In the West, it's convenient to cite Islam as the reason for the gender imbalance in the Middle East. But Islam's views of women are more complicated than that. In fact, Islam improved equality for women in some ways that it took years for Western countries to catch up with. For example, Islamic countries were the first to allow women to receive inheritance. 

Really, all three major monotheistic religions are products of their own times. The Bible, the Torah, and the Quran all say that women are unclean during menstruation. They reflect the unexamined patriarchy of their times, and have cemented it in ours. 

Ultimately, Alya argues, gender equality means women having control over their own lives. In the Middle East, it would also mean having social welfare structures in place to protect women who make their own choices.

### 9. Alya has learned that, as an immigrant, creating a home is all about perspective. 

Alya Mooro is both Arab and British. She is also neither. Worldwide, immigrants and their children, sometimes known as third culture kids, are an increasingly large community. In London, for example, 37 percent of residents were born outside the UK. 

Alya realizes that she and many of her friends react to the expectations put on them in their different milieux. In Cairo, she performs English-ness. But in London, she tries hard to act like she's not one of those Arabs who parks their Lamborghini on the sidewalk and buys out expensive department stores. 

Immigrants like her have a subconscious certainty that a condition for remaining in their new country is good behavior, regardless of their legal status. This crystallized for Alya when the UK revoked the citizenship of Shamima Begum, a teenager who traveled to Syria to join ISIS. The British Home Secretary said she could apply for citizenship in Bangladesh, where her parents had been born though she herself had never been. It was as if to say she was less British than other people. 

Instead of making these distinctions between people who are British-British and British-something else, governments should be fostering integration. Canada is a great example of how to build social cohesion, with generous family reunification policies and credits to encourage international students to make their lives there.

In the UK, by contrast, Brexit has made things more difficult for people of non-white backgrounds. Alya feels it legitimized racism in the UK, where hateful speech against non-white people is often delivered alongside pro-Brexit sentiments. One Sikh doctor, for instance, was asked by a patient "Shouldn't you be on a plane back to Pakistan? We voted you out."

In spite of this, most people still agree that immigration has enriched modern Britain: only 23 percent of respondents to a 2018 survey believe that immigration has undermined British cultural life. 

For Alya, she has realized that it's the people around her that make her feel at home, as well as the familiarity of finding a routine. It's little things, like knowing which underground car to get on so she's closest to the exit at her home station that make her feel most at home. 

Above all, she's gained the confidence that she can make a home for herself anywhere she chooses. That choice, for her, is the greatest freedom.

### 10. Final summary 

The key message in these blinks:

**Creating a home for yourself isn't easy for anyone, but it's especially difficult for Arab girls growing up in the West. Caught between two very different cultures and sets of expectations, Alya Mooro ultimately forged her identity through introspection, talking to other women of the Arab diaspora, and finding strength in her uniquely blended background.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Headscarves and Hymens_** **, by Mona Eltahawy**

You just snuck a peek into the life of a Middle Eastern woman living outside the stereotypes. As you just saw, in many ways author Alya Mooro's life has been shaped by external opinions of her as a woman and an Arab living in Britain. 

In _Headscarves and Hymens_ (2015), Mona Eltahawy engages with the stereotypes that many people have about women's lives in the Middle East. She breaks them down, investigates the extent to which they are true, and examines how the Middle East should change to be more equitable to women — if at all.
---

### Alya Mooro

Alya Mooro is a British-Egyptian author and journalist who has contributed on topics of culture, beauty, and fashion to publications including _Grazia_, _Refinery29_, and _The Telegraph_. Using social media and her blog as platforms, she has come to embody the millennial third culture kid: sophisticated, introspective, and opinionated.

