---
id: 54abf58d38386300090e0000
slug: the-greatest-salesman-in-the-world-en
published_date: 2015-01-07T00:00:00.000+00:00
author: Og Mandino
title: The Greatest Salesman in the World
subtitle: None
main_color: CF5960
text_color: 80373B
---

# The Greatest Salesman in the World

_None_

**Og Mandino**

In _The Greatest Salesman in the World,_ you'll learn the unconventional secrets to becoming the best salesperson you can be. It turns out that the path to reaching sales targets doesn't actually involve studying salesbooks or copying the methods of others. Rather, it's about developing healthy habits and becoming a better person overall.

---
### 1. What’s in it for me? Learn how to become a better salesperson by becoming a better person. 

Regardless of profession, we all have to be salespeople to some extent in life.

For those in marketing or sales, of course, learning how to sell is an important part of the job. But for the rest of us, a surprising amount of our time is as well spent trying to sell: whether our talents as part of a job interview or our opinions on social media.

So how can you learn how to sell better? You could absorb the latest 10-Ways-To-Success book, or you could take another, deeper and holistic approach.

In these blinks you'll learn that to become a better salesperson, you need to change your life, approaching sales in a more mindful, philosophical manner. Only by developing better life habits can you hope to become the best salesperson in the world!

In the following blinks, you'll discover

  * why the power of love will help you become a better salesperson;

  * why you should always laugh in the face of difficulty; and

  * how to pick yourself up when you're feeling blue.

### 2. To learn and thus succeed, you have to form good habits that will become your positive foundation. 

How can you make sure that everything you do is the _best_ you can do?

Whether being in the best physical shape, being the most efficient worker or selling the most products, the secret to doing things effectively is in fostering excellent habits.

The majority of our daily lives are packed with bad habits, things we do almost without thinking yet that keep us unproductive and essentially unhappy.

For example, you may lie in bed for too long in the mornings. This might make you late, as you hit the alarm clock for the fourth time and then, end up racing to get to work on time. Yet even though you know it makes you stressed and unhappy, you laze around every morning!

To stamp out bad habits, you need to replace them with good ones.

Good habits are created by repeating them daily, until they become part of your life.

For example, if you want to get out of bed earlier in the morning, you should set an alarm and get up straight away, eat a healthy breakfast and maybe do a little exercise.

Although it might be tough at first, eventually you'll automatically start your day in this healthier way. What's more, not only will you have formed a healthy habit, but also you'll have beaten that bad habit forever.

This also sets the foundation for being an effective salesperson. In the next blinks, we'll look more closely at changes you can make to improve your life and in turn, your sales abilities.

To get the most out of this program, turn the ideas in these blinks into good habits by reading them every day. You'll find yourself automatically thinking about them, and adopting them into your life.

> _"My actions are ruled by appetite, passion, prejudice, greed, love, fear, environment and habit; and the worst of these tyrants is habit."_

### 3. A successful salesperson communicates love every day; with love, comes trust. 

Selling is all about being a people person. Before people decide to buy from you, they need to be able to trust you.

But how do you gain trust? By demonstrating _love_ to people every chance you can.

Start by telling yourself, "I will love everything and everyone" each morning when you get up. When you talk to another person, ensure that the words you use are loving, because words infused with love have great power.

For example, lovingly speaking to those you might not actually like can help turn them from adversaries into friends. Furthermore, if you consistently speak with love to your friends, they may become even closer friends, almost like family.

Acting from a place of love is vitally important. One way to do this is the next time you meet someone, recite in your mind the words, "I love you."

Silently thinking these words will actually affect your body language and facial expression. You will become more open; you will smile; your eyes will light up and your tone of voice will become friendlier. This way of interacting with someone too will open their heart to you.

But how does such thinking apply to the marketplace?

Not only will you stand a better chance of gaining customers when you interact with loving thoughts and behavior, but also you'll be able to effortlessly weaken your competition.

Let's say you work alongside a rival salesperson who behaves poorly toward you, steals your stock or spreads lies about you. You have a choice: you could seek revenge or you could treat them with love.

You'll find that if you continuously treat a person with love, for instance by commending them on their good work and perhaps helping them in some fashion, eventually, and even if they don't reciprocate, they'll at least stop behaving negatively toward you.

### 4. To be a good salesperson means to be unique and never give up, even in the face of failure. 

It can be so easy to give up after you've experienced a misfortune, but luckily there are some things you can do to help you get back on track and succeed.

The first step is to each day tell yourself, "I will persist until I succeed."

When you come face-to-face with an obstacle, resist the urge to run away from it. See it as a challenge and push harder to overcome it.

Imagine you're a pharmaceutical sales representative, and all day you have been walking from one clinic to the next, with no sales to show for your sore feet.

Sure, it would be easy to give up. But don't! Instead, take a break and focus on how you can approach your next potential customer, then carry on with a renewed, positive spirit.

Remember this: you never know if your next sale could happen at the next visit!

Whatever you do, don't finish the day with a failure, but continue until you have achieved some kind of success, even if small.

Perseverance is key, but of course, it's not enough. As a salesperson, you also need to stand out from the competition.

To do so, don't imitate others, no matter how successful they are. Find a way to flourish and prosper in your own, unique way.

Doctors are constantly hounded by sales reps as they juggle an endless stream of patients. As a pharma rep, what could you do to stand out from the crowd? If you want to make a sale, you need to be different from all those other salespeople. So develop a pitch that is unique to you and you'll be far more likely to catch a busy doctor's attention!

And finally, make sure you begin each day fresh, with an attitude toward doing your best. Even if yesterday felt like a slowly sinking ship, put it behind you. Go to bed and wake up fresh and ready to face the new day!

### 5. If you want to perform at your best, you need to control your emotions and laugh whenever you can. 

Have you ever been a slave to your emotions? It happens to all of us. But if you want to be a top salesperson, you cannot let your emotions overpower you.

If you're feeling angry and you're having trouble containing it, it's likely that you'll behave in a negative, pessimistic way toward your customers; not exactly the attitude to make a sale!

Bear in mind that life has peaks and valleys, and work to keep your emotions in check accordingly. If you feel down or depressed, lift your mood a little by singing. If you're feeling inferior, raise your voice to help give you more confidence.

This also works the other way around. When you think you're being too arrogant, think of a time when you failed to keep your feet on the ground.

When you're aware of how life impacts your emotional state, you can also start to understand and accept other people's actions.

For example, when a client is in a bad mood, consider that they may be going through a rough patch in their life. Instead of getting angry, give them some space and try talking to them the next day when their mood may have improved.

Finally, the most effective way to get into an ideal emotional state is to laugh at yourself and the world. When we stop taking things so seriously and see the funny side to life, we relax and our concerns diminish.

Say you have an important sales pitch, but the manufacturer is late in delivering the product to you. What will happen if you let this stress you out? You will worry and probably won't be able to think about how to effectively solve the problem.

But consider this: if you're able to laugh at the situation, you'll see that it's not the worst thing in the world. Perhaps you don't even need the product with you to pitch it effectively!

### 6. To get things done, set up a series of actions so you work your way to your ultimate goal. 

You are coming closer to becoming the greatest salesperson in the world, but you're not quite there yet. To really get momentum going, here are a few more good habits to learn.

First, you should know that whatever you dream cannot happen if you don't take action.

Turning a dream into a reality is not a simple path; along the way, it's easy to procrastinate.

So many of us have resolved to get fit, yet when it comes to actually changing our behavior and going to the gym, we almost immediately give in and say we'll go tomorrow. In the end, we never do.

Resist taking the easy way out! Whenever you make a plan or have an idea, set up a series of actions that will help you achieve it straight away. If your idea seems intimidating initially, remember, even if you fail on the first attempt, it's far better to have tried, than forever wonder "what if...?"

The next habit is to set yourself goals that you will meet.

Everyday, every week, every month and every year, figure out how you can surpass what you've already achieved. Do better today than yesterday, and aim to consistently sell more, making sure that this year is even more productive than the last.

It's also a good idea to announce your goals and objectives to others. The looming threat of having to make an excuse to someone about why you didn't achieve your goal is a great motivator not to fail!

Lastly, have faith in God and call for help when you need it.

Whenever you feel you need some extra strength, you can always request assistance from above. For example, if you're nervous about an upcoming sales call, ask for divine guidance on how to act and talk, for it to be successful.

### 7. Final summary 

The key message in this book:

**To be a successful salesperson, you need to enforce good habits: be optimistic, persistent, committed to self-improvement and learn to manage your emotions. Doing these things will not only help you reach beyond your sales targets but also help you to be a better person.**

**Suggested further reading:** ** _The Richest Man In Babylon_** **by George S. Clason**

_The Richest Man In Babylon_ is a series of parables set in ancient Babylon concerning financial wisdom. In these blinks you'll find these parables distilled into modern day advice that can help you accumulate wealth.
---

### Og Mandino

Augustine "Og" Mandino II was a bestselling American author and the president of _Success Unlimited_ magazine who passed away in 1996. He is an inductee of the National Speakers Association's Hall of Fame. This book, _The Greatest Salesman in the World,_ has sold over 50 million copies worldwide.

