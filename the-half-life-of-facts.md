---
id: 538f46096435340007260100
slug: the-half-life-of-facts-en
published_date: 2014-06-03T16:29:01.000+00:00
author: Samuel Arbesman
title: The Half-Life of Facts
subtitle: Why Everything We Know Has an Expiration Date
main_color: F35131
text_color: B23C24
---

# The Half-Life of Facts

_Why Everything We Know Has an Expiration Date_

**Samuel Arbesman**

As we continue to accumulate knowledge, we begin to realize how often old ideas are overturned due to new facts that contradict them. _The_ _Half-Life_ _of_ _Facts_ explores patterns in the way knowledge is created, and permeates our world and our personal lives. The book also gives some helpful on how to deal with our ever-changing world and regain a sense of control.

---
### 1. What’s in it for me? Learn how our cumulative knowledge grows and evolves over time. 

It's hard to believe that smoking was once _recommended_ by doctors: today we're constantly reminded of just how deadly the habit can be. What about red wine? Do doctors still recommend a glass a day or has the consensus suddenly changed?

And what about other beliefs that influence our habits or even the way we see the world? After all, the Earth was once believed to be flat and located at the center of the universe. Of course we know better today, but that's not the only fact that's ever needed to be corrected. What _else_ have we missed?

Using mathematical and scientific techniques, _The_ _Half-Life_ _of_ _Facts_ takes a closer look at how knowledge changes over time, revealing the fleeting nature of facts as our knowledge continues to evolve.

In these blinks, you'll learn

  * why your brand-new computer seems like a tool from the Stone Age after a year,

  * why we have to constantly measure Mount Everest,

  * how Sherlock Holmes' mortal enemy ended up in scientific papers and

  * how to cope with the inevitable changes in our modern world.

### 2. We can use mathematical models to track and measure how knowledge grows and develops. 

What do you have to do to become a doctor? One major prerequisite is to spend many years in medical school gathering all the vital knowledge you need to diagnose and treat patients.

Yet even after all that training, there's still be more to learn because the field of medical science is continuously evolving; as new discoveries are made, old ideas fall out of use. Thus, doctors _must_ continue learning all the time if they're to keep up.

Indeed, humanity continually advances its knowledge in many other fields as well. In order to track these developments, we use something called _scientometrics,_ i.e., the "science of science." Using the power of modern computers, scientometrics creates complex mathematical models to scour for patterns in the accumulation and evolution of knowledge.

As we'll see, it's found some interest things...

Much like the half-life of radioactive materials that describes the time it takes for half the material to "die," knowledge, too, has a half-life. In every academic field, you can measure the time it will take for half of the accepted facts to be overturned by new knowledge. In other words, it turns out you can measure the _half-life_ _of_ _facts_.

For example, in physics, half of today's new academic books will have to be rewritten in 13 years; in history, within a mere seven years!

In addition, scientometrics helps us measure how quickly knowledge grows. For instance, after tracking the changes in medical science over time, we can project that the amount of major contributions, such as new findings, methods and ideas, will double every 87 years. In the field of chemistry, this takes only 35 years.

We can learn quite a bit by using scientometrics to look for patterns in the development of knowledge. In fact, the simple awareness that knowledge will change over time can encourage us to stay open to revisions to our worldview.

> **Keep** **learning.**  

Learning is a lifelong task: knowledge is constantly changing, so you need to continue learning to keep up with the times.

### 3. Knowledge built up over time allows us to make discoveries quicker than ever before. 

Over the course of human history, we've gone from _worshipping_ nature to taking steps to truly _understanding_ it. What was it that helped us get where we are today?

For one thing, although our knowledge certainly isn't perfect, it does inch closer to the truth with every new development. For example, the once accepted notion that the world is a sphere isn't quite true — it's actually an ellipsoid. Still, this false belief is a far better approximation than the previously held belief that the Earth was _flat_.

In our modern world, this accumulation of knowledge means that each new scientific advancement has the power to spark massive, groundbreaking developments. Consider that across the world there are now thousands of specialists working in their fields of expertise. But because many of these fields are interconnected, each new discovery made by specialists in one field could help solve a more complicated problem in another.

To conceptualize this, imagine you're making a pile of sand one grain at a time. Your pile will grow higher and higher until suddenly one tiny grain of sand will cause the whole thing to collapse.

The development of knowledge functions much in the same way: little advances from different areas eventually add up to create an avalanche — a huge breakthrough that transforms our way of thinking.

In fact, we might even witness multiple large-scale changes in thought within a single human life. Being aware of this trend and consciously keeping current with new developments in knowledge helps us take a few steps closer to managing the rapidly changing world around us.

Now that we've gained some insight into the nature of our knowledge, these next few blinks will explore some of the major influences in how knowledge changes.

### 4. Technological growth makes up a great deal of the nature of knowledge. 

Have you ever bought a new computer or phone only to discover that, within just a few short months, a newer, better one has come to market? The speed of technological progress is much faster nowadays than ever before. But why?

Because technological development follows the same pattern as the scientific knowledge we learned about in the previous blink: each new innovation sparks more and more breakthroughs.

Like other areas of knowledge, we can track this development as well.

The most famous formula for tracking technological development is _Moore's_ _Law_, which states that the computing power of a single computer chip will double every year. There is even a similar law regarding the power of robots. In fact, one study has shown that a robot's ability to move with regard to the duration and speed of movement has roughly doubled every two years.

This doubling upon doubling means that technology actually develops _faster_ as it continues to evolve!

But it's not only scientific discoveries that cause technological advancements — it also works the other way around. The invention of the particle accelerator, for example, has led directly to the discovery of new atomic elements, especially larger, heavier ones.

Moreover, digitizing information and using computer models has also made it far easier to connect and combine information from differing sources, thus enabling (once) _hidden_ _knowledge_ to emerge.

The computer program Co-Pub Discovery, for example, found new associations between genes and diseases by scouring a large database of studies, and aggregating and synthesizing the findings.

Ultimately, the program was able to identify some important genes as being associated with Graves, an autoimmune disease of the thyroid gland, thus uncovering a hidden connection that would have otherwise gone unnoticed.

### 5. The evolution of measurement is closely intertwined with the nature of knowledge. 

How do we know that Mount Everest is the tallest mountain in the world? The obvious answer is that we measured its height and then compared the result with those of other mountains.

Indeed, by measuring something's properties, we are able to learn more about the world, and this measuring is a continuous process. After measuring Mount Everest a few more times, scientists even found out that its height is not static — it changes all the time due to erosion and the collision of the continental plates.

In fact, we can measure just about anything: mountaintops, the speed of individual particles, the strength of materials or even our own methods of generating facts.

Knowing the breadth and importance of measurements, it's no surprise that we want them to be as _accurate_ as possible when we conduct scientific research. However, errors can even creep into good science, especially during the measuring process, and this can lead to false ideas.

That's why it's become the scientific standard to repeat experiments many times in order to reproduce the results rather than relying on the results of a single trial.

Interestingly, the relationship between measurements and science is reciprocal: our ability to measure influences science, and scientific and technological developments also foster advances in our ability to measure.

For instance, the official length of a _meter_ used to be measured against a metal, but since metal corrodes and expands with heat, it's hardly an exact definition. However, using breakthroughs in theories on light waves, scientists have now defined the meter in much more specific terms, i.e., the specific distance light travels in a specific timeframe.

In addition, advances in computer power are intertwined with our means in measuring it. For example, terms like "megabyte" or "gigabyte" are novel expressions of measurements which coincided with novel advances in computer storage.

### 6. Social connections between people can help the spread of knowledge. 

Have you ever wondered why so many universities and research laboratories are located in big cities?

Because people think that the larger the population, the more people there'll be with specialist knowledge who can work together to develop new ideas.

However, that's not necessarily true.

In fact, it's not the density of a population that fosters the development of ideas but the social connections between people — regardless of their proximity.

Think about it: How many of your Facebook friends live near you? Some of them are on the opposite side of the globe, yet you still exchange information with them.

And this isn't just a modern trend. In the fifteenth century, for example, the printing press spread from Germany to northern Italy before reaching other cities in Germany. Why?

Rather than being showcased to the nearest people, this new technology followed the social connections between "thinkers" living in certain German and Italian cities.

Conversely, history offers stark examples of the consequences being cut off from social connections.

For example, the population of the island of Tasmania was once one of the least technologically advanced societies in the world because it was cut off from Australia by the sea, completely isolated from the rest of the world.

Consequently, Tasmania missed out on the development of many technologies, ranging from special fishing nets to boats to better clothing.

However, just as society influences the availability of technology, technology can also influence the development of society, as is most easily seen in the development of cities.

Modern cities are bigger than ever before precisely because of improvements in our knowledge and technology. Medical knowledge has dramatically increased the modern population; modern construction techniques mean we live in larger, safer buildings; and the sewage system allows for a larger population without sacrificing much cleanliness.

### 7. The creation and spread of errors in our knowledge are closely tied to the human condition. 

If the accumulation of knowledge might have seemed like a smooth process so far, that's only because we've neglected one crucial element: human beings. We humans, with all our faults, play an important role in these developments.

The Latin proverb _errare_ _humanum_ _est_, "to err is human," shows just how long we've been grappling with our innate faults. In fact, history is full of accounts of basic human errors affecting our knowledge.

For example, when a chemist misplaced a decimal point as he wrote down the amount of iron in spinach, it made the vegetable seem a lot more nutritious than it actually was. This small error had larger consequences: iconic cartoon character Popeye the Sailor Man couldn't have used spinach as his secret weapon without it.

Moreover, once errors occur, they can spread like wildfire. For example, one serious scientific dictionary referenced a scientific theory about asteroids authored by James Moriarty, whose theory was subsequently referenced by other scientists in their own works.

However, it's highly improbable that Dr. Moriarty wrote _any_ scientific theories whatsoever given that he never existed! He was, in fact, the mortal enemy of fictional detective Sherlock Holmes. Somehow someone had mistakenly added his name to the dictionary, others used it, and the "reference" spread in spite of its inaccuracy.

So how do we stop the spread of errors? The _only_ way is to go through the laborious task of making corrections. And yet, only 20 percent of scientists actually read the articles they cite, meaning that errors can spread very easily.

The obvious solution would be for scientists to seek out the literature and examine the line from the work they're citing. If they spot errors, they should make note of them and ensure that they're corrected.

### 8. Several obstacles stand in the way of our willingness to adopt new or changed facts. 

So far, we've only explored collective human knowledge. Now let's turn our attention to the way we, as individuals, handle new information, and the problems our approaches cause.

First, individuals hardly notice changes that happen slowly over a long period of time. For example, 200 years ago there was an abundance of fish on the Newfoundland coast. Yet now, due mostly to fishing, there are hardly any left. Why on Earth was this allowed to happen?

When scientists started this research, they measured the amount of fish in the water and tracked future changes against this initial figure. By the time they moved on to another job, they noted relatively little change in the fish population. And the next scientists who came along to study fish _also_ started by counting the amount of fish at the beginning of the study.

In other words, because each new generation has its own _baseline_ to measure fish stocks and the stocks themselves don't change that much over an individual's career, the wider, long-term changes often go unnoticed.

And this same principle can be applied to _any_ field of study where changes are slow.

Furthermore, we tend to ignore information that doesn't fit our preconceived worldview, and instead look for information that does. Nothing is more dangerous to an idea than a fact that contradicts it, and those who are very attached to their worldviews would rather suppress or ignore such facts than face any changes.

For example, when Galileo Galilei asserted that the Earth was not the center of the universe, he contradicted the Catholic Church's assertion that God had centered everything around humanity. The Church responded by viciously attacking him and his ideas, doing everything they could to suppress them.

The defense of established ideas is one of the reasons why each generation seems more iconoclastic than the last; unlike their parents, they have not yet grown attached to old ideas, making them more accepting of change.

### 9. A few simple lifestyle changes can help us cope with the world’s rapidly changing knowledge. 

Unfortunately, we don't cope very well with changes in knowledge. Luckily, we can take steps to ensure that we keep up to date in this ever-changing world.

For starters, we can structure our routine so we systematically and actively continue learning throughout our lives.

One way to do this is by reading continuously. For example, you can look at media diets — i.e., influential thinkers' summaries of what and how they're reading — to guide you towards pioneering knowledge so that you don't get lost in the times.

Since knowledge is always growing, it's incredibly difficult to keep up. We should use the technology around us, especially online tools, to keep ourselves informed of the latest. For instance, it's worthwhile to subscribe to blogs or other media channels that provide summarized knowledge of a particular field.

Second, abandon memorization. There's simply too much information out there to cram into your brain. And if you try to memorize everything, your brain will become overcrowded with a jumble of information. A brain too full of facts and ideas isn't very efficient.

Instead, we should try to _free_ our minds from acting like warehouses of knowledge by using the tools at our disposal, such as the internet.

The internet, with its search engines and permanent storage of knowledge, means that you don't _have_ to remember information in order to use it. In fact, it's better to just learn how to research and access the knowledge on the internet to the best of your ability rather than trying to accumulate the knowledge yourself.

If you follow this advice, you'll have a better shot at overcoming your natural aversion to change. The more aware you are of the inevitable changes in thought, the less likely you are to be tethered to old ideas.

### 10. Final Summary 

The key message in this book:

**While** **our** **understanding** **of** **the** **world** **is** **constantly** **changing,** **there** **are** **also** **regularities** **in** **the** **way** **that** **facts** **change.** **Being** **aware** **of** **these** **patterns** **and** **actively** **fighting** **the** **human** **tendency** **to** **fear** **change** **helps** **us** **to** **understand** **the** **world** **and** **cope** **with** **confusing** **uncertainties.**
---

### Samuel Arbesman

Samuel Arbesman is a scientist in the field of applied mathematics and a fellow at the Institute of Social Sciences at Harvard University. He also has a blog on _Wired_, and his essays have been printed in popular newspapers such as _The_ _New_ _York_ _Times_ or _The_ _Wall_ _Street_ _Journal_.

