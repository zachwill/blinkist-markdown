---
id: 58ce6f4a1714b70004eb6aa7
slug: the-happiness-equation-en
published_date: 2017-03-22T00:00:00.000+00:00
author: Neil Pasricha
title: The Happiness Equation
subtitle: Want Nothing + Do Anything = Have Everything
main_color: 2FB5E9
text_color: 1F7799
---

# The Happiness Equation

_Want Nothing + Do Anything = Have Everything_

**Neil Pasricha**

_The Happiness Equation_ (2016) unlocks nine essential secrets to living a happy life. These are guidelines that can help anyone reap the rewards that come with having a positive outlook on life. Happiness isn't just about reducing stress and anxiety; it's about paving the way to great work and success.

---
### 1. What’s in it for me? Become a happier person. 

How happy would you say you are with your life, on a scale from one ("not at all") to ten ("my life is perfect bliss")? If you answered anything below seven or eight, these blinks are for you.

Happiness isn't a given; it's something you can achieve. And it's not as difficult as you might think. You don't need to go to expensive yoga retreats, visit all the wonders in the world or be swimming in money to feel great about your life. All you need are the nine secrets outlined in these blinks. Let's get started.

Along the way, you'll learn

  * that you're already a lottery winner;

  * about a part of the human brain that actually _looks for_ problems; and

  * why retirement isn't what it's cracked up to be.

### 2. You need to start from a place of happiness and do things because you enjoy doing them. 

Early on in life, you may have been taught that happiness comes only after you've done a considerable amount of great work and become a huge success. But it's this exact lesson that's responsible for many people's unhappiness, because the equation actually works the other way around.

Which leads us to the first secret of happiness: success and great work come from people who are _already_ happy.

Happiness shouldn't be something you chase. External circumstances, such as getting a raise at work, account for only 10 percent of personal happiness. The rest comes from within and is based on seeing the world in a positive light.

To help you recognize everyday happiness, use the _20 minute replay_ before going to bed. This simple practice involves keeping a daily journal to write down at least one thing that made you happy during the day. Not only will this help you recognize future moments of happiness; it will allow you to relive that happiness at the end of the day.

Another important contributor to happiness is motivation, which brings us to the second secret of happiness: do things because you love doing them, not because of external goals and rewards, or what other people think.

When your motivation is money or admiration, you're entering a never-ending cycle of unhappiness. Even if you reach your goal, it will only result in a fleeting moment of happiness before you feel the compulsion to set another goal.

The author started his blog, _1000 Awesome Things_, because writing brought him joy and he liked the idea of posting one happy detail for a thousand workdays.

However, when the site became popular, he began setting goals. At first he wanted to reach one million visitors. Once he'd done that, he wanted to reach ten million. Then he set his sights on publishing a book. And then the book had to become a bestseller. There was simply no end.

He was chasing a happiness that didn't — and couldn't — last. So he resolved to only do things for the sheer joy of doing them.

### 3. Two conflicts interfere with feeling happy, but by simply being alive, you already have the ticket to happiness. 

There's no escaping negative thoughts; they'll turn up no matter how positive your outlook on life is.

When trying to fight these thoughts, you may have felt like a battle is raging in your head. Well, that's because a battle _is_ raging, one between two parts of your brain: the amygdala and the prefrontal cortex.

The amygdala is very old, and its job is always to be on high alert, scanning every situation for potential problems and threats. Way back when, our ancestors were constantly on the lookout for saber-toothed tigers and other predators, and the amygdala is basically what kept them alive. Obviously, things have changed, but the amygdala still has to do its job, so it sets off alarm bells even when the threat — that upcoming presentation, for instance — is relatively benign.

The prefrontal cortex, which developed more recently, tries to counter these worries with rational thought and comforting logic. However, the amygdala often wins these battles, keeping us anxious and unhappy.

As if that weren't enough, there's another battle that we're constantly having with ourselves, a conflict between feelings of self-worth and feelings of doubt about how well we measure up to others.

This might happen when you see your neighbor's new car. Though you were fine a minute ago, suddenly you start feeling miserable about your life and your inability to afford your own new car.

While it's not always possible to calm your amygdala, especially if you're about to make your public-speaking debut, there are reliable ways to deal with your envy and discontent.

This brings us to the third secret, which is all about putting things in the right perspective: don't forget how lucky you are to be alive in the first place.

Remember how astounding it is that all the conditions necessary for human life to develop on the earth came together — how remarkable it is that you're even capable of engaging with these blinks!

Keep in mind that while there are 7 billion people alive today, there are 108 billion people who have died, which means that 14 out of every 15 people who have ever been born are already deceased. Being alive is a bit like winning some wild cosmic lottery.

### 4. Retirement won't make you happy; you need to stay purposefully active. 

Are you looking forward to hitting retirement age, sitting back and enjoying the fruits of your labor? If you want to stay happy and healthy, the answer to that question should be, "No!"

Retirement is a concept the Germans invented in 1889. The idea was to open up the job market to younger people and let people over the age of 65 enjoy their final years.

Sure, it sounds like a noble idea — but it's no longer appropriate. In 1889, the average lifespan was 67 years. Today, someone at that age might still have decades of healthy, active living ahead of them.

Studies show that healthy people have a desire to stay productive. A survey from the mid-twentieth century shows that 50 to 60 percent of people over the age of 65 would rather postpone retirement and continue working.

And this is a good thing, since being productive gives us purpose and a reason to get out of bed in the morning. Purpose is a crucial factor for happiness and it's something people living on the island of Okinawa, in the East China Sea are very familiar with.

Okinawans have the highest life expectancy on Earth, and have _no_ concept or word for retirement. What they do have is _ikigai_, which roughly translates to "the reason you wake up in the morning," and they recognize this as being vital to longevity and quality of life.

Researchers at Tohoku University spent seven years studying the impact of ikigai on 43,000 different people. Their results showed that people who felt healthier and less stressed all reported having a reason to get out of bed. And by the end of the study, 95 percent of those with an ikigai were still alive, compared to 83 percent of those without one.

So don't let retirement strip you of your purpose in life and remember the fourth secret to happiness: stay productive and keep your ikigai alive!

### 5. How you spend your time is more important than how much you’re being paid. 

It's a good feeling to be paid well for the work you do, but the dollar amount on your paycheck doesn't tell the whole story.

For instance, a Harvard graduate makes an average of $120,000 per year, but they also receive a maximum two weeks of vacation while working 85 hours per week during those remaining 50 weeks. That adds up to 4,250 hours per year and only $28 per hour.

Now let's look at an assistant manager of a retail store, who earns an average of $70,000 per year. She also gets two weeks of vacation but her average workweek is 50 hours, which makes it 2,500 hours per year. And lo and behold, the assistant manager also earns $28 per hour.

So what do you value more — the number after the dollar sign on your paycheck or the time you lose by working rather than doing the things that bring you joy?

Time should be given a higher value in your life, since we have precious little of it. In fact, you should adhere to the fifth secret of happiness and _overvalue your time._

This means being aware of how you are spending your time and reconsidering how much your time is worth when it's not being spent doing things that give you joy, meaning and purpose.

If you have trouble managing your time, try setting up a structure. Divide your week into three separate categories — "Sleeping," "Work" and "Things I Love Doing."

When you evenly divide the 168 hours of a week into these categories, you'll get 56 hours for all three, which should leave you well-rested and feeling the benefits of a healthy work-life balance. 

This is not what those Harvard graduates are feeling. With their 85-hour workweeks, it's a struggle to find the time to sleep and do anything other than work.

In the next blink, we'll explore how to make room in the day for the important things.

### 6. To feel less stressed and find more time for what’s important, reduce your choices and create space. 

Making decisions can be stressful. And it doesn't help that most people's average day is chock-full of them. When the author counted all of his daily decisions, he found 285 of them. And most of these weren't even very important — things such as deciding when to check his mailbox.

Yet every decision requires mental energy, which means it causes some level of stress. You can reduce this stress, however, as well as save time and increase productivity, by eliminating your choices.

Benjamin Lee, the author's former colleague, had this realization and acted on it. He only goes shopping for clothes once a year, at which time he buys 30 boxer shorts, 30 identical pairs of socks, 15 shirts and five pairs of black pants.

With this selection, he only needs to do laundry once a month and he never has to think about what to wear. According to his calculations, by removing these decisions, he saves 15 minutes every day, which adds up to an entire week every year.

Lee has created space to help him focus on the things he really wants to do and which make him happy — and that's what the sixth secret to happiness is all about.

Another way to give yourself more time is to take that time away from competing tasks by being creative with your deadlines.

In 1955, the scholar C. Northcote Parkinson developed _Parkinson's Law_, wherein he suggests that work will always take as long as the deadline allows.

So if you have a writing assignment that is due in three weeks, you will likely spend that entire amount of time researching and writing as well as procrastinating and worrying. But what if you made your own deadline of one week? With no time for procrastination, you'd likely get to work immediately, and chances are the quality of your work wouldn't suffer.

Setting fake deadlines like this is just one way you can make more time when it appears to be in short supply.

### 7. Break through barriers and do the things you love, even if you’re afraid. 

When's the last time you ticked something off your bucket list? If it's been a while, what's stopping you from doing so? It's usually not money or resources that prevent us from doing something; we actually set up our own barriers and fool ourselves into staying away from things that would make us happy.

Typically, the two barriers that keep us from happiness are the _Can't Do_ and the _Don't Want To_ barriers.

Thinking that you can't do something is an effective way to keep yourself from ever trying. The author believed he couldn't swim, but this belief stemmed from a bad experience he'd had in the water as a child, which led to his creating the false belief that he couldn't do it.

Another trick of the mind is to convince yourself that you actually don't want to do something, which creates a second barrier that often follows the first.

The author came up with plenty of reasons to avoid swimming, telling himself that it's a waste of time, or that there are many other ways to get exercise.

These thoughts simply prevent you from experiencing the many joys life has to offer. Fortunately, you have the power to break through these barriers, which is the seventh secret of happiness.

Even though it can be difficult to imagine yourself doing something that's unfamiliar and scary, imagining it is an important part of overcoming your fear. By using your imagination and picturing yourself doing the frightening thing, your brain will start getting used to the idea.

The next step is to just dive in and do it. When the author met his wife, a woman who loved swimming, he decided that he had to do it and overcome the barrier. He stopped thinking up excuses and signed up for swimming lessons, and he never looked back.

Remarkably, once he got in the water, his thinking changed and a new belief took shape: he _could_ swim. Soon thereafter, he realized that he actually _wanted_ to swim.

### 8. Be yourself and don’t rely on the advice of others. 

If you've ever tried to be friendly with someone who is being rude and hurtful, then you're probably familiar with the pain and discomfort that comes with not honoring your true emotions.

Which brings us to the eighth secret of happiness: be yourself!

The most important relationship you have in life isn't with anyone else — it's with yourself.

Yet many people neglect this relationship and fail to live according to their true self. They put on an act and do things they wouldn't do otherwise, to either earn the respect of someone else or to further their career.

To help improve this relationship and be your authentic self, take the _Saturday morning test_.

Ask yourself, "What would you like to do on a Saturday morning, when there are no other obligations?"

Do you love going to the gym? Then maybe you could become a personal trainer. Do you love writing? Do what the author did and start a blog or submit articles to a magazine.

Whatever your answer is, think it over and put yourself on the path to your true self. You'll find that the more Saturday-morning activities you do, the happier you'll become.

Finally, there's the ninth secret to happiness: trust your own thoughts and feelings and don't live according to the advice of others.

Every day there are ads, articles, experts and friends trying to give advice on how to live and be happy, and it's not uncommon to run into contradictions.

If your husband or wife cheats on you, one friend might tell you to immediately file for divorce, while your mom might tell you to try couples therapy and work things out. Even clichés like "the early bird catches the worm" and "good things come to those who wait" are contradictory!

Ultimately, you're the only one who really knows your hopes and desires, so you're the only one with the right answers for your situation. To be happy, you can't rely on the advice of others — you have to ask yourself what you really want and trust your feelings.

### 9. Final summary 

The key message in this book:

**To be happy, you need to stop chasing after external goals, like that new car or promotion, and adopt the right mindset and healthy habits. Happiness lies in realizing that you already have everything you need. Once you start doing the things you love and being yourself, you can start enjoying your life and living it to the fullest.**

Actionable advice:

**Try a seven-day challenge.**

If one of the practices you read about resonates with you, challenge yourself to keep at it for seven days and see how it affects you. Keep track of your progress in a calendar. By doing it for seven days, you'll also convince yourself that you can do it for another seven, and then another and so on.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How To Have A Good Day_** **by Caroline Webb**

_How to Have a Good Day_ (2016) explains how you can make the most of your working day, with advice based on recent findings in the fields of psychology, economics and neuroscience. These blinks will teach you how to navigate the challenges of the modern workplace like a pro and boost your energy level during nerve-racking or tiring days.
---

### Neil Pasricha

Neil Pasricha, a _New York Times_ best-selling author, is an entrepreneur, public speaker and advisor. He is also the creator of the popular blog _1,000 Awesome Things_ and its accompanying publication, _The Book of Awesome_.

