---
id: 5640802c3866330007780000
slug: the-happiness-industry-en
published_date: 2015-11-13T00:00:00.000+00:00
author: William Davies
title: The Happiness Industry
subtitle: How the Government and Big Business Sold Us Well-Being
main_color: FFCC33
text_color: 806619
---

# The Happiness Industry

_How the Government and Big Business Sold Us Well-Being_

**William Davies**

What makes you smile, laugh or feel like skipping down the street? Teams of professional psychologists, neuroscientists, marketers, economists and your boss all want to know. Why? So they can make sure you are a productive employee who buys lots of stuff! _The Happiness Industry_ (2015) takes an in-depth look at how our happiness is studied, measured, and profited from — often without our knowledge or consent.

---
### 1. What’s in it for me? Learn what your happiness means to others. 

Ever asked yourself the question, "Am I happy?" Ever realized that you're not the only one who wants to know the answer? Time to learn who — other than your mom — is so keen to find out. Hint, these aren't self-help blinks, but a journey into e-commerce, neuroscience and big data.

_The Happiness Industry_ is full of valuable insights into a world in which happiness has become a commodity that can be quantified in an MRI or an algorithm.

In these blinks, you'll discover

  * what Jeremy Bentham had to say about happiness;

  * how neuroscience, economics and happiness are interlinked; and

  * which (hidden) incentives your government might have to make you happy.

### 2. Utilitarian philosophers believe that happiness is real and objective. 

Are you happy right now? How can you tell? Perhaps you can't stop breaking into a grin. Or maybe it's just that feeling that everything will turn out alright. Though everyone might answer that question differently, it's clear that happiness is something that can be _measured_. 

Developments in neuroscience have allowed us to understand pleasure as a physiological event, comprised of observable chemical processes. Put simply, the human brain operates with a special "code" of emotion, including happiness. This code is computed in the orbitofrontal cortex. 

Neuroscience isn't the only field convinced that happiness is measurable. Some schools of philosophy work from that premise too. Jeremy Bentham (1748–1832), founder of modern utilitarianism, suggested that the intensity of happiness can be objectively observed using two metrics: _human pulse rate_ and _money_.

Findings suggest that people's pulse rates increase during moments of happiness — a straightforward way to measure the intensity of their feelings. Money can also help us gage positive emotions. If we count happiness as utility or usefulness, then when you buy a product the amount of money you spent on it becomes a measure of your happiness with the product or its usefulness to you. 

Politicians also have a vested interest in happiness. One of their responsibilities is, after all, to maximize the happiness of society. Governments strive to achieve this goal by _punishing and rewarding_. 

Punishing refers to making an individual suffer for committing a crime or bad behavior. A criminal, for instance, must serve a certain sentence in prison. Conversely, in the free market people can be _rewarded_ with money in the form of income for being hardworking and talented. In this way, a government is theoretically able to direct human behavior toward maximizing happiness.

> _"Happiness has become a new religion."_

### 3. We’re constantly persuaded that money is the greatest indicator of happiness. 

From the early 1990s on, economists began to use (and abuse) research into the relationships between pleasure and money. Studies compared the happiness of people with different levels of income, with the controversial, but perhaps unsurprising, findings that the richer are indeed happier. 

Research into the topic continued, with neuroscientists taking the lead. Further studies revealed that a pleasurable activity corresponds with an increase in dopamine levels in the brain. 

Dopamine is a neurotransmitter that activates the brain's reward and pleasure centers. One of these centers is the nucleus accumbens, a region of the brain that also triggers decisions to purchase a product.

In light of this, some neuroscientists have proposed that every buying decision is actually the result of specific neural pathways in the brain. Perhaps, then, when we spend $10 on a pizza, or $500 on a new tech gadget, we receive an equivalent quantity of dopamine as a reward.

Of course, it's important to consider who is carrying out such research, and why. Economists, business leaders and marketers would all be absolutely thrilled if they could prove that money wields power over the chemical processes of happiness.

So long as capitalism exists, there will be people trying to link psychological happiness and money together! And happiness itself is becoming increasingly commodifed. Find out more in the following blinks.

### 4. Modern market research delves into behavioral analysis to make us spend more. 

How gullible are you? Most of us think we're pretty tough to fool, but just think about all the products that you own. There might be several that you're using right now. Why did you buy those _particular_ headphones, or socks, or snack bars? 

Because our brains are more easily swayed than we'd like to believe. Cunning advertising teams have made use of neuroscience to get inside your head and make you buy and then buy some more. 

Brian Knutson, a Stanford neuroscientist, found that humans experience a significant amount of pleasure when _anticipating_ receiving a product. In fact, thinking about having a product makes us happier than actually owning it. 

Current advertising trends reflect this finding in their fixation on the experience of using a product, rather than the characteristics or features of the physical product itself. 

For example, an ad for jogging shoes sells the experience of happy, carefree running, perhaps encapsulated in a smiling, toned female jogger running along a forest track. This approach is highly effective at increasing the anticipation of receiving the marketed product. 

Marketers also make clever use of research into behavioral analysis. Advertisers have long known that they aren't just selling a product, but seeking to generate a specific psychological response. Consumers' feelings can be manipulated, and powerfully too. 

John Watson (1878–1958), an American consumer psychologist, evoked feelings of purity and cleanliness in his ads for baby powder, making shrewd use of the profound emotions of young mothers. The ad was a success, but of course not all women were taken in by it.

That was one shortcoming of advertising in the past: even with focus groups and market research, it was tough to get a comprehensive picture of how your ads were received by the public. 

Today, things have changed. With online advertising and growing e-commerce, behavioral analysis is being used more slyly than ever. Online, it's incredibly easy to measure how consumers behave, and in turn, easier to get consumers to do what you want them to. 

But it's not just marketers that want to harness our happiness. Your employer does too.

### 5. Governments and businesses invest in health care to boost our productivity. 

Capitalist companies love making profits. But what happens when their employees don't love their jobs? Dwindling enthusiasm and productivity are some of the greatest threats to any aspiring firm. 

A study by Gallup revealed that as little as 13 percent of the global workforce are engaged in and enthusiastic about their jobs. A further study also found that a quarter of absenteeism is not the result of real sickness, but general burnout experienced by employees. 

A lack of engagement doesn't just mean suffering employees, but massive costs to businesses and governments too. Active disengagement, be it absenteeism, sickness or even just inertia when on the job, costs the US economy a whopping $550 billion a year.

Simply put, capitalism can't sustain itself unless businesses can rally commitment from their employees. Why? Because productivity goes hand-in-hand with health and happiness. 

Researchers estimate that companies can increase their output by 12 percent if employees feel happy. Findings also highlight the personal dangers of burnout for employees, leaving them at greater risk of heart attacks, strokes and nervous breakdowns, all of which are naturally disastrous for productivity. 

With this in mind, some businesses make serious attempts to promote their employees' happiness and health, hoping to improve their performance and reap the benefits. Executive wellness programs, for instance, are designed for the senior employees of companies in order to keep them fit enough to run the ship. 

An increasing number of companies even employ specialist "happiness consultants" who guide managers to creating a cheerier workplace. Google, for example, has an in-house "jolly good fellow" to spread mindfulness and empathy in order to make employees healthier and happier, and, of course, maintain profits for the business.

### 6. Marketers use social relationships to their advantage. 

Who are you more likely to believe: a pushy salesman, or one of your close friends? Word-of-mouth is one of the oldest forms of advertising. It's stood the test of time because it works. But these days, it's received a bit of a makeover. 

Marketers today have figured out just how much use they can make of social bonds when advertising products. There are two main strategies. First, they deepen the "social" relationship between company and customer. How?

One highly effective tactic is _corporate giving_. This is nothing more than giving products and services away for free. Companies do this not out of generosity, but to win a customer's favor and build up their own reputation in the market. 

Even simpler is the technique of _saying thank you_. For example, Tesco, a leading UK supermarket chain, released YouTube videos with men in Christmas jumpers singing "thank you" as part of the brand advertising campaign. This marketing tactic boosted revenue and allowed Tesco to recover after an unsuccessful sales performance in 2011. 

The second strategy that companies use to take advantage of social bonds is _friendvertising._ Friendvertising refers to the creation of images and videos with positive brand messages that appeal to social media users, who in turn _share_ the ad with their online friends. 

The most shared ad of 2013 was a three minute YouTube video called "Dove's Real Beauty Sketches." The beauty and skincare brand centered their ad on the positive message "you are more beautiful than you think," something highly relatable and highly shareable. Online sharing is rather like a faster, wider-reaching version of word-of-mouth advertising. 

This is something to remember next time you scroll through your feeds! You may have been subject to marketing campaigns without even realizing. Or your friends may have been turned into brand advocates, whether they were aware of it or not.

### 7. Our hyper-connected world allows businesses to surveil our happiness. 

If you take a moment to think about the vast amount of digital information you create everyday, from your Google searches to Facebook likes to your Amazon.com purchases, it's reasonable to say that it's quite a lot of data. And data, like happiness, is rapidly becoming a commodity. 

Our everyday digital transactions with retailers, governments and each other generate confidential data that is nevertheless used to analyze and predict human behavior. This makes it easier for scammers to hijack our credit cards, for example. 

Despite this, we continue to pour out personal information online, often without a second thought. Think about it: there are 500 million new tweets per day on Twitter. This means that the risk of privacy invasion is high. 

Our personal information, so sought after by companies and advertisers, is now, for the first time, readily available to them. Facebook sells information about our activity on Facebook to advertisers, for example. This information is analyzed and manipulated by marketers who ensure we see ads that match our tastes and interests, often with spooky accuracy. 

Moreover, in 2014, Facebook published a paper on emotional contagion in social networks. Unbeknownst to us, Facebook had been using an automated system to identify positive or negative words in news feeds.

Facebook then experimented by altering levels of positive or negative content across the feeds of 700,000 users for a week to assess the impact on their emotions, based on their status updates. In other words, your happiness is constantly under surveillance.

### 8. Final summary 

The key message in this book:

**Happiness is an emotion that is real and measurable. In our capitalist world, those in power seek to use money to measure positivity, manipulate our emotions to make us more productive, and mine our data to channel our happiness into consumption.**

Actionable advice:

**Trust your own subjective feelings of happiness.**

People you know and don't know constantly try to measure and define your level of happiness. Don't let them! Your emotions are your best guide as you learn what makes you happy, what doesn't and how you can experience more happiness in your everyday life. 

**Suggested** **further** **reading:** ** _Who Gets What — and Why_** **by Alvin Roth**

In _Who Gets What — and Why_ (2015), Nobel Prize winner Alvin Roth brings his groundbreaking research on market design to a broader, nonspecialist audience, explaining how markets work, why they sometimes fail and what we can do to improve them. Using contemporary examples, Roth outlines the nonfinancial factors that shape markets and shows how we can make more informed marketplace decisions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### William Davies

William Davies is a senior lecturer and director at the Political Economy Research Centre at Goldsmiths, University of London. He is also the author of _The Limits of Neoliberalism_. His writing has appeared in _New Left Review_, _Prospect_, the _Financial Times_ and _Open Democracy_. His website _www.potlatch.org.uk_ was featured in the _New York Times_.

