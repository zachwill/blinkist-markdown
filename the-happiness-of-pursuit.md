---
id: 548a16e733633700096e0000
slug: the-happiness-of-pursuit-en
published_date: 2014-12-24T00:00:00.000+00:00
author: Chris Guillebeau
title: The Happiness of Pursuit
subtitle: Find the Quest that Will Bring Purpose to Your Life
main_color: 2FB5EA
text_color: 1F7799
---

# The Happiness of Pursuit

_Find the Quest that Will Bring Purpose to Your Life_

**Chris Guillebeau**

_The Happiness of Pursuit_ is about embarking on a quest to fill your life with a sense of purpose. It explains how the process of finding and following a personal journey can be deeply rewarding and life-affirming. The reader can expect to come across many interesting case studies of people who have followed their passions — from a young environmentalist who spent a year living in a Tasmanian tree to an Oklahoma woman who dedicated herself to cooking a meal from every country in the world.

---
### 1. What’s in it for me? Learn how to discover your true passion and follow your dream. 

_The Happiness of Pursuit_ speaks to the timeless human impulse for self-discovery, but its message is even more relevant in today's world, where so many people unthinkingly follow the herd by embarking on the traditional path of school, work and family. The author encourages people to set their own course, and devote themselves to their true calling.

In addition, you'll learn

  * why someone spent 100 days seeking out rejection;

  * why cooking a meal from every country might be better than visiting just one; and

  * how much it costs to travel the entire globe.

### 2. Many people share the desire to embark on a life-affirming journey. 

What does sailing around the world have to do with spending a year living in a tree? Well, much more than you might think.

Both of these scenarios are _quests_. A quest is a journey that heads toward a specific life-affirming goal. A key feature of a quest is that it includes various challenges.

Here's a great example of a quest: Tom Allen was a young British graduate who turned down a dream job to go cycling around the world, despite the fact that he was inexperienced as both a cyclist and a traveler.

He encountered many challenges along the way: Once he was forced to navigate the Sudanese desert without a map; another time, he caught malaria. But Allen confronted every challenge he encountered, eventually completing his quest. When he returned to England a year later, he was no longer the same naive young man he'd been before he started out.

If his experience sounds appealing to you, that's because many people share the desire to go on a quest.

For some, this desire manifests itself as a response to uncontrollable external factors. This was Sandi Wheaton's motivation. She embarked on a quest to document her journey on Route 66 after she was laid off from her job at General Motors.

For others, like Laura Dekker, a quest arises out of the desire for self-affirmation and discovery. Dekker was born on a boat and spent her whole life sailing. Ever since she was ten years old, she'd planned to become the youngest person to circumnavigate the world by sailboat. When people asked about her motivation, she said, "I just loved the sea and wanted to do this for myself."

Others pursue a quest as a way of taking a stand. Miranda Gibson, for example, spent more than a year living in a Tasmanian tree to protest illegal logging.

Ultimately, quests are born from hard-to-shake ideas or urges, and are typically intended to benefit either the individual or the wider world.

> _"You can stop thinking about a hobby, but a quest becomes a total fascination."_

### 3. A quest arises when an inner calling is set in motion by outside factors. 

As Bob Dylan once said in an interview with _Rolling Stone_, "everyone has a calling" — that is, a motivating passion that leads them to their own personal quest.

It could be that you feel "victorious" when you find a superb piece of tuna — which is the case for Jiro Ono, owner of an award-winning Tokyo restaurant. Or perhaps your heart races when you see a vintage train. Whatever it may be, when something consumes your attention, it's a good candidate for a life quest.

Identifying your own passion isn't always easy; sometimes we need a triggering event to get going.

Some quests arise out of discontent. The author explains this using the following equation: dissatisfaction + big idea + willingness to take action = new adventure.

For example, although Juno Kim had a stable engineering job in South Korea, she was dissatisfied and unhappy with her life. Her big idea? To leave everything and go traveling. At that time, most backpackers were Western, but she was willing to break the mold. She took action and went on an adventure, eventually becoming an advocate for Asian female travelers.

Sometimes, a quest is triggered by a growing awareness of mortality. Once you start thinking about the end of life, you might start living every day as if it were your last.

This was what bird enthusiast Phoebe Snetsinger experienced when she was diagnosed with melanoma and told she only had a year to live. Realizing that there were still many things she wanted to do, she scheduled trips all over the world. Ultimately, she ended up with the world record for most identified birds.

So as you can see, quests arise when an inner calling is set into motion by outside factors.

> _"Unhappiness can lead to new beginnings."_

### 4. Believe in yourself and embrace rejection to stay motivated to complete your quest. 

Once you have a quest, how can you achieve it? Well, you need to believe in yourself, because that's what will keep you going in tough times.

For example, classical music DJ Gary Thorpe spent 28 years trying to produce the longest symphony ever written — Havergal Brian's _Gothic Symphony_. No one else saw the point, but he firmly believed it would be worthwhile.

Eventually, after many failed attempts, he managed to organize the concert. It was a hit: One review called it "a triumph beyond all expectations."

Of course, even when we believe in ourselves, sometimes we still need a little help to stay motivated. That's why it helps to divide your quest into manageable tasks.

Steve Kamb tried this method when he decided to change his boring life into a more exciting one. To help him reach his goals, he created a video game based on his journey, _His Epic Quest of Awesome_. Every time he achieved five goals on the game, he "leveled up." Then the game would prompt him to embark on more complicated tasks. This enabled him to move on from smaller goals, like getting fit, to more complicated ones, like piloting a stunt plane and learning Portuguese.

Here's another way to stay motivated: Draw energy from rejection and failure.

Jia Jiang shows how empowering it can be to use rejection as a motivator. Jiang spent 100 days trying to get rejected as many times as possible. For instance, he went to a fire station and asked if he could slide down the pole. When people said no to his weird requests, he would simply move forward and make more requests, always anticipating rejection.

For him, this process ended up being empowering — because if you aren't scared of rejection, you can try anything.

> _"Not everyone needs to believe in your dream, but you do."_

### 5. Whatever your passions or personal circumstances, no quest is too big or too expensive. 

Don't have the money to realize your space travel dreams? No problem. No matter what your circumstances are, there's a quest for you.

It's true: Even if you think you don't have the time for a quest, you can easily tweak your dream to make it fit your personal circumstances.

For example, Sasha Martin from Oklahoma didn't have the means to travel the world, even though she badly wanted to. Instead, she brought the world to her dinner table, by embarking on a quest to cook a traditional meal from every country in the world.

Often, you'll need to adjust your life to accommodate your quest. For Allan Bacon, adjusting his life _became_ his quest. He embarked on a series of _life experiments_ — that is, small changes oriented toward improving his life. These were as simple as spending his lunch hour visiting a museum or enrolling in a photography class. Surely you can find similar small ways to change your own life.

Of course, sometimes quests involve spending a bit of money, so before you start, make sure you know how much your journey will cost.

And if it seems too expensive, consider breaking down the larger project into smaller, more affordable components.

The author learned this when he realized that his dream quest, visiting all 197 countries in the world, would be impossibly expensive and time consuming. But when he broke down the costs for the first 50 countries, he realized that traveling the world would only cost around $30,000, and take five to seven years to complete.

Even though he didn't have $30,000 lying around, he knew that if he lived frugally over the course of several years, it was achievable.

So as you can see, even if you're busy or don't have much disposable income, you can find a way to complete your quest. You just have to set manageable goals and create a strategy for achieving them.

> _"If you want to achieve the unimaginable, you start by imagining it."_

### 6. Pursuing something you love is a reward in itself. 

You might think that the ultimate point of embarking on a quest is the feeling of accomplishment you'll experience when you finally complete it.

And yet, this isn't the whole story. Because it's not just the end of the journey that will thrill you, it's also the process itself.

As the author says, "When you find something you love to do, you don't want to let it go." That's the ultimate appeal of a quest: It feels good to strive for something you love and believe in.

This was independent musician Stephen Kellogg's experience. He grew up listening to Bon Jovi and playing air guitar in his room. When he first started his music career, things were tough. But he told himself, "It's better to be at the bottom of a ladder you want to climb than at the top of one you don't." He was glad to be aspiring toward his goal of becoming a real musician.

People like Kellogg, who manage to turn their quest into their vocation, tend to share something: They're obsessed with what they do and are motivated to continually refine it.

This doesn't necessarily mean that they enjoy every single day throughout the quest — after all, practicing the guitar for hours a day can be lonely and difficult — but they do find a way to get pleasure from the overall process.

We can see this when we consider the careers of famous comedians like Jerry Seinfeld. Seinfeld has earned millions, and yet he continues to practice new stand-up routines, often in small venues.

As Seinfeld demonstrates, when you really care about something, you keep it up. Because when you stop, you'll notice a deterioration in your skills.

As you can see, although achieving a quest can be deeply satisfying, so is the process of refining your craft.

> _"Effort can be its own reward."_

### 7. If your life lacks purpose, you need to find your quest. 

Do you ever find yourself pondering the meaning of life and bemoaning your own lack of purpose? If so, you need a quest!

Having a personal journey can give your life a sense of purpose, especially if your quest is directed toward a greater cause.

This was Stephanie Zito's experience. After working in nonprofits for 15 years, Zito was disillusioned by the wasteful bureaucracy she'd witnessed. So she set up a project called #Give10. She committed to donating $10 a day to a cause of her choosing, as well as an extra $600 three times a year to charities other people recommended.

Her quest wasn't only about giving, but also about discovery. It enabled her to discover many inspiring smaller charities, like a project called Liter of Light, which created cheap lighting using plastic bottles, or another that was initiated by a five-year-old boy who sold paintings online to raise money for medical research.

There's another aspect of quests that can be edifying: Failure. This might seem counterintuitive, but failure can actually support our efforts, because it gives us information about whether we're on the right path.

For example, Mark Boyle wanted to walk to India, but lost his motivation early on after encountering language problems on the French coast. He returned to England disheartened, and couldn't stop mulling over this embarrassing failure. But after a good deal of contemplation, he realized his worries derived from the fact that he'd pursued the wrong quest.

He hadn't, as it turned out, really wanted to walk to India. Rather, his true dream was to live a life without money. So instead of traveling to India, he embarked on a new quest. He spent a year surviving without money, by bartering and working for shelter and sustenance.

Today, he guides others who want to follow a similar path with his annual Freeconomy Festival. For Boyle, going on a quest — and failing at it — was just a stage on the path to a sense of purpose.

> _"Quests bring meaning and fulfilment to our lives."_

### 8. Quests can change your life for the better but they also change people for the better. 

Many of us are fed up with the humdrum lives we lead. But a quest can change all that by transforming us and lifting us out of tedium.

Because ultimately, the journey itself — not just the end result — will make you more self-sufficient, more confident and more empowered.

This is what happened to Nate Damm, who experienced a complete personality change when he walked across the United States. Before his quest, he was shy in social situations. But his journey forced him to overcome his timidity: Often, he had to knock on strangers' doors to ask permission to sleep on their properties. He came out of the experience feeling more confident and independent.

In addition to making you feel more confident, completing a quest can empower you. Once you've accomplished something, you can't help wondering, "What _else_ can I do?"

For example, when the author ended his quest to visit all the countries in the world, he started to think, "There's nothing I can't do!" This self-confidence was so constructive and empowering, he was inspired to take on new, greater challenges.

This was Sasha Martin's experience, too. Martin set out to cook meals from all around the world, but over time, her quest assumed a greater meaning: She began spreading a message of peace via her blog, instigated by her culinary interaction with different cultures at schools, social occasions and other events. Her perspective shifted from culinary to human, from personal to public.

Ultimately, Sasha's story is typical of the nature of quests: Quests enable us to improve by allowing us to become more confident and expanding our scope in the world.

> _"Undertake a quest or any long, challenging project, and you won't come out of it the same."_

### 9. Once you’ve completed a quest, finding a new one will restore a sense of purpose to your life. 

Completing a quest is empowering, but it can also feel a little sad. If you feel directionless after finishing a project, just know that the only way to stop feeling lost is to embark on another quest!

However, before you do, take a moment to debrief and reflect on what you've learned.

For example, Alicia Ostarello gave up her apartment and all her possessions to go on 50 dates — one in every state in America. She completed her quest, but was left with the question, "What should I do next? Should I spend some time crashing at the houses of friends and family while I think about what I've learned? Or should I get straight back into the world of work?"

She chose to go back to work, and although she found a great job, it was the wrong choice. She couldn't focus properly because she hadn't processed what she'd experienced on her quest.

So take your time, but once you've assimilated and processed the old quest, then you should find a new one! This will bring a renewed sense of purpose into your life.

This was the case for Scott Young, who had devoted one year to learning the four-year MIT computer science curriculum. He spent five days of the week doing intensive study, one day earning money and one day resting. Although there wasn't much variation, he found the experience challenging and stimulating.

And when he was finished with his quest, he found another: He decided to try a similar approach to learning languages. Before he could change his mind, he boarded a plane to Spain — and he spent the next year learning Spanish from scratch.

Follow Scott's example: The best way to avoid the feelings of drift and emptiness that arise at the end of a quest is to find another.

> _"After completing a quest, the next steps are up to you."_

### 10. Final summary 

The key message:

**A quest is a path to happiness, because it brings direction and purpose into our lives. Ultimately, the journey will transform you, because you'll be actively confronted by new challenges. Instead of sitting slumped on the sofa all day, you'll be out learning new skills, accumulating experiences and seeing new things, all in the service of a big, meaningful goal.**

Actionable advice:

**If you aren't ready to make a big change, spend a week tinkering with your life in small, beneficial ways.**

Try getting rid of your television, speaking to a stranger everywhere you go, using your lunch hour to visit an art museum. Making even these tiny adjustments will change your life, and also motivate you to pursue more challenging goals.

**Suggested further reading:** ** _The Art Of Non-Conformity_** **by Chris Guillebeau**

Based on the author's blog and online manifesto, "A Brief Guide to World Domination,"_The Art of Non-Conformity_ deals with ways of pursuing unconventional living and offers tools for setting your own rules, succeeding with your passions and leaving a legacy.
---

### Chris Guillebeau

Chris Guillebeau is a traveler, entrepreneur and _New York Times_ -bestselling author. He fulfilled his own personal quest — visiting every country in the world — before he turned 35.

