---
id: 532859853635380008930000
slug: the-happiness-project-en
published_date: 2014-03-25T15:30:00.000+00:00
author: Gretchen Rubin
title: The Happiness Project
subtitle: Or, Why I Spent a Year Trying to Sing in the Morning, Clean My Closets, Fight Right, Read Aristotle, and Generally Have More Fun
main_color: 2BBBD5
text_color: 197080
---

# The Happiness Project

_Or, Why I Spent a Year Trying to Sing in the Morning, Clean My Closets, Fight Right, Read Aristotle, and Generally Have More Fun_

**Gretchen Rubin**

What is happiness and how can we bring more of it into our lives? Gretchen Rubin asked herself this question because although she fulfilled all the prerequisites for a happy life — an intact family, a good job and enough money for a rainy day — she found herself frequently unhappy. During her year-long _Happiness Project_, she read about various techniques and theories on increasing happiness and tried to become happier with their help.

---
### 1. What’s in it for me? Look for happiness and find it – in all facets of life. 

For thousands of years, humankind and its philosophers have tackled the question of happiness. Aristotle declared that happiness was the _summum bonum_,or the chief good in life — that it was the sense and purpose of the entire human existence. But what is happiness and how can we achieve it?

To approach this question, Gretchen Rubin dived into research, turning to the most important thinkers, psychologists, writers, intellectuals and researchers for ideas on the subject. Factoring her personal experiences into the mix, she came to this realization: happiness has many facets. But we know when we're happy and there are ways to enhance happiness.

With that in mind, she launched her happiness project. Over the course of one year, she dedicated each month to a different area of life in which she tried to boost the level of her own personal happiness.

She saw a particularly high potential for boosting happiness in the following areas of life and thought:

  * Social relationships (partners, children, friends),

  * Structuring time (work, play, hobbies),

  * Prospects/point of view/perspective (basic attitude, contemplation of eternity),

  * Money and vitality.

Her happiness project didn't usher in 100 percent sunny days, but she managed to secure various little islands of happiness and lay the foundation for more _moments_ of happiness by taking small, constructive steps such as a good exercise regimen, a better time management system and a more balanced everyday life with her family.

She experienced many interesting things about herself along the way. She stuck to pre-established basic principles that enabled her to, above all, reduce _negative_ feelings. Because, as Rubin found out, it's usually our own wrongdoing that makes us unhappy. Once she stopped allowing herself to form feelings based on her inclination to nag and gossip, amass useless things and pay too little attention to her health, she was consequently happier.

In her book, Rubin describes what constituted her personal increase of happiness and how we can apply it to our own lives.

### 2. On the road to happiness: make good resolutions and take small steps. 

Given that people are better suited to working toward concrete goals than abstract things like _being happy_, Gretchen Rubin gave herself concretely formulated and measurable resolutions and requirements that she adhered to throughout the entire project. They served as universal principles to be followed in each of the phases of the experiment she undertook.

Rubin knew what easily soured her mood and tried to avoid those things using her resolutions. By giving herself instructions such as "Do it now!" and "Be Gretchen!" she hoped to achieve positive feelings in all kinds of situations. What she saw was a basic opportunity to feel more happiness by, for example, simply carrying out tasks as they arose (rather than putting them off) and staying true to herself every step of the way.

She had several realizations concerning these general guidelines, which she had amassed over the course of her life. She then repeated these to herself constantly during each phase of her project — regardless of whether she was seeking a professional challenge or more harmony in her family life.

For example, phrases like "People usually notice your mistakes less than you think," "You don't have to be good at everything," or "What you do every day matters more than what you do once in a while," helped her to stay focused when things got rocky and not lose sight of her path to happiness.

Finally, Rubin realized that the small steps and moments of happiness were what really helped her achieve the desired change. With the motto _Change your life without changing your life_, she wanted to attain the highest level of happiness possible from the most mundane of situations. Rather than dropping out of society and moving to the rainforest or turning her life completely on its head in her search for happiness, she simply wanted to become a little happier and more satisfied with the life she was already leading.

### 3. Release your vital energy: be active and get rid of energy guzzlers. 

It's indisputable that energy and vitality are fundamental factors in self-satisfaction. Feeling physically and mentally fit is enough to make us happy. And feeling energized makes us want to engage in other activities, like sports or social events, which in turn generate moments of happiness. That's why "more energy" was the top priority of Rubin's happiness project.

But what generates energy? It may sound banal, but it's true: enough sleep, a balanced diet and physical activity. A lack of sleep weakens our immune systems and memories and slows down our metabolism. Outdoor activities, by contrast, raise our energy level and help us improve our thinking. It's been proven that light is good for the psyche and stimulates the production of serotonin and dopamine, two hormones responsible for feelings of happiness.

A mere ten-minute walk produces an energy boost, but it's even more effective to walk 10,000 steps every day. To that end, Rubin got herself a pedometer and started keeping track, which made her feel more active because she was consciously energizing herself. And every day she gave herself the thrill of fulfilling her step quota yet again.

And what eats up energy? Rubin discovered that, for her, a big part of her energy was sucked up by _junk_ that had been sitting around for ages waiting for her to sort it out and just piling up higher and higher. One study claims that sorting out unnecessary things on a regular basis saves up to 40 percent of housework. Therefore Rubin started to let go of useless things whose sheer existence created work for her: clothes she didn't wear, useless (promotional) gifts, bad buys or duplicates.

It's just as important to get rid of mental ballast. When unfinished tasks are put off for a long time they end up haunting us, making us feel unsatisfied and robbing us of our energy. Rubin found that crossing things — like making that appointment for an overdue check-up or setting up a back-up system for her computer — off her to-do list gave her little boosts of happiness that required very little effort.

Thoroughly sorting things out is extraordinarily satisfying because disorderly _clutter_ — whether literal or figurative — always wears us down.

### 4. For a harmonious relationship, work on yourself: it will prevent negative experiences. 

Rubin established the following in her research: happy people's relationships last longer. And it also works the other way around: harmonious relationships set the standard for the happiness we feel elsewhere in life.

Rubin based the part of her project dedicated to her marital happiness on two basic insights into relationships:

Number one: You can't change your partner; you can only work on yourself. And number two: What you do every day matters more than what you do once in a while.

She therefore made an effort to keep bad habits and sources of unnecessary arguments to herself within a controlled framework.

Recognizing that her tendency to nag and burden her husband with her complaints was exhausting, she resolved to no longer transfer so many negative feelings onto him and to try to act more agreeably in general. She replaced rude remarks and aggravated accusations with neutrally uttered one-word reminders for him like "garbage" or "garage" and noticed how much it lifted her own mood.

In addition, she internalized the words of French author Pierre Reverdy, who once said there was no such thing as love, but rather only proofs of love. She attempted just that in one week of practicing extreme kindness without asking for anything in return. By surprising her husband with little gestures and tokens of appreciation made with no expectation of praise or pats on the back in return, she not only made her husband happy, but made herself happy too.

According to studies, it takes at least five positive actions to offset a negative action in a relationship. That's because negative experiences and feelings generally take root more quickly and deeply than happy ones. Bearing that in mind, we should try to reduce the negative ones to make room for positive experiences in our relationships.

In order to do so, we should learn how to "fight right," i.e., to not let ourselves be carried away by fundamental debates about all the mistakes the other is supposedly making. A big part of being happy with one another has to do with not taking each other for granted and with a constant examination of the way you treat your partner.

### 5. Happiness rubs off on others: if our children have fun, so do we. 

Many adults say that children are the greatest source of happiness in their lives. Rubin considers them a kind of happiness that we don't see in a specific moment, but in the long term. In order to foster this happiness, she decided to be more tender and playful with her two daughters as part of her project.

Rubin had two resolutions: to control emotional outbursts directed at her children and to refrain from scolding them. Her strategy? When we act happily, we are happy. Someone who — like Rubin — is easily irritated and gets angry about childish behavior would benefit from singing every morning and acting as if they were in the best of moods. By doing so, we can infect ourselves with a good mood and begin our day that way. Finding humor in our challenges — for instance, by making a joke instead of a reproach — brightens up our everyday life.

Like many parents, Rubin tended to want to talk her children into believing that they'd really better take a bath (a severely hated activity) or that taking their socks off wasn't nearly as horrible as they thought it was and surely didn't warrant crying fits. Instead, Rubin made an effort to take her children's feelings seriously and acknowledge them by saying things like, "Socks can be tough to get off," or by writing down their wishes.

She also found out that it made her happy to get involved in time-consuming arts and crafts projects or long games with her children — things she'd always shirked doing before. Preparing a children's birthday party isn't usually the most fun for parents, but the parties make their children happy, which in turn rubs off on the parents' own mood. In that vein, Rubin saw leisure hours with her daughters as moments conducive to happiness.

Getting along better with her children also helped her drop a lot of stress in her everyday life. And it made her happier in general because she rid herself of the feeling that she was an uncaring mother.

### 6. Shared joy is joy doubled: making time for friends brings fun into the stress of everyday life. 

Friendship is an essential part of a happy life: good friends we can trust and talk to and have fun with help us to get through our stressful everyday life with a smile.

And so it's all the more paradoxical that many social contacts are at risk in a happy life. We always end up with the feeling that we don't have enough time for all our friends. Rubin also experienced this, which caused her to feel guilty and angry with herself. She thus decided to deliberately make time for her social life and make new friends in the process.

She began with the resolution to always remember her friends' birthdays, call them regularly and get out more often. Suddenly her fleeting acquaintances became part of her circle of friends.

Just as in marital relationships, a big source of happiness in friendships is based on generosity. Rubin's realization is that the surest way to be happy is to make others happy. And the surest way to make others happy is to be happy yourself.

We don't have to lavish our friends with expensive gifts: motivating our friends to continue rising above themselves, being there for them when they need us, and spending time with them will make them much happier anyway. Rubin helped several friends clean out their closets and thus felt happiness doubled — for her friends and for herself.

### 7. With the right job, new challenges and growth make us feel happy about our work. 

Happiness and work are inextricably linked in our society. When we work, we maintain social ties and feel valued. The feeling of growth we get from working is an inexhaustible source of self-confidence and recognition.

Ever since becoming a writer, Rubin just keeps on working — even when she's having a bad day — and often gets a kick by knowing that she's gotten something done despite a bad mood. Deciding to start a career as a writer was a huge step for her, as she was coming from a stable job in the legal field. But it paid off: now her passion — writing — is her career. She urges others to work up the courage to make such a rupture in their lives, whatever their passion might be. Because if your job happens to be your favorite thing to do, it can be a great source of happiness.

Rubin saw tackling challenges and engaging in new things as key criteria on her search for happiness. Challenges positively stimulate our brains and dealing with new situations often brings about unexpected satisfaction. Rubin, for instance, gave herself the ambitious goal of writing a novel in 30 days. Achieving that made her incredibly proud and gave her a serious motivational boost.

And this boost inspired her to start a blog even though she didn't know the first thing about blogging. At first, the challenge of learning a brand new media technology was difficult and frustrating. But she learned to accept her passing failures as a necessary part of her mission. And the feeling of joy she felt after mastering her first blog entry made up for all her previous struggles anyway. Rubin's new identity as a blogger created a lot of positive feedback, which in turn boosted her self-esteem and her feeling that she'd grown. Her level of happiness soared.

As a general rule, if we want to feel happy in our work, we shouldn't just aim for the moment when our goal has finally been achieved. It's the little moments of success that make us happiest. This is usually short-lived, and then we set our sights on the future and our next goal.

### 8. Do more activities you truly enjoy in your leisure time. 

One big — and indisputable — happiness factor is having free time full of fun and relaxation. Because doing things that don't serve any purpose other than our own personal enjoyment makes us happy.

But "fun" means something different to everybody. And many people — Rubin among them — have a guilty conscience because they consider other people's hobbies more cultivated or creative than their own. And she realized that, even if sporting events, crossword puzzles or knitting were great leisure activities for other people, they weren't necessarily the right ones for her. If you've enjoyed something since you were ten, it's a good indicator that it's something you truly have fun doing.

Rubin remembered that, at ten, she loved to make collages, pasting newspaper snippets, pictures and cut-out quotes into empty books. And she immediately took that up as part of her happiness project. She also started a children's literature reading group: aside from feeling happy about being involved with her favorite literary genre, the new social connections also brought her many further moments of happiness.

Having lots of new things to do also makes us happy — and so it makes sense to seek out some new hobbies. You can also keep a diary of what you pick up from other people and then see what works best for you.

That's how Rubin came up with the idea to start collecting something. She thought that, as a popular hobby, collecting was something that could make her happy. A collection is a mission and the growth of happiness comes - at the very latest - when it's complete. But the process of sifting through, hunting and finding things in new areas brings a lot of joy.

Rubin's collection of bluebird figures (bluebirds are a symbol of happiness in the United States) created the little bit of chaos that was allowed in their house. As nice as it is to avoid clutter, Rubin believes it's OK if you have _one_ drawer filled with "useless" things, like collector's pieces, so you can conjure up memories or little surprises every once in a while when you open it.

### 9. You can’t buy happiness – but you can buy worthwhile things that make you happy. 

We've all heard it before: money can't buy happiness. But the subject isn't closed so simply. Because money is like health: it's not a guarantee of happiness, but not having to worry about it makes our lives a whole lot easier.

Money can help us to be happier if we spend it wisely. Initially, a sufficient amount of money is an important happiness factor for many people who strive for security in their lives. But Rubin also specifically searched for ways to use money to fulfill her remaining resolutions to boost happiness. For example, she began to spend more money on a good fitness program, a healthy diet and social events.

The belief that distinctive consumer behavior doesn't make us happy is widespread. Happiness theories say that buying a new car or a nice piece of clothing will only make us happy for a short period of time. As soon as we've gotten used to the new acquisition we go back to the same level of happiness we had before. Rubin's observation speaks out against this: the satisfaction of a buy may be short-lived, but it's real, and always involves a positive feeling of growth.

That's why she decided that it'd be all right for her to indulge in the occasional modest splurge as long as it provided her with some added value. During her honeymoon, for instance, she treated herself to room service for the first time ever. Another time, she shamelessly bought an expensive food processor, which she has since used to make healthy fruit shakes for herself and her family every day — giving her energy and putting her in a good mood.

However, if we buy too many of these purchases just for the thrill of it, the practice becomes ineffective and can potentially lead to new collections of superfluous stuff. And since only _we_ can decide which purchases will make us happier, we should consciously choose which purchases make our lives more pleasant.

### 10. Spiritual support: become aware of happiness in the here and now. 

If we believe studies on the subject, spiritual people are happier and healthier, can handle stress better and live longer. Contemplating eternity and death — a central part of many religions and of philosophy — enables another approach to the present and happiness. Rubin was fascinated by the idea of living her life filled with gratitude while thinking about eternity.

She then came up with the following insight: "The days are long, but the years are short." This motivated her to become aware of both her existence and her transience during her project.

On the one hand, she began a "one-sentence" diary in which she wrote down events she'd like to remember every day, such as her daughters' funny anecdotes. The spiritual mission to confront death inspired her to come up with the idea of updating her, and her husband's, last will and testament.

Since she had decided to take small steps in her project and didn't want to devote herself to the rituals of any specific religion, she integrated small meditation sessions into her everyday life — at the bus stop or while waiting in line at the supermarket. She thus managed to make a conscious break by making short pauses for reflection between her usual activities.

Another resolution on Rubin's spirituality list was to be more grateful because gratitude is known as a key to happiness. One tool she had heard about was a gratitude notebook. She wrote down in that notebook the happiest moment of each day — or the things that she took for granted but knew she was actually very lucky to have.

All in all, Rubin came to the conclusion that the awareness of only having one finite life lets us perceive more intensely the happiness of every moment of that life.

### 11. Combat negative feelings with mindfulness and by expanding your consciousness. 

In her search for happiness, Rubin kept stumbling upon the Dalai Lama's book, _The Art of Happiness_. The Buddhist view of life had always fascinated her, so she decided to get more deeply involved in it during her project. The book introduced her to the key concept of _mindfulness_. The idea behind it is to pay more conscious and unbiased attention to all things in the here and now instead of being constantly plagued by the past or obsessed with the future.

And so, one of the final phases in her happiness project was to experiment with techniques for opening one's consciousness in order to become more mindful of things.

First, she wanted to get her thoughts and everyday actions out of autopilot mode. And since she wanted to be mindful of her diet — which had till then been a frequent source of bad moods and remorse — she began to keep a food diary. Thanks to this, she realized that she was consuming far too many ready-made foods and unhealthy snacks. Once she kicked that habit, she finally got rid of the bad feelings that had been tormenting her every day.

This first positive experience inspired her to try out other popular practices of expanding her consciousness. After attending a session with a hypnotist, she found that it didn't influence her mind as much as her own private practice of listening to a hypnosis tape. Doing so made her more mindful of her thoughts and actions and helped her change them for the better. Thanks to this insight, she was able to prevent the imminent negative emotional outburst she might have experienced when, say, her computer crashed.

She also figured out during a laughter yoga course — a combination of yoga exercises, singing and laughing — that she didn't need any special Indian technique to see that laughing out loud can make us feel happy. It's important to pay attention to happiness-boosting activities — whether by taking a course or all on your own.

### 12. Final summary 

The main message of this book is:

**Happiness is different for everybody. And we're all capable of boosting our happiness if we recognize what makes us happy and take concrete steps to make those things happen.**

This book in blinks answered the following questions:

**What are the basic steps of Gretchen Rubin's happiness project?**

  * Look for happiness and find it — in all facets of life.

  * On the road to happiness: make good resolutions and take small steps.

  * Release your vital energy: be active and get rid of energy guzzlers.

**How can our social ties boost our happiness?**

  * For a harmonious relationship, work on yourself to prevent negative experiences.

  * Happiness rubs off on others: if our children have fun, so do we.

  * Shared joy is joy doubled: making time for friends brings fun into the stress of everyday life.

**How can we shape our happiness at work and in our free time?**

  * With the right job, new challenges and growth make us feel happy about our work.

  * Do more activities you truly enjoy in your leisure time.

  * You can't buy happiness — but you can buy worthwhile things that make you happy.

**How can we pursue happiness spiritually?**

  * Spiritual support: become aware of happiness in the here and now.

  * Combat negative feelings with mindfulness and by expanding your consciousness.
---

### Gretchen Rubin

Gretchen Rubin studied law but decided to become a writer instead. Thanks to her blog, _The Happiness Project_, she became a famous blogger. In addition, she has written biographies of Winston Churchill and John F. Kennedy, as well as several self-help bestsellers, such as _Power Money Fame Sex_.

