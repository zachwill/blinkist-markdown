---
id: 580b6c9ed4d2ce0003dd2475
slug: the-happiness-track-en
published_date: 2016-10-26T00:00:00.000+00:00
author: Emma Seppälä, PhD
title: The Happiness Track
subtitle: How to Apply the Science of Happiness to Accelerate Your Success
main_color: E9632F
text_color: B24C24
---

# The Happiness Track

_How to Apply the Science of Happiness to Accelerate Your Success_

**Emma Seppälä, PhD**

_The Happiness Track_ (2016) outlines the simple steps you can take to become happier and more successful. Referencing the latest scientific research, these blinks debunk common myths about how to be successful and set out a concrete plan for you to reduce stress in your life.

---
### 1. What’s in it for me? Be happier! 

When a friend or acquaintance asks you what you want, you might say "to be happy." Who doesn't want that?

But despite our deep-seated desire for happiness, most of us spend our time pursuing other things such as careers, money or love — even though these things are just shades of what might make us happy.

What if instead of pursuing these elusive goals, we focused on finding happiness directly?

These blinks explore how your mind and body work and how you can use these facts to find the true path to happiness. From pretending you're a child to making sure both your nostrils aren't clogged, you'll learn practical ways to live a brighter, happier life.

In these blinks, you'll learn

  * why people should be like antelopes to achieve happiness;

  * how planning ahead can be counterproductive; and

  * why study participants were inspired to give themselves electric shocks.

### 2. We focus too much on the future, but we can train ourselves to remain in the present moment. 

What were you thinking before you started reading these blinks? Perhaps you were mulling over plans for the upcoming weekend or worrying about a work deadline.

Spending time thinking about the future may make you feel as if you're productive, but often the opposite is true.

Why? Focusing on the future reduces your attention in the present moment, making you more likely to miss significant things that are happening now. This lack of presence can cause problems with relationships at work and home. What's more, constantly thinking about what's to come can leave you less productive today and worse, emotionally drained.

Staying fully present in the moment will help you perform better at work and may even make your job more enjoyable. And forget about the belief that multitasking is efficient. It isn't. In fact, performing multiple tasks makes you complete each task much less efficiently.

One study, for instance, monitored people having conversations while driving. Researchers found that activity in the section of the brain dedicated to driving was reduced by 37 percent while the driver was talking!

Other research shows that people feel much happier when they are 100-percent engaged in a task, even if the task is relatively boring.

So how can you stay present in every moment, whether minor or major? There's no shortcut. Being present is a skill you need to practice.

The first step is to remove unnecessary distractions. Try decluttering your workspace; also, block access to websites you like to browse on your laptop and silence your mobile devices while you work.

The next step is to focus on tasks in small time increments. Setting a timer can be useful. When you carve out specific, limited amounts of time to complete tasks, you'll find yourself enjoying each task more, and additionally, your sense of achievement will skyrocket.

Practicing meditation is another way to stay in the moment. By letting thoughts go, you'll become calmer and more attentive to the world around you.

In addition to boosting productivity and happiness, meditation can have other surprising results. Some people who meditate say they experience a world that appears more vivid, with brighter colors and richer tastes!

### 3. Many of us live in a world fueled by constant stress, which is said to help us thrive. But it doesn’t! 

Many people think stress is a necessary fuel to stoke certain behaviors, such as ambition or personal achievement. Let's examine this assumption to see if it holds water.

There are two kinds of stress: the good, short-term kind and the bad, long-term kind.

Short-term stress sends adrenaline coursing through the body and often inspires better mental and physical performance. Consider the exhilaration an athlete might feel when people cheer, motivating him to perform at his best. Practicing and playing in a "real" game are different precisely for this reason.

Too much stress, however, can harm the body and eventually turn into chronic stress, which can lead to illness — and even cause stress in people around you.

Emotions, both good and bad, are contagious. They are transmitted through pheromones, or chemicals, that the body releases when we sweat. So when a coworker is anxious or afraid, that person's stress is bringing everyone else down!

Not to worry, though. Long-term stress can be curbed through the body's natural resilience. Think of an antelope that goes back to grazing just minutes after narrowly evading a predator. We too are gifted with the ability to calm ourselves — without it, we'd hardly be equipped to meet life's challenges.

You've no doubt seen how a small child can bounce back from tears with laughter. This is a perfect example of the resilience of the human spirit.

But many people today have a weakened resilience to stressful situations. Consider how advertisers use stress to grab a buyer's attention, promising beauty or an attractive companion if you buy a product. This scheme stresses you by reminding you that you might lack these things in your life.

The daily barrage of emails, social media updates and news further contributes to the modern human's high levels of stress.

### 4. The best way to reduce stress is through the body, by learning how to breathe properly. 

Have you ever been told to "just relax" when you're stressed out? It probably didn't feel very useful. Taking a bath or going for a walk, on the other hand, often helps to calm frayed nerves.

So what do these actions tell us about the nature of stress?

The bottom line is that we've been misled in how to deal with stress. Two common approaches are either talking ourselves out of stressful, upsetting situations or suppressing our emotions and pretending nothing's wrong. Is it a surprise that neither "trick" really works?

Crucially, research shows that attempting to control or suppress a thought or feeling when you are stressed only makes the problem loom larger in your psyche. Suppression can lower self-esteem, increase negative emotions and worsen memory over the long term.

Researchers at Stanford University found that people who regularly suppress negative emotions, for instance, generally experience _more_ negative emotions overall than positive emotions.

So how do we tame stress in a healthy fashion? We now know that the best way to calm down is actually from the outside-in — in other words, through the body.

Think about the way you breathe when you're anxious — you probably take shallow breaths. When you're calm, however, you breathe deeply and steadily. Research supports this practice: by changing your breathing, you can change your emotions.

The author successfully used breathing techniques in her work with war veterans suffering from post-traumatic stress disorder (PTSD). Deep breathing helps to normalize levels of the stress hormone cortisol, which increases emotional resilience.

You too can practice deep breathing exercises. Not only do these exercises make you calmer but they also they give you a boost of energy!

It may seem counterintuitive that being calmer can energize you, but it's true! Let's explore how you can change your breathing habits for the better.

> "_By controlling your breathing, you can use a voluntary mechanical behavior to make a profound change on your state of mind."_

### 5. Breathing techniques and slow-paced activities can help build resilience against stress. 

The British wartime slogan, "Keep Calm and Carry On," has become a popular quote on T-shirts, coffee mugs and other items in recent years. But how does one keep calm in reality?

This is where simple breathing exercises come into play. Doing so regularly helps you essentially rewire your nervous system.

Start by closing your eyes and taking slow, deep breaths for a few minutes every day. Pay close attention to your breath, noticing whether it is deep or shallow. Make sure to exercise deep, conscious breathing especially if you are angry or stressed.

Breathing in such a manner might make you feel self-conscious at first, but soon your practice will become a natural reflex that will help quiet your nerves!

Another effective technique is called _alternate nostril breathing_.

You might be surprised to know that only one of your nostrils actually "does" the breathing on a daily basis. To experiment with alternate nostril breathing, try blocking the airflow of one nostril, pressing down on it with your hand. Then alternate, blocking the flow of the other nostril.

This exercise will increase the airflow through the less dominant nostril. By exercising your less active nostril, you'll enable greater airflow through your nose. More air means easier, calmer breathing — and in turn, a calmer you.

Try practicing alternate nostril breathing for a few minutes a day and see if you can feel a difference!

Of course, there are other exercises you can do to calm yourself: slow-paced activities such as long walks are helpful. In fact, any activity that slows you down, such as meditation, yoga or tai chi, is healthy for you. Long walks in nature have been shown to reduce anxiety, lighten the mood and even improve memory.

And if you ever needed an excuse to hug someone you love, here it is! Research has shown that physical contact with a loved one reduces stress, too.

### 6. Burnout happens when we worry too much, exhausting our minds and our nerves. 

Have you ever felt "burnt out" from a job? In the working world, this has become a widespread phenomenon, and it is growing. What exactly causes burnout?

There are three main ways in which you can exhaust your mind.

The first is experiencing extreme emotional highs or lows. Such events can drain you both mentally and physically.

The second is trying to exercise self-control all the time. Doing so tires your mind, making you less likely to make sound decisions later on. Resisting the temptation of a breakfast donut in the morning, for example, can weaken your ability to say no to a bag of potato chips for dinner.

And finally, if you constantly worry (or worry about upcoming events that you feel will make you more worried), you can experience constant, often crippling, fatigue.

The solution to preserving your energy is to find something positive in each and every situation.

Doing things that make you feel good is simply good for you. One study found that when people watched a funny video or received a surprise present, they showed no fatigue and displayed more resilience in maintaining self-control.

Even a boring task can be enjoyable if you focus on why the task is worthwhile — maybe this chore is helping you save up for a vacation or is teaching you a new skill.

Staying positive and calm will also help you perform better in stressful situations.

One of the author's patients was a soldier injured in a firefight. By using calming techniques, the soldier said he was able to give orders and carry out commands while injured that he otherwise couldn't have if he was over excited or nervous.

### 7. Learn these three easy steps to becoming more creative by learning to make time for idleness. 

How do you feel when you're doing absolutely nothing? Do you feel bad, or guilty? Yet idleness might be just the medicine your body and soul needs.

Contrary to the popular belief that you have to be focused to come up with innovative ideas, _not_ concentrating might be what you need to do.

In interviews, professionals across a number of fields have said that creativity is highly prized. To be creative, however, you have to let your mind wander — only then can you come up with novel solutions.

One study found that participants performed better on a difficult task after they stepped away from their desks and spent time daydreaming. If you've ever had a "eureka" moment while in the shower, you know that this is true!

But many people are so used to being overstimulated that it's an uncomfortable feeling to let the mind just wander.

A study conducted at the University of Washington showed one extreme version of this inability to just be. Participants could sit and do nothing or give themselves electric shocks. Many participants — even those who said they'd pay to never experience an electric shock again — still chose to shock themselves, to do "something" instead of nothing.

So how do you combat an aversion to idleness? Ignite your _creative idleness_ with three easy steps.

First, in between tasks that demand your full attention, perform mindless tasks that only require partial attention, like data entry. But don't confuse mindless tasks with leisure tasks such as surfing Facebook or Twitter feeds, as these tasks actually hold your full attention.

Second, make time for play. Whether jumping up and down on a trampoline or playing hide and seek with your children, activities that aren't goal-oriented allow your brain to change gears and in doing so, work to expand your thinking horizons.

And finally, allow for moments of silence. Instead of filling all your time watching, listening or reading, find a space in which your mind is allowed to be totally quiet — for instance, through meditation.

You'll be more creative and successful overall if you spend more time doing nothing. Activities unrelated to your work are frequently the ones that can have the greatest positive influence on it.

> "_Albert Einstein famously said, 'To stimulate creativity, one must develop the childlike inclination for play.'"_

### 8. It’s time to conquer the negativity bias. Less self-criticism, more self-compassion and self-love. 

When a friend tries something new and fails, what do you do? It's likely you choose not to criticize her or tell her to give up. Yet this is exactly what many of us do to ourselves.

Being your own worst critic can be good in small doses, but being too self-critical is counterproductive.

Rather than boosting motivation, constant self-criticism can lead to depression. Being tough on yourself makes you focus disproportionately on your negative attributes instead of on your positive ones.

Why are we so hard on ourselves? Researchers say being tough may have been a survival mechanism for early humans. Focusing on the negative probably helped our ancestors avoid dangers in the wild.

Today, however, this _negativity bias_ isn't helpful, as most mistakes don't immediately mean you're dinner for a hungry predator. Excessive self-criticism instead is crippling, making you afraid of failure, which in turn can lead to poorer performance overall — which becomes a self-fulfilling prophecy.

Studies conducted with athletes found that a fear of failure was enough to make a star performer fail at critical moments despite years of rigorous training.

The answer to all these problems is to have more compassion for yourself.

Self-love and understanding will give you a healthier, more balanced view of your skills and the world around you. If you adopt a less critical perspective of yourself, you'll be affected less by internal and external criticisms, and consequently become more resilient to daily stress.

Self-compassion will also make you more likely to take on challenges you think you may fail at — which is necessary for success.

Once you better understand your mistakes and failures, you'll bounce back more easily from the lows, instead of letting your critical internal voice keep you down.

### 9. Expressing gratitude helps you focus on what you have instead of what you think you lack. 

For many, Thanksgiving is a day of gratitude and gorging on traditional dishes. Roasted turkey and pumpkin pie aren't bad in moderation, of course. But gratitude is a dish we should always eat more of.

Research shows that only around half the people in the United States regularly show gratitude to one another. As the benefits of showing gratitude are so overwhelming, it's surprising we don't express thanks more often. In fact, expressing gratitude is possibly the greatest act of self-compassion there is.

Gratitude helps us focus on what we have instead of what we lack, and thus makes us as people less materialistic and more positive-thinking. Showing gratitude regularly can even help you sleep better, simply because as you drift off, your positive thoughts will make you feel more peaceful.

Other simple steps to be kinder to yourself include repeating supportive phrases. Be your own cheerleader, and don't forget that mistakes are a part of life.

Sure, "to err is human" — so remember that everybody has faced failure at some point. If you acknowledge this fact, it will help you approach your failures as a learning experience.

Writing is another way to become more self-compassionate. Start a journal and let your feelings flow onto the page.

You could even experiment by writing a letter to yourself, but write as if you were talking about a friend. Creating this distance between your feelings and thoughts may help you put life events into better perspective.

You might also try writing down a daily list of achievements or things for which you are grateful.

Taking a pen to paper is a simple action that can have positive, long-term health effects. Recent studies have shown what writers have always known — that you can regulate emotions by writing about them.

### 10. Reap the many rewards that come from being kind to others instead of focusing on yourself. 

Growing up, you may have been told that you need to look out for yourself before anyone else. How much truth does this instruction hold?

Focusing too much on yourself can have negative effects. It makes you a poorer judge of people, generally less liked by others and can even make you sick.

Looking out only for "number one" can also make you egotistical, a classic trait of narcissists. Such people are blinded by their egos, making them exaggerate their own abilities. While narcissists may feel superior, when they fail, as everyone must, it usually hits them harder than it would those who are humble.

The behavior of selfish people often makes them disliked by the crowd. Egotists are also less likely to form meaningful personal bonds, which can lead to health issues, such as depression.

The antidote to being self-absorbed is to look outward with a compassionate, open heart. Compassion means being sensitive to those around you and acknowledging the distress of others. Being empathetic is, in fact, hard-wired in human nature.

This might come as a surprise given the "dog-eat-dog" world we live in. But German researchers found that chimpanzees and young children, without knowledge of learned social practices, will go out of their way to help a companion in need.

Interestingly, being compassionate can also be good for business. A 2011 study from Michigan University that focused on compassion in the workplace found that showing compassion improved company performance levels and profit margins and increased employee happiness.

Curiously, those same employees were not only happier but also healthier. The same study found that compassionate individuals on average lived longer and were more resilient to stress.

Many of us are taught to look out for ourselves, but the proof of the pudding is in the eating; being kinder improves business success, relationship bonds, overall health and personal happiness.

### 11. Final summary 

The key message in this book:

**The key to happiness is being present in the moment, eliminating stress and letting your creative juices flow freely. Self-care is important, but so is showing compassion for others. Only by being kind to ourselves and each other can we thrive both as individuals and as a society.**

Actionable advice:

**Show you care with words.**

When a friend appears to be distressed, verbally communicate that you see she might be hurting. Doing so shows you care about her feelings and gives her a chance to correct you, in the case that you've misread her emotions. Communicating with and caring for each other compassionately amid hard times helps to reduce stress for everyone involved.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Happiness By Design_** **by Paul Dolan**

Drawing from his own behavioral research and economist experience, Dolan explains how happiness arises and how we experience it in our everyday lives. He exposes some of the traps we fall into when trying to be happier and demonstrates some simple tools for adapting your environment to feel happier without having to radically change who you are and what you think.
---

### Emma Seppälä, PhD

Emma Seppälä is the science director of the Center for Compassion and Altruism Research and Education at Stanford University. At the forefront of happiness research, she regularly publishes in the _Harvard Business Review_ and _Psychology Today_. _The Happiness Track_ is her first book.

