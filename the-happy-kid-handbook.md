---
id: 58582e33d1a2f300047968ab
slug: the-happy-kid-handbook-en
published_date: 2016-12-23T00:00:00.000+00:00
author: Katie Hurley, LCSW
title: The Happy Kid Handbook
subtitle: How to Raise Joyful Children in a Stressful World
main_color: F14666
text_color: 992C41
---

# The Happy Kid Handbook

_How to Raise Joyful Children in a Stressful World_

**Katie Hurley, LCSW**

_The Happy Kid Handbook_ (2015) explains the key components of a happy childhood that allows children to thrive as unique individuals. Whether your child is introverted or extroverted, these blinks will guide you through helping them understand stress, negative emotions, social relationships and the importance of finding calm in their lives.

---
### 1. What’s in it for me? Become an attuned parent. 

The world is full of advice on how to raise children: Just watch _Supernanny_, read about rigorous "Tiger Moms" or ask any harassed parents on the receiving end of unsolicited advice. The problem is that everyone tells you something different. And while you're trying to figure out who's right, you forget about consulting the world's greatest expert on your child's needs: your child.

Of course, this doesn't mean that you should give in to your child's every wish. But as you'll learn from these blinks, the first step toward raising your child to be happy and confident is to listen to her and encourage her to explore and express her emotions. If you tune in to your little one, she will teach you how she needs to be raised.

These blinks will also delve into the benefits of play and explain how you can help your child to cope with today's immensely stressful world.

And you'll discover

  * why it's better for your children if you don't try to be fair all the time;

  * how to help an exuberant child to relax; and

  * about a scary Easter bunny.

### 2. Cookie-cutter approaches won’t help you raise your unique child. 

Say you've got two adorable kids — one girl and one boy. You love them both to bits and take great care to share your love fairly. You're diligent about dividing your attention equally between them and never cuddle one child without cuddling the other. So why is it that your son is so distant when you talk to him, and why does he squirm out of your hugs so often?

It could be because you aren't meeting his needs in the right way. The reality is that different kids have different needs. It's your responsibility as a parent to raise your children in accordance with their personalities.

It's possible, for instance, that your son doesn't need lots of interaction and physical affection to be happy. Your daughter, on the other hand, craves stimulation and wants to engage with you all day long. By spending lots of time chatting to your daughter and less with your son, you aren't being unfair. Quite the opposite — you're ensuring each of your unique kids is equally happy.

In keeping with this, you'll need to respect your child's wishes and needs, even if it means they don't match yours.

A very sociable parent who puts lots of time and effort into throwing a huge birthday party for their kid might be disappointed to see their child hiding in their bedroom, overwhelmed by all the noisy kids crowding the living room. But even if your child's needs surprise you, you must respect them. After all, it's not your child's job to fulfill your expectations. So do your best to embrace every aspect of their character.

In short, there's no place for a one-size-fits-all approach in child-raising. Children are unique, and it's vital that you respect their needs. And how do you do this? By learning about them!

### 3. Introverted kids need gentle encouragement and a safe space to share their feelings. 

When it comes to temperament, nothing's black or white — we've all got a mixture of both extroverted and introverted traits. Still, some children lean strongly toward introversion. Perhaps they love to disappear into their daydreams or tend to retreat to their bedrooms to reenergize alone. Raising introverted children comes with challenges, but keeping two strategies in mind will help.

First, it's vital that you help your child feel comfortable sharing their feelings. Introverts naturally process their feelings internally, which in itself is fine. But it can cause problems as they fail to ask for help when they really need it. They may also keep quiet about something they're unhappy about, bottling it up until they can't cope anymore. This inevitably leads to an emotional outburst which is as confusing for you as it is for them.

Your child can make a habit of sharing emotions by writing them down in a _feelings book_. Write down several emotions, such as love, envy, happiness and anger. Ask your child to draw a situation where he experienced each of these feelings in the book. Discuss these feelings and your child's experiences together. This technique won't turn your child into an extrovert overnight, but it will reinforce the feeling that he can feel safe to share his feelings whenever he needs to.

The second thing to remember when raising an introverted child is to criticize her gently and in private. Introverted kids are sensitive and get embarrassed easily. Criticizing your child in front of others is the last thing you should do, as it'll upset your child and make her clam up or want to hide. Wait to address any issues in private and make sure your child feels safe and cared for as you discuss it with her.

### 4. Extroverted children need to talk problems through and learn to unwind. 

If you've got a child who's largely extroverted, watch out! These kids are boisterous, impulsive and born to be wild. Little extroverts can be a handful, especially if you're more of a quiet type yourself. Learn to help your extroverted kid balance the interaction and movement with letting off steam and learning to relax.

Talking and sharing (a lot!) is vital for an extroverted child. They process their emotions externally, and will typically cope with challenging feelings by sharing them with you. Talking things out is essential for them in other ways too. You might find that your extroverted child is able to solve homework problems best when talking them through out loud with you, rather than mulling them over alone in their room.

Extroverted kids also have a lot of energy, and it's great if you can help them let off steam. Physical activity is a great outlet. Whether it's playing a team sport or roaming through a park, she'll invariably feel calmer and more focused afterward. Other creative activities such as puppet play and building projects are great ways to burn off some energy inside on a rainy day. Extracurricular activities like sports incorporate these outlet activities into your kid's weekly routine.

Finally, your extroverted kid will benefit from learning relaxation skills. Even the most energetic extroverts need a little downtime. But sometimes, they don't know how to calm themselves down. This is where you come in. From child-friendly yoga DVDs to easy breathing exercises, there are many resources out there that you can experiment with to find a calming routine that suits your kid best.

### 5. Play boosts your child’s social and emotional skills. 

Remember how fun it was to play as a kid? Most parents don't. That's why they tend to see play as a waste of time that distracts kids from homework and other important tasks. But nothing could be further from the truth. Play is central to a healthy childhood.

Through fun and games, children learn to relate to each other, share and cooperate. When adults meet for the first time, they go through the motions of introducing themselves, shaking hands and sharing what they do for a living. When kids meet each other, they make contact by joining one another's games.

Children learn to solve conflicts, find compromises and negotiate in play. From deciding on roles when playing hospital to working out how to build a treehouse together, kids ensure their play session doesn't end in chaos and tears. But social skills aren't the only benefit of play.

Playing is a great way for kids to understand and express their feelings. Children rarely look as happy as when they're running around playing with each other. And kids who don't feel comfortable expressing anger or sadness can use puppets, toys or playing pretend to express them. In this way, play has become an important part of therapy for children.

For example, one of the author's young patients played with a doll house and let a doll test out the different ways of expressing sadness and anger, from running away from her adopted parents to shouting at them. This helped her and her therapist gain a better understanding of her negative feelings. This, in turn, helped the child explain her feelings to her caretakers rather than acting out.

### 6. Help kids understand that negative emotions are a natural part of life. 

All loving parents just want their kids to be happy. But kids needn't be happy all the time. For one thing, this is nearly impossible! And on top of this, negative emotions are a necessary experience that helps children grow into adults.

Despite this, many parents attempt to reject their children's unhappiness. They categorize emotions into "good" and "bad," signalling to many kids that feeling fear, anger, jealousy and sadness is wrong. Parents often feel bewildered and irritated by a child's strong negative reactions. For instance, in the mall one Easter, the author witnessed several toddlers sobbing in fright after seeing someone in a bunny costume. Visibly annoyed, the parents didn't comfort them and instead told their kids to stop making such a fuss.

Through experiences like this, children learn that it's only acceptable to share positive emotions. This can even lead them to mask and repress their negative feelings. But it doesn't have to be this way. All emotions, pleasant or painful, are natural parts of childhood experience. Parents are responsible for helping kids learn to understand and deal with them.

Younger children, in particular, have a hard time coping with their emotions. They're often overwhelmed and unable to identify or manage their reactions. Parents are there to help them make sense of these feelings and feel safe again.

If a toddler cries in fright because of a scary Easter bunny, his parent could tell him something along the lines of "Oh dear, you're upset. You must be scared because of that bunny. But he won't hurt you! You're safe here with me." This helps the child understand where his feeling is coming from, that it's OK to feel like that, but also that he's safe to stop feeling that way too.

### 7. Children need strong role models to develop their empathy skills. 

Humans are, in some ways, instinctively empathetic. That's why babies tend to cry if they hear another baby crying nearby! But even though humans demonstrate the ability to empathize from a very young age, they still have a lot of emotional development to do throughout their lives. The ability to empathize with someone's situation and perspective is more a skill than an instinct, and parents are there to help children learn it.

Begin by giving kids an empathetic role model. If you show a lack of empathy toward your kid, you can hardly expect them to become empathetic individuals of their own accord. Show your kids empathy and it won't take them long to realize that they can do the same. Respect their feelings, listen to them without interrupting and show children that you understand and care.

You can also enlist older siblings or friends to help. While children often struggle to relate to the adult lives of teachers and parents, brothers, sisters and older kids are natural role models for younger children. Ask these kids to help your child learn the importance of empathy, by encouraging them to share their emotions with them or talk through why they're struggling to be empathetic. This can create valuable relationships from which your child learns a lot.

> _"No one cares how much you know, until they know how much you care." — Theodore Roosevelt_

### 8. Protect your child’s health by helping them cope with stress. 

These days, we're all stressed out. Unfortunately, this includes our kids. Between competitive classrooms and jam-packed schedules, it's no wonder children constantly feel the need to perform. Help your kids recharge and recover by learning about how they deal with stress.

First, we need to remember that some things can be stressful for our kids without us realizing. For example, you might be used to watching the news, but your kid isn't. Watching footage of natural disasters, famines and wars can be downright terrifying for your kid. Don't let your kid watch the news with you and you'll cut the extra stress out of her life.

Secondly, consider the role schoolwork plays in your child's stress. As homework increases, some extroverted children tend to push themselves too hard. Reluctant to abandon their beloved extracurriculars, high energy kids can stress themselves out because they have no time to wind down. Protect your child from this and encourage her to plan time in the week to relax.

This is crucial, because stress can harm your child's physical and mental health. Even low levels of stress can interfere with your child's sleeping patterns while bottled up stress may lead to migraines and back and neck pain. Chronic stress can even cause longstanding illnesses and high blood pressure at a young age. Stressed out kids are also at risk of suffering anxiety and depression, causing their home, social and academic lives to suffer.

Help your child learn to manage stress by using relaxation techniques. A timeless trick is relaxation breathing, where your child breathes in and counts to three, holds her breath for three and releases for three. You can further complement this calming technique by guiding your child's imagination to a favorite place or describing a walk along the beach.

### 9. Final summary 

The key message in this book:

**Raising your child right is about letting them thrive, rather than restricting them with rules. Get to know their unique personality and learn about their specific needs. Be a role model they can learn from and above all, give them the freedom they need to play and grow.**

Actionable advice:

**Consider the great potential of discarded stuff.**

Next time you're doing your spring cleaning and thinking about throwing out piles of old possessions, think again! Old concert tickets, first aid supplies — even old luggage — make wonderful toys for kids. After all, they've got a great imagination and will happily turn them into props for scenes they've dreamt up. Create a dress-up or prop box for your kids and let them rummage through it as a treat.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How to Talk So Kids Will Listen & Listen So Kids Will Talk _****by Adele Faber and Elaine Mazlish**

_How to Talk So Kids Will Listen & Listen So Kids Will Talk_ (1996) is a practical, clear guide that helps parents improve communication with their children. Faber and Mazlish present realistic scenarios that can have parents tearing their hair out in frustration, and then provide precise tactics for not just coping, but turning around the situation and creating more harmonious parent–child interactions.
---

### Katie Hurley, LCSW

Katie Hurley is the blogger behind Practical Parenting and the author of articles at PBS Parents and _The Huffington Post_. She lives in Los Angeles, where she splits her time between being a child and adolescent psychologist and counselor, a parenting expert and a mother to two children.

