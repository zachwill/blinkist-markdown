---
id: 57dd8955f9916500030ea7b2
slug: the-haves-and-the-have-nots-en
published_date: 2016-09-23T00:00:00.000+00:00
author: Branko Milanović
title: The Haves and the Have-Nots
subtitle: A Brief and Idiosyncratic History of Global Inequality
main_color: 515F34
text_color: 515F34
---

# The Haves and the Have-Nots

_A Brief and Idiosyncratic History of Global Inequality_

**Branko Milanović**

_The Haves and the Have-Nots_ (2010) shows how inequality throughout history has made its mark on society at large. These blinks explore three types of inequality: inequality among individuals in a single country; inequality among countries; and global inequality, or inequality among all the world's citizens.

---
### 1. What’s in it for me? Achieve a more nuanced understanding of inequality. 

Inequality is a world problem. In many countries, a thin slice at the top seems to hold all the wealth, while many suffer to make ends meet. Huge gaps exist even between countries, as some receive billions in foreign investment while others struggle to improve roads and basic services for their citizens.

What exactly is inequality, and why does it exist? In short, inequality is an imbalance, the measure of haves and have-nots. There are various factors that come into play when we talk about or try to measure inequality. These blinks will help you better understand the causes of inequality and how one person, one country or even the world can work to level the economic playing field.

In these blinks, you'll discover

  * why inequality in certain cases is considered a positive thing;

  * how scholars measure inequality; and

  * why inequality is on the rise again.

### 2. Inequality between individuals can change depending on developments in society. 

Do you think a capitalist society would have different levels of inequality than would a socialist society?

Your answer is probably "yes," but in the early days of income distribution studies, the answer wasn't always so obvious.

In the early 1900s, Italian economist Vilfredo Pareto, a pioneer in the field of inequality, was the first scholar to study inequality in terms of income distribution among individuals rather than among classes.

Pareto's central belief was that social arrangement — whether a society was capitalist, socialist or feudalist, for instance — had almost no effect on income distribution.

He reached this conclusion through the omnipresent _80/20 law_, which states that 80 percent of an economy's effects — such as sales in stores — are a result of 20 percent of the causes — such as the demand of clients.

In turn, this means that the wealthiest 20 percent of the population controls 80 percent of the total income.

In Pareto's view, changes in social arrangement couldn't ever effectively challenge this law. As a result, Pareto believed that levels of inequality would always remain more or less unchanged.

Russian-American economist Simon Kuznets challenged this idea in 1955 when he theorized that inequality among individuals does _,_ in fact, change as society changes.

After conducting extensive research, Kuznets found that economic growth initially increases income inequality, but later decreases it.

When society shifted from an agricultural focus to an industrial focus, the new industrial class began to earn much more money than did farmers, and thus income inequality increased.

As society advanced, however, increased education and progressive state policies — such as government spending, taxes and other forms of income redistribution — caused income inequality to stall and eventually decrease.

Such policies shifted the balance of income toward the bottom and showed that, in contrast to Pareto's ideas, Kuznets was correct in believing that social arrangement does have an impact on income inequality.

### 3. Inequality is intertwined with both economic growth and economic justice. 

Inequality is a hot-button issue for good reason, as it affects everyone by directly impacting a country's economic growth.

In fact, one way to determine whether inequality is "good" or "bad" is to see how much inequality encourages or discourages economic growth.

In this way, we can liken inequality to cholesterol — it can be either good or bad, depending on the amount and the effect it has on the overall "body" of society.

For instance, when inequality motivates people to apply themselves and work hard, perhaps even pursue groundbreaking entrepreneurial projects, it facilitates growth and is thus considered good, or even essential.

On the other hand, when inequality creates complacent populations in which individuals remain in protected positions, thus stifling creativity and economic growth, it is considered bad.

Such situations are easy to imagine. Consider a society in which only the smallest, wealthiest group of people receives the best educational training and the top jobs, limiting the rest of society to taking low-skilled and limited-growth positions.

In such a society, only a very small segment of the population would be contributing significantly to overall growth. As a result, growth would be much lower than in a society where a greater part of the population was contributing more to total economic output.

In this way, inequality is directly linked to a society's economic efficiency.

Inequality is also strongly linked to _economic justice_ or the acceptance or repudiation of certain social arrangements.

For instance, when inequality is the result of practices that favor or discriminate against certain populations based on race, gender or inheritance, it is clearly bad and unjust, even if it doesn't negatively impact economic growth.

When people come together to protest income injustice, they are questioning the social arrangement that enables such injustices. Their demand for a refashioning of the very social foundations in place shows the relationship between inequality and economic justice. __

### 4. While there are ways to measure inequality, doing so is still a difficult task. For now, we have Gini. 

If inequality as an element of society is so influential, it's necessary to have a way to measure its effects.

Let's investigate some of the methods in place to do so.

One of the first things to realize about measuring income inequality, however, is that it is hard to do.

Household surveys measuring income haven't existed for long. Though there are surveys on record that were taken during nineteenth-century England, most are incomplete and unreliable. Most available data starts in the early 1950s, and for developing countries, even later — some African nations have surveys dating back only to the 1980s.

Let's say that we did have complete household income data — even then, measuring inequality would prove difficult. We can assess a country's national income with a single number, such as gross domestic product (GDP), by simply adding each person's yearly income. Yet when it comes to the diversity of income distribution, it's much more difficult to sum up neatly.

This doesn't mean that people haven't tried. Take the Italian economist Corrado Gini, for instance, who invented a method for measuring inequality which is still preferred today.

His method was to compare the income of an individual with the incomes of the rest of the population. Mathematically, this consists of dividing the sum of all income differences by the total number of people and their average incomes.

This calculation results in a unit of measurement called the _Gini coefficient_, which ranges from zero, indicating no inequality whatsoever, to one, which indicates maximum inequality, in which one individual is getting the income of the entire community.

Using the Gini coefficient, we can compare inequality levels of entire countries and continents.

Accordingly, Latin America is the most unequal, followed by Africa, Asia and the rich and postcommunist countries. The most egalitarian countries, such as the Nordic countries, have Gini scores from 0.25 to 0.3, while the most unequal countries, such as Brazil and South Africa, have Gini scores around 0.6.

> "_There are no countries where all people are paid equally, nor are there countries where only one person appropriates the entire income."_

### 5. We can compare the inequalities of countries and individuals across different eras. 

Let's say you're doing a research project and need to compare China's income in 1850 with that of France in 2000. Luckily, there are methods that enable such comparisons, though the math won't necessarily be easy!

To make this comparison, we convert the currency of the different countries into an imaginary standard currency, based on the US dollar. This currency is called the PPP, or _purchase power parity_, and it has the same purchasing power everywhere — so $1 PPP buys the same basket of goods in Japan as it does in Spain.

With some complicated math, we can figure out the GDP per capita of a country in PPP dollars, fix it for a specific year, and then, using the country's GDP growth rates over the years, project backward to find GDP per capita in the past.

Another method to compare the wealth of nations at different times is to measure income by its ability to purchase human labor.

American industrialist John D. Rockefeller was the richest person ever by this measurement — his worth of $1.4 billion in 1937 equaled that of around 116,000 people. So although Microsoft founder and philanthropist Bill Gates' fortune was estimated at $50 billion in 2005, adjusting for time and inflation renders the sum about 75,000 workers fewer than Rockefeller's total.

This method also makes it possible to look at the global makeup of wealth distribution. For instance, half of the world's richest one percent is made up of 29 million Americans. The rest is made up of four million Germans; three million French, Italians and British, each; two million Canadians, Koreans, Japanese and Brazilians; then around one million Swiss, Spaniards, Australians, Dutch, Taiwanese, Chileans and Singaporeans.

Interestingly, despite the stereotype of the well-funded Russian oligarch, there aren't any significant numbers from Russia or Africa, India and Eastern Europe.

### 6. Although socialism was more egalitarian than capitalism, it diminishes the incentive to work. 

It's no shock to discover that a socialist system creates more equality than does a capitalist system.

Socialism is commonly positioned as the egalitarian alternative to capitalism. But, as we can see from models in pre-World War II Europe, as well as in the Soviet sphere, socialism's alluring ideals of equality can also cause serious problems.

After World War II, socialist systems were clearly more egalitarian than capitalist systems, with socialist countries having Gini scores in the upper 0.2 to lower 0.3 range. Western countries with capitalist economies, such as West Germany, France, Denmark and Italy, had Gini scores in the low to mid-0.3s.

Among the various causes that made socialist systems more equal was socialism's push for the nationalization of large industrial and landowning fortunes, as opposed to privatization. This was particularly true in countries like Russia following the 1917 Revolution, and in post-World War II Hungary and Poland.

The problem with such societies, however, was that citizens had little incentive to work hard or creatively. When everything — your education, your job and your health care — is predetermined by the state, it's easy to see how innovation and growth could be stifled.

Even in East Germany, which at the time was the most technologically advanced socialist country, factories produced little else than Trabant and Wartburg cars, which were actually mediocre copies of West German cars.

In fact, no socialist country ever produced a product that proved to be internationally successful. The lack of incentive to innovate proved the crux of socialism's failure, and led to the question of whether equality was a worthwhile goal.

It's also questionable whether socialism actually creates egalitarian societies — despite lofty ideals, socialist governments almost always were mired in corruption, with those at the top funneling money from the rest of society.

### 7. Inequality among countries emerged after the Industrial Revolution and has continued to rise. 

Countries like Brazil and India are noticeably poorer than the United States or Great Britain. Just as we see inequality among individuals, inequality also exists among nations, which can be measured by comparing average incomes per capita.

Such intercountry inequality became an issue only after the Industrial Revolution. Of course, it did exist before then — the Roman Empire at its peak, for instance, was much richer than its neighbors. But income disparity was relatively limited because most nations were still operating at the subsistence level.

The Industrial Revolution changed this because industrial countries that could produce goods charged ahead while others lagged behind economically. Other than a few exceptions, this inequality continued to rise until the 1950s.

After a period of decreased inequality in the interim, the last 30 years have seen a resurgence of inequality. 

Why? One reason is the Lucas Paradox, which refers to the tendency for capital to circulate among rich countries instead of flowing from rich countries to poor countries.

Capital also tends to flow upward from poor countries to rich countries. You might think that the advent of globalization would encourage more investment in poorer countries, due to their low wages and high return on capital. This hasn't been the case, however. Both rich countries and wealthy people in poor countries tend to invest in rich nations to limit financial risk.

In 2007, for example, China received $138 billion in foreign direct investment. That's comparable to what the Netherlands received and is less than what was invested in France and Great Britain. By contrast, the United States received almost double the amount — $240 billion.

Considering China's population versus these other smaller countries, its total investment sum is actually quite small. We can attribute this in part to the country's lingering status as a poor nation, thus making it still a risky investment bet.

> Before 2007, India received around $4 billion to $6 billion per year in foreign investment; the United States receives the same amount of foreign investment in a week.

### 8. Place of birth and family income class make it hard to improve one’s financial position in a country. 

When Karl Marx published his economic analysis of society in the late 1800s, his ideas that explored how income inequality was driven by class were largely true.

In modern times, however, inequality is almost entirely based on place of birth.

Today, there is more income disparity _between_ countries than _within_ them. As a result, your income depends chiefly on your country of birth, and this data point actually accounts for more than 60 percent of the variability in global income.

Are you surprised? You may have thought that class-based inequality within countries would transfer to the international stage, with the working classes of different countries uniting in solidarity.

That's not exactly the case. We can see this in action: the working classes of emerging economies such as South Korea, Taiwan and Chile have more in common with the working classes of rich countries such as the United States and France, rather than with the poorer classes in developing countries like Angola or Cambodia.

And so the overall wealth of the country into which you were born is more important than the specific income class of society to which you belong.

Of course, this is not to negate the effect of being born into a rich family — this still does matter!

It's actually these two factors combined — your place of birth and parents' income class — that explains more than 80 percent of a person's income.

The remaining 20 percent? That's where factors like gender, race, age and hard work come in.

Keep in mind, however, that even if you're trying hard to move up within that 20 percent, doing so will often have a negligible impact on your global income position — which, as discussed before, is largely predetermined by your birth country.

### 9. We can compare inequality among nations by examining Gini scores and the global middle class. 

It's common for people to draw cultural comparisons between the United States and Europe. In this blink we'll see that it's interesting to contrast the different causes of inequality between them, too.

In 2007, the United States and the European Union shared roughly the same Gini coefficient, just above 0.4.

But when comparing the Gini score of the United States with individual countries of the European Union, such as France and Germany, America's inequality measure was higher.

Why does the United States have a similar Gini score to the European Union but a higher score when compared with individual European countries?

In the United States, rich people and poor people are dispersed throughout the country. In the European Union, however, inequality among different member countries is the main factor of income inequality.

The European Union includes countries such as Luxembourg, the wealthiest country in the world with a GDP per capita of more than $70,000 PPP, as well as countries such as Romania, with a GDP per capita of $10,000 PPP.

Let's shift gears and take a look at the global middle class to further compare inequality.

One way to measure a country's middle class is by taking the incomes that are 25 percent above or below the _national median income_ — that's the income that divides a population in half, with 50 percent earning more and 50 percent earning less than the sum.

In most Latin American countries, only about 20 percent of the population fits this definition of middle class, whereas in developed countries, it's more around 40 percent. 

If we calculate for a _global_ middle class, however, we find that most of this group resides in Asia (at just under 600 million people), with 100 million in Africa and 90 million in Latin America.

Revealingly, almost none of the global middle class comes from developed countries. That's because the upper-income boundary of the global middle class is much lower than even the poverty threshold of rich countries — in other words, rich countries, in comparison to developing countries, are not poor enough to be included.

> _"In the United States, inequality is a matter of individuals; in the European Union, it is a matter of countries."_

### 10. Final summary 

The key message in this book:

**There are three main types of inequality: that of individuals or communities within a nation; between nations and countries; and global inequality. As there is more inequality between nations than between individuals, your country of birth has the largest impact on your individual income.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Spirit Level_** **by Richard Wilkinson & Kate Pickett**

This book provides a detailed explanation of how inequality is responsible for many of our present-day problems, including violence and mental illness. It provides detailed explanations and studies to support this and shows how inequality not only hurts the poor but everybody in a society.
---

### Branko Milanović

Branko Milanović is a specialist on inequality and poverty. He is the visiting presidential professor at the Graduate Center of the City University of New York and an affiliated senior scholar at the LIS Cross-National Data Center. He was formerly a lead economist with the World Bank's research department and visiting professor at University of Maryland and at Johns Hopkins University. Milanović is the author of _Global Inequality: A New Approach for the Age of Globalization_.

