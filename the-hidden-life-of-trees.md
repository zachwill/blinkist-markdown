---
id: 57d4615cb20bac00031a0d2d
slug: the-hidden-life-of-trees-en
published_date: 2016-09-13T00:00:00.000+00:00
author: Peter Wohlleben
title: The Hidden Life of Trees
subtitle: What They Feel, How They Communicate – Discoveries from a Secret World
main_color: 45583E
text_color: 45583E
---

# The Hidden Life of Trees

_What They Feel, How They Communicate – Discoveries from a Secret World_

**Peter Wohlleben**

Trees are engaged in countless complex cycles and they constantly struggle for water, light and their own survival. This struggle has led to some astonishing abilities: trees communicate with one another, give each other assistance, collaborate with fungi and other creatures, have memories and have even developed their own version of the internet!

---
### 1. What’s in it for me? Discover a hidden world. 

A book about trees?! Those boring, green-and-brown things that do nothing but stand there? That, at best, provide some shade or a convenient place to hang a hammock?

Well, yes! Thing is, trees aren't as boring as you might think. Or did you already know that they have their own kind of electric internet that enables them to warn friends and relatives, from miles away, about an insect attack? Or that, when there's not enough nitrogen in the ground, they collaborate with fungi? Or that they have different personalities and choose when to shed their leaves?

You see, there is a lot to discover in the woods. In their million-year-long history, trees have developed astonishing abilities that help them in the lifelong struggle to secure water, light and nutrients. They have occupied the craziest ecological niches and have established friendships, antagonisms and alliances with every other possible living thing.

No one is better suited to tell you all this than Peter Wohlleben, who has spent his entire life among the trees. As a child, he already wanted to work in environmental protection, and now has worked for more than 30 years with trees. He practices a natural kind of forestry that is productive and humane — one that he's honed with his unbelievable eye for detail and his immense factual knowledge. His awe of the woods and its inhabitants is legible in every line that he writes. In these blinks, join him on a walk through the forest that will put our green friends in an entirely new light.

These blinks also explain

  * what happens when aphids drink a tree's blood;

  * how a mushroom can kill more living things than a shower of bombs;

  * how trees go to the toilet.

**This is a Blinkist staff pick**

_"Everybody is talking about the secret lives of animals, but no one thinks about trees. As a forest-lover, these blinks fascinated me. I always knew there's more to trees than what meets the eye."_

– Laura, German Editorial Lead at Blinkist

### 2. Our planet’s lungs: Trees play a vital role in global water and carbon dioxide cycles. 

Before diving into the fascinating abilities of trees, let's take a brief look at their general importance.

Humanity owes them a great deal: they clean the air we breathe and help ensure the availability of water, even in the world's most remote locations. In fact, without trees humans would be unable to survive.

If there were no trees, large swathes of the earth would dry out. As you probably remember from school, the way the global water cycle works is that water evaporates from the oceans, condenses into clouds that blow onto dry land, where it then rains down and trickles into streams and rivers that flow back into the ocean.

However, this straightforward explanation omits one crucial fact: without trees, every cloud would rain down within 600 kilometers of the coast, leaving the inner parts of continents bone dry. Trees essentially act as gigantic water pumps, transporting water further inland. When it rains in a forest near the coast, much of the rain remains on the leaves of trees and the forest floor. This water then evaporates, forming new clouds that make their way further inland, where they rain down.

In addition to hydrating the inner reaches of continents, trees also clean the air of carbon dioxide, thereby protecting the climate. They gather CO2 from the air and store it, and when they die, some of this gas is re-released into the atmosphere, but much of it remains in the tree.

By burning these dead trees, whether in the form of coal or gas, we're releasing this CO2 back into the atmosphere and contributing to global warming. What's more, we're producing so much CO2 that the trees can't keep up — they are unable to store it.

As you can see, without trees the earth and its climate would be much less hospitable to humanity.

### 3. These roots run deep: Trees interact in many different ways with the forest soil. 

Trees aren't only an essential part of our global climate; they're also the basis for the soil in which we grow our food. And they interact with the earth's soil in many different ways.

Surprisingly, the majority of the earth's soil is comprised of trees. When our planet was formed, there wasn't much around: there were minerals (for instance, cliffs and rocks), and air and water. Beaten by wind and rain, sections of these cliffs eroded and, over time, became a kind of gravel or sand. This is where single-celled organisms and algae settled.

Small plants and eventually the first trees followed these initial biomasses. When these plants and trees died, they decomposed and, with the help of small organisms, turned into humus (or topsoil), returning to the earth from which they sprang. So, in the form of oil and coal, this ground contains the trees that died long ago.

Living trees are connected to the soil in many ways as well. They are anchored to it via their roots, through which they absorb water that is then pumped to their leaves for photosynthesis. And these roots also connect trees to one another; it is not unusual for two adjacent trees to share nutrients and information through them. Furthermore, trees develop underground connections to fungi, a phenomenon that we'll examine later.

By dropping their leaves in the fall, trees give back nutrients to the ground, nutrients that are then used by forest-dwellers. Among these nutrients are also all sorts of material that the tree has no more use for. When a tree loses its leaves, it's basically using the toilet.

> _In one handful of forest soil there are more creatures than there are people on this planet._

### 4. A high rate of child mortality: Young trees live dangerous lives. 

Without trees, we'd have no water to drink, only bad air to breathe and hardly any soil. High time, then, to get acquainted with our arboreal friends! What does the life of a tree look like?

Let's start at the beginning of a tree's life. Every type of tree follows a different reproductive strategy. Some have small seeds that are disseminated by the wind. Others, such as oaks and chestnut trees, have larger seeds, which are usually transported with the help of animals.

Where a seed ends up is dictated by chance. And each type of tree also prefers a different location. Of course, these preferences are mostly based on whether the tree will get the right amount of light and water, be protected from the wind and situated in the right type of soil.

There are types of trees that prefer growing next to one another, like birch trees, which need the protection of the forest. And others, like poplar trees, that often stand alone in meadows, where they can enjoy a whole day of sun and a lot of space. But, of course, this exposes them to storms and other dangers.

Unfortunately, the chances of a tree seed surviving are very low. Often, they land in places where they can't thrive or come up against conditions that prevent their growth. For example, many land in water (too much moisture!) or on asphalt (too little moisture!) or are brought to caves by animals, where there isn't enough sunlight.

And even when the seed lands in a place where it could grow, other dangers lurk. It may be eaten by deer or other animals, trampled to death, crushed by a storm or damaged by hail. In fact, over the course of every tree's life (which can, by the way, last for several hundred years) each tree will only raise one child to adulthood. But as soon as a small tree has managed to take root and survive its first years, it will show the kind of astounding abilities that all trees possess.

### 5. Nursery school: Trees have personalities and are able to learn. 

Young trees don't just grow; they develop a personality and, as the years pass, learn more about their environment and how they should best behave in it.

Personality, just as among people, varies among trees. Some are anxious, some bold. We tend to think of trees as doing nothing more than what we see them do. The weather turns cold, trees lose their leaves. Spring arrives, trees sprout buds and leaves. But it's not that simple.

On the author's land, for example, there are three oak trees growing close together. Each tree's trunk almost touches that of its neighbor. In autumn, one of the oak trees always starts to shed its leaves two weeks earlier than the others. Since they all experience the same temperature, the same soil and the same length of day, such variables can't be the explanation. So what's happening? Well, this tree is simply more careful than the others. Whoever holds on to their leaves longer can do more photosynthesis and store more nutrients. However, the longer a tree keeps its leaves, the higher the risk of injury: a tree will get hurt if it still has its leaves during a frost or a freeze.

Trees learn from experience. They have to make a lot of decisions throughout their lives. When do they shed their leaves? Where do they let their roots grow –toward the east, where there might be more nutrients in the earth, or toward the west, where there is more moisture?

Not only do trees make their own decisions; they also learn from their mistakes. A tree that, for example, kept its leaves too long during one year will never make this mistake again. This leads to several other conclusions: trees must notice the temperature and the length of the day and be able to save their experiences somewhere. Obviously, trees don't have brains, but it is thought that in the sensitive tips of their roots they keep track of information and experiences.

But trees aren't only clever when it comes to caring for themselves. They also support each other.

### 6. Chatterbox: Trees communicate in different ways, both with their own kind and with other creatures. 

It's good that trees can learn to cope with the threat of danger because they also like to talk about what they've learned. They do this in two ways: with scents and… with e-mail!

Trees can contact not only their own kind but also other creatures by using scent. Depending on the situation, they release different pheromones. They can be quite ingenious: when, for example, an elm tree or a Scots pine tree suffers a caterpillar infestation, the tree releases a scent that attracts a species of tiny wasp. These wasps fly to the affected tree and lay their eggs in the caterpillars; when these eggs hatch, the larvae attack and eat the caterpillars.

Here's another exciting example: trees have a way of identifying what kind of creature is trying to eat their leaves — by tasting that creature's saliva!

Information travels even faster through the forest's own internet. Electric pulses can only spread very slowly within a tree itself. If, for example, a caterpillar starts munching on a leaf, the leaf's fibers send out electric signals; these signals travel along the fibers at a rip-roaring pace of one centimeter per minute.

Under the ground, however, almost every tree is linked to countless fungal threads, which can transmit electric signals much more quickly. One single fungus can spread itself over several miles and thus connect many trees with each other. We know that trees can send specific electric signals to the fungi and thus inform other trees nearby about insects or drought or other dangers.

How that actually happens is still not fully understood. But it is being studied!

### 7. My friend the tree: Trees help one another. 

Trees aren't dumb. They are dependent on their ecosystems and conspecifics and can get in touch with them. No wonder, then, that they help each other out whenever there's trouble.

Trees often warn their conspecifics of potential dangers, by using scent and the "fungi-internet." Mostly, that works very well. For example, on the African Savanna, where giraffes like to eat umbrella acacias. Within minutes of this happening to an acacia, it will release poison in its leaves and, at the same time, emits a warning gas — ethanol — that alerts other trees within a 100-meter radius of the attack.

The giraffes know this game well; within a few minutes, they head off to another tree that's about 100 meters away, or to trees that are upwind, and continue eating. But for a bit the immediate surroundings of the umbrella acacia is protected against further attacks.

And not only do trees warn their tree friends about dangers; they also take care of sick and weak conspecifics with nutrients. For example, one time the author found a very old tree stump. Its insides had rotted a long time ago to topsoil, a clear sign that the tree had been felled over 400 years ago. But the wood on the outside of the stump was still living. How was this possible? After all, the stump didn't have its own leaves to do its own photosynthesis.

Well, the stump was nourished by its neighbors with nutrients from the root system, and had been for at least 400 years! There's no way for this stump to heal, but for trees which are only badly hurt, this system can save lives.

Why do trees do such a thing? It's simple: it's better together. Trees need the forest; it protects them from storms, provides the right microclimate and warns them of attacks. So they help each other out.

### 8. Lucky mushrooms: Trees intentionally work with fungi. 

We already saw that trees use fungi to spread information in the forest. The fungi do this via their mycelium — a network of thread-like filaments. But that's not all: trees and fungi work together on other levels, too.

For instance, they help each other procure water and nutrients, a collaboration that starts when a fungus lets a few filaments grow into the roots of the tree. Then the process gets started: the fungus helps the tree absorb more water. Sometimes the ground is rather dry, and the tree can't get enough water with its roots alone. The filaments of the fungus are much finer, allowing them to permeate more ground, drawing water and nutrients and passing them on to "their" tree.

In return, the fungi get sugar — produced through photosynthesis — from the tree. It's a good trade: trees that work with fungi store twice as much nitrogen and phosphorus — both important for life — than those without fungal friends.

This may sound perfectly harmonious. However, when the going gets tough, the fungi can resort to drastic, deadly measures. If, for example, the quantity of nitrogen in the ground sinks below a certain point, certain fungi can produce a poison that kills off all the microorganisms in the surrounding topsoil. These animals die and release the nitrogen that they stored in their bodies, so that it is available for the fungi and the tree.

So fungi can make life much easier for a tree. At the same time, they are also sometimes very dangerous, as we'll learn in the next blink.

> _In Oregon there is a fungus with a 9.2 km spread and a weight of about 600 tons._

### 9. That hurts! Trees protect themselves from injury. 

We've seen how trees warn each other when, for example, beetles, giraffes or, a bit closer to home, deer attack. The injuries that trees sustain during such attacks are painful — and so, quite naturally, they want to avoid them at any cost.

There are different types of injury that a tree can suffer. And a major inflictor of these injuries are, of course, animals: deer eat young shoots, woodpeckers peck holes into the trunk and bark beetles drill through the bark and eat almost all of the living wood.

Smaller animals can also do great damage. Aphids attach themselves to the leaves and drink out their fluid, which contains sugar. Unfortunately, this fluid, the blood of trees, contains so little sugar that the aphids have to drink a lot. And all that liquid has to go somewhere! Maybe you've parked under a tree that aphids are drinking from, and found your car covered in a sticky mess.

The other big danger is weather. A storm can break branches or split the trunk. Rain, snow and hoar frost also pose a threat. Extreme weight or cold can strain the limbs so severely that they snap off, leaving an open wound.

Trees have thus developed different strategies for dealing with these types of dangers. Spruce trees, for example, have arranged their branches so that, when strained by the weight of snow, they bend downward, virtually lying atop one another.

Trees luckily don't catch bacterial infections or viruses like we humans, but every injury includes the risk of a different kind of infection: fungus. As soon as the bark is opened, whether by a woodpecker or a broken branch, fungus can enter the tree. The tree tries right away to close the opening with new wood (we see the result of these efforts — a bulge in the bark — at the edges of holes left behind by fallen branches). Unfortunately, this isn't exactly a speedy process.

And if the fungus gets in, the tree, even if it manages to heal itself, won't survive for more than 100 years; once the fungus makes it inside, the wood starts to rot and the tree will inevitably, though maybe slowly, die.

### 10. Respect: Trees should be treated humanely, as animals are. 

The image most people have of trees is dated. Unfortunately, the forestry industry is also behind the times. Traditional forestry, practiced almost everywhere in Germany today, does a few things wrong.

Of course, in the forestry industry, it's mostly about producing wood. Foresters have long had a misconception: they thought that younger trees produce more wood faster than old trees. But this is untrue. Younger trees actually grow slower than older ones.

In most forests trees are felled when they are about 100 years old. But, for instance, beech trees aren't sexually mature until anywhere between the ages of 80 and 150. No functioning ecosystem can exist in forests that only contain a few types of trees that are supposed to be harvested so quickly. Many things that we learned in the last blinks simply don't work there. Trees don't build partnerships with fungi or warn each other of danger, for instance. This results in unhealthy forests that are prone to pests and are generally unproductive.

A natural forest is more productive. Also, a forestry industry that mimics nature produces both higher amounts of, and better quality wood. Let's think back to the example of that tree stump, the one that was taken care of for years by its neighbors. This kind of cooperation only takes place in naturally grown forests. Only there do organisms live in balance.

So we should cut down trees later, and, when we do, we should do it carefully and not just walk through the forest with chainsaws. Whoever understands that trees have a memory, that they can feel and live together with their children, won't be able to fell trees whenever it's convenient. This person will look for the trees that have fulfilled their duty in the ecosystem, those occupying a position that other trees can soon grow into and take over.

### 11. Final summary 

The key message in this book:

**Trees are undervalued organisms. They can do much more than is often realized: they communicate and help each other out, they have senses and are designed to fit well into their place in their ecosystem. We should show trees respect and care about their welfare, just as we do with animals.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Botany of Desire_** **by Michael Pollan**

_The Botany of Desire_ (2001) explores the complex and fascinating relationship between humans and plants. In these blinks, we'll see how plants manipulate humans by taking advantage of our four basic desires for sweetness, beauty, intoxication and control, and how, in turn, we help plants reproduce and even grow stronger.
---

### Peter Wohlleben

Peter Wohlleben studied forestry and, for more than 20 years, worked in forest management. He quit his job to start his own forest enterprise in Germany's Eifel region, where he championed alternative forestry and advocated for primeval forests. He has published multiple titles about forests and environmental protection, one of which is called _Der Wald — Ein Nachruf_ ( _The Forest — An Obituary_ ).

