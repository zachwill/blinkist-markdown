---
id: 57179eb93f35690007554dde
slug: the-hidden-wealth-of-nations-en
published_date: 2016-04-25T00:00:00.000+00:00
author: Gabriel Zucman
title: The Hidden Wealth of Nations
subtitle: The Scourge of Tax Havens
main_color: D12A2E
text_color: D12A2E
---

# The Hidden Wealth of Nations

_The Scourge of Tax Havens_

**Gabriel Zucman**

_The Hidden Wealth of Nations_ (2015) reveals the truth about the decades of deceitful business practices that have added to today's economic turmoil. Trillions of dollars worldwide go untaxed, and nations put the burden on innocent citizens, which only increases economic tensions. So what can be done to stop tax evasion and get corporations to start paying their dues?

---
### 1. What’s in it for me? Step into the murky waters of the world’s tax havens. 

Have you heard of tax havens? You probably have. Recently, there's been a parade of scandals, all having to do with the rich and their unsavory habit of trying to hide money from the government. And where do they hide that money? Tax havens.

Since their inception, tax havens have been a popular way for the affluent to avoid paying taxes. The issue is often depicted as a moral one. But there's a bit more to it than that. This practice actually removes money from the treasuries of governments, which could use it for other, more important things. So let's explore where the hidden wealth of nations really is, and what can be done to uncover it.

In these blinks, you'll discover

  * why new tax havens came into existence in the 1980s;

  * how we can make a good estimate of all the unreported wealth in the world; and

  * what FATCA is and how it can be a good model for combating tax havens.

### 2. The first tax havens appeared after World War I, and their numbers grew as time went on. 

As you can imagine, world wars are expensive. And after World War I, many European nations raised income taxes in an effort to rebuild communities and assist veterans. 

An extreme case is post-war France. In 1924, the country's previous top-tier income tax (two percent) had been raised to a staggering 72 percent.

Similar tax increases were popping up across the continent, and, as a result, wealthy Europeans devised methods for dodging these taxes — most commonly, by moving their money to other countries.

Switzerland was the most obvious tax haven. A neutral state during World War I, Switzerland was untouched by combat and had no reason to raise taxes, and it also had a well-established banking network with high interest rates.

By 1938, Swiss banks stored ten times more foreign wealth than in 1920. At the time, this amount accounted for 2.5 percent of the total wealth of European households and would equal about $130 billion in today's value.

Then in the early 1980s, tax havens began to proliferate.

When Prime Minister Margaret Thatcher came to power in the United Kingdom, she deregulated Britain's financial markets. This allowed cities like London and Hong Kong, as well as British territories like the Virgin Islands, to compete with Switzerland in the private wealth-management market.

Soon, these policies spread to other European nations like Ireland and Luxembourg, which gave people more options to choose from.

Despite of the competition, Switzerland's private banking sector continues to thrive. As of 2015, almost $2.3 trillion is still being held in Swiss banks, an 18 percent increase since 2009.

Remarkably, $1.3 trillion of that still comes from wealthy Europeans, representing a staggering ten percent of the total wealth of European citizens **.**

Now we know how tax havens got their start. In the next blink we'll learn how people redirect their money.

### 3. In 2014, there was an estimated $7.6 trillion hidden in tax havens. 

You may have heard about the financial crisis in Greece, which had racked up $350 billion in public debt. That's a disturbingly large sum. Even more disturbing, however, is the fact that, in 2014, $7.6 trillion of the world's money was being kept in tax havens. In other words, eight percent of our global wealth was hidden away.

Thought hidden, we can nonetheless track this amount through recorded assets and liabilities.

What does that mean? Well, let's take a look at how a French investor buys stocks in a German corporation like BMW.

German accountants would record the sale of BMW stocks to a foreign investor as a _liability_, which just means that BMW is liable to make payments to the French investor.

On the other hand, in France, this sale would be recorded as an _asset_, because BMW's payments would flow into the French economy and be taxed.

But here's the catch: If the French investor deposits these stocks into a Swiss bank account they will neither be recorded as an asset nor be taxed by France.

This creates a global imbalance: liabilities outweigh assets. Indeed, in 2014, this discrepancy reached a whopping $6.1 trillion. If we combine this with the $1.5 trillion in anonymous bank deposits, we hit $7.6 trillion being hidden away for the primary purpose of avoiding tax.

And this is a conservative estimate. It doesn't include high-value, non-financial items like jewelry, yachts or real estate purchased in tax-exempt locations.

Really, it's impossible to determine how much these non-financial items are worth. But even if we could, it's unlikely that the $7.6 trillion figure would significantly increase. 

After all, people who remain wealthy rarely invest their money in things like yachts or mansions. It's far more likely they make smart and secure investments, such as buying stock in a reliable company like BMW.

Perhaps this sounds harmless enough, but, in the next blink, we'll see how much damage it can cause.

### 4. Tax havens have a damaging impact on governments and citizens. 

If you're wondering how these tax havens affect the world, take a look at Greece's $350 billion debt and consider the fact that 4.5 percent of the nation's wealth was being tucked away in foreign banks. 

It is estimated that, in 2014, tax havens cost world economies an estimated $200 billion in revenue. Even though that figure represents around one percent of the world's total government revenue, these losses still have enormous effects.

Let's look at how tax havens can make a bad economic situation in Europe even worse.

Since the financial crisis began in 2008, economic growth has dwindled dramatically, leading to increasing debt. In 2014 alone, income hidden away in tax havens removed $78 billion in tax revenue for European countries.

This kind of large-scale tax fraud has a deep impact on European economies; governments end up making severe budget cuts that include defunding beneficial programs for middle and working class families, which makes it harder for these people to spend and put money back into the economy, which only adds to the problem of decreasing growth and increasing debt.

Meanwhile, the rich keep getting richer and continue to tuck ten percent of their earnings away in Swiss bank accounts.

For instance, in 2014, European personal wealth in Switzerland reached $1.3 trillion, a 20 percent increase since the financial crisis began — a huge amount of lost revenue that could have been used to prevent the budget cuts that hurt working and middle-class Europeans.

In France, for example, the past few years saw an estimated loss of $300 billion in tax revenues due to tax havens. This money could have boosted the current GDP by 15 percent, an enormous amount.

The funds could have been used to pay off public debt, decrease tax rates and help growth, ultimately reducing the gap between rich and poor.

In the next blink we'll see what can be done to prevent tax havens and dismantle economic inequality.

### 5. Most of the laws against tax havens have failed, but there is hope for the future. 

We've seen the damages that tax havens can cause. So what can be done? A number of initiatives have sprung up; unfortunately, however, most have been ineffective at best and complete failures at worst.

Essentially, most attempts at combating tax havens are simply not strong enough.

There have been noble efforts. At a G20 meeting in 2009, the representatives of the 20 major economies gathered to announce they had come to an agreement on combating tax havens. French Prime Minister Nicolas Sarkozy even declared it as the "end of tax havens."

In theory, it was a good idea: nations would be allowed to demand that banks in foreign countries reveal financial information about their citizens.

But it was a disaster in practice: to obtain this information, governments first needed to provide evidence to these tax havens that tax fraud was being committed. It's a catch-22. Even if tax evasion is suspected, it is extremely difficult to provide evidence without receiving the requested information. Therefore, tax havens rarely have to provide any information whatsoever.

The failure of the G20 plan is painfully obvious; since 2009, the amount of personal wealth stored in tax havens has increased by 25 percent.

This is likely due to the fact that the very politicians who are ostensibly against tax havens are actually taking part in the evasion. French Minister of the Budget Jérôme Cahuzac, who was leading the fight against tax evasion in France, resigned after his own offshore bank account in Singapore was discovered.

Fortunately, there are more recent measures that show more promise.

The United States has recently passed the _Foreign Account Tax Compliance Act_ (FATCA), which provides an automatic exchange of financial information between international banks and US tax authorities. It also enforces economic sanctions on any foreign banks that withhold information from US authorities.

But, while some countries have made progress, there is still a lot of work to be done.

### 6. To rid the world of tax havens, we need to implement effective and forceful solutions. 

So where does the fight against tax havens go from here? Can nations learn from past mistakes as well as take cues from plans that show promise?

The author suggests a twofold plan of action.

First, there should be a global FATCA — that is, all countries continuing to act as tax havens should face economic sanctions and trade levies.

Obviously, this is a tall order, requiring full cooperation between different nations, especially those in Europe and the G20. But it means that any foreign bank or nation refusing to comply with tax-fraud investigations would face financial penalties.

And even if those nations were able to avoid such penalties, they would still be met with trade tariffs on commercial goods and services that they rely on.

This strategy of trade tariffs underscores the importance of a truly international and cooperative approach.

For example, let's say that France imposes tariffs on Switzerland for refusing to release information. Rather than deciding to cooperate, Switzerland might take further offence and decide to close its borders to French tourists. However, if all of Europe stands behind France against Switzerland, the government would be much more likely to cooperate rather than retaliate.

The second step would be to ensure tax haven compliance. This would involve creating an international wealth database containing a detailed account of international ownership of all stocks and bonds, which would make it easy for international tax authorities to verify when banks are being honest and when they're withholding information about their clients.

As a bonus, this kind of database would also provide nations with better tools in the fight against money laundering and the financial activities of terrorist organizations.

### 7. To truly combat tax evasion, we must also look at the practices of multinational corporations. 

While the measures enumerated in the previous blink would be big victories in the battle against personal tax evasion, preventing corporations from sheltering their money is another matter. Why is it so hard to do away with corporate tax evasion? Well, most of the time, it's perfectly legal.

As a matter of fact, multinational corporations have a number of ways to avoid paying taxes.

The very nature of being a multinational corporation means that profits are earned around the world and the corporations can move this money anywhere they want, including tax havens.

Much of the problem has to do with how corporations handle transfer prices, that is, how much it costs one branch to buy products from another branch within the same multinational corporation.

For example, if Google or Microsoft wants to avoid the high corporate tax rate in France, they'll get a branch located in a tax haven like Ireland to sell them services at a better price. That way, the profits of the sale remain in Ireland, and they don't have to pay as much tax.

Such corporate tax-evasion methods end up costing the US government $130 billion per year in revenue, which is why we should completely rethink how we tax corporations in today's globalized world.

Again, we've seen G20 nations get together in attempts to tighten regulations on transfer prices, but this has gotten us nowhere, as corporations are usually at least two steps ahead. Instead, we need to think big and come up with a way to impose global taxes on global profits.

One way this could work is to think proportionately. 

For example, take a large multinational corporation like Apple: If 50 percent of their products are sold in the United States, then the United States should tax 50 percent of their profits.

This would make it very difficult for multinational corporations to evade their taxes. Apple would have to find a way to sell iPhones to every American customer from their branch in Ireland!

### 8. The Final Summary 

The key message in this book:

**On average, tax evasion costs global governments an estimated $200 billion per year. Approximately $7.6 trillion is being stored in offshore tax havens, accounting for eight percent of global household wealth. These practices harm society and add to increasing economic inequality. So far, initiatives to shut down tax havens have been unsuccessful, but nations could join forces and create effective and forceful measures and end the scourge of tax havens.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Treasure Islands_** **by Nicholas Shaxson**

_Treasure Islands_ offers insight into one of the darkest parts of the financial world: tax havens. It explains how wealthy people and corporations are able to avoid paying taxes by relocating their assets offshore. Tax havens are highly damaging to all but the tiny percentage of people who can afford to use them, and they contribute to the growing gap between rich and poor.
---

### Gabriel Zucman

Gabriel Zucman is a French author and economics professor at the University of California, Berkeley. He is also a frequent contributor to the _Quarterly Journal of Economics_.

