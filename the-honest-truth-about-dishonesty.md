---
id: 537c6d9d3465300007490000
slug: the-honest-truth-about-dishonesty-en
published_date: 2014-05-20T09:25:21.000+00:00
author: Dan Ariely
title: The (Honest) Truth About Dishonesty
subtitle: How We Lie to Everyone – Especially Ourselves
main_color: EC2F3C
text_color: CC2934
---

# The (Honest) Truth About Dishonesty

_How We Lie to Everyone – Especially Ourselves_

**Dan Ariely**

Using various experiments to uncover the hidden factors that drive us to cheat in everyday situations, author Dan Ariely finds that certain anticipated motivators — for example, money — actually don't play a crucial role in our dishonesty. At the same time, other, quite unexpected forces influence us very strongly — for instance, the social acceptability of cheating, and even our altruistic tendencies.

---
### 1. What’s in it for me? Understand the psychology of cheating to control it in yourself and in others. 

Most of us would like to believe that we're good, honest people, but unfortunately that's simply not true: we're all cheaters. 

While the majority of us may not be cheaters on a grand scale, every day we are motivated — usually irrationally — to lie, deceive and cheat in one way or another. And cheating is everywhere: in our schools, at the office, in the home and — in the form of self-deception — even in our own minds.

But why are we so dishonest? 

In _The_ _(Honest)_ _Truth_ _About_ _Dishonesty_, behavioral economist and author Dan Ariely investigates the many, often opposing forces which motivate us to cheat and lie — and to stay honest. Using many experiments designed to reveal the drivers and parameters of cheating — such as one's moral sense, physical and mental fatigue, temptation — Ariely presents a fascinating examination of dishonesty in its many contemporary forms.

In these blinks, you'll find out:

  * Why being reminded of the Ten Commandments can reduce the chances that you'll cheat.

  * Why knowingly wearing fake designer clothes will affect your overall moral behavior.

  * How cheating is contagious, transmitted like an infection from person to person.

  * Why committing one dishonest act leads to another.

  * And why, if we cheat, we're likely to trick ourselves into believing that we didn't.

### 2. There is no shortage of lying, cheating and corruption in our society today. 

Do you think that just a "few bad apples" are responsible for most of the cheating in the world, or that the problem is actually more widespread?

Unfortunately, the correct answer is the latter: We _all_ cheat.

Take, for example, a gift shop located in a Washington DC arts center, which was run like a traditional "lemonade stand," using just a cash box instead of a cash register.

Although business was good, each year $150,000 would go missing.

The organization searched for the thief, eventually finding and dismissing the employee they believed was stealing the money.

That should've been the end of the matter. However, money continued to disappear.

Finally, the organization decided it should manage the shop more strictly, setting up an inventory system with price lists and sales records.

It worked: from that point on, no more money went missing.

So, who stole all that money?

It turned out that, all along, the problem was not just one thief, but many well-meaning volunteers who each took just a small amount of cash.

Of course, cheating sometimes occurs on a much grander scale. Consider the example of Enron, one of the largest firms in the United States, which had achieved its massive success by means of a series of "creative" accounting techniques.

Essentially, Enron's employees had been "cooking the books" or lying about their revenues and profits. Moreover, they were assisted by consultants, rating agencies and Enron's board of directors, all of whom turned a blind eye to the deception.

As the lies spread throughout Enron, the deception intensified. Ultimately, when the truth was made public, the company collapsed.

If it were true that just a "few bad apples" were responsible for most of the cheating in the world, the problem might be fairly easy to solve. However, as we'll see in the following blinks, most of the world's deception is actually made up of many different and subtle kinds of dishonest acts that each of us regularly practice.

### 3. Cheating is not based solely on rational thinking. 

How do we decide whether or not to cheat or lie in a specific situation?

According to the prevailing notion of cheating, the cheater or liar makes a rational decision based on the following factors: 

  * How much can I gain from the crime? 

  * What is the probability of getting caught? 

  * If I _am_ caught, what's the expected punishment? 

By weighing benefits against costs, people decide whether or not to cheat.

In the real world, however, when it comes to cheating, people don't act in such a rational way.

Firstly, the chances of cheating do not increase in accordance with the potential gains.

Consider this experiment, conducted by the author: two groups of participants were asked to solve similar mathematical problems. For every one solved, the participants would receive 50 cents.

The first group, where the participants' written answers were checked by a facilitator, solved four out of 20 problems correctly, on average. The second group, whose work went unchecked, claimed to have solved six problems on average.

In other words, the second group cheated. However, the amount of cheating did _not_ increase when participants were promised up to $10 for each correct answer.

Secondly, the probability of getting caught isn't as big an influence on our decision to cheat as we might think — as the author demonstrated in a variation on the above experiment.

This time around, there were three different groups and conditions.

In the first, participants were allowed to shred half of their worksheets before handing them in and getting paid.

In both the second and third groups, the entire sheet could be shredded, but in the latter group the participants were also instructed to _pay_ _themselves_ from a large bowl of cash.

The result?

Of course, many participants cheated. However, the amount of cheating was the same across all conditions. In other words, the odds of being caught had no influence on the participants' decision to cheat.

### 4. Our own morality is connected to the amount of cheating we're comfortable with. 

As we've seen, people don't necessarily cheat more just because they're given the opportunity.

So, what stops us from cheating as much as possible?

In short: our own morality.

People usually have to handle two opposing impulses: to get ahead by dishonest means and to consider themselves to be good, honest people.

Consider this scenario: the author invited an accomplished business consultant — actually a comedian in disguise — to come and speak to his students. The guest proceeded to offer the students many tips for how to succeed through cheating.

By the end of the lecture, the students were impressed by the advice they'd heard, but couldn't help feeling disturbed by the "consultant's" explicit recommendation to cheat.

On the one hand, the students found the advice rational, and tempting. On the other hand, something was preventing them from agreeing wholeheartedly with it. Even though people might want to cheat, their sense of morality tells them not to go through with it.

Moreover, if we're reminded of the expected ethical standards before we're tempted to cheat, our capacity to cheat is further diminished.

As an example, consider the math experiment described in the previous blink, a variation on which was conducted by the author under two new conditions.

Before the math test begins, the first group is asked to recall the Ten Commandments, while the second is instructed to recall ten books they studied in high school. Afterwards, both groups are lured into cheating on the math test.

When the author conducted this experiment, he observed the usual, moderate cheating in the second group. But in the first group, no cheating took place at all.

Why?

The moral code they were reminded of beforehand had influenced their behavior, making them reluctant to cheat.

So, on the one hand, we desire the benefits that come from cheating, and, on the other, we're afraid of behaving immorally.

But how do most people resolve this dilemma? As we'll see in the following blinks, they use methods of rationalization and self-deception.

### 5. Cheating results from a process of rationalization and self-deception. 

Is it possible for us to reap the rewards of cheating while simultaneously viewing ourselves as honest people? In fact, the answer is a definitive "YES!" — we're especially creative when it comes to justifying our dishonest behavior.

Let's return briefly to the math test experiment. Initially, participants were put in charge of checking their own test results, which led them to cheat a little: to improve their results, they replaced their wrong answers with the correct ones.

Afterwards, the participants were asked to predict how well they could solve the same math problems if they were unable to check their own results.

The participants predicted that their level of mathematical ability would be the same, whether or not they were able to cheat. In other words, they'd deceived themselves into believing that the ability they displayed when cheating was their _actual_ ability.

Another way we deceive ourselves with regards to our dishonesty is that we'll accept our own cheating more easily when there are "steps" between ourselves and the dishonest act.

In other words, the larger the psychological distance between ourselves and the action, the more easily we forgive ourselves for cheating.

For example, we all know that we shouldn't steal money. However, stealing products that were purchased with money seems to be less problematic because there's a psychological distance between the act itself and the money involved.

Take, for example, the following experiment conducted by the author. In a student house, a six-pack of Coke is placed in one refrigerator and several $1 bills in another.

In both cases, the students would've known that the money and the Coke belong to someone else, so they were off limits. 

The result? While the money remained untouched in the refrigerator, every can of Coke was stolen.

The reason? There was a sufficient distance between the thief and the dishonest act.

The authors calls the balancing act between the conflicting motivations to succeed and to act morally _cognitive_ _flexibility_. In contrast to any rational cost–benefit analysis, this flexibility is the main _internal_ driver behind our dishonesty. In the next blinks, we'll turn to look at the _external_ factors which make us prone to cheat.

### 6. Exhaustion makes us more prone to lie and cheat. 

Imagine coming home after a grueling day at work. You're exhausted, grumpy and starving — it's a take-out night, for sure.

Why do we tend to opt for unhealthy fast food after a tiring day?

After we use our brains intensively — causing so-called _cognitive_ _strain_ — we're more easily tempted. This was demonstrated in the following experiment.

A group of people are briefly shown a two-digit number, then asked to memorize it, leave and recite the number in another room.

On the way, they pass a cart offering chocolate cake and healthy-looking fruits, and are told that they can eat either one after they have recited the number. But there's a catch: they have to make their choice _now_.

The same procedure is repeated with a second group, with a crucial difference: this group are given a _seven-digit_ number to remember.

The result?

Each group's choice of snack depended on the number of digits they had to keep in mind. Those participants who had to remember the seven-digit number more frequently chose chocolate cake.

But cognitive strain not only increases the likelihood of succumbing to temptation; it also causes people to cheat.

Take, for example, this experiment: Participants are divided into two groups. The first is instructed to write a short essay without using the letters "a" and "n" — a very tricky task — and to then solve a math problem.

The second group is also asked to write an essay without using "x" and "z" (a relatively easy task). They also have to solve a math problem.

Since both groups were allowed to shred their worksheets before reporting the number of questions they answered correctly, the opportunity for cheating was great.

In fact, both groups _did_ cheat — but the amount of cheating in the first group was three times as high as in the second group.

### 7. The simple act of wearing fake designer clothes can make us cheat more often and distrust others. 

At some point in our lives, a significant number of us have worn fake designer clothing. However, which of those people would've guessed that this apparently insignificant act of dishonesty could affect them in unexpected, negative ways?

In fact, experimental evidence shows a correlation between wearing fakes and the likelihood of performing other dishonest acts.

In one experiment, three groups of participants were given designer sunglasses. The first is told that the glasses are authentic, the second that they are fakes, and the third is given no information as to the glasses' authenticity.

Then participants had to take a math test where they were given an opportunity to cheat.

The result? According to the amount of cheating evident in the third group (who were given no information about the glasses), the average level of cheating was 42 percent.

However, in the other groups the results were quite different: In the first group, the positive self-image engendered by the participants' belief in the glasses' authenticity meant that just 30 percent cheated on the test. But in the second group, the negative effect of wearing fakes was so significant that an incredible 74 percent of the participants cheated.

As this shows, committing one dishonest act (in this case, wearing fake designer sunglasses) increases the chances of committing another.

Not only that, but wearing fakes can even make us more suspicious of others.

In another experiment, the author again provides three groups with sunglasses under the same three conditions as before.

After having enough time to enjoy their sunglasses, the groups are asked to complete a survey evaluating the morality of other people.

The result was that, in comparison with the other groups, those participants who knew they were wearing counterfeit glasses judged their acquaintances as more likely to act dishonestly.

### 8. The more socially acceptable cheating appears to be, the more we're likely to cheat. 

So far, we've looked at the self-deception behind an individual's dishonest behavior. But what about the social aspects of cheating? Can dishonesty be transmitted from person to person?

In fact, dishonesty can spread via the process of _social_ _contagion,_ through which similar behavior spreads among individuals.

This is shown in the following experiment: participants are split into groups and set a math test under differing conditions. For each problem they solve correctly, the participants receive money.

One group is given the opportunity to cheat: the participants' completed worksheets are shredded and they simply report the number of math problems they solved.

In another group, however, a social element is introduced: soon after the test begins, a (fake) participant rises from her seat and exclaims: "I've finished! I solved everything!"

Although it's obvious that she cheated, the participant is paid in full in front of the whole group.

The result?

While participants in the former group do cheat a little, those in the latter group cheat much more: the participants claim to have solved about twice the number of answers they actually got right. By witnessing a single person getting away with cheating, the other participants' tendency to cheat increased dramatically.

Furthermore, it's not always true that being in a group limits the likelihood of cheating — as can be seen in the next experiment.

In groups of two, participants collaborate on a math test. If the partners just observe each other, cheating doesn't occur. But if they're also given the freedom to talk, the participants learn that they could mutually benefit from acting dishonestly — thus the groups end up cheating.

Why?

There are two forces at work here: When the participants only watch each other, they're effectively _supervising_ one another, which limits cheating. Yet, once they're able to converse, _altruistic_ _tendencies_ emerge: cheating increases among participants when team members can benefit from their dishonest behavior.

### 9. We can curb cheating only by understanding exactly why people behave dishonestly. 

Now that you've learned all about dishonest behavior, how can you use this knowledge to keep such behavior under control?

First, you have to take into account the psychology of cheating. Take, for example, the following situation.

A woman realized that her maid had been stealing meat from the freezer every couple of days. What did she do?

Her first act was to put a lock on the freezer. Then she told the maid that she suspected some people who occasionally worked at the house, and therefore only the maid and herself should be keyholders. Also, she gave the maid a small raise to compensate for the increased responsibility.

Did her approach work? Yes.

Why? There are several reasons for her plan's success, and all of them are connected to the motivations for cheating presented in the previous blinks:

1\. By locking the freezer, the woman curbed the maid's temptation to cheat.

2\. By targeting the responsibility of the maid she caused the maid to feel closer to the dishonest act. Having to consciously use the key rendered the act of stealing more difficult to justify.

3\. By entrusting the key to the maid, the woman established honesty as the social norm in her household.

Another way to limit dishonest behavior is to decrease the temptation to cheat.

How?

This can often be achieved by reducing the _conflicts_ _of_ _interest_ in situations where people are rewarded for acting in opposition to their agreed role.

For example, the role of a doctor is to take care of patients' physical health. Yet in some cases doctors receive a bonus from pharmaceutical companies whenever they prescribe their drugs to patients.

The result is a conflict of interest, where the doctor is tempted to cheat by prescribing unnecessary drugs to patients.

In order to lessen the doctor's temptation, the conflict of interest should be removed: doctors should be prohibited from receiving money from pharmaceutical companies.

### 10. Final Summary 

The key message in this book:

**Cheating** **is** **a** **widespread** **phenomenon.** **Surprisingly,** **most** **of** **the** **drivers** **of** **our** **dishonesty** **are** **not** **rational** **ones,** **as** **we** **might** **expect,** **but** **irrational** **ones.** **By** **learning** **about** **the** **psychology** **of** **cheating,** **we** **enable** **ourselves** **to** **control** **dishonest** **behavior,** **both** **in** **ourselves** **and** **in** **others.**

Actionable advice:

**Analyze** **your** **own** **motivations.**

The next time you catch yourself cheating, try to analyze your motivations. Ask yourself: "Why did I cheat?" Did you act rationally for your own selfish interest? Or can you think of another reason? Learning how to catch yourself in the moment of cheating, and discovering your motivations for lying, will be a great help the next time you find yourself tempted to cheat.

**Rest** **before** **you** **get** **tired.**

One external factor in cheating is mental and physical exhaustion. In this state, we're more likely to give into temptation, cheat and lie. It's important, then, that — where possible — you don't let yourself become over-tired. Instead, make sure you take regular breaks, and rest _before_ you end up exhausted and likely to cheat.
---

### Dan Ariely

Dan Ariely is professor of psychology and behavioral economics at Duke University. His main field of interest is the psychology of irrationality. In addition to _The (Honest) Truth About Dishonesty_,Ariely is the author of two other international bestsellers: _Predictably Irrational_ and _The Upside of Irrationality_.

