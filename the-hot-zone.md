---
id: 54c64a6837323900090e0000
slug: the-hot-zone-en
published_date: 2015-01-26T00:00:00.000+00:00
author: Richard Preston
title: The Hot Zone
subtitle: The Terrifying True Story of the Origins of the Ebola Virus
main_color: F15030
text_color: B23B24
---

# The Hot Zone

_The Terrifying True Story of the Origins of the Ebola Virus_

**Richard Preston**

_The Hot Zone_ offers an intense, often terrifying account of the real-life events surrounding one of the world's most deadly diseases, Ebola. The book explains not only how the virus spreads, but where it came from and how it may continue to play a role in the future.

---
### 1. What’s in it for me? Discover how the deadly Ebola virus has set modern society on edge. 

The threat of Ebola is serious and real. Whenever there is an outbreak of this viral disease, the international alarms go up: borders are shut, travelers are scrutinized, isolation wards are readied.

What is it that makes this virus so terrifying? Ebola combines two deadly characteristics, in that it spreads easily and quickly, and kills a large proportion of those it infects.

These blinks give you the history of Ebola, from early outbreaks to recent research to stem its spread. Yet while concerns of a pandemic shouldn't be discounted, our ability to isolate Ebola and keep it from doing its worst has improved the more we have learned about the virus itself.

In the following blinks, you'll discover

  * why you should never fiddle with a test tube if you don't know what's inside;

  * how Ebola can live on even when its host has already died; and

  * why sitting in a cramped airplane cabin makes you a sitting duck for a virus.

### 2. The Ebola virus belongs to a family of viruses. Each acts differently, depending on its form. 

At the end of 2014, the deadly virus Ebola had claimed many victims in Africa and caused panic across the world. Yet how much do you really know about the virus itself?

Let's first look at a few basics.

The virus commonly called Ebola belongs to a family of viruses called _filoviruses_ ; _filo_ is the Latin word for "thread." Scientists named this family of viruses in this fashion as under a microscope, the virus resembles a short thread, or filament.

There are a number of different viruses in the filovirus family. Some of the major ones include:

  * Marburg virus. This virus was named after a city in Germany where an outbreak occurred, causing the death of a number of factory workers.

  * Ebola Zaire. This virus is the most-aggressive known strain of Ebola, which has been found to kill some 90 percent of people who are infected with it.

  * Ebola Sudan. Another strain of Ebola that is less aggressive, but still very dangerous.

  * Reston virus. This is an Ebola strain that infects monkeys, but not humans, and was discovered among laboratory monkeys in Reston, Virginia.

Each filovirus acts differently, depending on how it has developed and mutated. For example, the Marburg virus can infect humans, while the Reston virus so far does not.

Interestingly, the Marburg virus, which was discovered in the 1960s, is not transmitted by air, unlike more recent strains of Ebola, which may be able to spread via air in certain circumstances.

To be sure, the filovirus family includes some highly contagious diseases. Yet what makes Ebola uniquely dangerous?

### 3. Ebola is deadly, quickly destroying the body by liquefying organs and causing much blood loss. 

Ebola was first discovered in humans in 1976, near the Ebola River in what is now the Democratic Republic of Congo. Since then, various outbreaks have shown just how deadly the virus can be.

The virus essentially attacks the body's cells, working to dissolve tissue and even entire organs.

Scientists who examined the liver of a recently deceased victim of Ebola, for example, found that the organ had turned yellow and was partially liquified — as if the person had been dead for weeks.

Ebola victims also lose a lot of blood in the final stages of the illness, and frequently die by simply bleeding out.

The virus also liquefies parts of the brain, thus changing the behavior of its victims and permanently shutting down important brain functions.

With such devastating symptoms, Ebola can quickly claim the lives of those it infects.

In 1967, in the German city of Marburg, a viral outbreak claimed the lives of seven factory workers, with a total of 37 workers infected. This was one of the earlier instances of the Ebola virus. It's likely that the workers contracted the virus while producing vaccines from kidney cells, which were taken from African green monkeys.

In 1976, a second outbreak occurred in a remote area near the Ebola River, with a large number of fatalities from the disease. As the virus spread, fear spread as well — and people stopped bringing in provisions and medicine to the affected area, essentially cutting the area off from the rest of the world, and practically ensuring that those infected would receive no help.

An outbreak of Ebola among primates in a laboratory received much media attention in 1989. A shipment of monkeys to a laboratory in Reston, Virginia — close to the nation's capital of Washington, D.C. — turned out to have been infected by an Ebola virus.

While this particular strain of Ebola affected only monkeys, the fear of Ebola's deadly effects had now become contagious on a worldwide level.

### 4. The Ebola virus is so contagious that it can be spread through just a small drop of infected blood. 

The Ebola virus is not only extremely deadly but also spreads quickly and efficiently.

Part of the virus's effectiveness involves its _incubation time_, during which an infected person can spread the virus to others before noticing any disease symptoms.

It takes seven days from infection until the first Ebola symptoms begin to appear, such as a headache. Before this time, an infected person could easily spread the virus unknowingly.

What's more, for a short period of time the Ebola virus remains contagious even after an infected victim has died.

Researchers discovered this while examining the bodies of Ebola victims who had recently died. When they mixed the deceased person's cells with uninfected test samples, the still-active virus quickly infected the test samples.

Another reason Ebola is so dangerous is the way it spreads. The virus is transmitted through bodily fluids, such as saliva, blood or feces. As Ebola symptoms include heavy bleeding and vomiting, people who tend to Ebola patients are particularly at risk of contracting the virus too.

Ebola victims sometimes produce what is called black vomit, a mixture of bile and blood which is highly infectious. Victims too can experience seizures, which some doctors have observed can also spread the virus, with infected blood and other bodily fluids haphazardly spread across a wider area.

Chillingly, just a single drop of infected blood can contain up to 100 million viral particles, which is more than enough of the virus to successfully infect another person.

Yet Ebola's damaging effects aren't just limited to the symptoms caused by viral infection.

### 5. Working with Ebola can leave you with mental scars, even if you don’t actually contract the disease. 

So far we've seen how Ebola affects the body. Yet the psychological damage of dealing with Ebola — even if you've never contracted the virus — cannot be discounted.

It's hard to find good scientists willing to work with those stricken with Ebola, as many are simply afraid of the consequences if they too contract the disease.

Those who do work with Ebola, however, have found it terrifying. Many researchers have reported panic attacks and nightmares after coming in contact with Ebola patients.

Researcher Eugene Johnson suffered constant nightmares, waking himself in the middle of the night with his own screaming amid fears of a widespread outbreak of the disease.

In Reston, researcher Paul Jahrling worked around the clock with little time to sleep to contain and understand the outbreak in the lab's monkey population. Eventually he was so mentally and physically exhausted that he fell asleep inside a decontamination shower.

What's more, if you are suspected of being infected with Ebola — which can easily happen if you work with Ebola samples on a daily basis — the necessary isolation during quarantine can take a toll on your mental health.

Some medical facilities are equipped with small isolation area called _a slammer_, where patients are forbidden from having any contact with outsiders. They are visited only by doctors in hazmat suits, and must stay there until every risk of an infection can be ruled out.

This process can take weeks. Some patients become paranoid, believing that the whole world has forgotten them; sometimes isolation ward patients even try to escape!

Ebola is a contemporary virus that clearly poses a risk to certain populations. Yet where did it come from, exactly? The following blinks will examine the history behind this deadly contagion.

> _"To mess around with Ebola is an easy way to die. Better to work with something safer, such as anthrax."_

### 6. The curious deaths of tourists who visited an African cave led researchers to Ebola’s possible source. 

Although there have been a few Ebola outbreaks in different parts of the world, it appears that some strains that have jumped from animals to humans may have developed in a single place.

The question that scientists now grapple with is: Which place?

Some evidence shows that a form of Ebola may have been first contracted in the Kitum Cave, located in Mount Elgon National Park in Kenya. In 1980, a Frenchman named Charles Monet visited the cave shortly before falling ill and dying from Ebola. Peter Cardinal, a young Dutch boy, contracted Ebola and died after visiting the _same_ cave with his family in 1987.

Sensing a pattern, researcher Eugene Johnson organized an expedition to conduct research inside the cave, yet his research came up with no concrete results.

The Kitum Cave is home to various animals and spores, and is covered in bat guano, believed to be able to carry the Ebola virus. In the cave itself, many sharp rocks present an opportunity for cuts or scratches, thus giving the virus a potential opportunity to enter a victim's bloodstream.

While there is no concrete proof that the Kitum Cave was the source of Ebola, scientists and researchers nonetheless theorize that it is possible the virus did first spread to humans from here.

Another possibility is with animal holding facilities, such as the one in Reston, Virginia, where monkeys are stored in close quarters. In such an environment, it is almost too easy for a virus to jump from one monkey to the next, infecting many animals in a short time.

Such facilities also can house many different species of monkey, giving a virus ample opportunity to adapt itself and infect other species. This could have led eventually to the jump from ape to human.

Yet if Ebola originated in one place, how exactly did it spread so far and wide?

### 7. Sloppy medical practices help spread viral infections. Airplanes and modern roads do too. 

To thrive, viruses such as Ebola need to continually find new hosts. Our modern society unfortunately makes it all too easy for a virus to spread from place to place and person to person.

The close quarters of traveling by airplane is an ideal environment for the transmission of viruses. If your neighbor is sick and vomits, for example, your chances of infection are fairly high.

Moreover, the speed and breadth of air transportation networks mean that, once a virus is onboard a flight, it can be transported to literally anywhere on the globe.

Hospitals as well offer viruses ample opportunity to spread. Sick patients come into constant close contact daily with others, such as doctors, nurses and relatives.

Crucially, sloppy medical practices can assist in the spread of Ebola. The first cases of Ebola Zaire appeared in 1976 at a hospital in which only five needles were used to give all the day's injections. As one needle was used on an infected person, the virus was then spread easily among other patients.

Modern transport infrastructure too plays a huge factor in the spread of viruses. The construction of the Kinshasa highway, for example, which is one of the most important travel corridors in Africa, played a huge roĺe in the spread of Ebola.

This paved road allowed travelers to drive much more quickly from one place to another, giving the virus the opportunity to travel and spread between towns during its symptomless incubation period.

Previously, the time it took to travel between cities was much longer, which would give a potential victim time to develop symptoms, and thus ideally halt travel before the virus could spread further.

### 8. Basic foolishness and negligence are often responsible for spreading the Ebola virus. 

With a virus as deadly as Ebola, you'd think that people would take the utmost caution when dealing with it. Yet simple negligence and incompetence have been also directly responsible for its spread.

Researchers Tom Geisberg and Peter Jahrling may have just been curious when they decided to sniff inside a test tube that contained an unknown virus. To their horror, they later learned that the unknown virus was Ebola!

Luckily, the Ebola strain was actually one that was unable to spread to humans.

A tragic case of negligence took place in 1976 when a nurse named Maynga, who after she knew she was infected with Ebola, walked around the Congolese city of Kinshasa.

Afraid that her coworkers would learn of her infection, she decided to visit two other hospitals, and in doing so came into close contact with many people, exposing them to the virus.

Often people don't take the necessary safety precautions despite the well-known risks of Ebola exposure.

During the Ebola outbreak at the lab in Reston, Virginia, a military team wore appropriate biohazard suits before entering the facility, due to the high risk of infection. Yet they were surprised to see that inside, some of the employees were only wearing respirators.

Why? They had simply not been informed about the risk.

The trade in monkeys also brings with it many safety risks. Such facilities that house many animals at once are at risk of spreading disease; what's more, some unscrupulous traders knowingly sell sick monkeys.

We have seen how humans (sometimes foolishly) help Ebola to spread. Yet can we learn from past mistakes?

### 9. More advanced disinfection technologies and better biohazard suits have helped stem Ebola. 

Let's start off with some good news. Despite Ebola's virulent nature, we still have a good chance of fighting the virus and containing outbreaks.

New research has led to new discoveries that can help us better protect ourselves against the virus. Indeed, in recent years we've seen a steady improvement in the quality of equipment and medical suits used in quarantine situations.

For instance, the design improvements to equipment and medical suits for the expedition into the Kitum Cave made it possible for scientists to safely explore a remote and potentially Ebola-infected cave without serious risk. The same suits were used during the Reston crisis as well. 

Quarantine methods too have improved. After the infected monkeys were disposed of in Reston, teams disinfected the entire building with formaldehyde gas, a strategy that can completely destroy even the most resilient microbes.

Some cultures have employed home-grown strategies for dealing with viruses and containing outbreaks. For example, in some African villages, such as those near the Congolese town of Bumba, villagers have isolated themselves from the outside world during Ebola outbreaks by blocking roads to the village with logs, to prevent strangers from entering.

Sometimes luck plays a part, too. Researchers Tom Geisberg and Peter Jahrling were certainly lucky that the Ebola virus in that test tube wasn't contagious to humans.

### 10. Whether from Ebola or another virus, the threat of a pandemic remains. 

Now, the bad news. There will always be a threat of an Ebola pandemic, as there is still too much we don't know about this virus itself.

We know that the virus is incredibly resilient, able to mutate and adapt to its surroundings, making it far more dangerous than many other viruses.

And we know that so far, we've been lucky. But we can't be sure that we will always be, especially considering the virus's propensity to mutate.

There is mounting evidence that some forms of the Ebola virus can spread through the air. Eventually, there could be a strain of Ebola that evolves to spread as quickly as a common cold.

And despite our best efforts to prevent infection, there is always the possibility of a security breach where the virus is accidentally released into the wild.

For example, during the operation in Reston, Virginia, the military crew wore the latest-technology hazmat suits to protect themselves. Even so, one soldier discovered a hole in her suit while inside the infected building — an oversight that could have easily led to infection.

And finally, the lack of information about Ebola makes it difficult to predict how the virus will evolve.

This is especially true regarding how people become infected. There have been some cases where individuals have contracted Ebola without ever having had direct contact with an infected person. Meanwhile, other people have cut themselves with a scalpel covered with infected blood, yet never showed any signs of infection. Scientists simply can't explain why this is the case.

And because there is still so much that we don't know, we are still very much at risk of being unable to completely fend off a serious epidemic.

We should never forget the risks. Ebola is still one of the most dangerous and aggressive viruses in history. The danger of a pandemic will never truly disappear, but we still need to do our best to be prepared.

### 11. Final summary 

The key message in this book:

**The Ebola virus is among the most aggressive and dangerous diseases of our time. While we have so far been both resourceful and lucky in preventing a full-blown pandemic, inevitable blunders and the nature of the virus mean that an Ebola catastrophe on a global scale can never be fully ruled out.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Richard Preston

Richard Preston is an American author and journalist best known for his non-fiction writing, including titles such as _First Light_, _The Cobra Event_ and _The Demon in the Freezer_.

