---
id: 5444b35f6161330008170000
slug: the-hour-between-dog-and-wolf-en
published_date: 2014-10-20T00:00:00.000+00:00
author: John Coates
title: The Hour Between Dog and Wolf
subtitle: How Risk-Taking Transforms Us, Body and Mind
main_color: 9F2027
text_color: 9F2027
---

# The Hour Between Dog and Wolf

_How Risk-Taking Transforms Us, Body and Mind_

**John Coates**

_The Hour Between Dog and Wolf_ is an illuminating look at the influence of human physiology on the thinking and behavior of stock market traders. The author, inspired by his experiences on the trading floor, investigates the hormonal basis of financial decision making, and demonstrates the way in which the body's mechanisms can destabilize our financial markets. The book also explains what can be done to reduce the detrimental effects of our biology on the market, and even how we can use this knowledge to our financial advantage.

---
### 1. What’s in it for me? Learn how to think with your body on Wall Street. 

Besides a whole lot about stock markets and a bunch of clever trading tips **,** what can you learn from a former Wall Street trader? Perhaps you'd be surprised to discover that by analyzing the behavior of stock-market traders, you can learn how your hormones influence your everyday life and how to improve the way that you deal with stress.

Written by a trader-turned-neuroscientist, _The Hour Between Dog and Wolf_ aims to explain the bubbles and crashes of the stock market in terms of the central role played by our physiology and biochemical processes. These blinks contain the key lessons of the book, which — as you'll soon find out — are not only relevant to making your fortune on the stock market but to _all_ areas of your life.

In these blinks, you'll learn

  * how intuition is as much a product of the body as it is of the brain;

  * that we make decisions to act _after_ our body has already performed the action;

  * why men and women differ greatly in their stress responses; and

  * why hiring more women and older men to work on the trading floors is likely to decrease the risk of a market bubble and crash.

### 2. You think with your whole body, not just your brain. 

When you think about the connection between your brain and your body, how do you picture it? Do you imagine your brain as somehow separate from the rest of your body — a kind of control center that resides in your head, steering the body and causing its every action?

If so, then you're in for a surprise: thinking involves the _whole body_, not just the brain.

For instance, our thoughts and actions are influenced by hormones that stem from different regions of the body.

Take Ghrelin, a hormone secreted by cells lining the stomach whenever it's empty. Release of the hormone causes the brain to register hunger, prompting you to raid the refrigerator for something to eat.

But you're not a slave to Ghrelin. If you have strong reasons for not eating — e.g., a religious fast, hunger strike or a diet — you can effectively ignore Ghrelin's signals. But this is true only up to a point: over time, the hormone acts somewhat like a lobby group, its signals becoming louder and harder to ignore.

As this shows, certain body regions influence the processes in your brain — in this case, registering hunger. And this can be further demonstrated by looking at the way that the gut interacts with the brain.

Whenever we're stressed, the brain will inform the gut that the body is facing an imminent threat, and must therefore prepare for it. Upon "learning" this, the gut will halt the digestion process, in order to save energy for either fight or flight. Stress, in other words, is processed by both body and brain.

It works the other way around too: an overly sensitive gut can influence our thinking. In fact, sufferers of Crohn's disease, a type of inflammatory bowel disease, are more sensitive to emotional stimuli. So, if sufferers are exposed to emotive images — such as a distressed child — they're more likely to become emotionally aroused than healthy people are.

### 3. Testosterone has a massive influence on our behavior, especially by inclining us to take risks. 

Testosterone is considered one of the body's most powerful hormones because it has a major influence on our behavior.

If you're facing a challenge such as an athletics tournament the body releases testosterone, enabling you to compete for prolonged periods. It accelerates our metabolism and the rate of cell growth, increases our physical strength, lifts our mood and heightens our cognitive performance.

But high levels of testosterone also make us more prone to risk taking. In a competitive environment — such as the stock market trading floor — the impulse to take risks can be a destructive one.

In the author's examination of the influence of traders' testosterone levels on their skill and risk taking, he found that high testosterone levels didn't increase skill, as skills can be increased only through experience, but it increased the amount of risks traders took, compared with individuals with lower testosterone levels.

While risk-taking can often pay off, that success will prompt the body to release even more testosterone. This feedback loop is known as the _Winner Effect_, and it's a process that can result in extremely reckless behavior.

This effect has been observed in animals, too. After a fight, the victorious male experiences a spike in testosterone, increasing his testosterone levels for the next fight, while the loser's testosterone levels fall, causing him to avoid fights in the future.

The high testosterone levels in the winner will, after a while, transform him. The French even have a name for this moment of transformation: "the hour between dog and wolf" — that hour where the light is so dim that we can't know whether we're safe or threatened.

Transformed, the victor becomes overconfident and engages in reckless behavior.

Animals' high testosterone levels can lead them to win many of their battles, but, since they are more willing to take too dangerous of risks, they are also likely to die younger.

### 4. Since our physiology is slow and automatic, our brain anticipates movements and our consciousness acts like a bystander. 

Have you ever wondered how you're able to catch a fast-moving ball so quickly and accurately, even when there's little time to make a conscious effort? That's because our brain _anticipates_ such movements, since our physiology is too slow for us to see things in real time.

Due to the structure of the human eye, we experience the external world with a delay of one-tenth of a second. This 100-millisecond delay between the throwing of a ball and our registering that movement means that the brain is forced to anticipate the position of the ball in mid-air.

This phenomenon was demonstrated by an experiment in which participants were instructed to look at a screen that showed a circle made of two color bands — blue on the outside, yellow on the inside. The inner yellow circle, however, flashes on and off.

Then, this blue-and-yellow circle is moved around the screen. But the observer doesn't see a travelling blue-and-yellow circle; he sees a moving blue circle with a yellow one lagging behind.

Why? Because the brain can anticipate the location of the blue circle, but can't anticipate that of the flashing yellow one. This experiment demonstrates the way that the human brain anticipates the location of moving objects, rather than seeing that movement in real time _._

Also, since catching a ball and many other reactions are automatic, our consciousness appears to be a mere by-product of our actions.

In the 1970s, physiologist Benjamin Libet performed an experiment in which he measured the electrical activity in his participants' brains, and instructed them to lift one of their fingers.

The participants' brain region responsible for _preparing_ the action of lifting a finger was active 300 milliseconds _before_ the region responsible for making the _decision_ to lift a finger was engaged.

This suggests that conscious decisions come _after_ our brain has prepared for an action. In other words, our consciousness acts much like a bystander, observing a decision already taken.

### 5. Traders learn to predict the markets through their automatic physical reactions. 

Often, even if we cannot explain how we know something, we still somehow _feel_ that it's true. This is perhaps the reason that people tend to think of their intuition as a kind of supernatural gift.

The fact is, however, that intuition is actually an effect of our body recognizing patterns in the environment.

This kind of pattern recognition is particularly important in the cutthroat world of the stock exchange, where traders have to predict the ups and downs of the market.

For, in fact — contrary to the Efficient Market Hypothesis, which states that markets cannot be predicted at all — traders _can_ learn to predict the market.

The Efficient Market Hypothesis is a central principle in economics which argues that markets fluctuate upon the arrival of new information. Since news is by definition unpredictable, proponents of the hypothesis argue that markets must be equally unpredictable.

However, if the Efficient Market Hypothesis is true, traders couldn't make consistently more money than the stock market makes itself.

The author tested the hypothesis by examining the Sharpe Ratio, a measure for success in the stock market, of experienced traders, and found it to be 2.5 times higher for them compared with less experienced traders. This shows that they were, on average, making more money than the stock market.

Because the most skillful traders were those whose Sharpe Ratio had steadily increased over the years, the researchers concluded that predicting the stock market must be learnable.

_Yet_ traders themselves can't explain how they beat the market; that's because they depend on their intuition, which is influenced by their automatic physical reactions — or _Somatic Markers_.

The traders can learn the patterns of the market environment subconsciously, because incoming information triggers an unconscious physical response.

For example, when a trader considers his available options, his body might respond by tensing up in slightly different ways for each option. So, even though he might not consciously register these body memories, he'll get a "hunch" and intuitively make the right trade.

> _"Intuition cannot be trusted in the absence of stable regularities in the environment." — Kahnemann_

### 6. The stock market demands a lot from traders, so they need to have cognitive skills and physical fitness. 

Even though we might think of stock market trading as an "office job," the trading floor is often brimming with young, fit males — and often former athletes. Why? To be successful, stock market traders need to possess qualities such as high stamina, excellent concentration skills and a quickness in making trades.

For example, traders often must engage in visuo-motor scanning as they survey the screens for — sometimes minute — price anomalies. It's very taxing on our brain to engage in this kind of activity for a prolonged period, yet traders have to do it for hours at a time.

Because this activity requires great concentration, being good at it depends heavily on physical fitness — getting the right nutrients, enough sleep, and a range of other physiological factors.

Traders must be quick, too. If they see a good trading opportunity, they have to pounce on it, since if another trader is quicker to purchase stock, this would cause the stock price to rise and would diminish their returns.

Moreover, physical fitness gives traders a greater awareness of their bodies. Thus, they'll likely be better at interpreting their bodies' signals — the Somatic Markers — which, as we've seen, enable traders to predict patterns in the market, and leads to more reliable hunches on the trading floor.

Indeed, in one study, researchers found that they could accurately measure people's sensitivity to Somatic Markers with a "heartbeat awareness" test. Participants listened to a repetitive tone and were asked to indicate whether or not that tone was in synchrony with their heartbeat.

The study showed that fit and well-trained people are better at accurately identifying synchrony than their overweight counterparts, leading some researchers to suggest that possession of this skill should be a prerequisite for the recruitment of traders.

### 7. When stock prices rise, traders can become addicted and prone to excessive risk taking. 

While traders are in the middle of a "bull market" — a market in which stock prices are rising — they can begin to feel that they can do no wrong. At first glance, this might seem to be a great situation for a trader to find himself in, but the truth is it can lead to both addiction and greater risk-taking.

Just like gambling, risky trading can be addictive because it has the potential to increase our dopamine levels — a hormone associated with feelings of desire.

In a study, rats were given one drop of juice which caused their dopamine levels to rise. When this was repeated for a while, the onset of the dopamine rise started _before_ the rats were given the juice. Researchers concluded that the increased dopamine levels indicated that the rats were longing for the next drop.

This process is a very similar one to that in drug addicts — and, as it happens, stock traders. When risky trades pay off, traders experience an extremely pleasant surge of dopamine, a rush that they want to experience again and again.

As if the power of dopamine weren't enough, when the hormone interacts with testosterone, traders become willing to take even greater risks.

During bull markets or bubbles, testosterone — a hormone which increases risk taking — interacts with dopamine, because testosterone levels increase whenever dopamine surges through the brain.

A trader's testosterone level increases as a result of making a killing on a particular day, which leads him to experience the _Winner Effect_. This testosterone boost increases the effect of the dopamine in his system, so that every win feels even better than the last.

These repeated wins will cause the trader's testosterone to rise, making him even more willing to take greater risks on the trading floor. As you'd expect, this can easily turn into a vicious cycle, and when enough traders experience these, a market bubble can form.

### 8. During a market crash, traders experience a strong physical reaction, which makes it even harder for them to recover from it. 

Whenever a big stock market crash occurs, the whole world is affected. And the stress that traders go through as a result of that crash makes it harder for them to regain control of the market.

During a crash, traders' bodies undergo a strong reaction. Among other stress responses, the hormone cortisol is released, significantly affecting the brain's functioning. Over a short period, cortisol can energize us to escape a threatening situation, but if it's distributed for a prolonged time, it becomes harmful.

First, cortisol influences our memory negatively. Under the influence of cortisol, traders tend to remember their losses and bad trades more than their wins, which lowers their testosterone levels and makes them more risk averse.

Second, it inhibits the frontal lobe, which is the brain region responsible for rational thought. Because traders become unable to think clearly, they're prone to believing in rumors about the market. In turn, these rumors make them even more afraid to take risks.

Additionally, the _locus coeruleus_, a brain region which helps us select the stimuli we want to attend to, starts firing incessantly. This makes it almost impossible for traders to distinguish the signals from the noise, thus making it harder to make good trades.

These physiological effects can lead to irrational behavior in traders, causing them to miss worthwhile trades, therefore increasing the market's volatility.

What's more, the effects of stress during a market crisis are further exacerbated in that traders start behaving in mean ways toward each other.

According to Robert Sapolsky, who studies primates, it's common for stressed-out dominant males to start bullying weaker males.

The exact same thing happens on trading floors during a crisis. For instance, it's typical for middle managers to fire employees even before layoffs are officially announced, or — perhaps even more stressful — to strongly _hint_ at impending changes in the staff, making everyone fear for his or her job.

As you'd expect, this leads to an increase in the traders' anxiety, thus worsening the effects of their stress response.

### 9. Toughening up traders and reducing their stress can help them make fewer bad decisions. 

As we've seen, the human body's stress response can lead people to make bad decisions. But, knowing this, is there something we can do to help minimize the impact of our physiology on our decisions?

Even though our resilience is genetic, repeatedly putting ourselves under moderate stress can help to toughen us up.

For instance, one study has shown that young rats that are handled by humans — a very stressful experience for the rats — exhibit fewer stress responses as adults, and they live up to 18 percent longer.

However, the stressor has to be moderate. Major stressors, such as separation from the mother, actually have the opposite effect.

Therefore, in the recruitment of traders, those candidates who've experienced moderate stress in their past might be preferable, because they're likely to not be as easily stressed on the trading floor.

Another stressor that can fortify us is physical exercise, especially if it involves being exposed to cold weather. Exercise releases what are known as "growth factors." These help our neurons to keep growing, which strengthens our brain against stress and aging.

One study found that rats that swim regularly in cold water respond more quickly and powerfully to negative stimuli; they have a better arousal response. When those same rats are exposed to stressors afterwards, they are tougher than their counterparts.

Researchers have observed the same effect in humans and speculated that this is one reason for the development of the Nordic practice of a sauna followed by a cold shower.

Of course, even the toughest traders can become mentally fatigued by stress. However, if they're able to freely switch between tasks, they'll experience less stress and will thus make fewer mistakes.

Many work-related illnesses and mistakes occur simply because employees are given no power of choice in the work that they do at any one time, and therefore what they should pay attention to. But, if employees are able to voluntarily switch tasks throughout the day, they complain less about fatigue and often feel rejuvenated after switching tasks.

### 10. Mixing up the staff of the stock exchange can minimize the effects of testosterone on the market. 

You might think it's merely a cliche of Hollywood movies, but the fact is that trading floors are populated mostly by young men.

Since testosterone levels are at their highest in young men, their behavior is also the most volatile in response to the hormone's effects.

The solution? Enforce diversity on the trading floors, as this would have a calming effect on the market.

First, we should consider hiring older men, as this would lessen the impact of testosterone on the market. In males, testosterone levels rise until they are in their mid-twenties and after that point they fall gradually until stabilizing around the age of 50.

This means that older men are less susceptible to the effects of testosterone, and less likely to respond in a volatile way on the trading floor.

Despite this fact, trading floors are hostile to older traders because their reaction times are slower and their cautious attitude is interpreted as fear.

However, famous investors like Warren Buffett or Benjamin Graham actually achieved their success later in life, showing that traits such as caution and slowness needn't preclude success.

Second, we should hire more female traders. Currently, women constitute only 5 percent of the trading floor's population, but — like older men — they're also less sensitive to the testosterone-induced market frenzies.

One reason is that women have a vastly different biology from men, and naturally possess only 10 to 20 percent of males' testosterone levels, making them far less prone to the hormone's effect. For instance, women don't experience escalating testosterone that occurs in males, such as during the Winner Effect.

Another reason is that women have a different stress response than men. While they can experience the fight-or-flight response in moments of physical danger, they most often show the "tend-and-befriend" response. Originally, "tend and befriend" referred to the observation that females protect their offspring and seek out a group (by "befriending") for mutual protection. Transferred to the workplace, it means that women respond to stress by being extra caring towards others.

### 11. Novelty can add to our stress, while a feeling of control can reduce it. 

Think back to the last time you were going through a particularly stressful period. How would you have responded to the opportunity to take an exotic vacation? Most likely, you'd have thought that this was perfect timing: a change of scenery would do you good.

However, you'd be wrong. When we're stressed, novelty — even the pleasant kind — actually _adds_ to our physiological stress load, and can even make us ill.

In one study, psychologists asked a group of men about life-changing events in their lives — such as divorce or the death of someone close; but also more positive events, like weddings and childbirths. They found that _all_ such events, even the ones we tend to think of as positive, lead to an increased risk of illness and mortality.

Why? The researchers believe that the novelty of changed living circumstances adds to our physiological load and makes us sick.

Therefore, it's best not to take an exotic vacation if you're already under stress, as the novelty of the place will simply increase it — even though you might not feel that increase immediately.

What we really need in times of stress is not novelty, but familiarity. However, it's sometimes impossible to avoid putting ourselves in novel circumstances. In such situations, one way to decrease stress levels is to attain a sense of control.

As one study showed, patients in pain suffer even more if they're uncertain of when they'll receive their next pain medication, or if they can't administer it themselves.

Doctors who allow patients to have some degree of control over their pain medication reported that those patients required less medication in general. One possible explanation for this effect is that being in control reduces stress, which in turn minimizes the experience of pain.

So, when patients gain a little control over an otherwise difficult situation, they feel less pain and actually need less medication.

### 12. Final summary 

The key message in this book:

**The behavior of traders in the stock market isn't rational. Rather, it's largely influenced by the trader's physiology, such as hormone levels and the body's muscle memory. Both during market bubbles and crashes, hormones like testosterone and dopamine have a great impact on traders' behavior, and therefore a destabilizing effect on the markets. Diversifying the gender and ages of people on trading floors would have a stabilizing effect on the stock market.**

**Suggested** **further** **reading:** ** _Antifragile_** **by Nassim Nicholas Taleb**

Some things seem to improve if they are placed in environments of volatility and unpredictability. _Antifragile_ analyses why this is the case. It suggests that this quality has been vital for the progress of human civilization since ancient times. Nassim Nicholas Taleb takes a critical look at modern society and its aim to smooth out life by interfering in systems like the economy.
---

### John Coates

John Coates is a neuroscientist working at the University of Cambridge, and a former _Wall Street_ trader (for _Goldman Sachs_, _Merrill Lynch_ and _Deutsche Bank_ ). In 2012, he made _The Financial Times_ and _Goldman Sachs Business Book of the Year Award_ shortlist, and was named on the magazine _Foreign Policy_ 's list of Top 100 Global Thinkers.

