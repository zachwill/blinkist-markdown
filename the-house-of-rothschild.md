---
id: 5694b07d2f68270007000018
slug: the-house-of-rothschild-en
published_date: 2016-01-13T00:00:00.000+00:00
author: Niall Ferguson
title: The House of Rothschild
subtitle: Money's Prophets 1798–1848
main_color: 786256
text_color: 786256
---

# The House of Rothschild

_Money's Prophets 1798–1848_

**Niall Ferguson**

_The House of Rothschild_ (1998) offers a detailed, insider look into the famed Rothschild family's multinational partnership. By examining the relationships and strategies that launched the Rothschilds to success, the book demystifies this historic family, making their meteoric rise to tremendous wealth and fame much easier to understand.

---
### 1. What’s in it for me? Learn about the famously wealthy Rothschild family. 

What does the name Rothschild mean to you? While many people associate the name with a wealthy Jewish family or a thriving investment banking company, most probably don't know that Rothschild was in fact the biggest bank in the world during the nineteenth century!

In these blinks, you'll learn how the Rothschild family's multinational partnership secured their immense financial success, and how their proximity to Europe's political elite and their Jewish lineage gave rise to a series of family myths. 

By peeling back the many layers of the Rothschild family's story, you'll better understand the historical reality of its most mysterious figures, as well as one of the most successful businesses in the history of capitalism.

You'll also learn

  * how antiques made Mayer Rothschild's office flood with bags of money;

  * how bribery ended up playing an important role in the family business; and

  * which initiatives the Rothschilds took to advance Jewish emancipation.

### 2. Mayer Amschel Rothschild established the Rothschild banking business in Frankfurt amid widespread anti-Jewish sentiment. 

The Rothschild family was among the richest and most influential families of the nineteenth century. But how did they come to enjoy such a coveted position? 

It all began with Mayer Amschel Rothschild (1744-1812), father of the famous Rothschild brothers. However, as a Jew in Frankfurt, Mayer Amschel didn't have the easiest start to his career.

During the eighteenth century, Jews in Frankfurt faced persistent, systematic discrimination and intensely anti-Jewish attitudes.

Virtually every aspect of the lives of Frankfurt Jews was affected by this anti-Semitism. Jews were confined to a ghetto, a single narrow street called the _Judengasse_ (Jews' Lane), with gates erected at either end. They were forbidden to live outside the Judengasse, and likewise forbidden to stay overnight anywhere else.

Jews were barred from entering major public gathering places like parks, inns, coffee houses and the town's promenades. For Frankfurt Jews, a major portion of life was spent within the high walls and gates of the Judengasse.

Yet, amid Frankfurt's anti-Jewish sentiment, Mayer Amschel Rothschild still managed to start a successful banking business.

Unlike most eighteenth-century German towns, Frankfurt was a center of commerce, which necessarily went hand in hand with banking.

Mayer Amschel's journey into banking began after he'd established himself as Frankfurt's leading antique dealer. With the capital he accumulated through buying and selling these goods, Mayer Amschel began extending credit to his growing network of suppliers and customers, thus firmly planting himself in the world of banking.

By 1800, he ranked among the richest Jews in Frankfurt, owing in no small part to his success in banking. In fact, his business was turning over so much cash that bags of money would often lie around the Rothschild office, filling the cupboards and piling up on the floor.

> _"All banks have histories . . . only the Rothschilds, however, have a mythology."_

### 3. Nathan Rothschild took the banking business to England; when his father died, the firm was passed on to the five Rothschild sons. 

Of Mayer Amschel Rothschild's many clients, the most important was William IX, Hereditary Prince, Landgrave and Elector ( _Kurfürst_ ) of the former German state of Hesse-Kassel. According to the Rothschild myth, it was this relationship with William IX that formed the real foundation of the Rothschild fortune. 

When Napoleon declared his intention to end the reign of the House of Hesse-Kassel, William had little choice but to flee. According to legend, it was at this critical moment that William turned to Rothschild, hastily leaving his entire fortune in Rothschild's hands.

In truth, however, Rothschild provided only one service to William, which proved crucial to his family's success: the management of William's English investments.

These investments were part of the reason why Nathan Rothschild, Mayer Amschel's third son, was able to establish a successful banking business abroad, in England.

Mayer Amschel sent Nathan to England around 1800. Like his father, Nathan was gradually transitioning from trading textiles to banking. Initially, Nathan took orders from Frankfurt, but gradually began making his own decisions and deals. It was the service of managing William's English investments that helped Nathan make the transition from a merchant to a London banker. 

By the time Nathan established himself in London, the family's Frankfurt bank had effectively passed from Mayer Amschel to his five sons following his death.

Mayer Amschel had become seriously ill on September 16, 1812, and died only three days later. In his testament, Mayer Amschel demanded strict unity among his sons in continuing the banking business. Seldom has a father's last testament been carried out more conscientiously — or profitably. 

As you'll see in the blinks that follow, Mayer Amschel's final wish — to maintain fraternal unity — had an enduring impact on the Rothschild family.

> Participation in the family business was passed to Mayer Amschel's male descendants, meaning Rothschild women were effectively excluded from the core of the business.

### 4. The Rothschilds made profits by giving loans to governments and with the help of their “commanding general.” 

By the 1830s, the Rothschilds had amassed about £4 million in wealth — more than ten times that of their biggest competitor, Baring Brothers & Co. So what led to the Rothschild brothers' immense success?

In part, this success stemmed from the profits the Rothschilds made by giving loans to European governments.

In 1814, Nathan Rothschild was officially entrusted with financing Britain's war against France to defeat Napoleon's troops. But Britain wasn't the only country that benefited from the Rothschilds' financial backing.

France, along with the "Holy Alliance" formed by Austria, Prussia and Russia, emerged from the Napoleonic Wars (1803-1815) in dire financial straits — and the Rothschilds were willing to help.

The diverse intergovernmental payments the Rothschilds made during this period all had one thing in common: they each produced profits in two distinct ways.

The first way was in the form of commissions of up to eight percent. The second, less obvious, way involved exploiting the often rapid and significant fluctuations in exchange rates.

By this point, the Rothschild brothers had established permanent bases in Frankfurt, London, Paris, Vienna and Naples, making it possible for them to buy a currency at a discount in one market and sell it at a profit in another.

Another critical ingredient to the Rothschilds' success was Nathan's emergence as the leader among the brothers. Technically, the five brothers had equal stake and power within the business, and profits were divided equally among them; but Nathan's English office generated by far the most capital. 

This, combined with Nathan's aggressive temper and the increasingly anglocentric nature of the Rothschild bank's operations, effectively reduced the other brothers to mere agents.

Nathan, as his brother Salomon half-joked, was the "commanding general" of their operation. The others were merely his "marshals."

Despite his dominance and success, Nathan's rule was never absolute, and the partnership did not degenerate into dictatorship.

> _"You are certainly right that there is much to be earned from a government which has no money." - James Rothschild to Nathan Rothschild_

### 5. The Rothschilds’ close contact with political elites gave rise to myths that fostered hostility. 

At the turn of the nineteenth century, it was a matter of course that European politicians would accept favors from bankers. Investment tips and outright bribes would be rewarded with things like economically relevant inside information or favoritism when a loan was needed.

The Rothschilds also engaged in this practice, and in doing so maintained close contact with the political elite of their time.

Specifically, the Rothschilds extended credit to influential figures in order to foster "friendships." For example, the family made individual loans to French King Louis XVIII, Prussian diplomat Wilhelm von Humboldt, members of the English royal family and so on.

But the most prominent example of such "friendship" was the Rothschilds' relationship with Austrian statesman Klemens von Metternich. Not only did the Rothschilds advance money to Metternich privately, Salomon, the Vienna-based brother, also provided him with insider information that he would receive from his brothers in London, Paris, Frankfurt and Naples.

Metternich rewarded these insider tips by informing Salomon about Austrian political machinations. Information about Austria's war preparations or the collection of taxes was of great value to the Rothschilds, as such information influenced their financial activities.

Sometimes, however, a more subtle approach was needed. In these cases, the Rothschilds would give presents, such as the jewelled caskets given to the Elector of Hesse-Kassel, to those with whom they wished to forge an alliance.

While the Rothschilds' close contact with the political elite was immensely profitable, it also gave rise to myths that encouraged hostility against them.

Their involvement in so many major financial transactions made the Rothschilds increasingly familiar to the public during the 1820s. But this newfound publicity was seldom positive: rumor quickly spread that the Rothschilds wielded extensive, clandestine influence over European politics. 

However, much of the myth making and hostility that the Rothschilds endured was not based on actual facts, but rather on simple economic rivalry coupled with anti-Jewish sentiment.

> _"From the very earliest days, the Rothschilds appreciated the importance of proximity to politicians . . . and politicians soon came to realize the importance of proximity to the Rothschilds . . ."_

### 6. The Rothschilds never forgot their “co-religionists” and advocated for Jewish emancipation. 

The Rothschilds' wealth and influence afforded them a privileged social status — this despite belonging to a class that had been historically disadvantaged, even in the Rothschilds' time. 

For example, the Rothschilds owned prestigious residences, where they would host dinners for the high society of the time. They even went on to acquire noble status in 1817, a formal confirmation of their privileged position in society.

The Rothschilds also used their social status as a force for good by advocating for Jewish emancipation. They never lost sight of the fact that Jews were still the targets of a wide range of discriminatory laws and regulations. Jews were entirely excluded from politics, and were subject to a range of economic and legal restrictions, such as being barred from owning land.

The Rothschild brothers urged their friends in politics to advocate for the Jewish cause. Salomon Rothschild petitioned Metternich as well as Prussian chancellor Hardenberg for assistance in obtaining equal civil rights for Jews. In one letter to his brothers, Carl Rothschild called upon them to do more to help elevate the status of Jews in Europe.

Though the Rothschilds didn't reserve their political action for wealthy Jews alone, the relationship between the Rothschilds and the larger, poorer Jewish community has long been the subject of myths and the butt of jokes. At least one cartoonist at the time suggested that, having made their fortune, the Rothschilds were indifferent to the plight of their "poorer co-religionists."

But the brothers were indeed involved in philanthropy. For example, in 1825 they donated money to build a new hospital. Nathan was especially interested in helping the disadvantaged, and contributed to a number of charities for the poor and sick.

> _"'[I]f one Jew is a Baron every Jew is a Baron:' that was how they saw it in the Judengasse."_

### 7. The Rothschilds’ success lay in their unique communications network and the cooperation between the five houses. 

For years, people have wanted to know the Rothschilds' secret. While there is no _single_ reason that can explain their financial success, there are at least two factors that we can identify as critical to their incredible prosperity. 

The first is the development of the brothers' communications network.

The Rothschilds were always trying to find ways of expediting their communications. Nathan, for example, would pay premiums to the captains of postal ships for express delivery.

However, mail was routinely opened by German post offices if it appeared to contain politically sensitive or useful information. When discretion was of the essence, the Rothschilds had no choice but to employ trusted private couriers.

Until the mid-1830s, when the railway and the telegraph ushered in a new era of communications, the Rothschild courier service was better than any other existing form of communication. Because it could deliver letters faster than the regular post, those who used it would get politically and financially relevant information, such as updates on the stock market, before the rest of the public.

But the principal reason for the Rothschilds' success lay in the cooperation between the five Rothschild houses.

As mentioned earlier, the Rothschilds profited from the differences in exchange rates between various European markets. Their multinational partnership meant that they were uniquely positioned to conduct this kind of business effectively.

When taken as a whole, the five Rothschild houses in London, Frankfurt, Paris, Vienna and Naples comprised the largest bank in the world. This multinational partnership allowed the Rothschilds to disperse their financial influence throughout five of Europe's major financial centers.

To reinforce their sense of collective identity, the brothers agreed that each house would inform the others of the transactions it carried out on a weekly basis.

### 8. The death of Nathan Rothschild affected not only the Rothschild house, but the entire world. 

In June 1836, a dark shadow was cast over the Rothschild family: Nathan Rothschild was dying. By the end of July of the same year, he lapsed into a violent fever and passed away. Inevitably, Nathan Rothschild's death shifted the constellation of power in the Rothschild family.

Since the death of their father Mayer Amschel in 1812, Nathan had been the leader of the Rothschild brothers. Even to the outside world, Nathan was seen as the crucial pillar upon which the entire Rothschild house rested. 

After his death, all Nathan's rights transferred to his four sons. But who would become the new leader of the Rothschild family business?

Usually, people assume that Nathan's role was filled by the youngest of the five brothers, James. Indeed, he was quick to try to impose his authority on his nephews following Nathan's death, and on one occasion exhorted his nephews to pay more attention to his letters.

But it's far from clear that James ever wielded power equal to Nathan's. In reality, the relatively equal distribution of power among the five houses meant that no single brother was really in a position to give marching orders to the others, as Nathan had once done.

Nathan Rothschild's death also had much broader consequences than those the Rothschild family had to wrestle with. Nathan was the richest man in Britain, and therefore, given Britain's economic dominance at this time, almost certainly the richest man on earth.

His economic influence reached across all of Europe, and his death meant drastic, sudden changes to the European financial market.

### 9. Patronage of the arts was important to the Rothschild family. 

For the Rothschilds, an important source of enjoyment — and prestige — was patronage of the arts. 

Indeed, the family had amassed a great collection of prestigious works of art over the years. But it was the art created in seventeenth-century Holland that the family was most drawn to. In 1840, for example, James Rothschild purchased works by Rembrandt, Peter Paul Rubens and Anthony van Dyck.

Perhaps the best explanation for their fascination with Dutch artists was their tendency to favor secular subjects over religious themes; in contrast to other artists of the time, Dutch painters were mostly secular. Interestingly, however, the Rothschilds also acquired works displaying explicitly Christian iconography.

It seems that the Rothschilds weren't too interested in the art of their contemporaries. With very few exceptions, the only contemporary paintings the family owned were the portraits of themselves that they'd commissioned.

The Rothschilds likewise patronized some of the premier composers and performers of the nineteenth century. The most obvious reason for this is that good music was an absolute necessity for a successful evening ball.

Notable performers who played at Rothschild gatherings included Felix Mendelssohn, Franz Liszt, the pianist and conductor Charles Hallé and the famed violinist Joseph Joachim. Some also believe that the Polish pianist Frédéric Chopin's career in Paris was launched in James Rothschild's own home. 

Important writers of the time also received the Rothschilds' favor. Two of the best-documented cases were Heinrich Heine and Honoré Balzac, both of whom were closely associated with James Rothschild in the 1830s and '40s. 

A number of anecdotes paint Heine as a kind of licensed jester for James Rothschild, poking fun at every opportunity. The surviving correspondence suggests that Heine became a relatively humble recipient of James's indulgent patronage. Balzac, in contrast, happily borrowed from James, and avoided repaying him for as long as possible.

### 10. The Rothschilds’ investments in railways made them targets for the social revolutions of 1848. 

The mid-1830s marked a shift in the Rothschilds' financial focus toward industrial finance. In particular, the Rothschilds invested heavily in the emerging business of railway construction. 

The Rothschilds played a leading role in developing the French railway network due to the profits that railways promised at the time.

However, because the Rothschilds were such a dominant force in railway development, a growing number of journalists began expressing hostility toward what they regarded as a corrupt, private monopoly. 

This hostility became especially bitter in 1846, when a train on one of the Rothschild lines derailed, causing an accident in which at least 14 people died.

Soon, the public prominence that the Rothschilds had won with their railways made them targets for the social revolutions of 1848, which resulted in a series of political and social upheavals throughout Europe.

Not only were the revolutionaries concerned at the time with civil rights, such as free speech and freedom of the press, they were also concerned with combating the era's growing disparity of wealth. The Rothschilds, a wealthy and influential family that gained public prominence through their endeavors in the railway system, came to personify this inequality.

As a result, the Rothschilds faced serious threats to their safety. For example, in Frankfurt, the Rothschild houses were singled out for attacks, and windows were smashed.

Financially, the Rothschilds faced serious losses due to the wealth they held in the form of government securities. Government securities were falling dramatically in value as a result of many governments' efforts to counteract social revolution by increasing welfare spending, and thereby falling into debt.

The Rothschilds suffered heavy losses in 1848, teetering on the verge of bankruptcy. But because the revolution never reached Britain, the London house was able to bail out the other four, thus ensuring the survival of the Rothschild family legacy.

> _To the Rothschilds, the revolution in 1848 seemed "the worst revolution that ever happened"._

### 11. Final summary 

The key message in this book:

**The Rothschild family ranked among the richest and most influential families in nineteenth-century Europe. Exceptional in their international scope and the way they conducted their financial business, the meteoric rise of the Rothschild family has long been the subject of various myths, all of which seldom reflect the truth.**

**Suggested further reading:** ** _The Ascent of Money_** **by Niall Ferguson**

_The Ascent of Money_ is an explanation of how different historical events led to the development of the current financial system.

It aims to show how, despite its proneness to crises and inequality, the financial system and money itself are drivers of human history and progress.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Niall Ferguson

Niall Ferguson is an author, political commentator and fellow and tutor of modern history at Jesus College, University of Oxford. His previous works include _Paper and Iron: Hamburg Business and German Politics in the Era of Inflation 1897-1927_ and the best-selling book _Virtual History: Alternatives and Counterfactuals_.

