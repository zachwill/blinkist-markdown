---
id: 566ffc6acf6aa3000700004d
slug: the-idea-factory-en
published_date: 2015-12-18T00:00:00.000+00:00
author: Jon Gertner
title: The Idea Factory
subtitle: Bell Labs and the Great Age of American Innovation
main_color: 1B893E
text_color: 14662E
---

# The Idea Factory

_Bell Labs and the Great Age of American Innovation_

**Jon Gertner**

_The Idea Factory_ (2012) charts the influence of Bell Labs, the research arm of telephony monopolist AT&T. This innovative laboratory, established in the 1920s, was the source of dozens, if not hundreds, of technological innovations, effectively ushering in our modern digital age.

---
### 1. What’s in it for me? Learn about Bell Labs, the source of most of today’s technological innovations. 

There is little doubt that we are living in the golden age of technology. We have impressive access to so many life-changing devices: the personal computer, the smartphone and the internet. 

When asked which person or company is responsible for coming up with these innovations, one would be forgiven for giving credit to modern-day giants like Google or Amazon. 

Yet these companies are really late to the scene. They've reaped the benefits of technological innovation, rather than helped create it. The true birthplace of the tech revolution was Bell Labs, a research arm of telephony monopoly AT&T. 

These blinks will show you just how talented and creative the teams at Bell Labs were, and why we all share a great debt to their thirst for scientific knowledge and discovery.

In these blinks, you'll learn

  * how Bell Labs created the perfect environment for innovation;

  * what the connection is between satellites and solar panels; and

  * why scientists work best when deadlines aren't a stress.

### 2. Bell Labs was born from the early AT&T, which hired top scientists to improve telephone service. 

Alexander Graham Bell patented the first working telephone in 1876. Looking to capitalize on his groundbreaking invention, Bell founded the Bell Telephone Company, which later evolved into the American Telephone and Telegraph Company, or AT&T. 

In the 1890s, however, Bell's telephone patent expired, opening the door for other companies to take advantage of the technology. To remain competitive, AT&T resolved to use the power of science and research to provide better service to its customers.

This is how, in 1925, AT&T founded Bell Telephone Laboratories.

One of the goals of the labs was to make phone service more reliable, and cheaper. At the time, the telephone was still a rudimentary technology. Phones did not ring; dial tones and busy signals had yet to be invented. A caller would shout into a receiver until someone on the other end noticed!

Yet Bell Labs's overarching focus was to investigate communication in its many forms, whether through a cable or over a radio wave, with recorded sound or using visual images. AT&T president Theodore Vail was the driving force behind this vision, encouraging teams to explore advances in physics and chemistry that might transform the way people communicate.

Bell Labs hired top scientists, and a majority of its employees were graduates of the leading universities in the United States, such as the Massachusetts Institute of Technology, the University of Chicago and the California Institute of Technology. 

Mervin Kelly, for example, was one of the most gifted physics students at the University of Chicago before coming to Bell Labs. In 1951 he was named director of the labs, and his work was pivotal in changing the communications industry. 

Bell Labs became an intellectual hub for many eminent scientists, some of whom even went on to win the Nobel Prize.

> New employees at Bell Labs were asked to sell the rights to their future patents — for a symbolic sum of one dollar. None ever refused.

### 3. During World War II, Bell Labs focused on developing military technology, such as radar. 

The struggles and catastrophes of the first half of the twentieth century left a mark on Bell Labs. 

During the Great Depression of the 1930s, for example, Bell Labs management needed to reduce employee working hours. Nevertheless, the young scientists made the best of the situation and used non-working hours to participate in study groups and pore over scientific textbooks. 

With the outbreak of war in the 1930s, Bell Labs shifted its focus to developing military equipment. In 1940, when US politicians demanded that the nation's scientists contribute to the war effort in Europe, Bell Labs almost completely abandoned its regular work. As a result, some 75 percent of Bell Labs's work was focused on immediate military use. 

Bell Labs's research department was also tasked with exploring the possibility of building a weapon out of uranium — that is, to create a nuclear bomb. Consequently, the physicists at Bell Labs discovered how to create a nuclear reactor.

But Bell Labs's most significant contribution to the war effort was the radar. 

Even as an early, simplistic technology, radar was a crucial part of military strategy, as it could be used both offensively and defensively. Radar could detect incoming enemy aircraft and submarines, guide gunfire and bombs, and help land a plane at night or in dense fog.

Bell Labs scientists were given the task of further developing and improving radar technology, which they did with impressive results. Scientists created workable designs for new radar instruments that could detect objects from distant locations.

### 4. The transistor, or crucial building block of all today’s electronic devices, was invented at Bell Labs. 

Bell Labs early on realized that good employee communication leads to innovation. Employee offices and the labs were located in different areas, so that scientists had to walk between the two. The idea was that when scientists crossed paths, they could chat and exchange ideas. 

This strategy worked, as the Labs were a hotbed of groundbreaking innovation in the post-war decades. One major achievement came in the form of the transistor, invented by Labs scientists in the 1940s.

Bell Labs physicists Walter Brattain and John Bardeen developed a method of amplifying — or switching on and off — an electrical signal by sending it through a slice of silicon. This was the world's first transistor. 

Yet it was almost useless as a practical device, as even a simple hand motion could make the transistor behave unpredictably. Fellow Labs scientist William Shockley, however, found a way to improve the transistor's design to fix this issue.

Even though the transistor came to be seen as one of the most significant technologies of the twentieth century, at first the public just didn't get it. The _New York Times_ reported a four-paragraph story on its invention — on page 46 of the daily paper.

The computer industry, however, was intrigued.

It was quickly realized that the transistor was a critical digital tool, as it could be turned on or off with a small burst of electricity — which essentially could be translated into a "yes" or "no," or a one or a zero. Furthermore, once encoded in ones and zeroes, transistors could be connected to process batches of information.

This early transistor went on to become the building block for all electronic devices, and led to the development of the modern computer industry.

> In 1956, Bell Labs physicists Walter Brattain, John Bardeen and William Shockley were jointly awarded the Nobel Prize for the invention of the transistor.

### 5. Bell Labs mathematician Claude Shannon was the great mind behind information theory. 

When the transistor was unveiled publicly in 1948, its importance and potential wasn't immediately grasped on a grand scale. Yet there was one person who got it: Bell Labs mathematician Claude Shannon.

In the mid-1940s, Bell Labs technicians began experimenting with more efficient methods for transmitting telephone calls. One idea that seemed promising was a method known as pulse code modulation, or PCM.

At the time, phone signals were generally transmitted by electrical waves. PCM translated these waves into "on" and "off" pulses. This meant that, with PCM, rather than sending waves through a telephone cable, a series of electric pulses could be sent instead.

Building on the concept of PCM, Shannon was the first to realize that _all_ communication could be thought of in terms of information encoded as binary digits — or "bits." 

Each bit would correspond to a piece of information, each piece essentially representing a choice between two things. So one bit could be defined as a choice between a one and a zero.

What Shannon had conceived would become the basis of what we now know as _information theory_ — one of the most significant intellectual achievements of the twentieth century. 

In effect, Shannon's ideas, combined with the invention of the transistor, became the foundation of all modern digital communication, from cell phone transmissions to compact discs to even deep space communication.

Had mathematicians been eligible for the Nobel Prize, Shannon would certainly have been in the running. Instead, in 1985, Shannon was awarded the Kyoto Prize for outstanding contributions in the field of mathematics.

> Claude Shannon built the first primitive chess computer — in his spare time.

### 6. Bell Labs not only helped build the first satellite, but also figured out how to power it with the sun. 

Since the invention of the telephone, engineers dreamed of the next big communications step, allowing a person in North America to call family in Europe. Yet this seemed a Herculean task, considering the thousands of miles of cable required to cross the Atlantic — and even then, it just wasn't possible to transmit signals over such a distance of cable. 

It wasn't until the 1950s when overseas telephony became possible — and with it a new invention called satellite technology. Of course it was Bell Labs scientists who developed one of the first communications satellites.

In the 1950s, engineer John Pierce proposed an idea for satellite communications. Pierce had envisioned an orbiting, uninhabited spaceship — or satellite — that could relay radio, telephone or television signals over great distances. 

He thought that a signal from one part of the world could be directed toward a satellite orbiting in space. The satellite would receive the signal, then direct it to another part of the world.

The very first satellite was ready to launch on August 12, 1960. Bell Labs scientists, with the help of the newly established US National Aeronautics and Space Administration (NASA), unveiled Echo 1, the first communications satellite.

Bell Labs scientists were pivotal in building Echo 1, yet the process posed many difficulties early on. For instance, the satellite required a reliable, inexhaustible power source. 

While studying the properties of silicon, Bell Labs scientists Cal Fuller and Gerald Pearson discovered a way to build a silicon solar battery, which became the first functional solar power device. A satellite was the ideal recipient of this new solar technology. 

Bell Labs technicians were also responsible for inventing and developing a type of horn-shaped antenna, which allowed for the more focused reception of signals and reduced interference.

### 7. Bell Labs invented many other visionary products, but endured a fair share of failures as well. 

The transistor and the radio. Solar cells and communication satellites. All of these technologies originated at Bell Labs, and each have completely revolutionized the modern world. Yet there was still more to come. 

In the 1960s, Bell Labs scientists began research on mobile telephony — the working or use of mobile telephones. There were two problems they had to solve: First, there were few available wireless frequencies, meaning only a few calls could be placed simultaneously. Second, it wasn't clear how a caller could move around without a call disconnecting.

By 1970, though, engineer Amos Joel managed to solve both these issues, and Bell Labs had invented the basis for the reliable mobile technology we depend on today.

Another visionary invention occurred in 1969, when a group of computer scientists wrote a revolutionary computer operating system. This evolved into Unix, the system which would become the foundation for many subsequent computer languages.

Despite all this success, Bell Labs saw its fair share of failures. One huge flop was the Picturephone. The Picturephone was a device that allowed audio-visual communication, not unlike what we can now do with Skype and Facetime. 

But this was 1970. Although AT&T was right to believe the technology had huge potential, its public release was a complete failure. As it turned out, customers preferred the more impersonal method of a regular phone call!

Just the same, among so many innovative inventions, failures like the Picturephone were accepted as inevitable by-products of the broader vision.

### 8. Bell Labs was a force for innovation, yet was fueled by AT&T’s lucrative telecommunications monopoly. 

What exactly is innovation? An idea that strikes a lone inventor in a moment of inspiration? 

Actually, innovation tends to be more of a convergence of ideas over the course of time, whether months or even decades, as groups of people carefully consider various questions.

It was Bell Labs's Mervin Kelly who created a more strategic approach to the process of innovation. Kelly was a physicist and researcher, and was also the Labs's company director from 1951 to 1959. He is credited with establishing a structure to foster innovation at Bell Labs.

Kelly encouraged interdisciplinary groups, combining physicists, chemists, metallurgists and engineers to tackle problems together. He brought together theoreticians and experimentalists to ponder questions of new technology and truly think "outside the box."

He also instigated a policy whereby junior scientists were encouraged to approach more distinguished minds with questions. Importantly, when they did so, senior scientists were not allowed to turn junior scientists away — thus encouraging a free flow of ideas.

Many researchers also enjoyed full autonomy, as Kelly allowed them to explore and ponder new ideas without having hard and fast deadlines or even concrete objectives.

However, such an approach relied heavily on AT&T's monopoly status. Kelly knew that to innovate and bring great ideas to the world, the company needed not only excellent minds but also a reliable stream of funds.

Bell Labs was indeed well-funded, backed by AT&T's de facto telecommunications monopoly, reestablished in the 1920s when the company scooped up independent, local telephone companies.

This monopoly was also supported by the government, as politicians viewed telephone competition as an "endless annoyance" and preferred to deal with just one corporate representative in the telephone industry. The government thus permitted AT&T's monopoly with the understanding that its scientific research worked in the interest of the public.

> _"Inventing the future wasn't just a matter of inventing things for the future; it also entailed inventing ways to invent those things."_

### 9. Bell Labs shuttered its operations in 2006. Yet few tech giants have really followed in its footsteps. 

By the 1970s, the political and social acceptance of AT&T's telecommunication monopoly began to wane. 

The US Justice Department headed up a lawsuit that demanded AT&T be divided into smaller, competitive companies. In 1984, AT&T was finally forced to let go of its local phone company holdings, which then became independent corporations.

With the dissolution of AT&T, it was clear that Bell Labs would soon follow. Yet the management of AT&T stuck to their original commitment to continue to fund basic research and throughout the 1980s, Bell Labs scientists continued to wow the world with significant discoveries.

One team, for example, discovered a phenomenon known as the fractional quantum Hall effect, and subsequently won the Nobel Prize.

Yet cut-throat competition in the telephony market hit AT&T hard. It needed to reduce spending and was forced to cut staff at Bell Labs, too. In 2006, the main campus was shuttered for good.

So how do the research efforts of today's technology leaders compare to the legacy of Bell Labs?

While leading companies such as Google, Apple and Microsoft do maintain research departments and have a steady cash flow to support such efforts, basic research isn't their top priority. Breakthroughs in scientific knowledge are clearly far less important to these companies than market expansion and profit maximization.

> _"The value of the old Bell Labs was its patience in searching out new and fundamental ideas, and its ability to use its immense engineering staff to develop and perfect those ideas."_

### 10. Final summary 

The key message in this book:

**Bell Labs was the birthplace of groundbreaking innovations like the transistor, radar, satellite communications and mobile telephony. Innovation at Bell Labs was cultivated by an egalitarian philosophy and mutual idea exchange, the results of which few tech giants today can measure up to.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Soul of a New Machine_** **by Tracy Kidder**

_The Soul of a New Machine_ is a Pulitzer Prize-winning book about one team at the computer company Data General who was determined to put its company back on the map by producing a new superminicomputer. With very few resources and under immense pressure, the team pulls together to produce a completely new kind of computer.
---

### Jon Gertner

Journalist and author Jon Gertner writes for the _New York Times Magazine_ and edits for _Fast Company_. He grew up just around the corner from Bell Labs in Berkeley Heights, New Jersey.

