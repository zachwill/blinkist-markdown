---
id: 580614c5319fa90003feb9b8
slug: the-ideal-team-player-en
published_date: 2016-10-19T00:00:00.000+00:00
author: Patrick Lencioni
title: The Ideal Team Player
subtitle: How to Recognize and Cultivate the Three Essential Virtues. A Leadership Fable
main_color: A82947
text_color: A82947
---

# The Ideal Team Player

_How to Recognize and Cultivate the Three Essential Virtues. A Leadership Fable_

**Patrick Lencioni**

_The Ideal Team Player_ (2016) explores the role teamwork plays in today's business environment and shows you how to build a team geared for success. These blinks explain what makes a good team player, how to find them and which strategies you'll need to build a company around the concept of teamwork.

---
### 1. What’s in it for me? Find out what makes someone an ideal team player. 

The days of the solitary genius are gone. Many of the inspiring innovations produced recently have been developed by teams of talented people — the Apple iPhone, Pixar's film _Up!_ and even Wikipedia.

If you want to produce something creative and groundbreaking, you too will need a team. But teams are not easy to manage — people can squabble or fall out with each other — thus limiting innovation and stymying productivity. So how can you create an effective team?

These blinks describe the three traits every great team player should have and how these traits can be fostered in employees.

In these blinks, you'll also find out

  * why you need employees who do more than they're told to do;

  * why teams with average players sometimes outperform teams with top talent; and

  * why the way a potential job candidate deals with a receptionist can tell you volumes.

### 2. A team with star talent that can’t work together is dysfunctional. Learn the five traits of broken teams. 

Have you ever wondered why a team of all-star soccer players can still consistently lose matches? Such teams can be considered _dysfunctional_, and there can be five possible reasons for this.

First, in a dysfunctional team, team members might not care about the team's overall results, as each member is too focused on personal success. A star player might be so determined to score a goal that he refuses to pass the ball to a teammate ready to take a shot. As a result, the team as a whole misses a chance to score.

Second, members of dysfunctional teams may lack accountability. This means that the team doesn't call its individual members out for mistakes, such as a player showing up late or even hungover to practice. This attitude lowers the team's standards overall, as members see it's acceptable not to give their best.

Third, dysfunctional teams struggle with committing to collective decisions. If a soccer team decides on a strategy but one player does his own thing during a match regardless, the strategy falls flat and the team fails.

Fourth, dysfunctional teams often suffer from a fear of conflict. When team members avoid necessary conversations or debates, issues fester and the potential for conflict grows.

Finally, dysfunctional teams are marked by an absence of trust. When a team member distrusts the team as a whole, that member will hide weaknesses, avoiding asking for help even when needed. In turn, a member who hides weaknesses won't be trusted by other team members, either.

So how can you avoid the pitfalls of dysfunctional teams when building an organization?

Great teams are made of team players. A bad team player can disrupt not only the work of an entire group but also an entire company — just like that soccer player who refuses to get on board with the team strategy.

Employees with good social skills and who are driven workers — the team players you should cultivate and reward — won't stick around if your team is ineffective or even hostile. Thus you need to be proactive by hiring team players, coaching dysfunctional staff and letting go of those employees who won't change.

But how can you ensure you have the right people on your team? Let's explore the three traits for which you need to keep an eye out in current and potential employees.

> _"I'd bet my job on the fact that we could get more work done with fewer people if we had real team players."_

### 3. An ideal team player is a hungry worker or an individual who wants to do more than is necessary. 

Let's say you've found a potential new employee with an impressive work history, but you're wondering whether she's a solid team player.

To determine this, be on the lookout for three key virtues. Remember that all three are essential to the success of your prospective hire!

The first virtue of a team player is _hunger_. Hungry potential employees are always in search of more: more achievement, more learning and more responsibility. These types of people do way more than what is expected because they're driven and passionate about their work.

Conversely, there are employees who only do what's necessary. You can't begrudge people like this, since they aren't technically doing anything wrong — but they will hold your team back.

When you assemble a team of hungry people, you'll never need to push them since they'll already be self-motivated and attentive. As a result, this team will automatically think ahead and spot opportunities.

For instance, an average soccer team can prevail against teams with more individual star players as long as the team is united in its hunger to win. Hungry players will train extra hard and push their limits constantly!

Yet while it's easy to identify a hungry worker, inspiring hunger in others isn't as simple. You can of course easily identify employees who could stand to be a little more hungry, by observing and measuring individual commitment — such as looking at the hours put in at the office and general output.

But even when you identify such workers, you'll struggle to make them hungry. It's difficult to change someone against their will, and the fact is, many employees don't _want_ to be hungry.

Quite simply, some workers realize that it's to their benefit to be un-hungry. Just consider how an employee happy to maintain the status quo enjoys more free time and less stress than a hungrier peer.

Yet in rare cases, hungry colleagues can "infect" and inspire less motivated coworkers. But there are two more traits that will help you identify your ideal team player: let's explore them now.

### 4. Great team players have social smarts, because people who “get along” work more effectively. 

Having a crew of hungry employees is essential, but you'll also need team players who are _smart_. Please note though that being smart isn't the same as being intelligent!

In the context of teamwork, _smart_ means having social skills, or the ability to get along with people and to work with them efficiently and effectively.

Smart refers to a person's capacity for interpersonal awareness — that is, their ability to act appropriately in a range of scenarios and understand both group dynamics and individual personalities.

Smart employees tend to know what teammates are feeling during a meeting. Smart employees also communicate and listen well, ask great questions and are fully engaged in a conversation.

While it's not challenging to list the traits of a smart person, sometimes it is easy to overlook a worker who _isn't_ smart. This is important because if you do catch such people, you can help them change.

Many people hide a lack of social skills behind technical know-how, a humble nature or insatiable hunger for success.

Such employees might not recognize this weakness, but once they do, they can change it.

In fact, these people often feel unhappy because they can tell that they don't belong in a group. So if you want to retain these workers, you need to offer them the assistance they need. Luckily, most will embrace such an opportunity, spurred on by their unhappiness at not "fitting in."

But why is smartness so essential to a team?

Simply put, smart team members are less likely to exhibit any of the five dysfunctions that we explored earlier, traits that keep a team from achieving its best. For example, good social skills help a worker build trust by empathizing with others. Team players don't shy away from healthy conflict since they know how to negotiate without being overly aggressive or negative.

In other words, being personable and attentive is key to managing a team — and without positive social skills, teamwork just isn't possible.

### 5. Great team players are humble, putting the team’s needs and goals before their own. 

Do you feel that your supervisor deserves your respect — but not the office intern? If so, you could use a dose of humility. Great team players are almost always _humble_ individuals.

These team players don't pay too much attention to status or ego, focusing instead on the welfare of the team as a whole.

For instance, a humble person will be quick to point out the contributions that other members make and define success collectively, strengthening team spirit. But team players also know that everyone on the team deserves respect, regardless of individual status.

Someone who isn't humble can be disruptive to a team's overall psyche. Such an employee might compete with other members for attention, resenting it when someone else is the center of the group's focus. Naturally, this behavior disturbs a group's cohesive feel.

Let's look at an example. Our team is brainstorming a new product name, and one team member offers a perfect suggestion. Problem solved! But wait — while most of the team is applauding the idea, another team member is jealously criticizing it and still suggesting alternatives, trying to regain the spotlight.

When keeping an eye out for workers who lack humility, remember that this trait often shows up as the first dysfunction — caring more about one's status than the goals of the team. Remember the soccer player, who only thinking of himself, refused to do the right thing for the team by passing the ball?

So while being humble is about supporting team members, it's also about being unpretentious and refraining from gloating about one's humility. After all, people who are so eager to appear humble are often quite the opposite!

Real humility means you care for others before yourself. A lack of humility can become glaring in simple ways — consider a person who strives to appear unassuming and humble during a job interview but, in an arrogant slip, fails to thank the receptionist on the way out.

> _"Humility isn't thinking less of yourself, but thinking of yourself less."_ –C.S. Lewis

### 6. When interviewing a prospective hire, check to ensure the person exhibits the three virtues. 

Nobody enjoys job interviews. It's an awkward process; more often than not, a nervous interviewee will try to tell his prospective employers exactly what they want to hear and not what they _need_ to hear.

So how can you tell if a candidate is the team player for whom you're looking?

First, keep an eye out for the three virtues of a team player — or crucially, the absence of those virtues.

For instance, try to detect if the candidate is humble by paying attention to whether he thanks the secretary for the coffee he is served.

Remember that smart people tend to know themselves well. They'll easily answer when asked for four adjectives that best describe their personality. And to find out if someone is hungry, ask the person if he worked hard in high school or was involved in competitive sports. Both are strong indicators of a hungry individual.

It's also essential to test a candidate, by explaining that the behavior of everyone in your company is held to account. This will deter candidates who lack the necessary hunger or who believe they wouldn't be able to meet the demands of the team.

And remember, you'll learn more about a candidate if you interview them with a team or at least discuss the one-on-one interview with others.

It's likely that other people will catch things that you miss. It pays to have your team present for an interview; afterward, you can ask members for their thoughts, to make sure a candidate displayed the three virtues.

In a team interview setting, you'll also learn how a candidate deals with multiple people at once, telling you a thing or two about the person's smartness.

So if you decide to let team members conduct one-on-one interviews with the candidate instead, the team should meet after every session to discuss the candidate's virtues or lack thereof, and decide what to focus on next.

### 7. Detect and develop the three virtues in employees and build them into the company culture. 

You probably have plenty of valuable employees in your company, but how can you turn these individuals into great team players?

You can start by searching for the three virtues among current staff. Come up with some criteria for each virtue, and ask yourself how many of these are present in any given employee.

Beyond this, you should also have employees do a self-evaluation, in which specific questions work to highlight the virtues for which you're looking. For instance, you could ask an employee to describe a situation in which he exhibited smarts, or have the employee reflect on whether he deals well with conflict, in or out of the office.

The next step is to help employees build the virtue or virtues they lack. After all, almost anyone can develop these virtues if they want to — you don't have to be born hungry or smart.

Most employees do want to improve once attention is drawn to an issue that is lacking. Your process should include letting employees know what they should do to develop virtues through timely and constructive feedback.

You could alert an employee to a situation in which he's addressed a coworker in an inappropriate way, for example, and praise other workers as they make progress.

But remember, if a worker is resistant to change, that individual may have to be let go.

Finally, embed the three virtues into your company's culture. Not every company needs to be built on teamwork. Yet if you do choose this path, it's essential to drill the three virtues into your employees.

Company managers must be bold and explicit, calling out people when they violate a virtue, identifying examples of the virtues in practice and praising employees for exhibiting them — for instance, praising an employee for his hunger in staying up all night to fix a software bug.

Finally, remember that managers must be good role models, by displaying positive team player virtues, too!

### 8. Final summary 

The key message in this book:

**Every team player has three virtues: a hunger to go above and beyond, the social smarts to interact positively with a group and the humility to subsume the ego for the team's common good. If even one of these traits is lacking in a single team member, your whole team will suffer.**

Actionable Advice:

**Don't talk about flaws, talk about change.**

When you approach an employee about a trait he lacks, present the issue as an opportunity to change. Tell the employee that everyone can improve, and you are there to help. This way, the worker will focus less on the trait that is lacking and more on the overall process of improvement. People really _can_ change, but doing so requires hard work — which a person will pursue only if he believes the goal is feasible.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Five Dysfunctions of a Team_** **by Patrick M. Lencioni**

_The Five Dysfunctions of a Team_ presents the notion that teams are inherently dysfunctional, so deliberate steps must be taken to facilitate great teamwork. A knowledgeable team leader can do a great deal to make his or her team effective, and the book outlines practical tools for achieving this.
---

### Patrick Lencioni

Patrick Lencioni is the founder and president of The Table Group, a consultancy. He is also a highly acclaimed public speaker who has written 11 bestselling business books.

©Patrick Lencioni: The Ideal Team Player copyright 2016, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

