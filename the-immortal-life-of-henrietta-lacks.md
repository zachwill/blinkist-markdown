---
id: 5420831a63656400089f0000
slug: the-immortal-life-of-henrietta-lacks-en
published_date: 2014-09-24T00:00:00.000+00:00
author: Rebecca Skloot
title: The Immortal Life Of Henrietta Lacks
subtitle: None
main_color: 80271A
text_color: 80271A
---

# The Immortal Life Of Henrietta Lacks

_None_

**Rebecca Skloot**

_The Immortal Life of_ _He_ _nrietta_ _La_ _cks_ tells the story of a poor tobacco farmer who died from cervical cancer, and her cell strand, HeLa, which scientists used to develop a cure for polio and other diseases. In a fascinating and revealing investigation, author Rebecca Skloot uncovers the history of Henrietta and her family, of the exploitation of black Americans by the medical industry, and of Henrietta's immortal cells.

---
### 1. What’s in it for me? Learn about the woman behind the world’s first immortal cells. 

Until recently, the life of Henrietta Lacks, the donor of the world-famous HeLa (pronounced _hee-lah_ ) cancer cells, has been a mystery.

Even though her cells have been essential to some of the most incredible advances in medicine of the past half-century — treatments for polio, and seminal research on AIDS and cancer cures — most people don't know her name. Those who _are_ aware that there's a person behind the cells often get her name wrong, referring to her as Helen Lacks or Helen Lane.

This book tells the parallel stories of Henrietta Lacks and her cells. In addition to recounting Henrietta's battle against cancer, to which she ultimately succumbed, author Rebecca Skloot details the history of the cells which outlived her, along with the rise and establishment of the cell-culture and gene-patenting industry.

As these blinks will show, _The Immortal Life of_ _He_ _nrietta_ _La_ _cks_ is a must-read for anyone with an interest in medical history, black American history, and the future of cell research and gene patenting.

In these blinks, you'll discover

  * why cell strands are so important to medical research, and how financially valuable they can be;

  * why your cells are kept in storage for years after you've visited the doctor for a routine checkup;

  * why plantation owners would spread stories among their slaves about doctors who'd come to abduct them in the middle of the night; and

  * what's behind the historical tension between black Americans and the medical industry.

### 2. Henrietta Lacks was a poor black American woman who died of an extremely aggressive form of cancer. 

On August 1, 1920, in Roanoke, Virginia, a small girl who would change medical science forever was born. Her name was Henrietta.

Like most of her family, young Henrietta helped out on their tobacco farm, harvesting the crop and hauling the leaves to South Boston to be sold.

When she wasn't working, young Henrietta played with her cousin, David Lacks — nicknamed Day.

Henrietta married Day when she was 20 and the couple soon began having children. But these were difficult times for small farmers, and the family were struggling financially. So Henrietta and Day decided to move to Sparrows Point, near Baltimore.

A decade later, in early 1951, Henrietta walked into the designated coloreds-only examination room of Johns Hopkins gynecology center. She'd discovered a lump on her cervix.

After doctors took a sample and rushed it to the pathology lab for diagnosis, they sent Henrietta home. There, she quickly returned to her usual daily routine: looking after her children, cooking for her family and keeping the house in order.

For a while at least, life continued as normal.

Then her biopsy results arrived: Henrietta had epidermoid carcinoma of the cervix, stage I.

At the time, Johns Hopkins were using radium — a radioactive material — to treat cervical cancer. But while radium is extremely effective at killing cancer cells, this comes at a cost: it also destroys any other cells it comes into contact with. Indeed, radium is so powerful that, in high doses, it can even burn the patient's skin.

Giving her official consent for any treatments or surgery the doctors deemed necessary, Henrietta was once again led to the Johns Hopkins ward for colored women.

There, Henrietta endured hours upon hours of excruciating radium exposure — the first of many treatments she'd receive over the course of the year.

Even though these treatments were intensive, to the extent that much of her body was visibly burned, they were ultimately ineffective.

Henrietta Lacks died on October 4, 1951.

### 3. Although Henrietta Lacks succumbed to her illness, her cells – named “HeLa” – survived and thrived. 

In the early 1950s, scientists and doctors were searching for ways to keep human cells alive outside the body so that they could conduct experimental research that would contribute to curing such illnesses as cancer, polio, herpes and influenza.

However, they struggled to keep the cells alive. When extracted from the body and placed in a culture medium (a liquid which helps cells survive), most of the cells would quickly die.

A new technique was needed.

As luck would have it, Johns Hopkins' head of tissue culture research, George Gey, was also an inventor and visionary. In his tireless search for a way to keep cell cultures alive outside the body, Gey had come up with what would be his most important invention: the roller-tube culturing technique.

This involved a cylinder, punched with holes to accommodate special test tubes (known as "roller tubes"), rotating very slowly, 24 hours a day.

Gey's reasoning was that, for cells to survive, constant rotation was necessary, as it mimicked the motion caused by the flow of blood and other fluids around the human body.

Although the logic of Gey's technique was indisputable, his assistant, Mary Kubicek, was skeptical that Henrietta's isolated cancer cells — labelled "HeLa," after the first two letters of her first and last name — would survive.

However, Kubicek, along with other researchers, was shocked when, two days later, she observed that the cells were not only still alive, but actually thriving. They were dividing at an unprecedented rate: Kubicek noted that the cultures were doubling every 24 hours — faster than the cells in Henrietta's body!

Why did the HeLa cells survive? Gey's technique certainly played a major role, but it was also the cells' aggressive nature — measured by how quickly they metastasized in Henrietta's body — that enabled them to live and thrive.

Kubicek continued the testing, distributing the cells over multiple test tubes. Meanwhile, Gey proudly announced to colleagues that he'd grown "the first immortal human cells," and soon began sharing them with labs that wanted to use them in research on illnesses like polio and cancer.

### 4. To help combat diseases like polio and cancer, scientists created a factory for producing HeLa cells. 

Soon after Henrietta's death in 1951, preparations were underway for the creation of a "HeLa factory," a project to mass-produce HeLa cells on a weekly basis. The project had one overarching goal: to find a cure for polio.

But why were the HeLa cells so amenable to mass production? There were several reasons.

First, the cost of producing the cells and performing research on them was relatively low.

At the time, most studies aiming to find cures for diseases used monkeys as test subjects. But experimenting on monkeys was problematic — and not because testing on animals was considered unethical. Rather, the problem was that experimenting on monkeys was expensive, making it difficult for labs across the country to conduct large-scale experiments and research.

Second, the HeLa cells were able to survive in a culture medium. In contrast, other cells could grow only on a glass surface and would stop growing when they ran out of space. So as long as there was a culture medium, HeLa cells would continue to grow.

Third, having tested various modes of transportation, George Gey discovered that HeLa cells were particularly resilient to being transported across the country. For one thing, they reproduced at a much faster rate than other cells, which increased the odds of their survival during shipping.

The final reason for the success of HeLa cells was that they were highly susceptible to the polio virus.

These reasons led the National Foundation for Infantile Paralysis (NFIP) to set up the HeLa Distribution Center to facilitate the growth and distribution of these cells for research labs.

Ultimately, because the mass-produced HeLa cells were quickly exposed to many different conditions, they were soon used to explore not just polio, but many other illnesses too. And soon, what began with Gey isolating and mailing a cell culture to other labs quickly evolved into a lucrative institution.

### 5. Although her cells spread across the globe, Henrietta and her family were largely forgotten. 

The HeLa cells spread around the world, from lab to lab, at a fantastic rate. Yet as the cells' fame and importance grew, their source was quickly forgotten.

In 1999, the author stumbled across a number of articles that were presented at a conference on the HeLa cells at Morehouse Medical School in Atlanta — one of the oldest historically black universities in the United States.

She contacted the conference organizer, Roland Pattillo, who also happened to be the only black student of George Gey.

Pattillo explained that the Lacks family was reluctant to discuss Henrietta or her surviving cells with anyone, journalist or otherwise.

Why the reluctance?

The family believed that the doctors had extracted the cells from Henrietta without asking her permission or telling her their plans for the cells.

Additionally, the family had a general mistrust of the medical industry due to the history of exploitation of black Americans in medical studies — such as the notorious Tuskegee syphilis experiments. In the 1930s, scientists began studying the development of syphilis from the time of infection to death. To that end, the researchers recruited hundreds of uneducated, poor black men, many of whom did not realize they had syphilis, and let the disease progress unchecked as they observed its symptoms — even though they could have cured the men with penicillin.

After grilling the author to make sure her intentions wouldn't upset Henrietta's family, Pattillo handed her their contact details.

Immediately, the author called Henrietta's husband and children to arrange a meeting. Unfortunately, no one showed. She'd been stood up.

So the author decided to travel to Clover, Virginia to get a feel for Henrietta's hometown, and to locate distant relatives of the Lacks family. Her reasoning was that, if she could meet with Henrietta's cousins, she would increase her chances of meeting her immediate family one day.

### 6. After Henrietta’s death, her family struggled to survive. 

Following the death of Henrietta, her family struggled financially. To make ends meet, Henrietta's widower, Day, began working two jobs. Meanwhile, her oldest son, Lawrence, left school to look after his two brothers, Sonny and Joe, and his sister, Deborah.

In the midst of these difficult times, there was also the burning question among the children of what had happened to their mother. Even though they were keen to know the answer, the children didn't press their father; he'd already told them explicitly not to ask any questions that might rock the boat, and to accept that their mother had simply gone away.

Years later, Henrietta's daughter, Deborah — then in high school — would confront her father and demand that he reveal her mother's identity and explain what had happened to her.

But all her father would say was, "Her name was Henrietta Lacks and she died when you was too young to remember."

In her own search for answers decades later, the author traveled to Baltimore and Clover, Virginia to talk with Henrietta's distant relatives. She also contacted the doctors involved in Henrietta's case.

Just as the author had hoped, this brought her closer to Henrietta's immediate family. Although they were initially reluctant to discuss Henrietta, they later opened up and stayed in touch throughout the writing of the book.

But it wasn't just the author who gained new knowledge about Henrietta; the family, too, learned a great deal about Henrietta's condition and the contribution she'd unwittingly made to the field of medicine.

Questions, however, still remained: Why were many in the family still reluctant to talk to the author and the medical profession about HeLa?

### 7. Black Americans have a long history of being wary of the medical profession. 

The author's uphill battle in researching Henrietta's life was in large part due to the family's deep-seated lack of faith in her — a white reporter — to tell the story. Also, as mentioned previously, their hesitancy had its roots in a major distrust of and apprehension toward the medical field, because of its history with black Americans.

However, even though there are documented cases of the exploitation of black people by scientists, many of the stories circulating among black Americans were fictional. Since the 1800s, one of the tales shared via black oral history was a particularly sinister one: the case of the "night doctors" that kidnap black people to experiment on them.

As this story had it, doctors from Johns Hopkins would set out in the middle of the night and abduct black people living near the hospital for purposes of research.

In fact, one reason such stories took hold was that white slave owners would use them to scare their slaves, dissuading them from trying to escape. Indeed, they would even dress up in white sheets, posing as spirits who'd infect the slaves, or as doctors who'd kidnap them. Those white sheets would later become infamous as the source of the Ku Klux Klan's hooded cloaks. Although the "night doctors" story was fictional, medical experiments had in fact been conducted on slaves to test new surgical techniques.

In the 1900s, with hospitals and research centers offering money for bodies on which to conduct their research, black people's distrust of the medical field intensified. And the fact that Johns Hopkins was located near a poor black area certainly contributed to local black people's suspicion of the school.

Yet, the myth of the "night doctors" actually obscured the raison d'être of Johns Hopkins: to provide medical care for those who couldn't afford it.

It was this historical tension between black people and the medical industry that drove Henrietta's family's suspicion of doctors, researchers and anyone else interested in Henrietta and her cells.

### 8. Although HeLa cells helped many scientific discoveries, their prevalence threatened much research. 

After distributing HeLa cells to research centers and hospitals all over the world, scientists continued research on HeLa and other cell cultures in the hope that cures for various diseases would shortly follow.

But in 1966, a major problem with the research was revealed.

Geneticist Stanley Gartler had been conducting research with the aim of finding new genetic markers — DNA sequences that reveal the identity of species or individuals.

At a 1966 conference on cell culture, Gartler revealed that, in the process of looking for new markers, he'd discovered that the most commonly used cultures in cell research all had one marker in common. In other words, the HeLa cells had contaminated all the cultures they'd been near.

Although scientists knew that cell cultures could contaminate one another, they did not know exactly what HeLa was capable of, or its effect on other cell cultures.

Not only could Henrietta's cells survive in the air attached to dust particles, but they could also pass from unwashed hands and pipettes to other cultures. And they were strong enough that, once passed on to another culture, they would quickly reproduce and contaminate it.

Gartler's revelation undermined the vast amount of research that had been conducted on what scientists had assumed to be different cell cultures. If all cell cultures had traces of HeLa, then they all shared the same genetic characteristics and were therefore not as different as originally thought. In effect, the research had been a waste of money.

While the majority of doctors continued working on the cultures, a few took Gartler seriously and believed that his findings were correct. Those doctors needed to find a way to identify the presence of HeLa, a need that led them to Henrietta's family.

### 9. The search to learn more about HeLa brought scientists to the Lacks family. 

The widespread contamination of HeLa cells led doctors to try to locate Henrietta's surviving family. Luckily, the family were patients at Johns Hopkins, so there was little difficulty in obtaining their contact information.

The doctors hoped that by taking samples from the family, they would be able to continue research on the contamination, and develop a way to map the human genome.

It was at Yale University in 1973, at the First International Workshop on Human Genome Mapping, that doctors and researchers determined that locating the HeLa contaminant required first locating Henrietta's surviving children.

One of the scientists who had published a paper on HeLa, Victor McKusick, asked his postdoctoral fellow Susan Hsu to find the Lacks family and request blood samples from them.

Hsu contacted Day Lacks and asked him to get his children, Lawrence, Sonny and Deborah together to contribute samples. Hsu also sent a nurse to collect samples from Joe — now named Zakariyya — who was in prison.

While Hsu claims that she told Day the blood work was for research purposes, Day told his children that it was in order to find out whether the children had cancer. Which of them was telling the truth is unclear.

Around this time, Deborah began to worry about getting cancer, like her mother. The arrival of doctors only exacerbated her concern. Since Deborah was nearing the age her mother was when she had cancer, Deborah wanted to have more information about her mother.

She demanded her father give her as much information as he could about Henrietta, her life and her condition. When she didn't get all the answers she was looking for, she went to the doctors at Johns Hopkins, who took the opportunity to request additional blood samples for their study.

And that's how the doctors and the Lacks family came together to discuss Henrietta.

### 10. The HeLa case is not the only one that involved concerns about privacy in cell donation. 

The case of Henrietta and the HeLa cells is certainly exceptional, since Henrietta was unfortunate enough to have a very aggressive form of cancer that nonetheless aided research and development to cure other diseases.

But it's also unexceptional, in that what happened to Henrietta could have happened to others — and in fact did, in at least two similar cases.

The first case involves the Alaskan pipeline worker, John Moore.

In 1976, Moore felt like he was going to die: his belly was swollen and his body was covered with bruises. At 31 years of age, Moore was told that he had a rare and deadly form of cancer: hairy-cell leukemia.

Cancer researcher David Golde at UCLA treated Moore, removing his bulging spleen, which weighed 22 lbs — 11 times more than a regular, healthy spleen.

While Moore continued his treatment, Golde developed and marketed Moore's cells — which he named "Mo" — without informing him.

Once Moore discovered what had been going on, he sued Golde for both breach of privacy and profiting from his own cells without obtaining consent or even informing him.

Ultimately, Golde won the court case and was able to continue marketing the "Mo" cells.

Another case from around the same period involved Ted Slavin. Slavin was born a hemophiliac. As a result, his body had already been producing antibodies for hepatitis B. These antibodies were extremely valuable, both financially and medically.

However, the difference between Slavin and Moore was that Slavin's doctor informed him that he could market his cell line and make a lot of money. So Slavin did just that, while also teaming up with the Nobel prize-winning virologist, Baruch Blumberg, to help cure hepatitis B.

What distinguished both Moore and Slavin from Henrietta was the fact that they were both able to contest the use of their cells. As Henrietta had passed away, she could no longer even claim property rights for her cells.

As you'll see in the next and final blink, the HeLa case has many repercussions for the future, especially when it comes to patients' rights.

> _"And the dead have no right to privacy — even if part of them is still alive."_

### 11. To whom do my cells belong: The right to cells vs. the right to medical research. 

At this point, you might reasonably wonder whether the actions of doctors in cases such as Henrietta's — that is, isolating and marketing a patient's cells without their consent — were illegal or not.

In fact, from Henrietta's day to the time the author was writing (2009), such practices were _not_ illegal.

Why?

The issue revolves around consent and money.

In the United States, there is a growing database of tissue samples. A 1999 report stated that, based on a conservative estimate, there are more than 300 million tissue samples stored in the United States, taken from more than 170 million people. And in 2009, the National Institute of Health (NIH) invested $13.5 million to establish a database for samples taken from newborns.

Today, if doctors want to take samples for research, they require the consent of the patient. However, they do not require consent if they wish to store samples from diagnostic procedures (e.g., the removal of moles for biopsies) for future research.

Supporters of these practices argue that current legislation is sufficient, and that there are enough institutions and committees investigating the issue.

But those who oppose the status quo argue that the patient has a fundamental right to know the purposes for which their cells will be used — in case, for example, they involve ethical issues, such as nuclear weapon testing, abortion, intelligence studies, experiments to find racial differences, and so on.

As for the marketing and commercialization of cells, this will surely continue. But the issue of whether doctors will be required to inform their patients of the potential financial gains is uncertain.

Currently, the greatest worry in this arena is gene patenting, as it concerns the right of ownership and distribution of biological materials.

In 1999, President Clinton's National Bioethics Advisory Commission issued a report stating that federal oversight of tissue research was lacking. And, though they advised that patients should be granted more rights regarding what their cells were used for, they remained quiet about the financial issue and who should profit.

> _"When tissues are removed from your body, with or without your consent, any claim you might have had to owning them vanishes."_

### 12. Final summary 

The key message in this book:

**The cells of Henrietta Lacks– named HeLa — became fundamental to research into a cure for polio and other diseases. While in treatment, Henrietta was not informed, and so she died not knowing the incredible role her cells played in medical science.**

**Suggested** **further** **reading:** ** _The Violinist's Thumb_** **by Sam Kean**

_The Violinist's Thumb_ is an exploration into DNA, the double-helix of life itself. The following blinks chronicle the scientific discoveries that led us to understand DNA and the major role it's played in the emergence of life on earth.
---

### Rebecca Skloot

Rebecca Skloot is an award-winning science writer whose articles have appeared in the _New York Times Magazine_, _Discover_ and others. She has also worked as a correspondent for NPR and PBS. In 2010, Skloot sold the film rights for _The Immortal Life of_ _He_ _nrietta_ _La_ _cks_ to Alan Ball and Oprah Winfrey.

