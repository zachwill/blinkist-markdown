---
id: 56376c7862346500072d0000
slug: the-impact-equation-en
published_date: 2015-11-03T00:00:00.000+00:00
author: Chris Brogan and Julien Smith
title: The Impact Equation
subtitle: Are You Making Things Happen or Just Making Noise?
main_color: FAB732
text_color: 805D19
---

# The Impact Equation

_Are You Making Things Happen or Just Making Noise?_

**Chris Brogan and Julien Smith**

_The Impact Equation_ (2012) takes a fresh look at how companies can successfully introduce a product to a market with maximum impact and keep it relevant over the long-term. These blinks will teach you how to articulate your message, deliver it effectively to your audience and build the trust necessary to keep customers coming back for more.

---
### 1. What’s in it for me?  Make an impact with your company in today’s crowded market. 

If you were to look at the last three years of "app" purchases on your phone, chances are you'd find quite a few that no longer exist. Maybe it was an application that briefly grabbed your attention, but about which you quickly forgot. 

This happens a lot, not just to flash-in-the-pan apps but to other products, too. Many companies seem to succeed in making an initial market impact, yet over the long term, they lose our interest. 

What can a company do to avoid this, and ensure that a product not only makes a solid initial impact but also endures? The secret, as these blinks will show, is to follow the _impact equation,_ a simple formula which any company or entrepreneur can follow to become a success.

In these blinks you'll discover

  * **why simplicity is the key to success;**

  * why a handwritten letter is better than 1,000 tweets; and

  * why every marketing executive needs to think in terms of patterns.

### 2. There’s a handy equation to maximize your product’s impact; it relies on two key elements. 

With the immediacy of social media and the rapid production potential of three-dimensional printing, it's never been easier for companies to bring products to market. 

But once you've entered the market, how can you ensure that your product stands the test of time?

Here's where you need the _impact equation_. The impact equation is a formula for attaining and maintaining the product success you want. It's simple; here's the formula.

_IMPACT = Contrast x (Reach + Exposure + Articulation + Trust + Echo)_

But that's a mouthful. It's easier to remember as _IMPACT = C x (R + E + A + T + E)._

To understand the equation, let's start with _exposure (E)_ and _reach (R),_ as they're the two elements that are the most essential in making your product's presence resonate in a market. 

So what do these elements actually mean?

Exposure is a marker of how often your audience hears your message. Reach measures the size of your audience. You'll need to maximize both to succeed!

One way to increase your _reach_ is to take your business international, pushing your product to different countries and markets. US President Barack Obama once quoted a line from _The Alchemist_, a book by Paul Coelho. The president's quote garnered worldwide attention, but primarily because Coelho's book was translated and available in more than 170 countries. Similarly, for your message to resonate on an international level, people in many countries already need to know about your product.

Separately, you can use _exposure_ to heighten people's expectations, for example by limiting exposure to rare public appearances in order to build excitement. 

Apple's yearly product presentations were managed down to the most microscopic detail, so that when CEO Steve Jobs took the stage, you could almost hear the world hold its breath. This sort of exposure guaranteed ideal buzz for the next Apple gadget. 

So now you know how to expand your reach and increase your exposure — let's explore the impact equation's other elements.

### 3. To be successful, you need to establish trust and an emotional connection with your audience. 

In addition to _exposure (E)_ and _reach (R),_ there are four more elements to the impact equation.

_Trust (T)_ is self-explanatory, in that you want people to have faith in your idea or product. To build trust, you need to offer a credible product which a potential customer can believe in. Sure, plenty of companies claim their product is the "best in the world" or capable of "changing your life," but such claims can ring hollow; customers tend to discount them.

Once you have _trust_, you can use _contrast_ (C) to make your idea or product stand out in a crowded marketplace. This requires striking a balance between being familiar yet innovative: if your product is too commonplace, people might have a hard time making a connection with it; yet the same goes for ideas that are too abstract or spectacular. 

Director George Lucas in _Star Wars_ built a story around familiar themes such as war and heroism yet for contrast, set the story in a galaxy "far, far away" where people battled with light sabers and the power of the "Force." The result was an accessible yet unpredictable film experience.

Contrast is the most important part of the impact equation, because without it, no one will notice your product, making all the other factors useless. This is why contrast is used as a multiplier for the other factors.

The next two aspects go hand in hand. _Articulation (A)_, that is, describing your product accurately and eloquently, is inseparable from _echo (E)_, the resonance your product finds with its audience. 

Simply put, if a customer can understand and remember your product then your articulation is spot-on. To improve articulation, companies often employ stylistic devices, such as clever names or word play, to help consumers remember. 

Choosing particular sounds or repetitions, called alliteration, such as with Coca Cola or PowerPoint, is one simple way to improve articulation. 

Once you've got your audience's attention, you'll need to deepen your message to make it stick. Echo essentially is about communicating a message that relates to what people feel themselves. 

For example, rapper Eminem sings about familiar themes, such as being broke, in his music. In his popular song, "Lose Yourself," such themes help connect what he feels to what his audience feels.

### 4. Harness the power of patterns to help make your product or idea stick in the minds of customers. 

Every year, hundreds upon hundreds of new companies enter the market. Yet how can each business make a meaningful connection with each and every customer? 

A product needs to find a way to stand out from the pack, have something that "rings a bell" in a potential customer's mind when she sees it. One way to ensure this is to harness the power of patterns. 

Pattern recognition is a process in which a person categorizes new information according to a pattern to optimize understanding. A crucial element in the study of cognitive psychology, pattern recognition shines a light on how we think as well as the stereotypes and prejudices we hold. 

For example, it is widely held that Apple products are easy to use. But do you know why?

The technology giant employs familiar psychological patterns in its product design to facilitate an easy, step-by-step operation that is nonetheless sophisticated. 

For instance, Apple's graphic symbols and interfaces often mimic real tools, such as scissors or rulers, to better help a user connect an action to a pre-existing pattern or connection in his mind. 

Another way to connect with customers today is through the use of social media. Yet companies should be wary — using social media effectively is fraught with challenges, too. 

Too many companies jump to create an online presence without really thinking through exactly what they should be posting. While it's certainly easy to share information digitally, company executives should review carefully posts and announcements before they're tweeted or shared.

For instance, announcing the delay of a product release on your company's Facebook page may seem like good communication, but it might backfire. Followers can quickly share the post, and potentially complain about your company's broken promises — or worse, compare you to a competitor that in contrast is always on time with products.

But that's not the only reason to be wary of social media. Social media can cause companies to obsess too much over appearances and forget why people use social media in the first place: for the joy of real human interaction. 

So be sure to share with your customers information and experiences that are actually relevant and useful to them, as this is what will keep your audience attentive and loyal.

### 5. Choose media wisely; just because social media is easy, doesn’t mean it’s the most effective for you. 

With technology such as social media, it's now easier than ever for companies to connect with customers with just the click of a button. 

But while technology has opened new opportunities, it also comes with a downside.

With so many avenues for communication, customers are now drowning in competing voices. It's not easy for a new or emerging company to cut through the noise. 

In fact, people may have too much choice! For instance, if you want to learn more about global politics, you could pick up a newspaper, read a magazine online, watch a video on YouTube or subscribe to any number of newsletters that will appear in your inbox every morning. 

And it's almost too easy for any company to add to the din. Nearly anyone can become a creator of media, just as anyone can now consume it. 

Therefore, as a company looking to communicate effectively, you need to be clever about how you reach your audience. But how?

By understanding all your media options and how you can best use each. While digital media is important, don't forget about other key media channels — traditional options such as printed advertising or billboards can still make an impact. 

Apple invests undoubtedly millions of dollars in billboard advertisements, plastering its brand images on large buildings in major urban centers. Sure, the average passer-by may not notice; yet such grand gestures ensure that a brand idea is stated boldly. Yet a bigger issue is, of course, that such a campaign is only available for companies with serious capital. 

Another thing to remember is that while social media may be quick and to the point, analog media and its personal touch can build greater trust with customers. 

For instance, writing a personal letter to a client or taking the time to speak with a client on the phone are great ways to build stable, long-lasting relationships.

### 6. Help your product make a serious impact by crafting simple, easy-to-understand messages. 

From a soda can to your favorite app, marketing messages in the form of text are looking to grab your attention. A company should ensure that all its words are inspiring and well-chosen. 

But how do you do this?

A company should never discount the value of a simple, easy-to-understand explanation. Many people think that complicated or fancy words equal sophistication, and that simple words suggest something trivial or of no value. 

This couldn't be further from the truth!

To get a customer to connect with your product, you need to grab his attention with a straightforward description — not assault him with long-winded instructions or superfluous details that do nothing but turn him away in boredom or frustration. 

The truth is however that writing a simple yet well-crafted text is far from straightforward — yet you can really give your product and your company a leg up if you always strive for this in your marketing copy. Proper _articulation_ (A) is key to making your message easy to comprehend and remember.

And since you know that people use patterns to make sense of the world, it's easy to see how a great piece of writing can enhance a reader's interest by offering unexpected patterns. 

For example, William Shakespeare once wrote, "Love all, trust a few, do wrong to none." What makes this aphorism so effective?

First of all, it points out exactly three things, which makes it memorable. But the quote is also short and easily understood; and importantly, will make big changes in the lives of anyone who takes it to heart. 

In fact, proper articulation can be the missing piece an idea needs to finally take hold and become a success. Stephen Hawking's book, _A Brief History of Time_, is a great example. The book didn't break new scientific ground but its success was a product of Hawking's peerless ability to condense complex ideas into simple language.

### 7. Creating excitement about your project is important, but be sure not to overwhelm your clients. 

So you've got every element in place to ensure your company communicates effectively. Now it's time to apply your knowledge to the real world by taking your idea public.

You do this through _exposure_ — the key to building awareness and a positive reputation for your product or project. 

At the start of any career, your most important goal is to build trust, something you can accomplish by simply introducing yourself as a person. 

You can share your background with clients, explaining to them your path thus far and even how you feel personally about certain issues. Just be sure to connect everything back to your current goal. 

After a while, your clients will know you and trust you. When you get to this point, they'll keep coming back to you for business on their own — and you won't need to say a thing. 

But to achieve this, you need to ensure you're in touch with clients at the right time and with the proper frequency. That's because people can grow annoyed with your product if the timing or even the message goes against what they expect in the moment. 

For instance, holding a free webinar on a topic that's complicated without giving your audience basic information as a primer could cause problems. Customers might feel overwhelmed and put off by the high level of information, and decide to abandon both your webinar _and_ your business. 

So _what_ you tell clients is important; but it's also crucial to moderate _when_ you reach out. 

For instance, it's important not to overdo media appearances. If people see your face every day, they might start taking you for granted or worse, consider you an "attention hog" and come to think of your products as holding no real value.

### 8. Final summary 

The key message in this book:

**Making an impact isn't easy, but thankfully there's a handy equation to help you find the best way to make the impact you want. By applying the impact equation C x (R + E + A + T + E), you'll achieve results and reach your goals in stride.**

**Suggested** **further** **reading:** ** _Platform_** **by Michael Hyatt**

The book offers detailed information on how to create compelling products, then establish an online platform to get visibility for them. These blinks will walk through the process the author, Michael Hyatt, went through to build a successful career, by mastering social media channels like Facebook and Twitter. This book will show you how you, too, can monetize your social media platform and even turn it into a career.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Chris Brogan and Julien Smith

Chris Brogan and Julien Smith are consultants and online communication experts, and have worked with many leading companies such as Google, Microsoft and American Express. Their work has been praised by self-help gurus Seth Godin and Tony Robbins.

