---
id: 57617f3a0beabd0003161b2e
slug: the-importance-of-being-little-en
published_date: 2016-06-23T00:00:00.000+00:00
author: Erika Christakis
title: The Importance of Being Little
subtitle: What Preschoolers Really Need From Grown-Ups
main_color: 7E3D2B
text_color: 7E3D2B
---

# The Importance of Being Little

_What Preschoolers Really Need From Grown-Ups_

**Erika Christakis**

_The Importance of Being LIttle_ (2016) is all about the powerful and curious minds of children, and the way our current preschool educational standards of testing and standardized worksheets limit their natural skills. These blinks explain why a focus on the personal development of each child is a much better strategy.

---
### 1. What’s in it for me? Let’s make preschool great again. 

Childhood is far too short. Before long, the little boys and girls running around without a care in the world will become serious-minded adults with deadlines to meet and bills to pay.

This is exactly why we should let children enjoy their all-too-brief phase of happiness and wonder. In the past we largely did; children were allowed to play, explore and discover.

These days, however, children are sent off to preschool at a very young age and are forced to "learn." In this environment, they aren't treated as kids, nor are they given room to explore and develop; they are treated like little adults, made to sit, listen and do what they're told.

These blinks will help explain why this happens, and what we can do to make preschool great again.

You'll also learn

  * why the No Child Left Behind program actually held most children back;

  * which professions pay the same as teachers; and

  * why teaching young children how to read a calendar is a pointless exercise.

### 2. Modern preschools aren’t suited for the natural skills and curiosity of children. 

If you could walk through the door of a preschool 50 years ago, you'd probably see kids having fun and playing around. But today, you'd be greeted with young children, rigorously studying math and grammar.

So, what has changed?

Simply put, the style of teaching has shifted dramatically. These days, preschools are all about strict uniformity at the expense of real learning. In fact, preschools in the United States operate according to the _Common Core State Standards_, or _CCSS_, a state-mandated set of goals for all subjects.

For instance, the CCSS says that kindergarteners should be able to "demonstrate command of conventions of standard English grammar." In other words, all preschoolers have to work toward this goal, instead of focusing on the actual process of learning.

The problem is that this one-size-fits-all approach to education means that kids don't receive enough attention for their individual needs and capabilities. After all, education that operates according to such strict guidelines denies students the opportunity to learn on their own terms, and with the help of a teacher who can recognize their individual talents and learning styles. It also creates clear distinctions between who does well and who doesn't.

But if it works so poorly, why is this system still in place?

The strict curriculum that shapes American preschools is, in fact, the result of social and political changes. For instance, before the 1980s, preschool was relatively uncommon; but as women began entering the workforce in greater numbers, preschools rose in popularity. Initially, they functioned as a form of daycare rather than the rigorous learning environment that characterizes them today.

However, as inequality spiked over the ensuing 30 years, so did the gap in educational achievement between the rich and poor. The result was the _No Child Left Behind_ legislation of the early 2000s.

This law was an attempt to close the achievement gap in society by standardizing education. To do so, it implemented a strict, universal curriculum designed to arm every preschooler with the same skills.

### 3. Preschools have adopted teaching styles built on the needs of adults, not children. 

In any given business meeting you'll see a bunch of adults sitting around being quiet — and if you go to a modern preschool, you'll probably see the same thing. Today's preschools are meant to meet the needs of parents, not children.

A driving force behind this change is the increasing anxiety and responsibility parents feel for the safety of their kids. Over the last half century, public health data has become more easily available. Naturally, parents armed with this information have grown increasingly worried about the safety and well-being of their children, which is why they want preschools to be safe places. And what could be safer than just sitting around quietly?

So, while this trend has resulted in a 57 percent decrease in the accidental deaths of children aged one to four between 1960 and 1990, it has also produced a distrust of child-centered learning; parents think it's unsafe and inefficient.

Not only that, but parents also expect a lot more from their children's education, which has led to a focus on teaching instead of play. In fact, studies have found that families with the lowest incomes, and ones with little to no education, are the most opposed to play-based education.

Instead, these families favor curriculums founded upon preparatory academic skills. After all, they want their kids to get the best education possible and think that "proper" teaching is how they'll get it.

The result of this adult focus on academic achievement is clear when you look at the style of _Direct Instruction_, a teaching method used in schools across the country. This passive teaching technique essentially has the teacher tell his students what they need to learn. The problem is that this method of instruction simply doesn't engage young children.

For example, most preschool teachers will teach the days of the week and months of the year on a daily basis, perhaps by asking the students how long it is until a certain date. However, it has been documented that children tend to forget such details and don't think in terms of weeks, months and years like adults do.

### 4. Adults forget what it’s like to be a child and underestimate the degree to which children know what they need. 

So, the needs of adults trump those of children when it comes to early education. But this is really a result of our general inability to see that every environment is a learning environment for young children. Kids are remarkably adept at teaching themselves what they like.

In fact, by forcing a strict curriculum onto creative children, we actually damage this natural ability. So why do we do it?

It all boils down to a poor understanding of children's cognitive capabilities. Kids, even very young ones, have an incredible ability to learn. For instance, babies of just ten months can tell when they're being spoken to in different languages.

As society has learned more about this ability, the response of adults has been to cram more and more information into the brains of their kids. However, this misguided strategy misses the fact that children learn through fun, and not through rigor.

For instance, when kids play games, they learn how to interact with others, share and wait for their turn — all vital social skills. But the best part is that kids engage with these skills more than those learned in books, simply because they enjoy playing.

Unfortunately, adults have missed this simple fact; preschools, which once prepared children for their education, are becoming more like real schools.

It's easy to see this poor understanding of children's cognitive abilities in how we measure their success and categorize their behavior. When we measure the abilities of children, we use the same criteria as we do for adults. The result is the knee-jerk, unfair labeling of kids.

A child who doesn't respond to a particular learning environment, or who develops at a different rate, might end up being diagnosed with ADHD when it's simply not the case. This incessant pigeonholing creates a misinformed sense of right and wrong, or good and bad, very early on in a child's life.

### 5. Poor educational funding limits possibilities for improvement. 

Today we know more about children's minds than ever before. We also clearly understand the importance of high-quality education. But then why aren't we allocating resources to make these ideas a reality?

The fact is that teachers today have the hefty responsibility of closing the achievement gap and educating future generations, yet they remain some of the worst-paid workers in the country.

This is important because, as Steven Barnett of _The National Institute for Early Education and Research_ showed in his study _Low Wages = Low Quality_, low teaching salaries are directly correlated with low-quality teaching. The plain reality is that certified preschool teachers make just a little more than a truck driver or bartender, and rarely even receive health benefits.

But low salaries aren't the only problem; we're also failing to allocate enough money for teacher training, especially for those teaching young children.

As a result, direct instruction is utilized as a passive but common approach that can be lazily trotted out by those who don't know how to connect with young kids. The issue is that these teachers haven't received the training they need to excel; all they are left with are hopelessly inferior teaching methods.

Finally, the lack of funding in general for education has resulted in poor curriculums. Individual care and attention to students' social or developmental needs are extremely effective ways to close the achievement gap. Even so, no substantial funding is allocated to implementing such strategies.

The result is that many teachers use vocabulary lists to measure success in a standardized, cost-effective way, instead of developing students' language on an individual basis according to their interests.

Furthermore, many teachers end up making compromises on their students' education simply to meet the rigid targets and standards that their licenses and funding depend on.

> _"We are smothering young children with attention and resources, and yet, somehow we are not giving them what they really need."_

### 6. Trusting relationships with instructors and an active learning environment that incorporates play are key to a child’s development. 

You might not think that the time you spent playing at recess taught you anything. But the truth is that play is a fantastic way for children to learn — and the best part is they actually enjoy it.

So, while there's a common misconception that play is the opposite of book learning, play actually serves a critical function in the development of children. For instance, play is essential to building cognitive abilities like memory.

As a result, play is utilized by virtually all mammals to build survival skills. In fact, more intelligent mammals like chimps and elephants play even more than others. Much like humans, they rely less on instincts and more on learning through practice and experience.

As well as an environment that facilitates play, it is vital that trust can built up between teachers and children. A strong relationship between a teacher and child will produce a more positive learning experience as the child interacts freely with the teacher and the subject they are teaching.

For example, a teacher might present a question to the class, perhaps related to something a pupil has asked about. The teacher can then urge the children to come up with an answer together. The instructor can help by reiterating knowledge the children already have while making sure that the kids lead the discussion.

Essentially, instead of teaching kids the answer, the instructor actively involves the students in figuring it out. This technique works well across learning levels; the simple fact is that putting the engagement of students first helps them develop at their own speed.

It's clear that young children are much more capable of self-motivated learning than we once thought. Even so, we still struggle to apply this knowledge to education, largely due to poor funding.

> "Play's survival function is so obvious that the absence of play has long made good fodder for childhood nightmares."

### 7. To improve preschool education, the child’s personal development should be the focus. 

Would you rather the children in your life had an enthusiastic passion for learning or an ability to recall facts? Most people would opt for the former — but how can we guide the future of education in this direction?

We can start by making sure that childcare is informed by the needs of children and focused on their personal development. To do so we need to visualize our goals for young kids and build the learning environments necessary to realize them. At the core of this process is the need to move away from an academic preschool curriculum to one that puts the development of children first.

For instance, we should teach skills over fact-based knowledge because they are more widely applicable and can be adapted by a child to fit her needs. In addition, skills like organizing, problem solving and communicating in a group can be taught in daily life as well as in the classroom.

However, reforming educational funding is an essential first step because a focus on individual children requires resources, like time, high-quality teacher training and access to classroom materials.

But why is investing in the goals of improved preschool education worth your while?

Well, preschools need reworking because they form the basis of our whole learning system and will profoundly affect the achievements of older students. For instance, the Finnish preschool curriculum emphasizes the child's independence, well-being and behavior towards others. It's no coincidence that an international comparison found Finland to have the highest secondary school performance.

Furthermore, teaching _transferable skills_ through age-appropriate activities like play and games can mean stronger educational performance down the line. A child who learns to organize herself at a young age will have a much easier time planning and performing well later in life.

Putting the personal development of children at the forefront of their education will make for more nurturing and appropriate learning environments for students of all ages.

> _"Learning and love are mutually reinforcing concepts in the mind of a growing child"_

### 8. Final summary 

The key message in this book:

**Active learning and trusting relationships are the most effective base for a preschool education. So, while play-based learning might be considered a frivolous alternative to academic curriculums, there's clear evidence that shows the benefits of play within the broader learning process.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Screamfree Parenting_** **by Hal Edward Runkel**

_Screamfree Parenting_ (2007) is your guide to a better relationship with your children. From the space and boundaries that a child needs, to the trust and love you need yourself, these blinks shed light on the principles of screamfree parenting.
---

### Erika Christakis

Erika Christakis is a parent and a teacher who, in addition to teaching preschool, has experience in the public health sector. She currently works in the field of child development and education policy at the Yale University Child Study Center.

