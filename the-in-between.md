---
id: 530335606431310008270000
slug: the-in-between-en
published_date: 2014-02-19T08:05:45.000+00:00
author: Jeff Goins
title: The In-Between
subtitle: Embracing the Tension Between Now and the Next Big Thing
main_color: B3514F
text_color: B3514F
---

# The In-Between

_Embracing the Tension Between Now and the Next Big Thing_

**Jeff Goins**

Most people spend their lives impatiently waiting for the next big thing to happen. But what if those moments between big events were actually hugely influential on our identities and lives? What if instead of focusing on those big events, we slow down and savor those in-between moments? _The In-Between_ (2013) comprises a collection of Jeff Goins' life-changing experiences that happened in the in-between, which he would like to share with readers.

---
### 1. Let go of your expectations and be present in the life-changing moments around you. 

When Jeff became a father, he came to realize how important each and every moment is. He understood that children grow up so fast that if you miss even the shortest moment, you actually miss a lot.

This became especially clear to Jeff one night when he was trying to get his infant son to go back to sleep. It was the middle of the night, and Jeff was eager to just get it over so he could also get some sleep. On this particular night, however, his son simply refused to fall asleep again, laying waste to Jeff's hopes of getting a good night's rest.

Finally, Jeff gave up; it looked like he wasn't going to get any sleep at all, so he decided he'd better just get used to it. He took his son downstairs to the living room, and they sat there playing in the middle of the night. All of a sudden, his son started smiling and speaking, using new words he had never used before.

Astounded and overjoyed by this powerful, happy moment, Jeff realized that he could have missed it very easily. If he had stuck to his expectations of getting back to sleep quickly, he would have never brought his son downstairs.

The fact that he let go of his expectations and the desire to control his own sleep allowed him to share this moment with his son. This is a potent reminder of why we should always strive to be present in what is happening right at this moment, instead of trying to force it to go as we expected it to.

### 2. Instead of trying to control what happens to you, focus on the people you are with. 

Another important life experience for Jeff occurred with his rock band, when they spent almost a year touring North America and Taiwan.

Jeff had high expectations for the tour, dreaming of adventure and rock stardom. Often, however, the reality of the tour was less glamorous and more gruelling: some nights would involve 12-hour van rides to play for just 30 minutes in front of a crowd of as few as 20 people.

Late one night, the band rolled into a small town in Montana, starving after the long drive. They expected at least a few restaurants to be open, but frustratingly it turned out that this was not the case in this small rural town.

They were staying overnight at a local pastor's house, and he, seeing their hunger, took them to his church and showed them a pantry full of donated food. The band protested of course, but the pastor insisted on making a meal out of the food for them, because at that moment they were exactly the "needy" the food was meant for.

They ended up having a very simple but delicious meal together at the pastor's house. Their frustration and hunger had turned into a great evening where they enjoyed each other's company.

It was then that Jeff realized that by focusing on his expectations for the tour, expecting the life of a rock star, he was missing the fact that the band was growing very close to one another. They were forming a close community, almost like family.

The year was not so much about the experience of touring in a band, but more about the people Jeff was with and how they changed him. The same is true for most experiences in life.

### 3. What you are becoming is more important than what you’re doing. 

Another life-changing experience for Jeff was when, as a college student, he spent an exchange semester in Seville, Spain. Setting off for the journey, Jeff expected and desired an adventure. He wanted his exchange year to provide good stories to tell, and therefore went out almost every night, immersing himself in Seville's nightlife and flamenco clubs. 

Jeff stayed with his "host mother" Loli — a local woman who took him and another exchange student, Daniel, into her home. While Jeff went out to make the most of his new surroundings and Spanish culture, Daniel stayed in with Loli most nights, preferring to watch TV. Jeff felt this was boring in contrast to his own expectations of adventure: Daniel was missing out on great stories.

Then one night, exhausted by the hectic pace, Jeff decided to make an exception and stay in and watch TV with Daniel and Loli.

In the midst of an incomprehensible Spanish TV show, Loli burst out laughing at something one of the characters said. Jeff and Daniel, though they couldn't understand what was so funny, were so amused that they also started to laugh. The laughter was infectious, and soon all three were laughing so hard tears were streaming down their faces. They ended up laughing together for hours.

This one night in front of the TV made Jeff realize that he had been too focused on what he wanted to do and experience in Seville to realize that he actually had a community of people around him: Loli and Daniel. And it is this community that has the most profound influence on what kind of person you become. Subsequently Jeff slowed down his wild pace of life in Seville and began to enjoy time spent with Loli and Daniel at breakfast, Bible study and, of course, evenings in front of the TV.

### 4. Learn to pay attention so you can embrace and savor every moment as well as appreciate the people in your life. 

After his exchange semester, Jeff was making his way back home to spend the holidays with his parents. However, after the excitement of Seville, he was not looking forward to his return to "boring" Illinois and the humdrum of his parents' house. In fact, he was going more out of obligation than free will.

Jeff did not have a car in college, so he wound up going home by train — a long ride, but at least it had an allure of adventure to it. However, unlike his grande experiences travelling by rail in Europe, he found the train home was tattered and the scenery extremely boring: one gray town and empty cornfield after the other.

This is why he was surprised when a fellow passenger, a florist, pointed out of the window and exclaimed, "It's beautiful!"

Jeff naturally wondered what the florist was talking about: an empty cornfield is not especially beautiful.

Later the florist said something that stuck in Jeff's mind: "There's nothing like going home."

Chatting with the florist, Jeff surmised that he was in fact a widower heading home to see his daughter and visit his wife's grave.

Jeff's mindset had been that going home was just an unavoidable obligation, made worse by the dull scenery, but the florist was in fact subtly encouraging him to appreciate the people around him while he had the chance. The people who create a place called "home" for you may not always be around. 

The florist also showed Jeff how to appreciate the ordinary yet beautiful things that are all around us in everyday life, like the cold, barren beauty of cornfields in the winter.

### 5. Be grateful for the little things in life. 

For a few years after they had gotten married, Jeff and his wife attended a small church in Franklin, Tennessee. There they met two couples who taught them about the importance of gratitude for the small things in life.

Firstly, Jim and Lois were an elderly couple who were prominent members of the church. Even though she was friendly and energetic, Lois' health was failing, and it was clear she might not have much time left.

Lois loved to fish, so one day, on the spur of the moment, her husband Jim took her fishing to Virginia Beach, 700 miles away. On the road, he stopped at a gas station to fill up his big RV. A fellow customer at another pump saw the RV and exclaimed, "Wow! I bet it costs a fortune to fill up that thing, especially in this economy!"

Jim kept pumping gas, shrugged and told the man that he did not know how much time he had left with his wife, and therefore he was going to enjoy every last remaining moment — no matter what it cost.

The church held a special service around Thanksgiving where people were encouraged to explain what they were thankful about. Lois got up from her wheelchair to speak to the parishioners about how grateful she was that she had been able to live another year and spend that time with her husband and family.

The second couple, Madelyn and Al, were also church members and living through difficult times, as Al's health was deteriorating. At the very same Thanksgiving service, Madelyn got up to speak. She explained that although it had been a difficult year for Al, she was grateful for every hug, tear and moment they had together.

Both Al and Lois passed away, but their legacy stuck with Jeff: life is a gift, and one must relish even the smallest things, for who knows how many days any of us have left.

### 6. Be grateful for and celebrate the big moments in life. 

From early on, Jeff was never a big fan of the idea of marriage. He felt that the actual act of having a wedding and getting married was a bit of a pointless ceremony; what mattered was what came afterwards.

This is why when it came to his own wedding ceremony, he did not really have high expectations. He figured it would be just another day. When he mentioned this to a friend, however, Jeff was surprised to hear that the friend felt his own wedding day had been "the best day of his life."

This stirred the idea in Jeff that perhaps the wedding, and by extension the marriage, could actually be something more significant than he thought.

When his actual wedding day arrived, Jeff stood at the altar with his best man at his side, waiting for his fiancée to walk down the aisle. To his surprise, when she emerged, instead of hearing the classical wedding tune they'd chosen, he watched as his best man left his side, grabbed a guitar and started singing a song Jeff had written for his wife.

This took Jeff totally by surprise. Watching his wife-to-be walk to him with his own words of promise and commitment to her being played in the background, Jeff started crying. He found the moment so beautiful and powerful that he could not stop.

Looking back, despite those low expectations, it really was the best day of his life. This experience taught him the importance of celebrating those truly big events in life. Sometimes the things you have to wait for for a long time really are the best.

### 7. Find reasons to be grateful even for the disappointing moments in life; this will give you hope. 

Just as it is important to be grateful for the small things and the big moments in life, it is in fact also important to try to be grateful in disappointing times.

Why? 

Because often when you look back on them later on, you find those sad or negative events actually had some positive influence on your life after all. Perhaps they taught you an important lesson or otherwise molded your identity in a big way somehow?

Jeff discovered this lesson after his grandfather Charles, a true renaissance man of the arts, had passed.

Jeff, then just 16 years old, was there at Charles' deathbed. Though Charles was never a religious man, a priest was called to give him his last rights. When the priest spoke to Charles about accepting God into his life, Charles surprised Jeff by robustly agreeing, "Yes, I do," as tears rolled down his cheeks.

Jeff intuitively understood that his grandfather was not just agreeing out of fear or physical discomfort, but instead something profound and significant was happening: Charles was having a sincere religious conversion, almost at the last possible minute.

Years later, this event touched the lives of many more people: Jeff's band was playing a concert at an old people's home in Taiwan, when Jeff was asked to give the seniors an impromptu sermon. Short on stories he thought would be relevant to the crowd, Jeff spoke of his grandfather's passing, and how it demonstrated that it's never too late to make life-changing choices.

So even a sad and tragic experience like his grandfather dying, in retrospect, had some good in it. The experience allowed Jeff to inspire and comfort those elderly people in Taiwan.

This phenomenon gives us hope: even when dealing with the most disappointing or negative events imaginable, we can hope that a silver lining will emerge sometime, perhaps years down the line.

### 8. Final summary 

The key message in this book:

**Most people spend their lives waiting for the next big thing to happen, but in fact, life really happens in those mundane and ordinary moments between big events. We should learn to slow down, let go of our illusion of control, savor those in-between moments and appreciate the people around us. They have a bigger influence on our lives and identities than most of us realize.**

What are the questions the book answers?

**Why is it important to let go of your expectations and stop trying to control the things life throws at you?**

  * Let go of your expectations and be present in the life-changing moments around you.

  * Instead of trying to control what happens to you, focus on the people you are with.

  * What you are becoming is more important than what you're doing.

**Why should you learn to slow down and embrace each moment?**

  * Learn to pay attention so you can embrace and savor every moment as well as appreciate the people in your life.

**Why should you be grateful for every moment?**

  * Be grateful for the little things in life.

  * Be grateful for and celebrate the big moments in life.

  * Find reasons to be grateful even for the disappointing moments in life; this will give you hope.
---

### Jeff Goins

Jeff Goins is a musician, author and blogger who has written for over 100 magazines, publications and blogs.

