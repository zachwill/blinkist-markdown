---
id: 55fff1dd615cd10009000030
slug: the-inclusion-dividend-en
published_date: 2015-09-21T00:00:00.000+00:00
author: Mark Kaplan and Mason Donovan
title: The Inclusion Dividend
subtitle: Why Investing in Diversity and Inclusion Pays Off
main_color: DB2C30
text_color: B22427
---

# The Inclusion Dividend

_Why Investing in Diversity and Inclusion Pays Off_

**Mark Kaplan and Mason Donovan**

"I'm not sexist, it's just that the top candidates are all guys." "I don't believe in quotas — surely you should pick the best person for the job, whatever the colour of their skin?" If you've ever had thoughts like these, challenge your assumptions with _The Inclusion Dividend_ (2013). It explains how diversity and inclusion actually boost the productivity and business success of any company, while presenting a guide to transforming your workplace for the good.

---
### 1. What’s in it for me? Discover the financial benefits of making inclusion a key word at the office. 

We have all heard of people being left out at work for reasons which have nothing to do with their skills, but with their gender, where they're from or their religious beliefs. This is usually viewed as an ethical problem, but what if it's also a business problem? What if a lack of inclusion is actually bad for your bottom line?

In _The Inclusion Dividend_ the authors show how biases and a lack of inclusion influences more parts of business than we might think. They also provide us with ideas and programs to tackle the issues and create a more inclusive environment that isn't only good for employees, but also good for business.

In these blinks, you'll discover

  * why MIT professor Sendil Mullainathan sent out 5,000 applications for jobs;

  * how Apple extended its inclusion program to vendors; and

  * why two million of America's best and brightest left their jobs in 2007.

### 2. While a meritocracy may be seen as the basis for successful businesses, the numbers suggest otherwise. 

Have you ever had the disappointing experience of putting in long hours and being the best in your workplace, only to discover that success doesn't always boil down to hard work? That's because while the _idea_ of meritocracy is deeply engrained in the business world, it might not be the _reality_. 

But what is a meritocracy exactly?

It's the long-held belief that hard work will be rewarded. In fact, it's even the basis of the American Dream — the idea that anyone can reap the benefits of a rewarding and comfortable life by putting in the necessary work. 

So, if you asked a board member of a Fortune 1000 company whether their corporation abides by a meritocratic system they would naturally say yes. They would maintain that employees are hired, promoted and kept on board solely on account of their abilities. 

Unfortunately, this claim doesn't quite align with reality. For instance, in the United States, about half the population is female, but a census conducted in 2010 by Catalyst found that women only accounted for 15% of all executive positions at Fortune 500 companies. When women were asked why they thought this was, they said the problem was due to a lack of opportunities for promotion and respect.

So, now you know a meritocratic system doesn't exist, but did you know that low diversity is actually bad for business?

Just consider the fact that global corporations spend about $8 billion annually to foster a more inclusive corporate culture. They don't just do it to avoid discrimination lawsuits, but because it makes them money. 

For instance, take the companies listed on Standard and Poor's 500 stock index. They're profitable companies that brought in an 89% return on their investments over a three-year period. But the S&P corporations that also made it onto _Working Mother's_ 100 top companies for inclusion brought back 98% on every dollar invested!

### 3. Predispositions can negatively affect your business. 

Have you ever been left wondering why you didn't land an interview even though your application was loads better than your competitor's? You might have been a victim of _unconscious bias_. 

What's that?

Well, _bias_ is an ever present human characteristic that's hard to avoid; it's a prejudice that you hold against other things or people. For instance, you might be biased toward thinking your home sports team is the best because you grew up rooting for them, even though you know they're not so great. 

Biases help you navigate the world by allowing you to rapidly react based on instinct. But while having an opinion without conscious thought is helpful for simple decisions, it can also have negative impacts, especially if you're unaware of your biases. 

Which brings us to unconscious biases: the judgments you make about others without being aware of why. For instance, an employee might earn a promotion simply because he went to the same college as his boss. The boss might not consciously intend to make choices this way — he might just have positive associations with his alma mater. 

But unconscious biases can cause bigger problems when they spur behavior that influences the workplace. That's because biases don't just affect your thoughts, they affect your actions too. For example, the MIT professor Sendil Mullainathan conducted a study in which he sent out about 5,000 resumés for 1,300 jobs. 

The goal was to determine whether highly skilled applicants would receive differential treatment simply based on changing their names. So, while one group of applications bore names that sounded Caucasian, the names of another group sounded African American.

Here are the results.

For one, the white-sounding applicants had a 50% higher chance of landing an interview in general, but were also more likely to get an interview than highly skilled applicants with names that sounded African American. It was even true for companies making an active effort to diversify.

So, unconscious bias might go against your intentions and cause you to miss out on great opportunities, like hiring star employees.

> _"Taller men earn, on average, about $800 more a year per inch of height from five feet seven inches to six foot three inches."_

### 4. An exclusive environment creates counterproductive group dynamics. 

Most people are right handed and few right-handers ever consider what life is like for their left-handed peers — everything from can openers to tape measures are awkward to use. It's just one example, but being unaware of more common differences exacerbates _insider-outsider_ dynamics.

What's that?

Well, every team within a greater organization is divided into insider-outsider groups based on any number of factors like age, sex, class, what college you attended and even where you grew up. As a result of these groupings, a power dynamic emerges in which the people in the insider group dominate those in the outsider group, in other words, those who are different. 

For instance, a common inside group in many companies is comprised of men who attended Ivy league schools. Members of this group are more likely to give orders and tend to have more say in company decisions than women or people who didn't graduate from an Ivy League university. 

The issue is that being unaware of insider-outsider dynamics can cause imbalances in the workplace. For example, say a meeting is held in which female employees brainstorm some great ideas, but their genius is overshadowed by their male peers who disproportionately engage with one another. 

Failing to address such power dynamics won't just cost you brilliant ideas, it will also render your workforce unstable and dissatisfied. That's because strong insider dynamics result in both decreased productivity and creativity.

For instance, someone in the outside group might understand an issue better than anyone else on their team but be powerless to make productive changes. Just think of trying to break into a female-dominated market. The women on staff would be best equipped with the necessary knowledge but if they're disempowered their suggestions will fall on deaf ears. 

Another issue is that unfair power dynamics cost companies highly skilled employees. For example, a study published in 2007 by the Level Playing Institute discovered that two million professionals who were not white, heterosexual men, quit their jobs every year as a result of such power dynamics.

### 5. Inclusion initiatives can make companies more diverse and successful. 

So, a lack of diversity is bad for your company, but you can change that by looking within your firm as well as beyond it. 

How?

By using an _Inclusion Initiative Phases_ or _IIP_ model, a technique the authors developed for ensuring long-term, sustainable inclusion at any organization. 

Here's how it works.

The program is comprised of four phases designed to increase the diversity and inclusivity of your company. The four steps build a structure through which any company can forge their _own_ path to inclusion, and it's easy to get started:

Phase one is called _Research and Needs Identification_. In this phase you'll gather information through employee engagement surveys, focus groups and interviews to better understand your staff. 

Phase two is _Strategy Creation_, and to build a good strategy you need strong data, a clear path to profitability, and finally, a realistic plan for implementation. In the best scenario you'll have a diverse team overseeing the process from start to finish. 

Phase three is _Implementation and Integration_, a time to put your strategy into direct action. The key to this phase is to ensure that your employees don't view the activities they're made to participate in as a burden. 

In Phase four you'll conduct _Measurement and Re-calibration_ to constantly evaluate what works best about the other phases, especially number three. Once you know what techniques are most effective you should amplify them while letting go of those that failed. 

What will you gain by doing all this?

Well, when inclusivity increases the impact isn't just felt in the office. It also affects the ways you can market your company. That's because customers are the most important aspect of any business venture and in many ways reflect the companies they buy from. 

For instance, business research commonly shows a correlation between the experience of employees and that of customers. Therefore, if you only employ white men, you're likely to only have white male customers. On the other hand, a diverse company will have more diverse customers and be more successful!

### 6. Inclusion is best implemented by company leaders. 

So you're ready to make your company more inclusive, but it won't happen without some effort. While inclusivity is a group effort, it's best implemented from the top down, and to change their business, company leaders need to change themselves. 

That's because growth in the modern era means reaching a global market that's both diverse and complex. To do so you'll need to develop interpersonal relationships with all kinds of people.

For instance, Apple embraced inclusion to this end. In 1993 their executives expanded their pre-existing diversity programs by adding a supplier diversity initiative, which required the company's suppliers to also adhere to certain diversity standards. 

The result?

In 2011 Apple was bringing in the largest profit per employee of any company in the world, with each employee generating $400,000 a year. Not just that, but because the inclusivity encompasses program employees, vendors and suppliers, Apple is one of the most successful brands today!

So a company won't change unless the people in charge also change, and business leaders can reap big rewards by challenging themselves in diversity. For instance, one technique is to _audit oneself_, a process that requires you to take a good look at your own biases. Because while turning a critical eye on your prejudices and relationships can be uncomfortable, it will help you build relationships with people different from yourself, just as Apple did with its suppliers.

In fact, adopting an inclusive style of leadership can even build a real meritocracy. That's because every leader can make the meritocracy they desire more of a reality by critically examining the company's culture.

For instance, bosses should pay more attention to company taboos like the informal relationships and unspoken agreements between employees. The culture at any company is mainly a product of the leadership, so if a leader acknowledges the absence of a real meritocracy and goes as far as challenging his or her own unconscious biases, the commitment to inclusion is likely to affect the rest of the company.

### 7. Final summary 

The key message in this book:

**It's a struggle to find instances of true meritocracy among companies today, and there's much to be done before the playing field can be considered level for employees. The good news is that increased diversity and inclusion can ensure equality in a workplace while also boosting the profitability of any business.**

Actionable advice:

**Keep your biases in mind the next time you're recruiting.**

The next time you're conducting an interview pay attention to any prejudices you might hold toward the prospective employee. Are your biases causing you to write them off despite their stellar qualifications? Don't let your predispositions cost you the top talent your company needs!

**Suggested** **further** **reading:** ** _Reinventing Organizations_** **by Frederic Laloux**

_Reinventing Organizations_ discusses why companies around the world are getting rid of bosses, introducing flat hierarchies and pursuing purpose over profit. And ultimately, by adopting a non-hierarchical model, these organizations _thrive_.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mark Kaplan and Mason Donovan

Mark Kaplan is a consultant who has specialized in management since 1986. He holds a master's degree in HR development and is a prolific writer on diversity in management.

Mason Donovan is a consultant who has contributed his services to most of the Fortune 500 companies. He holds a master's degree in international business.

