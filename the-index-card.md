---
id: 5a5b9785b238e100078c4fd8
slug: the-index-card-en
published_date: 2018-01-16T00:00:00.000+00:00
author: Helaine Olen and Harold Pollack
title: The Index Card
subtitle: Why Personal Finance Doesn't Have to Be Complicated
main_color: F8DE65
text_color: 665B2A
---

# The Index Card

_Why Personal Finance Doesn't Have to Be Complicated_

**Helaine Olen and Harold Pollack**

_The Index Card_ (2016) simplifies personal finance. These blinks cover everything from getting a good deal on your mortgage to securing quality life insurance, starting your own savings account and even choosing a financial advisor.

---
### 1. What’s in it for me? Rein in your financial life before it’s too late. 

In 2013, a University of Chicago professor named Harold Pollack interviewed the financial journalist Helaine Olen. Somewhere along the conversation, he told her that personal finance was so simple that everything you needed to know about it could be written on an index card.

In fact, he was so convinced of this point that he proved it by grabbing an index card, writing down a set of rules, and posting a picture of it online. It wasn't long before the picture went viral and the pair understood there was a book behind this idea.

In the fleshed out version of this viral financial advice card, the authors explain in precise detail how you can gain control over your finances and why you should. The good news is that it really _is_ simple. By adopting a few straightforward tips, you can begin saving for the future today. The trick is to make smart financial decisions, seek advice from the right people and start immediately!

In these blinks you'll learn:

  * why you should pay for your financial advice;

  * how your employer can help you save for retirement; and

  * the secret to getting out of debt.

### 2. Saving money today is hard, but a few tips can help. 

We've all been there; you cash your paycheck only to see it disappear as you pay rent, cover utilities and buy groceries. And let's not forget the ever-present threat of an unexpected car repair.

So, how can you pull yourself out of this financial debacle and gain control over your money?

Naturally, it starts with saving, but that's easier said than done. After all, the median annual household income in America dropped by $3,000 between 1998 and 2013 as wages decreased or stagnated. At the same time, the cost of living has gone up.

This perfect storm of plummeting wages and rising costs has resulted in some frightening statistics: a whopping 27 percent of US households have a net worth of just $5,000 or less, and 47 percent of Americans say they couldn't come up with $400 in an emergency without borrowing money or selling something.

Since so many people are struggling to get their financial houses in order, if you don't have any savings, you shouldn't feel bad. But you _should_ take action. If you fail to come up with a savings plan soon, you'll never be able to invest for the future or even pay off your debt.

Think of it this way: if you save just 10 percent of your income every month, by the end of your first year you'll already have a whole month's salary put away!

Here are a few tips to get you started:

First, use cash instead of plastic. This may sound silly, but lots of studies have shown that people spend over 20 percent more when paying with a credit card or online than they do when they pay with physical money. Spending bills and coins is less abstract, so it's harder for people to part with them.

You can also set up an automatic savings account. These automatically deduct and save a portion of your paycheck every month, which means you don't have to endure the temptation of spending it. Lots of employers can even reroute some of your salary toward a savings account for you.

### 3. Take financial control by repaying your credit card debt in full. 

When your grandparents stumbled upon a must-have purchase that they didn't have the cash to cover, they paid for it in weekly installments, eventually paying off the item in full. But today, no such delayed gratification is necessary. Credit cards have removed this barrier, and the average credit-card owning American has 3.7 cards to her name.

However, this doesn't spell financial freedom. In fact, it's a recipe for financial doom. That's because this rapid transition toward easily accessible, high-interest credit means that the average American household bears a shocking $7,000 in credit card debt.

Here's why:

In the 1970s, the credit card companies realized how much money they could make from people racking up massive debts and never paying them back in full. The profits these companies now reap are a result of massive interest rates that are paid monthly by consumers who never manage to pay off the principle on their debts.

That's why you should always pay your full credit card bill, not just the interest. Unfortunately, only about one-third of credit card holders take this route.

How come?

Well, psychologists think it's because of a concept known as _anchoring_. People see the minimum required payment prominently displayed on the statement and "anchor" their thoughts about how much to pay based on that minimum.

If you fall into this category, or if you simply can't pay off all your bills right away, just adopt this simple strategy. First, work out the _Annual Percentage Rate_ or _APR_ of each debt. This number refers to the interest that's charged on a debt over the course of a year. It's important because the fastest way to get out of debt is to pay off the debts with the highest interest rates first.

So, make a list, ranking all your debts by APR, highest to lowest. From there, prioritize those with the highest interest while making minimum payments on the others. Once you pay off your first debt, just move down the list until they're all gone.

### 4. Shop around for your mortgage to save thousands. 

The budget people set when shopping for a house is generally based on the amount they can borrow. But invariably, people fall in love with a dream home; before long, they begin to wonder if they can stretch their mortgage just a little bit.

Unfortunately, this is a huge mistake. Rather than reaching past your price bracket, you should assess what you can afford. To do so, there's a simple rule of thumb: only buy a house if you can put down 20 percent of the purchase price.

In other words, if you have $20,000 for a deposit, you can afford a $100,000 house. This is important because the bigger your initial deposit, the lower your monthly mortgage payments will be. You'll have borrowed less money and therefore have less to pay off. In this way, a higher deposit can save you thousands of dollars over the lifetime of your loan.

It may even result in a lower interest rate as well. That's because smaller loans are less risky for lenders and they aren't compelled to charge as much interest.

Beyond that, the bigger your deposit gets, the less likely your house's market value is to drop below what you owe on your mortgage. This is a real threat if the housing market crashes, and you need to sell your property urgently. After all, in such a case you likely wouldn't have time for the housing market to recuperate.

So, a 20 percent deposit is key. Once you've got one, you're ready to start shopping for your mortgage.

That being said, research has found that just about 50 percent of people _do_ shop around for a home loan. Why should you?

Well, consider the difference between settling for a $200,000 loan at 4.5 percent compared to shopping around for one at 4 percent. The first loan will run you an additional $700 a year. Over a 30 year period, that's an extra $21,000 just because you didn't take the time to look at your options.

### 5. Start saving for retirement today and make the most of all your options. 

Imagine you've just started a new job and are signing your contract and other forms. You come across one that asks what percentage of your salary you'd like to contribute toward your retirement plan. You're young and healthy, so you toss it in the trash. Just like that, you may have thrown away your future. This mistake can easily be avoided.

That's because the common failure to save for retirement often stems from faulty logic. People tend to think they'll never retire while the majority of retirees report being forced to stop working sooner than they had planned. In fact, just 20 percent of people work past age 65. You never know when health difficulties, age discrimination, or a crisis in the family might take matters out of your control.

Another common belief is that people have decades to save for retirement and there's no rush. But compound interest complicates this assessment. For instance, if you save just $104 a month, starting at age 25, and put it in a pension fund that yields a 6 percent annual return, by 65 you'll have saved $200,000. However, if you wait until age 45 to start investing in the same fund, you'll need to deposit $430 a month to save the same amount by retirement age.

So, how much do you really need to save?

Experts estimate that you should put away 15 percent of your monthly gross pay to ensure a comfortable retirement. That sounds like a lot, but your employer could be your key to success.

That's because lots of employers offer an "employer match" for retirement savings. In such plans, an employer will match your pension contributions, — usually up to 6 percent of your annual earnings. So, make the maximum contribution you can to reap all of the benefits.

If you don't, you'll be among the 25 percent of people who, according to studies, fail to take full advantage of their employer matching options. Just think of it this way: it's the closest you'll ever come to making money for doing nothing!

> "_Surveys routinely find about half of us are petrified of running out of money in retirement."_

### 6. Protect your family and finances by planning for the worst. 

It's no secret that shopping for insurance can be a depressing and demoralizing experience; preparing for the future brings you face-to-face with all the horrible things that could happen to you and your family. Who wants to do that?

But it's even scarier to think about what would happen if something went wrong and you _weren't_ properly insured. What would your loved ones do if something happened to you? Would they suffer financially? If so, you need life insurance.

A life insurance policy pays out when the insured individual passes away. But not all life insurance is equal. The best protection you can get at the lowest cost comes in the form of _term insurance_. This is a type of life insurance policy that's limited to a set amount of time — in the author's recommendation, a 30 year period.

During that time, the policyholder makes an annual payment to keep herself covered, and the policy can be canceled on a yearly basis. If you don't want it anymore, you just stop making your payments and your coverage goes away. But most importantly, when shopping for term life insurance, get a policy that has stable annual payments throughout its life.

So, life insurance is key for covering the worst possible outcome, but things go wrong every day, and to keep yourself covered you'll also want home insurance. A good home insurance policy is really insurance on your net worth since your home is likely to be your family's most valuable asset.

That's why it's crucial to know what any given policy does or doesn't cover. So, when you shop around, think in worst-case scenarios like gale force winds destroying your home, a forest fire overtaking your town or severe flooding. Pose these scenarios to the insurance salesman and ask if the policy would cover you. But his word isn't enough; you also need it in writing. Getting it down on paper will prevent vital claims from being denied due to technicalities.

> _"If you have teenage children, make sure to take the offered insurance on their cell phones. Just trust us on this one."_

### 7. Choose your financial advisor carefully and be prepared to pay her. 

If you're in the market for a financial advisor, you might come across people with titles as confusing as "chartered college planning specialist" or "retirement management analyst." After all, financial advisors depend on convincing clients of their reputation. As a result, there are over 50 different job titles to describe people who specialize in financial planning for senior citizens alone!

So, who should you trust for your financial advice?

Well, there's some good news on this front: there's just one job title to look out for. It's _fiduciary_, and you should always choose a financial advisor who follows the _fiduciary standard_. This is key since a fiduciary has a legal obligation to put your interests above her own. That means you can trust her to give you objective financial advice, to steer you clear of overpriced investments and to keep your interests at the forefront.

Remember, while anyone can call herself a financial advisor if she isn't a fiduciary, she's just a salesperson trying to make a living off of financial products while posing as an unbiased advisor. These so-called advisors have zero legal obligation to put your interests above their bottom line.

Sound financial advice isn't free. Make sure that your financial advisor is being paid by _you_ and you alone. This is important because lots of non-fiduciary financial advisors offer services for free, only to push you toward particular investments on which they collect a commission.

By contrast, while the fees that fiduciaries charge can take multiple forms, they're all valid. You might be charged an hourly rate that ranges from $50 an hour for advice on how to get out of debt, to $500 an hour for specialized investment advice. Certain fiduciaries may even charge a flat rate for an agreed upon group of services, like a budget review or comprehensive financial plan.

Whatever tier you choose, find yourself an advisor that will give you sound advice and get your finances in order today!

> _"76% of us believe anyone going by the designation 'financial advisor' is obliged to give investment advice in our best interests."_

### 8. Final summary 

The key message in this book:

**Personal financial planning is the difference between a sound future and a deferred retirement. But the good news is that a few simple rules can de-stress your relationship to money and get your financial life in order. By planning early and often, you can get out of debt and ensure yourself a comfortable retirement.**

Actionable advice:

**Support our social safety net**

Government benefits sometimes get a bad rap, but the truth is that social programs like Medicare and Social Security make our financial lives much more comfortable and manageable. Without them, retirement might not be possible at all: 96 percent of all Americans rely on some form of government financial support, whether that's a student loan, mortgage deduction or unemployment insurance, at some point. We all have an obligation to support these programs, and you can do your part by speaking up when they're criticized.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The One-Page Financial Plan_** **by Carl Richards**

_The One-Page Financial Plan_ (2015) makes budgeting easy: once you know why money matters to you, it's just a matter of making sure you've got enough to do what you want with it, whether it's creating a stable income or saving for the future. This simple planning solution will give you all the tools, tips and tricks you need to realize your financial dreams.
---

### Helaine Olen and Harold Pollack

Helaine Olen is an award-winning financial journalist who's among the top 50 Women Who Are Changing the World according to _Business Insider_.

Harold Pollack is a professor at the University of Chicago.

