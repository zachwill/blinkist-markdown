---
id: 570976e58b5d6e00030000a0
slug: the-industries-of-the-future-en
published_date: 2016-04-13T00:00:00.000+00:00
author: Alec Ross
title: The Industries of the Future
subtitle: The next wave of innovation and globalization
main_color: D02A47
text_color: D02A47
---

# The Industries of the Future

_The next wave of innovation and globalization_

**Alec Ross**

_The Industries of the Future_ (2016) gives a sneak peak at the effects information technology and the next wave of innovation will have on globalization. These blinks explain how people, governments and companies will need to adapt to a changing world driven by big data.

---
### 1. What’s in it for me? Understand the future impact technology will have on our jobs and society. 

The digital age, the information society, the technological revolution — the time we live in has a number of sobriquets, all of which point to an exciting fact: we are living in a new world. And what makes this time so interesting? The prevalence of data. Today, data is _the_ raw material, and it can be processed and used to create new industries or revolutionize old ones.

The industries that thrive — the industries of the future — will create opportunities for new jobs and businesses, and make it possible for developing nations to transition smoothly into the new information age.

But with these developments come new challenges. For instance, how to use big data and create jobs for people displaced by technology? Let's dive right in.

In these blinks, you'll discover

  * why people in Kenya prefer to transfer money by text message;

  * how cloud computing will make robots the preferred type of labor; and

  * why Estonia is called e-Estonia.

### 2. Advances in robotics will increase efficiency, but also cost people their jobs. 

Can you remember your first job? For lots of people, their entry into the job market was at a cafe or a restaurant, waiting tables or serving coffee. But in the future, such jobs might not even be available.

That's because the introduction of cloud computing has enabled robots to do the work of humans. For instance, advances in _cloud robotics_ — robots' ability to upload experiences to the cloud and learn from the experiences of all other robots — mean that robots can now learn and grow faster than ever before.

As a result, robots are going to be capable of completing new tasks. In fact, an Oxford University study found that 47 percent of all US jobs are at great risk of being done by robots in the next two decades. For instance, the car service Uber runs a research lab that is building an automated taxi-fleet and Google's research lab, Google X has been working on a driverless car project for six years!

But why would we want to replace human workers with robots?

Well, robots can service more people at lower prices. That's because, unlike humans, who are cheap to hire but costly to keep on staff, robots don't get paid a salary or take vacations. In fact, they can work all the time.

Just consider SEDASYS, a robotics system that oversees the sedation of surgery patients. Without SEDASYS, an anesthesiologist can only monitor one operation at a time, and his presence adds $2,000 to the cost of the operation. With SEDASYS, however, a single anesthesiologist can simultaneously oversee ten operations, and his efforts only add $150 per operation.

However, the value produced by robots isn't going to be equally distributed. Therefore, governments need to do something about the income people will lose.

In fact, most of the savings made possible by robots will go straight to the big corporations that produce them. That means it's up to governments to redistribute part of those savings into things like social-safety nets, education and skills training for the labor force displaced by robots.

> _"Robots will be doing things faster, safer — or less expensively than humans."_

### 3. Advances in health technology will mean big changes across the globe. 

In the early 2000s, the human genome — that is, all of the information encoded within human DNA — was sequenced for the first time. The project cost $2.7 billion, but, since then, technological advances in _genomics_, the field of genome sequencing, have cut the cost of sequencing to around $1,000.

But why is genomics important?

Well, a better understanding of the human genome will mean better prevention and treatment of disease in general and cancer in particular. Cancer is the result of mutated DNA, which malfunctions, failing to stop the growth of unhealthy cells. With advances in technology, doctors will be able to find, and hopefully prevent, this growth earlier.

For instance, a new blood sample test known as a _liquid biopsy_ lets technicians identify even the smallest pieces of cancer DNA in a blood sample. This makes it possible to discover a tumor that's 1 percent the size of those an MRI can find.

This new technology will mean that 47 percent of cancers can be detected while they are still stage 1. This is a huge advance. For instance, ovarian cancers have a 95-percent cure rate when in stage 1. But the present technology tends to detect such cancers at stage 3 or 4, when chances of curing the patient are just 5 percent.

New medical technology in the developing world will mean something even more essential: connecting people to the fundamentals of health care. For instance, mobile communications are now present throughout Africa, but the number of doctors is quite low, standing at just 0.2 doctors per 1,000 people in Kenya.

Seeing this discrepancy, smartphone-based programs could be developed to connect patients with doctors and other specialized health-care workers who could then respond to medical questions, deliver diagnoses and check-ups and monitor the progress of treatment.

In fact, EyeNetra has produced a small plastic lens that can be attached to a smartphone and, when combined with an app, diagnose vision problems and prescribe the necessary corrective lenses. This obviates the need for a long trip to an optometrist and makes expensive machines redundant.

### 4. Current payment methods are inefficient, but technology is changing that. 

Nearly every person has the same routine before leaving the house: cell phone, wallet, keys. But pretty soon, you'll only need two of those essential items.

That's because current methods of payment are hurting business, a fact that's bound to change. For instance, credit-card technology is not only complicated, but it sucks up a huge percentage of retail profits.

After all, most internet companies maintain profit margins of around 5 percent and about half of that gets paid out to credit card companies or service providers. For example, companies pay monthly statement fees, monthly minimum fees, gateway fees and transaction fees of between 2 and 5 percent per transaction.

Seem like a lot of information?

Well, MasterCard's policy on interchange fees is over 100 pages long!

Furthermore, since most countries have low access to banking systems, people tend to rely on alternative forms of payment. In fact, in most low-income countries, simply setting up a bank account is a seriously difficult task and securing a credit card is just about impossible.

That's why Kenya developed an alternative that has since spread to 45 other countries. It's called M-Pesa and it's a payment system that uses mobile phones. It allows anyone with a SIM card to transfer money by sending a text.

And it really caught on. In fact, about 25 percent of Kenya's GDP moves through M-Pesa. But the best part is that it works internationally, an essential feature, since approximately $40 billion per year is sent back to Africa by workers living abroad.

However, such systems are just the beginning. Technology is going to push payment to even greater heights as encrypted currencies, like Bitcoin, gain more steam. Here's how they work:

Such alternative money technologies send encrypted messages that make for easier, faster payments. That kind of access means anyone can set up shop and charge for services. For instance, Airbnb and Uber are great examples of how business is becoming more distributed and local. And encrypted digital money is so safe that it can even be used to purchase a home!

> _"Only about 20 countries have what we consider a full functioning banking system."_ \- Marc Andreesen, Bitcoin expert

### 5. Cybersecurity is becoming increasingly important as data collection increases. 

It used to be that parents who wanted their kids to secure a steady career would tell them to go to medical school. Today, however, being a doctor isn't as appealing as some other professions, such as cybersecurity.

Here's why:

More and more data is collected and stored every day. In fact, as soon as a kid is handed a smartphone, they begin leaving a lifelong trail of data. Indeed, there are now private corporations that specialize in data collection and maintain around 75,000 individual data points about the average American consumer!

And with the Internet of Things — that is, the online network of normal objects like toasters and fridges and cars — this number is only going to spike.

The problem is that once we create data, we have no say in how or by whom it is used. Not just that, but some companies who focus on targeted marketing sell extremely sensitive data. For instance, a company from Illinois sold lists of rape and domestic violence victims, as well as HIV/AIDS patients, to pharmaceutical companies at the bargain rate of just $79 per 1,000 names.

So, since it's almost impossible to control your data, you need to be clear about what data you're making available. And that goes for individuals, companies and governments alike, because all three are prone to cyber attacks.

In fact, banks, energy companies, air traffic control systems and just about every business that exchanges information depends on data technology. This reality makes such operations prime targets for criminals and enemies. Further complicating things, the internet has no borders, which means there are no clear jurisdictions that help mediate disputes between states.

Naturally, all this data, and the potential for its destructive use, produces a serious demand for experts in the field of cybersecurity.

### 6. Big data technology still relies on experts in different fields to put its information to use. 

OK, so now you know the future is cybersecurity, but does that mean other careers will be irrelevant? Actually, tech workers will always need the support of other professions. Big data analysis is just one part of the equation.

For instance, data analytics produces insights by analyzing huge quantities of data from both the present moment and the past. The result is the streamlining of existing processes and access to information on an unprecedented scale.

Just take the combination of satellite-gathered weather information with ground-based sensors that measure water and air quality, nitrogen levels and other environmental conditions. This pairing enables one to find the exact right mixture of fertilizer, water and customized seeds to plant every square meter of soil.

Such _precision agriculture_ will help farmers grow more food while emitting less pollution, which is our best option for solving world hunger. It's also essential, however, that big-data technology and cheaper sensors team up with expert knowledge in each field — and that's where other professionals come in.

Here's why:

To apply technology most successfully it's essential for someone to be on the ground with the requisite expertise in the field.

For instance, Pasture Meter is a technique of precision agriculture that originated in the Manawatu-Wanganui region of New Zealand. It employs advanced sensor technology to take 200 measurements a second, determining how much grass exists in a given field and how the cattle can be best distributed.

So, while it's unlikely that anybody in Silicon Valley would think that watching grass grow could be a profitable business, the farmers of New Zealand, home to twice as many cattle as people, had an inside perspective. In fact, in part due to Pasture Meter, New Zealand's beef sales to China have climbed by nearly 487 percent in just one year. They're even outselling their much larger neighbor, Australia.

> _"Strategies for countries, companies and people are much more about building on your strengths than offsetting your weaknesses..."_

### 7. Future innovation requires open and equitable countries. 

So, big data could mean major improvements for the industries of tomorrow. But what can be done to ensure that these improvements are actually made?

Fostering an open society. That means _physical openness_, like strong infrastructure, but also _virtual openness_, like a rapid and uncensored internet, as well as _economical openness_, like reduced tariffs on trade.

For instance, in 2000, Estonia declared access to the internet a human right. As a result of their widespread use of the internet, strong infrastructure and limited regulations on all types of services, they've earned themselves the nickname _e-Estonia_. In fact, the country even offers e-citizenship to foreigners, providing them equal civil rights and making it easy to conduct business with the country from across the globe.

An open society also means encouraging innovation by giving more authority and money to young people with big ideas. Born in the digital age, the young people of today are often more open to new ideas and take greater innovative risks.

For instance, in many traditional cultures like Japan, it takes decades for young people to reach the level of authority they need to fund their ideas. This hinders innovation. After all, it's no coincidence that Google, Facebook, Oracle, Microsoft and loads of other information-technology giants were founded by entrepreneurs in their twenties.

But not only young people need to be empowered; women do, too. In fact, giving women more power is not merely a matter of ethics; it's good economics as well. Countries that suppress women's rights are, in essence, depriving themselves of half their workforce.

For instance, during the reconstruction following Rwanda's horrific 1994 genocide, gender equality became central. The reforms resulted in women being able to register as landowners, which increased registrations by 20 percent and decreased the number of impoverished women by 20 percent.

Furthermore, between 2001 and 2013, the country's GDP rose by eight percent per year! As a result, Rwanda is currently the only country in the world with a female majority in its parliament.

### 8. Final summary 

The key message in this book:

**The importance of data has grown considerably, and the way we use it will have a major impact on the future of the economy and society. Big-data analytics spurs innovation. However, it will also significantly change the job market and the security of our everyday lives.**

Actionable advice:

**Have a "data talk" with your kids.**

These days, it's essential for children to be educated about data, privacy and online security. So, it's important to sit down with your kids and explain how their online actions today may have affect their lives tomorrow. While it's impossible to control everything your children do — on- or offline — you can help them learn the risks associated with putting their data out there and teach them to make wise decisions on their own.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Future of the Professions_** **by Richard Susskind and Daniel Susskind**

_The Future of the Professions_ (2015) examines how modern technology and the internet have revolutionized our society. These blinks in particular address how technology has changed the way society views the work of experts, the so-called professionals. The role of such experts is evolving quickly; here you'll discover just what the future of professions will look like.
---

### Alec Ross

Alec Ross is a leading expert on innovation, as well as an advisor to investors, corporations and governments. When Hillary Clinton became Secretary of State, she created a new position for him called "Senior Advisor on Innovation" and tasked him with bringing new tools to diplomacy and foreign policy.

