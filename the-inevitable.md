---
id: 57dbd448da50fd0003de9ad0
slug: the-inevitable-en
published_date: 2016-09-19T00:00:00.000+00:00
author: Kevin Kelly
title: The Inevitable
subtitle: Understanding the 12 Technological Forces That Will Shape Our Future
main_color: E08F3F
text_color: 805124
---

# The Inevitable

_Understanding the 12 Technological Forces That Will Shape Our Future_

**Kevin Kelly**

_The Inevitable_ (2016) is your guide to understanding the technology trends that are gaining momentum today and will undoubtedly shape the future. These blinks delve into the ideas and motivations that are driving technology and what it all means for the world of tomorrow.

---
### 1. What’s in it for me? Prepare yourself for the inevitable future. 

In the future, will you be married to a handsome android? Will we travel in flying cars?

These blinks show you that the future is now. Computer technology and the internet are already fundamentally changing how we think and the ways we work, consume and relate to each other.

And the forces and ideas that inspired these changes are not going to go away but only get stronger. Let's learn about the 12 distinct trends or forces that will shape society over the next decades.

In these blinks, you'll also learn

  * why the concept of a "supermind" is more reality than science fiction;

  * how Uber provides faster service than regular cab companies; and

  * why in the future, there will be no more experts but only "newbies."

### 2. Technology is constantly improving and always in flux. 

How would you like to live in a _utopia_ — a perfect world in which you never suffer yet also never change?

If this prospect sounds boring, don't worry — this isn't what the future holds.

Instead, what we're experiencing and will continue to experience is a _protopia_ — a world that keeps getting better, one baby step at a time.

In a protopia, things are always a bit better today than yesterday. For instance, even if some inventions produce problems such as higher energy demands, the benefits always outweigh the costs.

But living in a protopia also means that nothing is fixed. Technology moves forward, accelerating progress. To put it differently, everything is in a state of _becoming_ — which means you and everyone else will always be "newbies" in this ever-evolving technological world.

No invention is ever finalized or fixed if each invention is a starting point for another invention or development. Just think of how the advent of personal computers paved the way for the internet.

Basically, all human technology is permanently moving forward, and this process, in which every innovation has the potential to trigger another innovation, will never be straightforward or predictable.

Because of this, the best predictions we can make about future events are generalized trends.

And since technology never stops improving, you'll always feel like a newcomer no matter what you learned or experienced in the past. You'll have to upgrade your devices to adapt to the evolving digital landscape — and we all know how long it takes to get used to new technology.

Your permanent newbie status is also a result of the decreasing longevity of devices and software, which means you likely won't get to spend enough time with any technology to truly master it.

For example, the average lifespan of a smartphone app — the amount of time it takes a user to stop using the technology — is now 30 days.

So we now know that technology, especially information technology, never stops moving forward. Next, we'll explore how this fact is tied to the rapid growth of artificial intelligence.

> _"Endless newbie is the new default for everyone, no matter your age or experience."_

### 3. Artificial intelligence and robust online filters will shape how we work and what we learn. 

Over 100 years ago, the advent of electricity transformed the tools we use every day. The next technological step is artificial intelligence.

Today, AI is _cognifying_ objects, turning them into tools that are "intelligent."

Making previously "dumb" objects "smart" is one trend that will dramatically shape the future. This process isn't limited to physical objects, however. In fact, AI will cognify chemistry, for example, using algorithms and databases to run virtual chemistry experiments.

But if this is the future, will AI replace all human intelligence?

Definitely not. Yet AI will certainly redefine human intelligence and perhaps humanity as a whole.

While AI can enhance certain areas of study, such as medicine, this doesn't mean AI will replace human specialists in those fields.

People and AI working together will produce the best outcomes. The most accurate health diagnoses will be made by physicians, for example, who combine intuition with state-of-the-art diagnostic software.

Humans won't become superfluous, but we will have to constantly evaluate the things we can do better than computers and delegate other tasks accordingly. This will leave us to perform tasks that are uniquely human, such as caring for others and engaging in intuitive thinking, while letting AI take over other jobs such as memorization. As a result, AI will shape what we do and, in time, who we are.

But AI isn't the only technological force guiding the world of tomorrow.

Online content has been expanding at an exponential rate. To make sense of all this data, we need to be able to filter out what's important to us. _Filtering_, which depends on AI, will help shape the future.

In just 9,000 days, people produce over 60 trillion web pages! If you ever want to find what you're looking for online, you'll need to use a robust filter, such as a Google search engine.

AI can ably filter through content and identify relevant pieces because it can process massive quantities of data. All the while, AI learns to personalize results to individual user preferences.

> _"Let the robots take our jobs, and let them help us dream up new work that matters."_

### 4. “Hard” goods were yesterday; flowing content and shareable resources are the future of commerce. 

Material goods used to define commerce. With the advent of the internet, however, much of what is bought and sold is intangible, streamed online or shared offline.

In other words, modern goods are _flowing_.

A flowing good is a good you can purchase as a service or real-time update. For instance, previously if you wanted an answering machine for a phone, you had to purchase and install hardware. Today, software can automatically install itself on your smartphone without you lifting a finger.

Once a good is digitized, a user can transform, copy or share it. Consider music streaming service Spotify, which allows a user to build playlists to then listen to, like or share with friends.

Services such as Twitter, Spotify and Netflix which offer real-time content streams are just the beginning. People who subscribe to and use these services also contribute to another force that's shaping our technological future.

When you watch a movie, listen to a great song or share an insightful tweet over a social network, you're adding your content to the general stream of online information. _Sharing_, therefore, is another key force that will affect how we engage in commerce in the future.

Every day, people share 1.8 billion photos on social media. We're witnessing the evolution of a _sharing economy_, a market in which people form networks, exchange resources and collaborate on projects — such as the open-source, online community encyclopedia, Wikipedia.

A sharing economy allows any person to become a producer, by giving that person access to equipment, such as a 3D printer, for example, without having to own it personally.

So we've explored how AI will play a major role in the future of technology and humanity. Yet have you considered how, in just a few hundred years, machine intelligence will meld with human minds to produce a "super intelligence?"

This intelligence is called the _force of beginning_, and it marks a new era — that starts today.

Consider this: 15 billion devices are already linked through the internet, essentially forming one massive circuit. New connections are made every second. What will this mean for the future?

### 5. Why buy goods when you can rent them instead? Possession will give way to access in the future. 

In the not-so-distant past, possession, whether of a business or a good, was essential to engage in a commercial transaction.

Today the largest provider of accommodation in the world, Airbnb, owns none of the real estate it offers as rentals. This business is part of a larger trend, in which physical possession is less important and _access_ is more important.

Businesses are seeing the benefits of providing services instead of selling physical goods. Offering car rentals instead of selling a vehicle outright means a business can accrue greater benefits from the same object. A business doesn't even need to own the object it's "selling," thus requiring less capital outlay for maintenance, too.

Additionally, access-based businesses are often available in real time, and therefore offer better service.

Peer-to-peer taxi service Uber, for example, can offer a ride to a customer faster than can a centralized taxi service, precisely because of its decentralized workforce. Every Uber driver is self-employed and owns a car, thus allowing Uber to offer the services of multiple drivers at any time, anywhere.

Accessing instead of owning is a key driver of the future. This force will enable another crucial force, _remixing_. More and more growth today is generated by rearranging content — even further rearranging rearranged content — instead of producing new resources.

It's not possible to talk about remixing without mentioning the transformation of people from passive consumers to active content producers. While Hollywood makes 600 feature films yearly, 100 million video clips are shared on the internet every day, and many are simply remixed snippets of preexisting movies.

Now that it's so easy to copy and remix content, the next step should be a service that offers video hyperlinks, allowing people to cite specific frames of a film or video at the click of a button.

This also means that property rights will need to adapt, as most are written for a world of tangible, owned goods, not digitized snippets that are easily accessed and remixed.

> _"Accessing rather than owning keeps me agile and fresh, ready for whatever is next."_

### 6. Virtual reality and screens will transform human interaction, bringing the world into our living rooms. 

Virtual reality, or VR, is a form of simulation that is essentially fake but feels real, which is why it could change our lives.

For instance, the possibilities offered by VR will transform human interaction while increasing the actual number of interactions people have. _Interacting_ is another key force that will affect the future.

As devices become less expensive to produce and purchase, people will be able to spend more time in virtual reality scenarios. As the technology develops to react more accurately to voice and movement, for instance through eye tracking, VR will begin to feel more "natural."

But VR will also offer people the feeling that they have "superhuman" senses, such as x-ray vision. With time, people may start to perceive such senses as synonymous with regular human senses.

A person's interaction with VR technology will undoubtedly change his self-perception; and as a result, that person will want to keep interacting with VR. Immersion in technology will become the norm, and objects that don't offer VR capabilities, such as printed photographs, will start to appear "broken."

VR will open up space for greater interactivity between people. Within a virtual world, you can meet thousands of people from all over the world without setting foot outside your home!

While virtual reality will transform the world and our relationship to it, screens as well are having a profound impact on how we absorb information. Indeed, _screening_ is another powerful future trend.

In the future, screens won't just be fixed on subway station platforms or smartphones, but will appear everywhere. This is an important development. On screen, text and ideas aren't fixed and authoritative as they appear in printed books but rather constantly changing and thus open to question.

Compare the Encyclopedia Britannica, a series of printed tomes, to its online equivalent Wikipedia, where users can discuss and edit entries.

Screens _encourage_ us to interact, pushing us to research and discuss the ideas on display.

### 7. Personal privacy and certainty will disappear in the future, but really, it’s not all that bad. 

Do you value your privacy? If you do, you might be curious about a trend that has the potential to chip away at your privacy online.

_Tracking_ will bring both big risks and big opportunities in the future. Here's what it will do.

Today we quantify and document an ever-greater amount of our lives, and in the process, we are producing tons of data. If performed accurately, data collection can be then used to customize aspects of our lives to match specific needs, such as creating personalized medicine.

But if tracking becomes too invasive, we'll spend too much time monitoring personal data and little time living our lives.

There is also the risk that personal data could be compromised. Businesses or even other people could collect your data from sources such as blogs, social media and apps, using the information against you.

Is this frightening to you? Even if it is, this new "transparency" is inevitable. Society will undoubtedly learn to accept it, even enjoying its upsides. While coworkers will be able to track you, you'll have the option to track them as well — and as a result, everyone will be more accountable.

As tracking becomes a larger part of our lives, so will _questioning_. The future, with its exponential growth of content, will force us to sift through a tremendous amount of information in search of truth.

The amount of information society generates doubles every 1.5 years, yet much of it is contradictory and uncertain. For every fact we discover, we'll find another to dispute it.

And remember, plenty of society's longest-held assumptions have been proven false over time. For instance, we used to assume people would never work for free, but today free intern labor is widespread.

The point is, people have begun to question everything, no longer believing in a universal truth but rather in multiple truths assembled from a stream of facts.

This desire to question can only grow since every truth or solution produces new questions and as a result, new knowledge.

### 8. Final summary 

The key message in this book:

**The future will be shaped by 12 inevitable forces that are emerging today. These are becoming, cognifying, flowing, screening, accessing, sharing, filtering, remixing, interacting, tracking, questioning and beginning. While these forces are mostly technological in nature, they will touch every aspect of our lives. Despite potential problems, for the most part, we're facing a future of great opportunity.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Out of Control_** **by Kevin Kelly**

Though written from the perspective of 1994, these blinks paint a startlingly current and still futuristic image of how technological developments like the internet and artificial intelligence could affect society and humanity.
---

### Kevin Kelly

Kevin Kelly is the founding executive editor of _Wired_ magazine, author of 14 books and contributing writer to publications such as the _New York Times_, _Science_ and _TIME_ magazine. Kelly was a college drop-out, opting instead to backpack through Asia and pursue his interests on his own. He also helped found the WELL, an influential online community.

