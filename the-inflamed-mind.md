---
id: 5e2703756cee07000686af09
slug: the-inflamed-mind-en
published_date: 2020-01-24T00:00:00.000+00:00
author: Edward Bullmore
title: The Inflamed Mind
subtitle: A Radical New Approach to Depression
main_color: None
text_color: None
---

# The Inflamed Mind

_A Radical New Approach to Depression_

**Edward Bullmore**

_The Inflamed Mind_ (2018) explains the latest science behind a new theory linking depression to inflammation of the body and brain. Bringing together insights from medicine, psychology and evolutionary theory, psychiatrist Edward Bullmore reveals the complex connections between our immune system and our mental health — and shows how a new holistic understanding of body, mind and brain could revolutionize the way we see and treat depression.

---
### 1. What’s in it for me? Learn about a promising theory of depression. 

If cancer is the emperor of all maladies, then depression surely is the king of mental illness. But what exactly is depression? And what causes it? While the overwhelming majority of research so far has looked for the origins of depression in the brain and the mind, groundbreaking new studies are revealing how depression is not just a mental illness, but a bodily one too. And the crucial link is the immune system.

These blinks take a look at some of the latest science to explain how the immune system uses inflammation to fight disease, why this can negatively affect our mood and behavior, and how the inflammation hypothesis could advance revolutionarily new, holistic treatments of depression. 

In these blinks, you'll discover

  * which unlikely side effect exposed the depressive effects of inflammation;

  * how depression could have saved our ancient ancestors' lives; and 

  * why antidepressants might be shooting at the wrong target.

### 2. Physical illness can make you feel depressed, and the reason for this could be inflammation. 

Have you ever caught a bad cold that made you inexplicably sad, lethargic and antisocial?

When you're sick, it's normal to feel a bit gloomy. In fact, it makes sense for you to feel this way, so that you'll stay in bed and save your energy for fighting off the infection. Your immune system does this by causing _inflammation_ throughout your body that's meant to destroy the cold virus. Once the virus is gone and the inflammation subsides, your mood lifts again.

But there might be more to the process of cold-induced gloominess than we thought. A growing number of scientists are arguing that it could provide an essential clue to understanding depression.

They believe that gloominess is not caused by the sickness itself or your negative thoughts about being sick, but by your immune system's _reaction_ to the sickness: widespread bodily inflammation. These scientists are also starting to suspect that lingering inflammation could be the cause of the long-term mood disorder we commonly call "depression."

An anecdote about a drug for _rheumatoid arthritis_ illustrates the powerful effect that inflammation can have on our mood.

Rheumatoid arthritis is an autoimmune disease of the joints that makes them increasingly stiff and swollen. In autoimmune diseases, the immune system causes an unnecessary, chronic inflammation in response to some of the body's parts or components — in this case, the joints.

In the 1990s, scientists finally found an antibody against one of the key inflammatory proteins of the disease. With this antibody, they developed a drug that would help reduce the inflammation, and so ease the painful side effects of stiffness and swelling.

In 1999, the first such drug came to market under the brand name _Remicade_. To many rheumatic patients, Remicade felt like a miracle cure. Administered as an infusion, not only did it immediately relieve pain, but it also made users feel instantly happy and cheerful. At the University College Hospital in London, the patients' euphoric "Remicade high" caused nurses to fight over who got to administer the infusion.

Though not well studied, this Remicade high suggests that reducing inflammation can boost people's moods. Conversely, it seems that too much inflammation can make people depressed. To understand why this might be, let's have a closer look at how inflammation works.

### 3. Inflammation is a natural reaction of our immune system to protect us from infection. 

Since the beginning of time, humans have been at war. With one another often enough, but also against the thousands of tiny non-human invaders that swarm our bodies on a daily basis. The immune system doesn't only activate when we get a cold. Day and night, our immune systems defend us against hostile bacteria, germs, bugs, pathogens, viruses and worms — collectively called _antigens –_ which could otherwise lead to serious illness or death.

The human immune system consists of various parts and structures. Its most important soldiers are white blood cells, which can be divided into different types of specialized immune cells such as _macrophages_, and _lymphocytes_.

Macrophages are big eating cells whose job it is to destroy harmful antigens by enveloping them in a membrane of digestive enzymes. They are stationed in different locations throughout the body, and are especially numerous in sensitive areas that often come in contact with stuff from the outside, such as your gut, lungs, eyes and urinary tract. 

Macrophages have been trained over thousands of years of evolution to identify all kinds of dangerous particles. Once a macrophage has identified and eaten such a particle, it travels to the nearest lymph node, which are glands in your neck, armpits or groin. There, it shares what it has learned about the intruder with other cells — the lymphocytes _._

The information about the infectious particle allows the lymphocytes to coordinate the body's immune response. If the threat is serious, lymphocytes will produce antibodies that travel through the blood and bind to the harmful antigens. This, in turn, helps other macrophages identify the invaders more easily and quickly destroy them. 

Macrophages also communicate information to each other through _cytokines_, proteins which stimulate other macrophages to become more mobile and aggressive. Unfortunately, their blind rage as they try to find and digest the harmful intruder can cause collateral damage to our nerve and muscle tissue. That's why heavy inflammation is typically accompanied by redness, stiffness and pain in the affected area. Increases in both blood flow and fluid retention also cause it to swell.

Inflammation can be acute, for example when you suffer a cut or a wound. But inflammation can also become a chronic condition. In chronic inflammation, your macrophages, lymphocytes and cytokines are permanently moderately activated, and their activities cause damage to surrounding tissue and impede your body's normal functioning.

### 4. The doctrine of Cartesian dualism has kept modern medicine from exploring the links between mind and body. 

In his early training as a physician, the author treated an older lady for her rheumatoid arthritis. Because the woman seemed very depressed, he questioned her about her mood and social life during the examination. Back then, he didn't suspect that the inflammation underlying her arthritis could also be causing her depression — but he felt it was important to address all of her issues holistically. 

After the examination, the author was sternly told by his supervisor that it was his job to treat the lady's arthritis, not her depression. After all, anyone with such an awful disease would probably be a little depressed.

Even today, medicine draws a sharp line between physical and mental health issues. Physicians rarely ask their patients about mental symptoms, while therapists and psychiatrists seldom consider underlying physical issues.

There is one man to blame for this. René Descartes was a famous seventeenth-century mathematician and philosopher who proposed that humans are divided in two. He posited that the human body is a machine that acts according to physical laws, while the human mind, or "soul", is an immaterial spirit that can't be studied through scientific methods. Descartes thought that the soul came in contact with and controlled the body through the _pineal gland_, a small bulbous structure at the base of the brain. 

We now know that this is wrong: the pineal gland is a pretty basic brain structure that functions as our biological clock, regulating our daily and seasonal cycles. 

But Cartesian dualism ruled medicine for centuries, as scientists focused exclusively on controlling and measuring the body, while the mind remained mysterious and unapproachable. It was only in the nineteenth century that the mind began to be considered a subject worthy of serious scientific study. 

The father of modern psychology, Sigmund Freud, developed the discipline of psychoanalysis to guide this systematic inquiry into the human psyche. But the specter of Cartesianism haunted him too. At the beginning of his career, Freud wanted to examine our elusive minds _together with_ our physical brains, holistically. But after falling out with his mentor over this preposterous, anti-dualist idea, Freud turned away from this view, and the discipline of psychoanalysis came to target the mind as its own separate entity.

In the twentieth century, scientists finally began exposing the connections between mind and brain. But even as the field of _neuroscience_ became its own respectable discipline, they often ended up treating the head as if it was severed from the rest of the body — and the impact of this conception can still be felt today.

### 5. Since the invention of Prozac in the 90s, there have been no major breakthroughs in depression treatment. 

If you're an ironclad Cartesian dualist, the notion that you can cure the mind by tweaking chemicals in the brain is inconceivable. 

But today, antidepressant drugs that claim to balance the chemical composition of the brain have become one of the few treatment options for depression. 

Like many drugs for mental illness, they were discovered by accident. Shortly after World War II, scientists found that the molecule _iproniazid,_ extracted from rocket fuel used by German forces, stopped the reproduction of dangerous _Mycobacterium tuberculosis_ bacteria. Back then, the lung disease Tuberculosis _,_ or TB, was one of the leading causes of death.

When the new iproniazid drug was tested at one of the many TB sanatoriums in the US in 1952, it was a miraculous success. Not only did it kill the TB bacteria in a matter of weeks, it immediately made patients more cheerful, active and social. Nurses reported that previously bedridden patients were dancing in the wards.

Some years later, scientists found out that iproniazid didn't only kill TB bacteria, but it also boosted certain _neurotransmitters_ — chemical molecules that the brain's 100 billion nerve cells use to communicate with each other. 

To explain why this had an antidepressant effect on patients, scientists popularized the theory that depression is caused by an imbalance of neurotransmitters in the brain.

Based on this theory, US pharma company Eli Lilly developed a new class of antidepressants boosting levels of the neurotransmitter _serotonin_, dubbed _Selective Serotonin Reuptake Inhibitors,_ or _SSRIs._

In 1987, the first SSRI came to market under the now iconic brand name _Prozac._ By 1995, it had generated $2 billion in sales worldwide. Since Prozac, research for new antidepressant drugs has fizzled out. The ones that have come after are merely variations of the same thing.

The trouble is, even though SSRIs seem to work for some people, the serotonin theory of depression is not very convincing. To this day, no studies have been able to prove that depressed people actually have lower levels of serotonin than others. The same is true for any other neurotransmitter.

This means that the only somewhat effective drug treatment for depression available today is not much more than the result of a lucky stab in the dark. It's no wonder, then, that science has made no major advances in drug treatments for depression in 30 years — it still hasn't found the right target.

### 6. New fields of medical research are uncovering promising links between inflammation and depression. 

Around the time Prozac launched in the 90s, the first papers linking depression to the immune system were published in obscure medical journals. They included titles such as The Macrophage Theory of Depression (1991) and Evidence for an Immune Response in Major Depression (1995). In hindsight, these theories seem like a massive breakthrough. But at the time, they were largely ignored — they didn't fit the new serotonin story.

Nevertheless, these papers marked the beginning of two new medical fields of research: _neuroimmunology_, which studies the link between the immune system and the brain; and _immunopsychiatry_, which focuses on the connection between the immune system and mental health. 

Since then, numerous studies have demonstrated the link between depressive tendencies and high levels of certain _biomarkers_ — namely, inflammation proteins known as cytokines and _C-reactive protein_ (CRP). One Danish study, which involved over 70,000 members of the general public and concluded in 2014, showed that the higher the levels of CRP in a person's blood, the more likely they were to have negative, self-critical thoughts. 

But to show that inflammation _causes_ depression, we need to prove that it develops first, before depression. Recent studies have done exactly that. In 2014, a long term study of 15,000 British children showed that those who were slightly inflamed at the age of nine were one and a half times more likely to be depressed at the age of 18.

Another way to test the inflammation hypothesis is to study the effects of vaccination. When you get a vaccine against a disease, your doctor is injecting you with a weakened version of the disease's antigen, so that your immune system can develop antibodies against it. This causes a temporary inflammatory reaction that often has an immediate effect on people's thoughts and behaviors. 

In one study a typhoid vaccination was given to 20 healthy young people, temporarily increasing their levels of inflammatory cytokines. When they were subsequently shown pictures of sad faces, their brain scans showed increased activation of their emotional brain network, something typically found in depressed people.

But how is it possible that an ancient defense mechanism of the human body affects mood in such a drastic way? In the next blink, we'll take a look at the connections between the immune system and the brain.

### 7. Inflammation in the body can cause an inflammatory reaction in the brain that affects our mood and behavior. 

Just a few decades ago, scientists were convinced that it was nearly impossible for the immune system to affect our brain. They thought the brain had its own, completely separate immune system, shielded from the rest of the body by the so-called _Blood-Brain Barrier,_ or BBB. The BBB is a dense wall of cells that surrounds the brain and prevents molecules circulating in the blood from directly passing through to the brain. 

But in recent years, scientists have discovered that the BBB isn't as impermeable as previously thought. Not only are there gaps between the BBB cells that allow certain molecules and proteins to enter the brain; the cells themselves can respond to inflammatory signals of the body, and pass that information on to the brain.

For example, when inflammatory cytokines in the body reach a certain threshold, a specialized nerve called the _vagus nerve_ sends a signal to the brain that kick-starts its own inflammatory response. As a result, macrophages in the brain known as _microglia,_ become more "angry" and active, and start to pump out cytokines themselves. Just like the macrophages in the rest of the body, this activity can cause collateral damage to surrounding cells, destroying the connections between them, slowing their communication or outright killing them. This nerve damage could be a potential explanation for the memory loss and cognitive impairments associated with chronic inflammation. 

Inflammation in the brain also disrupts the flow of neurotransmitters. Serotonin, for example, is made by nerve cells from the base protein _tryptophan._ But inflammatory cytokines encourage nerve cells to use tryptophan to make other proteins like _kynurenine_ instead, which support the inflammation process but are toxic to nerve cells in the long term. Moreover, by depleting its basic building material, kynurenine potentially lowers levels of serotonin in the brain. This, in turn, could affect our sleep, appetite and mood, and might also explain why some severely inflamed patients do not respond well to SSRIs. 

So, contrary to what scientists believed for decades, heavy or chronic inflammation of the body can easily lead to inflammation of the brain. But the brain also acts on the immune system. By sending an electrical signal back through the _vagus nerve_ to the immune system control center of the spleen, it can inhibit inflammation in the body.

### 8. Chronic inflammation can be caused by autoimmune disorders, age, obesity and stress. 

So far, we've learned that inflammation is a natural response of the immune system, but it causes serious collateral damage to our bodies and brains when it becomes chronic. This, in turn, affects our moods, thoughts and behavior.

But what causes chronic inflammation?

Autoimmune disorders like rheumatoid arthritis, lupus and Hashimoto's disease are possible sources of persistent inflammation. In recent years, many previously misunderstood diseases have been revealed as autoimmune diseases, caused by the body's faulty immune responses to some of its own building blocks. 

When the immune system begins to turn against the cells in the pancreas that produce insulin, for instance, and the body loses control over glucose levels, this results in diabetes. Like many other autoimmune disorders that cause chronic inflammation, diabetes is frequently linked with depression.

But even outwardly "healthy" people can suffer from chronic inflammation as a result of genetics, as well as certain circumstances and lifestyle choices. For all of us, levels of inflammatory cytokines and CRP naturally increase with age. Inflammation has also been shown to be naturally higher in the winter months — probably as a result of all the flu and cold viruses going around at that time of year.

Obesity is another risk factor for inflammation. Around 60 percent of the cells in fat tissue are macrophages, and overweight people have higher levels of cytokines and CRP. This may also be a reason for higher rates of depression among overweight people.

But perhaps the biggest contributor to chronic inflammation of all is stress, which is also the best known, yet least understood, cause of depression. 80 percent of depressive episodes are preceded by a stressful event such as the loss of a loved one. 

Scientists now know that stress, meaning our body's acute or chronic response to social or physical threats, also impacts the immune system. Levels of cytokines and CRP are higher in people experiencing extremely stressful situations such as poverty, debt or social isolation. But even minor social stresses such as public speaking can cause temporary inflammatory activation. 

In one study, scientists measured participants' cytokine levels, and then asked them to address an audience of four people for 12 minutes, followed by the audience asking the participants math questions. When the scientists measured cytokine levels again after the task, they were significantly elevated — the stress of public interrogation had caused the participants to become temporarily inflamed.

### 9. A sensitivity to inflammation and depression could have helped our ancestors survive. 

Now that you've learned about how inflammation can cause depression, you probably want to know why this is the case. 

When we ask why our bodies and brains do this or that, the answer will usually be _natural selection_. If we adhere to Darwin's evolutionary theory, then somehow, at some point in human evolution, inflammation and depression must have helped the human species survive.

This seems like a paradox, seeing that depression cuts about ten years off the average life expectancy. But many human traits are ancient, and while they may have helped our ancestors survive long enough to reproduce and pass on their genes, they are useless or even disadvantageous to us in the modern age. 

For the author, the most compelling evolutionary theory of depression focuses on the immune system. It goes back to our earliest ancestors living in the African savannah 150,000 years ago. Back then, life was dangerous, and infectious diseases contracted through childbirth, injuries or harmful pathogens abounded.

This meant that gene mutations that boosted inflammation — making macrophages more active and angry, increasing cytokine levels and enhancing the body's germ-killing power — would have made a person more likely to survive. 

But the sickness behaviors associated with inflammation, which we would today recognize as symptoms of depression, would also have been advantageous. For one, lethargy and exhaustion force the body to rest and save resources for fighting off the infection. And social withdrawal could have been beneficial for the tribe as a whole, as others would have been less likely to catch the infectious disease. Thus, even if the sick person ended up dying, genetically related members would be more likely to survive.

Infection often happened as a result of injuries or wounds sustained during fighting. Anyone who automatically became inflamed in situations that could lead to aggression would have a headstart in fighting it. 

For example, imagine two of our ancestors quarreling over a piece of meat. One of them has inherited genes that activate inflammation prematurely, as a response to the social stress of quarreling. So when the physical fight finally breaks out, this ancestor will be able to fight off subsequent infection of the wounds sustained in the fight more quickly and easily. This could be why to this day, even minor social stress, such as public speaking, can cause an inflammatory response in the body.

### 10. Medication and therapy that reduce inflammation promise new hope for treating depression. 

If you suspect that your depression is caused by inflammation after reading these blinks, you probably want to know what you can do about it. 

Unfortunately, professional treatment options for inflammatory depression are still limited.

Your physician could test for inflammatory biomarkers such as CRP through a blood test. If your CRP levels are over 3 mg/L, this could be a sign of chronic inflammation. But unless you have a concurrent physical illness that is causing the inflammation, such as gum disease or IBS, your doctor won't prescribe you an anti-inflammatory drug.

That's because anti-inflammatory drugs are still not licensed for the treatment of depression. Right now, there simply aren't enough studies to conclusively prove that the effects of anti-inflammatory drugs on depression outweigh their side effects, such as thinning the blood or damaging the gut. 

The pharma businesses aren't necessarily eager to develop experimental new drugs either. For years, they have kept hammering away at antidepressants that target serotonin and other neurotransmitters, without any new theories or targets to explore, and with few lucrative results. As a result, they have backed away from the mental health sector. 

Based on his own experience in the industry, the author estimates that the first anti-inflammatory drugs for depression will come to market in about five to ten years. 

A quicker route could be to repurpose anti-inflammatory drugs used for other diseases. Recent studies have shown that anti-inflammatory drugs that treat rheumatoid arthritis, psoriasis and asthma have a mild antidepressant effect on people with depression that still outdoes the average SSRI.

Another route that is already licensed for the treatment of depression is using bioelectric devices that stimulate the vagus nerve through vibrations in the ear, to inhibit inflammation. For many depressed patients, this treatment does indeed seem to work, but it's not very widespread yet.

Your physician is more likely to recommend therapy and lifestyle changes that fight potential underlying causes of inflammation, such as stress and obesity. This could mean trying psychotherapy, yoga or meditation; or changing your diet and exercising. 

One thing that's become increasingly clear is that there is no one-size-fits-all treatment for depression. Mental illness can have many causes, which is why treatment must be personalized and holistic, aiming to break the vicious cycle of stress, inflammation and depression. Understanding just how inflammation figures into the equation will be an important step toward achieving this.

### 11. Final summary 

The key message in these blinks:

**Since the introduction of Prozac** **in the 90s, there have been few advances in our treatment and understanding of depression. But in the past few years, science has been uncovering new links between depression and inflammation caused by our immune system. When inflammation becomes chronic due to autoimmune diseases, stress or other lifestyle factors, it can negatively affect our mood, thoughts and behavior. Anti-inflammatory drugs and stress-reducing therapies promise a powerful treatment option for depression.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Undo It!_** **by Dean Ornish M.D. & Anne Ornish**

If these blinks have convinced you that depression can be caused by inflammation, and inflammation can be caused by stress, obesity and poor health, then _Undo It!_ will give you the tools you need to do something about it.

While anti-inflammatory drugs for depression may not yet be available, embracing healthy habits and lifestyle choices, such as a better diet, smart exercise and stress management techniques, could be all you need to heal depression and even chronic disease. If you're ready to make some simple but powerful changes, head over to our blinks to _Undo It!_
---

### Edward Bullmore

Edward Bullmore is a psychiatrist, neuroscientist and mental health expert from the UK. He studied medicine at the University of Oxford and is now a Professor of Psychiatry at the University of Cambridge. Since 2005, he has been working for pharma company GlaxoSmithKline on developing new anti-inflammatory drugs for depression.

