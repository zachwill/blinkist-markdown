---
id: 5a43af49b238e10006e1ad0d
slug: the-inner-life-of-animals-en
published_date: 2018-01-04T00:00:00.000+00:00
author: Peter Wohlleben
title: The Inner Life of Animals
subtitle: Love, Grief, and Compassion – Surprising Observations of a Hidden World
main_color: A58255
text_color: 806442
---

# The Inner Life of Animals

_Love, Grief, and Compassion – Surprising Observations of a Hidden World_

**Peter Wohlleben**

In _The Inner Life of Animals_ (2017) Peter Wohlleben discusses the latest research on animal feelings and emotion. He draws insights from multiple studies as well as from his personal experiences of the woodland where he has lived and worked for decades. The book argues that animals' inner lives are really not so different from our own.

---
### 1. What’s in it for me? Discover the inner life of animals. 

Anyone who's ever caught his dog stealing a snack from the kitchen would guess that dogs are capable of feeling shame. Those big round eyes full of regret, that "oops . . . Sorry" look — it feels unmistakable.

Examples like this seem to indicate that animals can experience complex emotions.

But what does shame feel like for _them_? Perhaps it feels the same as it does for us humans. And what other sorts of emotions do they feel? Can a horse be sad? Can birds exhibit mendacity? These are the kind of questions addressed in the following blinks. You'll discover just what makes animals tick as Peter Wohlleben explains using scientific studies as well as his own experience.

Also, in these blinks, you'll find out

  * the contents of laboratory rats' dreams;

  * why the author's rooster is a sly fox; and

  * why no one does any research on water bears.

### 2. Though we shouldn’t anthropomorphize animals, evolution suggests they have similar emotions. 

Have you imagined what it would be like to read an animal's mind? Well, by the end of this book-in-blinks you might have an idea. But we've got to begin with some basics. There are some pitfalls to avoid when it comes to discussing animals and emotions.

For starters, it's all too easy for us humans to anthropomorphize animals, that is, project human-like qualities onto them that they don't possess.

Take the squirrel. It has those big eyes and that bushy tail. Seems harmless enough. But that cuteness is distracting. Squirrels are pretty ruthless predators, quite content to gobble up nestling baby birds. That's not to say that squirrels are "bad," in the moral sense, either. It's just the way they've been programmed. They behave this way to survive.

Just because squirrels provoke an emotional response in us, it doesn't mean that we have a deep insight into their nature. In fact, it tells us more about _us_ than the animals.

There's no doubt humans are different from animals. However, from an evolutionary perspective, there's a strong argument to be made that _emotionally_ we do share some similarities.

After all, humans and animals do have some common ancestors. And this biological bequest forms our emotions. We certainly have much in common with other mammals, especially regarding brain and nervous system structures.

These similarities in brain structures would suggest that we share similar feelings with other animals, especially if we're closely related. And with good reason. Emotions are primarily handled in the relatively old parts of our brains, namely those sections that are alike in all animals.

For instance, when they give birth, both nanny-goats and human mothers produce large quantities of the neurotransmitter _oxytocin_ — a hormone that bonds the mother to her offspring.

So, there seem to be some differences and some similarities between humans and animals when it comes to emotional consciousness. But why exactly is that?

### 3. An animal’s worth is dependent upon its practical use for humankind. 

There's no escaping it — we just can't help judging animals through the lens of human experience.

We've been distinguishing between "useful" and "harmful" animals for millennia, even if these aren't the most helpful classifications at times.

Snails are "harmful" because they devour our gardens. Meanwhile, hedgehogs seem useful because they eat the snails. Dogs are perceived to be kind because they're our protectors and companions.

But if we want to get some basic and objective knowledge about animals, it's best to dump these useless categories, because — unfortunately — human nature means we seldom consider the true inner lives of animals.

Pigs are a great example. Rather than icky and stupid, they're in fact intelligent, sociable and quite clean creatures.

When a group's territory is invaded by another sounder of boar, pigs are pretty good at detecting if they're closely related to the invaders. With that knowledge, they'll decide whether it's worth picking a fight or not.

Boar are also astute enough to outwit German hunters, who are under orders to cull them. The wild pigs can steal bait without getting caught in traps.

And as for cleanliness, pigs can even identify suitable places to defecate, a good distance away from where they sleep.

In fact, it's probably because we pay so little heed to these loveable animals that we have no qualms about eating them on a regular basis. We don't want to know how intelligent the bacon on our plates had been in a previous incarnation.

We've got to break the bias and realize there is no such thing as a "good" or "bad" animal.

### 4. Some animals display their emotions openly, while others are a little harder to figure out. 

It's easy to sense the inner workings of some species, as we've been exposed to them a great deal over the course of human history.

Sometimes we can guess what more familiar species are trying to communicate. It's pretty direct. When a dog bares its teeth, for instance, we know that caution is best advised.

It's even possible to suss out equine emotions. In 2015, scientists at ETH Zurich discovered that horses neigh at two different frequencies. They use one frequency to communicate a positive or negative emotion, while they use another to indicate the strength of the feeling.

And as for the great apes, things can get even more complex. The US psychologist Penny Patterson taught sign language to a female gorilla called Koko. Koko understood 2,000 signs and used a further 1,000 herself.

Despite these advances, however, there are some animals about which we're still in the dark. Often that's because they're too small to study or seem uninteresting to us.

Take the weevil, a flightless bug found in some German forests. It grows to between two and five millimeters in length and looks like a tiny elephant. When it's in peril, its defense mechanism is to play dead. And that's about the only thing we know about weevils. After all, why would we be interested in learning more about this creature's inner life? We just can't relate to it.

The same is true for the tardigrade or water bear, a microscopically small species. There are more than 1,000 subspecies of them. The tardigrade has eight legs and a squidgy body that indeed looks a little bear-like. But as they are so tiny and are of no known value to humankind, we haven't bothered to study their behavior closely at all.

### 5. Animal behavior is motivated by instinct, feelings and consciousness – just like human behavior. 

People who don't give a fig for animal rights like to argue that animals' emotions are inferior to our own, that they're simply products of chemistry and instinct. They're wrong.

When you think about it, human emotions, like those of animals, are just as much the result of body chemistry.

Let's look at animals first, with the case of squirrel mothers or _does_. If their nests are compromised, squirrel does will carry their offspring away, running until they collapse in a state of exhaustion, without any regard for their own welfare.

Why? Because a squirrel doe is innately heroic? Or because its little body is so pumped full of hormones that it can't do anything else?

We'll never know for certain. But these questions are equally valid when applied to human mothers, who experience the same hormonal blend as our fluffy relatives.

Additionally, it's clear that both human and animal behavior is determined largely by instinct. Instinct is essentially unconscious action. Such movements are effected instantaneously: they circumvent conscious thinking.

Some people assert that animals don't have any emotions, just instincts. But this argument is flawed — humans have instincts too. When you touch a hot plate, you'll pull back before you're even consciously aware of it.

These days science is learning ever more about instinct's role in influencing our everyday lives. And it looks like our subconscious is very much in charge.

In 2008, a study at the Max Planck Institute in Leipzig showed that the subconscious parts of our brains "know" what we will do seven seconds before we consciously make the decision.

There's no denying it: humans and other animals are bound by both the whims of subconscious instinct and as well as free will. We're far more alike than different.

### 6. Affection between humans and animals is complex, and misunderstandings abound. 

Humans feel. Animals feel. Sometimes they even have feelings for one another.

However, we'll never really know if animals are capable of loving humans as we love them.

Even though dog owners swear that their pets adore them, this relationship has little in the way of real unconditional love or friendship about it. After all, a dog is dependent on an owner whose role involves supplying food or acting as a replacement pack leader.

An extreme form of bonding between animals and humans is known as _imprinting_. If a newly hatched bird sees a human, then this experience will be "imprinted" onto the bird. It will think that the human is its mother. It will try to follow and imitate him.

But there are also examples of animals voluntarily bonding with humans. For example, a dolphin named Fungie has been regularly seen swimming up to boats in Dingle Bay in Ireland for 30 years. He clearly loves interaction with humans, but why exactly is still completely unknown.

On the other hand, there's no doubt humans are capable of loving animals, even if these emotions manifest in strange ways.

For instance, humans love to breed dogs because we think we're producing desired qualities. Whether it's to make a dog more cuddly or a better hunter, it's a sign of our affection. But the results can be dangerous. Pugs can't breathe at all well as we've bred them to have extremely short muzzles.

On top of that, people sometimes use their pets as a substitute for human affection. They pamper or overfeed them, but this torture is hardly doing those animals a favor.

It's clear then that our affection toward animals isn't always a blessing for them. All the more reason to have a close look at their emotions and behavior and work out what exactly is going on.

### 7. Animals feel fear and pain just as we do. Surely that’s an argument for animal rights. 

Now let's think about the inner lives of animals, particularly whether it's possible for animals to feel fear or pain.

There's a lot of evidence to show that animals are pretty similar to us in this regard.

The section in our brains in charge of emotions is the limbic system. This area of the brain is so old that it's common to most mammals.

In particular, the amygdala, which is responsible for basic emotions, plays a key role. It exists in a variety of species, and even in fish, where it's known as the _telencephalon_.

Additionally, many animals also have very similar or even identical hormonal messenger substances to ours. That's to say, our hardware is basically the same. Our reactions to pain are alike. Just like a cow or a goat, if we touch an electric fence, we're going to jump back and avoid it in the future.

The biggest difference between animals and humans is our relative degree of consciousness. But we still have little understanding of what consciousness really is.

These facts provoke some moral questions, particularly when it comes to animal rights.

Some scientists, who side more with the interests of the food industry than animals, argue that we simply can't compare how humans and animals feel. Others, like Victoria Braithwaite of Penn State University object to this view.

She studied the pain receptors around fishes' mouths and discovered that these animals experience extreme pain when they're caught on hooks. Before that, people had commonly assumed that fish didn't feel pain at all. This finding is perhaps something that anglers should bear in mind next time they head out to fish.

In the end, each one of us has to draw his own conclusions. But as we're dealing with so many scientific unknowns, surely it's better just to support the victim's side?

### 8. Some animals recognize their own names and display signs of self-awareness. 

Do you think that maybe emotions wouldn't be such a big deal if animals weren't as conscious of them as humans? No easy way out there. Animals, it turns out, are also self-aware.

Some animals give each other names. Some can even recognize the names that humans give them. These are indications of self-awareness.

Ravens, for instance, form lifelong bonds and even develop personalized cries that they use to announce themselves and which other ravens use to address them. By any other name — a name!

Elsewhere, scientists at the Friedrich Löffler Institute needed a method to avoid overcrowding at pig feeding troughs. So they gave the pigs names. It took a few weeks of practice for the pigs to follow their roll call. When the researchers called the pigs in turn, each one recognized his personalized signal and knew what action was required of him.

There's a deeper significance to all of this. If an animal or human can recognize his individual name, then it's an indicator of more than just consciousness. Self-awareness is what we're talking about now.

Then there's the famous mirror test. It demonstrates whether a species can recognize itself or not.

In this test, an animal has a colored dot painted on its forehead. Then it's placed in front of a mirror. If the animal recognizes itself in the reflection, it should try to remove the dot from its forehead rather than from the mirror image.

Great apes, dolphins, elephants, pigs and some bird species have passed this test so far. That said, not passing this test is no proof of lack of self-awareness. After all, maybe Fido simply can't be bothered to remove it.

In the end, it does look like a fair few species have some sort of sense of self-awareness. It's another reason to treat them with respect and decency.

### 9. Social animals, just like humans, have social emotions. 

The way a species lives also affects the sorts of emotions it can feel. Simply put, some emotions can only be identified when a given species lives together in groups.

Shame, for instance, can only really exist among animals that live in herds or packs.

The author knows this from personal experience. If one of his two horses starts eating before it's been given the signal and is chastised for impatience, it will only look sheepish and give a little yawn if the other horse has witnessed this indiscretion.

The feeling of shame probably operates just as it does for humans. If an animal has violated a norm, it's keen to show its eagerness not to make similar mistakes in the future. Admitting failure means it can be more easily reintegrated into the group.

In addition to shame, animals that live in groups also have a distinct sense of fairness.

In one study, two dogs sat beside each other and were asked in turn to present a paw to their handler. If they did so, they received a reward. Over time, the rewards decreased, until only one of the dogs was getting something. The hard-done-by dog started sulking and refused to cooperate. However, when the experiment was carried out with just one dog there was no reason for it to get annoyed, and so it just kept on going.

Some species can also feel empathy among themselves. Some rather cruel experiments with rats proved this.

It transpired that rats experienced pain more intensively if they'd seen a fellow rat experiencing the same pain — like the burning of a paw — just beforehand. Conversely, their pain levels decreased if they were with rats that they'd known for a long time and that hadn't been subjected to the same experience so could support them.

It turns out that emotions, whether for humans or animals, are as much social as they are individual.

### 10. Animals can behave altruistically or deceitfully and seem to know the benefits of both. 

Evolutionary advantages are hard to come by, but for animals that live together, behaving sociably brings some benefits. It's altruism by another name, and it's pretty widespread.

Take the vampire bats of South America. When a bat returns to the cave after a successful hunting trip, it'll regurgitate some food and share with those who have been less lucky in the skies. But it only helps those who have shown generosity to it before. It proves that kindness really does pay off.

This also shows that animals do have a form of free will: their altruism isn't automatic, but voluntary.

But not all social behavior is quite as nice as that of bats. Sometimes deceit and wiliness are employed to a creature's advantage.

The author knows this well from observing his own chickens. The rooster makes a special clucking sound when he finds a delicious tidbit, and the hens come bounding over. As he's a real gent, he leaves the treat for them. But there are occasions when he clucks even if there's nothing to brag about. It's a useful way to start the mating process.

Animal behavior can also be pretty complex when it comes to theft and protection of property.

Come autumn, jaybirds bury acorns for the winter and spring. But if their supplies are depleted, they're not above pilfering from their neighbors' hoards.

An experiment showed that jaybirds are well aware of what they're doing and take precautions to stop others doing the same to them.

They were observed burying their acorns differently depending on whether or not they were alone. If they were surrounded by other birds, they preferred to dig on the noiseless sandy side of the aviary. But if they were alone it made no difference. They were just as content to hide their stash in a noisy pebble bed as in the sand.

### 11. All animals sleep, and we think that some of them dream, too. 

One feeling that we certainly do share with animals is tiredness. And they need sleep just as much we do, even if the positions they sometimes nap in can get a bit weird.

Chickens sleep standing on a perch. They don't fall off because their claws grip the perch automatically and without any extra effort. Their leg muscles, which might be expected to move if they were dreaming, are just switched off. This means they can sleep soundly the whole night through with their heads tucked under their wings.

Swifts can even sleep while flying, though this is not without risk. They ascend a few kilometers and then spiral downward, all while having a short nap.

It's also been shown that some animals dream, just like humans. We dream in the Rapid Eye Movement phase of sleep. Animals, too, experience REM sleep. Even the teensiest animals — fruit flies, for instance, can be seen to twitch their legs while dozing.

Although we can't be sure what they're dreaming about exactly, we do have a few clues.

Researchers at MIT measured rats' brain waves twice over, once while the rats were stuck in a maze then later while they were dreaming. The similarities were so great that the scientists had a fair idea which part of the maze the rats were dreaming about at a given time.

Everybody who owns a cat or a dog knows they dream too. This was proved back in the 1960s.

Scientists prepared the muscles of several cats in such a way that they didn't relax when the cats fell asleep. Incredibly, the cats began running around even though they were fast asleep; at times they even hissed at imaginary enemies.

It was a clear sign that the cats were living fun feline adventures in their dreams.

### 12. Animals have souls, and they can often sense their deaths coming. 

One elusive question remains. Do animals have souls? The author is convinced that they do.

But before we get there, let's establish what we mean by "soul." One dictionary defines a soul as "the principle of life, feeling, thought and action in humans."

As we've have seen, animals are indeed capable of feeling and sensing. But what about thinking? Well, we've seen ravens naming themselves, and roosters tricking hens. So there's certainly some form of calculated intent somewhere.

However, the same dictionary provides another definition — a religious one. It suggests that the soul is also what remains after one dies and enters heaven.

All in all, the author believes that if we're going to trust in evolutionary theory, we should also believe animals have souls.

The question comes down to when people first began going to heaven. Was it 2,000 years ago? 4,000 years ago? Or was it by the appearance of the first _Homo sapiens_, 200,000 years ago? In that case, who's to say that a hominid who lived 200,023 years ago didn't have a soul? If we believe in evolution, it's simply not possible to fix a point in history where we broke our bonds with earlier life forms. Evolution is a gradual process consisting of small changes spread over many generations. Therefore, if we claim that humans have a soul, it's impossible to say animals don't.

And as for death itself, many species seem to see it coming and act sociably in spite of it. There's no scientific way of knowing if animals are afraid of death or have a sense that their lives will end soon. But some observations can give us a clue.

Some herd animals, like goats or deer, separate themselves from the herd when they're about to die. This way they don't endanger the herd by making it vulnerable to predators. Apparently, they can see death coming.

However, nobody knows what happens to humans or animals once they die. But it's clear that animals lead their own fascinating, complex and rich lives and are blessed, like humans, with sensory and spiritual souls.

### 13. Final summary 

The key message in this book:

**It's nearly impossible to look at animals' lives without indulging in a bit of anthropomorphizing. That's why some animals' emotions have been more closely studied than others. Despite patchy research to date, it's clear that some aspects of animals' inner lives are very similar to ours. They may even possess souls. Therefore we should treat them with respect and decency, and not just exploit them without regard to their well-being.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Animal Madness_** **by Laurel Braitman**

_Animal Madness_ (2014) is all about the emotional disorders from which animals suffer and the way these problems resemble psychiatric illnesses in humans. These blinks demonstrate how similar we are to our furry friends and how we can improve the mental well-being of all animals.
---

### Peter Wohlleben

Peter Wohlleben is a German forester and the _New York Times_ -bestselling author of the renowned _The Hidden Life of Trees_. He spent many years working as a civil servant in the forestry commission. He now works in his own woodland where he tries to apply alternative and more environmentally friendly forestry methods.

