---
id: 516bbd58e4b01b686deb9b18
slug: the-innovators-dilemma-en
published_date: 2014-02-25T08:00:00.000+00:00
author: Clayton M. Christensen
title: The Innovator's Dilemma
subtitle: When New Technologies Cause Great Firms to Fail
main_color: None
text_color: None
---

# The Innovator's Dilemma

_When New Technologies Cause Great Firms to Fail_

**Clayton M. Christensen**

_The_ _Innovator's_ _Dilemma_ explains why so many well-managed and well-established companies fail dismally when faced with disruptive technologies and the emerging markets they create. Through historical examples, Christensen explains why it is precisely these "good" management companies that leave big players so vulnerable.

---
### 1. What’s in it for me? 

These days, many executives of well-established and well-managed companies are worried. They're worried because technological innovations are occurring in every industry, and many of them are so revolutionary that established companies are losing customers

The _Innovator's_ _Dilemma_ explains why it is precisely the fact that established companies are well-managed that makes them susceptible to disruptive new technologies.

You'll find out why almost 90% of the earthmoving industry's established players disappeared when they were too slow to embrace hydraulic technologies, allowing new players like John Deere and Caterpillar to seize the market.

You'll also understand what the solution is and how established companies can not only survive in the face of disruptive technologies, but take advantage of them, just like Intel and IBM.

### 2. When existing technology is updated or improved, established companies tend to maintain their dominance in a market. 

How do high-tech firms succeed in bringing new innovations to market? Theorists used to think that they did so by maintaining a frantic pace of innovation, research and development, always striving to stay technologically ahead of competitors. But is this true?

To answer this, one must first understand that when technological innovations hit a market, two types of firms can be identified.

_Established_ _firms_ were already present in the market before the new technology was introduced, whereas _entrant_ _firms_ only entered the market along with this innovation.

These two types of companies differ in size, goals and need for profit, so they employ very different approaches to the market.

Research indicates that established businesses dominate when it comes to innovations known as _sustaining_ _technologies_. They are innovations that improve upon existing technology — for example, by offering greater functionality and performance. Sustaining technologies maintain the industry's growth and rate of improvement in product performance.

A great example of this can be seen in the disk drive industry, where the established players were able to lead the way in further improvements to the existing technology. IBM, for instance, spent hundreds of millions of dollars to develop innovations like thin-film heads that were better at reading and writing disks. This improved the capabilities and the quality of their disk drives, thereby providing customers with a better version of their product.

With sustaining technologies, established companies can build on their existing market position and technology, making it difficult for entrant companies to break their dominance. Thus the established players tend to maintain their market-leading position even after sustaining technological innovations.

### 3. Established firms often neglect the new markets created by disruptive innovations, allowing entrant companies to thrive. 

Despite their powerful position, established firms don't always manage to maintain their market dominance. Sometimes entrant firms swoop in and grow to dominate the market.

How does this happen?

Basically, established firms are so focused on developing sustaining technologies that they can often neglect another type of innovation: _disruptive_ _technologies_. These are innovations that transform an industry's growth and rate of improvement in product performance. In other words, they create a new market, completely different from the old one.

One example of a disruptive technology was the advent of free, downloadable greeting cards, which created a new market adjacent to the market for paper greeting cards.

These disruptive technologies need not be complicated, and often they are merely new ways of using current technology. In other words, established firms _could_ use them as well, but usually fail to take advantage of them.

A good example is the disk drive industry, where the development of smaller drives proved a disruptive technology. Established firms like Seagate knew of the possibility of smaller disk drives and even produced some, but they opted not to pursue them due to the lower profit margins, the fact that their existing customers did not need them and the prevailing uncertainty regarding this new and emerging market.

This reluctance leaves space for entrant firms to grow and dominate the potential new market. In the disk drive industry, entrant firms like Shugart Associates and Micropolis were able to develop and introduce smaller disk drives. Although initially the market outlook was bleak, once small computers began to emerge as an important new product group, demand for these smaller drives also grew, as they were the only ones that fitted the computers. The entrant firms found themselves well-placed in a promising new market.

Even though the established firms had the technological capabilities to make smaller drives, they did not enter the market until it was too late. Eventually, the entrant firms wound up dominating the huge personal computer market.

Next, you will find out why well-managed companies are particularly prone to succumbing to disruptive technologies.

### 4. Don’t always listen to what your customers want – they can often lead firms towards the wrong strategies. 

When a disruptive technology emerges, established companies can falter. Interestingly though, it is usually _well-managed_ established firms that succumb to entrant companies. This is because, paradoxically, some of the features of good, sensible business management can put established firms in danger when a disruptive technology comes around.

A prime example of such a mistake is listening to customers — long-considered an integral part of sensible business management. Business schools teach students that it's important to know what customers want so that businesses can provide it. This is why marketers are constantly seeking out the opinions of customers via focus groups, interviews and surveys. The results are then carefully considered when formulating strategies and designing new products.

But what if the customers don't know what they want? If this is the case, firms will be led toward the wrong strategies.

Although customers may claim to know what products they want or need, a new technology can come along and change their expectations completely.

For example, in the earthmoving industry, the introduction of hydraulic machines was a major disruptive innovation. Established companies were producing larger cable-actuated machines at the time and their customers — big civil engineering projects, strip mines and so forth — wanted the biggest machines possible, so the established players opted to steer clear of smaller hydraulic machines.

This proved to be a mistake, as the hydraulic machines were more reliable, maneuverable and safer, and so the potential market for them was huge. In the end, just four out of the 30 established cable-actuated machine suppliers survived the turmoil that hydraulic machines brought about in the earthmoving industry. This disruptive new technology allowed entrant firms like John Deere and Caterpillar to lead the market.

Due to the inability of customers to predict what they need, companies would be wise to use _agnostic_ _marketing_ : look at how customers are actually using the products, and not how they say they'd like to use their products.

### 5. Firms should not innovate more than their customers want them to. 

Somewhat counterintuitively, another technology-related risk that well-managed firms run is being _too_ focused on innovation.

It is often considered good business practice for a company to be right at the cutting edge of technology, which is why many companies strive to offer their customers the latest and greatest innovations.

But this is not always the right strategy, because sometimes too much innovation can result in firms _overshooting_ _the_ _market_ : giving customers more than they are willing to pay for.

Customers' capacity for technology actually has an eventual limit, and if firms provide better product performance than their customers are willing to purchase, this is known as _performance_ _oversupply._

A good example of this can be seen in the market for hard drives for desktop computers. The manufacturers of computers consistently demanded more disk space from the drives throughout the 1980s, and hard drive manufacturers answered this demand by continuing to develop their technology until 5.25 inch drives could hold almost a gigabyte of data. But it turned out that this was in fact more space than was demanded.

When performance oversupply occurs, competitiveness in the market is no longer determined by technological advantage. Other factors like functionality, reliability, convenience and price become key determinants for success in the market.

When desktop computer manufacturers found that the 5.25 inch delivered more than enough disk space, they began to look at other factors in the drives they procured. They saw that 3.5 inch drives had enough space for their needs, and their smaller size would enable them to build the smaller, more user-friendly computers that were in demand. Thus success in the hard drive market became dependent on helping to make computers more functional, while having the technology to offer the most disk space became irrelevant.

### 6. Established firms tend to target the higher-end market, leaving room for entrant firms below. 

By now you know that supposedly sound management practices can be dangerous to established companies. This is also true in the case of their constant drive for profitable higher-end markets and satisfied shareholders.

Established companies in dominant positions often focus on the higher-end markets where they can make the largest profits. This is because managers have been taught that generating large profits is the key to satisfying shareholders. This strategy results in the companies having more profits to spend on research and development, so they build new products and get even more profits.

When asking executives to approve projects, managers end up favoring the ones that can deliver these high profits. A cut-off point might, for example, be a gross margin of over 50%.

However, chasing after the top slice of the market leaves the bottom wide open for competition. Typically, entrant firms do not have huge cost structures or persuasive shareholders, allowing them to profitably pursue projects with lower gross margins. This enables them to capture market share, while the more established companies focus on the high-end market.

A good example is the steel industry, where a disruptive new technology emerged: minimills. These minimills could produce steel more cheaply than the established integrated mills, though at first it was only low-quality steel suitable for, for example, steel rebars.

The more established mills were happy to relinquish the low-end rebar market to the entrant firms so that they could concentrate on more profitable higher-end markets. However, minimills began to improve the quality of the steel they produced and could eventually compete with integrated mills in the structural steel and sheet steel markets as well. Thanks to the minimills' more efficient processes and lower required gross margins, they were more competitive than the established firms.

Thus, the integrated mills had left themselves vulnerable to disruptive technology by adhering to supposedly good management practices: chasing after the most profitable slice of the market.

### 7. Emerging markets are unpredictable, so established companies must be patient and flexible to succeed in them. 

Good business management requires detailed information about the company's market. But the development of emerging markets is notoriously hard to predict, which can make established companies wary, as they need to justify their investments, and for this they require data on the relevant markets.

As you know, disruptive technologies transform existing industries in profound ways, and this can often lead to the emergence of an entirely new market. But it's hard to know how profitable this new market will prove; you can't measure a market that does not exist yet.

For example, consider the _Disk/Trend_ _Report_, a market research publication that has made projections for the sales of every disk drive product ever released. While its predictions for sustaining technologies like the larger disk drives were reasonably accurate, its estimates for the emerging market of the smaller disk drives were off by a whopping 550%.

This difficulty in forecasting emerging markets means established companies can't justify the investments necessary to enter them, and hence they are prone to miss out on disruptive technologies and emerging markets.

However, there is a remedy: they must be flexible and patient.

They must first accept that markets for disruptive technologies are unpredictable, so they should always assume that the experts' predictions are wrong.

They should also use _discovery_ _driven_ _planning._ This means they invest in learning about the disruptive technology in question. Since it takes time to overcome the inherent uncertainties and learn how the technology will be used, a flexible investment plan is needed.

As an example, consider the Intel Corporation. They bought the patent for microprocessors from a Japanese calculator manufacturer, even though there was no market or strategy for microprocessors at that time. Instead of scrapping the product, Intel used discovery-driven planning and gave the microprocessor unit more resources incrementally as its profits slowly rose. Eventually, this paid off when IBM chose the Intel 8088 microprocessor for its latest personal computer.

### 8. Management is not always to blame; many firms are just too inflexible to adapt to disruptive technologies. 

It may seem as though the fall of an established company is always the fault of management, as they rely too much on customers' opinions, seek higher-end markets and fear unknown opportunities, but this is not the case. In fact, the problem lies much deeper, in their capabilities in the areas of _resources,_ _processes_ _and_ _values._ This _RPV_ _framework_ defines a company's capabilities and culture.

_Resources_ refer to anything that can be bought, sold, hired or fired. They are the easiest part of the framework to adjust, and redirecting them helps organizations deal with strategic changes.

_Processes_ are the patterns of interaction and decision-making that determine how an organization uses its resources. Processes can be formal — for example, written policies — and informal, like verbal communications. Processes are very difficult to adjust, which is crucial because disruptive technologies often require a change in processes.

Finally, _values_ are the criteria used to make decisions about priority. Employees use values to decide if a customer, product or order is important or not.

The failure of established companies to adapt to disruptive technology is often caused by a deeply ingrained RPV framework rather than by management mistakes **.**

One example is the Digital Equipment Corporation (DEC), which failed to adapt to the emerging personal computer market.

In the 1980s it had all the financial and human resources it needed to be a leader in the computer industry. But the processes that made it a leader in minicomputers — like batch manufacturing and selling to corporate engineering organizations — were poorly suited to producing personal computers. Also, the company's values included the pursuit of products with a minimum gross margin of 50%, although personal computers had lower margins.

Therefore, there was little that management could do to help DEC succeed in the personal computer market when its RPV framework was so poorly suited to it.

### 9. Theoretical models for technological innovations rarely work with disruptive technologies. 

For a long time, companies have modelled their thinking regarding technological innovation in business by using theoretical models.

One well-known model is the _S-curve_, which can help guide firms' strategies when products are at the beginning or end of their life cycle.

Basically, the S-curve describes the product's sales over time: at first it grows slowly until the market becomes properly aware of it. Then the product's popularity skyrockets until it is again made less desirable by competition, copycats and new technologies. As new products replace old ones their performance will follow this same pattern, and if you were to chart their sales on a graph, it would look like a series of overlapping Ss. Companies can use models like this to plan when they need to launch new products and to predict when they are likely to be profitable.

But the trouble with theoretical models like the S-curve is that they don't always work in the real world. This is especially true for disruptive technologies. They don't follow the standard S-curve because they start their life cycle in a different market than the one in which they generate the most profits.

For example, the 2.5 inch drive actually started out in the market for personal computers, which meant that it was undetected in the S-curve of disk drive products. It was on a completely different graph to the one the established disk drive companies were watching.

Indeed, understanding and applying the life cycle of disruptive technologies requires a totally new measurement of performance, demand and technological development.

This means companies relying on models like the S-curve may well be in for a nasty surprise when a disruptive technology emerges.

In the next blinks, you will find out how companies can harness the power of disruptive technologies.

### 10. To take advantage of disruptive technologies, companies must get creative and find the right customers. 

When there are so many ways of missing the opportunity provided by a disruptive technology, how can businesses actually succeed in seizing one?

They simply need to anticipate the market for them. Disruptive innovations are usually variations on existing technologies that open up a new customer base, so if companies wish to take advantage of a disruptive innovation, they must find this customer base, a.k.a. the market.

Consider how Honda managed to penetrate the American motorcycle market almost by accident. At the time, Honda's Super Cub model was a lightweight delivery bike, and was not well-received in America, where larger, more powerful bikes were preferred. But when Honda started selling their bikes as adventure bikes for off-roading, they disrupted the entire American market. This was done simply by opening up a new use for their product.

In order to find the right market for their disruptive innovation, companies need to be creative.

First of all, they must understand that it is more important to find a market that can directly benefit from their innovation than a larger, but less-suited, market.

For example, let's look at the disruptive potential of electric vehicles. Just as the smaller hydraulic earthmoving machines were useful for small-scale projects, the market for electric vehicles could consist of drivers who don't need rapid acceleration or to drive for distances of over 100 miles. These limitations make marketing the vehicles in the general automobile market impossible but they could be sold as cars for teenagers, delivery vehicles in cities with heavy traffic, or as taxis.

Making the most of disruptive technologies requires smart marketing and planning ahead, as well as flexibility when the innovations are brought to market.

### 11. To take advantage of a disruptive technology, established companies must create separate, autonomous organizations. 

Considering all the pitfalls of disruptive technologies, how should companies proceed when they spot a market for potentially successful disruptive innovation? Basically, there are three options:

Option A:They can try to grow the emerging market quickly enough to satisfy shareholders with the profits.

Problem: Emerging markets usually do not grow this quickly.

Option B: They can wait until the market is more mature before taking action.

Problem: Disruptive technologies come with an enormous _first_ _mover_ _advantage_, meaning that the firms that are the first to enter the market tend to dominate it.

Option C: They can try to create or acquire an organization that has the necessary processes to deal with disruptive technologies and is small enough to be motivated by the potentially small and infrequent successes and profits. This is by far the best approach.

There are, however, a few guidelines to follow when creating this new organization:

First of all, it's important that the organization that pursues the new market already has customers with a demand for the new technology that the established firm's main customers may not have.

Secondly, the established company should expect trial and error, and therefore plan ahead so that the new organization can fail early and without great expense.

Finally, the new organization should be able to use the established firm's resources, but not import its values and processes, as they will negatively interfere with the pursuit of the emerging market.

IBM's successful entry into the personal computer market was based on all these guidelines. The company set up an autonomous organization in Florida, far from their own New York headquarters, and gave them resources like staff and money to work with. At the same time, they got autonomy in their processes and values so they could adjust their cost structure and profitability criteria to match the personal computer market, without worrying about the rest of IBM.

### 12. Final summary 

The key message in this book:

**Well-run companies fail in the face of disruptive technologies because their management policies disincentivize investment in unknown markets. This leaves them vulnerable to attacks from these unknown markets. Thus, to survive and grow, established companies must know when _not_ to use traditionally approved practices. **

Actionable advice:

**Watch your customers, don't listen to them.**

When you want to know what kind of innovations customers would really use, observe how they actually _use_ a product, not just what they say about it.

**New technologies demand flexibility.**

If you're trying to succeed in a whole new market with a new disruptive technology, expect things to go wrong and prepare for a trial-and-error learning process. Use flexible budgets and schedules.
---

### Clayton M. Christensen

Clayton M. Christensen is a professor at Harvard Business school and one of the foremost management researchers in the world. He is the best-selling author of nine books and more than a hundred articles. The _Economist_ named _The_ _Innovator's_ _Dilemma_ as one of the six most important business books ever.

