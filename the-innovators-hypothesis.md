---
id: 57a4a96c4098350003c58821
slug: the-innovators-hypothesis-en
published_date: 2016-08-11T00:00:00.000+00:00
author: Michael Schrage
title: The Innovator's Hypothesis
subtitle: How Cheap Experiments Are Worth More than Good Ideas
main_color: 6CC059
text_color: 39662F
---

# The Innovator's Hypothesis

_How Cheap Experiments Are Worth More than Good Ideas_

**Michael Schrage**

_The Innovator's Hypothesis_ (2014) shows us how modern innovation no longer comes from big, costly, time-intensive research and development departments. These days, the innovation process is different. Big ideas come from business experiments being quickly conducted by small teams at little cost. It's time to get on board and find out how your business can adapt for the future — before it's too late!

---
### 1. What’s in it for me? How to set the scene for successful innovations. 

You're already aware of it: the market, no matter which business you're in, is moving forward at a rapid pace. There are new products, new designs, new ways of marketing — and the list continues.

So what do you do? Well, you keep innovating, which, surely, is what you're trying to do. Indeed, for decades, people have spent millions and millions of dollars on research and development, all in an attempt to keep up with potential newcomers and incumbents.

But that process is somewhat cumbersome, and it's certainly lengthy and expensive. So how about getting a few tips and tricks on how to innovate with both more speed and less expenditure?

In these blinks, you'll discover

  * how a small team came up with the trackball mouse;

  * why innovators keep the company's core values in mind; and

  * that discarding your planner mentality is the way to go.

### 2. Business innovation is shifting away from costly research and development to inexpensive experimentation. 

True progress is made when people introduce new and novel ideas. Remember what life was like before the telephone was invented? Okay, probably not — but it forever changed the way people communicate.

This kind of world-changing innovation continues to take place; it's just happening in a different way.

It used to be well-funded research and development (R&D) departments that created innovative ideas and products. These days, however, innovation is coming from businesses that expand upon successful small-scale experiments.

This shift is taking place because the traditional R&D model — a lengthy research phase followed by a team of experts developing an idea from scratch — has proved too complex and costly. This old model can lead to innovation, but it takes massive amounts of analysis and financial assistance.

Just consider how much planning and money went into making a basic prototype for a hybrid car or HD television.

And for every revolutionary product, there are many more ideas that never see the light of day. In fact, an R&D team might spend millions doing market research only to abandon the idea after it tests poorly with consumers.

For these reasons, innovation is now the result of businesses conducting scalable experiments that are inexpensive, simple and less time consuming.

Businesses are moving away from the costly analysis of R&D teams to conduct small-scale experiments that test a _business hypothesis_, or innovative idea, using a variation of the scientific method.

Take the innovative ideas behind Windows or the Mac, for example: You might think it was one genius idea that launched these products, but, in reality, each was the result of inexpensive experiments, step-by-step tinkering and imaginative prototypes.

This is exactly the kind of innovation that is catching on today: starting out with small-scale, cheap experiments, and then scaling up the development process if the results turn out to be successful.

In the next blink, we'll take a closer look at this modern method and find out how businesses are conducting these experiments.

### 3. The 5x5 model method allows businesses to quickly generate inexpensive experiments to test their ideas. 

So what exactly does the modern method for innovation look like? Well, it's not unlike the method used in chemistry class, where students split up into teams, conduct experiments to solve a problem and present the results to the class.

Generally speaking, this kind of method is represented in _the 5x5 model_.

This model works by assembling cross-functional teams of five people, such as a designer, a developer, a marketer, a sales representative and an engineer. Each team is then given five days to develop five ideas that can be tested over the course of five weeks, with each team competing against one another.

By using this model, businesses can cut down on time and costs compared to the old analysis-driven R&D model.

For instance, imagine you're a video game company developing a new product. While you've made it easy for players to test a sample of the game on your website, many are neglecting to sign up for the newsletter, leaving you wondering how many potential buyers you have.

You could tackle this problem by using web surveys — but this isn't time- or cost-efficient, and it requires a lot of analysis.

So you implement the 5x5 method, conducting fast and cheap experiments that you can scale up if the results are good.

First, divide your employees into teams of five and let them each develop five ideas.

In this case, let's say the best idea is to change the timing of the newsletter request. You go with this idea and get the five teams to experiment with it.

Here's a possible experiment: ask 50 percent of the users to register before playing and the other 50 percent to register after they've played the game for more than two minutes.

The results might show that those asked to register immediately were put off and refused, while the majority of the other half did register and formed a reliable group of potential buyers.

Whatever the results may be, the 5x5 method is the efficient way to solve your problem.

### 4. Getting your business experiments up and running requires a certain corporate culture and mentality. 

Now that you're familiar with the 5x5 model, it should be smooth sailing from here on out, right? Well, not so fast. Whenever you introduce change, you should expect some resistance from those who prefer more traditional ways of doing things.

And instating the 5x5 model is no exception. A different way of approaching innovation will require a cultural shift within your company.

With traditional models, your company might be used to a top-down managerial approach toward planning and delivering proposals that requires a lot of analytical work. Therefore, your top managers might see experiments coming from small teams working from the bottom-up as a backward and ineffective path to innovation.

Many of these managers will have a negative view of experimentation, as they equate it with a lack of direction, so they'll resist the idea of scaling down.

That's why a different mentality is required to implement the 5x5 model and conduct experiments to test business hypotheses.

For example, creating and testing new ideas can be difficult when you're surrounded by people who have a _planner mentality_. These people won't seek out new ideas because they put value on what past experience has taught them and tend to believe that they already have the answers to every problem.

But remember: it's experimentation, not experience, that drives innovation. So, instead of a planner mentality, a s _earcher mentality_ is needed.

Unlike planners, searchers will admit to not having all the answers; they can see the need to run experiments in order to acquire more information.

Searchers are also action-oriented. They tend to focus on actionable ideas that can help them quickly turn a good hypothesis into a working prototype.

All this makes searchers the perfect type of person to help foster the 5x5 method and bring about innovation in your company.

But, as we'll see in the next blink, having the wrong mentality isn't the only obstacle to innovation.

> _"Big companies like big projects, not little experiments."_ — Chinese telecom manager

### 5. To avoid resistance, align your business hypothesis with the company’s core values and get everybody involved. 

Life is filled with periods of resistance. At times, life can feel like one big uphill struggle. But by persevering, you can work past resistance, and life once again becomes more manageable and productive.

This same kind of resistance is what gets in the way of innovation and the 5x5 model. Luckily, there are simple ways to get around it.

First of all, take your plan directly to the executives and let them see the impact 5x5 experiments will potentially have on the company.

To avoid resistance, get the executives on your side by showing them how your business hypothesis is connected to the core values and main strengths of the company.

Let's say you're meeting with the executives of a huge telecom company whose core value is user experience. If you present them with a business hypothesis about enhancing the customer's user experience, you're likely to get support — certainly more support than if you were to suggest using the 5x5 method to improve the company's discount packages.

But making the executives feel involved is only part of the solution; if you can make everyone feel involved, there will be no resistance at all.

To make this happen, formulate a business hypothesis that assigns a role to every department of the company. When you present this plan, you can show how it mobilizes everyone from sales, marketing and IT to HR and customer service.

You'll soon learn that, if an idea is big enough, it will naturally involve the whole company, and this fact will help legitimize the 5x5 model as a way of testing the hypothesis and creating innovation.

This is the kind of big, innovative idea the beauty company Avon had when it suggested turning its customers into salespeople. Getting customers to sell their product was such an enormous strategic shift that it naturally involved every department, from HR to Marketing, to come together and turn the hypothesis into a reality.

### 6. Future users of the 5x5 model will be assisted with automated recommendations and hypotheses. 

On any given weekend, you might be feeling lazy enough to binge watch a series on Netflix. And there's a good chance it might be a series that was recommended to you by Netflix based on a similar series you had enjoyed before.

Well, as it turns out, the future of innovation will be just like that: binge experimenting based on binge hypotheses.

Not too long from now, business experiments will be suggested to 5x5 teams by intelligent recommendation engines.

Just like Netflix, companies like Amazon and Spotify also help customers by recommending items and products that match their user profile or previous shopping, watching or listening decisions.

This same idea can be applied to business experiments and the hypotheses that get them going. But rather than getting recommendations for new songs or movies, 5x5 teams will get recommendations for what hypothesis to test next.

For example, the engine might say, "Account managers who tested this hypothesis also tested…" or, "Marketing designers who tested this hypothesis also tested…"

Even better, all of this will happen automatically through artificial intelligence (AI).

And this engine won't just use information collected from human behavior, like current recommendation systems; it will be fully automated artificial intelligence.

It might sound far out, but people are actually using this technology right now.

The sciences are already employing AI to design experiments that are later published in major scientific journals. Just take Adam from the United Kingdom. Adam is a fully automated robot that generates scientific hypotheses and experiments regarding the genome of baker's yeast.

This is the same technology that will one day be applied to business experiments. Humans will still be the main factor in these experiments, but the teams will have AI to support them and help them come up with the next hypothesis that will lead to world-changing innovation.

### 7. The Final Summary 

The key message in this book:

**Business experiments are a cheap, quick and simple way to generate smarter, better, safer and more scalable innovation that will benefit companies and their customers' future.**

Actionable advice:

**Don't forget to act.**

As the great economist Joseph Schumpeter said, "Innovation is less an act of intellect than an act of will." Think about a business hypothesis but don't stop at the idea of the hypothesis. Carry it out. Go to the store, buy what you need to make it — and then make it happen!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Lean Startup_** **by Eric Ries**

The _Lean Startup method_ helps start-ups and tech companies develop sustainable business models. It advocates continuous rapid prototyping and focusing on customer-feedback data.

The method is based on the concepts of _lean manufacturing_ and _agile development,_ and its efficacy is backed up by case studies from the last few decades.
---

### Michael Schrage

Michael Schrage is an advisor and consultant on innovative risk management. He is also a research fellow at the MIT Sloan School of Management for Digital Business as well as an in-demand speaker on innovation and business experiments.

