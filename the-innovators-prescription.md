---
id: 55fff5f4615cd1000900005b
slug: the-innovators-prescription-en
published_date: 2015-09-23T00:00:00.000+00:00
author: Clayton Christensen, Jerome H. Grossman, Jason D. Hwang
title: The Innovator's Prescription
subtitle: A Disruptive Solution for Health Care
main_color: 355AA2
text_color: 355AA2
---

# The Innovator's Prescription

_A Disruptive Solution for Health Care_

**Clayton Christensen, Jerome H. Grossman, Jason D. Hwang**

_The Innovator's Prescription_ (2008) is a guide to applying business concepts to the field of health care and, in the process, revolutionizing the industry and making it more affordable and accessible. These blinks explain how health care has become mired in its old ways and how disrupting stagnant business models can make the field run like new.

---
### 1. What’s in it for me? How innovation from the world of business can fix healthcare. 

No issue is more widely debated in the US than health care. For decades a debate has raged between those who defend the system of HMOs and private insurance, and those who want a more public-controlled system. The debate has picked up even more intensity since President Obama initiated the Affordable Care Act.

But are we focusing on the wrong things when discussing health care? Rather than look at public versus private, we should be looking at innovations in the world of business. If we take the right cues from the business world and apply them to health, we could end up with a more affordable and more efficient system. So how do we do this? These blinks will show.

In these blinks, you'll discover

  * why we should look to IBM when we want to improve healthcare;

  * why building more hospitals might not lead to better service; and

  * why getting doctors to do less will help us all.

### 2. Disruptive technology could make health care more accessible and affordable. 

Health care is one of the major problems America faces. It's hard to get and can cost you a fortune. There's much room for improvement.

There are two ways to do this: Either treating health care like any other business or using what's known as _disruptive technology_ to make it easier and cheaper to access. You may not have heard of disruptive technology, but it's been used by every successful industry, from automotive fields to financial services. 

So what is it?

Disruptive technology, also referred to as _disruptive innovation_, is a combination of three aspects:

The first is a _technological enabler_, an innovation that simplifies problem-solving by routinizing solutions, thus making the entire process cheaper. The second is _business model innovation_, which allows firms to deliver affordable and accessible services while still turning a profit. 

For instance, IBM was using disruptive technology when it adopted an innovative business model that based their business operations in Florida — far removed from their mainframe and minicomputer businesses in New York and Minnesota. 

This allowed them to avoid paying the high margins, overhead costs and unit volumes typical to the Northern part of the country. 

But there's more to their success story. IBM then paired their business innovation with a technological enabler: the microprocessor. The combination of a fresh, money-saving business strategy with this stellar technology led them to revolutionize the computer market by introducing the first PCs. 

IBM's example makes clear one of the key advantages of disruptive technology: lowering costs while making products and services easier to access. 

And the third element of disruptive technology?

It's a _value network_, essentially an infrastructure composed of multiple companies each with disruptive, mutually-reinforcing business models. Say a company builds microprocessors. To build a value network, every company involved in the process, from suppliers to transporters, would adopt disruptive technology and, in the process, all of them would benefit.

So, you know what disruptive technology is, now it's time to learn about the different types of disruptive business models.

### 3. An innovative business model is crucial to harnessing the power of disruptive technology. 

Most business people know that market research is key and that companies will shell out big bucks to learn what their customers want. That's because this knowledge helps businesses direct their energy toward a specific product category. But, however common this approach may be, it's rather ill-advised. 

Why?

Because there's a better route to a successful business model that's tailored to customer needs: disruptive technology implemented through _business model innovation_, the process of creatively rethinking your business model to ensure it delivers what your customers are after. 

How do you do that?

Well, a business model is only truly innovative, and successful, if markets are _job-defined_ instead of _product-defined._ Basically, that means instead of choosing a product and then finding customers to buy it, you should put yourself in the customer's shoes and determine what they need a product to accomplish. 

One American fast-food chain had monumental success adopting this strategy. By reassessing their business plan, they determined that over 40 percent of their milkshake sales occurred in the morning, purchased by bored commuters. What they realized about these customers was that they were short on time and looking for a one-handed pick-me-up to stave off that 10 AM tummy growl. 

With this information in mind, the chain optimized their milkshakes to the needs of their customers by making them thicker, and therefore longer-lasting — just the thing to keep customers occupied on their drive to work. 

In short, by creatively re-examining their market, they improved their product (and boosted their profits!) by tailoring it to the needs of customers. 

But it's also key to package products in a way that helps people accomplish what they want. For example, by branding their product as practically being a meal in and of itself, the restaurant found a tailor-made marketing strategy that sold loads more milkshakes!

### 4. The three different business models of business innovation could revolutionize health care. 

OK, so you understand how the business-model innovation strategy works, but how could it transform health care? 

By splitting the current pairing of general hospitals and general physicians into three distinct business models:

The first is _solution shops_ — businesses that diagnose and cure unstructured issues while billing customers on a fee-for-service basis. Examples of this style of business include consulting firms, advertising agencies and research and development companies. 

The second is _Value-Added-Process_ (VAP) businesses that turn incomplete things into fine-tuned, and therefore valuable, products. These businesses charge on a fee-for-outcome basis. Examples of this model are automobile manufacturers, retailers and CVS' MinuteClinic _,_ which posts prices for every procedure it has available. 

The third model is called _facilitated networks_. These are organizations that allow people to share things with each other by charging for memberships, or by transaction. Great examples of this style of business are companies like eBay; mutual insurance companies; and the model for treating chronic illnesses used by dLife, a diabetes networking organization for patients and families. 

But why is dividing the conventional system into these three different business models such a good idea? Because, although disruption can occur within a business model, the most groundbreaking disruption happens when one business model replaces another. 

Luckily, doing so can happen in an easy three-phase process:

First, hospitals should be split into solution shops, VAP's and networks. Then lower-cost business models should be allowed to emerge in each of the three categories — for instance, mobile clinics that replace specialty hospitals. And lastly, disruption should occur across all three business models. For example, retail clinics could switch care from solution shops to VAP businesses. 

By making these changes, health care providers will be able to innovate their business models in a way that provides patients the affordable, accessible health care that they're after!

### 5. Technological enablers reduce costs by making work simpler and less dependent on human skill. 

Before modern technology, doctors were unable to see what was going on inside a patient. Instead, they deduced whatever they could by conducting painstaking external examinations. 

But today, instead of undertaking a time-consuming exam, doctors use machines that snap photos from inside the body. This process is cheaper, more effective and even simpler. Clearly, technological innovation has seriously improved healthcare. 

That's because _technological enablers_ tackle problems on a smaller scale while cutting costs and the need for human skill. But what exactly is a technological enabler?

Simply put, it's a technology that simplifies and routinizes medical problems. But that's not all; enablers also bring down costs. That's because instead of paying hefty fees to professionals who are trained to diagnose the physical symptoms of a disease, we can use things like diagnostic imaging technology, molecular diagnostics and telecommunications. 

Diagnostic imaging technology, the tool that takes images from inside our bodies, is, as mentioned above, fast, cheap and extremely accurate. 

But technology lowers costs in other ways, too. For one, it replaces many complex instinctual processes with simple rules-based work. For instance, examining a patient's symptoms using guesswork, a highly-educated mind and a massive trove of medical literature is a complex and intuitive process. Luckily, straightforward rules-based treatments can often avert this hassle; they treat problems that have been precisely diagnosed based not on the symptoms but on the cause of the condition.

For instance, insulin is an entirely rules-based treatment, since it goes to work directly on the cause of type 1 diabetes. 

The second way technology makes medical care cheaper is by employing inexpensive technicians instead of highly-trained and costly experts. For example, for several years BMW has been writing the algorithms necessary to produce hyper-realistic computer based car models that only require virtual testing. Since replacing their highly-trained experts with rules-based work overseen by technicians, the company has saved loads of money.

### 6. Technology is transforming health care by delivering precise and personalized medicine. 

So, you now know that technology is key to disruption because it cuts costs. But did you know that the way it cuts costs in health care is by producing an entirely different type of medical service? 

The difference is between _precision and personalized medicine_ and the more traditional _intuitive medicine_. But how does technology enable precision medicine?

Well, intuitive medicine looks at symptoms, but many diseases result in the _same_ symptoms. For instance, having a fever might mean you have an ear infection or it could mean you've got Hodgkin's disease.

Then, after traditional medicine makes a diagnosis based on symptoms, it prescribes medications to cure them. For example, you can take a pill to make your fever better. But this won't cure the disease that's causing it. 

On the other hand, medical technology can precisely diagnose diseases and cure patients instead of simply ameliorating their symptoms. For instance, through antibiotic therapy, a development of precision medicine that uses microscopes and staining techniques, scientists have discovered that humans are covered in masses of micro-organisms, some of which are harmless and others of which cause diseases. 

Uncovering this fact has allowed doctors to design precise antibiotics that kill the unwanted organisms and prevent the diseases they cause. 

But that's not all technology does for medicine; it also personalizes it. Using IT as a technological enabler, for instance, encourages large networks of interconnected companies with like economic models to share information and respond to the specific needs of individuals. 

But what is personalization really?

It's focusing on an individual's specific situation rather than the general conditions of other people diagnosed with the same disease. And information technology, using the global nature of social networking, can make this a reality through the internet's "someone-like-me" feeling. Being connected to a worldwide network of people is a great way for patients to access advice relevant to their specific conditions. Such specific information is invaluable.

### 7. Disrupting the hospital business model will mean more effective and cheaper services. 

Did you know that the modern-style hospital dates back to eighteenth-century Europe? Built, among other reasons, to handle leprosy and tuberculosis, these hospitals were originally just places for people to die. At the turn of the nineteenth century, however, they started to become respected places of scientific research.

But no matter how adept these hospitals were at treating tuberculosis in the early 1900s, or AIDS in the 1980s, the people who solved problems in the past aren't necessarily the best problem-solvers of today. After all, the modern age poses different issues. 

However, since many research activities are still conducted by high-cost hospitals with a "do everything for everybody" mentality, it's increasingly difficult for them to make health care affordable and accessible. In fact, it's clear that this approach will never produce the viable business model health care needs. Here's why:

It combines two business models that are naturally opposed to one another. There's the diagnosis of patients and there's their treatment — two distinct practices with different methods of turning a profit. By trying to do both, hospitals are unnecessarily bleeding money while giving patients overly-complicated and poorly delivered service. 

But how can we solve this problem?

By disrupting the business model of hospitals — that is, having fewer of them and breaking their activities into two distinct models:

One model is the solution shop, which focuses on diagnosis and often relies on expensive technology like MRI machines. The second is VAP activities that provide treatment to patients once they've been diagnosed.

So we could either construct hospitals-within-hospitals or build entirely distinct facilities. For instance, the Mayo Clinic is a solution shop that provides highly-reliable diagnoses at a low cost and then sends diagnosed patients to another section of their medical complex to receive VA processes on a fee-for-outcome basis. 

The Mayo Clinic's division between two distinct areas, each operating on their own model, allows each division to operate at a high-standard while minimizing costs and wastes.

> _"Yesterday's frontiers are now more than adequately addressed by the capabilities of most hospitals."_

### 8. The practices of physicians are no longer working and need to be disrupted by shifting responsibilities to other practitioners. 

Hospitals aren't places that have unsuccessfully combined two entirely incompatible business models. Physicians are guilty of the same and, just like hospitals, doctors need to separate their contradicting business models. 

In fact, as it stands, the practice of physicians is not working. For instance, physicians are usually responsible for four areas: diagnosing and treating ailments that are generally associated with acute pain; overseeing patients with chronic diseases; conducting physicals as well as providing disease prevention; and preliminarily identifying diseases. It's clear that the responsibilities of doctors are a mouthful — and that the model covers too many bases to ensure both affordability and accessibility. 

But that's not the only problem. The type of doctor who can simultaneously juggle all those responsibilities with success no longer exists. That's because there are far too many potential diseases; the human mind simply can't keep track of them all. 

For example, in 2007, physicians could prescribe over 13,000 drugs in the US. Could you remember all 13,000? Could any human?

So, doctors have too much on their plates and should hand over some of their responsibilities to nurses and other medical practitioners. For instance, diagnosing and treating disorders should be handled by nurses at retail clinics that operate on a VAP business model. 

Not just that, but entities that use a network-facilitator business model (profiting not from sickness, as physicians do, but from wellness) should oversee patients with behavior-intensive diseases. 

With these changes in place, physicians would be free to continue administering ongoing wellness examinations while using technology to compete with solution shops run by specialists. That might mean providing affordable on-site testing and imaging; or maybe online diagnostic tools that conglomerate extensive research to put more diagnostic power in the hands of primary care physicians; or telecommunications technologies that allow people to examine data no matter where they are.

### 9. Integrated capitation combined with the pairing of high-deductible insurance and health savings accounts can overcome problems with reimbursement. 

OK, so now you know that disrupting the health-care system could bring major improvements. But such changes aren't possible without a reimbursement system that enables them. 

Actually, the current reimbursement system doesn't work very well in general. For instance, the most common form of reimbursement, called _fee-for-service_, or FFS, means that, as a medical provider, the more services you offer, the more money you're paid. 

Why is this a bad system?

Because it maintains expensive providers instead of allowing the emergence of disruptive ones. Since big providers control the price of services, they also control investment into new products and services — a fact that makes hospitals _more_ expensive. That's because they offer an unnecessary amount of services that require financing, thereby speeding up already rising healthcare costs. 

A better alternative would be a reimbursement system that uses _integrated capitation_, wherein all treatments are available through a single contract that pairs _high-deductible insurance_, or HDI, in which your premiums are lower but deductibles are higher, with _health savings accounts_, or HSAs. 

But how can integrated capitation help the health-care system overcome its current problems?

By encouraging the development of disruptive business models — a result it could produce by using less expensive medical staff like nurse practitioners and physician's assistants. Spending less money on these practitioners will mean more profits for a practice. 

But insurance is key to financial stability, as it protects our prosperity against things like exorbitant hospital bills. It's therefore essential to invest in both HDI and HSAs to ensure well-funded personal health accounts. 

For instance, HSAs don't just help people save for medical expenses; they also help them make decisions that generally increase their wealth, as the money in their accounts is their own. This gives people a financial incentive to live healthily. Doing so literally earns them long-term increases in savings.

### 10. Final summary 

The key message in this book:

**The health-care system is dysfunctional and the key to making it work well is creatively disrupting the field. That means dividing general hospitals into distinct business models that offer different services, and profit in their individual ways. Technology can aid this process by cutting the costs of intuitive examinations and specialized labor.**

**Suggested** **further** **reading:** ** _How Will You Measure Your Life?_** **by Clayton M. Christensen, James Allworth and Karen Dillon**

As a leading business expert and cancer survivor, Clayton M. Christensen provides you with his unique insight on how to lead a life that brings both professional success and genuine happiness. In _How Will You Measure Your Life?_, Christensen touches on diverse topics such as motivation and how you can harness it, what career strategy is the best for you, how to strengthen relationships with loved ones, and how to build a strong family culture.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Clayton Christensen, Jerome H. Grossman, Jason D. Hwang

Clayton M. Christensen is a professor at the Harvard Business School and author of several New York Times bestsellers, including _The Innovator's Dilemma_. In 2011, _Thinkers50_ named him the world's most influential business thinker.

The late Jerome H. Grossman, M.D. was an expert in health care policy and a pioneer in health informatics. He served as director for the Harvard/Kennedy School Health Care Delivery Policy Program.

Jason Hwang, M.D., is an internal medicine physician turned entrepreneur. His prior work includes co-founding and serving as executive director for Innosight Institute, a consulting firm specializing in innovation and strategy.

