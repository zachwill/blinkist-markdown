---
id: 55ac2da76536380007210000
slug: the-intel-trinity-en
published_date: 2015-07-20T16:00:00.000+00:00
author: Michael S. Malone
title: The Intel Trinity
subtitle: How Robert Noyce, Gordon Moore and Andy Grove Built the World's Most Important Company
main_color: 3A91BC
text_color: 2F7699
---

# The Intel Trinity

_How Robert Noyce, Gordon Moore and Andy Grove Built the World's Most Important Company_

**Michael S. Malone**

_The Intel Trinity_ (2014) offers the first comprehensive history of Intel, arguably the most important company of our "digital age." The company's success is based on three key people — Robert Noyce, Gordon Moore and Andy Grove — whose personalities formed a formidable business trinity.

---
### 1. What’s in it for me? Learn about the business trinity that drove Intel to incredible success. 

Businesses across the globe are constantly searching for that visionary leader who can turn a company into a global player, a personality who can guide a firm through tough times to future success. The truth is that many companies fail because such a leader cannot be found.

Global technology company Intel, however, had great leadership. Yet its strength rested not in one leader, but in three — men who today are considered some of the greatest business minds of the twentieth century.

These blinks show you how the complementary skills and abilities of Gordon Moore, Bob Noyce and Andy Grove allowed Intel to massively outpace the competition.

In these blinks you'll discover

  * why Intel's visionary leaders needed a "business brain" too;

  * which of Intel's top brass was terrified of sacking employees; and

  * why three _is_ truly a magic number.

### 2. A perfect symbiosis among its top brass made Intel the world’s most important company. 

Regardless of a tech device's outside appearance, like your smartphone or laptop, it needs _microprocessors_ inside to function.

The microprocessor is perhaps the most important technological invention of our modern world, and its development is largely due to an incredibly influential company, Intel.

In fact, the impact of the microprocessor has been so significant that Intel could be considered the most important company in the world.

For the last 40 years, Intel has been at the forefront of the microprocessor industry, managing tough competition from companies such as Hewlett-Packard. And it has been very successful.

In 2000, Intel was valued at almost $500 billion, a valuation larger than the American car sector combined. At the time, these kinds of numbers just weren't seen in the tech industry. Only Apple has come close, and many years later.

So what was Intel's secret? Many point to the personalities in the company's boardroom.

According to management theorist Peter Drucker, top CEOs need three character traits to succeed: good with people, thoughtful and willing to take action.

Intel didn't have one CEO that embodied all three characteristics. But it _did_ have three unique individuals, and each person embodied one of Drucker's necessary traits.

Robert Noyce was the people man; Gordon Moore was the thinker; and Andy Grove the man of action. Together, these men formed a powerful troika, complementing each other's strengths and weaknesses.

Together, they transformed Intel into one of the most successful companies of all time.

### 3. Intel’s Robert Noyce was a charismatic, brilliant scientist who lacked essential management skills. 

Every great company requires a charismatic, visionary leader, someone who can both come up with superb ideas and communicate effectively to get people to follow through on those ideas.

Bob Noyce was the "people person" of the Intel trinity. His strong points were charisma, vision and scientific brilliance.

Noyce placed a lot of importance on being loved. He would listen to everyone and was patient. His willingness to listen won him the affections of all stakeholders, be they customers, shareholders, investors, the media or government.

But his charisma wasn't to be outdone by his scientific brilliance. In fact, he co-invented the integrated circuit, one of the most important inventions of the twentieth century.

Both Noyce's charisma and scientific talents combined well with his ability to keep a big-picture perspective. He wasn't afraid to rethink whole industries, which made him a legendary leader.

For example, at Fairchild, the company Noyce and Gordon Moore left to found Intel, Noyce released a chip for $1, much less than the chip cost to produce at the time. His genius was his ability to foresee that the same chips would actually cost even less to produce in a couple of years, as technology progressed.

By enticing customers with low prices, he could capture more of them in the early phases, and then make more money from them later, once production costs fell. This strategy became known as _learning curve pricing_, the standard pricing strategy in the tech sector.

For all his positives, Noyce was also non-confrontational, indecisive and lacked essential management skills.

He just couldn't say "no" and avoided conflict as much as possible. Though he was famous for hiring the best talent, he couldn't bring himself to fire an employee, even when the survival of the company was at stake.

When he was forced to lay off 3,500 people due to mismanagement, he gave up his position as CEO and became chairman instead, thus removing himself from such tough decision-making.

### 4. Gordon Moore was Intel’s mighty man of thought, modest yet a seriously talented scientist. 

Opposites often attract, and this was equally true at Intel. Alongside the bold, charismatic Noyce worked the modest, bookish Gordon Moore. 

Being a man of thought, Moore's qualities were scientific prowess and precision, mixed with modesty.

As the technical leader at Intel, Moore developed the eponymous Moore's Law, which states that the number of components per integrated circuit doubles every year, thus reducing its cost in relation to its performance.

Moore's Law became the guiding force of the tech industry, the pulse that dictated progress and innovation and the key element around which Intel's strategy and products would revolve.

As a person, Moore was self-effacing and ego-free. In contrast with Noyce's very physical presence, Moore had more of a spiritual one. To him, only technological advancement and scientific integrity mattered.

Even after having become billionaires, Moore and his wife didn't significantly change their lifestyle. For example, conversation at the dinner table with Moore revolved around geology and fishing rather than business or money.

However, Moore suffered the same shortcomings as Noyce. He too was a manager unable to make day-to-day business decisions, and the research and development departments he ran at both Fairchild and Intel were undisciplined and chaotic.

Andy Grove would say of Moore that, while he could answer almost any technical question quickly and accurately, he couldn't resolve conflicts between people. Moore was sometimes unable to defend even his own point of view, despite often being correct.

So with Moore in the lab working on breakthrough technologies and Noyce acting as the connection to the outside world, the team still needed a savvy, business-minded partner who could get things done. Enter Andy Grove.

### 5. A man of action, Andy Grove provided Intel with its business smarts and nimble workings. 

If Noyce and Moore were the technological visionaries, then Grove was the businessman who could harness their lofty ideas and make them a success.

Grove is the last piece of the Intel trinity: the man of action. He was decisive, empirical, brutally honest and extremely well-organized, all of which helped Intel to operate at light speed and ahead of the competition.

To outperform competitor Motorola, for example, Grove implemented "Operation Crush," a cutting-edge marketing strategy designed to surpass Motorola's efforts in just a couple of months.

As chief operating officer, Grove reviewed and validated his strategy in just a week. The next week, he presented the plan to 100 salespeople. And by the following week, Grove had rounded up more than 1,000 employees to execute the plan.

His business skills would also play an important role in Intel's success with microprocessors. Although the decision to jump into the business was made without him and he was absolutely against it, he nevertheless used his skills to take the company to new heights.

Grove was also wary and temperamental. Unlike Noyce and Moore, he wasn't one of Intel's founding members. He was an employee with a salary, and his financial stability hinged on the success of the company. This made him wary of risky strategies.

It was this fear that made him so skeptical of the company's move into microprocessors. He wanted Intel to stick to its bread and butter, which was memory chips. Thankfully, he wasn't able to stop Moore and Noyce, who knew the move was the correct one. Had Grove quashed the idea, Intel certainly would not have become the success it turned out to be. 

This is how the trinity worked. Noyce and Moore would bring together vision, confidence and technological expertise to formulate a strategy over the long term, and Grove would use his management skills to enact the plan in an efficient way.

Yet the alliance wasn't without its problems, as you'll find out in our next blink.

### 6. Although the trinity struggled, each challenge was successfully overcome to add to Intel’s successes. 

There's no question that putting such vastly different personalities in a room is bound to result in a clash or two.

Noyce and Moore deeply trusted one another, and Grove looked up to Moore as a mentor and scientific sage. However, Grove resented Noyce for putting on airs and being irresponsible, and in general, not caring about day-to-day business needs.

Noyce would evade conflicts at meetings and act aloof, as if he wasn't concerned about the business at hand. This enraged Grove and fueled his contempt for Noyce.

According to Grove, Noyce would take on a pained look when there were conflicts, and would do anything he could to avoid them — not the behavior of a serious businessperson!

For example, when Noyce decided to pursue the microprocessor project, he clearly should have talked to Grove. Perhaps fearing a potential conflict, he ignored him instead, breaking every management rule in the book to make a seriously big decision all on his own.

Ultimately, Moore would often have to act as a mediator between Grove and Noyce. Yet the two men were able to put their feelings aside in the end to fully concentrate efforts to grow Intel's businesses.

Even though Grove was furious at Noyce for having initiated the microprocessor project without his knowledge, he was still the one who made it successful with his business wizardry.

Noyce knew that Grove disliked him, but he nonetheless agreed that Grove should eventually become CEO. Noyce even put Grove in charge of the microprocessor launch.

Noyce simply recognized that Grove was the only person who could align Intel's operations with his own grand vision.

### 7. Intel balanced an egalitarian openness with a ruthless commitment to excellence in engineering. 

The strong personalities of Noyce, Moore and Grove set a powerful example for the rest of the company, creating a revolutionary corporate culture that would lead to success.

Since 1968, Intel's organizational structure has been characterized by a flat hierarchy and openness. Executives didn't get special privileges, such as executive dining rooms or reserved parking spaces.

In fact, one of Intel's top executives once got angry at his wife for showing up to the company campus in the couple's expensive Mercedes. From that point on, he was only seen driving an ordinary sedan.

Communication within the company was informal and lateral, and moved easily throughout the organizational structure. Any employee could walk up to an executive's desk — no fancier than any other — and talk about his ideas.

One example of this open communication is the chat between employee Ted Hoff and Robert Noyce about what would later become the microprocessor. At the time, Intel was struggling in the memory chip business; Noyce was intrigued with the idea and told Hoff to pursue his research.

However, an informal workplace was just a characteristic of Intel's ultimate goal: to be a peerless innovator, the best in the world and ferociously successful. It was this drive that caused Intel to introduce the standard 80-hour workweek to Silicon Valley.

"Creative confrontation" was also the norm. Everything was permitted, so long as the behavior or action solved a company problem and secured its competitive position. Personal attacks were tolerated; people were shouted down at meetings.

At Intel, if you made a mistake you knew it and were reminded of it ruthlessly. The next day you would work twice as hard, however, and everything would be forgotten.

This mix of drive to be the best and a flat hierarchy made it possible for Intel to quickly adjust to new challenges. Its pride and confidence led it to take risks, and yet it always found a way to grow stronger.

### 8. Final summary 

The key message in this book:

**Great success is defined by great people. With Intel, success was based on the partnership among three men, whose distinct personalities and strong desire to succeed were unmatched.**

**Suggested** **further** **reading:** ** _Moore's Law_** **by Arnold Thackray, David Brock and Rachel Jones**

_Moore's Law_ (2015) tells the story of Gordon Moore, a chemist from San Francisco who helped revolutionize the technology industry. Over the years, Moore's innovations have fundamentally changed all kinds of electronic technology, from digital watches and personal computers to the internet and Facebook.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael S. Malone

Investigative journalist Michael S. Malone has penned or co-authored over a dozen award-winning books. He is also an adjunct professor at Santa Clara University as well as an associate fellow at the Said Business School at Oxford University.

