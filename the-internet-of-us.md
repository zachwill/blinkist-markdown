---
id: 58a2dfa2bcb3590004c82740
slug: the-internet-of-us-en
published_date: 2017-02-17T00:00:00.000+00:00
author: Michael Patrick Lynch
title: The Internet of Us
subtitle: Knowing More and Understanding Less in the Age of Big Data
main_color: None
text_color: None
---

# The Internet of Us

_Knowing More and Understanding Less in the Age of Big Data_

**Michael Patrick Lynch**

_The Internet of Us_ (2016) investigates how the internet places our ability to acquire knowledge, conduct critical debates and form justified beliefs at risk. From corporate and government data mining, to filter bubbles and crowdsourced knowledge, it delves into the new developments we need to keep in check to survive in the coming digital era.

---
### 1. What’s in it for me? Learn how the internet transforms the nature of our knowledge. 

If we think about what makes us human, one thing will quickly come to mind: our thoughts. We humans are gifted with highly complex and fascinating brains, which allow us to acquire and share knowledge and engage in critical and autonomous thought.

Our ability to think has brought us to where we are today — to a world of knowledge, where information is created and disseminated with incredible speed via the internet. However, the internet is in the process of radically transforming the very fabric of our knowledge and thoughts.

In these blinks, you will learn how our use of the internet undermines critical and autonomous thought. You will see how we are trapped into perceiving all information online as the truth and how our approach to information online affects our societies and ourselves.

You'll also learn

  * how many CD-ROMs you'd need to store all the information created on a daily basis;

  * what the difference is between information and knowledge; and

  * why googling comes close to believing.

### 2. We’re entering a new age of the internet driven by connected objects and big data. 

Who could forget the futuristic world of Star Trek? This sci-fi series imagined a future in which the borders between the human and the digital were increasingly blurred; just think of Data, the sentient android. Today, our world is more digitally advanced than ever. So where are our androids?

The truth is that we're moving toward a digital future that looks a little different than science fiction. Web 2.0 was all about collaboration — think Wikipedia and social networks. Right now, we're already on the road to Web 3.0, or the _Internet of Things_, where intelligent, connected devices bring the internet into every aspect of our day-to-day lives.

As this third wave of the internet approaches, "smart" objects become more and more common. Through the internet of things, watches, fridges, lighting, speakers and even socks equipped with connective capabilities offer new ways to make our daily routines easier and more efficient than ever.

But these connected gadgets are more than just helpful, high-tech tools; they're also data-collecting machines. Smart devices track user behavior and share information with other connected objects.

Your car's black box records your destinations, your speeding habits and the routes you take, while your phone can't help but spew data about how long you spent in the lingerie store. The amount of data that our devices collect is immense, surpassing zettabytes — that's 1021 bytes. If you were to try to store zettabytes on CD-ROMs, you'd have enough discs to pile up to the moon five times. That's what we mean when we say "Big Data."

For governments and tech corporations like Amazon and Google, these massive amounts of data are a gold mine, as they allow them to learn about our personality, networks and behavioral patterns. These insights give them the power to develop sophisticated new products that shape individual lives and entire societies.

### 3. Our society depends on knowledge and understanding, not factual information, to function and progress. 

In the sixteenth century, philosopher Francis Bacon declared that _knowledge is power_. But what exactly is knowledge?

We can consider knowledge to be information that's true, reliable and supported by reason. As such, knowledge is distinct from mere factual information. Let's demonstrate this difference with an example.

Imagine you're looking for a certain store and ask a passerby for directions. They've got no more of a clue than you do, but take a guess and tell you to turn left. As it happens, they were correct. They didn't _know_ where to go, but they were able to give you factually accurate information anyway.

Factual information is enough to lead us in the right direction. So how important is knowledge after all?

Well, let's consider our example again. If you'd asked the same passerby on a different day, they might have guessed wrong and sent you walking down the wrong road. Their information wasn't based on reason, which makes it unreliable. By contrast, someone with knowledge understands what they're talking about and can provide reasons as to why you should believe them.

In our digital world, devices collect factual information, but don't necessarily create new knowledge. Facebook might know what links a user opens, but doesn't know the reason why. Thus, Facebook gathers information, but not knowledge.

It's knowledge, not information, that's crucial for a functioning society. The seventeenth-century philosopher Thomas Hobbes argued that humans band together and form civil societies because it enables them to share resources and have a better chance of surviving. Knowledge is one of these vital shared resources.

Our survival depends on joint efforts to develop and expand our collective knowledge, from how to grow crops sustainably to how to respond to an Ebola outbreak. But to ensure that we're sharing reliable knowledge rather than just information, we need to be able to think independently and critically.

Unfortunately, our digital culture places less and less emphasis on these very skills and, as a result, our society may well be heading toward an intellectual crisis.

### 4. In the digital age, we have easy access to information, but much of it is unreliable. 

If, in previous eras, seeing was believing, these days it seems that _googling_ is believing. Today, we have unprecedented access to information, but much of the content circulating online isn't reliable knowledge. Humans have an evolutionary tendency toward filtering information, which makes us liable to accept information without judging it critically.

Our brains filter the information we receive from our senses so that we can focus on the most important things in our environment. This allows us to react quickly and instinctively without too much conscious thought, and stops us from becoming overwhelmed. This filter is an evolutionary advantage — but it comes at a price.

When we're bombarded with content of all kinds online, our brains respond in the same way we do to our environments. Just as we trust that our vision shows us the world exactly as it is — even though our brain filters out most of the details — we also tend to blindly trust information online. If a statement seems convincing, and if it is backed up by photos, statistics or a person of authority, few people will question it.

Take the photos that circulated online in the aftermath of the Boston Marathon Bombing of 2013. One image apparently showed a man holding his wounded girlfriend; it was said that he had planned to propose to her that day. The heart-wrenching tale went viral and was picked up by mainstream media. It wasn't until later that the people depicted spoke out, clarifying that they actually didn't know each other prior to the event and certainly weren't in a relationship.

### 5. Online, we seek out information that reinforces our existing biases, which divides our society. 

What would you do if you woke up tomorrow covered in itchy red spots? You probably wouldn't ask the postman for advice — you'd consult your physician because you count on her expert knowledge. In the real world, we value experts for their ability to provide us with reasonable explanations of situations that would confuse us otherwise.

But in the online world, our behavior is different: we don't seek out the advice of experts on controversial topics, since all we're after is validation of our own views. As a result, our society becomes polarized on topical issues.

By consuming information in an unreflective way, such as reading only the headlines of articles, we accept "facts" without questioning them. A 2014 Pew Research Center study found that we tend to seek out online content that backs up our existing opinions, from blogs and tweets to articles and videos.

Conservatives reinforce their stances on certain issues by watching Fox News, while liberals do the same by reading the _Huffington Post_. The internet may be full of contrasting opinions, but as individual users, we tend to spend time in one corner of the web where we can interact with people who share our beliefs.

But if we only care about validating our opinions, we don't have much patience for learning to understand different attitudes, or even testing our own arguments to see if they're logical and reasonable. This leaves us vulnerable to _misinformation_ — that is, online content that uses false information masked as truth to reinforce our prejudices.

Such divisions in society are a very real threat. In order to truly address the issues causing controversy among communities, people need to find common ground and engage with one another. The twentieth-century philosopher Karl Popper argued that if people didn't interact and debate shared beliefs and values, society would become nothing more than an abstract and depersonalized idea.

### 6. If we want to prevent our private information from being used against us, we need better data protection. 

How would you feel if someone read your diary while you were away? You probably wouldn't be too pleased, and with good reason. Private information can easily be abused by others if they get their hands on it, and this is exactly what happens with the private data we submit online.

Since Edward Snowden's shocking revelations in 2013, the world knows that the US National Security Agency (NSA) systematically stores tremendous amounts of data about American citizens without their knowledge or permission.

Governments and data-collecting tech giants like Google would like us to believe that it's in our best interest to have our information mined.

Unfortunately, private information is rarely safe from abuse. In 2013, for instance, it was revealed that NSA employees had spied on their sexual partners. Personal data also makes us vulnerable to discrimination on various grounds, as employers increasingly use algorithms to filter out job applicants.

Today, our society urgently needs better data protection, especially as we enter the age of the Internet of Things, where our everyday behavior will be under constant surveillance. Clear data protection laws can prevent companies from burying their real data-harvesting practices within dense privacy policy documents.

Broader education about the use and abuse of data in contemporary society would also encourage people to stop trading off information blindly, whether it's ostensibly for the good of national security or simply for a more personalized experience on apps and platforms. Taking back control of our information is the first step to preventing our privacy from being harmed.

> _"Knowledge may be transparent, but power rarely is."_

### 7. As knowledge becomes increasingly networked, it’s our responsibility to stay critical. 

****2,000 years ago, in the streets of ancient Athens, the great philosopher Socrates was nagging his fellow citizens. He pushed them to shine a light on the real reasons for their stubborn beliefs, revealing conflicting and irrational thoughts. Most philosophers agree with Socrates, arguing that beliefs must be grounded. So how would we hold up today to a critical investigation by Socrates?

Thanks to the developments made during the Web 2.0 era, including collaborative projects like Wikipedia and the rise of communication about current events via Facebook and Twitter, our thoughts and beliefs are interwoven with those of other people. Just as the internet itself is a network, our thoughts and ideas are connected online so that our knowledge becomes networked, too.

A network relies on intersection points, or _nodes,_ to connect an entire structure. In the case of our society's networked knowledge, nodes take the form of our shared beliefs that we can all rely on; these beliefs are ideally based on sound reason.

However, precisely because knowledge is networked, we're also at risk of becoming influenced by individual actors who create new nodes of shared beliefs that aren't based on reason, but rather on their own personal prejudices. As a result, we're at risk of substituting our knowledge for biased group beliefs.

Before the internet age, we relied on the knowledge and experiences of experts. Today, we rely on thousands of people's opinions — this means that anything can be seen as true, as long as enough people believe it. The internet may allow us to bring knowledge together more effectively, but as a networked group, we're much more likely to come to the wrong conclusion.

As individuals, it's our responsibility to ensure that the beliefs we hold and contribute to our network are well grounded. This way, our society will make it through the new age of the internet intact.

> _"Our beliefs are nodes in a network, supported by the overall coherence of the fabric of beliefs to which they belong."_

### 8. Final summary 

The key message in these blinks:

**The internet has fundamentally changed the way we approach information, knowledge and beliefs today. We must remain vigilant to avoid getting stuck in filter bubbles that reinforce our prejudices, having our personal information harvested and abused by those in power and becoming influenced by beliefs that aren't grounded in truth and reason.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Magic and Loss_** **by Virginia Heffernan**

_Magic and Loss_ (2016) explains the profound effects the internet has had on our society, for better or for worse. These blinks explore the true magic of the online world, while shining a light on the social losses that come along with it.
---

### Michael Patrick Lynch

Michael P. Lynch is the author of several books on epistemology, truth and democracy. He is a philosophy professor at the University of Connecticut and a regular contributor to the _New York Times_ ' philosophy blog.

