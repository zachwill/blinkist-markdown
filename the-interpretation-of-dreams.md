---
id: 592c3216b238e1000507d18e
slug: the-interpretation-of-dreams-en
published_date: 2017-06-02T00:00:00.000+00:00
author: Sigmund Freud
title: The Interpretation of Dreams
subtitle: None
main_color: 2E8171
text_color: 246659
---

# The Interpretation of Dreams

_None_

**Sigmund Freud**

Sigmund Freud's cornerstone work, _The Interpretation of Dreams_ (1900), was one of the most influential books of the twentieth century and continues to shape the way we think and create. These blinks offer a fascinating insight into Freud's understanding of dreams: what they mean, where they come from, how they are formed and how we can understand them.

---
### 1. What’s in it for me? Let Sigmund Freud teach you how to interpret your dreams. 

Have you ever woken up from a dream with the feeling you've missed something significant, like a new insight into your life? Skeptics might say no, but they would be missing the way in which dreams tell us about our internal thought processes. Whether a dream feels scary, commonplace or just mundane, it has something to tell us. But what?

In this, his seminal work, the father of psychoanalysis shared his groundbreaking ideas about what dreams really mean. Sigmund Freud endeavors to move away from esoteric and spiritualist interpretations of dreams as omens and instead connects them to what lurks in the depths of our unconscious emotional lives.

In these blinks, you'll find out

  * how dreams are a way of fulfilling our unconscious wishes;

  * why dreams work like a journalist in a country with heavy censorship laws; and

  * how some dreams are shared by many people — and why.

### 2. Ancient interpretations of dreams were unscientific. A far better approach is to relate dreams to real psychic memories. 

Dreams can be confusing, often making no apparent sense whatsoever. Throughout the ages, humankind has worked to understand them and penetrate their mysteries.

In classical antiquity, it was believed that dreams were prophecies from divine sources. To understand these predictions, our ancient ancestors would draw on two methods of interpretation.

The first was to interpret the dream in its entirety before relating it to the future. An oracle or dream reader would usually be responsible for this process.

In the second technique, the dream reader would deconstruct the dream and translate it piece by piece. For instance, Alexander the Great had a dream while he was fighting a battle for the ancient Phoenician port city of Tyre. In this dream, he saw a woodland god known as a _satyr_ dancing on his shield. In the dream reader's interpretation, "satyr" was taken as "_sa tyros_," which meant "Tyre will be thine," so the dream was thought to mean that Alexander would win the battle.

The problem with this approach is that it was basically unscientific guesswork. A more sophisticated technique for interpreting dreams is relating the content of the dream to the real psychic memories and experiences of the dreamer herself.

Freud once dreamed that he was with three people: a friend named Otto, an authoritative psychologist named Dr. M and Irma, a family friend whom Freud had treated. In the dream, Dr. M says that Irma is sick because of an infection, likely caused by Otto having used a dirty needle to inject her with.

This dream related to a real phone conversation that the author had with Otto just the day before. In the phone call, Freud felt that Otto was blaming him for Irma's poor health. Yet in the dream, it's Otto, not Freud, who is actually to blame for the illness.

The dream was telling Freud, "it's not your fault," thereby fulfilling his wish to not be responsible for Irma's sickness.

### 3. All dreams fulfill wishes, although most of them are disguised. 

So, Freud's dream about Irma was a form of wish fulfillment. But is that true of all dreams?

Well, in certain cases, it's very clear that a dream satisfies a wish. Dreams of _convenience_ or _lethargy_ are good examples. For instance, a colleague of Freud's knew he had to get up early in the morning to go to a hospital appointment. When his landlady came to wake him up, he was dreaming that he was already lying in a hospital bed.

Dreaming about already being in the hospital meant that he very _conveniently_ didn't need to get up, thereby fulfilling his _lethargic_ desire to keep sleeping.

That being said, the majority of dreams are not so obvious about wish fulfillment. In fact, some dreams are incredibly painful.

Just take the story of one patient who dreamed that her youngest nephew, Charles, lay dead in an open casket. How could such a horrific dream be fulfilling a wish?

In this instance, the patient was deeply in love with a particular family friend, a professor who had recently had a falling out with the family and stopped coming around. One rare occasion when the professor _had_ visited was to attend the funeral of Otto, Charles' older brother.

The logic was that if Charles were also to die, the professor would return. The wish that was being fulfilled here was simply that of seeing the professor again.

That's a pretty opaque situation, but dreams often disguise their true meaning. In the case above, the patient consciously rejected loving the professor because her desire was futile. She suppressed her wish to be together with him and, in this way, kept it even from herself.

It, therefore, makes perfect sense that in the dream her desire was also hidden. Instead of being clearly laid out, it was _distorted_. It's a bit like reading the news in a country that censors its media, where journalists need to distort their reporting to circumvent censorship. In a similar way, dreams distort their message to evade censorship by the psyche.

### 4. The subject matter for dreams can come from several places. 

Now you know that dreams stem from the desire to fulfill wishes. But that doesn't make their interpretation simple. After all, a wish can be expressed in innumerable ways, and the content of dreams varies wildly.

So, where does the subject matter of dreams come from?

Well, you can always count on a dream to contain certain memories from the previous day. In some instances, these experiences enter a dream more or less as they were in real life. If you met an attractive person that day, they might be a character in your next dream.

However, more often than not, a real life experience connects to other thoughts and memories, which end up in the dream instead. Imagine you had a conversation with a man named Mr. Gartner. "Gartner" sounds like "gardener," and "gardener" might bring to mind your favorite flower, and so your next dream could well feature lilacs.

Another source of dream content is childhood memories. In fact, in some cases, a childhood experience is so profound that it reappears, often correlated to more recent events.

When Freud was seven or eight, he overheard his father yelling that he would never amount to anything in life.

As a result, every success he experienced reminded him of that childhood memory. When he won an award, the experience with his father was likely to appear in a dream in one way or another. For example, he dreamed of tying his tie incorrectly the night before an awards ceremony.

And finally, the content of dreams can also be produced by bodily stimuli during sleep. If someone slowly shakes you as you dream, you might experience a sensation of dizziness in your dream state.

Any stimulus to your body will readily manifest itself in the dream. So, if you need to go to the bathroom while you dream about traveling, you might dream that you're rafting down a river, as moving water tends to symbolize urination.

Between these three sources, that's a lot of content for a single dream. Next up, you'll learn how it all gets synthesized.

### 5. Dream elements are condensed, displaced and made coherent by the psyche. 

You've learned that dream content comes from many sources, but how does a dream take its final form?

For starters, the subject matter of a dream is heavily _condensed_. It undergoes so much compression that just half a page of dream content can require six to eight pages of interpretation.

One day, Freud saw a detailed study in the window of a bookshop. In the past, he had also done research on the medicinal potential of cocaine.

These two pieces of information combined in the author's next dream, where he was looking through a botanical study that he had written himself.

Dreams also take shape through _displacement_. This refers to the act of shifting something important toward something trivial. Imagine you don't care much for Ms. X. You receive a letter from her and read animosity into her having signed off with "best," instead of "all the best." This way, you displace the true animosity you feel toward Ms. X herself into the words you read.

This is precisely what happens in dreams. In Freud's case, he had been accused of spending too much time on his hobbies. Because of this, he was driven to prove that he worked hard enough to justify his hobbies.

So, in the dream about the botanical study, his wish for justification is displaced into the study, which serves as a symbol of his hard work.

And finally, once all of the material of a dream has been displaced and condensed, your psyche forms a logical sequence out of it.

Here's how:

In waking life, people tend to patch up inconsistencies they encounter. You might skip over errors in a text by imagining the correct words in their place. The same thing occurs in dreams.

If you dream about a horse and then a man, you'll fill in the narrative gap by imagining the animal belongs to the man. Through this process, you end up with a dream that, though perhaps strange, advances in a somewhat logically plausible manner.

### 6. Dreams represent thoughts indirectly or through symbols. 

Sometimes events from your life can show up directly in your dreams. For instance, a lake you passed earlier or a book you saw might show up in a dream as a lake or a book.

However, in other cases, this process can be much more obscure. This is especially true for abstract thoughts and impressions, which are represented indirectly in dreams.

Just imagine you ran into Mr. X during the day. After the encounter you thought to yourself, "wasn't he on his high horse?"

Such a thought can only be expressed indirectly in a dream, which means you might dream of Mr. X literally sitting on a horse.

Or say you lost your train of thought at some stage during the day. In your dream, this might come up as a typed text that's missing the final lines.

Dreams also contain a variety of symbols. Certain objects often, although not always, symbolize the same thing. Just take "king" and "queen," which tend to symbolize a father and a mother. Or right and left, which often stand for what's morally right and morally wrong.

Many of these symbols may have sexual connotations too. Any weapon, stick, or long object can be a symbol for the male member. A small box, oven, ship, or vessel of any kind can similarly stand in for female genitalia. And finally, ascending a steep hill or climbing a ladder can be a symbol for sexual intercourse.

Such symbols are important because they allow you to better interpret the meaning of your dreams. Just take a female patient of Freud's who dreamed that she was confidently strolling down a public street wearing a lopsided straw hat. In this case, the hat was a symbol for her husband's genitals, and the dream was produced by a fear that they were misshapen. This fact came out when she asked the author if it was strange for a man to have one testicle hang lower than the other.

### 7. Some dreams are shared by all people; they even stem from the same roots. 

Have you ever noticed that there are some dreams that everyone seems to have? What wish do these common dreams fulfill?

Well, one such dream is of being naked in public; it fulfills a wish for exhibitionism. As kids, people often enjoy being nude in front of others without giving it a second thought. But as children grow older, adults begin to reproach them for such behavior.

And so people retain their exhibitionist desires but repress them. As a result, we dream of being naked in public, thereby fulfilling this repressed exhibitionist wish.

That being said, such dreams are also incredibly embarrassing, a feeling that serves as a distortion, which hides the true underlying wish.

Another common dream is one of flying or falling, both of which might originate from childhood games. After all, children enjoy being thrown up in the air or bounced on a relative's knee, being suddenly dropped and caught. Dreams about flying or falling may actually stem from a wish to relive that exhilaration.

And finally, dreams involving the death of a loved one correspond to childhood fantasies. Many people have had such dreams. In certain cases, such dreams are a result of actually wishing death upon the person in question.

So where does such a sadistic wish come from?

As children, people can be extremely covetous of their parents' attention. When a three-year-old gets a new baby brother, she might respond by wishing he didn't exist at all.

Not just that, but children also don't have the same understanding of death as adults. For a three-year-old, dying is just another word for being far away forever. In this way, a child can comfortably wish for her brother's death so she can get all of her parents' attention again.

### 8. Children may wish for the death of a parent so that they can be closer to the parent of the opposite sex. 

We've seen how people might wish death upon their siblings, but that's actually not the most sinister of our lingering childhood desires. We may also wish for one of our parents to die so that we can be closer to the other, and this desire is also expressed in our dreams.

But where does it come from?

It all begins with the awakening of sexual desire during childhood. This early sexual desire is not about sexual intercourse, which children can't even understand, but can be seen in the way boys take a special liking to their mothers and girls to their fathers.

Naturally, parents reciprocate this affection and tend to be strict with children of their own gender, while spoiling a child of the opposite gender.

Children cherish this loving relationship and will do absolutely anything to maintain it. Just consider a scenario in which, every time a boy's father goes away, he gets to sleep in his mother's bed. Then, when the father comes back, the boy has to go back to his room.

He comes to see that if the father were gone forever, he could always be close to his mother at night. He also knows, from conversations about his grandparents, that "being dead" means going away forever. Because of this, the boy wishes his father would die.

Such a childhood wish is expressed in brilliant detail in Sophocles' _Oedipus Rex_, an ancient Greek tragedy. In this play, the protagonist Oedipus kills a man whom he doesn't realize is his father, and marries his mother who bears four of his children. When he learns the truth, he blinds himself and goes into exile.

We can relate to Oedipus because on some level, we can imagine experiencing his fate ourselves. However, as adults, most people have repressed any wish to follow in Oedipus' footsteps and recoil at the mere thought.

### 9. Interpreting dreams is essential for understanding the psyche and treating its issues. 

Now you know where dreams come from and how they're formed. But interpreting dreams brings much of the psyche to light too.

By analyzing dreams, you can learn about the inner workings of that most mysterious entity called the human psyche. We know that dreams can express suppressed wishes that often originate in childhood.

These suppressed wishes are active, although you're not conscious of them in waking life. They're hidden in the _unconscious._

Analyzing dreams can help people understand and treat _psychosis_, a state in which a patient loses touch with reality. Psychotic episodes take place when the repressed wishes of the unconscious are insufficiently censored, leading to their expression during waking life.

Dreams can also help treat _neuroses_, in which repressed wishes result in observable symptoms like phobias.

But can we really trust this method of analysis? After all, some associations seem random.

In Freud's world, there's no such thing as a random connection, only surprising and revealing ones.

Just take the earlier example, in which the word "Gartner" sounds like "gardener." A garden in a dream might remind a patient of an accusation made by Mr. Gartner, claiming that the patient doesn't work hard enough.

Such seeming randomness makes it essential to dig deep for deeper connections.

In the "gardener-Gartner" example, the first word sounds like the second, but more deeply, gardening might also be the patient's most cherished hobby.

And if you need further proof that analysis works, just look at the results. Surprising connections between dreams and waking experiences are consistently uncovered through this kind of interpretation, and these connections provide invaluable information about the way the human psyche truly functions.

### 10. Final summary 

The key message in this book:

**Dreams aren't the result of divine inspirations, nor are they random images. They're manifestations of the dreamer's psychic inner world, and they can be interpreted and analyzed. In the process, we can learn about ourselves as individuals and alleviate issues of the psyche as they arise.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Subliminal_** **by Leonard Mlodinow**

_Subliminal_ shows us as we are, under the bonnet. It's about how the unconscious mind is in charge, working away like an efficient yet imperfect machine, while we go on with our lives unaware. The reader finds studies, examples and anecdotes about the peculiarities of the unconscious mind, such as the pitfalls of memory recall, choosing a mate, buying a stock or scheduling a project. These are mined from historical events, science experiments and the authors' own experiences, as well as from his friends in the scientific community.
---

### Sigmund Freud

Sigmund Freud (1856–1939) was an Austrian neurologist who founded psychoanalysis — a revolutionary practice that treats psychopathologies through structured conversations between a patient and an analyst.

