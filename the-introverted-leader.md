---
id: 5398646e64663700072e0000
slug: the-introverted-leader-en
published_date: 2014-06-10T14:22:14.000+00:00
author: Jennifer Kahnweiler
title: The Introverted Leader
subtitle: Building on your Quiet Strength
main_color: 40728D
text_color: 40728D
---

# The Introverted Leader

_Building on your Quiet Strength_

**Jennifer Kahnweiler**

Kahnweiler explores the specific challenges introverts face in an extroverted business world. She then sets out to show how introverted executives can push their limits, employ their characteristic strengths and still become great leaders.

---
### 1. What’s in it for me? Learn how introverts can overcome the challenges of an extroverted world. 

What is a leader's role? They need to coach, convince and direct their team, negotiate with business partners and represent their company in a winning way. This reads like the perfect job description for intelligent extroverts — after all, extroverts tend to be naturally eloquent and assertive, like to be the center of attention and enjoy working with others.

Yet as it turns out, many highly successful leaders are introverts. In fact, introverts have particular strengths that extroverts don't have — but to get to the top of the ladder, they had to work extra hard.

Jennifer Kahnweiler interviewed more than 100 introverted professionals to learn how they harness their strengths, overcome challenges and develop strategies to succeed in an extroverted business culture. The results are summarized in these blinks.

Keep on reading and you'll find out why cocktail parties can be the biggest obstacles to an introvert's career.

You'll also find out how being an introvert could make you look like a sluggish thinker — even if you're the most brilliant person on earth.

Finally, you'll find out how to harness the power of introversion to get the one up on extroverts.

### 2. There are more introverts than you think – even among highly influential people. 

Whether it's in a chatty classroom or at a party, it often seems that extroverts greatly outnumber introverts. But appearances can deceive: there are actually quiet people in every walk of life — and they aren't even a minority.

In fact, introversion and extraversion are basic temperaments that are evenly distributed throughout the population.

The terms _extrovert_ and _introvert_ were first introduced by the psychologist C.G. Jung to distinguish between two basic personality types:

Introverts tend to direct their attention inward, focusing on their own thoughts, whereas extroverts are active, social and focus on their external environment. The two types also differ in how they recharge their mental batteries: extroverts are energized by social interaction, while introverts recover energy through quiet, solitary contemplation. Finally, their communication styles are different: extroverts are outspoken and decisive, whereas introverts are reserved, prefer listening to talking and ponder all their options before taking action.

So how many introverts are out there? Scientists interviewed different groups of people about introverted personality traits such as needing a lot of quiet time or preferring listening to talking.

The result?

About 47-55 % of the US population are introverts.

But what about business and politics — surely there is only place for extroverts there?

Indeed, our business culture does seem to favor extroverts by emphasizing the importance of socializing, decisiveness and gregariousness over introvert traits like reflectiveness or conscientousness. Yet one study shows that no less than 40% of all executives describe themselves as introverts. The most famous of them all was probably President Abraham Lincoln, known for his tendency to frequently withdraw inside himself: he would often be found alone in his library reading law books instead of socializing.

> _"There is a difference between introversion and shyness. Shyness is driven by fear and social anxiety"_

### 3. Introverts miss out on opportunities because they don't go for the limelight. 

Imagine a crucial meeting where many employees are fighting to share their thoughts on a marketing problem. Each individual is competing for the boss's limited attention — and if you're an introvert, this is one area where you could lose out.

Why?

Because if you don't speak up in a meeting, your ideas may never get heard — whilst the loud and the outspoken end up with most of the desirable assignments and project funds. And of course, extroverts are much more likely to raise their voices.

For example, maybe you have a great idea of how to deal with that marketing problem, but you hesitate to enter the fray. In a setting like this, even your best ideas won't speak for themselves: to keep them from getting lost, you'll have to speak up and convince your team.

And it's not only in meetings that introverts can miss out on opportunities: if you keep a low profile at work, you might be overlooked when your boss looks for the right person to take charge. 

This is because your boss may not have the time to search for the most suitable person to take on a prestigious project — so he'll probably assign the task to someone who recently made a favorable impression. For example, if an extrovert and an introvert are both performing well, but the extrovert is also drawing more attention to his performance, he'll be more likely to get the promotion. Therefore if you want to be entrusted to take charge, you need to make sure you stay on your boss's radar — and that he doesn't forget about your accomplishments.

So next time you see your boss, give him a subtle reminder about your performance.

### 4. An introvert’s need for time alone can be a disadvantage. 

Everyone who's worked in an office knows that sometimes people prefer to work alone. But introverts need that alone time even more — to a point where it can be to their disadvantage.

It's important for introverts to regularly work in solitude, but it can take diplomatic skill and effort to defend this preference.

This is because a typical extrovert-friendly business setting with open offices, frequent meetings and continuous teamwork can fail to provide the solitary time necessary for introverts to recharge their batteries. But extroverted colleagues might not understand an introvert's need for solitude — and they might even feel rejected if an introverted colleague doesn't always welcome their company.

Another characteristic which disadvantages introverts is their reluctance to participate in the informal socializing between conferences or meetings. This time is often used to strengthen connections, exchange information and make deals — without the introverts. For example, on a business trip, an introverted account executive chose to spend some time alone instead of golfing with the other executives. At the official meeting, he found out that most of the vital decisions had already been made on the green. 

So introverts should just make sure they socialize more, right? Problem is, going too long without spending time alone can hurt an introvert's performance and morale.

This is because introverts tend to work best when they're alone, and need solitude to recharge their batteries. If they continually lack alone time, they will end up exhausted and consistently less productive. After some time, they may even suffer from stress-related symptoms like headaches and back problems. In fact, an introvert with no opportunity to work alone throughout the day will be as unhappy as an extrovert passing his days in a solitary office.

If you're an introvert, make sure you plan your solitary time carefully.

### 5. Extroverts may misjudge a cautious, quiet and reserved colleague. 

Imagine you're an introvert: when your boss asks you to deal with a problem, you'll tend to retreat to your office and take the time you need to concentrate on your assignment. Two weeks later, a staff evaluation comes up, and you're shocked to find you've been criticized for working too slow and acting aloof.

Being on the receiving end of this kind of misperception is common for introverts. One reason introverts are mistaken for slow thinkers is their tendency to carefully consider what they want to say and make sure it's the best answer they can offer. Extroverts on the other hand are prone to contributing a suggestion as soon as it comes to their mind — and the combination of these two thinking styles can lead extroverts to believe that introverts are slower thinkers. 

For example, imagine two equally bright pupils, an extrovert and an introvert. In class, both come up with great ideas for the science fair. But the extrovert blurts out his suggestion immediately, whilst the introvert double-checks her own idea and answers much later. Result? In comparison, the introvert will appear slow — and to many, less intelligent.

Introverts may also be mistakenly characterized as cold, aloof or scheming. Introverts are less emotionally expressive, often focusing on their feelings rather than expressing them. For example, if you've suffered a loss, they might become very quiet because they empathize deeply with your grief. But outwardly, this could look like aloofness. 

And since extroverts are more likely to express their emotions, when someone doesn't express their emotions, it makes them think one of two things:

Either the introvert is insensitive — or he isn't sharing his feelings with his coworkers because he dislikes them. This can lead an extrovert to believe that the introvert is either cold-hearted or has become estranged from the team — which can have dire consequences in an office environment.

If you notice one of your colleagues takes their time to speak, don't judge them too soon: they might just be introverted.

> _"In the absence of words, sinister assumptions can be formed and projected onto the quiet person."_

### 6. Carefully choosing words is one of an introvert’s key assets. 

Do you hesitate before you share your thoughts? Do you pause to check the pros and cons of your statement? If so, you may well be introverted. And whilst some might misunderstand why you're taking your time to react, it can also be a big advantage.

Why?

Because not blurting out your thoughts prevents you from making a costly faux pas. And in some professional fields, one wrong word can cost your job — for instance if you're a press spokesman, a politician or a diplomat. For example, think of a diplomat impulsively blurting out some comment about a first lady's funny hat!

Another advantage to thinking before speaking is that when everything you say is well-founded, your statements will gain extra consideration.

By thoroughly processing your thoughts before you share them, you'll greatly improve the quality of your statements — and people will notice. Imagine a panel discussion where you provide all the relevant data, present your position in a conclusive way, and anticipate counter-arguments. In consequence, you'll appear more competent and worth listening to compared to a co-panelist who hogged the mic to share nothing of substance.

Furthermore, if people know you don't speak out impulsively, you're more likely to get access to helpful privileged information. When people know you can hold your tongue, they expect their secrets to be safe with you — which means they might entrust you with helpful, privileged information. For example, your boss might confide in you that he secretly applied for a job in another company. If you wanted to become your boss's successor, this knowledge would allow you to start distinguishing yourself from the crowd.

In the following blinks you'll learn how introverts can improve their leadership skills.

### 7. Introverts’ keen listening and observation skills can make them excellent leaders. 

To further our exploration of introverts, let's consider the case of Judy, a quiet four-year-old. Judy rarely plays with her peers, but is a brilliant observer and knows the exact activities and playmates each of her peers prefers.

In fact most introverts like Judy are good observers. This is because introverts often don't participate in games, banter, chit-chat or discussions, but rather stay at the fringes and intently observe the interactions.

For example, Judy might be reluctant to play tag — but while her peers are focusing on trying to escape, she'll spend her time closely observing the action, learning who always giggles nervously before being caught and which children cooperate to divert the chaser.

This tendency to pay close attention also makes many introverts great listeners: when they listen to you, introverts really focus on the ideas you're sharing. They're also less distracted than extroverts, because they aren't thinking up witty replies. These tendencies lead many introverts to become well practiced listeners: if you had a pair of fraternal twins, one of them a talkative extrovert, the other an introverted listener, by the age of eight, the introvert will have much more listening practice than his brother.

But why are these skills important? 

Because these observational and listening skills are crucial to being a great leader.

Through attentive listening and observing, a leader gains useful information about her team's needs. For example, she'll know the specific motivation that can help a team member overcome a dry spell and improve overall performance. Also, good listening skills help build rapport with customers and superiors, because people feel respected and valued when you listen to them attentively.

### 8. Networking is often seen as an exclusively extroverted chore – but there's an introverted way of doing it. 

Meeting the right people and nurturing relationships are essential for a leader's success. But does this mean that introverts have to exhaust themselves spending their workdays in team meetings and their evenings collecting business cards?

The tension between introverts and traditional networking arises because extroverts and introverts prefer different types of interaction: extroverts enjoy small talk, whilst introverts prefer in-depth conversation. For introverts, networking often consists of too much small talk which they can't connect to.

But there are other ways of networking for introverts — like networking websites. This is great for introverts because many of them prefer to communicate through writing, which they can do in the relative quiet of their cubicle. Also, many introverts are already proficient writers, and are adept at making good first impressions via email or messaging. Finally, on a website introverts have the opportunity to think through and revise everything they want to contribute as they desire.

Another great way for introverts to network is to focus on one-on-one conversation. This allows them to get to know the other person in depth, and increase rapport by adapting to each person's need. For example, they can find out about the way the other person thinks — like if they prefer abstract concepts or a hands-on approach. In a focused one-on-one conversation, people are also more likely to open up and share intimate details which are useful to leaders.

Finally, introverts can connect with people who would keep silent in a team meeting — maybe other introverts — as they'll appear much more present and approachable than in a meeting.

### 9. In our extroverted business culture, introverted leaders need to push their limits to succeed. 

Imagine: maybe in a hundred years there will be no more offices, everyone will work from home and every social interaction will take place on the internet: this could be the era of the introverts. But until then, introverted leaders will have to push their comfort zones to adapt to our extroverted world.

Why?

Because if introverts stay in their comfort zone, they'll avoid many kinds of interaction. For example, their dislike of the limelight will make them avoid public speaking. Or they might steer around unofficial get-togethers with their colleagues in an attempt to stock up on solitary time, and avoid the small talk typical of informal gatherings. Finally, they might email urgent messages instead of calling their co-workers on the phone, because many introverts prefer writing to talking.

But embracing these kinds of interaction and overcoming fear and discomfort is crucial for an introverted leader's career. As a leader, an introvert must educate her team, represent her company and publicly present her ideas — all tasks which will involve public speaking at some point. And unofficial get-togethers actually provide introverts with great opportunities to form professionally helpful alliances, learn more about their team and create rapport.

Finally, emailing is an ineffective way of communicating important news, because mails take time to be opened — or can be missed altogether.

### 10. Preparation and practice will help advance an introverted leader’s career. 

Some people struggle so much with small talk they come to believe they're simply lacking the small talk gene. But they're mistaken — because in fact, everyone can learn small talk.

Small-talking is a particular challenge for introverts, and their tendency to shun it can make them appear rude, unfriendly or incompetent — which can seriously harm their careers. But if introverts _prepare_ for small-talk in advance, they can become confident enough to stop avoiding it. They could for example prepare backup topics, entertaining anecdotes and generic open-ended questions they can employ when needed. 

Preparation can also help introverts speed up their response time — and avoid being intellectually underestimated. As we saw, introverts take their time to work out a well-founded answer. But if an introvert anticipates and thinks about some likely questions before a meeting, she can respond quickly when the time comes. 

Introverts benefit not only from preparation, but also from regularly practicing small talk and public speaking — two crucial aspects of being a good leader. Like preparation, practice can address any of the challenges of being an introverted leader. Maybe you lack practice at public speaking because you shun the limelight, or avoid small talk because you like deep conversations. But if you practice things you wouldn't do naturally, like making small-talk, you'll gradually get used to them. It's like a right-handed person practicing painting with their left hand: eventually, it'll stop feeling awkward.

Practice also helps you add tools to your communication skill set — like the ability to easily raise your voice or dramatically whisper during a public speech to emphasize your key point. This is especially important when preparation is not an option, like when a position requires giving great ad lib presentations.

### 11. Final summary 

The key message in this book:

**Being an introvert can make taking on leadership positions challenging, but there are ways of overcoming the obstacles. Your listening and observational skills and your tendency to carefully choose your words can give you an advantage over extroverted leaders and support your success.**

**Suggested futher reading: _How_ _to_ _Be_ _a_ _Positive_ _Leader_ by Jane E. Dutton and Gretchen M. Spreitzer**

_How_ _to_ _Be_ _a_ _Positive_ _Leader_ examines cutting-edge research from the field of positive organizational behavior, in which companies aim to foster both a positive attitude to work and high performance among employees. The research is complemented with vivid examples from real organizations.
---

### Jennifer Kahnweiler

Jennifer Kahnweiler is a renowned executive coach who specializes in the development of introverted leaders. She has worked as a consultant for Fortune 500 companies, and is also the author of _Quiet Influence: The Introvert's Guide to Making a Difference_.

