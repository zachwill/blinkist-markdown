---
id: 5714be52344e1b0007a4c8a0
slug: the-invention-of-nature-en
published_date: 2016-04-22T00:00:00.000+00:00
author: Andrea Wulf
title: The Invention of Nature
subtitle: Alexander von Humboldt's New World
main_color: 96A055
text_color: 6C733D
---

# The Invention of Nature

_Alexander von Humboldt's New World_

**Andrea Wulf**

_The Invention of Nature_ (2015) shines a light on the extraordinary life of explorer and scientist Alexander von Humboldt. Discover Humboldt's amazing perspective on our relationship to the world and find out how his ecological discoveries and observations are just as relevant and profound today as they were in the nineteenth century.

---
### 1. What’s in it for me? Get to know Alexander von Humboldt – the great nature discoverer. 

Who invented nature? Well, obviously no human can claim to have done that — not even Alexander von Humboldt. But if we talk about who created the human vision of nature as something understandable and able to be categorized, then von Humboldt is certainly a key candidate.

That's what these blinks are all about — the invention of the _idea_ of nature. They provide you with great insights into Alexander von Humboldt, ranging from his personal life to the detailed observations and descriptions that he made on his grand voyages at the dawn of the nineteenth century, in particular through South America.

In these blinks, you'll discover

  * why Humboldt — and Goethe — considered science and poetry to be interconnected;

  * why he was so fascinated by volcanoes; and

  * how Humboldt was ahead of his time in explaining how human actions could harm nature.

### 2. From an early age, Alexander von Humboldt had a longing to see the world. 

Let's travel back in time to meet the adventurous explorer, Alexander von Humboldt. He was born to a respected and aristocratic Prussian family on 14 September 1769. His father was an advisor to King Wilhelm III, and his mother came from a wealthy manufacturing background.

Alexander had one brother, Wilhelm, who was two years older than he was. Unfortunately, their father died when Humboldt was only nine years old. Afterward, their mother grew reclusive and let tutors take most of the responsibility of raising her children as intelligent and honorable young adults.

But unlike his brother, Alexander always preferred nature to studying. 

While Wilhelm took to his books, Alexander roamed the countryside estate where he lived, filling his pockets with plants and bugs and earning the nickname, "the little apothecary." Though his mother wanted both of her boys to be civil servants, Alexander was fascinated by the new age of exploration and grew up dreaming of leaving home and seeing the world.

So, as young Alexander von Humboldt reached adulthood, he spent his non-working hours following his passion for science and nature. 

He dutifully continued his education, excelling in science, math and language. But he found a special interest in geology and, at the age of 22, landed steady work as a mining inspector. While improving conditions for miners, Humboldt also studied the underground plant-life of the mines and published his first book on subterranean flora.

Also, whenever he found the time, he obsessed over the latest scientific advances in zoology, botany, and the mechanics of life.

He became particularly interested in _galvanism_, the study of biological reactions to electric currents, even using his own body to conduct experiments!

But all the while, Humboldt found it more and more difficult to calm a restless mind that longed to escape Europe. But before he could venture to uncharted lands, he met the man who would change the way he saw the world around him.

### 3. Before he left Germany, Humboldt was already learning to combine art and science. 

Meanwhile, Humboldt's brother Wilhelm had moved to the German town of Jena where he fell in with a group of friends that included Johann Wolfgang von Goethe, widely considered the country's greatest poet.

Goethe was already internationally famous by the 1790s and was becoming one of the founders of the German Romanticism movement. His novel _The Sorrows of Young Werther_ became a generational landmark. He would soon achieve further renown by publishing _Faust_.

So, when Alexander paid his brother a visit in 1794, Wilhelm knew Goethe was also keen on the sciences of zoology and botany, so he invited him to meet his brother. 

The two proved to be terrific intellectual sparring partners, endlessly chatting and helping each other develop their scientific theories. Goethe introduced Humboldt to the writings of philosopher Immanuel Kant and the ideas around _subjectivity_ : that our understanding of the external world is shaped within our mind as we receive it through our senses. 

Goethe and Humboldt came to the understanding that science and nature should also be part of this process of subjectivity. And in order to truly understand nature, including geology and botany, one must experience and interact with it.

Both Goethe and Humboldt agreed that simply sitting in a room and classifying rocks, plants and animals would not lead to an understanding of them. One had to leave the office or laboratory and experience them.

For Goethe and Humboldt, poetry and science were all part of the same process.

Poetry and science might sound like two completely different disciplines, but with this new perspective, Humboldt began seeing connections in everything around him. 

In the Romantic writing style of Goethe, Humboldt saw the best way to help others understand what he was observing in nature. By writing his own feelings and awakening those feelings in others, he could connect with people far more effectively than through any purely scientific text.

And Humboldt was about to put this new perspective to work on his long-awaited adventure.

### 4. Humboldt’s arrival in South America was an eye-opening experience. 

In November 1796, Humboldt's mother passed away. No longer under the obligation to please her, Humboldt took his inheritance and resigned his position as mining inspector. He quickly set about planning his long-awaited expedition by stocking up on equipment and scientific tools.

But finding a boat to take him away from Europe wasn't so easy.

In the late 1790s, the ongoing Napoleonic Wars made it difficult to find safe passage to other continents. So, after many frustrating delays, it wasn't until 1799 that Spain finally agreed to provide Humboldt and his French botanist expert and friend, Aimé Bonpland, with passage to South America.

On 16 July 1799 Humboldt's childhood dreams came true when he reached the kaleidoscopic wonders of New Andalusia, now known as Venezuela.

They landed in the province of Cumaná, and as soon as Humboldt and Bonpland disembarked they were presented with an overwhelming display of palm trees, fish and colorful birds including the exotic flamingo. It was truly an eye-opening experience, unlike anything they'd seen in Europe.

But rather than seeing these things as totally foreign, Humboldt looked at his new discoveries in a global, interconnected context. As they collected samples of everything from rocks and plants to insects, Humboldt recorded his observations and always compared them against his knowledge of Europe.

By doing this, and not just cataloging things in isolation of one another, Humboldt started to understand how connected everything in the world was.

For example, he saw caverns around Cumaná that reminded him of similar ones in Europe's Carpathian Mountains. He saw trees in the same area that reminded him of Italian pines. 

But perhaps the most eye-opening event was the earthquake that greeted him upon his arrival.

The earthquake was a revelation, changing his whole perception of the planet's creative and destructive forces. Many scientists at the time were convinced that the forces shaping our lands were oceans and water. However, the earthquake had convinced Humboldt that these seismic forces had created earth's landscape.

### 5. South America was filled with both wonders and warning signs. 

Humboldt and Bonpland continued their journey along the coast of the Caribbean Sea, and down into New Granada, traveling along the Orinoco River. 

In 1800, Spain was colonizing South America. As they traveled, Humboldt not only observed the brutal practices of slavery but also noted the colonists' disregard for South America's natural bounty. He saw how they were destroying ecosystems by mining for valuable minerals and planting cash crops like sugar and indigo.

As they passed Lake Valencia, its particular ecosystem offered Humboldt insight into the importance of trees and the dangers of deforestation.

Locals in the area told Humboldt that the water levels had dwindled rapidly in recent years. Humboldt took note of the deforestation and excessive irrigation that was going on in the area and it was clear that the lake's ecosystem was being harmed by such practices.

He realized that trees were of vital importance in storing water, protecting soil and impacting the climate through oxygen production and the cooling effect of shade. 

But Humboldt was just getting started. All along the trip, he was developing ideas about how human actions can cause chain reactions in nature.

As they traveled along the Orinoco River, Humboldt witnessed the perfect system of hunter and prey among the amazing animal life of jaguars, crocodiles, monkeys and capybaras. 

Humboldt noticed that when hunting and deforestation depleted the number of jaguars and crocodiles in certain areas, the numbers of capybaras would explode. 

And he continued to see how humans could show little regard for nature.

For example, Spanish monks were running the risk of completely extinguishing the local turtle population by using the oil in their eggs to fuel their lanterns.

It was clear that people were still under the long-held beliefs promoted by Aristotle, Francis Bacon and even the Bible, which suggested nature existed only to serve man. But everywhere he looked he could see people had little understanding of how nature really worked.

> _"Man can only act upon nature and appropriate her forces to his use by comprehending her laws."_

### 6. When Humboldt finally reached the top of Mount Chimborazo, he was struck with a revelation. 

The grueling journey through South America continued into 1802. Humboldt and Bonpland were plagued by ruthless mosquitos, fever, dysentery, blizzards, and more than a couple of near-death experiences.

But nothing could keep Humboldt from his goal of reaching the Andes and encountering the fascinating volcanoes that lined the horizon. 

His vision of a connected world would be perfectly embodied by these volcanoes.

Unlike many at the time, Humboldt didn't view volcanoes as an isolated force. He saw them all as extensions of the same force within the earth's core. 

Along his journey, Humboldt had climbed and inspected many volcanoes but none would compare to the inactive volcano, Chimborazo, located in modern-day Ecuador.

At 21,000 feet, Chimborazo is not the tallest summit in the world, but due to its proximity to the equator, its peak is considered the furthest point from earth's center.

Humboldt reached Chimborazo in June 1802, and it turned into an arduous climb. Along the way the men were forced to crawl on all fours in order to continue along narrow ridges, they suffered from altitude sickness, and jagged rocks tore up their shoes and bloodied their feet. 

But every few hundred feet Humboldt always stopped to take measurements and collect samples of everything around him. And as he was climbing he realized he was walking through layers of vegetation that represented the entire plant world.

By 19,413 feet, the path was cut off and they could go no further.

Whatever discomfort Humboldt was suffering was nothing compared to the view with which he was now presented. He realized he was looking down upon the earth's equator and from the base to the summit he could see all of nature laid out before him. 

It was on Chimborazo that everything fell into place. Comparing his previous observations on the Alps to Chimborazo, he saw the threads of nature that connected everything.

### 7. Humboldt’s ideas quickly spread across Europe and the United States. 

When the time came for Humboldt to leave South America in 1803, he'd compiled hundreds of sketches and tens of thousands of geological, astronomical and meteorological observations. On top of that, his expedition had collected around 60,000 plant specimens — 2,000 of which would be seen as completely new species to Europeans.

But before returning to Europe, Humboldt stopped to visit President Thomas Jefferson in the United States.

Humboldt supported America's fight for independence and though he opposed slavery, he and Jefferson were in agreement on issues such as the importance of agriculture and sustainability. In fact, Jefferson considered himself more of a farmer than a politician and embraced Humboldt's ideas on agriculture.

Plus, Jefferson was eager to receive any information Humboldt had on the Spanish colonists and Mexico. As always, Humboldt was a believer in the free exchange of information and helping others expand their scientific knowledge.

When Humboldt finally did return home to Europe, he received a hero's welcome. It was now August 1804, almost five years after he'd left.

Humboldt's mind and notebooks were filled with big ideas and he was eager to finally share them with the rest of the world in a book.

But not just one book! After settling down in Paris, Humboldt set about his ambitious plans of turning his research into a series of books.

His trip received plenty of international attention, so audiences were ready when his first book, _Essay on the Geography of Plants_ hit the bookstores in 1805. Considered the world's first ecological book, it was a revelation and instant success.

Its centerpiece was a fold-out portrait of what Humboldt called his "_Naturgemälde_," an illustration of Chimborazo that showed the spectrum of plant life that existed in relation to climate zones and altitudes around the world.

The book was the first to show direct relations between plants, climate and geography. It also described the ancient connections between continents — a century before popular science would recognize what is now known as the _continental drift_.

### 8. Humboldt’s ideas influenced politics as well. 

Humboldt continued to write and publish his observations, and in 1807 he released his landmark _Views of Nature_. His use of poetic language to describe his findings made the book a blueprint for nature writing for future generations.

As Humboldt continued to promote the use of art, poetry and lyricism to speak of nature, he served as an inspiration to many, including the revolutionary Simón Bolívar.

Fascinated by Humboldt's South American adventure, 21-year-old Bolívar visited him in Paris shortly after he returned.

Bolívar was born in the province of Caracas, which Humboldt had traveled through. They discussed South America's uncertain future and the cruel treatment of the natives by the Spanish colonists. Bolívar felt a passionate desire to see the Spanish driven out of his homeland and soon promised to free the people of South America.

In 1808, Bolívar made good on that promise and set sail for South America.

It would take years of bloody battles to win independence away from the Spanish. During that time Bolívar kept his revolutionary followers inspired by using nature and the poetic language Humboldt made famous. Bolívar published and recited his own poetry to describe the majestic mountains and valleys, reminding his people what they were fighting for.

During the years of Bolívar's struggles, between 1808 and 1811, Humboldt was not shy from expressing his own political feelings. He published the four-volume series _Political Essay on the Kingdom of New Spain_.

Both Thomas Jefferson and Bolívar read these books, which were filled with facts and hard data pointing to the disastrous effects of colonization. The book included scathing indictments of slavery as well as the damage caused by mining and the removal of naturally growing vegetation in favor of planting cash crops like corn and sugar.

These were noble statements, but as we'll see in the next blink, they may have come at a price.

### 9. Despite his popularity, Humboldt had trouble launching another expedition. 

In 1815, Humboldt's success in the publishing world continued with another highly influential book called _Personal Narrative_. This was a more linear travelogue of his South American adventure and would go on to inspire many future explorers.

Humboldt had been living in Paris for a while, but in the 1820s, following Napoleon's exile and the restoration of Louis XVIII, he saw a rise in censorship and oppression.

Disillusioned with politics, Humboldt returned to Berlin and conducted a series of immensely popular lectures where he presented his vision of a deeply connected world to fascinated audiences. Crowds were mesmerized as he linked together the stars, the land and the sea with discussions of art and poetry.

In 1828, this culminated in a groundbreaking scientific conference that united 500 scientists from around the globe in Berlin to exchange ideas and unite interdisciplinary minds.

However, despite his popularity among writers and scientists, his disapproval of colonialism didn't sit well with the political forces Humboldt would need to launch another expedition.

Ever since Humboldt had returned from South America, he'd longed to embark on another adventure.

At the top of his list of places to go was the Himalayas. He saw the Indian mountain range as the perfect place to complete his observations and truly tie together his connective view of nature and the world.

However, to get there, he would need permission from the East India Trading Company. This organization was both a powerful British commercial enterprise and the military power that controlled much of India.

Though Humboldt was a member of Britain's Royal Society and a good friend of Prime Minister George Canning, the East India Trading Company continually refused his attempts throughout the 1810s and 1820s. 

Unfortunately, they didn't quite care for Humboldt's public contempt of colonialism — considering that colonialism was the core principle behind the East India Trading Company.

Humboldt eventually reached out to the Russian Empire to see if he could gain access to India through its territories, but as we'll see in the next blink, the Russian finance minister had an interesting counter-offer.

### 10. Humboldt’s journey through Russia would provide the final ingredients for his work. 

In autumn 1827, Humboldt received a letter from the Russian finance minister asking him if he was interested in taking a funded trip across Russia. The nation had recently discovered platinum in the Ural Mountains and its rulers were interested in other potential discoveries.

It wasn't the trip to the Himalayas Humboldt was hoping for, but he jumped at the chance to tour the wilds of Russia.

However, the trip didn't start out in a promising way.

By mid-June 1829, Humboldt was travelling along the Trans-Siberian Highway. While he was happy to be exploring outside Europe, at first, the trip only offered a barren landscape that looked a lot like what he'd seen in Germany. 

To make matters worse, his every step was being observed by associates of Tsar Nicholas I.

To fund the trip, Humboldt had agreed to check in on Russia's various commercial mines and file updates at every stop. Though it was a nuisance, it did offer Humboldt the opportunity to test out theories of how certain minerals appear together all around the world.

For example, the mines proved that the gold and platinum deposits in Russia were similar to deposits found in South America. And much to the delight of the Russian government, Humboldt revealed that the land also contained diamonds.

But Humboldt grew tired of the mines, so when he reached the easternmost part of his trip at Tobolsk, he decided to venture off-course and head for the Altai Mountains, where Russia meets Mongolia.

By August 1829, the now 60-year-old Humboldt was happily climbing the Altai Mountains, crawling into caves, collecting rocks and plants. 

His observations would provide the final details needed for his connective view of the world. The Altai Mountains are approximately 3,500 miles east of Berlin, as far away as South America is to the west, making for perfect comparisons.

Humboldt now had what he needed to start his masterwork, and he returned to Berlin on 28 December bubbling with ideas.

### 11. Cosmos was the influential culmination of Humboldt’s message to the world. 

It was Humboldt's goal to produce a single work that would fully represent his view of a world that is entirely connected and intricately woven together. This work would expand upon the "_Naturgemälde_ " idea that struck him atop Chimborazo 40 years previously, and ended up becoming a five-volume book called _Cosmos_.

While science was drawing lines between disciplines and separating itself from the arts, the aim of _Cosmos_ was to bring it all together.

In the 1830s, professional scientists were drifting further apart into specialized fields like chemistry, biology and astronomy, and narrowing their perspective to individual elements of the universe.

The message of _Cosmos_ was to unite, showing how nothing behaves independently of the world around it. Humboldt wanted to reveal how a true understanding can only result when you consider everything from the stars above to the smallest organism in the sea as being part of a unified whole.

And so, _Cosmos_ would end up being his parting gift to the world.

The first volume of _Cosmos_ was published in 1845, after a decade spent on research and design. It was an instant bestseller, covering everything from celestial phenomena, geomagnetism and weather patterns to plant and animal life.

The second volume followed in 1847; it was an equally ambitious book that charted human life from ancient to modern times. Along the way it detailed the progress humans have made in politics, science and art.

Between 1850 and 1859, Humboldt worked tirelessly to publish the next three volumes, each one filling in more detail for his worldview. He sent volume five of _Cosmos_ off to the publisher two days before collapsing. 

He died at 89 on 6 May 1859. News of his death quickly spread and sent many around the world into mourning. Newspapers wrote loving obituaries, with many in the United States referring to Humboldt as one of the most remarkable men to have ever lived.

### 12. Many influential people carried Humboldt’s teachings into the twentieth century. 

Just how influential was Alexander von Humboldt? His unique perspective continues to inspire explorers and environmentalists and many people have carried his message of uniting art and science into future generations.

Charles Darwin was famously inspired by Humboldt's _Personal Narrative._

In fact, the book made Darwin want to embark on his own journey and informed the connective way he looked at nature during his voyage across the world on H.M.S Beagle.

Unfortunately, Humboldt would die before Darwin published _On the Origin of Species_, but Darwin's landmark book on evolution and natural selection expanded on the foundation Humboldt left behind.

Another man who was inspired to greatness by Humboldt was the author Henry David Thoreau.

Thoreau published _Walden_, considered a pinnacle of the _transcendentalist_ movement. Much like German Romanticism, this philosophy searched for a unified view of humanity and nature. 

Thoreau published _Walden_ in 1858 after years spent living in a tiny cabin next to Walden Pond in Massachusetts. During this time he was very taken with Humboldt's work and his name appears throughout Thoreau's journals. It wasn't until he began seeing the world through Humboldt's eyes that the unifying perspective became clear to Thoreau and he saw the bond between life, nature and poetry.

Ernst Haeckel brought art and science together for the twentieth century.

Haeckel was a German zoology professor heavily inspired by the works of Humboldt and Darwin. He understood Humboldt's vision of nature and art coming together when he looked into his microscope to study the intricately beautiful new species he'd discovered — tiny seawater organisms called _radiolarians_.

Haeckel filled his own books with drawings of nature and published a magazine called _Kosmos_. His art would, in turn, become a major inspiration for the Art Nouveau movement at the turn of the twentieth century. 

These are just three people who would help ensure that Humboldt's vision lived on into the future.

### 13. Final summary 

The key message in this book:

**Alexander von Humboldt was warning the world about the negative impact man was having on nature all the way back in the early 1800s. Since then, science has separated itself into narrow individual fields, failing to see the big picture. Humboldt's vision of the universe as one unified organism is just as relevant now as it was when he was alive.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The World Until Yesterday_** **by Jared Diamond**

_The_ _World_ _Until_ _Yesterday_ explores the lessons modern humans can learn from the primitive hunter-gatherer societies that roamed the earth before centralized governments emerged.
---

### Andrea Wulf

Andrea Wulf trained as an art historian at London's Royal College of Art. Her previous books include _Chasing Venus_ and _Founding Gardeners_. She has also contributed to the _New York Times_ and the _Wall Street Journal_.

