---
id: 561c3bac3635640007b50000
slug: the-invisible-gorilla-en
published_date: 2015-10-16T00:00:00.000+00:00
author: Christopher Chabris and Daniel Simons
title: The Invisible Gorilla
subtitle: And Other Ways Our Intuition Deceives Us
main_color: C93D4C
text_color: C93D4C
---

# The Invisible Gorilla

_And Other Ways Our Intuition Deceives Us_

**Christopher Chabris and Daniel Simons**

_The Invisible Gorilla_ (2010) explores the way our intuition is not the beacon of guiding light we think it is. In fact, it's often erroneously based on illusions. By debunking some examples of common knowledge, Chabris and Simons argue why our intuition often cannot be trusted.

---
### 1. What’s in it for me? Discover how your intuitions can deceive you. 

Do you consider yourself a smart, attentive and rational person? However you picture yourself, chances are that you often overestimate your brain's capacities.

Many self-help books want us to "go with our gut feelings" and "follow our intuition" in order to make good decisions in life. In these blinks you'll learn why this is bad advice. In fact, it will be argued that in many ways you cannot trust your intuition at all — you can't even count on it to spot a gorilla walking across the room!

In these blinks you'll also learn

  * how a police officer was able to miss a brutal attack happening right next to him;

  * how it's possible to create a false memory about sitting next to Jean-Luc Picard from _Star Trek_ ; and

  * what high ice cream sales have to do with high drowning rates.

### 2. Though we’re taught otherwise, our intuition often can’t be trusted. 

Ever tried to navigate through a situation by listening to your intuition, but ended up in an even more tangled mess? If so, you're not alone. Sometimes gut decisions can go wrong. Here's why:

We're often taught to let our intuition be our guide, to "go with your gut" because "it's just common sense." These sayings are based on the idea that our intuition — that is, our ability to understand something instinctively — is an ideal means to making decisions and judgments about situations and events. 

In recent years, self-help books on management and psychology have lauded intuitive decisions over decisions based on analysis. In Malcolm Gladwell's _Blink — The Power of Thinking without Thinking_, for example, he argues for intuition over analysis. He attempts to demonstrate this by revealing how a Greek statue appeared on the art market and was subsequently proclaimed to be a fake by art experts who trusted their gut. In contrast, several analyses weren't able to show it was fake.

But our intuition has its limits and can actually be unreliable. There are many examples of forgery slipping through the cracks, undetected by expert intuition.

For example, book dealer Thomas J. Wise found and sold several manuscripts for unknown books by well-known writers. Librarians and book collectors alike were convinced they were the real deal, but after analysis by two British dealers who took into account information about the authors' lives, the books were all found to be fake.

It also pays to remember that we have phrases in our language that tell us of the limitations of our intuition. For instance, many of us say "never judge a book by its cover" because we know we can't reliably assess something at first glance.

### 3. We believe we see more than we do, and that we’re more attentive than we actually are. 

If someone was being beaten up on the street, you would notice, right?

Well, though we're likely to notice anything unusual, when we're focusing carefully on something else, the rather unnerving thing is that we can miss just about anything.

The authors devised the Invisible Gorilla experiment, which has become a classic for demonstrating how we can miss the obvious. In the experiment, participants were asked to watch a video of people playing with a basketball, and to count the number of passes made by one of the teams. But in the middle of the video something rather odd happens: a man in a gorilla suit enters the court, stays for nine seconds and beats his chest. Surely the participants would see such a bizarre event. Actually, no. Roughly half of those in the study didn't notice the gorilla because they were too focused on counting the passes.

It's not so bizarre, then, that Boston police officer Kenny Conley missed a black police officer being beaten up next to the fence that he was climbing to try to catch a criminal; all of his attention was on the pursuit.

But we don't only miss things because we're focused elsewhere — we also fail to see what we're not looking for.

Picture being in a supermarket looking for the items on your shopping list. While you do this, chances are you're not registering a lot of the other things on the shelves. Of course this isn't a big deal, but could we miss something big and potentially dangerous? Yes. In fact, it's even easy to miss a motorcycle.

More than half of all motorcycle accidents involve collisions with cars, and 65 percent of these accidents are a result of a car turning left and the driver not seeing the motorcycle. This is because they're anticipating other cars rather than motorbikes.

> _"We are aware only of the unexpected objects we do notice, not the ones we have missed."_

### 4. Our memory is not as clear and effective as we think. 

Many of us have confidently recalled a vivid childhood memory, only to be told by a family member that our recollection is not what really happened. So why do we think we remember things so accurately?

In a study by the authors, 47 percent of people believed that memories don't change, and 69 percent reported that memories are like videos that capture reality accurately, remain unchanged over time, and can be reexamined at will.

But this isn't the case, as our memories sometimes store _more_ information than actually exists out there in the world. In one experiment, students read 15 associated words such as "slumber," "drowsy" and "tired." Ten minutes later, they were asked to recall them. What was interesting is that participants reported the word "sleep" to be on the list, even though it wasn't. Why?

As all the words were related to sleep in some way, participants simply inserted another word that fitted the others without being conscious of it. Rather than recording a perfect sequence of things or events, our memory is more occupied with recording the meaning of them.

Furthermore, some memories don't come from where we thought they did. Ken, a friend of the authors, used to tell an entertaining story about sitting next to actor Patrick Stewart in a restaurant. He recalled how the actor ordered a baked Alaska and signed some autographs. Surely he didn't just make this up. But actually, the event never happened to him, but to one of the authors. Ken wasn't deliberately lying — the story had been told to him with such detail that he believed it was his own!

This phenomenon is called _failure of source memory_ and is another way in which our memory deceives us.

> After the assassination of John F. Kennedy, a poll showed that two thirds of those questioned recalled that they had voted for the president in his last election, in which the vote was actually split 50/50.

### 5. Confidence can deceive us. 

You probably think you're smart, perhaps even smarter than the average person. This is just one way that confidence can be illusory.

It's true that we tend to overestimate our own abilities, making us confident when we sometimes have no real reason to be. 

In fact, national surveys have shown that 69 percent of Americans and 70 percent of Canadians believe they're more intelligent than the average person. This is a little odd, since 50 percent should be average or below. So around 20 percent have rather lofty opinions of themselves!

The authors found a similar result when interviewing chess players at a national tournament. Chess players have a distinct ranking, but the majority of them still thought they were underrated by at least 100 points. Why? Well, the lower the skill, the more people overestimate their abilities.

It turns out that the lower the ranking of the chess players, the more they overestimated their chess skills.

But it's not just our own inflated confidence that can muddy reality — we also incorrectly interpret other people's confidence, assuming it to be a reliable indicator of skill.

In one study at the University of Rochester in 1986, participants watched a video of a medical appointment. The doctor prescribed antibiotics in both clips, but in one he acted very confidently and in the other he looked up the disease to make sure he was correct before he wrote the prescription. The majority of the participants had more confidence in the doctor who wrote the prescription without looking up the disease than in the doctor who checked beforehand.

This goes to show that when we see someone acting confidently, we tend to presume they're superior to someone who is not so self-assured.

> _"Ignorance more frequently begets confidence than does knowledge." - Charles Darwin_

### 6. We think we know more than we do. 

Do you know how your toilet works? Chances are, you don't. Often even simple, everyday things are beyond our general comprehension.

Take a bike, for example. Most of us would claim to know how a bike works, but is this really true? British psychologist Rebecca Lawson conducted an experiment where participants had to first rate their knowledge of bikes, and then draw one.

This task may seem more fitted to an elementary school, yet many failed miserably. On a scale of one to seven, participants rated their knowledge at 4.5, even though several of them drew the chain as joining the two wheels, which would make it impossible to steer, or failed to connect the pedals to the chain, which would make it impossible to turn the chain at all. Most of us know _what_ something does, rather than _why_ it does it, and the more familiar something is to us, the more we _believe_ we understand it.

We also incorrectly believe we understand complex things if we receive a lot of information about them.

For instance, in one study by behavioral economist Richard Thaler, participants invested money in mutual fund A or mutual fund B. 

The experiment simulated a span of 25 years and participants received information on the performances of the funds either every month, every year or every five years. At each of these intervals they had the option to change their investments. Obviously, the more updates, the better, right? Wrong! Those who were updated every five years ended up with more than twice the returns because they had a longer-term view of their investment. On the contrary, those who received updates every month changed their investments more often, which resulted in them missing out on the long term gains.

This shows that more information doesn't necessarily equal better understanding, and can even obscure the bigger picture.

> _"Everything should be made as simple as possible, but not simpler." - Albert Einstein_

### 7. We have illusions of correlations and cause that don’t exist. 

We often hear the phrase "one thing leads to another," but does it really ring true? It doesn't, but we often believe it does because narratives and consequences are so ingrained in human society. 

One common belief that exemplifies this is that many people think that listening to sexually explicit song lyrics causes teens to engage in risky sexual behavior. No scientific research supports this claim.

We also tend to see correlations where none exist.

Humans perceive patterns as a way of making sense of the world, but often these patterns don't exist. For example, wanting to test the common belief that there is a connection between weather and arthritis pain, doctor Donald Redelmeier and psychologist Amos Tversky asked patients with arthritis to document their pain level every day. After comparing the reported pain levels to the weather they saw that day, they found no correlation between the two. 

So they decided to conduct a second experiment, this time with undergraduate students who were asked if they could detect any patterns in a series of pain levels randomly matched to weather information. Interestingly, 87 percent of them did indeed find correlations. Why? Because they honed in on the data that supported their preconceived notion that weather and arthritis pain correlate.

So in what other areas do we find erroneous correlations?

When two things happen at one time, we often believe that one caused the other. Consider the classic conundrum in which high drowning rates occur at the same time as elevated ice cream sales. Does one cause the other? No, but hot summers are the culprit behind both of them: drowning is a tragic phenomenon that has higher chances of happening when more people feel like swimming, and ice creams are just a pleasant way to cool down during hot weather.

### 8. We believe in an illusion of potential that can be easily unlocked. 

If you weren't glued to Facebook all the time, you could become a successful artist or be as clever as Albert Einstein, right? Well, perhaps not.

As a matter of fact, we like to think our brains hold far more potential than they actually do. We willingly perpetuate the belief that we only use ten percent of our brain's capacity, for instance. But is this really the case?

Well, there's no way of measuring potential brain capacity, and if we really did only use ten percent of our brain, the unused tissue would wither and die, leaving us 90 percent brain dead! Even if this _were_ the case, what would be the point of having such big, largely empty brain cases? Childbirth is dangerous for women because babies have large skulls. If those skulls are filled with 90 percent useless gray matter, the risks of childbirth make no evolutionary sense. And yet, in a survey conducted by the authors, 72 percent still think 90% of their brains goes unused!

Not only are we convinced of our unlocked potential, we also think it can easily be unleashed.

One example of this is the so-called _Mozart effect,_ a theory touted by physics professor Gordon Shaw, stating that listening to Mozart's music makes us smarter because its structure resembles our brain's structure. Shaw led an experiment where some participants listened to Mozart for ten minutes, others listened to easy listening and some just sat in silence. They then conducted some cognitive tests to see how it affected their IQ. The IQ of those who listened to Mozart increased by a massive eight to nine points. Since that experiment, however, several researchers have attempted to replicate the test, but none have found any evidence of the Mozart effect.

Still, more than 40 percent of people in a study conducted by the authors believed that listening to Mozart unlocked potential IQ.

> In a hospital in Slovakia the infants get headphones playing Mozart's music to improve their intelligence.

### 9. Final summary 

The key message in this book:

**Our intuition, and the cognitive functions we base it on, can't always be trusted. In reality, we believe we're more attentive and intelligent than we actually are, and perceive far more correlation in the world than there actually is.**

Actionable advice:

**Be careful what you look for.**

When you're focusing your attention on something specific, you can miss things that you would otherwise see. Sometimes this can be something beautiful or funny, but it could also be extremely costly, especially if you're on the road in a car.

The next time you drive somewhere, there is a real risk of you not seeing cyclists, pedestrians or motorcycles because you've been trained to watch out for other cars instead. So it really pays to be more aware of your surroundings and to expect the unexpected.

Even in business, focusing too narrowly on one thing could mean that you miss incredible opportunities that are right in front of you.

**Suggested** **further** **reading:** ** _Thinking, Fast and Slow_** **by Daniel Kahneman**

Daniel Kahneman's _Thinking,_ _Fast_ _and_ _Slow_ — a recapitulation of the decades of research that led to his winning the Nobel Prize — explains his contributions to our current understanding of psychology and behavioral economics. Over the years, Kahneman and his colleagues, whose work the book discusses at length, have significantly contributed to a new understanding of the human mind. We now have a better understanding of how decisions are made, why certain judgment errors are so common and how we can improve ourselves. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Christopher Chabris and Daniel Simons

Christopher Chabris is associate professor of psychology and co-director of the neuroscience program at Union College in Schenectady, New York. He is also a chess master who writes about the game for the _Wall Street Journal_.

Specializing in experimental psychology, Daniel Simons is a professor both in the department of psychology and the Beckman Institute for Advanced Science and Technology at the University of Illinois.

Together, Chabris and Simons won the Ig Nobel Prize (awarded to research that "makes people laugh, and then think") for their work on the invisibility of gorillas.

