---
id: 57593d49496d1c0003e862cb
slug: the-invisible-orientation-en
published_date: 2016-06-16T00:00:00.000+00:00
author: Julie Sondra Decker
title: The Invisible Orientation
subtitle: An Introduction to Asexuality
main_color: 995397
text_color: 8E408C
---

# The Invisible Orientation

_An Introduction to Asexuality_

**Julie Sondra Decker**

_The Invisible Orientation_ (2014) provides a helpful introduction to asexuality, including valuable information on both what it is and what it is not. You'll also learn about how people experience this sexual orientation, the difficulties that come with it, and why there is no need to cure it, condescend to it or consider asexual people as being any different from you or me.

---
### 1. What’s in it for me? Become informed about asexuality. 

Over the last few decades, there's been an immense increase in tolerance for and understanding of non-heteronormative sexualities. The LGBT community is more visible than ever and has gained considerable rights, an indication that, hopefully soon, no sexual orientation will be viewed as abnormal or deviant.

But what if all this talk of sex doesn't make much sense to you? What if you don't feel sexually attracted to anyone?

This is the situation for those who identify themselves as _asexual_, and even as more and more of the sexual spectrum comes into the light, asexuals are, by and large, unacknowledged. So let's explore how asexuality works and how we can improve our understanding of this invisible orientation.

In these blinks, you'll find out

  * the difference between sexual attraction, sexual arousal and sex drive;

  * why asexuals are often pressured into having sex; and

  * what all of us can do to be more supportive of asexual people.

### 2. Asexuality is a sexual orientation that describes experiencing a lack of sexual attraction to anyone. 

If you've ever encountered someone and felt sexually enticed by his or her appearance, then you're familiar with basic sexual attraction.

Let's take a moment to define some terms, here. Don't confuse sexual attraction with arousal, or sex drive, all of which are different things.

Sexual attraction simply refers to the emotional reaction of finding someone sexually appealing, whereas arousal describes a physical reaction and sex drive is the desire to respond to that arousal.

For many, this distinction isn't that important, since these reactions usually go together, with one following the other: if you find someone sexually attractive, you are easily aroused and then feel the desire to pursue these feelings.

But there are also people who identify as _asexual_, which means they're not sexually attracted to anyone. If you were to ask them who they find sexy, the answer would be "Nobody."

Now this doesn't mean they can't get aroused or feel the desire to do something about it.

Yes, some asexual people are repulsed by the idea of having sex, but others are indifferent and might perform the act as a favor to a partner.

If an asexual person chooses to engage in sex or masturbation, that does not put an end to their asexuality. It's important to remember that behavior is not what defines sexual orientation. Just like anyone else, asexual people are perfectly capable of having sex with people they don't find attractive.

In fact, asexual people might masturbate for the same reason most people do: because it feels good, and they want to enjoy the rush of endorphins or relieve stress.

And as with other sexual orientations, being asexual is neither a decision nor a choice. After all, no one can choose to find someone attractive; it just happens.

Others will often respond to an asexual person with condescension, saying, "Just wait, one day you'll find someone attractive," suggesting that the person doesn't fully understand his or her own feelings.

The fact of the matter is this: asexuality _is_ a sexual orientation just like any other, since it describes how a person experiences attraction.

> _Approximately one percent of the population is asexual._

### 3. Strong relationships can be built upon romance, without sex. 

Perhaps you've heard the saying, "Sex isn't the same as love." It's a common argument in many discussions around infidelity. And for asexual people, this is definitely a true statement.

For asexuals, a loving and romantic relationship isn't based on sex.

This might sound unusual for people who think that a relationship needs sex in order for it to be called _real_ love. But believing this is like thinking a tailless dog isn't happy because it doesn't wag its tail.

For asexual people, sex simply isn't part of the equation. But just because there's no tail to wag doesn't mean there's no romance in the relationship.

People in asexual romantic relationships still find fulfillment through intimacy and having a partnership in life — an intimacy that doesn't need to be defined by sex, but instead comes from the comfort and trust of partnership.

Even without the act of sex, there are other intimate physical activities to take into account, such as caressing, cuddling, spooning or even enjoying the sensual experience of kissing.

Partners can form strong bonds without sex. They can live and run a business together, share the same life goals and even raise children. None of this requires the relationship to be sexual or even romantic in the traditional sense.

After all, everyone tends to define romance in different ways.

Asexual people are no different; they describe their romantic feelings toward other people with labels like _heteroromantic_, _homoromantic_ or _biromantic_.

For example, even though an asexual, heteroromantic woman might be having a dinner with a man to whom she isn't sexually attracted, she can have romantic feelings toward him as he lights a candle and pours her a glass of wine.

If this same heteroromantic woman were sitting with another woman in the same environment, there would be no sense of romance to the occasion.

There are also _aromantic_ people, who have no romantic feelings for anyone and don't believe that having a partner is fundamental to their happiness.

> _"My romantic partner is also my best friend. I can't point out any specific thing that makes our relationship romantic, but it definitely is."_ \- anon

### 4. Many people don’t know asexuality exists, while others misinterpret the orientation. 

Have you ever thought it would be great to be invisible? Well, be careful what you wish for. A feeling of invisibility is familiar to many asexuals, since most people aren't even aware their orientation exists.

Society has a hard time understanding why people don't engage in sexual activity, which leads to characterizations of asexuals as overly religious, passionless or antisocial. Or simply as lonely people who've failed the dating game.

Take an average asexual and aromantic man who is happily single and simply doesn't desire sex.

Society will view this person as impotent and weak since virginity is considered laughable and a man's strength is often defined by his sexual prowess.

Likewise, an asexual woman can be seen as frigid, pathetic and lonely.

Even compassionate people tend to assume that asexuals are immature and that it is just a passing phase.

Asexual people are often told, "I have a friend who went through a phase like that." Or, "Maybe you're a late-bloomer," implying that asexual people aren't living real and adult lives.

But maturity and adulthood are utterly unrelated to one's sexual desires. Let's not forget that most teenagers, and other truly immature people, usually feel the urge to have sex.

And sadly, the confusion doesn't stop there: Even if someone understands that asexuality is a mature and permanent sexual orientation, they still tend to belittle it by saying things like, "Wow, I couldn't live without sex. I'd die if I were you!" Or, "Your life must be so uncomplicated!"

While the first statement is exaggerated nonsense, the second is yet another condescending remark that disrespects asexual people by falsely suggesting that their life isn't as complex as others'.

On the contrary, asexual people face many complexities, such as finding a partner whose sexual needs are in balance with their own. Finding a partner is already tricky for people who aren't asexual — but it's even more difficult for asexual people, since their social circle is usually made up of people who aren't asexual.

So, people have many misconceptions when it comes to asexuality. And, as we'll see in the next blink, such misconceptions can even lead to accusations and diagnoses of mental illness.

> _"This isn't about putting yourself in an asexual person's shoes; it's about recognizing that people wear different shoes because they have different feet."_

### 5. Asexuals are often asked to prove their orientation is not “caused” by a mental or physical illness. 

What would you say to a friend who started dating someone she wasn't sexually attracted to? Chances are, you wouldn't tell her to go see a doctor, and yet this is an all-too-common reaction to asexuality.

Unfortunately, some people think of asexuality as a disorder, and even demand proof of physical health before accepting this orientation.

Asexual people are often asked whether their genitals function properly or made to undergo hormone checks before being accepted for who they are.

There are medical conditions that can lead to a decrease in sexual appetite, but such symptoms are not the same as having never experienced sexual attraction at all. After all, a doctor cannot diagnose a sexual orientation, so it makes no sense to check someone's physical health before accepting that person's true feelings.

Making matters worse, asexual people are also sometimes asked to seek psychological help and have their mental health examined.

A common suggestion is that asexual people must have been abused at some point in their lives. Apart from being incredibly insensitive, it is also inappropriate to try to link someone's sexual orientation to a traumatic experience.

Even when it comes to professional psychology, knowledge of asexualty is often still lacking. Whether they're in a one-on-one session or in couples therapy, asexuals often find themselves being asked to change their natural feelings.

In other situations, people will suggest that asexuals are in denial of being gay or afraid to come out of the closet.

But being asexual doesn't make life easy, so it makes little sense for someone to hide homosexuality behind asexuality. And as we've seen, asexuals are still pressured to have heterosexual sex.

But just as society finds it difficult to imagine people living without sex, it is also common for people to assume that anything that's not heterosexual must be homosexual.

What people fail to realize is that asexuality is not something that needs to be diagnosed, let alone fixed.

> _"Asexuality is the only sexual orientation that needs to defend it actually exists."_

### 6. It’s terribly stressful being seen as unnatural and having people try to “fix” and change you. 

What is the purpose of life? It's an age-old philosophical question, and some might say that part of the answer involves reproduction, which supposedly drives our sexual desires.

Due to this belief, heterosexuality is still viewed as the most "natural" orientation, since it produces children. And while society has advanced and is now less discriminatory of non-procreative sex, people who don't want to have sex are still faced with the label of being "unnatural," despite the fact that asexual people can conceive children as well as anyone else.

Still, this leads people to try and convert asexuals into liking sex.

Common suggestions are, "You should try it just to be sure!" Even if they've had sex and didn't like it, then it must be because they had it with the wrong person. Others might even view their orientation as a challenge, suggesting, "Sex with me will fix you!"

In this way, asexuals are still being misunderstood and mistreated, and such treatment even comes from people who are in relationships with asexuals.

Asexuals can end up in abusive relationships with partners who think sex is being withheld and that it is "owed" to them.

But even if you're in a healthy relationship, facing society's pressure to change yourself can cause immeasurable stress.

This pressure is often insidious, with people claiming that it's for the sake of the asexual person's happiness. People claim that they're trying to help, or make condescending remarks about how much asexuals are missing out on by not having sex.

But trying to impose your views on someone else is far from helpful, especially when an asexual person is completely happy with who they are.

Just imagine going to a restaurant and placing an order for your favorite dish, only to have the waiter insist that you don't really know what you want and that you must try his favorite dish instead!

> _"Telling people they have to engage in unwanted sex, before you consider their desires valid is a form of social coercion."_

### 7. A growing number of forums and organizations provide support and spread awareness about asexuality. 

What's better than sex? A while back, a survey among asexuals provided a plausible answer: cake. Since then, the image of a cake has become a symbol of asexuality on many websites on the topic.

And the internet has proven to be a valuable resource, since there is so little awareness elsewhere in society.

With so few offline support groups, some asexuals have turned to LGBT communities, more of which are opening up to asexual people.

The invisibility of asexuals is a product of a general lack of awareness: most people simply don't know that such an orientation exists. People in the LGBT community, on the other hand, tend to experience a more deliberate kind of oppression. Nonetheless, asexuals are still an enriching presence in the LGBT community, introducing different romantic identities and models of attraction.

Still, the internet is where asexual people can find the greatest variety of resources — forums, for instance — and discuss issues among themselves and with others.

There are worldwide groups and dating sites dedicated to asexuals, as well as sites that offer material on how to educate people about asexuality.

Some of these sites have chat rooms where curious non-asexual visitors can ask all the questions they want and get answers from experienced asexual people who have learned how best to explain their orientation.

Hopefully, this will lead to more non-asexual people spreading awareness and including the orientation in their work.

This way, asexuality will gain better representation in academic articles on sexual orientations and people can get a "None of the above" or "Asexual" checkbox included alongside other orientations in surveys and questionnaires. And perhaps society will even get to the point where sexual attraction isn't something we assume everyone experiences.

As it stands now, asexuality is barely on most people's radar and is certainly not something that many people understand. So we need to make every effort to spread awareness, make the orientation more publicly visible and help a large population of people feel free to be who they are.

### 8. Final summary 

The key message in this book:

**The resounding message asexual people get from society is that they either do not exist or should change who they are. But asexuality is a healthy and mature state that describes how someone simply does not experience sexual attraction. By definition, this** ** _is_** **a sexual orientation and asexual people should be trusted to navigate these issues themselves. No one has an obligation to be sexually active.**

Actionable advice:

**Acknowledge that asexual people exist.**

If an asexual person comes out to you — listen. You are not being recruited or being asked for advice. Just process what is being said and be respectful. What asexual communities most often request — when asked about what they want — is simply acknowledgment of their existence.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Sex at Dawn_** **by Christopher Ryan and Cacilda Jethá**

_Sex_ _At_ _Dawn_ argues that the idealization of monogamy in Western societies is essentially incompatible with human nature. The book makes a compelling case for our innately promiscuous nature by exploring the history and evolution of human sexuality, with a strong focus on our primate ancestors and the invention of agriculture. Arguing that our distorted view of sexuality ruins our health and keeps us from being happy, _Sex_ _At_ _Dawn_ explains how returning to a more casual approach to sex could benefit interpersonal relationships and societies in general.
---

### Julie Sondra Decker

Since 1998, Julie Sondra Decker has helped raise awareness of asexuality by providing interviews and insight to such mainstream media as _The New York Times_ and _Marie Claire_. Decker is also a webcomic artist and a writer of science fiction and fantasy. Being aromantic and asexual, she currently lives, happily single, in Florida.

