---
id: 5740a66f2edd1b00032e6b71
slug: the-it-marketing-crash-course-en
published_date: 2016-05-24T00:00:00.000+00:00
author: Raj Khera
title: The IT Marketing Crash Course
subtitle: How to Get Clients for Your Technology Business
main_color: 1C488C
text_color: 1C488C
---

# The IT Marketing Crash Course

_How to Get Clients for Your Technology Business_

**Raj Khera**

_The IT Marketing Crash Course_ (2013) is a guidebook to accelerating your IT sales and growing your technology business. These blinks show you how to earn the attention and trust of potential clients through well-calculated marketing strategies.

---
### 1. What’s in it for me? Build and grow your technology business. 

The realm of IT businesses is expanding at an exponential rate. In the face of such rapid growth, it's hard to know which IT-marketing strategies to employ within your own business.

But have no fear!

These blinks lay out the essentials for effective marketing within the business world of technology, whether you're starting, assembling or growing your IT business. From learning how to succinctly sell your services to efficiently building your reputation, this crash course will give you the skills to reach the peak of your IT business game.

You'll also learn

  * why you should secretly stalk your ideal customer;

  * how to tease a potential customer with a live lead; and

  * how a company blogged its way to $5 million in revenue.

### 2. To build up your IT business, you need to know exactly what you sell and to whom. 

What exactly does your business do? A seemingly simple question — but you'd be surprised how many new entrepreneurs can't answer it clearly.

For instance, when the author posed this question to an entrepreneur at a conference, the response was extremely vague: web design, managed services, network management and software development. He might as well have said that his company would do anything for a buck.

Being vague doesn't work, because convincing customers means being crystal clear about what you offer. Prospective customers want specialists, not generalists. For instance, say you want to add a deck to your house. Would you rather hire a general contractor or a company that specializes in building decks?

The obvious choice is the specialist because specialists have a reputation for expertise. And the same thing holds for building your IT business: pick a specialty and communicate it clearly.

But it's also important to know who, specifically, buys your products. That's because knowing your _buyer persona_ — that is, your ideal customer — provides a detailed picture of who you're trying to reach.

And the research you do to determine this should give you a clear understanding of what motivates your buyers. Because to address their wants and needs you'll first need to know what those wants and needs are.

For example, Jeff LoSapio of ThreatSim, a company that aids businesses in countering phishing attacks, found that, within large organizations, the buyer persona was the employee responsible for threat assessment. Once LoSapio knew which people to target, he could get even more specific by taking into account the size of the organization, as well as its priorities and challenges, and then tailoring the marketing message accordingly.

And finally, it's also key to know where your ideal customers hang out and what they read. This will put you a step ahead and dictate which networking events you should attend.

> _"Success demands singleness of purpose."_ — Vince Lombardi

### 3. Successful marketing efforts require a honed message that clearly states the value of your product. 

Once you've nailed down what you sell and whom you're targeting, you're ready to start spreading your marketing message. So, back to that seemingly simple question, "What do you do?" It's essential that you're prepared with a clear and crisp response, and that means honing your IT marketing message.

For instance, can you explain what you do in the time it takes for an elevator to go from the top floor to the lobby?

Well, in business, it's essential to craft a so-called "elevator speech," a snappy explanation of your services that will grab the attention of a potential customer and convince them that you're an expert. To develop an elevator speech, try this exercise:

In 20 seconds or less, clearly state the services you provide or products you sell.

And remember, it's essential to give specifics and avoid generalized responses. Don't say that you do programming and design. Get specific. A good description will detail the benefits a customer will reap from your service.

For instance, you might say, "We provide a cost-effective newsletter tool for companies that want to communicate more effectively with their customers to generate repeat business." And always close with an offer, whether it be a follow-up phone call, an article you found or wrote, or even a free trial.

Whatever you do, the essential task is to convince your customers that your products or services are valuable. A great way to accomplish this is pricing by value, not by time.

For example, if you send an invoice charging $500 for five minutes of work and the pushing of a button, your customers won't understand the price. However, if you explain the _value_ of your work — that is, knowing which buttons to push — your customers will gladly pay the price, no matter how much time went into it.

> _"Price is what you pay. Value is what you get."_ — Warren Buffett

### 4. Use your network to connect with new prospects and ask potential customers plenty of questions. 

Say you call a new prospective client. She isn't available at the moment, so you leave a message, but nobody calls you back. If this scenario seems surprising, it shouldn't. After all, would you call back a complete stranger?

The only way to avoid this is by making a connection, something your network can help you do. For instance, to encourage that potential client to connect with you, you can turn to your network, which may be able to offer her something of value.

This technique — offering a _live lead_ — is a surefire way of getting someone to call you back. For instance, say you find out that that elusive potential client is looking to hire a graphic designer. You're a managed services provider and graphic design isn't something you offer. However, you do know just the right person for the job.

Pass that lead on to your potential client. Be sure not to give away all of the lead's information in your e-mail, though. You want to get your quarry to contact you, which will give you the chance to establish a personal connection.

Once you've made contact, keep asking your prospective customer questions. After all, you don't want to see a valuable client choose another company for, say, network management because they thought you only provided network monitoring.

So, to avoid this scenario, keep asking your customer questions about their wants, needs and problems. This will prevent the customer from missing out on a needed service that you offer.

For starters, ask what types of things go wrong in their day-to-day operations and whether they want to change them, and then find where your services — or services provided by people in your network — fit in.

> _"Whenever you hear about a need you can't fulfill, find someone who can and make the connections."_

### 5. To reach potential customers, create an effective website, write blogs and engage in social media. 

How can you grab the attention of potential customers with your marketing message? And maybe more importantly, how can they find you and get in touch?

Well, the first step is creating an effective website for your business. This will involve avoiding the many common mistakes that people make when building websites. So, here are a few to avoid:

Do not create a _brochureware_ website. This is a site that consists solely of existing printed materials that were converted to an online format. Websites like this fail to engage people simply because they look unprofessional.

Also, don't forget to add educational material, case studies and informative content. Remember, your potential customers came to you looking for useful information that can solve their problems.

And finally, don't forget to utilize _search engine optimization_, or SEO. This is a service that will rank your site higher in search results, thereby making it easier for customers to find.

It's also important to write blogs that build your reputation as an expert. Blogs are an excellent way to put your expertise on display, to make yourself stand out on search engines and to engage in dialogues with potential customers.

In fact, lots of companies have blogged their way to the top. Just take Brian Williams of Viget, who attributes $5 million in revenue over the past four years to his blog on software solutions. The company, which provides custom software and websites, earned several of its high-profile clients like Puma and Duke University through blog posts they stumbled upon while searching for answers to technical questions.

And finally, use social media to engage your audience. The best part about this is that you can feed your Twitter, Facebook and LinkedIn with content straight from your blog. But it's also important to update your LinkedIn or Facebook status with links to articles you find interesting. After all, other people might like them too and will remember you for the helpful information you post.

### 6. There are a few other marketing tools that can help you boost your IT business. 

Some people think that e-mail is already an outmoded form of communication, but don't be so quick to drive the nails into its coffin. In fact, e-mail newsletters are a fantastic way of keeping in touch.

For instance, say you meet a potential client at a networking event and exchange business cards, but the contact isn't sales-ready. What do you do?

Instead of just putting him into your customer-relationship database and forgetting about it, you could add his e-mail address to your newsletter list. This will keep your company in his consciousness, and, when he _is_ ready to buy, he'll be able to contact you for your services.

Seminars and webinars are also great tools, both of which allow you to position yourself as an expert in your field.

Giving seminars is a great way to display your knowledge to a target audience, and also a helpful method for reaching new groups. You could, for example, present a seminar to the local branch of your professional society, establishing your expertise and expanding your network at the same time.

Or, if you want to reach an audience beyond your geographical location, you can produce a webinar — that is, a web seminar. It's easy to present a professional webinar with the help of tools like GoToMeeting or Webex.

And finally, don't think about your marketing expenses as just another cost factor. Rather, think of them as an investment. As a small business owner, you're watching every penny and it's easy to see any money you spend as an expense. Marketing is different, however, and if you want your business to grow you'll need to invest in it.

In fact, even companies with just one or two people need to invest at least between $200 and $300 per month. Just remember, if you put these marketing strategies to work, your effort and money will pay off in the form of steady growth.

> _"Either write something worth reading or do something worth writing."_ — Benjamin Franklin

### 7. Final summary 

The key message in this book:

**Successfully building a new IT business requires strategic marketing. Get started down the right path by clearly defining what you sell and to whom you sell it. Then you can use your network to reach new prospects and display your expertise through blogs, social media and seminars.**

Actionable advice:

**Become a "you marketer."**

When browsing the internet it's common to stumble upon a lot of "me" phrases, like "What _we_ do," "_Our_ projects" or "_Our_ clients." But this "me marketing" doesn't give prospective clients a sense of how you can solve problems for _them_. Expert websites are better off using "you" phrases, like "What _you_ do," "_Your_ goals" and "_Your_ problems solved." It's easy to make this change; just go through your website, count up all the "me" phrases and replace them with phrases that focus on "you."

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Built to Sell_** **by John Warrillow**

_Built to Sell_ details key strategies for growing a small service company and preparing the business for a future sale. These blinks illustrate these insights by telling the story of Alex Stapleton, owner of a marketing agency, and his advice-giving friend Ted Gordon, who is a successful entrepreneur.
---

### Raj Khera

Raj Khera is co-founder and CEO of the e-mail marketing software company MailerMailer. In 1995, before taking on his current role, he co-founded GovCon, which went on to become the largest business-to-government web portal in existence.

