---
id: 5e52a7216cee0700079a1a4e
slug: the-job-ready-guide-en
published_date: 2020-02-24T00:00:00.000+00:00
author: Anastasia de Waal
title: The Job-Ready Guide
subtitle: Employability Skills and Strategies for Career Success
main_color: None
text_color: None
---

# The Job-Ready Guide

_Employability Skills and Strategies for Career Success_

**Anastasia de Waal**

_The Job-Ready Guide_ (2019) is a helpful roadmap for anyone seeking to get themselves on the first rung of the career ladder. In addition to offering advice on job-searching, writing a compelling resume, and performing well in interviews, it provides insight into what modern employers are really looking for.

---
### 1. What’s in it for me? Kick off your professional journey with some helpful hints from an education expert. 

For many people entering the world of work for the first time, finding the right career can be a daunting prospect. In between figuring out what you want to do, compiling a list of your skills and experience, and scrolling through endless job sites, you also have to consider how you'll stay ahead of the competition.

Today's workforce is more educated than ever before and employers have raised the bar on what they expect from their employees. Fortunately, there are strategies that can help you navigate today's job market and ensure that you stand out from the crowd. 

While these blinks are mostly aimed at students graduating from college, they also contain a good deal of general know-how for anyone embarking on a new career path. You'll find out how to boost your employability, create a winning application, and master fundamental skills. 

In these blinks, you'll learn

  * foolproof, systematic methods for job-searching;

  * ways to enhance your resume and brush up on your interview technique; and

  * how to prepare for your new working environment.

### 2. Exploring your options in a focused way is vital to making the right career choice. 

So, you've just finished university and you're thinking about where to go next. If you're like most people, you might find that your degree doesn't necessarily align with a specific job or industry, like law or medicine. 

Firstly, don't freak out. Many people leave university without a clear pathway ahead. The best thing you can do is thoroughly investigate your options so that you can work out what type of job would suit you best. 

A good place to start? Jot down your education history, including your qualifications, the focus of your studies, and the grades you achieved. This will help you to set up your own "filters" — as if you were a search engine — breaking down all possible careers into the ones you're most eligible for. 

For example, some careers will call for a specific degree in a particular subject, or will require candidates to have a minimum grade average or postgraduate training. Applying your own filters will ensure your attention is focused on the jobs you have the right skills for, in the industries you're most suited to.

Next, it's time to start your job research. The aim here is to get a solid idea of what jobs are out there — especially in the slice of the labor market that your filters suggest is most suitable for you.

To paraphrase a recruitment consultant interviewed by the author, many graduates fail to identify what their career options are simply because they have little awareness of the jobs actually out there.

To avoid falling into this trap, try to keep up-to-date with the current labor market. You can do this by looking at job descriptions on job sites, chatting with a career advisor at your college or university, or booking a meeting with a recruitment consultant. 

The key takeaway here is to utilize all the resources available to you. Even chatting with friends of your older siblings or parents about how they launched their own careers can help you pick up valuable advice. Ask them to talk you through how they selected which industry they wanted to work in, and how they found their way in. They may even have some helpful contacts to share with you.

### 3. Job hunting requires a solid plan of action. 

When embarking upon a job hunt for the first time, some graduates think they need to just get _a_ job rather than finding employment in an industry that actually interests them. But we spend a large part of our lives working, so it's important to look for a job that aligns with your long-term ambitions.

Let's be real, though: job-hunting is, in itself, a full-time job. To ensure the process is a smooth and successful one, it pays to have a strategy. 

Start by setting up a timetable: realistically, how long do you have to find a job? This will largely depend on when you graduate, how long you can stay financially afloat without working, and on whether you have any important plans after graduation.

Next, set up a schedule. How much time will you spend, per day or per week, job hunting? What resources do you have on hand to assist you in your search? Try to avoid aimlessly scanning the internet and think strategically about which websites you should visit.

Online job sites are a go-to resource, ranging from sites that advertise jobs across industries to those that are qualification- and industry-specific. Use filters on these sites to refine your search, and sign up for new job alerts that will be sent straight to your inbox.

Also, check out the websites of specific organizations you'd like to work for and look at their list of vacancies. Better still, follow them on social media. Many organizations will announce openings on their Twitter and Facebook feeds, as well as on LinkedIn. 

Another key thing to consider? Not all jobs are advertised. This is because employers sometimes recruit people from within their organization, or simply create a job because an excellent candidate has come along. The latter often results from sending a _speculative application_, which can be worth sending if there is an organization that you especially want to work for. While it's a long shot, they do sometimes work. However, your application will have to be concise, polished, and well-thought-out for it to really get noticed. 

As an employer interviewed by the author explains, a lot of speculative applications focus more on the _applicant's_ interests rather than on the needs of the organization — which doesn't give a very good impression. 

Instead, you should find out what the organization aims to achieve and think about how you can contribute.

### 4. Identify what employers are looking for and match your skills to their requirements. 

Employers can seem intimidating — but the truth is, they're humans too. While organizations want candidates with relevant experience and educational background, they also want to recruit people who will be nice to work with. That's why leadership and teamwork are highly sought-after skills in the modern workforce.

Employers will test these skills in different ways throughout the job application process. Sometimes they'll invite you to an assessment center to observe how you interact with other people; other times they'll ask you to give examples in an interview. That's why keeping in mind a few scenarios where you've been a good leader or team player is crucial. 

So how can you demonstrate leadership? Think about a time where you've taken charge of a situation, or have been instrumental in solving a problem. What were the steps you took and what was the result? Take a mental note of anything that comes to mind. 

It's important to remember, though, that leadership also involves showing appreciation, trusting your coworkers, and being fair. Identifying moments where you've showcased any of these qualities will help employers gain an understanding of what kind of person you are. 

And what about teamwork? In some ways, being a good leader and a good teammate go hand-in-hand. Working in a team involves shouldering part of the workload, delivering what you've agreed to on time, and being able to give and receive constructive feedback.

So, how can you demonstrate these skills? Try thinking back to clubs you were a member of in college. What made you a successful team member? If you were part of the debating club, you could give an example of how you had to work with others to form an argument or organize travel arrangements to competitions. 

Leadership and ability to work in a team aren't the only things employers will be looking for, though. As the author explains, the qualities that exist "between the lines" — which are sometimes called _soft skills_ — are just as important. 

Employers want to work with people whose attitudes, values, and behaviors reflect those of their organization. Throughout the application process, they'll be on the lookout for candidates with a strong work ethic, a positive attitude, and a desire to learn and develop, among other things. Make sure you include evidence of these attributes too when talking to a potential employer.

### 5. Brush up on your knowledge of the organization and the wider industry. 

Perhaps it goes without saying, but before you apply for any jobs you need to know the industry you're targeting inside out. 

Whichever sector you're seeking to enter, be it medicine, finance, media, or art, employers will be on the lookout for your knowledge of two key areas: the industry as a whole and the specific organization you're applying to. So, where to begin? 

The best approach is to start by gaining an overview of the industry as a whole. To do this, you need to be able to answer the following questions: How big is the industry? Is it divided into different sectors? And, what factors influence the industry? For example, does law, the economy, or politics affect the way it's run?

This might sound overwhelming, but many of the answers can simply be found on Wikipedia. Industry-relevant government websites are also great resources, as are trade journals, which libraries usually keep in stock.

Next, research the main players in your target industry. Which organizations or industry leaders are most influential? Also, do different organizations work together within the industry or do they operate independently?

It's also important to consider the current state of the industry, including its main priorities and biggest current challenges. Finally, it's worth keeping in mind external factors — such as regulation, funding, or technological developments — that could affect the industry in the future. 

Once you've covered the whole sector, you can turn to the organization you want to work for. 

Start by studying their website. It's a good idea to have a grasp of the history of the organization, the background of the founders, and the number of people employed. Also, be sure to get a detailed overview of the organization's focus and what products or services they offer. 

For extra credit, really zoom in on the ethos of the organization. Ask yourself what philosophy guides their work and how it is different from that of other organizations? Clear command of the organization's aims and ambitions will prove your commitment to your desired role. 

Another tip? When it comes to finding a place to file your industry research, set up a separate email account that you can use to sign up to newsletters and receive Google Alerts. Having a separate inbox for your industry research will help you comb through the information more systematically.

### 6. Creating a compelling resume shows employers that you mean business. 

As you've probably heard, writing a kick-ass resume is a big part of applying for a job. Think of it like an advertisement for yourself: not only does a resume showcase your education, experience, and interests, it also gives employers insight into what makes you, you. So, what should you include?

It goes without saying that your name and contact details need to be at the top of your resume. Your educational background, qualifications, and work experience — including paid work, work experience, and voluntary work — should then follow. Finally, add contact details for your references. These are people you've worked with before who can vouch for your skills and character. 

How you use the rest of the space on your resume is optional. For extra credit, you may wish to include any additional skills you have, such as foreign languages or specialized IT skills, and any other training you've completed, like a first aid course. You could also include your interests or hobbies. For example, if you're a budding artist or hockey player, then be sure to note this down too. 

Once you've mastered the content of your resume it's important to consider the presentation. The goal here is to make it as simple and easy to read as possible. Employers are busy people who will be looking to extract information from your resume as quickly as possible; twirly fonts and garish colors will only be distracting. 

So, let's take a look at a few basic guidelines to keep in mind. First of all, keep your resume to a maximum of two pages long. Make sure the information you have included is absolutely necessary, and get rid of any superfluous details.

Next, create a heading by writing your name in a large font at the top of the first page. Use a clear font, such as Times New Roman or Calibri, throughout your resume, in 10-12 point size. Also, remember to use bold for section headings and include bullet points under subheadings to clearly separate listed items. 

And, finally, a note on design. Excluding the points listed above, the way you lay out your information is entirely up to you. A quick search online will direct you to many examples of resume templates that you can use for inspiration.

### 7. Cultivate a professional brand across social media platforms. 

In the digital age, employers look up prospective candidates on social media platforms, including Twitter and Facebook, to get further insight into what kind of employee they would be.

According to recent research, two-thirds of employers will do exactly that before they even invite candidates for an interview. That's why curating a professional brand for yourself online can help you get the attention of potential employers. 

But first, what is a professional brand? Well, imagine that you're an entrepreneur pitching a product to an investor. What qualities would you highlight and how would you make this information exciting? Now, think back to yourself. What attributes, ambitions, and passions do you have, and how would you articulate them to an interviewer? 

This is the kind of information that you should think about including on your social media accounts. Not only will this give employers a sense of your character, but it will also showcase what you have to offer beyond your resume. 

Before we dive into how you can create your own professional brand, let's first do some damage control. Is there anything about you that already exists online that could raise alarm bells for employers? This could be anything from incriminating photos to posts that include swear words. 

It's important that you spring clean your social media accounts of anything that puts you in a bad light before you attempt to create a professional online image. 

Once you've got this out of the way, you can then start to build your social media persona. Your profile on any platform needs to be short and sweet and it needs to contain vital information about your educational background and your target industry. It might also be worth including an email address in your biography to ensure employers can easily get in touch. Then, choose a professional-looking photo. 

Since social media is all about sharing content, it's important that you keep your accounts updated with carefully curated posts. Ultimately, you are promoting what you have to offer, which can be achieved through posting about any work you've done or any engagement you've had with your target industry.

For example, you could post a link to a blog you've been writing that relates to issues within your chosen sector. Or, if you attend an industry event, you could post key insights about it that will demonstrate your knowledge about the ins-and-outs of the industry.

### 8. Demonstrate in interviews that you have creativity, initiative, and the ability to solve problems. 

You've probably heard that employers are always on the lookout for effective problem-solvers. These are people who can think laterally and analytically when it comes to difficult situations; who can research, absorb, and analyze relevant information; and who can come up with practical and creative solutions. 

When it comes to being interviewed by a potential employer, you need to be quick-off-the-mark to give them solid examples of your problem-solving abilities. So, let's take a look at common things employers will ask you. 

One key thing employers will want to know is how you identify a problem and analyze its details. They may ask: what information would you need to obtain in order to decide whether a new product or service is a success? 

They'll also want to test you on your decision-making skills. For instance, they might ask you to give an example of a time where a decision you made turned out to be a mistake, and then ask what you did afterward to make up for it. 

Let's not forget, though, that innovation and creativity are essential parts of problem-solving. Many employers will want to test your ability to conjure up exciting ideas and apply a fresh perspective to existing problems. For example, they may ask you how you would increase the donations a charity receives, or how you would increase the sales of a particular product.

The goal here is to present a new way of thinking. This will give employers a good idea of where you fit into their organization and how you could be a part of helping them grow.

This might already sound like an overwhelming amount of information to keep in mind. That's why the author advises putting together a "problem-solving portfolio" that will help you pull together all your experience of effective problem-solving.

Try to select a few scenarios that illustrate as many problem-solving skills as possible, such as moments where you've had to use initiative, stay calm under pressure, or meet a tight deadline. Be sure to provide a good amount of detail on what you did and what the outcome was.

Even better — try and think of _quantifiable_ examples, where you can throw in some specific numbers or statistics. This can be anything from how much money you raised at a charity event to what mark you got on a specific test.

### 9. Creating a good first impression will set you on the path to success. 

So, you've put the legwork into researching your chosen industry, writing a kick-ass resume, and refining your interview technique, and you've now secured yourself a job. The work doesn't stop here though; in order to really thrive in your new career, you need to get a few practicalities out of the way first.

Consider what you need to start your new job: do you require new clothes or shoes, a different transport card, or even new accommodation? Try to tie up these loose ends before you start your new job, as this will reduce your first day nerves. 

This also goes for any work you have left over from a previous part-time job or college course. For example, if you're working an evening or weekend job that you can't get out of immediately, you should consider finishing that first before you start your new job. 

Once you have these things straightened out, it's time to prepare for the job itself. Read through any information you were sent by the organization, complete any tasks you were given, and prepare any forms required to start work, such as a work permit or bank account details. Coming to work fully prepped will show your employers that you're committed to getting off to a good start.

Keep in mind, though, that making a good first impression over the first few days of your employment isn't just about taking care of your own priorities. It's also about demonstrating how you interact with and support other people in the workplace. 

Good working relationships are founded on one crucial thing: respect. That means learning how to communicate in a positive and empathetic way. Let's take a look at a quick example of this.

Imagine that a coworker has behaved unjustly by taking all the credit for an achievement that was the result of effective teamwork. Confronting him, however, is likely to lead to an argument. To avoid this, give constructive feedback instead. Explain the reasons you're upset with his behavior, and be sure to maintain a civil tone of voice. 

Remember, patience and diplomacy are fundamental skills that you will use throughout your life, so it's important that you master them early on in your career.

### 10. Keep your professional development in mind and know when to move on. 

We all have to make difficult career decisions throughout our lives. In the modern world, many people move from place to place, have several different jobs, or drastically change the direction of their careers at some point in their journey. 

As a graduate, it's important to remember that the first job you have probably won't be your last. That's why it's crucial to keep your own professional development in mind throughout your career. In practice, this means monitoring and evaluating your own performance and checking that you feel sufficiently challenged and stimulated in your job.

A good time to reflect on your career is a year after you land your first job. Ask yourself what the organization you are working for offers you in terms of career development and future opportunities, and whether this is enough to help you achieve your aims.

Then, consider whether you should stay in your current role, seek a promotion, or move organization altogether.

Don't be too quick to jump ship, though. To paraphrase an executive at a multinational company, leaving a job after less than a year can give a bad impression. If you leave a job too soon, it may look like the organization wasn't happy with your performance, or that you lacked commitment to the role.

If, however, you feel that it is time to move on, there are a few things you can do to ensure a smooth and amicable end to your working relationship. As the author explains, leaving a job in a positive way is essential for the all-important three "Rs": your relationships with your colleagues, your references, and your reputation.

First, arrange a meeting with your boss in person to tell them that you're leaving. Be sure to outline how much you've enjoyed working with the organization and thank them for the guidance they've given you. If you're moving to a different organization, make sure to discuss this with them too. Remember: transparency is key and your boss will thank you for it.

Also, it's important that you check your notice period before you discuss any plans to leave your current role. If a new job awaits you, make sure your start date is after the notice you've given your previous employer.

> _"The first job you have will contribute to, but not dictate, the rest of your working life."_

### 11. Final summary 

The key message in these blinks:

**Getting your foot on the career ladder is a challenging process for anyone. However, there are strategies that can help you optimize your career planning and preparation. If in doubt about where to start, try to imagine what your ideal first job would look like, and envisage yourself in that role. Think about what skills you would require, and what kind of tools or connections you could utilize to help you get there. Put in the hours to research into different sectors, refine your focus, and watch as doors start to open.**

Actionable advice:

**Admit to your failures.**

If you ever find yourself screwing up at any point throughout your career, be honest about it instead of covering it up. It sounds simple, but coming clean about your mistake can really help you reduce its overall damage.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Reinvent Yourself,_** **by James Altucher**

In these blinks, you've seen a detailed roadmap to successfully securing your first job and getting yourself onto the very first rung of the ladder of professional life. But what about the rest of your life? In our blinks to _Reinvent Yourself_, we look at all the steps that follow that first one.

With actionable tips for achieving and maintaining professional and personal success throughout your life, the blinks to _Reinventing Yourself_ add up to a practical guide for reinventing yourself every five years. They include advice on how to overhaul your habits, deal with failure, and develop only the very best of your ideas. If the idea of refining yourself to be your very best piques your curiosity, then head over to the blinks on _Reinvent Yourself_.
---

### Anastasia de Waal

Anastasia de Waal is a social policy analyst. In addition to being the Deputy Director and Director of Family and Education at British think tank Civitas, she is also a qualified primary school teacher, broadcaster, and writer. Anastasia also heads the Visiting Women at Work Programme, a charity dedicated to helping inner-city primary schoolgirls meet women in a broad range of occupations.

