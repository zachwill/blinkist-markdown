---
id: 54f57bac623735000a120000
slug: the-knockoff-economy-en
published_date: 2015-03-03T00:00:00.000+00:00
author: Kal Raustiala and Christopher Sprigman
title: The Knockoff Economy
subtitle: How Imitation Sparks Innovation
main_color: 4B7196
text_color: 37618A
---

# The Knockoff Economy

_How Imitation Sparks Innovation_

**Kal Raustiala and Christopher Sprigman**

_The Knockoff Economy_ challenges the idea that copying and imitation pose serious barriers for industries. Instead, it uses concrete examples to demonstrate how, in an age where copying is easier than ever before, innovation is at its historical zenith.

---
### 1. What’s in it for me? Learn why imitation can often drive innovation. 

In 2000, Metallica brought a lawsuit against the music sharing website, Napster _._ They were fed up with their fans using the site to steal or copy their music for free. Metallica's opinion is common in the creative industries: If you want to benefit from the fruits of our creativity, they say, you have to pay for it.

Yet, as prevalent as this idea is, it's wrong! Imitation and copying, far from robbing creative talents of their work, can in most cases bring them increased benefits. As these blinks show, many industries from fashion to sports have benefitted greatly from people copying and imitating.

In these blinks you'll discover

  * why no chef is really original;

  * why Radiohead made more money when they gave their album away free; and

  * what happened to the Encarta encyclopedia.

### 2. Copying and imitation do not need to stifle creativity and innovation. 

In any discussion on copyright law, you'll often hear that copying and imitation damage creativity and innovation. People believe that copying is cheaper than creating, which in turn reduces the incentive to innovate. After all, why spend all your time and money coming up with the best idea if you could just copy it instead?

But the industries that are the most creative are not even protected by copyright laws. Copying is simply a matter of course in fashion, and yet the industry thrives nevertheless.

In fact, data from the US Bureau of Labor shows that, from 1998 to 2012, the average price of garments has remained virtually unchanged. The companies whose goods are often copied produce the highest priced garments, and one might think that copying would hurt their bottom line. In reality, these brands flourish, despite selling their garments at 250 percent the price of the copying competition.

In the same vein, there is no copyright protection for recipes. Despite this, many believe the culinary world is at a creative peak.

If you are a chef whose prized recipe is copied, your reputation as an inventive cook is burnished. Thomas Keller's dish "Oyster and Pearls," for example, has been widely copied. Did it run him out of business? No. In fact, it brought Keller's restaurant The French Laundry — and Keller himself — international recognition and success.

Furthermore, industries that promote strict copyright laws are not necessarily more creative. Industries such as music and film, which have more stringent protections against copying, have actually been experiencing a decline in their markets. These protections, rather than simply hindering pirating, can make it harder for new competitors who could have been at the forefront of innovation in the field to freely sample existing work in their own creations.

Obviously there are industries where copying is prevalent, and they are nonetheless creative, but in some areas copying is _part_ of the creative culture.

> _"For a celebrity chef, cooking means handing someone a recipe." — Chef Bobby Flay_

### 3. Copying can serve as a foundation for creativity. 

In some fields, copying isn't a nuisance to be stamped out. Rather, it's the way the field functions. In these fields, creativity and innovation exist through the act of copying itself.

Some markets enjoy a high level of creativity whose success is driven by the fact that work is constantly imitated and copied.

Open-source computer programming, for example, is a highly creative market — one in which all the source code for any particular program can be copied at any time.

This model has proven incredibly successful. Indeed, 25 percent of _all_ corporate servers run the open-source operating system Linux. More than half run Apache, an open-source web server software. The free and open-source web browser Mozilla Firefox has more than 150 million users.

Each of these programs develops and grows precisely because of their simplicity and availability for copying.

Copying is likewise an integral part of the culinary world, where aspiring chefs copy recipes and techniques to refine their craft and sharpen their taste.

The culinary world demonstrated its awareness of the importance of copying in 2006, when three of the most innovative chefs of recent decades, Ferran Adrià, Heston Blumenthal and Thomas Keller, published a manifesto in the _Guardian_, declaring that the best culinary traditions are "collective, cumulative inventions."

What's more, nonprofit, shared, creative materials can actually be more successful than the versions of those products that are protected by copyright.

Take Wikipedia, for example. It is written by anonymous, unpaid contributors who know that their material can be copied by anyone at any time as long as Wikipedia is credited. And yet it is today's most extensive encyclopedia, with more than 20 million entries.

Compare this to Microsoft's now defunct encyclopedia _Encarta_, which had only 62,000 entries despite its copyright protection and considerable resources.

### 4. Copying current inventions and tweaking them can lead to new innovations. 

When you think of Thomas Edison, the first thing that comes to mind is: "Oh, yeah. He's the guy who invented the lightbulb." But was this invention, for which he has won so much renown, a completely new idea? Or did he build upon others' work?

Edison's designs were actually variations on ideas from a number of different proto-lightbulbs, which eventually led to what we now know as the lightbulb.

Indeed, by imitating and tweaking ideas and inventions that already exist, we are able to widely expand innovation.

Fonts, for example, which aren't protected by copyright laws, are extremely easy to copy and tweak. Because these symbols remain unprotected, innovators have had the opportunity to create new, interesting fonts based on existing ones.

Even one of the most used fonts in the world today, Microsoft's Arial, was created as a way to avoid paying licensing fees for an existing font.

This kind of copying and tweaking has resulted in an explosion in the amount of fonts available to users. A survey conducted in 1990 showed there to be around 44,000 fonts. In 2002 that number was revised to 100,000, and 2012 the number of fonts reached 170,232!

Furthermore, copying speeds up market cycles and puts immense pressure on companies to innovate if they want to earn their place at the forefront of the market.

We see this clearly in the fashion world, where copies of name brand articles by less expensive brands cause those goods to fall out of fashion. While you need enough people wearing your designs to make them "trendy," their value diminishes as more and more people start wearing the same item.

Thus designers whose articles are copied have to work hard to create new items, and this increase in tempo leads to increased innovation.

### 5. Social norms can self-regulate copying without the need for intellectual property regulations. 

We don't always need laws to protect intellectual property. In fact, there are strong social norms within several of the fields not protected by copyright law that keep copying from becoming degenerate.

In the world of comedy we find that while comedians are not protected by copyright law, there is nonetheless a pact among them not to steal material from one another.

When comedian Dane Cook was accused of stealing material from Louis C.K., he was shunned by other comedians and received hefty criticism from his peers. These criticisms weren't without consequence: Cook has called the time after the accusations emerged the hardest in his life.

Similarly, a 2008 study on highly esteemed French chefs showed that while they borrow extensively from one another, they don't copy recipes exactly. The study also showed that when chefs disclose information about their dishes to other chefs, the trade secrets aren't passed on without permission. And when permission was given, the original chef would receive proper recognition.

What's more, consumers are able to discern copycats, and often prefer to have the original product over the copy.

In the example of Cook and Louis C.K., fans joined in the conversation. Even though Louis C.K. didn't bring the issue to light or comment on it, fans who had discovered the similarities accused Cook of plagiarism, both in blogs and anonymous videos on YouTube.

In fashion, a large part of the consumer experience is the social status granted by the garments. Copies generally convey less status. So wearing the right brand and being the first to do so upholds the status of the originals. An original Louis Vuitton bag, for example, carries much more status than a copy bought in a knock-off market in Thailand.

It certainly doesn't seem like copying is going away anytime soon. So how does the future look for innovation in a world with more copying?

### 6. In the future, competition will drive innovation through tweaking. 

It's easy to see how companies with strong intellectual property protection can stay ahead. All they have to do is submit a patent. But how exactly do companies compete when copying is abundant?

Competition drives innovation through the process of _tweaking_, or copying something that already exists, and improving it with small alterations.

We can see tweaking in action on the football field every Monday evening, when NFL coaches copy and improve upon plays to keep up with their competitors.

Even the legendary Steve Jobs of Apple has been described as a _tweaker_ rather than an innovator. Indeed, one of the last innovations credited to Jobs, Apple's iPad, was essentially a tweak of an idea from its competitor Microsoft.

In the financial industry copying and tweaking investment strategies is another way for investors to stay ahead of the competition.

For example, when John Bogle invented the first index fund — an investment strategy designed to emulate the average performance of stocks on the stock market — his contemporaries were unimpressed. At first they scoffed, and even took to calling his grand idea "Bogle's folly."

By 2001, however, there were more than 400 index funds in the American market. In spite of the fact that Bogle's strategy has been so prolifically copied, his company, The Vanguard Group, is still the leading provider of index funds.

Even some of the most highly competitive industries, such as tech and finance, are full of copying, imitating and tweaking. And yet none of these industries have come crashing down. Instead, they serve as prime examples of the kinds of innovation that copying and tweaking can bring to the table.

> _"Steve Jobs was the greatest tweaker of his generation." — Malcolm Gladwell_

### 7. Creativity itself will live on, even if some businesses will not. 

With every pirated copy of a hot new album, music industry executives lament the end of an era. But does copying actually threaten the industry? Or does it simply force change?

The record industry has indeed struggled as a result of file sharing, but this hasn't stopped artists from creating new music, and creativity still thrives.

Though the industry might suggest otherwise, musicians don't necessarily need the music industry's institutions to be successful.

Radiohead proved this in 2007, with the online release of their album _In Rainbows_. Downloaders paid by donation, giving whatever they wanted to or whatever they could afford. So how did this model affect the album's profitability?

A year after the release of _In Rainbows_, Radiohead's licencing agent confirmed that the album had made _more money_ than the band's previous record, _Hail to the Thief_, which had been released in the traditional manner by EMI.

While record sales decline, musicians are earning more money from live shows today than before. In the United States, ticket revenues for live shows have more than tripled from 1999 to 2009, jumping from $1.5 billion to $4.6 billion, which interestingly correlates with plummeting record sales.

Clearly, copying hasn't meant the death of the industry, and the same is true for film.

Today, it is exceedingly easy to create copies of movies through streaming or file sharing. You would think that this would kill movie sales, but this hasn't been the case for theaters that focus on the movie-going experience.

The Arclight movie theater chain in California, for example, charges twice the US average for tickets. How do they get away with it? By elevating the movie-going experience with larger, more comfortable seating, top of the line audio and visual equipment and a user-friendly booking system.

In fact, they're so successful that they've opened three new theaters in just a few years.

### 8. Copying is not going away, so business needs to harness the power of imitation to be successful in the future. 

In a world where imitation and copying seem more and more commonplace, how can innovative companies stay successful?

For one, having your work copied can act as a sort of advertising for your original product. By utilizing this "advertising effect," innovators can gain an advantage over their competitors.

This was demonstrated by a study conducted by Renee Gosline of MIT, which showed that copies can act as "trial versions" of the original product. The two-and-half-year-long study showed that as many as 40 percent of those who bought a knock-off handbag eventually decided to buy the original.

A similar study of 31 shoe companies showed that counterfeiting actually had a _positive_ impact on sales. Why? Because the high visibility of copies worn in the street made the genuine objects more desirable.

Moreover, the technology that has made copying easier can also be used to help companies stay relevant and competitive in the future.

Companies working with an open-source model, for example, are able to use the power of imitation and tweaking to promote other services and products they produce.

The tech giant IBM, for instance, employs 600 people to develop the Linux operating system. While Linux doesn't earn them money directly, they have recognized that the normal servers and hardware they sell will benefit from a market where there are several different operating systems.

Yet another open-source operating system, Google's Android, is now the largest mobile platform in the United States. Since it is licence-free, Google doesn't make money from the operating system itself. Rather, they earn money by ensuring that consumers use Google's search engine in order to profit from search-related ads.

As the development of technology and the speed of our global economy continue to accelerate, copying will not become less ubiquitous, but rather the opposite. Learning how to turn this into an advantage will be one way to ensure future success.

> _"Copies act as a 'gateway drug' that leads to consumption of the harder, original items."_

### 9. Final summary 

The key message in this book:

**People often picture copyright laws as the shield that protects innovators from pirates who would destroy creative industry. However, copying isn't a bane. In fact, it's a boon to creative fields! Imitation, copying and tweaking are the foundation for innovation in today's world.**

**Suggested further reading:** ** _Steal Like an Artist_** **by Austin Kleon**

This book will help you unlock the secret to creating great art: theft. No artist creates their work in a vacuum: all art is influenced by the art that came before it. _Steal Like an Artist_ teaches you how to "steal" from the work of your heroes, and use it to create something new and unique. It also provides important advice on using the internet to launch your career, so others can enjoy your creativity!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Kal Raustiala and Christopher Sprigman

Kal Raustiala is a professor of law at UCLA as well as the author of _Does the Constitution Follow the Flag?_ His research focuses primarily on international law, international relations and intellectual property.

Christopher Sprigman is a professor at New York University School of Law. His research deals primarily with copyright and intellectual property.

