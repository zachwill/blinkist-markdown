---
id: 569e81adeaed8a0007000044
slug: the-knowledge-en
published_date: 2016-01-22T00:00:00.000+00:00
author: Lewis Dartnell
title: The Knowledge
subtitle: How to Rebuild Our World from Scratch
main_color: D63F42
text_color: B23334
---

# The Knowledge

_How to Rebuild Our World from Scratch_

**Lewis Dartnell**

_The Knowledge_ (2014) is a practical guide to surviving the apocalypse. These blinks explain both how to survive a horrific catastrophe and how to create a self-sustaining life and rebuild civilization.

---
### 1. What’s in it for me? Gain the tools that’ll help you survive the apocalypse! 

Think about all the possible apocalyptic events: a pandemic, an asteroid, a nuclear war. Any of them, at any time, could utterly demolish civilization as we know it. An unsettling thought, to say the least. 

So, what if _you_ lived through such a catastrophe? It'd probably be helpful to know some basic survival techniques. What if all of civilization's technology was destroyed? What would you do? How would you find food, treat medical issues and stay warm?

In these blinks, you'll find out

  * how to treat water so it won't make you sick;

  * how to start a fire without matches or a lighter; and

  * which simple drink cures cholera.

### 2. In the event of a catastrophe, your priorities are shelter, warmth and water. 

What would you do if a cataclysmic event occurred? In the aftermath of such an event, whether a catastrophic asteroid collision or an unstoppable pandemic, civilization as we know it will begin to fall apart. So, if you're lucky enough to survive such an event, it's imperative that you make wise use of the brief period before total collapse. 

First off, you'll need a roof over your head and a fire to protect you from the elements. Shelter shouldn't be too hard to find, though; after a catastrophe and its massive death toll, empty houses will be plentiful.

But keeping yourself warm is another story, because without electricity, heaters will be useless. One accessible option for fighting off the cold is clothing. Most small shops will have been destroyed, but goods in large shopping centers will probably be safe. After all, they're buried deep inside the cavernous interior of a mall. Malls are therefore a good bet for finding clothes that'll last you a few years. 

Sometimes, however, clothes aren't enough, and you'll need a fire to stay toasty. You'll probably have found some matches or lighters, but if you haven't, you can always go old school: grab a magnifying glass or a pair of glasses and use it to focus the sun's heat on a piece of paper. 

Once you're warm and secure, you'll need clean water. Right after the catastrophe, you'll be able to get potable water from the municipal supply and bottled water from supermarkets. Be sure to save all the water you can. That means filling clean buckets, baths and sinks with water. Keep your supply covered to prevent light from harming it, detritus from contaminating it and algae from growing in it.

And keep in mind, you'll need to sterilize any water that didn't come out of a sealed bottle — an easy thing to do. Just boil it for a few minutes and you'll kill any dangerous pathogens.

### 3. Staying well fed will mean scavenging, consuming wisely and starting your own farm. 

All right. You've got your shelter, you're warm and you've stored water. Next up is food and here's the good news: a single intact supermarket contains enough nutrition to sustain you for anywhere from 55 to 63 years! That is, if you're not averse to eating pet food. 

So, get to work scavenging as much food as you can. In the immediate aftermath of the catastrophe, you're best bet would be to hit the supermarkets. But the real question is, how long before the food goes bad?

Keep in mind that fresh food spoils fast. Fresh milk: less than a week. Fresh fruits, vegetables and cheeses: a few weeks. Eggs, though, are good for over a month, and grains can last for three to ten years if they're stored in a cool, dry place. Therefore, try to eat the foods that will expire the soonest. But be sure to maintain a balanced diet!

The goal is for this food to last until you can produce food for yourself. So, while scavenging, keep an eye out for seeds in backyards or on farms. Because eventually you'll need them to start a garden. 

But to grow anything, you need to know a few things about soil:

Soil is made up of gravel, sand and clay held together in a matrix of organic matter called _humus_, which, unlike the yummy chickpea spread, is soft and black. To ascertain what's in your garden, simply fill a third of a jar with soil and top off the remaining two-thirds with water. Shake your mixture until it looks like muddy soup and then leave it undisturbed for a day. After this time the composition will sort itself according to particle size and separate into distinct layers. 

On the bottom will be coarse material. Silt will be in the middle. The top will be fine particles of clay. Remember that the ideal soil for growing food is about 40 percent sand, 40 percent silt and 20 percent clay.

### 4. A successful farm requires hard work and attention. 

So, good soil is important, but it won't automatically grow the food necessary for you to survive. In fact, growing healthy plants requires a lot of hard work. 

How so?

Well, you've got to break up compacted soil and control weeds that (if left to their own devices) will choke your crops. Finally, you must prepare the highest layer of soil, also known as _topsoil_, in which to sow your seeds. 

Here's how to do that:

Use a plough to turn over the uppermost layer of soil. Doing this will both control weeds and mix organic material as well as nutrients into your topsoil. After that, smooth out the soil as much as you can by removing any furrows or ridges. Then you're ready to plant your seeds. Moisten the soil, and align your seeds in rows, keeping them evenly spaced. Well-spaced planting makes it easier to keep weeds under control and to harvest your food when it's ready. 

But that's not all. It's also essential to rotate the plants you grow to maintain healthy soil. In fact, to keep the nutrients of your soil balanced you'll need to carefully plan what you plant. For instance, some plants remove nitrogen from the soil while others inject it as they grow. Therefore, if you only ever grow one crop — say, wheat — you're liable to damage your soil. 

A better strategy is to rotate your crops. For example, you might just plant wheat once every four years and plant other crops in the years in between.

Then, once your produce is grown, you're ready to harvest, a relatively straightforward process for most crops. In fact, it's largely based on common sense. For instance, you dig up potatoes, pull up carrots and pick apples off the tree. 

On the other hand, harvesting cereal grains like wheat, oats and corn is a bit more labor intensive. The best strategy is to just cut down the entire plant and separate the grain off the field.

> _"As long as you can grow grain for yourself, along with some other fruit and vegetables for the sake of nutritional balance and a more interesting diet, you'll never starve to death."_

### 5. Sources of heat energy will be essential, both for daily use and industrial redevelopment. 

What do baking bread, brewing beer, blowing glass and forging metal all have in common? Every one of these activities, each essential to our contemporary society, rely on one thing: thermal energy — that is, the energy of heat. 

So, in a post-catastrophe scenario, charcoal production will be an essential means of rebuilding these industries. That's because you won't have access to more sophisticated methods of extracting heat energy, such as oil or coal. 

And while firewood is fine for keeping your house warm, accomplishing more complicated tasks, like those mentioned above, will require a fuel that burns much hotter than wood. Therefore, it'll be essential to produce charcoal. 

Charcoal is not only a clean energy source; it's also easy to produce from wood. So, while charcoal is _made_ out of wood, it burns much hotter and weighs much less, as the water content of the wood evaporates during its transformation into charcoal. 

Here's how to produce charcoal:

Start by digging a large trench and filling it with wood. Then start a fire in the trench and let it get quite large. Once the fire is raging, cover up the trench with corrugated sheet metal. Then put soil on top of the sheet metal, to prevent oxygen from getting in. That's it! In a few hours, you'll have charcoal, a resource that will prove essential to civilization's rebirth.

### 6. Preventing diseases is as simple as keeping yourself and your surroundings clean. 

Okay, at this point you've got most of your survival bases covered, but there's one important element missing: health care. The good news is that there's an easy way to prevent most infections from ever occurring, one which will become even more important in a world where doctors are hard to come by — simply keep clean!

In fact, simply washing your hands and drinking clean water effectively deters disease. That's because clean hands help you steer clear of most gastrointestinal and respiratory infections as well as diseases caused by microorganisms passed from person to person. 

But dirty water, loaded with pathogens and infectious potential, is also a major source of disease. Therefore, it's essential to boil any drinking water that could possibly be contaminated!

And if you don't have any soap?

Well, luckily, it's easy to make. In fact, it's simply oil and alkali, which are found respectively in plants and in ash. For instance, you can gather oil by pressing plant material like coconuts or olives; harvesting alkali from ashes is as easy as putting them in a pot of water. That's because the insoluble wood minerals will sink to the bottom and the mineral that you need will dissolve in the water. 

Then you just have to pour the water into another vessel and boil it all off. When all the water has evaporated, you'll be left with a white crystalline residue, also known as _potash_, which has the alkali you need. From there, you simply mix the potash with a boiling tub of oil and you've got soap!

### 7. Good medical care doesn’t require complicated technology. 

So, keeping clean is key to staying healthy, but you and those around you will inevitably need some sort of medical treatment, and calling an ambulance won't be an option. Thankfully, some common medical issues like infections of the intestines, also known as _enteric infections_, are easy to treat. 

For instance, cholera is an enteric infection that, historically, has wreaked much havoc. Oddly enough, however, cholera itself isn't deadly. The reason the infection can prove fatal is because of the rapid dehydration it causes, costing you up to five gallons of fluids a day!

Luckily, treating this menace is simple: just drink a mix of four cups _clean_ water, three tablespoons of sugar and one tablespoon of salt. 

And other diseases?

Well, thankfully, the complicated diagnostic tools that doctors use today, like blood tests and x-rays, aren't so essential. In fact, they're all more or less recent inventions, originating in the early nineteenth century. 

Up until that point, doctors simply diagnosed diseases based on visible symptoms, like off-colored skin, or audible cues, like the sound of the patient's heartbeat. But the invention of the stethoscope in 1816 certainly improved diagnoses, and lucky for you it's easy to make this tool with nothing but a hollow piece of wood. Simply press the tube to the patient's body and put your ear to the other end. 

This technology may be simple but it's an invaluable way of amplifying the internal sounds of the body. In fact, a stethoscope can identify the crackled wheezing of lung disease and even pick up the barely audible heartbeat of a fetus.

### 8. Measuring time will mean going back to basics. 

Nowadays, if you lose your way you can pull up a map on your phone or call for help. After a catastrophe, such options won't be available. So learning old methods for situating yourself and keeping time will be essential. 

For instance, you can use shadows to measure the time of day. As the earth orbits the sun, shadows shift, allowing you to keep track of the hours. 

Here's how:

Plant a stick upright in the ground and pay attention to the length of the shadow it casts. The longer it is, the closer it is to dawn or dusk, because at these times the sun is lower in the sky. Conversely, a short shadow means it's closer to midday. 

Not just that, but a sand timer, or hourglass, can measure the amount of time that has elapsed. It's easy to make: just connect the narrow necks of two glass light bulbs after filling one with sand. When the hourglass is stood upright, the sand will flow from one bulb to the other and the time it takes will depend on the width of the opening, the size of the bulbs and the amount of sand they hold. 

You'll also need to navigate your surroundings and there won't be any GPS to help. So, you'll have to use maps and a compass. A compass works by pointing north and, used in conjunction with a map, can help you find your way. They're also easy to make. You just need a magnet, a needle, a cork and some water.

Begin by rubbing the needle against the magnet, thereby magnetizing it, but be sure to always rub the needle in the _same direction_. Then, cut the cork to one inch and impale it on the needle. Now simply fill a wide cup with at least an inch of water, place your needle and cork on the surface and wait for it to point north!

### 9. Final summary 

The key message in this book:

**After an apocalyptic event strikes Earth, it'll be imperative for any survivors to immediately find shelter and clean water. Only once you've secured these life essentials should you go out looking for food. With enough food to sustain you, you can start growing your own. Once you've set up a small farm, you can stay healthy using a few old-school tricks of medicine and comfortably survive society's collapse before starting to build it anew.**

**Suggested further reading:** ** _Emergency_** **by Neil Strauss**

_Emergency_ is the personal story of the author's transformation from a helpless urban dweller to an independent survivalist. It's a first-hand account of how to train for complete autonomy should society as we know it collapse.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Lewis Dartnell

Lewis Dartnell is a UK Space Agency Research Fellow at the University of Leicester, where he does work in Astrobiology and searches for microbial life on Mars. His science writing and outreach work have earned him several awards. _The Knowledge_ is his third book.

