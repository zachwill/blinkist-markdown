---
id: 538f2fc33761300007170100
slug: the-laws-of-simplicity-en
published_date: 2014-06-03T14:45:53.000+00:00
author: John Maeda
title: The Laws of Simplicity
subtitle: Design, Technology, Business, Life
main_color: DC4047
text_color: BF383E
---

# The Laws of Simplicity

_Design, Technology, Business, Life_

**John Maeda**

_The_ _Laws_ _of_ _Simplicity_ consists of a set of "laws" formulated by the author to try to grasp the meaning and essence of simplicity. Along the way, it provides useful advice on how to introduce simplicity to our daily lives, business and product design.

---
### 1. What’s in it for me? Learn how to reap the benefits of simplicity at work and at home. 

Do you ever reminisce about the "good ol' days" when the phone in your living room and the mailbox in front of your house were the only ways to communicate with you from afar? Back when things were simpler?

Today, you're accessible basically everywhere imaginable. You get email on your laptop, texts on your smartphone, Facebook messages via your tablet, and who knows what will come next.

It can't be denied: although technology has made many things in our lives easier, every new technology — every upgrade, every new device and invention — adds a certain degree of complexity. So is there no escape?

Don't worry, there is. Not only _can_ we aim to make things simple, often, we actually _have_ to (if not to make money, then at least to stay sane). In fact, simplicity — rather than complexity — is sometimes even the secret to a product's success.

But it's not only businesses who benefit from making things simple: by simplifying our private lives, we can ensure that we get a lot more enjoyment out of them.

In these blinks, you'll find out

  * why your remote control is so small,

  * why people buy sleek, simple phones but zany accessories,

  * why it's worth asking the chef for his personal recommendation and

  * why simplicity can ruin your doctor's bedside manner.

> _"Simplicity is about subtracting the obvious, and adding the meaningful."_

### 2. In this increasingly complex world, simplicity is a luxury. 

Have you ever wondered what makes Apple products, like the iPod and iPhone, so successful? The answer is that Apple has managed to make their products _simple_ : easy to use and understand.

And in a world that's becoming increasingly complex — with more technology, more means of communication, more available information, etc. — simplicity has become a luxury worth striving to achieve. As such, simple products stand out in the market.

For example, the iPod stood out among competing mp3 players precisely because it was simpler than the other complicated brands. Indeed, because technology will always become more and more complex, you too can make your own products stand out by striving for simplicity in their design.

But how do you go about achieving simplicity?

Simply put (pun intended!), simplicity is all about "subtracting the obvious, and adding the meaningful."

The design of Apple's iPod is a perfect example of these principles in action. Apple knew that adding different buttons for each and every function was the _obvious_ design choice. So instead, the iPod shed all the complexity of other mp3 players by merging these different functions into a single, round control pad.

By combining elegance with function and ridding the device of unnecessary complexities, they were thus able to create a _meaningful_ innovation.

The formula for simplicity isn't just useful in the tech world; we can also use it in our private lives by subtracting the unimportant so the meaningful can stand out. For example, if you love someone, then taking away small talk and drama and simply acting according to your meaningful feelings will simplify — and enhance — the relationship.

No matter what area of life, the right kind of simplicity can make things easier and more meaningful. These next blinks will explore exactly how.

### 3. Simplicity can be attained through easy hacks like reducing functionality or shrinking the product. 

So now that we've seen why simplicity is an important characteristic for innovative products, how can we achieve this in product design?

The most direct path to simplicity is to remove functionality, i.e., decrease the number of things that your product can do. To do this successfully, you have to find the right balance between what's necessary and what's not.

For example, the simplest design for a TV remote control would be a device with only a "play" button. However, this would come at the cost of being able to pause a movie, for instance.

Unfortunately, there is a finite number of functions you can remove before your product is no longer valuable. However, you can get around this by shrinking the product itself to make it seem simpler.

Consumers are more tolerant of limited functionality when products are physically smaller. If a remote control is really small, people are surprised by what it can do rather than disappointed by what it can't.

Another strategy is to actually _hide_ some complexity without sacrificing function. Looking back to remote controls, for example: some look quite simple, but actually have a hidden section with additional functions for more advanced users.

Still, you should always keep the risk in mind that taking away functions might reduce the appeal of your product. Therefore, it should be designed to seem _valuable_ to customers. This means that your product should have some actual or perceived quality that makes up for its reduced function.

For example, although the iPod had few functions, it was nevertheless a hit because its high-quality design gave it value _beyond_ its basic functionality.

If, however, your product does not offer any value beyond its simple design, then you will have to _create_ perceived value with things like commercials and other marketing strategies.

### 4. In the realm of time management, organization can bring simplicity. 

As advances in technology make our lives increasingly complex, it becomes more and more important to reintroduce simplicity to your life wherever possible, including the way you manage your time. In order to prevent yourself from being overwhelmed by the huge amounts of information and responsibility that come with technology, it's essential that you stay organized.

Proper organization starts with grouping and labeling your tasks. For example, if you want to plan out all your responsibilities for the week, you should first write down each task on a Post-it and then group them under a common theme. Doing so makes it easier to process a large amount of information by reorganizing that jumble of tasks into ordered information.

For instance, you could group your tasks into "shopping," "meetings," "phone calls," etc. Now, instead of having a huge pile of Post-its, you have a few groups of related activities. Also, adding labels to your task groups helps clarify what needs to get done. Rather than thinking of _all_ the things you need to shop for, for instance, you can consolidate them into one simple word: "shopping."

Once you've grouped your tasks, you should prioritize them. There will always be some tasks that are more important than others, and you should do those first.

Here, you can make use of the _Pareto_ _Principle_ to best prioritize your tasks. According to this principle, about 20 percent of your tasks account for 80 percent of the results you want to achieve. So, when looking at your tasks, try to identify the most effective and urgent 20 percent and do those first.

Let's say you have five reports to write. You should focus on the one that will potentially have the greatest impact, e.g., the one that could get you promoted, rather than focusing on the one that is due earlier but that no one actually cares about.

### 5. Using the time you have in the right way is crucial in simplifying your life. 

Everyone knows the feeling of being in a rush to finish their huge to-do lists. When this happens, everything seems overwhelmingly chaotic and complicated. However, the same to-do list can seem simple — and even fun — when you have plenty of time to get everything done.

In fact, having more time actually produces the feeling of simplicity. And while we can't add more hours to the day, we _can_ use our time more efficiently, thus leaving us more of it to spend elsewhere.

First, avoid wasting your time with waiting. When forced to wait, we squander the time we could have spent doing other things, so try and cut out as much waiting from your life as possible. For example, instead of waiting in line at the store, consider ordering online and having the products mailed to you overnight.

But if for some reason you truly must wait, then you can simplify your life by making the wait more tolerable. In fact, time spent more comfortably seems to pass by faster, thus making your life feel simpler.

While it's impossible to shorten the huge line at the post office on Christmas Eve, an employee could make the wait more bearable by handing out cookies to the waiting customers.

Finally, we can use tricks to fool ourselves into feeling like we have more or less time than we actually do. For example, if you're enjoying your free time, consider taking off your watch: when you can't see how much time passes, you'll feel like you have more of it.

Conversely, if you want idle time to pass faster, make sure you can see a clock. Studies have shown that time spent waiting for a computer task to be completed seems much longer and more painful to the user when there is no processing bar to measure the time, even if both time spans are the same.

### 6. Because knowledge makes everything simpler, learn how to teach things right. 

Have you ever had a great mentor or teacher? What was it that made them so great? It was most likely their ability to help you understand and do things on your own that you couldn't before. Essentially, they helped make things easier for you: what used to be complicated became simple. So what's the secret to being a good teacher?

While many teachers try to create motivation through rewards or punishment, i.e., positive and negative motivation, in reality, it is the style of teaching itself which fosters motivation. In order to motivate others to learn, follow these two rules:

1\. Always start with the basics. If you dive right into the complex subject matter, then people just get frustrated, and therefore lose motivation.

2\. Use plenty of repetition. The more something is repeated, the more likely it will be remembered permanently.

By utilizing this teaching style, you will inspire confidence in others because their solid understanding of the basics will motivate and inspire them to learn more. Once they get into the heavier stuff, it will seem much more simple.

When designing products, keep it simple by making sure people can use the product without having to learn anything first. Indeed, a newly designed product should seem instantly familiar to users — they should automatically feel as if they know how to use it.

However, you also want to make sure that there's something new and surprising to the design, thus keeping users attached, rewarding their curiosity.

However, when a product or its features are a bit more complex, then you can use the same teaching principles as before. For example, if you design a new computer program, make sure new users can at least use its most basic functions easily, otherwise normal users won't use the product.

### 7. We can simplify our lives by placing our trust in others. 

Have you ever been at a restaurant and _everything_ sounded so good that you couldn't to decide what to get? Wouldn't it be simpler if there were something or someone you trusted who could have made that decision for you?

Indeed, you can simplify your life by relinquishing control to someone or something you trust. If you trust that technology will work to your benefit, then you can hand over some of your responsibilities to it.

Take Amazon: you've probably noticed that when you look for new products, it offers you recommendations according to your latest purchases. If you trust Amazon's algorithm to have a good understanding of your taste, then you can find a new read with a single click rather than having to go through hundreds of books yourself.

Of course, it's not always easy to put your trust in someone else; in fact, it's completely natural to second-guess both others _and_ yourself.

Taking a closer look, we see that it arises from two different sources: trust in mastery and trust in the power of "undo."

When we decide to trust someone, it's because we place our confidence in that person's competence. Imagine going to a Japanese restaurant where you can choose a menu made by the chef especially for you. If you trust that the chef is a master in his art and can put together a great meal, simplify your life by ordering that menu. It's one decision less you need to make.

With regard to ourselves, we look to whether we can "undo" our actions. These days, we can return purchases to the store, delete embarrassing comments we made on Facebook or effortlessly correct any spelling mistakes in our thesis.

The ability of "undoing" things brings more simplicity to our lives because we don't have to focus on our actions as much.

Now that you have a solid grasp of the importance of keeping things simple, the following blinks will show you how to find the balance between simplicity and complexity.

### 8. Simplicity is not always the answer, especially when it comes to dealing with emotions. 

Imagine you're sitting in the waiting room at the doctor's office waiting for test results. The doctor comes into the waiting room and suddenly announces: "You have cancer," then turns around and leaves to attend other patients, leaving you alone in the waiting room.

While this is certainly the quickest, simplest way of communicating your diagnosis, it's obviously not the right one. This goes to show that pure simplicity is not always the best solution, especially when people's emotional well-being is at stake!

In fact, sometimes we have to make things a bit more complicated out of consideration for human emotions.

Consider text messages, for example. While they've certainly brought us simplicity, allowing for the quick exchange of concise information, that simplicity comes at a price: our voice, facial expression, gesticulation, etc. are all totally absent from text messages.

Removing these elements of communication means that we have a more difficult time expressing human emotion while communicating through texts. In order to compensate for this, we had to re-introduce some complexity in the form of smileys and other icons that do justice to our feelings.

Even in product design, simplicity sometimes has to give way to complexity, as people also want the products they purchase to express their individuality.

The seeming contradiction between the desire for simplicity and expressing emotions can be easily seen in the way we accessorize our mobile devices. For example, people buy the iPhone because they love its simple design, and then wrap it up in colorful and creative cases as an expression of their individuality, i.e., their emotions and feelings.

Ultimately, this model puts control of the phone's simplicity or complexity directly in the hands of the customer.

> _"(...) although the idea of ridding the earth of complexity might seem the shortest path to universal simplicity, it may not be what we truly desire."_

### 9. Technological developments change which facets of our lives can be simplified. 

As technology constantly changes and improves, its developments impact the way we understand and conceive simplicity.

For starters, technology is increasingly able to hide complicated processes so that we — the users — see only simplicity.

One example of this is the way that the internet cuts through space and time. For example, whenever you search for something in Google, the actual work of retrieving and sorting immense amounts of data happens at far-away servers, totally out of sight.

However, what _you_ experience is the local action of simply entering your search query and then seeing a clear and ordered list of search results. Rather than having the complexity of immense data stores in your bookshelves, you enjoy the simplified, local result of complex work done far away.

In addition, open-source technology — where the product's design or programming code is unlicensed, open and free to everyone — enables entire communities of individuals to work on problems together, thus making those problems simpler to solve.

Because open-source software is free for _anyone_ to use and develop, it often evolves through the collective effort of an invested community. Indeed, when many people have access to a complex task or problem, the sheer size of the group makes solving it easier.

For example, open-source software like the Linux operating system has an openly accessible source code, meaning that anyone who wants to can look at and modify the software. As such, a community has developed around the software, consisting of thousands of experts who know the code inside and out.

For the average user, this means that if you have a problem, you can ask the community and get an expert response very quickly, which greatly simplifies the troubleshooting process.

### 10. Final Summary 

The key message in this book:

**In** **a** **world** **full** **of** **complex** **technologies** **and** **products,** **it** **is** **simplicity** **that** **makes** **a** **product** **or** **service** **stand** **out.** **It's** **therefore** **crucial** **that** **businesses** **understand** **how** **to** **balance** **complexity** **and** **simplicity,** **and** **can** **implement** **this** **understanding** **in** **their** **product** **designs.**

**Organize your life.**

If you want to simplify your life, organize it! Prioritize the most important 20 percent of your tasks and do those first.
---

### John Maeda

John Maeda is a graphic designer, computer scientist and visual artist, professor of Media Arts and Sciences at MIT, and founder of the SIMPLICITY Consortium at the MIT Media Lab. He has also authored a number of books, including the critically acclaimed _Design_ _by_ _Numbers._

