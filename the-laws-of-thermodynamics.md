---
id: 5e40adac6cee070006b5f901
slug: the-laws-of-thermodynamics-en
published_date: 2020-02-12T00:00:00.000+00:00
author: Peter Atkins
title: The Laws of Thermodynamics
subtitle: A Very Short Introduction
main_color: None
text_color: None
---

# The Laws of Thermodynamics

_A Very Short Introduction_

**Peter Atkins**

_The Laws of Thermodynamics_ (2010) is a short and accessible introduction to thermodynamics, the field of physics concerned with the relationships between different forms of energy. Authored by one of the world's preeminent authorities on the subject, Peter Atkins, it explains the four laws that govern the universe — the zeroth, first, second, and third laws. Along the way, _The Laws of Thermodynamics_ unravels the workings of familiar-sounding concepts like temperature as well as more exotic ideas like entropy and energy states.

---
### 1. What’s in it for me? Learn about the laws of the universe. 

Why does boiling water make the lid of a pot rattle, and why do the potatoes inside the pot cook? Why does burning gasolene make two-ton cars move? How do refrigerators cool your leftovers?

The answer to each of these seemingly simple questions takes us to the heart of thermodynamics, a form of theoretical physics that studies the transformation of energy. Everything that happens in the universe, from the expansion and contraction of gasses to the heating and cooling of metals, is governed by the discipline's laws.

In these blinks, we'll be following top physicist Peter Atkins as he explains the workings of the zeroth, first, second, and third laws of thermodynamics. His method? Assume nothing — not even seemingly obvious concepts like temperature. The result? A rock-solid theoretical structure that explains how our world really works.

You'll also find out

  * why steam engines can't use all the heat they produce;

  * what happens when you cool atoms to the lowest possible temperature; and

  * why crystals have higher entropy than gasses.

### 2. If two systems are in mechanical equilibrium, then a third system in equilibrium with one will also be in equilibrium with the other. 

Thermodynamics concerns itself with _systems_. What we mean by this is: anything that has boundaries. A block of steel is a system. A combustion engine and the human body are also systems.

Beyond those boundaries, we find the system's _surroundings_. This could be a bath of cool water in a laboratory or the atmosphere around a system. Together, a system and its surroundings make up the _universe_.

Systems can take different forms. This depends on the nature of their boundaries. Imagine a flask without a lid. That's an "open system." Pop a lid on the same flask and you've got a "closed system." Then there are "isolated systems." These aren't affected by their surroundings at all. A vacuum flask is a good approximation of such a system.

Right, now that we've defined our terms, let's unpack the first concept we'll need to get to grips with to understand thermodynamics — _mechanical equilibrium_.

Picture two metal cylinders next to one another. Both are fully sealed except for a horizontal tube joining them together like a walkway between two buildings. This tube contains two pistons held together by a rigid rod.

The pistons move this rod back and forth according to the pressure in their respective cylinders. If the pressure is higher on the right than on the left, the right piston pushes the rod toward the left cylinder and vice versa. This is like a tug-of-war, but with pushing rather than pulling.

If the pistons don't move, we can infer that the pressure in both of these cylindrical systems is equal. In this case, we say that they are in _mechanical equilibrium_.

At this stage we can give our two existing cylinder systems names — we'll call them "A" and "B" — before adding a third, "C," and seeing what happens.

Cylinder C is connected to A with another tube containing movable pistons. Let's say these pistons don't move and there's no tug-of-war between A and C. We can now conclude that the pressure in both systems is the same and that A and C are in mechanical equilibrium.

But what happens if we detach C from A and connect it to B instead? In a word, nothing. There won't be a tug-of-war here, either. If C and A _and_ A and B are in mechanical equilibrium, then C and B will also be in mechanical equilibrium.

Why is this important? Let's find out...

### 3. The zeroth law is about thermal equilibrium, and it allows us to introduce the concept of temperature. 

Now that we've gotten to grips with mechanical equilibrium, it's time to unpack the first law of thermodynamics — law zero or, as physicists call it, the _zeroth law_.

We'll move forward in the same way we did in the previous blink — that is, without assuming anything. Before we can talk about temperature or heat, we must first show our workings. To do this, we'll begin by introducing a new concept: _thermal equilibrium_.

Think back to our cylinders, A and B. We're now going to modify them a bit. Instead of being joined by a tube containing moveable pistons, we'll simply place them next to each other so that their sides are touching. What will happen next?

Well, since these are two distinct systems, we might expect some changes to occur as they influence one another. There might, for example, be a change in pressure or color. If there is no change, by contrast, we can conclude that cylinders A and B are in thermal equilibrium.

Let's introduce a third cylinder, C. We'll position C so that it comes into contact first with A and then with B. If no change occurs in the first instance, we also know that nothing will happen in the second instance. Put differently, if A and B _and_ A and C are in thermal equilibrium, then B and C will also be in thermal equilibrium.

This is the zeroth law of thermodynamics. At this point, we _can_ talk about temperature. This in turn will allow us to summarize the zeroth law.

In the first blink, we looked at mechanical equilibrium and used the concept of pressure to show how it works. Equal pressure, we concluded, means mechanical equilibrium.

The zeroth law allows us to infer that there must be a similar property determining thermal equilibrium. We don't yet know how this property works, but we can give it a name: temperature. This in turn gives us our summary of the zeroth law. It states that if A and B have the same temperature and B and C have the same temperature, then A and C will also have the same temperature.

### 4. The Boltzmann distribution tells us how atoms are distributed across energy levels given a certain temperature. 

So far we've looked at pistons and cylinders from the outside, but what's going on inside these systems? To answer that question, we need to zoom in to the atomic level. We won't be talking about individual atoms in this blink, however — we're going to be exploring groups of atoms.

Zooming in like this takes us beyond _classical thermodynamics_, a field of physics established in the nineteenth century — a time before the existence of atoms had been widely accepted. Instead, we'll be dealing with _statistical thermodynamics_. This approach emerged later and is concerned with the probabilities of certain behaviors when you look at lots of atoms simultaneously.

So how do you do that? That's where something called the _Boltzmann distribution_ comes in. The Boltzmann distribution refers to the precise distribution of atoms over their "allowed states." Let's break that down.

Every atom has a defined energy, but this energy is confined to a set of discrete _energy states_. Each of these states has either more or less energy — there can't be anything in between. You can see how this works if you picture balls tidied away in a school gymnasium storeroom. The balls are either on the shelves or on the floor, and there are no balls floating between two shelves, or between the lowest shelf and the floor.

The Boltzmann distribution essentially tells us where atomic "balls" will be. According to the Boltzmann distribution, all atom groups are distributed exponentially across their available states. This essentially means that the largest group will be clustered in the lowest possible energy state — the so-called _ground state_. A slightly smaller group will be in the next highest state, and fewer number in the state above, and so on through the different energy states.

The Boltzmann distribution also states that groups of atoms move to higher energy states as their temperature increases. Their distribution across the available states is still exponential — that is, there are fewer groups in the higher states than there are in the lower. But overall, there's an upward migration: the lower states lose atoms and the higher states gain atoms.

The Boltzmann distribution is important because it tells us how groups of atoms will be distributed across energy states if we can determine their temperature. As we'll see later on, this is the key to explaining all sorts of phenomena.

It also gives us a new, molecular definition of temperature. Put simply, temperature is the parameter that tells us how atoms are distributed across energy states.

### 5. The first law of thermodynamics states that the internal energy of an isolated system remains constant if no work is done on it. 

To understand the _first law of thermodynamics_, we need to explain a new concept — work. The word itself is pretty familiar, but we're not talking about what you do when you get to your office. In this context, work is a _mechanical_ concept.

Here's the basic definition: work is motion against an opposing force. Think of a pulley lifting a heavy object, for example — the pulley's work is a constant battle against the opposing force of gravity. When you struggle to remain upright in a strong wind, your body is also "working" in this sense.

All systems are capable of doing work. This capacity is called _energy_. Different systems, however, have different amounts of energy, meaning that some are capable of more work than others. But whether it's a pulley or an electric heater, all forms of work induce the same change in a system's energy.

This is a bit like climbing a mountain. As long as you start at the bottom and are heading to the top, it doesn't matter which path you pick — you're always gaining altitude.

This is why a system's capacity to do work is termed _internal energy_. It's "internal" because it's a property of the system rather than the nature of the work being done. Once you're at the summit of a mountain, your altitude is always the same, regardless of how scenic or repetitive the journey was that brought you there.

Unless a system is completely isolated, some of its work will be transferred to its surroundings. If a system's temperature is raised, for example, the temperature of its surroundings will also rise. Similarly, if its temperature falls, the temperature of its surroundings will also fall. The name for the process by which energy is transferred from a system to its surroundings or vice versa is called _heat_.

That brings us to fully isolated systems. These do not transfer heat to their surroundings and their surroundings are equally incapable of transferring heat to the system. We can therefore conclude that, as long as no work is done on an isolated system, the internal energy of that system will always remain constant. This is the first law of thermodynamics.

### 6. Some heat is lost to the surroundings as it’s converted into work, and work must be done to transfer heat from a cold object to a hotter one. 

The _second law of thermodynamics_ is notoriously tricky, so let's bring things down to earth with a concrete example — the steam engine — before we get into the more abstract stuff.

On one level, steam engines are extremely complex feats of engineering, combining hundreds of separate parts and carefully designed pistons and valves. The principles behind the mechanism, however, are relatively straightforward. Steam engines essentially have three components. First, a hot energy source — steam. Second, a device that transforms that heat into work — that's the job of pistons and turbines. Finally, there's the _cold sink_, a vent that extracts energy that hasn't been used as heat.

The cold sink is a vital part of every steam engine, and it helps us begin explaining the second law of thermodynamics. Why is that? Well, it tells us that when heat is converted into work, some of the heat will be transferred to a system's surroundings.

But there's more to the second law than this insight. Let's look at an everyday occurrence to see what else we can learn.

When you fill a mug with boiling water, the mug itself gets hotter. This happens without you doing any work — no pistons or electricity are required. In thermodynamics, this is known as a _spontaneous_ heat transfer because it simply happens without any work being done.

But let's say you've forgotten your mug of tea and it's tepid. You decide to make an ice-tea popsicle and put it in the freezer. Here, it will get colder and colder as the freezer works to transfer heat from the tea to the warmer surroundings of your kitchen outside the freezer's boundaries.

This process is anything but spontaneous. Freezing a beverage requires work. Somewhere, fuel is being combusted in a power station to generate the electricity that keeps your freezer functioning.

So here's the conclusion we can now draw: heat cannot be transferred from low-temperature systems to high-temperature systems without work being done elsewhere. This is our second key insight.

### 7. The second law of thermodynamics states that the entropy of the universe increases during spontaneous changes. 

So far we've discovered two important things. First, when heat is converted into work some of the heat is transferred to the surroundings. Second, heat cannot be transferred from high-temperature systems to low-temperature systems without work occurring somewhere along the line.

The challenge we now face is combining these two insights into a single statement. To do this we need to understand _entropy_.

Entropy concerns the quality of a system's energy. When we talk about entropy, we're actually talking about disorder. Energy and matter in a gas, for example, are distributed in a disorderly pattern. This means gas has high entropy. Energy and matter in a crystal, by comparison, are neatly arranged. Crystals thus have low entropy.

Now, when heat is transferred to a system there is also a change to its entropy. How this change plays out depends on its initial entropy and the size of the heat transfer.

Sneezing can help us unpack this idea. Imagine two places. The first is a library — our metaphor for a low entropy system. The second is a bustling main street filled with shops and passersby. This is a high entropy system. A sneeze meanwhile represents the transfer of energy as heat.

If you sneeze in a library, you will create a great deal of change and disorder — think of the startled readers, the scattered papers, and the dropped books. Sneeze in a busy main street, though, and nothing will really happen. The lower the entropy of a given system, in other words, the more easily this entropy will change. Similarly, just as a bigger sneeze creates more disorder than a smaller sneeze, the larger the heat transfer, the larger the change of entropy.

At this stage we're in a position to venture a statement of the second law of thermodynamics and see if it captures the two insights about heat and work we explored in the previous blink.

We can start by stating the entropy of the universe increases during spontaneous changes. Remember, "universe" means the system we're looking at together with its surroundings while "spontaneous" means that no work is occuring.

So here's what our statement boils down to: whenever heat is transferred without work being required, the entropy of a system and its surroundings increases. There's the theory. But does it work like this in practice? It's time to find out.

### 8. The second law explains the way cold sinks work and the direction of heat transfers. 

Before we move on to the third law of thermodynamics, we need to answer a couple of questions. First off: does the second law as we've formulated it make sense of our first statement that some heat will be transferred to the system's surroundings when heat is converted into work?

Put differently, what would happen if a steam engine consisted of a hot energy source and a turbine but lacked a cold sink?

Well, let's see. When heat leaves the hot energy source, the entropy of the system decreases — the hot source becomes less disordered. But as the turbine converts this heat into work and transfers this work to its surroundings, the system's entropy won't actually change at all. Why? Because entropy only changes when heat is transferred, not when work is done. So without a cold sink, no heat would leave the turbine and there would be no change to the system's entropy.

All of this means that, overall, entropy will decrease in an engine without a cold sink. But this can't happen; it contradicts the second law of thermodynamics. As we've seen, the entropy of the universe _must_ increase during spontaneous changes. All of this allows us to conclude that a heat engine without a cold sink is simply impossible: the cold sink must be present if entropy is to increase within this universe, and this is why it is so vital.

That brings us back to our second statement, that spontaneous heat transfers from hot to cold objects. Let's attempt to imagine the opposite of that statement — namely, heat being transferred from cold to hot objects — to see how it works.

When energy leaves the cold object in the form of heat, entropy decreases. But this cold object is like the library we discussed in the previous blink: it is orderly. This means the decrease in entropy is relatively large. As this energy enters the hot object — the busy street outside the library — the entropy increases, but only by a smaller amount.

All in all, the entropy of the universe has decreased. According to the second law, this means it cannot have been spontaneous — there must have been some work. This is just what we proposed in our second statement. Our conclusion? We have successfully verified the second law of thermodynamics!

One question, however, remains: what exactly is entropy? So far, we've said it concerns disorder, but the full significance of this isn't yet clear. In the next blink, we'll take a closer look at this tricky concept.

### 9. Entropy is a measure of the probability of determining the energy state occupied by a molecule. 

What do we mean when we say that entropy is a measure of disorder? To answer that question, we need to zoom in and look at the matter at a molecular level.

Think back to our gymnasium storeroom. The balls were either on one of the many shelves or on the floor — there weren't any balls floating between any two "levels." This was how we illustrated the distribution of particles across their available energy states.

We also observed that, once the temperature of a group of atoms increased, these atoms migrated upward from the floor to the shelves and from lower to higher shelves. The result was that a wider range of energy levels or "shelves" were occupied after the temperature had risen than before.

Remember, though, that we were talking about groups of atoms — we still haven't talked about the energy level of single, randomly selected molecules. So how can we predict the energy level occupied by these individual molecules?

Let's start by restating that when the temperature is higher, the range of possible energy levels is greater. This means the probability of predicting the energy level of any given molecule is lower. And this — the greater uncertainty about the energy level occupied by molecules — is what we have in mind when we talk about increased disorder.

All the pieces of this puzzle fall into place when we look at the entropy of a substance at _absolute zero_. This is the lowest possible temperature, minus 273 degrees Celcius. In thermodynamics, which measures temperature with the Kelvin scale, this is zero Kelvins — hence absolute zero.

At absolute zero, the Boltzmann distribution shows us that only the lowest energy state — the ground state — is occupied by molecules. In other words, when you turn the storeroom into a freezer, all the balls end up on the floor. So what happens if we select a molecule at random?

Well, we will have absolute certainty that it will be occupying this ground state. This is equivalent to saying that there is zero uncertainty regarding the energy state of individual molecules, and thus zero disorder and zero entropy.

### 10. Enthalpy, Helmholtz energy, and Gibbs energy are all useful accounting tools in thermodynamics. 

Think back to your first paycheck. It looked like a lot of cash, right? It was probably at that point that you realized that a good chunk of that money was going to be being deducted to cover taxes. When heat is converted into work, something similar happens. Every time a system produces heat, as when burning fuel creates steam, that system has to pay a "heat tax."

Imagine you're burning a hydrocarbon like coal in a cylinder fitted with a movable piston. The combustion of the fuel produces carbon dioxide and water vapor. Together, these take up more space than the original fuel and oxygen. To accommodate this extra volume, the piston is driven outward. This requires work, and some of the heat produced by the reaction is used to cover this energy expenditure.

Some reactions work in the opposite way: the products of the reaction take up less volume than the reactants. In these cases, the system's surroundings take up the slack and do the work. The result? The system has more energy that it can release as heat. Think of it as a kind of tax rebate.

When physicists interested in thermodynamics take account of this tax, they use a concept called _enthalpy_. The enthalpy of a system is equal to its internal energy, plus or minus the tax. This allows us to work out how much heat a system is capable of generating.

Taxation, however, cuts both ways. As we've seen, when it produces work, it pays a heat tax; when it produces heat, on the other hand, it pays a work tax. This is because of the second law of thermodynamics.

Any spontaneous change — that is, any change capable of producing work — is also accompanied by an increase in the entropy in the universe. We'll call this an entropy tax.

Thermodynamics has two accounting tools that help physicists keep track of these changes. First off, there's _Helmholtz energy_. This is the total amount of work a system is capable of producing during processes occurring at a constant temperature and volume given this tax. Then there's _Gibbs energy_ — the total amount of work a system is capable of during processes occurring at a constant temperature and pressure given this tax.

So much for the tax man! In the next blink, we'll wrap things up by looking at the third, and final, law of thermodynamics.

### 11. The third law of thermodynamics states that crystalline substances at absolute zero have zero entropy. 

Now that we understand the zeroth, first, and second laws of thermodynamics and seen what entropy is all about, we can move on to the final law — the third law. This law is a little different from the others as it won't require us to introduce any new concepts. Instead, it will allow us to tie together the concepts we've discussed so far.

The first thing we need to say about the third law is that it draws on an important insight into _cyclical processes_. As the name suggests, these processes go full circle, meaning that the system in question returns to its original state. You can see how this works if you think about your refrigerator. When you place something inside a fridge, it removes heat from this object and transfers it to its surroundings. The refrigerator itself, however, always stays the same temperature.

Cyclical processes cannot bring an object to zero degrees Kelvin — absolute zero — in a finite number of steps. No one, for example, has ever managed to place an object inside a refrigerator and incrementally feed the appliance more and more power until the object had been cooled to absolute zero.

This also applies when you attempt to reduce the temperature of an object by targeting its entropy directly. One way of doing this would be to thermally isolate the object and then apply a process known as _adiabatic demagnetization_. This essentially makes electrons spin in the same direction, thus lowering their entropy. But repeating this process won't bring the object to absolute zero — no finite sequence of cyclic processes can cool a body to absolute zero. This means there must be a point at which the cooling process ends and at which its entropy cannot be lowered any further.

And this is what the third law of thermodynamics states, albeit with a couple of modifications. First off, the law only applies to certain substances — so-called _perfectly crystalline_ substances. These have zero entropy when their temperature is absolute zero. Other substances have non-zero entropy when their temperature reaches absolute zero. These are known as _degenerate_ substances, and they're not covered by the third law.

Second, for the sake of convenience, we say that crystalline substances converge on a common entropy value of "zero," even though we don't know its absolute value. The third law can therefore be summarized as: the entropy of all perfectly crystalline substances at absolute zero temperature is zero.

### 12. Final summary 

The key message in these blinks:

**The zeroth law of thermodynamics governs thermal equilibrium and introduces the concept of temperature. The first law states that the internal energy of an isolated system remains constant so long as no work is done on it. The second law introduces the concept of entropy — a measure of disorder in energy — and states that entropy in the universe must always increase during a spontaneous change. Finally, the third law tells us that the entropy of all crystalline substances approaches the same value as the temperature nears absolute zero. These are the pillars of thermodynamics.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _A Brief History of Time_** **, by Stephen Hawking**

If you've enjoyed this introduction to thermodynamics, you might just be wondering how this subfield fits into physics as a whole. There are few better places to start answering that question than Stephen Hawking's _A Brief History of Time_.

Using non-technical terms, Hawking tackles the big questions — where does our universe come from, and how will it end? Along the way, he breaks down key concepts like time and space, forces like gravity, and theories like general relativity. So, now that you've got the basics down, why not take a look at the bigger picture and check out our blinks to _A Brief History of Time_, by Stephen Hawking?
---

### Peter Atkins

Peter Atkins is a renowned physicist and the author of over 60 books, including _Physical Chemistry_, a standard textbook used by students around the globe. Atkins is a Fellow of Lincoln College, University of Oxford, and a well-known face on the international lecture circuit. He has been a visiting professor in China, France, Israel, and New Zealand.

