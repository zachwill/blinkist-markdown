---
id: 581da3167882c20003e52c57
slug: the-leader-as-a-mensch-en
published_date: 2016-11-07T00:00:00.000+00:00
author: Bruna Martinuzzi
title: The Leader as a Mensch
subtitle: Become the Kind of Person Others Want to Follow
main_color: A2524B
text_color: A2524B
---

# The Leader as a Mensch

_Become the Kind of Person Others Want to Follow_

**Bruna Martinuzzi**

_The Leader as a Mensch_ (2009) is a guide to becoming a great leader. Learn the importance of caring for those you lead, and how reflecting on yourself will bring you closer than ever to becoming the best leader you can be.

---
### 1. What’s in it for me? Discover what makes a true leader. 

In many Jewish communities, the German word "Mensch" no longer simply means "man" or "human being"; it connotes a host of positive attributes and admirable traits. A Mensch is a man or woman with integrity and honor — a stalwart human being.

The traits that constitute a Mensch — or, for that matter, any truly great leader — have a tree-like structure. Just as a tree has roots, a trunk and branches and leaves, a Mensch has traits that root her firmly to the ground, others that keep her upright and some that provide a comfortable shade for those she leads.

When all these traits are combined, you get a leader that inspires others and can create a creative atmosphere in which people do their best work. But what exactly are these traits?

In these blinks, you'll also learn

  * why former NFL player Anthony Kevin Dungy never brags;

  * why a sense of responsibility is crucial in a leader; and

  * what a cleaning lady has to do with curing cancer.

### 2. The roots of a great leader lie in humility, empathy and authenticity. 

We all know that a tree without roots doesn't stand a chance. It'll fail to grow and a gust of wind could bring it crashing down. Well, the same goes for leaders. But what makes the solid roots of a great leader — or, in the author's words, a great Mensch?

First off, _humility_ helps a Mensch view things through the eyes of a novice, and this opens the door to infinite possibilities. Just take Anthony Kevin Dungy, a former NFL player and the current coach of the Indianapolis Colts. Despite his innumerable accomplishments, he never brags or even talks about himself.

Rather, he teaches his players in a toned-down, humble manner. He listens to their thoughts and is constantly learning himself.

The second thing about Mensch leaders is their awareness that sticking to their values will enable them to make a unique contribution to the world. Or, to put it simply, they're _authentic._

That means they stick to what they know is right, no matter what. For instance, if a Mensch profoundly cares for the environment, but learns he can make more money by using unsustainable materials, he'll ignore that option altogether. This is a key Mensch quality because it makes those around them happier and more respectful.

And finally, the third root of Mensch leaders is _empathy_, which helps them build trust by making others feel safe and understood. For instance, a Mensch takes into consideration how others feel about themselves during interactions. By being present and focusing entirely during any conversation, a Mensch shows others that he cares about what they have to say.

But empathy _doesn't_ mean you have to make everyone like you. To paraphrase former US Secretary of State, Colin Powell: if you try to please everybody, you'll avoid tough choices and shy away from confronting people when you should. According to him, this will only lead to your coming off as fake and untrustworthy.

> "_How we make others feel is the secret to human relationships."_

### 3. A strong trunk based on optimism, responsibility and mastery will hold a Mensch upright. 

Okay, now that you know what gives a great leader her roots, it's time to look at some above-ground attributes. It goes without saying that the first thing a leader should be is optimistic. Without optimism, a leader can neither inspire nor convince — but, beyond that, there are two essential pieces to a Mensch's "trunk."

The first is a willingness to take responsibility. After all, nobody wants to do a bad job and, when that inevitable misstep occurs, it takes courage to do the right thing: to be responsible and fix what went wrong. Not just that, but being honest and admitting you made a mistake will help you maintain the trusting relationships you've established with others.

Just take Michael McCain, the CEO of Maple Leaf Foods. When the company became implicated in the spread of a foodborne illness, he publicly announced that the company's best efforts had failed and that it was their job to fix the problem.

So, responsibility is the first essential piece of a Mensch's trunk. The other, equally essential piece is a resolution to live life with mastery. A true Mensch will always strive to master every dimension of her work, knowing that her every interaction can inspire others to do their best as well.

A Mensch's mastery involves staying calm, regardless of the situation. She should also be honest and clear in her communication. And, most importantly, she should listen more than she speaks. This last one may need special attention, because most people tend to be _conversation stealers_, interrupting others with their own stories.

To overcome this, try honoring a person's story and only offering your own once they've finished. This is a great skill; it will improve your communication with others, as well as your relationships in general.

### 4. Mensch leaders create a creative atmosphere through their mood, generosity and appreciation. 

Now that you've got a picture of the roots and the trunk, it's time to learn about the branches of a Mensch leader. The first one is a leader's mood, which sets the tone for the organization. That's why a Mensch is acutely aware of how his mood affects the productivity and engagement of those around him.

For instance, leadership literature is loaded with studies that show how leaders set the office environment. These studies explain that more energetic and enthusiastic leaders make for more cooperative teams that work together more effectively.

Just consider Whole Foods, a company that truly values a positive workplace environment. Through the positive attitudes of their leaders, they produce comfortable customer relationships that lead to repeat visits.

But they also maintain complete transparency in regard to salaries, allowing everyone to see what their bosses take home. Naturally, this honesty produces an atmosphere of trust.

The second branch of the Mensch leader tree is generosity toward those under one's leadership — and that doesn't just mean being generous with money. Generosity means many things, like giving someone a second chance or making people feel that their role, however small, is important.

For instance, during staff interviews at a US cancer hospital, even the janitor reported that her role was to cure cancer. Somehow, the leader had made every single employee feel that their role was essential.

And finally, Mensch leaders have confidence in themselves, which leads to _self-efficacy_. Self-efficacy is defined by the psychologist Albert Bandura as a belief in one's ability to perform efficiently. It's high self-efficacy that gives people the confidence they need to seek challenges and face failure.

So, the higher your self-efficacy, the more courageous you'll be to seek out challenges and persist through failure. This is huge, because people who think like this tend to be healthier and more successful at their jobs.

### 5. Take time for introspection and confidence building, and be sure to compliment your employees. 

Now that you know what makes a Mensch, it's time to ask yourself a few questions to see where you stand. Before sharing your gifts and knowledge with others, it's essential to unravel the Mensch hidden within you.

Here's how:

At the end of the day, just before heading home, take a few minutes to reflect on any significant conversations you had or emails you sent. Do you feel proud? Can you do better?

Allowing for such introspection will help you improve both yourself and the way you communicate with others. And self-improvement is central to leadership. After all, you can't make people feel confident in their abilities until you're confident in your power.

Therefore, learning self-confidence is essential. You can do this by spending time with confident people, paying attention to their habits and the way they think. Or, if that's insufficient, seeing a therapist is also a fantastic way to get to the root of the issue and establish a new, more confident foundation.

Once your self-confidence is in good shape, it's important to remember that the way you treat others will affect the success of your company. You shouldn't be afraid to show people that you appreciate them. After all, expressing your appreciation can never do harm, but it _will_ encourage employees to do their best and make them feel cared for and important.

So, make a habit of complimenting people's work in front of them. It's a gift to those you lead, and it will boost their self-esteem as well as their work ethic.

### 6. Final summary 

The key message in this book:

**At the core of any successful leader is the desire to be helpful and caring in the most sincere way possible. That means, as a leader, it's your responsibility to give people the tools, support and feedback they need to excel. First, however, you've got to develop your inner Mensch, firmly planting your roots, establishing your trunk and letting your branches grow.**

Actionable advice:

**Be generous with praise.**

The next time you want to admire someone's work, be generous with your words. Say, "I couldn't have done it without you!" instead of saying "I appreciated your help." Generosity engenders generosity. If you give praise, others will continue to give their best.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Leaders Eat Last_** **by Simon Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.
---

### Bruna Martinuzzi

Bruna Martinuzzi is the author of _Presenting with Credibility: Practical Tools and Techniques for Effective Presentations_, and a founder of Clarion Enterprises Ltd., an organization that, through coaching and trainings, has helped hundreds of individuals improve their leadership skills.

