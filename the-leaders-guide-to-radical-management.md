---
id: 59ec853bb238e1000645541f
slug: the-leaders-guide-to-radical-management-en
published_date: 2017-10-27T00:00:00.000+00:00
author: Stephen Denning
title: The Leader's Guide to Radical Management
subtitle: Reinventing the Workplace for the 21st Century
main_color: CD3E55
text_color: 992E3F
---

# The Leader's Guide to Radical Management

_Reinventing the Workplace for the 21st Century_

**Stephen Denning**

_The Leader's Guide to Radical Management_ (2010) offers seven key principles that will help managers stay focused on making their customers happy. You'll find that the practical tools presented in these blinks will not only increase your profits; they'll keep you and your workforce focused on what's really important.

---
### 1. What’s in it for me? Find a new, radical way to manage your business. 

Most leaders constantly think about how best to manage their teams, departments or businesses. The conventional way can be crudely described as a top-down hierarchy, where the main purpose is to maximize profits. But in the new economy, this traditional rigidity is not enough; today, we need a new type of management: _radical management._

Radical management focuses on customer delight and innovation. It's based on seven principles that will get your team to focus on your customers. This is a step-by-step guide on how to apply these principles, abandon the ossified approach of traditional management and adopt radical management instead.

You will also find out

  * why your Net Promoter Score is crucial;

  * how self-organizing teams drive diversity and innovation; and

  * why leading transparently is key to radical management.

### 2. Every business should seek to delight its clients and turn them into promoters. 

If you want to practice radical management, you need to start by asking an important question: "What am I trying to accomplish?"

The answer to that question, and the guiding principle for your business, should be _client delight_.

If you can keep your clients delighted, you're bound to have a successful business, with loyal customers who'll provide you with steady profits. This money can then be used to fund innovations for maintaining that delight.

And you should not only inspire delight in your clients; it should be used to motivate and inspire your employees as well.

Instead of just working for a paycheck, employees should recognize the value in helping others and putting a smile on a customer's face. Client delight doesn't just make a customer's day better. It also makes the lives of employees happier and more fulfilled.

To measure client delight, you can reach out to customers directly with surveys or questionnaires that seek to determine their willingness to recommend your service to others.

Many businesses, including the Boston-based consultancy firm Bain & Company, have found that a willingness to recommend is one of the best indicators of client delight.

Fred Reichheld is a manager at Bain & Company, and his surveys use what's called a _Net Promoter Score_ (NPS).

This tool works by asking customers to use a scale of zero to ten to rate the likelihood of their recommending your product or service to other people. A very high score like nine or ten indicates the client will act as a _promoter_ of your business, while a low score of zero to six suggests they'll be a _detractor_ — someone who will speak negatively about your business to others.

By using regular NPS surveys you can be sure you're as effective as possible by targeting your detractors and putting your efforts into turning them into promoters.

But facilitating this metamorphosis requires that you know how to delight your clients, which is what we'll explore in the blinks ahead.

### 3. To solve the complex problem of delighting clients, you need diverse, self-organizing teams. 

So how can you make sure your customers are delighted?

Well, unfortunately, there isn't one simple answer to this question. But there _are_ steps you can take to ensure you're doing everything you can.

This brings us to the second principle of radical management: _use self-organizing teams_.

Delighting your clients is a perfect example of a complex problem. Solving such a problem requires the right kind of team — a collection of diverse individuals that can organize and be responsible for their work.

Diversity means an eclectic mix of team members. You want people with expertise in varying fields, people who won't all approach the problem the same way.

A self-organizing team will have no controlling leader. Everyone should be equally responsible for coming up with the solutions for the complex problem or challenge at hand.

In a sense, a self-organizing team is sort of like a jury. Good juries are designed to represent the full spectrum of a community, with different ages, cultures, religions and classes all present. These different people are asked to put their heads together and come up with an answer to a complex problem.

Diverse groups work better than like-minded ones because when everyone has similar expertise and a similar background, they tend to get stuck and unable to move forward. When a certain problem stumps one person in a homogenous group, it'll likely stump everyone else. But the more diverse a group is, the better the chance that someone will know the answer to any question that may arise.

This was the essential idea behind the 2007 book _The Difference_, written by University of Michigan professor Scott E. Page, who specializes in complex systems. Page gives plenty of examples illustrating that a diverse, self-organizing team is better at solving complex problems than any group of like-minded experts.

### 4. With client-driven iterations, you’ll please both clients and employees. 

No one wants to sell a product that customers have no interest in buying. But, nevertheless, there's no shortage of businesses that continue to get stuck with massive and costly inventories of unsold products.

This is what the third principle of radical management seeks to avoid by using _client-driven iterations_.

When your business is focused on client-driven iterations, it only produces things that customers want, when they want it. And with the use of customer feedback and a trial-and-error approach, you can continuously improve your product or service through multiple iterations to make sure you're meeting the client's needs.

This process ensures your clients will be delighted, and you won't be stuck with warehouses full of useless products.

The company Quadrant Homes attained success by becoming more client-driven.

In the 1990s, Quadrant was buying up pieces of land on which to immediately build pre-designed houses. The company simply used their best guess to determine what customers might want from these houses, and unsurprisingly, was left with far too many unsold houses.

Clearly, something had to change. By working through iterations, Quadrant began focusing on what clients actually wanted and selling houses _before_ building them.

Clients can now choose their location and lot, and then select from a variety of combinations to customize everything from floor plan to exterior. Quadrant can then make the appropriate adjustments and get to work.

This process has made Quadrant an efficient and successful business. By letting the client decide both what and when it should produce, costs have plummeted and client delight has soared.

But this process doesn't delight clients only; it also affords employees more enjoyment at work.

In many businesses, employees are removed from the results and unable to see how their everyday work directly contributes to customer satisfaction. But client-driven iterations allow employees to maintain a relationship with customers from beginning to end.

They actually get to see the link between their work and the satisfied clients, making their job significantly more rewarding.

### 5. Create a smooth workflow by creating value every step of the way. 

If you have a daily commute and drive to work every day, you know how frustrating it can be when too many cars come together at once and create a traffic jam.

But the same thing can happen at work. When you take on too many projects and responsibilities, you can end up stuck, and, as a result, both your productivity and your clients can begin to suffer.

This is exactly what began happening to cancer patients at a certain medical center offering chemotherapy.

Every day, patients would show up bright and early to get a blood test and speak with an oncologist who would check their condition and make sure they were fit for treatment.

Once everything was in order, the oncologist would send the patient to the part of the facility where the chemo takes place. Since chemotherapy can be quite an ordeal for patients, most oncologists would schedule the treatment as soon as possible so that patients could get on with their day.

All of this was done with the best of intentions, but, in reality, it didn't work out so well. Patients often ended up in line, waiting for hours for their treatment. Employees also struggled with this system, since it forced them to run around frenziedly as they tried to tend to every patient at once.

A better system, and one that doesn't overflow and jam up the works, is one that follows the fourth principle of radical management: _deliver value to your clients in every iteration_.

A great way to do this is to analyze every step in the process to find ways of creating value, whether it's reducing wait times, improving customer service or making it easier for people to conduct a transaction.

At the medical center, when they analyzed the problem, it became clear they were trying to do too much in the morning and that the afternoons were relatively quiet.

So oncologists began to spread out the appointments more evenly throughout the day, reducing both the waiting time and the stress that employees had to cope with.

This both saved the medical center money and obviated the need to hire extra help to deal with the morning rush.

### 6. Spot and address problems by running a transparent organization. 

You may have noticed that traditional management sometimes refuses to acknowledge a problem when it exists. And this lack of honesty and openness often puts it at odds with problem-solving.

Managers deny problems to maintain power and keep up the appearance that they're in complete control. But it also means that employees are less likely to report real problems when they arise.

In an environment of avoidance, a business is unlikely to solve a complex problem, such as the problem of ensuring that clients are delighted. To tackle this challenge, everyone needs to feel free to openly and honestly discuss what is really happening. And this includes pointing out problems and figuring out how to solve them.

This is why the fifth principle of radical management is _transparency_.

To really see how damaging a lack of transparency can be, let's examine Robert McNamara's time as president of the World Bank, from 1968 to 1981.

During this time, McNamara expanded the World Bank's role in developing agriculture and helping alleviate poverty. This meant drastically increasing the amount of lending that was going on, which had many people worried that McNamara was financing low-quality deals that would not really help the lender countries.

To these accusations, McNamara said, "Absolutely not!" In his opinion, only a fool would deny that the World Bank simply had to expand on lending.

Of course, later analysis would show that the loans were troublesome and put many developing countries into a damaging state of debt. But his refusal to acknowledge the issue made it impossible for staff to fix the problem. Not only did McNamara's brand of management obscure the truth; his denial of the problem made it unlikely that anyone in his staff would try to fix what was wrong.

This is the situation that radical transparency seeks to avoid. By being honest and clear, problems can be identified and dealt with the moment they appear.

### 7. You must be on continuous lookout for self-improvement opportunities. 

What would you do if you noticed a coworker was scamming company clients? Are you the kind of person who would stay quiet and not create an incident, or would you report it?

For a successful business, it's important for management to create an environment where employees feel safe and comfortable enough to report a problem. In such an environment, it's harder for minor issues to grow into massive problems, and it's easier for employees to constantly be on the lookout for ways to improve.

Often, a problem in one area can affect everyone. And this is why Japanese engineer Taiichi Ohno invented a problem-prevention system for the Toyota car company in 1955.

Ohno installed pullcords — called _andon_ cords — near factory workers along the production line so that if anyone saw a problem they could pull the cord, ceasing production so that everyone could focus on solving the issue.

It's also important to recognize problems as opportunities to make improvements, which leads us to the sixth principle of radical management: _continuous self-improvement_.

To keep self-improvement in mind, you should focus on two things: _the goal_ — to always be improving — and _the means_ to reach that goal, which is a dedicated workforce.

Within this workforce, employees should be encouraged to report any problems they find, at the moment they're found. They should also feel empowered to come up with solutions.

This transparent and empowered system is proven to bring down costs, inspire innovation and, ultimately, bring about more client delight.

At Toyota, they've been working with a system of self-improvement for decades now, and it's been key to delighting clients worldwide. It's part of their ability to keep their prices low, their quality high and their problems at an absolute minimum.

### 8. With interactive communication, you can juggle all your tasks and be an effective manager. 

If you're a manager, it's likely you've felt overwhelmed at one point or another. And who can blame you! After all, managers are expected to juggle three big responsibilities all at once.

First, managers need to make sure everyone's getting adequately compensated for their work.

Second, they need to accept their position of authority and answer the inevitable questions their employees will have.

And third, managers need to do all of this while keeping people happy and motivated.

This brings us to the seventh and final principle of radical management: _interactive communication._

To juggle these responsibilities, managers should be good communicators who are socially comfortable and know how to react to a variety of situations.

If a manager is only concerned with making sure that employees are adequately compensated, the staff is bound to feel like they're nothing more than hired help.

Likewise, if a manager is only concerned with being authoritative, the employees will feel that they're being bossed around by a dictator.

So, to motivate employees and make sure they're putting in the kind of engaged and productive work that will lead to client delight, a radical manager needs to know how to be socially mindful and give employees the attention they need.

Truly interactive communication is about being respectful, genuine and keeping a two-way path of communication open between you and your staff. This will help you to keep a healthy balance of authority and motivation going, as well as get you through those tough salary negotiations successfully.

Now that you know the keys to radical management, it's time to put them into action! Before long, you'll start seeing your customer satisfaction numbers going up, and your business will reap the rewards.

### 9. Final summary 

The key message in this book:

**There is another way to organize your business than along the traditional, outdated lines most companies use. With seven principles:** ** _delighting clients, self-organizing teams, client-driven iterations, delivering value to clients in each iteration, radical transparency, continuous self-improvement,_** **and** ** _interactive communication_** **you can create an organization that keeps innovating and delighting both employees and clients.**

Actionable advice:

**Make sure your teams work with purpose.**

When setting up self-organizing teams, make sure they work for a purpose. Often, teams don't feel very motivated if they merely work toward a bureaucratic goal, like increasing productivity. Instead, you should share with them a gripping purpose, like making a difference in your clients' lives. This will make the work far more engaging for them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Emotional Intelligence for Project Managers_** **by Anthony Mersino**

_Emotional Intelligence for Project Managers_ (2007) builds on the writings of Daniel Goleman, who developed principles of _emotional intelligence_. Emotional intelligence is crucial in many aspects of life, and in these blinks Anthony Mersino tailors Goleman's principles specifically to the needs of project managers.
---

### Stephen Denning

Stephen Denning is an Australian business guru who's consulted for organizations around the world. He's a specialist in management innovation and helping businesses find their own path to success. His other books include _The Secret Language of Leadership: How Leaders Inspire Action Through Narratives_ and _The Leader's Guide to Storytelling: Mastering the Art and Discipline of Business Narrative_.

© Stephen Denning: The Leader's Guide to Radical Management copyright 2010, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

