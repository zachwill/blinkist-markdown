---
id: 53fd9dac37626200082a0000
slug: the-leadership-challenge-en
published_date: 2014-08-26T00:00:00.000+00:00
author: James Kouzes and Barry Posner
title: The Leadership Challenge
subtitle: How to Make Extraordinary Things Happen in Organizations
main_color: 2183A6
text_color: 286880
---

# The Leadership Challenge

_How to Make Extraordinary Things Happen in Organizations_

**James Kouzes and Barry Posner**

In _The_ _Leadership_ _Challenge_, James Kouzes and Barry Posner explain how anyone can become a better leader. Citing various examples from their 25 years of experience and extensive research, the authors present their theories on what makes a successful leader, and give practical advice on how to learn good leadership behavior.

---
### 1. What’s in it for me? Learn how to be the best leader you can be. 

At some point or another in our lives, most of us have had a superior who was a terrible leader: a tennis coach who always yelled at you when you didn't win or a boss who used to pass off your good work as his own.

What was it that made these people such bad leaders? Was it simply their character? To put it bluntly, no. They simply didn't stick to the important rules of good leadership behavior.

_The_ _Leadership_ _Challenge_ tackles exactly this problem by asking the following question: What do you have to do to be a good leader? After all, good leadership can tremendously boost team performance and impact team members' behavior for the better. When it comes down to it, we all learn from watching the people we admire, so leaders should serve as good role models.

In these blinks, you'll learn

  * how to lead by example;

  * how to throw your team a party;

  * why just asking your team what their favorite dish is can improve performance; and

  * how one garbage pail transformed an entire company's culture.

> _"Leaders are dreamers. Leaders are idealists. Leaders are possibility thinkers."_

### 2. Everybody’s watching you: leaders should be conscious of the influence their behavior has on others. 

When you think about the most important role models in your life, different people might spring to mind: your grandfather, a professor, etc. And when it comes to leadership, role models are equally diverse.

Leadership role models are not necessarily businessmen in a suit and tie. Theoretically, anyone could be a leadership role model.

Although it seems reasonable to assume that celebrities or sportsmen are the most likely candidates to be teenagers' leadership role models, it's actually not true. It turns out that the people we have direct contact with have a much stronger influence than strangers. Research has shown that family members are the most important leadership role models. Teachers, coaches and business leaders also rank high.

So since there's a pretty good chance we'll be a leadership role model for someone sooner or later, we'd all better start learning how to be good leaders.

But what makes a good leader? Leadership isn't an inherent part of our character but something defined by our behavior. And, as the authors argue, anyone can learn to be a good leader if they're driven to become one and have a lot of patience.

Researchers from Florida State University found that, in general, the best performance in all fields wasn't due to extraordinary intelligence or talent alone, but to effort and continuous practice. And this also holds true for leadership.

### 3. Avoid flip-flopping: leaders should be clear about their own and the team’s shared values. 

Have you ever known people who constantly change their opinion depending on who's around? Chances are you don't feel comfortable placing a whole lot of trust in them.

As a leader, it's critical to be clear about your opinions. Doing so inspires trust and increases good work performance in teams.

But before openly defining the team's and their own values, leaders need to know exactly what those personal and professional values are.

Our own values affect everything we do. For example, if a leader values an obedient staff over a staff that takes the initiative without his permission, it will affect the way the staff acts.

According to studies, leaders are more motivated and productive when they define their own personal values. So at this point, you need to define your own values. One way of doing so is to examine your past and ask yourself which values have influenced your decision-making processes.

At this point, leaders should be clear about their own and the team's shared values. They should ensure that everyone's values are talked about openly. That way, the team can reach a certain level of agreement.

What's more, research shows that, if team members' values match the firm's, commitment to the firm will increase. One General Electric (GE) employee proved just that.

Hilary Hall was part of a very international team at GE. Her manager asked staff to fill out forms with personal questions about things like their hobbies and favorite dishes. Afterward, they all talked about their answers. With more knowledge about each other, they established a certain level of trust. And by learning more about the other team members, they were able to connect and align their individual values. This helped them to start working toward shared goals, and eventually resulted in higher team performance.

### 4. Show, don’t tell: exemplary behavior is a more effective leadership style than just giving orders. 

If your boss always comes to the office in shorts and a T-shirt but demands you wear a suit, you might be reluctant to follow his orders. Conversely, you'd probably feel out of place wearing a T-shirt and shorts when your boss dresses formally to make a better impression on clients.

If you're gonna talk the talk, you gotta walk the walk. That means leaders need to make sure that they themselves act according to their team's values.

Steve Skarke makes for a great example. When he became plant manager of Kaneka Texas, a polymer manufacturer, he realized that the company's goal of becoming a "world class plant" was not reflected in their housekeeping: there was garbage everywhere.

What did Skarke do? He bought a garbage pail and, without comment, started collecting garbage in the plant every day. After just a few weeks, he noticed one day that there wasn't enough garbage around to fill his pail. What changed? The employees had learned from his behavior: they realized that in order for their plant to be "world class," it had to be clean, and started cleaning up themselves.

Since no single person can do everything, the best leaders encourage others to behave in line with established values. They need to make everyone feel responsible for teaching others to live the shared values.

Telling stories is one effective method of doing so, because people can relate to stories and are likely to retell and spread them. Phillip Kane, president of Wingfoot Commercial Tire Systems, would send a regular letter to his 25,000 employees with stories about the most important lessons learned in the company each week. By talking about the stories, and reciting them to others, Kane's staff indirectly started educating others.

> _"Leading by example is more effective than leading by command."_

### 5. Use your imagination: leaders need to picture possibilities in order to create remarkable outcomes. 

It's always easier to work toward a future goal if you can picture it clearly. So if you want to lead successfully, you have to be able to think ahead about future possibilities.

Albert Einstein himself said that intelligence is less important than having a vivid imagination.

Research shows that 75 percent of people believe that leaders must be forward-thinking, whereas only 30 percent of people say the same for their colleagues. In fact, it's the ability to picture what the future might hold that's the key trait distinguishing the people who get perceived as leaders from everybody else.

To draw a comparison to the chess world, leaders need to think like a grand master. A grand master doesn't just contemplate his next move but a series of possible next moves and countermoves: he must envision where his opponent might put each of his pieces and act preemptively.

However, unlike Bobby Fischer, leaders have to think about the rest of their teams, too. It's important that the aim of a team is not just the leader's vision, but a collective aim.

Professor Henry Mintzberg of McGill University found in his studies that it's important to feel like we belong to something — be it our job or a personal cause. When people feel they have contributed to setting a collective goal, it becomes a cause or commitment for them.

Good leaders support this behavior by collecting feedback, truly listening to team members' opinions and involving everyone in setting goals. This is especially important when times are tough: even if we feel like we want to give up, a fight for a personal cause can give us new motivation.

It's like driving: when visibility is poor, we tend to drive more slowly, because we get insecure and can't anticipate what's ahead. But if we can envision a goal that is important to us, we'll keep going through the fog to reach it.

> _"Every organization, every social movement, begins with a dream. A dream, or vision, is the force that creates the future."_

### 6. Get everybody on board: recruit others to take part in your collective vision. 

Leaders need to motivate and inspire their team with common ideals that everyone will support, like the pursuit of happiness, freedom, world peace or even just more confidence and self-respect.

Certain ideals make us feel like we're doing something meaningful. When we recognize that our actions matter, we feel proud of our work. Good leaders inspire these ideals and pride in their team members.

If you can communicate your own convictions to your team it can be extremely effective, as demonstrated by Nancy Sullivan, vice president of disability benefits at the Trustmark Companies. When her team was struggling, Sullivan wrote a four-page letter explaining why their work mattered and why they needed to reach their goals, and hung it up in the office kitchen where everyone could read it.

This action renewed the team's sense of purpose. Once they were reminded of exactly what they were working for, they ended up not only meeting but exceeding their business goals.

Enthusiasm is also a key factor. And there are techniques for creating an emotional connection between team members and the collective vision. One such powerful tool is the use of symbols and figures of speech.

When Janet MacIntyre started her job as unit leader of the intensive care and cardiac unit at a Canadian hospital where new machines and equipment had just been installed, she knew that most of her staff would need to learn to use the new equipment from scratch.

She therefore used symbolic language to unite her staff. She created a logo, a slogan and a mascot that related to Canadian culture and symbolized how the team had embarked on a journey. Moreover, she introduced a "passport," which included maps and all the information necessary for staff to find their way around the new unit, plus a personal checklist for everyone to track what they learned.

The team could truly experience what being on a journey meant, and it helped them adapt to the new technology.

Now we've seen several measures leaders can take to ensure that their team follows them. But merely following someone who leads without a specific purpose is not enough to create extraordinary outcomes.

Leaders also need to inspire their team to follow a mutual purpose — and in the following blinks you'll find out how.

### 7. Pave your own way: good leaders need to actively look for challenges and opportunities. 

While it's only natural to want to stay within our comfort zone, sometimes we have to venture out into the unknown if we want to be a successful leader.

All the best leaders step forward to seize new opportunities and challenges. Frequently, things lag just because nobody thinks about the possibility of change. Someone needs to take the initiative to bring about positive change.

Research shows that people with proactive behavior are considered more effective leaders.

When Starbucks didn't want to invest in blenders, one store manager decided to buy her own blender to make and sell a drink she had created herself. The drink became more and more popular, and most of us have probably tried it at least once: the Frappuccino.

Once Starbucks recognized how good an idea the blenders were, it decided to invest and have made millions selling Frappuccinos.

Ideas can come from anywhere at any time, and the Frappuccino is just one piece of evidence for that. And since nobody wants to be caught off-guard by sudden innovations or market changes, leaders need to make a point of being aware of what's going on around them and to try to look beyond that.

There are a number of things leaders can do to gather ideas about what's going on in their external and internal environment. For example, if you work for a company that offers a professional service, you could visit the office of a company that offers the same service to get a new perspective.

You could also call someone who only used your company's service once, and ask him why he didn't come back again. Another option is to make mystery calls to your own company, i.e., to pretend you're a customer interested in the service, so you can see how phones are answered.

### 8. Small wins should be emphasized – even if you risk a big failure. 

Part of being a good leader is taking chances and keeping your team behind you when you take them.

But you can spend a long time taking big risks in pursuit of big achievements before you get concrete results, so it's important for leaders to emphasize small wins. When tasks feel achievable, we feel more certain and motivated when completing them.

Harvard Business School researchers found that even small successes can motivate people immensely. Take Don Bennett, the first single-leg amputee to climb Mount Rainier, which towers more than 14,000 feet above sea level. But he didn't accomplish this incredible feat by constantly envisioning himself at his goal, but by concentrating on each individual hop.

Leaders should support this kind of thinking: focus on hopping from small win to small win, not just on big, unreachable goals in the distance.

In addition, good leaders have to accept that making mistakes is natural, and to make sure they and their teams learn what can be learned from them. Innovation process researchers even go as far as to say that success usually isn't born of success but of failure — and that success actually often induces failure.

That's what Kelli Garvanian, a solution consultant at Emdeon, found out when her team's business results failed. In the beginning, she was very demanding and sometimes even shouted at her team, but eventually she asked for feedback, realizing that something had to change.

Subsequently, Garvanian made it a top priority to become more open to learning and to changing her own behavior. She kept her team's feedback in plain sight on her desk so she wouldn't forget it and, at the end of every meeting, she asked team members what they had learned from what had been discussed.

Finally, she was able to create a great climate for learning. Nobody had to be afraid of failure any more, resulting in better team performance.

### 9. Good teamwork and a supportive team climate should be high on any leader’s agenda. 

Sure, we all know that Rome wasn't built in a day. But it wasn't built by one person, either. So teams and leaders need to work together in order to achieve great things. And one essential factor for good teamwork is trust between team members and their leaders.

Research on trust and organizational performance agrees: in organizations where employees trust their leaders and organizations, profitability and performance are higher and people are more innovative.

One way to facilitate trust is to let your team members know that you trust them in return.

When Masood Fakharzadeh, a program manager at KLA-Tencor, was asked to lead an offshore product development team for the first time, he openly told the team that he needed their advice and feedback, showing them that he trusted their abilities. As a result, the team started to trust him and openly share information — and the project succeeded.

Not only should a team trust its leader, but leaders should also foster trust among team members. Only then will they feel like they're all in the same boat working toward a common goal.

One way to enhance team trust is to establish more face-to-face communication between team members. This is especially important in our globalized world, where teams aren't always working in the same office.

Wilson Chu, a program manager at RingCentral, proved just that when he asked his offshore development team to use their webcam during online meetings. Suddenly communication between the team members became more personal. And because they felt they knew each other better, they also trusted each other more and were less hesitant to express new ideas.

### 10. When leaders empower their team members, the result is outstanding achievement. 

Imagine you had a colleague or boss who was always taking credit for your good work. You'd probably feel less motivated to continue striving for good results because it'd make you feel weak and insignificant.

Leaders should instill in their team members a sense of ownership of their own work.

Research has shown that people who feel powerless or exposed to uncontrollable circumstances perform worse than those who don't. So by trusting and empowering their staff, leaders can facilitate a boost in performance.

The management of Aruba Networks, for instance, placed a huge amount of faith in its employees when it completely did away with its vacation policy. Managers had noticed that the coordination of employees' vacation calendars was eating up unnecessary time and energy, so they decided to let everyone choose their own vacation times as long as it didn't interfere with their current work.

Ultimately, empowering staff with this freedom and trusting them to make the right choice greatly increased employee performance.

Apart from giving more power and freedom of choice to their teams, leaders should also simultaneously develop their team's confidence and competence.

Professor Mihaly Csikszentmihalyi of Claremont Graduate University found that we perform best in the face of challenges if we have the appropriate skill set. When we challenge ourselves just the right amount, we get the feeling of _being_ _in_ _the_ _flow_.

Contrarily, when we lack the skills to complete something, we get anxious, which inhibits us from performing well. To ensure team members' best performance, leaders should seek conditions where everyone is in the flow while working.

For instance, consultant Abhijit Chitnis's boss boosted his confidence by coaching him before his first client proposal presentation and by giving positive feedback during a break. Although he had made a small mistake, nobody noticed it over his great performance and the presentation was a full success.

### 11. Feedback and recognition of great work are some of the most powerful tools for motivating team members. 

As we have seen, giving staff the freedom to make their own choices is very important. But, as a leader, you should also make it your duty to acknowledge when your team members have done a good job.

Leaders can bring out the best in their team members and will motivate them more by communicating that they expect everyone to perform exceptionally. They should explain what their idea of an exceptional performance is and openly praise very good work, while staying positive and giving employees everything they need to complete their tasks.

And regular feedback is essential to making sure that team members know if they're on the right track.

When the high employee turnover rate was hurting his research and development (R&D) department, manager Harun Ö;zkara of HVAC Manufacturing introduced regular meetings every Friday, where he updated his team on what department managers thought could be improved in R&D.

Through these meetings, he clearly communicated that he expected improved performance from the team, and gave them feedback. Finally, Harun was able to actually decrease the high turnover and achieve better work from his team.

It's also equally important to personally acknowledge people for doing positive things.

It has been shown that impersonal acknowledgements, like sending an end-of-year bonus announcement by an automated email, have a much smaller effect than basic personal acknowledgements, like telling someone they did a good job.

Recognition is even more effective when it is unexpected, but well deserved.

In an interview with the authors, one woman recalled how thrilled she was when her CEO came from Singapore to Hong Kong just to thank her for a job well done. She said that this gesture of thankfulness motivated her more than any monetary reward.

### 12. Leaders should encourage their team to celebrate its own values and successes. 

People love celebrating. So why shouldn't leaders take advantage of that in the workplace? Company celebrations unite people around goals and values, and create a sense of community that leaders can foster.

Communities are important for us humans. Extensive research shows that when people have many active social connections, they're happier and wealthier, more trusting, and share more information.

Studies have also revealed that corporate celebrations make staff bond and socialize more and help foster a corporate community.

These celebrations don't have to be very big, as Kurt Richarz, executive vice president of sales at Seagate Technology, has demonstrated. Each month, he asks everyone on his staff to nominate another staff member for his or her contributions throughout the month via peer review. Then, during the monthly conference call with the whole sales organization, the top nominee's performance and photo are singled out and applauded. The enthusiasm everyone shows for this small monthly celebration has greatly increased the community spirit.

But to give organizational celebrations credibility, leaders need to be involved in them. As we've seen: trust and credibility are key factors for making your team successful. Just saying that you want people to celebrate the organization's wins and shared values isn't enough: leaders need to personally take part.

There are many things leaders can do to get involved. They can show their staff they truly care by going to meetings and organizational events, or even make gestures as small as saying hello to them in the hallway.

Granted, all of this has been a lot to take in. But if you as a leader incorporate these insights into your behavior step by step, you can be certain that you and your team will realize extraordinary achievements.

### 13. Final summary 

The key message in this book:

**The** **best** **leaders** **are** **aware** **that** **they** **need** **to** **develop** **a** **good,** **reciprocal** **relationship** **with** **their** **team** **members** **based** **on** **a** **mutual** **climate** **of** **trust** **–** **not** **by** **leading** **autocratically.** **They** **are** **not** **afraid** **to** **try** **new,** **unconventional** **things** **if** **they** **are** **good** **for** **the** **team** **and** **the** **team's** **success,** **and** **are** **always** **open** **to** **learning** **more** **and** **getting** **feedback** **to** **improve** **themselves.**

Actionable advice:

**Show** **up** **at** **your** **own** **party.**

Mark important events and milestones in your organizations' calendar as you would birthdays and holidays in your personal calendar. If you're celebrating something, make sure everyone involved knows about it far in advance. It's crucial that you take part in the celebrations personally. How else will you convey credibility and show that you really care?

**Suggested** **further** **reading:** **_Winning_** **by** **Jack** **Welch**

_Winning_ is a collection of no-nonsense advice and original thinking on successfully running a company, managing people and building a career. It answers the toughest questions people face both in and outside their professional lives.
---

### James Kouzes and Barry Posner

James Kouzes is the Dean's Executive Fellow of Leadership at Santa Clara University's Leavy School of Business and gives lectures and speeches on leadership worldwide. He has won many awards for his work, including the 2010 Thought Leadership Award.

Barry Posner is the Accolti Professor of Leadership at Santa Clara University's Leavy School of Business, where he served as dean from 1997 to 2000. He has been a visiting professor at several other universities worldwide and has received various awards for his work.

The authors have worked together for over 30 years, lecturing on and researching leadership.

James M. Kouzes and Barry Z. Posner: The Leadership Challenge] copyright 2012, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

