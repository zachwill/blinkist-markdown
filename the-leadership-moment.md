---
id: 593e64adb238e100051fdde4
slug: the-leadership-moment-en
published_date: 2017-06-15T00:00:00.000+00:00
author: Michael Useem
title: The Leadership Moment
subtitle: Nine True Stories of Triumph and Disaster and Their Lessons for Us All
main_color: C24542
text_color: 994341
---

# The Leadership Moment

_Nine True Stories of Triumph and Disaster and Their Lessons for Us All_

**Michael Useem**

In _The Leadership Moment_ (1999) we accompany leaders from a range of different fields on their incredible journeys, and learn, by witnessing their responses to critical challenges, some of the central principles of great leadership. From seeking out new opportunities to winning the trust of your team, these blinks are your guide to leading well in the trickiest of times.

---
### 1. What’s in it for me? Get inspired to take on leadership challenges. 

Think about your experiences at work or in the community: are there times when you or someone else had to take the lead in important decisions? Well, such leadership moments can teach us something important about how to act as leaders. By analyzing the experiences of other leaders in these moments and asking yourself what lessons they teach, you can better anticipate what to do when faced with similar challenges.

In these blinks, we look at nine situations in which one leader was faced with a difficult challenge. The stories are about business leaders, astronauts, leaders of NGOs, civil war generals and mountain climbers, and they all give us insight into how to tackle decision-making in this fluctuating world — insight we can use when facing our own leadership moments.

In these blinks, you'll learn

  * how a lack of action almost was the end of financial traders Salomon Inc;

  * That a lack of communication can be literally fatal; and

  * what an astronaut can teach us about quick and precise decision-making.

### 2. Seek out new challenges to grow your leadership skills. 

Are you always on the lookout for new challenges? That's a good thing. Taking on new responsibilities is usually stressful, and sometimes nightmarish! But it's a surefire way to gain new skills and experience, which, in turn, prepares you for even more challenges.

For Clifton Wharton, the former US Deputy Secretary of State, challenging assignments were an opportunity to sharpen his skills and enrich his practical knowledge. A development economist, Wharton faced his first landmark challenge when he was elected president of Michigan State University.

In the eight years Wharton spent there, he successfully led and administered the university in spite of its huge campus and vastly complicated systems. When invited to head the State University of New York, Wharton accepted.

The State University of New York had an administration about _ten times larger_ than that of Michigan State, and control was very centralized. But through his experiences at Michigan State and as a board member of large corporations, including Ford Motor Co., Wharton knew that even the largest, most resistant enterprises could be restructured. Again, Wharton was able to leverage lessons learned in previous challenges to solve another and eventually reshaped and improved New York State.

These experiences prepared Wharton for yet another huge challenge as he accepted a job offer from the Teachers Insurance and Annuities Association-College Retirement Equities Fund (TIAA-CREF), the biggest non-government retirement provider for college and university professors.

With products ranging from retirement planning to life insurance, the TIAA-CREF had a lot on its plate. Clients of the TIAA-CREF were losing patience with their restricted investment options and lack of control over their own retirement funds. When the organization offered Wharton a position, they were in dire need of a solution for their inflexible structure.

In 1987, Wharton took office and began modernizing the state system. He created three top management teams to steer the enterprise while introducing greater autonomy and accountability in customer service. Clients were given access to diverse investment options, greater flexibility in transferring their funds and improved counseling services.

You, too, can immerse yourself in your current role and all the learning opportunities it offers. These will give you the tools to succeed in positions of greater responsibility later on.

### 3. Understand and adhere to your vision and values. 

When on the hunt for a new job, which positions catch your eye? Probably those offered by companies that have goals similar to your own. Sharing a vision for the future with the organization you work for is also a prerequisite for great leadership, as is a deep understanding of your own values. This is something which Nancy Barry's career reflects.

As a senior official at the World Bank, Barry was beginning to doubt whether their aid projects for developing countries actually did that much good. So she envisioned an alternative: alleviating poverty and hardship through _microfinancing_, providing financial and educational services to low-income groups to help them provide for themselves. The World Bank didn't share this goal, but Women's World Banking (WWB) did, which is why Barry accepted their job offer.

The WWB offered women in developing countries the financial tools and resources to become self-sufficient, and needed Barry's expertise as much as she needed their platform to realize her vision.

During her time at the World Bank, Barry had developed a network with leading financiers, which helped her create valuable relationships with established institutions for the WWB. Barry's leadership experience drove the decentralization and expansion of the WWB, which in turn successfully helped microfinance reach millions of women in need.

Like Barry, Roy Vagelos was also driven by a clear vision — to cure _river blindness_, a waterborne parasitic infection found in Africa. Vagelos developed the drug _Mectizan_, a scientific breakthrough that both helped cure river blindness and prevent the spread of the disease. The cost of manufacturing Mectizan was high, but this didn't stop Vagelos. He stuck to his company's values which were to "put patients before profits."

### 4. Stay calm and focused to make the right choice, even when time is tight. 

Some leaders find themselves in life-and-death situations, where a failure to make the right choice fast can have devastating consequences. Eugene Kranz was one such leader. His brilliant leadership as the flight director of the Apollo 13 lunar mission shows that a decision made quickly needn't be a decision made poorly.

In 1970, the spacecraft Odyssey, in flight, suffered an explosion. This was nothing short of a catastrophe — air pressure, electricity and fuel cells were being lost at an alarming rate. The astronauts had to return to Earth as soon as possible. The situation called for rapid action; the team had mere minutes to dash from the command module to the lunar excursion module following the explosion.

In the mad rush, it was Kranz's job to uncover the facts of the situation and make the right calls. Any errors on his part would have been disastrous; a single blunder could have sent the spacecraft hurtling along the wrong path, never to return to Earth. And there was no time to waste, either. But by remaining composed and concentrated, Kranz gathered enough information to make the right decision at every step, successfully getting his team home.

Unlike Kranz, John Gutfreund's leadership career was marked by his failure to act in an urgent situation. In 1991, Gutfreund, chairman and CEO of investment bank Salomon Inc. became aware of their bond trader Paul Mozer's excessive bidding. While the US Treasury did not accept bids exceeding 35 percent of an auction total for a government bond, Mozer was bidding under different names on multiple occasions in order to exceed the legal percentage.

How did Gutfreund respond? Well, he didn't. Mozer was allowed to remain in the company, and his conduct was not investigated nor reported to authorities until three months later. This delay not only caused public outcry, but nearly proved fatal for the company. Salomon Inc. was forced to pay a massive $290 million settlement to federal regulators.

With their reputation tarnished permanently, their stock went into free fall. No companies were willing to work with Salomon after the scandal. This was a nightmare for Gutfreund, one that could have been avoided had he simply acted immediately.

Of course, acting fast means having a fast team. In the next blink, we'll investigate how leaders mobilize their teams.

### 5. Leadership without communication can have disastrous consequences. 

Back in 1949, Wagner Dodge was the chief of a 15-man crew of firefighters, working together for the very first time. One day, they were called to Mann Gulch, a rugged area of central Montana; a forest fire was burning. Dodge had vast experience dealing with forest fires. But it was a different story when it came to leading a crew.

A man of few words, Dodge rarely offered appraisals of the situation at hand, nor was he particularly adept at expressing and explaining his decisions. Unfortunately, Dodge's poor communication skills had fatal consequences at the Mann Gulch fire.

Just as Dodge and his crew arrived and were heading for the mouth of the gorge between the fire and the river, Dodge retreated to the landing zone without explanation. He had simply gone to collect some forgotten food, but the team had no idea why he'd suddenly left.

Dodge returned and they continued on, until they discovered that a new fire was quickly closing off their escape route. Dodge reversed his course and ordered his men to drop their equipment. He was alarmed about the growing fire and wanted his men to be able to get away from it as fast as possible. But, once again, Dodge failed to explain this to the crew.

The forest fire began to outrun them, and Dodge stopped to light a backfire to prevent the real fire from advancing. He took shelter behind it, and urged his crew to do the same. At this point, however, they weren't going to listen to Dodge anymore. His lack of communication meant that they doubted his ability and decided to go their own way.

At the moment when Dodge needed them to trust him, they abandoned him. Tragically, all 14 crew members lost their lives as a result. Here, the importance of explaining your decisions when leading your team hardly needs further emphasis.

### 6. Listen to your team and keep them involved in decision-making to win their support. 

It was in 1863 that Colonel Joshua Chamberlain, who commanded the 20th Regiment of Infantry, Maine Volunteers, during the American Civil War, defended and secured Little Round Top, a crucial strategic point in Gettysburg. This would not have been possible without his top-performing army.

Given that a third of Chamberlain's army were mutineers recently decommissioned from the 2nd Regiment of Maine, you wouldn't think he'd have a very cohesive force on his hands. And yet, Chamberlain was able to win full cooperation of these mutineers by giving them what they needed: someone to voice their grievances to.

He listened to their stories and showed his understanding. When he asked for their support in his upcoming clash with the Confederates, none of them hesitated. His compassion, as well as his powerful words about the Union's determination to set all people free, no matter their background, led the mutineers to follow him into battle to defeat the Confederates — one of many moments in history where gaining support has proved to be the deciding factor between victory and defeat.

Another way you can secure the support of your team is by reaching consensus with them before carrying out a decision. Arlene Blum knew this as she led a team of all-female mountaineers on a climb of Annapurna, in Nepal. The extreme conditions at the summit meant that only one attempt to reach it might be possible.

As they neared the peak, Blum consulted her team. It was clear to her that they simply couldn't all head up to the summit at once. They agreed together that safety should come first, and supported Blum's plan to have just four people in one group make the first attempt. Two women were accompanied by two local guides; they all reached the summit successfully. Though this meant that the rest of the team didn't get the chance to see the peak, it didn't matter.

Because they had reached a consensus beforehand, the team was happy to support those who did make the journey to the top. At the end of the day, powerful team feeling is the greatest accomplishment, and your supporters will cherish it.

### 7. Final summary 

The key message in this book:

**The stories of leaders contain powerful lessons about the importance of learning, vision, decision-making, communication and compassion. Leadership comprised of these factors will enable you to guide your team to a performance that will make everyone proud!**

Actionable advice:

**Think about what type of leader you are.**

There are many different leadership styles, though not all of them make for effective leadership in every situation. From autocratic leadership, where decisions are made fast without consultation, to democratic leadership, where opinion matters, to laissez-faire leadership, where team members themselves take the lead, it helps to know what leadership approaches are available to you. Before choosing yours, think about which one will work best in a given situation.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Business Adventures_** **by John Brooks**

_Business_ _Adventures_ comprises twelve riveting case studies of key developments in business, economics and finance. While they concern events and companies you may never have heard of, the case studies are highly entertaining and the lessons learned from them are still applicable today.
---

### Michael Useem

Michael Useem is a professor at the Wharton School of the University of Pennsylvania as well as the Director of the Center for Leadership and Change Management. Renowned for his expertise in the theory of leadership, Useem is also the author of _The Leader's Checklist_ and _The Go Point: When It's Time to Decide_.

