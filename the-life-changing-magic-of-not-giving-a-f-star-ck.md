---
id: 58b21329ae485a00045cfcee
slug: the-life-changing-magic-of-not-giving-a-f-star-ck-en
published_date: 2017-02-27T00:00:00.000+00:00
author: Sarah Knight
title: The Life-Changing Magic of Not Giving a F*ck
subtitle: How to Stop Spending Time You Don't Have with People You Don't Like Doing Things You Don't Want to Do
main_color: A4356A
text_color: A4356A
---

# The Life-Changing Magic of Not Giving a F*ck

_How to Stop Spending Time You Don't Have with People You Don't Like Doing Things You Don't Want to Do_

**Sarah Knight**

_The Life-Changing Magic of Not Giving a F*ck_ (2015) is your definitive guide to freeing up your time, money and energy. These blinks teach you how to stop caring about the things that don't serve you so you can focus your attention on the parts of life that bring you joy.

---
### 1. What’s in it for me? How to not give a fuck and feel damn good about it. 

You attend pointless meetings at work. You help your aunt clear out her basement. You go to your friend's boring board game "parties."

All these good deeds waste your precious time; and really, you couldn't give a fuck about them.

_Not giving a fuck_ means to stop spending time, energy and money on things that annoy you or make you unhappy. Importantly, when you stop giving a fuck, the things you do and the time you spend doing them will bring you true feelings of joy.

These blinks will show you how to stop giving a fuck without hurting other people's feelings. You'll learn practical methods on avoiding what makes you miserable and get clear on what to say "yes" to.

In these blinks, you'll also learn

  * what to do with unwanted gifts;

  * how to not donate to a friend's Kickstarter campaign; and

  * how to deal with the fucks you just can't stop giving.

### 2. Learning how to not give a fuck means being honest while respecting people’s feelings. 

As we get older, we tend to stop caring about certain things, like being in perfect shape, keeping up with reality TV shows or following celebrity gossip.

But there are other things that are harder to let go of. Most of us have things in our lives that we wish we didn't care so much about, but it's difficult to shake them loose.

To change this, you need to explore the _Not Sorry Method_.

This method has two main steps. The first is to decide that you won't give a fuck about a certain thing; the second is to actually not give a fuck about the thing. What's more, you should not feel sorry about your lack of interest!

For instance, let's say you hate attending baby showers. You receive your umpteenth invitation to a shower and this time, you decide you won't go. You don't give a fuck, so you make decisions accordingly!

Not giving a fuck doesn't mean you should disregard the feelings of others, but you should ignore their opinions. You can't control what other people think, and really, their opinions are the least of your worries.

Opinions don't matter; feelings do. Unless you want to be a jerk, you need to consider how other people feel and, while not caring about some things is great, you don't want your actions to wreck your relationships.

Let's say you detest the all-natural peanut butter that your friend makes and sells. She wants you to attend one of her peanut butter parties, but your impulse is to just tell her how much you hate the stuff.

Doing so, however, would hurt her feelings. The better option is to politely but honestly decline her invitation. You can let your friend know that you don't share her love for peanut butter in general, which is honest and a stance that won't necessarily burn any bridges.

So that's the basics behind the Not Sorry Method. Next you'll learn how to deploy it, step by step.

> "_It's time to flip the script, reverse the curse and stop giving all of your fucks to all the wrong things for all the wrong reasons."_

### 3. Categorization will help you separate essential concerns from inessential ones. 

Imagine a barn, but instead of bales of hay, it's filled to the rafters with crap. That's basically what your mind is like, filled with piles of waste. And it needs to be cleaned out.

This spring cleaning is essential to not giving a fuck, because before you can stop caring about things, you need to identify all the stuff you _do_ give a fuck about.

Most people's minds are filled with junk and noise, and identifying these pointless questions and thoughts is the first step to letting go.

So make a list of everything that takes up space in your mind. It will soon become clear how many of your thoughts are mere clutter.

Once you've made a list, consider which items you don't need to care about. For instance, maybe worrying constantly about getting a raise at work is unnecessary.

When you clean up your thoughts, you'll be able to focus on the things that truly matter.

Letting go of pesky things in your mind can be difficult, so to make this task easier, divide the items on your list into categories.

Let's start with a category for _Things_. This category can include anything from your Google Plus account to having a perfect bikini body. These things are inessential; nobody will come after you with a baseball bat for failing to give a fuck about them. In other words, you should be able to easily ignore the worries that fall into the Things category.

The next category is _Work_, which is a bit more complicated. After all, you might depend on the financial security of your job, and because of this, you probably want to pay attention to it.

But that doesn't mean you have to attend every boring, drawn-out meeting. It also doesn't mean you have to run the half-marathon Susan from Finance organized to "save the Arctic."

### 4. The difficulty of not caring about a person’s opinions rests on your relationship to that person. 

Let's practice the Not Sorry Method to perfect your ability of not giving a fuck. Here's how to not care when it comes to the needs and opinions of strangers, acquaintances and even family.

There's no question that not caring is easier when it comes to strangers than it is with friends or acquaintances. But you can do it.

It certainly is less stressful to turn down a stranger when a request doesn't serve your interests or needs. For instance, there's no reason for you to feel sorry about not donating to a stranger's Kickstarter campaign, especially if you think the idea is bogus.

If a similar campaign was being run by a friend, however, the calculation is more complicated. If donating money doesn't bring you joy but you don't want to insult a friend, you might tell him that you have a personal policy of never donating to anything. Or you could say that you'd love to help, but you just don't have the money.

It's more difficult to not give a fuck about friends than strangers, but the most difficult is family. Here's the trick:

Say your mother wants to give you her set of china. You think the pattern is ugly and don't even like china in general, but feel like you have to accept the gift to spare her feelings and avoid the guilt of saying no.

In the end, you accept a gift you hate. Was that the best decision? Not really. Instead, you could have decided to not give a fuck while still being honest and polite.

Let's say you're eating dinner at your aunt's house. Someone raises the topic of the presidential election and you have to decide whether to respond to your aunt's conservative views, which you find distasteful.

The best strategy is not to jump down your aunt's throat but share with the table that you prefer to avoid politics, since your opinions differ from your host's. Acting in this fashion is both truthful and polite; and importantly, you've avoided hurting anyone's feelings.

Now you're ready for the next step: to not give a fuck for real.

> _"… all relationships are complicated, and sometimes friends get on friends' nerves."_

### 5. Start out with the fucks that are easiest not to give and then work your way up. 

When you start to think about it, you'll find that there are all kinds of things you don't care about.

So where should you start?

Start with the fucks you can easily afford to stop giving. These are things that don't affect other people and are thus easy to let go. What's more, you probably don't have to worry about being polite in the process.

For instance, if you're tired of people on Facebook posting pictures of toddlers and "amazing" meals, simply unfollow them. While your friends would know if you unfriended them, they likely won't know that you unfollowed them.

Now move on to the fucks that are a bit more challenging to stop giving. These are more complicated issues that affect at least one person.

Since ceasing to care about these things can affect another person, you might need to be polite in the process of stepping back from them.

Let's say your friend is moving this weekend and asks for your help, in exchange for the standard offering of beer and pizza. You can wiggle out of the commitment by saying you've got to work that Sunday. After all, how likely is it that he knows your work schedule?

Finally, move on to the hardest fucks to stop giving. These are items that not only affect others but also could trigger hurt feelings. To handle these, you'll need a couple of advanced tricks.

If you don't enjoy the company of children, it's not a good strategy to tell your friends flat out that you hate their offspring.

That said, it's important to be honest. You might say something like, "I'm not too keen on kids in general." You can add some politeness to this strategy by also throwing in a sugary comment about how cute your friend's daughter is, or "liking" a couple of their Facebook pictures.

### 6. Advance from beginner to expert level in the various categories of not giving a fuck. 

There are certain things that universally get on people's nerves, and these things fall into categories. Once you establish which things in your mind you can chuck and which category they fall under, you can advance from beginner to expert level in not giving a global fuck.

When it comes to Things, for instance, there are many items that irritate; one modern scourge is social media.

Social media is exhausting. Many online platforms are just full of attention-grabbing tripe, things that suck up your energy, time and even money.

How do you deal with it? Easy. Pick a social media platform and stop using it. From this step you can then move on to disabling _all_ of your social media accounts and refusing to even discuss posts with friends.

And as an expert in not giving a fuck about social media, you can even make comments to show how little you care about Facebook, Twitter or any other platform. You might ask people how they feel about "catfishing," or the practice of pretending to be someone you're not on social media.

When it comes to Work, you might care a whole lot in general, but want to stop giving a fuck about things like team-building exercises and unsolicited emails.

To handle this, you could use a vacation day to avoid a team-building boat trip, for example. You could even call in sick! And as a no-fucks expert, you could even take a perfectly timed personal day.

Emails can be addressed in a similar fashion. Just hit the "delete" button on a message you can't be bothered with. You could even delete the message _and_ block the sender. And at the expert level, you could install a program that sends the unsolicited emailer all manner of unwanted junk!

### 7. Be polite and honest when dealing with your social circle; but know that sometimes it’s not enough. 

Now that you know how to build your skills in not giving a fuck about Things and Work, let's look at how to manage strangers, acquaintances, and friends and family.

An event like a wedding offers a useful example here, as it's a place where you'll encounter all sorts of people. The main rule, as always, is if you don't want to give a fuck, then you shouldn't. But you still need to remain polite and honest in the process.

Say your friend is getting married on the very weekend you usually take your well-deserved annual vacation. How can you not give a fuck about the wedding and enjoy your trip instead?

You could ask your friend to change the date, or say that you just happen to have an appendectomy scheduled on the same day. However, the first option is impolite; the second is dishonest.

Instead, you should tell your friend that you're so happy to be invited but unfortunately, you can't make it since you've already planned a vacation for that same weekend, and there's no way to reschedule.

To make up for your absence, you could send your friend a lovely wedding gift.

This might work for friends, but when it comes to family, sometimes you have to give a fuck. When this is the case, you can at least throw in a bonus for yourself.

For example, let's say you're forced to attend a tedious family get-together. There are lots of reasons to fear such an event, from uncomfortable political discussions to dealing with your drunk uncle, but one thing is certain: you have to go.

Since sometimes you can't avoid the obligation, do something to counteract the pain later. You could schedule a manicure or massage for the following day, to give yourself something nice to look forward to. Doing so will balance out the fucks you give, and put you back on the path to not giving any!

### 8. Final summary 

The key message in this book:

**Everyday life forces us to care about things we don't really give a fuck about. But the truth is that you can ditch unwanted commitments without looking bad or hurting the feelings of friends and family. The solution is to follow the** ** _Not Sorry Method_** **.**

Actionable advice:

**Keep a budget of all fucks given.**

Just like you balance a checkbook, you need to balance the fucks you give and don't give. For instance, if you absolutely have to go to a baby shower that you'd rather miss, put it in your budget as a "fuck given." This means that in the future, you can balance your tally by not giving a fuck about something else, such as spending cash to take a taxi on a rainy day.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Life-Changing Magic of Tidying Up_** **by Marie Kondo**

_The Life-Changing Magic of Tidying Up_ isn't just a guide to decluttering, it's a best seller that's changed lives in Japan, Europe and the United States. The _Wall Street Journal_ even called Marie Kondo's Shinto-inspired "KonMari" technique "the cult of tidying up." Kondo explains in detail the many ways in which your living space affects all aspects of your life, and how you can ensure that each item in it has powerful personal significance. By following her simple yet resonant advice, you can move closer to achieving your dreams.
---

### Sarah Knight

Sarah Knight is an American freelance writer and editor. She holds a degree from Harvard University.

