---
id: 55103a1e666564000a940000
slug: the-life-changing-magic-of-tidying-up-en
published_date: 2015-03-27T00:00:00.000+00:00
author: Marie Kondo
title: The Life-Changing Magic of Tidying Up
subtitle: The Japanese Art of Decluttering and Organizing
main_color: D0353D
text_color: D0353D
---

# The Life-Changing Magic of Tidying Up

_The Japanese Art of Decluttering and Organizing_

**Marie Kondo**

_The Life-Changing Magic of Tidying Up_ isn't just a guide to decluttering, it's a best seller that's changed lives in Japan, Europe and the United States. The _Wall Street Journal_ even called Marie Kondo's Shinto-inspired "KonMari" technique "the cult of tidying up." Kondo explains in detail the many ways in which your living space affects all aspects of your life, and how you can ensure that each item in it has powerful personal significance. By following her simple yet resonant advice, you can move closer to achieving your dreams.

---
### 1. What’s in it for me? Discover the secrets to tidying up your house – and your life. 

What's the best way to tidy your house? How do you know when it's time to throw away that disco party jacket? What about the holiday postcards from Grandma? Behind these housework chores are deeper questions that only someone who's devoted half her professional life to tidying can answer. Marie Kondo also spent five years as an attendant at a Shinto shrine, learning what's really important in our chaotic modern world.

These blinks define declutter guru Marie Kondo's KonMari method, in which tidying is not just cleaning your house, but also a way to become a better decision maker, live more healthily, and reach your dreams.

Here's just one example of how the KonMari method changes the way you think about your stuff: When you buy new clothes, remove the packaging and tags right away and welcome them to your home. Treat them as part of your life, not as a product.

In these blinks, you'll find more on the KonMari method, including

  * why tidying is like meditation;

  * how to hang your clothes in a way that will make you feel lighter; and

  * why you only have to tidy your house once in a lifetime.

### 2. The first step to achieving your dream lifestyle is visualizing your dream life in your dream space. 

Before you can begin to tidy up, you first need a vision for your ideal life and your living space. Before reading further, take a moment to visualize what that would look like.

For example, when one of the author's clients, a woman in her twenties, did this exercise, she confided that she wanted a more "feminine" lifestyle. So she visualized arriving home to find a room free of clutter — as tidy as a hotel suite — with a pink bedspread and an antique lamp.

Before going to bed, she would take a relaxing bath and enjoy the scent of aromatic oils, listen to classical music while doing yoga and drinking herbal tea, and finally fall asleep feeling totally at ease and unhurried.

Once you've figured out what you want in your life, you're in a position to start _tidying_ and moving closer to your ideal life.

A tidy environment is one where you can efficiently access the things you need and like, which is why it's so important to first understand what it is that you want.

Your space should be filled with things that you truly love. Don't, for example, turn the clothes you don't like into your pajamas. Though it might feel pragmatic, it would be better for you to respect your belongings if they're going to reflect your vision.

Take your books, for example. By ridding your space of books you don't read and keeping the ones you are truly passionate about, you become more passionate about reading.

Don't be afraid to discard things. Even if you have to give away a dusty book that you really meant to read but just never found time for, these pangs of regret show you that the book is something you're truly interested in. Now's not the right time for the book, but you can always buy it again later.

> _"Order is dependent on the extremely personal values of how a person wants to be."_

### 3. A tidy home will improve both your body and mind. 

The purpose of tidying isn't just to keep things clean and organized. Rather, your goal is to create a space that improves your body and mind.

To do this, you should organize your living space in a way that feels most natural to you. When you're sorting through your things, for instance, you're also evaluating their purpose and usefulness to you. Letting go of something that no longer serves a purpose — and welcoming something new in its place — can be deeply therapeutic.

As you are sorting, focus on what you want to keep, not what you want to throw out. Arrange your belongings by category, closely examining each one with your eyes and hands. Ask yourself: "Does this make me happy? What is its purpose?"

For example, that loud jacket that was thrilling to buy but not to wear has already served its purpose, and now only collects dust. Let it go with gratitude, and move on.

In this way, tidying ceases to be a cold, calculated process, and instead both comes from and heightens your own awareness of self.

This is where the power of tidying lies, much like meditation. Its lasting effects come from doing what feels right for your environment, your belongings and, ultimately, your well-being.

In fact, you can picture tidying your living space as a means of detoxing your body and mind.

By discarding and re-organizing your belongings, you will effectively get rid of all the dust that has settled over time, thus bringing fresh air into your house.

Some clients noticed physical reactions that coincided with the process of tidying, as if their bodies and homes were expelling toxic materials together. For example, after cleaning her cupboard and shed for the first time in ten years, one client had to rush to the bathroom immediately to expel diarrhea.

> _"The whole point in both discarding and keeping things is to be happy."_

### 4. Put your past in order and gain clarity about the future you want. 

Have you ever gone through your yearly spring clean only to find something which means the world to you, that you'd forgotten you still had? Tidying is more than just keeping a clean house: in the process of tidying, you can use the stuff from your past to help guide you into the future.

Keepsakes from your past should bring you joy when you look at them. It's worth keeping the ones that capture happy or exciting memories.

When you begin the process of tidying, start with the easiest categories, e.g., clothes, books, documents, miscellaneous items, etc., and end with your sentimental items. Photos are the most difficult because of the sheer volume and emotional value. Be sure to keep the ones you remember taking and relive the excitement of that moment.

But what do you do with sentimental items that you no longer want? Too many people look to their parents' homes as a place to dump their junk. But it's wrong not to consider their feelings when getting rid of sentimental trinkets. Only give them things you think they will enjoy, and when discarding childhood keepsakes, ask your family whether they would rather have it.

As you're tidying, you should also ask yourself whether a particular item corresponds to your current wants and needs or your vision for your ideal future.

Documents, for instance, rarely succeed in doing this. Realistically assess their purpose according to your current needs. Warranties and manuals, for example, have a short-term purpose, and can therefore be discarded once they've outlived that use.

Old course materials have likewise outlived their usefulness. After all, you didn't take the course for the materials, but for the experience and the knowledge.

Going through your things, you may, as one of the author's clients discovered, rediscover old passions that bring you closer to your ideal life. After cleaning her bookshelf, she realized that the books that remained were mostly on social welfare. So she went back to school, quit her job in IT and started a successful babysitting company.

> _"Every object has a different role to play. Not all clothes have come to you to be worn threadbare."_

### 5. Create a comforting and invigorating environment by surrounding yourself with neatly organized things that make you happy. 

Though it might sound unbelievable, changing the way that you tidy your space can have profound effects, not just on your happiness, but also on your very ability to take action. For many, tidying is the first step toward grasping control of their lives.

Strive for simplicity and visual order when storing and organizing. Consider carefully how you feel whenever you use or look at your belongings in the space you've designated for them.

Your wardrobe is a good place to start. Try organizing your clothes in a way that is visually appealing to you. For example, you could arrange your clothes by fabric, then size and color. Or you could hang them by size, starting with the longest on one side and the shortest on the other. The shape that this creates is visually comforting, and will make you feel lighter.

As you tidy more of your space, you will discover that organization doesn't just affect your disposition. In fact, it can also give you the power to make decisions and act on them.

Some of the author's clients believed that they were born messy, something they'd never be able to change if they wanted to keep a tidy house.

But the author doesn't buy it. Instead, she advises them to abandon this negative self-perception, and instead to strive for perfection when visualizing their tidy living space.

Once they've achieved this vision, they feel like they can do anything if they just put their mind to it!

More practically, tidying makes your decisions around the house straightforward, and thus improves your decisiveness. Efficient and intuitive storage eliminates the stress of having to search through clutter for the things that you need. It allows you to make decisions instead of being stuck searching helplessly for hours.

### 6. You only have to tidy once to make a lasting change in your life. 

Many people find the idea of maintaining a tidy home daunting. After all, doesn't it seem like a lifetime of effort? But they're simply thinking about it the wrong way. Rather, if you organize your space so that it embodies your dream lifestyle, then you only ever have to do a comprehensive tidying once in your life.

You can make this seemingly daunting task easier by turning it into a special event. This breakthrough tidying will serve as a crucial and sudden change in your life — a fresh start on the path to your ideal life.

The process of tidying is exhaustive, and you should expect to spend a good deal of time on this endeavor. In fact, it takes the author an average of six months to tidy a client's house.

And as you are carrying out this project, it is important that you approach your space and your belongings with the respect that they deserve. Communicate with your house and your possessions; if you show them that you care, they will respond.

While you're tidying, communicate with your possessions and your space. In doing so, you can clarify your relationship to your belongings, thereby gaining a more intuitive feeling of what is of use to you and what is ready to be discarded. It makes the project smoother and more natural.

When the author works in a client's home, she begins by kneeling on the floor out of respect, and offers the space a silent greeting, as if she were in a shrine. Even her clothes, usually a dress and a blazer, embody the respect that she shows for the space.

Though exhaustive tidying is a one-time project, your day-to-day relationship with your space is likewise important. When the author, for example, takes off her shoes when entering her home, she thanks them for their work. She greets and thanks her house every day, and empties the contents of her bag completely, putting each item in a specific place.

### 7. Though it may be hard, you’re going to have to let some of your belongings go. 

It's now time to approach the most difficult part of the tidying process: throwing things out. It can be difficult to part with your belongings, even the ones for which you have no need. Luckily, there are some tricks you can use to make it easier.

Start by determining the purpose of the object and decide whether that purpose has yet been fulfilled. If you hesitate in your answer, ask yourself: Why did I get this thing? When? And how?

Miscellaneous belongings that you keep "just because" are common causes of clutter and chaos. For these items, it's best to start with personal, clearly defined categories, such as CDs and DVDs, skin care products, accessories, etc., and work your way to more general things, like household equipment and supplies, kitchen goods and the like.

Be clear about the purpose of each item, and be willing to get rid of things that have outlived that purpose. For instance, the purpose of a novel you only half-read was not to finish it, but rather to experience the story. If you haven't finished it, you probably won't. Be grateful for it and let it go.

Sometimes intuition and reason will be at odds with one another, especially if you are emotionally attached to certain items.

In these cases, consider carefully the ways in which this item contributes to your life. Does it make you happy when you see it? Do you even see it at all? If it has been stored away for a long time, chances are that it's less important to you than you think.

Gifts and greeting cards also have a purpose: they are gestures from friends and loved ones to show you they care. But once this message has been conveyed, isn't it time to let them go?

By cutting away the unnecessary, you'll be that much closer to creating your ideal space.

> _"The real problem is that we have far more than we need or want."_

### 8. Final summary 

The key message in this book:

**Decluttering your life should be extremely simple: all you have to do is decide what you want to keep and discard what you don't — i.e., everything that doesn't make you happy. Accomplishing this, however, requires you to dig deep to figure out what kind of life you want to live.** **It's up to you to develop the kind of space that reflects your ideal future.**

**What to read next:** ** _The Happiness Project_** **, by Gretchen Rubin**

As we've just found out, merely having it all — a good career, a nice home and money in the bank — doesn't translate into happiness. But aside from decluttering, what else can you do to find peace and joy in life? Gretchen Rubin is the person to ask.

In _The Happiness Project_, Rubin takes you along on her quest for joy. The principles and techniques she discovered along the way can help anyone build a life that's both less stressful and more fulfilling. To start changing your life for the better, and to learn the art of the "one-sentence diary," read our blinks to _The Happiness Projec_ t.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Marie Kondo

Marie Kondo has spent more than half her life helping people transform their cluttered homes into tidy reflections of their ideal lives. Her professional services and courses are so popular in Japan that people wait three months just to get an appointment. Her books have sold over two million copies and been the subject of a TV movie.

