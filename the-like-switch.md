---
id: 55062be3666365000a7a0000
slug: the-like-switch-en
published_date: 2015-03-19T00:00:00.000+00:00
author: Jack Schafer and Marvin Karlins
title: The Like Switch
subtitle: An Ex-FBI Agent's Guide to Influencing, Attracting and Winning People Over
main_color: B4383E
text_color: B4383E
---

# The Like Switch

_An Ex-FBI Agent's Guide to Influencing, Attracting and Winning People Over_

**Jack Schafer and Marvin Karlins**

In _The Like Switch_, author Jack Schafer explores the realm of nonverbal social cues and other communication practices that draws people to one another. A former FBI agent and doctor of psychology, Schafer presents useful strategies to make new friends and influence people.

---
### 1. What’s in it for me? Get people to like you using inside tips from an FBI special agent. 

We are social animals. Turning strangers into friends is a process that is as natural to us, as humans, as are breathing and walking.

Yet many of us seem to lack the skills to make friends effectively. Perhaps we're too shy, or we don't have the ability to empathize with others.

If this is you, there is hope. Rather than resigning yourself to living a lonely life, read the advice in these blinks. You'll discover the secret skills to getting people to like you, the same tricks and talents that the Federal Bureau of Investigation uses to snare spies and informants!

In these blinks, you'll discover

  * why we only expose our arteries to someone we actually like;

  * what eyebrows and fireflies have in common; and

  * why long-term relationships have everything to do with LOVE.

### 2. If you want someone to like you, follow the formula. Frequency, proximity, duration, intensity. 

If you want someone to like you, what should you do?

Advice from most people would probably be, "just be yourself!" Yet the author disagrees; there is more to being liked than just being genuine. There is a friendship "formula."

The first steps are to _frequently_ be in _proximity_ with the person you want to like you.

People who share the same physical surroundings are more likely to be drawn to one another. So try to be around the person you want to win over as often as you are able.

Let's look at a scenario. FBI agent Charles needed to befriend a foreign diplomat, codename Seagull, to convince Seagull to become a spy for the United States. Using the technique of proximity, he started following Seagull's daily route to the grocery store, so that they both would share the same environment for a time each day.

In doing so, Charles worked to increase the number of times when Seagull would become aware of him, so he'd start to seem familiar.

Other steps in the formula are the _duration_ and _intensity_ of time spent with your target.

Duration is important. The more time you spend with someone, the more you'll be able to influence them to like you.

After a couple of months, Charles used the power of duration by actually following Seagull into the grocery store, which added to the contact time between them.

He also enlisted _intensity_ — meaning how well one is able to satisfy another person's psychological and/or physical needs — by offering nonverbal "friend signals." By nodding his head and catching Seagull's eye on occasion, Seagull naturally became interested in whom Charles was and why he was always around.

By the time Charles introduced himself as a FBI agent, Seagull was already primed to become a friend. And because of this familiarity, Seagull agreed to be a spy.

### 3. Nonverbal cues set the stage for a potential friendship. Eyebrows, head tilts and smiles speak volumes. 

While humans don't exactly have a lot in common with fireflies, we do share one particular behavior trait.

Fireflies flash their light-emitting organs as a signal to attract partners for mating. Similarly, humans use an eyebrow "flash" to attract positive attention.

This nonverbal cue is a rapid up-and-down movement of the eyebrows, lasting for around one-sixth of a second. Even from quite far away, an eyebrow flash sends a signal that you don't pose a threat to the person you're approaching.

A second primary nonverbal cue is the head tilt. This is a nonthreatening gesture, since tilting your head to the left or the right exposes one of your carotid arteries, which provides oxygenated blood to the brain.

We unconsciously expose these arteries only to people who are harmless. Conversely, we tuck our neck into our shoulders to protect ourselves when we feel threatened.

It has been shown that people who tilt their heads toward the person they are talking with are perceived as more friendly, kind and honest when compared to individuals who keep their heads upright.

A third powerful nonverbal signal is a genuine smile. We judge those with smiling faces to be more likeable, attractive and less dominant, as well as friendly.

As an added bonus, a smile releases endorphins, giving us a sense of well-being. It's also difficult not to smile back at a person who is smiling at you; a reciprocated smile makes your target feel good.

Making people feel good about themselves is critical to befriending them, as we'll see in the next blink.

### 4. You have to make people feel good about themselves if you want them to like you! 

So you've made your person of interest receptive to interacting with you, and now you're ready to make verbal contact. How exactly should you go about this?

The _golden rule of friendship_ posits that if you want people to like you, you need to make them feel good about themselves.

In fact, this is central to all successful relationships. As an FBI agent, this rule helped the author persuade people to become spies and confess to crimes! The rule even helped him get upgraded to business class on planes, several times.

But how does the golden rule work?

People tend to gravitate toward people who make them happy, because they want to give back the positive feeling that was offered to them.

For example, when the author had a layover in Frankfurt before boarding an eight-hour flight with a middle seat in coach, he chose to spend his time chatting with the ticket agent.

He spoke using so-called _empathetic statements_ to keep the conversation revolving around the ticket agent, as opposed to himself. This way he showed that he was interested in the agent's well-being and what the agent said.

A standard empathetic statement could be, "So you are having a good/bad day?" By using this method, Schafer encouraged the agent to talk about himself and his day, and therefore generally feel good about himself.

Consequently, as Schafer was about to board the plane, the agent replaced his coach boarding pass with a business class pass.

This demonstrates how employing the _golden rule of friendship_ inspires reciprocity. Even in one-time interactions, the tenet "If you make me happy, I want to make you happy" works wonders.

### 5. To enhance the potential for friendship connections, don’t forget the laws of attraction. 

So far we've seen the basic rules for making friends. Yet there are even more strategies you can add to your friendship toolbox.

These are called the _laws of attraction_.

One of these laws is the _law of similarity_, which recognizes that those people who share the same principles and beliefs tend to become friends. This law works because people who are similar tend to strengthen the similar aspects they share, which promotes mutual attraction.

Similarity can be used by looking for things you have in common with the person with whom you want to have a good relationship. For instance, you might notice if your target's clothes give away a potential shared interest, say if she is wearing a shirt with a particular sports team's logo.

If you find a commonality, gear the conversation toward that shared interest and avoid topics you don't both seem to be interested in.

Another law is the _law of curiosity_, which states that when you show you are curious about a person, you automatically pique that person's interest in you.

Curiosity is a universal human trait, and one you can use by creating a "curiosity hook" to attract your person of interest.

For instance, if you visit a café by yourself hoping to attract a new friend, order something unusual from the menu or bring an odd book with you to read. Standing out from others will mean there's a good chance someone will ask you about your behavior.

A third law is the _law of reciprocity_, which states that when someone gives you something, you are inclined to return the gesture in kind.

You can exercise this law by doing someone a favor, such as buying them coffee or picking something up for them. When they say thank you, don't default to, "You're welcome," but say instead, "I know you'd do the same thing for me." This will evoke a sense of reciprocity.

### 6. Listen up! Let others tell their story. And to cement relationships, remember the acronym LOVE. 

If you remember only one rule about making friends, let it be the rule of encouraging other people to speak.

To do this effectively, remember the acronym LOVE: _Listen, Observe, Vocalize and Empathize_.

First, you have to _listen,_ and listen properly!

You know the feeling of telling a story to someone who's listening half-heartedly or constantly glancing at something else? Isn't it annoying? Speakers usually know whether they're being listened to or not, so pay attention when people speak by maintaining eye contact and _never_ interrupting.

Second, _observe_ your partner's nonverbal signals and body language while you're interacting. Distancing actions, such as leaning back, crossing one's arms over the chest or pressing the lips together are strong indicators that the conversation isn't going so well.

If you see this kind of behavior, you may be talking too much about yourself. So as soon as the other person starts to disengage, switch subjects to something that interests them more.

Third, the way you _vocalize_ and the content of what you say influences how you make and keep friends. Your tone of voice is pivotal to the message you want to convey. For instance, a low-pitched voice often denotes romantic interest, and a high-pitched voice may communicate surprise.

And although it may seem obvious, _what_ you say influences how others perceive you. Remember to verbally acknowledge others and give compliments to make people feel good about themselves and see you as a friend.

Finally, use _empathetic_ _statements_ to let the other person know that you understand what's happening around you both. You could show empathy, for instance, by using the phrase, "Wow, you're busy. I don't know how you do it!" to a waiter during a busy time at your local café.

### 7. Does a friend meet your eyes when you talk? Does she mirror your movements? Then you have rapport. 

Rapport means having a relationship with someone based on mutual understanding. In essence, rapport is the thing that ensures the friendship you are developing is fulfilling for both parties.

Testing for rapport lets you know how your relationship is progressing. Rapport establishes a psychological bridge between people, and determines the level of friendship that is being developed.

If you want to build and maintain lasting relationships with people or want something specific from a one-time encounter — like an upgrade — you need to test for rapport to determine when and if you've arrived at a point where you can push for your relationship objective.

So how do you gauge rapport? Eye gaze is a tried and true way to test for rapport. If a person refuses to lock eyes with you, the relationship might need some more maintenance before advancing to the stage of prolonged eye contact.

You can also look for behaviors, such as touching or mirroring, to test for rapport.

While we typically only briefly touch each other below the shoulders or shake hands when we don't know someone, touching that occurs outside this area signifies a closer relationship.

A light touch on the forearm or knee when you are both seated will show whether rapport has been established or not.

You can use mirroring, also called isopraxism, to test for rapport, since people who are psychologically connected will often mirror each other's body language in conversation.

To test this, try the "lead and follow" approach by changing your body position, like crossing or uncrossing your arms. If the other person mirrors you, rapport has usually been established.

### 8. Final summary 

The key message in this book:

**There are a variety of nonverbal signals you can use to attract people before you even speak to them. Frequently staying within close proximity, making eye contact, tilting your head and smiling are just a few of the techniques you can use to endear yourself to others and eventually form and maintain solid relationships.**

Actionable advice:

**To maintain a thriving partnership, it's essential that you CARE.**

When your relationship has grown into a friendship, it's time to CARE. This entails _compassion_, such as offering kind words and a helping hand; _active listening_, or giving attention to what your friend is saying; offering _reinforcement_, like praise and acknowledgement when the friend does something well; and _empathy_, or understanding how your friend feels and actually caring about it.

**Suggested further reading:** ** _What Every BODY is Saying_** **by Joe Navarro**

This book is all about the hidden meanings we can find in our body language. Our brains control our body movements without us being conscious of it, and sometimes those movements can be very revealing. This book goes into detail about the ways you can train yourself to become an expert in observing other people's nonverbal cues and uncovering their meaning.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jack Schafer and Marvin Karlins

Former FBI Special Agent Jack Schafer is a doctor of psychology and professor at the Law Enforcement and Justice Administration Department of Western Illinois University. He has also worked as a behavioral analyst.

Marvin Karlins is a doctor of psychology and an interpersonal effectiveness consultant. He has authored 24 books, including two bestsellers, _What Every Body Is Saying_ and _It's a Jungle In There_.

