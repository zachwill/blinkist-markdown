---
id: 54eae8e2393366000a000000
slug: the-little-book-of-common-sense-investing-en
published_date: 2015-02-23T00:00:00.000+00:00
author: John C. Bogle
title: The Little Book of Common Sense Investing
subtitle: The Only Way to Guarantee Your Fair Share of Stock Market Returns
main_color: EF444B
text_color: B23338
---

# The Little Book of Common Sense Investing

_The Only Way to Guarantee Your Fair Share of Stock Market Returns_

**John C. Bogle**

_The Little Book of Common Sense Investing_ provides a detailed overview of two different investment options: actively managed funds and index funds. These blinks explain why it's better to your money in a low-cost index fund instead of making risky, high-cost investments in wheeling-and-dealing mutual funds.

---
### 1. What’s in it for me? Learn why index funds are the best investment you can make. 

Mutual funds are like chocolate bars: there are so many different kinds, and new versions are always popping up. How are you supposed to decide where to invest?

One way is security. Pool your investment along with other people's in a low-risk fund that's highly diversified and avoids turbulent markets. Or you could include high-risk and high-reward funds, or funds that play in particular statistics.

The options can get confusing and the last thing you want is to make poor decisions with your hard-earned money!

This pack will hedge the odds in your favor, laying out a straightforward case for one kind of mutual fund: The _index fund_. These blinks explain why all mutual funds are not created equal, and why index funds are the only way to not get robbed by fees and expenses.

In these blinks, you'll find out

  * the difference between a passive fund and an actively managed fund;

  * why you should always choose the fund with the lowest fees; and

  * how you can avoid market bubbles.

### 2. Actively managed funds are expensive and consequently often underperform the market. 

Have you ever invested in the stock market? If so, you might have realized that evaluating the attractiveness of a stock is tricky business.

That's why many investors choose not to invest directly into stocks, but instead put their money in an _actively managed fund._ Here money is pooled from several investors and then invested into stocks by a specialized fund manager, who regularly evaluates and revises the stock portfolio according to the current situation.

Unfortunately, that kind of investment is risky.

Why?

Because the costs of investing in such a fund are very high. As an investor, you'd pay the brokerage commissions, the fund manager's fees and so forth. All those fees add up to a hefty chunk of your expected profits.

If the funds perform extremely well, you might not mind those costs, but in the long run, actively managed funds are likely to yield you _less_ profit than the overall stock market.

How can that be?

For one, speculating on stock prices is simply not a sustainable strategy. You might think that a fund can generate huge profits by, for example, buying stocks when they are undervalued and selling them later when they reach their true higher value, but in the long run this strategy can't produce more earnings than what the underlying companies are earning, which is reflected in the overall development of the stock market.

Add that pitfall to the high costs of the funds, and the result is that an actively managed fund will generate significantly less profit for you than a passive, low-cost _index fund_ that merely mimics the performance of the overall market. In fact, if you had invested $10,000 in 1980, by 2005 you would walk away with 70 percent less if you invested in an active fund rather than an index fund, due to fees alone!

> _"The expectations market is about speculation. The real market is about investing."_

### 3. Few funds perform well, and there’s no guarantee even those few will continue to do so. 

Despite what you've just discovered about the costliness of funds, you might still be thinking of investing in an actively managed fund. Before you do, though, consider if those funds perform well compared to the overall stock market.

Unfortunately, they probably don't. Most funds go bankrupt or fail to generate significant returns.

Investors pay huge fees to funds, deferring to financial experts who have a solid understanding of the stock market. However, despite industry knowledge or expertise, only 24 of the 355 mutual funds that existed in 1970 have outperformed the market consistently and remain in business.

Armed with these facts, you'd be throwing your money away by paying for financial experts to manage your fund.

Furthermore, even profitable funds can't promise good performance in the future.

You might decide to invest your money in the funds still outperforming the market consistently; those funds that have beaten the odds. However, even if you analyze their track record, the same conditions that caused the fund to perform well over the past 35 years may not repeat themselves in the following decades.

For example, if a fund consistently outperformed the market in the last 35 years, the fund manager probably played a huge role in its success. But, the manager will inevitably retire at some point. How do you know the next one will have a similar rate of success?

Also, future investment opportunities will differ from the ones of the past 35 years. How do you know the possibilities for future investments? You don't!

> Fact: Of the 366 equity funds open in 1970, 223 have closed.

### 4. Most people invest in actively managed funds unaware of all the implications. 

We've painted the picture of how poorly most funds perform, so you might wonder why people keep investing in them.

First of all, investors often underestimate the true cost of actively managed funds.

As you've learned, actively managed funds automatically come with high costs. However, fund managers rarely disclose the dollar amount. Instead, they boast about the high returns but forget to divulge what the investor will really earn after deducting all the performance and portfolio fees.

Surprisingly, that omission occurs often: 198 of the 200 most successful funds in the latter years of the 1990s reported higher returns than the investors actually earned!

Secondly, in many avenues of investing, people often tend to let their emotions and current market trends make important decisions for them.

All too often, people make unsound investments because they let popular opinion and clever marketing sway their decisions.

For example, recall the high-risk investments of the late 1990s. In contrast to the first half of the decade when people invested a mere $18 billion in the stock market, a whopping $420 billion was invested in the second half of the 1990s when the market thrived and stocks were overvalued. When the bubble eventually burst, people realized too late that they had given into the popular hype.

This same principle applies to actively managed funds: investors pour money into these funds because everyone else is doing the same thing.

So if you shouldn't allocate your money to actively managed funds, where should you put it? Let's continue on to learn about sensible alternatives.

### 5. Put the majority of your assets in safe, low-cost index funds. 

Don't let what you now know about actively managed funds persuade you to keep all your money under your bed. The _index fund_ is your best alternative.

In contrast to actively managed funds, index funds are much more cost-efficient.

By definition, an index fund holds a diversified portfolio that reflects the financial market or a specific market sector. However, instead of betting on the market, index funds hold their portfolios indefinitely, eliminating the risks of making short-term, volatile bets while simultaneously minimizing operating costs.

Because index funds track the performance of all stocks included in the index without betting on individual stocks, they're also called _passive funds_.

Since they simply hold shares across particular market sectors, you will not have to bear operating fees for buying and selling shares, financial consultants, or fund management. You will, however, reap the benefits of commercial net returns.

Another advantage to index funds is that they're likely to outperform actively managed funds in the long-term.

You might argue that holding shares indefinitely instead of buying when stocks are cheap and selling when they're expensive results in lost opportunities. However, you already learned that in the long term, the rises and falls of the stock market eventually level out at the real value of the stock. Because of that net effect, index funds usually outperform actively managed funds in the long run; they offer returns at the real value of the stocks while eliminating active management costs.

The next blink will help you choose the right index fund.

### 6. Choose the cheapest index fund. 

Each index fund comprises an expense ratio that represents management fees and operating expenses. These expenses, though typically totalling less than one percent, can add up in a long-term investment.

For instance, the Fidelity Spartan Index fund has an annual expense ratio of 0.007 percent, while the J.P. Morgan Index fund's is 0.53 percent. Both funds have expenses below 1 percent, but over longer investment periods like a decade, those tenths of a penny add up.

Since index funds fluctuations follow the overall market, go ahead and choose the fund with the lowest cost structure, knowing that a company's expense ratio doesn't equate with its level of performance.

### 7. Be wary of new investing trends. 

Whenever you're considering where to invest your hard-earned money, be skeptical when it comes to the latest investment fads.

The fierce competition between index funds causes a continuous cycle of new trends. Index funds were invented in 1975, and today there are already 578 index funds competing with each other! Long-standing funds compete by trying to lower their costs as much as possible and thus lure in discerning investors. Meanwhile, new funds entering the market attempt to gain clients by promising larger rewards through new stock-picking procedures. Consequently, they also charge more.

For example, _The New Copernicans_ abstain from compiling their portfolios with stocks chosen by customary methods, like _weighted market capitalization_, whereby stocks are bought in proportion to the total market capitalization of each company, meaning the total number of shares multiplied by the average share price. Instead, the movement might calculate the proportions of each stock in their portfolio based on, for example, how much profit each company earns, or how much they've paid out in dividends.

Remember though, no matter how a fund claims to operate, it's nearly impossible to know which stocks are over- or undervalued, so you should adhere to funds that retain a normal portfolio.

Furthermore, since you can't predict which new investment trends will be successful, you should remain cautious and keep low costs the priority.

> _"The stars produced in the mutual fund field are rarely stars; all too often they are comets."_

### 8. Final summary 

The key message in this book:

**Investing in actively managed funds is a bad idea: they only eat up your hard-earned money, while financial mediators make a fortune. Make more of your money by investing in an index fund.**

Actionable advice:

**Reconsider what funds to invest in.**

Invest the majority of your assets in an index fund, and if you want to gamble with some of your hard-earned money, then place a small amount in actively managed funds. If, despite what you've read you still want to partake in the thrill of risking some money and making fast profits in actively managed funds, bet on no more than 5 percent. You can afford to risk this small amount but absolutely keep the majority of your assets in a safe long-term investment.

**Suggested** **further** **reading:** ** _The Intelligent Investor_** **by Benjamin Graham with comments by Jason Zweig**

_The Intelligent Investor_ offers sounds advice on investing from a trustworthy source — Benjamin Graham, an investor who flourished after the financial crash of 1929. Having learned from his own mistakes, the author lays out exactly what it takes to become a successful investor in any environment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### John C. Bogle

John C. Bogle is the founder and now-retired CEO of the Vanguard Mutual Fund Group, an American investment management company. His other books include the bestselling classic _Common Sense on Mutual Funds._

_©_ [John C. Bogle: The Little Book of Common Sense Investing] copyright [2007], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

