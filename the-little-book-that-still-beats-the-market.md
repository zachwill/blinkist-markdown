---
id: 54cf9276323335000a6c0000
slug: the-little-book-that-still-beats-the-market-en
published_date: 2015-02-05T00:00:00.000+00:00
author: Joel Greenblatt
title: The Little Book That Still Beats the Market
subtitle: None
main_color: 436085
text_color: 436085
---

# The Little Book That Still Beats the Market

_None_

**Joel Greenblatt**

Want to invest your hard-earned cash in something that'll pay real dividends? Not sure how to negotiate the mumbo-jumbo of the financial world? _The Little Book That Still Beats the Market_ is a _New York Times_ bestseller that introduces and explains a simple formula that enables anyone to make above-average returns on the stock market.

---
### 1. What’s in it for me? Discover the magic formula that will make you a killing on the stock market. 

We all want to grow a little nest egg for the autumn years of our lives, and one surefire way of growing it is by investing in the stock market.

But how many of us trust our financial abilities enough to manage our own investments? Most of us are happy to turn our money over to experts, and let them do it. This is wrong: they charge enormous fees for their expertise and they often get it wrong.

No, the best way to make the most of the stock market is to manage your own investments, but how?

These blinks show you what factors you should look out for when judging the profitability of a share. If you follow their advice you stand to beat the market average by a decent margin.

In these blinks you'll discover

  * the magic formula for profits;

  * who Mr. Market is and how you can beat him; and

  * how you can cheat the tax man.

### 2. It is extremely difficult to find a good financial professional who can guarantee you large returns on your money. 

Your childhood belief that money magically appears under your pillow evaporated when you discovered the tooth fairy didn't really exist. But as you grew older, this idea of magically increasing your wealth suddenly didn't seem so crazy when you learned about investing in the stock market.

Most of us have a rudimentary understanding of the stock market, yet because hardly any of us know how it works, we employ financial experts for help. Unfortunately, the majority of financial experts are not worth their fees.

One expert we expect to make us high profits is the _stockbroker_. Many of us use their services, but stockbrokers aren't the best people to trust with our investments. Stockbrokers get paid by selling you investment products, regardless of how profitable they are. They have no incentive to sell you the best products available; they simply need to sell you any products they can.

People also rely on a _mutual fund_ to manage their investments.

Mutual funds, in which investments from many people are pooled, usually come with a high management fee. So although they often yield decent returns, by the time you've subtracted the fees, you're often left with below-average returns.

There is, however, one type of financial investor who can manage your investments well.

An _index fund_ such as Standard & Poor's 500 Index (S&P 500) is a mutual fund which aims to match the market's best performing companies, as opposed to trying to beat them. You may think that this would give you underwhelming results, yet, as index fund fees are lower, and because on average the market performs well anyway, index funds often provide you with very healthy profits.

So if you're happy with someone else managing your money, opt for index funds.

But there is an even better way! If you want to maximize your profit, there is no substitute for learning to do it yourself, and the following blinks will show you how.

> _"On Wall Street...there is no one to tuck you in, no one to take care of you, and no one you can turn to for good advice."_

### 3. The stock market values of most companies swing drastically over short time periods for no rational reason. 

If you were to draw a graph showing the stock prices of Google or Disney over the last five years, what would it look like? Probably a fairly straight, rising line marking stable increases in price, right? Wrong.

Although there would be a general trend of increasing prices, the line would be jagged, depicting fluctuations which affect all stock, regardless of how successful a company is.

For example, on 13 November 2014, the stock price of Google on the NASDAQ was $558.25, with its price ranging between $510.67 and $615.04 over the previous 52 weeks. On the same day, the stock price of Disney was $89.90, ranging between $68.80 and $92.00 over the previous 52 weeks.

But why do companies' stock prices vary so much?

The real value of a company isn't reflected in its share price; sometimes it's undervalued, sometimes overvalued. One way to understand this is by thinking of the stock market as if it were an emotionally unstable person.

Investment legend Benjamin Graham takes this analogy even further, by describing the market as a crazy person called Mr. Market.

Mr. Market really wants to sell you shares in his company, but he's volatile. Some days he's incredibly optimistic, eager to sell at a high price, but on others he's depressed, believing his company isn't worth that much and therefore offering his shares at a lower price.

You can exploit Mr. Market's temperament by buying stocks when he's in a bad mood and offering his shares at a low price. Then when he's feeling better, you can offer to sell the shares back at a higher price.

The following blinks will show you even more tricks for using Mr. Market to your advantage.

### 4. Look at the “earnings yield” and the “return on capital” when putting together your investment portfolio. 

If you want to stand a chance against Mr. Market, you need to ensure that you buy stocks when their price is below the company's true value and sell them when the share price is above it.

To find out whether the share price is too high or low, you should consider two numbers.

First look at the earnings yield, meaning the company. This tells you what the business earns in relation to its share price.

The earnings yield is calculated by finding the ratio of _earnings before interest expenses and tax_ (EBIT), to _enterprise value_ (EV). The EV is the _market value_ plus the _net interest-bearing debt_.

The EBIT/EV calculation has an advantage over the often-used _price divided by earnings_ ratio (P/E) as it's not influenced by debt and tax rates.

Say an office building is bought for $1 million, with a mortgage of $800,000, and $200,000 in equity. If the building's EBIT was $100,000, then its earnings yield (EBIT/EV) would be ten percent ($100,000/$1,000,000).

If the interest rate on the $800,000 mortgage was six percent, the earnings yield on the equity purchase price ($200,000) would be 26 percent. That is, $100,000-$48,000 in interest expense = $52,000 in pretax income. ($52,000/$200,000 is 26 percent.)

This earnings yield would change according to debt level, but the $1,000,000 purchase price, and its EBIT of $100,000 would stay the same.

However, a high earnings yield isn't enough. We need an additional calculation to tell us if the company is actually profitable. That is, we need to look at the _return on capital_ (ROC).

Although there are many variables that influence a company's health, such as the value of its brands, its strategy, and so forth, the simplest variable to estimate is the ROC. ROC is calculated by dividing the after-tax profit by the _book value_ of invested capital (the total amount of money invested by the company's shareholders, bondholders, and so on).

ROC reveals how effective a company is at transforming investment into profit. For example, if you spent $300,000 setting up a restaurant and made a profit of $100,000 in the first year, your ROC for that year would be 33.33 percent, which is good. Anything above 25 percent indicates that a company is doing well.

So if you buy shares of companies that have a high return on capital at low prices, you'll systematically buy into companies which are currently undervalued by Mr. Market.

### 5. The magic formula combines earnings yield and return on capital. 

The previous blink showed you that when buying shares you should look at the earnings yield and ROC. But what exactly should you do with these figures? You need to use them to create the _magic formula._

The magic formula combines both the earnings yield and ROC into one a single measure. It starts with a list of the 3,500 biggest companies available for trading on one of the major US stock exchanges, such as the NYSE or the NASDAQ, and ranks those companies on a scale between one and 3,500, based on their ROC. The company with the highest return takes the top spot.

The same companies are then ranked according to their earnings yield and the company with the highest earning yield is placed at the top.

Lastly, both rankings are combined. So a company which ranked number one on ROC, but at 181 on earnings yield, would have a total score of 182 (one plus 182). The companies with the lowest combined ranking therefore have the best combination between both factors of earnings yield and ROC.

So what does this mean? It means you should be buying shares from these companies!

The magic formula does a really good job over time. Over a 17-year period from 1988 to 2004, a portfolio of around 30 stocks with the best combined rankings returned on average about 30 percent per year, whereas the market average was a mere 12 percent. So the magic formula really pays off.

### 6. As the magic formula is a long term strategy, it is unattractive to financial managers who need to perform every year. 

If the magic formula is such a brilliant money-making strategy, why doesn't every investment manager use it?

One reason is that the strategy doesn't always beat the market. On average, for five months of every year, the overall market average will beat a portfolio based on the magic formula. The same goes for 25 percent of all full-year periods.

Even during the massively successful period from 1988 to 2004 — when a magic formula portfolio generated on average 30 percent profit per year — sometimes it actually performed worse than the overall market average for two or more consecutive years.

In reality, professional money managers can't afford to perform lower than average for long periods because they need to keep their clients satisfied all the time. It's difficult to hold onto a strategy which might not work for months or years at a time, no matter how effective the strategy is in the long term. Money managers don't use the magic formula because the window of time where they need to show profits to their clients is just too narrow. A manager who can't consistently show their client a profit is highly likely to be fired, despite their promise that a profit will be made some time in the future.

The bottom line is, although the magic formula works just fine in the long term, most professional investors can't afford to perform below the market average for such a long time. You can, though. All it takes is some patience to stick with it during the good and the not-so-good times.

### 7. To reduce risk, invest in at least 30 stocks of big companies and use tax laws intelligently to further maximize your profit. 

We know the magic formula works, but to apply it in the best possible way, you need to take the following precautions.

First, apply the magic formula when choosing stocks of large companies, not small ones. Smaller companies don't have much to offer in terms of shares, so even a slight increase in demand can force their share price up. You may therefore find it challenging to buy their shares at a reasonable price.

Applying the magic formula to larger companies is smarter. If a company's market value (the number of shares multiplied by the stock price) is in excess of $50 million, this indicates it's a large company.

What you should do then, is own at least 20 to 30 large company stocks at a time.

Just bear in mind that the fewer stocks you own, the higher your risk of straying from the average, healthy performance of the magic formula.

Owning multiple shares can cushion the financial blow resulting from a negative industry event such as a tanker spill. It's wise, then, to have your stocks in different industries.

Finally, you need to be able to use tax laws intelligently to maximize your profit.

This means holding each stock for roughly one year and then selling it. However, whether you sell them slightly before or after this date depends on tax. In the United States there is a lower tax rate for profits on stocks held for more than one year. So you should sell stocks which lost value before the one-year holding period ends. On paper, this decreases your yearly income and thus the taxes you will need to pay.

However, for stocks which increased in value, do the opposite. Sell them after the one-year period is over in order to reduce your tax.

### 8. Final summary 

****The key message in this book:

**Anyone can make above-average returns on the stock market if they follow the easy to calculate, tried and true formula detailed in these blinks.**

Actionable advice:

**If you want to generate profit using the stock market, use your resources.**

To help you get started, the author created the free resource www.magicformulainvesting.com, which uses high-quality data to enable you to put the magic formula into practice. It also provides a list of high ranking companies on the current stock market.

**Suggested further reading:** ** _The Intelligent Investor_** **by Benjamin Graham with comments by Jason Zweig**

_The Intelligent Investor_ offers sounds advice on investing from a trustworthy source — Benjamin Graham, an investor who flourished after the financial crash of 1929. Having learned from his own mistakes, the author lays out exactly what it takes to become a successful investor in any environment.
---

### Joel Greenblatt

Joel Greenblatt is an American hedge fund manager and author. His books include _You Can Be a Stock Market Genius: Uncover the Secret Hiding Places of the Stock Market Profits_ and _The Big Secret for the Small Investor: A New Route to Long-Term Investment Success_. He also teaches as an adjunct professor at the Columbia University Graduate School of Business.

© Joel Greenblatt: The Little Book that Still Beats the Market copyright 2010, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

