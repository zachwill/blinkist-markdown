---
id: 570a1b378b5d6e0003000115
slug: the-locust-effect-en
published_date: 2016-04-14T00:00:00.000+00:00
author: Gary A. Haugen and Victor Boutros
title: The Locust Effect
subtitle: Why the End of Poverty Requires the End of Violence
main_color: 954538
text_color: 954538
---

# The Locust Effect

_Why the End of Poverty Requires the End of Violence_

**Gary A. Haugen and Victor Boutros**

_The Locust Effect_ (2014) argues that foreign aid is only useful to developing countries if their impoverished citizens have protection from violence and crime. Without this, aid money is wasted because neither individuals nor businesses are safe to grow. Financial donations should aim to strengthen national criminal justice systems, so countries can serve themselves in the long run.

---
### 1. What’s in it for me? Grasp why you need to fight violence to overcome poverty. 

Today, many people in developing countries are living in fear — and no amount of food, medical care or education is going to change that. They fear the criminal violence that's rampant in their neighborhoods.

Violence traumatizes millions of individuals and can stunt the economic growth of entire nations. Like locusts, criminals attack the poorest, effectively devouring and destroying whichever structures aid organizations are trying to build up.

So why is it that many governments fail to keep that violence at bay — and what can developmental organizations do to help them? These are two of the questions you'll find answered in these blinks.

You'll also find out

  * why enterprises in poor countries often pay huge amounts of money to take their causes to private courts;

  * the historical reason policing in India is so fundamentally different from police work in the United Kingdom; and

  * how one short-term project in the Philippines achieved a massive reduction in commercial child abuse.

### 2. Violence has a big impact on the populations and economies of developing countries. 

Many people believe that impoverished communities are the root of society's problems. Wealthier classes even tend to fear them. But low-income communities aren't the real threat — that comes from those who are violent toward them.

Violence is the most destructive force in society. It's worse than any natural disaster, even hurricanes.

Hurricane Stan, for instance, took a big toll on Guatemala's economy when it hit in 2005, and was widely regarded as a huge disaster. However, criminal violence costs Guatemala roughly _twice_ as much each year. It accounts for a 7.3 percent loss in GDP, according to World Bank development reports.

Violent crime has an even greater cost in countries like Colombia or El Salvador, where it's said to reduce economic growth by as much as 25 percent every year.

Violence harms a nation's economy in a number of ways. For one, it cuts down the workforce by rendering healthy people incapable of work.

_Disability Adjusted Life Years_ measures this problem by estimating how many years of work are lost because of violence. Every year, nine million Disability Adjusted Life Years are lost just due to women being raped or abused.

This has a big impact in places like Africa, where 80 percent of the farm work is done by women. Violence can severely cut food production in some of the world's poorest regions.

And of course, violence has an even greater impact on the victims. This is especially disastrous in developing countries like Ethiopia. A 2009 Human Rights report in Ethiopia showed that victims of violence suffer higher rates of depression, substance abuse and suicide.

> _"Without the world noticing, the locusts of common, criminal violence are right now ravaging the lives and dreams of billions of our poorest neighbors."_

### 3. Law enforcement fails the poor in many developing countries. 

Imagine you're being assaulted, then the police show up and side with your assailant. Sadly, this is a common occurrence in many parts of the world.

Poor people lack even the most basic protection in many of the world's developing countries. In fact, recent UN studies have found that law enforcement systems in developing countries are often so corrupt that poor communities live completely outside protection of the law.

The story of Maria, a 14-year-old Peruvian girl, is a tragically common example. Maria was raped three times by the same man, a taxi driver from her town. When the police found out, they refused to investigate and even shouted at Maria, accusing her of seducing her rapist.

Developing countries also often fail to provide their citizens with legal protection, as in the case of Dan, a teenager from Kenya. Dan has spent eight months in jail and now faces a life sentence without a fair trial.

Dan and two of his friends were guarding the local water fountain when a fight broke out between them and an older man, who appeared to have siphoned off some water. The boys were arrested for violent robbery and are now up against the Kibera Law Court. Dan's proceedings don't meet the lowest criteria for a fair trial: he has no legal representation and doesn't even speak the language used in the courtroom.

Poor people don't suffer from violence because they're poor — they suffer because they're unprotected.

Consider the fact that 90 percent of all malaria deaths happen in poverty-stricken populations. It's not that malaria disproportionately affects poor people, it's that poor people don't have access to good medical treatment for it.

Violence works the same way. Crimes against the poor often go unpunished, so criminals like Maria's rapist aren't deterred from violating others again and again.

### 4. The police and the justice systems are often dysfunctional in developing countries. 

You wouldn't go to the local butcher if you needed emergency heart surgery. Sadly, this is what law enforcement is like in a lot of developing countries: police are untrained and ignorant about what they're tasked with doing.

In fact, a 2009 Human Rights Watch report found that 85 percent of police officers in India are _constables_, meaning they're largely untrained in dealing with criminal cases. Constabulary staff in India only receive a few months of military training and virtually no training in law.

Moreover, there simply aren't enough police officers in many parts of the world. In Bangladesh, for example, the government only spends $1.50 per capita on law enforcement each year, according to a 2005 study by the Development Bank of Asia. Compare that to Washington DC, which spends $2.33 per capita on the police force _each day_.

Courts in developing countries also often suffer from a shortage of prosecutors. The United States has one prosecutor for every 12,000 citizens, according to the United Nations. That might seem like a low ratio, but it's far higher than the ratio in several developing countries, such as Malawi. In Malawi, there are only ten prosecutors for the entire population of 15 million citizens!

Criminal justice systems are very slow and inefficient in countries that have a shortage of prosecutors. In the Philippines, for instance, it can take up to ten years for a court case to be concluded, according to a 2008 study by Danilo Reyes, a leading official of the Asian Human Rights Commission (AHRC). The accused has to wait in prison while the case is being carried out. If they're innocent, they'll have lost a significant chunk of their lives for no reason.

### 5. Developing countries have inherited colonial justice systems designed to protect the elite. 

Phrases like "Let them eat cake" or "Throw them into the dungeons" might evoke memories of bygone eras in European countries, but in their former colonies, the legacy lives on.

Post-colonial countries typically still have criminal justice systems that harken back to the colonial era. The very concept of the "police" was created by the British in the mid-eighteenth century century when soldiers and guards were replaced by Home Secretary Robert Peel with a new kind of civilian police. These early police officers were unarmed, uniformed, and gradually won public support through their efforts to suppress crime.

It was a different story in the colonies, however. In the colonies, the goal of the police was to maintain British rule.

This became even truer after the 1857 rebellion in India when the British decided to implement the Irish Constabulary police method in the colonies. The distinctly militaristic Irish Constabulary method aimed to serve the ruling elite, not the general population. And they didn't just use the method in India, but in all their colonies.

Unfortunately, many laws weren't reformed after the colonial period ended, so police forces in lots of countries still exist to serve the elite. India's Police Act, for example, still regulates the Indian police force today, even though it's 140 years old. The country's entire legal system is founded on anachronisms.

Colonial-era police codes explain much of the present-day dysfunction in India and other South Asian countries. The police simply aren't trained to protect the public from violence and other crimes. They're trained to protect the wealthy and beat the masses into submission when necessary. Education and modern equipment aren't important for those ends.

### 6. Private justice systems are overtaking public justice systems, widening the gap between rich and poor. 

When the police force doesn't serve you, you look for other means of protection if you can afford it.

That's why parallel systems of private security have arisen in the developing world. In some places, the private security industry is experiencing an unprecedented boom.

According to a 2010 report by journalist Manu Kaushik, Indian private security firms employ over 5.5 million agents. That's roughly four times the number of people employed in the entire Indian police force!

The situation is even worse in some African countries, according to the World Bank. In Kenya, for instance, 80 percent of all businesses rely on private security firms for protection.

Broken criminal justice systems also allow elites in developing countries to circumvent courts of law. That's why so many nations have seen an emergence of private courts, also called _alternative dispute resolution systems_.

In alternative dispute resolution systems, an impartial arbiter intervenes to help the parties come to a mutual agreement, almost like a therapist. These systems are very popular in transitioning states, as they allow wealthier parties to avoid the delays of dysfunctional state systems.

This emergence of separate judicial systems only widens the gap between rich and poor. The more a country relies on its citizens to pay for private security and judiciary systems, the further the poor are excluded from these systems.

In fact, according to a 2010 study, even several countries that are experiencing overall growth, like India and many sub-Saharan African nations, aren't experiencing a decrease in extreme poverty. The poorer sections of the population don't have access to the resources that are making life better for the wealthy.

### 7. Very little developmental aid has been invested in criminal justice systems, but this might change. 

What do you do if a plague of locusts destroys all of a country's crops? It wouldn't help to just replant them — the locusts would only eat those crops too. You have to fight the locusts themselves.

Violence and crime work the same way. We've seen how dysfunctional justice systems hold back the poor and leave them vulnerable to criminal attacks — this means that any aid money poured into those dysfunctional systems is wasted. If you send more aid in, crime will only devour it faster.

Virtually no developmental aid has been invested in strengthening criminal justice systems, largely because foreign aid institutions don't want to be seen as interfering in a developing country's internal affairs. So international agencies tend to show restraint when it comes to criminal justice matters.

The World Bank, for example, has statutes that prohibit investment in structures aimed at improving local law enforcement.

Even when organizations _do_ try to strengthen a country's legal system, they rarely target the criminal justice system. There are too many other issues considered to be more important, such as corruption and security. Overall, less than one percent of all financial aid from development institutions is invested in helping the criminal justice system better protect the poor from violence and crime.

World Bank policies on this matter may change in the near future, however. That could have a big impact on protecting the poor from abuse and violence.

In February 2012, World Bank experts recommended that future investments should take broader economic concerns into consideration, rather than focusing on narrow economic gains. Investment in criminal justice falls under this, because protecting the poor from violence and exploitation would strengthen the economy in turn.

> _"Without security, there cannot be development." — Department for International Development, United Kingdom._

### 8. The Project Lantern initiative illustrates how NGOs can improve criminal justice systems. 

When it comes to fighting crime in developing countries, aid organizations can't replace domestic law enforcement. They _can_ advise and support national law enforcement agencies, however.

An NGO called International Justice Mission oversees one such program in Cebu, Philippines. It's called Project Lantern, and it aims to protect children from sex trafficking. Social workers, lawyers and police officers worked with local authorities for four years to build structures to protect local children.

They successfully advocated for the creation of a police task force specializing in fighting human trafficking and even convinced the Philippine Supreme Court to make sex trafficking a top priority.

Project Lantern has been quite successful. Over 250 victims of sex trafficking have been rescued, and roughly a hundred suspected traffickers have been charged and prosecuted. And the best news is that since the Project Lantern crackdown, the number of children involved in the local sex trade has decreased by 79 percent. Criminals are now strongly deterred, knowing they'll be punished if caught.

International Justice Mission was also able to change the way the victims of sex trafficking were treated. Before Project Lantern, victims were often kept in close proximity to their abusers at police stations. They had no protection from being abused again or being threatened into denying that the abuse had ever taken place.

The Project Lantern team was able to establish safe houses for victims, so they didn't have to go to police stations. In safe houses, they felt more comfortable speaking with investigators and naming their abusers, which made it much easier to track them down. The system was so effective that the Philippine government took over its costs and now manages it.

### 9. Final summary 

The key message in this book:

**When a country's impoverished communities have no protection against violent crime, the nation as a whole is held back. Overseas aid then becomes useless as violent crime devours it like a swarm of locusts. Foreign aid organizations need to improve national justice and policing institutions to protect the poor. Only then can developing nations move closer to not needing aid at all.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Better Angels of Our Nature_** **by Steven Pinker**

_The_ _Better_ _Angels_ _of_ _Our_ _Nature_ takes a close look at the history of violence in human society, explaining both our motivations to use violence on certain occasions and the factors that increasingly restrain us from using it — and how these factors have resulted in massive reductions in violence.
---

### Gary A. Haugen and Victor Boutros

Gary A. Haugen is a lawyer and the CEO of International Justice Mission, an NGO that supports local authorities in protecting the poor against crime. Victor Boutros is a US Department of Justice prosecutor who investigates human trafficking and hate crimes. Haugen and Boutros also co-authored _And Justice for All: Enforcing Human Rights for the World's Poor._

