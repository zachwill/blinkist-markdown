---
id: 519de649e4b0a17389a8ac08
slug: the-long-tail-en
published_date: 2014-03-18T11:18:00.000+00:00
author: Chris Anderson
title: The Long Tail
subtitle: Why the Future of Business is Selling Less of More
main_color: 40B0DA
text_color: 29718C
---

# The Long Tail

_Why the Future of Business is Selling Less of More_

**Chris Anderson**

_The Long Tail_ challenges existing notions of the market and the entertainment industry by looking at the massive influence of the internet on the economy. Due to new modes of content creation and distribution, it can be more profitable to offer a large number and wide variety of products that appeal to niche consumer groups rather than one certain "hit," e.g., a blockbuster or bestselling book.

---
### 1. What’s in it for me? Find out why niche markets are the future of commerce. 

For a long time, only the products that made the most money were marketable and popular. Then the internet came along, which brought about the slow, gradual decline of "the hit."

From author Chris Anderson, former editor of the tech magazine _Wired_, _The Long Tail_ identifies a new paradigm of the online entertainment market: the "long tail" of consumer demand. Anderson argues that satisfying the various and specific needs of several well-defined niche markets can bring just as much, if not more, success to a company than aiming to attract the widest possible audience to a single product.

In these blinks, you'll learn exactly what the "long tail" is and what it means for the future of online retail. You'll find out how the inexpensive consumer PC has been a driving force behind the superabundance of content in the world. You'll also discover how online retailers exploit the internet to increase their company's odds of success, such as buying keywords for Google Ads at auctions.

Finally, you'll learn how, in the current online retail climate, it's easier than ever before for a company to not just survive, but _thrive_.

The monopoly of the hit is over. Welcome to the long tail of demand.

### 2. In the world of online entertainment, a market of niche goods in small but steady demand rivals a market of hits. 

There's an established demand curve in any industry: the most-valued products generate the highest demand (the head of the curve) and the least-valued generate the smallest (the tail).

We're inclined to think of bestselling books and blockbuster movies — goods at the head of the curve — as the prime sources of profit. But, in the present economy, businesses can thrive by offering a large selection of goods that appeal to many small groups of people.

In other words, nowadays businesses can succeed by using the strategy of the "long tail."

The fact is, when it comes to total sales in an online market, a sufficient number of niche titles can compete with the best sellers.

For example, Amazon's Jeff Bezos recognized the potential of the internet to offer an unprecedented selection of books. Amazon offers approximately five million books on its website, whereas the largest superstores can only carry around 175,000. In fact, Amazon's statistics reveal that books that aren't available in the average bookstore make up over 30 percent of the existing market.

Why?

In short, popularity is no longer necessary for profitability. In the online market, to sell a niche product is just as valuable as selling a "hit."

Consider the online music-streaming service Rhapsody. It offers more than 4.5 million songs and each track — regardless of whether it's one of their top 900,000 — gets streamed at least once per month. Since every track costs the same price and generates the same profit, it doesn't matter to them whether 20,000 people buy the same track or whether each member purchases whatever tracks they like.

And, like Amazon, a huge number of Rhapsody's total sales (45 percent) come from tracks that aren't also sold in traditional offline retail — suggesting that there's a demand for a bigger selection.

That's the essence of the "long tail" concept: a huge number of relatively unpopular products can rival a small number of very popular ones.

### 3. Making the means of production available to everyone generates more content and makes the tail longer. 

One factor determining the relation between supply and demand is the amount of content that's available. The more content released on the market, the longer the tail will be, and the digital age we live in has made creating and releasing content possible for almost everyone.

Indeed, the ubiquitous PC has enabled most people in the Western world to record and produce their own music, make a film or create an ebook. For example, in 2004–2005, the number of albums released increased by a staggering 36 percent, and more than 300,000 free tracks were uploaded to MySpace.

Furthermore, because the means of production are inexpensive, nowadays people are motivated to create content by a variety of factors, not only economic ones.

If a professional invests a lot of money in the production process, it makes sense that they'd take the economic factors seriously. But since today's production costs are next to nothing, the incentive of profitability is no longer an overriding motivation, and, as a result, much of the content out there is created for no other reason than fun, experimentation and curiosity.

Also, amateurs no longer only consume; they also produce. Think of Wikipedia, whose many thousands of contributors are largely amateurs. Together, these people have created the biggest encyclopedia in the history of the world. Just to give a rough idea of its breadth: whereas the _Encyclopedia Britannica_ has 120,000 entries, Wikipedia contains more than 4 million.

In the digital age, content is no longer something made solely by professionals for consumers, but can be created by anyone anywhere in the world with access to the very cheap means of production.

But creating content is only half the battle. The other half is making sure potential customers can find the content and, once they have, convincing them to buy it. In other words, _distribution_.

### 4. Making the means of distribution available to everyone allows for access to more content and fattens up the tail. 

It doesn't matter how much content there is in the world; finding what we're looking for isn't always easy.

Nowadays, distribution needs _aggregators_, i.e., companies like Amazon or eBay that have access to a massive amount of goods and can make them easy to find and buy. The more ways there are of searching for and finding niche products, the more of them can be sold.

The job of aggregators is to structure unorganized markets so that anyone searching for any existing product can find it. In short, they provide a means of distribution.

Imagine you're looking for a very specific book. You'd probably never consider going to a used bookstore as the chances of finding what you're looking for would be slim at best. But, if there were a way to search _all_ the used-book stores simultaneously, it's likely that you'd have much better luck — and you'd probably find that book for a competitive price.

For example, the online retailer Alibris created a database where bookstore owners could upload their inventories and customers could search them and place their order. The result was that desired and potentially rare products were much easier to locate.

But aggregators like Alibris aren't the only kind operating today. Rather, aggregators differ in the access they provide to niche goods. There are two main types: the _hybrid_ aggregator, which sells physical goods online (e.g., Amazon, Alibris or eBay) and the _digital_ aggregator, which sells only digital content online (like iTunes or Rhapsody).

In theory, digital aggregators provide almost unlimited access to niche goods because they have almost no economic limitations: it doesn't cost them extra to offer more.

But for hybrid aggregators like Amazon the economic constraints are still a reality. If a book sells only one copy annually, it still needs to be stored and it may cost more to offer it than not to offer it at all.

### 5. Helping customers filter their options enables them to find and buy what they really want. 

With today's superabundance of niche products and content, we need help in finding the content and products that suit our individual tastes.

That's where filters come in.

A genre-specific Top 10 list on iTunes; a blog about a new album; a "ranked" list of search results on Google; customer ratings on Amazon — all of these are filters that help us discover something we like among the glut of available products.

Of course, consumer life wasn't always like this. In the past, so-called _pre-filters_ predicted our behavior as consumers, filtering content and products before they even hit the market. But now _post-filters_ actually shape and stimulate consumer behavior, filtering whatever is already available on the market.

For instance, music executives are pre-filters of content, as they try to predict consumer behavior in deciding whether to sign a particular band. In contrast, a music blog, a customer review or an online playlist are all post-filters, shaping and directing the consumer's demand to the products and content they're seeking. And an effective post-filter will guide that demand toward niches, as doing this will enable the consumer to find content or products tailored to their specific needs and desires.

It's hard to imagine how confusing and overwhelming the online marketplace would be without post-filters like reviews or recommendations. Faced with a superabundance of goods, we as consumers use the experiences reported by others to help us narrow down our choices and find precisely the content and products we truly want or need.

In short, post-filters are the ideal connector between supply and demand. As a consumer in a saturated marketplace, you're ensured by post-filters that you're not being persuaded or coerced into purchasing something you don't really want. Instead, you're the one with full authority and control over what you buy.

### 6. Shelf-space is costly, forcing businesses to focus only on bestsellers – which means customers’ interests are neglected. 

Unlike traditional stores, online businesses are able to focus on the long tail because they don't have to worry about the usual overhead costs of retail — like shelf space.

In fact, shelves carry a lot of hidden costs. The average square foot of shelf space needs to generate between $100 and $150 per month for a company to break even.

Furthermore, shelves carry "opportunity costs" for the products a customer can't find. For instance, at your local supermarket there's a big chance you won't find what you're searching for because you're either looking in the wrong place or the product isn't clearly visible. In traditional retail, therefore, supply and demand are not always in balance.

To succeed, a traditional store has to focus on making its bestselling products the most accessible. Such businesses operate according to the _Pareto principle_, i.e., that only 20 percent of products for sale account for 80 percent of revenue. Hence, it's logical that a store should focus on that 20 percent so it can generate enough profit to offset the distribution costs.

Since such stores operate this way, only the most popular products can be placed on the shelves — which means reducing consumer choice. A movie rental store, for instance, pays around $22 per year to offer a single DVD, and few titles are rented frequently enough to break even.

The result is that traditional retailers can no longer cater to the diverse interests of consumers.

The average supermarket stocks around 30,000 items, arranged to make delivery and stocking easy and to extract the most profit from the smallest space. Yet each product can only be in one place at a time, even though any product — say, a can of tuna — could be "placed" in several categories: "low fat," "under $2," "canned food" and "fish." Another disadvantage for traditional retailers like supermarkets is that the product can't "pop up" as a match to other purchases — unlike in digital retail.

As you can see, while physical shelves are successful for traditional retail, they carry economic costs which don't exist in the long-tail strategy of online retail.

### 7. Internet-based distribution has lower inventory costs and can thus cater to individual needs. 

Because the internet can provide unlimited "shelf space" and a superabundance of choice, it's primed to give rise to long-tail economics.

Unlimited virtual shelf space means a business needs a lower profit margin to break even or make money. For instance, movie studios charge companies offering digital subscription services the same acquisition costs per title as they do per DVD in physical rental stores. Yet, unlike those stores, digital subscription services don't have to pay $22 per year to offer a title so they can continue to offer it indefinitely.

The unlimited shelf space of online retailers also enables them to provide the consumer with a massive choice of goods because no title ever has to be removed from the online catalog. 

This abundance of choice allows a company to satisfy the individual desires of many people without incurring any extra costs. And if a company offers nearly everything a consumer might want, its customers will be able to locate the products they like best. In traditional retail, such a company would most likely be a specialist store.

For example, consider the niche market of documentary films. Because documentaries appeal to very specific desires of consumers, you'll probably find only a handful of documentaries on offer in any given video rental store.

Netflix, on the other hand, is able to offer almost unlimited content, and, for that reason, it accounted for nearly 50 percent of the revenue for some documentaries in the United States in 2003.

But couldn't this model also work in traditional retail if they could find a way to avoid shelf-space costs?

In a word, no.

Studies show that, in traditional retail, a wide array of choice is confusing because it's unfiltered, which actually leads to customers buying less.

Because shopping _online_ comes with a "built-in" filter — a system of recommendations and rankings — it's relatively simple for customers to arrange a massive number of products in order of preference, thus making it easier for them to decide on a purchase.

### 8. From toys to advertising: niches and long tails exist in a lot of markets, not only entertainment. 

Most of the examples so far have been from the entertainment market. But the long tail as an economic concept, although prevalent in the media and entertainment markets, can be extended to most of the world's industries — among them, manufacturing and advertising.

First, let's take a look at the manufacturing industry and, more specifically, at LEGO. LEGO takes advantage of the long tail because it caters to niche markets and encourages users to create their own products. Its online store offers more than one thousand products, 90 percent of which aren't available for purchase in traditional retail stores.

Consider also its service, LEGO Factory, which made the creation of content accessible to everyone, not just professionals. Specifically, LEGO Factory allowed you to design your own LEGO models and upload them, after which all the bricks required to actually build your model would be sent to you.

Other LEGO enthusiasts could purchase the model you'd designed, and the more popular ones were even released as official LEGO products. Before the program was discontinued, more than 100,000 LEGO kits had been designed using LEGO Factory.

Now consider the long tail in the world of advertising — specifically, Google's self-service advertising model. Instead of providing just a couple of ads that are triggered by the most popular search terms, Google sells millions of ads that correspond to the millions of unique search terms.

In fact, the top ten search words on Google account for just 3 percent of the total searches, while the other 97 percent covers tens of millions of other search terms.

In order to advertise your product or business with Google, companies must purchase keywords in an automated auction. Of course, using a rare and specific search term for your ad costs much less than choosing a common one. And because the ad is text only, with no images or banners, it's also inexpensive to generate and place on websites.

### 9. To use the long tail in your own business, maximize your offer and help customers find what they want. 

Now that you've discovered the benefits of the long tail, you're probably wondering how you can incorporate it into your own online business strategy.

To take advantage of the long tail, the first step is to centralize and expand your inventory to reduce the costs of making so many products available.

Having a centralized inventory is the cheapest way to offer a variety of products online. The actual physical location of a product is irrelevant as long as it's presented alongside the other products you offer. Even better, you could make your inventory entirely digital, as iTunes and Rhapsody do.

Also, if you exclusively sell digital media, like music, ebooks and movies, you should offer the same product in a variety of digital formats (such as mp3, flac, m4a and wav). Customers' preferences will differ, so by doing this you can further increase the chances of meeting any customer's specific needs or desires.

The second step is to make sure your potential customers can access your services or products whenever and from wherever they like.

For example, consider the various ways that people can watch TV shows nowadays: on-demand, streaming, downloading (iTunes) and on DVD (rental and purchase). The time when TV programming could influence the schedules of vast numbers of people has definitely passed. Today, most people are able to watch their favorite shows whenever they like, as is true of their other pastimes and hobbies.

Therefore, the best distribution system is one that makes it possible to buy a product independent of time or place. Just think of Amazon, eBay or iTunes, all of which are totally accessible to everyone, everywhere all the time.

### 10. A niche marketing campaign is more effective than a traditional one-size-fits-all advertisement. 

Long-tail marketing targets niches rather than the largest possible audience. Because of this, it's able to appeal to potential customers in ways that are specific to those customers.

One such marketing strategy is consumer-generated advertising, which can help to define a brand's identity and stimulate a dialog around it.

For example, when Chevrolet's advertising agency had to come up with a web advertisement, they chose to use consumer-generated advertising, allowing the general public to create their own TV ads using prepared professional footage.

More than 30,000 videos were created, many of them fitting the company's initial aims. However, some of the ads were subversive, tackling global warming, the oil crisis and social irresponsibility. These ads created a viral dialog, increasing consumer awareness of the brand and leading to the average Tahoe selling in just 46 days, compared to the previous year's four months.

Another niche-marketing strategy is based on the idea that if you reveal people's individual and unique experiences, others will be able to relate.

For instance, in 2005 Jeff Jarvis blogged about his frustrating experiences with Dell computers under the catchy title "Dell Hell." Quickly, the mainstream media picked up on it. Soon after, "Dell Hell" had become part of the online vernacular and nearly 2,500 comments were added to Jarvis's blog posts.

As a result, Dell now has an entire team dedicated to finding any mentions of problems with their computers on the web. This means that if a Dell user has a blog and reports on an unresolved problem with their PC, it's very likely that an employee of Dell will get in touch with you to resolve the problem.

Microsoft, too, transformed its image with Channel 9, a webpage for Microsoft employees to blog and discuss their jobs. Channel 9 attracts around 4.5 million unique visitors per month.

Rather than presenting the image of a monolithic company, Microsoft now offers complete transparency: customers can explore any area that interests them, be it Xbox programming, font usage in the Media Center or SQL issues.

### 11. Final Summary 

The key message in this book:

**The future of culture and commerce is in niche products with a small but specific demand. In short, the future is in the long tail of the demand curve. Focusing on the long tail is a strategy that the most successful companies of the last decade all have in common. The monopoly of the hit — the wildly popular product with the broadest audience — is a thing of the past.**

Actionable advice:

**Make it easy for people to find your product.**

For any product or content imaginable, there's always someone out there who wants it. So if you want to find those potential customers, you have to make sure that they can find your product. In the world of online retail, you can achieve this by listing it on Amazon, selling it via iTunes or using Google's self-service advertising system.

**Diversify your product range.**

Rather than wondering how to make your product appeal to a greater number of people, you could instead try to create variations on it that will cater to a wide array of different tastes. For example, if your company produces beer, you could also offer a gluten-free option or a selection of non-alcoholic beers.
---

### Chris Anderson

Chris Anderson is an author and entrepreneur. Previously, he was the Business Editor at _The Economist_ and Chief Executive Editor at _Wired_ magazine. In addition to _The Long Tail_, Anderson has also published _Free: The Future of a Radical Price_ and _Makers: The New Industrial Revolution_.

