---
id: 581f1c7bf52436000382a5e9
slug: the-longevity-project-en
published_date: 2016-11-10T00:00:00.000+00:00
author: Howard S. Friedman, PhD, and Leslie R. Martin, PhD
title: The Longevity Project
subtitle: Surprising Discoveries for Health and Long Life from the Landmark Eight-Decade Study
main_color: AC3B37
text_color: AC3B37
---

# The Longevity Project

_Surprising Discoveries for Health and Long Life from the Landmark Eight-Decade Study_

**Howard S. Friedman, PhD, and Leslie R. Martin, PhD**

_The Longevity Project_ (2012) is about the impressive and revealing Terman Study, which followed a group of people for eight decades in order to find out what habits and practices helped them live long and healthy lives. These blinks explain why marriage might not be as healthy as you think, and what you can do to improve your chances of happiness and longevity.

---
### 1. What’s in it for me? Learn if you’re on track to live to a hundred. 

In 1921, Dr. Lewis Terman, an American psychologist, started a study he called the Genetic Studies of Genius, which aimed to find the determining factor behind genius by observing the development of precocious children into adulthood. Terman considered all information that seemed relevant, like how many books the children had in their home or how they spent their playtime. Over time, the study amassed a wealth of data.

The study, known as the Terman Study, continued after Terman's death, and when the authors began working with it in the 1990s, they were very interested in the data — but for reasons of their own. They wanted to use it to answer another question we humans have asked for millennia: What factors contribute to a long life? One surprising finding is that your personality has a major influence on your lifespan. But that's just one discovery among many. What else did they learn?

In these blinks, you'll find out

  * what impact being conscientious has on your life expectancy;

  * why a man's lifespan benefits more from marriage than a woman's; and

  * what effect religious faith has on longevity.

### 2. Conscientiousness is a characteristic that helps ensure a long life. 

Have you ever been teased by easygoing friends for being stubbornly organized and always playing it safe? If so, you may end up having the last laugh, because this kind of behavior is part of being conscientious, a characteristic that can lead to a long and healthy life.

In children, one of the best indicators of longevity is conscientiousness, or carefulness. But even if it doesn't develop until later in life, conscientious behavior can still have a positive impact.

When Dr. Terman considered these effects, he looked at the behavioral patterns of his subjects twice: once at the beginning of the study, when they were children; the second time when they were adults, 20 years later.

One of the subjects of the Terman Study was Patricia: She was a conscientious child and remained that way until she died in her nineties.

Another subject was James: He wasn't a conscientious child but as he got older his habits changed and he became more detail and goal oriented. Over the course of 20 years he had moved from the bottom 25 percent to the top 25 percent in the conscientious score, and, as a result, he lived well into old age.

Researchers still aren't sure why conscientious people are less likely to die from disease, but there are three possible reasons why they tend to live longer:

First of all, by nature, conscientious people are not risk-takers and they lead healthy lives. This means they are unlikely to drink, smoke, do drugs or drive recklessly.

The second reason has more to do with biology and the chemical composition of the brain. Research suggests that these types of people have higher levels of _serotonin_, a neurotransmitter that helps you feel good and stabilize your mood, and keeps you from making dangerous and irrational decisions.

Lastly, being conscientious can also lead to healthier personal and professional relationships. So, they're not only happier and healthier individuals, they also attract partners, friends and jobs that help them stay that way.

### 3. We shouldn’t confuse cheerfulness with happiness. Happiness stems from positive lifestyle choices. 

As the old saying goes, laughter is the best medicine. And if you've ever been battling a cold or feeling blue, you can probably agree that a good laugh can go a long way to helping you feel better. But how does laughter fit in with living a long life?

As it turns out, the cheerful participants in the Terman Study were actually _less_ likely than their counterparts to live a long life.

The common assumption is that happy people live longer. But one shouldn't assume that laughter equals happiness.

The study revealed that cheerful children often grew up to smoke more cigarettes, drink more alcohol and engage in riskier hobbies than people who were not considered cheerful.

Take Paul, for example: as a child, his parents and teachers described him as optimistic, cheerful and worry-free. Yet, despite his laughter, Paul didn't outlive other participants who were more serious and less joyful.

After all, being cheerful is different than finding happiness, which usually only happens when you make the right lifestyle changes.

Those who research happiness tend to offer similar suggestions on how to bring more happiness into your life — watching less TV, being more physically active, practicing gratefulness, helping others and improving your social relationships.

These suggestions can help you live a longer life as well, but the authors discovered that the best results came from those who changed their overall lifestyle to naturally fit these activities into their daily routine.

So, rather than relying on a television show for some much needed laughter, you can combine these suggestions to regularly spend evenings laughing with friends and strengthening your social bonds. Not only will this be more fun; it will be healthier as well.

Most participants in the study didn't go out of their way to find happiness. It came as a byproduct of having a satisfying lifestyle and being healthy, wealthy and wise.

They found that strong social connections, a good career and interesting hobbies were the qualities that brought happiness and led to a long and healthy life.

> _"Healthy people are happy but happy people are not necessarily healthy."_

### 4. When parents divorce, their children need to be resilient in order to combat the negative impact. 

When people talk about divorce, they often say that those hit hardest are the children.

Unfortunately, this is true when it comes to longevity as well, since the children of divorced parents die almost 5 years earlier than children of parents who stayed together.

For this reason, the study showed that divorce during childhood is the strongest social indicator of an early death — a remarkable find, since it predicts something that is still decades away.

Divorce affects longevity by increasing the likelihood of someone engaging in unhealthy behavior, such as smoking and drinking. It also makes the child more likely to later enter a marriage that will also end in divorce.

This is what happened to Donna: When she was ten years old, her parents divorced and Donna and her brother remained with their mom.

In college, Donna started smoking, and though she maintained a successful career, her marriage fell apart and resulted in divorce.

With her job and the added stress of providing financial security for her children, Donna's social life ending up in shambles, with few friends and little time for socializing. Donna died at the relatively young age of 59.

While children can't control whether their parents get a divorce or not, they still have the chance to live a long life by practicing the power of _resilience_.

Resilience is what gives you the strength to overcome difficult events, and it is the characteristic that the authors saw in children who overcame their parents' divorce and went on to live long and happy lives.

Resilience is usually developed in the first years of adulthood, and the most important contributor to this quality is the sense of being satisfied with your life.

The authors found that participants who were proud of their achievements and believed that they were living up to their potential, despite the divorce of their parents, were healthier and lived longer.

### 5. Men’s lifespans are more affected by marriage and divorce than women’s. 

Marriage is a life-changing event for both members of a partnership — but you might be surprised to find out that it impacts the man's life expectancy more.

In fact, men who stay married tend to live longer than those who stay single or get divorced.

Of the male participants in the study, those who remained married usually lived to be at least 70 years old. In contrast, only a third of those who got divorced lived to see that age, while none of the men who remarried lived this long.

The authors looked at two possible explanations for this trend: The first is that a wife can provide life-saving help during an illness or emergency; the second is that wives also tend to encourage healthy habits in their husbands.

However, shouldn't remarried men experience these benefits as well?

Since divorce is one of the biggest social stressors, the pressure doesn't stop once someone remarries, and it can trigger harmful habits. Remember, conscientious people are more likely to have happier marriages and not be divorcees.

On the other hand, for the women in the study, there was hardly any difference between staying married or remaining single after a divorce.

While men usually live longer as a result of getting married in general, the life expectancy of women only improved if they were in a truly great marriage. Therefore, divorced women who stayed single lived nearly as long as those who stayed married.

The results of the Terman Study disprove the popular advice that women should get married if they want to live longer. Actually, if women are content with their lives and have a healthy social circle that keeps them active and engaged, there's no harm in staying single. So they shouldn't feel pressured to marry, but their chances of living longer _does_ increase if they enter a good marriage that allows them to thrive.

> **"** _Statistically, 100 percent of all divorces start with marriage."_

### 6. For living a long life, being religious isn’t as important as having strong social connections. 

Aside from marriage, many people consider religion a key part of a long life. But this popular belief is also only partially true.

The study showed that any positive effects religion had on health and longevity were highly individual and not the result of prayer.

And the relationships participants had with religion were quite varied. Some, like Linda, were very religious; others, like John, hardly ever set foot in a church. There were also folks like Donna — people who drifted away from religion as they got older.

Of these three, both Linda and John ended up living a long life, whereas Donna died prematurely.

Religion isn't without its health benefits. It does have a positive influence, but this comes mainly from the community and social life that religions can provide.

The men in the study gave more importance to their families and careers than to religion, because, when it comes to social life, men tend to rely on their wives.

For women, the community that religion provides was of great importance. Women like Donna, who become less involved in religious activity, can experience a feeling of isolation and begin to engage in unhealthy behavior.

And the Terman Study makes clear that building social connections can definitely help you live a longer life.

John, for instance, who wasn't religious at all yet still managed to live a long life, had plenty of strong social connections so he didn't need the health benefit of religion.

Another important finding of the study is that merely _feeling_ socially connected doesn't provide the same benefits as actually having meaningful connections with other people.

So, having thousands of friends on Facebook probably isn't as important as having a small network of close friends you can count on and to whom you can turn for help when needed.

### 7. There are many different paths to a long life, and by following some simple guidelines you can create your own. 

In the previous blinks you've heard many stories about the different paths people have taken, and you might be thinking, "That's great, but what is the best path I can take to make sure I live a long life?"

Well, there is no one path that works for everybody. But there are two major ones that we will call _the high road_ and _the road less traveled_.

Patricia, the conscientious child who lived to her nineties, is a perfect example of taking the high road. Along with her careful nature, she entered into a good marriage and maintained strong social connections. Like others who take the high road, Patricia liked to plan things out and remained persistent in her relationships, all of which contributed to her long life.

As the name suggests, the road less traveled is a very different path — one for people with less concern for social conventions.

Consider Emma, who, having never married, was passionately devoted to her career and to keeping a close group of friends and family that she could rely on when times got tough. Emma lived a long and healthy life and never saw marriage as something that would make her life complete; instead, she remained a pleasant and spirited woman who grabbed opportunities when they came.

Since everyone is unique, you can create your own path by keeping some common themes in mind.

First of all, you have to find what's meaningful and interesting to you and shape your path accordingly. Know that it's going to take hard work, persistence and strong social bonds to live a long life.

Along the way, you'll need to actively pursue your goals so you can be satisfied with yourself and maintain a strong sense of accomplishment.

It will be hard, but you can make it easier by finding something that excites you and makes you want to jump out of bed in the morning. With this kind of passion, you'll not only find that your quality of life improves — you'll probably live longer, too.

### 8. Final summary 

The key message in this book:

**There isn't one simple trick to living a happy and healthy life, no matter what you might hear or read these days. And enjoying life well into your later years will take more than just a healthy diet and regular exercise. If you really want to live to be a hundred, there are many things to consider, such as having a career that you enjoy, forming strong social bonds and finding a purpose in life and feeling satisfied.**

Actionable advice:

**Change your catastrophic thoughts.**

When something goes wrong, do you tend to think it's a temporary, limited problem, or do you immediately think that your whole life is a mess? The Terman Study found that people prone to catastrophic thoughts like the latter tend to die sooner. So clearly this is something to avoid. Luckily, it is possible to adjust your thinking. The first step when facing catastrophic thoughts is to recognize your thoughts for what they are — just thoughts. The second step is to literally tell yourself "Stop!" But since it's very difficult to stop thinking about anything at all, you then need to replace the negative thoughts with positive ones.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Blue Zones_** **by Dan Buettner**

The Blue Zones (2012, first published in 2008) whisks you through the regions of the world with the highest concentrations of healthy centenarians. By examining how people in these regions live and interact, we gain insight into how to extend our own lifespans.
---

### Howard S. Friedman, PhD, and Leslie R. Martin, PhD

Howard S. Friedman, PhD, is a psychology professor at the University of California whose work in health and longevity has won him awards from the Association for Psychological Science and the American Psychological Association. His other books include _Health Psychology_ and _The Self-Healing Personality_.

Leslie R. Martin, PhD, a psychology professor at La Sierra University in California, is a recipient of the Anderson Award for Excellence in Teaching and the Distinguished Researcher Award. She specializes in finding ways to improve the physician-patient relationship and in forging psychological paths we can follow to live longer and healthier lives.

