---
id: 5947e3c7b238e10005eecf21
slug: the-looming-tower-en
published_date: 2017-06-22T00:00:00.000+00:00
author: Lawrence Wright
title: The Looming Tower
subtitle: Al-Qaeda and the Road to 9/11
main_color: D7AB6A
text_color: 80653F
---

# The Looming Tower

_Al-Qaeda and the Road to 9/11_

**Lawrence Wright**

_The Looming Tower_ (2006) is all about al-Qaeda, its formation, and the personalities behind it. These blinks detail the route taken to power by al-Qaeda leader Osama bin Laden and the run-up to the September 11, 2001 terrorist attack that devastated the United States.

---
### 1. What’s in it for me? Learn the long history of 9/11. 

For years, we've read reports of the increasing radicalization of Islam, of terrorist attacks and insurgent wars throughout the Middle East. The main narrative for many in the West began with the 9/11 attack over 15 years ago. Al-Qaeda became a household name, but was the attack on the World Trade Center really the starting point?

In these blinks, we'll attempt to trace the radicalization of Islam in general and the formation of al-Qaeda in particular. We'll follow the instigators and intellectuals who crafted the movement's ideological base, why they did it and how.

In these blinks, you'll learn

  * why mid-century Egypt was the perfect hotbed for a new, radical variant of Islam;

  * how Ayman al-Zawahri introduced a new ideology of religious violence; and

  * why the United States wasn't al-Qaeda's first target.

### 2. The 9/11 attacks were the culmination of longstanding tensions and the work of three masterminds. 

The September 2001 attacks were an unconscionable tragedy in American history, but also the climax of a long conflict between Islam and the West. When two planes collided with the World Trade Center in New York and a third struck the Pentagon in Washington D.C., 2,996 people were killed, another 6,000 were injured, and the catastrophe launched the so-called "War on Terror."

But how did such a horrific event happen in the first place?

Well, the emergence of al-Qaeda, the group responsible for 9/11, was the result of a unique convergence of three personalities. The first man to play a key role was Sayyid Qutb, the intellectual father of the Islamist movement and a great influence on the other two primary actors.

The second was Ayman al-Zawahiri, the current leader of al-Qaeda and a promoter of the apocalyptic idea that only violence and vengeance could transform history. The third, and most notorious, was Osama bin Laden, the founder and former leader of al-Qaeda.

When bin Laden first formed al-Qaeda in 1988, it wasn't clear that it would become the powerful terrorist organization it is today. In the early days, there was disagreement about the organization's trajectory. Abdullah Yusuf Azzam, another al-Qaeda founder, wanted to use the group to win back lands once held by Islamic peoples from Soviet Central Asia to Bosnia, while Zawahiri dreamt of beginning a vicious cycle of terror and repression in Egypt, a vision not necessarily shared by Azzam and bin Laden.

A long and complicated history led to the group becoming what it is today. Next up, you'll learn all about these three key figures and the reign of terror they launched.

### 3. One of the pioneers of Islamic fundamentalism emerged from the hotbed of 1940s Egypt. 

In the 1940s, Egypt was under British occupation, with the complicit King Farouk at its head. While Egypt was staring down crippling poverty, unemployment, disease and illiteracy, Farouk was driving around Cairo in one of the 200 red cars that only he was allowed to own, openly flaunting his wealth.

It was this blatant disregard for his people, combined with the British occupation, which caused the formation of an underground Islamic group called the _Muslim Brotherhood_. They founded their own hospitals, schools, welfare societies and even factories, rejected the idea of a secular, democratic government and fought for universal Islamic rule. By 1948, they had over one million members and supporters in a country of only 18 million.

During these historic years, Sayyid Qutb was coming of age. Born in 1906, he would become an influential Egyptian writer, thinker, theorist and teacher.

Growing up, Qutb loved classical music and Hollywood movies. He was also a big fan of French authors like Victor Hugo, great thinkers like Einstein and Darwin, and British writers Byron and Shelley. In 1948, Qutb went to America to study its education system, attending school in Washington D.C. and Colorado.

Previously, he had been a serious Egyptian nationalist, slowly forming notions of Islamic fundamentalism. His hatred of America was less pronounced at this time, and he saw the United States as a land of immigrants who had broken free from British colonial rule.

However, Egypt and the armies of five other Arab states were losing a war that would lead to the establishment of the Jewish state of Israel in the heart of the Arab world. The resentment that Qutb would come to feel so powerfully toward the US government really took shape when America threw its support behind Zionism in 1948, in what was seen by Qutb and countless other Arabs as an act of betrayal.

> _"I hate those Westerners and despise them! All of them, without any exception: the English, the French, the Dutch, and finally the Americans, who have been trusted by many." - Sayyid Qutb_

### 4. Sayyid Qutb’s writings would become the intellectual framework for Islamic fundamentalism. 

When Qutb arrived in America, he was immediately disturbed by what he viewed as a corrupt and godless culture. He was mortified by America's rampant sexuality, materialism, racism, superficiality and extraordinary emphasis on individual freedom.

But more specifically, he was offended by the way American capitalist society solely addressed the material needs of humans while neglecting the needs of their spirit. When he returned to Egypt in 1950, he was more radical than ever.

He now equated America with everything that was wrong with the modern world, from secularism to rationality, democracy, individual materialism, the intermingling of the sexes and tolerance for others. He saw Islam as the antidote to this corrupt vision of society, offering a complete system of laws, social norms, economic and governmental rules for a society that was both enlightened and righteous.

As Egypt began to turn toward a secular political vision, Qutb planted the seeds of Islamic fundamentalism. It began in 1952 when Gamal Abdel Nasser seized control of Egypt's government, overthrowing King Farouk. While Nasser and Qutb attempted to work together at first, with Qutb even accepting a job at the ministry of education, their political visions were at odds.

Nasser envisioned a modern, egalitarian, secular and industrialized pan-Arab Egypt. Naturally, this thought was horrifying to Qutb, and the two men were soon at each other's' throats.

Not much later, in 1964, Qutb published _Milestones_, a text calling for all-out war against the non-Islamic world to establish the universal reign of Islam. His argument centered around the premise that the world was in _Jahiliyyah_, or defiance of God.

But one of his most extreme ideas was that individual devotion would not free one from sin or defiance. He said that even if a person was a devout Muslim, to tolerate or obey institutions that stood in defiance of God was to defy the divine itself.

### 5. Zawahiri, a key figure of the Islamic revolutionary movement, was deeply influenced by Qutb. 

You can only openly defy those in charge for so long. Eventually, Qutb was imprisoned by the Nasser regime for his defiance. In 1966, his execution by hanging secured his place as a martyr in the burgeoning movement for an Islamist revolution.

Qutb, his martyrdom, and his ideas would have a particularly profound influence on Zawahiri, whose uncle was a pupil and protégé of Qutb. At the trial that resulted in Qutb's execution, Zawahiri's uncle even served as the future martyr's lawyer.

But first, more about Ayman al-Zawahiri. He was an Egyptian doctor, Jihadist and a central figure in the development of al-Qaeda. Born in Cairo on June 19, 1951, Zawahiri's ultimate goal was to breathe life into Qutb's ideas.

Zawahiri's background and ideological growth are key to understanding al-Qaeda's ideals.

He was a devout and bookish middle-class kid who, at the age of just 15, helped form his first Islamic revolutionary cell. That cell would go on to become a key building block of _al-Jihad_ or Islamic Jihad.

This underground group, along with the Muslim Brotherhood and a third organization called the Islamic Group, was dedicated to the overthrow of the secular Egyptian government and the establishment of an Islamic state that abided by _Sharia_ law. This legal code, drawn from the Koran and the teachings of the Prophet, lays out rules for the governance of every aspect of life.

In 1981, Zawahiri was imprisoned for selling weapons and for having had a hand in the assassination of the former Egyptian president Anwar al-Sadat, who had been killed by al-Jihad. These offenses led to Zawahiri's torture in Egyptian prisons, an experience that hardened his resolve and taught him the importance of vengeance.

Egyptian prisons became a networking tool for Jihadists. The cruel practices of Egyptian jailers would be redirected back at the government a thousandfold by people thus radicalized.

### 6. Zawahiri was essential to initiating and justifying extreme violence in the radical Islamist movement. 

So, Zawahiri was a key figure in the development of radical Islam and his revenge on the government of Egypt took a powerful form. After getting out of prison in the early 1990s, Zawahiri launched a campaign against Egypt's government, making frightening use of the suicide bombers that would become key to radical Islamism.

In 1995, Zawahiri planned a suicide bombing at the Egyptian embassy in Pakistan. In this incident, 18 people were killed, including the two bombers.

It was an attack that Zawahiri justified by arguing that the anti-Islamic policies of the Egyptian government meant that all those working at the embassy deserved to die. He believed that the deaths of innocent Muslim bystanders were simply an unfortunate and necessary form of collateral damage.

This is theologically difficult — not least because Islam prohibits suicide — but Zawahiri used the history of a group of Muslim martyrs who were given the choice to either renounce their faith or die, as his reasoning. They chose death, and other Muslims accepted the decision as a heroic act of martyrdom since it was done out of devotion to God.

But suicide bombings justified through suspect means weren't the only tool of Zawahiri and his violent, fundamentalist vision of Islam. In 1995, in Sudan, he convened a Sharia court of his own devising to try two teenage boys for treason, sodomy and attempted murder.

He had the boys stripped naked to demonstrate that they had reached puberty and should be tried as adults. The court found them guilty, Zawahiri had them shot, and both their confessions and executions were videotaped and publicly distributed as a warning to other traitors.

What had the boys done to deserve this fate?

They had been working for the Egyptian government against Zawahiri and his organization after the group had nearly killed President Hosni Mubarak.

This is an example of Zawahiri's insane machinations, which are of particular importance since he would go on to become the mentor of the third key figure in al-Qaeda: Osama bin Laden.

### 7. Osama Bin Laden’s path to the top of al-Qaeda was far from straightforward. 

Today, the name Osama bin Laden is synonymous with terrorism, murder and Islamic fundamentalism. But that wasn't always the case.

Born in 1957 to the heir of a tremendous Saudi fortune, Osama was a shy boy with a deep love for American TV. As a teenager, he became more solemn and religious and eventually came under the influence of a charismatic Syrian gym teacher who belonged to the Muslim Brotherhood.

But even after al-Qaeda's official formation, it took bin Laden years to settle on an action plan. Here's how the organization came into being:

In 1968, Zawahiri and bin Laden met in Peshawar, Pakistan. However, al-Qaeda was officially founded during a further meeting in 1988, when bin Laden, Abdullah Azzam and Zawahiri's organization, al-Jihad, joined forces in the service of _jihad_, the fight against the enemies of Islam.

Even then, bin Laden was unsure, and the lure of peace was as strong as the cry of battle. After all, he wasn't opposed to American culture. He let his young sons play Nintendo, and al-Qaeda trainees often watched Hollywood thrillers in their down-time, particularly those featuring Arnold Schwarzenegger. One of bin Laden's wives even used American brand-name cosmetics and lingerie while another had a doctorate in child psychology.

Aside from that, when bin Laden was exiled from Saudi Arabia to Sudan in the early 1990s for speaking out against the House of Saud, he became enthralled with agriculture and is said to have told several friends that he was considering quitting al-Qaeda to take up farming. He bred horses, took his sons on picnics, grew prize-winning sunflowers and, at the young age of 34, seemed to be at peace.

In other words, al-Qaeda, which we now consider a brutal terrorist organization, had become an agricultural institution, with members working on different farming projects and playing soccer together after Friday prayers. But of course, that didn't last.

### 8. Bin Laden attacked the United States because of its political and military actions in the Islamic world. 

When al-Qaeda was formed, it was from the remnants of several underground Islamic groups that had once fought some of the greatest enemies of the United States. These included the _mujahideen_, or insurgent Jihadists, who fought in the Soviet-Afghan War, which took place from 1979 to 1989.

So how did these groups become the sworn enemies of America and why did they launch the attacks on 9/11?

Well, American intervention in the Middle East deeply angered bin Laden. The US military occupation of Saudi Arabia and Iraq during the First Gulf War and America's continued support of Israel were evidence to bin Laden and Zawahiri of a war on God and Muslims, as well as the exploitation of Muslim land by American forces.

So, in 1998, he issued a _fatwa_, or an Islamic ruling, called "Jihad Against Jews and Crusaders." It told Muslims to kill Americans, wherever they might be.

From there, 9/11 was a strategic move on the part of bin Laden to lure America into the invasion of Afghanistan. Before this major strike, bin Laden launched attacks in 1998 against American embassies in Africa, and in 2000 against the US Navy ship, _Cole_, to provoke American aggression in Afghanistan. This region, often referred to as the "graveyard of empires," is a land where invaders like the Soviet Union and the British Empire had failed.

Once US troops were deployed, the strategy was to swarm them with the mujahideen, bleeding the American military until the empire collapsed.

However, when neither of these smaller attacks provoked the American retaliation bin Laden desired, he opted instead for a much larger strike: the attacks of 9/11.

So, while September 11, 2001 may seem like the beginning of the story for many of us in the West, it's actually the culmination of a long history influenced by innumerable sociopolitical, religious and historical factors.

### 9. Final summary 

The key message in this book:

**The radical Islamic ideology of al-Qaeda didn't emerge from a vacuum. Rather, this terrorist group grew out of feelings of political, cultural and social humiliation at the hands of the West, culminating in the devastating attacks of 9/11.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Going Clear_** **by Lawrence Wright**

_Going Clear_ offers a rare glimpse into the secret history and beliefs of Scientology as well as the conflicted biography of its founder L. Ron Hubbard. It also details some of the Church's darker qualities: a tooth and nail method of managing criticism and systematic approach to celebrity recruitment.
---

### Lawrence Wright

Lawrence Wright is a staff writer at the _New Yorker_ who has written a number of other non-fiction titles. He received the Pulitzer Prize for _The Looming Tower._

