---
id: 58d12492ed3a570004158157
slug: the-lucky-years-en
published_date: 2017-03-23T00:00:00.000+00:00
author: Dr. David B. Agus
title: The Lucky Years
subtitle: How to Thrive in the Brave New World of Health
main_color: 609754
text_color: 497340
---

# The Lucky Years

_How to Thrive in the Brave New World of Health_

**Dr. David B. Agus**

_The Lucky Years_ (2016) is your guide to understanding the cutting-edge developments in medical science which are addressing society's most pressing health problems. While advances in genetics may seem to be the key to curing cancer, infertility and aging, these blinks show that simple, sensible health strategies may more effectively improve the health and happiness of the world's population.

---
### 1. What’s in it for me? Better understand the benefits of living during an age of advanced medicine. 

If you visited a doctor during the early twentieth century, you would have received the standard treatment for people with your sort of ailment.

Yet with technological development, medical treatments today have changed. You are now much more likely to have your cure tailored not only to your specific illness but also to you personally.

But what exactly does this entail? These blinks will shed light on new medical treatments and the revolutionary ways that doctors are fighting chronic conditions, in addition to introducing everyday health practices to keep us fit.

It's time to toss out preconceived notions of health and wellness and immerse yourself in the new world of modern medicine.

In these blinks, you'll learn

  * which techniques are being developed to cure cancer;

  * how having a test-tube "threesome" can combat infertility; and

  * why health is keeping 70 million Americans from performing their best at work.

### 2. Advances in genetics may give us tools to defeat cancer, but using such tools is still risky. 

When Apple's Steve Jobs was diagnosed with cancer, he sought out every possible cure for his illness.

Author Dr. David Agus was part of the medical team in 2007 that tried to return Jobs to health, using a cutting-edge approach to genetics.

By sequencing cancerous cells, scientists can identify the areas of a cell's DNA that cancer seeks out and damages. This, in turn, allows doctors to prescribe cancer medication that is highly specific, targeting only the defects revealed by cell analysis.

Unfortunately, cancer is cunning. Damaged cells can mutate and render prescribed drugs ineffective. A new analysis and course of medication then have to be developed, and the process repeats itself.

Ultimately, Agus's team was unable to save Jobs's life. But their work marked the beginning of a journey into the possibilities of genetic medicine.

One gene editing tool is leading the way, called _CRISPR: Clustered Regularly Interspaced Short Palindromic Repeats._

In the near future, CRISPR could allow scientists to effectively delete a specific gene that is involved in the functioning of a person's immune system, the CCR5 gene. Doing so could make a person immune to HIV, for example, protecting them from contracting AIDS.

The risk is that such genetic modifications are permanent. The genetic strand that carries the CCR5 gene could mutate, for example, making a person more likely to contract other diseases, such as the West Nile Virus.

There is still too much we don't know about the dangers of gene editing to use such tools in treating patients. But new developments and research may change that.

For example, cancer isn't the only area of study that has captured the imagination of genetic scientists. Researchers are also making new discoveries in the biology of human aging.

> _"It has been only a decade since we read the first human genome. We should exercise caution before rewriting it."_

### 3. Genetics may hold the key to explaining why some people seem “younger” than their real age. 

The question, "How old are you?" has more than one answer. While most of us consider age to be the number of years since birth, we also have biological "ages" that define our overall health.

Think of a rosy-cheeked 30-year-old who still needs to show identification when buying a beer, while a mature-looking 21-year-old has no problem grabbing a six-pack without being carded!

In 1972, scientists in New Zealand began the Dunedin Study, tracking the health of 1,000 infants as they aged. When participants reached the age of 38, researchers met each person for a general and comparative health check.

Researchers considered multiple factors, including vital organ function, cholesterol level, dental health and immune system efficacy in calculating the difference in participants' _biological age,_ even though their "real" age was the same — 38 years old.

The results were enlightening. While most participants aged fairly normally, some exhibited the physical strength and resilience of a younger 28-year-old.

In contrast, other participants showed signs of exhaustion normally seen in people in their 60s. Some had even aged as much as three biological years in a single chronological year!

Scientific studies into biological age today are increasingly precise, and scientists have even developed the capability to calculate the age of a person's organs.

Researchers at Boston University, for example, have come up with an online calculator that helps you determine your heart's biological age by answering a few questions. A 2015 report based on this data revealed that three out of four Americans have a heart that is at least five biological years older than the individual's chronological age.

Some people, on the other hand, have astonishingly "young" hearts, and these are the people that scientists are most interested in studying.

Genetics can help researchers work out which parts of human DNA are responsible for extending the biological youth of our bodies. For the time being, however, research continues on this fascinating question.

### 4. New technologies show promise in addressing genetic infertility, but many questions remain. 

Thanks to technology, many infertile women today can conceive and deliver healthy children. However, other fertility solutions based on genetic research are still in development and can come with considerable risk.

Doctors have found that infertility in some women can be traced to defective cellular _mitochondria_. Mitochondria are small structures in human cells that generate the cellular energy a body needs to function. Mitochondria have their own DNA; when this genetic material is defective, however, muscular and neurological disorders can result.

Crucially, women who have defective cellular mitochondria are still able to conceive via in-vitro fertilization (IVF), for example, but their offspring are likely to be born with degenerative illnesses.

To circumvent this, scientists have developed an IVF method that combines not only the egg and sperm of the parents to be but also a sample of healthy mitochondria from a second, fertile woman. This "three-way" solves a genetic problem by introducing healthy mitochondria to address the cellular defects in the infertile woman's egg.

While promising, the impact of modifying human molecular structures with methods like three-way IVF are still unclear.

In 1997, New Jersey doctor Jacques Cohen successfully used mice in a three-way IVF experiment. Later he tried the method with humans, bringing 17 babies into the world. Three of the offspring, however, were either autistic or suffered from Turner syndrome, a genetic defect in female infants that can lead to infertility and stunted growth.

While the health progress of the other children wasn't monitored, experiments with mice have shown that mice produced from three-way IVF can develop cognitive defects in middle age.

It's yet unclear how the development of genetic abnormalities or defects are related to the process of three-way IVF, but nonetheless, the procedure is still too risky to perform on a wide scale.

> _"The idea that we can edit our genes might make Darwin shift in his grave."_

### 5. Preventative medicine is both smart and sustainable, and too often ignored in Western culture. 

There's nothing quite so appealing as a comprehensive health solution that promises to fix problems quickly with minimum effort. The problem is that there's just no way to guarantee global good health as all human beings are "built" differently.

Doctors and scientists thus are limited to making blanket statements about health. The author, for example, used to recommend taking a "baby" aspirin daily and using cholesterol-lowering drugs called _statins_ to maintain long-term health.

Large-scale studies have confirmed that taking such medications regularly can help prevent heart disease and general inflammation. Yet such treatments can also cause negative side effects in certain people.

This doesn't mean that the people who indeed benefit from such medication should stop treatment, but the problems indicate a need for better alternatives for regulating cholesterol.

We can identify an alternative, perhaps, in society's approach to medicine overall. People from Western cultures can learn a lot about preventative medicine from African culture, for example.

When the author traveled to Africa in 2014, he was surprised to learn that his guide refused to take antimalarial medication. The guide explained that he didn't take the medication because it'd require him to keep taking it for the rest of his life.

Instead, the guide made sure to wear clothes that covered his skin completely, to sleep in spaces protected from mosquitoes and to use insect repellant. Doing so reduced his risk of contracting mosquito-borne diseases.

The author realized that such a common-sense, preventative approach was missing in contemporary Western medicine. Treating coronary issues with a life-long dependency on statins was a reactive, unsustainable solution.

Preventative actions, in contrast, can keep a body healthy without the constant intake of drugs. In the end, however, each person must take the time to work out which solutions suit them best.

### 6. Large-scale medical studies are a solid choice for sound medical advice. Wikipedia is not. 

Do you believe everything you read online about your favorite celebrity? Hopefully not.

You should also exercise caution when you Google what's ailing you online in search of a cure. In other words, don't trust the editors at Wikipedia to give you an accurate diagnosis!

Though many collaborative online platforms can offer helpful, generalized information about human disease, they can't be trusted for issues as complex as your personal health.

A 2014 study in the _Journal of the American Osteopathic Association_ revealed that 90 percent of Wikipedia articles on major diseases, including heart conditions, depression, diabetes and back problems, contained significant factual errors.

So when it comes to your health, it's always best to do a certain amount of critical research from reliable sources.

You might have heard about the virtues of saw palmetto extract, often advertised as a miracle cure for prostate issues. Advertising and online advice might lead you to believe you're ingesting a harmless product made from the fruit of a small palm tree.

Yet further research will tell you that saw palmetto can also cause negative side effects, potentially preventing medications that slow blood clotting from working and throwing your body's hormone regulation out of balance, causing dizziness and nausea.

Try to avoid seeking advice or believing the claims of online forums or advertising. Instead, look to recent, large-scale medical studies for solid health advice, as these tend to be well-researched and follow strict scientific methods that allow other researchers to replicate and verify the results.

Unfortunately, medical studies can also inspire misinformation, when study groups are too small or results are interpreted erroneously.

Doctor Andrew Wakefield's study on the measles, mumps and rubella (MMR) vaccine appeared to show that receiving the vaccine, as almost all children do, can potentially lead to the development of autism.

This study was based on just 12 children as subjects, however, and was found to be utterly false. Despite this, the myth that vaccines can cause autism persists in popular culture.

### 7. Society often fails to recognize when weight issues become a problem, for both adults and children. 

Some 67 percent of American people are overweight — an astonishing statistic.

Here's another data point that's even more surprising: just 36 percent of overweight people recognize that they are indeed overweight. The others believe their weight is average or even underweight.

This phenomenon was demonstrated by a study published in _Obstetrics & Gynecology_ in 2010. As obesity becomes more and more common, society gradually sees being overweight as the norm.

This tendency is clear in the way parents perceive their children's weight, as revealed in a 2014 study published in the journal _Pediatrics._

Up to 50 percent of parents of overweight or obese children are unaware that their child has a health problem. The results of this can be devastating, as being overweight puts a person at risk of a number of health complications later in life.

Breast cancer is one of these complications. After menopause, women who are obese have a 60 percent greater risk of contracting breast cancer. A more informed knowledge of the connection between cancer and obesity could encourage people to take the issue more seriously, as individuals and as parents.

Weight isn't the only health issue for which society has a blind spot. A study from Cambridge University revealed that many people have no idea how much sugar they consume daily.

Many study participants claimed to eat relatively little sugar, yet urine tests showed that they ate far too much sugar, which in turn led to obesity and other health issues.

Curiously, participants who felt they ate too much sugar were shown to consume hardly any refined sugar at all!

### 8. Regular exercise can add years to your lifespan, but many people just can’t or won’t get off the couch. 

Our ancestors spent time hunting, farming and gathering food, their bodies evolving to move more efficiently and with greater dexterity. Yet amid the pressures of modern society, many people just don't get the exercise needed to live longer, healthier lives.

A 2012 Harvard University study published in the medical journal _The_ _Lancet_ confirmed that a lack of exercise presents a medical challenge of epidemic proportions in America, as 80 percent of Americans fail to get enough exercise generally recommended for good health.

A study performed by physician Jannique van Uffelen showed that women who spend at least nine hours a day sitting for work or other reasons face an increased risk of depression when compared to women who sit for less than six hours daily.

Sitting isn't unhealthy per se, especially in positions that are an effort for the body to maintain. Sitting on the ground, for instance, requires the muscles of the legs and lower trunk to activate.

But reclining in a chair or slouching on a sofa require little effort. Sitting for long periods in such positions makes the body's metabolism shut down. This, in turn, allows blood sugar and fat to accumulate.

If sitting puts a body at risk of health problems, getting up and moving does the opposite.

Daily exercise is one of the most potent anti-aging treatments we know. In 2012, the National Cancer Consortium in America measured the effect of exercise on overall health. They discovered that if a person walks at least 75 minutes weekly, they can add a year and a half to their life expectancy.

If you bump your walking to 7.5 hours per week, you can expect to add 4.5 years to your lifespan!

The study also found that by actively exercising for at least two hours each week and maintaining a healthy weight, people can raise their life expectancy by as much as seven years.

### 9. Athletes can’t perform at their peak without a good night’s sleep. It turns out that neither can you. 

What does it take to be a star athlete? Perhaps you'd think a strict, fat-free diet, strenuous exercise and intensive training are crucial. Yet there's one important aspect of training that most people forget — and that's a good night's sleep.

Sleep is essential for maintaining a healthy body. Insufficient sleep has been shown to diminish the performance of top-level athletes, leading to slower reflexes, poor judgment, incomplete recuperation and difficulties in maintaining focus.

The impact of sleep was the subject of a study published in 2009 by Christopher Winter, a doctor from Virginia, who spent ten years observing the performance of baseball players.

Winter found that teams that crossed a single time zone while traveling to an away game performed at a slight disadvantage to the home team. But when a team crossed three time zones, however, the team's chance of winning a game dropped by 50 percent. The significant sleep disruption caused by jet lag was a huge disadvantage for the team on the road.

It turns out that deep, well-regulated and sufficient sleep isn't just vital for athletic performance. A lack of sleep works to make illnesses such as obesity, cancer, diabetes and heart disease even worse.

People who suffer from a lack of sleep tend to feel cold all year long. Individuals who sleep less than six hours each night are four times more likely to catch a cold compared to people who sleep eight hours.

The physical and mental health, reflexes and professional performance of at least 70 million Americans who suffer from sleep disorders are impaired, according to the National Institutes of Health.

A better night's sleep could be one of the most powerful solutions for improving the health of a population!

### 10. Final summary 

The key message in this book:

**Advances in genetic science may hold the key to medical challenges including cancer, infertility and aging. But while many of these techniques are still too risky to implement, there are many solutions that are both simple and effective. From walking daily and getting sufficient sleep, the most basic solutions could have a profound impact on our overall health.**

Actionable advice

**Let yourself touch and be touched.**

In addition to exercise and sleep, touch is an essential element in keeping a body rejuvenated and healthy. So reach out and touch someone! This doesn't mean you have to be overly intimate or have sex — just cuddling a close friend is enough to boost your health and mood.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Eat, Move, Sleep_** **by Tom Rath**

_Eat, Move, Sleep_ (2013) offers simple tips for improving your health and well-being in some very important ways. You don't have to revolutionize your lifestyle to get in shape and increase your energy levels — little changes can make a big difference, and these blinks will show you how.
---

### Dr. David B. Agus

Dr. David B. Agus is a medical doctor and engineer, known as one of America's leading oncologists. He has founded a number of medical companies focused on offering personalized medication. He is also the author of other books such as _The End of Illness_ and _A Short Guide to a Long Life_.

