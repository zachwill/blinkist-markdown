---
id: 56ebe8c470f382000700003e
slug: the-machine-that-changed-the-world-en
published_date: 2016-03-22T00:00:00.000+00:00
author: James P. Womack, Daniel T. Jones and Daniel Roos
title: The Machine That Changed the World
subtitle: The Story of Lean Production: Toyota's Secret Weapon in the Global Car Wars That Is Now Revolutionizing World Industry
main_color: 3F85A9
text_color: 306480
---

# The Machine That Changed the World

_The Story of Lean Production: Toyota's Secret Weapon in the Global Car Wars That Is Now Revolutionizing World Industry_

**James P. Womack, Daniel T. Jones and Daniel Roos**

_The Machine That Changed the World_ (1990) reveals the secret that propelled Japanese car manufacturer Toyota to the forefront of the global automobile industry, a process called lean production. These blinks give you an inside look at the industry's early history and show how Toyota's innovative process allowed the company to dominate the market.

---
### 1. What’s in it for me? Learn how Toyota’s lean production system revolutionized manufacturing. 

"The car in front is a Toyota." Do you remember this famous ad slogan from the 1990s? For decades Japanese car manufacturer Toyota has maintained its solid reputation for innovation and quality. The company's cars sell well worldwide, being valued for both their efficiency and reliability. 

A cornerstone of its successful global business is Toyota's legendary production system, called TPS — more commonly known as lean production. In fact, this production method became so successful that it has since been adopted in other industries, such as design, programming and start-up management.

These blinks give you the inside scoop on how lean production came to be and explain in detail how this system helped skyrocket Toyota to global success. 

In these blinks, you'll also learn

  * why Toyota constantly (and purposefully) nearly runs out of stock;

  * why every Toyota employee can stop a production line; and

  * why many car companies prefer to ignore mistakes than fix them.

### 2. From the “horseless carriage” to the modern assembly line, the automobile industry has evolved. 

The automobile industry has grown tremendously since the early days of the "horseless carriage," first patented by Karl Benz in 1886. 

There's good reason the automobile industry is often called the "industry of industries." In 2014 alone, some 90 million cars and commercial vehicles were produced. The automobile industry as a whole represents the world's largest manufacturing activity. Let's look at how this came to be. 

In its early days, the auto industry was defined by _craft production_, meaning that highly skilled engineers and manufacturers tailored each individual car to a customer's taste. This was slow and expensive, thus few people could afford cars — and only some 1,000 vehicles were produced yearly. 

Only luxury cars are produced via craft production today. In general, the industry has moved to _mass production_. Inspired by Henry Ford, this change came about at the end of the twentieth century. 

Henry Ford simply sought to produce more cars in less time. He realized he could achieve this by designing cars with the same, interchangeable parts and producing the parts separately, instead of working on a whole car at once. 

The development of the _assembly line_ further accelerated the manufacturing process. An early assembly line was a moving belt with workers stationed at different spots along it; each worker performed one or two simple tasks, over and over again. These basic tasks didn't require highly specialized skills, and in fact, many assembly-line workers were immigrants who spoke little English. 

The automatization of the production process meant that cars were no longer customized (or were customized very little), but instead offered a great advantage: any person could drive or even repair them, just by following a standard 150-page manual!

### 3. Assembly line production grew in the US and in Europe, but the process wasn’t foolproof. 

With the innovation of mass production, a flood of cheaply produced American cars were sold worldwide. By the early 1930s, some 32 million vehicles were in use globally, and 90 percent of those were American. 

Most of these vehicles were made by three leading US companies, known as the Big Three: Ford, Chrysler and General Motors. Henry Ford's assembly-line process dominated the automobile industry for about 50 years, but this gradually changed as European companies began to compete; Germany founded Volkswagen, France Renault and Italy Fiat.

As the European companies made inroads, Ford, Chrysler and General Motors began to lose their leadership positions — and even the assembly-line production process grew outdated.

The problem was that mass production followed strict rules and offered little reward to workers who performed the line's boring, repetitive tasks. As you might expect, employment turnover was high. 

An assembly line was also practically unstoppable, as each process depended on the next, and time was always tight. So if a worker found a problem, he might not report it, as it was simpler to just send the defective part on down the line. 

Moreover, an assembly-line manager's performance was measured by the number of cars the line produced, so workers were essentially incentivized to ignore flaws to meet overall quotas.

Mistakes were caught and repaired — in a space called the _rework area –_ once a car had come off the assembly line. Sometimes an entire vehicle would have be taken apart and rebuilt, a costly and tedious process that caused delays. With so many moving parts to examine, even rework area employees couldn't catch every problem, and many defective cars were sold as a result.

### 4. Mass production didn’t take off in Japan because it wasn’t suited to market demands or the labor force. 

Mass production as a process waxed and waned with companies in the United States and Europe, but it never really took hold in Japan.

This was partly because demand in the Japanese automobile market was more diverse. In the United States and Europe, a majority of the market sought cheaper cars; yet in Japan, the market was broader, with more particular demands. 

Politicians wanted luxury cars, farmers and entrepreneurs needed small trucks and goods sellers wanted larger trucks for transporting wares. People who lived in crowded cities wanted smaller cars that would use less oil. Thus, the one-size-fits-all vehicles produced in the West weren't practical for the Japanese market. 

Nonetheless, between 1925 and 1936, America's Big Three dominated the Japanese market, producing some 200,000 vehicles yearly. (Japanese companies produced about 12,000.) 

But the foreign influence was intentionally limited. In 1936, the Japanese government passed the Automobile Manufacturing Industry Law, which pushed American firms and their mass production techniques out of the Japanese market. 

The process of mass production also wasn't suited to the makeup of the Japanese workforce. Unlike the United States and Europe, Japan didn't have an immigrant or "guest worker" labor class, and thus companies struggled to find low-income workers willing to do the boring, repetitive tasks required by assembly line production. 

Japanese employment in general was based on long-term contracts, another barrier to the success of assembly-line manufacturing. In the hierarchical Japanese workforce, if an employee quit his job and joined another company, he had to start over at the bottom of the career ladder, even if he had accrued 20 years of experience elsewhere. 

Thus once an employee started with a company, he preferred to stick with it, which left little flexibility in the workforce for the demands of assembly-line manufacturing.

### 5. After discovering flaws in Ford’s manufacturing process, Toyota developed its own innovative plan. 

In 1937, the Toyoda family founded the Toyota Motor Company. Concerned that the family name sounded too traditional (it means "abundant rice field" in Japanese), they exchanged the "d" for a "t" to create Toyota.

Success didn't come easy; by the end of the 1940s Toyota already had laid off a large portion of its workforce. But things changed when two Toyota engineers, Taichii Ohno and Eiji Toyoda, paid a visit to the Ford Rouge Factory in Michigan.

Ohno and Toyoda studied Ford's mass production techniques to see if they could make use of them back home. They soon realized that Ford's process wasn't ideally suited for the Japanese market, as it couldn't produce a wide enough variety of vehicles. They also found some inefficiencies, places where both time and resources were lost. 

So Ohno and Toyoda came up with what they thought was a better process for manufacturing cars, calling it the _Toyota Production System_, or TPS. 

TPS was essentially based on the values of what we today call _lean production_ : continuous improvement (in Japanese, _kaizen_ ) and a respect for customers and employees. 

The Japanese idea of _kaizen_ holds that processes are never perfect, and can always be improved. This concept clashes with the assembly line process, which values continuous production above all else. 

In TPS, production is halted each and every time an error is identified; the system is then reexamined and refined. This is part of the reason that Toyota cars, even today, are renowned for their safety. 

Importantly, TPS doesn't just concern itself with customers' well-being but also ensures the company respects its employees and business partners, too. 

Toyota also offers _on-the-job development_ programs to help employees climb the company's career ladder. Such programs build workforce loyalty and help lift everyone up in the long run.

### 6. Lean producers leverage the benefits of both craft production and mass production. 

Toyota's lean principles remain influential across the globe, even today, and not just in the automobile industry. What is it that makes this process so successful?

Lean production works well largely because it's aimed at eliminating mistakes. Assembly lines, on the other hand, are focused on production output, so mistakes are easily overlooked. 

Perfection is the goal for lean production. An ideal lean-production process doesn't produce any flaws, leave excess stock or impose constraints on innovation. 

Realistically, no lean-production system has ever reached complete perfection and none probably ever will, but such systems generate much better long-term results. 

The lean principle of reducing waste, for example, has enabled Toyota to spend more working hours focusing on innovation. The company keeps each of its car models in production for an average of four years; in contrast, Western companies produce a car model for an average of ten years. 

Lean principles also utilize the best features of both craft production and mass production. 

Lean automobile production focuses on a buyer's demands, just like craft production. The production process is based on what a customer wants, which reduces costs and prevents overproduction.

Toyota is always readjusting its market goals. The company keeps track of customers and their income levels, family size and driving preferences. Then it tailors its production output to customers' specific needs.

Yet mass production methods can't do this. Such systems produce as many cars as possible, and sometimes, there aren't enough customers to buy them. 

In the 1980s, the lean-production system finally triumphed and Japanese manufacturers overtook their American counterparts. Toyota is still the largest car manufacturer in the world today.

### 7. The lean-production process is founded on maintaining a happy and knowledgeable workforce. 

With mass production, workers are little more than cogs in the machine. Their only responsibility is to perform the same simple tasks, over and over.

That's not how Toyota works. The company highly values the welfare of its employees. That's why two of the company's core values relate to employee happiness: teamwork and personal respect. 

Toyota employees are guaranteed a lifetime of valuable employment. Salaries and bonuses increase with employee experience and company profitability. Workers also have access to Toyota's extensive facilities, such as housing and recreational areas. 

Toyota ensures that its employees' professional development never stops, from their first day at work (usually around age 18) until retirement. Employees are encouraged to improve their skills and given access to training, seminars and lectures. 

This system benefits both sides: employees have greater job satisfaction, and the company has more knowledgeable and motivated workers. 

The system also allows Toyota to give employees more responsibility. Employees are placed in teams and led by a team leader, and are tasked with overseeing parts of the production line. 

The whole team works together to make the process more efficient, instead of a leader simply barking orders to subordinates. So if one team attaches car wheels, the team can freely experiment until it finds the most effective way. The leader helps to coordinate, rather than command. 

If a team comes up with a larger-scale idea for improving a process (like a new way of repairing wheels, for example) it can share its ideas with company engineers in _quality circles_.

### 8. Lean production methods seek to eliminate errors by asking “why?” to get to the root of problems. 

What's more efficient: Rushing haphazardly to finish your work or working more carefully, correcting mistakes as you go? Toyota executives would choose the second option. 

In the lean-production process, workers keep an eye out for problems and investigate the roots of errors when they are brought to light. Production is paused when a mistake is found, and the power to pause production isn't limited to managers or supervisors; any team member can do it. 

Once a mistake is identified, it is analyzed so a team can prevent the error from happening again. This process is guided by _The 5 Whys_.

If an employee notices a loose wheel, for example, he should ask, "Why is this wheel loose?" Perhaps the answer might be that it's missing a bolt. So the next question should be, "Why is the bolt missing?"

Maybe the bolt is missing because the assembly team wasn't given the correct number of bolts. Thus the next question: "Why wasn't the assembly team given the right supplies?" Here, perhaps the wrong supplies were ordered. Now the team should ask, "Why did that happen?"

This series leads the team to the root cause: maybe the instructions for placing orders are out of date. Once the problem is solved, the production process can be resumed. 

The 5 Whys approach ensures the quality of a final product, and also makes the overall process more efficient in the long run. When a team eliminates the root of a mistake, the mistake is less likely to reoccur in the future. Thanks to this process, Toyota's cars have fewer defects than any other cars in the world. 

When Toyota first started with its process, the production line was stopped often. Yet with time, the process worked out most of its kinks and became smoother and more efficient. Today, Toyota's lines almost never have to be stopped and its plants have virtually no rework areas.

### 9. A company employing lean-production methods also needs to maintain a lean supply chain. 

While Toyota's own production process is based on lean principles, how does this affect its supply chain?

A lean supply chain is based on the _kanban_ system, which aims to produce only the exact amount of products that customers want. This means that car models and other products need to be constantly evolving to suit changing customer needs. 

The kanban system also means that companies need to keep supply stocks low. If the company has a large inventory of wheels for Model X, for example, the wheels become useless when the company shifts to Model Z. 

The kanban system utilizes a delivery system called _just in time_, meaning that supplies are only ordered and received just before they're needed. So Toyota plants don't keep a stash of car seats, for example. A plant will contact a supplier five hours before a car is ready for its seats, to make sure the plant only orders the exact quantity it needs. 

Because of this Toyota has little waste, but it also means it doesn't have much of a safety net if it or its suppliers are hit with an unexpected delay, like a natural disaster or a labor strike. If faced with such a situation, the company deals with it in two ways:

First, Toyota makes sure its assembly plants are flexible, so team leaders can switch stations and cover for absent workers if necessary. Second, the company design each plant to serve more than one market. If there's a big change in market demand, a variety of plants can be mobilized to address it. 

All in all, Toyota is expert at identifying potential threats to its supply chain and developing strategies for any problems that arise.

### 10. Final Summary 

The key message in this book:

**Henry Ford revolutionized the automobile industry with the invention of the assembly line, but nonetheless his process was inefficient and flawed. Since mass-production systems weren't suited to the Japanese labor market, Japanese auto manufacturer Toyota developed its own system to improve upon Ford's early innovation. This "lean production" system was so effective that it eventually spread to other industries. Lean production is one of the reasons Toyota still leads the global automobile market, even today.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Gemba Kaizen_** **by Masaaki Imai**

_Gemba Kaizen_ (1997) is an introduction to the Japanese business philosophy of Kaizen, which revolutionizes working standards to reduce waste and increase efficiency at little cost. Author Masaaki Imai reveals the aspects of Kaizen that are crucial to building lean business strategies.
---

### James P. Womack, Daniel T. Jones and Daniel Roos

James P. Womack is the founder of the Lean Enterprise Institute, a nonprofit management resource organization that promotes the values of lean production. Daniel T. Jones is the founder of the Lean Enterprise Academy, and co-authored several books with Womack. Daniel Roos was the founding director of the International Motor Vehicle Program (IMVP) and of the Massachusetts Institute of Technology's Engineering Systems Division. All three were researchers at IMVP when this book was published in 1990.

