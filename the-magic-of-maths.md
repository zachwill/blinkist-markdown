---
id: 56d44a35bb82ae000700005f
slug: the-magic-of-maths-en
published_date: 2016-03-01T00:00:00.000+00:00
author: Arthur Benjamin
title: The Magic of Maths
subtitle: Solving for x and Figuring Out Why
main_color: 897E9F
text_color: 585166
---

# The Magic of Maths

_Solving for x and Figuring Out Why_

**Arthur Benjamin**

_The Magic of Maths_ (2015) reveals the magic hidden within the fascinating world of mathematics. These blinks will show you the beautiful and often surprising patterns of mathematical observations, expand your knowledge of geometry and algebra, teach you numerical party tricks and illuminate the mysterious properties of numbers like π (pi).

---
### 1. What’s in it for me? Be mystified by mathemagic. 

The mere mention of a magician typically brings up images of someone with a top hat saying _abracadabra_ and conjuring white bunnies, doves and handkerchiefs from thin air with the simple touch of a wand. But without the props and the grand spectacle, the magic, too, would be gone.

Unless, of course, you enter the world of math! These blinks explore the beautiful magic of mathematical patterns, showing you how you can use some of these patterns to perform impressive tricks and do seemingly impossible calculations in your head. See how magic can likewise be found in numbers like π, as well as in the concept of infinity.

You'll also discover

  * how to easily square big numbers in your head;

  * how to impress people with a simple algebra-based magic trick; and

  * why there are exactly as many positive integers as there are even integers.

### 2. Numerical patterns are not only magic – they can lead to real applications too. 

Mathematics is more than just tedious textbooks and laborious calculations; it is a whole world of patterns that are not only _magical_, they can be really useful too.

Consider the so-called numerical patterns — patterns made of numbers — and their surprising and beautiful properties.

The author first discovered these patterns as a child, when he was playing with pairs of numbers that add up to 20:10 and 10, or 9 and 11, for instance.

He wondered: what's the largest possible product I can get by multiplying these pairs together?

Let's find out:

7x13=91

8x12=96

9x11=99

10x10=100

So, the largest product is when both numbers are 10. Nothing extraordinary, right?

But if you look closer, there's something interesting about these numbers. Examine how far each product is from 100, counting downwards, and you'll notice a pattern: 0, 1, 4, 9. These are the _first square numbers_, that is, the numbers that follow the sequence 1², 2², 3², and so on.

This pattern applies all the way up and down the scale: if we calculate 5 x 15, we can get back to 100 by adding 5². And what's more, the same pattern emerges no matter what number the pairs add up to!

These numerical patterns aren't only magical, they're also useful in the real world. If we can learn their secrets, we can use them to increase the power of our mental arithmetic, that is, the calculations we make in our head.

For example, we can use the above pattern to easily calculate the square of a number.

Say you want to square the number 13. Instead of calculating 13x13– a nightmare to do mentally — we can perform the easier calculation 10*16 where both numbers add up to 26, just like 13 and 13.

Now we've got 10x16=160, but we're not quite there yet. Our previous pattern tells us that since we went up and down 3 from 13, we need to add 3² to the result. Thus we get 13²=(10*16)+3²=160+9=169.

> _"And yet, we often don't get to see that numbers possess a magic of their own, capable of entertaining us, if we just look below the surface."_

### 3. You can use algebra to perform mathematical magic tricks. 

Now that you know that mathematics can be magic, you probably want to learn a party trick to impress your friends. So, here's an example of _mathemagics_ that you can perform. Choose a friend and lead him through the following five steps:

(1) First think of two numbers from 1 to 10, 

(2) then add those numbers together, 

(3) multiply that number by 10, 

(4) add the larger original number,

(5) subtract the smaller original number, 

(6) and ask him for the result.

Now, if you practice the following technique, you'll be able to wow him by instantaneously telling him _both his original numbers_!

Imagine his answer is 126. Take the last digit of his answer, 6, and add it to the preceding number(s), 12. Then divide this by 2 to get (12+6)/2=9. This will be his larger number. 

To determine the smaller number, take the larger number you just calculated — in this case 9 — and subtract the last digit of his answer, 6, to get 9-6=3. That's it! But what's the magic behind the trick? It's the power of algebra, the form of arithmetic where letters take the place of numbers.

Let's look at the algebra behind our party trick. So, let X and Y be two numbers, where X is larger than or equal to Y. Following the steps from above we get:

Step 2: X+Y, in our case 9+3

Step 3: 10(X+Y), 10(9+3) = 120

Step 4: 10(X+Y)+X, 10(9+3) + 9 = 129

Step 5: 10(X+Y)+(X-Y), 10(9+3) + (9-3) = 126

Notice that a number of the form 10(X+Y)always ends in 0, and the digit(s) preceding the 0 are X+Y. X-Y must be a one-digit number, which is why the last digit of the answer in step 5 is X-Y.

But once we know X+Y and X-Y we can calculate ((X+Y)+(X-Y))/2=((X+X)+(Y-Y))/2=2X/2=X. In our case ((9+3) + (9-3) / 2 = ((9+9) + (3-3)) / 2 = 2x9 /2 = 9

Finally, we calculate X-(X-Y)=(X-X) +Y=Y. In this case, it would be 9 - (9 - 3) = (9-9) + 3 = 3

Done. Magic!

> _"[Do] unto one side as you would do unto the other" — this is what the author calls "the golden rule of algebra."_

### 4. The number 9 is magic in many ways. 

Ever since the author was a kid, he was fascinated by the number 9. Why 9? Because 9 is a magic number.

We can see the first magic property of 9 in its multiples. The first multiples of 9 are: 9, 18, 27, 36, 45, 54, 63, 72, 81, 90, 99, 108, 117, … .

All these numbers have something in common. If you take the digits of each multiple and add them together, the answer is always 9! For example, 9+0=9, 1+8=9, 2+7=9, and 3+6=9. At first glance, 99 looks like an exception, because 9+9=18. However, if you add 1 and 8 together, once again, you get 9.

But the converse is true as well: any number whose digits add up to a multiple of 9 is _itself_ a multiple of 9. Take 123,456,789 for example. The total of its digits is 45: a multiple of 9, which means 123,456,789 must be a multiple of 9 too. Indeed, 9x13717421= 123,456,789.

We can see another magic property of 9 when we look at numbers that have different digits, written from smallest to largest. These numbers include 12345, 2368, 379, or 135789, and follow the form abcde... with a<b<c<d<e<. So, a is smaller than b, b is smaller than c, and so on.

Now for a bit of magic: let's multiply one of these abcde numbers by 9, say 9x12345, then add the digits together. We already know that the result must be a multiple of 9, because the digits of multiples of 9 add up to 9.

But with abcde numbers, you don't just get a multiple of 9; in fact, the number is always _exactly_ 9\. For example: 9x12345=111,105, where 1+1+1+1+5=9;9x2358=21,222, where 2+1+2+2+2=9.

### 5. Fibonacci numbers reveal a series of astounding patterns. 

In 1202, the Italian mathematician Fibonacci wrote the book _Liber Abaci_ ("The Book of Calculation"), a book that would change the world of mathematics. It contained a variety of arithmetic problems, the most famous one involving, curiously enough, immortal rabbits. Here is what it looked like:

Suppose that baby rabbits take one month to mature, and that a pair of mature rabbits produces a new pair of baby rabbits every month. Starting with one pair of baby rabbits, how many rabbit pairs will there be after 3, 4, or 12 months?

The answer to this question leads to the famous _Fibonacci numbers_.

Let's explore this mathematically, using the lowercase "r" to denote a pair of baby rabbits, and a capital "R" for an adult pair.

From the rules of the puzzle, we know that every month a "r" (baby rabbit) will become "R" (adult rabbit), and each "R" will be replaced by "Rr", a pair of adults plus a pair of babies.

So what do we get? 

Month 1: r

Month 2: R

Month 3: Rr

Month 4: Rr R

Month 5: Rr R Rr

Month 6: Rr R Rr Rr R

...

If we count the resulting rabbit pairs in digits, we get the sequence 1,1,2,3,5,8 ... 

Notice something? 

Each number is the sum of its two predecessors. For example, 1+1=2, 1+2=3, 2+3=5, and so forth. This is the extraordinary sequence known as _Fibonacci Numbers_.

But that's only the beginning of their wonders.

For example, what do you think happens if we add squares of Fibonacci numbers?

1²+1²=2,

1²+2²=5,

2²+3²=13, … . 

At first glance there doesn't seem to be a pattern. But in fact, the results are still part of a Fibonacci sequence, except every second number is left out!

How about this: What happens if we add all of the squares of Fibonacci numbers together?

1²+1²=2=1x2,

1²+1²+2²=6=2x3,

1²+1²+2²+3²=15=3x5, … .

Here, the sum of the squares of Fibonacci numbers is the product of two consecutive Fibonacci numbers!

> The number of petals on a flower often is a Fibonacci number, as are the number of spirals on a sunflower, a pineapple or a pinecone.

### 6. Proofs are the lifeblood of mathematical reasoning. 

There's one thing that separates mathematics from all other sciences: mathematicians can prove that their propositions are _absolutely_ true. 

This is made possible by the certainty of mathematical _proofs_, series of equations that are true under all circumstances. 

Let clarify with an example.

It's probably common sense to you that adding two even numbers will result in another even number. For example, 2 and 2 is 4, or 2 and 6 is 8.

But how can we be certain that this applies to _all_ pairs of even numbers without calculating each and every one? Through the power of proofs.

Say we want to prove that if m and n are even numbers, then m+n is an even number.

By definition, even numbers are multiples of 2. Therefore, we can write every even number as 2k with k being an integer, another word referring to all whole numbers.

Now let m=2k and n=2j with j,k as integers.

It follows that m+n=2k+2j=2(j+k).

Since j+k is an integer, this means that m+n is a multiple of 2 — and therefore even!

That's the power of a proof: it allows us to be certain without having to perform endless calculations.

Another great example is the method called _induction,_ recognized as especially elegant by mathematicians. First, one shows that _if_ a statement is true for an arbitrary number, k, it must also be true for its successor, k+1.

Second, one finds a specific number n for which the statement _is_ actually true. Having found n, the first step forces the statement to be true also for n+1. But since the statement is true for n+1, the first step forces the statement to be true for (n+1)+1=n+2, and so on. Hence, the statement must be true for _all_ of the successors of n.

> _"A good proof is like a well-told joke or story — it leaves you feeling very satisfied at the end."_

### 7. The number π has many mysteries. 

There are few numbers as mysterious as the number π, pronounced _pi_. You probably remember it from school, when you used it to calculate measurements related to circles.

To be precise, the number π is defined as the ratio between a circle's circumference and its diameter.

Let's put down a few definitions. The _radius_ r of a circle is the distance between the circle's center and one of its points. A circle's _diameter_ D is defined as twice its radius: D=2r. D is the distance from any point of the circle to another across the circle's center. Finally, the distance around a circle is called the circle's _circumference_ C.

We can then look at the ratio of a circle's circumference to its diameter, C/D.This ratio is the famous π (the Greek letter for "p") and it's approximately **3.14** , regardless of the size of the circle.

So now you can remember π. But did you know that it has many interesting uses beyond geometry?

If you're familiar with statistics, you've probably heard of the famous _bell curve,_ the graph that describes the distribution of a common characteristic like IQ. π is actually crucial to the formula that describes this powerful graph, which is used in domains ranging from experimental science to signal processing.

That's not the only interesting place π pops up. Take the infinite sum of fractions where we always have 1 on top and a square number below:

1+1⁄22 +1⁄32 +1⁄42  ... = 1⁄4+1⁄9+1⁄16 +....

The great mathematician Leonhard Euler (1707-1783) showed that the result of this sum is the number π²/6. This is unexpected, because there's no obvious link between π, which is derived from circles, and an infinite sum of fractions.

π has many more intriguing features. For example, π is an _irrational_ number, meaning that it cannot be written as a fraction of integer numbers. Other examples of irrational numbers include √2.

Now that we've learned more about π, let's discover two other important numbers.

### 8. There are two other prominent numbers in mathematics – the imaginary number i and the Euler number e. 

Believe it or not, mathematicians have _favorite_ equations. If you ask them, many will mention this one: _e iπ_\+ 1 = 0.

This simple equation, attributed to Leonhard Euler, is considered by many to be the most beautiful equation in all mathematics, because it contains the five key numbers of mathematics: 1, 0, π, i, and e.

We already know 1, 0 and π, so let's look closer at _i_ and _e_.

The mysterious number i is the square root of -1. Not only is it mysterious — in mathematics it's called the _imaginary_ number that solves the perplexing equation i²=-1.

You're right, that does look strange. How on earth can the square of a number be negative?

Negative numbers have probably seemed weird ever since you were a child. For example, you never go to the supermarket and ask for minus four oranges.

But now that you're an adult, you've probably seen how, despite their counterintuitive nature, minus numbers have their use in the real world — to display debt, for example.

Similarly, mathematicians have found uses for imaginary numbers, especially when solving certain complicated equations. Take the equation x²+1=0. If you replace the unknown value x by the concrete value i — the imaginary number — this will solve the equation since i²=-1, and thus i²+1=-1+1=0.

Another prominent, curious and useful number in mathematics is the Euler number e .

_e_ is like π. It's a number found everywhere in mathematics, and it has its own strange numerical value. In fact, the Euler number _e_ is approximately 2.71828.

This number is used to help people calculate _interest_, the amount someone's money has grown after a specific period of time at a certain interest rate. And like π, it also plays a crucial role in the calculation of bell curves, the powerful graphs mentioned in the previous blink.

> Mathematicians often refer to _e iπ_\+ 1 = 0 as "God's equation."

### 9. When it approaches infinity, mathematics yields strange results. 

Infinity is one of the most mind-boggling concepts in all of mathematics. From afar it sort of makes sense, but once we get too close things start to get really bizarre.

For instance, the number 0.99999..., which comes infinitely close to 1, will never actually reach 1, right? We can prove otherwise.

Let w=0.99999.... If we multiply it by 10 we get 10w=9.99999.... And if we subtract the first equation from the second, we obtain 9w=9.00000...which means that w=1!

This makes sense because two numbers are only different if there's a number between them. But in this case, that's simply not possible.

Another counterintuitive aspect of infinity is shown in the size of infinite sets of numbers.

There are an infinite amount of positive integers — the numbers we use on a day to day basis, like 1, 2, 3, 4, 5 and so on. Now, every second positive integer is even: 2 is even, 3 is not, 4 is even again, and so on. So it makes sense to think that there are more positive integers than there are even positive integers, since only half of them are even.

Wrong.

In fact, there are _exactly as many_ positive integers as there are even positive integers, and this is because of infinity.

If you compare both sets of numbers — the set of positive integers and the set of even positive integers — they are both infinite, and are therefore the same "size".

Mathematicians express this by saying that there is a _one-to-one correspondence_ between these two sets: every number from the set of positive integers can be paired up with exactly one even positive integer as follows,

1 → 2,

2 → 4,

3 → 6,

4 → 8,

Whenever sets have such a one-to-one correspondence, they are said to have the same (infinite) number of elements.

> _"[... When] you enter the twilight zone of infinity, very strange things can happen, which is part of what makes mathematics so fascinating and fun._

### 10. Final summary 

The key message in this book:

**Mathematical patterns can reveal surprising results that are both beautiful and magical at the same time. Getting a deeper understanding of these patterns can actually be useful, helping us increase the speed of our mental arithmetic and perform number tricks. But beyond patterns, numbers like π,** **i** **and** **e** **, and the concept of infinity have their own special magic, too.**

Actionable advice:

**Baffle your colleagues and friends with a mathe** ** _magical_** **trick!**

Choose one of your colleagues or friends and let her think of a number between 20 and 100. When she's chosen one, ask her to perform the following three simple calculations:

  1. add the digits together (e.g. 23 → 2+3=5)

  2. subtract the total from the original number (23-5=18)

  3. add the digits of the new number together (18 → 1+8=9)

You can easily guess her number, because she doesn't know that no matter which number she starts out with, she'll always end up with the number 9!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _In Pursuit of the Unknown_** **by Ian Stewart**

In this book, Ian Stewart focuses on 17 famous equations in mathematics and physics history, highlighting their impact on society. Stewart gives a brief history of the wonders of scientific discovery, and peppers it with vivid examples and anecdotes.
---

### Arthur Benjamin

Arthur Benjamin is a regular speaker at TED talks and is Professor of Mathematics at Harvey Mudd College. He holds a PhD from Johns Hopkins University and is also the author of _Secrets of Mental Math_.

