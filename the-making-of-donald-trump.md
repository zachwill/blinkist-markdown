---
id: 59064c1fb238e10008b85733
slug: the-making-of-donald-trump-en
published_date: 2017-05-05T00:00:00.000+00:00
author: David Cay Johnston
title: The Making of Donald Trump
subtitle: None
main_color: B5A039
text_color: 665A20
---

# The Making of Donald Trump

_None_

**David Cay Johnston**

_The Making of Donald Trump_ (2016) examines the man behind the highly polished public figure presented to the media — and now the voting public — of America. His thousands of court cases and shady business dealings give a clear picture of the deception and dishonesty that Donald Trump would rather keep out of public view. Now more than ever before, it's crucial that people know whom they're dealing with.

---
### 1. What’s in it for me? Take a peek behind Trump’s showy public image. 

Donald Trump may well be the unlikeliest president in the history of the United States. After all, he's a total outsider, a real estate magnate and reality TV star without any political or diplomatic experience.

But he turned his outsider status into an advantage, winning the votes of those who'd grown wary of the establishment. Trump projects strength and savvy — and, to many, he seems like the kind of guy who will get the job done.

If Trump plans to run the United States like a business, it makes sense to investigate Trump's history as a businessman. How does he choose his allies? How does he deal with his enemies? Is he a knowledgeable and responsible leader?

These blinks, after delving into the subject, provide some rather unsettling answers.

And you'll also discover

  * why Trump University was anything but a university;

  * about the special relationship between Trump's moods and his "facts"; and

  * how Trump withdrew medical funding for his gravely ill grandnephew.

### 2. Donald Trump has manipulated the media to help create a fictionalized version of himself. 

If you're wondering how Trump attracted so many voters in 2016, it's important to look at the public image he constructed. He's positioned himself as a savior, a sort of modern Midas with the ability to step in and turn any project into gold.

Trump has a deep understanding of the media's workings, preferences and weaknesses. Because of this understanding, he has been able to craft a public image that has captured the public imagination.

Many journalists work under strict deadlines, which don't always allow for in-depth research. Trump is keenly aware of this weakness, which is why he feeds the media ready-to-print stories, complete with images that support his agenda.

He's had years of practice at this, and the stories he delivers are often full of deception.

In the 1970s, for instance, he agreed to a lawsuit settlement that forced him to accept non-white tenants in his New York apartments. But when he spoke to the press, he spun the story to emphasize that he didn't have to admit any guilt in the "minor settlement."

And in June of 2015, when Trump announced his presidential campaign, it already looked like a success; the auditorium at Trump Tower was filled with young people cheering enthusiastically. Yet this was a carefully staged event for the press. Many of the participants were extras who had been hired for $50 to smile and clap.

Trump's history of media deception even includes him posing as fictional people within his organization during phone interviews to plant favorable stories that cast him in a positive light.

In 1980, he called the _New York Times_ pretending to be "John Baron," the vice president of The Trump Organization. He then attempted to dispel the rumors about a work of art that had gone missing at one of his construction sites, in the hopes of preventing a scandal.

Years later, this time with _People_ magazine, he claimed to be an assistant named "John Miller." As Miller, Trump told the reporter that his boss — that is, Donald Trump — was unable to give an interview due to so many beautiful celebrities, such as Madonna and Kim Basinger, also vying for his attention.

### 3. Trump distorts the truth and uses intimidation tactics to maintain his image. 

Maintaining the public image of a powerful and flawless businessman is difficult. And since Trump's success depends, in large part, on his facade, he'll do almost anything to defend it.

This defense has often involved suing or threatening to sue any journalist who publishes or intends to publish something that questions the veracity of his claims or the prudence of his actions.

Even if he knows he can't win a particular lawsuit, the threat of one still scares off many publishers and journalists who either lack the funds to deal with the legal fees involved or dread the thought of years of arbitration.

Trump is especially defensive about the image of his vast wealth, which is why he sued Timothy O'Brien, the author of _TrumpNation_. Trump wants the media to believe that his net worth is in the billions, but, based on documents he personally showed to O'Brien, it would seem he's actually worth somewhere between $150 and $250 million.

For years, O'Brien was bogged down by the lawsuit, which was eventually dismissed. Trump wasn't displeased with the outcome, however, and he even admitted that the real goal of the lawsuit was to make O'Brien's life miserable. So, in that regard, the lawsuit was a success for Trump.

Trump has also become a master of deflecting any facts that might contradict the image he nurtures.

Here's a good example: during his campaign for president, in an appearance on NBC's _Today Show_, Trump was asked about his court testimony in 1991, when he confessed to posing as John Baron and John Miller.

At first, Trump simply lied and claimed that this was the first time he'd ever heard about such a thing. But when the host pressed him about it, Trump expertly deflected the inquiry by questioning its legitimacy and suggesting that even asking about a 25-year-old incident was below the dignity of a journalist.

As we can see, Trump has a unique way of handling facts, which we'll explore further in the next blinks.

### 4. Trump tries to hide the considerable gaps in his knowledge by making up his own facts. 

Another carefully constructed part of Trump's image is the facade of superior intellect. He tries to give people the impression that he has the answer to every question.

But as hard as Trump tries to keep up the appearance of intellectual superiority, it's clear there are some holes in his knowledge.

When the author first met with Trump in the 1980s, they talked about his casino business in Atlantic City. When it became apparent that Trump was somewhat clueless about gambling, the author made up some fake rules about the game of craps. Trump took the bait and began to elaborate on the author's made-up details, acting like he knew what he was talking about.

This could be seen during the 2015 Republican presidential debate as well, when Trump was asked about his thoughts on the "nuclear triad" — the capability of launching nuclear attacks by airplanes, ground missiles or submarines.

Clearly ignorant of this triad, Trump went far off topic into nuclear power and his 2003 opposition to the war in Iraq. When pressed on the question, his nonsensical response was, "I think — I think, for me, nuclear is just the power, the devastation is very important to me."

Aside from his lack of knowledge, another distressing phenomenon is Trump's refusal to see facts as objective truths. Instead, his acceptance of facts seems to be a function of his mood at the time.

This was clear in his lawsuit against Timothy O'Brien. When asked under oath about his net worth, Trump couldn't offer any hard facts or figures. Instead, he said that this amount fluctuates by billions of dollars based on how he feels about the economy on any given day.

So, for Trump, a hard figure like someone's net worth isn't a fact, but rather something that's open to interpretation based on feelings, moods and outside circumstances.

In the next blink, we'll see how this approach to facts influences his business ethics.

### 5. Trump has a long history of illegal business practices. 

Another key ingredient to Trump's success is his unethical business methods, which have allowed him to pursue big money opportunities, even when they weren't exactly legal.

Such was the case in 1980, when Trump was planning the construction of Trump Tower. There was a problem: another building, the twelve-story Bonwit Teller building, was in his way.

To avoid paying union wages and following regulations, he put together an illegal Polish demolition crew to work 84-hour workweeks, at four to five dollars per hour, with no hard hats, facemasks or protection from asbestos.

During the day, around 50 workers would be on site, but at night, when no one was looking, there were 200 workers there — a dangerously high number. Worst of all, many workers weren't paid and they had to sue Trump, who claimed he didn't know anything about the arrangement. It would take 18 years for them to see a single penny.

One of Trump's more fraudulent enterprises — Trump University — resulted in even more lawsuits.

Even the name is misleading since it wasn't a university at all, but rather a firm that offered programs for learning about the real estate business. Even though it's illegal for an unauthorized enterprise to use the word "university," and New York officials ordered Trump to stop using it, he managed to ignore them for five years.

But that wasn't all Trump ignored. He promised students there'd be a staff of hand-picked teachers at his "university," but during his testimony, he couldn't name a single one. And then it turned out they weren't teachers at all, but salespeople, many of whom had no experience in real estate.

One has to wonder why a three-day seminar would cost nearly $1,500 — especially since consumer-fraud agents determined that there was no practical value in the out-of-date material that was being taught.

> _Donald Trump has been involved in more than 4,000 legal battles._

### 6. Trump has profited thanks to help from known criminals and mob associates. 

Considering Trump's lack of business ethics, perhaps it won't come as a shock that he's also done business with known criminals.

Yet you might not be aware that those business associates include a mentor-like figure with deep Mafia connections.

That mentor was the lawyer Roy Cohn, who played a key role in the Trump Tower project.

Trump's willingness to cut corners on Trump Tower extended to his decision to use ready-mix concrete, a cheap and relatively risky option that needs to be poured quickly. This made the building process especially vulnerable to the ongoing union strikes that were going on at the time.

However, with Cohn on his side, Trump had the ear of the New York mobsters, who wielded great influence in the unions. Those mobsters were some of Cohn's biggest clients and together they ensured that the construction of Trump Tower continued even during the 1982 cement-workers strike.

Another of Trump's infamous business associates was Joseph Weichselbaum, a major drug trafficker who had already been convicted of grand theft auto and embezzlement when their relationship began in 1982.

That year, Trump asked Weichselbaum to help him helicopter in the high-roller clients of his Atlantic City casino. As it was common for big spenders to get whatever they desired, including drugs, it is believed that this was also part of Weichselbaum's role.

In 1985, Weichselbaum again faced charges of drug trafficking, but the case was mysteriously moved to New Jersey, where it was assigned a new judge who just happened to be Trump's older sister. She ended up resigning from the case, but Trump did Weichselbaum a special favor by writing a letter to personally ask the new judge for leniency. And it appears to have worked. Weichselbaum only served an 18-month prison term, while others involved in his case received sentences of twenty years.

Once Weichselbaum was released, he got his own apartment in Trump Tower, and the two continued doing business together.

### 7. Trump is not as charitable as he claims to be. 

We've seen how Trump's business practices are largely motivated by greed and cutting whatever corners he can. Yet a significant part of his public image is that of a charitable man, or as he puts it, "an ardent philanthropist."

But a look at his charitable record tells the story of someone who likes to make big promises but gives nothing.

In 1987, Trump made a public announcement that the royalties of his recent book, _The Art of the Deal_, would be donated to three groups of people: the homeless, veterans of the Vietnam war and people with deadly diseases.

Based on initial sales, these donations were estimated at around $4 million. But the only sign of any charitable gesture was the $2 million that he gave to the Donald J. Trump Foundation, with no evidence that it ever benefited the people he said it would.

In February of 2016, at a campaign rally in Iowa, another charitable announcement was made. He said he'd raise $6 million dollars for veterans.

The _Washington Post_ kept an eye on this in the months afterward and found no evidence of any money being donated to a veteran-related charity. Yet when Trump discovered that the _Post_ was about to publish their story, he made an overnight transfer of $5.6 million to veteran groups.

Trump's avoidance of spending money on good causes can also be seen in his tax dodging.

Despite his frequent claims of being a multi-billionaire, Trump has a variety of schemes set up to avoid paying federal income tax.

He does this by taking advantage of a tax loophole that allows real estate investors to offset their total income with whatever losses the value their buildings might have undergone. So, by adjusting these numbers, his income can be negative while he continues to earn millions and enjoy a lavish lifestyle.

Trump has proudly admitted to not paying any income tax. No one is sure how long this has been going on, but he didn't pay any taxes in 1978 and 1979, and in 1995 he claimed a loss of $916 million which allowed him to avoid income taxes for 18 years.

### 8. Revenge plays a significant part in Trump’s business and family life. 

Donald Trump puts everyone into one of two categories: friend or enemy. And chances are you won't be confused about which group he's placed you in, since he tends to show no mercy to those in the latter.

Revenge has become such a regular part of doing business for Trump that he even devoted a whole chapter to it in his book _Think Big_.

In this chapter, he describes how he fired one of his employees when she wouldn't ask her friend, who was a top executive at a bank, to do a favor for him. But he didn't just fire her. He wrote bad reference letters for her and was happy when he heard of her company's bankruptcy and that she'd lost her home and her husband.

Sadly, Trump's taste for vengeance isn't limited to the business world.

In 2000, a lawsuit arose when Trump tried to keep the family of his brother Fred, Jr. from inheriting any money from Fred, Jr. and Donald's father, Fred Trump. When the family sued Trump, they accused him of unfairly influencing the impaired mind of his elderly father, but this just made matters worse.

Trump then withdrew all health-care benefits for the family, which jeopardized the life of Fred, Jr.'s seriously ill infant grandson, William Trump. Donald's grandnephew had been suffering from seizures since birth and was especially dependent on medical care. It took a judge to reverse Trump's action and continue providing care for the child.

When asked if his actions weren't coldhearted, Trump replied that he couldn't help it.

Trump is who he is, but the facts — the records of his thousands of legal battles — show just how different he really is from the phony image he's created.

### 9. Final summary 

The key message in this book:

**Donald Trump has spent decades in the public spotlight and has masterfully created a public image that is quite different from the man behind it. The real Donald Trump is revealed by the thousands of lawsuits that show him to be a petty, immoral and vengeful man who will break the law and risk the lives of others to save money and get even.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Crippled America_** **by Donald J. Trump**

In _Crippled America_ (2015), American businessman and billionaire Donald J. Trump diagnoses the problems America is facing today and explains how he will truly make America great again. With disastrous trade agreements hurting our middle class and the government failing to protect us against ISIS, it's no wonder America doesn't win anymore. Some major changes need to be made — and Trump is going to make them.
---

### David Cay Johnston

David Cay Johnston is a Pulitzer Prize-winning investigative journalist at the _New York Times_, where he's been following Trump's career for over 30 years. He's also the author of the best-selling books _Free Lunch_ and _The Fine Print_. He teaches at the Syracuse University College of Law.

