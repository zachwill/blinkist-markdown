---
id: 53f460d53433390008350000
slug: the-man-who-fed-the-world-en
published_date: 2014-08-19T00:00:00.000+00:00
author: Leon Hesser
title: The Man Who Fed the World
subtitle: Nobel Peace Prize Laureate Norman Borlaug and His Battle to End World Hunger
main_color: 866A57
text_color: 866A57
---

# The Man Who Fed the World

_Nobel Peace Prize Laureate Norman Borlaug and His Battle to End World Hunger_

**Leon Hesser**

_The_ _Man_ _Who_ _Fed_ _the_ _World_ tells the story of Norman Borlaug, Nobel Peace Prize laureate, and his work in fighting world hunger. The past and future of world agriculture and hunger are examined.

---
### 1. What’s in it for me? Find out how one man saved a billion people from starvation. 

In the 1960s, the issues of famine and mass-scale starvation were prominently discussed around the world. In some countries, the situation was so dire that the international community considered doing nothing, as starvation was seen as something that was "necessary." At the time, it seemed as if there was simply not enough food to feed the world.

One Iowa-born agricultural scientist felt differently, however, and threw himself at the challenge of increasing wheat crop yields. Thanks to the efforts of Norman Borlaug, wheat yields as well as the grain's resistance to disease improved so radically that horrors such as "necessary starvation" were averted.

In these blinks, you'll be captivated by the story of this remarkable man and discover:

  * why the threat of starvation far outweighs any argument against biotechnology,

  * why famine on a massive scale is still a lurking danger, and

  * how lessons from a small farm in Iowa helped to successfully fight world hunger.

### 2. Norman Borlaug devoted his head, hands and heart to fighting world hunger. 

The most influential figure of the twentieth century came from very humble beginnings. Norman Borlaug was born in 1914 on the Iowa prairie and was educated in a one-room country schoolhouse.

Yet as an adult, he championed the fight against world hunger from the 1930s until his death and profoundly changed the way we think about agriculture.

How did he do this?

The Nobel Prize committee, which awarded Borlaug the Nobel Peace Prize in 1970, summed it up best.

"You have made the fight against hunger your lifelong mission, your passionate calling, to which you have devoted your brains, the brains of a first-rate scientist, your hands, the hands of the Iowa farmer, and your open, broad, warm heart."

Borlaug's life goal was to fight world hunger, and he pursued this goal with great zeal and optimism.

Optimism was a trait he always carried, even when faced with dire news. For example, in the 1960s when it looked like the world was facing hunger conditions on a near-apocalyptic scale, Borlaug saw a way out, even when no one else did.

Importantly, Borlaug's optimism was fuelled by his strong belief in human potential.

Borlaug's childhood experiences also helped form his passion to end world hunger. The rural environment of Iowa imbued him with a strong impulse to help others in need as well as made him a strong believer in the importance of education and hard work.

As his family was poor, Borlaug at an early age learned to work hard and help his family with farming duties. His parents also emphasized the importance of a good education, and though he was not the strongest student, his devotion and dedication made him stand out among his peers.

Borlaug also wrestled in school, and recounted a particularly valuable lesson he learned from his wrestling trainer that he then applied to his later life. He would always fully devote himself to any task; that is, either give your all, or don't bother trying.

### 3. Act now, perfect later: Borlaug took risks to push for immediate solutions to the threat of hunger. 

Borlaug was highly committed to his goal of eradicating world hunger, but he was no perfectionist.

Although he wanted to constantly improve on what he viewed as potential solutions to the problem, what motivated him was making an immediate difference — putting his ideas to the test — as the threat of hunger waits for no one.

He knew he had to take risks to find solutions that would work quickly. When asked about this strategy, he said, "You can't eat potential."

Increasing the yield of wheat crops was the center of Borlaug's agricultural research. The goal was to essentially make plants more productive, so that more food could be extracted from the same amount of farmland.

The new farming techniques and high-yielding wheat varieties that Borlaug's work produced sparked what is now known as the "green revolution," a movement which spread across many countries, from Mexico to India, helping governments meet the food demands of their countries' growing populations.

But getting everyone on board and working together wasn't easy. One of the prime drivers of Borlaug's success — and thus the green revolution — was his strong work ethic. Working hard, day in and day out, wasn't just habit for Borlaug: it was a question of honor. His dedication inspired those around him.

Borlaug's work in India is a key example of how his work ethic and emphasis on collaboration bore fruit. In India, small-scale farming was difficult to organize and steeped in old traditions. The government needed to offer incentives for farmers to use new seeds; what's more, companies had to pitch in to make sure an adequate amount of fertilizer was available when farmers needed it.

Borlaug's tireless efforts to bring all the parties involved together was ultimately successful, and the adoption of high-yield wheat in India saved countless lives.

> _"Perfection is a butterfly the academics chase and never catch … We will have to do the best we can with what we have."_

### 4. Society and government need to partner to uproot deeply entrenched agricultural traditions. 

Changing agricultural practices that date back decades — or even centuries — is no easy task.

Since most societies have their roots in agriculture, any massive-scale change requires the backing of the whole society. This sort of change can only be achieved through aggressive grassroots campaigning combined with governmental cooperation.

One of the things that riled Borlaug were the inefficient government bureaucracies he encountered. He saw the threat of famine increasing every day due to out-of-control population growth, and yet the very governments he was trying to help were obstructing necessary changes.

For example, in Pakistan a new government made the grievous error of lowering the set price at which farmers could sell wheat. While the idea was that this would help poor people afford wheat, Borlaug predicted that this change would be disastrous, as farmers would refuse to sell their grain.

Indeed, this is what happened: farmers began hoarding grain, hoping to sell it for a higher price on the world market; but this couldn't be done either, due to government regulations.

In his later years, Borlaug also criticized modern governments for their insistence on regulating the development of biotechnology. He himself hoped that biotechnology could help to stop world hunger and thus should have governmental support, not disapproval.

This kind of directness even on controversial issues was typical of Borlaug, and it helped him generate support and publicity to advocate for government assistance and for policy changes. He never hesitated to argue loudly, publicly and undiplomatically with politicians if he felt that a policy change was the only way to avoid a famine.

This directness is how he succeeded in convincing the Mexican and later the Pakistani and Indian governments to support the production of more crop fertilizer and allow farmers to sell their grain at higher prices.

Next, let's take a closer look at the green revolution that Borlaug started.

### 5. The green revolution sprouted in Mexico then spread, thanks to an international research network. 

In the 1940s, the green revolution had its early beginnings in Mexico, where Borlaug was working on high-yielding wheat varieties, funded by the Rockefeller Foundation.

The work was to profoundly change Mexican agriculture. At this time, however, these agricultural changes weren't yet known as a green revolution, but rather as just a wheat and corn revolution.

Borlaug led the program for wheat, the second most-important crop in Mexico after corn, and the varieties and practices he developed eventually spread to India and Pakistan, thus inspiring a widespread green revolution.

So how did Borlaug turn the Mexican wheat revolution into a global green revolution?

First, he trained local scientists how to run their national programs independently from international leadership and funding, so that they could continue their work even if an international program ended.

At the same time, Borlaug used a network of international research centers to spread local discoveries and best practices globally. One example of this is that Borlaug always insisted that scientists should carry out their own field research. This actually became the norm for all international research centers.

Borlaug's view was that national programs were the vanguard in fighting hunger, but that they could also benefit tremendously from the work of the international research centers, as such centers were well-funded, independent and nonpolitical.

The green revolution proved that with international connections and funding, it is possible to fight world hunger.

### 6. Despite criticism, the green revolution made strides toward curing hunger and ensuring peace. 

You may be wondering why Borlaug won the Nobel Peace Prize, considering that his mission was ending hunger, not war.

The argument the Nobel Prize committee used was that by helping avert hunger and famine, Borlaug simultaneously furthered the cause of world peace, as famine can be an incentive for going to war. It was this logic that made Borlaug a Nobel laureate.

In his acceptance speech, Borlaug already warned that the next threat the world food supply would face was out-of-control population growth. He felt that population-control advocates and those working to end world hunger should actually join forces.

This, in addition to other controversial opinions, made Borlaug and his green revolution a target for many critics. Yet this did not bother Borlaug.

The most common criticisms were directed at Borlaug's farming techniques, which revolved around farmers using lots of chemical fertilizer to induce growth and buying high-yield wheat seeds instead of cultivating the seeds themselves. Critics felt that this dynamic would lead to the agriculture business consolidating into just a few big players, while at the same time creating an exodus of unemployed farmers to cities.

Borlaug believed his critics were pseudo-scientists who had not considered the real implications of their criticisms. For example, while fertilizer could be produced through natural means, it would never be enough to feed the world. Chemical fertilizer was simply a necessity.

Borlaug also felt strongly that many of his critics were elitists who had never experienced true hunger. This was in contrast to Borlaug, who himself had experienced poverty first-hand during the Great Depression in the late 1920s and 1930s. Experiences like this helped him understand the effects of hunger, and solidified his view that immediate action was required.

For Borlaug, there was only one group of people whose opinion he truly cared about: the poor and the hungry. And they did not criticise him. On the contrary, they celebrated and praised both the green revolution and Borlaug himself.

Today, Borlaug's green revolution is credited with having saved around a billion lives. So how can we continue his legacy and continue to fight world hunger today?

> _"... he worked day in and day out, from sunup to sundown, in the heat and dust and wind and rain — always in the fields and experimental plots or laboratories."_

### 7. To really make a difference, scientists need to leave the lab and get into the fields. 

As the Nobel Prize committee stated, Borlaug had the brain of a scientist. But he was also a very practical man who disdained the pride and complacency that he saw in many scientists.

He was constantly worried about new plagues and pests that could wipe out entire crops. He felt strongly that it was the duty of the scientific community to stay one step ahead of such developments, growing alternative crop varieties as contingencies.

His concern with the spread of diseases was manifested by the fact that of all the trial plots he examined, he was always more interested in his disease-riddled plots than the healthier ones. He was ever mindful of the danger of diseases.

Another key tenet Borlaug believed in was that to truly understand agriculture, scientists had to get their hands dirty by working on a farm. This was the only way that a farmer would accept and respect them, respect being a prerequisite for a farmer to accept advice from an outsider.

What's more, such experience would also allow scientists to communicate effectively with farmers, many of whom are often uneducated or even illiterate.

The only way to tap into the wealth of knowledge that these farmers have is for scientists to get to know their work and to learn how to talk to them. The information available this way is far more valuable than anything gleaned from governments.

### 8. Borlaug worked from the ground up, teaching locals and in turn, convincing governments. 

Although Borlaug was a man of action, he also knew that he could not do everything by himself. This is why he saw himself primarily as a teacher.

In the field of agricultural science, there has always been a shortage of trained and motivated personnel. Borlaug knew this was a problem he had to address if he truly wanted to effect worldwide change.

To solve this issue, he chose to work from the ground up, finding employment opportunities for young and inexperienced scientists and giving them responsibility so they could learn on the job.

Many of the young scientists who were trained and who worked as interns in Borlaug's agricultural training program in Mexico went on to have successful international careers. Perhaps the most shining example is that of Ignacio "Nacho" Narvaez, who was trained by Borlaug and later became the head of the Mexican agricultural research program.

Though this bottom-up approach was slow, it was necessary because radical measures would have spooked local governments and farmers. It was only by training locals and showcasing whatever successes they had that Borlaug was able to bring about change.

So rather than laying down a long list of things that had to be modified, Borlaug illustrated the potential of his work by showcasing his flourishing experimental fields. This approach of showing, not telling, lead to interest in Borlaug's recommendations and created pressure on governments to implement changes more broadly.

The success of this approach can be seen in the case when the Indian government refused to follow Borlaug's advice in using large amounts of fertilizer on each field. Rather than try to argue, Borlaug simply showed them the great success of his highly fertilized fields in Pakistan. The Indian government was convinced, and soon after the country was producing so much grain it was totally independent of food imports.

In the next blinks, we'll examine what the future of agriculture will be like.

### 9. The green revolution was only temporary; we need new research and action immediately. 

When Borlaug started the green revolution, the danger of famine around the world was very real. And though his work helped avert disaster, it was only a temporary victory and not a permanent solution.

Support for agricultural research has decreased sharply since the late 1980s. Because it takes relatively long to turn any research findings into concrete farming solutions, it could be that we're already running out of time to address the next hunger crisis. This was a major source of concern for Borlaug.

Meanwhile, poverty is still widespread and many countries struggle with managing their food supply in the face of rapid population growth.

Since the 1980s, however, Borlaug focused his efforts on combating hunger and famine in Sub-Saharan Africa. In many ways, this area is even more at risk than India was in the 1960s as it lacks irrigation systems and other vital infrastructure, experiences frequent droughts and maintains isolated rural communities.

Africa has even seen new strains of plant plagues emerge, such as _stem_ _rust_, which is a devastating disease for wheat. Stem rust has not been a problem for decades, thanks to Borlaug's previous work developing wheat plants that are resistant to it. But without further research and development, new diseases like this can wipe out crops in Africa and spread elsewhere, too.

To avert severe hunger and the emergence of such diseases in Sub-Saharan Africa, it is imperative that funding for agricultural research be increased.

Before Borlaug passed away in 2009, his biggest concern was the lack of new research and international engagement in Africa. He knew that the achievements of the green revolution alone could not save the continent from the threat of hunger.

### 10. Borlaug fully supported taking advantage of biotechnology to help fight hunger. 

One famously controversial area in agricultural research is the use of biotechnology. But for Borlaug, it was not controversial at all: he was a staunch advocate of using it to feed the world.

Borlaug knew that to feed a growing global population, more food would be needed. Some could come from cultivating crops on previously unused land, such as in Brazil or Russia. These areas can now be exploited for food production, thanks to salt-resistant plants, for example.

But this alone will not be enough to feed everyone. The rest of the shortfall will have to be made up through further increases to crop yields, and biotechnology is the method to attain these increases.

Borlaug saw biotechnology as just a more advanced form of experimental genetics and plant breeding. It makes larger changes possible in a shorter time span than traditional methods do. He emphasized that nature has always evolved by crossing genetic barriers and creating transgenic plants, so essentially there is nothing unnatural about biotechnology.

Biotechnology could not only produce plants resistant to plagues and pests but also to extreme weather, like droughts and torrential downpours. It could also increase crop yields per acre drastically.

Due to all these potential benefits, Borlaug saw the arguments of those who opposed biotechnology as ideologically motivated pseudoscience. He felt it unconscionable to deny the benefits of biotechnology based on such motives.

He wanted to see more research in the field of biotechnology and felt that excessive governmental regulation, as could be seen for example in the European Union, was harmful to all of mankind.

Borlaug's stance was that the dangers of malnutrition and mass starvation far outweighed any criticisms leveled against biotechnology. His strong advocacy was important as it helped increase popular support for biotechnology.

### 11. Final summary 

The key message in this book:

**Norman** **Borlaug** **was** **an** **outstanding** **scientist** **who** **devoted** **his** **entire** **life** **to** **fighting** **world** **hunger.** **The** **result** **was** **the** **green** **revolution,** **an** **agricultural** **movement** **that** **helped** **feed** **the** **globe** **for** **decades.** **But** **today,** **more** **research** **that** **follows** **in** **Borlaug's** **footsteps** **is** **needed** **if** **we** **are** **to** **avoid** **the** **ever-present** **threat** **of** **famine** **and** **starvation.**

Actionable advice:

**To** **teach** **people,** **give** **them** **responsibility** **and** **let** **them** **get** **their** **hands** **dirty.**

The next time you find yourself needing to teach or train someone, take a page from Borlaug's book: Give them plenty of responsibility so they learn the best way to manage it, and also insist that they dive into the most tangible part of the work, so they truly understand what the purpose of their job is.

**Suggested** **further** **reading:** **_The_** **_Blue_** **_Sweater_**

_The_ _Blue_ _Sweater_ is an autobiographical look at author Jacqueline Novogratz' travels in Africa and how her experiences helped her understand the failures of traditional charity. These blinks also outline why a new type of philanthropic investing, called "patient capital" and developed by the author, may be part of the answer.
---

### Leon Hesser

Leon Hesser is an agricultural economist and former farmer who coordinated the U.S. program to increase food production worldwide.

