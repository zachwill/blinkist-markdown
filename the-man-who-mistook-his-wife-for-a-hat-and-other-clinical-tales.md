---
id: 56c1d855587c820007000043
slug: the-man-who-mistook-his-wife-for-a-hat-and-other-clinical-tales-en
published_date: 2016-02-17T00:00:00.000+00:00
author: Oliver Sacks
title: The Man Who Mistook His Wife for a Hat and Other Clinical Tales
subtitle: And Other Clinical Tales
main_color: 64BDC5
text_color: 346266
---

# The Man Who Mistook His Wife for a Hat and Other Clinical Tales

_And Other Clinical Tales_

**Oliver Sacks**

_The Man Who Mistook His Wife for a Hat and Other Clinical Tales_ (1985) explores the fascinating effects of brain damage on our perception, capabilities, personalities and behavior by examining some of the world's most interesting (and bizarre) psychological and neuroscientific cases.

---
### 1. What’s in it for me? Discover the unusual ways brain damage can affect behavior. 

Our brain makes us who we are. It determines how we think, how we behave and how we remember. For most people, the brain generally behaves in a predictable way, leading to behavior that society considers "normal." 

However, some individuals' brains don't function quite as they should. It may be due to injury, illness or genetics, but brains can become damaged and begin to malfunction. When this happens, people can start behaving in ways that, by pretty much any standard, would be considered different or unusual.

These blinks tell the tale of people who have experienced brain damage, and how they have found ways to construct lives around their disabilities. 

In these blinks, you'll also learn

  * why one woman spends her life imitating other people;

  * why an old man lived his entire life as if he were in his twenties; and

  * why changes in brain composition allowed one man to smell the color brown.

### 2. Brain damage changes the personality and the behavior of a patient. 

Imagine you're a neuroscientist and you come across these three cases: 

First, a man mistakes parking meters for children at play, and affectionately caresses their "heads." He also mistakes his wife's head for a hat and causes quite a commotion when he tries to put it on. 

Second, a patient at your hospital lives in constant fear, as she believes someone has left their leg in her bed. In reality, the offending limb is her own.

Third, a man understands body language, facial movements and tone perfectly, but has absolutely no comprehension of language. 

So, what connects these people? They all suffer from some form of _brain damage_.

Damage to the brain can severely alter or impair our abilities, regardless of whether it was caused by a tumor, poison, inflammation or a stroke.

For example, if your brain overproduces the neurotransmitter _dopamine_, then you're likely to develop compulsive twitching, imitate others and grimace and curse without being able to stop it.

Even brain damage caused by alcoholism can have severe impacts on the mind. For example, _Korsakow syndrome_ (which is named after a Russian psychiatrist and neurologist who studied alcoholics) is caused by alcohol-inflicted damage to the _hippocampus_. Many sufferers of Korsakow syndrome lose years' worth of memories and can't retain new information. 

Brain damage can lead to the most grotesque and absurd sounding impairments, but in rare cases it can actually have a positive impact on a person's life.

Such was the case for an 89-year-old woman who suddenly started to feel energetic and more open than ever, and even started flirting with younger men. The cause was _neurosyphilis_, which stimulated her _ancient cerebral cortex._

On one hand, she wanted to stop the disease from getting worse, as neurosyphilis can be fatal; but she also desperately wanted to maintain the positive effects of feeling youthful, self-confident and happier than she had ever been.

Clearly, brain damage can cause people to act very strangely. In the following blinks, we'll look at some specific examples of the other bizarre behavior it can cause.

### 3. In an effort to compensate for malfunctions, the brain will sometimes create its own bizarre reality. 

In the 1980s, American sailor Jimmie G related his life story to his doctor. He could easily remember the streets and friends of his childhood, as well as his time in school. Looking back on events that happened decades ago, he could recall information just as well as anyone else. 

But when he described his time as a radio operator in the Marines, something would change: Suddenly, he switches to the present tense, and is convinced that he' s still the young soldier from 30 years ago, only just home from World War II. Why?

When the brain is damaged, it can invent stories to help compensate for a world that just doesn't make sense. Whether it's damaged by alcohol, disease or even brain tumors, the brain can come up with absolutely fascinating explanations and parallel worlds to make everything appear normal once again.

Sometimes, the brain overcompensates and invents stories that are patently bizarre. In other cases, the stories take the form of memory loss, whereby the patient reverts to times long since past.

Jimmie G had lost the last 30 years of his memories due to his alcoholism. To compensate for this loss and make sense of the world, his brain concocted a whole new reality in which he continued living in the year 1945.

Many patients like Jimmie G also suffer from some kind of inhibition to their normal functions. When the brain is compromised, these patients sometimes lose the ability to speak or have altered perceptions of their own bodies.

Because their brains have constructed new realities, these patients live in a world that seems utterly bizarre to most of us.

### 4. Damage to the visual center can cause bizarre new perceptions. 

Most people think their eyes are to thank for their ability to see. But really, they should be thanking their brains. 

The case of Dr. P, a professor of music, offers a great example of the ways the brain can affect our visual perception. 

At first glance, Dr. P was a normal, charming and well-educated person, with a great sense of humor and a gift for playing the piano. But he had a problem: he suffered from brain damage that affected the way he saw things.

For example, he would see faces where there were none, and began patting the "heads" of fire hydrants and parking meters, believing them to be children. He would even walk up to grandfather clocks to offer them a warm greeting!

When someone's _visual center_ is damaged, as was the case with Dr. P, it can fundamentally alter the way they perceive the world.

Our eyes are connected to the visual center in our brain, and though our eyes are responsible for accessing visual stimuli, it's the visual center that connects what we see to what we know, thus enabling us to identify objects. 

So, if the visual center is damaged, our ability to understand what we see is likewise impaired. 

Dr. P's eyes worked just fine; his problem involved the visual center of his brain. Damage to the visual center can cause _visual agnosia_, an impairment in our ability to recognize objects. 

For example, Dr. P wouldn't perceive a glove as a glove, but rather as a surface with five weird protuberances. He even suggested that they could be containers for coins! Dr. P was unable to see and recognize objects for what they were and instead saw only their characteristics.

On the other hand, he had no problem with structures and abstract objects. Playing cards and dice, for example could be done simply by recognizing characteristics. He could even play complicated games, like chess, by analyzing the board and imagining his next move. ****

### 5. Excess brain activity can also lead to severe impairments. 

So far, we've looked at the ways in which brain damage causes a loss of certain functions. However, _excess_ brain activity can also lead to fundamental changes in personality and behavior.

Sometimes certain areas of the brain will produce more chemicals than they're supposed to, and this can have extraordinary effects.

For example, there is one case of a woman who compulsively imitated passers by as she stood in the middle of a pedestrian zone. In only a tenth of a second, she could perceive their subtle gestures and start to imitate them in ridiculous ways — but only for a few seconds. She would then move on to the next person. In just two minutes, she was able to imitate over 50 people. 

This woman suffered from a severe form of _Tourette's syndrome_, caused by an excess of dopamine. Unable to control her body's movements, she was condemned to a life devoid of individual identity and inhibitions; she could only copy others.

Another interesting case of brain activity run amok involves a patient who only remembers the world around him for a few seconds. As a result, he first identifies his doctor as a customer, then as an old friend then a butcher and sometimes even as Sigmund Freud.

To add some stability to his life, he escapes into fantasy: sometimes he is a character in the ancient story _A Thousand and One Nights_. Other times he's a priest or a seller of gourmet food. Unable to remember his circumstances, he constantly adjusts his fantasies to the events around him as he perceives them in that moment. 

Both of these patients suffer from excessive brain activity, the first leading to extreme compulsions, and the second to exuberant fantasies. Both people are governed by their illness, which is the center of their entire existence.

### 6. Changes in the brain can cause a shift in perception and bring back long-forgotten memories. 

One night, a young man dreamed that he was a dog, complete with an incredible sense of smell. When he awoke, he found that his olfactory sense had dramatically improved. His newfound abilities enabled him to recognize all the shops in New York City by their smell alone! He was able to smell dozens of different nuances of brown, and could even smell feelings like pain or excitement.

This change in perception was once again caused by changes in the brain. This young man's improved olfactory sense was like an awakening of his primal sense of smell, it was as if he were experiencing the world as our ancient ancestors might have experienced it, when smell was crucial to survival. So, brain damage can actually bring back capabilities lost by modern humans, but which remain hidden in the brain.

Similarly, changes in the brain can sometimes activate memories that seemed long-since forgotten bringing them back with startling clarity.

Consider, for example, the case of a young Indian woman who started feeling weak and numb on the left side of her body. The cause was a tumor growing in her temporal lobe, which sometimes caused seizures. 

One side effect of this tumor was that she became dreamy, and began visually remembering her memories. She had visions of her old homes and gardens, as well as villages that she had visited long ago. But she was also able to remember entire conversations, people, songs and dances from the distant past.

Then there's the case of a man who murdered his girlfriend under the influence of drugs. Since he had blacked out during the murder, he was unable to remember his actions. Years later, however, he suffered a severe contusion of the temporal lobes; suddenly, the terrible memories of the night of the murder resurfaced, unbidden and uncontrollable.

### 7. The stimulation of the right temporal lobe causes auditory hallucinations. 

Here's a strange story: A patient named Mrs. O'C, a nostalgic, elderly and nearly deaf woman, was constantly reminded of her childhood in an unusual way. Suddenly, out of nowhere, she'd start hearing old Irish songs. But they weren't coming from the radio — so where did they come from?

Again, the answer lies in brain damage. Mrs. O'C had suffered a stroke in her _right temporal lobe,_ the area of the brain that remembers the basics of music. Each time the music started to "play," her brain was actually undergoing temporal lobe seizures. 

The temporal lobe is responsible for reminiscence and memory, and seizures in this part of the brain can cause hallucinations, sometimes in the form of music. 

So, even though the music had no outside source, Mrs. O'C still heard the melodies and had no way to stop them! In fact, the music in her head was sometimes so loud that she couldn't even have a normal conversation; the noise made it impossible for her to understand other people.

Over time, the music gradually "turned down," finally allowing her to communicate almost as well as before. Eventually, the melodies faded and became sporadic. As the effects of the stroke lessened over time, the music left with them. 

When the music eventually stopped altogether, Mrs. O'C, though incredibly relieved, found herself missing the nostalgic memories that her musical recollections had brought.

> Aspirin poisoning and temporal lobe epilepsy can cause vivid hallucinations that seem entirely real.

### 8. People with brain damage and disabilities can achieve incredible things. 

Some people might assume that intellectually disabled people would have difficulties developing special skills or talents, but that couldn't be further from the truth.

In fact, there are people with intellectual disabilities that can accomplish fantastic feats. Though lacking in intellectual ability, people with brain damage can still have fascinating and unique talents. The brains of these people can, in contrast to the average brain, specialize in certain functions, allowing them to pour all their focus into self-improvement. 

One example is the case of a 61-year-old man who suffered from meningitis in childhood and was subsequently hemiplegic, impulsive and intellectually disabled. Despite having only rudimentary education and needing help to care for himself, there is nonetheless something very special about him, namely an amazing musical memory.

This remarkable talent allows him to remember over 2,000 operas. You could ask him about the singers, stage designs and costumes of every single one of these operas, and he would answer without difficulty. He also knows the entire _Dictionary of Music and Musicians_ by heart!

Then there's the case of John and Michael, two twins with mental health issues including autism and psychosis, who suffered from hallucinations and were unable to care for themselves. Their analytic abilities were so underdeveloped that they couldn't even do simple addition or subtraction. 

However, they could somehow figure out remarkable mathematical calculations, and often spent their time thinking of prime numbers so high that they couldn't even be found in math books. Even more fascinating is that these two brothers were able to "see" numbers.

For instance, when the author dropped some matches out of a matchbox, the twins instantly shouted "111!" Amazingly, once the matches had been counted up, it turned out that they were right! They described the process as being something different from counting; rather, they simply "saw" the number 111.

Clearly, though brain damage can lead to serious impairments, it can also lead to some amazing and seemingly supernatural capabilities.

### 9. Final summary 

The key message in this book:

**Brain mapping has given us new insights regarding the complex circuits that enable specific abilities in the human brain. When these are damaged, it can lead to fantastic and truly bizarre changes in behavior, personality and perception of the world.**

**Suggested** **further** **reading:** ** _Hallucinations_** **by Oliver Sacks**

This book explores the complex realm of hallucinations, and explains how they happen not only to people who are ill, but also to those who are completely healthy. Drawing on various studies, patient cases and the author's own experiences, it describes the different causes and types of hallucinations, and shows that they're actually a common phenomenon that manifest in a variety of ways.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Oliver Sacks

Oliver Sacks was a world-renowned neurologist, writer and professor at the New York University School of Medicine. He has written several successful books, including _Musicophilia: Tales of Music and the Brain_ and _The Man Who Mistook His Wife for a Hat_. His book _Awakenings_ was adapted into an Academy Award-nominated film. **  

**

