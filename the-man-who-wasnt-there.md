---
id: 5694ad222f68270007000000
slug: the-man-who-wasnt-there-en
published_date: 2016-01-12T00:00:00.000+00:00
author: Anil Ananthaswamy
title: The Man Who Wasn't There
subtitle: Investigations into the Strange New Science of the Self
main_color: DB4048
text_color: B2343A
---

# The Man Who Wasn't There

_Investigations into the Strange New Science of the Self_

**Anil Ananthaswamy**

_The Man Who Wasn't There_ (2015) explores the mechanisms that form our fundamental sense of self, and shows what happens when things go awry. By examining the surprising effects of disorders like schizophrenia, depersonalization and autism, the book shows just how flimsy the human sense of self can be.

---
### 1. What’s in it for me? Discover how your brain fabricates your sense of self. 

Most of us have, at some point in our lives, wondered exactly who we really are. But at the same time, very few people feel that their thoughts belong to someone else, or that their body is something alien to themselves, or that they are already dead and rotting. And even in the throes of a deep teenage identity crisis, hardly any adolescent will ever lose his fundamental sense of self.

Consequently, it's easy to overlook how incredibly fragile our sense of self really is — that is, unless you know someone who suffered brain damage and all but lost herself, for instance because of Alzheimer's disease. As you'll discover in these blinks, many regions of our brains work together to make us feel like ourselves, and the tiniest damage to the right spot or an innocuous little malfunction can throw it completely off balance. 

In these blinks, you'll also find out

  * why some people are yearning to get rid of a healthy leg;

  * what causes people to feel as if they were stuck in a perpetual dream; and

  * about a man who mistook himself for his doppelgänger (and almost died as a result).

### 2. Studying people who feel like the walking dead teaches us how the brain constructs our fundamental sense of self. 

Who are you? Most of us consider the self — the subject, the "I" — as an unchanging part of who we are. We feel an attachment to this self, as well as the body it inhabits, and have no concern as to whether we are really in control of our bodies or actions; we _feel_ that we are. 

This sense of self is the result of our brain's elaborate work. But what if your brain fails to provide you with a proper sense of self? And what if, as a consequence, you come to the conclusion that you're actually _dead_?

As bizarre as it sounds, individuals who suffer from _Cotard's syndrome_ are truly, unshakeably convinced that they're dead. 

Neurologist Adam Zeman reported a particularly interesting case of Cotard's syndrome. Graham, a middle-aged patient at a psychiatric hospital, claimed that he was brain-dead. After a divorce and a failed suicide attempt, Graham suffered from severe depression and his emotions had lost all vividness. His conclusion: he must be dead. He also claimed to have lost the need to sleep, eat or drink — although he continued doing all these things — and he even stopped brushing his teeth.

When he was informed that he was, in fact, still living, he refused to believe it. 

Certain regions of the brain are vital for our sense of self. In the brains of people with Cotard's syndrome, some of these regions are damaged or misfire, thus disrupting fundamental elements of a person's sense of self, such as the feeling of being alive. 

Physicians scanned Graham's brain to see what was going on. They discovered that his _frontoparietal network_, an area of the brain that is involved with conscious awareness, was hardly showing any metabolic activity.

The network concerned with awareness of internal activities (such as emotions) was especially affected, causing him to lose awareness of his emotions and physical needs. Detached from this awareness, he concluded that he was dead.

> _"If I try to seize this self of which I feel sure . . . it is nothing but water slipping through my fingers." - Albert Camus_

### 3. Thanks to Alzheimer’s patients, we now know that our sense of self is made up of two parts. 

Most of us look back at our lives in the form of stories, and together these stories form the _narrative self_ that makes up our identity. Interestingly, Alzheimer's patients lose the ability to maintain their narrative self, and the results can be catastrophic.

According to neuropsychologist Robin Morris, a special form of semantic memory stores the data and notions we have of ourselves in a "self-representation system." These memory stores are filled with _episodic memories_, the memories of events we experience, which are then integrated into our narrative self.

The brains of those who suffer from Alzheimer's are unable to carry out this process. We can see how this plays out in a patient who had the _hippocampi_ removed from his brain — the results were similar to Alzheimer's patients. When asked what his favorite memory of his mother was, he could say little more than "Well, that she's my mother." He was no longer able to form a story of who she was based on his episodic memories of her.

And without access to this narrative, people can no longer maintain their identity.

While it might seem as if our entire sense of self boils down to the narrative self, further research into Alzheimer's has proven differently. Rather, part of the patient's sense of self is preserved, the _embodied self_, which comprises memories that are inscribed into the body. 

Typing, for example, is something you do more or less automatically. It's as if this knowledge were stored in your fingers, not in your brain.

As researcher Pia Kontos explains, this embodied knowledge persists even as the brain suffers from Alzheimer's. 

On a Jewish holiday, one of Kontos's Alzheimer's patients, who was unable to speak, lined up with other men to sing a prayer in a synagogue. Incredibly, he was able to recite the prayer just fine, as the years he spent participating in this same ritual had etched it into his body. In essence, he could perform without thinking.

### 4. Your brain works hard to convince you that your body is part of yourself – but some brains can’t do it. 

Look at your hand. Whose hand is it? It's _yours_, right? It simply _feels_ like it belongs to your body. Well, consider yourself lucky. The feeling that your limbs are part of your body is nothing to take for granted.

In fact, it takes serious work for your brain to construct this sense of ownership that most of us experience — and it's easily manipulated.

A group of scientists at Carnegie Mellon University demonstrated how easy it is to manipulate this sense of ownership. In their experiment, they told otherwise normal subjects to sit down and place one hand on the table in front of them, right next to a rubber hand. Both hands were separated by a barrier that prevented the subject from seeing their own hand, and they instead saw only the rubber one. Then the hands, both the real one and the rubber one, were stroked simultaneously with paint brushes. 

The subjects reported not only that they "felt" the brush on the fake hand _instead_ of on their real one, but for many of them the rubber hand felt like it was actually their _own_ hand.

But this can also work the other way around, where part of your body feels alien to you, resulting in _body integrity identity disorder_ (BIID).

Sufferers of BIID develop an obsession with having a specific part of their body amputated, typically an arm or a leg that feels like it doesn't belong. 

The reason lies in the brain. Normally, our brain possesses "maps" representing all our body parts. This way, when someone tickles your foot, the corresponding brain region can be activated. As long as the maps remain intact, your brain will understand that the foot belongs to you. 

However, if the maps are incomplete, then your foot might feel foreign, as if it weren't really yours.

> _". . . sufferers [of BIID] . . . precisely delineate their limb into the part that feels their own and the part that doesn't."_

### 5. Studying schizophrenia offers insights into the sense of agency over our bodies. 

Before reading on, find the most ticklish spot on your body and try to make yourself laugh. It's hard, isn't it? For some schizophrenic people, it's no problem at all! But why?

In general, schizophrenics have lost the feeling that they are in control of their bodies. In other words, their _sense of agency_ is disrupted.

For example, if they do something like lift a glass, it doesn't feel like they were the ones who told their body what to do. This may cause them to think that they're possessed by someone else who is making them do things.

This might also explain why so many schizophrenics hear strange voices. It may just be the usual mental chatter that every person experiences — they simply fail to recognize it as their own. 

This kind of dissociation occurs when the brain mechanism responsible for generating our sense of agency is impaired.

To get an idea of how this mechanism works, imagine that you want to kick a football. Your motor cortex then sends two signals: one command to move the leg, and a copy of this command to other parts of your brain. With this copy, your brain makes predictions regarding the sensations that your leg is going to encounter during this action, such as the impact of the football.

It's the alignment of your brain's predictions and the reality of your experience that creates a sense of agency.

A schizophrenic person's brain fails to send this copy command, leaving it unable to make predictions about the sensations the body is about to experience. Without these predictions, your actions don't feel like you actually initiated them. Instead, it feels like someone — or something — else is responsible.

This explains why some schizophrenic people can tickle themselves so easily. Their brains can't predict the movement of their hands; the body is unprepared and can thus be tickled.

### 6. Studying people who feel unreal teaches us about the way our brain processes emotions. 

Imagine waking up one morning only to feel that you're still dreaming. You can pinch yourself until you're sore, but nothing seems to wake you up to a world that feels real. While this sounds more like a Hollywood movie script, this is the actual lived experience of people who suffer from _depersonalization_.

Depersonalization fundamentally disrupts your sense of self and the vividness of your overall perception. People who suffer from depersonalization feel completely detached from their body, their emotions, their life and the world around them. To them, everything feels like a dream. And while they can preserve a semblance of a normality in their everyday lives, they nonetheless feel deeply alienated. 

Again, this phenomenon is caused by malfunctioning brain mechanisms. This time, it involves the way in which your brain processes emotions, and, as with schizophrenia, it has to do with impaired prediction mechanisms.

As you've already learned, the brain is essentially a prediction machine. It perceives the world by making predictions about the causes of sensory signals and then weighing those predictions against reality.

But these predictions aren't just related to your body. The brain also makes predictions about your _body states_, which include very basic feelings like anger or enjoyment. When your brain's predictions about your feelings are accurate, you feel like they are _your_ feelings, and this feeling underpins your sense of self.

However, in cases of depersonalization, the brain fails to make accurate predictions about incoming emotional signals. Put another way, your emotions don't feel like they belong to you.

### 7. By studying autism, we can learn more about the brain as a prediction machine. 

Many people today have heard the term "autism" before. But do you know what it actually is and what causes it?

Autism describes a wide range of symptoms resulting from a disorder of the brain's development. One typical symptom of autism is a difficulty relating to others.

People with autism often have trouble interpreting other people's facial expressions and identifying their emotional states. For example, they don't naturally understand that a frowning person might be concerned about something. Rather, they have to be taught the meaning of this facial expression the same way someone would memorize vocabulary.

Autistic people often fear anything new and unforeseen, and thus thrive on repetition and rituals. For example, kids with autism often watch the same film over and over again without tiring of it. 

Like with schizophrenia or depersonalization, these symptoms are a result of problems with the brain's ability to make predictions. 

As you've learned, the brain makes sense of the world by making predictions about upcoming experiences. These predictions are based on your previous experiences, stored in the brain in the form of models.

Sometimes, however, these models are inaccurate, leading to poor predictions. The brain is "surprised" by the disparity between its prediction and reality, and seeks to correct these models with the help of new signals from the sensory organs.

But the brains of autistic individuals have problems processing these signals correctly, whether they're hunger cues from their own body or another person's sad face.

Similarly, the brains of autistic people are full of "brain static," causing feedback signals to become so distorted that they no longer make sense to the brain. 

Consequently, the feedback signals can't be used to update the brain's prediction models, meaning that it sometimes can't integrate new knowledge or stimulus.

As a result, individuals with autism regularly suffer prediction errors, leaving them continually surprised. This surprise is stressful, and is one reason why they prefer experiences, like films, that they're already familiar with.

### 8. Brain damage can cause you to doubt that you inhabit your body and cause doppelgänger hallucinations. 

The idea of a _doppelgänger_, someone's "double," is well known from books and movies. But the phenomenon is indeed real, and some people who suffer from the _doppelgänger effect_ actually hallucinate that their bodies have multiplied.

If you were to experience this complex hallucination, you'd not only see another "you," but you'd likely also feel that your center of awareness jumps between your real body and the hallucinated one.

There is a well-documented case of a young man from Zurich who almost killed himself due to a doppelgänger hallucination. One morning, after discontinuing his seizure medications and drinking a lot of beer, he decided to stay in bed rather than go to work.

As he got out of bed later, he turned around dizzily only to see _himself_ still lying in bed. Enraged at his lazy "self," he shouted and shook the person lying in bed; that is, his doppelgänger. 

While this was going on, his consciousness shifted rapidly from one body to the other, leaving him unable to discern which of the two men was actually him. Horrified, he leaped from his window, but luckily survived.

So what causes these complex hallucinations? Again, we look to the brain to find the answer. 

There's a region of the brain called the _anterior insula_, which is responsible for integrating all kinds of sensory signals. It is this brain region that gives you the feeling that you are located inside of your body and that identifies where your body is located in space. 

People who experience the doppelgänger effect have a damaged left anterior insula and, as a result, lose the feeling that they are situated in their body. Their brain no longer knows where they are, resulting in hallucinations that place them elsewhere.

### 9. Epileptic seizures can be ecstatic experiences – and can teach us about our brain’s reality check mechanism. 

Up to this point, we've looked exclusively at brain dysfunctions with consequences that are at best weird and at worst horrific. But there's a condition with a telling name, _ecstatic epilepsy_, whose effects are anything but unpleasant.

During an ecstatic seizure, heavy electrical discharges are produced in certain regions of the brain. The subject often remains conscious, and people who experience such seizures report overwhelmingly positive emotions, like bliss and a sense of total security.

Russian novelist Fyodor Dostoevsky wrote eloquently about his ecstatic seizures, describing unimaginable happiness and a feeling of perfect harmony. Apart from this sense of bliss, people also report feeling a sudden sense of clarity and a deep understanding of reality.

Interestingly, the explanation for this phenomenon is related to the neurological explanation of autism discussed earlier: while people with autism suffer because their brains' predictions are often wrong, those with ecstatic epilepsy enjoy the feeling that their brains are right all the time. 

Neuroscientific research has found that ecstatic epilepsy also originates in the anterior insula, where awareness and subjective feelings are generated. Not only does the anterior insula integrate the signals it receives from the body, it also translates these signals into emotions.

Swiss neurologist Fabienne Picard hypothesizes that the anterior insula plays a key role in the brain's predictions of what we'll experience next. When our brain gets it wrong — in other words, when it makes a bad prediction — we experience anxiety and uncertainty. But if the brain's predictions are correct, we feel safe and upbeat. 

During ecstatic seizures, electrical storms in the anterior insula disrupt the mechanism that compares the brain's predictions with one's actual experiences. From the perspective of someone experiencing an ecstatic seizure, the brain's predictions seem to simply come true — as if everything in the world were exactly as it should be.

> _"The sensation is so strong and so pleasant that one would give ten years . . . for a few seconds." - Fyodor Dostoevsky_

### 10. Final summary 

The key message in this book:

**Our brain puts a lot of working into making sense of the world around us and making sure we understand how we relate to that world. But when our brain gets important mechanisms, predictions and models wrong, it can fundamentally alter the way we relate to the world — and ourselves.**

**Suggested** **further** **reading:** ** _Hallucinations_** **by Oliver Sacks**

This book explores the complex realm of hallucinations, and explains how they happen not only to people who are ill, but also to those who are completely healthy. Drawing on various studies, patient cases and the author's own experiences, it describes the different causes and types of hallucinations, and shows that they're actually a common phenomenon that manifest in a variety of ways.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Anil Ananthaswamy

Anil Ananthaswamy is a science writer and author of the highly acclaimed book _The Edge of Physics_. He is also a consultant for _New Scientist_ in London, where he once worked as deputy news editor.

