---
id: 5489d4213363370009130000
slug: the-man-without-a-face-en
published_date: 2014-12-15T00:00:00.000+00:00
author: Masha Gessen
title: The Man Without A Face
subtitle: The Unlikely Rise of Vladimir Putin
main_color: CA494F
text_color: A63C41
---

# The Man Without A Face

_The Unlikely Rise of Vladimir Putin_

**Masha Gessen**

A biography of Russian President Vladimir Putin, _The Man Without A Face_ shines a clear light on one of contemporary history's more shadowy political figures. The book charts Putin's almost accidental rise to Russia's highest office, starting from his benign beginnings in the state secret police. His vindictive personality, overwhelming greed and disdain for democratic norms continue to transform Russia today.

---
### 1. What’s in it for me? Find out how Russia’s Vladimir Putin consolidated his power in devious ways. 

Vladimir Putin wasn't ever supposed to lead Russia.

For much of his life, he was an anonymous paper pusher, a bureaucrat among thousands in Russia's enormous spy network. Even when he was tapped as president, most Russian politicians had little idea who he was.

But now Putin is more than widely known; he's widely feared, not only within Russia but also globally. While first presenting an incorruptible face, Putin has matured into a nefarious, greedy politician whose flagrant abuses of democracy and silencing of dissent have shocked the world.

After reading these blinks, you'll discover:

  * how Putin became president of Russia almost by accident;

  * how evidence shows Putin may have used terrorist attacks to further his own goals; and

  * how Putin's kleptomaniacal tendencies nabbed him a Super Bowl ring.

### 2. Young Vladimir Putin was a fighter, quick to anger, but dreamed of being a spy. 

The story of Vladimir Vladimirovich Putin begins in the desolate years following the end of World War II. Leningrad, today's Saint Petersburg, was a bombed-out shell, its people equally damaged. It was in this bleak environment that Putin's aggression and violent tendencies took form.

The Putin family suffered during the war: Putin's soldier father had damaged, disfigured legs and two children had died. Fortunately, they still had somewhere to live, an uncommon luxury. In 1952, Vladimir Putin was born.

The family lived in a small, 20-square-meter room, with a stove in the communal hallway and a makeshift toilet in the stairwell. Putin called this place home until he was 25 years old.

As a boy, Putin was easily angered. He channelled a lot of his aggression in his practice of the martial art Sambo, yet still was frequently involved in fights; as a result of this, he was excluded from the Communist youth organization, called the Young Pioneers.

Putin's young life wasn't only hardship, however. Despite their close living quarters, the family had a telephone, a television and even a summer cabin. How could they afford these luxuries?

When Putin's father had served as a soldier in in World War II, he had ties with the then state secret police, the NKVD. After the war, he remained in active reserve, so kept his factory job — and in exchange for giving information to the secret police, he received additional pay.

It was around this time that Putin too became interested in spy work. When he was 16 years old, Putin approached the KGB, the Soviet intelligence agency, about a job.

However, he was told that before he could join, he needed a college degree or army experience. So despite not being an exemplary student, Putin applied to Leningrad University and was accepted.

### 3. Putin was bored with his job as a KGB bureaucrat; yet he witnessed the fall of the Wall himself. 

After he graduated from Leningrad University in 1975, Putin's dream of working for the secret service became a reality. For most of his career with the KGB, however, he was a paper pusher, not a spy.

He spent four years in the 1980s working for the KGB in East Germany; what's more, he was posted to Dresden, not Berlin. His work consisted of collecting newspaper clippings and writing reports.

In Dresden, Putin and his wife were neighbors with other Russian KGB officers. Perhaps depressed at his situation, he had gained weight from drinking too much beer. His most significant professional accomplishment was purchasing a U.S. Army manual from a student for 800 German marks.

In the late 1980s, things began to change. Even though communist resistance movements began to take hold across Eastern Europe, Putin maintained his loyalty to the KGB and the Soviet Union.

In 1989, riots and protests in East Germany signaled the beginning of the collapse of the entire communist edifice. East Germans in Dresden saw a way to freedom, and as borders opened, they hopped on trains to Prague and Warsaw in order to reach West German embassies.

As the Berlin Wall fell, so did the giant secret organization created to control East German citizens, the Stasi. Protesters swarmed Stasi headquarters in major cities; when they approached the Dresden offices where Putin worked, he claims to have talked to the protestors himself.

Yet aware of the threat, Putin returned to his offices and burned sensitive documents while requesting help. While the army later arrived to defend the building, it was clear that Moscow would do no more. The state that Putin had been devoted to had left him defenseless in East Germany.

Angry and confused at the events following the fall and East Germany's interference with Soviet businesses, Putin and his wife soon returned to his parent's house in Leningrad.

### 4. With the Soviet Union crumbling, Putin played his cards close to see who would emerge victorious. 

When Putin returned to Russia, he found a country changing rapidly and profoundly. The pinnacle was when the KGB, in cooperation with government hardliners, attempted a coup against Mikhail Gorbachev.

Gorbachev had, as part of his reform program called "glasnost" (openness), freed imprisoned dissidents, rescinded hardline policies made under Stalin, and gave Russians access to previously banned books and Western radio.

In 1989, in a wave of "openness," many pro-democracy politicians were elected to parliament. Among these was law professor Anatoly Sobchak. After Dresden, Putin was still a member of the KGB; yet he became an advisor to Sobchak while working at his alma mater, Leningrad University.

To curb the overarching power of the KGB, Gorbachev had created the Committee for Constitutional Oversight. Yet this did little to keep the KGB from spying on pro-democratic politicians.

In August 1991, the KGB in cooperation with a group of politicians went forward with their coup, placing Gorbachev under house arrest near the Black Sea.

Putin considered his options, and in a bid to protect himself in case the coup failed, he tried to distance himself from the KGB, saying he had handed in his resignation before the coup — but that the KGB had "lost the letter."

In partnership with Sobchak, who wasn't as pro-democracy as he initially claimed, Putin stayed close to political circles and essentially played both sides of the coup.

While protesters fought to protect the actual pro-democracy politicians, Putin and Sobchak continued to position themselves. Putin allegedly sent in yet another resignation letter to the KGB, while Sobchak gave public speeches, encouraging pro-democracy protesters to continue the fight. (Yet Sobchak's speech apparently was approved by the coup junta beforehand!)

During the actual coup attempt, Putin and Sobchak hid in a bunker underneath a factory until it was safe to emerge; that is, until two days later, when the coup attempt failed.

### 5. In the chaotic 1990s, ordinary Russians suffered while Putin and Co. amassed small fortunes. 

After the failed coup, both Putin and Sobchak took advantage of Russia's political and financial chaos to line their own pockets.

In May 1991, Russia suffered a food shortage. Putin negotiated with a German company to import meat to Russia; yet in return, as Russia had no cash, he agreed to export some $92 million worth of natural resources.

Apparently the paperwork for the deal was so shoddy that in reality, some $1 billion worth of commodities were exported abroad. And while Putin's negotiated contracts were actually legally void, he still benefited from a kickback of some $34 million — a deal that could have got him fired, had Sobchak not been there to protect him.

In 1993, shortly after President Boris Yeltsin dissolved parliament, but before he established a new constitution, Sobchak convinced him to break up the Saint Petersburg (previously Leningrad) city council. Sobchak himself then led the city until elections one year later.

In the interim, Sobchak gave his friends and family cushy apartments while the city infrastructure buckled and some 75 percent of residents lived in poverty.

Sobchak meanwhile tried to bribe the press to polish his public image. Yet when he ran for the office properly in 1996 (with Putin as his campaign manager), he lost.

Sobchak was soon arrested on corruption charges, yet fell ill and was placed in a hospital. Putin helped him escape and go to Paris. Putin then moved to Moscow to become deputy of presidential property management.

Putin's fortunes turned for the better in Moscow. In 1998, he became head of the FSB, the successor of the KGB. A year later, he was able to have the corruption charges against Sobchak dropped.

Later, Sobchak eagerly spoke to the press and much of his story conflicted with Putin's own recollections of the era.

After meeting with Putin in 2000, Sobchak apparently was sent to Kaliningrad, where he died. The official cause of death was a heart attack; though people present at the time held suspicions of mild poisoning.

### 6. President Yeltsin saw in Putin a loyal, energetic and eager leader, and anointed him next in line. 

Once communism was over, the hopes the Russian populace put in President Yeltsin quickly soured.

Even though life improved somewhat, with people able to travel abroad, buy more products and enjoy a freer press, the economic situation was grim and the inequality gap had widened tremendously. By 1998, Russia had defaulted on its foreign debt.

At the time, Yeltsin had few political allies; crucially, he feared that his enemies in the parliamentary opposition, the party Fatherland-All Russia, once in power, would try to prosecute him.

To protect himself, Yeltsin sought a successor from his own circles. Yeltsin's small political clique, dubbed "The Family," decided that Putin was the ideal candidate, as he was believed to be compliant and safe.

Oligarch Boris Berezovsky was also instrumental in aiding Putin's rise. Berezovsky took advantage of hyperinflation to finance his interests in car manufacturing and servicing, and then used his amassed fortune to invest in the banking and oil industries, and well as purchasing a television network.

Berezovsky admired Putin's seemingly modest way of life and resistance to bribery. As a kingmaker of sorts, Berezovsky was able to convince Yeltsin to make Putin prime minister.

Yeltsin saw in Putin a loyal, energetic and eager potential leader; and thanks to Berezovsky, Putin then became the head of Unity, a new political party with no particular ideology. Berezovsky as well helped shape Putin's positive image, even funding a biography of the soon-to-be leader.

On the last day of the twentieth century, Yeltsin unexpectedly announced his resignation and appointed Putin, who had next to no experience as a politician, as acting president of Russia.

In January 2000, attendees of the World Economic Forum queried senior Russian politicians on who exactly Putin was. They could offer nothing in response.

Putin now held the reins of power. So how did he rework Russia's politics, constitution and people?

> _"The only thing smaller than the pool of candidates seems to have been the list of qualifications required of them."_

### 7. The threat of Chechen terrorism played into Putin’s hands, and from it he gained more powers. 

A series of terrorist bombings gave Putin the chance to reinforce his presidential credentials by responding with ruthless and deadly force.

In September 1999, hundreds of Russians died in a string of apartment bombings. The blame was thrown at native Chechens, who sought independence from Russia.

Yet one night in Ryazan, a bus driver observed people loading sacks into an apartment cellar. The apartment was evacuated, and the sacks were found to contain bomb equipment.

The head of the FSB, Russia's secret service, claimed the episode was part of a training exercise. However, the belief was that the FSB was responsible for the apartment bombings, with the goal of inciting fear in the populace and sanctioning a Chechen war.

Russians would need a strongman like Putin to lead them; interestingly, Putin had left the FSB just weeks before the incidents.

The Chechen crisis continued. In 2002, Chechen nationalists held hundreds of people hostage in a Moscow theater. After three days, Russian special forces released a gas into the ventilation system to end the siege. Some 129 hostages died, many because treating doctors weren't informed what sort of gas was used. Regardless, using gas is illegal under international and Russian law.

In 2004, Chechen rebels took over a school in Beslan, in southwest Russia. Federal troops charged the building three days into the siege, killing 334 people, including 186 children.

Later, the only surviving hostage taker revealed that federal troops had used flame throwers, grenade launchers and tanks to flush the attackers out. The barrage was so severe that hostage takers had tried to shield the _hostages_ from the "rescue" attempts.

After the siege, Putin declared that Russia needed to be stronger. Governors, the lower house of parliament and mayors were to be appointed, rather than elected. Votes applied to parties, not candidates; and all bills were to be reviewed by a special chamber of deputies, appointed by Putin.

### 8. Putin’s climate of fear silenced opposition, yet some whistleblowers still tried to take him on. 

Putin's policies created a climate of fear. Some entrepreneurs were able to take advantage of the profits amid privatization, yet those who disagreed with Putin were quickly cut down, losing their fortunes or their freedom. Some even lost their life.

Entrepreneur Vladimir Gusinsky created a media empire, establishing Russia's first independent television channel. Yet Gusinsky didn't support Putin's presidential campaign, and aired a program that implied the involvement of the secret police in the 1999 apartment bombings.

On Putin's second day as president, armed men invaded Gusinsky's media company, taking documents with them. An arrest was issued for Gusinsky on fraud charges, but he went into exile. Later, a leaked document revealed that Gusinsky had exchanged his media empire for his freedom.

People who worked with or played a part in Putin's rise to power soon witnessed his ruthless side, and some openly criticized him. 

Putin's old friend Berezovsky even lambasted the president's anti-democratic reforms. Berezovsky was then charged with fraud and, as a result, decided to remain overseas. His assets were either stripped from him or coercively sold at low prices.

Further investigation into the siege of the Moscow theater revealed new details. Alexander Litvinenko, a former Russian secret service officer based in London, had learned that one hostage taker, Khanpasha Terkibayev, had vacated the theatre safely before the deadly end of the siege.

Litvinenko gave his information to pro-democracy politician Sergei Yushenkov and journalist Anna Politkovskaya. Yushenkov, only two weeks afterward, was shot dead.

Politkovskaya however interviewed Terkibayev, and although he was renowned for boasting and lacking discretion, said he worked for Moscow and the FSB, of which Putin was the former head.

Yet these tentative revelations went no further. Terkibayev died in a car crash in 2003. Politkovskaya was shot dead, and Litvinenko died from a curious case of radioactive poisoning in London in 2006.

### 9. Friends of Putin go far, controlling much of Russia’s wealth. Enemies find themselves in jail. 

When Putin first entered politics in the 1990s, he was viewed as incorruptible. This first impression soon changed drastically.

Putin has an estimated $40 billion in assets, including a palace on the Black Sea worth $1 billion, all of it accumulated through unabashed thievery and shady business deals. But how did he do it?

Some of his wealth can be traced to the cuts he took from donations made by oligarchs to fund medical equipment for hospitals and medical facilities.

Yet on a smaller scale, Putin bizarrely has been witnessed stealing in public. For example, when he met Robert Kraft, the owner of the New England Patriots, Putin asked to try on his Super Bowl diamond ring yet never returned it!

Those wanting to expose Putin should be wary, however, unless they are willing to lose everything in the process. The story of Mikhail Khodorkovsky is a potent example.

Khodorkovsky became an oligarch through government loans and his subsequent control of Yukos, a large Russian oil company. Also a philanthropist, he later created the Open Russia Foundation, which helped fund nongovernmental organizations and other social projects.

Khodorkovsky even hired international auditing firms such as McKinsey & Company and PricewaterhouseCoopers to evaluate his companies, and he would speak publicly, as well as to Putin directly, about how corruption was destroying Russia.

When Khodorkovsky questioned the president of state oil company Rosneft about the lawfulness of a merger, however, Putin cut in, berating Khodorkovsky for his opinions.

Shortly afterwards, Khodorkovsky was arrested on charges of tax evasion and fraud, and after a show trial, was sent to a penal colony. Amnesty International considered him a prisoner of conscience — hardly commonplace for a billionaire oligarch.

Khodorkovsky's oil company was then auctioned — unsurprisingly, to a company run by an ally of Putin.

> Fact: When alcohol was Russia's main export, Putin consolidated 70 percent of Russia's alcohol manufacturers into one company and got his friend to run it.

### 10. Activists or anyone opposed to Putin’s rule were quickly quieted or moved out of the way. 

It could be that only in the 1990s was Russia a quasi-democratic country. As he came to power, Putin crushed this democratic progress and placed restraints on a free media.

In March 2000, Putin was elected president with 53 percent of the vote and _without_ a campaign.

Within months he'd altered the upper house of parliament, having members appointed rather than directly elected, and enabling them to be removed on any grounds.

And within a year, all federal television channels were under state control.

In the 2004 presidential election, Putin won 71 percent of the vote. At the end of his second and final term in 2008, he selected Dmitry Medvedev as president, and Putin himself became prime minister.

The prime minister's role traditionally isn't a powerful one, yet Putin as prime minister exerted more power behind the scenes than Medvedev did as president. Presidential term limits were also extended to six years.

In 2010, Transparency International reported that Russia was 86 percent more corrupt than other countries.

Putin and his government actively suppressed and threatened reformers, activists and anyone who challenged his political rule.

In 2005, chess master Garry Kasparov tried to campaign against Putin. But suddenly, venues he planned to speak at closed for nonsensical reasons, and the state-controlled media stopped reporting on his talks.

In 2011, Putin brought two dummy political parties together to represent a more left-wing view and a more right-wing view, to contrast with his own political party.

He opted for Mikhail Prokhorov, one of the richest men in Russia, to lead the right. But when Prokhorov refused to participate, he was then excluded from his own party. Putin's party went on to win 49 percent of the vote in the 2011 parliamentary elections, though independent observers witnessed major infringements in 134 of the 170 Moscow precincts.

In 2012, the Russian parliament prohibited foreign funding for nongovernmental organizations and made espionage and treason laws applicable to any citizen. The same year, the band Pussy Riot were imprisoned for two years for playing an anti-Putin song in a cathedral.

### 11. The few people who have seen beyond Putin’s mask find the politician narcissistic and heartless. 

You can't have missed the press photos of Putin conquering nature, riding bare-chested on horseback. It is crucial for him to appear strong and powerful at all times.

Other shots have Putin subduing wild animals like Siberian tigers or polar bears, but environmental bloggers or locals claim that these animals were taken from a zoo or captured days before.

Putin also received media coverage for diving into the Black Sea and "discovering" ancient Greek jars. Yet these too were a plant, another public relations stunt for the powerful leader.

Putin once announced to a friend that he was an "expert in human relations." However, it is clear that Putin is a poor communicator, and lacks empathy.

In 2000, months after his presidential inauguration, rusted torpedoes on a Russian nuclear submarine doing maneuvers in the Barent Sea exploded. The crew, with some 118 men aboard, were stranded.

Norway and Britain offered assistance, yet Russia refused on security concerns. Five days after the accident, Russia changed its mind and asked for assistance; a full week after the disaster, a Norwegian crew was able to rescue the submarine.

There were no survivors.

As the news of the disaster reached Putin, he opted to stay in his Black Sea holiday home for a few more days. Much later, he visited the town where the submarine had been stationed.

Yet he seemed to be more concerned about the loss of equipment than the lives of the officers, and in front of an audience, Putin lost his temper, attempting to camouflage his clear public failure.

Later, Putin was interviewed on the U.S. talk show, Larry King Live. When asked about what happened to the submarine, Putin simply said, "It sank."

### 12. Final summary 

The key message in this book:

**At best, Vladimir Putin has been a lifelong ultranationalist, doing everything he can to retain Russia's superpower status in the world through his influence on people in power. At worst, Putin has wiped out adversaries and been party to the unnecessary loss of hundreds of innocent lives. Stories about him paint a picture of a leader who is vengeful, greedy and self-absorbed.**

Actionable advice:

**Read multiple independent sources for news on current affairs, politics or economics.**

Putin's nationalization of the media within Russia highlights that the stories we're fed are rarely unbiased. News from major outlets often promote a particular worldview, funded by corporations or governments. Instead of taking everything you hear or read at face value, inform yourself with multiple news sources and make up your own mind.

**Suggested further reading:** ** _The Prince_** **by Niccolò Machiavelli**

_The Prince_ is a 16th century guide on how to be an autocratic leader of a country. It explains why ends like glory and power always justify even brutal means for princes. Thanks to this book, the word "Machiavellian" came to mean using deceit and cunning to one's advantage.
---

### Masha Gessen

Masha Gessen is a Russian-American journalist and activist.

