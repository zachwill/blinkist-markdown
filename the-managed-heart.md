---
id: 588f5e7d1a8b8f00045e131c
slug: the-managed-heart-en
published_date: 2017-02-02T00:00:00.000+00:00
author: Arlie Russell Hochschild
title: The Managed Heart
subtitle: Commercialization of Human Feeling
main_color: BA3A45
text_color: BA3A45
---

# The Managed Heart

_Commercialization of Human Feeling_

**Arlie Russell Hochschild**

_The Managed Heart_ (1983) is the seminal sociological text that introduced the concept of emotional labor. These blinks reveal how we adjust our emotions to our advantage in social and professional contexts, and shed light on the risks and consequences of this form of self-management.

---
### 1. What’s in it for me? Develop a new appreciation for emotional labor. 

Have you ever marveled at how waiters and waitresses maintain their upbeat attitude and positive smile for a seemingly endless stream of customers? Often seen as an unskilled work, waiters and anyone else working in customer service are in fact using an essential skill we don't often think about: managing feelings.

But this emotional labor implies more than simply keeping one's cool, not getting upset or holding back an untimely snicker in everyday situations; it is a matter of actively invoking feelings, understanding your own feelings and those of the people around you, as well as the societal rules that affect them. Let's take a closer look at how this works and why it has so often been overlooked.

In these blinks, you'll find out

  * how feelings signal what is hidden within us;

  * why women do more emotional labor than men; and

  * how feelings can be a bit like money.

### 2. Emotional labor plays a central role in our personal and professional lives, but we don’t usually talk about it openly. 

From baristas to flight attendants to supermarket cashiers, we expect service with a smile from workers across many industries. But as anyone who's worked in a customer-facing job knows, faking a friendly attitude all day long is difficult, to say the least. And yet, it's one of those job requirements that's so ubiquitous, employers rarely think to name it.

Sociologists studying the world of work, on the other hand, call this _emotional labor._ Emotional labor is when we consciously manage our feelings to ensure they're appropriate for a particular commercial or social setting.

The work done by flight attendants is a powerful example of emotional labor. During their training, flight attendants are taught to smile "genuinely," emphasizing that this outward display of a good mood can't appear forced. Attendants must be warm and cheerful when serving food and drinks to passengers.

A charming "How are you doing today?" is part of the service, too. Small talk and smiles might seem trivial, but when they aren't there, we notice. Without the emotional work that flight attendants put in, many passengers would consider their service inadequate.

Another profession where emotional labor plays a central role is, of course, acting. On stage, an actor demonstrates his talent by creating the illusion of experiencing emotions that aren't his own, that he has perhaps never even felt.

However, there's a crucial difference between the emotional labor of actors and flight attendants: while theater involves emotional labor in the pursuit of art, the flight industry's emotional labor policies are engineered by corporations that, above all, want to make a profit.

### 3. Our emotions reveal our underlying attitudes toward others, which might clash with the rules we’ve been taught about feelings. 

When you feel sad, excited or bored, your emotions don't just reflect your current mood — they're also powerful signals that reveal what the people and things in our lives mean to us. Let's illustrate this with an example.

Say, for instance, a university student agrees to host a party with an old friend from high school. Soon, though, they find themselves dreading the thought of it. Why? Because, deep down, they just don't want to be associated with their old friend anymore.

So, the student decides to cancel the party the day before. She knows it's a selfish move but, for some reason, doesn't feel that guilty about it. She mostly just feels relief, which is a signal that the friendship wasn't so important in her life anyway.

This disconnect between how we feel and how we know we _should_ feel shows that there are rules for which emotions we should have, and when. When we feel guilty about our emotions, it's because our feelings go against what we've been taught to feel. This also happens when we find ourselves feeling too much or too little.

Imagine you're seeing off your boyfriend at the airport before he flies to India for a month. He's tearfully saying goodbye to you, while you're trying to force yourself to show an appropriate amount of sadness to match his. This might be difficult if you actually feel excited at the prospect of your newfound alone time!

In these situations, there's a good reason why we force ourselves to create the illusion of feeling emotions that we don't. We know that if we don't display the appropriate emotion, we could be scolded or shunned by the other person. Comments like "You should be grateful, considering all I've done for you," or "Aren't you excited about this party?" reflect these kinds of sentiments.

Emotional labor and social rules regarding feelings play central roles in our lives. But feelings are also subject to another factor — _interpersonal exchange._

### 4. Emotional displays are currency whose value fluctuates depending on your position in a hierarchy. 

Have you ever done a favor for a friend and felt like they should have shown a little more gratitude? In our society, feelings are often exchanged as gifts or forms of payments. Let's explore this with another example.

Say a new intern needs help with a task and seeks advice from a more experienced colleague. Afterward, the intern might "pay" for the expert's time and wisdom with a comment like "Thanks so much Sarah! I really appreciate it; I know how busy you are right now." By doing this, he acknowledges that he isn't really "worth" the expert's time, and compensates for this by giving them an ego boost. In the end, both parties profit from an exchange like this.

But exchange rates fluctuate. If the expert constantly spends time helping out the intern, her own work may suffer. She'll need more time to catch up, and the help and advice she shares will become more and more expensive — a flattering comment just won't cut it anymore.

The intern, as a result, feels pressure to make his _feeling-gifts_ more extreme, whether this means taking the blame for a mistake made by the expert, or feeling compelled to keep a friendly demeanor even when the expert abuses him verbally.

Ultimately, those in power receive more emotional rewards. For those with little power, on the other hand, emotional labor is an unavoidable aspect of their lives. Strangely enough, those on the receiving end of such flattering behavior often believe that it's simply a part of their employees' personality. But this isn't true — it's all just part of the exchange.

### 5. Women are forced to do more emotional labor than men because of unequal power dynamics. 

Think back to your childhood. Which of your parents took more responsibility for things like organizing your playdates, purchasing gifts and remembering friends' and family members' birthdays? In most cases, the answer is our mother. How come?

In terms of power, status and independent access to money, women are generally less well off. For example, statistics from 1980 show that around 50 percent of American men received an annual salary over $15,000 — as opposed to only 6 percent of women. While the situation has of course improved since then, inequality still exists between men and women.

Since women find it more difficult to obtain independent financial status, their ability to manage feelings becomes a financial asset. For instance, women often find themselves straining to remain pleasant and friendly while handling their repressed aggression.

To demonstrate this, researchers at UCLA conducted a study that surveyed whether people deliberately used emotions to get what that they wanted. They found that 45 percent of women did this, as opposed to just 20 percent of men. One woman put it this way: "I pout, frown and say something to make the other person feel bad, such as 'You don't love me, you don't care what happens to me.' I'm not the type to come right out with what I want; I'll usually hint around."

In day-to-day life, women find themselves doing far more interpersonal labor than men. This labor includes things like listening patiently and enthusiastically, giving advice and generally nurturing others.

Many see these activities as just another part of being a woman. But the reality is that women need to do these things in order to make up for their inherent disadvantage when it comes to power and status. This is shown in workplaces, where emotionally laborious jobs cover half of the jobs done by women, but only a quarter of those done by men.

> "What are little girls made of? Sugar and spice and everything nice. What are little boys made of? Snips and snails and puppy dog tails."

### 6. Women’s opinions are dismissed more often, while their jobs are also more emotionally taxing. 

So far, we've looked at the way in which women do more emotional labor than men. But what are the effects of such an imbalance on the women doing this additional labor?

First of all, women find that their opinions are more quickly ignored or rejected than men's. One reason for this is that the feelings that women carefully manage are taken to be involuntary or uncontrollable — think of the way good actors don't seem like they're acting at all.

Generally, in situations when people fail to manage their emotions, women are taken less seriously than men. For instance, when a woman loses her temper, others are likely to think that she is being illogical or unreasonable. On the other hand, when a man loses his temper, it is often seen as an expression of a strong, rational belief.

We can see this clearly in the credence that doctors give their patients depending on their gender. One study found that doctors are more likely to take men's claims seriously than women's. When patients of both genders complained of back or chest pain, headaches and fatigue, more medical service was given to men than women. This shows how women are generally less likely to be taken seriously.

Women also face the problem of having to balance emotional labor with wielding authority. In many jobs, women are expected to do emotional labor to make customers feel good — but they're also expected to uphold certain rules and guidelines. These two expectations can conflict with each other.

For instance, airplane passengers are required to follow a number of strict rules; it is the flight attendant's job to uphold these rules, as well as to interact on an emotional level with passengers. But women have a harder time enforcing these rules, because passengers take them less seriously as a result of the emotional labor they perform.

### 7. Final summary 

The key message in this book:

**Although emotional labor is a central aspect of many of our jobs, it often goes unacknowledged. This is a problem for women in particular, who, due to traditional gender roles and power dynamics, are expected to perform more emotional labor. By learning about the various forms of emotional labor that exist, we can get better at recognizing the role it plays in our everyday lives.**

**Actionable Advice: Ask yourself who you're complaining to.**

The next time you're frustrated with a delayed bus or a lousy product, take a step back and think about who you're complaining to. Ever notice that it's usually the women in our lives who are on the receiving end of our frustrated rants? Consider the emotional labor you're putting them through and remember that they're expected to deal with far more of it than men.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Emotional Intelligence 2.0_** **by Travis Bradberry and Jean Greaves**

_Emotional Intelligence 2.0_ (2009) gives you expert insight into which skills you need to read others and build better relationships. It breaks down the four aspects of emotional intelligence, or EQ, and gives advice on what you can do to improve your own skills.
---

### Arlie Russell Hochschild

Arlie Russell Hochschild is a Professor Emerita of Sociology at the University of California, Berkeley. A renowned author, Hochschild has written three _New York Times Book Review_ Notable Books of the Year: _The Second Shift, The Managed Heart_, and _The Time Bind_. Her latest book is _Strangers In Their Own Land: Anger and Mourning on the American Right_.

