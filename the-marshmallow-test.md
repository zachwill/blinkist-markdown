---
id: 5460db6731363000086f0000
slug: the-marshmallow-test-en
published_date: 2014-11-11T00:00:00.000+00:00
author: Walter Mischel
title: The Marshmallow Test
subtitle: Mastering Self-Control
main_color: 85BCBB
text_color: 486665
---

# The Marshmallow Test

_Mastering Self-Control_

**Walter Mischel**

_The Marshmallow Test_ explains why being able to delay gratification and exercise our self-control is essential for living a successful life. Using insights gained from several psychological studies, it explains how exactly our self-control skills function, and what we can do to improve them.

---
### 1. What’s in it for me? Learn all about self-control. 

In the 1960s, the author of this book, Walter Mischel, performed a famous psychological experiment on children. They were shown a treat (often a marshmallow), then told they could either eat it now, or wait and have two later.

This seemingly simple experiment as well as the follow-up studies on the child participants immensely deepened our understanding of human self-control. In _The Marshmallow Test,_ Mischel revisits the topic, and gives detailed information on how exactly our brains regulate our actions when we're up against a temptation. He also explains how self-control skills develop when we're a child, and why they're so important to us throughout our entire lives. In these blinks, you'll learn about how your self-control works, and what you can do to improve it.

You'll also learn

  * why certain kids could resist the marshmallow and others couldn't;

  * what the Cookie Monster taught kids about resisting immediate gratification; and

  * how parents' interactions with their toddler influence the toddlers' self-control as adults.

### 2. The “marshmallow test” was developed to understand how children control their urges. 

Have you ever teased a young child by offering her a treat, then saying she can't eat it right away?

If not, that's alright, because scientists have done it for you in a clinical test! It was called the _Marshmallow Test_, and it provided us with some interesting insights on human behavior:

For the test, children were first allowed to choose their favorite treat. A researcher would tell the child they could either have the treat now, or have two later after the researcher had left and come back. Then the researcher left the room, but told the child they could call them back if they couldn't wait any longer. The child was then left alone with the treat on a tray in front of them.

Researchers secretly watched the children to see how they would handle the situation. Naturally, some children ate the treat right away. Others waited a bit, then ate it. Some children, however, managed to resist the treat, and they all used the same strategy to do so. The children who successfully resisted eating their treats managed it by distracting themselves from it.

They used different strategies for this. Some sang songs, and others tilted and played with their chairs.

The kids came up with those strategies themselves, but then the experimenters tried teaching the children distraction techniques beforehand. This turned out to help the children a great deal.

Before the test, researchers taught the children _if-then_ plans. For example, _if_ my hand moves towards the treat, _then_ I will start singing a song. When the children reminded themselves of what they were doing each time they went for the treat, they found it easier to wait.

The Marshmallow Test seems simple, but it actually has profound implications, as you'll see in the following blinks.

### 3. The part of our brain that regulates our self-control develops as we grow up. 

How do we decide whether or not to give into temptation? Should we eat that chocolate bar, or resist it?

Our self-control depends on two systems that are functioning in our body: one that instantly reacts to our environment, and one that controls our behavior.

Our emotions and basic biological needs are regulated by our _limbic system_, also called the _hot system_. The hot system immediately reacts to any arousing stimuli. So when a child sees a marshmallow, his hot system makes him want to eat it right away instead of waiting patiently for more.

On the other hand, we also have a _cool system_. It's located in our prefrontal cortex, and it's the part of our brain that's responsible for self-control. The cool system is essential for making decisions or planning anything ahead. Whenever we need to control ourselves, our cool system is active.

When children successfully distract themselves from the marshmallow, the strategies they use activate their cool system. The hot and cool systems work together, so that when one becomes active, the other becomes less active.

Although our hot system is functional straight from birth, the cool system develops throughout childhood. This is why it's much more difficult for young children to resist any immediate gratification: their actions are regulated mostly by their hot system.

Children under four usually aren't able to use their cool system at all. As we get older, we gain more self-control, but our cool system is still developing as we go through adolescence. Most people don't have fully functional cool systems until they're adults, which contributes to why many teens can't resist the temptation of doing drugs or drinking alcohol.

> _"The power resides in the prefrontal cortex, which, if activated, allows almost endless ways of cooling hot, tempting stimuli by changing how they are appraised."_

### 4. Our environment shapes our self-control abilities. 

We've seen that self-control is regulated by two biological systems in our body. You might think that implies our genes determine our self-control, but this is not true.

Like any other skill, self-control is _partly_ genetic, but it can also change quite a bit depending on our environment — and on us.

Everyone has the ability to improve their self-control, because our life experiences are constantly remodeling who we are. One group of researchers illustrated this with an experiment on rats.

The researchers bred groups of rats to be either very dull or very smart. The rats were then put into a running maze, which was either full of stimuli or boring.

Interestingly, the duller rats became smarter when they were put into the exciting maze. Conversely, the smarter rats became stupider when they were put into the boring maze. Even though their genetics determined their intelligence, their environments influenced their ability to use that intelligence.

The way our parents raise us is also important. Our prefrontal cortex is developing the most rapidly during our first few years of life, so our environment at that time has a huge impact on our self-control.

Parents can teach children self-control by distracting them from unpleasant situations. Imagine a parent who calms her crying child by handing him a fun toy. The child internalizes the idea that the toy is a good distraction, and they can use that method later to distract themselves.

On the other hand, if a parent doesn't interact much with a child, the child won't be introduced to distraction techniques like this one. Such children usually have difficulty coming up with their own self-control strategies as they get older.

So, the control we have over our desires is shaped more by our environment than our genes. In the next blinks, we'll see how we can deliberately call on our self-control, and even improve it.

### 5. How much self-control we have depends on the context, but it’s ultimately unlimited. 

When the public heard about President Bill Clinton's affair with Monica Lewinsky, many wondered if he could still be trusted as president. They figured if he lacked self-control and honesty in his personal life, he must lack it in his political life as well.

In fact, our self-control depends on context. If someone can't control herself sexually, it doesn't necessarily mean she can't control herself at work. We choose the situations in which we exercise our self-control.

When deciding whether or not to give into temptation, we usually consider the consequences, our goals and how strong the temptation itself is.

So when someone can't control food choices, for example, he's decided that the consequences are less important. Gaining weight isn't so bad if the desire to eat a burger is extremely strong. That same person might be able to control himself perfectly well in another environment, like at work.

If you think of self-control as limitless, you'll be able to control yourself much better than others. One team of researchers conducted an experiment that illustrates this.

In the study, participants were given a challenge of controlling their facial expressions. So, for example, they were exposed to something unpleasant but had to try to hide their disgust.

One group of participants was told that this exercise was energizing. The other simply thought of it as a self-control exercise, so it seemed more exhausting. After the exercise, both groups were given a second task: squeezing a handgrip as hard as they could.

Those who had believed that the self-control challenge was energizing did much better on the handgrip task. Just viewing their self-control as something positive gave them more power over it.

So, if you view your self-control as unlimited, you can increase your productivity because you won't get exhausted and bored so easily!

> _". . . Like all skills, self-control skill is exercised only when we are motivated to use it."_

### 6. The result of a child’s performance in the Marshmallow Test correlates with their self-control as an adult. 

You might think that the Marshmallow Test was a simple psychological experiment just for children. Actually, the results of the children who participated were shown to correlate strongly with their life as adults.

In follow-up studies, when the children who'd participated in the Marshmallow Test were older, researchers did another assessment. The children who managed to wait longer for their treats were assessed as being more successful later in life.

They found that those who'd been able to wait for the bigger reward, or even those who waited longer before giving into temptation, were better at concentrating as adults. They found it easier to pay attention to something, even in distracting environments, and they were better at planning ahead.

In addition to the Marshmallow Test's correlation to concentration, the researchers also found that those who'd waited for their treats as children scored higher on their SATs (Scholastic Assessment Tests) and in school. They were also better at maintaining personal relationships.

Some of these differences were observable in the brain. In another follow-up study, the participants' brains were examined with an fMRI scanner. The researchers found that those who excelled in the Marshmallow Test and in school had much more activation in their prefrontal cortex (which houses the cool system, as we discussed in an earlier blink).

The adults who'd waited _less_ when they were in pre-school had more activation in their _ventral striatum_, which is linked with pleasure and addiction. They didn't have as much self-control, and were more susceptible to addictive substances.

The Marshmallow Test thus proved itself to be a useful indicator of a child's future development and behavior. Even though the children were very young when they participated in the test, they already had formed self-control habits that stayed with them as they grew older.

### 7. Children should be taught that they’re capable of making their own decisions, and that those decisions have consequences. 

So, we've seen that teaching children self-control is crucial during their upbringing. Let's look at how exactly this can be done.

Children need to understand that the choices they make have consequences. When we're young, we typically get any self-control strategies we use from our parents. Thus, it's important that parents support their children and be there for them, but also give them a sense of autonomy.

It's essential that parents make their children understand that their actions have consequences. If they make good choices, they'll get good results back and vice versa.

This is similar to the _if-then_ plan that the researchers taught the children before the Marshmallow Test. For example, parents can allow their kids to take piano lessons, and show them that _if_ they practice regularly, _then_ they'll be able to play songs by themselves.

Parents also need to praise their children when they work hard. They'll feel rewarded and be inspired to keep working hard in the future. Children also need to understand that mistakes happen sometimes, and they shouldn't get overly stressed out or panic when they do.

When do we need our self-control the most? In stressful situations, usually. But kids often give up easily if they feel they might fail at something. In these situations, their parents need to encourage them.

Parents need to make their children understand that they should always try to do their best. Children should know that they can do whatever they set their minds to, because their abilities aren't limited to their genes.

When children are praised for working hard, they'll be much more likely to keep striving for improvement. If they're practicing a difficult piano piece, for instance, they'll be motivated to keep going because they'll know they can still succeed even if they make a few mistakes.

### 8. You can improve your self-control skills by concentrating on the future, and putting distance between you and your temptation. 

Exercising self-control can be very difficult at times, but the good news is that this is something we can practice.

So, how does one do this? Well, try to focus on the long-term consequences of your behavior, instead of instant gratification.

The real secret to sharpening your self-control is to put _psychological distance_ between you and your behavior.

There are a couple ways to do this. The distance can be in time, meaning you should focus on the future when you're faced with temptation. You can also put physical distance between you and whatever's tempting you. The goal is to think about the issue in a more abstract and detached way, so you can activate your cool system.

Imagine that you want to quit smoking, for example. When you feel like having a cigarette, you should visualize yourself in the future, and think about how you'd feel if your doctor told you that you have lung cancer. Suddenly, the cigarette will seem less appealing.

Also, the _if-then_ plans the children used in the Marshmallow Test are just as useful for adults. So, use the same method to make rules for yourself! For instance, you could set a rule that _if_ your alarm goes off in the morning, _then_ you'll go for a run. Or _if_ you feel like eating chocolate, _then_ you'll eat some celery instead. Eventually, you won't need those rules for yourself anymore, as the behavior will become automatic.

Another important strategy is to identify _when_ exactly you need to exercise more self-control. Analyze yourself. You might realize that you get angry and lose self-control when your husband forgets to take out the trash. If so, you could come up with an _if-then_ plan for that situation: _if_ he forgets to take it out, _then_ I will count down from ten and calmly remind him to do it.

### 9. In addition to parents, teachers and even TV programs can teach children the importance of self-control. 

We've seen that parents play a vital role in the development of a child's self-control skills. But what about kids whose parents fail to do this? What about the other influences in their life?

Aside from parents, teachers have a huge impact on this kind of development.

Children who grow up in less fortunate environments usually have more difficulty controlling themselves. They're also more likely to get addicted to drugs or alcohol because their parents might not support them in making the right choices.

To address this problem, some cities in the United States have Knowledge is Power Programs (KIPPs) in schools. KIPPs aim to teach less fortunate children important strategies for life, such as self-control. The goal is to enable the children to make good choices on their own.

Such programs have proven successful. In fact, children who participate in KIPPs have a college graduation rate of about 40 percent. Their peers from similar backgrounds who don't do KIPPs have a rate of 8 to 10 percent.

After the results of the Marshmallow Test were published, some educators began using new strategies to try to help kids develop self-control.

And when it comes to appealing to children, what could be better than a TV program? You may know the _Sesame Street_ character the Cookie Monster, who has no self-control. In one episode, the Cookie Monster was subjected to the "waiting game," which was essentially the Marshmallow Test. The children watched the Cookie Monster resist the temptation of eating the cookie, and then he received two as a reward.

TV programs like this can be quite useful for children, especially those who might not be able to learn similar lessons from their parents.

> _"Choice is about children having genuine options in how they make their lives, regardless of their demographics."_

### 10. Final summary 

The key message in this book:

**Our self-control skills start developing when we're very young, and they continue to be determined by our environment as we get older. This continuous development process means our ability to control ourselves is fluid. Anyone can improve his or her skills by focusing on the future and strategically using distractions.**

Actionable advice:

**Put distance between yourself and your temptation.**

It could be a distance in time: craving a cigarette? Close your eyes and imagine yourself in twenty years telling your son you have lung cancer. It could be physical: craving potato chips? Walk out of the kitchen and reassess the situation in the bedroom. Try to look at the situation critically like an outsider, so that you activate your cool system, and improve your self-control.

**Suggested** **further** **reading:** ** _Willpower_** **by Roy F. Baumeister and John Tierney**

_Willpower_ brings the concept of self-control back into mainstream discussions on achievement. It takes contemporary scientific studies and breaks them down, demonstrating that, despite the previously held views of many experts, willpower can indeed be harnessed and strengthened to make the changes in your life you've always wanted to make.
---

### Walter Mischel

Walter Mischel is a renowned psychologist and writer. He invented the "marshmallow test" when studying child psychology in the 1960s, and he was educated at New York University, City College of New York and Ohio State University.

