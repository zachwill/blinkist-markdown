---
id: 5489d6b233633700092a0000
slug: the-master-switch-en
published_date: 2014-12-16T00:00:00.000+00:00
author: Tim Wu
title: The Master Switch
subtitle: The Rise and Fall of Information Empires
main_color: 67C4DC
text_color: 3C7280
---

# The Master Switch

_The Rise and Fall of Information Empires_

**Tim Wu**

In _The Master Switch_, author Tim Wu traces the development of information technology such as radio, film and television and illustrates how great innovations always come to be controlled by big corporations. Critically, Wu asks whether the internet will succumb to the same fate, or if its inherent design could help it avoid corporate domination.

---
### 1. What’s in it for me? Learn how the internet may be in danger of becoming “closed.” 

In many ways, the free flow of information defines our modern times. So who controls the _master switch_ on the flow of information?

More specifically, who controls the internet? As a global resource, it is open to whomever wants to use it, contains information from everywhere, and importantly, is the exclusive property of no one.

At least, for today. Yet like information technology before it, the internet may be in trouble. 

These blinks show you how throughout history, innovative, open technologies gradually became the money-making tools of private corporations. Yet the structure of the internet — and perhaps the lessons of the past — may preserve it as the open platform it was always meant to be.

In the following blinks, you'll also learn:

  * how a humble hobbyist's garage is often the site of the biggest tech breakthroughs;

  * why AT&T kept the invention of the answering machine secret in the 1930s; and

  * why believing that the internet can fight off monopolies may be wishful thinking.

### 2. Phone, radio and film all had a period of “openness” in the early days of the technology. 

Throughout history, the lifecycle of information technology has followed a typical progression. A technology starts out being freely accessible, but gradually becomes controlled by a single corporation or cartel.

From open, then, to closed. This progression is so typical that it's been given a name: _the cycle_.

The development of the telephone exemplifies the cycle. The cycle usually begins in a laboratory, attic or garage where a hobbyist or engineer tries to solve a concrete technical problem.

For the telephone, it began when Alexander Bell rushed to register a patent after tinkering with metal rods tuned to different frequencies. He wanted to convert electrical currents into sound.

By the time Bell's patent expired in 1894, hundreds of independent telephone services had already appeared, which allowed for the "open" phase of the telephone. Everyone could tinker with the new technology.

Radio also experienced an early open phase. Like the telephone, it was pioneered by amateurs and accessible to hobbyists early on. In the 1920s, any group could launch its own local broadcast station.

This openness resulted in a wide variety of broadcasting, with content limited only by the creativity of broadcasters. Some stations played jazz, for instance, while others focused on political issues.

There was also a period of openness in film. In the early twentieth century, American film was controlled by the Edison company, a film cartel that held all the important patents on motion picture technology. By 1909, however, American film theaters started declaring themselves "independent," and eventually broke up the Edison monopoly.

By 1915, the film industry too opened up, which ushered in an era of creativity. Specialty films that spoke to particular groups or interests, about all sorts of subjects, proliferated.

### 3. Telephone, radio and film eventually succumbed to the control of closed, commercialized industries. 

So what happens after a period of openness? Industries get monopolized, and the media becomes closed.

For the telephone, this happened when the American Telephone and Telegraph Company, or _AT &T_, established its monopoly in the 1910s.

AT&T was actually founded by Alexander Bell. The company held exclusive rights to the lines that made long-distance calls possible, but by the early 1900s, it was losing control of the market. During the technology's open phase, local telephone companies grew big enough to threaten its dominance.

AT&T responded by starting a price war, dramatically lowering prices as to force competitors to join them or go out of business. This is how AT&T established its monopoly.

The radio industry's open period ended shortly after the telephone's, becoming completely commercialized by the 1930s.

Advertising on radio was uncommon until the early 1920s. However, broadcasting stations soon found that advertisements offered a solid financial base — and more listeners meant more advertisements, and thus more profits.

So radio stations sought the largest audiences possible. By the 1930s, the American radio industry was at its heart a commercial enterprise, dominated by a few, powerful broadcasting networks such as _CBS_ and _NBC_.

Film as well came under the effective control of just one corporation. After the Edison film cartel was broken up, the industry became monopolized again ten years later by Paramount.

If theaters wanted to show Paramount's hit films starring major actors, they had to buy a full year's worth of films. They also had to buy the films without previewing them first!

So Paramount came to dominate the industry by first controlling the movie stars, directors and writers, then eventually the studios and theaters, too.

> _"The closing [of the cycle] is driven by a hunger for quality and scale — the desire to improve, even the perfect medium and realize its full potential, which is limited by openness, for all its virtues."_

### 4. Industry monopolists worked to quash innovations that might challenge their dominance. 

There are two main ways technological innovation is sparked: by enthusiastic hobbyists at home or by professional engineers in the laboratories of big companies.

Yet there's a problem with research done in corporate labs: it's limited to developing innovations that benefit a company's bottom line. This has led some companies to suppress valuable breakthroughs made by company engineers, if the innovation is thought to potentially hurt the business.

This clash of technological progress and business happened in 1934 at AT&T, when company engineer Clarence Hickman invented the answering machine.

The company feared that people wouldn't want to have "live" telephone conversations as frequently if the answering machine was available to leave a message. So AT&T suppressed the invention, and it wasn't "discovered" again until the 1990s.

In the same year, an electrical engineer named Edwin Armstrong invented FM radio. It could broadcast using far less power than AM radio, which was dominated by a few broadcasting corporations that could afford the fees for these high-wattage stations.

Since FM radio could open the doors to more broadcasters in the market, the radio monopolies fought against it. Their campaigns criticized FM radio as an unproven, experimental technology that wasn't truly useful.

In the film industry, Hollywood introduced a production code that suppressed competitive innovations. The code was _content-oriented,_ meaning it created rules about which films were allowed to be screened in theaters.

The code banned dance scenes that suggested sexual or "indecent" acts, for instance.

The code also prevented the production of more scandalous or bizarre films that were deemed less marketable. At its core, the code impeded the development of creative niche films that might've competed with films from the bigger studios.

> _"The cycle is powered by disruptive innovations that upend thriving industries, bankrupt the dominant powers, and change the world."_

### 5. Television skipped the “cycle,” missing an early open phase yet falling victim to cable competition. 

John Logie Baird presented the world's first working television in 1926. Similar to how Alexander Bell had coaxed voice out of a wire, Baird discovered how images could be sent through a filament.

Television however didn't have an early period of openness before it was used to make money.

As soon as the technology was introduced, big radio companies like CBS and NBC saw that it would have a big impact on the media and communications industry. As a result, these corporations made sure they could completely control it from the start.

The companies proposed that only experienced, reliable broadcasters should be given licenses for television, because only they could be trusted to uphold the high ideals of service. The ruse worked: the proposal was adopted, so companies had to apply for licenses to work in the industry.

There were tough requirements, such as having a certain number of engineers and facilities, and lots of startup money. These requirements kept small companies out of the market, so big corporations could control television exclusively.

The television industry remained a closed shop until the advent of cable television in the 1980s. Hobbyists developed it, by creating a system to transmit television to places that a broadcasting signal couldn't usually reach.

The big corporations feared that cable television could destroy their control over local television. This proved to be true, as local cable television channels were able to compete with local channels run by national networks.

So cable television overturned the world of "free" television. By 2010, the majority of American households were paying to access television programs, either via cable, satellite or fiber optics.

### 6. The government stepped in and broke up monopolies in film and phone, and innovation flourished. 

When a monopoly on information technology breaks up, the industry has a chance to become open. When the cycle turns like this, innovation and creativity is possible again.

This happened when AT&T lost its monopoly in the late 1960s, during the Nixon administration. Representatives from both the U.S. Justice Department and the White House Office of Technology Policy were wary of AT&T, as they believed competition was healthy for a market.

The government decided to break up AT&T's monopoly by dividing the company into smaller firms, to create new areas of competition within the larger telephone market.

One pool of competition was in other devices that could attach to phone lines. Inventors were suddenly free to develop new technologies that could take advantage of this. This gave birth to several new innovations, such as the fax machine, which connected to a telephone jack.

A similar period of experimentation occurred in film when Paramount's monopoly was broken up.

By the 1920s, Paramount was one of Hollywood's biggest studios, and it controlled a large number of theaters. In 1948, however, the Supreme Court and the Justice Department agreed that Hollywood was involved in an illegal conspiracy against market competition, and such collusion needed to stop.

So the government stipulated that every Hollywood studio had to sell off its theaters, to break up the studios' control of film distribution.

This triggered the collapse of the production code system and ushered in a new period of experimentation and innovation. Independent producers began to emerge, and they could sell their films directly to theaters, which suddenly had a much broader variety of films from which to choose.

### 7. The broken monopolies in telephone and film were reinstated after short periods of openness. 

So what happens after a monopoly is broken up, and an industry experiences openness again?

Well, the experimentation that follows spawns a new generation of closed industries, and the cycle turns.

This happened when the telephone industry came under AT&T's domination for a second time. In 1984, regulators forced AT&T to turn their regional subdivisions into individual companies, called _Baby Bells_. Twelve years later, AT&T and its former Baby Bells signed the _Telecommunications Act._

The Telecommunications Act was supposed to bring more competition to the industry. It was intended to be the final act in breaking up AT&T's monopoly.

Instead, however, it had the opposite effect: it allowed the Baby Bells to eliminate even more competition. Each company subdivision worked to crush local rivals, by spending millions of dollars in lobbying.

These local competitors gradually withered away, and within 20 years, the telephone industry was dominated again by "Ma Bell."

The film industry went through a similar process. By 2000, the industry found itself under the control of just a few media conglomerates.

The open period of film production _did_ nurture creativity, but it didn't last. Innovation was risky because a single big failure could take down an entire studio. The movie _Heaven's Gate_, for example, was such an expensive failure in the early 1980s that it led to the near-collapse of its production studio, _United Artists_.

Studios needed to protect themselves from this kind of disaster, so they started merging. When smaller studios linked up, they could share the risks.

_Warner Communications_ and _Time Inc._, for instance, merged to create _Time Warner_ in 1987. Time Warner is now one of the world's largest entertainment companies.

So again the industry consolidated, controlled by the biggest players that are still active today, such as Time Warner, Walt Disney and 21st Century Fox.

### 8. The internet could fall prey to monopolistic trends, following the same path as the phone and film. 

So now we ask ourselves: Is the internet somehow different?

Every other bit of information technology has had a period of openness, only to become closed. What is more powerful, the design of the internet or the inevitability of the cycle?

Today we appear to be in the early stages of a monopolization of the internet. Consider the power of corporations such as Apple, Microsoft, Google, Amazon or Facebook, each having increased their status and control as the internet has grown in size and influence.

Some even hold effective monopolies over aspects of the market already, like Google with internet search and Apple with music downloads.

Clearly, the internet isn't immune to monopolization, as many people thought it would be in its early days. It seems to be charting a similar development path as other technologies before it.

Not only have companies like Apple, Google and Facebook been increasing their power, they've also tried to create closed structures within the market.

Since releasing the Mac, Apple's philosophy has been that closed systems are easier for consumers to use. Apple computer cases can only be opened and disassembled with specific Apple tools, and Apple software is only compatible with Apple computers.

Google has also built a closed environment, by creating a web browser, an email service, a storage service and a social network that are all linked together.

Facebook, too, has a closed structure. All Facebook pages are property of Facebook, and aren't linked to the rest of the open web.

So there is little doubt that the internet is also susceptible to a monopolistic threat, similar to those which have subsumed other technology industries.

The future path of the internet industry, however, is less clear.

> _"Both the web and internet are open systems...But as the history of information industries shows, such ecosystems tend to be short-lived."_

### 9. Openness still might win the day, as the internet could follow a different path, breaking the cycle. 

So the internet might not be immune to a monopoly, but are there any examples that could show the industry may be able to follow its own path, breaking the power of the cycle?

The history of _America Online_ (AOL) is one example of how the open design of the internet can destroy a monopoly.

In the 1990s, AOL was the original online platform. Users didn't surf the open web, but were limited to specific content provided to them by AOL. The company's business model was based on "dial-up" services, that is, having a computer connected to a landline to "call" AOL to access the service.

As the internet grew, other companies began to offer home internet services. The new options forced AOL to change its business model and give its users direct access to the internet. Open access made AOL's service unnecessary, and its subscriber numbers dwindled rapidly.

The advantage of open systems can also be seen by comparing software designed by Microsoft and Google to software designed by Apple.

Former Apple CEO Steve Jobs decided that all Apple products should have integrated systems, meaning the hardware and software couldn't be separated. Microsoft, on the other hand, has been more successful because it made software that could be used on any computer.

Google created _Android_, an open-platform software for phones, and it offers for free a set of tools that programmers can use to write new applications. As Apple's iOS only works with its iPhone, Android reaches more consumers — and in 2011 became the world's top-selling smartphone software.

An open system determines what software can be written for it, which influences how we use the internet. Therefore, open systems might beat out closed systems used in internet monopolies.

### 10. Final summary 

The key message in this book:

**Information technologies churn through a typical cycle, from a period of openness and creativity to an end point of closed, commercial control. While the internet appears to be in the early stages of this process, its structure is more inherently open than the other technology industries that preceded it. Whether this will enable the internet to escape the fate of "the cycle" remains to be seen.**

**Suggested further reading:** ** _Who Owns the Future?_** **by Jaron Lanier**

_Who Owns the Future?_ explains what's wrong with the current way the information economy works, and why it's destroying more jobs than it's creating.
---

### Tim Wu

Tim Wu is an author, policy advocate and law professor at Columbia University. He coined "network neutrality," the principle that internet providers should treat all data on the internet equally. He's written for a number of publications, including _Slate,_ _The New Yorker_, _Time_, _The New York Times_, _The Washington Post_ and _Forbes_.

