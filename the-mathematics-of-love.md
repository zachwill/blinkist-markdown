---
id: 5640536838663300070e0000
slug: the-mathematics-of-love-en
published_date: 2015-11-09T00:00:00.000+00:00
author: Hannah Fry
title: The Mathematics of Love
subtitle: Patterns, Proofs, and the Search for the Ultimate Equation
main_color: F69A93
text_color: 99605B
---

# The Mathematics of Love

_Patterns, Proofs, and the Search for the Ultimate Equation_

**Hannah Fry**

Love is full of patterns — from the number of sexual partners we have to the way we select potential mates from dating websites. In _The Mathematics of Love_ (2015), Hannah Fry sheds some light on these patterns and teaches you how to calculate your chances of finding The One, make a mathematical argument to justify approaching someone in a bar, and to use a mathematical trick to plan your wedding.

---
### 1. What’s in it for me? Discover the beautiful patterns of romance. 

Love is fantastic, love is complicated, love can be painful — and love is full of patterns. That's what mathematician Hannah Fry has poured _her_ love into, in order to reveal what mathematics can tell us about the secrets of lasting relationships.

From your odds of finding The One to the way game theory reveals the best pick-up strategy, these blinks will take you on a journey through the beauty of math and love, and the way they inform one another.

You'll also learn

  * what alien civilizations have to do with The One;

  * what bidding can teach us about dating; and

  * why 37 percent is the magic number.

### 2. We can calculate our chances of finding a partner – but we shouldn’t be too picky. 

The search for romantic love can sometimes leave us feeling somewhat hopeless — as though the odds are against us. One such discouraged bachelor was mathematician Peter Backus. In 2010, Backus went so far as to prove that there were more intelligent _alien civilizations_ out there than there were potential girlfriends for him!

His conclusion was based on calculations led by the following questions:

How many women live near me? In his case — in London — four million.

How many are likely to be of the right age range? This came to 20 percent, or 800,000 women.

How many are likely to be single? This amounted to 50 percent, or 400,000 women.

How many are likely to have a university degree? This would be 26 percent, or 104,000 women.

How many are likely to be attractive? He calculated this at five percent, or 5,200 women.

How many are likely to find me attractive? Again, this was calculated at five percent, or 260 women.

Finally, how many am I likely to get along well with? This came to ten percent, or 26 women.

This left Backus with just 26 women to date. In contrast, scientists currently estimate there to be around 10,000 intelligent alien civilizations in our galaxy.

But if Backus had been a tad less picky and relaxed his criteria a little, he would've had a substantially larger pool of potential partners. For example, he assumed that he would only get on with one in ten women he met. Yet if he increased this percentage to 20 percent, along with increasing the percentage he found attractive to 20 percent, and the percentage of women who would find _him_ attractive to 20 percent, he would be left with a far more optimistic total of 832 potential partners.

In love, it pays to be moderately flexible with your criteria. 

As luck, or perhaps math, would have it, Backus eventually tied the knot in 2014.

> _"[... Mathematics] can offer a new way of looking at almost anything — even something as mysterious as love."_

### 3. Some mathematical concepts are linked to beauty, but the golden ratio is not one of them. 

We all know beauty is in the eye of the beholder, but there are a few people whose faces are so attractive that they seem to be almost unquestionably beautiful or handsome. Therefore, there must be some basic criteria for beauty that we all agree on. 

Some people think beauty is encoded in a mathematical concept known as the _golden ratio_, which has been repeatedly applied to human attractiveness. The golden ratio is a number approximately equal to 1.61803399 and is usually denoted by the Greek letter _phi_, or _Φ_. However, the theory doesn't hold up very well.

You may have heard, for example, that the perfect face should have a mouth that is 1.618 times larger than the base of the nose, eyebrows that are 1.618 times wider than the eyes, and so on.

But the problem with applying the golden ratio is that if you're looking for a pattern, you're highly likely to find one, especially if you're prepared to be somewhat lax with your definitions. For instance, how do you define where the start of your ear is, or precisely the point at which your nose ends?

Aside from the golden ratio, there are some mathematical ideas that do seem to be connected to beauty. One such idea is our preference for an average face shape. Researchers have long known that overlaying images of numerous faces from a given ethnic group leads to an average face that is widely considered attractive.

The theory behind this preference is that when looking for a partner, we tend to be averse to unusual face shapes as they may indicate a genetic mutation that we don't want to pass on to our offspring.

Facial symmetry is also an important factor for attractiveness, and people with more symmetrical faces consistently score highly on attractiveness surveys.

> _"Beauty is what lies between the equations. Everyone truly has a unique ideal, so there's no mathematical solution here."_

### 4. Math says it isn’t always best to go for the most attractive person – but you should seize the initiative anyway. 

You're single and you're at a party. Should you sit back and wait for someone to approach you, or waltz up to the most attractive party-goer, chat them up and risk rejection? 

Well, it's not the smartest idea to make a beeline for the person you are most attracted to, according to the Nash Equilibrium.

Perhaps you've seen the film _A Beautiful Mind_, which tells the story of noted mathematician John Nash.

In one famous scene, Nash and three of his male friends see a group of five women in a bar: four brunettes and one very beautiful blonde.

The men are immediately drawn to the blonde, but rather than tripping over each other to get to her, Nash suggests a tactic based on mathematics that accommodates the best interests of all of them.

His theory of equilibrium states that if they all went for the blonde, they'd block one another, resulting in none of them going to bed with her. However, if none of them approached the blonde woman, they wouldn't get in each other's way _and_ they wouldn't insult the other women by making them feel like second choice. The conclusion, then, would be to approach the four brunettes.

However, sometimes it does pay to simply approach the person you've singled out. At a party, if you do the asking, thus risking continual rejection, you end up better off than those who sit back and wait for people to come to them.

If you start at the top of the list of people you'd like to hook up with, and work your way down, you'll end up with the best possible person who accepts your advances. But if you sit and wait for people to talk to you, you'll wind up with the least preferable person who approaches you.

> _"It is always better to do the approaching than to sit back and wait for people to come to you. So aim high, and aim frequently: The math says so."_

### 5. Algorithms on dating websites are elegant but cannot accurately predict compatibility. 

Too many parties in a row can really sap your energy for meeting potential partners, so what about online dating?

Dating websites apply algorithms to calculate how compatible people are with one another. OkCupid is a free site founded by a group of mathematicians, and is based on a particularly elegant algorithm that generates a score to show how well two people match. The key ingredients are: your responses to a questionnaire, the responses you'd like a partner to give, and how important each question is to you. 

The values you can assign to your questions are as follows: Not at all important = one, A little important = ten, Somewhat important = 50, Very important = 100, and Mandatory = 250.

Let's demonstrate with two fictional characters — Harry and Hermione — and two questions: "Do you like quidditch?" and "Are you good at defeating dark wizards?"

First, the algorithm calculates how good a match Hermione is for Harry as follows: say Harry answered the first question "Yes" and rated it as "A little important." Say Hermione answered this question with "Yes" too. This would mean that she gets ten out of ten possible points. But say she responded to the second question with "No," although Harry had wanted his match to answer "yes" and had rated the question as "Very important." She would then get zero out of 100 possible points for that question.

The result? Hermione's total match percentage for Harry would then be (10+0) / (10+100) = 10/110 = 9.1 percent. Lastly, the algorithm would calculate Harry's match percentage for Hermione and these percentages would be averaged.

But even elegant algorithms often fail to accurately predict our compatibility with others. Obviously, if the internet were the ultimate matchmaker, there wouldn't be so many of us still suffering through awful dates. The flaw is that using _individual_ data to predict how well two people will get along together often doesn't work: two people may both love the Harry Potter movies, but that doesn't tell us anything about whether they would enjoy watching them _together._

> _"An algorithm that can accurately predict your compatibility with another person simply doesn't exist yet."_

### 6. Game theory reveals how men can persuade women to have sex with them and why complacent women don’t get married. 

So you meet someone and sparks are flying — are there any mathematical rules that can help you get what you want? Try game theory.

Game theory applies best to men with only one thing on their mind, so if you're a man dead set on persuading women to have sex with you, mathematicians Peter Sozou and Robert Seymour can help.

Sozou and Seymour's approach assumes that you have a range of offerings at your disposal such as flowers or a candlelit dinner. Your tactic, then, should be to go for the gifts that are most likely to get you what you want without attracting women who just want the gifts.

In game theory, the woman is regarded as an opponent whose task it is to win the best man by using sex as a means of bargaining. Here's how the strategy works: in order to impress the woman in question, choose gifts that have a high value, but have to be shared to have meaning. So a candlelit dinner, a fancy firework display or pulling up to her house in a Ferrari would all be great options. Buying her jewelry would not. 

Game theory also posits that women shouldn't get complacent when seeking a partner. Dating can be viewed as the mathematical equivalent to auctions, where bidders submit bids. Now, it may seem that the strong bidder — in this case, the most attractive woman — has the highest chance of winning the man. But it's actually often the bidder in the weaker position who comes out on top. Why? Because when a weak bidder encounters a man she likes, she does everything to attract his attention. A confident bidder, however, is less likely to make an effort and is therefore less likely to get the man.

> _"No matter how hot you are, if your goal is partnership, don't get complacent."_

### 7. The number of sexual partners we have isn’t random, and mathematics can give us an insight into the spread of sexually transmitted diseases. 

Once you've found someone you click with romantically, you'll probably progress to the bedroom. Luckily, mathematics can also offer us some handy tips there, too!

Interestingly, the number of sexual partners we have is not totally random. In 1999, Swedish sociologist Fredrik Liljeros and a team of mathematicians found a formula that can actually determine the number of sexual partners we're likely to have over our lifetime.

Many would think this falls under the normal bell curve distribution much like height or IQ, but the formula instead follows what is known as the _power-law distribution_.

The way we form networks on Twitter is a great way of understanding the power-law distribution. Most people on Twitter have approximately the same number of followers, but some have a huge number. For example, Katy Perry has 57 million followers and is the biggest hub in the Twitter network.

As with Twitter followers, most people have around the same number of sexual partners, but there are some people who have a significantly higher number.

So what can we do with this information? Well, a mathematical understanding of sexual networks can reveal how sexually transmitted diseases are spread.

Those who have a very high number of sexual partners create focus points in the sexual network — and these are the critical players in the transmission of disease. But how can we find them? Let's go back to Katy Perry's Twitter presence for a second. If we selected a person at random from the 500 million people on Twitter, we'd have a paltry one in 500 million chance of finding Katy. But, if we chose someone at random and asked them to lead us to the most popular person they follow, it would bring us to Katy 57 million times. So we'd have approximately a ten percent chance of finding her.

This handy rule has been used to anticipate the spread of epidemics.

> Scientists found that the average number of sexual partners was around seven for heterosexual women and around 13 for heterosexual men.

### 8. Mathematics knows how many potential partners you should reject before you land the perfect one. 

Perhaps you've been dating for a while and now it's time to settle down. The question is, how can you opt for the best person to settle down with if you can't rule out potential partners that you have yet to meet?

Here's where mathematics can help again. In this case, math can tell you how many potential partners you should reject until you find the one you want to commit to. It's known as the _optimal stopping theory_ and works as follows: if you're destined to date ten people over your lifetime, the formula dictates that you have the highest chance of finding The One if you reject your first four partners. By doing this, the probability of your fifth partner being The One stands at a fairly reasonable 39.87 percent.

If more partners are on the cards — say 20 — then you should call it quits after your first eight partners. At this point, the likelihood of Mr. or Miss Right turning up in your life would be 38.42 percent.

Have you spotted a flaw in this calculation? Unless you're a member of the English royal family in the 1500s, your potential suitors won't be patiently waiting outside your chamber ready to be courted. There is simply no way to determine how many people you'll date across your lifetime.

But there is still hope for this model, and it lies in the number 37. Luckily, it's enough to just know how _long_ you wish to date in your life. So, if you start dating when you're 15 years old, with a view to being settled at 40, then you should reject _everyone_ in the first 37 percent of your dating window — until just after you turn 24. This gives you just over a one in three chance of finding your perfect match!

### 9. Mathematics can help you optimize your wedding planning. 

Say you've found the love of your life and it's time to plan the wedding. But before you dive head first into all the inevitable stress, let's see how mathematics can assist.

Near the top of everyone's list of stressors is the guest list — assembling a reasonable list of invitees can be a nightmare. Budgetary constraints and venue size can force you into sticky situations, and of course not everyone you invite will actually turn up on the day. 

Once again, mathematics can come to the rescue and help you anticipate more precisely the number of people who will attend your wedding.

First, list all potential invitees who are grouped as couples or families. You can enter this list into a spreadsheet, with the name of the group in the first column, and the number of people the group contains in the second column.

Next, decide how likely it is that each group will show up if they were invited. For example, your best friend from home is perhaps 95 percent likely to attend, so they and their group would collectively get a score of 0.95. Enter this score into the third column of your spreadsheet.

Then multiply the second column — the number of people in each group — by their score in the third column. This will give you a number for a fourth column containing the expected number of people who will accept the invitation.

As you work your way down the list, keep a running total of the expected number of guests in a fifth column. With this system, you will, on average, invite the right number of people to your wedding, thus alleviating one of the biggest headaches of wedding planning.

> A wedding of one hundred guests and ten tables will have 65 trillion trillion trillion trillion trillion trillion trillion different possible seating plans.

### 10. Mathematics can predict how likely a marriage is to last. 

Finding the one you want to settle down with is one of life's greatest pleasures, but sadly many marriages don't last. Wouldn't it be nice to know how best to act in a long-term relationship in order to maintain your wedded bliss? As you may have guessed, there's a formula for it!

To be precise, there is a formula for predicting how positively or negatively marriage partners will behave when it's their turn to speak or act in a conversation. When we look at arguments in particular, the way couples argue can differ considerably and can be a useful predictor of their long-term happiness.

Psychologist John Gottman actually found a way to score how positively or negatively a couple can act toward each other. Gottman's scoring system was surprisingly accurate, predicting divorces with up to 90 percent accuracy merely by observing a couple talking to one another.

But it wasn't until Gottman teamed up with mathematician James Murray that he unravelled how spirals of negativity in conversations are created.

Murray came up with two similar formulas for the husband and wife. These formulas allow us to calculate how positive or negative the husband and the wife will be in the next thing they say.

The wife's response, for instance, depends on her general mood, her mood when she is with her husband, and the influence her husband has on her. If the husband behaves slightly negatively, like interrupting her when she is speaking, he will negatively impact his wife. 

Through this formula we can determine at which point the husband is sufficiently negative to tip the mood of his wife, resulting in a likely break up.

Interestingly, Murray's equations have also been shown to successfully describe what happens between two countries during an arms race.

> _"The most successful relationships are the ones [... where] couples allow each other to complain, and work together to constantly repair the tiny issues between them."_

### 11. Final summary 

The key message in this book:

**From online dating to understanding the spread of sexually transmitted diseases, mathematics provides insights that can expunge some of the mysteries of love and give us the best chance of finding The One we want.**

Actionable advice:

**Make them loathe you or love you.**

When people choose their online dating profile pictures, they tend to hide things that might make them unappealing, for example, by using cropping techniques. But this is the opposite of what you should be doing.

People who are incredibly good looking are always going to be inundated with messages, but the rest of us would do better to divide opinion than to aim to look as cute as possible. Statistical analysis has proven that people who elicit polarizing opinions about their looks end up being far more popular on internet dating sites than people whom everyone agrees is just "quite cute."

So when selecting your profile picture, play up to whatever makes you different — including the things that some people might find really off-putting!

**Suggested further reading:** ** _Dataclysm_** **by Christian Rudder**

_Dataclysm_ shows what data collected on the internet can tell us about the people who use it, opposed to information gathered from the sterile environment of a scientific laboratory. What you'll learn is not all good news: when we think no one is watching, we often behave in nasty, brutish ways.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Hannah Fry

Dr. Hannah Fry is a mathematics lecturer at University College London's Centre for Advanced Spatial Analysis. She specializes in using mathematical models to study patterns in human behavior, from riots to shopping. Fry appears regularly on TV and radio in the United Kingdom.

