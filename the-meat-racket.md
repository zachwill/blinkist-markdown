---
id: 558929f23466610007950000
slug: the-meat-racket-en
published_date: 2015-06-25T00:00:00.000+00:00
author: Christopher Leonard
title: The Meat Racket
subtitle: The Secret Takeover of America's Food Business
main_color: AB8564
text_color: 735943
---

# The Meat Racket

_The Secret Takeover of America's Food Business_

**Christopher Leonard**

_The Meat Racket_ (2014) tells the bleak story of how a few giant corporations managed to monopolize the entire meat market in the United States. Through cunning business strategy, massive loans and a fair amount of bullying, companies like Tyson Foods rule the meat industry virtually unchallenged.

---
### 1. What’s in it for me? Learn the uncomfortable truth behind the meat you eat. 

Have you ever eaten meat from Tyson Foods? No? Well, if you've ever had a Chicken McNugget, or a hamburger in your staff cafeteria, it turns out you probably have!

Today, Tyson Foods is the biggest meat company in the US; in 2011 they raised 2.1 billion chickens, and slaughtered 7.2 million cows and 20.5 million pigs. They produced about a fifth of all meat consumed in the United States.

So why don't you know much about them? The company isn't very vocal about its business practices and, as a result, few know how they have risen to the top of their industry. These blinks change that; they show you the reasons behind the company's growth, and highlight the fact that while it might have brought cheap prices to consumers, it has spelled disaster for farmers.

In these blinks, you'll find out

  * why Tyson foods force their farmers to take part in a tournament;

  * what chickenization is; and

  * why Barack Obama's plans to reform the meat industry have failed.

### 2. The secret to Tyson Foods’ huge success was vertical integration. 

What's the best way for a company to increase its profits without actually having to improve its product? One business might start by improving its marketing campaign, while another could streamline processes in order to reduce redundancy.

For a huge company, however, these small-time solutions simply won't cut it; it would need something much grander.

For Tyson Foods, this grand idea was _vertical integration_, a process by which a company takes control of all the independent businesses that work with it.

In Tyson's case, companies from throughout the meat supply chain, such as farmers, feed producers, slaughterhouses and distributors, were placed under the company's tight grip.

This doesn't necessarily mean that Tyson _owns_ each of these partners. Rather, they are simply under Tyson's control by virtue of their contractual relationships: among other things, they have to comply with Tyson's strategy and prices, as well as work with other Tyson companies.

The big advantages of vertical integration are that it helps mitigate competition and unify strategy.

When a single company controls the entire supply chain, it's extremely difficult for a competitor to enter the market. Where will competing companies find farmers if Tyson controls them all? Where will they find people to butcher and process the meat? In all likelihood, they won't.

Furthermore, when you control the entire production process, you can force people to work toward your own strategic goals. With the competition vanquished, farmers, butchers and processors simply have nowhere to go if they oppose Tyson's practices.

For example, Tyson favors quantity over quality in its production, preferring mass-produced meat over higher quality alternatives. To this end, Tyson has forced its farmers to provide cattle with Zilmax, a drug that makes them grow faster. Farmers either agree to this practice, or get cut out of the business.

For Tyson, this strategy is incredibly profitable — for its suppliers, it isn't at all. In 2010 alone, Tyson Foods sold $28.43 billion worth of meat, earning $780 million in profits.

### 3. Tyson Foods’ success was built upon the insecurity that farmers experienced during the Great Depression. 

Until as recently as the first decades of the 20th century, the basic model for American farming was the small family farm. The huge, centralized industrial farms that dominate the market today were practically unheard of.

When the Great Depression hit, everything changed. Like everyone else, many farmers suffered immensely during the Depression. Markets dried up, and many farmers faced bankruptcy. They needed a savior, but who? Enter Tyson Foods.

In order to build their chicken empire, Tyson needed the support of the farmers.

At the time, chicken was not a very popular choice when it came to meat, and the chicken market was tiny. Tyson wanted to change this, but faced a major obstacle: Chicken is an unreliable meat. It easily loses mass in transport, and can easily go bad.

To help solve this problem, Tyson began implementing its model of vertical integration by buying a food mill, chicken hatcheries and slaughterhouses. But it still needed farmers.

Luckily for the company, the Great Depression left many farmers desperate for work. Instead of buying out their farms, Tyson came up with a different strategy: "Work for us, do as we say and you will earn a steady paycheck. Your financial troubles will all be over."

Many farmers jumped at the chance to earn a living once again, and allowed themselves to come under Tyson's control.

To sweeten the deal, Tyson also offered many farmers loans to purchase new equipment. For Tyson, the benefit was threefold: new machines would ramp up productivity, competitors would be driven out and the farmers would be indebted to the company.

But this was only the beginning.

> _"They would have to depend on Tyson to set rules that were fair."_

### 4. Industrialized chicken processing was Tyson’s next big step toward market dominance. 

So what was Tyson's plan after putting their system of vertical integration into place? Would it use its newfound efficiency to refine its product? Or would it go for growth?

It went for the latter, and did so with a passion. The company established a new official policy: _expand or expire_. But expanding is easier said than done, and Tyson faced many difficulties along the way.

First and foremost, it needed to grow the market for chicken. By the 1970s, chicken was becoming an increasingly popular choice of meat, but it was still a small player in the booming fast-food industry.

In response, Tyson chief Don Tyson spent 14 years pressuring McDonald's to include chicken on its menu. He finally succeeded when McDonald's started selling the chicken nugget, a product designed by Tyson to be cheap, long-lasting and easy to eat.

With fast food on board, chicken became increasingly ubiquitous. In 1969, the average American ate an average of only 39 pounds of chicken each year; by 1995, that amount was 70 pounds.

But another roadblock Tyson faced was the need for constant expansion, especially in order to supply the huge orders from companies like McDonald's and Walmart, all while keeping costs at a minimum.

Tyson's solution was _standardization_ : all their suppliers would use the same feed, and Tyson would standardize and rationalize their production processes.

Every bit of the chicken was to be used, including parts that would never be sold as chicken in a supermarket. Think "chicken tenders," a breaded chicken "product" made up of otherwise unusable cuts and sold to Burger King.

Even the chickens themselves became standardized. With its 1994 purchase of chicken breeding company Cobb-Vantress, Tyson gained exclusive ownership of a breed of chicken that not only grew faster, but could be slaughtered younger. Consider that in 1925 it took 15 weeks to raise a chicken to 2.2 pounds; by 1990, it only took about four weeks!

### 5. Because Tyson Foods doesn’t own its farms, the individual farmers take all the risks. 

So far, we've seen how vertical integration allowed Tyson Foods to dominate the chicken market, employing both industrialization and standardization.

But if control over subsidiary businesses was so important to Tyson, why didn't it simply buy them all? Many Tyson farms are still "owned" by individual farmers. Why?

The primary reason Tyson chooses to control farmers with contractual agreements rather than by simply purchasing their farms is because non-ownership mitigates risks and costs.

Early on in Tyson's ascendence, it realized that chicken farming was the riskiest area of its business. Farming is under constant threat of price swings that could eat into profit margins. And, of course, animals don't always grow the way you want them to! Some die on the farm, and others remain too small to become profitable.

So, instead of taking on these risks itself, Tyson left farmers with ownership of their own farms, thus leaving them responsible for the risks and costs.

Yet, despite the fact that they still own their farms, farmers have virtually no control over how they manage them.

Tyson exerts almost total control over the feed farmers use with their livestock, the medicines they give to them and the weight each chicken must reach before slaughter. Even the chickens don't belong to the farmers; they're owned by Tyson.

Tyson forces contracts upon its farmers that give them little leeway in terms of how the farmers price their products. The contracts enable Tyson to set the price they'll pay for each chicken, and this price is subject to change at Tyson's discretion, leaving farmers completely powerless.

For instance, if a farmer has higher utility costs than expected, they can't adjust the price of their goods accordingly, and might even end up losing money for each bird.

Despite all this, the farmers can't speak out against Tyson, because the company controls the entire industry; if one farmer complains, there are hundreds more ready to take her place.

> _"These loans reflected the same sort of hazy math and willful blindness that characterized the wave of subprime mortgages."_

### 6. Tyson uses a “tournament model” to keep competition between farmers high and costs low. 

What do you think of when you hear the word "tournament"? Perhaps regally clad knights jousting at a Renaissance fair? Or a giant sporting arena where athletes compete for prizes? Whatever comes to mind, chances are that a tournament is somehow fun or exciting.

For Tyson Foods' farmers, however, the word "tournament" has a much more ominous meaning.

Tyson uses a _tournament system_ with its farmers, allowing the company to constantly push down the price it pays its farmers for chicken.

For each of its farms, Tyson collects a huge amount of efficiency-related data. This data is then analyzed, and the most efficient farms earn a slightly higher price for their chickens.

So, higher efficiency is rewarded — seems fair, right? Well, things aren't quite so simple.

For starters, farms are measured against each other without taking into account key factors like modernization, size or available capital. In this system, huge industrial farms compete with tiny, old-fashioned ones.

The data review itself is conducted in secret; only afterwards do farmers discover their respective rankings, making it difficult for them to plan for the future. Farmers can't accurately budget for the following year because they have no idea what sort of ranking they'll earn next time, and thus don't know how much they'll earn for each chicken.

Finally, farmers aren't allowed to compare prices and results with each other. The results themselves are the "confidential and proprietary information of Tyson Foods, Inc.," meaning that they can't be discussed. As such, unionization and collaboration among farmers is almost impossible.

Tyson is the real winner of this tournament: farmers compete in a race to the bottom, and as they battle to become the most efficient, the price of chicken continues to plummet.

So far, you've seen how Tyson Foods climbed to the top of the food chain in the chicken market. The following blinks will examine its ascent to become the largest meat company in the US.

### 7. The chickenization of other meat sectors started with hog farming, although it wasn’t Tyson that benefited. 

As you've learned, Tyson Foods made great strides in bringing efficiency to the chicken business. In fact, their methods were so effective that they earned their own name: _chickenization_.

Specifically, chickenization describes how other meat production industries followed the example of the chicken industry: larger production, industrialization, vertical integration and the concentration of power in the hands of a few companies.

It comes as no surprise that one of the first companies to attempt this shift was Tyson Foods. In an attempt to break into other meat markets, Tyson tried to implement its chicken production methods within the hog farming industry.

In the early 1990s, the hog industry was in deep crisis. The rising power of the chicken industry meant that pork and beef consumption were in decline. Chicken was simply more affordable.

The answer was simple: take the farming strategy that worked with chicken and apply it to hog farming in order to cut costs and increase profits. So, Tyson bought up hog slaughterhouses, contracted hog farmers and began the industrialization of hog farming.

However, they faced a number of obstacles, one of which was a 1938 governmental regulation that placed limits on meat production in order to keep prices high and farms profitable.

Tyson and its competitors had long been lobbying to have this law overturned. Luckily for them, in 1994, the Republican Party regained a majority in the US Congress, and that meant deregulation.

Soon after, the Freedom to Farm Act was passed, which removed any barriers to production. The newly industrialized meat business was thus able to begin a campaign of massive overproduction.

Tyson's prospects in the hog market seemed limitless, but it still couldn't break through.

Its more established competitors, especially Smithfield Foods, grew much faster, and quickly claimed market dominance. Tyson simply couldn't compete.

Unaccustomed to following a market leader, Tyson largely withdrew from the hog business. With this defeat, their expansion seemed to come to an end — but it was only a temporary setback!

### 8. With the purchase of Iowa Beef Processors, Tyson Foods finally became the meat king of the world. 

With Smithfield's dominance in the hog industry, Tyson had hit a wall. But there was still one area where they could expand: the beef industry.

In the 1990s, the big player in the US beef market was Iowa Beef Processors (IBP). The company was enormous, much larger and far more profitable than Tyson. Yet, it was also in a period of transition. In an attempt to take the company off the stock market and return it to private ownership, a group of IBP managers had launched an attempt to buy out shareholders.

When news of this attempted buyout broke, IBP's rivals made bids of their own. Smithfield, for instance, made a bid, as did Tyson. But Tyson was a much smaller company, and didn't seem to have the capital to actually buy out its rival.

To give you an idea of just how far behind Tyson was, consider that the presumed selling price for IBP was in billions of dollars. Tyson's profits were below $300 million.

But Tyson's new leader, Johnny Tyson, was determined to force the sale through. In the end, against the advice of his father and majority shareholder, Don Tyson, he took out loans amounting to $1.4 billion in order to purchase IBP for $4.2 billion.

With this purchase, Tyson became the dominant power in the meat industry. In fact, about five percent of the average American's grocery bill would go straight to the one company.

But wait! Wouldn't this kind of market domination violate the government's antitrust laws? Not according to regulators.

They felt that, as IBP and Tyson were in "separate" industries (chicken versus cattle) and as there were still other large competitors in their field, the buyout didn't constitute a monopoly. Thus, Tyson was free to become the largest meat company in the world.

### 9. With large companies now in control of all meat markets, independent farming became more difficult. 

Over the course of the previous blinks, you've seen how Tyson Foods and its competitors have effectively come to control the entire meat industry. With vertical integration strategies, they can dominate whole markets, forcing their subsidiaries to accept their offers or simply go out of business.

But what about the farmers who refuse to deal with the leviathan of the meat industry? How difficult is it to remain independent?

As you might have guessed, it's incredibly difficult to both stay independent _and_ turn a profit.

The problem is that there is hardly any market for independent meat. The major companies in the chicken, beef and pork markets only work with farmers that have signed contracts with them. Farmers who refuse to sign have almost no one left to sell to.

In fact, in the beef industry, there are only four companies left who will buy meat from independent farmers. Usually there is only _one_ buyer that can use its advantageous position to set prices.

Further complicating matters is the fact that the price of meat is on the decline. Industrialization and modernization in the meat industry, combined with practices like the tournament method, constantly drive down the price of meat. For an independent farmer, the chances of actually making a profit decline along with it.

Faced with these grim circumstances, it's not surprising that most farmers sign the contract handed to them by the likes of Tyson Foods. Although you might lose the control over your product, a fixed-price contract at least means an opportunity to earn an income, no matter how paltry.

But was there no concerted attempt to work against such extreme concentration of power within the meat industry? Our final blinks will look at the efforts made by both governments and producers to distribute power more evenly, along with their miserable results.

> _"Feedlot owners say that they are lucky to get two companies to bid on their animals."_

### 10. Amazingly, companies like Tyson haven’t actually violated any antitrust laws. 

The last few blinks have outlined nothing short of the takeover of an entire industry by a few companies. Surely it is exactly these sorts of situations that antitrust laws were designed to prevent — so where was the US government during all of this?

Antitrust laws are generally designed to protect producers _and_ customers. Yet, whereas farmers were hurt by turbulent times in the meat industry, customers saw nothing but advantages.

From about 1950 through to the early 21st century, the price of beef, pork and chicken was on the decline. This was largely as a result of industrialization in meat production. The rise of the big firms, it was argued, was beneficial for consumers, who now pay less for their meat.

Despite this reasoning, some initiatives did take shape to work against chickenization and vertical integration.

One early move against these practices happened in Iowa in the mid-1990s. Iowa had an anti-monopoly law in place that prevented meatpackers from owning the animals they processed. Smithfield had ignored this law when it implemented vertical integration strategies in the state.

A lawsuit was brought against Smithfield, but it was actually quite difficult to prove the company's guilt. To dodge this attack on their production methods, Smithfield simply transferred ownership of the animals to a single person.

Nevertheless, this initial resistance did encourage people to act against vertical integration in a broader sense.

In total, 16 states joined Iowa in an effort to regulate vertical integration in the meat industry, and even the ruling Clinton administration took a stand at the behest of consumer advocate Ralph Nader.

Yet, it all came to nothing. Clinton shifted his priorities once pressure from Nader subsided, and the momentum in all 16 states gradually died away.

Only Iowa made any concrete progress with regulation over the tournament system; to this day it remains the only state to do so.

Still, there was hope. Barack Obama had listened to the farmers that helped him win the Iowa caucus, and eventually the presidency. Changes in agriculture were on his agenda.

### 11. The Obama administration’s attempts to give power back to farmers have proven to be futile. 

When President Obama took office in 2009, he brought with him a zeal for reform. He promised to change the way that health care, immigration and even agriculture were run.

As part of this reform, Obama brought in Tom Vilsack as Secretary of Agriculture. Vilsack, an Iowan, took the agreement made in his home state — one that limited the power companies exercised through the tournament system — and hoped to use it as a model for reform throughout the country.

For the first time ever, someone high up in antitrust regulation was prepared to closely scrutinize vertical integration in the meat industry and take steps to regulate it.

Not surprisingly, Tyson and its ilk fought hard against any attempt at reform. Fighting regulation was especially important to them, because at the time, the meat industry was facing one of its biggest crises in decades.

Government subsidies for biofuels had pushed corn prices up, and the price of corn-based feed rose with it. The price of meat climbed as a result, hurting sales and causing the first drop in per capita chicken consumption since World War II.

Facing declining profits and a reformist administration, the industry went all out in an effort to preserve their advantageous position. In total, the industry spent $5.94 million in lobbying in 2010, with Tyson alone contributing $2.59 million.

They pressured Congress with the argument that the industrial farming system was too complex and efficient to change. The Tea Party movement backed them up, arguing that the Agriculture Department was acting outside of its jurisdiction.

They even organized their own "grassroots" movement of farmers against these new regulations.

All their hard work eventually paid off. Faced with a mountain of pressure from the big meat industry, the government backed down.

Government and consumer advocates couldn't overcome the wealth and organizing power of this enormous corporate interest. Today, Tyson Foods and a few other companies remain as powerful as ever.

> _"In retrospect, the Obama administration seems almost naive in the way it attempted to reform the meat industry."_

### 12. Final summary 

The key message in this book:

**Meat today isn't produced on Old McDonald's farm. Rather, it's produced by highly industrialized farms that are all controlled by a few corporate giants. Within only a few decades, these corporations have completely reshaped rural America — and have gone virtually unnoticed.**

**Suggested further reading:** ** _Eating Animals_** **by Jonathan Safran Foer**

_Eating Animals_ offers a comprehensive view of the modern meat industry and demonstrates how the entire production process has been so completely perverted that it is unrecognizable as farming anymore.

The book explains the moral and environmental costs incurred to achieve today's incredibly low meat prices.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Christopher Leonard

Christopher Leonard is a fellow at the New America Foundation, a nonpartisan public policy institute, and has worked as an agribusiness reporter for the _Associated Press_. _The Meat Racket_ is his first best-selling book.

