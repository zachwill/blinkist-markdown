---
id: 55ffff58615cd100090000b6
slug: the-membership-economy-en
published_date: 2015-09-24T00:00:00.000+00:00
author: Robbie Kellman Baxter
title: The Membership Economy
subtitle: Find Your Super Users, Master the Forever Transaction, and Build Recurring Revenue
main_color: E14143
text_color: C7393B
---

# The Membership Economy

_Find Your Super Users, Master the Forever Transaction, and Build Recurring Revenue_

**Robbie Kellman Baxter**

Today, ownership is out. Consumer trends show that more and more consumers want temporary access, not permanent ownership. And within this changing landscape, membership-oriented businesses are king. _The Membership Economy_ (2015) outlines key strategies and tactics based on real-world examples for successfully building a membership organization.

---
### 1. What’s in it for me? Get in on the membership economy. 

Imagine always having access to a new, clean car, without worrying about having a garage or paying taxes for it. Doesn't sound bad, right? In our modern, networked world, this is the reality for an increasing number of people who simply want _access_ to a product, rather than ownership of it. The result is that more and more companies are changing their business model from a customer-based to a membership-orientated one. 

These blinks explain the membership business model and outline the workings of modern membership organizations. Based on real-world examples of companies that successfully use the membership business model, you'll be provided with handy tips on how to transform a traditional ownership-oriented company into one oriented around membership.

You'll also learn

  * how Facebook started small and then focused;

  * why Adobe is suitable for access; and

  * how the "free" membership option ruined Napster.

### 2. Internet-powered businesses offering customers “access” rather than “ownership” are on the rise. 

Just 15 years ago, pretty much everyone had their own collection of CDs and movies. Today, that's no longer the case. Obviated by services like Amazon Prime, Netflix and iTunes, such collections now seem slightly decadent. With the unprecedented global access to media provided by the internet, a new business model, that of access-oriented streaming services, now dominates the market. 

The advantage of this model is that it allows us to bypass all the responsibilities and costs associated with ownership. But it's a big shift, because the traditional economy is built around the principle of ownership; when customers buy things under the standard system, they can do whatever they want with them.

Just think of owning a car. You can paint flames on the side or adjust the chassis. But, of course, there are also costs. If the car breaks down, you have to fix it. You also have to pay taxes and secure a parking spot. 

But what if you only need a car once or twice a month? Renting might be the best solution. That way, you'd have access to a car when necessary, but not have to bear the responsibility or pay the costs of ownership. 

Fortunately, the internet makes this kind of access easier than ever before. And over the past few years, new web-powered companies have emerged, giving their customers convenient alternatives to ownership in almost every sector. 

For instance, Zipcar members have access to a fleet of company-owned cars. Using a slightly different approach, RelayRides pairs car owners with those who want to rent a vehicle. Similarly, you can rent movies on Netflix or book a temporary room through Airbnb.

Crucially for companies specializing in access, the focus changes from products (the traditional economic model) to customer. More specifically, the customer is no longer merely a client, but a member, as we'll see in the upcoming blink.

### 3. Membership-oriented businesses create an ongoing relationship between company and customer. 

Imagine going to a store to buy a hammer. After you've paid for it, your relationship with the shop is over. This is the classic model of customer interaction. The membership model, on the other hand, is different. When customers commit to a company, they get all the benefits and responsibilities of an ongoing relationship. 

This relationship could be determined by a subscription (as with Netflix) or a user ID (think of Facebook), or with a simple membership card, like what you'd get at the gym. What's important is this: in the _membership economy_, the relationship between customer and company changes. Unlike traditional "customers," members have a stake in the company. 

The advantage of this model is that both parties gain something from the relationship — but they also have to provide something. Netflix provides access to movies, but customers have to pay for them; Facebook grants members access to a dynamic social platform in exchange for customers' personal data. 

As we've hinted, companies in the membership economy come in many different forms. And we can break them down into four basic categories:

  * _Digital subscriptions_ : online businesses like Netflix ask members to pay a recurring fee to access content, features or services.

  * _Online communities_ : platforms like Facebook or LinkedIn create networks around shared interests or goals. 

  * _Offline loyalty programs_ : brick-and-mortar businesses like Starbucks track and reward loyalty, perhaps by providing discounts for frequent shoppers.

  * _Traditional Membership Economy companies_ : like banks or phone companies, these membership-oriented firms predate the internet. To use the service, customers must set up an account.

Once you've settled on which form your company will take, you've got to attract members. We'll look at how that's done in the next blink.

### 4. Attract new members by setting up and refining an effective acquisition strategy. 

As we've discussed, members are the meat and potatoes of any membership-oriented business model. But how does a company attract members? Well, you just need the right acquisition strategy, and soon enough members might start recruiting new ones, all on their own!

An optimal acquisition funnel will follow four phases. We call it a funnel because the first (or top) phase tries to include as many people as possible. In each successive phase, some people will drop out. Then, in the last phase (when we've reached the bottom of the funnel), a relatively small amount of potentials will actually become loyal members. 

Here are the four phases:

  1. Start by going after people who are _aware_ of your brand but haven't engaged yet.

  2. Then, some of the people will do a _trial_ to get to know your company better.

  3. Next, some people will actually _sign-up_. 

  4. Finally, a percentage of those sign-ups will stay on in the longer-term and become _loyal members_. 

Once you've established an acquisition funnel, you can improve it by learning about your members' behavior. Start at the bottom of the funnel to understand your target member. Then, track how many potential members make it through each phase of the process. 

If you see that not enough people are converting from one level to the next, you've identified a hole in the funnel. A typical hole might be that potential members like the service in the trial phase, but think the price is too high or the duration of the contract is too long. Address those issues to fix the hole.

Also, it's crucial to retain members once they've signed-up, so track new member behavior and do everything you can to convince them to stay. Typically, someone who keeps their membership for at least 30 days will end up staying for much longer.

### 5. Implement the right onboarding strategy to retain new members and transform them into superusers. 

Imagine if Facebook and Pinterest had zero members. If no one posted or pinned content, the platforms would basically be worthless! And that's the thing about membership organizations — if members don't regularly participate, the companies fall apart. 

That's why you need a good onboarding strategy that kicks in right after sign up. Typically, onboarding should progress through the following three phases:

  1. _Remove friction_ : Make the registration process as easy as possible. You don't want to lose potential members because the trial or sign-up phases are either faulty or overly complicated. 

  2. _Deliver immediate value_ : Engage the member immediately after they sign up, either with a gift or by offering immediate access to your network (the latter being what Facebook offers). During this stage, you should also ask new members for feedback and be responsive to their critiques. 

  3. _Reward desired behaviors_ : If you want your users to refer new members, reward them for doing so! That will increase their loyalty while also improving the likelihood that they'll refer others in the future. 

The point is, with the right techniques, you can create _superusers_ — members who are particularly devoted to your company, meaning they create lots of content, help other members and attract new ones. 

And there are three basic rules that can help you gain more superusers:

  1. _Make it easy_ by providing meaningful benefits to your members from the moment they sign up. For example, offer an easy-to-follow tutorial.

  2. _Make it personal_ by showing that you care about your members. Stay in contact with them!

_Get members involved_ by providing benefits to members who help others or attract new members. Offer a referral program, for instance.

### 6. Use a simple pricing model and be aware of the downsides of a “free” membership option. 

When buying a car, you factor the price of repairs into your purchasing decision, to understand the long-term cost. But how do you calculate the cost of membership?

Subscriptions are one of the most common pricing models in the membership economy — and for good reason. A company offering subscription-based pricing can provide ongoing value for the duration of a user's membership. Plus, tiered pricing offers greater flexibility because it's based on the value provided to each member. 

For instance, at first, SurveyMonkey offered only one low-priced subscription package. But when the company introduced two higher-priced subscriptions, which offered added features and services, the new packages attracted plenty of new members. Meanwhile, most of the old members stayed in the low-priced tier. 

Ultimately, the key to subscription-based pricing is to communicate costs and benefits clearly. Members need to know _exactly_ what they get for their money — meaning, there are no hidden costs. That way, you can build trust from the start.

As you know from using Facebook and other social platforms, free memberships are also an option. Although this model will attract more members, however, it should be used with caution. 

First, the upside: having a "free" membership will raise awareness for your brand, allowing you to build a large community and thus provide content and added value for members. 

Sounds good, right? Well, unfortunately, free models also have a significant downside: when you condition people to expect a free service, the potential pool of paying members is diminished. 

Consider the story of Napster. The free peer-to-peer file-sharing platform was founded in 1999. After a number of copyright violations, Napster was sentenced to pay a total of $36 million in 2001. To meet its debts, Napster tried to convert to a subscription-based model. But the users were by then conditioned to expect a "free" service, and only a fraction were willing to pay. In the end, the owners had to sell their company.

### 7. Boost loyalty by personalizing the user experience and tracking member behavior. 

As we've discussed, members are the foundation of any membership organization. And thus, keeping them content is the core task of any successful membership organization. 

To do so, keep your service simple and personalize the user experience to build loyalty. In addition to the onboarding strategy we introduced earlier, there are a few easy ways to promote member loyalty. 

In fact, you can start building loyalty even before someone becomes a member, during the free trial phase. If you notice that someone doesn't continue with your service after signing up for a trial, you can do two things:

First, send the potential member a note with advice about how to get the most out of their trial. Then, if they don't respond, prompt the person to cancel their membership; otherwise, they might feel tricked and develop negative feelings about your company. 

Keeping the sign-up phase and pricing process simple and transparent will also boost loyalty. As we've hinted, providing simple solutions tailored to different groups will improve the experience of individual members. 

And to that end, personalizing each member's experience is crucial. You can do this explicitly — by asking members to choose personalized options — or by analyzing user data to glean preferences. 

In fact, tracking user behavior is key, allowing you to learn what members like and dislike and to adjust your offer accordingly. To effectively gather and process that data, though, you'll need the right software. Here are a few tips:

  * Customer relationship management software, like Act-ON, helps organize marketing, customer service and tech support. 

  * Customer success software like Gainsight helps track and analyze member engagement. 

  * Loyalty software like Belly helps you recognize and reward member loyalty.

### 8. Start-ups need to establish a membership-oriented culture from the beginning to ensure members gain value right away. 

When first launching, start-ups often encounter the "chicken and egg" problem: they need a critical mass of members to gain any members. Because remember, without members creating content, there's little value to attract new users. 

To overcome this problem, the company needs to follow the membership principle from day one. Meaning, if you truly want your new venture to be part of the membership economy, you have to start with the right team and company culture. 

Taking a step back, you might remember that traditional business models focus on the product, while membership-oriented companies focus on the user. And within this new framework, your members don't make a one-time buy like a traditional customer. Instead, they generate ongoing value for your firm as long as the membership exists. Accordingly, compensation for salespeople should be based on customer lifetime value, not simple one-time transactions.

Furthermore, to provide value to your members from the very beginning, focus on starting small and customizing your service to a specific target audience. That way, keeping your niche membership happy will be easier and less expensive than trying to please a huge, general audience. 

Take Facebook, for instance, which started out by serving a homogenous group of Harvard undergrads. Since it was a social network, it needed to attract a community of people who knew one another. If all the initial members had been perfect strangers, the service would have provided little value. 

Another way to make your start-up concept compelling at launch is to start at the bottom of the funnel. LinkedIn offered its first members value by giving them an online platform to save and share their CVs. And then, once it built up a larger membership base, Linkedin could transition into more of a community and become a place for people to manage their professional connections.

### 9. To transition to a membership-oriented model, analyze your company’s eligibility and prioritize transparency. 

Technology has made it possible for people to get access to products and services via the cloud. But in the process, it has narrowed the market for ownership. This transition, from ownership to access, hasn't been easy. But for some companies, it's necessary to make the shift. 

Still, before you plunge into the membership economy, make sure your company is eligible for the membership model. Not every company is suited to this style of doing business, so if you're thinking of making the transition, consider these points:

The transition from ownership to access should benefit your customers (or future members). If it doesn't, they'll leave. Benefits might include lower risk, lower up-front expenses and lower maintenance. 

You should also research the membership market in your sector, to make sure it's not already saturated with the product you offer. Just think how hard it would be to launch another social community like Facebook!

If you do move forward with the transition, some dropouts are inevitable, but you can reduce their number. Start by adjusting your pricing to reflect the new membership approach (people aren't paying to own, but rather to use your product for a finite amount of time) and make sure your product is suitable for the access model. 

Consider Adobe: in 2013, the company shifted from selling its software as a physical disk to adopting a cloud-based subscription model. The firm now offers its product for access, by allowing its members to access the software from anywhere, simply by logging into the Adobe Creative Cloud. 

Finally, transparency is the key to member retention during a transition. Clearly communicate all benefits _and_ drawbacks to your stakeholders. That way, your move into the membership economy is sure to be a success!

### 10. Final summary 

The key message:

**To stay competitive in a membership-oriented business world, you need more than a basic company homepage; your whole organization needs to be built around the concept! If you fully commit to a membership model, put the right strategy in place to gain and retain members, appropriately price your product and track user behavior, your business will prosper.**

Actionable advice:

**Treat your clients like members, not like customers**.

Since your clients finance your business, make sure to show them you care. If they have complaints, listen. But also be proactive. Regularly ask clients for feedback about your company, product or service and then act on their advice. This way, you'll improve your service, retain your clients and successfully acquire new ones.

**Suggested** **further** **reading:** ** _The Automatic Customer_** **by John Warrillow**

From groceries to ski-slope access to MP3s — today all kinds of businesses operate by subscription, and everyone from giants like Amazon to small local firms are benefitting. _The Automatic Customer_ breaks down the multiple models you can use to tap into the power of subscriptions, explains how to measure your new success, and gives you tips on keeping up the good work.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Robbie Kellman Baxter

Robbie Kellman Baxter is a leading consultant and speaker. Her past clients include Netflix, Yahoo and eBay.

