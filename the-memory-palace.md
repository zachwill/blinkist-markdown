---
id: 566fdbcacf6aa30007000004
slug: the-memory-palace-en
published_date: 2015-12-15T00:00:00.000+00:00
author: Lewis Smile
title: The Memory Palace
subtitle: Learn Anything and Everything (Starting with Shakespeare and Dickens)
main_color: 405E85
text_color: 2B4C77
---

# The Memory Palace

_Learn Anything and Everything (Starting with Shakespeare and Dickens)_

**Lewis Smile**

_The Memory Palace_ (2012) is a step-by-step guide to using your spatial memory to help you remember absolutely anything. It teaches you how to build a palace of memories that will give you the power to recall everything you read, and even to memorize the names of every Shakespeare play in just 15 minutes.

---
### 1. What’s in it for me? Increase the capacity of your memory. 

How many US presidents can you list? And can you do it chronologically? Sure, you got JFK and Lincoln, but you probably missed someone like Martin van Buren or William Henry Harrison. So what if there was a way to improve your memory so things like this were a cakewalk? 

Turns out there is. We have an untapped reserve capacity in our memory. With a little awareness and a few techniques you'll find in these blinks, you can tap into this fresh memory reserve — called spatial memory — and remember more than you thought possible.

In these blinks, you'll discover

  * why crazy stories play a key role in remembering things; 

  * how Obama can help you remember when the Magna Carta was written; and

  * a trick for remembering the titles of Shakespeare's plays.

### 2. Your memory is like a muscle: you can strengthen it through training. 

Do you often have trouble remembering where you put your keys? Or maybe you're all too familiar with finding yourself wandering the aisles of a supermarket confused because you forgot your shopping list? Chances are you've found yourself in these situations as they are typical results of having an untrained memory. But don't worry, with a bit of exercise we can get our memory back in shape.

Think of your memory as a hidden muscle. Like other muscles, it can deteriorate if neglected. Perhaps you think that only the smartest and brightest have the ability to retain knowledge and recall facts. But the truth of the matter is that the potential muscle power of memory is strong in all of us. With some simple training and exercise you too can unlock this potential and be the next superstar of trivia night.

So what kind of training can you do to drastically improve your memory?

Start by using a simple technique that can turn anyone into a world champion of memorization in no time at all. The most effective tool for remembering information like names and dates is to connect them to an image or place. This technique takes advantage of our spatial memory, and the crazier the image the better it works.

For example, say you need help remembering the names of the early plays of William Shakespeare. Well, just imagine the unforgettable image of a three-headed Richard Nixon standing on a chair claiming to be Richard III. By tapping into the potential of spatial memory and linking a three-headed Richard Nixon with _Richard III_, you'll never forget the name of that play again.

Sounds simple enough, right? But what makes these memory techniques so effective?

### 3. Your spatial memory is old and powerful – use it to remember virtually anything. 

Do you ever wonder why it is you can remember every street and corner shop of your hometown but seem to forget the details of your shopping list the moment you put down the pen? The answer lies in your _spatial memory_.

The capacity for spatial memory is something all humans share and it is crucial for navigating our way through the world we live in. You use your spatial memory every day to remember important places and find your way home from work. Without it we'd be lost, constantly forgetting where we are and how to get to our destination.

Humans have developed this spatial memory over millions of years as an important life-saving tool. In the days of hunters and gatherers, before we had Google Maps in our pocket, remembering the best route back to our cave was vital for our survival. So the human brain developed spatial memory to recall things as they relate to our environment. It was more important to remember where to run to safety than to remember, for example, long lists of names. Unfortunately, this means nowadays we're actually fighting our evolution when we try to remember those long lists of names and numbers.

Does this also mean you should give up on being able to memorize every bone in the human body? Not so fast! It's just time to apply that spatial memory and train your brain muscles.

By training your spatial memory to meet the demands of the modern world you'll find that you can in fact connect factual information to familiar physical places. And with some regular exercise you'll be able to hold onto those names and numbers forever.

So how exactly can you exercise your spatial memory? Don't worry, there is a fascinating yet easy technique to help you!

### 4. Use a mental image of a familiar place to improve your memory. 

One way to expand your memory is to create a mental picture of a physical space that will act as a structure for your memories. Let's call this space a _memory palace._

It doesn't have to be an actual palace — anywhere will do. You can use your imagination to turn your own house into your personal memory palace, or maybe the familiar path from the bakery to work. As long as you are very familiar with it and can clearly visualize it, your memory palace will work: its looks aren't as important as the way your brain connects information to it. The most useful technique is to create a place with separate rooms or areas that will each "house" specific information. 

Now, to turn your memory palace into a memory powerhouse we just need to add a story, and the crazier the story, the more memorable the images and information will be. 

As you move through your memory palace, each room should contain a short and silly story that will connect the information you want to remember to the room in question. 

For example, instead of starting your story by simply waking up and walking to the bathroom, imagine it begins with President Obama sitting in the corner of your bedroom, waking you up with laughter and telling you that the Magna Carta was established in England in 1215. You probably won't be forgetting that information any time soon. Then you continue your story, going from room to room, seeing equally wacky and memorable things.

It's best to take a moment and imagine this image as vividly as possible. For the memory palace to really work, you have to use your imagination and see things in your mind's eye. With that in mind, let's see in the next blink what story we can craft to help us remember the plays of William Shakespeare.

### 5. Use your memory palace to remember Shakespeare’s most important plays. 

How many of Shakespeare's 37 plays can you remember? Probably not all of them. So let's try putting your memory palace to work. Just remember, make the stories in your memory palace as vivid as you can.

Let's start in the bedroom. Imagine waking up with the powerful urge to go to the theater to see a Shakespeare play. You jump out of bed in a rush to get ready, but your path is blocked by a tiny shrew that is so tame it is jumping through hoops! This is, of course, _The Taming of the Shrew_.

You step around the shrew and head to the kitchen for breakfast. But here you encounter four drunk siblings struggling to remember lines from a script. They're making errors and laughing hysterically. Obviously, what you're witnessing is a _Comedy of Errors_.

You decide to skip breakfast and head straight to the theater. You go to the next room to grab your bike when all of a sudden Julius Caesar himself comes galloping in on a beautiful white horse with a black mane and offers you a ride. Who would say no to _Julius Caesar_?

When you arrive outside the theater you notice two teenagers locked in a passionate kiss. Immediately, they stop and shout, "O, for we are _Romeo and Juliet_," and drop dead on the spot.

You enter the theater hoping to get a good seat. Suddenly a fat Henry VIII and his many wives jump onto the stage wearing roller skates. Immediately, they all start making figures of eight. Why? Because the name of the play is _Henry VIII_! 

So, did you really concentrate on vividly imagining these stories? If so, go ahead and test yourself. After a few hours, see if you can write down the names of all the plays in the story. It's safe to say you'll remember each and every one. Even better, this same principle can be applied to other areas, like your shopping list!

### 6. The Final summary 

The key message in this book:

**Our brain has an enormous capacity for memory and it goes largely unused. By utilizing spatial memory and building a memory palace we can unlock this potential. This technique can make a memory champion out of anyone: in just 15 minutes you can retain more information than most people can memorize in an hour.**

**Suggested** **further** **reading:** ** _Moonwalking with Einstein_** **by Joshua Foer**

_Moonwalking_ _with_ _Einstein_ takes us on the author's journey towards becoming the USA Memory Champion. Along the way he explains why an extraordinary memory isn't just available to a select few people but to all of us. The book explores how memory works, why we're worse at remembering than our ancestors, and explains specific techniques for improving your own memory.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Lewis Smile

Lewis Smile has led an eccentric life that has taken him from being the lead singer for the 1990s band Soulful Repetition to becoming an expert in the field of memory improvement. His other books include _MLM Scripts_ and _The Magic Square_.

