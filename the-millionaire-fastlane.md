---
id: 548a15c63363370009640000
slug: the-millionaire-fastlane-en
published_date: 2014-12-23T00:00:00.000+00:00
author: MJ DeMarco
title: The Millionaire Fastlane
subtitle: Crack the Code to Wealth and Live Rich for a Lifetime
main_color: D19746
text_color: 8C6A3A
---

# The Millionaire Fastlane

_Crack the Code to Wealth and Live Rich for a Lifetime_

**MJ DeMarco**

The world is full of people working hard with dreams of getting rich — but only a handful succeed. As _The Millionaire Fastlane_ shows, there's a simple reason for this; conventional roads to wealth don't work. But thankfully, there is one way that works, and if you're eager and willing to put in some effort and make some sacrifices, it's far quicker than you might think.

---
### 1. What’s in it for me? Find out why the conventional way to get rich is flat-out wrong. 

We've all heard the kinds of strategies that are supposed to make us rich: Get a good education, work hard, save up, retire and roll around in your savings like Scrooge McDuck. But what if you don't want to only enjoy your riches in the last part of your life? And anyway what if the stock market collapses and you lose all your savings? There has to be another way.

Thankfully, _The Millionaire Fastlane_ explains a better approach to getting wealthy. Starting with having the right mindset, and learning to think of wealth in a new way, you can forget about the old paths to wealth and end up young, rich and free to do what you like with your life.

In these blinks, you will find out

  * why a high income won't make you wealthy;

  * why looking for one "big hit" is misguided; and

  * the secret to starting your own business.

### 2. You’ll fail if you have the wrong mindset, no matter how much you earn. 

How do people get rich? Is it luck, or perhaps fate? A lot of people think so, which might be the reason why they themselves aren't rich. If you believe becoming wealthy is out of your control, this mindset is jeopardizing your chances. You _can_ control your future. It just takes planning.

If you believe that it's pointless to plan ahead, this puts you in a weak position to deal with life's challenges. Without precautions such as creating a financial plan or arranging for health insurance, external events like losing your job, becoming ill or having to suddenly deal with a recession can cause you far more grief than necessary.

Sometimes prioritizing the moment in terms of spending can have serious drawbacks. For example, if you live from paycheck to paycheck, suddenly finding yourself without a job would mean you can no longer pay your rent.

If you refuse to take responsibility for your life, even earning an impressive income won't keep you from going broke. If you think you have no power over your own life, it's tempting to want to spend your money while you have it. After all, what if you were struck by lightning tomorrow? This is why people splurge on instant gratifications like expensive dinners and luxury cars.

Yet having this mindset will drain your income and your credit range, because regardless of how much you earn, you'll spend it all. We've all heard stories of how some rappers go from carousing about with the cash from a huge hit to losing everything in one year.

Of course it's possible you could be run over by a train tomorrow. But this is unlikely. And even if something terrible should happen in the future, you'll be glad if you at least made some plans for worst case scenarios, like making sure you have a buffer of cash when you need it.

### 3. The conventional path to wealth is unsound. 

We all know the strategy: get a good education so you can get a well-paid job, then work hard and save and invest your well-earned money. Then, after around 40 years, you'll eventually be able to retire and enjoy your wealth. But there is a problem with this strategy.

First, it might not actually get you where you want. There's a ceiling to how much you can earn from a salary alone.

Clearly, you can't exactly keep raising your work hours indefinitely; it's just not possible to work 1,000 hours a week! Nor can you request your boss to dish out a 200 percent pay raise, even if you're an exceptional employee.

The stock market, too, is unpredictable and out of your control. Thus, the stocks you invest in might plummet in value or simply grow more gradually than you expected.

Finally, your savings and investments are probably going to be subjected to inflation and you might find yourself left with only a small fraction of the value you first invested in.

Therefore, if you're fortunate enough after some decades to save around $2.5 million, it may only be worth the equivalent of just $250,000 in today's dollars. Add to that the impossibility of knowing whether you'll even be alive to enjoy it.

Even if your plan works out, when you get to the stage where you can finally reap the benefits of your money and time, you'll be old. This strategy is based _entirely_ on working really hard while you're young, so that you can enjoy the rewards when you're older.

So here's a better idea: to make the most of your retirement, retire while you're young. Vitality and health tend to deteriorate as we get older and these are two qualities that enable us to appreciate free time. It's far more enjoyable to go hiking when your joints don't ache from arthritis.

### 4. You don’t need a flashy degree to get wealthy. 

Some specialist fields like medicine require a prescribed education. But remember, enrolling in a formal education isn't always the best route to wealth.

Often, the expense of education cancels out the higher salary you'll hopefully earn at the end of it.

Whereas it _is_ possible you'll earn more with a college degree, especially if, for example, you go into business with an MBA, the financial setback is a formidable consideration. The College Board reports an average college degree (including room and board) sets you back nearly $60,000. 

To afford this, you'll probably have to take out a student loan, making your education even pricier.

There's also no guarantee that you'll land a well-paying job after college. College-educated adults typically earn around $54,000 per year. If you also consider that completing a college education takes _years_, this leaves you with less time to earn money later.

A conventional college education may also encourage you to think in conventional ways. After absorbing the key concepts of your field and studying specific tools to solve problems, you may find it difficult to think outside the box. In finance, this could mean that you understand how to calculate return on investment, but are ill-equipped to find a new, innovative way to judge investment value.

The bottom line is, you can become rich and successful _without_ a prestigious degree.

Amazing business ideas don't require formal degrees. However, you may need some special skills. For example, to develop a lucrative internet service by yourself, you need the skills to write code, but you don't need a computer engineering degree.

Bear in mind some of the richest, most successful people in the world never completed college. People like Bill Gates, Michael Dell and David Geffen.

So, now we've uncovered the thoughts and strategies that may prevent you from getting rich and successful. What ways _do_ work? And how do you define wealth anyway?

### 5. Wealth doesn’t equal money, but money can increase your wealth. 

How would you define wealth? When most of us hear the word, we tend to instantly think about money. But really, there's far more to it than that.

Wealth is a combination of things that make you feel fulfilled. Actually, it has three main components, each of which is necessary for us to feel happy with our lives.

The first factor is meaningful relationships, including family; the second is our health and physical fitness; and the third is freedom.

So being wealthy isn't just about owning piles and piles of cash. Does a depressed and lonely person with an oversized bank account look like the picture of wealth to you?

That being said, being financially independent does put you on the path to wealth, in particular because it enables you to have freedom. Sure, money isn't all-powerful. Its impact is limited. For example, you can purchase the best health care with money, but no amount of money will transform your body into top condition. Nor can you spend cash in exchange for meaningful relationships.

In the case of freedom, though, it's slightly different. Once you have plenty of money to live comfortably, you no longer need to trade your time for money as you would do in a job.

Therefore, when you find yourself in this situation, you can use your money to buy you the freedom to spend your time as you wish, to visit places you want to visit and generally do what you want.

Being free means living where you want, traveling and enjoying your hobbies, even if they're expensive. This is really how having a lot of money can increase your wealth.

### 6. Becoming a millionaire is the result of a process, not one “big hit.” 

We've all skimmed those articles about some 20-year-old being presented with millions of dollars for their new internet company. It's news like this that encourages the notion that becoming rich happens in a single moment. But really, it's a process.

Becoming rich doesn't occur at the snap of a finger. Preceding every deal or offer is a well-thought-out plan and often _years_ of work. Consider an elite basketball player's contract. It's the result of years of conscious practice and games, and giving up leisure activities that might jeopardize performance or take up valuable practice time.

So stop praying for your one big break, because doing so encourages you to make poor decisions. Hoping for one big chance might influence you to gamble on minimal odds, like playing the lottery, getting into risky investments or auditioning for a game show. The unrealistic idea that one event will blast you to success will keep you from working in a way that might actually let you accomplish your goals.

So what should you do? Make a plan!

To become wealthy independently, you must create a product or business that is self-sustaining. This takes time. If you want to become so wealthy that you no longer need to work, you need to build a company or a product that generates income even while you're not actually working.

One great example is J.K. Rowling. Her _Harry Potte_ r books became the best-selling book series of all time and the movie adaptations were also immensely successful. As a result, Rowling became a multi-millionaire, and still reaps the monetary rewards from her books.

This system is the most effective way of being rich, because your income is independent from your time. But to make it work, you need to find a market gap, create a solid business plan and learn all the necessary skills.

### 7. To get really rich, think like a producer, not like a consumer. 

Right from childhood, we're taught to be consumers. We're exposed to constant, pervasive advertisements in the media, and at home we're taught to place value on Christmas lists and family brand preferences. The result? A constant desire for products. 

If you really want to be rich, get on the other team and start thinking like a producer.

Staying bound to a consumer mentality will limit you. When you view an ad from a consumer perspective, you focus on the product, the happiness it will bring you and how you can get it. All this prevents you from seeing behind the ad.

If you look at the world as a producer does, a lot of useful information about marketing and product design will be revealed to you. You'll understand which features make a product successful, and the strategy used to sell it.

It's even better to look for information on how companies produce a product, where they manufacture it and what their revenue model is. Just doing this can teach you a whole lot about different business strategies.

The only way to get rich quickly is to start your own business, and seeing the world through a producer's eyes will help you do just that.

As you now know, the traditional get-rich methods aren't reliable. As long as you have an employer, you won't be in a position to become very rich. Since you can't rely on winning the lottery next week, the only real option is to start your own business. Then when you're more productive, _you_ get more income!

So, to begin your successful business, you need to have the strategy and the knowledge of a producer.

### 8. If you want to be a millionaire, forget the saying “do what you love.” 

You've probably heard this multiple times: find a way to cash in on what you love and the money will follow! But the truth is, customers couldn't care less about your passions. All they want to know is what your business can do for _them_. 

As the German proverb goes: the worm must be tasty to the fish, not to the fisherman.

People will purchase your product only if it meets their needs or adds value to their life. Most of us are motivated by our own interests, _not_ by the desire to make others happy. Therefore, if you want a potential customer's money, you need to offer them something desirable in exchange. This means a product that solves a problem, meets a need or enables them to feel better in some way.

In a crime-ridden neighborhood, for instance, this product could be safety locks or window braces, not glamorous handbags or organic cheese.

If you really want to make doing what you love into a business, that's fine, but you better be _exceptional_ at it if you want to stand out from the competition. Because whatever it is you love to do, it's likely you won't be alone.

Just think about the amount of people who desire to be famous singers, painters, actors or other kinds of artists.

If you're convinced your hobby should become your profession, be prepared. For example, if you want to be a professional actor, you'll need to be among the handful of people who get offered role after role, or work for a theater company. To get to this stage requires _years_ of training, experience and dedication.

Remember, this commitment and excellence applies to all passions, not just the arts. If you wish to produce computers, for example, you'll need to do it better than Apple, HP and all the other top brands on the market.

### 9. A business that’s worth entering can’t be entered without effort and isn’t open to everyone. 

Imagine you hear about an awesome new internet business. Your friends are raving about it and there are already several bestsellers telling everyone how they can get rich from it. It seems like a safe bet, so would you go for it? After all, everyone is sure it'll pay off!

Before you dive in, you should be aware that the easier it is to go down a business road, the less effective this road will be at making you rich.

If anyone can enter a business, competition is going to skyrocket. Much like those passions so many of us love (mentioned in the previous blink), if it's overly competitive, there's too much supply, and if there's too much supply, the prices will plummet.

So to get rich in a business situation like this, you need to be extraordinarily good at what you're doing.

Hopefully you know that starting a business takes effort, so if it seems like a breeze to join a business, alarm bells should be ringing in your head.

Starting a business is a series of actions and choices. Even a simple business like a bed-and-breakfast in Napa Valley means you need to seek out, finance and renovate a suitable property, insure it, get the official permits and paperwork done, all before you even start thinking about hiring staff and everything else that comes later.

Also be aware that some founders of network marketing companies promise you can join simply by purchasing a starter kit and filling out an application. Again, you should treat this with suspicion, as you'll probably be joining a business that's not worth anything. Rather than ending up an entrepreneur, you may actually end up a paying client of the real entrepreneur: the network marketing company!

### 10. Final summary 

The key message in this book:

**A lot of us try to safeguard our future the traditional way: find a good job, work hard for about 40 years and save everything we can for retirement. Yet this way leads to lifelong servitude and there's no guarantee you'll end up rich. There's another way, though: pinpoint a good market niche, work out a business plan and take a few years of effort to build a system that generates income, even when you're not working.**

Actionable advice:

**The next time you're bored, learn something new.**

The next time you're bored, use the time to brainstorm or soak up ideas for a product. Listen to some talkback radio while you're stuck in a traffic jam, or read an article in a tech or art magazine while you're waiting at the doctor's office.

**Study your new purchase.**

Look at the latest product or gadget you bought from a producer's perspective. What are its selling points and features? How is it advertised and how would you describe the target customer? Considering these questions will help you get into the mindset of creating something you can sell and maybe even identify a market niche.

**Suggested further reading:** ** _I Will Teach You To Be Rich_** **by Ramit Seth**

_I Will Teach You To Be Rich_ takes a straight-talking and amusingly cocky approach to smart banking, saving, spending and investing. You don't need to be an expert to become rich, you just need to have a plan and know a few tricks. Sethi will teach you the benefits of saving as early as possible and setting up automatic investments so you can sit back and let your money work for you.
---

### MJ DeMarco

MJ DeMarco is an entrepreneur and founder of Limo.com, the company that made him a multimillionaire by the time he was 33. He is now the founder of Viperion Corp., an online and print content distribution company.

