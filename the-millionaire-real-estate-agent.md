---
id: 57bb4a45003a4b0003f1a856
slug: the-millionaire-real-estate-agent-en
published_date: 2016-08-25T00:00:00.000+00:00
author: Gary Keller with Dave Jenks and Jay Papasan
title: The Millionaire Real Estate Agent
subtitle: It's Not About the Money
main_color: EF3830
text_color: B23833
---

# The Millionaire Real Estate Agent

_It's Not About the Money_

**Gary Keller with Dave Jenks and Jay Papasan**

_The Millionaire Real Estate Agent_ (2004) is your guide to success in the real estate business. These blinks detail practical strategies for building a real estate empire from the ground up and keeping it profitable.

---
### 1. What’s in it for me? Become the successful real estate agent you were meant to be! 

If you've ever bought or sold a home, you've probably dealt with a real estate agent. How was that experience? Did you get a sad sack who couldn't convince a man on fire to buy a bucket of water? Or perhaps you had a smooth operator with a glistening smile and a $3,000 suit?

As with most sales roles, a winning personality and the ability to convince others are skills necessary to close a real estate deal. However, it takes far more than that to create a successful, sustainable real estate business.

As you'll see in these blinks, by setting the right goals, knowing what motivates you and learning a few tricks, you can turn a fledgling real estate business into a true empire.

In these blinks, you'll learn

  * why you need more sales leads than you think;

  * what the four-minute mile has to do with being a real estate agent; and

  * how Dr. Joyce Brothers finally got on television.

### 2. Becoming a better agent means understanding the how, what and why of motivation. 

When faced with a big new project, most people are so driven to get to work that they fail to realize how important good planning is to reaching the desired outcome. To find long-term focus and determination, you need to understand _why_ you want to do something.

This is the secret weapon of all high-achievers: they define a motivating purpose to help them maintain perfect focus. You can witness the power of purpose for example on the last day of work before you leave for vacation, when you probably complete much more work than usual. This drive is due to your new purpose — to wrap up loose ends and hit the beach!

But every day isn't the day before your vacation. To succeed, you'll need something that gives you _constant_ purpose.

And when it comes to finding that constant purpose, _intrinsic motivators_, such as the drive to always be a better person, are better than _extrinsic motivators_, such as trying to earn a certain amount of money. Extrinsic motivators are goals that can be reached, but when they are, the motivation disappears. So think about what intrinsic motivator you can focus on.

Once you're focused, it's time to set goals. It's important to be ambitious here. After all, failing to reach a huge goal is better than falling short of a small one. This is why we teach children to dream big and that anything is possible. We shouldn't tell ourselves anything different!

So instead of progressively increasing the size of your goals, set a major goal straight away and let smaller milestones naturally form the steps along your journey to success.

For instance, if your interim goal is to close $5 million in property sales, after which you shoot for $10 million, you might feel satisfied after attaining that first goal and lose motivation to achieve the second. This is why it's better to aim for a major goal like $80 million and use the smaller milestones as inspiration to keep working at it.

### 3. A strong real estate strategy works from the ground up, and to succeed, you need leads, listings and leverage. 

The success of all realtors, whether bullish and business-savvy or humble and obliging, depends on their ability to pursue three key objectives: _leads_, _listings_ and _leverage_.

The real estate business is a numbers game, which means to increase sales, you first need to maximize the number of leads you get.

If you look at the process of selling property in reverse, there are no contracts without listings, and there are no listings without leads. Everything boils down to _lead generation_, since leads ensure that you can keep doing business!

After all, an excess of leads means you can pick the best ones and focus on converting those leads to listings. On the other hand, too few leads will eventually mean lagging sales and failure.

So once you have enough leads, you can move on to the next goal: _listings._ It's to your advantage to advertise your services mostly to people looking to sell a home, rather than those looking to buy one.

This is because doing so will give you greater exposure. Just think of all the prospective buyers who come to viewings and are exposed to your brand, possibly becoming customers later on. Essentially, listing for sellers is a way of getting free marketing to produce further leads and listings.

And finally, by _leveraging_ people, systems and tools, you can cut down your workload while maximizing income. For example, you can start employing other real estate agents and take a commission of the deals they close. Or you can make your job easier by hiring an administrator to manage your office and lead database, as well as direct your advertising and social media efforts. This will leave you with more time to focus on sales.

Now that you know the strategies behind generating leads, listings and leverage, it's time to learn how to execute these strategies effectively.

> _"Don't be afraid to take a big step if one is indicated. You can't cross a chasm in two small steps."_ — David Lloyd George, former British Prime Minister

### 4. Using quantitative tools effectively can help you put your strategy into practice. 

So far we've touched on the key aspects of running a real estate business, but now let's look at four important tools that will let you grow your business successfully and sustainably.

Let's start by introducing the first two of these tools.

First is the _economic model_, an analytical tool that calculates the numbers you need to hit to reach your target income.

In other words, once you've decided how much money you'd like to earn, the economic model helps you use records and statistics to determine how many leads you need to generate to reach this goal.

For instance, if you want to earn revenues of $200,000, and you charge a 2.5-percent commission, you'll need to sell $8 million worth of property. If your average closing price is $400,000, you'll know that you have to sell 20 properties to meet your target. From there, knowing that your listings-to-sales conversion rate is 25 percent, the tool will tell you that you need to list 80 properties to sell 20.

The final step is to calculate the required number of leads. If you have a lead-to-listings conversion rate of 10 percent, you'll have to generate 800 leads to reach your goal.

That's the economic model, and it's great in helping you determine from the start how many leads you'll need to meet your financial goals.

The _lead generation model_, our second tool, is the one that will help you secure those leads so you can acquire and maintain clients. How does it work?

Good lead generation efforts involve frequent contact with prospective clients, but not so frequent that your efforts become annoying. When advertising to people that you have not had contact with yet, for instance via telemarketing or cold mailings, you can be fairly aggressive, contacting them up to 12 times yearly. With people you have met face-to-face, you can go into higher gear, reaching out to them eight times over the eight weeks that follow the meeting, whether by writing them a letter or sending them a fridge magnet or calendar as a gift.

And for those people you know well, you should aim to communicate with them 33 times a year, sending birthday cards, personal emails, other real estate listings and general reports about the real estate market.

These two tools, the economic model and the lead generation model, are powerful on their own. Next, we'll learn about the two other tools which complete our real estate success package.

### 5. Using the budget and organizational models can help you maximize your strategic potential. 

In the previous blink, we introduced the first two tools that can help you put your strategy into action. Now let's examine the other two, which define spending limits and maximize organizational efficiency.

The third tool, the _budget model_, works by preventing you from using more money than you make. As soon as possible, you should begin limiting yourself to only spending money that your business generates. This might slow down growth, but just as it takes time for a concrete foundation to set, patience in these early days will provide you with a much stronger base going forward.

On the other hand, if you don't curb spending early, the future of your business will be far less secure. Just consider the dot-com boom. Expectations of rapid growth caused startups to make huge capital investments — yet once the companies' true valuations came to light, all that money was lost.

So how do you know when it's ok to spend more money to pursue an opportunity? Think about it like a set of traffic lights: when you see a chance to increase your revenues, you have a green light and can increase your spending. But then, the light turns red, and you have to wait until you hit the revenue goal that justified the additional spending. Only then will the light turn green again.

Finally, the _organizational model_ will help you employ skilled people and free up your time to concentrate on the more important parts of your business. The first person you hire should be someone to handle administrative tasks, letting you focus on selling property.

When hiring it's important to remember that you get what you pay for — and it's worth offering a higher wage to find and secure a talented, motivated employee. Think of it this way: if you want to employ a person asking for an annual salary of $20,000 more than you're offering, consider that the employee only needs to generate an extra $2,000 per month in revenue to cover that increase. This is probably easily justified if the employee frees you up to make more sales.

Know, however, that these tools are just templates. Your business, of course, will change as it grows, but if you start out using these basic tools, you'll make the road to success just that much smoother.

### 6. The four models will help you pursue leads, listings and leverage. 

Once your business is established, your main goal will be to perfect your strategies in dealing with _leads_, _listings_ and _leverage_.

Remember again that you're in the business of generating leads — they form the backbone of your real estate business! When marketing your business, yourself and your listings, all your efforts should be directed at generating new leads. This is where a well-defined lead-generation model will help.

But naturally, leads are only good if they can be converted into listings — the all-important opportunities to make sales and maximize your business exposure. To ensure you get enough listings, track the economic model monthly to stay abreast of how many listings you need to reach your budgetary goals and how many leads this means. If you don't, you won't know what you need to do every day to earn you ideal income! It would be like traveling to a city you've never been to before without a map.

You also need to define an organizational model to give you more leverage by allowing you to focus on what's important.

Your organizational model should be based on the needs of your business not only today but what they might be in the future. You need to consider whether to hire a person who can adapt to changes over time, or who instead has the specific skills to fit an immediate need.

Last but not least, you must use the budget model to ensure your spending doesn't exceed your earnings in the long term.

In general, it's important to remember that implementing the four models will take effort and discipline — yet your journey doesn't end once these models are in place. Your road toward continual self-improvement is paved by the perpetual determination to modify and improve on these models.

> _"The greatest thing in this world is not so much where we are, but in what direction we are moving."_ — Oliver Wendell Holmes

### 7. Accepting failure is a crucial milestone in the constant pursuit of personal success. 

Now that you've mastered these practical tools, it's time to get personal — and that means talking about the fear of failure.

As humans, we are all naturally fearful — but when it comes to business, you need to let go of your fear of failure.

In most cases, you'll fail before you succeed. Only through perseverance will you make it far enough to see your hard work come to fruition. So instead of seeing failure as a negative, you should embrace failure as a necessary part of reaching the goals you want.

Consider the example of a certain US congressman who went bankrupt not once but twice, lost a bid not only for a legislative seat but also failed in two congressional campaigns, two senatorial campaigns and a race to become vice president.

This loser's name was Abraham Lincoln, which goes to show how crucial perseverance is.

It's important to accept failure, but you also need to conquer the beliefs that hold you back, especially if those beliefs are based on a falsehood. For example, people used to think it was impossible to run a four-minute mile.

But then, on May 6, 1954, Roger Bannister succeeded in this feat. And after the physical and psychological barrier of the four-minute mile was broken, it took a mere two months for Bannister's record to be surpassed! The false belief of an unbreakable barrier had been holding everyone back.

Whether on the track or in real estate, success is about visualizing yourself as a victor, and the truly great individuals can do this without someone else leading the way.

Great real estate agents also know that they can never rest on their laurels. When you pursue personal success, there's no finish line.

### 8. Work on the business instead of in the business, and you can earn plenty of income with little work. 

Even the most skilled agents have limits to what they can charge for their time, but there's no limit to how successful your real estate business can become.

No matter how streamlined your sales process, how many deals you close or how big these deals become, your most valuable resource will always be _your time_.

When you decide that you want your business to generate more passive income for you, it's time then to work on your business, not on making more sales yourself.

For instance, when you reach a point where you no longer want to work, a passive income of $100,000 a year might suffice, and attaining it can be simple.

Here's how you can do it.

If currently you're earning say $200,000 per year, you can pay a business manager half this sum to do your job while enjoying a steady stream of income with little active work.

This dynamic is exemplified by one of the authors himself. As a teenager, Gary Keller charged $15 to mow lawns in his neighborhood. As his business grew, he hired friends to help out, paying them $10 per lawn and keeping the remaining $5 without lifting a finger.

It's even possible to earn millions of dollars in passive income by creating multiple businesses and leveraging your time by hiring others to run those businesses. As a successful real estate agent, you'll be able to watch the figures generated by the four models, define your target passive income and determine how many agencies you'll need to reach your target.

At that scale, your organizational model will include managers, administrators and people specialized in helping both sellers and buyers in a single business unit that is then replicated across multiple entities.

Once you've made the decision to switch to a passive income, your new focus will be accountability — for your decisions and how these decisions affect your business at large.

### 9. Long-term success, regardless of your personal goals, relies on a high-level of focus. 

Why is it that, even after receiving the same exact training, two people will produce entirely different results?

A major difference between people who are successful and people who are _highly_ successful is how much they focus their energy and in which direction they choose to focus it in.

In business, it doesn't make sense for a manager to focus on everything. Rather, you need to focus on what's important.

It's also key to keep the _Pareto principle_ in mind: 80 percent of your success comes from the top 20 percent of your resources.

So when you're getting started in the real estate business, you'll probably have to work with every listing you can get just to keep yourself afloat. But once your business starts to thrive, you can start being pickier about which leads you pursue.

Don't spread yourself too thin. Instead, focus on your biggest leads and let other agents handle the smaller ones. As a business owner, you'll still make a profit from your agent's work and be able to spend your time more wisely.

The key lesson is to focus all your energy on your goals and not to be deterred by failure.

For instance, the famous Dr. Joyce Brothers applied to be a contestant on a game show but was rejected because, being a homemaker and psychologist, she wasn't considered "interesting" enough.

However, instead of giving up on her dream, she stayed focused and spent a year learning about boxing.

She tried again and was accepted to the show, as now she was considered an interesting candidate. She went on to win the show's grand prize and then leveraged her fame into a successful career as a celebrity writer and psychologist.

Success comes in all shapes and forms, but all successful people have one thing in common: an intense level of focus.

### 10. Final summary 

The key message in this book:

**Success in real estate boils down to leads, listings and leveraging resources. Once you've set up your business using these principles, you can step back and change your focus, growing your business while nurturing your agents to earn even more money for you.**

Actionable advice:

**Plan the finances of your real estate future from the start.**

When getting started in real estate, begin by determining how much money you want to earn. Then use the economic model to understand exactly how many deals, listings and leads you'll need to reach your goal. By following this strategy you'll be able to set tangible goals right off the bat!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Zillow Talk_** **by Spencer Rascoff and Stan Humphries**

_Zillow Talk: The New Rules of Real Estate_ (2015) gives the reader all the tools they need to buy, sell or rent a home. From the conundrum of whether to rent or buy, to when to sell and how to boost the value of your property, these blinks shed light on the perennially important dilemmas of real estate — the biggest investment of your life.
---

### Gary Keller with Dave Jenks and Jay Papasan

Gary Keller is an author and public speaker who cofounded the largest real estate franchise in North America.

