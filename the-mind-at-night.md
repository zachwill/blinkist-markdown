---
id: 590e5ae9b238e1000792cb14
slug: the-mind-at-night-en
published_date: 2017-05-09T00:00:00.000+00:00
author: Andrea Rock
title: The Mind at Night
subtitle: The New Science of How and Why We Dream
main_color: 161F70
text_color: 161F70
---

# The Mind at Night

_The New Science of How and Why We Dream_

**Andrea Rock**

_The Mind at Night_ (2004) provides valuable insight into the mystery of the dream world. Find out exactly how busy our minds are while we slumber, and discover the many physiological, psychological and evolutionary advantages that dreaming gives us.

---
### 1. What’s in it for me? Learn the true importance of sleep. 

Many of us pulled all-nighters in college while cramming for exams or writing a last-minute paper — and, probably, it was no big deal. As we get older, though, it becomes more obvious how devastating a messed-up sleep schedule can be. Indeed, a sleepless night often leaves us feeling frazzled, sad and slightly out of control.

But why is this, exactly? What does our brain do while we're asleep? You might be surprised to learn that, far from being turned off, it's as active as when we're awake, and that sleep and dreams are an essential part of our survival and self-understanding.

In these blinks, you'll learn

  * what depressed people's dreams are like;

  * that dreams provide insights into real problems; and

  * how to train yourself to fly.

### 2. We journey through a cycle of five stages of sleep every night. 

Considering that we spend a third of our life asleep, it makes sense to know about what's going on when we're sleeping.

It all starts with a period of pre-sleep, when our mind settles down, stops trying to make decisions and plans, and enters an almost meditative state to prepare the mind for sleep.

From there, we enter light sleep, which covers the first two stages.

During the first stage, or _sleep onset_ as it's known, we often see flashes of disjointed images, known as _hypnagogic imagery_. This is the brain's way of sorting through the day's experiences and deciding what to trash and what to stash. Some things get forgotten, while others are deemed important enough to store in our long-term memory.

Then stage two begins and our brain winds down further to make way for the stages of deep sleep that follow.

These deep third and fourth sleep stages are characterized by slow brain waves, and we generally cycle through the first stages once again before arriving at the fifth and final stage, which is marked by _REM_ — or _rapid eye movement_ — sleep.

It takes between fifty and seventy minutes to get through the first four sleep stages, and the REM period can be as short as ten minutes. So, altogether, the process lasts about ninety minutes, with the combined stages of deep sleep and REM usually accounting for a quarter of our total sleep during any given night.

While all five stages have their own important functions, it's at the fifth REM stage when we experience our most vivid dreams and when our mind goes through its internal processes.

In the blinks that follow, we'll take a closer look at how important these dreams have been and continue to be.

### 3. The unique characteristics of REM sleep are brought on by physiological changes in the brain. 

Dreaming can occur at any time while we're asleep — but dreams are at their most intense during REM because our emotions also become involved at this stage.

In this fifth stage of sleep, the limbic system, which is home to our emotional memory, becomes 15 percent _more_ active than when we're awake.

The most active part of the limbic system is the _anterior cingulate gyrus_, which some scientists consider to be directly related to our consciousness and sense of free will and self-awareness. Another vital part of the limbic system is the _hippocampus_, which connects our emotions with past memories in order to make new ones.

All of this is happening as, over the course of the night, the periods of REM grow longer, providing these areas of the brain with the necessary time to do their work.

The first period of REM can be as short as ten minutes, and our dreams will usually deal with current emotions. But the final REM periods can last nearly an hour and the narratives of these dreams will incorporate long-term memories.

During REM, our brain's prefrontal cortex is almost entirely shut off. This is why our dreams are often utterly bizarre; the prefrontal cortex is largely responsible for our logical reasoning.

The areas of the cortex that do function during sleep are connected to our _amygdala_, another part of the limbic system that controls our fight or flight instinct. Thankfully, due to the fact that we're more or less paralyzed during REM, we don't hurt ourselves by acting out our dreams.

But why is it important for us to dream in the first place? In the next blink, we'll take a closer look at how strongly dreams are connected to our survival.

### 4. Our ability to practice skills, and to be aware that we’re dreaming, has made dreams a valuable tool. 

Many people attempt to interpret their dreams and infuse them with meaning and significance. This can be a fruitful activity. But beyond potentially providing insight into our emotional lives, dreams also allow us to evolve as a species.

By enabling us to practice our survival skills, dreams have kept us learning and adapting, millennium after millennium.

One of the best examples is the recurring dream of being chased, which, no matter their circumstances or where they're from, people have had for centuries — indeed, ever since the first person had to run from a dangerous predator.

While most of us no longer have to worry about finding a tree in order to escape a charging tiger, such dreams still have benefits for getting us out of life-threatening situations.

This is because the human brain exhibits the same neural patterns in these dreams as it would when awake — which means that dreams function much like real-life practice, preparing us for real-life events.

Other species also have the ability to practice their skills during sleep, but humans have the advantage of being able to distinguish between dream and reality.

Without recognizing the difference, dreams actually make other animals vulnerable, such as a cat that dreams that the neighboring dog has moved away. It might wake up and go safely prancing into the yard next door, only to find the dog lying in wait, as usual.

We can thank our parents — and our ability to learn and comprehend — for our ability to differentiate between the dreamworld and the real world.

When we're children, we wake up from a nightmare crying, confused about an experience that seemed very real. Fortunately, our parents are there throughout our childhood to remind us that it wasn't real, that we were only dreaming — and, eventually, we learn to remind ourselves.

However, since the parents of animals don't have the ability to communicate as well as we do, they continue to believe their dreams are real, leaving them less protected.

In the next blink, we'll have a look at the other ways we can learn while we sleep.

### 5. Sleeping and dreaming is a time for us to continue learning and solving life’s problems. 

Sleep is generally considered one of the best ways to take a break from working, but, as far as our brains are concerned, sleep isn't time off.

When we fall asleep at night, it's time for the brain to get busy storing the information we've learned during the day in the memory bank.

In a paper published in 2001, MIT neuroscientist Matthew Wilson demonstrated this process in a lab experiment involving rats that continued learning how to navigate the pattern of a maze in their sleep.

By monitoring the rats' brain activity as they ran through a maze, and then monitoring it as they slept, researchers learned an interesting fact: the rats' brain functions while running through the maze were exactly the same during REM sleep. It was so precise that, while the rats dreamed, the scientists could see exactly where they "were" in the maze, and it was clear that they were still learning and trying to commit the right path to memory.

And it's the same for us. Even though we think we're resting our mind when we sleep, it's still firing the exact same neurons during REM as when we're awake. But in our case, the mind uses metaphors in dreams to help us solve our real-world problems.

A good example of this can be seen in an experiment conducted by William Dement, a prolific scientist who specializes in dreaming.

He asked his subjects to go to sleep while thinking about what the letters "HIJKLMNO" represent.

Upon waking, no one thought they had the answer, yet one subject mentioned that there was a lot of water imagery in his dream.

Dement then explained how the water imagery was the brain leading him to the answer, which was that HIJKLMNO are the letters H _to_ O, or H20, the chemical composition of water.

Dreams help us memorize things and draw connections, and are thus crucial to our intellectual growth. And, as we'll see next, they play a big part in our emotional growth as well.

### 6. Dreams serve as a form of self-therapy, but depression can keep this from happening. 

You don't have to interpret your dreams, or even remember them, in order to benefit from them. Simply dreaming keeps the mind emotionally healthy.

If you've ever experienced a psychotherapy session, you probably know that part of this treatment involves connecting a current negative emotion with a past event that led to a similar feeling. Since this is exactly what happens in dreams, dreaming could be seen as a form of self-therapy.

When our dreams connect negative emotions like fear and anxiety to our memories of difficult situations that worked out fine, they're trying to show us that, eventually, everything will be okay. We can take comfort in reminding ourselves that we've survived something similar in the past.

However, when we try to reflect on our dreams, they can often seem randomly disconnected, and you might wonder what someone from your past has to do with the present. But it's not random at all since our brain has labeled all our memories with certain emotions, enabling it to bring up the ones that are currently relevant.

By casting these memories in a positive light, our dreams give us confidence that we can overcome our current situation.

Sometimes our dreams malfunction, however, and this can lead, or contribute, to depression.

The average person will reach their longest period of REM sleep just before morning, and it will usually involve a vivid and exciting dream, full of past memories, to help us wake up in a good mood.

However, for those with depression, this is not the case. Nor is it the case that they suffer from sad or melancholic dreams. The dreams of the depressed are often like their waking mind — dull and bogged down by the weight of the world.

This is one way in which antidepressant medications can help; many of these medications keep people from experiencing REM sleep, which helps prevent their dreams from reinforcing their depression.

> _"A properly functioning dreaming system may actually be more effective than [some] forms of psychotherapy..."_

### 7. Dreams can be enlightening and inspiring, especially when you achieve lucid dreaming. 

Many artists have turned to their dreams for inspiration, and others were just lucky enough to remember a particularly striking moment.

Paul McCartney has often explained how shocked he was to have come up with the melody for the song "Yesterday" in a dream. At first, he believed he'd dreamed about it because he'd heard it elsewhere first while he was awake.

There are many stories like this. And the explanation is simple: our brain is at its most creative when it's dreaming.

At this time, both the barriers of logic and the sensory boundaries of reality are gone. Our brain is free to do whatever it can imagine.

While many artists, including musicians and painters, have gained inspiration from a dream, it's the visual artists who are especially prone to nocturnal epiphanies, since dreams are primarily a visual experience.

One way to really tap into the creative potential of dreams is to train yourself to experience _lucid dreaming_ — that is, to realize you're dreaming while you're dreaming, and to stay asleep.

If you can do this, you might be able to then take control of your dream and do things that you can't do while you're awake, like take to the sky and fly.

This isn't always possible, but there are a few techniques to increase your chances.

While you're awake, stop from time to time to ask yourself whether what you're seeing is reality or a dream. This will make it easier to raise this question in your dream without waking up, which is the key trigger to achieving lucidity.

You can also increase your chances by imagining yourself in a dream-like state before you fall asleep, this is said to make lucid dreaming 150 percent more likely, according scientist Stephen LaBerge.

To interact with your dream is to inhabit a fantasy world of your own creation, which not only makes for a fun time, but can also be a limitless source of creative inspiration. So never let anyone tell you that sleep is a waste of time!

### 8. Final summary 

The key message in this book:

**Dreaming is an incredibly important function of the human mind that has, in many different ways, helped us get to where we are as a species today. Although remembering your dreams isn't that important, knowing how and why you dream most certainly is.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Exploring the World of Lucid Dreaming_** **by Stephen LaBerge and Howard Rheingold**

_Exploring the World of Lucid Dreaming_ (1990) presents a step-by-step guide to the fascinating world of lucid dreams. It introduces various techniques on how to evoke lucidity and how lucid dreaming can be used to enrich your waking life.
---

### Andrea Rock

Andrea Rock is an investigative journalist who has won a number of awards, including the National Magazine Award and the Investigative Journalist and Editors Award.

