---
id: 57a8c1141c2e11000357faba
slug: the-mind-club-en
published_date: 2016-08-10T00:00:00.000+00:00
author: Daniel M. Wegner and Kurt Gray
title: The Mind Club
subtitle: Who Thinks, What Feels and Why It Matters
main_color: E44246
text_color: B23437
---

# The Mind Club

_Who Thinks, What Feels and Why It Matters_

**Daniel M. Wegner and Kurt Gray**

_The Mind Club_ (2016) is all about how we perceive beings as having a mind or not, and how this determination affects our moral judgments. These blinks explain what constitutes a mind as well as how and why we perceive minds the way we do.

---
### 1. What’s in it for me? Explore what it means to have a mind. 

Imagine this scenario: you're alone in a universe of mindless beings. Everyone around you only _appears_ to have a mind — even your mother is really a zombie who merely simulates the behavior of a conscious creature.

Although it's an outlandish idea, we can't unequivocally disprove this scenario. But, somehow, we're generally convinced that we're surrounded by conscious, sentient creatures. So, how do other people convince you that they have a mind? How do you decide whether your dog, your unborn child or your wonderful new computer belong to the Mind Club? And is this always a question of in or out, yes or no?

These are just some of the questions that will be discussed in these blinks.

In these blinks, you'll also learn

  * why some people can hit a CEO without being punished;

  * how the minds of God and Google are alike; and

  * the story of someone acquitted for murder thanks to his sleep lab results.

### 2. Minds are defined by two traits – but not all minds act the same way. 

You're part of a special club and you might not even know it. Chances are your neighbor and cat are as well, but your smartphone isn't — at least not yet. This club is called the _Mind Club_, and it's the group of all creatures considered to have a mind.

So, who qualifies?

Well, the authors conducted several studies in which participants were asked about the mental characteristics of various beings: a robot, a CEO, a family dog, a dead person and so on. They found that people generally attribute a mind to beings with two specific traits.

The first is _agency_, or the ability to think, act in a planned manner and control oneself. The second is the ability to _experience_ emotions like happiness, to be conscious and to feel physical sensations, like hunger.

It's that simple; if someone has these abilities, they're in the Mind Club. But once in the Mind Club, people can be characterized by the relative strength of their agency and experience.

For instance, if you're primarily characterized by your propensity for rational action, then you fit into the group of _thinking doers_. A prime example of this group are the CEOs of big corporations; they are generally considered to be thinking doers since they wield lots of power and have engaged in loads of planned action to get to where they are.

On the other side of the spectrum are the _vulnerable feelers_, those who primarily feel and experience, but are less apt when it comes to effective action.

Babies belong in this category. If they're threatened and experience fear, they can't plan to defend themselves and, therefore, their only response is to cry.

But these types aren't immutable. A CEO could turn into a vulnerable feeler if, say, an illness rendered him entirely helpless.

> _"A mind is not an objective fact as much as it is a gift given by the person who perceives it."_

### 3. We judge a person’s moral rights and responsibilities based on the mind they seem to have. 

Whether it's rich and poor or parent and child, certain concepts come in pairs and it's impossible to understand one without its counterpart.

And this is exactly the case with morality. We understand moral acts as involving two parties: the _moral agent_, or the person who engages in the action, and the _moral patient_, the person who receives the action.

This pairing is called _dyadic completion_ and here's how it works:

Whenever something or someone becomes a victim, such as by being hit by a car, or is the recipient of something good, like a gift, we automatically look for the person who inflicted the pain or offered the generosity — that is, the moral agent.

But how we judge moral acts doesn't depend solely on the deed. It's also influenced by the type of mind the doer and receiver appear to possess. Imagine the CEO of a powerful corporation punching a baby. It's unthinkable, totally outrageous and he'd surely be sent to prison.

But if the tables were turned and the baby punched the CEO, we would see it as cute or funny. The baby certainly wouldn't be blamed for its actions.

So, while the CEO did the exact same thing as the baby, we're more inclined to see him as a moral agent, since he's a thinking doer. In other words, we see him as acting consciously and in control of his actions. Therefore, we assume he deliberately inflicted pain on the baby and should be held responsible.

However, we rarely view vulnerable feelers as moral agents because we don't see them as being able to act deliberately. In other words, when the baby punches the CEO, we don't assume he's doing it to inflict pain.

And since vulnerable feelers are geared toward experience and not agency, we tend to see them as moral patients and focus on their moral rights to, say, not be hurt, rather than on their moral responsibilities.

Next, we'll see how when we want to inflict an inherently immoral act upon a person, we try to deny the existence of their mind.

> _"(Im)morality = Agency (of Agent) + Suffering (of Patient)"_

### 4. To ease the guilt of hurting others, people can convince themselves that certain people don’t think or feel. 

Have you ever wondered how a soldier might appear to be a genuinely kind person when he's at the park in his hometown, but then cruelly torture and kill civilians once he's shipped overseas? Well, it's largely due to a psychological phenomenon known as _dehumanization_, the process of denying that someone has a mind to validate the poor treatment we inflict upon them.

We do this to ease the guilt about the bad deed and numb ourselves to the pain we're inflicting on others. For instance, to do such barbaric acts to other humans, that soldier has to convince himself that the unfamiliar people he is suddenly faced with aren't human at all; he has to convince himself that they don't think or feel like him or those he grew up with. Once he's done that, he won't feel guilty about hurting them.

This is _why_ we dehumanize, but the process can happen in two different ways: _animalization_ and _mechanization_. These two strategies work because, as we know, we only perceive people as having a mind if they have agency and experience; dehumanization eliminates one or the other.

The first mode, animalization, is when a person convinces himself that another person isn't an agent like he is, but rather an unthinking feeler, someone who cannot make his own decisions and who therefore benefits from being controlled by another.

During the colonial era of the eighteenth and nineteenth centuries, white Europeans labelled African natives "savages," depicting them as immoral, weak-willed creatures and implying that they were doing the natives a favor by treating them like pets, housing, feeding and subduing them.

The other mode, mechanization, is just the opposite. It occurs when someone denies that the other has feelings and therefore perceives them as an extreme version of a thinking doer. This allows the moral agent to mistreat others because we empathize with someone only if we think they can feel. For instance, during World War II, US propaganda depicted the Japanese as evil people who never tire, continuously functioning like ruthless machines.

> _"One quote by Martin Luther… is revealing: 'But the Jews are a pernicious race, oppressing all men by their usury and rapine.'"_

### 5. We can’t be sure whether there’s intent behind an event, but there are upsides to assuming that there is. 

Just as it's impossible to say if your childhood cat died because it's what God wanted, when dealing with humans, we can't always tell if something was done by an intentional doer or an agent.

For instance, sleepwalkers might appear to be acting consciously, even in a planned manner; but in reality, they're fast asleep. Just take Kenneth Parks, the gentle young man who killed his mother-in-law while he was sleeping.

At first, the court couldn't believe that he'd been entirely unaware of what he was doing. But as sleep specialists testified about Parks' abnormal brain activity during sleep, his account gained plausibility and he was eventually acquitted.

So, on the face of it, we can't be sure whether any actions other than our own are the result of a conscious person acting intentionally. However, our chances of survival (and procreation) increase if we suspect intentional minds to be at work everywhere.

Say you're alone in the jungle and hear a rustling in the bushes. It's hard to tell whether it's caused by a hungry lion, creeping up on your camp, or just the wind blowing through the grass. But it's better for you to assume the first option is true. If you do and run away immediately, you'll be safe from the lion if it's there; if it's not, the worst you'll suffer is a bit of embarrassment.

There are plenty of other examples of how a compulsion to see minds everywhere helps us survive. But despite all the advantages of this tendency, it can also lead to paranoid delusions. For instance, someone might begin to suspect scheming minds or terrorists to be behind every outbreak of swine flu.

So, most people agree that adult humans have high levels of both agency and experience — in other words, that they have minds. People also tend to agree that denying such minds is immoral. But what about the minds out there that can only be perceived by some and not others?

> _"(...) Descartes (…) concluded that the only thing that was unquestionably true was that he existed, because he was thinking."_

### 6. People can disagree about whether someone has a mind but, ultimately, it’s our perception that decides. 

Unless your best friends are philosophers, they probably won't challenge the assumption that your older sister has a mind. But besides the more typical members of the Mind Club, like your sister, there's another, more divisive class. They're called the _cryptominds_ and they're only allowed entry by some gatekeepers.

People disagree about whether any given cryptomind has a mind at all. The group includes beings like God, animals and robots, along with dead and permanently unconscious people in persistent vegetative states.

And the same differences apply _within_ the category of cryptominds. For instance, there are predominantly thinking doers, like God and Google, both of which are immensely powerful and have tremendous agency, but are seen as unable to feel.

On the other end of the spectrum is the baby mouse, which appears as an extreme vulnerable feeler since it experiences hunger, pleasure and other feelings while not engaging in much deliberate action.

So how can we tell if a cryptomind truly has a mind? Well, simply put, something has a mind if we perceive it as having one.

This proof comes from the British mathematician Alan Turing who, in 1950, suggested the _Turing Test_ to determine whether a machine has a mind. While the test was created for machines, it has something to teach us about minds in general.

The Turing Test has a person exchange text messages with a human and a computer, and then asks the person to determine which of his conversation partners was the other human. Turing's argument was that if a computer can convince someone that it has a human mind, then it does, because a mind is real as long as we perceive it to be.

However, since the computer could easily convince some people and not others, its mind remains a matter of perception that could be real for one person, but not for another.

As such, there's only one mind that you can be certain you'll perceive: your own. It's the only mind that's real to you, which makes the Mind Club rather exclusive after all.

### 7. Final summary 

The key message in this book:

**The mind is a matter of perception, and the way we view another being's mind depends on whether or not we see them as having agency and the ability to experience. As a result, humans can disagree about whether someone has a mind, and our decisions can have major impacts on those people's lives.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Mindwise_** **by Nicholas Epley**

In _Mindwise,_ author Nicholas Epley looks at our ability to read the minds of other people, arguing that we believe ourselves to be far more adept at "mind reading" than we actually are. He reveals the common mistakes we make when trying to figure out what other people feel or want, and provides an entirely new perspective on how to handle both your own stereotypes and those of other people.
---

### Daniel M. Wegner and Kurt Gray

Daniel M. Wegner was an eminent psychologist, author of _The Illusion of Conscious Will_ and recipient of, among many other awards, the 2011 _Distinguished Scientific Contribution Award_ from the American Psychological Association.

Kurt Gray is a psychologist and a former student of Wegner's. He has written various essays and op-eds, including "The Myth of the Harmless Wrong" for _The New York Times_.

