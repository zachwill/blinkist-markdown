---
id: 56541a8a63626400072b0000
slug: the-mindful-athlete-en
published_date: 2015-11-25T00:00:00.000+00:00
author: George Mumford
title: The Mindful Athlete
subtitle: Secrets to Pure Performance
main_color: FA5947
text_color: BF4436
---

# The Mindful Athlete

_Secrets to Pure Performance_

**George Mumford**

_The Mindful Athlete_ (2015) explains how to unlock your hidden "superpowers" by practicing mindfulness. When you learn how to channel your own inner divinity, you'll reach your peak performance — in sports or in any other field.

---
### 1. What’s in it for me? Get the superpowers of mindfulness you need for top performance! 

Have you ever seen someone make a 90-yard touchdown run or complete the 100-meter dash in under 10 seconds? If you have, you'll know that such feats are things of beauty. They seem like things only a superhero could do — not the accomplishments of mere mortals. And these physical performances require more than just a well-trained body, so what enables these athletes to do what most people can't?

It's about training your mind and using its potential superpowers to reach top performance. But these techniques aren't limited to improving your athleticism. Whether at work, in school or just to improve life in general, we'd all like to enhance our performance. Using the concept of the mindful athlete as a jumping-off point, we'll explore the practices that'll get you in peak condition.

In these blinks, you'll discover

  * why NBA legend Michael Jordan sees his failures as key to his success;

  * how to handle monkey mind; and

  * why soccer player Zinedine Zidane headbutted an opponent in the 2006 World Cup final.

### 2. Sometimes you have to hit rock bottom before you can discover your superpowers. 

People find enlightenment in different ways. Some travel to India; others do yoga. For George Mumford, the author, it was the pain of hitting rock bottom that drove him to discover mindfulness and, as a result, his own _superpowers_. Here's his story:

In middle school, Mumford was a talented basketball player. He seemed poised for a professional career. And then he got injured while training. Instead of letting his body recover, however, he kept playing; this wore down his body, and ruined his shot at a career in professional sports.

So instead of playing for the NBA, he abandoned his dream and went to the University of Massachusetts, where he studied finance. Since childhood, he'd known only one way to deal with pain, whether emotional or physical: drown it in alcohol. To fight the chronic pain caused by his injuries, as well as the emotional pain caused by his compromised dreams, he began self-medicating. And his medicine of choice was Seagram's Seven whiskey. 

Mumford didn't smoke cigarettes or pot because he was concerned about how they would affect his physical growth, so when he started taking drugs, he went straight for heroin instead. 

In 1984, he got a severe staph infection. Mumford calls this his _Ass On Fire_ situation, or _AOF_. His AOF pushed him to finally make a change, so he joined his first twelve-step program: Alcoholics Anonymous. 

His AA program was where he was first introduced to _mindfulness_, which, in the '80s, was called "stress management." Through yoga and meditation, he learned to listen to his body instead of dulling his pain with drugs.

For years, Mumford continued practicing mindfulness at the Cambridge Insight Meditation Center and eventually left his job as a financial analyst to devote himself to teaching mindfulness to others. 

That's how Mumford came to develop the concept of the five _superpowers_ : _mindfulness_, _concentration, insight, right effort_ and _trust_. In the following blinks, we'll look at each one.

> _"I also came to realize that you couldn´t solve problems with the same consciousness that created them."_

### 3. Mindfulness, the key to high performance, is about focusing on your inner self. 

Imagine you're giving a presentation. You can't focus because you're worried about what the audience thinks of you. _Mindfulness_ would be the savior here. But how do you become mindful?

Mindfulness comes from within. Everyone has a quiet, inner strength that can protect them from external distractions.

Jon Kabat-Zinn, the godfather of mindfulness, said that mindfulness means paying attention to the present moment as if your life depended on it. 

Of course, that's easier said than done, because we're constantly surrounded by distractions. Our minds jump from topic to topic like a monkey swinging from branch to branch. 

Buddhists call this _monkey mind_. The monkey mind is hard to control, but you can pacify it by practicing Buddhism. And once you reach a high state of self-control, you'll find yourself in _the Zone_. 

In sports, the Zone is the ultimate experience; athletes enter it when performing at their highest possible level.

The psychologist Mihaly Csikszentmihalyi believes the Zone experience occurs when your skill and the situation's challenge are both high and equal to each other. The Zone is like the calm at the center of a storm. It's what keeps the mindful athlete in the present moment. 

So you need to be aware of your own thoughts and emotions. You can practice mindfulness meditation by sitting still, focusing on your breathing and practicing _bare awareness_ : staying aware of what's going on in your mind and body at the present moment. 

It's easy to get distracted while doing this. You might feel a breeze, for example, and recall a nice memory and start to dwell on it. 

You can avoid this by becoming a _Watcher_. Being a Watcher means _watching_ what's happening in your mind instead of letting it control you. Stay in charge of your thoughts. Don't let it be the other way around.

> _"Mindfulness is a practice not a destination."_

### 4. Concentrate by focusing only on your breathing. 

In the 2013 NBA playoffs, some camera people caught LeBron James sitting courtside with closed eyes, focusing on his breathing. _Concentrating_ on breathing like this is one of the most fundamental parts of practicing mindfulness. 

You can enter a state of relaxation by controlling your breathing. Think of the space between an inhalation and an exhalation as your inner center, where your Watcher watches everything. This kind of _Awareness of Breath_, or _AOB_, brings you back to the present moment.

Our breathing works in tandem with two other parts of our _autonomous nervous system_, both of which regulate our heart rate and other body functions. 

The first is the _sympathetic system_, which is activated by fear, anxiety and stress. It floods our body with stress hormones, increases our blood pressure and makes our breathing shallower. 

The second is the _parasympathetic system_. It releases the neurotransmitter acetylcholine, which lowers our heart rate and makes us more relaxed. And when you focus on your breathing, your parasympathetic system kicks into action. 

Conscious breathing can also get you into moments of _flow_. The easiest way to practice AOB is to sit down, close your eyes and concentrate on the air moving in and out of your lungs.

You can also lie down and do an _internal body scan_, where you imagine breathing through different parts of your body. 

You don't get into a state of flow by _stopping_ your concentration; you get into it by concentrating on as few stimuli as possible. Our brains generally focus on a number of things simultaneously. Reducing that number is what will get you into the Zone.

That's why LeBron James was focusing on his breathing: it allowed him to be in the Zone when he stepped back onto the court.

### 5. Insight is about understanding your own thoughts and the impact they have on your life. 

There are a lot of talented people in the world, but few of them reach their full potential. Why is that? Because they don't fully believe in themselves.

Most people aren't fully aware of the effect their beliefs have on their life. Our beliefs don't just exist in our heads, however: they manifest themselves as habits.

So, if you want to change your behavior, you have to think about your habits and the underlying thoughts behind them. In other words, you need to understand the _emotional blueprint_ your beliefs are founded upon. Here's another way to think about it: scrutinizing the thoughts behind your habits is like looking under the hood of your car, rather than just staring at the dashboard.

Everyone has a unique collection of emotional blueprints, which includes their insecurities and other negative emotions. It's important to be aware of these emotional blueprints, because the negative ones can build up over time and burst out in negative actions.

That's what happened when Zinedine Zidane, one of the most talented soccer players in history, lost his temper and headbutted Marco Materazzi in the 2006 World Cup.

Succumbing to negativity like Zidane did will only hinder your progress. Practicing mindfulness means letting go of who you think you are. So accept negative emotions like anger or resentment for what they truly are: fleeting distractions that shouldn't define you. 

Look at mistakes and failures this way, too. Your mistakes don't define who you are! And failures are just opportunities to learn and improve yourself.

Michael Jordan, one of the best basketball players in history, embraced this idea in his "failure" commercial for Nike. "I've missed more than 9,000 shots in my career," he said, "...lost almost 300 games. I've failed over and over again in my life. That's why I succeeded."

> _"There is no separation between who we are as people in the world at large and who we are as players on the court."_

### 6. The right effort is about focusing on the journey and letting the situation you’re in run its course. 

Are you familiar with the Greek myth of Sisyphus? Sisyphus, a deceitful king, was punished by the gods. They condemned him to push a boulder up a hill; when he reached the top, however, the boulder would always roll back down again, and he'd have to start over. He had to repeat this task for all eternity.

The gods punished Sisyphus by forcing him to focus on a future goal, which he was doomed never to achieve.

In order to avoid a Sisyphean fate, be sure that you put in the right kind of effort when journeying through life.

A person who puts in the right kind of effort is a sort of _spiritual warrior_. Bruce Lee is a famous example. Martial artists like Lee use their intuition to connect with the Zone. They focus on the journey, not the final destination. 

Carlos Castaneda, a famous anthropologist, once said that spiritual warriors don't earn their victories by beating their heads against the wall. They earn them by jumping over the wall instead.

So when your actions align with wholesome, positive thoughts like love, kindness, compassion and generosity, that's the right kind of effort for your path. 

Another important part of reaching your maximum performance is letting situations play out by themselves. Mindfulness is about knowing yourself and focusing on the present, but you have to forget yourself again and let the situation go if you want to reach your highest possible performance level. 

That doesn't mean you forget yourself completely — it just means you keep yourself at a distance from the situation and allow it to run its course. 

Shaun White, the famous snowboarder, did this when he won his gold medal in 2010 with a trick he'd never done before. He said he felt completely focused and, at the same time, detached from what was going on.

### 7. Unlock your full potential by trusting yourself and your inner divinity. 

When you hear the word "faith," you might think of religions like Christianity, Islam or Buddhism. Mindful athletes have another kind of faith, however: a faith in the self.

Faith is related to the concept of "God" — a word that means different things to different people. But the idea of God is really about trusting the divinity within yourself.

You have to understand the concept of God to master the last superpower. Anne Lamott, the famous author, said it doesn't matter what name you give to this idea. You can call it a "force" or even something random, like "Howard." The point is that it's the divine spark within yourself. It's beyond human comprehension. 

Mumford calls this inner divinity the _Buddha nature_. We all have it and we have the potential to awaken it. It doesn't come from the external environment; you find it by looking within.

That's why Sheldon Kopp titled his book _If You Meet a Buddha on the Road, Kill Him!_ He means that if you ever encounter someone who claims to be the Chosen One, he's really just a false god. 

Trusting in yourself instead of looking for guidance from others fosters your own self-efficacy. Mindfulness allows you to connect with higher forms of consciousness, and you can build a strong spiritual foundation by practicing it. And that, in turn, will keep you open to the unknown future, or whichever sport you play!

This openness is the ultimate form of self-empowerment. That's why the fifth superpower, _trust_, is about self-confidence and staying open to new ideas. 

When you live with full faith in yourself, mindfulness becomes your conviction. In the end, you'll fully believe in yourself, and know you can handle any challenge along your path.

### 8. Final summary 

The key message in this book:

**Focus on the present. Throughout the day, we're distracted by all kinds of stimuli, but when you narrow your focus and concentrate on your body, your breathing and your inner self, you can unlock your full potential and connect with higher levels of consciousness. Mindfulness is the key to reaching your peak performance — whether you're on or off the court.**

Actionable advice:

**Concentrate on only one thing for an entire minute.**

Focus on your breathing, your walking or what you want to accomplish today. It's harder than it seems. Once you manage to focus on only that one thing for an entire minute, try increasing your time to two minutes, then three. Narrowing your focus is key. 

**Suggested further reading:** ** _How Champions Think_** **by Dr. Bob Rotella and Bob Cullen**

_How Champions Think_ (2015) is a concise guide to the psychology behind success, as used by athletes and business professionals. Chock-full of fascinating examples and useful tricks, these blinks will set you on your path to achieving your maximum potential everyday.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### George Mumford

George Mumford is the sports psychology consultant for the Boston College men's basketball team and the meditation coach to the LA Lakers. A public speaker, he's shared his famous techniques with a number of NBA teams and coached athletes in other sports as well.

