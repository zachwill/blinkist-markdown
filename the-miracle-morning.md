---
id: 5404836e3663330008310000
slug: the-miracle-morning-en
published_date: 2014-09-02T00:00:00.000+00:00
author: Hal Elrod
title: The Miracle Morning
subtitle: The Not-So-Obvious Secret Guaranteed to Transform Your Life (Before 8 A.M.)
main_color: 4E82BC
text_color: 4B7099
---

# The Miracle Morning

_The Not-So-Obvious Secret Guaranteed to Transform Your Life (Before 8 A.M.)_

**Hal Elrod**

In _The_ _Miracle_ _Morning_, Hal Elrod explains the techniques that got him through the aftermath of a near-fatal car accident. Elrod realized that the key to a successful and fulfilling life was dedicating some time to improving ourselves every day. He details six crucial steps we can take every morning to help us jump-start our days and get us well on our way to a fulfilled life.

---
### 1. What’s in it for me? Find out how changing your morning can change your life. 

If you want a fulfilling and happy life, your morning routine is the place to start. Many successful people, multimillionaires, top managers and TV stars are early birds and probably get more done before you have even brewed your first coffee. But it's not just about waking up early. In _The_ _Miracle_ _Morning_, Hal Elrod explains the importance of creating a morning ritual consisting of six simple activities that you can utilize to start creating the life you've always wanted.

In these blinks, you will find out exactly which techniques you can employ to change your way of thinking and your daily habits in order to achieve your dreams and goals.

You will also discover

  * what professional athletes do to reach their goals;

  * why hitting the snooze button costs you more than you think; and

  * how to follow through with a healthy habit.

### 2. Many of us live a mediocre life, though we all have the potential to be successful. 

The average American wallows in $10,000 of debt, is overweight, doesn't like their job, has fewer than one person to call a close friend, and is mildly depressed.

Evidently, most Americans are traipsing through a life that falls far short of its potential.

The Social Security Administration says that if you select a hundred people at the beginning of their working life and observe them for 40 years, only _one_ will have become wealthy, four will be financially stable, five will need to continue working, 36 will have died, and 54 will lean on friends and family for financial support. These statistics paint a grim picture indeed.

Assuming that none of these people had planned to just "get by" throughout their life, this means that a shocking 95 percent of them aren't living the life they wanted for themselves.

Being financially successful is tied to a sense of freedom, as you don't have to worry about paying your bills or being trapped in debt.

Furthermore, studies show that more prescription drugs are being taken than ever before; one in two marriages in America are falling apart; Americans have more individual debt than ever before; obesity has become an epidemic; and heart disease and cancer are on the rise. However, despite all of this, we're capable of leading a successful and happy life; it's possible to turn things around.

The author, Hal Elrod, is a stunning example of this. Elrod actually _died_ for six minutes following a car accident. After spending several days in a coma, he awoke to doctors telling him that he had permanent brain damage and might not be able to walk again.

Yet he was able to recover.

He accepted his circumstances, instead of wasting time wishing that they were different, and was able to create the life of his dreams and fulfill his potential.

### 3. Rearview Mirror Syndrome and Isolating Incidences prevent people from reaching their full potential. 

Have you ever considered that the way you think about your life might be the very thing that is preventing you from fulfilling your potential?

The majority of us tend to make decisions based on our past and in doing so suffer from something known as _Rearview Mirror Syndrome_ (RMS).

If you're an RMS sufferer, you believe that who you used to be is who you are now, and your choices and decisions are informed by the limitations of your past experience. So when you come across new opportunities, you often turn them down on the grounds that you've never experienced them before.

For example, someone who can't commit to their partner because of failed relationships in the past probably suffers from RMS.

Aside from RMS, another reason we don't reach our potential lies in our habit of _Isolating Incidents_.

This means we treat events in our life as though they're disconnected from everything else — which is contrary to reality.

For example, perhaps you think it's perfectly fine to skip your workout today, since you can always do it tomorrow. But rather than your decision just affecting this moment in time, you're actually affecting the person you're becoming.

Author and businessman T. Harv Eker highlights the significance of this habit in his book _Secrets_ _of_ _the_ _Millionaire_ _Mind_ when he states, "How you do anything is how you do everything."

So, if you look at incidents in isolation, you end up being more lenient on yourself, and the events that you think of as mere exceptions will become the norm. As a result, your ability to achieve your dreams is stunted and you succeed only to the level which your excuses allow you.

If you want to create life on your own terms, you need to change the way you think about it. Dwelling on the past and making excuses will only hinder your success.

### 4. To start the day well, stop hitting the snooze button and change how you think about sleep. 

So maybe you're convinced that you need to change your life. But where do you start? Well, first a quick question — did you hit the snooze button this morning?

Many of us would be guilty of this. So what's the problem? Well, hitting the snooze button keeps us from waking with a sense of purpose. Each time you reach for that button, you are subconsciously saying to yourself that you don't want to rise to your life, your experiences and the day ahead.

Think about those who suffer from depression. For these people, mornings are often the hardest time of day. When you resist waking up, you reduce your chances of enjoying a satisfying day.

Conversely, if you wake up every morning with a purpose in mind, you'll be on your way to crafting a happy life.

Take a look at Oprah Winfrey, Bill Gates, Albert Einstein and even Aristotle. All of these famous people have one thing in common: getting up early!

If you find you're always sluggish in the morning, try changing the way you think about sleep.

Consider this — have you ever woken up completely drained on a special day? Probably not. If it's your birthday, wedding day or Christmas morning, then no matter if you got only a few short hours of sleep, you likely woke up full of energy and anticipation for the day ahead.

This is because the beliefs we hold about our sleep play a crucial role in how we feel when we wake up.

The problem with most of us is what Elrod found out for himself — when you go to sleep thinking "this is way too little sleep, I will feel exhausted tomorrow," you're spoiling your morning before you've even dozed off.

When he told himself that he would feel good in the morning, he felt refreshed with as little as four hours of sleep.

### 5. Change your morning routine to increase your Wake Up Motivation Level. 

So how do you stop wasting your life in slumber and start fulfilling your potential? One significant leap you can make is to change your morning routine by raising your _Wake Up Motivation Level_.

This level can be described on a scale of one to ten, where ten means that you're eager to rise and greet the day ahead, and one means you would rather just roll back into bed.

Fortunately, employing just a few basic techniques can make you more alert and give you more of a spark during your morning routine.

First, before heading to bed, you should affirm to yourself that you'll rise the next morning feeling refreshed. If you can get yourself to look forward to the next day somehow, waking up will be a lot less of a chore for you!

Next, place your alarm clock on the other side of the room to your bed. This of course means that you'll need to actually get up out of bed when it rings in the morning in order to turn it off.

Once you're out of bed and have switched off your alarm, head to the bathroom and brush your teeth. Doing this will give you a feeling of freshness and help you feel more awake first thing.

After this, go to the kitchen for a glass of water and drink it as fast as you can comfortably manage. This allows you to rehydrate after a night of losing water through breathing. It's important to note that dehydration can actually make you feel extremely tired.

By completing these simple steps, you'll wake up feeling brighter and prepared for the core practice of the Miracle Morning routine, which we'll turn to now.

> _"Your first thought in the morning is usually the last thought you had before bed."_

### 6. Practicing purposeful silence in the morning will help you fight stress. 

It's highly likely that you, like most of us, get stressed. One powerful way to reduce this is to use the first step of the Miracle Morning routine. This means carving out some time for _purposeful_ _silence_ after you wake up.

One example of purposeful silence is meditation — a popular technique that many people in high-pressure jobs use to combat stress.

The Huffington Post, for example, reported that Oprah Winfrey believed Transcendental Meditation® in particular helped her "connect with that which is God."

A lot of well-known people such as Sting, Jerry Seinfeld and Russell Simmons state that meditation is an essential part of their life.

So how can you introduce purposeful silence to your routine? Well, you can try the _Miracle Morning Meditation_.

Before you start, take a break from your worries and concentrate only on yourself. Find a quiet, comfortable place to sit, like a pillow or the couch, and sit cross-legged and upright.

Next, close your eyes or look forward at the floor in front of you. Bring your attention to your breath, inhaling through your nose and exhaling through your mouth. Breathe slowly and into your belly, not your chest. Then set a pace for your breath — breathe in for three seconds, then breathe out for three seconds. Try to push away any thoughts but if they come, simply refocus on your breath.

Though this may be difficult at first, it will gradually become easier if you practice every day. Bit by bit, you'll feel your stress levels reduce.

If you have already attempted such techniques and think it's not your thing, you can also find other methods to help you relax.

This could mean taking some time to reflect on your life and the things you're grateful for, or you may find that praying works better for you, if you're so inclined.

### 7. Use affirmation and visualization in the morning to start creating your ideal life. 

How do we go from a mediocre life to a life that we love and that truly fulfills us?

One way is to look at our self-talk. Our brains are subconsciously programmed by the way we talk to ourselves, but it's possible to alter this by using positive affirmations.

All of us have constant streams of thoughts in our head which are grounded in our previous experiences and they can work for or against us, depending on how we use them.

To turn some of your thoughts into positive affirmations, you can follow these steps: articulate and note down how you want your life to look in every area, clarify your motives by asking yourself _why_ you want what you want and ask yourself what you're committed to doing to get your life there, or even just to get to the next level.

After you have created your affirmation, be sure to read it out loud to yourself at least once a day.

Another important tool to have at your disposal to achieve success is one that many professional athletes use: visualization or _mental_ _rehearsal._

You, too, can employ this technique to visualize your ideal life, your dreams and goals.

For example, if you want to write a book, visualize yourself feeling inspired and joyfully writing page after page at your desk.

Your affirmations can help you visualize the answers when you ask yourself questions such as, "What do I want?" "Why do I want it?" and "What am I committed to doing in order to get there?"

Affirmation and visualization are powerful tools. Visualizing the life of your dreams and affirming to yourself the action you're willing to take to make it happen will change the perspective you have on your daily routine.

### 8. Morning exercise will keep your body healthy and spur your success. 

We often find that we're far too busy to exercise. It seems as though our lives are so packed with activities that we often feel totally depleted in the evenings. So why not try starting the day with some exercise to get it done first thing?

Exercise is vitally important to keeping our bodies healthy, so it's a smart idea to make room for it in our lives.

Too few of us manage to exercise during the day, even though we're all aware of the benefits, like a reduced risk of disease, and just _feeling_ _good_ because you're in shape.

The thing is, we also know that our days will be busy: last minute appointments get scheduled and important tasks can crop up. So often we just collapse on the couch at the end of the day, too exhausted to go for a run or workout.

Even though we know exercise is important, we frequently push it to the side. Yet making time for it as part of our morning can really aid our success.

For instance, when asked in an interview what the number one key to success was, self-made multimillionaire entrepreneur Eben Pagan replied: "Start every morning off with a personal success ritual." He then emphasized the value of morning exercise as part of this ritual, explaining how this gets his heart rate up, his blood pumping and his lungs filled with oxygen.

One way you can work out in the mornings is to follow a yoga DVD. The author makes time for 20 minutes of yoga every day by following an instructional DVD. This helps him focus, wakes him up and allows him to maintain increased levels of energy throughout his day.

> _"Morning exercise should be a staple in your daily rituals."_

### 9. Read and write in the morning to focus on personal growth. 

Once you've exercised, it's time to focus on your personal growth as the next stage of your Miracle Morning. Reading and writing are two significant activities that can help you reflect on your successes and move toward what you want out of life.

We can all find a little time to read, and reading books on personal development in the morning is one quick means of gaining insight from others who have experienced success.

There are books on all kinds of goals such as increasing your earnings, improving your relationships or building a business.

A good reading target to aim for is a minimum of ten pages per day. This means around ten to 20 minutes of reading per day, depending on how quickly you read. Surprisingly, this amounts to approximately 3,650 pages a year, which means you'll be reading around 18 books a year.

Furthermore, re-reading, highlighting or circling useful information makes it easier for you to recall what is of most value to you.

Next up is writing. Why should this be useful to us? Well, writing for just five to ten minutes every morning will accelerate your personal growth.

Writing down your thoughts, feelings and insights in particular can be highly beneficial.

For example, Elrod noticed that he was far happier and felt more gratitude for his life because his writing allowed him to focus on the things he had already achieved, as well as on the goals he wanted to reach in the future.

He did this by splitting a page into two columns entitled _Lessons_ _Learned_ and _New_ _Commitments_.

This helped him stop making the same mistakes, and committed him to the changes he wanted to make in his life.

By writing each day, you can review what you have learned, gain clarity on your problems and achievements, and acknowledge your progress along the way.

### 10. Customize your Miracle Morning to make it fit your own specific needs. 

Now that you've been introduced to six morning habits that can bring you fulfilment and success, it is important to learn how to tailor them to fit your needs.

You can take less or more time, depending on what is best for you. If you like, you can use 60 minutes in the morning to prepare yourself for the day and divide the hour in different ways. For instance, you could do ten minutes of each activity or you could workout for 30 minutes and dedicate five minutes to all the other activities, if you think you'd benefit more from the exercise.

Even a few minutes is better than nothing. So if you are stressed out about the thought of adding anything else to your schedule, just take six minutes and divide them like so:

Minute one — sit in silence; minute two — recite your affirmations; minute three — visualize your day going perfectly; minute four — note down some things to be grateful for and what you want to achieve during the day; minute five — read two pages of a book; and finally, minute six — do some push-ups and crunches.

To add even more freedom to your new routine, the time and place of your Miracle Morning can also be flexible. For example, the author travels a lot and often carries out his Miracle Morning on the fly, taking with him some yoga lessons as well as a book and journal. By doing so, he can sit down anywhere to meditate or pray, recite his affirmations and visualize his goals.

If you work night shifts, you could also do your Miracle Morning when you wake much later in the day. The only vital thing is that you have some specific, dedicated time for self improvement.

### 11. Make your Miracle Morning a new habit by having an accountability partner and committing to a 30-day challenge. 

Like any healthy habit, the Miracle Morning works best when you commit to it and do it regularly.

One effective way to make this happen is to seek out an accountability partner to help you stick to your commitment.

We've all had those days when we intended to go to the gym, but didn't because we just didn't _feel_ like it. However, if we had a friend there waiting for us, we would be far more motivated to show up.

We're more adherent when other people hold us accountable for our behavior. So find an accountability partner who wishes to do the Miracle Morning with you. That way you can both call each other to make sure you're on track. If finding someone is tricky, you could also join an online community.

We know that it takes around 30 days to form a habit, so you should be ready to dedicate yourself to the _30 Day Miracle Morning Transformation Challenge_.

The Transformation Challenge can be divided into three phases. The first ten days can be difficult and particularly hard to bear; the next ten days become easier, but still feel foreign and somewhat disagreeable. However, during the last ten days, your new habit will become part of your identity and you will start to enjoy it.

For example, the author initially despised running, but dedicated himself to running every day for 30 days straight.

In the first ten days, he wanted to give up. Each day he fought with his inner voice, who told him it was OK if he wanted to quit. But he didn't. During days 11 to 20, he no longer hated running, and it started feeling more normal to him. Finally, as the last phase rolled around, he felt that it had almost become enjoyable!

> _"Considering that our habits create our life, there is arguably no single skill that is more important for you to learn and master than controlling your habits."_

### 12. Final summary 

The key message in this book:

**The** **answer** **to** **a** **successful,** **fulfilling** **life** **lies** **in** **our** **morning** **ritual.** **Elrod** **encourages** **us** **to** **use** **six** **steps** **every** **morning** **to** **start** **living** **the** **life** **of** **our** **dreams:** **silence,** **affirmations,** **visualization,** **exercise,** **reading** **and** **writing.** **Completing** **these** **simple** **activities** **first** **thing** **every** **morning** **sets** **the** **tone** **for** **an** **effective,** **successful** **day** **ahead** **and,** **in** **turn,** **has** **a** **profoundly** **positive** **effect** **on** **our** **life** **in** **general.**

Actionable advice:

**Get** **happy** **about** **tomorrow!**

Before you go to sleep, think about what you could look forward to in the next morning. Are you meeting with a dear friend? Are you simply looking forward to a delicious breakfast? Finding reasons to be excited about the next day will make it easier to get up in the mornings.

**Find** **a** **Miracle** **Morning** **partner.**

Significantly increase your chances of making a lasting commitment to Miracle Mornings by having a buddy system — you and a partner support one another and hold each other accountable to your new morning habits.

**Suggested** **further** **reading:** **_The_** **_Power_** **_of_** **_Habit_** **by** **Charles** **Duhigg**

_The_ _Power_ _of_ _Habit_ explains how important a role habits play in our lives, from brushing our teeth to smoking to exercising, and how exactly those habits are formed. The research and anecdotes in _The_ _Power_ _of_ _Habit_ provide easy tips for changing habits both individually as well as in organizations. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Hal Elrod

After been declared dead for six minutes following a horrific car accident, Hal Elrod is a living example of triumph over adversity. Author of _Taking_ _Life_ _Head_ _On_ _(The_ _Hal_ _Elrod_ _Story):_ _How_ _To_ _Love_ _the_ _Life_ _You_ _Have_ _While_ _You_ _Create_ _the_ _Life_ _of_ _Your_ _Dreams_, Elrod is a best-selling author, international keynote speaker and one of America's top success coaches.

