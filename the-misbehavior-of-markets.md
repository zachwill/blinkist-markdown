---
id: 5462fd1463356600081e0000
slug: the-misbehavior-of-markets-en
published_date: 2014-11-12T00:00:00.000+00:00
author: Benoit Mandelbrot and Richard L. Hudson
title: The Misbehavior of Markets
subtitle: A Fractal View of Risk, Ruin and Reward
main_color: 353492
text_color: 353492
---

# The Misbehavior of Markets

_A Fractal View of Risk, Ruin and Reward_

**Benoit Mandelbrot and Richard L. Hudson**

The financial theories you learn about in school are coherent, neat, convenient — and wrong. In fact, they're so wrong that they might also be dangerous: in underestimating the risk of markets, we inadvertently set ourselves up for catastrophe. _The Misbehavior of Markets_ lays out the flaws of economic orthodoxy, and offers a novel alternative: fractal geometry.

---
### 1. What’s in it for me? Discover why economic orthodoxy has failed – and what we can do about it. 

Mainstream financial theories offer precise, comprehensive and self-contained models of financial markets. Yet, all around the world, those same financial markets occasionally undergo massive collapse that stays completely hidden from mainstream analysts until it's too late.

We only have to look back as far as the financial crisis of 2008 to know that markets are far more irregular and volatile than our current economic models would otherwise claim.

Luckily, there is an alternative approach, fractal geometry, that can be used to look for ways to reckon with the inherently _rough_ nature of changes in the market, rather than viewing these changes as anomalies or trying to force them into neat and convenient models.

In these blinks, you'll find out:

  * how conventional economic theories view us all as Lieutenant Commander Data;

  * what Romanesco broccoli and market trends have in common; and

  * how one company doubled its net capital by abandoning certain market principles.

### 2. We’re considerably less rational than mainstream theories of finance suppose. 

If you've ever seen _Star Trek_, then you surely remember the unforgettable android, Lieutenant Commander Data. As a completely rational being, Data often struggles to understand the strange behavior of his human crewmates.

According to dominant theories in finance, every investor can be seen as Data.

This is nothing new. In fact, the concept of the _homo economicus_, i.e., completely rational agents striving only to maximize their utility, was introduced by John Stuart Mill in the mid-nineteenth century, and since then it's been a mainstay in orthodox economic theory.

Mainstream theories of finance, like those of the Chicago School, contend that each individual will make the most profitable, obvious rational choice. With enough relevant information about a particular stock, there will be one choice which will yield the greatest profit, and this is what we'll choose.

For example, if the stocks of one Venezuelan bank perform better than competitors in the past month, then it seems like the best investment that a completely rational, self-interested and informed investor could make.

In reality, however, we don't always behave rationally — even investors. In part, our irrational behavior is due to our entirely human tendency to misinterpret information and misjudge probabilities.

Take this experiment, for example, in which participants were given the choice to either collect $100 immediately or flip a coin and win $200 for heads and nothing for tails. Unsurprisingly, most people opted to collect the free $100.

Then, the rules were altered: now, people had to choose between _paying_ $100 or flipping a coin and losing $200 for heads and nothing for tails. This time, most people decided to gamble.

Objectively, the potential wins and losses were the same, so any rational person should make the same choice under both conditions. But we're _irrational_, and so most people acted as if the odds for both games were different.

As we'll see, investors are not rational automatons. They misinterpret information, miscalculate probabilities and let emotions distort their decisions — just like everyone else!

> _"People simply do not think in terms of some theoretical utility measurable in dollars."_

### 3. The orthodox theories of finance wrongly assume that all investors follow the same strategy. 

Even if investors made rational choices all the time, would that mean that the same choice is rational for every investor? According to orthodox economic theories, yes.

Many of these orthodox theories are rooted in the works of notable economists, like those from the University of Chicago's Chicago School, a Neoclassical group that produced and attracted many Nobel laureates.

According to these orthodox theories of finance, all people will act in essentially the same way so long as they're in comparable situations. They presume that all people share the same investment goal, namely: make as much money as possible, just for the sake of it.

Additionally, they're all thought to have the same _time horizon_, the point at which they re-evaluate their investment decisions. For example, this could mean that they'll all hold their stocks for five years before deciding what to do with them.

However, in reality, people make very _different_ investment choices. For instance, investors have varied time horizons: some speculate on the internet and exchange stocks daily, while others buy stocks for their pension fund and hold on to them for decades.

They also follow vastly different strategies. For example, some are _growth investors_, who tend to buy stocks of companies that appear to be growing quickly in comparison to their competitors. Sometimes this even means paying somewhat higher prices or receiving no dividends for a period of time.

Others, in contrast, are _value investors_, preferring instead to invest in companies that are already mature, relatively stable and slow-but-steady growers. 

Obviously, mainstream economic theories are based on grossly oversimplified assumptions about human behavior. However, it's not uncommon for a scientific theory to simplify its subjects for the sake of clarity.

Yet, the question remains: does this kind of simplification create bad predictions about market behavior and, consequently, bad investments?

### 4. Contrary to widespread assumptions, prices jump significantly in value from one moment to the next. 

According to orthodox financial theory, prices don't jump — rather, they glide. The assumption is that when prices increase or decrease, they are bound by _normal distribution_, i.e., the tendency for variations to stay close to the mean. The farther a value is from the mean, the rarer it will be.

For example, body height follows normal distribution. In the US, men's heights cluster around a mean of 70 inches. In terms of percentages, 95 percent of American men are between 66 and 75 inches tall, and 98 percent between 64 and 76 inches. 

A normal distribution in prices means that the _majority_ of price changes will be small. Just like with extremely tall or short people, the correlation follows that the larger the spikes or plunges in price are, the less likely they are to occur.

From this trend, we can then assume that dynamic prices, such as stocks or foreign currency, won't jump in price, but rather follow a smooth, gradual change over time.

But prices _do_ jump! Thus, changes and price can't be considered normally distributed.

One reason for this is quite trivial: when for example currency brokers quote prices, they tend to round decimal values. So, if a currency's value moved from 4.2 to 4.8 (a difference of 0.6), brokers would say it jumped from four to five, thus overestimating the magnitude of the jump by 0.4.

Jumps in price can also be caused by _order imbalances_, in which the sell or the buy orders don't match, either when there's a large amount of buy orders with few people selling or the opposite, where many want to sell but few want to buy.

Order imbalances are often caused by big news. If the FDA approves a new life-saving drug, for example, hordes of people will rush to buy pharma stock, causing the price to spike.

> _"Discontinuity, far from being an anomaly... is an essential ingredient of markets that helps set finance apart from the natural sciences."_

### 5. Despite the assertions of orthodox economics, changes in price are not independent from each other. 

The math professors at the Sorbonne in Paris weren't thrilled when Louis Bachelier presented his doctoral thesis in 1900: rather than exploring something lofty and traditionally academic, like complex numbers, Bachelier worked out a probabilistic model analyzing the money-grabbing speculations at the Paris exchange in order to evaluate stock options.

Despite his professors' disappointment, Bachelier's model would become the very foundation of mainstream financial theory some 70 years later.

Bachelier's theory stated that prices move up and down randomly, with each variation being completely independent from the last.

To illustrate this, imagine that you want to invest, and the price of a certain stock has risen three quarters consecutively. Does that mean that it will continue to grow into the next quarter?

According to Bachelier, despite our historical information, we can't predict this any better than we could the outcome of a coin toss. If you toss a coin and get ten heads in a row, this tells you nothing about the outcome of your next toss: the odds of getting heads remain unchanged at 50 percent.

So, if what Bachelier suggests is true, then there should be no predictable pattern of price movements, just like there is no predictable pattern of heads or tails. 

Yet, several empirical studies have demonstrated that price movements aren't actually independent from one another. One such study was conducted by the economist Campbell Harvey, who found proof that prices indeed follow trends. That is to say, they are more likely to rise if they rose in the last month.

So what causes these trends? A trend might result, for instance, from major news leaks, like the FDA's approval of a novel drug. As people scramble to get in on the action before it's too late, the availability of the stock decreases, thus further lifting its price.

On the other hand, Harvey noticed that stocks that rose over a five-year stretch are increasingly likely to _fall_ in the next five years.

Clearly, the mainstream theories of finance are unconvincing. Our following blinks reveal how to describe economic development more accurately and cautiously.

### 6. Some things are naturally complex and “rough,” so the tools to explore them must be fit for roughness. 

In the past, scientists treated irregularities as exceptions to the rule — little more than meaningless aberrations from a perfect shape or graph. Think back to those normally distributed price movements: economists visualize them as adhering to a _smooth_ bell curve. Consequently, extreme changes in price — that deviate from the curve — are seen as simple anomalies.

However, many phenomena and processes in both markets and nature are inherently complex, irregular or _rough_, and not smooth like a bell curve.

For instance, when wind speeds increase, the air movement in the wind tunnel undulates forward and backward between periods of smooth flow and turbulence, with sudden gusts and complex swirls.

The same kind of turbulence seen in the wind tunnel is visible in financial markets, with sudden and extreme changes in the stock prices.

Mainstream financial theories try their best to impose a smooth understanding of market dynamics, but can't match up to reality. Theories that embrace the market's roughness would be more accurate and helpful.

In managing this roughness, we can draw inspiration from a kind of math called _fractal geometry_. "Fractals" — tracing their roots back to the Latin word _fractus_, meaning "broken" — exhibit a unique kind of regularity:

Many inherently "rough" — or fractal — structures, such as Romanesco broccoli, _appear_ irregular on the outside. However, if you change your perspective and zoom in on them, you find a complex kind of order: their patterns repeat themselves in smaller and smaller copies.

For instance, Romanesco broccoli appears as if it were made up of many tiny broccoli, which in turn seem to be made up of even smaller broccoli, and so on. This phenomenon is called a _self-similar pattern._

These fractal, or self-similar, patterns aren't just found in our favorite vegetables: you can also find them in wind turbulence and in the behavior of financial markets.

> _"[A fractal's] power comes from its unique ability to express a great deal of complicated, irregular data in a few simple formulae."_

### 7. The dynamics of the market are best described as a fractal phenomenon. 

In 1961 Harvard professor Houthakker was immensely frustrated: the New York cotton exchange had kept meticulous price records for a century, yet despite his best efforts, Houthakker simply couldn't fit the data on the cotton market into Bachelier's model.

The cotton market, far from being smooth, was extremely rough. In fact, financial records report enormous surges and plunges in price — far too many to be considered part of a normal distribution.

Not only did the prices change in huge leaps, the mean magnitude of those leaps also varied significantly from one period to another. In some years, cotton prices didn't change much at all, while in others, they varied extremely.

In other words, both the prices as well as the change in price saw immense variance. In the face of this data, Bachelier's model leaves us few means to cope. Fractal geometry, however, allows us to make sense of this roughness.

Data on the cotton market couldn't fit into Bachelier's model. Luckily, we can deal with these kinds of patterns by using the _power law_, used to understand statistical relationships. Different power laws are used to make sense of distributions in a wide range of phenomena — from the magnitudes of earthquakes to income disparity.

But what does the power law have to do with fractal geometry? Think back on the quality of self-similarity found in fractal patterns, as is the case for Romanesco broccoli.

Power laws share this property with fractal geometry, exhibiting a phenomenon called _scale invariance_, meaning that at any magnification of an object, there's a smaller piece of the object that's similar to the whole. Scale invariance is simply a more exact form of self-similarity.

So, if you, for instance, plot two log-log diagrams for cotton prices, one with the data set for a week and the other with data for a decade, you'll find out that both curves look roughly the same.

### 8. An adequate theory of real markets doesn’t measure time with a clock. 

Surely you've experienced variance in the passage of time, when it creeps along like molasses during a boring meeting yet flashes by in an instant when you're out with an old friend. In those cases, the way you experience time is wildly different from what it might say on a clock.

The same holds true for markets: time, as measured by clocks, is ill-suited as a tool for understanding market behavior.

As we've seen, it's difficult to find market patterns, as price movement can be distributed with great irregularity. Some days price changes are dramatic and numerous, while on others such changes are slight and scarce. In other words, sometimes there's a lot of change, or _information_, and other times there's very little.

However, despite the uneven distribution of information, we can find market patterns with the help of _trading time_ :

In fractal analysis, the same formulas apply to any scale: the proportions of price changes — but not the magnitudes — are always the same. So, it's immaterial whether you're analyzing changes over hours, weeks or years.

To find patterns in market behavior, it's helpful to stop defining intervals by clock time — i.e., days, months, years, etc. — and start defining them by amounts of information. For example, forty units of information, such as price movements, might constitute one interval for the sake of your calculations.

These intervals are defined irrespective of time, since one turbulent day could produce as much information as six quiet days. Looking at information this way distorts time, as some intervals are spread over, say, six days, others over just one day.

This distorted time, _trading time_, is just one of the tools that enable financial analysts to deal with the roughness of markets.

### 9. Today, some economists are implementing fractal geometry for their analyses. 

In theory, you could argue that fractal geometry is a better tool for describing markets than the standard theories of finance founded on Bachelier's model, normal distribution or concepts of human beings as _homo economicus_. Yet, as of today there's no comprehensive economic theory based on fractal mathematics.

Nevertheless, some labs, financial providers and consultant firms employ these models. For instance, the financial provider _Oanda_, who offers an online currency conversion platform in addition to information on foreign exchange, uses fractal analysis for its services.

All of the tick-by-tick data on Oanda — that is, the price changes displayed in real time — are analyzed with fractal geometry. In this way, it makes an interesting case study into the effectiveness of such analyses.

Another tool used by firms today is an advanced form of fractal geometry called _multifractal analysis_, which helps us to manage market heterogeneity, e.g., the different kinds of investors we discussed in a previous blink, as well as irregularly distributed price changes — and thus devise new trading strategies.

These fractal strategies seem to work for Oanda: in 2003 their net capital more than doubled!

Furthermore, France's largest hedge fund firm _Capital Fund Management_ uses fractal geometry in its trading strategies. While their strategy isn't based completely on fractal models, they do employ multifractal analysis for risk control and option pricing.

In addition, when they plan trades and portfolios, they use mathematical techniques derived from fractal analysis.

These techniques may have also benefitted Capital Fund Management. In 2002, when the market overall had fallen by a third, their largest fund reported stock-market gains of 28.1 percent.

Clearly, fractal geometry has a place in our contemporary understanding of economics. The next step is to develop these fractal models into new, comprehensive economic theory.

### 10. Final summary 

The key message in this book:

**For decades financial specialists have clung to orthodox understandings of market behavior. Yet, these rigid models vastly underestimate the true risk of markets. Thankfully, we have begun to implement mathematical tools that have helped us to cope with these volatile market risks.**

**Suggested** **further** **reading:** ** _Fooled by Randomness_** **by Nassim Nicholas Taleb**

_Fooled by Randomness_ is a collection of essays on the impact of randomness on financial markets and life itself. Through a mixture of statistics, psychology and philosophical reflection, the author outlines how randomness dominates the world.
---

### Benoit Mandelbrot and Richard L. Hudson

Benoit B. Mandelbrot was the world-famous inventor of fractal geometry as well as a Sterling Professor of Mathematical Sciences at Yale University and Fellow Emeritus at IBM's Thomas J. Watson Laboratory. Mandelbrot won numerous prizes, including the Wolf Prize for Physics, and wrote several books, such as the bestseller _The Fractal Geometry of Nature_.

Richard L.Hudson is a leading European science and technology journalist as well as former managing editor for _The Wall Street Journal Europe_. He is now CEO and Editor of the London-based media company _Science Business Publishing, Ltd._

