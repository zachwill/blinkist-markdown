---
id: 597f444ab238e100057035d9
slug: the-missionary-position-en
published_date: 2017-08-04T00:00:00.000+00:00
author: Christopher Hitchens
title: The Missionary Position
subtitle: Mother Teresa in Theory and Practice
main_color: 2DB2DF
text_color: 29738C
---

# The Missionary Position

_Mother Teresa in Theory and Practice_

**Christopher Hitchens**

_The Missionary Position_ (1995) tells the true story of the famous nun known as Mother Teresa. These blinks explain how a convincing yet false myth formed around this historic icon whose work and motivations weren't as noble as we've been led to believe.

---
### 1. What’s in it for me? Is it time to remove Mother Teresa’s halo? 

On September 4, 2016, Mother Teresa was made a saint by the Pope. The ceremony was witnessed by 1,500 homeless people from across Italy.

The fact that, nearly 20 years after her death, so many people were prepared to flock to her canonization shows the enduring appeal of Mother Teresa.

Mother Teresa's name is so synonymous with selflessness and virtuous altruism that it feels like blasphemy to even challenge her literal saintliness. But her holy status arguably makes it more important to closely examine her reputation — where did it come from? How true was it? And how did she benefit — or harm — others, because of it?

Let's take a critical look at Mother Teresa and her life's work.

In these blinks, you'll learn

  * how Kodak film was mistaken for a miracle;

  * why purity might be over-rated; and

  * how myth and martyr might be closer than you think.

### 2. Mother Teresa’s rise to media stardom was based on incorrect information. 

Have you ever heard of Agnes Bojaxhiu? Well, you might know her better by a name she adopted later in life: Mother Teresa.

She was born in Albania in 1910 to a religious Catholic family. At 18, she became a nun and, one year later, moved to India. By 1948, she was opening homes and hospices for the poor, the sick and the dying, first in Calcutta, then across the globe.

Teresa's work with the poor was the subject of widespread praise in the Western press throughout her life, but, upon closer inspection, it's clear that this fame rests on false premises. Here's how.

Her initial rise to international fame followed the release of a 1969 BBC documentary featuring her work. However, the film was based on the incorrect assumption that Calcutta was one of the vilest places on earth. This idea was propagated by the wide dissemination of images depicting people asleep on sidewalks and trudging through overcrowded streets.

The reality was very different. Calcutta was no worse than other cities of comparable size in India or other developing nations. It was crowded and poor, but it was also full of hardworking people, excellent universities and vibrant culture. Nonetheless, the film's depiction of the city as a living hell served to reinforce the saintly image of Teresa's work at what she called her "House for the Dying."

Not just that, but the same documentary depicted a supposed miracle, which was definitely not what it seemed. This so-called divine event occurred when a mysterious light, with no discernible source, appeared in a scene filmed in a darkened room at Teresa's House for the Dying.

The narrator of the film, Malcolm Muggeridge, was sure it was a divine light and the first recording of a miracle performed by Teresa. This story was widely spread by the media and rapidly launched Teresa into stardom.

Years later, the cameraman, Ken Macmillian, said that the light was simply caused by new Kodak film, but by then the miracle was firmly planted in the public conscience, and Teresa was a household name in the West.

### 3. Teresa’s work with Calcutta’s poor was not as noble as it seemed. 

While Teresa's work was being praised by the vast majority of the Western media, it's not clear whether her work was helping the poor at all.

A great deal of the medical care administered at her centers was of incredibly low quality.

For example, take the first-hand account of Dr. Robin Fox, the editor of elite medical journal _The Lancet_. He visited one of Teresa's Calcutta-based centers in 1994, and his description of the place was abominable. The scene he painted was more reminiscent of a shoddy aid tent on a war-torn battlefield than of an established medical center with 50 years of operation under its belt.

Only the most basic drugs, like paracetamol, were available — even for patients with dire conditions. More troublingly, the nuns were responsible for diagnosing conditions and were rarely authorized to transfer seriously ill patients to hospitals.

Similar accounts were described by Mary Loudon, a former volunteer at one of Teresa's Calcutta centers. Loudon compared it to a World War One clinic, with 50 to 60 people packed into a room on stretcher beds. The needles in the center were recycled and weren't even properly sterilized before use.

The truth is, Teresa's medical centers were more about fetishizing the poor than actually assisting them. This fact becomes even clearer and all the more strange when you learn that Teresa was raking in huge monetary donations.

One former acolyte reported that up to $50 million had been collected in just one of her organization's bank accounts. However, barely any of that money seemed to be put toward improving the lives of the people in Teresa's centers. She could easily have founded multiple medical clinics of the highest quality.

This suggests the real motive behind Teresa's centers: instead of helping people, she wanted to exploit their condition. She had a cult-like obsession with the suffering of poverty, and personally wished to emulate the pain of Jesus on the cross.

> _Ironically, Teresa herself was treated at some of the best hospitals in the world in her later life_.

### 4. Teresa’s work in the United States was much the same as that in Calcutta. 

So, Teresa wasn't all she was cracked up to be, but the testimony of Susan Shields, a nun who spent nearly ten years volunteering for Teresa, paints an even darker picture. Here's the story.

After acquiring a new center in San Francisco, Teresa ordered those below her to remove every sofa, mattress and carpet from the building since she considered them too materialistic. The house's huge, beautiful rooms were then filled with narrow stretchers that were unnecessarily crowded together to meet Teresa's picture of harsh austerity.

The place wasn't heated in the winter, and the cold weather even resulted in some of the nuns contracting tuberculosis. But that's not all that Shields witnessed.

She also recounts a time when plans were being made to launch a new center in an abandoned building in the Bronx, but city regulations required an elevator to be installed for any handicapped patients. Teresa vehemently refused the elevator because of her disdain for modern convenience and luxury, even after the city offered to cover the cost.

In the end, all the effort poured into planning the center came to nothing. The project died, simply because of Teresa's unwillingness to accept an elevator for the physically disabled.

Instead of serving the poor as she claimed, Teresa's centers were really just satisfying the rigid spiritual needs of her and her associates — and the feel-good aspirations of her donors.

> _"When the requirements of dogma clash with the needs of the poor, it is the latter which give way."_

### 5. Mother Teresa had a long history of associating with vile dictators. 

People in influential positions should ideally use their power to change the world for the better. But, as you're likely starting to understand, that's not the route Mother Teresa took. Not only did she do little to condemn oppressive regimes, in many ways, she also lent them credibility.

Consider her 1981 visit to Haiti. Rather than helping the poor, she gave credibility to a brutal and oppressive dictator. Jean-Claude Duvalier was running a despotic regime that saw thousands of Haitians murdered and tortured. He and his wife enjoyed an extravagant lifestyle in a country that suffered from some of the worst poverty in the world.

Rather than condemning this brutality, Teresa accepted an honor from the regime. She met and posed joyously with the Duvalier family and praised Jean-Claude's wife, Michele, for being so connected with the country's poor.

The regime milked Teresa's visit for all it was worth, broadcasting footage of her meeting with Michele and Jean-Claude for weeks, and Teresa never spoke out.

Even in her home country of Albania, Teresa did nothing to protest dictator Enver Hoxha. Hoxha, a Stalinist authoritarian, died in 1985 after ruling for 40 years, during which time he brutally repressed his country, prohibiting all religion, forcing the closures of mosques and churches, and subjecting a third of his citizens to interrogations and forced labor.

Yet in 1990, when Teresa finally visited her homeland, rather than speaking out against the anti-religious stance of the regime, she met with Hoxha's widow. She even visited Hoxha's grave to lay flowers on it.

### 6. Although Mother Teresa presented herself as beyond politics, her actions painted a different picture. 

Teresa often asserted that she was beyond politics, but her support of conservative policies in Spain says otherwise.

Following the death of right-wing dictator, Francisco Franco in 1975, Spain began to transition toward democracy. One part of this transition was the passage of legislation lifting restrictions on divorce, abortion and birth control.

To protest these changes, Teresa flew to Madrid, where she was greeted by a massive crowd of conservative activists. In other words, she claimed to be apolitical but was publicly standing in solidarity with the policies of right-wing Spanish politicians.

Unfortunately, that's not the only instance of such behavior. She did something similar on her 1988 visit to the United Kingdom.

Teresa's stated goal was to discuss the homelessness epidemic in London with then Prime Minister Margaret Thatcher, but the timing of her trip points to her ulterior motives. The day after Teresa's meeting with Thatcher, Britain's House of Commons was going to vote on a bill restricting access to abortions.

Thatcher and Teresa must have discussed abortion at length during their meeting, and proponents of the bill openly bragged that Teresa's presence was helping them win their campaign. So, while the bill ultimately failed, Teresa's visit undoubtedly boosted its public profile.

And then, during a 1985 visit to America, Teresa made comments regarding the Reagan administration's foreign policy that detailed her distaste for human rights. To the great horror of activists, Teresa commended the Reagan administration's policy in Ethiopia, which entailed support for the brutal Dergue regime, a military junta using starvation to break opposition to their rule.

But why did Teresa take such a position? Well, a starving population would certainly make good fodder for her organization. It's just one more example of how, rather than alleviating human suffering, Mother Teresa was actively supporting it.

### 7. Final summary 

The key message in this book:

**Most people know Mother Teresa as a hero of the poor and advocate for the sick. But the reality couldn't be further removed. Teresa's legacy is one of poorly run medical facilities, support for brutal dictators and the tarnished image of a woman who was more interested in her own PR than relieving the suffering of others.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Madness in Civilization_** **by Pope Francis**

_The Name of God is Mercy_ (2016) outlines Pope Francis's view of God and the Bible, and the reasons that the most important attribute of God is mercy.
---

### Christopher Hitchens

Christopher Hitchens was an English author, debater and journalist. A self-proclaimed socialist, Hitchens often took controversial positions on famous public figures. In his later years, he became famous for his anti-religious writing and strong support for the Iraq War, before passing away in 2011.

