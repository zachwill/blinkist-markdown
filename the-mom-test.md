---
id: 56afa9ca377c160007000078
slug: the-mom-test-en
published_date: 2016-02-04T00:00:00.000+00:00
author: Rob Fitzpatrick
title: The Mom Test
subtitle: How to Talk to Customers and Learn If Your Business is a Good Idea When Everyone is Lying to You
main_color: E2317B
text_color: BF2968
---

# The Mom Test

_How to Talk to Customers and Learn If Your Business is a Good Idea When Everyone is Lying to You_

**Rob Fitzpatrick**

The two most important types of conversation you'll have when founding a business are those with potential customers and those with potential investors. _The Mom Test_ (2013) offers advice on nailing the information you _really_ need from these meetings, and ensuring your business has the best possible foundation for success.

---
### 1. What’s in it for me? Let the Mom Test decide if your business idea is a winner. 

If you have a great idea for a new business, you'll probably want to know what your friends and family, potential investors and customers think about it before you get going. So you ask them, right?

WRONG! Friends and family might conceal what they really think about your idea because they want to be encouraging, and investors might tell you they really like your idea just to get you to stop talking.

So how do ensure that your business idea is really good when everyone around you is lying?

Enter the Mom Test, a set of rules that will tell you whether people _really_ think your idea is good or whether they're like your mom, who is always supportive and likes everything you do. So before you go squander your kids' college funds on your new business, make sure you read these blinks carefully.

In these blinks, you'll learn

  * why asking the right questions is so important;

  * why investors who "love" your idea are a bad sign; and

  * why talking over coffee beats any meeting.

### 2. Collect useful data about your idea and customer base by asking the right kind of questions. 

An important part of being a skilled entrepreneur is asking the right questions. Good questions are the key to developing and advancing your idea.

So stop _talking_ about your idea and start _asking_ about it. Ask your potential customers about the problems they have and the kind of solutions they're looking for. See if you're on the right track to offering them something they need.

Imagine your target customer base has a problem with Excel, for example. If they can find a cheap, effective way around that problem, don't waste time and money producing an alternative application for them. They won't need it.

Asking good questions also helps you collect the right kind of data. "Good" data is data that tells you what your customers are looking for and what they're willing to pay for it.

Let's say your idea is for a paid app that offers daily recipes to stay-at-home moms. It wouldn't be effective to merely ask some moms if they'd be interested in such an app. Instead, ask if they ever have trouble finding new recipes and how they go about looking for them.

If your target customers can easily find recipes online, they probably won't pay for your app. That's the kind of feedback you need.

"Bad" data, on the other hand, misleads you. When coworkers or friends give you insincere compliments or tell you white lies because they don't want to be rude, that's bad data. They might also make vague statements about possibly investing in you in the future — don't take these empty promises at face value.

> _"You shouldn't ask anyone whether your business is a good idea."_

### 3. Look for commitment from investors, not compliments. 

Sometimes when you pitch your business idea to potential investors or sponsors they're just not interested in it. And if they're not going to invest in you, you're ultimately wasting both your time and theirs. So make sure you're on the right track by learning to recognize the signs of a good or a bad meeting.

There are a few tell-tale signs of a conversation gone bad. Compliments, for example, seem positive on the surface but usually indicate disinterest. When a people compliment your idea, they're probably just being polite by avoiding outright rejecting you. Compliments allow them to divert the conversation away from the topic of investment so they can get back to doing whatever they're really interested in. Compliments are often a polite way to get rid of you.

When an investor is truly interested, on the other hand, he'll show it by asking specific questions about the terms of the investment and the progress he can expect. he'll want to learn more about the concrete details.

A lot of young entrepreneurs make the mistake of taking compliments seriously. They'll feel hopeful if a venture capitalist gushes over their idea or business plan. The good news is there's a strategy for avoiding this: focus your talks on _commitment_ instead.

An investor who's truly interested in you will be keen to commit. So be upfront: be clear about your expectations for first payments or deadlines for prototypes.

If they don't want to commit, they'll tell you and you'll know not to waste further time with them. And if they do commit, that's great! It means your project is a go!

### 4. Focus on a small group of target customers. 

Start-ups rarely suffer from a shortage of options or ideas. In fact, they usually have _too many_. If you pursue every possibility out there, you'll drown in a sea of projects and have nothing to show for it in the end. And how do you avoid this? By focusing on a small group of target customers.

Imagine you're developing a fitness app, for example. You might think it would be wise to target young men and women with jobs to work around who want to stay healthy and fit. That's not good enough, however. Your target group should be much smaller than that.

If you survey too wide a variety of people, their answers won't provide you with meaningful data. That's why you have to go after a more specific group, like employed men age 18 to 25 training for a marathon next year.

Such men would need a highly specialized training and diet plan to get them ready for the marathon. They'd probably be overwhelmed by online information about diets and not know who to trust. That's where you could come in with a specific app to help them.

And once you've identified your target group, get to know them. Talk to them! Learn all you can about what exactly they need.

You can't just select a target group and try to imagine what they want. You have to get out there, find those aspiring marathon runners and ask them how their training is going. Don't forget: they may not need your app after all. Maybe they can train just fine without an app or they prefer personal trainers instead.

On the other hand, they could need just the kind of service you want to provide. They might even be willing to pay for it. _That's_ the kind of data you need!

### 5. Strive to have casual and relaxed “meetings” with your potential customers and investors. 

If you've ever been to an awkward meeting that dragged on for hours, you'll know those kinds of meetings aren't productive. You don't want your talks with customers to end up the same way, so here are some strategies for keeping meetings effective.

First off, be casual. Allow your conversation partner to feel relaxed so he'll open up about his feelings.

Customers and investors alike dread that meetings will be a waste of time, so don't bore them with too many formalities. Keep the formalities to a minimum and stay personable instead. Ask them how their day is going, then ask about their problems relevant to your product and their ideas for solving them.

People are much more likely to be open with you if they feel you're genuinely interested in them. They'll feel better about sharing their honest opinions.

Sometimes you don't even need a formal "meeting" to get valuable information from your prospective customers. Just go to the places where they're likely to be and strike up normal conversations.

Imagine you're in the business of scheduling public speakers, for example. Why not go to a conference for public speakers and ask some of them to get coffee with you? You'll make contacts easily and it's much more casual than scheduling a meeting for a few months later.

Casual, comfortable settings make a huge difference when you're looking for useful data. No one is going to open up about their opinions if they're nervous. So don't put anyone on the spot; talk to them normally instead. They'll appreciate it and give you what you're looking for in return.

> _"If it feels like they're doing you a favor by talking to you, it's probably too formal."_

### 6. Final summary 

The key message in this book:

**When you've got a brilliant business idea, bring it to life by asking the right questions. Don't let compliments lead you astray — look for commitment instead. Choose a small, highly specific group of target customers and talk to them directly to find what they need. Stay casual so they'll be relaxed enough to open up to you. When you communicate sincerely, you'll be able to give them something truly meaningful.**

Actionable advice:

**Pick out your three most important questions.**

Preparation is the key to having a productive conversation with a potential customer or sponsor. So write out the three most important questions you have for them in advance. This ensures you'll get the information you need.

**Suggested** **further** **reading:** ** _The Lean Startup_** **by Eric Ries**

_The Lean Startup method_ helps start-ups and tech companies develop sustainable business models. It advocates continuous rapid prototyping and focusing on customer-feedback data.

The method is based on the concepts of _lean manufacturing_ and _agile development_, and its efficacy is backed up by case studies from the last few decades.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rob Fitzpatrick

Rob Fitzpatrick is a tech entrepreneur and partner at Founder Centric, where he helps universities, businesses and EU-funded start-ups design and deliver better start-up education programs.

