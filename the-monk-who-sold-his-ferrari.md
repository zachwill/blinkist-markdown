---
id: 59a2d8e4b238e10005eeb756
slug: the-monk-who-sold-his-ferrari-en
published_date: 2017-09-01T00:00:00.000+00:00
author: Robin S. Sharma
title: The Monk Who Sold His Ferrari
subtitle: A guide to using ancient practices to improve your quality of life and achieve your goals
main_color: AF9D77
text_color: 665C46
---

# The Monk Who Sold His Ferrari

_A guide to using ancient practices to improve your quality of life and achieve your goals_

**Robin S. Sharma**

_The Monk Who Sold His Ferrari_ (1997) offers a remedy to the problems of modern life. A fable, it tells the story of the enlightenment of ex-lawyer Julian Mantle and gives advice on how to live a happier, more rewarding and enlightened existence.

---
### 1. What’s in it for me? Change your view of life by reading a mystic fable. 

You probably know the story of the tortoise and the hare. The hare, sure that he'll win, challenges the tortoise to a race. The hare speeds off and is so far ahead that he decides to take a nap; in the end, the tortoise, though much slower, wins the race. Such stories, or fables, were used as a vehicle for delivering thoughts and morals in an enjoyable and memorable way.

Now imagine you were told a fable so powerful that it made you sell your most prized possessions and leave behind the life you've accustomed yourself to. What kind of fable could be so persuasive?

Well, these blinks unfold a fable with the power to change the way you look at life. It concerns the life of a fictional lawyer who ends up selling his Ferrari and becoming a monk.

You'll also learn

  * why your mind is like a garden;

  * how the image of a lighthouse will help guide you to a better life; and

  * what a sumo wrestler can help you remember.

### 2. While working as a wealthy, high-power lawyer, Julian Mantle had a spiritual awakening. 

This is the fictitious tale of Julian Mantle, a man who, seemingly, had it all. A graduate of the famous Harvard Law School, Mantle was one of the most well-known trial lawyers in the United States. He was making seven-figures; he lived in a mansion; a red Ferrari was parked outside. He was living the dream.

However, underneath it all, he was struggling. His workload was far too heavy for him to handle. Each day there was a new, important case for Mantle to take on and he approached every proceeding diligently. Eventually, the stress became too much for him, and one day Mantle suffered a severe heart attack and collapsed in the courtroom.

After that incident, he never returned to practicing law.

In fact, no one at his firm heard from him after his heart attack. Rumor had it that he'd moved to India in search of some answers and a simpler life — and, indeed, that's exactly what he had done. Before moving, Mantle sold his mansion and his Ferrari; he was sure that his quest for meaning was more important.

And then, three years later, he returned, showing up without warning at his former colleague's office. He was the picture of health, with a Buddha-like smile stretched across his face.

Mantle had been traveling from village to village in India by foot. On his journey, he had learned about yogis who seemed to defy aging. In Kashmir, he had heard about the Great Sages of Sivana. And these new discoveries inspired him to venture to the Himalayan Mountains, where he came across the monks that lived there.

It was there that Mantle had a reawakening and finally found his soul.

### 3. A mystical fable teaches the seven principles of the Sivana System. 

In the mountains, Mantle found the group of monks known as the Sages of Sivana. One monk, Yogi Raman, shared his wisdom with Mantle.

They discussed the meaning of life and Mantle learned about ways to achieve greater vitality, how to become more creative and to feel more fulfilled.

He was taught all of this on one condition — that he'd return to where he came from and spread the word. And this is why he went back to his law practice: to teach the life-changing _Sivana System_.

There are seven basic virtues that underpin the Sivana System, and each virtue is part of a fantastic fable.

The fable begins in a gorgeous green garden, silent and serene, abloom with beautiful flowers.

In the middle of the garden is an enormous red lighthouse. But the tranquility is suddenly disturbed by a sumo wrestler who comes striding out of the lighthouse door. Nine-feet tall and weighing 900 pounds, the wrestler is clad in nothing but a pink wire cable to cover his modesty.

While wandering around the garden, he stumbles upon a golden stopwatch. Out of curiosity, he puts it on — and immediately loses consciousness and crashes to the ground.

Eventually, he awakens and is filled with energy thanks to the fragrance of the surrounding yellow roses. He quickly leaps up and looks to his left; to his amazement, he sees a path covered with diamonds. Entranced, he walks along it. And this path leads him to everlasting bliss and joy.

To some, this story may sound ridiculous. But each element represents an aspect of the Sivana System. In the following blinks, you'll gain a greater understanding of these principles.

> _"Investing in yourself is the best investment you will ever make."_

### 4. Finding fulfillment is a matter of mastering your mind. 

The garden from Yogi Raman's fable represents the mind. A lot of people end up littering their mental gardens with waste — that is, negative thoughts or fears.

The first virtue of the Sivana System, then, is to control your mind. You have to tend to your mental garden, and the best way to take care of it is to stand guard at its gates. Only let pleasant, positive thoughts in and ban the detrimental ones.

At the end of the day, our thoughts shape our lives. You'll have a better standard of living if you fill your head with worthwhile thoughts. Want to live a peaceful, meaningful life? Then let the peaceful, meaningful thoughts in!

But how do we get our minds to focus solely on fulfilling thoughts? Well, we all have the ability to choose what we think about, so it all comes down to exercising our minds like a muscle.

The first step is to boost your concentration. There's a technique for improving your ability to focus that the Sages of Sivana call _The Heart of the Rose_.

In order to implement this technique, you'll need a quiet space and a rose. To start, simply stare at the rose's center. Pay close attention to its color and texture, and fill your mind with thoughts about how beautiful the rose is.

At first, random thoughts may enter your mind. But after you've been practicing for a while, you'll find that your mind is more disciplined.

Try to perform this on a daily basis, each day spending a longer period of time enjoying the rose.

Eventually, you'll find it easier to command your thoughts. You'll stop worrying and instead be filled with a sense of calm and joy.

### 5. To lead a fulfilling life, you need a purpose to guide you. 

The second virtue of the Sivana System is all about purpose. This principle is represented in the fable by the lighthouse.

The Sages of Sivana have a clear sense of purpose, and therefore never waste time. They know that they have a duty to fulfill this purpose.

When referring to their purpose, the monks use the Sanskrit word _dharma_ — which means "life's purpose." Dharma comes from the ancient belief that, while on earth, we each have a mission to complete. By honoring dharma, you can achieve lasting satisfaction and inner harmony.

The best way to realize your life's mission is to set clearly defined goals — after all, you can only hit a target if you're able to see it.

Here's the five-step method developed by the Sages to achieve personal purpose:

First, you must create a mental image of the outcome. If your goal is to lose weight, you'd envision a leaner, fitter version of yourself.

The second stage is to place pressure on yourself, but in a good way. Pressure can be a wonderful source of inspiration as it can often push a person to realize their full potential. A great way to generate pressure is to tell others about your plan.

The third step is constructing a timeline. To get your goal under way, you have to have a deadline in sight.

The fourth step is what Yogi Raman dubbed the _Magic Rule of 21_. This is the idea that a new behavior becomes a habit after you've done it for 21 days in a row.

The last step? Just enjoy the process!

### 6. Constant self-improvement is key to a radiant life. 

Remember the sumo wrestler? Well, he represents a virtue in the Sivana System, too.

This virtue is called _kaizen_, a Japanese word that means constant, never-ending improvement. It's all about unlocking your potential.

For this virtue, the Sages created ten steps known as the _Ten Rituals of Radiant Living_ ** _._**

The first is the _Ritual of Solitude_, and it stipulates that your daily routine must feature a moment of silence. This is so that you can calm your mind and access your creativity.

Second is the _Ritual of Physicality_. The aim of this stage is to get your body moving. By caring for your body, you ultimately care for your mind.

Next is the _Ritual of Live Nourishment_. You should only eat live foods, so it's best to follow a vegetarian diet.

Fourth is the _Ritual of Abundant Knowledge_. Throughout your life you should keep learning — so find a way to keep your mind stimulated by reading or studying.

The fifth is the _Ritual of Personal Reflection_. This is about looking at the way you behave on a daily basis. Could you have done anything better today?

Sixth is the _Ritual of Early Awakening_. This one's tough for those who enjoy sleeping in, because the idea is to sleep for around six hours and to rise with the sun.

Next up is the _Ritual of Music_. It's wonderful to listen to music as much as you can because it lifts your mood.

Number eight is the _Ritual of the Spoken Word_. This is about creating a personal mantra to inspire you.

Ninth is the _Ritual of a Congruent Character_. The point of this step is to make sure that you always follow your principles.

Last is the _Ritual of Simplicity_. Conduct a simple life and focus on your priorities and meaningful activities.

Let the sumo wrestler serve as a hefty reminder of kaizen!

### 7. Live a disciplined life and manage your time wisely. 

There's another quality to recall about the sumo wrestler in the fable. He was stark naked apart from the pink wire cable covering his privates!

The cable actually represents the fourth virtue of the Sivana System. This principle is concerned with living a disciplined life.

During his discussions with the Sages, Mantle learned that the wire is a symbol for strict self-discipline.

Just like your concentration, you can build up your sense of self-discipline. Yogi Raman explained to Mantle that one of his favorite exercises for doing so was to not speak for a day. The Sages assured Mantle that a vow of silence for a prolonged period of time is an excellent way to condition the will.

As the fable progressed, the sumo went on to discover a golden stopwatch.

This symbolizes the fifth virtue of the Sivana System, which is about respecting your time. The Sages, though removed from society, fully respect time and pay attention to its passing.

They taught Mantle that time mastery is life mastery. They see hourglasses as a stark reminder of mortality and believe that we should always live life to the fullest.

Therefore, it's important to use your time efficiently and plan how you spend it. To do this, you could take 15 minutes before you go to bed to plan out the next day. On Sundays, take an hour to plan the following week.

Another teaching is to be ruthless with your time and learn when to say no. Live each day as if it were your last — that way, you won't end up wasting time agreeing to an activity that you don't want to engage in. It helps to ask yourself, "Would I want to spend my last day on earth doing this?"

> _"Failing to plan is planning to fail."_

### 8. Selflessly serve others and live in the present for a joyful, rewarding existence. 

In the same way that Mantle collapsed at work under the weight of his responsibilities, the sumo in the fable went unconscious and fell to the ground. But he awakened full of energy, thanks to the scent of the yellow roses.

So what do these flowers symbolize? An ancient Chinese proverb says that a trace of fragrance always remains on the hands that present you with roses.

Therefore, the roses stand for the sixth virtue in the Sivana System — the concept of selflessly serving others.

The monks said that you should always be kind and compassionate toward others, as it improves your own life.

Take a moment every morning to think about what goodness you can spread in the world and how you can better the lives of other people.

You can do this by sincerely praising others, helping your friends when they're in need and by showing affection toward your family. Behaving in such a kind, earnest way will lead to a happier way of life.

After being revived by the roses, the wrestler discovered a diamond-encrusted path that led him to everlasting bliss and joy.

For the Sages, "living in the now" is the seventh virtue. They understood that happiness is not the destination but the journey itself.

As we travel through life, the path will be decorated with small wonders — that is, diamonds. In order to appreciate the diamonds, you have to practice gratitude on a daily basis.

That means always appreciating your health, your family and even the sound of the birds singing in the trees. Nothing is more important than right now.

Upon his return, Mantle regaled his former colleague with the tale of his journey to meet the Sages of Sivana. Ever since, he has continued to share their wisdom and has therefore fulfilled his pledge.

### 9. Final summary 

**It's possible for each and every one of us to live a joyful, rewarding life. By following the seven virtues of the Sages of Sivana, as told in Yogi Raman's fable, we can eliminate negativity and focus on achieving what we were born to do.**

Actionable advice:

**To keep your mind focused on positive thoughts, practice** ** _Opposition Thinking_** ** _._**

The first virtue in the Sivana System of wisdom is concerned with mastering your mind. In order to do this, the Sages often practiced what they called Opposition Thinking. Whenever a negative thought floats into your head, actively replace it with a positive one. For instance, if you're thinking about criticizing your appearance, think up a compliment for yourself instead!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Inner Engineering_** **by Sadhguru Jaggi Vasudev**

_Inner Engineering_ (2016) explains how happiness can only be found within yourself. These blinks introduce spiritual wisdom that will make you happier, more fulfilled and at peace with the life you are living.
---

### Robin S. Sharma

Robin S. Sharma, a former lawyer, is widely recognized professional speaker in the field of life improvement and leadership. His other books include _Megaliving: 30 Days to a Perfect Life_ and _The Saint, the Surfer, and the CEO_.

