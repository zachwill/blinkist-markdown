---
id: 5547768b36663200074a0000
slug: the-monopolists-en
published_date: 2015-05-06T00:00:00.000+00:00
author: Mary Pilon
title: The Monopolists
subtitle: Obsession, Fury and the Scandal Behind the World's Favorite Board Game
main_color: D1E9E2
text_color: 5B6663
---

# The Monopolists

_Obsession, Fury and the Scandal Behind the World's Favorite Board Game_

**Mary Pilon**

_The Monopolists_ (2015) unveils the true yet checkered history of Parker Brothers's most successful board game, Monopoly. It tells the tale of the game's origins in progressive, anti-capitalist thinking to its evolution under the control of Parker Brothers, a company that went to extraordinary lengths to rewrite Monopoly's history and crush any competition in the process.

---
### 1. What’s in it for me? Learn the bizarre truth behind the creation of the board game, Monopoly. 

Family fights over hours-long games of Monopoly are as predictable as picking a "Go To Jail" card. Parents deliberately bankrupting their children, and grandparents gleefully sending grandkids into paper-money penury has caused countless tantrums and tossed game pieces.

And yet the strife that can result from this winner-takes-all board game is nothing compared to the behind-the-scenes war over who exactly invented Monopoly. These blinks give you the little-known story of the battle over America's best-loved board game, a fight that even made its way to the Supreme Court.

In these blinks you'll discover

  * why early Monopoly games were considered too much like socialism;

  * how far one company went to hide where Monopoly came from; and

  * why the word "monopoly" is like the words "zipper" and "thermos."

### 2. Parker Brothers created the myth of Monopoly’s origins to try to keep competitors at bay. 

The board game Monopoly, first developed in the early twentieth century, is not only a wildly popular game but also a cultural icon. So much so that it even has a mythical origin story, too.

A man named Charles Darrow allegedly invented Monopoly during the Great Depression. Living in poverty, Darrow tried to sell his game idea to companies such as Parker Brothers, but had no luck.

Frustrated by his failures, Darrow instead decided to produce the game himself. As the board game's popularity grew by word of mouth, game companies started to pay attention. Parker Brothers came around and in the end, bought Darrow's game. He became wealthy, a true rags-to-riches story.

But is this really how Monopoly came to be?

It's not. Darrow never invented the game Monopoly. Friends taught him how to play it; he then redesigned it. So why did Parker Brothers push this made-up history instead of the truth?

At the time, Parker Brothers was an ailing game company. On the brink of collapse, the company's main troubles stemmed from its inability to keep competitors from stealing its games.

Parker Brothers had trademarked the highly successful game _Ping Pong_, for example. As Ping Pong's popularity grew, however, competitors got around trademark issues simply by producing similar versions that were still different enough to be legally defendable.

The popular game Monopoly offered Parker Brothers a new start. If the company could write its own history of how the game was created, it could ensure that it maintained complete control of the concept. Darrow, the alleged inventor, was happy to go along; and Parker Brothers sealed the deal.

But what is behind the lies? How did Monopoly really come into being?

### 3. Monopoly was originally based on a game called Landlord’s Game, invented by Elizabeth Magie. 

Have you ever played the Landlord's Game? Probably not. But without this game, Monopoly as we know it today would not exist.

Elizabeth "Lizzie" Magie invented the Landlord's Game in 1904. While the game had a lot in common with modern-day Monopoly, it also had some unique characteristics.

The Landlord's Game board had a "Go To Jail" option, railroad stations placed on squares on each of its four sides, and even used fake money, just like Monopoly. Some other elements, however, a modern Monopoly player might find unusual.

The Landlord's Game was based on the political ideas of Henry George, a famous writer and economist who believed that people should pay fees for the land they used rather than income tax.

George's ideas inspired how the game was played. Players could compete to obtain a monopoly of properties, or they could all pay the same fees and profit together.

The Landlord's Game in general wasn't a huge success. Magie secured the patent for the game, and sales were brisk, with even a version made in Scotland. Yet Magie's real inspiration was not so much financial success but the spread of Henry George's ideas. Also a poet, Magie was passionate about many social issues, and campaigned for fair wages for workers. She even called herself a "young American slave."

Henry George's progressive ideas were embraced by many groups, including socialists, a fact that led to criticism of George in certain circles of the United States — which didn't help Magie or her game.

Magie regardless kept producing the Landlord's Game and updated her patent in 1923. Interestingly, the game started to take on a life of its own, also becoming popular on the east coast of the United States, unbeknownst to Magie.

> _"Critics such as George noted that capitalism was good at creating wealth, but it could be lousy at distributing it."_

### 4. Players modified the game to reflect local realities. On the east coast, it was called “Monopoly.” 

The Landlord's Game continued to be sold but it never became a bestseller. Yet as the game's popularity grew, players began to modify the game, creating something new.

It all started with a community of Henry George advocates in the small town of Arden, Delaware. Players there added the names of New York streets and neighborhoods to the board.

Then Upton Sinclair, the highly influential social critic, helped popularize the game's modified version, particularly on the east coast.

It was around this time that people first began calling the game "Monopoly." The point of the game was actually a warning against the formation of monopolies, as the game play illustrated the danger of monopolies on society. Even some universities used the game as a teaching aid.

Changes to the game kept coming. Players in Atlantic City added many of their own streets and neighborhoods to the game, including some of the area's poorest corners. These players wanted Monopoly to reflect reality as much as possible.

Daniel Layman made some final adjustments to the game, producing his own version in 1931. He added cards of "chance" and even miniature houses, based on models his friend had brought back from Ukraine.

Layman called his game _Finance_. Despite Monopoly's popularity, however, Layman was unable to make money off his new version. Layman eventually sold the rights to his game for just $200.

So the Monopoly we know today is really the result of player communities and long-term collaboration. So how did its creation come to be attributed to just one man?

### 5. Parker Brothers used patents and cash to cover up Monopoly’s origins, and to secure its ownership. 

The story of how Parker Brothers managed to copyright Monopoly is still shrouded in mystery.

Charles Darrow secured the copyright for Monopoly in 1933, although it's not clear what exactly the patent covered, as the original documents have been lost. Parker Brothers had to secure a further copyright if the company wanted to the rights for the entire game, however.

So in 1935, the company filed for a patent as the inventor of Monopoly's official rules. The patent was accepted in just one month, an incredibly short period of time. To this day it is still unknown why or how the company's patent application was accepted so quickly.

Furthermore, it is odd that Parker Brothers was granted a patent for game rules that were very similar to those for the Landlord's Game. In retrospect, the patent should have been rejected; but it held.

After securing its patent, Parker Brothers sought to tie up loose ends with Monopoly's other "inventors." First the company bought the rights to the similar game, Finance, then it paid another developer $10,000 to keep his game out of the market (and keep quiet about it, too).

What's more, Parker Brothers tried to purchase as many copies as it could of earlier versions of the game from private owners.

And as a final coup, Parker Brothers approached Magie, buying her patent for the Landlord's Game with a promise that the company would re-release the original version.

The company did release a version of the Landlord's Game, but only for a short time.

Some players complained that Parker Brothers and Darrow had "stolen" Monopoly, but such claims were ignored. Charles Todd was one of these men, and he would know — he was the one who had originally taught Darrow how to play Monopoly.

Darrow however never contacted Todd, or shared any of his earnings from Monopoly with him.

So the truth behind Monopoly was covered up. How did the real story come to light years later?

### 6. An economics professor wanted to create an “Anti-Monopoly” game, but Parker Brothers said no. 

After securing patents and copyrights and silencing alternate histories of Monopoly's creation, Parker Brothers went on to enjoy profitable decades as Monopoly became its best-selling game.

Few people knew or cared about how Monopoly came to be, and Parker Brothers had no problem with this state of affairs. Until that is, in 1973, when an economics professor shook things up.

Ralph Anspach had an issue with the values he believed that playing the board game Monopoly encouraged. The point of the game — to gather up as many properties as possible while bankrupting your friends in the process — rewarded selfish, greedy behavior.

What's more, in the United States, monopolies according to law were illegal; a game that made creating monopolies a virtue was something Anspach could not understand. The 1970s, remember, was marked by the OPEC oil crisis — and people were wary of the dangers of monopolistic practices.

So Anspach came up with an alternative version of the game, in which players were encouraged to share resources and _break up_ monopolies. He called his version _Anti-Monopoly_.

Unable to find a games manufacturer, Anspach decided to produce the game himself, and it began to take off. Public distrust of power was particularly high after the political scandal of Watergate, and Anspach's anti-authoritarian game naturally clicked with people.

Yet eventually a letter from Parker Brothers arrived at Anspach's door. The company told him to stop producing his game. He offered to compromise by calling it _Anti-Monopolism_ or _Anti-Monopoli_. Parker Brothers refused; neither side backed down. A court date was set.

In preparation for his court date, Anspach did some research into Monopoly's history. This was when he started uncovering the unsavory details of Monopoly's checkered past.

### 7. Was “monopoly” a generic term, like aspirin, zipper and yo-yo? The courts would decide. 

Ralph Anspach knew he was up against a large, powerful corporation with a long history of crushing potential copyright breaches. The company even went after a priest who created a board game called Theopoly!

So how could he defeat Parker Brothers? Luckily, Anspach's son learned a key bit of information, after reading a book called _The Toy is Born,_ which said the inventor of Monopoly was a woman named Lizzie Magie.

With this knowledge, Anspach looked deeper into the game's history. When he discovered that Monopoly had existed _before_ Parker Brothers and Darrow got a hold of it, Anspach started to build a case that the game itself was beyond copyright laws.

Anspach sought to argue that the term "monopoly" was _generic,_ meaning it was a product name that had been adopted into everyday language, like aspirin, zipper, thermos, yo-yo or escalator — all former brand names. When a product name becomes generic, it can't be copyrighted anymore.

But how could Anspach prove this? He had to talk to people who'd been involved in the game's developmental years.

He contacted Layman, the inventor of the board game Finance, as well as some Atlantic City families who'd added local city streets to their versions of the game. One family even showed Anspach a spelling error they'd made, that Parker Brothers' Darrow had then mistakenly included in his own version.

Anspach was finally able to trace the story back to Magie. Unfortunately, she had since passed away, but he discovered that his game had quite a bit in common with her own. Both were anti-monopolistic board games that encouraged players to share and work together.

He continued collecting interviews and evidence he thought he could use in his court case, hoping to prove that the term "monopoly" was beyond copyright.

Anspach's hope would remain just that, however.

### 8. Parker Brothers tried to pay Anspach off, but he pushed for a trial – and lost his fight. 

So what happened after Anspach uncovered Parker Brothers's lies? How did the company react?

Initially, Parker Brothers offered Anspach $500,000 (some $2.2 million today) plus damages, as well as the opportunity to become a company executive — if he dropped the case and handed over the rights to his game.

Anspach said no. He was more concerned with the principle of the matter, that one can't copyright a word in common usage.

So the case went to trial. Unfortunately for Anspach, however, it was heavily slanted in favor of Parker Brothers.

Anspach had gone against his lawyer's advice when he turned down Parker Brothers's offer. The lawyer expected Anspach to lose his case, and what's more, not be able to pay his lawyer's fees — so he quit. Switching lawyers so late in the process hurt Anspach's case.

The judge overseeing the case also clearly favored the rights of big business. In the end, the judge ruled in favor of Parker Brothers, agreeing with the company's evidence that the Anti-Monopoly game breached the company's trademark and was illegally riding on the coattails of Monopoly's success.

Anspach was ordered to hand over all the remaining copies of Anti-Monopoly so Parker Brothers could destroy them, which it did quickly. Certain victory was complete, the company wanted to finish the matter and move on.

Parker Brothers's success wasn't quite as complete as the company might have thought, however. Anspach sought an appeal, and with this, the situation was about to change.

> _"We have a monopoly on the word monopoly...and it is not the law."_

### 9. The Supreme Court stepped in, and Anspach was victorious. Yet the truth still sits in shadow. 

In his appeal, Anspach tried a different approach. He and his lawyers decided to argue that when people bought the game Monopoly, they were really buying it because of its _name_ rather than its brand.

This, they argued, negated the trademark. The _name_ was the game's appeal, not the company. To prove this, they even conducted a survey illustrating that people cared only about the name "Monopoly" regardless of whether it was produced by Parker Brothers or not.

At first, the judge didn't go for their argument, but then the U.S. Supreme Court got involved.

Interestingly, the Supreme Court sided with Anspach. They agreed that the term "monopoly" was generic and thus could not be trademarked, and what's more, that Darrow hadn't been the original inventor of the game.

Finally, Anspach won his case. Parker Brothers had to pay up and let him sell his game!

Unfortunately for Anspach, interest in his anti-establishment game had waned, as the political mood of the country shifted from the anti-business era of the mid-70s to the conspicuous consumption of the Reagan years.

This wasn't the end of the fight, however. The Supreme Court's decision threatened big businesses and trademarks everywhere. If anyone could prove that a customer cared for a product's name rather than its brand, they stood a chance at defeating a company's trademark.

Naturally, several companies, from Parker Brothers to Procter & Gamble, fought against the ruling. Since then laws have changed somewhat, and the Supreme Court has handled a number of trademark cases, ruling for or against trademark laws, depending on the case.

Parker Brothers's case wasn't the last. Trademarks, such as "elevator," were ruled to be generic terms and thus not able to be trademarked; while other trademarks, such as "Coke" or "Teflon," retained their status.

Yet the true history of Monopoly only came to light as part of Anspach's legal fight. But as late as 2014, Parker Brothers's historical timeline for Monopoly on its website begins — curiously — at 1935.

The company too declined to comment or cooperate in the research for _The Monopolists_, and refused to open their archives to the author.

In the end, the fight over who owns history is far from over.

### 10. Final summary 

The key message in this book:

**The true history of Monopoly is a tale as steeped in greed and capitalism as the board game itself. Though originally developed as a political and economic commentary and modified by players over the years, the board game came under control of Parker Brothers, a company which managed to fabricate a version of Monopoly's history allowing it to secure exclusive trademark rights. While the truth came to light in the 1970s, Parker Brothers to this day is still trying to cover it up.**

**Suggested** **further** **reading:** ** _Finite and Infinite Games_** **by James P. Carse**

_Finite and Infinite Games_ (1986) offers two contrasting viewpoints on how to live your life, whether you're engaging in sexual relationships or warfare. Carse argues that any activity can be seen as either a finite or an infinite game, the former being end-oriented and the latter leading to infinite possibilities. He reveals how the world appears through the eyes of those who play with the finite or infinite in mind, and concludes that how and what games we play are our own choice.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mary Pilon

Mary Pilon is a reporter for _The New York Times_, and has also written for _The_ _Wall Street Journal_, _New York Magazine_ and _USA Today_.

