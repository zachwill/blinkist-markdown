---
id: 55062a72666365000a6c0000
slug: the-moral-case-for-fossil-fuels-en
published_date: 2015-03-18T00:00:00.000+00:00
author: Alex Epstein
title: The Moral Case for Fossil Fuels
subtitle: None
main_color: 43BFE7
text_color: 256980
---

# The Moral Case for Fossil Fuels

_None_

**Alex Epstein**

In _The Moral Case for Fossil Fuels_, author Alex Epstein explores the benefits of fossil fuels such as coal, natural gas and oil. The book outlines the dramatic and positive effects using such fuels has had on society and examines the many myths associated with fossil fuels. Importantly, using fossil fuels is a moral decision in that their benefits to humanity outweigh any of today's environmental concerns.

---
### 1. What’s in it for me? Learn why, despite calls to the contrary, we should continue to use fossil fuels. 

It's a message that's been drilled into our heads: fossil fuels are bad. Burning coal pollutes the atmosphere, causing the planet to warm; extracting and transporting oil leads to spills and potentially catastrophic environmental effects.

Yet is this really true? These blinks suggest otherwise.

You'll learn that not only is our use of fossil fuels beneficial, there is, a least for now, really no alternative to coal and oil. The development and use of so-called green energy, whether wind farms, biomass or solar, is today just too unreliable and too expensive to meet the world's energy needs.

In the following blinks, you'll discover

  * how many people have actually died as a result of nuclear power;

  * how fossil fuel use leads to less climate-related deaths; and

  * why we can thank fossil fuels for helping curve malnutrition.

### 2. Fossil fuels have been crucial to human life, helping to curb hunger by advancing agriculture. 

Everyday you hear that fossil fuels such as coal, natural gas and oil are bad for the environment.

So how could you support the contrarian opinion, that fossil fuels are actually good?

As a society, we owe much credit for our current prosperity to fossil fuels, as the use of these fuels have brought tremendous benefits.

Fossil fuels have had a hand in combatting global malnutrition, for example, the use of which basically revolutionized modern agriculture.

Methane-based fertilizers and electric irrigation systems that run on coal have allowed farmers to grow much more food. Oil-powered mechanization has increased the amount of farmland that can be cultivated per worker, and oil-powered transportation systems have allowed us to deliver food to more people.

You also might not realize just how ubiquitous fossil fuels are. There are at least 50 things in the room you're in that are made from oil. The insulation in your walls; the carpet under your feet; the computer screen you're looking at; and even the chair you're sitting on, were all made with oil.

Fossil fuels are also the only resource that gives us cheap, plentiful and reliable energy. For a product to be inexpensive for a customer, every part of its production process needs to be inexpensive, too.

While it is difficult to locate and extract fossil fuels, the total process — from finding a source to refining the raw product — is actually cheap compared to other "green" energy resources.

Energy from the sun or wind is limitless, but the process and materials required to utilize solar or wind power are not only complicated but also expensive. Such energy sources are also unreliable, as we can't control when the skies will be clear or when the winds will blow.

All in all, fossil fuels are immensely beneficial to _human_ life. They're good for us and support our society, so the benefits we gain from them should outweigh any other concerns.

> _"Energy is ability, because energy can help us do anything better."_

### 3. We’ll soon be able to mitigate most of the risks and side effects of using fossil fuels. 

Despite the benefits, it's true that using fossil fuels comes with risks and potential side effects.

But does this mean we shouldn't use them at all? Certainly not.

Throughout history, people have worked to solve problems with whatever resources were at hand. Why can't we do this now with regard to fossil fuel use?

During the Industrial Revolution, London's coal-induced smog was worse than the polluted skies over Beijing today. Yet Thomas Edison's invention of electric power generation and distribution in 1882 solved this problem by replacing the use of coal in homes with electricity.

Before this, coal was the main fuel, and its benefits far outweighed its costs. And many years later, the same still holds true.

There's a lesson to be learned: as a society, we should use whatever the most progressive form of energy is _at the time_, even if it comes with risks. And right now, this means fossil fuels.

And while fossil fuels do have their risks, using them might not be as dangerous as you think.

For example, many people are concerned about the use of _hydrofluoric acid_, which is a key material in certain kinds of oil drilling. This acid is so powerful that it can melt bones!

Yet risks can always be mitigated, however. We can avoid using such dangerous chemicals by finding new methods for drilling, such as fracking. We can also implement strict safety procedures for people who do have to deal with dangerous substances.

The use of nuclear power, once the _most_ feared energy source, has never actually caused a single death. In fact, the type of uranium used in nuclear power plants is not even physically able to explode — so fears of a nuclear meltdown at plants have been completely unjustified.

The same is true of fossil fuels, in that critics tend to exaggerate the risks.

### 4. Fossil fuels don’t harm the environment; they’ve actually made the earth a better place to live. 

Two main points are raised when considering the reasons against using fossil fuels. One, fossil fuels cause pollution; and two, fossil fuels contribute to global warming.

The argument against the use of fossil fuels is based on such beliefs.

But how valid are these statements?

While it is true that our planet is warming up slightly, it's not happening at a catastrophic rate — and this change may even be beneficial.

Much of what is said about climate change is inaccurate or dishonest. Our climate _has_ changed, to be sure, but weather changes haven't been as extreme as many people have claimed.

Fossil fuels and carbon emissions have actually had positive impacts on the environment. Aside from the fact that mild warming in some areas would be desirable, fossil fuels improve our fertilizers.

The increase in temperature has made our planet more fertile, allowing us to grow more crops.

Fossil fuels also are not polluting. In fact, they've made our world cleaner, safer and generally better overall.

For example, we use fossil fuels to purify dirty water. We now can disinfect and treat water using synthesized chemicals found in plants. We also transport our drinking water in plastic pipes.

Fossil fuels have made the world safer for people. In 1932, there were 5,073,283 climate-related deaths, from droughts, floods or extreme temperatures.

In 2013, the figure was 29,404, some _99.4 percent less_. Machines powered by fossil fuels have allowed us to build cities and structures that are much more durable in rough weather conditions.

Fossil fuels have also allowed us to fully master our climate. Thanks to air conditioning and heating systems (powered by fossil fuels), we can now live and thrive in extreme environments. Even desert areas like Southern California can become very desirable places to live.

> _"Fossil fuels transform our environment to make it wonderful for human life."_

### 5. Fossil fuels are a sustainable energy source, at least for the foreseeable future. 

How many times have you heard that the world is doomed if we don't switch immediately to using renewable energy sources, such as solar or wind power?

This is simply false!

Research has shown that the earth still contains huge quantities of fossil fuels. In fact, these sources are expected to last us another 3,050 years! It's just that many sources of fossil fuels are hidden, and thus difficult to extract with current technology.

It's certainly likely that we'll develop new ways of extracting fossil fuels over the next 3,050 years. We might even replace fossil fuels altogether within that time frame.

Even now, scientists are developing better technology for utilizing solar energy and nuclear fusion (which is different from current nuclear fission methods for producing energy).

Also, if oil reserves dwindle, we can use coal and gas to produce more. This is possible as all fossil fuels are _hydrocarbons,_ made from the same two elements: hydrogen and carbon.

Even if using fossil fuels _were_ unsustainable, we still would struggle to replace their use with any of the current "green" alternatives.

Solar and wind power just aren't viable options, as they're still too expensive and unreliable.

Energy from biomass, or plant or animal matter (like wood, crop waste, grass and manure), is naturally limited as it needs to be grown on farmland that is already scarce. The production of biomass indirectly results in increased food prices, through competition for farming space.

Hydroelectric energy, which transforms the power of flowing water using turbines into electricity, also has its limitations. The problem is finding suitable sites to harness the power of water on a large scale. While China and Brazil rely heavily on hydropower as they have sufficient water sources, such as large rivers, the United States has already run out of suitable rivers to dam.

Environmentalists don't often consider how much we as a global society rely on energy, and how difficult it is already to produce enough energy for the world's needs.

At the moment, only fossil fuels can sufficiently provide the energy we all need at a cost we can afford.

### 6. Final summary 

The key message in this book:

**As a global society, fossil fuels as a main energy source are simply the only viable option right now. "Green" alternatives are impractical, as they are still too expensive and unreliable. Fossil fuels have revolutionized our world, dramatically improving our lives and shaping our society. Because we rely on fossil fuels so much for the vital energy they provide, it's moral for us to continue using them. The earth's stores of fossil fuels should last long enough for us to develop preferable alternatives.**

**Suggested further reading:** ** _The Burning Question_** **by Mike Berners-Lee and Duncan Clark**

_The_ _Burning_ _Question_ deals with our generation's most pressing problem: climate change. The book discusses why it's important to make drastic changes in our politics, markets and society, and what we have to do to achieve a sustainable future for ourselves and our grandchildren. The authors not only explain how we've failed so far but also point towards the source of the problem.
---

### Alex Epstein

Alex Epstein is an energy theorist and industrial policy expert. He's the founder and president of the _Center for Industrial Progress_, a for-profit think tank. _The Moral Case for Fossil Fuels_ is a _The New York Times_ bestselling book.

