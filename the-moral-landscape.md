---
id: 534d56636633610007770000
slug: the-moral-landscape-en
published_date: 2014-04-15T06:01:25.000+00:00
author: Sam Harris
title: The Moral Landscape
subtitle: How Science Can Determine Human Values
main_color: A13F3E
text_color: A13F3E
---

# The Moral Landscape

_How Science Can Determine Human Values_

**Sam Harris**

These blinks explore cutting-edge research in neuroscience and philosophy to explain human morality. Harris argues that morality can best be explored using scientific inquiry, rather than relying on religious dogma or theology.

---
### 1. What’s in it for me? Discover what makes you feel that something is right or wrong. 

Most people consider themselves morally upstanding citizens, yet every day the newspaper is filled with depictions of crimes and deplorable actions that spark moral outrage.

How can so many good people commit actions that are morally wrong?

The answer is, of course, that our moral compasses are highly variable and, as it turns out, highly unreliable.

To better understand them and morality in general, we should look deeper into the inner workings of the human brain, where sensations of morality occur, rather than in ancient religious tomes which claim to lay down the law when it comes to morality.

In these blinks, you'll read about moral relativists, who believe you shouldn't care whether you live in a world of constant bliss or one where you're tortured continuously.

You'll also see how you could stop a careening train in its tracks as long as you'd be willing to murder someone.

Finally, you'll find out why certain cultural practices, like genital mutilation, can be considered morally deplorable no matter what the cultural background.

### 2. Morality isn’t supernatural: it’s governed by physical and chemical processes in our brains. 

Most people have a sense of morality and will readily proclaim, for example, that helping people is right but killing them is wrong. You're probably nodding in agreement in your seat right now, right?

But have you ever paused to think about where morality comes from?

Many people believe that morality has been derived from religious teachings and was originally determined by a divine power. However, the more we understand about the brain, the more we see that morality is actually a completely natural consequence of our neurology.

So let's examine the brain more closely: it turns out that there's nothing mystical about it and, like all our other organs, it functions only through chemical, electrical and physical processes, for example, the release of dopamine or a firing synapse.

These processes together form our _brain_ _stat_ e: a physical state which reflects how we're experiencing the world at that moment. These brain states are near universal to all humans, which means that if you examine the brains of two people who both feel sad, for example, the brain states will have the same pattern.

This physicality means that morality can also be traced back to brain states: we decide whether an action is moral or immoral by discerning whether it makes us feel good or not, and the brain state of "feeling good" is induced by the regulation of neurotransmitters like dopamine and serotonin.

For example, cooperating with someone is an action that typically makes us feel good. It's no surprise then that feeding the hungry (cooperative behavior) is usually considered more moral than letting them starve, which entails no cooperation at all.

The fact that brain states determine how we perceive the morality of actions means that the _only_ possible way to approach moral questions is through our understanding of the brain. This approach can offer us far more useable knowledge about morality than the anecdotes and parables in ancient religious texts.

### 3. Morality has its roots in our evolutionary history. 

As you now know, our sense of morality is intimately tied to our brain states. To properly understand this connection, we must examine how the brain came to be in the first place.

Like all other products of evolution, the brain was built upon the evolutionary developments that preceded it.

We may look at organs like, for example, a giraffe's neck or a hawk's eye, and think them exquisite tools for their seeming purpose. But remember, no matter how elegant an organ seems, it did not develop for any "purpose."

Evolutionary developments always happen by accident. Coincidental mutations in genes cause animals to gain certain attributes like a slightly elongated neck, or to lose them, and this change impacts their ability to survive and reproduce either positively or negatively.

This means evolutionary development is always built upon what came before. Our brains didn't just appear out of nowhere: they're an amalgamation of ancient and new developments. The newer additions contain things like the frontal lobe, which is responsible for more complex behavior. But there's still some ancient wiring in there, too, and the newer additions must cooperate with these less-sophisticated structures for the whole to function.

So where does our sense of morality come from?

From our evolutionary ancestors, it turns out. In terms of brain states, the new brain structures show no difference between beliefs like "2+2=4" and "killing the innocent is wrong." To form moral beliefs, the older brain structures must be consulted.

This indicates that, just like our brains, our morality has also followed a long path of development before arriving where it is today. One waypoint on this path can even be seen in our evolutionary relatives: in one experiment, monkeys demonstrated the "morally correct" behavior by forgoing food to prevent their cage mates from receiving electric shocks.

### 4. We can’t reconcile moral relativism with our lived experience of the world. 

When you travel or read about other countries and cultures, you might wonder whether the right answers to moral questions aren't culture dependent: what's considered right in one place is wrong in another.

But some go even further than this assertion, claiming that there simply aren't any right or wrong answers to moral questions at all.

These _moral_ _relativists_ argue that our sense of morality merely depicts our _preferences_ about the way the world should work. This would mean that something "morally right" (helping those in need) isn't really distinct from something "morally wrong" (murdering people). Neither is universally correct; some people just prefer one over the other.

There is some validity to this view, but a simple thought experiment can easily demonstrate that sometimes right and wrong are unambiguously clear.

Imagine two parallel worlds: in the first, everyone's life is as miserable as possible; every living moment is punctuated by the worst suffering imaginable. Meanwhile, the inhabitants of the second world experience pure bliss and serenity nonstop.

Would you really say that there is no meaningful difference between those two worlds?

This illustrates the absurdity of pure moral relativism, for if there are supposedly _no_ correct answers to moral questions, then we must ignore our own experiences, like pain and pleasure, too.

Thinking back on the two imaginary worlds, it's clear that it doesn't matter if they're not _objectively_ different, what matters is that we _experience_ them differently. And if you think about it, what else could you possibly use, other than your own experience, to make moral judgments?

For example, if you were constantly tortured in the miserable world, you'd probably gain little comfort in the fact that your pain is merely subjective and your idea that torture is wrong is merely your own preference.

If you question moral relativists more closely, you'll find that they're almost always only relativists in principle, not in practice. Otherwise they'd also be indifferent to their own suffering, and that's very rare.

### 5. In spite of being difficult to define, we can use increases and decreases in “well-being” to gauge morality. 

Besides debating the very existence of universal morality, one of the big questions for moral and ethical philosophers is: "What criteria do we use to judge moral behavior?" In other words, what makes something moral or immoral in our eyes?

Simply put, the only way we can measure the morality of an action is by analyzing whether it increases or decreases _well-being_, meaning everything we value in life, such as our health, happiness, relationships and so forth.

In fact, even religions claiming that morality has been handed down from a higher power still encourage morality through its beneficial effects on people's own well-being.

For example, a religion may state that moral deeds are rewarded by gaining entrance into heaven, while immoral deeds are punished through eternal damnation. Thus, while believers may talk about acting according to God's will, they are ultimately doing so to avoid hell and get into heaven, i.e., bettering their own well-being.

Though it helps us make moral judgments, well-being can also be a slippery criterion to apply.

One such dilemma is: Should you be more concerned about your current or future well-being? Sometimes they conflict.

For example, if a smoker considers quitting, she faces a trade-off: suffering the sharp, short-term decrease in well-being due to nicotine withdrawal versus an increased well-being later on thanks to better health and finances. Her actions will no doubt depend on whether she's more concerned with her immediate or future well-being.

Another dilemma is that, although some actions increase one person's well-being, they decrease that of another.

For example, if two people apply for the same job, only one of them can get it. That means only one of them gets the increase in well-being through the excitement and financial stability it brings. Meanwhile, the other person will experience a decline in well-being through feelings of rejection. So which well-being would you use as a criterion for moral questions?

### 6. Answers to moral questions are meaningful, even if the absolute answers are unreachable. 

Many people feel that morality is strongly dependent on cultural context: what's considered wrong in one culture may be right in another. They also insist that, because of this, there are no answers to moral questions about whether one cultural practice is more right or wrong than another.

But, as you will see, this reasoning just doesn't stand up against closer scrutiny.

Let's start with a broader topic. Can all questions, moral or otherwise, be answered?

In some cases, no: because we lack the means to gather all the information to answer them practically. For example, can you tell me what every person on earth wished for their birthday last year? Of course not, because you don't have that information at your disposal and can't collect it easily.

Similarly, we can't really gather background information on all aspects of all cultural practices to properly compare them.

But just because a question doesn't have an answer _in_ _practice_ doesn't mean it's totally unanswerable _in_ _principle_. You could, in theory, start a mass polling operation to cover the entire globe from Antarctica to Greenland and record everyone's birthday wish so you'd have your answer.

This may seem a little excessive, but it proves that an answer does exist. And even if you don't go that far, you can still say something meaningful about the possible answers. For example, you can state fairly confidently that at least one person did not wish for a toaster oven.

So circling back to moral questions about cultural practices, though we may not have absolute answers, we can say something meaningful about them.

For example, we can say that practices like genital mutilation or forced marriages are not morally ambiguous, because they rarely result in increased well-being.

This illustrates that, though we may not be able to say which practices are optimally moral, we can at least say which ones aren't.

### 7. In a moral landscape, there are many possible “right” answers to moral questions. 

People often tend to think of morality as binary: something is either right or wrong, with no exceptions or middle ground.

But this isn't a good way to accommodate the diverse experiences we encounter in life.

So even though some moral claims can be said to be _generally_ true, they're likely to have a few exceptions depending on external circumstances, too.

Take the moral maxim "thou shalt not kill," which seems like a generally "good" one to live by. But it's easy to imagine a set of circumstances where killing someone would be the right thing to do, for example, by performing euthanasia on someone who has had a long and fruitful life, but is now suffering greatly and asks you to kill him.

So rather than thinking of morality as a binary function, we should think of it as a _moral_ _landscape_ : a plane full of peaks and valleys, the highest peaks denoting maximum well-being and the deepest valleys denoting unbearable suffering. This view allows the flexibility for multiple right answers to moral questions.

In this moral landscape, when we make moral decisions, we should always try to climb the peaks and avoid the valleys. There are many possible peaks to climb, and the one you should aim for is determined by your cultural background.

But this multitude of peaks doesn't mean that all actions are equally moral. Though we can't say which one is the absolute highest peak, we can at least clearly see that the peaks are better than the valleys.

This can be applied to the simplest of things, like food: though everyone on earth can't agree what the best food is, we can agree on the fact that most foods are healthier than toxic waste.

Similarly, we can say that inflicting lifelong torture upon someone will not increase anyone's well-being enough to consider it a morally correct act.

### 8. Our moral compass is fickle, so we need objective science to answer moral questions. 

Do you have a strong moral compass? Most people think they do, but our moral judgment is, in fact, often steered by situational factors.

For example, our moral judgments can be swayed by the fact that we tend to naturally fear losses more than we appreciate gains.

Consider an example where a hospital commits malpractice on two child patients. One of the children was a genius with an IQ of 200, but she was accidentally injected with a poison that brought her IQ irreversibly down to an average 100. The other child is an average 100 to start with, but the hospital forgot to inject her with a genetic enhancement that would have given her an instant IQ of 200.

Because we emphasize losses more than we do forgone gains, we see the first mistake as more morally condemnable, even though the end result is exactly the same.

Another factor impacting our moral judgment is that we're reluctant to answer moral questions when we're personally involved.

For example, consider a scenario where a runaway train is speeding down the track, about to crash into ten oblivious construction workers. Luckily, you have the possibility of pulling a switch so the train veers off on another path, where only one oblivious worker will be killed.

Would you pull the switch? Most people would say yes, because they would be saving ten lives for the price of one.

But what if there was no switch, and you had to instead _physically_ _push_ one person in front of the careening train to stop it? Although the outcome is the same, most people feel much less comfortable with this idea because it requires direct personal involvement.

Knowing how such factors affect our moral intuition, can we trust it?

Not really. We need the assistance of objective scientific evidence and inquiry to make consistent, impartial judgments. This evidence comprises things like the number of lives saved or the positive effect on our brain states.

### 9. Free will is an illusion, but our social institutions are based on it. 

Do you think criminals deserve to be punished for their crimes?

Many of the social institutions in society, e.g., the legal system, are based on the idea that we have the freedom to choose our own actions.

But what if this belief is flat-out wrong?

Recent science seems to indicate that _free will_, meaning our ability to freely choose our actions, doesn't exist.

This was demonstrated by experiments where it was found that our experience of making a decision, for example to raise our arm, actually takes place up to ten seconds _after_ the brain has already started the necessary processes to perform that action. 

Thus it seems we don't really decide what actions to take, but rather a myriad of unconscious processes in our brains result in a "decision" that our conscious mind becomes aware of later on.

This discovery means we must reconsider the foundations of many of our social institutions.

For example, our criminal justice system is based on the fundamental assumption that people have control over their actions. When someone commits murder, the system assumes they made a decision and carried it out all on their own, so they deserve to be punished. But what if they never truly _decided_ to commit murder?

Clearly, it must be considered whether such institutions need to be changed now that we know people may not really be making the decisions they think they are.

### 10. Final Summary 

The key message in this book:

**There's nothing supernatural about morality; it arises from physical and chemical reactions in our brain. To find right and wrong answers to moral questions, we can look at whether the answers increase or decrease "well-being." However, if we think of morality as a landscape with peaks and valleys, we see that there are many possible "correct" answers to moral questions.**

Actionable advice:

**Be wary of your moral intuition.**

If you want to live a morally consistent life, you need to be aware of your biases with regard to moral decisions. The more personally involved you have to be in order to act upon a decision (e.g., firing someone at work), the more difficult it will be for you to think objectively in terms of the greater good. Thus, if you find yourself in such a moral dilemma, try imagining what you'd think about it if it were happening to someone else. This can help you regain a more objective view.
---

### Sam Harris

Sam Harris has authored a number of the New York Times bestsellers, such as _The End of Faith_, _Letter to a Christian Nation_ and _Free Will_. In addition, Harris is co-founder and CEO of the nonprofit foundation Project Reason, which aims to promote scientific knowledge and secular values within society.

