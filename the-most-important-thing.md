---
id: 544cd0596563320008000000
slug: the-most-important-thing-en
published_date: 2014-10-27T00:00:00.000+00:00
author: Howard Marks
title: The Most Important Thing
subtitle: Uncommon Sense for the Thoughtful Investor
main_color: 478A61
text_color: 3B7351
---

# The Most Important Thing

_Uncommon Sense for the Thoughtful Investor_

**Howard Marks**

In _The Most Important Thing_, Howard Marks outlines the sometimes controversial investment philosophy that he developed and honed through many years of market experience. In his view, successful investment requires us to pay thoughtful attention to many different aspects of the current market, and too often use that information to counter the predominant trends.

---
### 1. What’s in it for me? Learn how to achieve investment success by understanding market cycles. 

_The Most Important Thing_ will show you how to understand the current market in order to make good financial decisions and avoid common pitfalls that ensnare so many investors. Rather than offering a manual with step-by-step instructions, these blinks outline a thought-provoking and sometimes controversial investment philosophy.

In these blinks, you'll learn:

  * Why most commonly-held ideas pertaining to the current market are usually wrong.

  * Why market forecasts are typically useless, even when they're right.

  * Why psychology plays such a powerful role in market cycles.

  * Why a dumb financial decision can sometimes make an investor look like a genius.

### 2. Since opportunities to beat the market are rare, successful investing requires perceptive thinking. 

If you want to become a successful investor, you need solid grounding in the basics.

So let's start with a simple definition: _investing_ consists of putting money into assets and expecting an increase in value.

Taking it a step further, _successful investing_ means buying _mispriced assets_ (assets that are priced too low), selling them later at a higher price and earning a profit.

It sounds simple, but mispricings are rare. Why? Because most of the time, there are thousands of participants actively gathering information about various assets and evaluating them diligently. When this thorough assessment is indeed the case, the asset's price doesn't stray far from its _intrinsic value_ — that is, the price the asset is actually worth.

And when an asset is priced appropriately, which is typically the case, it's hard to profit from it.

Still, in practice, mispricings do occur. For example, in January 2000, shares of _Yahoo_ sold at $237 apiece. But then by April, share prices had fallen to $11. If you had owned the stock, your investment would have suffered massive losses. And how do we account for this sizable fluctuation? Well, the price must have been wrong on at least one of these occasions.

In general, mispricings make profits and losses possible in a big way. But as we mentioned, detecting these mispricings is a difficult task.

So if your investment goal is to earn higher-than-average returns, your thinking has to be different and better than everyone else's.

This is called _second-level thinking_, and it works like this: first-level thinking says, "It's a good company; let's buy the stock." Second-level thinking goes beyond the conventional wisdom by saying, "It's a good company but _everyone_ thinks that, so the stock is probably overrated and overpriced; let's sell."

This approach is so effective because it acknowledges the fact that all the investors put together actually _make_ the market. Second-level thinking takes all these other investors into account in order to beat the market, not just submit to it.

> _"Investing, like economics, is more art than science."_

### 3. Understanding the relationship between value and price is the cornerstone of successful investing. 

The oldest rule in investing is also the simplest: "Buy low, sell high." That might seem blindingly obvious, until you try to put it in practice. And then you start to wonder, what's low and what's high? To answer that, there has to be some objective standard. And for our purposes, the most useful one is the asset's intrinsic value.

In fact, coming up with an accurate estimate of intrinsic value is the ideal starting point for successful investing. You would do this by analyzing a company's general attributes, which are called _fundamentals_. Fundamentals analysis involves asking questions pertaining to the company's economic well-being — such as, is the company earning a profit? Can it repay its debts?

Making this kind of accurate estimate of intrinsic value is the cornerstone of successful investing, because it will then allow you to spot (and buy) assets when the current price is lower than their intrinsic value.

The relationship between current price and intrinsic value is crucial, and yet many overlook it. Some people say, "We only buy asset A," or "A is a superior asset class." These statements sound a lot like, "We'd buy this asset at _any_ price."

But that's not a smart way to make financial decisions. In other words, if someone offered to sell you her car, you'd find out the price before buying it. It works the same way with assets.

So, to be sure that the price is right in relation to the asset's value, a prospective buyer should also look at two other factors: psychology and _technicals_.

Technicals have nothing to do with value. They occur, for example, when a market crashes and, in order to avoid total bankruptcy, investors sell regardless of price.

Psychological factors, like greed or fear, also exert a powerful influence on price. Consider collective fear of buying, which occurs sometimes during economic instability; since no one buys it, the asset's price is too low relative to its intrinsic value.

### 4. Dealing with risk is an essential aspect of investing. 

Investing requires us to deal with the future. But since we can't predict what will happen with any real certainty, risk is unavoidable.

Investment risk is always present, even when it's hard to recognize. This is especially true when prices are high, since they're ever more likely to fall. High prices are due to excessive optimism and insufficient skepticism that prices will remain high. So perhaps paradoxically, the prime element of risk creation is the belief that risk is low — maybe even non-existent.

But this isn't actually true. In fact, as long as prices can fall, risk persists. And as we discussed above, in some ways risk becomes even more dangerous in good times, when the possibility of loss is hardest to imagine.

For example, it's hard to truly estimate the risk of buying a home in California. The house may or may not have construction flaws that would cause it to collapse due to an earthquake. Is there a way to find out if this is the case? Wait around for an earthquake, and see if the house collapses.

This instance illustrates a general principle: Risk is only really observable when negative events create great losses.

And yet, as we outlined above, risk is an unavoidable aspect of the investment process. Thus, serious risk assessment is necessary to make wise investment decisions.

Since most of us are naturally risk averse, whenever we're thinking about taking an investment, we have to consider whether it justifies the risk.

Risk assessment is especially important because ultimately, the amount of money you gain or lose in a particular investment doesn't say anything about the level of risk you took initially. That's why the risk has to be assessed on its own — apart from the investment's intrinsic value or any other factors.

> _"Good times teach only bad lessons: that you know its secrets, and that you needn't worry about risk."_

### 5. Swooping in at the bottom of a market cycle yields the greatest opportunities for gain. 

Although there are very few sure things, when it comes to the world of investments, there are two rules we can always count on.

_Rule number one_ : Most things are cyclical, due to the nature of humanity. Mechanical things move in a straight line, but humans don't. People are emotional, inconsistent and mutable. And more specifically, when investors become emotional, it creates cyclical patterns in markets.

Consider the credit cycle — which is the increase and decrease of access to credit over time. During a period of prosperity, when bad news is scarce, risk awareness totally disappears. As a result, banks give access to credit with very low restrictions. In extreme instances, banks will even finance borrowers that aren't worthy of being financed.

Then, when losses occur, banks get discouraged. Risk awareness rises, and along with it, so do credit restrictions. Starved for capital and with a lack of credit, some companies face bankruptcy.

And then at that point, the process reverses: with fewer competitors offering customers lines of credit, banks can demand high percentage returns and better creditworthiness. Over time, better standards lead to prosperity and on it goes, _ad infinitum_. This scenario exemplifies why processes involving humans tend to be cyclical.

This leads us to _rule number two_ : Some of the greatest opportunities for gain and loss arise when others forget about rule number one.

Ignoring the cyclical nature of markets and extrapolating trends is dangerous from an investment standpoint. And in fact, this kind of thinking is precisely what leads to bubbles: Buyers don't worry about whether a stock is overpriced because they're so sure that someone else will buy it from them.

But eventually, the cycle reverses and the market crashes. When this happens, buyers may be forced to sell regardless of price in order to avoid bankruptcy.

And for an investor, _this_ is the perfect moment of opportunity: There's no better investment than buying from someone who has to sell, regardless of price, during a crash.

### 6. If you want to make successful investments, buck the trend and seek out unusual bargains. 

Most investors tend to follow the trends. Superior investors, however, do the exact opposite.

In fact, bucking trends is the key to successful investment, because crowds make errors with an almost mathematical regularity, creating insane market turbulence.

Markets swing from overpriced to underpriced constantly: When more people want to buy than sell, the market rises; when the market rises, even more people become buyers, inflating the market even more. The value of the asset hasn't changed, but the cost has gone up immensely.

Figuratively speaking, the top of the market occurs when the last person buys the stock. At this point, the market cannot climb any higher, so it begins to plummet as soon as one of the buyers moves to sell her stock.

The cause of these wild extremes? The so-called wisdom of the crowd, which creates (and inflates) these overpriced or underpriced assets.

For this reason, the most profitable investment decisions are by definition contrarian: Buy when everyone else sells and vice versa.

Bargains are an investment holy grail and since, as outlined above, they're usually based on irrationality or an incomplete understanding of the market, a good place to start looking for them is among assets that are controversial or scary, unpopular or unknown.

Make it your goal to find underpriced assets which are perceived to be considerably worse than what they really are.

In other words, if nobody owns something, demand for it can only go up. And if the asset goes from taboo to even just tolerated, it will perform well for you.

Although it might feel uncomfortable at first, the best opportunities exist where others wouldn't dare go.

### 7. Since market forecasts are mostly useless, investors should have a good understanding of the present economic situation. 

If you follow market forecasts, then you know that these investing tools can sometimes be right.

However, the key question isn't whether they're sometimes right, but whether forecasts consistently provide actionable and valuable information.

And frankly, the answer is no — market forecasts are of little value.

You might notice that sometimes when people predict the future, it looks like the recent past. That doesn't mean the prediction is wrong; in fact, most of the the time future repeats the past.

But once in a while, things turn out very differently. Predictions about market movements are most useful when they correctly anticipate real change, since it's only at these moments that we stand to lose or earn a lot of money. But unfortunately, forecasts are least likely to correctly predict real market change.

Consider the fact that few forecasts correctly predicted the global credit crisis and massive economic meltdown of 2008. A year later, forecasters were equally confounded by the first signs of economic recovery.

Therefore, since forecasts are of little value, investors need to have a good sense of the _present_ market situation. No one can predict the future, but smart investors will work to understand the given moment in terms of market cycles.

Figuring out where we stand in a market cycle gives us valuable insight into future events, but it doesn't mean we can ever know exactly what's coming next. It's impossible to know future, but it's not that hard to understand the present.

All we need to do is "take the market's temperature," so to speak. We might ask: are investors optimistic or pessimistic? Does the media say we should buy stock, or avoid the market altogether? These valuable questions will help you determine a smart course of action

### 8. To achieve investment success, avoid analytical and psychological sources of error. 

When it comes to analyzing the financial market, plenty of people are going to follow similar lines of reasoning and reach similar conclusions. And yet, people who arrive at the exact same conclusion often behave differently. That's because psychological factors don't influence everyone the same way.

In fact, some of the biggest investment errors come from these psychological factors, like greed, fear, ego and envy. These forces can drive us to pursue high returns by accepting high levels of risk.

Greed is perhaps the most powerful of these psychological forces. It's strong enough to vanquish our aversion to risk, caution and other elements that usually keep us out of trouble financially.

Like greed, fear can also act powerfully, causing us to make serious investment errors. Although you might think this emotion corresponds to risk aversion, in fact, fear can manifest as panic, preventing us from taking constructive and necessary action when we should.

In addition to these psychological sources of error, investors can also make analytical mistakes. These are more straightforward, occurring when we don't have enough information, applying the wrong analytical processes, making computational errors or accidentally omitting something pertinent.

Analytical mistakes are relatively easy to avoid — it's a matter of being careful. Furthermore, in order to achieve investment success, we must also avoid making the kinds of psychological errors we outlined above.

Why are these emotional factors so sticky? Investment usually involves high levels of risk, especially if we're seeking high returns. And striving for such great success sometimes results in failure, which can be crippling.

On the other hand, trying to avoid losses by taking on less risk might not lead to astounding returns, but it won't bankrupt you. And ultimately, if you care about protecting your money, trying to avoid losses is better than striving for phenomenal results.

### 9. Investment outcomes often hinge on random events, so find an investment approach that anticipates various scenarios. 

Every now and then, someone takes a huge risk that pays off. And although it's great to be on the receiving end of such a scenario, it's worth remembering that winning a gamble is the result of luck and boldness, not skill.

Indeed, luck plays a huge part in investment. When it comes to the markets, a great deal of success is a matter of being in the right place at the right time. And moreover, success can also entail being in the right place for the wrong reasons. In fact, outcomes often hinge on totally random events.

For example, someone buys a stock because she expects a certain development; that development doesn't occur, but the stock performs well anyway — which makes the investor look super insightful, even though she was just lucky.

As you can see, it's important to create an investment approach that anticipates random events, whether lucky or unlucky. That's why investors should commit to a strategy that will serve them through a variety of scenarios.

This consists of finding the right balance between _offensive_ and _defensive_ investment tactics. An offensive tactic means accepting high levels of risk in pursuit of gains; on the other hand, defensive tactics build on the avoidance of losses, rather than the risk for profits.

Since few people have the ability to switch tactics to match market conditions on a timely basis, a mix of these two approaches is what will best serve you through a variety of scenarios.

> _"There are old investors, and there are bold investors, but there are no old bold investors."_

### 10. Final summary 

The key message in this book:

**Successful investing requires a deep understanding of both the technical and psychological forces that create market cycles. There are many different elements in play, including plenty of risk; thus, dealing with risk is a crucial part of the investment process. Ultimately, successful investing requires us to recognize and avoid common pitfalls.**

Actionable advice:

**Before making any investment decision, start by "taking the market's temperature."**

The next time you sit down with your financial portfolio, ask yourself key questions to understand the current moment in the market.

You might ask: are investors optimistic or pessimistic? Does the media say we should buy lots of stocks, or avoid the market altogether?

These kinds of questions will give you insight into the market's possible future course — if things are up right now, then there's a high likelihood they'll come down, and vice versa. Understanding this should help you determine a smart course of action with regards to your own investments.

**Suggested** **further** **reading:** ** _The Black Swan_** **by Nassim Nicholas Taleb**

_The Black Swan_ offers insights into perceived randomness and the limitations we face in making predictions. Our over-reliance on methods that appeal to our intuition at the expense of accuracy, our basic inability to understand and define randomness, and even our biology itself all contribute to poor decision making, and sometimes to "Black Swans" — events thought to be impossible that redefine our understanding of the world
---

### Howard Marks

Howard Marks is chairman and cofounder of the Los Angeles-based investment firm, _Oaktree Capital Management_.

