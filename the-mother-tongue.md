---
id: 58da625a232de90004a6e64e
slug: the-mother-tongue-en
published_date: 2017-03-30T00:00:00.000+00:00
author: Bill Bryson
title: The Mother Tongue
subtitle: English And How It Got That Way
main_color: 85A2DC
text_color: 55678C
---

# The Mother Tongue

_English And How It Got That Way_

**Bill Bryson**

_The Mother Tongue_ (1990) provides a unique and personal look at the history of the English language. You'll learn how, thanks to its flexibility and adaptability, English has endured and flourished, despite centuries of invasions, uprisings and censorship.

---
### 1. What’s in it for me? Discover the rich history of the English language. 

When we use English in everyday life — while talking to a friend or writing an email or swearing at someone in traffic — we rarely stop to think about where all the words we use come from. But each and every word is part of a detailed and curious history — the history of the English language itself.

These blinks take you back through time and trace the origins of the many words we use every day. Along the way, you'll learn a great deal about the history of Western civilization, too.

You'll also find out

  * what names people came up with for things they'd never seen before;

  * who invented the most words in the English language; and

  * which words English speakers share with Scandinavians.

### 2. Many of the world’s languages can be traced back to a common ancestral language. 

Many people will tell you that today's world is more connected than ever before. This may well be the case, but that doesn't mean that connectedness is anything new. One age-old indicator of a near-global connection is language. Take the word "brother." In German it's "bruder," in Sanskrit it's "bhrata," and in Persian it's "biradar."

Why are these words so similar? An eighteenth-century English judge wondered the same thing — and his attempt to answer that question essentially launched the field of historical linguistics.

Sir William Jones was working in India when he took up the unusual hobby of learning Sanskrit.

The language had already been dead for hundreds of years, yet it managed to survive thanks to the priests who'd memorized certain hymns, called the Vedas. Though the priests were ignorant of the words' meanings, they managed to pass them down from one generation to the next.

As Jones studied these texts, he began to recognize unmistakable similarities between Sanskrit and the European languages. In Latin, for example, "king" is "rex," and in Sanskrit it's "raja." And Sanskrit for the English word "birch" is "bhurja."

Once he'd noticed these similarities, Jones began comparing other languages to Sanskrit, and he found more and more evidence for a budding theory: that a wide variety of classical languages — Persian, Latin, Celtic, Sanskrit, Greek — had their roots in a parent language.

Eventually, Jones presented his theory in Calcutta. This presentation gave birth to a whole new field of scholarship. European scholars began conducting research of their own and, in the end, they agreed with Jones. They named the parent language _Indo-European_.

Deducing the existence of Indo-European is an impressive feat of historical linguistics. The speakers of this language would have only been alive during the Stone Age (around 7000 BC) and there are no traces of Indo-European writing. Nonetheless, scholars have offered convincing hypotheses about these people's lives, solely based on common words in the descendent languages.

Since the words for "snow" and "cold" are similar, we can deduce that the Indo-Europeans didn't live in tropical climates.

Likewise, since there is no common word for "sea," they likely began as inland tribes. And when they migrated to the coast, they invented their own separate words for the ocean.

### 3. Repeated conquests of the British Isles changed and expanded the English language. 

Nowadays, it's not uncommon to hear English words pop up in other European languages. But this wasn't always the case — in fact, historically speaking, English tended to be the main adopter of foreign words.

There are a few major evolutionary points in the development of the English language, and the first came when two Germanic tribes — the Angles and the Saxons — migrated to Britain.

These tribes left their lands in northern Germany, near Denmark, and crossed the North Sea to Britain, after the Romans had vacated the area around 450 AD _._ Once they arrived, they displaced the Celts and began developing the English language.

Then came the Viking invasion of 850 AD.

It began when 350 Viking warships left Scandinavia and sailed up the river Thames. Thus began a battle that would rage for almost three decades, until the English finally settled things in 878 AD.

Afterward, an area of Britain called the Danelaw was established to divide the nation between the southern English and the northern Vikings.

This arrangement had lasting linguistic effects. The English language absorbed many Scandinavian words and terms, like "window," which comes from the Old Norse word _vindauga_, meaning "wind eye," and the names of more than 1,400 places in northern England are of Scandinavian origin. Eventually, Old Norse and Old English merged into one language.

Next came the Norman conquest of 1066.

The Normans were a tribe of Vikings who had settled in northern France two centuries earlier, and their invasion of England ushered in a new era. They maintained control of the country for 300 years and added over 10,000 words to the English language.

Under Norman rule, a two-tiered society emerged. There was a ruling class, which spoke French, and a working class, which spoke English. Anglo-Saxon words like "baker" and "miller" were used to describe jobs of labor, and French words like "painter" and "tailor" were used for skilled trades.

Interestingly, when an animal lived on a farm, it went by its English name — such as "cow" or "pig" — but once it was cooked and served, its name changed to the more French "beef" or "bacon."

### 4. As English evolved, new words were added and old ones took on new meanings. 

The word "manufacture" used to mean making something by hand, since it is derived from the Latin word for hand. However, it has since taken on a new meaning and now refers to machine-made products.

This kind of transformation isn't uncommon. Of all the words in the English language that come from Latin, over half of them have changed with time.

Take the word "brave," for instance. It was once a near synonym for "cowardly," since it's related to "bravado," which implies false courage and shares a Latin root with the word "depraved."

In other instances, words will gain new meanings while retaining the old ones. The word "set" has grown to take on 58 definitions as a noun and 126 definitions as a verb!

Then there is the introduction of new words, often by creative individuals.

One of the most plentiful periods spanned the years between 1500 and 1650, when more than ten thousand new words were brought into the English lexicon.

Shakespeare is responsible for over a thousand of these coinages, including "leapfrog," "excellent," "lonely" and "majestic." Another contributor was Isaac Newton, who provided the English language with such words as "centrifugal."

The invention of new words was usually a matter of adding or subtracting prefixes and suffixes from already established words.

In English, it's pretty easy to reverse a word's meaning or to create an adjective just by changing that word's beginning or ending. For instance, we can make "visible" into "invisible" or "labyrinth" into "labyrinthine."

Oddly enough, an added prefix can sometimes end up creating a word with the same meaning, such as "habitable" and "inhabitable," both of which tell us that something is suitable to live in.

New words are also created by simplifying and shortening longer words.

After all, it's easier to say you're going to the gym, rather than the gymnasium. In other cases it's a matter of shortening Latin phrases like _mobile vulgus_, which means "fickle crowd," into a simple and handy word like "mob."

### 5. Words were created in the New World through adaptation and “America” came about through a misunderstanding. 

As the saying goes, "Necessity is the mother of invention" — and this certainly holds true for the English language. When settlers arrived in the Americas, they encountered a whole world of strange new plants and animals, each of which required immediate naming.

Of course, these animals and plants already had names — Native American names — and the settlers were quick to introduce them to the English language.

The word "hickory" came from _pawcohiccora_, which was used by the Algonquin tribe in Canada. And "squash," "raccoon" and "hammock" are just a few of the many words that can be traced back to different Native American tribes.

Other words, such as "mustang" and "canyon," were taken from Spanish settlers, whereas "landscape" and "prairie" were taken from Dutch and French terms, respectively.

Then there are compound words — single words that were once two separate words. Such words, like "rattlesnake," "eggplant," "grasshopper" and "catfish," are often particularly vivid and perfectly descriptive.

Nicknames are also extremely evocative, though they vary from country to country. So if you're playing a slot machine in a British pub, you're using a "fruit machine," which, in the United States, could be called a "one-armed bandit."

Obviously, this can lead to misunderstandings. But, actually, at least one extremely common word resulted from a major misunderstanding.

The word "America" derives from the name of a fifteenth-century Italian explorer, Amerigo Vespucci, who made four trips across the Atlantic Ocean, but actually never laid eyes on North America.

Later, a German mapmaker, believing that Vespucci had discovered the entire continent, named the new country "Americus Vespucius," in the explorer's honor.

But by the time the error was discovered, it was too late. The name was already in wide circulation and the name that Vespucci proposed — "Mundus Novus" — never caught on.

> _'In Britain the Royal Mail delivers the post, not the mail, while in America the Postal Service delivers the mail, not the post.'_

### 6. Our last names can reveal information about our family’s past. 

If you know someone with the surname of Smith, Schmidt, Herrero, Kovacs or Ferraro, then it's quite likely that their ancestor worked as metalsmiths.

But this is just one way in which our last names can provide insight into our ancestry. Another detail a last name can reveal is where our ancestors lived.

Long ago, Englanders would go by names like James of Preston, but these were eventually simplified to skip the preposition, leaving just James Preston.

The purpose of a surname was to distinguish one person from the rest, which is why the last name "London" is less common than, say, "Worthington"; obviously, it made no sense to have a crowded city filled with people with the same last name. But if a Londoner did leave for the country, he might then take a name like John of London.

Other English surnames can be traced back to a descriptive nickname, such as Armstrong — just as other parts of the world would use names like Tolstoy ("fat") and Gorky ("bitter").

Then there are what's known as _patronymics_, surnames such as Peterson or Johnson, that indicate a lineage. What was once "Charles, son of John," eventually became Charles Johnson.

Finally, there are the trade names like Smith and Baker. Many of these are still obvious, though other professions have since died out. For instance, "Fletcher" refers to a person who makes bows and arrows, and "Bateman" is an adulteration of boatman.

While these methods made it easy to distinguish the village's two Peters from one another, they didn't make it easy to track multiple generations. This changed in 1413, however, with the introduction of medieval bureaucracy in England.

At this point, everyone was required to have legal documentation giving his name, occupation and place of residence. In other words, they had to pick a surname and stick to it.

### 7. Words can be perfectly acceptable in one century and profane swear words in the next. 

In Hollywood, there's a list of around seventeen words that can automatically earn a movie an R rating.

But such lists are never set in stone. What we consider vulgar or profane tends to shift over time.

There was a time when today's most offensive swear words were used with utter casualness in writing and conversation. They were even on public display. Before new attitudes about vulgarity emerged in the eighteenth century, one London street, known for its prostitutes, was called "Gropecuntlane."

Words can also lose their vulgar connotations. Many words that we now consider harmless or even cute could have gotten you in trouble in the past.

If you called someone a "puppy" in the nineteenth century, you might get a slap, given that the word referred to arrogant and unpleasant youth.

And, of course, blasphemous language was once considered far more offensive than it is today.

In 1623, the English Parliament made it illegal to swear and threatened to fine people who used words like "Jesus" and "hell," or even seemingly harmless phrases like "upon my life." Twenty years later, the laws grew even stricter, and swearing at your parents could lead to a death sentence!

But the most widespread case of extreme primness started in the nineteenth century, when England's morality censors tried to sanitize language, going so far as to remove swear words from literature.

Even the relatively clean works of Shakespeare weren't safe from these censors; they made sure the word "damned" was plucked out of every play.

Americans were even more prudish.

For instance, in nineteenth-century America, merely mentioning a woman's legs was considered a social faux pas. Even parts of chickens were deemed unmentionable, which is how the term "white meat" came into existence. Calling it "breast meat" was too scandalous.

As history shows, whether times are oppressive, liberating or just a tad uptight, the English language will continue to grow and find creative ways to flourish.

### 8. Final summary 

The key message in this book:

**English is a dynamic, vigorous and flexible language that has withstood the test of time. The many periods of dramatic upheaval that it's endured have enriched it immeasurably, highlighting its ability to absorb foreign words and influence foreign tongues.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _At Home_** **by Bill Bryson**

At _Home_ (2010) offers an in-depth look at the history of the home. These blinks walk you through stories that each "take place" in a different room in a house, explaining the history of spaces such as a bathroom or kitchen. Interestingly, you'll explore how each space evolved into the rooms we live in today.
---

### Bill Bryson

Bill Bryson, a prolific and witty author, has written over 20 books on travel and language, including the bestseller, _A Short History of Nearly Everything_.

