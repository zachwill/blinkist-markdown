---
id: 55101ca6666564000a400000
slug: the-motivation-manifesto-en
published_date: 2015-03-24T00:00:00.000+00:00
author: Brendon Burchard
title: The Motivation Manifesto
subtitle: Declarations to Claim Your Personal Power
main_color: EFD244
text_color: 807024
---

# The Motivation Manifesto

_Declarations to Claim Your Personal Power_

**Brendon Burchard**

_The Motivation Manifesto_ (2014) explains the fundamental driving forces of human nature and how these either help us realize or keep us from our life's goals. In a few easy steps, you'll learn how to inspire and increase your own levels of motivation to live a happier life.

---
### 1. What’s in it for me? Learn how to become more motivated and get more out of life. 

Why do you get up in the morning? What inspires you to greet the day and live a fulfilling life?

In a word, motivation. Motivation is the power to achieve the things you want in life. You need to be motivated to pursue anything, from simple daily goals to lifelong dreams. People who are deeply motivated are also influential, achieving their own goals while motivating others.

These blinks teach you how you too can become both motivated and a powerful motivator, but what you'll find here isn't just another feel-good motivational rant. This is a manifesto — a call to make a serious commitment to finding a focused purpose and pursuing the things you love.

In the following blinks, you'll discover

  * why being fearful is like keeping a wolf as a house pet;

  * why you should never feel "just fine"; and

  * how to recapture the wide-eyed joy of childhood.

### 2. A yearning for freedom and the burden of fear are humankind’s most fundamental driving forces. 

Does it ever seem to you like there's always something stopping you from being completely free?

If so, you're not alone. We all feel frustrated when we sense our freedom is curtailed.

But why do we feel this way?

Without personal freedom, we can't realize our potential as an individual or as a society. And when we stop demanding the freedom we need, life rapidly loses all its vigor and drive and we become slaves to the wants and expectations of others.

This lack of freedom can easily spiral out of control, doing serious damage not only to people but also to entire societies. Just think of atrocities such as the Holocaust, in which many people stood idly by in fear as thousands had their freedom cruelly taken from them.

Even today many of us still feel oppressed, worrying about what other people think and afraid to pursue what we really want from life. 

So how can you overcome your obstacles to reach whole new levels of joy and fulfillment?

The only way to attain personal freedom is to overcome _fear._ Fear is a powerful part of the human psyche. In our evolutionary past, fear served us well, keeping us from danger.

But how does fear benefit us today, in the modern world?

You might hear people say, "Fear is natural," or "Fear makes you work harder." Sure, this may be true at times. But in most cases it isn't.

In fact, pretending that fear is our friend is rather like having a wolf as a pet. Sooner or later, this wild animal will turn on us and eat us alive!

### 3. Motivation needs fuel to burn hot. It is in your power to keep that fire burning! 

Ever met someone brimming with energy, full of purpose and life? You might think they were born that way, but this is simply not true.

We can all pump up our motivation by first reflecting on our true _ambition_ s.

Ambition is simply the choice to seek out something greater in life, through more experience or higher achievement. Without ambition, your life is nothing but a ship, drifting aimlessly over a vast sea. But with ambition, you can set goals that you really want to reach.

So how do you kindle your ambition? You need to always ask yourself questions. For example, if you've been toiling away at a job for too long, ask yourself: "Is this the best job I could have?" Or if you feel trapped in a stagnant relationship, ask yourself: "Is this the best marriage I could have?"

Asking yourself questions can draw your attention to ambitions you might otherwise ignore.

It's also important to remember that having ambition is different than having hope, the difference being a question of expectancy. But what does that mean, exactly? 

Say you dream of completing a triathlon. While daunting, you've always _hoped_ to get in enough shape to one day achieve it. But with this perspective, you'll never make it.

Hope just isn't enough — you have to visualize yourself in the water, on your bike, in your new running shoes, actually _participating_ and in the end, crossing the finish line.

In other words, _you have to expect that you will succeed_. This truly is the spark of motivation!

Yet once you've kindled your motivation, you need to keep it burning. How do you do this?

Motivation needs fuel, and that fuel is continuous effort. We all have days when we'd rather curl up in bed and do nothing. But through developing solid routines and surrounding ourselves with strong support systems, you'll be able to jump out of bed and into your running shoes — even when you'd rather hit the snooze button!

> _"We feel motivated because we choose to, not because the sun happens to shine on our side of the street."_

### 4. Stop looking over your shoulder at what was. Concentrate on what is now, and practice happiness. 

Children live in the moment, looking at the world as it is now, seizing every second to explore and play and push boundaries. It is an exhilarating and freeing perspective on life!

Yet as we become adults, we often lose this ability to see the world in this way.

Too many of us are obsessed with the past. Some people are always looking backwards, to what they believe was a happier time, thinking, "If only life could be like that again, I'd be happier."

Others see the past as the cause of all of today's problems, and spend hours thinking, "If only that hadn't happened, I'd be happier."

With such a perspective, the present moment is tainted by the sadness of _what could have been_. Obsessing about the past closes us off from the world and drains our motivation.

So how can you break this vicious cycle of sadness and regret?

Ask yourself, "What in my life could I focus on right now to feel enthusiastic and appreciative?" Don't just do it once — make it a mantra that you repeat. The more you consider this question, the easier it will be to truly live in the moment.

With this approach, being happy is something you can practice. If you want to live life to the fullest, you've got to work at being joyful and curious!

Happiness isn't something that "just happens," a state of being outside of your control. Thinking this way pushes you toward negativity, or dark feelings that can be contagious.

Yet negativity isn't the only thing that's contagious; happiness is as well!

Think about the last time you spent an afternoon surrounded by children playing — you won't find better teachers of joy. Learn to ask: "What if I tried every day to look at the world with the curiosity and presence of a child?"

> _"The present is all there is."_

### 5. A little humble faith will help slay your inner demons. A little reckless inspiration helps a lot, too. 

We've all got inner demons. They lurk in the shadows of our minds, keeping us from achieving what we really want.

Fortunately, these monsters can be conquered. All we need are the right weapons.

When starting a new project, you might ask, "Is this really a good idea?" Or perhaps, "Can I really pull this off?" If we let doubt take over, our motivation can disappear.

There is only one cure: faith. Not an institutionalized faith, but a humble kind of faith that lives in you and strengthens you when you feel doubt. Tell yourself, "If I work hard and dedicate all my efforts to my task, in time I will achieve my dreams."

By having faith in yourself and shaping your thoughts, you will find that it is easier to work steadily toward your goals.

But what if your goals seem impossible, perhaps even a little crazy? Keep in mind that no great innovation has ever come about by taking a safer road. To contribute to the world, sometimes we need to be a little reckless and perhaps a little bit crazy.

But the sort of recklessness you need — the kind that will serve you best — is the willingness to challenge what you take for granted and to keep seeking out something truly new.

To do this, you'll need to realize that you can actually _bend reality to your will_. Your life is not carved in stone. If you put your mind to it, you can change it.

Great men and women throughout centuries have pulled their motivation from this realization!

The U.S. moon landing in the 1960s is a great example. At the time, sending a man to the moon seemed impossible, if not dangerous. But the project was a success, and from that moment, changed the world's idea of what is possible.

### 6. “Fine” isn’t enough. Strive for the fantastic, living on your own terms and inspiring others to follow. 

When you do what you love, it's not just _your_ happiness that grows. You'll also radiate good energy and even inspire others.

Everyone has the potential to become a beacon of hope for other people, and this role is something for which you should strive. But how do you do this?

It's simple. _Do what you are passionate about._

When you're asked how you're feeling, do you often say, "I'm fine." But is _fine_ really the best you can do? Why aren't you _fantastic_, _great_ or _phenomenal_?

If you had more time to do the things you really enjoy and that make you feel proud of yourself, you could live a life that transcends "just fine."

Try asking yourself if the people closest to you, your family, friends and colleagues, know what your true passions are. If they don't, it probably means you're not realizing who you truly are inside.

Don't let your life be dictated by what other people want you to do! If you allow others to pen you in and keep you from your passions, your integrity and dignity will suffer. You need to say "no" to doing anything that goes against your passions, no matter what the price.

Today's world is full of conflict and poverty. Yet at the same time, apathy is widespread. Ours is a world in desperate need of role models and inspiring leaders. So why look to others to fill this void?

We must realize that we can all play that role; that we can all inspire. 

Yet perhaps you don't think yourself a leader, or capable of telling other people what to do. Remember this: great leaders don't tell people what to do; they _inspire_ them to find their path.

### 7. Final summary 

The key message in this book:

**To remember the key lessons from these blinks, just keep in mind the six F's of motivation: _Forget_ the past, _Fuel_ the fire of your motivation, have _Faith_ in yourself and strive for _Fantastic_ things, never forgetting that ****_Freedom_ and _Fear_ are your primal motivators.**

Actionable advice:

**Be patient!**

Being impatient can drain energy and motivation. When you are feeling impatient, try to ask yourself: "What could I do to calm myself down right now?" or "Why does this kind of situation always make me impatient?" To follow up, ask: "How will I handle a situation like this next time?" Take a deep breath, and you'll feel positive energy flooding back again!

**Suggested further reading:** ** _Drive_** **by Daniel Pink**

In _Drive_, Daniel Pink describes the characteristics of extrinsic and intrinsic motivation. He reveals that many companies rely on extrinsic motivation, even though this is often counterproductive. The book explains clearly how we can best motivate ourselves and others by understanding intrinsic motivation.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Brendon Burchard

Brendon Burchard is a world-famous personal development trainer, bestselling author and founder of personal development program, High Performance Academy. American television host Larry King has called Burchard "one of the top motivation and marketing trainers in the world."

