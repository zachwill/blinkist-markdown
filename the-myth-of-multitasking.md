---
id: 5977927cb238e10005703396
slug: the-myth-of-multitasking-en
published_date: 2017-07-28T00:00:00.000+00:00
author: Dave Crenshaw
title: The Myth of Multitasking
subtitle: How "Doing-It-All" Gets Nothing Done
main_color: BA3A40
text_color: BA3A40
---

# The Myth of Multitasking

_How "Doing-It-All" Gets Nothing Done_

**Dave Crenshaw**

_The Myth of Multitasking_ (2009) reveals an important truth: that the concept of multitasking is a lie that we've been buying into for far too long. The truth is that the human mind is not built to divide its focus, and if you want quality work done in an efficient, timely manner, you need to take things on one at a time.

---
### 1. What’s in it for me? Ditch the multitasking myth. 

How many different apps do you have open on your phone right now? How many tabs on your internet browser? And how many different e-mail threads and Facebook messages?

If you're like most people, you probably have a lot of each of these — too many, in fact. The myth of multitasking has seeped into every aspect of our lives, not only in terms of how we communicate but also the way we work and live the rest of our day-to-day lives.

And multitasking might be a bigger problem than you think; not only is splitting our time into fragments _not_ helpful or efficient, it is actually counterproductive.

In these blinks, you'll learn

  * why "multitasking" isn't even an accurate name for what it describes;

  * why you should set designated office hours; and

  * how you can find more time for the things you love.

### 2. There’s no such thing as multitasking, only active and passive switchtasking. 

These days, with more ways than ever to be connected and communicate with one another, it can feel like there's an endless supply of tasks that demand to be juggled all at once.

Appropriately enough, the traditional way of dealing with multiple tasks at once was to _multitask_.

But here's the thing: multitasking is a lie. A more accurate name for this method of working is _switchtasking_, and it is an inefficient and inadequate way of getting things done.

The human brain is an impressive thing to behold, but it doesn't perform well when you attempt to focus on more than one thing at a time.

There are plenty of studies to back this up, including a recent one from Vanderbilt University. Researchers couldn't find a single piece of neurological evidence to suggest that the human brain is capable of taking on more than one task at a time.

What the brain _can_ do is switch back and forth, from one task to another. It can do this quickly enough to give you the impression that it is multitasking, but what it is really doing is switchtasking.

Now, there are two different kinds of switchtasking: you can either make _active switches_ or _passive switches_.

Active switches happen in situations you create yourself, such as deciding to check your e-mail while talking to someone on the phone. These would be switches that you are actively making.

Passive switches happen in situations that are initiated by something or someone else. An example of this would be when you're facing a deadline and, in the middle of preparing documents, a coworker decides to stop by and start talking to you. Here, you're being asked to switch your attention between the documents and your coworker.

In the blinks ahead, we'll take a closer look at why these switches are so harmful to your work and what you can do to reduce them in the workplace.

### 3. Switchtasking isn’t an effective or efficient way to get things done. 

At one time or another, you may have been proud of your ability to switch between multiple tasks and highlighted this talent on your resume.

But the truth is, switchtasking isn't an effective or time-saving way of working.

For a good example, let's look at a typical workday scenario:

Helen is a hardworking CEO for a successful retail clothing company. But every hour of every day, she'll usually have to deal with multiple interruptions.

A typical scenario would find Helen trying to compose an e-mail when her assistant, Sally, interrupts with an important question. Since Helen needs to get her e-mails out quickly, at first, she'll attempt to continue typing and switch her attention back and forth between Sally and the e-mail.

However, since Sally's question are generally complex, Helen will eventually be forced to stop typing and give the question her full attention in order to come up with an answer. Once she gives Sally an answer, Helen can switch back to the e-mail, but it is important to recognize that it will take her a few minutes before she can get back to the level of focus she had previously.

This is why switching between tasks is ultimately inefficient and a waste of time, because whether you are making active or passive switches, you will inevitably need to stop one train of thought to start another.

When Helen realized she couldn't give her e-mail _or_ Sally's question the attention they each required and deserved, she had to stop what she was doing. And no matter what, switching takes extra time to focus on the new task and then refocus on the old task, both of which eat up valuable time from the day.

You're probably wondering, "How can I avoid switching? Aren't these interruptions just a fact of life?"

In the blinks ahead we'll find out how you can minimize the amount of switching in your day, and maximize the efficiency of your work.

### 4. Avoid interruptions by making yourself available to employees and coworkers at regularly scheduled times. 

Let's say you're the busy CEO of a successful company. What steps could you take to reduce interruptions and minimize switches?

One of the most common reasons for someone to come knocking on your door is that they don't know when they'll be able to get your attention otherwise.

The solution here is to set a schedule of _recurring meetings_ with the coworkers who need your input the most. This way, they'll know that they can save their questions until the next scheduled meeting and will get their answer soon enough. And since they won't have to knock on your door, you'll get uninterrupted time to focus on your work between these meetings.

For our beleaguered CEO, Helen, once she set up a daily meeting with Sally at 10:00 a.m. every morning, she no longer had to hear Sally knock on her door a dozen times every day.

Another way to keep interruptions at bay is to set a regular period when your office door is open to any employees who may need your help.

Most stores aren't open 24/7, and that works out just fine because customers are well aware of when they can show up to get what they need. The same concept can work for your office: set up regular office hours during which anyone can show up to ask questions or get the clarifications they need.

Be clear about these times by posting the hours on your door and sending a mass email to everyone in your company or organization.

You can also take a similar step to take care of those annoying phone calls: leave an outgoing message on your voicemail that tells callers when they can expect to get in touch with you.

For example, you could leave a message that says something like, "I'm currently unavailable, as I'm either in a meeting or with a customer. Please know that I check my messages daily at 10:00 a.m., 2:00 p.m. and 4:00 p.m., so if you leave a brief message, I will get back to you shortly."

### 5. By budgeting your time, you can take control of your schedule and prioritize what’s important. 

Think back to last week and ask yourself, "How many hours did I spend watching TV, sleeping or commuting to and from work — and how many did I actually spend working?"

Even if you're passionate about your work, you might not be aware of how you really spend your time.

This is what Helen, our clothing company CEO, realized after meeting with a business consultant who asked her to add up her weekly activities. Helen estimated that, alongside many other tasks, she spent 49 hours sleeping, 28 hours with her family and 70 hours per week working.

But this was impossible since there are only 168 hours in a week and all of Helen's estimates added up to 190.

This revealed a common misconception most people have about how they spend their time. When Helen reconsidered, she realized that she'd overestimated the amount of time she spent with her family. Even when she was at home, Helen was often still working by responding to emails and reading trade magazines.

By switchtasking between her family and her work, Helen had given herself the illusion of spending more hours with her family, even though she wasn't really present during this time. This realization left Helen devastated because she loved her family dearly and wanted to prioritize them above her work.

So, if you want to make sure that you're spending your time when and where you want to, create a schedule and make a time budget for the future.

First of all, be honest and accurate about how you're currently spending your time, and write your activities down on the schedule.

This should help you identify the areas that you would like to improve upon, whether it's finding more time for family and friends, or perhaps putting more hours toward a new project that you'd like to focus on.

Next, you can make a future time budget. If you want to spend more time exercising or with your kids, block off this time so that it will be devoted to what's important to you.

### 6. Rather than forcing change, set an example and let your employees follow. 

Let's put on the CEO hat one more time and find out what can be done to get your employees to stop switchtasking and start focusing on one task at a time.

Your first instinct might be to hold a meeting and simply tell everyone to stop switchtasking — but there's a more effective way to change people's habits.

Since the idea of multitasking has been idealized for so long, getting people to recognize the better way and change their habits should be seen as a process.

You can do this by setting an example and allowing your employees to see firsthand how inefficient switchtasking is and how much time they can save by focusing on tasks one at a time.

So don't force change among employees. Establish a personal system that will go on to influence the larger business system.

As the CEO or division head, your personal system might be different from how others operate, but once it becomes clear that it provides superior results, it will become the accepted way of doing business.

So start implementing the anti-switchtasking changes from the previous blinks: set regularly scheduled meetings, post regular office hours and start managing your emails and phone calls in a focused and deliberate manner.

All of these policies will soon influence broader business protocols in a positive way.

Employees will notice that your way of operating is reliable and consistent, while also making you available and responsive to employees when necessary.

Before long, they'll be implementing these policies in their own work.

In their interactions with you, it will be clear how effective it is to focus on one task at a time. And when they inevitably begin to adopt this approach themselves, it will be even more effective because it wasn't forced upon them and they made the choice to do so themselves.

Once this happens, you'll have a highly efficient business on your hands.

### 7. Final summary 

The key message in this book:

**It is time to close the book on multitasking. This flawed notion of being able to do everything at once has been a drain on workplaces for far too long. The fact of the matter is that dividing our attention among more than one task at a time is simply impossible; what we are really doing is switching back and forth between activities and tasks and, as a result, are spending more time getting less done.**

Actionable advice:

**Try a simple exercise to prove that multitasking is inefficient and eats up your time.**

Grab a pen and a piece of paper and set a timer. Now, prepare yourself to write a short sentence, such as "Multitasking is really switchtasking." For every letter, write a number underneath that letter, starting with the number one and proceeding in order. So, start the timer and write "M," then write "1" underneath it. Then write "U," followed by a "2" underneath it, and so on until the end of the sentence; stop the timer when you're done. Now, start the timer and, this time, write the sentence in full first and then write all the numbers underneath each letter afterward. The results will be clear: it's always faster to do one thing at a time.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Singletasking_** **by Devora Zack**

_Singletasking_ (2015) tackles some of the common myths surrounding multitasking and productivity. Full of practical advice and tricks to help you get more from your day, _Singletasking_ clearly demonstrates how immersive focus on a single task leads to a more efficient, and ultimately happier, life.
---

### Dave Crenshaw

Dave Crenshaw is a highly sought-after business coach who has helped improve companies around the world. His writing has appeared in publications such as _Time_ magazine and the _Washington Post_. His fourth book, _The Power of Having Fun,_ is due to be published in the fall of 2017.

© Dave Crenshaw: The Myth of Multitasking copyright 2008, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

