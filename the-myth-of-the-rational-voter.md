---
id: 5800c6bb230ff80003a1b801
slug: the-myth-of-the-rational-voter-en
published_date: 2016-10-20T00:00:00.000+00:00
author: Bryan Caplan
title: The Myth of the Rational Voter
subtitle: Why Democracies Choose Bad Policies
main_color: 943238
text_color: 881B22
---

# The Myth of the Rational Voter

_Why Democracies Choose Bad Policies_

**Bryan Caplan**

_The Myth of the Rational Voter_ (2007) is all about the barriers our democracy faces and why they matter. These blinks break down the various misconceptions people have regarding democracy, explaining how they connect to flaws in the democratic method and show why our current forms of democracy don't work.

---
### 1. What’s in it for me? Discover the biases that subvert our democracy. 

In the Western world at least, there is little disagreement that democracy is the best possible form of political organization and social coexistence. Indeed, the transition from aristocratic, dictatorial or otherwise authoritarian forms of government to democracy has brought freedom, justice and equality along with it.

But today, democracy is in crisis: Western economies are at a standstill, unemployment is skyrocketing and it is becoming ever more apparent that democratic governments are having major difficulties coping with the challenges they face.

How has this happened? Well, the answer lies at the heart of democracy itself.

In these blinks, you will discover how widespread biases — especially those concerning the economy — translate into bad policies and prevent our democracies from functioning correctly. You will find out about the hidden principles of democratic political systems and why we should trust in the free market a little more.

You'll also learn

  * how democracy is grounded in a miracle;

  * why there are only winners in free trade; and

  * why selflessness is a threat to democracy.

### 2. The averaging of extremes is fundamental to democracy and is what makes it such a functional system. 

Many people consider democracy and democratic governance to be two of humanity's greatest achievements.

After all, democracy is based on a miracle: the _miracle of aggregation_. Essentially, this refers to the phenomenon that an average answer given by a group tends to be correct.

So, if you ask some people to estimate how many beans are in a glass, some will guess too high and some too low. But when you average out their answers, the deviation in either direction will balance out, making the average very close to the correct number.

When this idea is applied to politics, it's plain to see that the average voter is not very well informed and that his assessments of political issues tend to be wrong. But, interestingly enough, the divergent opinions of a large group of voters will average out to close to what is true. So, in a democracy, uninformed or extreme positions tend to nullify one another, leading to a more informed and moderate result.

And it's precisely this middle way between the extremes that makes democracy such a sensible system. In a perfect democracy, popular ideas prevail while extreme viewpoints cancel each other out because of the miracle of aggregation.

This is what makes democratic governments better than dictatorships, in which only certain elites have a say and often hold opinions that are contrary to those of the majority. For example, when the East German government built the Berlin Wall in 1961, the decision stood in stark contrast to the general political sentiment of the East German people. If the country had been a democratic one, the miracle of aggregation would have never allowed such a wall to be built.

So, the miracle of aggregation is what makes democracy function. But sometimes democracies _don't_ work — and you're about to learn why.

### 3. Widespread biases prevent the miracle of aggregation, and therefore democracies, from working. 

You are probably well aware that many democratic governments implement policies that contradict the common good, such as debilitating protectionism. And in such cases, something is preventing the miracle of aggregation from fulfilling its role.

A major cause of such issues is widespread bias, which can stop the miracle of aggregation dead in its tracks. In the end, this miracle has one fatal flaw: it only works if opinions vary in all directions.

For example, when people guess how many beans a glass contains, the average guess will come close to the correct total because approximately the same number of people will overestimate as underestimate. But if people are given biased information, this principle doesn't stand a chance.

Just imagine that this exercise was undertaken in a society in which it had been widely publicized, although incorrectly, that people tend to undershoot the number of beans in the glass. This information would probably generate a bias for those who make subsequent guesses, causing them to guess higher than they normally would have, and resulting in an average that's above the actual amount.

That's a hypothetical scenario, but such widespread biases are very real and their popularity has been demonstrated in a number of surveys. For instance, the 1996 Survey of Americans and Economists on the Economy, or SAEE, asked a series of economic questions to different groups of people. One of the questions was, _Is high spending on foreign aid a big reason why the US economy is not doing better?_

A huge majority of everyday citizens answered that foreign aid spending was at least a minor, if not a major, reason for the country's economic problems. However, the majority of participating economists thought it was in no way related.

In fact, for a wide range of political topics, the average answers given by everyday people differed enormously from the average opinions of the economists. In other words, common misconceptions and widespread biases were influencing the opinions of the general population.

> _"If voters are systematically mistaken about what policies work, there is a striking implication: they will not be satisfied by the politicians they elect."_

### 4. People tend to distrust the free market and underestimate its power. 

Now that we know how damaging biases can be to democracy let's examine one of the most prevalent biases out there: a mistrust of the free market.

It's true — most people believe that any action motivated by profit is necessarily bad and antisocial. In many cases, it's taboo to even talk about the fact that most businesses necessarily want to turn a profit. Or, as the famous twentieth-century economist Joseph Schumpeter once put it: it's like the free market is on trial, but the entire jury has decided that they'll vote for the death penalty, long before the trial even begins.

So, a lot of people mistrust the free market. But this mistrust is largely based on false understandings of market principles, which result in people underestimating the power of free markets.

For instance, a common error is to equate a company's revenue with its profits. From this perspective, the money earned by selling a product — that is, the revenue — is turned directly into profits, which are passed on to the company's owners. It's logical that this process seems immoral; after all, why should money be given to people who are already very rich?

But what this interpretation misses is that revenue is entirely distinct from profit. Earned money is merely an incentive for a company's owners to make products that people will buy, and which can be sold at reasonable prices. Therefore, earned money is reinvested in, say, larger, more cost-effective factories or ones that employ a larger workforce.

This misunderstanding of basic market principles causes many people to underestimate the strength of free markets. But the truth is that most free market processes work very well and are of much greater benefit to society than people tend to think.

This misconception has led the _antimarket bias_ to spread far and wide. But there are plenty of other widespread biases; in the next blink, we'll take a look at another highly influential one.

### 5. There’s a lot of confusion about the benefits of foreign trade. 

We know now that many people don't trust the free market, but another widespread bias is a general mistrust of foreign trade. While trade and exchange actually benefit both the buyer and the seller, people often assume that when two countries trade, only the exporter stands to benefit.

To support such claims, people often bring up statistics showing that more goods are imported into the United States than exported from it. In their minds, this means the country is handing out money to other nations.

But that's a completely incorrect misconception; in reality, good trade deals benefit both parties. After all, if you purchase something at a lower price than it would cost to produce it yourself, you've come out on top.

For instance, if certain members of a household are better at certain tasks than others, it's only logical to break down household duties accordingly. So, if your partner can prepare a meal faster than you, but you can do a better job of fixing the television, you could both just stick to the tasks at which you excel.

By doing so, both of you will get your work done faster and can reap the benefits together, like sharing a meal and enjoying your favorite TV show. In other words, you trade one service for another — and it is sensible to do so, because it would have taken each of you longer to accomplish the other's task. In the same way, every participant in the market benefits from successful trades.

Even so, statistics show that the bias against foreign trade is quite common. For instance, in the 1996 SAEE, another question asked of participants was, _Have trade agreements between the United States and other countries helped create more jobs in the United States?_

The average answer among everyday citizens was that trade deals were more likely to destroy jobs. In contrast, the average view of economists was, by the exact same margin, that trade deals were more likely not to eliminate domestic jobs.

### 6. People tend to care too much about preserving jobs. 

So, we've unpacked a few common biases. But there's one more that's important to consider, and it can be a very emotional topic: jobs and, specifically, their preservation. Eliminating jobs is a surefire way for a company to invite public fury. But is laying off employees always such a bad thing?

Well, most people believe so, and as soon as a major company announces that they'll be reducing their workforce, a media uproar ensues — especially if the company isn't in danger of bankruptcy. When viewed this way, such a response is only fair; how dare a company make layoffs when they don't have to?

But when considered in a broader context, reducing jobs can actually be beneficial. Of course, losing gainful employment can imply serious difficulties for an individual. But for the economy as a whole, it tends to free up the workforce so that it can be used more efficiently in other places.

For instance, just a few decades ago, the majority of the population was employed in agriculture. But nowadays, because of technological advances, only a few people are required to do the farmwork that used to be done by many. So, while this development can be trying for the individual farm or family, it nevertheless opens up the workforce for other sectors of the economy.

Just consider how limited the expanding technological sector would have been over the past several decades without the steady supply of workers freed up by the industrial revolution.

Or consider it on a smaller scale: say you install a dishwasher in your home. Whoever used to do the dishes by hand now has more free time to put to other uses, such as spending time with the kids.

Now that we've learned about several key biases, it might seem like the most serious dangers to democracy have been covered. But, as we're about to see, it doesn't stop there.

### 7. Most people don’t vote selfishly, which, oddly enough, is problematic for democracy. 

While widespread bias is a threat to democracy, it's certainly not the only one. Democracy is facing other challenges — and this next one might come as a surprise: people aren't selfish enough.

In the context of democracies, this means that people don't vote as selfishly as one might imagine. After all, you would expect people to vote solely according to their interests; indeed, many people commonly complain that other voters only care about themselves, failing to see the bigger picture when casting their ballot.

But the truth is surprisingly different. In fact, in an article from 1980, the political psychologist David Sears disproved the idea of the selfish voter by showing that the unemployed are only slightly more in favor of government-guaranteed employment, and that uninsured people are only moderately more likely to support universal healthcare.

In reality, people's voting choices are motivated by other powerful reasons, namely their emotional beliefs, party preferences or opinions on what's best in general for, say, the economy as a whole.

This mentality is actually detrimental to democracy because voting for selfish reasons would produce much better results. For instance, if every voter was motivated solely by personal gain, they would be very careful about informing themselves and voting for the party or candidate that offered them the greatest chance of fulfilling their goals.

The end result would be the election of politicians and parties that could be expected to act in the interests of the majority of citizens, which is exactly what most people would want. In other words, by acting selfishly, we'd elect a government best suited to the needs of the many. While people are always told to take others into account, when it comes to voting, the best way to do so is by thinking about yourself.

> Interestingly, one of the best ways to predict the outcome of an event is to look at a betting market, where everybody just plays for their own gain.

### 8. Emotions run high in politics and democracy pays the price. 

It's clear that people are inherently biased and don't tend to vote purely out of self-interest. But those aren't the only threats to democracy; rather, they're compounded by the reasons people _do_ choose a particular candidate, party or political position.

We've already seen how common biases play a key role in the voting process by hindering the miracle of aggregation, which is fundamental to the functioning of a democracy. But there are other major influences at play, and an especially important one for voters is _emotional attachment_.

Everyone has specific beliefs that we just want to be true no matter what. For instance, if you hold the political conviction that it's immoral to lower taxes for the rich, you'll be deeply invested in it being true; if it weren't, your whole worldview would be thrown into disarray.

In fact, people are so emotionally attached to such beliefs that they'll vehemently argue against people who oppose them, even if these other people's reasoning is sound and logical.

Until we encounter a very strong counterargument that we simply can't ignore, we'll desperately cling to our beliefs, blocking out what other people say. In the process, we'll vote for politicians who promise to act in accordance with our emotionally fuelled beliefs and reject those who do not, even if they make persuasive arguments or have better ideas.

At this point, it's probably pretty clear how different factors negatively influence the voting process. But there's still one piece missing from the overall picture: in the end, we have very little influence over the outcome of any given election.

> _". . . citizens might vote not to help policies win, but to express their patriotism, their compassion or their devotion to the environment."_

### 9. Voters have no reason to act rationally. 

So, if we're emotionally attached to a belief, what could make us change our minds? Well, we _do_ tend to shift our beliefs when they appear to do us direct harm, such as when they cause us to suffer a financial loss. Ultimately, the only time we're forced to act rationally is when our personal interests are on the line.

For instance, say you have a shop and personally believe you should only sell goods to people of a specific religion or political view. You might feel good since you can abide by your emotional beliefs — but simultaneously, you're losing out on loads of potential customers. As soon as these lost customers start to seriously affect your bottom line, you'll likely reconsider your beliefs, or at least their influence on your business practices.

The problem is that, during an election, voters have little reason to think that how they vote will have an impact on their actual lives. In most democracies, millions of people vote, and any given vote is of little importance. In fact, even when elections come down to the wire and require recounts, as happened in Florida in the 2000 US presidential election, the chances of a single vote changing the outcome are basically nil.

So, since there's virtually no reason to think that our individual votes will change anything, there's also no reason to behave rationally. After all, if the only thing that can force us to change our beliefs is the threat of personal harm, and if we don't see such a danger as being connected to voting, then there's no reason to change our beliefs when it comes to electoral politics. As a result, people will continue to vote for whatever politician or party is closest to their emotionally determined beliefs.

In other words, there's no reason for people to vote rationally; instead, it's much more comfortable for people to stick to their biases or emotions. Understanding this reality is essential since our entire democratic system is based on the assumption that rational voters are in the majority.

### 10. Final summary 

The key message in this book:

**Democracy doesn't work because it's based on the assumption that rational voters will make good, informed decisions. But in the real world, voters are anything but rational. They hold broad biases about economic issues, tend to be uninformed and vote with their emotions rather than with their minds.**

Actionable advice:

**Identify and avoid your biases and emotionally determined beliefs.**

Everyone has beliefs that are, at least in part, based on emotions; these are the ideas that we can't bear to change because they just feel right. The problem is, by the time we realize these beliefs are hurting us, it may be too late. So, the next time you find yourself defending an idea even after running out of reasonable arguments in favor, ask yourself: are you defending this idea because you have good reasons to, or does it just feel good to hold onto it?

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Selfish Reasons to Have More Kids_** **by Bryan Caplan**

_Selfish_ _Reasons_ _to_ _Have_ _More_ _Kids_ examines the demands of modern parenting and why people today are choosing to have fewer and fewer kids. The author argues that this trend is due to modern parents placing too high expectations on themselves, even when a far more relaxed style of parenting would get the job done just as well and make the whole experience more enjoyable.
---

### Bryan Caplan

Bryan Caplan is an American economist and professor at George Mason University. He considers himself an anarcho-capitalist and is an expert on public choice theory. In addition to _The Myth of the Rational Voter_, Caplan is also the author of _Selfish Reasons To Have More Kids._

