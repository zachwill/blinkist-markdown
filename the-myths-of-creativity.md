---
id: 5471a95739353600091b0000
slug: the-myths-of-creativity-en
published_date: 2014-11-24T00:00:00.000+00:00
author: David Burkus
title: The Myths of Creativity
subtitle: The Truth About How Innovative Companies and People Generate Great Ideas
main_color: BFE6EE
text_color: 526366
---

# The Myths of Creativity

_The Truth About How Innovative Companies and People Generate Great Ideas_

**David Burkus**

_The Myths of Creativity_ debunks our common misconceptions of how creativity works. It provides practical insight and valuable advice on how to generate new ideas and let them flourish, and it gives real-world examples from history and recent well-known creatives.

---
### 1. What’s in it for me? Learn why we should approach creativity is a new way. 

Are you a creative person? Or do you leave all that innovation to the arty types, whilst you do the real work? If you think in these terms you've already fallen in one of the myths of creativity. The truth is that everyone of us can be creative. Everyone has that ability if she chooses to use it.

The myth that only some can be creative is one of many. Other myths? Creativity is the product of the lone genius, beavering away in his little workspace. Innovation comes as a spark of genius. Brainstorming is the best way to create lots of ideas quickly. How many of these do you believe?

These blinks will show you an alternate perspective and help you see what you can do to create innovative, world-changing ideas.

In these blinks, you'll learn:

  * why the apple didn't inspire Newton;

  * why creativity isn't genetic; and

  * who really invented the personal computer.

### 2. In order to reach our creative potential, we need to change our perception of creativity. 

We're all familiar with how Isaac Newton discovered gravity. One day, whilst sitting under a tree, an apple fell on him, which sparked his realization of the phenomenon.

Yet, this tale is a great example of a common myth that creativity comes from out-of-the-blue divine inspiration. Although most of us accept this myth, it's wrong. Creativity doesn't just fall to us from thin air.

For example, Newton's discovery of gravity isn't exactly accurate. He, in fact, observed the apple fall whilst he was with someone else, which sparked a scientific discussion between them.

Their discussion involved reviewing what they already understood in terms of the phenomenon of gravity. Therefore, rather than Newton being the sole receiver of a sudden revelation; the idea surfaced from the interaction between two intelligent minds. It was only after years of intensive research that Newton could then finally put forward his mathematical formula on gravity.

So even though we love stories of lightning-bolt revelations, we can see that creativity often requires time and effort.

Psychologist Mihaly Csikszentmihalyi actually found that insight is only a central step out of a _number_ of steps that successful creatives move through before their idea has come to fruition.

Ideas can only emerge after a foundation has been prepared and left to germinate for a while.

In order to get the best out of the process, many highly creative people such as da Vinci and Edison had a number of projects on the go simultaneously. This allowed all their ideas to have the necessary amount of time to develop whilst their creators worked on others.

However, as we will see, creativity is not just limited to these famous 'geniuses.'

### 3. Creativity is not an exclusive resource that only certain people can access. 

Why do we think people are more creative than others? We tend to believe certain people are naturally creative, either because they had the gift of creativity divinely bestowed upon them, or it's just in their genes. But this _breed myth_ is false.

In fact, scientific research has shown that creativity is not decided by our genes.

As scientists continue to examine Einstein's brain, there is still no proof that his intelligence was due to a genetically exceptional brain structure. Remarkably, his brain was actually found to be quite small.

Let's take a closer look at genetic influence with psychologist Marvin Reznikoff's study of the creativity of twins. The twins were divided into two groups; fraternal (different genetic code) and identical (shared genetic code) and given creative tasks to complete.

If genes were the key to creativity, there would be a higher similarity in creativity between identical twins than between fraternal twins, yet this was not the case. The creative difference between the sets of twins remained the same for fraternal and identical twins.

Unfortunately, despite this research, many organizations continue to act differently towards creative and non-creative employees and governmental authorities in the US differentiate between creative and non-creative professions, which causes differences in labor legislation.

In companies, creatives and "suits" are differentiated and given different rules to work by. The problem is, excluding those in "non-creative" roles from the innovation process restricts innovation.

However, we do know that companies who have bucked this tradition and allow anyone to participate in the creative process reap the benefits.

For example, _The Gore company_, (inventors of _Gore-Tex_ ), doesn't allocate tasks to its workers. Instead, everyone is free to work on new projects and anyone interested can join them. The result? A range of over a thousand extremely varied products.

### 4. Taking your time and working on preferred tasks can ignite the creative process. 

It's normal for companies to approach creativity in a top down fashion. Management inform "creatives" on their requirements and then expect them to deliver in a short period of time.

You're probably not shocked that this isn't the ideal way to cultivate innovation. But what is a better way to encourage a high degree of innovation and creativity? The answer is a democratic organizational structure.

Take the industrial manufacturer _Semco_. When Ricardo Semler took over Semco, which was previously his father's, it was on the brink of bankruptcy and it sorely needed some innovative ideas.

So Semler uprooted the organizational structure of the company by abolishing fixed teams, assignments and top-down management. Instead, he allowed all employees to work on whatever projects they thought were valuable.

The new structure was a success. In 2003, they celebrated 10 years since the CEO's last decision and made upwards of $200 million.

Assigning a creative task to someone and expecting them to produce something immediately is completely unrealistic. In fact, the opposite is often true. Contrary to what some may think, the creative process partially depends on an allowance for procrastination and mind wandering.

One study demonstrated their importance by taking three groups of students and asking them to come up with as many creative uses for a piece of paper as they could in just four minutes. Two of the groups were given another task to do for two minutes in between; one group worked on a related task and the other on an unrelated task.

The results told an interesting story on creative idea output. The group working four minutes continuously scored the lowest. The group who completed the related task did a little better. However, the best creative results by a long shot were produced by the group that worked an unrelated task in between.

So, the next time you find your mind wandering, remember that your next great idea might well be brewing in the background.

> _"...Mind-wanderers seem to have tapped into the power of incubation."_

### 5. Creativity is enhanced through a beneficial social environment. 

What exactly triggers us to be creative? One of the most important catalysts is the type of social network you interact with.

A valuable tip in terms of people you surround yourself with is to look beyond your immediate workplace in order to stimulate your creative potential.

Often we view innovation as being the result of one mind focusing for a long time on one idea. Yet this strategy is rarely successful. Innovation often stems from many minds collaborating and influencing each other.

Therefore, to find the best chances to innovate, you should take an interest in what others are doing.

It's fairly well known that Bill Gates and Steve Jobs didn't see eye to eye on the invention of the personal computer, but did you know that they both influenced each other with the _PARC_ company and the _Alto_ computer? Jobs saw the Alto on a tour of PARC, which encouraged him to create something similar at _Apple_ and Gates found inspiration for the early Apple computer after working for Jobs for a short spell.

We can clearly benefit from interacting with another person, but even better for fostering creativity is working with large and diverse teams. As we now know, creativity is not the work of a lone genius, but of many minds.

In fact, when we look back at many famous geniuses, we see that they were guided by working with others.

Thomas Edison, for instance, worked with a diverse team from varied disciplines, such as physicists, engineers and machinists who dubbed themselves "the muckers." It was this team that joined forces and came up with some of Edison's most well-known inventions, including the light bulb. 

But why do we connect Edison with the lone inventor myth? Well, the muckers acknowledged the power of Edison's name, and thought it would be an advantage to them if they emphasized his brand and reputation rather than attempting to promote their own work.

### 6. Creativity depends on the ability to connect different types of knowledge in the brain. 

We've just seen how valuable broadening your social network is for creativity. But the ability to be creative doesn't stop at sharing ideas with others. We also need to supply and stimulate our own minds with input from a range of areas.

The more pre-existing ideas we are aware of, the more chances we have to blend them and produce even more innovative ideas.

Research on new technologies shows that new ideas often evolve from combining technologies that already exist.

We can see this tendency with Thomas Edison and his team. They each gathered a huge amount of knowledge from other technologies, even taking bits here and there to use on their own projects. If you went into their workshop, you would see a room full of half stripped machines, all to be used for their own inventions.

Gaining knowledge from pre-existing ideas is often necessary, but creativity also depends on the physical connections within our brain.

Remember how Einstein's brain was not as special as we thought? This is true in the sense that it had displayed no genetic anomalies.

Still, neuroscientists have discovered that brains of exceptionally creative people can better connect the ideas stored in gray matter, the brain tissue that stores what we think. White matter is the connecting tissue responsible for transporting electric pulses. Thus, exceptionally creative people are found to have more white matter than others, allowing them to blend ideas in a more creative way.

However, this is not related to genes or winning some kind of genetic lottery; white matter grows the more we use it.

So if you want to better your chances of coming up with new ideas, you can train yourself and expand your white brain matter by simply working at being creative more often.

### 7. Creativity is often ignited when the innovator is constrained. 

Creativity works best when we are given the freedom to succeed, right? Sure, but this doesn't mean we should always run amok with all our ideas. Creativity is often born from certain types of constraint.

Using restrictions when coming up with new ideas can help even better ones to blossom.

When seeking out new ideas, many companies fall victim to the _brainstorming myth._ They're convinced that the best way to generate new ideas is to lump a big group of people together in one room and tell them to throw in as many ideas as they can think of.

Contrary to popular belief, however, this myth doesn't work so well. Sure, a number of novel ideas are often proposed, but a large number of them are useless.

The best way to generate ideas quickly is to place restrictions on the process. Instead of jumping on the first thing that comes to mind, identify the problem, then research some background knowledge. Then, after thinking of some ideas, you can begin to blend and develop them.

This method will help novel, useful and well-thought out ideas arise.

Constraints can also enhance creativity when they are placed on resources. We often assert that if only we had all the money and time we needed, we could achieve the incredible innovative results we're after. However, quite the contrary is true. Restraining our resources forces us to think outside the box.

The perfect example of this can be found in the world of poetry. Japanese haikus or European sonnets are some of the most innovative and beautiful literary forms. What makes them so impressive? The beauty behind them is that they can only emerge if the author strictly adheres to a limited number of syllables and skillfully navigates these constraints.

So next time you come across something that restricts your work, remember it can increase your personal potential for creative innovation.

### 8. Final summary 

The key message in this book

**Forget what you know about creativity. There are no more excuses for not being creative.** ** _Everyone_** **is creative. Realizing the value of taking your time, using restrictions and working with others are the key components for you to start making a real difference in your creativity and your ability to innovate.**

Actionable advice:

**Stop panicking about time.**

The next time you are worried you are wasting precious time you could be devoting to an important project, remember that you are actually giving your ideas the necessary time to ripen.

**Disregard the "non-creatives" myth.**

End your work division between "creatives" and "suits" by insisting that this divide is imaginary and that the societal limitations that perpetuate this notion cause people to hinder their creative potential.

**Share, revise, and combine your ideas.**

Whenever you are in charge of leading a creative process, organize it so that it comprises several stages in which ideas are regularly shared, revised and re-structured with everybody on the team.

**Suggested further reading:** ** _Weird Ideas That Work_** **by Robert I. Sutton**

_Weird Ideas That Work_ examines how innovations come about in companies and how they can be fostered. The book also shows how creative tasks require a very different work ethic to performing routine tasks. The author cites various examples from the business world and the work of famous geniuses to explain his ideas.
---

### David Burkus

David Burkus is assistant professor of management at the College of Business at Oral Roberts University in Tulsa, Oklahoma. He specializes in the fields of creativity and innovation as well as in entrepreneurship and organizational behavior. He has written for _Fast_ _Company_ and _Bloomberg_ _Businessweek_ and given keynotes for _Microsoft_ and _Stryker_.

© David Burkus: The Myths of Creativity copyright 2013, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

