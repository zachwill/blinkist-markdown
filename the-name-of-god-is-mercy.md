---
id: 573c7657841f7600039dc7d6
slug: the-name-of-god-is-mercy-en
published_date: 2016-05-19T00:00:00.000+00:00
author: Pope Francis
title: The Name of God is Mercy
subtitle: None
main_color: A92223
text_color: A92223
---

# The Name of God is Mercy

_None_

**Pope Francis**

_The Name of God is Mercy_ (2016) outlines Pope Francis's view of God and the Bible, and the reasons that the most important attribute of God is mercy.

---
### 1. What’s in it for me? Learn the importance of mercy. 

When the Argentinian Jorge Mario Bergoglio was elected the 266th Pope of the Roman Catholic Church in 2013, many wondered what kind of Pope he would be. Needless to say, the Pope has had an enormous impact so far, with a huge number of Catholics around the world hanging on his every word for spiritual guidance in their lives.

So, what kind of God does Pope Francis believe in?

As you'll see in these blinks, it's not a God of punishment, but one of love and — above all else — of mercy. Pope Francis has learned, from both personal experience and the scriptures, that to be merciful and to love others, despite their sins, is to be true to God.

In these blinks, you'll learn

  * why you should never let the sun go down on your anger;

  * why a church should be like a field hospital; and

  * why we should avoid corruption like the plague.

### 2. God’s most important attribute is mercy. 

"God" encompasses many things, like patience, benevolence and omnipotence. There's one word, however, that God encompasses most: mercy.

God is often compelled to make a choice between mercy or punishment, and he always chooses mercy. For example, "Do not let the sun go down on your anger" (Ephesians 4:26) tells us to let go of our anger before the sun sets, so we can wake joyfully the next day without resentment.

A lot of people have misinterpreted this quote as an instruction to hold onto your anger. That fits with the interpretation of God as vengeful — a far cry from the idea of God as merciful.

Mercy is what connects people to God, not anger or hate. God's mercy is the anchor that stops people from spiraling into a life of sin. It reminds them that life has meaning. God's mercy allows people to remember that they'll always be loved and their attempts to live righteously aren't in vain.

When people repent to gain God's love, they strive to live better lives and make the world a better place. Faith in the mercy of God makes people want to help others as well. As it says in Psalm 145:7-9, "He executes justice for the oppressed; he gives food to the hungry."

God also tells us that he'll answer us when asked, which stops people from trying to take matters into their own hands. Imperfect humans are naturally less merciful than God, so important matters should not be entrusted to them alone.

God's ability to show mercy rather than cause destruction is also a sign of his power. The same goes for humankind. That's why God sent Jesus to be an example for humankind, so we could learn from a being in our own form.

### 3. Jesus was the human manifestation of God’s mercy. 

There's mercy to be found in every story about Jesus.

Jesus said that he didn't come to Earth for the righteous, but for the sinners. He didn't come for the healthy, but for the sick. He showed mercy by focusing his efforts on people who'd been cast out of society, such as lepers.

Jesus approached lepers with love instead of staying away from them for fear of contracting their disease. As a result, he healed them, showing that God never turns away from anyone.

When Jesus once saw that a crowd had followed him and his apostles to a meeting that was supposed to be private, he canceled the meeting and offered counsel to the people. He realized they were lost sheep in need of a shepherd. Tending to their well-being was more important to him than keeping to his agenda.

Jesus also said that we should forgive people not seven times, but seventy _times_ seven times. In other words, always. He preached that people should show mercy even when doing so went against the law, illustrating where our priorities should be.

When a woman was charged with adultery, Jesus admonished the men who sought to punish her, saying, "Let him who is without sin among you be the first to throw a stone at her." (John 8:7) Adultery was punishable by law with stoning, but Jesus taught them that it was more important to show mercy. They remembered that they, too, were sinners and realized they didn't want to be stoned themselves.

Jesus himself had never sinned, so he could have thrown a stone at the woman without being hypocritical. Instead, he let her confess her sins and he forgave her.

Death was Jesus's final act of mercy. He sacrificed himself to redeem humankind. Today, the Church has the responsibility of carrying out his legacy by showing mercy to those rejected by society.

> "_We touch the flesh of Christ in he who is outcast, hungry, thirsty, naked, imprisoned, ill, unemployed, persecuted, in search of refuge_."

### 4. It’s the responsibility of the Church to carry out God’s mercy. 

The bishops and priests of the Church are tasked with continuing the work of Jesus. They must strive to act in _persona christi_, meaning to act with Jesus's sensitivity and willingness to go where he was needed.

God's mercy reaches humanity through priests and confessors. One of Pope Francis's former parishioners was a prostitute. She once thanked him for always calling her "Señora" because it taught her that even if she sold her body and didn't have food or money, she was still valuable and deserved respect.

Priests and confessors must aim to bring people in. If a confessor doesn't do his job properly, he can turn people away from the Church.

Pope Francis knew another woman who stopped going to confession when she was a teenager after her confessor asked her "where she put her hands when she slept." If people turn away from the Church as she did, they become more vulnerable to sin and stray further from righteousness.

The Church should function like a hospital where all people can come to heal their wounds. When people are spiritually or physically wounded, they don't always have the strength to seek out the Church. So the Church has to be available everywhere, not only in towns — but in places where people struggle more, like prisons.

If a prisoner has access to the Church, he'll grow stronger and continue to build his relationship with God when he gets out. In fact, to show his commitment to those who struggle, no matter where they ended up in life, Pope Francis used to carry around an olive branch made in a rehabilitation program for prisoners.

Society needs guidance from the Church like a child needs guidance from a parent. The Church has to make itself available to people who need the most guidance and help.

> "_Everything that the Church says and does shows that God has mercy for man._ "

### 5. God loves everyone like a father. 

God's love is unconditional. He would never turn away his children, no matter what their sins — just like parents always have love for their child.

There are several instances in the Bible where God is likened to a parent. In the Gospel of Luke, for example, when Jesus is deeply moved by human suffering, the Greek word used to describe his emotions derives from a word referring to a mother's womb. Jesus's reaction is like that of a parent who is moved by her child's suffering.

In the book of Ezekiel, Jerusalem is compared to a young girl left to perish. God takes her and gives her everything, yet she becomes a prostitute. Despite this, God tells her, "You will continue to be the chosen people and all your sins will be forgiven."

God's mercy also first reaches us through our parents. Parents don't judge their children when they do something wrong. Instead, they try to guide them to a better path with patience.

In the story of the adulteress, Jesus tells the woman she should continue without sinning but that she isn't condemned. He deals with her just as a parent would deal with a child.

In Eastern churches, a confessor welcomes a penitent by putting his stole over the penitent's head and an arm around his shoulder. Just as a parent's touch soothes a child, this action relaxes the penitent and helps her feel like she won't be judged, encouraging an open and honest confession.

Some people have a difficult time finding a source of parental love as they get older, but there's no limit to God's paternal love. Everyone can seek out God's love if they repent their sins and ask for forgiveness. God is always ready to forgive.

> "_Family … is the first school of mercy, because it is there that we have been loved and learned to love, we have been forgiven and learned to forgive._ "

### 6. We have to accept that we’re all sinners. 

In order to reap the full benefits of God's mercy, we first have to accept that we need his love and guidance because we're all sinners.

We're all sinners because of original sin. When Adam and Eve rebelled against God, they condemned humanity to weakness and the inability to always choose good over evil. We're all bound to commit sin.

That doesn't mean we should allow ourselves to fall into sin whenever we like because that displeases God. However, it's worse to deny our true nature and believe it's possible to live a life without sin. God is willing to forgive us because he understands it's in our nature to sin.

Pope Francis himself admits that he's a sinner. When he spoke to prisoners in Palmasola, Bolivia, in 2015, he said, "Standing before you is a man who has been forgiven for his many sins."

In fact, a sinner who repents is more precious to God than a righteous man. In the parable of the prodigal son (Luke 15:11-32), Jesus speaks of a father with two sons, the older of whom stays unwaveringly by his side and the younger of whom leaves the house, squanders all his money and acts selfishly. But when the younger son returns home, the father celebrates his return more than the older son's commitment. He tells the older son, "We had to celebrate and be glad because this brother of yours was dead and is alive again." Such is the miracle of repentance.

We can't escape sin, but we can strive to make the best choices possible. Remember that humankind has an innate tendency toward evil: there's no escape from sin, but we can work to be righteous and repent.

> "_Let us always remember that God rejoices more when one sinner returns to the fold than when ninety-nine righteous people have no need of repentance_."

### 7. We must be open to sin, but not to corruption. 

Sin is one thing, and _corruption_ is another. Corruption is the act of convincing yourself and others that your sins are justified. Corrupt people may put up a facade of Christianity to conceal their sins in the hopes of deceiving not just their peers, but God himself.

Whereas sins are usually committed in a weak moment, corruption tends to be woven into the fabric of a person's lifestyle, which makes it harder for corrupt people to return to God than for sinners. A corrupt person often focuses on money, fame and power rather than the virtues Jesus told us to pursue.

Money, fame and power are addictive. A person who devotes her life to those pursuits will struggle to admit they're meaningless, as it would mean that her life had been in vain. That's a difficult reality to confront, especially because her distance from God will have weakened her heart.

Corrupt people often choose to go down the same treacherous path. It's not impossible for a corrupt man to return to God, but it does take a lot of work. Often, corrupt people only return to God when confronted with an overwhelming challenge, like the death of a loved one, which forces them to reexamine their life choices and seek help.

Corruption isn't only harmful to the individual committing it — it's detrimental to society as well. Corrupt people rarely take responsibility for their actions. For instance, they might complain of having been robbed, but fail to see that their own tax evasion is a crime.

The man who committed the robbery might confess and repent, but if the corrupt man only focuses on the thief's sins, society suffers more from the repercussions of his selfish actions.

All in all, corruption is the product of ongoing egotistical behavior. And compassion is the key to overcoming it.

### 8. Showing each other God’s mercy by way of compassion will help heal society. 

Humans aren't divine beings like God. We can never be as merciful, patient or loving as He is. We can be compassionate, however, if we follow Jesus's example.

The world would be a more peaceful place if people were more compassionate. Most people are indifferent to the tragedies they see on the news. They only care if it affects them.

We should strive to be more involved in the world around us like Jesus did. When you reach out and touch more people's lives, you are also working to prevent evil.

The most difficult times to be compassionate or merciful are when you're hurt or feel like you've suffered some injustice. At those times, it's tempting to seek revenge. But the Gospel teaches us, "Love your enemies, and pray for those who persecute you." (Matthew 5:44) If we don't, we run the risk of falling into, "an eye for an eye, a tooth for a tooth," which only leads to a never-ending cycle of violence.

Governing with love is just as important as governing with the law. Jesus's actions were always guided by love, even when it went against what scholars of the law might do. Jesus taught that we should love everyone. After all, if we're all sinners, what right do we have to punish other sinners? That doesn't mean that we should let sin run wild — it means we should try to tame sin with love instead of punishment.

Sin is the wound of humanity and God's mercy is the only cure. When we show each other compassion, we encourage our fellow men to soften their hearts instead of sinning. That encourages them to open their hearts to God and begin the process of healing. The ability to show compassion is the true test of Christianity. As St. John of the Cross said, "In the evening of life, we will be judged on love alone."

> "_Whatever you did to one of these brothers of mine, even the least of them, you did it to me._ " (Matthew 25:40)

### 9. Final summary 

The key message in this book:

**Above all else, God is merciful. Mercy is at the heart of every story about God and Jesus, and God put Jesus on Earth to serve as a human manifestation of the righteousness of mercy. All humans are sinners, so sinners must be shown mercy to be guided back to the path of righteousness. Punishment, anger and revenge don't make the world a better place; mercy and compassion do. Mercy is the ultimate test for humankind.**

Actionable advice:

**Don't be afraid to go to God.**

No matter how much you've sinned, God will always be there to listen. Ask him for mercy, and he will grant it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: The Varieties of Religious Experience** **by William James**

Based on a series of lectures given by William James between 1901 and 1902, _The Varieties of Religious Experience_ (1902) is an in-depth exploration of how we experience religion and how a personal approach to religion can be profoundly useful to us.
---

### Pope Francis

Pope Francis is the 266th Pope of the Catholic Church and the first Pope from the Southern Hemisphere. His unwavering commitment to mercy has caused a lot of controversy. He has published many other books, including _The Church of Mercy, Walking with Jesus_ and _Corruption and Sin: Some Thoughts on Corruption_.

