---
id: 56b8dfa1d119f90007000061
slug: the-narcissist-you-know-en
published_date: 2016-02-10T00:00:00.000+00:00
author: Joseph Burgo
title: The Narcissist You Know
subtitle: Defending Yourself Against Extreme Narcissists in an All-About-Me Age
main_color: FA3732
text_color: 992E2B
---

# The Narcissist You Know

_Defending Yourself Against Extreme Narcissists in an All-About-Me Age_

**Joseph Burgo**

"Narcissism" has become a buzzword and a snap diagnosis, but how much do we really understand about this condition? _The Narcissist You Know_ (2015) unpacks the myths and the truths. Narcissism isn't just a serious psychiatric disorder, it's part of life — we all share some tendency toward it. By analyzing a wide range of narcissists — many of them celebrities — Joseph Burgo reveals the hidden shame that lies behind all the pain.

---
### 1. What’s in it for me? Learn to spot the narcissist. 

When news broke in 2009 that Tiger Woods's wife had chased him out of his home with a golf club following revelations of his serial cheating, many were shocked and surprised. How could this successful man who had everything and seemed so nice have done such a thing?

Perhaps we shouldn't be so taken aback, because behind the winner mentality, relentless drive and charisma of successful people like Tiger Woods, Donald Trump and Madonna, there's often a narcissist in hiding. And narcissism, psychology shows, can result in a whole bag of even less praiseworthy actions. 

So why are some people narcissists? 

In these blinks, you'll learn

  * where narcissism really comes from;

  * what Lance Armstrong has in common with Madonna; and

  * why you should never mess with Sarah Palin.

### 2. Narcissism is not one illness but a continuum of disorders. 

There's a lot of talk about narcissism nowadays. With the rise of social media and the selfie, it seems that being narcissistic has become normal. 

But narcissism isn't a trivial word to be casually thrown around. It's actually a dangerous tendency that exists in all of us. We've all lacked empathy toward others and acted egocentrically at one time or another. And while some of us only have this tendency in small amounts, there's a whole spectrum of narcissists out there — and some of them are highly dangerous.

In fact, there's an officially recognized psychiatric disorder called Narcissistic Personality Disorder, which defines the criteria for the 1 percent of the population who are pathological narcissists. These are the kind who often get jailed for crimes like murder or rape.

But there's another section of the population that often passes under the radar: the 4 percent who qualify as _Extreme Narcissists_ — narcissists who don't fit the psychiatric definition, but are still much more dangerous than "lesser" narcissists like you and me.

While they may not commit as many crimes, Extreme Narcissists are terrified of being seen as inferior to others, and use vicious and elaborate tactics to maintain an image of superiority. Many highly successful celebrities are Extreme Narcissists — but it's also likely you know one yourself.

That's why it's so important to learn how to identify and deal with Extreme Narcissists: you need to protect yourself. But not only that — you also need to identify your own narcissistic tendencies. 

Why?

Because being even a bit narcissistic can bring out the narcissistic tendencies in others.

For example, imagine you live in a shared house with other people. If you act narcissistically and keep eating all the food without replacing it, the others will start doing the same thing. In the end, everyone will behave increasingly narcissistically.

We'll learn more about Extreme Narcissists in the blinks that follow. But before you judge them too harshly, remember that they're in constant fear of being losers.

### 3. Behind narcissism lies core shame. 

We talk about emotions like shame all the time, but when asked, we often can't describe them. Shame isn't just a bad feeling we get when we feel we've done something wrong. It's a deeper emotion telling us that we are exposed and at risk. 

One particular kind of deep shame lies at the center of Extreme Narcissism: _core shame._

Core shame is created when something goes wrong in the child–parent relationship. According to British psychoanalyst D.W. Winnicott, children are born with an innate sense of how a parent should take care of a child — i.e., by paying attention to the child's needs and emotions. 

When parents are neglectful or absent, children think that something is fundamentally wrong with them. They feel unloved, and, not understanding why, they think it is their fault. This the source of core shame.

This core shame is awakened any time an Extreme Narcissist experiences a _narcissistic injury:_ the pain we all feel when our self-esteem is shaken by other people's criticism. We experience this because we are social animals who build our sense of self on the way others see us. But while our self-esteem becomes stronger over time, Extreme Narcissists remain stuck in their core shame. This means that the slightest criticism deeply damages their self-esteem. A trivial insult may cause great pain, and a bigger event, like divorce, unbearable humiliation.

So Extreme Narcissists develop powerful defense mechanisms to protect themselves from narcissistic injury. They blame others for their faults, delude themselves into thinking they are perfect and create a self-image of superiority to hide their true self from others.

### 4. Narcissistic and overindulgent parents raise narcissistic children. 

We've seen that negligent parents raise narcissistic children. But there are two distinct categories of negligent parents: the narcissistic type, who don't care about their children, and the overindulgent type, who don't know how to care for their children properly.

Let's look at the overindulgent type first. These parents often have good intentions, but don't realize that too much positive attention is actually bad for the child. While babies do need love for their brains to develop properly, they also need boundaries if they're to develop a realistic view of themselves and the world.

This is a challenge, because parents need to simultaneously love their children unconditionally, while also punishing them when they do something wrong. But the danger is that without appropriate limits, children come to believe that the world revolves around them, and with time they become narcissists.

While overindulgent parents typically care for their children, narcissistic parents use their children to boost their own fragile egos. Some parents manipulate their child, molding him or her to fit some ideal so that they feel superior to other parents. But when the child inevitably can't live up to the impossible ideal, it develops a sense of core shame — which leads to narcissism. 

These children often end up having careers that put them in the public eye, so both the narcissistic parent and child can drown their unconscious shame in positive attention. Tiger Woods is a good example of a narcissistic son of a narcissist. His hypercompetitive father lived out his ego through his son, producing a world-class sportsman — and a world-class narcissist.

Other narcissistic parents maintain their self-image by always putting their children down, thereby maintaining a feeling of superiority. This makes the children feel fundamentally worthless, which again leads to core shame. 

In both cases, the parents don't raise their children with the love and empathy a child needs. Instead, they exploit their offspring as tools that make them feel better about themselves.

Now that we know where narcissism comes from we can look at some different types of Extreme Narcissists.

### 5. Bullying Narcissists are the ultimate aggressive narcissists. 

All Extreme Narcissists separate the world into winners and losers. But _Bullying Narcissists_ are the most obsessed with competing — and with always being the winner. Only by always being top dog can they maintain their inflated self-image.

This obsession with winning leads them to prey on anyone vulnerable to bullying.

They seek out weak people they can put down to make themselves feel powerful. But interestingly, the Bullying Narcissists and their victims are very similar: unconsciously, they both feel deeply insecure and inferior.

Later in life the need to be the winner becomes so strong that Bullying Narcissists attack even those more successful than themselves. More successful people make the narcissists feel like a loser by comparison — so they attack to try and maintain their winner-image.

In fact, their obsession is so strong that they'll use any means possible to maintain the illusion of superiority.

Take Lance Armstrong as an example. His core shame came from growing up in extreme poverty, and not being able to buy what others could afford. So he did everything to become a winner — including using drugs to cheat — and defended his image with extreme lies and bullying.

When team members accused him of taking drugs, he said they were envious losers, pitiful and jealous of his talents. And when the press wrote about his suspicious superhuman strength, he used his wealth to sue the journalists who dared criticize him.

### 6. Seductive Narcissists seduce you to boost their self-esteem. 

While Bullying Narcissists try to make themselves feel better by putting you down, _Seductive Narcissists_ shower you with attention, not because they love you, but because they want you to love them.

These charismatic people exploit your narcissism to feed their own. They worship you, pretend to listen to you deeply, make you feel desired and important, but only so you do the same. And when you fall in love with them, they control you. They give you praise only when you do what they want, and always make you feel insecure and dependent on them. Once you're caught in their net, you keep pursuing them because you're afraid of the core shame of unrequited love.

Actually, it's often people who themselves didn't get enough love, or lost a loved one, who become Seductive Narcissists. Their experience of losing a loved one (or being insufficiently loved) is so painful that they vow to never love again.

This _anti-dependency_ makes them resistant to getting attached to people, but they still crave attention. So they make people fall in love with them without ever committing themselves.

Madonna is a good example of this: she lost her mother when she was five. Later in life, she became a serial seducer, starting relationships without getting emotionally involved. And although she was vulnerable, she hid it behind a well-polished facade of self-confidence — typical of Extreme Narcissists. 

The best way to cope with Seductive Narcissists is to be suspicious of people who adore you the moment they meet you. Ask yourself: what do they really want from me?

### 7. Grandiose Narcissists believe they’re exceptional; Know-it-All Narcissists think they’ve found the hidden truth. 

All Extreme Narcissists are caught in their own fantasy world of being the best. But _Grandiose Narcissists_ live in a whole other dimension.

They believe they are so exceptional that they don't have to conform to social norms like the rest of us. Some of them see being famous as an opportunity to enact their grandiose fantasies because they know that society is very tolerant of bad celebrity behavior. When they've made it into the public eye, Grandiose Narcissists can make a scene, get into fights, break laws — and get attention for it.

But others live so deeply in their fantasy world that they think they are exceptional even when they achieve nothing. These Grandiose Narcissists, often loners, believe they are unrecognized geniuses, despite having no accomplishments to speak of.

While they end up in different places, both successful and "secret" Grandiose Narcissists have something in common: they both have a deep fear of their potential insignificance. To counter their insignificance, they create an image of themselves as supremely important.

Julian Assange is a perfect example of a Grandiose Narcissist. Although he seems like a selfless crusader, he is actually obsessed with his public image and arrogantly unwilling to share credit for the work of his organization Wikileaks.

Closely related to the Grandiose Narcissists are the _Know-It-All Narcissists_. They are always showing off their superior knowledge of the world. And while sharing knowledge can be valuable, these narcissists use it to exploit others — on a large scale.

They take advantage of our need for intellectual and spiritual leaders to form religious and political cults. By using their powerful knowledge, they can convince many people that only they know the truth.

Take Bikram Choudhury, the founder of Bikram Yoga. His followers, many of them stars like George Clooney, see him as a superhuman Jesus-like visionary. But in recent years he has been embroiled in accusations of sexual abuse from women in his organization.

> _"Apparently, we are willing to forgive the Know-It-All Narcissist for his grandiose nature and lack of empathy provided that he offers us something of exceptional value."_

### 8. Self-Righteous Narcissists claim moral superiority, and Vindictive Narcissists know no limit to revenge. 

While most of us believe we're all entitled to our own morality, _Self-Righteous Narcissists_ firmly believe they are the only ones who know what is right and wrong. They believe that you are either on their side, or an enemy of all that is holy and good. And whenever you get into an argument with them, it's always your fault, and they're the innocent martyr.

When you first meet them, they often seem like virtuous people. They might be involved in organized religion, or fight for social issues. But behind the mask of morality lies their true desire: to always feel _more moral_ than others. In that way, they can maintain the illusion of being superior to everyone else.

Take a look at the militant vegetarian or vegan. Their black-and-white vision of the world divides people into the good and the bad — with themselves firmly on the right side. They aren't interested in arguments or the truth — like the fact that raising crops kills many more non-edible animals than farming red meat — and judge you harshly if you don't agree with their views. Of course, this does not apply to all vegans and vegetarians, most of whom hold sincere beliefs.

_Vindictive Narcissists_ go even further when you disagree with them: they take it as a personal attack — and seek revenge. They react furiously to even the smallest disagreement. And once you have become their enemy, they will do everything to destroy you — even if it means lying under oath.

Sarah Palin is a prime example. When her sister Molly got divorced from her state trooper husband Mike Wooten, Sarah did everything in her power to get him fired. She started a smear campaign against him, and lied in court about his supposedly abusive behavior.

So if you ever meet a Vindictive Narcissist, be sure not to get involved when they pick a fight. You never know how far they'll go to get revenge.

### 9. Narcissism and addiction are closely related. 

You might reach for a drink when your self-esteem takes a blow, like after a bad break-up. The next day you feel better though and get on with your life. But when someone with fragile self-esteem — like a narcissist — turns to alcohol or drugs to numb their pain, it can quickly spiral into addiction.

When a narcissist suffers a blow to their self-esteem, it reawakens their core shame. They then numb the shame with drugs — often to excess. The next day, they're ashamed they took so many drugs, and take more to numb the shame. Thus begins the vicious circle. 

As their addiction grows, they become more and more self-centered and focused on getting their next fix. In fact, addicts and narcissists are very similar: they both lack empathy and think their needs are more important than everyone else's.

There's a new kind of addiction closely linked to Extreme Narcissism: addiction to _massively multiplayer online role-playing games_ (MMORPGs).

In MMORPGs, gamers can create a virtual alter ego, and if they play enough, they can make it the most powerful alter ego in the world of that game. This appeals to many people who struggle with success in the "real world."

But just like those who obsess about real world success, many MMORPG gamers are trying to build a grandiose self-image to escape their shameful feelings of insignificance. And when they've been playing for long enough, they find their addicted life better than reality — like any other addict.

While some shame-ridden people get addicted to virtual alter egos, others like Michael Jackson get addicted to transforming themselves through cosmetic surgery. 

He was bullied by his father and brothers, and became ashamed of his body. This core shame led to his obsession with plastic surgery, which he used to change the shape of his face, mouth, eyes and nose.

If you worry that you're developing an addiction, or are stuck in a narcissistic rut, it's best to seek help. Though difficult, narcissism, like addiction, can be overcome. And the first major step is acknowledging the problem.

> _"All Addicted Narcissists use their drug of choice in the same way, to provide relief from a shame-ridden, damaged, or 'ugly' self."_

### 10. Final summary 

The key message in this book:

**Narcissism is not just a personality disorder but a trait we all share. We all suffer from shame and try to become winners to compensate, but some people — Extreme Narcissists — suffer from core shame. This makes them willing to do anything to not feel like a loser. But instead of condemning narcissists, we should understand them as human beings and learn how to deal with them.**

Actionable advice:

**Cope with narcissists by starting with yourself.**

Extreme Narcissists are challenging to interact with because they have such a powerful effect on your self-esteem. They can state their opinions so aggressively that you end up questioning your reality. The only way you can defend yourself is to strengthen your self-esteem.

To do so you need to confront your own shame. You too are attached to being a loser or a winner. You need to look deeply in the mirror and accept what you see. Then when people try to make you feel inferior, you'll always know that you are alright.

**Suggested** **further** **reading:** ** _Talking to Crazy_** **by Mark Goulston**

_Talking to Crazy_ (2015) acknowledges that each person has the potential to be a little crazy, giving into irrational behavior when the mood strikes. These blinks offer sound advice on how to empathize and communicate with a person in "crazy mode" so you can keep yourself from going off the deep end, too.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Joseph Burgo

Joseph Burgo, Ph.D, is a psychotherapist, psychoanalyst and writer. He has practiced psychotherapy for over 30 years, and is also the author of _Why Do I Do That? Psychological Defense Mechanisms and the Hidden Ways They Shape Our Lives_.

