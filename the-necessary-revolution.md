---
id: 58530bc058b1350004ed172b
slug: the-necessary-revolution-en
published_date: 2017-01-05T00:00:00.000+00:00
author: Peter Senge, Bryan Smith, Nina Kruschwitz, Joe Laur, Sara Schley
title: The Necessary Revolution
subtitle: How Individuals and Organizations Are Working Together to Create a Sustainable World
main_color: 2A7FD3
text_color: 2D6499
---

# The Necessary Revolution

_How Individuals and Organizations Are Working Together to Create a Sustainable World_

**Peter Senge, Bryan Smith, Nina Kruschwitz, Joe Laur, Sara Schley**

_The Necessary Revolution_ (2008) sheds light on the environmental and social challenges faced by people living in today's world. Drawing on stories from real people and real communities, these blinks introduce the mentality we must adopt to fight for sustainability.

---
### 1. What’s in it for me? Join the necessary revolution. 

In December, 2015, when the Paris Agreement, a legally binding global climate change deal, was signed at the Paris Climate Change Conference, people all over the world took to the streets to celebrate. We've long known about the devastating effects our constantly growing, resource-draining economies have on the environment, and a universal push to deal with climate change is indeed a big step forward.

But it's nowhere near enough.

Many of the underlying causes of climate change haven't changed or are changing too slowly. It's time for a revolution.

But what should that look like?

In these blinks, you'll learn

  * why constant growth is no longer an option;

  * how Per Carstedt helped bring ethanol cars to Sweden; and

  * why we should never underestimate our power as consumers.

### 2. The necessary revolution is a shift toward environmental sustainability. 

We are in dire need of a paradigm shift. Today, that old tune about constant progress for prosperity's sake is getting old. The financial crisis of 2008 was the final, damning piece of evidence that the neoliberal ideal of unlimited growth via open financial markets is utterly misguided. Our planet and its future is at the true heart of things — and it's high time we started acting like it.

In 1972, a report called "Limits to Growth," published at the global-sustainability think tank The Club of Rome, laid out the necessity of acknowledging mankind's limited resources. Since then, public concern about our unsustainable industries and economy has grown. These days, achieving environmental sustainability is no longer seen as a preferable option — it is a _necessary revolution_.

The necessary revolution impacts all humans, and must be conducted on an individual, political and economic level. By raising public awareness through word of mouth, and by sharing studies and initiatives on social media, the individual plays the first crucial role in the necessary revolution.

Companies should also get involved in the necessary revolution by shouldering _corporate social responsibility._ A concept that's been around since the early 2000s, CSR weaves social causes into the objectives of companies. For instance, major players in the food industry demonstrated CSR by reducing the use of unhealthy ingredients in their products.

Finally, the government must support the necessary revolution by subsidizing sustainable corporate initiatives and by implementing effective laws and policies. The Kyoto protocol, an international contract for sustainability signed in 1992 by nearly all world leaders, is a landmark example of governmental support for a greener future.

### 3. Kickstart the necessary revolution by maintaining an empowered mentality. 

If you consider the devastation climate change will wreak on our world, you'll probably feel pretty overwhelmed. The damage we've already done may seem too dire to reverse. But, of course, despairing won't get us anywhere. It's crucial that we develop an empowered mindset to push for a sustainable future. But how?

The first step is realizing that, in spite of everything, we aren't helpless. It doesn't take a national government or transnational organization to create radical change. Indeed, small groups constantly prove themselves capable of shaking up the system.

Take Per Carstedt, for instance. The owner of a Ford dealership in Sweden, Carstedt was a keen environmentalist and contributor at the United Nations Conference on Environment and Development (UNCED) in 1992. His idea? To bring ethanol cars to Sweden.

Ford itself had no clue how to help Carstedt. But through commitment and perseverance, Carstedt was able to convince more and more gas stations to install ethanol pumps. He even managed to persuade a few investors to import the first ethanol-fueled cars. By 2007, Carstedt's team had reached 1,000 stations, a quarter of the entire national network.

Carstedt used an empowered mindset to turn a big idea into a real contribution to sustainability. More organizations should take a leaf out of Carstedt's book. Whether they're heading up a large company or an NGO, leaders must move beyond the short-term and start initiating long-term projects that truly make a difference.

Some corporations have already started. Coca-Cola, for example, has been cooperating with WWF since 2007 to make fresh water more accessible worldwide. At the beginning of their collaboration, Coca-Cola and WWF discovered that every liter of cola produced required 200 liters of water, largely as a result of sugar production. By improving the sustainability of sugar production, however, WWF and Coca-Cola are now able to save more water and, in turn, be kinder to the environment.

As several pioneers have demonstrated, the right mindset makes progress possible. We should follow their lead!

### 4. Create the change you want to see by fostering positive focus. 

If you want to be active in the necessary revolution, where should you start? Well, first off, with your mindset. Anything to which you dedicate time and effort will move forward, a fact it's vital to remain aware of. So, in other words, we've got to stay optimistic. Of course, this is easier said than done.

In part, this is because it's much easier to concentrate on eradicating what we don't want than on creating the things we _do_ want. The way we name our causes certainly reflects this. From anti-nuclear groups to anti-smoking protests, a negative focus creates a complain-and-protest mindset, rather than a push toward constructive action.

So what does a positive focus look like? Well, one great demonstration of it comes from a green start-up in Germany. Original Unverpackt allows customers to buy their groceries without packaging. This strategy not only drastically reduces costs but also minimizes ecological damage. And the company isn't anti-packaging; it's simply pro-sustainability.

If we want to create change for the better in our world, then we need some serious green momentum! Time spent complaining or over-analyzing is time wasted.

Of course, the necessary revolution must occur on an international scale; however, this doesn't mean international organizations are solely responsible for catalyzing change. We're all aware that a shift to sustainability is needed urgently, as the consequences of climate changes are already affecting our world. We need change today, and small groups as well as individuals will play an instrumental role in creating it.

### 5. Corporations have a powerful hold on our society. 

Corporations play a central role in your life — not just because you might be employed by one but also because, for better or for worse, you probably rely heavily on their products.

Thanks to certain legislation, corporations have what's called _limited liability_. This means that, if a business fails, the business owner owes nothing to their investors. Furthermore, business owners are more or less free to damage the environment, so long as their business is economically successful. This is especially true if the products provided by the business are deemed vital for the people where the business is located.

On top of this, the way we measure a corporation's success is in dire need of an update. _Return on investment_ (ROI) is a basic metric that defines success as getting the most out of what you've put in. But this simply can't account for several other vital aspects of successful business, such as the happiness of employees and consumers, and, of course, the business's impact on the environment.

But what about good old _corporate social responsibility_ (CSR)? Surely this term implies that businesses do consider the role they play in society. The reality is that CSR simply isn't implemented effectively enough to create any real change. Why is this?

According to CSR expert Steve Lydenberg, businesses tend to use social initiatives for short-term financial gain, rather than for implementing long-term strategies of social and environmental responsibility. This obsession with immediate gratification unfortunately poses a major threat to the economy, as well as to society and the environment.

But not all businesses are using CSR to boost their revenue and polish their reputation. Some do take it seriously and are willing to reshape their entire strategies in the process. Take the Deutsche Post DHL Group, for example. They have implemented CSR in a big way — by distributing medicine to developing countries free of charge, as well as donating to educational initiatives in developing countries.

### 6. We’re all part of the necessary revolution. 

How will you contribute to the necessary revolution? Well, you have a lot of options. One option we _don't_ have, however, is impartiality. Like it or not, we're all part of the necessary revolution.

Disgusted by our corporate, consumption-driven society, some individuals decide to start living outside the system, becoming _autarkic marginals_. This act of protest may be radical, but it isn't actually effective.

Rather than abandoning our dysfunctional system, we must drive the necessary revolution by creating change from within. Only then will we be able to build a brighter, more sustainable future for our global society. Because of this, becoming an autarkic marginal isn't really a solution. So what can we do to start shaping the system?

Well, have you purchased something this week? Probably. Every day we consume a huge variety of products and services, which is quite sufficient to make us instrumental players in the necessary revolution.

As consumers, we have an incredible influence on the economy. After all, we determine the demand that industries must meet. Could we use this power to move things in the direction of the necessary revolution?

Yes! In fact, we're already doing it. Consider fast food chains, for example. Over the past few years, large fast-food franchises have struggled with profit and public image. At some point, disgruntled consumers began demanding new things, such as healthier products and fairer wages.

One of the greatest strengths of the necessary revolution is that the environmental benefits it creates _overlap_ with personal benefits. Take our diets, for example.

The biggest food study in history, _The China Study_, examined the relationships between a range of diseases and human diet. Its findings were pretty straightforward: staying healthy means eating healthily. And that means having a diet that is around 90 percent _vegetarian_. If we all ate less meat, CO2 emissions would drop and there'd be less clearing of forests for livestock. It's a win-win: improve your health and help the environment, too.

### 7. Final summary 

The key message in this book:

**A revolution of sustainability is absolutely necessary. We can create the change our planet needs by adopting an empowered mindset. Indeed, many pioneers are already leading the way, but they need our help to build momentum!**

Actionable advice:

**Play the "sustainability game" in your shared spaces!**

Team up with a group of people. Give everyone a specific role. One can be responsible for financing, the second for organization and distribution, the third for research and so on. Each role ensures an individual specialization in that field for the duration of the game, which doesn't mean that you need real experts. The goal is to make the facility of your building more sustainable. You can discuss changing windows, office supplies, electronics, whatever you think might help create a more sustainable space.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Theory U_** **by C. Otto Scharmer**

Today, there are major societal shifts underway, each of which creates major challenges for the leaders of today and tomorrow. Theory U describes how the so-called U-process can be used to tackle those challenges collectively and implement the solutions quickly. This is achieved by tapping into the blind spot, the source of our deepest creative instincts.
---

### Peter Senge, Bryan Smith, Nina Kruschwitz, Joe Laur, Sara Schley

A pioneer in his field, Peter Senge is the Senior Lecturer of Behavioral and Policy Sciences at MIT. In 1990, he published _The Fifth Discipline_, which sold more than a million copies and has been translated into over 20 languages.

