---
id: 53a15c573765650007230000
slug: the-net-delusion-en
published_date: 2014-06-17T00:00:00.000+00:00
author: Evgeny Morozov
title: The Net Delusion
subtitle: How Not to Liberate the World
main_color: 3380AC
text_color: 326480
---

# The Net Delusion

_How Not to Liberate the World_

**Evgeny Morozov**

_The_ _Net_ _Delusion_ tackles head on the beliefs we hold about the utopian power of the internet. Evgeny Morozov shows us how the internet isn't always a force for democracy and freedom, and reveals how both authoritarian and democratic regimes control the internet for their own interests.

---
### 1. What’s in it for me? Break the illusion that the internet will set the world free. 

No other recent invention has had such a wide influence on society as the internet. With its rise the world has become radically more connected and globalized.

Many Western thinkers believe that this connectivity will offer a whole new opportunity to spread democracy and freedom all across the planet: If people in authoritarian countries just had the ability to inform themselves about how life is better in democratic countries, democracy would spread automatically, wouldn't it?

While this is a tempting vision, reality disagrees. The internet _can_ be a force for good, but both authoritarian and democratic regimes have learned to manipulate the internet to fight for their slice of the globalized pie.

In these blinks, you'll learn how in some parts of the world, a simple "Like" on Facebook can get you arrested.

You'll also find out how authoritarian regimes use the internet to spread propaganda.

Finally, you'll discover why 1.7 million supporters on Facebook are incapable of raising $20,000 for a good cause.

### 2. The radio helped tear down the Berlin Wall, but the internet does not help overthrow modern dictators. 

What caused the fall of the Berlin Wall? Some argue that the informative power of Western radio played a key role. This creates hope that the internet will play a similar part in the collapse of modern authoritarian regimes — but in fact, the internet is not comparable to radio, and historical events can't be simply translated to the present.

This is because the fall of the Berlin Wall was a unique historical situation in which the power of radio came to the fore.

Before the radio was invented, there was no way for people to obtain information from independent sources. Regimes could easily control other mass media like newspapers and cinema, but not the open airwaves of the radio, which allowed people living in the German Democratic Republic — the Soviet part of Germany during the Cold War — to learn about what was going on outside their country. This new information led to more and more dissatisfaction with the regime, and eventually to the protests that brought down the Wall.

But even if the Internet is like the radio in some ways, the idea that it possesses the same political power is false.

Why?

Because the internet is easily manipulated.

Contrary to what many Western politicians think, the internet does not help overthrow modern dictators. They believe that like the radio, the internet will allow people living under authoritarian regimes to inform themselves about life in the Western world, and realize that life is better elsewhere. According to this line of thought, this awareness will eventually lead to a revolution within the repressive regime.

But what they don't realize is that unlike radio, access to the internet can be easily manipulated by authoritarian regimes. While the leaders of the GDR couldn't prevent their citizens from listening to radio channels from West Germany, modern dictators maintain control over the internet.

> _"Western policymakers simply can't change modern Russia, China or Iran using methods from the later 1980s."_

### 3. The internet doesn’t only threaten regimes – it can also strengthen them. 

It's tempting to think that the information available on the internet is the ultimate weapon against political oppression — but unfortunately, reality disagrees. In fact, the internet also helps support authoritarian regimes.

One reason for this is that internet bloggers can both be supporters as well as critics of authoritarian regimes.

Just as most Western bloggers who write about politics typically promote Western values, most bloggers from authoritarian regimes support the nationalistic and xenophobic values of their home regime — and often more virulently than the regime's own propaganda machine.

Authoritarian regimes can then encourage the bloggers who support them. For example, Maksim Kononenko, a conservative blogger from Russia who supports the government, even got to co-host his own prime-time TV show. And other bloggers affiliated with the Kremlin also get support from the Russian government.

The internet also allows critics of authoritarian regimes to be easily spotted.

Before the internet, opponents of governments organized in the underground. They held secret meetings to plan their protests, which made it hard for the government to find them. This is why in the German Democratic Republic, the Stasi — the official state security service — invested so much energy into monitoring citizens with bugs and informers.

But with the internet, it's a lot easier to monitor citizens — and therefore prevent protests. Not only can governments spy on their citizens with programs installed on their computers, as we'll see in a later blink, they can also track down dissidents with the help of social networks like Facebook. A simple click on the "Like" button of a critical video can turn a person into a target for political suppression. And when supporters of the regime with whom you are "friends" notice activity they consider "dodgy," they can inform the government.

### 4. Western countries manipulate and control the internet just like authoritarian regimes. 

Do you think that only authoritarian regimes like China censor and manipulate the internet? In fact, Western governments do the same.

Western governments use the internet to track down radicals and criminals in the same way that authoritarian regimes track down dissidents. For example, they attempt to shut down radical websites like far left or far right message boards, and track down their visitors and operators.

But this doesn't stop Western politicians complaining about the censorship and persecutions of political opponents in countries like China and Russia. For example, they demand that the blockage of certain websites like Google should stop. So even though the ideals that lead to censoring extremists might be noble, it's hard to believe in Western governments' idea of internet freedom if they don't practice what they preach.

Another way certain US-led Western governments manipulate the internet is by attempting to provoke riots against dictators.

When dissidents make use of the internet to organize their protests, Western regimes are happy to support them. For example, prior to the Iranian protests of 2009, the United States had imposed many sanctions against Iran, including the blocking of websites belonging to American companies. But when the protests broke out, American diplomats decided to loosen the sanctions, and allow Iranians access to American social networking websites like Twitter and Facebook. This in turn allowed the protestors to better organize themselves.

Why did the United States loosen the internet embargo?

Not because relations with Iran had improved. They did it to support the opponents of the Iranian government — which was in fact in their own interests. This is a clear example of how Western policymakers make use of their domestic industries — and especially internet companies — to advance their own foreign policies.

> _"As long as Western governments regulate the internet out of concerns for terrorism or crime, they also legitimize similar efforts undertaken by authoritarian governments."_

### 5. The internet allows intolerant people to easily organize themselves. 

Everyone knows that the internet makes it easier for people to connect with one another. But while some use the internet to organize Harry Potter meet-ups, other users with bad intentions connect with similarly minded people to organize for malicious purposes.

For example, criminals and narrow-minded people use the internet to connect with peers like everybody else.

Thanks to the internet's anonymity, people can express their secret dreams and desires online and connect with others in a way that's never been possible before. But this anonymity also means that criminals, racists and nationalists can organize themselves via the internet. Because the internet makes it easy to hide one's identity, people can easily spread their undemocratic and intolerant thoughts without fearing the repercussions. For example, there are platforms on the internet for hiring assassins, and message boards for planning attacks on homosexuals.

In Western countries, these kinds of platforms are often censored. But in authoritarian countries, intolerant people are not prevented from organizing via the internet — as long as their opinions support the government.

For example, in Saudi Arabia, Islamists who demand that the national law becomes Sharia based are not hindered from sharing their ideas. Sharia law is criticized by human rights organizations for — among other reasons — demanding the death penalty for homosexual activities. In this way, groups with anti-Western views can freely organize online — as long as they support the regime — while dissidents struggle to connect via the internet.

Modern dictators know where to suppress and where not to suppress on the internet. This way they indirectly strengthen their supporters, and weaken government opponents.

In the following blinks, you'll learn how authoritarian regimes manipulate the internet.

### 6. Modern technology allows the internet to be easily censored. 

With billions of web pages online and millions more added every day, you could think that it's nearly impossible to censor the internet. This is true in terms of mechanical censorship — but with the help of well written programs, modern regimes can achieve the impossible.

This is thanks to modern software that allows predictive and automatic censorship.

In many types of media, censorship has to be done manually: people must be paid to watch every program and read every article or book to decide whether it is appropriate for the society. And until the early twenty-first century, this also applied to the internet.

But lately, more and more sophisticated programs have appeared, which go beyond manually blocking access to a given list of websites. These programs can analyze what the user is doing and automatically guess if his behavior is allowed or not. For example, if there is too much fleshy pink on a website, it is automatically classified as having pornographic content — and blocked.

And using their considerable powers, dictators can force their people to install this kind of censorship program on their computers.

This is because people living in authoritarian regimes are not free to buy the products they want. In Communist states it is easy for the government to force the people to buy computers with pre-installed censorship programs. For example, the Chinese government announced in 2009 that all computers sold in the country had to have the software GreenDam installed — a program that tracks the user's activity, reports back suspicious behavior to a database and blocks "unwanted" content.

### 7. Authoritarian regimes use the internet to spread propaganda. 

The internet can be a powerful weapon in the struggle against authoritarian regimes. But the same regimes can also wield the internet to counter dissidents with their own propaganda.

For example, in authoritarian countries like Venezuela, websites like Twitter and Facebook are not only used by critics of the government, but also by the government itself. While many dictators initially tried to fight or ignore the internet, they've now realized its potential for propaganda, and have learned to use it for their own purposes.

Venezuela's late president, Hugo Chavez, was one authoritarian leader who tried to ignore the internet for a long time. But when he finally decided to join Twitter, he attracted 50,000 followers within 24 hours, and 500,000 within one month. By finally taking advantage of the internet's power, he was able to easily spread his authoritarian views — and become more popular at the same time.

Another way authoritarian countries spread propaganda through the internet is by backing key websites that are in direct competition with important Western websites.

In most Western countries, websites like Google, Facebook and YouTube are by far the most popular. But this is not true for countries like Russia and China. In Russia, vk.com — a Russian version of Facebook owned by a Russian company — is far more popular than Facebook, and in China, baidu.com — a Chinese search engine like Google — is the most visited website in the country.

Why is this dangerous?

Because it's easier for authoritarian states to control and manipulate websites owned by their own countries, which gives them direct control over what can be seen on the web — and therefore their citizens.

### 8. The internet can disempower potential activists and distract citizens from political issues. 

Everyone has been sent an email asking for signatures for an online petition. But when people support a cause online they may think that that gesture alone was enough — and that they don't have to engage with the issue directly.

For example, people can support a political cause by joining a group on social networks like Facebook. Psychologists have found that just joining one of these groups makes politically minded people as happy as writing a letter to their elected representatives — a method of political activism that's actually effective.

So what's the problem?

The problem is that although it makes people happy, this internet activism has no real impact in the vast majority of cases. But because internet activists have become satisfied by enacting their civic responsibilities online — or so they believe — they are less likely to express their protest in more effective, and sometimes more dangerous ways — like going into the streets.

For example, the Facebook group Saving the Children of Africa has over 1.7 million members, but the organization has only managed to raise about $12,000 — less than one-hundredth of a penny per person.

Besides discouraging people from showing their opposition in the offline world, the internet can also prevent people from even thinking about what is wrong in their country.

For example, the internet allows people to obsess about things which are only important to themselves. For instance, the most popular terms on Russian search engines are not "what is democracy," but rather "what is love" or "how do I lose weight." This shows that people are more interested in things that happen in their own bubble, rather than the politics that affect everybody.

And authoritarian regimes directly distract people from their political situation by supporting websites that produce apolitical entertainment. Russia.ru, for example, a website supported by the Russian government, produces more than two dozen regular video shows about frivolous topics such as a search for the perfect breasts in Moscow's nightclubs.

Remember that offline protests are normally far more effective than showing your protest online.

### 9. Final Summary 

The key message in this book:

**The** **internet** **does** **not** **only** **help** **to** **spread** **democracy** **and** **freedom** **–** **it** **can** **also** **distract** **its** **users** **from** **politics,** **distribute** **anti-democratic** **propaganda** **and** **allow** **intolerant** **groups** **to** **organize** **themselves.** **But** **authoritarian** **regimes** **aren't** **the** **only** **guilty** **parties:** **Western** **governments** **also** **manipulate** **and** **censor** **the** **internet** **as** **they** **see** **fit.**
---

### Evgeny Morozov

Evgeny Morozov is a writer and researcher who focuses on the political and social implications of technology. His work has appeared in the _New_ _York_ _Times_ and the _Wall_ _Street_ _Journal_ ; he is also the author of _To_ _Save_ _Everything,_ _Click_ _Here:_ _The_ _Folly_ _of_ _Technological_ _Solutionism._

