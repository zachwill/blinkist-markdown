---
id: 58d13885ed3a5700041581b4
slug: the-new-front-page-en
published_date: 2017-03-24T00:00:00.000+00:00
author: Tim Dunlop
title: The New Front Page
subtitle: New Media and the Rise of the Audience
main_color: 4F6EB0
text_color: 4F6EB0
---

# The New Front Page

_New Media and the Rise of the Audience_

**Tim Dunlop**

_The New Front Page_ (2013) explains how the advent of the internet radically changed the media landscape. Today, audiences are no longer a mere target for advertisers; they're empowered customers and, more often than not, even a part of the editorial process itself.

---
### 1. What’s in it for me? Step into the new media landscape. 

For the majority of the twentieth century, the media was dominated by a handful of enormous media conglomerates that practically had a monopoly on what was reported to the public.

The advent of the internet changed this forever. Business models that had worked for decades suddenly didn't cut it anymore and newspapers saw their subscription numbers plummet as more and more consumers turned to online sources.

Today, many newspapers are on their knees, desperately trying to find new ways to compete. Some people worry that this is the end of truthful and balanced reporting. But this shift has also triggered exciting innovations and, in many ways, the media landscape today is more varied and democratic than ever before.

In these blinks, you'll learn

  * why the media audience is no longer a mere product;

  * why the idea of a fourth estate isn't all it's cracked up to be; and

  * how the line between producer and consumer began to blur.

### 2. As the media landscape shifted, audiences went from being powerless products to influential customers. 

Whether you're turning on the television or firing up your web browser, you have a wide variety of channels to get your news from. These days, this is something people take for granted. But let's not forget that it's a relatively recent development.

Before the commercialization of the internet, there were a handful of media corporations that sought an audience for one primary reason: to sell advertising space.

The corporations were essentially factories, creating products designed to be appealing to advertisers with big bank accounts. And the bigger the audience, the more money they could charge.

Whether it was programming for radio, television or newspapers, the aim was always the same: maximize the profits.

In this sense, the audience wasn't so much a conglomerate of customers as a component of the product itself. But now the audience is no longer a voiceless mass; today, it's composed of true customers.

As the internet grew, audiences were given the ability to pick and choose where they got their news and entertainment — and, as a result, the big media outlets lost their power.

Every day there were new websites and blogs offering articles, videos and instant updates. People no longer had to rely on individual news providers for everything. They could now pick and choose.

What's more, rather than being a passive audience, people had the power to start their own blog or website and become their own news provider.

The time was ripe for this transformation. In the early aughts, bloggers became a popular source of news as the war in Iraq dragged on and turbulent presidential elections unfolded.

And while this was going on, social media sites like Twitter and Facebook began allowing people to compile their own "front page" made up of these various online news publications.

Today, media companies are under increasing strain to hold onto their audience. These companies do this by engaging with their audience and using audience opinion to create more desirable content — which means that, radically, the audience is no longer just a means to an end.

### 3. Old media was highly influential and saw itself as keeping the government in check. 

So how did the early relationship between the media and their powerless audience start?

Well, due to the prohibitive cost of running a media empire, be it television, radio or print, there were very few media outlets prior to the internet.

This was the case throughout the twentieth century, and this concentration of power allowed these corporations to become highly influential.

Since there were only a few of them, they could divide up the entire market, leaving each of them with an enormous audience and a huge reach. And with few competitors to worry about, they could completely ignore their audience and publish whatever news they wanted.

With this kind of power, they could swing elections and even push the government into passing favorable legislation. And their enormous audience shares meant they could charge a lot of money for advertising, which only made them more powerful.

To cover up this abuse of power, the big outlets marketed themselves as the protectors of democracy.

Rather than admitting to being the vessels for advertisements, media outlets pitched themselves as the "fourth estate," which was helping to keep democracy alive and well.

After the three branches of government — the judicial, legislative and executive — the media considered itself the fourth part of a functioning democracy — the one that kept the politicians' power in check.

According to the media, they were on the side of their audience, but this relationship has since begun to tarnish.

During the Iraq War, mainstream media failed to report on the Bush administration's false motives for starting the war. Nor did they report what was happening to the soldiers and civilians in the field. Only on blogs such as back-to-iraq.com could people read real and uncensored reports on the reality of the war, straight from the soldiers who were there.

Other sites emerged to serve as watchdogs for the mainstream media, too, pointing out their inaccuracies and failures to report the truth.

### 4. With the introduction of Blogads, internet media and its audience grew more powerful and influential. 

As the big media giants of the twentieth century made apparent, the amount of power and influence they were able to wield depended on the amount of advertising money they were able to generate. And as the websites of the new media became increasingly popular, they've found themselves in a similar position.

But unlike the old media, the new media didn't begin selling ad space. Instead, they derived income — and thus power — from Blogads.

Founded in 2002, Blogads was essentially an aggregator that brought together a large amount of blogs into a single market. As a result, advertisers began to look at these blogs in a more favorable light and the money these sites brought in was quickly put to use to make the blogs look more professional and respectable.

Consider the _Huffington Post_. Once a humble blog, it began generating a steady revenue of advertising money and is now a proper and influential media organization.

The advertising money also meant that blogging could be a legitimate profession — or at least a stepping stone to one. Many popular bloggers were eventually picked up by mainstream media outlets and became legitimate journalists.

This was the career path of Matthew Yglesias, who started out as a blogger before being hired to write for the _American Prospect_, _Slate_ and the _Atlantic_.

Oddly enough, as more bloggers became journalists, more journalists became bloggers. Big media and new media began converging into one entity.

And thus the definition of "journalist" and "blogger" started to blur.

Newspapers like the _New York Times_ and the _Washington Post_ hired bloggers to work as journalists. Simultaneously, journalists who were already on staff were told to start working on blogs for the newspaper's website, or they left their jobs to start their own blogs.

Andrew Sullivan, a British journalist based in the United States, went full circle: he quit working for the _New Republic_ to start the _Daily Dish_ blog, where he wrote about economics and politics. The site became extremely popular, and he, along with several other bloggers, was eventually hired by the _Atlantic_ to write for their website.

### 5. The public now plays a vital role in which stories are reported on and how they are covered. 

Even though the lines have blurred between "blogging" and "journalism," one thing is clear: media outlets are no longer closed and exclusive systems. Just a couple of decades ago, journalists wrote stories to a largely voiceless audience. It's now impossible _not_ to hear the voices of this mass of people — and they're included every step of the way.

Many of the big media outlets even bring their audience into the newsroom.

On the website of the _Guardian_, one of the United Kingdom's largest and most global newspapers, you can voice your opinion on what stories the outlet should cover.

There's a similar feature at the _Atlantic_, the influential US magazine. Through a service called Open Wire Initiative subscribers have a direct line to the publication's writers and editor. Users must log in, and then they can leave comments to help guide the process of generating and publishing certain stories.

The US newspaper the _Register_ has taken things even further. They've practically made their audience the editor.

At this outlet, writers are completely at the mercy of the readers, who not only suggest ideas and topics but also vote on which articles the reporters are assigned. The audience also holds the journalists accountable for any inconsistencies — even going so far as to question the quality of their writing style and the validity of their sources.

This is reporting done in a completely transparent way, freely showing the readers where their information is coming from.

Clearly, the media audience has come a long way. Once a powerless product of the media's effort to rake in advertising money, they are now empowered customers who play an integral role in the media's ever-evolving processes.

### 6. Final summary 

The key message in this book:

**Since the advent of the internet, the media landscape has evolved in many ways. A crucial part of this transformation has been the way it interacts with its audience and readership. Newspapers, radio and television once viewed the public as nothing more than a number that increased their advertising rates. But with the introduction and popularization of the internet and news blogs, the audience has become involved in every part of the production and publication of the news.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Breaking The Page_** **by Peter Meyers**

_Breaking The Page_ (2014) explores the new possibilities ebooks offer to readers. Throughout these blinks, you'll learn about the differences between traditional books and ebooks, and why we need to rethink what a book _is_ in order to make the most of the powers of the digital wor(l)d.
---

### Tim Dunlop

Tim Dunlop, a pioneer of political blogging in Australia, currently writes for _The Drum_. His websites, _Blogocracy_ and _The Road to Surfdom_ led him to be the first blogger in the nation to be hired by a major media organization. He also holds a PhD in political philosophy and communication and teaches at Melbourne University.

