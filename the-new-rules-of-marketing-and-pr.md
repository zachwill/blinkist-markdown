---
id: 574ffbd862588b00033e0e5f
slug: the-new-rules-of-marketing-and-pr-en
published_date: 2016-06-07T00:00:00.000+00:00
author: David Meerman Scott
title: The New Rules of Marketing & PR
subtitle: How To Use Social Media, Online Video, Mobile Applications, Blogs, New Releases & Viral Marketing to Reach Buyers Directly
main_color: 2CAFDE
text_color: 29728C
---

# The New Rules of Marketing & PR

_How To Use Social Media, Online Video, Mobile Applications, Blogs, New Releases & Viral Marketing to Reach Buyers Directly_

**David Meerman Scott**

_The New Rules of Marketing & PR _(2007) is your guide to the world of online marketing strategies. These blinks explain how and why communicating with authenticity and agility on social media will help you reach more customers and take your brand to the next level.

---
### 1. What’s in it for me? Learn how to ramp up your company’s marketing efforts with social media. 

Advertising has changed a lot since the "Mad Men" era of the 1960s. Gone are the three-martini lunches and huge, sprawling campaigns that took months to plan.

Any savvy business executive today knows that advertising broadly, across a number of media channels, is neither effective nor fruitful. Gone too are the days of spending huge amounts of cash on newspaper, radio, magazines or television placements.

Today, thanks to the internet, marketing and public relations offer many more opportunities than before. These blinks will walk you through how to produce effective marketing in our digital age.

In these blinks, you'll learn

  * how to react if your company gets bad-mouthed on social media;

  * how local blogs helped Barack Obama fine-tune his presidential campaign; and

  * why a podcast could help get your business the brand recognition it needs.

### 2. Revolutionize your brand with a robust online presence and say goodbye to old-fashioned marketing! 

The internet is a powerful tool for many things, from downloading movies to video chatting with loved ones. But it has also transformed the face of business, and not just through online shopping! Importantly, the internet has _revolutionized_ marketing.

Well-designed websites, powerful images, and engaging audio or video clips are the new tools marketers can use to encourage dialogue, gain publicity and raise awareness for a brand.

The internet's power stems not only from the fact that it's global but also that it's fast and _free_. There's no question that with such credentials, the internet holds incredible potential for marketers.

So how do today's marketing geniuses use the power of the internet?

One example is a rainforest eco-resort in Western Belize, the Lodge at Chaa Creek, which has a talented content team. This team provides potential travelers with relevant, well-crafted stories on their blog.

With posts such as "Ten Reasons Why Belize Makes a Honeymoon Blissful," as well as other engaging stories about what young and environmentally conscious adventurers can do in the country, the Lodge has become a respected source of information about tourism in Belize.

And even though the blog's content seldom advertises the Lodge at Chaa Creek directly, many of the blog's readers often end up staying there. Some 80 percent of new bookings are the result of intelligent content marketing, specifically created to show readers that the lodge is a business that has its customers' best interests at heart.

Are you ready to create a brilliant online presence for your brand? It's all a matter of taking the right steps, as we'll find out in the next blinks.

### 3. Ignore social media at your brand’s peril. Get involved and start the conversation! 

These days, customers can scrutinize your brand on social media and publish their own thoughts to an audience of hundreds, if not thousands, of people. Whatever they communicate can make or break a company's reputation!

If you want your brand to thrive in this hyperconnected world, there are a few things to keep in mind.

The first is the importance of open communication, a particularly valuable guideline when your brand is facing a potential public relations crisis.

Sony BMG Music didn't take this guideline to heart, and ended up paying a steep price for it.

Mark Russinovich, the chief technology officer at Microsoft's cloud computing platform Azure, posted an article in 2005 exposing potentially harmful copy protection measures on CD software issued by Sony BMG Music. The post quickly went viral, generating a lot of criticism of Sony.

Despite the international outcry, Sony stayed quiet for a lengthy period. Finally, Sony's global digital business president Thomas Hesse addressed the issue during a radio interview, but his condescending attitude coupled with the delay only made things worse. The crisis spiraled out of control, and Sony eventually faced a host of state lawsuits questioning the legality of its software.

Many business leaders ignore what's being said in real time on social media about their business. This was Hesse's error.

Consider how Sony's situation could have been quickly resolved with a _timely response_. Had Hesse responded to the crisis as soon as he knew about it, and had he delivered an official apology and presented a plan to correct the issue, Sony might have maintained its positive reputation.

Smart companies, on the other hand, know how to harness the power of social media for themselves.

The Canadian Tourism Commission's "Upgrade to Canada" program was an inventive, daring social media campaign that gave the country a leg up amid a competitive travel market.

The commission began by inviting travelers at airports in Frankfurt, Germany, and Lyon, France, to change their vacation plans on the spot and travel to Canada for free.

Surprisingly, many travelers took a leap of faith and accepted the offer. These people also agreed to become social media advocates for the tourism commission and share their adventures in Canada on social media.

This program generated a lot of buzz around the Canadian Tourism Commission, with the result that Canada saw a 21.5 percent increase in its share of tourists compared to other competitive tourist destinations.

### 4. Don’t underestimate the power of blogs, as they are powerful tools for promoting brand awareness. 

Wouldn't you like to know what people are saying about your company? Granted, the truth might not always be pleasant, but once you start seeing such chatter as brand feedback, it can be incredibly valuable. This is why the importance of bloggers should never be underestimated.

A blog is a personal website, in which someone who either specializes or is interested in a certain topic shares stories and opinions. Blogs can offer fascinating reading material and often build followings with thousands of dedicated readers.

A positive mention on a popular blog is like someone recommending your brand to a packed auditorium full of listeners who are hanging on every word. Get the picture?

Glenn Fannick, an expert in text mining and media measurement, stresses the importance of staying up to date with the chatter in the "blogosphere." Fannick says the vast amount of comments and text in blogs can contain valuable information, which companies can harvest through text-mining software to establish a 360-degree perspective on emerging trends.

Blogs are also a great platform for talking with customers who matter to you. US President Barack Obama's first presidential campaign used this to great effect, as the campaign focused on generating support through content creation on social media platforms.

Obama's New Media Blogging team was directed by Kevin Flynn, an expert who came from a group of Chicago-based online campaigners. Flynn created blogs for 15 states, each blog containing carefully curated and localized content.

Readers were encouraged to submit stories and photos, and this engaged and mobilized whole communities. The blogs received lots of valuable feedback and generated a range of powerful dialogues, which Flynn cites as the inspiration for improvements in the overall campaign.

### 5. Use video and audio clips to tell your brand’s story and build a loyal online following. 

Creating videos and sharing them across multiple online platforms has never been easier than today. In fact, video is primed to become the leading form of digital marketing.

But what should your videos be about? To take advantage of the power of video to communicate your brand message, you've got to tell an _authentic story._

Say you want to advertise a sunny, sandy beach. Unimaginative marketers might shoot images of the sea, the sand and a few smiling tourists. But if you want your destination to stand out, you need to tell the unique story behind the destination. Sometimes you can even let your audience tell the story for you!

This is what Tourism Queensland did in its 2009 campaign to raise awareness about Australia's Great Barrier Reef. The team created a video contest, called "The Best Job in the World." Applicants were asked to create a one-minute video explaining why they should be named caretaker of Hamilton Island on the Great Barrier Reef.

More than 30,000 applicants applied, and their application videos generated millions of views. What's more, the Great Barrier Reef as a tourist destination gained tremendous attention through the innovative campaign.

Another way to boost your brand's online reach is through audio content. In recent years, podcasts have emerged as a particularly strong marketing tool. Unlike radio stations with static programming, podcasts allow listeners to subscribe to a particular channel, which then delivers clips on topics they may be interested in.

VelocityPage cofounder Jon Nastor saw podcasts as an opportunity to generate attention for his business. He created the podcast, "Hack the Entrepreneur," in which he interviews entrepreneurs about their drive to bring useful "life hacks" to customers.

Nastor saw an almost 50 percent increase in sales as a result of his popular podcast. Even though the podcast isn't directly related to VelocityPage, a WordPress page builder, Nastor's listeners like his show enough to want to learn more about its producer — and are then impressed by his product!

### 6. Not every video goes viral; great content is based on big ideas and individual creativity. 

Going "viral" has a lot to do with luck and timing. But there are patterns to which a savvy marketer should pay attention. If you want to generate buzz for your brand, you can use these patterns to your company's advantage.

First, you need an idea that people will _want to share_. And although you might think your product is great, chances are a simple story about it won't go viral.

You need to think bigger. Ideas and stories that fascinate, entertain, provoke thought and inspire people are what will get your content to spread like wildfire.

Similarly, people will lose interest if you blatantly try to offer them a deal. Giving away free memberships for a limited time is just not that appealing!

So what can you do?

Be creative and think of other ways to mobilize viewers to share your story. What motivates your target audience? And why? These are questions that should guide your strategy.

Sometimes, the best viral content doesn't start with your company but with your viewers. If one of your customers is doing something fascinating online, support them!

This is what candy company Mentos did after witnessing two experimenters, Fritz Grobe and Stephen Voltz, mix a Mentos chewy mint lozenge with a glass of Diet Coke, creating an explosive chemical reaction that the two filmed and posted on YouTube.

The duo immediately wanted to take their experiment to the next level, mixing 200 liters of Diet Coke with over 500 Mentos. This video went viral, and with it, Mentos saw a brilliant marketing opportunity.

The company hosted the video on the official Mentos website and sponsored the two experimenters as they appeared on talk shows to explain their wacky stunt. Such viral marketing added to Mentos' brand story as a company that isn't afraid to take risks and have fun.

### 7. Offer real-time, behind-the-scenes updates on brand projects to get your followers excited. 

For better or worse, the internet has changed our lives forever. With instant communication and endless information at our fingertips, daily life moves at a much faster pace than before.

The same goes for doing business, and the most successful brands know how to act quickly to get the marketing results they want.

These days, brands that can move in real time will get more publicity. This was the motivation behind energy drink company Red Bull's decision to sponsor skydiver Felix Baumgartner's freefall to earth in 2012.

Baumgartner broke world records during his jump, falling faster than the speed of sound!

Thanks to Red Bull's online live stream, this feat was witnessed by eight million people in real time on YouTube. Red Bull is just one of many companies that have used live streaming tools to their advantage, giving viewers access to incredible events and at the same time, generating great publicity for their brands.

Offering people inside or exclusive access to certain events is a surefire way to keep them engaged and keen for more content. Continuous social media updates, for example, have proven more effective than traditional, occasional PR outreach.

Here's a great example of the differences between these two strategies. Actor Tom Cruise, promoting his blockbuster film _Edge of Tomorrow_, pursued a conventional PR campaign, giving TV interviews and so on.

Author John Green, in contrast, promoted the film adaptation of his book, _The Fault in Our Stars,_ by keeping fans updated on the movie's production, with behind-the-scenes info and insider tips. His followers, as a result, felt they were part of something special, creating a sense of community.

_The Fault in Our Stars_ earned over $58 million during its first weekend at the box office, and it was filmed with a budget of just $12 million! Meanwhile, _Edge of Tomorrow_ earned just $29 million, despite its whopping $178 million budget.

Sharing is caring, and when promoting a brand, it clearly pays off!

### 8. Get to know your buyers so you can tailor your brand strategy to their particular needs. 

Let's go back to the basics for a moment. If you want to target the right customers, you first have to know who they are. For this purpose, you need to establish _buyer personas_.

Buyer personas are the foundations for any effective marketing campaign. These personas allow you to create a model of your ideal customer, and then tailor your branding accordingly.

Let's say you want to sell tricycles. Preschool-aged children typically ride them. Yet your "customers" aren't exactly the people who will wander into a store and buy a tricycle — the child's parents or grandparents do, instead.

Here, you already have _two_ buyer personas, and each holds a different idea about your product.

Parents tend to choose cheaper, more basic models, as they know their child will grow quickly and graduate soon to a two-wheeled bike. But when grandparents make a purchase, they're more likely to opt for a nicer, more expensive model for their cherished grandchild. By creating two marketing strategies to make the most of these different perspectives, you can ensure you make both kinds of customer happy.

Once you've identified your buyer personas, you need to find out as much about them as you can! What do they like, what do they hate? What problems are they struggling with, and who shapes their worldview? Who or what is most important to them?

These are a lot of questions, sure, and the answers aren't always clear. Here's a suggestion: why not get inside your customers' heads with interviews?

This is what consumer electronics brand Beko does. Because it operates in a variety of markets with differing cultures, Beko always collects market intelligence by interviewing potential customers. Through such interviews, Beko discovered that in China, drying clothes in the sun is an indispensable part of domestic culture.

So the company created a dryer that only half-dried clothes, allowing customers to finish the drying process outside on the washing line. By listening to potential customers, the company ensured its brand and products were a hit in China.

### 9. Final summary 

The key message in this book:

**In today's information age, creating an authentic, engaging online presence is the key to successful marketing. Sharing stories on social media as well as audio and video content is one part of the plan. A brand also needs to get its community engaged with authentic storytelling to create a revolutionary marketing campaign in the twenty-first century.**

**Actionable advice:**

**Get your brand a blog!**

Looking to get your business online? Blogs are a great place to start. For inspiration, look at the blog of Alacra, which creates workflow solutions for financial institutions. Its blog adds a face and a story to the company in a way that traditional press releases never could.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Newsjacking_** **by David Meerman Scott**

_Newsjacking_ (2012) is about the best new way to get media attention — not by planning ahead but by reacting quickly and cleverly. These blinks not only explain how this new form of generating news works, but also how you can and should react to it, and how to use it to attain the best possible media coverage.
---

### David Meerman Scott

David Meerman Scott is a marketing and sales strategist, speaker and bestselling author of internationally acclaimed books such as _Real-Time Marketing and PR_ and _The New Strategic Selling_. Scott was previously vice president of marketing at NewsEdge Corporation, now owned by Thomson Reuters.

