---
id: 5c236f166cee070007b45d4f
slug: the-new-silk-roads-en
published_date: 2018-12-28T00:00:00.000+00:00
author: Peter Frankopan
title: The New Silk Roads
subtitle: The Present and Future of the World
main_color: AB3738
text_color: A22022
---

# The New Silk Roads

_The Present and Future of the World_

**Peter Frankopan**

_The New Silk Roads_ (2018) explores current affairs and political trends from an Eastern perspective. Using up-to-date examples and staggering statistics, the blinks explain the complicated global relationships and alliances at play in international relations today.

---
### 1. What’s in it for me? Is the sun setting on the West? 

Savvy historians now know that the world of today wasn't just shaped by the Western world of yesterday. In fact, the Eastern world, particularly those countries situated along the Silk Roads — the ancient trading routes running through China and Central Asia — were also hugely significant in shaping the modern world.

But that's ancient history. What about now? What role is the East playing in international affairs today?

Join us as we discover how Eastern nations are shifting the planet's center of economic gravity — with huge consequences for people across the world. In a changing economic reality, we'll learn about the new alliances being forged, the political backlash and the implications of the East's rise to global dominance.

We'll journey along the ancient Silk Roads, getting a contemporary picture of how these old trade routes are being revitalized and how they might challenge the Western status quo in the very near future. Furthermore, we'll look at the impact of the East's rise on some of America and Europe's best-loved brands and institutions. It's going to be an exciting ride.

Read on to discover:

  * why China is making new friends in Europe;

  * how the ancient Silk Roads are making a modern comeback; and

  * what impact Western politics is having on China's rise.

### 2. Newly wealthy Easterners are buying up Western trophies and shaking up the travel industry. 

In past centuries, wealthy Englishmen would flock to places like Rome and Venice as part of an extravagant travel tradition known as the Grand Tour. While abroad, they often snapped up expensive pieces of local culture to bring back to England. Precious objects such as sculptures, paintings and exquisite furniture were bought and shipped back home. But how could they afford such trophies? Simply because military and commercial accomplishments had transformed England into a vastly wealthy worldwide superpower.

But at the dawn of the twenty-first century, everything has changed. The world's center of economic power is shifting from the West to the East. Consequently, international trophy hunting still goes on, but the people indulging in it are no longer English. Instead, they are wealthy citizens of countries such as China and Russia.

Significantly, the modern-day trophies the new, non-Western elite covet are the football World Cup tournament, which has been snapped up in recent years by Qatar and Russia, the Winter Olympics, which was also held in Sochi, Russia in 2014, as well as magnificent, Western art galleries. For instance, when the Louvre wanted to open a new museum, they didn't pick Paris, they chose Abu Dhabi, United Arab Emirates. London's Victoria and Albert Museum plumped for Shenzhen, China, when deciding where to put their new museum.

And the trophy hunting doesn't stop there. World-famous English stores such as luxury department store Harrods and toy shop Hamleys, as well as influential newspapers like London's _Evening Standard,_ are all in the hands of owners from Emirati, Russian or Chinese backgrounds.

Interestingly, the Eastern world's economic rise has other consequences that will affect Westerners far more than simply losing ownership of their most famous brands.

For instance, over the last 30 years, there has been a five-fold increase in Chinese tourists' spending abroad. This spending has risen $500 million to a whopping $250 billion annually, twice as much as American tourists now spend abroad. This will have a huge impact on the tourism industry. Everything from airlines and online booking agents to the style of rooms in hotels, to what drinks are on offer in hotel bars is changing to reflect the rise of tourism in China. This recalibration of the travel industry will only accelerate since Chinese tourism is still in its infancy. Just one in 20 Chinese people have passports currently; there is plenty of room for more Chinese tourism.

### 3. As the West seeks economic isolation, the East is embracing free trade and economic partnership. 

Across the Western world, governments and ordinary voters are turning to the politics of nationalism and actively lessening cooperation with other Western powers. Just consider Donald Trump's 2016 election slogan: "America First," a rallying cry for the United States to isolate itself. And perhaps the biggest symbol of Western Europe's increasing division is Brexit: Britain's 2016 decision to turn its back on its neighbors and leave the European Union.

While Western powers have been fragmenting and isolating themselves, much of the Eastern world has been quietly doing the opposite: consolidating ties between nations, building more effective collaboration and finding mutually beneficial solutions to shared challenges.

Just consider the way in which states situated along the historic Silk Roads in Central Asia have recently been collaborating. In 2017, Uzbekistan and Turkmenistan unveiled a new bridge over the Amu Darya river, a crucial intersection between the two countries. This bridge enables a railway connection between the nations and opens up exciting new trade possibilities.

Exchanges are growing in other countries located along the Silk Roads, too.

For instance, trade undertaken between Uzbekistan and Kazakhstan rose by over 30 percent in 2017, and doubled between Tajikistan and Uzbekistan in the first half of 2018, compared to the previous year. In other words, while Western powers seek more trade tariffs and greater border controls with their neighbors, the East has focused on opening up possibilities for international commerce.

Furthermore, this boom in economic cooperation and trade has been made possible by important multilateral economic institutions that have sought to increase dialogue and deepen connections between nations.

For instance, the _Regional Comprehensive Economic Partnership_ ' _s_ members include Southeast Asian nations as well as South Korea, China, India and Japan, among others. Combined, these nations have a GDP of around $30 trillion, and the partnership encompasses 3.5 billion citizens. Thanks to the work of this mammoth institution, negotiations are underway to form a comprehensive economic agreement between all parties in the Regional Comprehensive Economic Partnership. If these negotiations succeed, this will lead to what economists are pronouncing the biggest free trade agreement in human history. In other words, while Europe and America are seeking to undermine their free trade agreements by voting for Brexit and Donald Trump, the East is seeking economic neighborhood harmony.

### 4. China is financing the new Silk Roads in a bid to boost trade and demonstrate leadership. 

In September 2013, China's President Xi Jinping spoke at Nazarbayev University in Kazakhstan and unveiled his vision for the future. It was his foreign relations priority, President Xi explained, for China to build a better relationship with her neighbors. The time had come, he said, to deepen trade and economic cooperation and to revitalize ancient bonds between Central and South Asia. In other words, the president concluded, China would build the new Silk Roads.

Significantly, Xi's idea for wider Asian cooperation was nothing new. Both the Obama and Bush administrations in America had paid lip service to the idea of revitalizing trade, communications and transport between Eastern nations. However, in the last five years, Xi has done much more than simply talk about this initiative. Instead, he has put up huge sums of Chinese money in his mission to forge greater connections throughout the East.

In 2015, the Export-Import Bank of China began financing what will eventually be over a thousand projects in nearly 50 nations, as part of what is now known as the Belt and Road Initiative. The countries involved include not only those of South East and South Asia, the Middle East, Eastern Europe and Turkey but also countries in the Caribbean and Africa. The inclusive scale of China's vision means that 4.4 billion individuals will live in this sphere of Chinese influence. These new trade routes, which will run between the Eastern Mediterranean and China, will thus account for over 63 percent of the global population, and around 30 percent of the world's total output.

Furthermore, the ambition of Xi's Belt and Road Initiative goes far beyond just trade and money. The president has stated that China's goal is to usher in a new sort of international affairs, in which partnership and dialogue are prioritized over confrontation, and friendship is emphasized rather than simply alliances.

Interestingly, with its emphasis on harmony, friendship and non-confrontational politics, China is hoping to capitalize on several broader trends. Firstly, it is attempting to provide some hope for other nations during a period of increasingly global change. Secondly, China is stepping into the vacuum created by the United States' and Europe's increasingly self-centered and isolationist political narratives. And lastly, the Chinese are hoping to demonstrate China's suitability, not just for being part of the world's national community, but also for providing positive global leadership that focuses on international cooperation for the benefit of all.

> _"The people of various countries along the ancient Silk Roads have jointly written a chapter of friendship that has been passed on to this very day."_ — President Xi Jinping of China

### 5. Disturbed by increasing foreign ownership of American brands, Americans are blaming China. 

In almost every respect, the world is a better place today than in the past. Access to health care and clean drinking water has increased, as has the provision of fast and cheap transportation, reliable communication and energy networks.

However, the fact that twenty-first-century humans have a lot to celebrate does not make it easier to accept the way in which our world is changing as much as it is improving. Indeed, for many Americans, used to being the world's undisputed superpower, the future seems downright disturbing rather than hopeful.

Significantly, American society is disturbed by the takeover of many of its most iconic firms — such as General Electric's Appliance Business — by foreign business owners. And many of these new owners hail from countries along the Silk Roads.

A good illustration of this odd new reality is the recent sale of a firm that quarries Italian marble. This particular marble was used to construct the Peace Monument in Washington D.C. and New York's 9/11 memorial. But in recent years, the controlling stake in this firm has been sold to the Bin Laden family. It is thus confusing and upsetting for many Americans that the materials used in the 9/11 monument come from a company owned by the relatives of the figure behind the attack.

Perhaps inevitably, acquisitions such as this have led to much soul-searching in America and calls to ban company sales to foreign owners. In particular, China, as a rising global superpower that has purchased significant amounts of American industry, has been singled out for demonization.

In a 2016 speech, then-presidential candidate Donald Trump made the extraordinary claim that China was waging "economic war" on America and that they were perpetrating "the greatest theft" from the American people. When Trump became president, he escalated hostilities further by imposing trade tariffs on over a thousand Chinese products, affecting around $60 billion in imports.

But crucially, Trump's thirst for a trade war with China is wholly misguided and will likely hurt American consumers just as much as it does the Chinese.

For instance, the CEOs of influential retailers such as IKEA, Costco and Gap have all advised the White House that slapping tariffs on China will merely drive up the prices of household essentials such as electronics, clothing, home products, and shoes. These price hikes, they correctly point out, will simply punish ordinary Americans, whose household finances are already under strain from the effects of globalization.

> _"A policy has taken root that sees that the best way to manage China is to put pressure on its economy."_

### 6. Regional conflict in the Middle East is complicating America’s future weapons deals. 

In 2017, the US State Department lost nearly two-thirds of its longest-serving ambassadors. This was disastrous, given that ambassadors are America's greatest resource of international knowledge and experience.

Unsurprisingly, America's recent lack of foreign know-how has led the government to stick with what it knows best when it comes to foreign policies and allegiances. And for the United States, what it knows in the Middle East is Saudi Arabia.

Despite the inconvenient detail that several families of 9/11 victims are in the process of suing the Saudi government in court, the United States continues to make the country a pillar of its foreign policy and commerce. This is not just due to Saudi Arabia's oil wealth but also the sheer amount of money the country shells out on buying American-made weapons. For instance, during Barack Obama's presidency alone, Saudi Arabia bought a staggering $112 billion of weapons, including a single deal in 2009 that was worth more than $60 billion.

Unsurprisingly, President Trump has lost no time in continuing America's relationship with Saudi Arabia, flattering the Saudi crown prince in 2018 that he regards the Kingdom as "a very great friend." Unfortunately for America, their relationship with the Saudis is becoming increasingly complicated.

Why? Because of the Kingdom's escalating conflict with the Gulf state of Qatar.

For instance, in 2017, President Trump expressed words of friendship for Qatar's leader, telling him that he considers their two countries' relationship to be "extremely good." But unfortunately for Trump, relations between his two Middle Eastern "friends" are not so warm. Not only has Saudi Arabia recently imposed a blockade on Qatar, but relations between them have also deteriorated to the extent that Saudi Arabia is even planning to dig a new canal in the region in a bid to cut Qatar off from the Arabian Peninsula's mainland. All these hostilities have put the United States into a difficult position, given that it had intended to keep supplying plenty of weapons to both countries.

Given the escalating situation between Qatar and Saudi Arabia, people throughout the Middle East are listening closely to Trump's announcements to predict on whose side America will eventually come down.

### 7. Europe’s disharmony allows China to make new friends and influence the European Union. 

In recent years, the European narrative has been one of separation, regaining national control and putting up barriers. Brexit is the most obvious example of this friction, but the rise of anti-EU parties in Hungary, Poland and other European nations, as well as vocal independence movements in Catalonia and Scotland, also attest to a growing threat to European solidarity.

In the wake of European disharmony, China has seen an opportunity to further its own interests.

Many commentators have been surprised by Europe's failure to reprimand or sanction China for its creation of human-made islands in the South China Sea that could potentially be used as military bases. Indeed, the president of the European Council, Donald Tusk, stated in 2016 that a "tough stance" would be taken in regard to China's actions. The fact that this robust rebuke never materialized is thanks to China's canny approach to European discord. Specifically, the country used its connections with increasingly eurosceptic EU members such as Croatia, Greece and Hungary to ensure that the European Union was not able to agree on firm action against them.

China's success in watering down the European response to its actions in the South China Sea is thanks to its continued strategy of cultivating new relationships across the world. While many European Powers such as Britain are seeking to weaken their friendships with their neighbors, China is seeking to strengthen them.

For instance, the 16 + 1 Initiative is a Beijing-driven project providing a dynamic forum for conversation between China and no fewer than eleven member states of the European Union. These nations are drawn from both Eastern and Central Europe, including Bulgaria, Poland and the Baltic states. Importantly, these European states are receptive to the Chinese hand of friendship not only because of the future financial investments that China might make in their countries but also because they are feeling increasingly overlooked by Western Europe. For instance, Gjorge Ivanov, the leader of the Former Yugoslav Republic of Macedonia, a Balkan state that is part of the 16 + 1 Initiative, has accused Europe's leaders of failing to make the Balkan states EU members, despite "its promise to do so." This neglect of Eastern Europe, he went on, has compelled powers such as China and Russia, to step into the regional development role that should have been filled by the European Union.

> _"China currently seems to be the only country in the world with any sort of genuinely global, geostrategic concept."_ — Sigmar Gabriel, German Foreign Minister

### 8. Final summary 

The key message in these blinks:

**Across the modern world, the rise of the East is being felt. From the increasing foreign ownership of iconic Western brands to China's mammoth investment across the Eastern world, the writing is on the wall: the West's era of unrivaled global dominance is over. Furthermore, this decline is being hastened by isolationist Western politics, as well as Europe and America's failure to provide global leadership.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Silk Roads_** **by Peter Frankopan**

Now that you've discovered the impact the Eastern world is having on our present and our future, why not take a look at its enormous influence on history? Join the same author, Peter Frankopan, as he takes us on a tour through the ancient Silk Roads, the networks of trades that stretched between the East and the West.

Touching on the birth of Christianity, the death of the Roman Empire and the spread of Islam, we'll learn about the history of the world from an Eastern viewpoint. So, to broaden your perspective, head over to the blinks to _The Silk Roads_.
---

### Peter Frankopan

Peter Frankopan is Professor of Global History at Oxford University and director of the Oxford Centre for Byzantine Research. He is also the international bestselling author of _The Silk Roads: A New History of the World_ (2015).

