---
id: 572f0176a35db30003e6be15
slug: the-new-strategic-selling-en
published_date: 2016-05-13T00:00:00.000+00:00
author: Robert B. Miller, Stephen E. Heiman, Tad Tuleja
title: The New Strategic Selling
subtitle: The Unique Sales System Proven Successful By the World's Best Companies
main_color: BF2646
text_color: BF2646
---

# The New Strategic Selling

_The Unique Sales System Proven Successful By the World's Best Companies_

**Robert B. Miller, Stephen E. Heiman, Tad Tuleja**

_New Strategic Selling_ (1995) teaches you how to close sales in the most positive, beneficial way for both you and your buyer. Long-term success in sales is all about building relationships, satisfying your customers and identifying the key factors that influence the sale, and these blinks will set you on your way.

---
### 1. What’s in it for me? Discover the essential steps to becoming a successful salesperson. 

Being a salesperson has never been easy; just think of all those door-to-door sellers, traipsing through whole neighborhoods of uninterested prospects. At least today, the fact that so much business is conducted online surely makes it much easier than it used to be.

Well, perhaps not.

Today, markets are constantly changing, job positions and roles are coming and going, while companies are downsizing or merging in the blink of an eye. As a result, making a sale is not so easy. You need the right tools, the right people and the right plan at the right time — otherwise, your efforts will be in vain.

And this is where these blinks can help. They'll provide you with great insights into the world of selling, and show you which factors to be aware of when hoping to become a successful salesperson.

In these blinks, you'll discover

  * which people you simply cannot afford to ignore;

  * why comparing a military strategist with a salesperson isn't so far off the mark; and

  * how specific you have to be with your sales objective.

### 2. Your selling ability depends on your strategy and tactics. 

Imagine you're managing Manchester United and you're set to play against Real Madrid next week; you wouldn't come into such an important match without a carefully considered game plan. Sales work the same way: you need a _strategy_.

A lot of companies fail to develop a strategy and make the mistake of planning as they go. The author has found that most of the sales representatives who attend his _Strategic Selling_ workshop don't spend much time planning, and prefer to focus on the sales process itself.

In other words, they focus on their _tactics_, what they do during this process, instead of their _strategy_, the overall plan for selling products or services.

However, strategy and tactics go hand in hand. Think of your strategy as homework; set out a plan in advance, but be prepared to adapt it during the actual sales process.

You can't have lasting success in sales without a strategy, just as you can't go into battle without setting up your troops first.

Strategy and tactics are equally important because they cover different objectives for the salesperson. Strategy is about long-term goals whereas tactics are about the short term.

Your short-term objective might be to make as many individual sales as possible, for instance. A long-term objective might be to maintain a good relationship with your clients so they'll stay open to new deals in the future. Tactics help you make individual sales and strategy covers your accounts.

Strategy and tactics don't work without each other, either. You might use clever tactics to get a company to buy your products quickly, but if they don't suit the company's long-term needs they won't buy from you again — or recommend you to anyone else.

### 3. Assessing your company’s current position is the first step in outlining a good strategy. 

Strategy is about planning — but where do you get started? The answer is simple: _positioning_.

You have to assess your current situation before you can outline a good selling strategy. Get to know your position in the market, so you can adjust your company accordingly and put yourself in the best position to reach your objective.

Just think of a military strategist. There's no way they can prevail in a battle if they don't know where they stand in relation to their opponent, particularly in terms of factors like natural geography, climate, weaponry and overall ability. As a salesperson, you also need to understand the variables and context for both you and your competitors.

That's why it's so important to analyze yourself and your market. Let's look at an example of how you might do this.

First, define the potential changes that could occur in your industry, market or environment. Make a list of everything that influences your day-to-day business, like the stock price of your buyers or the suppliers of your raw materials.

Next, determine which changes are threats and which present opportunities. Which would be positive and which would be negative? If your customers are starting to prefer one of your competitors, that would obviously be a threat; in contrast, new technology that reduces your costs would be an opportunity.

Finally, define your _single sales objective_ : the ultimate sale you want to make with the customer or customers in question. Be precise about what, when and how much you want to sell. So, don't say something like, "Get _x_ chain to buy TVs." Instead, say "Get _x_ chain to order a trial package of 100 4G TVs by August 1st."

### 4. Have a clear idea about which people have important roles that influence your sales process. 

Sales used to be about finding and making deals with the right people in another organization. These days, however, it's much more complicated. Modern markets require you to conduct _complex sales_, where multiple people must be on board before you can close a deal.

You can find the people who will have a major influence on your sales process by paying attention to the _roles_ they have. There are four important roles to consider when you're trying to sell your product or service.

First, find the people with _economic buying influence_. They're the ones who have the final say regarding your sale by arranging the payment between the two parties. A financial director or product manager, for example, might have economic buying influence.

Next, look for _user buying influence_ : the people who use or administer your product or service. Users want to know what kind of impact the sale will have on their everyday work experience.

The third key role concerns _technical buyer influence_. Technical buyers know how to sort through your options regarding potential suppliers. Lawyers who oversee contracts and legal issues, or accountants who calculate costs relating to your sales, are examples of technical buyers.

Finally, find yourself a _coach_ who can guide you through the sale process. Your coach can come from your client's organization or your own — the important thing is that they already trust you and your product or service. Make sure the other people among your buying influences also consider your coach to be credible, to ensure that deals go well on all sides.

### 5. Transform any weakness in your strategy into strengths. 

Now you know which people you need help from in the selling process; but there are other things to look out for, like _red flags_.

Red flags are signals that tell you you're not in the right position. They indicate threats that might jeopardize your sales efforts.

There are a few common red flags you should always keep an eye out for. For instance, if you feel like you've got all the information you need to make a sale, look it over again — salespeople often make mistakes because they're overconfident about the information they have.

New people in the client company are also a red flag. Stay wary of any new people until you're sure they feel positively about the deal.

The author once knew an insurance salesman whose account brought in an external consultant just when they were about to negotiate a deal. Fortunately, the salesman realized that the consultant was a red flag and contacted her right away. He developed a relationship with her and persuaded her that the deal was a good move, transforming her from a red flag into an ally.

When you catch red flags like this salesman did, you can turn them into advantages. So, aim to use your strengths to cover any weaknesses.

Say you're working on a new deal. If your user buyer is interested in the sale, but your economic buyer isn't, that's a red flag. You could perhaps try to get the user buyer to serve as a coach for the sale. This way, you'd use the strength of the user buyer's enthusiasm against the weakness of the economic buyer's reluctance.

### 6. Identify the state of your buying influences so you’ll know how they feel about the potential sale. 

We've seen that it's important to identify potential changes in your environment, whether they're threats or opportunities. However, sometimes changes you perceive as opportunities can be threats to your buyer influence.

You can avoid this by making sure you always understand how your buyer influences feel about potential opportunities; this is the only way to accurately predict how they'll respond to your proposal.

Remember, buyers only engage with a proposal when they want to change something about their current situation. So, if you alter your proposal at all, you have to make sure it still gives the buyers what they want. You have to move the buyer closer to their goals, whether their goals have to do with quality, quantity or another aspect of a product. Aim to diminish the discrepancy between what they have and what they want.

Your buying influences will go into different _response modes_ to your proposal depending on that discrepancy. Let's look at two of them:

One possible response mode is _growth_. In growth, the buying influences want to eliminate the discrepancy as soon as possible. They're on the lookout for options and are expecting change. If a buying influence uses words like "better", "faster" and "improved" a lot, it's a sign they might be in growth mode.

_Trouble_ is another possible response mode. A buyer in trouble mode is also looking for an immediate change, but they're doing so in hopes of escaping a bad situation. In contrast, growth mode buyers are looking make a good situation even better.

If your buyer is in trouble mode, make a proposal that can quickly fix their problems. You can't adjust a proposal to your buyer until you know exactly where they are and what they want.

> "People buy when, and only when, they perceive a discrepancy between reality and their desired results."

### 7. Show your buying influences that you care about their interests just as much as your own. 

Good salesmen never try to trick buyers into agreeing to just one sale. Instead, they aim to foster long-term relationships, satisfy their customers, get good referrals and close deals repeatedly. There are a few ways you can go about this.

First off, don't make any sales that treat one side as the winner and one side as the loser. If a deal isn't a win-win situation for both you and your buyer, it'll end up as a lose-lose.

If you profit from a sale and your buyer doesn't, for example, there's a good chance they won't do business with you again. Make sure your buyer feels like they're getting something important out of your product or service — this leads to a _win-result_.

A win-result occurs when a buyer feels like he has gotten a great deal, and also gets good results from his purchase. Imagine your manager wants to reduce costs because his department has exceeded its budget. If your product would help the department stay within its budget in the future, that would give your manager an excellent overall result and also make him feel like he has personally solved the problem.

However, you have to identify these potential wins before you can work towards one and achieve it. One good way to do this is to carefully assess any impressions you get about your buyers.

If your buyer influence covers the walls of their office in awards or plaques, for instance, they probably place great importance on recognition and achievement. If they cover their desks with pictures of their family, they might value security and stability.

If you're not sure, you can always ask. You might not get far with a blunt question like "What would be a win for you in this sale?" However, an _attitudinal question_ like "What do you think about this system?" will give you a good sense of how they feel about the situation.

### 8. Final summary 

The key message in this book:

**Don't try to trick your buyer — you're both on the same team. Listen carefully to what they want, figure out what they value and craft a proposal that satisfies their needs while giving them a feeling of satisfaction. Strive to make long-term, positive relationships that will lead to references and referrals. When you and your buyers both benefit from a sale, everyone wins.**

Actionable advice:

**Focus on your customer, not your product or competition.**

Salespeople often make the mistake of paying too much attention to their competition, which can give off a bad impression and make you stray from your sales goals. Remember to always focus on your customer and what he or she needs.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _SPIN Selling_** **by Neil Rackham**

_SPIN Selling_ (1988) distills the author's 12 years of research and 35,000 sales calls into a coherent and applicable sales strategy that is guaranteed to bring success to any diligent salesperson. You'll learn why traditional sales methods are limited while exploring the benefits of the SPIN strategy when approaching small and large sales opportunities.
---

### Robert B. Miller, Stephen E. Heiman, Tad Tuleja

Robert B. Miller has consulted and made sales with many Fortune 500 companies including Ford and General Motors. He created the sales systems used at Miller Heiman, a prestigious organization that offers sales programs and systems to professionals. He's also the co-author of best-selling books such as _Conceptual Selling_ and _The 5 Paths to Persuasion_.

