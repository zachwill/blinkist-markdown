---
id: 5587e74b3966300007000000
slug: the-new-trading-for-a-living-en
published_date: 2015-06-22T00:00:00.000+00:00
author: Dr. Alexander Elder
title: The New Trading for a Living
subtitle: Psychology, Discipline, Trading Tools and Systems, Risk Control, Trade Management
main_color: 7E2619
text_color: 7E2619
---

# The New Trading for a Living

_Psychology, Discipline, Trading Tools and Systems, Risk Control, Trade Management_

**Dr. Alexander Elder**

_The New Trading for a Living_ (2014) is your complete guide to getting started in trading. These blinks provide a detailed overview of a range of trading methods that will allow you to approach the market with minimum risk.

---
### 1. What’s in it for me? Learn how to begin your life as a trader by earning a lot and risking a little. 

Do you sometimes catch a glimpse of Bloomberg or the financial update on the news and think, "Wow, look at all those people making money. Maybe I should get in on that!" Well, why not? The financial markets are open to all, and there is a lot of money out there, waiting to be made. Just look at the vast wealth of George Soros or Warren Buffet.

However, jumping into financial trading without knowing what you are doing is not wise. If you get things wrong, you could end up losing a lot. Therefore, before starting your new life as a trader, read these blinks. Based on the thinking of a market expert, they map out the basic rules every beginning trader should know.

In these blinks, you'll discover

  * that every good trader is unemotional and calculating;

  * why making money on the market begins with learning how to read a graph; and

  * why you've got to study up on bulls and bears.

### 2. A new trader can fall into many traps and end up paying more than she’d like. 

Have you ever looked at famed stock traders like Warren Buffet and wondered why you can't live like them? Well, you can! It's challenging to get there, but if you're cognizant of some early pitfalls, it'll be much easier.

As you take the plunge into trading, the first thing to be aware of is the danger of commissions. You pay your broker or bank a commission every time you do a trade — and if you don't watch out, these commissions will eat up your trading money!

Say you're an active trader, doing two trades a day, four days per week, paying $10 per trade in commissions. By the end of each week those commissions amount to $80. Working 50 weeks a year, you'll have paid $4,000 in commissions by year's end! If you're trading with $20,000 per year, that's 20 percent of your budget spent on commissions alone.

To minimize commission costs, make sure you do your research on various brokers and banks. Compare their services and commissions carefully to avoid paying more than necessary.

Another common pitfall is _slippage_. _Slippage_ means that your order could have been filled for less money than you paid. If you want to avoid this, you'll need to make your orders the right way.

There are two order types: _limit_ _orders_ and _market_ _orders_. Using a market order is rather like saying, "give me a stock." This guarantees you a stock, but you don't know at which price. If the price has increased from $50 to $53, you are paying $3 more than you intended.

Using a limit order is like saying "give me that stock for $50." You won't pay more than $50, but if nobody is selling at this price, you might not get a stock. Limit orders are definitely the way to go, since they prevent you from overpaying for stock.

Paying too much, whether to brokers or for stocks, is one pitfall the new trader faces, but it's not the only one.

### 3. A good trader doesn’t gamble. 

Most people think trading is like gambling. Why? Maybe because both seem like very risky ways of making money. But that's not quite the reality. Though poor trading may resemble gambling, good trading is nothing like it.

If you trade like you gamble, chances are your capital will disappear rather quickly, as gamblers don't carefully control how much money they put down. But how do you know when you're trading like a gambler?

The inability to resist the urge to bet is a sure sign that you're an inveterate gambler. If you ever have the feeling that you _need_ to trade or that you just can't stop, you are gambling. Try taking a one-month break to get those risk-taking urges under control.

Another sign is when single trades begin affecting you emotionally. If the stocks move in your favor, you feel happy and powerful, but if the trade moves against you, you feel crappy. Emotional trading causes you to gamble away your money in search of positive feelings.

A professional trader doesn't get emotional, because she knows that trading is merely a way to make money. She doesn't feel a personal connection to any given stock.

Self-sabotage is also a common pitfall for traders, be they a beginner or a professional. In fact, one of the author's friends did just this. As a pharmacist, broker and trader, he was unsuccessful, but prone to careless decisions.

At the height of his trading career, he went on a trip to Asia, leaving open a vast position with no security at all. Naturally, when he returned, that position had decreased and all his capital was wiped out.

How can you prevent a devastating mistake like this? It's simple: Take responsibility for your actions and their consequences. Good trading also means taking responsibility for your own decision-making. Learn more about this in the next blink!

### 4. When it comes to the market, try not to follow the crowd. 

All we ever discuss in trading is the _market_, but what does this term actually mean? Amateurs think it's an entity in its own right; people with a scientific background often think it's a scientific body; but professional traders know that it is merely a mass of people following trends.

To become a successful trader, you must _avoid the market crowd_. But remaining an independent thinker isn't always easy. In fact, human instinct impels us to seek out the safety of the crowd. A stone-age hunter, for instance, was more likely to survive if he joined a crowd of hunters, and even if there aren't literally any saber-toothed tigers on Wall Street, those primitive impulses still affect your everyday trading.

Such impulsive decision-making is quite dangerous, however, and can suck people into disastrous trades.

One historic example is the _Tulip Mania_, which took place in Holland, in 1634. The prices of tulips were increasing and everyone thought that this pattern would continue. Many people abandoned their businesses to go into the tulip business. Ultimately, however, it all collapsed, leaving people broke and destitute.

So how do you stay away from the crowd? Start by identifying the most important types of group behavior in the market. Let's consider the market in terms of two groups: _bulls_ and _bears_. Bulls bet on prices going up; bears, on prices going down.

When prices are rising, it's because bulls are on top, optimistic that things are going well. When prices are going down, it's because those pessimistic bears are ascendant.

To work out whether you're in the midst of a bull or a bear market, you'll need some tools to analyze crowd behavior more closely. _Chart analysis_ is one incredibly effective way to do this. Read on to learn more.

### 5. Learn the basics of bar charts for decisive analysis. 

Have you ever seen a chart of a stock, or an index like the S&P 500, and yet remained unsure of its meaning? With classical chart analysis, you'll be able to identify price patterns _and_ benefit from them. But first, you'll need to understand how a bar chart is constructed.

It all comes down to the following five elements: opening prices, closing prices, the highs of a bar, the lows of a bar and the distances between the highs and lows.

The _opening prices_ tend to show the amateurs' opinion of the value, since they are often acting in the morning before going to work. The _closing prices_ reflect the decisions of professional traders.

If closing prices are higher than the opening prices, this means that professionals are probably more bullish than amateurs, and vice versa. That's exactly the kind of knowledge you'll need in order to discern whether you're in a market of bulls or of bears.

The _high of each bar_ reflects the maximum power of bulls, while _the low of each bar_ displays the maximum power of bears. This is something to keep in mind when deciding at what point in time to buy and sell your shares (which we'll learn more about in the next blink).

Finally, the _distances between the highs and lows_ reflect the intensity of the conflict between bulls and bears. If you want to estimate activity in the market, it's this distance you'll need to pay attention to.

An average-sized distance represents a cool market; a distance half the average size indicates a sleepy market; and a bar double the average size marks an overheating market. Slippage is normally lower in quiet markets, so avoid getting into the market when there is a huge discrepancy between prices, a clear indicator that the market is overheating.

> _"Getting into a boiling market is like jumping on a moving train: not a good idea."_

### 6. An understanding of support and resistance is key to effective chart analysis. 

You've learned how to read a bar chart and what it tells you about the market. However, there are two more elements that a bar chart reveals, elements that will help you grasp the _mood_ of a market.

To find the first of these, we'll need to examine the price level. When buying is so strong that it reverses or interrupts a downward price trend, we're witnessing _support._ You can imagine _support_ as the floor that you bounce a basketball on. Every time the ball hits the floor, it bounces back up.

You can find a _support_ level in a chart by connecting two or more lows on the chart with a horizontal line. A support level exists, because people in the market have memories. If the price of a stock was falling and stopped at a certain level and then increased, traders would remember this low level and be likely to buy when prices approach it again.

When selling is so strong that it reverses or interrupts an uptrend, the market is demonstrating _resistance_. Imagine a ball being tossed up, hitting the ceiling and dropping down. In this example the ceiling would be the _resistance_ level. Using chart analysis you can find _resistance_ levels by connecting two or more highs in a chart with a horizontal line.

From 1966 to 1982, there was a major resistance level for the Dow Jones Industrial Average (a major US Index). Every time there was an uptrend into the area between 950 and 1050, the uptrend was interrupted and reversed. Traders called this resistance zone "a graveyard in the sky" because it was so strong.

Essentially, you should begin selling when you hit the resistance level — before prices go down — and buy at the support level — when prices are at their lowest. In fact, many traders usually buy at support and sell at resistance zones, which also helps entrench these levels even further.

### 7. Keep an eye on liquidity and volatility, no matter what vehicle you trade. 

There are so many things you can trade: stocks, options, ETFs and futures, to name just a few. Each group has its benefits and drawbacks. _Stocks_ are what we most commonly trade. They're great for beginners, as they're easy to understand.

Yet this doesn't mean that you can be careless when trading stocks. In fact, it's crucial that you stay meticulous when deciding how many you try to trade. The author monitors only a single digit amount of stocks during the week, but some of his friends watch several dozen at a time. How much take on is up to you — just be sure you can manage it effectively.

No matter what you choose to trade, there are always two criteria they should meet: _liquidity_ and _volatility_. Liquidity is the average daily volume of traded shares. The higher the liquidity, the easier it'll be to trade.

The author himself learned how important liquidity is when he got stuck with 6,000 shares of a stock of which only 9,000 shares were traded per day. To get rid of them, he had to do several trades, paying commissions and _slippage_. To ensure you never find yourself in this position, concentrate only on stocks that are traded at a rate above a million per day.

Volatility is average short-term movement in the price of the stock. The higher the volatility, the more opportunities there is to make money — and lose it, too! We can measure volatility with a _beta_, which compares the volatility of a certain stock to its _benchmark,_ a general figure, such as a market index, that reflects the health of the market.

For example, if you have a beta with a value of two, that means that if the benchmark rises five percent, stock is likely to rise ten percent, and vice versa when the benchmark falls. It's advisable for beginners to focus on low betas, as this limits the amount you could lose when trading.

What you want to trade depends on your preferences and skills, but always keep volatility and liquidity in mind.

### 8. Use two simple rules to minimize your risk. 

No matter how great your profits are in trading, without proper risk management, all your capital could disappear in minutes. If you want to be secure, you'll need to follow two key rules.

The first of these is the _2% rule_. This prohibits you from risking more than 2 percent of your trading equity on a single trade. In order to implement this rule, you'll need to take a few factors into account. We can illustrate this with an example.

Say you have $50,000 of trading capital in your account. The 2% rule dictates that you can only risk $1,000 (=50,000 X 0.02) per trade.

Now, suppose you want to buy a stock worth $50, and to limit your risk of losses, you set a so-called _stop order_ at $48.

A _stop order_ means that when the price of the stock falls to a given amount, the stock is automatically sold. In our example, you risk $2 per share. If you are allowed to risk $1,000, you can buy a maximum of 500 shares. The 2% rule is one of the most effective ways of minimizing your losses.

The second rule you need to implement is the _6% rule_. This rule dictates that you can't open any new trades for the rest of the month if your total losses in the month plus the risk in open trades reaches 6 percent of your trading capital. Let's take a look at the 6% rule in action.

Start by calculating this month's total losses, then add your current risks in open trades. Using our earlier example, this risk is $1,000, or 2 percent (500 shares with a risk potential of $2 per share). Finally, add these two numbers together. If the sum equals 6 percent of your capital, you'd best abstain from opening new trades before the month is up!

### 9. Keep your trading on track with a trade journal. 

Know the saying "you can only improve what you can measure"? The same goes for trading, except it's not just mere measuring that you should be doing. The cornerstone of successful trading is _record keeping_, because it helps you develop and maintain discipline.

In trading, the importance of disciplined record keeping is comparable to weight control. If you don't know your weight and don't know if you are gaining or losing it, how can you get the results you want? The same is true for trading. If you don't keep an eye on how much you are gaining or losing, how can you ensure you improve your strategies in the future?

Maintaining a trade journal is one of the most effective ways to keep records. Reviewing your trades in your journal one or two months after you've sold will help you prepare for future sales. Oftentimes, trading signals that seemed vague and confusing at first become clear when you review them a month or two down the line.

Journaling can also prevent emotional trading, as you'll see for yourself how much feel-good trading costs you. And what's more, you're bound to find a pre-existing template online that suits you!

One of the main things you should be looking for in your journal entries is your _personal equity curve_. This will help you determine whether you're making money or losing money in the long-term, and reveal whether your trading system is up to scratch. If your equity curve shows a downtrend, it may be a call for you to pay closer attention to your system, or your discipline, to see what needs tightening up.

> _"Show me a trader with good records, and I'll show you a good trader."_

### 10. Final summary 

The key message in this book:

**Success on the market is determined by knowledge, focus and discipline. With a firm understanding of the traps of individual and crowd psychology, and confidence in classical chart analysis, you're well on your way to earning your living as a trader.**

Actionable advice:

**Test the waters before you take the plunge!**

If you're interested in trading, why not open a virtual portfolio as a test? If you think finance is something you're interested in, you can prepare yourself by broadening your knowledge of it. This background reading will give you a head start as you embark on your career in trading, and may well prove helpful in other areas of your financial life, too.

**Suggested further reading:** ** _The Education of a Value Investor_** **by Guy Spier**

In his bestselling book, Guy Spier recounts his transformation from greedy hedge-fund manager on Wall Street to a successful _value investor_. Sharing the incredible story of his career and the wisdom he acquired along the way, Spier has some surprising insights concerning, what he sees as a false choice between leading an ethical life and a financially successful one. With great admiration, Spier also names the people who were most influential to his professional life, explaining the specific effect each of them had on his mindset and career.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dr. Alexander Elder

Alexander Elder, M.D., was born in Russia and grew up in Estonia. He entered medical school at age 16 and, at 23, was granted political asylum in the United States. He worked as a psychiatrist in New York City, an experience that gave him unique insights into the psychology of trading. Today, he is a professional trader and trading teacher. His other trading books include _Come Into My Trading Room_ and _Sell & Sell Short_.

[Alexander Elder: The New Trading for a Living] copyright [2014], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

