---
id: 528242003334640008600000
slug: the-no-asshole-rule-en
published_date: 2012-10-16T09:07:09.993+00:00
author: Robert I. Sutton
title: The No Asshole Rule
subtitle: Building a Civilised Workplace and Surviving One That Isn't
main_color: A22023
text_color: AB3739
---

# The No Asshole Rule

_Building a Civilised Workplace and Surviving One That Isn't_

**Robert I. Sutton**

_The No Asshole Rule_ delves into the problem of bullying or aggressive co-workers, who in many cases rise to management positions. Sutton provocatively labels them _assholes_.

The book lays out the effect these employees can have on a business, and gives advice on how to develop an asshole-free environment.

---
### 1. Assholes is the right term for all those people who regularly bully or put down others. 

We all have bad days and act like assholes every now and then; we are all _temporary assholes_ occasionally.

_Certified assholes_, on the other hand, are people whose asshole-behavior is not a temporary outburst on a bad day — rather, it is part of their character. These are people whose bad behavior is continuous and long term.

This hostile behavior can be expressed both physically and mentally. It may be verbal or non-verbal. Assholes leave their victims angry, afraid, and humiliated.

In the workplace, this can be expressed in many ways: Assholes interrupt others while they speak, they violate their personal space, insult and intimidate them, put others down, stare at them aggressively or ignore them altogether.

Assholes often get away with this behavior by treating their own bosses or customers respectfully, whilst treating everyone else poorly. Hiding behavior like this can be very effective if lower-level employees feel the asshole's good reputation with superiors would lead to their complaints being ignored.

The general rule is:

**Assholes is the right term for all those people who regularly bully or put down others.**

### 2. Assholes are bad for business – especially when they hold management positions. 

Lots of workplaces tolerate poor behavior without realizing the damage it causes.

In offices where assholes go unchallenged, employee morale is undeniably lower than in comparable workplaces which maintain a friendly, respectful and professional atmosphere.

This has a huge effect on everybody's productivity.

Employees with low morale are more likely to resign, take more sick leave and are generally less productive. They may get their own back by skipping work, producing poor results, or even stealing.

Assholes sap their colleagues' energy — that goes for onlookers working in a hostile environment as well as direct victims.

Assholes in positions of power are particularly damaging. The employees under their management work with the constant threat of humiliation and expend their energy on avoiding it rather than focusing on good work. This quickly leads to a fearful, unproductive atmosphere.

Tolerating assholes is therefore a costly mistake for a business. The quality of work goes down as the best employees, who have more options available, leave for asshole-free workplaces.

**Assholes are bad for business — especially when they hold management positions.**

### 3. To create a great working-environment, businesses should adopt the No Asshole Rule. 

All too often assholes are tolerated in the workplace. Their temper tantrums and spitefulness are put down to character flaws and excused because they are talented, smart, or difficult to replace.

But in doing so, businesses are effectively hurting themselves; all assholes should be weeded out from the workforce from the very beginning.

To put it another way, it makes sense to regard an employee who cannot get along with their colleagues and makes them feel uncomfortable or upset as incompetent, regardless of their other qualities or abilities. This _No Asshole Rule_ should be applied to all, whatever their qualifications.

The founders and managers of a business should always make it clear that all their employees deserve to be treated with respect. This pays its own reward in loyalty and high employee morale.

Life is too short to put up with assholes.

To put this rule into effect, it needs to be well known by all employees. Google's slogan "Don't be evil" is a good example of this.

The rule should also be extended to customers and clients, as their behavior can have just as much of an effect on job satisfaction and morale as an employee's. Some airlines, for example, blacklist customers who have mistreated their staff by e.g. screaming at them or threatening them.

**To create a great working-environment, businesses should adopt the No Asshole Rule.**

### 4. More equality equals fewer assholes. 

Studies have repeatedly shown that people given higher status are more likely to behave like assholes. They talk more than others, take what they want without considering the people around them; in general they tend to see others just as a means to an end and take personal credit for a group success.

One such study had groups of three students discuss various issues. One student per group had been randomly chosen to rate the others' arguments. The results were that these _more powerful_ students tended to break social conventions more frequently. This was measured by providing a shared plate of cookies; the higher-status students were more likely to take the last cookie, chew with their mouth open and leave more crumbs behind.

The same effect is very noticeable in the business world. The greater the difference in status between managers and employees, the more lower-ranking workers are treated disrespectfully.

In order to reduce asshole behavior, a business should seek to reduce the social distance between employees. This leads to more respectful behavior, especially between managers and subordinates.

One way of doing this is to keep wage differences as narrow as possible. Earnings are the most important sign of status in the business world — by keeping wage gaps as unspectacular as possible, a company can reduce the differences in status that lead to workplace bullying.

**More equality equals fewer assholes.**

### 5. Being an asshole can be an advantage, but motivating employees through incentives is more effective. 

Every office worker knows that groups of managers can be pretty similar to groups of baboons. There are fierce rivalries between competing managers, and the most aggressive one often wins.

The interesting thing is, even though individuals acting aggressive and reckless are often rewarded as cold and unpleasant, but also as competent and resourceful.

Being cold and ruthless can be just as much of an advantage in the business world as in the Mafia or a tribe of chimpanzees.

This can be explained by evolution; throughout the rise of man, the most aggressive, loudest member of the group became leader. Hence, our brains have evolved to associate aggression with high status.

However, this doesn't mean that it's a good idea for companies to promote assholes to positions of power or to tolerate aggression in the first place.

Techniques that work well at outcompeting rivals do not translate well into good management techniques; rather than motivating subordinates, they often have the exact opposite effect.

Two things best motivate people: incentives and recognition. Not only do these positive techniques get better results than aggression and punishment — a tyrant's power only lasts as long as they're around to enforce it — but they keep office morale high.

The best employees, who can more easily find work elsewhere, are less likely to stay in a workplace given a negative atmosphere by tolerance of assholes; the people that stay behind are those that have few other options. Assholes drive away the cream of the workforce, whilst implementing the No Asshole Rule will help to attract and keep talented workers.

**Being an asshole can be an advantage, but motivating employees through incentives is more effective.**

### 6. To create a civilized, productive workplace, focus on cooperation, rather than internal competition. 

The business world is a cut-throat place; competition between firms is fierce and unending. This attitude is all too often carried over into companies' internal cultures.

Naturally, healthy competition between individuals can be good for a company; ambition is a great way to motivate employees to greater efforts, to take risks, and to come up with new ideas. It also helps to select the best candidates for promotion.

However, in the scrabble for advancement, rivalries can turn sour and the best interests of the company are easily forgotten. Too much internal competition can effectively hamstring a company and make it an uncivilized, unproductive workplace.

This is the reason that the most successful companies are often those in which internal competition is restrained and a culture of co-operation fostered. Not only does this produce a more civilized atmosphere, but also better results.

To achieve this, it's important firstly to note and reward co-operation.

Subtle approaches such as choice of vocabulary can have a surprising effect on a company's culture. Replacing aggressive, often warlike words and phrases ('the enemy', 'battleground' etc.) with more positive alternatives, stressing co-operation ('help', 'fairness', 'community') can be particularly effective.

Even such simple changes such as referring to 'we', 'our', and 'us' rather than 'I', 'my', and 'them' can subconsciously remind employees that they're all on the same team and direct focus towards co-operation rather than the differences and rivalries that sap time and energy.

**To create a civilized, productive workplace, focus on cooperation, rather than internal competition.**

### 7. Avoid assholes or you’ll become one yourself. 

Being an asshole is highly contagious.

Just as the proverb says "Lie down with dogs and you'll rise with fleas," simply being around angry and aggressive people noticeably alters your mood and behavior. If you find yourself working in a rude and disrespectful culture, you soon get used to rudeness and lack of respect, and gradually come to display both towards others. This behavior also has a way of creeping into private life and so has consequences far beyond the business world.

Sutton himself notes how he started treating his wife worse after spending too much time amongst assholes.

So to avoid becoming a certified asshole in both your business and private life, avoid assholes as far as possible.

Of course, you can't always choose your co-workers, so if avoiding assholes isn't an option, try to keep contact to a minimum to keep your exposure as low as possible. It helps to think of being an asshole as a virus — you'd avoid close contact with a colleague who has the flu, so treat assholes the same way.

Of course, it's always easier to do something from the start than it is to change course later on. If you've already invested a lot of time and energy into a relationship with an asshole, it's going to be more difficult to distance yourself from them. Hence, whenever you face an asshole, get off as quickly as possible!

**Avoid assholes or you'll become one yourself.**

### 8. Don’t let assholes get to you – build emotional distance. 

In many workplaces it's impossible to completely avoid contact with assholes. Working with, or worse, under an asshole can be very demoralizing, even if you don't realize at first how much stress it actually causes, so it's very important to have a good strategy to allow you to get through the day unscathed.

Generally, the most important thing is not to let an asshole drag you down to his level. It's difficult, but try to remain calm, don't respond to aggression with more aggression, and keep as much distance as possible between yourself and any hurtful things being said. It may help to remind yourself that the person you're dealing with is _just an asshole_, no matter how senior they may be.

To build emotional distance, treat interactions with known assholes with a positive frame of mind. Remember:

  * The problem is only temporary.

  * You're not the cause of the problem.

  * It's not going to ruin the rest of your life.

If somebody's an asshole, that's their problem, not yours.

Anticipating an uncomfortable situation with an asshole, it's helpful to always hope for the best whilst preparing for the worst. Happiness (and sadness) is found in the difference between what you expect and what you experience. So when dealing with an asshole, be prepared for the worst, but remember that it's due to the other person and not you.

**Don't let assholes get to you — build emotional distance.**

### 9. Everyone should use the No Asshole Rule all the time. 

Everyone knows that it only takes one asshole to ruin a conversation, whether in the office, at a party or just in everyday life. Even if everyone else is behaving normally, one asshole can spoil the mood.

Negative things affect our mood five times more than positives, meaning that meeting five nice people who compliment you and bring you good news might easily be neutralized by encountering one single asshole.

So, everyone should know about and apply the No Asshole Rule, in both their professional and private lives. This means, zero tolerance for assholes.

That said, it's also important to avoid becoming an asshole yourself, or if you already are, to try to change negative behavior. The first step is to take an honest look in the mirror and ask yourself when was the last time you acted like an asshole.

In fact the way to avoid being an asshole is simple; be friendly and respectful to other people at all times, and expect the same treatment in return.

Because life's too short to put up with assholes.

**Everyone should use the No Asshole Rule all the time.**

### 10. Final summary 

The key message in this book is:

**Companies should have a zero tolerance policy towards assholes and bad behavior. This raises employee morale, productivity and company loyalty.**

The book answers the following questions:

**How do you spot an asshole, and what harm do they cause?**

  * Assholes is the right term for all those people who regularly bully or put down others.

  * Assholes are bad for business — especially when they hold management positions.

**How should companies deal with assholes?**

  * To create a great working-environment, businesses should adopt the No Asshole Rule.

  * More equality equals fewer assholes.

  * Being an asshole can be an advantage, but motivating employees through incentives is more effective.

  * To create a civilized, productive workplace, focus on cooperation, rather than internal competition.

**How can individuals deal with assholes?**

  * Avoid assholes or you'll become one yourself.

  * Don't let assholes get to you — build emotional distance.

  * Everyone should use the No Asshole Rule all the time.
---

### Robert I. Sutton

_Robert I. Sutton_ (*1954) is a Professor at Stanford Business School. He has advised numerous international companies and published several popular scientific books.

The book draws on his extensive experiences working for some of the world's biggest companies and best-known CEOs such as Steve Jobs.

