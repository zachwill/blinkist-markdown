---
id: 537caff83465300007fe0000
slug: the-now-habit-en
published_date: 2014-05-20T14:01:33.000+00:00
author: Neil Fiore
title: The Now Habit
subtitle: A Strategic Program for Overcoming Procrastination and Enjoying Guilt-Free Play
main_color: 2889C8
text_color: 2E6F99
---

# The Now Habit

_A Strategic Program for Overcoming Procrastination and Enjoying Guilt-Free Play_

**Neil Fiore**

_The_ _Now_ _Habit_ investigates a problem many people can relate to today: procrastination. Why do people put off important tasks until the last possible second? And how can they be helped? _The_ _Now_ _Habit_ explains where this phenomenon comes from, and which mindset and tools can help us overcome it.

---
### 1. What’s in it for me? You’ve been taught to procrastinate, now you can unlearn it. 

Procrastination means spending your entire work day surfing Facebook and news sites instead of writing your report that's already overdue; it means cleaning your apartment for the third time this week instead of proofreading your thesis; it means calling all your friends just to say "hello," instead of finally filing your taxes.

Outside observers might find your procrastination amusing. But for chronic procrastinators, it's no laughing matter.

They feel their lives are out of their control. Every time they sit down at their desk and resolutely declare, "this time, I'll really write that report," they end up wasting yet another day on Facebook.

When they fail to meet their goals, they have feelings of guilt and shame; and since they never get anything done on time, they suffer from chronic stress and aren't ever really able to relax.

Neil Fiore is convinced that procrastination isn't innate, that is, we aren't born lazy. 

All we have to do is look to children to see that this is true — they never procrastinate! 

In fact, we _learn_ and adopt this unhealthy habit; it's taught in our schools, by our parents, and at the workplace.

Thankfully, since procrastination is something that's learned, it's also something we can _unlearn_. All we need is a shift of mind-set and a few simple tools.

These blinks will show you: 

  * how to best understand procrastination,

  * how to get two horses to work together as a team,

  * why setting lofty goals can actually wreck your motivation,

  * how the underlying mind-set that causes procrastination can be changed and

  * which simple tools you can use to overcome it.

Let's start by looking at the source of procrastination. Why do we do it?

### 2. Procrastination is a strategy to avoid fear of failure. 

Procrastination is a problem most of us are familiar with. In spite of its prevalence, procrastination is typically tied to very specific situations.

Usually, we procrastinate on _work_, that is, when there is a certain task we're required to perform like writing a report, organizing a seminar or making a presentation in front of a team.

These tasks are all significant and not part of your routine. You don't procrastinate going to the bathroom or answering a colleague who wants to take you to lunch, but you might put off starting an important presentation.

In fact, the types of tasks we procrastinate on usually have three important characteristics:

First, when you want to do a good job on something so you can live up to others' and your own expectations. 

Second, you find the work dull. It's no fun and getting started, for example, writing the first page or filling up PowerPoint slides takes motivation.

Finally, it's unclear what qualifies as a "good job": you simply don't know how to live up to others' expectations and deliver a great presentation or write an outstanding report. What is "good?" What is "good enough?" And what if you pour your heart and soul into a project that completely fails?

When faced with these kinds of tasks, the inevitable consequence is a choice between two options:

If you start working on the task, you spend your time on something boring, plus you risk failing and disappointing both yourself and others as well.

If you don't start working, you can avoid this boredom, uncertainty and the fear of failure.

So what do you choose? Most likely, you'll choose the second strategy and delay the unpleasantness associated with your task. And, in a certain sense, it _works_ : you learn that procrastination helps you avoid boredom and fear of failure — at least temporarily.

### 3. We’re taught to dislike work and fear failure. 

Both teachers and parents alike believe they know why their children procrastinate: obviously, it's because of their innate laziness! 

And a child needs discipline, reward and punishment to overcome this laziness. Children that aren't working hard enough simply need to be pushed harder! Right?

No, this understanding of procrastination is just as simplistic as it is wrong. In fact, there are many examples that demonstrate that humans aren't born either lazy or lacking in drive.

Consider all the things that you actually enjoy doing, like playing soccer, reading a book or meeting your best friend for a coffee. How much motivation do you need to do those things?

In fact, everyone has certain things they can do without procrastinating, and without the threat of discipline or the lure of a reward.

Interestingly, before they're "educated," young children never procrastinate. And when they play, they never rate their performance afterwards or wonder what others think about them.

How come we learn to procrastinate on our way into adulthood? What's so different about trying to get _work_ done?

First of all, we learn at a young age in school that "work" is no fun: in fact, it's the opposite of "play." 

We hear our parents say things like: "Sorry, you don't get to spend the afternoon playing; sit down and do your homework!" And if you resist, then they threaten, "No TV tonight."

Second, we're instilled with unhealthy perfectionism. We feel very conscious of our performance, that we need to do _the best_ — because otherwise it implies that we "weren't trying hard enough."

This, in turn, leads to unrealistic expectations: when everyone thinks that anything less than the very best isn't good enough, virtually no one can live up to their expectations!

So it's natural that most people find work unpleasant: on the one hand, we expect it to be dull and boring; on the other, we've learned that anything less than "the best" is unacceptable, and it's therefore likely we'll fail.

### 4. Our self-esteem is linked to work, and procrastination helps protect it. 

Each and every human being wants to be valued and held in high regard by others. If we can avoid it, we usually don't surround ourselves with people who constantly make fun of us or put us down.

In fact, we employ many strategies to combat any threats to this desire to protect our own self-esteem .

In the Western world, this is complicated by the fact that an individual's self-esteem is usually strongly linked to their work and work accomplishments.

Early on we learn that we're only worth something if we work hard and achieve a notable career.

People value doctors, managers and professors, believing that the poor and unemployed haven't tried hard enough and are thus less valuable.

Since they've learned that work is the main source of self-esteem, it's quite natural that most people are driven by a rather unhealthy perfectionism.

For them to feel valued as a person, they have to work as hard as possible in order to live up to their own insanely high expectations.

Of course, it's impossible for everybody to become the most successful person in their field, so it only makes sense that many people would employ strategies to protect their self-esteem from failures.

If a task seems too difficult to accomplish, such as being the valedictorian or the richest member of the whole family, strategies such as procrastination allow us to re-interpret our situation.

We say things like: "We're not failing because we're not good enough; we just haven't really tried."

In this way, procrastination helps us avoid the stress and anxiety caused by our unrealistically high expectations and the uncertainty of our success.

### 5. Adopt the anti-procrastination mantra: “You only learn when you fail.” 

One of the key ingredients of procrastination is perfectionism: no matter what we do, we always tell ourselves that the results must be the absolute best they can be. Mistakes are to be avoided at all costs, and failure is both unacceptable and a threat to our self-worth.

In the face of this overwhelming pressure to perform well, we lock up, meaning we do nothing, instead of starting our work and facing this challenge; we procrastinate by browsing the internet, sorting our stamp collection or leaving to run some "urgent" errand.

However, many high-performing people don't do this; they don't fear mistakes or setbacks, and instead just get cracking. Whatever their task, they try their best without worrying about the consequences of failure. If they _do_ fail, they quickly jump back on their feet and give it another go.

And as long as we're not dismantling an atomic bomb or swimming coast-to-coast through the Atlantic ocean, this strategy makes sense because the stakes aren't actually that high.

In fact, if you never _try_, then you never _get better_. Only people who try and try again are constantly improving their skills!

What do chronic procrastinators do instead? They freeze in the face of fear and avoid work, assuming that the only way is the perfect way, that is, getting it right the first time. Anything else means total failure and, consequently, a great depreciation of self-worth.

What they forget is that most great works, from Picasso's paintings to Thomas Edison's inventions, started with a set of failures and many half-baked attempts. 

Simply put: failure is an inevitable part of learning, so he who never fails, never learns!

### 6. To become a producer, change your self-talk from “I must” or “I should” to “When can I start?” 

The typical procrastinator experiences a specific inner monologue, which is dominated by an inner conflict. Usually, this self-talk uses phrases such as "I must," "I have to" or "I should," which all implicitly mean "I don't want to."

This permanent conflict is like two horses pulling in opposite directions: one horse pulls you towards the task that "should" or "has to" be done; the other away from it because you "don't want to."

Of course, this strategy will lead you nowhere fast. Not only that — it also causes immense mental stress and a negative attitude towards work.

However, it _is_ possible to overcome this conflict and change your inner monologue, transforming your role as a powerless victim into that of a _producer_ — someone who doesn't just _have_ to get things done but actually _wants_ to and produces results.

Unlike procrastinators, producers have a clear goal in front of them that they want to achieve. Thus, their inner voices say things like: "I want to," "I will" or "I decided to … When can I start?"

Positive self-talk makes you focus your energy in one direction: now both horses can pull you in the same direction rather than sabotaging your efforts because of some needless negativity.

Using this determination, producers can decide for themselves what they want to do — and when, where and how to do it.

After all, if you do something, it doesn't help to be ambivalent about it. Just do it. Use all your energy and focus to get it done, otherwise what's the point?

### 7. To be productive, integrate relaxation and play in your everyday life. 

Procrastination is a vicious cycle: once you start delaying your work, the guilty feeling of having to catch up on everything later accumulates into a larger and larger pile of guilt.

Thus procrastination leads to a life where you constantly feel like you have more work to do than you will ever manage to get done.

Such a lifestyle means you never really have time for true relaxation, real play or a private life you can fully enjoy. In order to manage the growing pile of work, all this has been put on hold until some point in the distant future.

Strangely, chronic procrastinators share the mind-set of a workaholic: both groups always feel that they have a ton of unfinished work waiting for them and that they don't have any time to rest and relax.

They are constantly pushing themselves, never giving themselves real breaks or well-earned rewards for their accomplishments.

As a consequence, they end up spending all their time either working or feeling guilty about the time they spend _not_ working.

Producers don't do this. Instead, they set aside significant amounts of their time for their private life, holidays, health, real relaxation and play.

High performers understand the true importance of rest, relaxation, play and fun, and therefore consciously use breaks and rewards as a means to recharge their batteries so that they can be productive again later.

In fact, guilt-free play and relaxation are absolutely _essential_ for anyone who wants to become a high-performing producer.

Moreover, those who know that a challenging task will be followed by play will have a much easier time motivating themselves to get started.

### 8. If you want to make a task less threatening, break it down into small, manageable units. 

Usually the hardest part about work is getting started.

Chronic procrastinators understand this: once they get started, they might actually have to write part of that report, so instead they spend hours opening the document, closing it, checking their Facebook page or cleaning the kitchen just to open the document again half an hour later…and then they repeat the cycle.

But why is getting started so hard? Usually because the task itself seems so large and insurmountable that we can't see a light at the end of the tunnel.

This is especially true for vague goals about the distant future, such as "earn a great degree" or "learn to play the piano." They don't offer us any rewards along the way, and are therefore totally unmotivating — you just work and work, hoping that you'll one day reach this huge goal.

In the face of these lofty, nebulous future goals, we tend to procrastinate and do things that offer quick rewards, such as checking email, surfing Facebook and so on.

So what's a simple strategy to overcome this tendency?

Easy: just chop your work into small, manageable tasks that can be checked off the list in no time at all.

If a task can be done in less than half an hour, it'll be easy to muster up the motivation to start, and once you've completed it, you'll be rewarded with a sense of accomplishment that gives you back a sense of control.

In addition, you can boost your motivation by following those small tasks with small rewards, such as short breaks where you relax or take a quick stroll.

So don't focus on finishing a big task; focus instead on getting started: Start with the first small chunk and forget about that vague goal in a distant future. For example, instead of counting the thousand pages left in _War and Peace_, get started by reading for half an hour.

### 9. For stress-free productivity, try “unscheduling.” 

One simple technique to make your work week both more pleasant and more productive is to employ the _Unschedule_ technique for time management.

The primary objective of this technique is to get as many short (around 30-minute) periods of uninterrupted, focused work into your day, instead of spending ten hours "working," half-focused and riddled with procrastination and distractions.

By structuring your day into many short, focused units, you not only get a whole lot more done but also enjoy a shorter workday.

Essentially, the technique is thus:

Select a task from a list of important things you want to get done; set a timer which ends each little "sprint" after 30 minutes; and spend all 30 minutes working with absolute focus.

After each sprint is finished, you "book" the unit in your _working hours account_.

At the same time, you compile a list of enjoyable things you want to include in your week, for example, meeting friends for lunch, taking a walk or going to the movies.

Unlike in a normal work planner, you schedule the pleasant things — not the work units — in your calendar. They build the framework that you will use to structure your work days.

Using this system makes your mind aware of two things:

First, your life is not just work! You get to enjoy many other pleasant, non-work-related things as well.

Second, the amount of time you have to get things done is limited, so you'd better make good use of it!

Since unscheduling means scheduling your work around your play, you only select tasks whenever you have time to work on them, and only book them in your working hours account if you've managed to get half an hour of focused work done first.

This way, you end up spending quite a lot of time on real, focused work, yet never feel like it's an obligation.

### 10. To fight off distractions, always have a piece of paper or a notebook within reach. 

There seems to be an infinite number of ways of getting distracted on the job:

Maybe you have a sudden stroke of genius and feel the intense urge to research or share it with a colleague. Or maybe you remember an urgent task and you're afraid you'll forget if you don't take care of it right away. Or maybe your favorite co-worker swings by to say "hello" or reminds you of a report you promised to send her by the end of the week.

The list goes on and on.

We tend to let these little interruptions steal our focus away from the task at hand, no matter how important what we're working on actually is. Instead of finishing our work, we let our minds slip.

But this is nothing more than a subtle form of procrastination!

These distractions, internal and external, are unavoidable: ideas and other thoughts will always pop into your mind, and chatty colleagues will inevitably come by to talk to you unless you lock the door or keep a pistol on your desk.

But there is another, simpler way of dealing with this problem — all you need is the right tools to capture the incoming distractions.

Just grab a piece of paper, a notebook or whatever it takes to create a little list, and collect every little thought and anything else that might otherwise distract you. Whatever is trying to steal your attention away from the task at hand, write it down as fast as possible on your list and get right back to work.

Afterwards, once you're done with what you wanted to do, look at your list and see if what you wrote down is still so urgent or such a great idea. You'll likely be surprised at how rarely that's the case!

### 11. Final summary 

The key message in this book:

**Nobody is born lazy. In fact, putting off work — procrastinating — mostly has to do with learning a negative attitude towards work as children. Since it's something learned, procrastination can also be unlearned by a changing our mind-set.**

Actionable advice:

**Schedule your work around your "play".**

Rather than trying to squeeze in life's most enjoyable moments wherever you can find space in your work schedule, try instead to structure your work week around the things you actually enjoy. This way, the time you spend working is limited and therefore _has_ to count. Plus, you have the added benefit of not having to sacrifice play for the sake of work.

**Suggested further reading: _The Pomodoro Technique_ by Francesco Cirillo**

_The Pomodoro Technique_ presents a simple yet effective method of structuring your work day. This method helps to overcome your lack of motivation by cutting large or complex tasks into small, manageable chunks. Using these techniques, you will gain more control over your work, thus making you a more effective worker and work more rewarding.
---

### Neil Fiore

Neil Fiore is an American psychologist and bestselling author of a number of books, including _The_ _Road_ _Back_ _to_ _Health_, with a primary focus on productivity and hypnosis. As a trainer and coach, he has helped thousands of clients and consulted many renowned companies.

