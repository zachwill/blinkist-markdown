---
id: 572ef324a35db30003e6bded
slug: the-nurture-assumption-en
published_date: 2016-05-12T00:00:00.000+00:00
author: Judith Rich Harris
title: The Nurture Assumption
subtitle: Why Children Turn Out the Way They Do
main_color: FD3381
text_color: BF366B
---

# The Nurture Assumption

_Why Children Turn Out the Way They Do_

**Judith Rich Harris**

_The Nurture Assumption_ (1998) addresses the long-standing debate about the role of nature and nurture. These blinks explain the different factors influencing a child's development and debunk the prevailing stance that children are a product of their upbringing.

---
### 1. What’s in it for me? Reconsider your role in your child’s development. 

As a parent, you can't help but feel proud about your bright, creative and considerate child. And, chances are, you're a loving and accepting mother or father, well-versed in parenting manuals and always set on doing the right thing.

But — unbelievable as it may seem — your parenting skills have nothing to do with your kid's personality! So how come we're all so convinced that parents shape their child's character? And if parents really have no say: Who _does_ determine the way kids turn out? Those are just a few of the questions these blinks will answer.

You'll also discover

  * the pros and cons of raising your child side by side with a chimpanzee;

  * what happened when two identical twins met after a life spent apart; and

  * the story of a boy who was reared as a girl.

### 2. The nurture assumption – that parenting plays the key role in a child’s development – is dead wrong. 

Ever wonder how you became the intelligent, charming specimen you are today? Maybe it was your genes or the way you were raised. In general, though, we lean more toward the latter, the _nurture assumption_. People tend to believe that parents play a major role in how their children turn out.

For decades, people have stood by the apparently self-evident fact that a child's personality is the product of two things: nature (her genes) and nurture (the way her parents brought her up). In fact, the belief that a child's upbringing shapes her character is such a deep part of our culture that even academic sociologists and psychologists have never really doubted it.

However, as you'll see, the evidence amassed by developmental psychologists to support the nurture assumption has been biased from the get-go.

The nurture assumption is a huge cultural myth. That's because a child's environment is about a lot more than the way her parents raise her. Think about all the important people in a child's life who are outside her family, such as friends, peer groups, an adoring teacher or an authoritarian football coach.

In that sense, parents are clearly not the _only_ influential factor in a child's life and, even if research happens to find connections between a child's character and her upbringing, the results tend to be unreliable. That's because scientists can't just take 500 children away from their parents for the sake of research.

Instead, researchers are resigned to looking for real-life correlations between a trait like shyness and an environmental factor like how often a child is punished. In the process, other factors in a child's environment, for example bullying, fall by the wayside and it's difficult to find multiple studies that show the same correlation.

> "_You cannot coat a child with honey and expect it to protect her against all the vinegar in the world._ "

### 3. Compared to our genetic makeup, the home we’re raised in barely affects our character. 

If you grew up with siblings, you probably felt at one point or another that you're nothing like them, only to realize later that you share some similarities after all. And, actually, that's not too surprising.

Why? Well, genes impact personality. Just look at genetically identical twins: multiple studies have found that even when they're separated at birth and raised in completely different homes, twins often end up with remarkably similar traits.

Take the two Jims, who were one of the pairs investigated in the famous _Minnesota Twin Family Study_, conducted between 1979 and 1999 by the behavioral geneticist Thomas Bouchard. The twins were genetically identical but raised in very different environments. Nevertheless, when they met later in their lives, they both bit their nails, did woodwork, drove the exact same car and even liked the same brands of beer and cigarettes.

Not just that, but they had named their respective sons "James Alan" and "James Allan." Pretty uncanny, right? And good evidence that genes _do_ affect our development.

But there's more to the story: twins' similarities exist whether they were raised in the same home or not. So, by that logic, shouldn't twins who grow up together be practically identical?

Nope! In fact, growing up in the same home doesn't produce any more similarities. That's because the similarities between twins depend entirely on their genetic makeup, not their upbringing.

For instance, the same Minnesota-based study looked at the personality traits of identical twins who grew up in the same house. The research considered things like how shy, conscientious or agreeable each twin was and found that the traits correlated by a mere 50 percent. Statistically speaking, this means they're no more similar than twins who grew up separately.

From this information, we can deduce that parents have little control over how their children turn out. But that's hard to believe, given that children's character is so obviously influenced by their parents' behavior in everyday life. We'll explain this incongruence in the next blink.

> "_It's not that good parenting produces good children, it's that good parents produce good children._ "

### 4. Our behavior isn’t written in stone but adapts to the social context. 

Have you ever tried to teach your cat that it's OK to be in your bed as long as your partner isn't around? Well, don't. Because the cat will either stubbornly plant itself in your blankets or anxiously shun the bed 100 percent of the time.

But humans aren't cats and we _can_ instinctively discover new rules when a situation changes. Unlike a cat, we don't just adopt a rule once and for all, regardless of the situation. That's because we're always looking out for new rules, even from a very young age. For instance, if you tie a ribbon around the leg of a six-month-old and connect the other end to a mobile hanging above her, the child will rapidly discover that she can jiggle the mobile by moving her leg.

However, if the situation changes, say the child's crib is moved to another room, after a few attempts at tugging on a string that's not there, the baby will realize there's no more mobile and move on to the next thing. Even as we get older, when faced with new situations, we still assume that new rules apply.

That's why we're so inclined to adapt our behavior to a situation, particularly when this concerns the people around us. Which makes sense. After all, different situations have different payoffs and punishments.

For example, to win the affection of his mother, a little boy may act cute. But he wouldn't behave that way in front of his friends because he knows they'd tease him for such babyish behavior. That is to say, the boy adapts his behavior based on the person he's interacting with.

That means, even if a child's behavior depends on his parents in a certain situation, it doesn't mean his personality is entirely determined by them. For instance, if a child's mother is depressed and rarely smiles at her kid, the child might in turn look sad whenever they're together. But that same child could be the smiliest kid in the room while at nursery school with his warm, smiling caregiver.

So, even if a child mirrors the sadness of his mother when she's around, the child's sadness is not inherent to his personality.

### 5. Children acquire language through imitation. 

Have you ever been at the zoo, hanging out around the chimpanzee cage and watching other zoo-goers imitate the movements and sounds of the primates? Maybe you've even made the "oo-oo" sounds yourself. But have you ever seen a chimp pretending to be a human?

Probably not. That's because humans have much more of a proclivity to imitate than chimpanzees do. In fact, we learn to speak by copying the words of other people.

For instance, early developmentalists wondered whether a child could automatically acquire language skills if it were raised in isolation. Naturally, conducting such an experiment is practically impossible, but a different type of experiment found that humans learn language by imitation and that the language they learn is dependent on _what_ they imitate.

In the early 1930s, the psychologist Winthrop Kellogg took in an infant chimpanzee named Gua and reared it in his own home alongside his infant son, Donald. The human child would consistently imitate the chimpanzee and eventually even began expressing himself as a chimpanzee, for instance by barking when he was hungry.

Donald also fell noticeably behind in learning English. For example, at 19 months, the average child has a vocabulary of about 50 words, but at this age Donald only used three, and that's where the study ended.

The experiment may have inhibited Donald's early capacity for language, but it also showed why humans don't need their parents' help to learn a language. That's because Donald would have learned English as soon as he encountered human playmates and found that they didn't respond to "Chimpanzee."

In fact, sometimes children don't even have the chance to learn the locally spoken language at home. For instance, their parents might be hearing impaired or immigrants. But nonetheless, these children pick up the language, whether from their peers or another source.

### 6. For a child to become a stable adult, no mommy is needed. 

If you walk into a kindergarten classroom, it's not uncommon to see a young child clinging to his mother's leg, begging her not to leave him. Why do kids do this? Why don't toddlers happily go and socialize with their peers?

Because young children like having their mothers close. As we all know, the mother-child relationship is powerful. After all, mothers provide security in the event of something bad, like a playmate getting too physical. It's a matter of survival — and who knows what dangers lurked in the wilderness where early humans lived.

In one study, researchers placed two babies in a room with their mothers and found that, even when the children were busy playing with one another, they closely watched their mothers, making sure they stuck around. It's this close bond that leads many scholars to the conclusion that a child's relationship with its mother is the basis of all future relationships. But in reality, peers can easily substitute a child's mother.

In fact, how well we relate to others doesn't necessarily depend on our relationship with our mother and peers can easily fill the same role. For instance, a study done by the famous psychoanalyst Anna Freud observed six children who'd been rescued from a Nazi concentration camp around the age of three or four.

All of them lost their parents as well as the close caregivers they had in the camp. Nevertheless, the children stuck together and were quite attached to their group. In fact, they grew upset when separated and demonstrated no signs of rivalry when playing. They would even hand food to each other before eating themselves, a particularly unusual characteristic for children of this age.

In essence, the children were replacing each other's mothers and it helped them grow up healthy despite their traumatic pasts. In fact, they are all reported to have grown up into well-adjusted adults and it stands to reason that they preserved their ability to make close human relationships.

### 7. Girls and boys are different and it’s got nothing to do with how their parents treat them. 

Modern parents tend to go to great lengths explaining to their son that playing with dolls is entirely all right or encouraging their daughter to have fun with a toy truck. But why is it that so many children still refuse to play with toys that are traditionally meant for the opposite gender?

Because even though boys and girls have many similarities, they're still different. Just look at human genetics: we share forty-five unisex chromosomes and it's just that tiny, forty-sixth chromosome, the Y, that accounts for all the difference.

Nonetheless, boys and girls _are_ different beyond just their physical features, i.e., their genitals and that Y chromosome. In fact, they often feel entirely differently about themselves.

For instance, a seven-month-old boy lost his penis due to a circumcision-related accident and psychologists advised the parent to "transform" him into a girl to cover his loss. So, they removed his testicles, created female genitalia, gave him a girl's name and treated him like one.

Despite these efforts, the boy never felt like a girl. In fact, he felt entirely hopeless in life and tried to kill himself at just 14. Later, the truth was revealed to him and he gladly embraced the chance to live as the boy he'd always felt he was.

So, children typically don't become masculine or feminine because of parental expectations. Rather, this occurs through interactions with peers. That's because kids relate to kids, not their same-sex parent, as Freud theorized. The idea isn't that shocking considering the fact that they're much more similar to children their age.

As a result, once they categorize themselves as a boy or a girl, children pay close attention to how children of their gender behave and act accordingly. Essentially, they socialize themselves to be like the other kids with their gender.

### 8. Children and teenagers want be like their peers, not adults. 

Why do kids grow up to be adults? Many people have said that it happens because children want to be like their parents, but the process actually has nothing to do with their parents at all. In fact, it's about maintaining their status in the group.

After all, just like any other human, a child or teenager just wants to fit in with his peers. However, in a group made up of children of slightly different ages, it's the older ones who tend to hold the greatest status. Or, if all members of the group are comparable in age, it's the more physically and psychologically mature individuals who hold higher status.

So, to avoid losing rank in the group, children imitate their "superior" peers. They do this simply because they've equated age or maturity with status and are compelled to avoid humiliation at the hands of the group.

For instance, a 1987 study found that children believed being shunned by their peers in school was the third-worst thing that could happen to them, topped only by losing a parent or going blind. In fact, it's this sense of belonging to a group that makes most teenagers rebellious. That's because, within our complex modern society, teenagers are a category in and of themselves. They're neither kids nor adults, and have no desire to be either of them.

As a result, teenagers see themselves as part of their own group with its own rules and thus hate it when adults impose the rules of their adult group on them. For example, a parent or another authority figure tells a teenager how to dress. The teen will be motivated solely by protecting her status within the group and dress herself in the exact opposite way to signal that she does not belong to the adult group.

### 9. Parents can become leaders of a family group, but it’s the exception to the rule. 

So, is this all to say that parents have no role in how their children turn out?

Well, in general, a family isn't a group, and children aren't invested in being like their parents. So, what does make a group?

Shared traits and a common goal or enemy. However, without a common goal or enemy, as is the case for most families, groups break into individuals who form their own identities by distinguishing themselves from one another.

Just take the real-life example of identical twins, separated as infants. One twin grew up to be a concert pianist while the other couldn't play a single note. One adoptive mother happened to be a piano teacher; the other was tone-deaf. Surprisingly enough, the concert pianist grew up in the non-musical home and music was her way of standing out from her family.

However, in certain cases, families become groups with parents serving as their leaders. For instance, if those same twins were adopted into families who happened to be each other's archenemies, the families would naturally have formed opposing groups.

Not just that, but it would seem to be more likely that the twin with the piano teacher for a mother would end up playing the piano as she would strive to be like her parent. That's because by forming an us-versus-them mentality, parents can transform their family into a group that they lead.

In fact, that's precisely what Donald Thornton did. Donald was a laborer who wanted to ensure that his six daughters ended up with more than he had. So, he made success their shared goal and pitted his kids against their peers, consistently telling them that they were better than other local teenagers and barring them from playing with anyone but their siblings.

As a result, the daughters saw themselves as a group, accepted their father as their leader and all became successful professionals.

### 10. Final summary 

The key message in this book:

**It's misguided to assume that parents are responsible for their children's characters. Parents are just one factor among many — including peers, siblings, culture and genes — that play a major role in the formation of a child's character.**

Actionable advice:

**Find the right peers for your kid.**

Nothing has a greater effect on a child as the attitudes of her peers. Peer pressure will affect your teen a whole lot more than any advice you give her and if you want your kid to succeed academically it's essential that she has good friends. For instance, you could enroll her in a magnet school geared toward intelligent, inquisitive children. In this environment, surrounded by kids who strive for good grades and care deeply about their academic pursuits, it will be easy for your child to do the same and feel good about it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Becoming Attached_** **by Robert Karen**

This book is about the importance of children's first relationships, especially with their primary caregiver, typically the mother. It offers insights into the ways that attachment can positively or negatively affect children's development, and offers a great deal of scientific research on important findings concerning attachment.
---

### Judith Rich Harris

Judith Harris is an American psychologist who holds a degree from Harvard University. She is an accomplished researcher with a long list of publications under her belt, has written textbooks on developmental psychology, and is the author of the book _No Two Alike_.

