---
id: 537c71fe34653000076b0000
slug: the-obstacle-is-the-way-en
published_date: 2014-05-20T09:38:09.000+00:00
author: Ryan Holiday
title: The Obstacle is the Way
subtitle: The Timeless Art of Turning Trials Into Triumph
main_color: B3934C
text_color: 735E31
---

# The Obstacle is the Way

_The Timeless Art of Turning Trials Into Triumph_

**Ryan Holiday**

In _The Obstacle is the Way_, Ryan Holiday brings the age-old wisdom of Stoic philosophy up to date. By examining the struggles of historical figures of inspiring resilience, Holiday shows not only how obstacles couldn't stop them, but more importantly, how these people thrived precisely _because of_ the obstacles. Holiday shows how we can turn obstacles to our advantage, and how we can transform apparent roadblocks into success, both in our businesses and our personal lives.

---
### 1. What’s in it for me? Discover how to transform even the greatest obstacles into advantages. 

All too often, people lose faith in their path because an obstacle appears in their way. They believe an obstacle means they need to completely change their path, or give up entirely. But people have tackled obstacles since the dawn of time, and some of the greatest humans who ever lived have come up with ways of dealing with the struggle of everyday life.

These individuals learned that obstacles will occur in every pursuit we engage in and in every stage of our lives, be it a disagreeable boss who blocks our hopes for promotion, or a precarious economic climate that prevents us from finding work. So instead of avoiding them, we have to tackle them straight on, use them to our advantage, and transform them into conditions for our success. This book shows us the way.

In these blinks you'll learn how one famous figure fought for his inheritance by practicing public speaking with pebbles in his mouth!

And you'll find out what Thomas Edison did when his million-dollar laboratory burned down — destroying all his latest prototypes.

Finally, you'll learn how NASA prepares its astronauts to tackle life-threatening obstacles.

### 2. Perception, action, and will are the keys to turning obstacles to our advantage. 

When faced with an obstacle, most of us get angry, fearful and frustrated. We think it will derail our plans and hinder our progress. But obstacles can actually become advantages, and far from limiting us, they can lead us to success.

How?

To turn an obstacle into an advantage, we have to focus on three things.

First, we should focus on our _perception_ of the obstacle. If we can see an obstacle in the right light, we can reveal the hidden possibilities we can use to our advantage.

This was the key to the oil baron John D. Rockefeller's success. During a financial crisis in 1857, Rockefeller, only 18 years old, watched how those who panicked behaved, and observed what they did wrong. By analyzing their failures, Rockefeller was able to see the obstacle — the Panic — in another way. He was then able to perceive the advantages the crisis offered, take action and start on his way to becoming one of the world's richest men.

But perception alone is not enough. When you are faced with an obstacle, you also need to respond with the correct _action_.

The correct action is born from the combination of creativity and flexibility. For example, at the beginning of the twentieth century, Amelia Earhart wanted to become a great pilot, but an obstacle stood in her way — back in those days, women just didn't become pilots. So she took a mundane job to survive, but kept looking for creative ways to accomplish her dream.

One day she received a call saying that someone would fund the first female transatlantic flight, but she could only be an unpaid passenger. Though this was far from what she wanted to accomplish, she accepted — and this jumping off point led to a career as a great aviator.

After identifying the best way to perceive an obstacle and the best action to overcome it, we use our _will_ to persevere until the obstacle has been overcome.

Over the next few blinks we'll look at each of these in more detail — starting with perception.

> _"People turn shit into sugar all the time — shit that's a lot worse than whatever you're dealing with."_

### 3. In order to perceive obstacles clearly, we must learn to see objectively. 

When you encounter an obstacle, how do you respond? Do you think the world might be against you, and that nothing ever goes your way? Instead, you should be thinking about taking a step back, looking at the situation objectively and thinking, "What can I do to turn this obstacle into an advantage?"

If we look at them in this way, even the biggest obstacles can be turned to our advantage.

For example, in the 1960s the then celebrated boxer Rubin "Hurricane" Carter was falsely accused of a triple homicide, and sentenced to jail for life. But as he entered prison, he resolved to not let his circumstances overwhelm him, and instead tried to see how he could make the best of it.

First, he vowed to not live with the injustice, but keep fighting to overcome it through the court of law. Then he turned his limited physical freedom to his advantage by using his time inside to study history, philosophy and law. Carter's case was eventually overturned and he was released after 19 years. Carter left jail as he had entered it, a free man. But in addition, he had also used the time to improve his education and himself.

So how can we achieve this objective perspective?

By learning to detach yourself from your own personal — and highly subjective — view of the situation. You can do this by imagining that you are advising a friend about overcoming the obstacle. What would you say to them? How would you let them approach it?

Or you can take after the Stoics, a group of philosophers from Ancient Greece. They would imagine how a sage — a person of perfect wisdom — would react to the obstacle. In that way, they would explore hidden sources of wisdom that they didn't consciously know they had.

### 4. Uncontrolled emotions cloud our judgment and perception. 

Our first reactions to an obstacle are usually anger, frustration, anxiety and confusion. Though natural, such emotional outbursts actually make it harder for us to deal with obstacles because not only do they prevent us from judging the situation objectively, they actually make us _see_ it differently. So if we want to turn our obstacles into success, we have to learn to control our emotions.

How?

The key to controlling our emotions is to steady our nerves.

A famous role model for steely nerves is the Civil War general, Ulysses S. Grant. He was once being photographed by famous photographer Matthew Brady when he nearly lost his life. As there wasn't enough light for the photo, Brady asked an assistant to open the studio's skylight. But the assistant accidently broke the window, and large shards of glass fell right next to where Grant was sitting. If he had panicked and jumped from his chair, he might have been seriously hurt — but Grant didn't flinch, and sat calmly in his chair as the shards shattered around him. By controlling his nerves, Grant prevented his emotions from getting him hurt.

So how can we gain control over our emotions?

By preparing ourselves for what can go wrong in any situation. This allows us to remain calm no matter how much external events may change.

NASA has realized how important this is. A panicking astronaut is the single largest factor that contributes to potentially life-threatening mistakes in outer space. That's why NASA prepares its astronauts to respond to any possible obstacle until their reaction becomes automatic. This allows them to deal with stress when anything goes wrong with the initial plan.

Emotional responses to obstacles blur our judgment, thus making it impossible to turn obstacles to our advantage.

### 5. A malleable perspective allows us to see an obstacle’s hidden advantages. 

When we are met with an obstacle, we often only see it in one way: insurmountable. But if we can change our perspective, then we can reveal the advantages an obstacle hides beneath the surface.

How?

By taking the larger context into consideration. Looking at the situation in isolation often makes the obstacle seem impenetrable; but if we place it in context, we can often see that it's truly unimportant.

For example, when setting out to battle during the Ancient Greek Peloponnesian War, the general Pericles and his men were suddenly cast into darkness by a solar eclipse. Many soldiers saw this as a bad omen, and became afraid of the impending battle.

But Pericles was undaunted, and used this situation to his advantage to motivate his men. He took a dark shroud and placed it over the face of one of his soldiers, and asked him if he was afraid. The soldier replied that he wasn't. Pericles then asked why they should allow another cause for darkness to deter them in battle and make them fearful. This allowed the soldiers to see the eclipse in a new light, and become emboldened for the fight.

Perspective can also be used to transform obstacles into advantages.

George Clooney learned this early on in his career. When he first came to Hollywood, he struggled to land a role. He became angry at directors, producers and casting agents, blaming them for not recognizing his talent.

But then he changed his perspective. He realized that it was not his obstacle, but the film producers' — they were the ones who desperately needed to find the right actor. So Clooney decided to mentally approach the audition as _the_ actor they were looking for. He believed that if he made himself stand out as the solution to their obstacle, he would get the job. And by changing his personal perspective, Clooney was able to transform the initial obstacle into the first stepping stone towards success.

In the following blinks, we'll find out what's the best course of action when confronted with the many obstacles of life.

> _"Great individuals, like great companies, find a way to transform weakness into strength."_

### 6. Action must be persistent and disciplined to be effective. 

We've seen the importance of changing perspective, but on its own, it's not enough — we also have to take action. However, in the face of the many obstacles that separate us from the good life, we need more than a single act — we need the strict discipline to act, and keep acting until the obstacle is overcome.

One person who proved that discipline wins out was Demosthenes — the greatest orator of ancient Athens.

On top of being cheated out of his inheritance by his guardians, Demosthenes was a sickly and frail person with a speech impediment. But when faced with all these obstacles he didn't give up. Instead, he came up with a plan of action and stuck to it with incredible discipline. He locked himself in his study and taught himself law so he could take his former guardians to court. And to improve his speech, he would repeatedly recite oratory with his mouth full of pebbles!

Through his persistence, Demosthenes became not only the most famous orator in Athens, but also won his trial against his ex-guardians.

Just like Demosthenes, the inventor and entrepreneur Thomas Edison knew that effective action depends on our persistence.

In 1878, Edison went through 6,000 different types of material in order to discover the correct filament (a piece of bamboo) for the incandescent light bulb. Though there were other inventors also attempting to discover the incandescent bulb at the time, no other inventor went through so many different experiments to make sure they got it. It's this persistence and tenacity that is universally recognized as the key to Edison's great success.

### 7. Focusing on each moment and the overall process helps us achieve our goals. 

Imagine you're part of a long-term project, and however persistently you act, it seems that the obstacles just keep on coming. What should you do? Instead of thinking about the goal, you should focus on _each moment_ and the _overall process_.

Why?

Because this allows us to deal with the tasks at hand as effectively as we can, which will increase our chances of long-term success.

Let's look at two highly successful companies that were both formed in incredibly difficult economic times.

The Walt Disney Company was formed eleven months prior to the market crash of 1929. And the IT company, Hewlett-Packard, was formed during the Great Depression in 1935. Both these companies succeeded because they didn't let the problems in the wider economy derail their thinking about day-to-day activities. Instead of concentrating on the overwhelming goal of becoming successful in times of economic crisis, they kept focused on the moment at hand, not the obstacles surrounding them. And step by step, they grew into the famous companies we know today.

Another way of looking at this is to see each moment as part of the larger process that leads toward the goal. Professional sport coaches use this paradigm to transform what might look like the impossible effort to climb a mountain into the single, regular steps necessary to reach to the top.

For example, Nick Saban, head coach of the University of Alabama American football team, teaches his players to follow the _process_. Saban tells his players not to focus on the goal of winning the championship, but rather on performing well in each game, and each play in the games. Breaking down the road to the goal in this way allows his players to invest their energy in the task at hand without worrying about all the "obstacles" such as defeats or possible injuries.

Correct action depends upon focusing on the entire process and living within each moment of that process.

### 8. By identifying their weakness, we can learn how to turn obstacles against themselves. 

Sometimes you will be faced with an obstacle so huge you'll think you can't overcome it. But often the largest obstacles hide the largest weaknesses.

For example, Gandhi's civil disobedience — the non-violent movement known as _satyagraha_ — and in particular, the Salt March, show how great power can be turned against itself.

Gandhi knew he couldn't challenge Britain's military rule in a direct conflict, so he decided to use non-violence and symbolic acts to show how unjust it was. His Salt March led hundreds of thousands of Indians to the ocean to collect salt in direct opposition to the British law that prohibited unregulated salt collection. And because it was a non-violent endeavor, it contested the British rule without lifting an arm in combat. The great strength of the empire was its monopoly of violence, and by challenging it non-violently, he was able to show how weak it actually was.

Another way to exploit an obstacle's weakness is to use its strengths against it.

Alexander the Great did this when he tamed his horse, the wild Bucephalus. Until Alexander gentled him, Bucephalus would not allow anyone to ride him, and would fight off anyone who tried with furious anger. So Alexander made Bucephalus run in a straight line until the horse was exhausted, and gave up trying to resist. When Alexander then mounted him, the stallion let Alexander steer and control him. Alexander had used his strength in riding (his endurance) to tame Bucephalus by exploiting the horse's obvious weakness (its constant anger used up its energy).

So by using its strengths against itself, even the strongest obstacle can be overcome.

In the following blinks, we'll learn how to keep persevering despite constant obstacles.

### 9. Our will enables us to accept what we cannot change, and change what we can. 

Perception — the ability to see a situation objectively — and action (the practical skill of transforming the obstacle into our advantage) are, by themselves, not always enough. What really makes the difference is the last ingredient: our _will_. Unless we believe in ourselves and apply this internal power to all our actions, we will be unable to turn obstacles into advantages.

The will is what channels our perception and action into recognizing and changing what we can, and not worrying about things we cannot. This philosophy was developed by the Stoics in Ancient Athens and Rome, such as Epictetus, Seneca and Emperor Marcus Aurelius. They focused their will by always asking themselves what was in their control and what was not. They believed that we could not change external factors. These include natural events, other people's actions, the inevitability of death, etc. They also believed, however, that we _could_ change internal factors. These include our emotions, judgments, attitudes, responses and decisions.

Armed with this knowledge, we can then apply our will to change what we are able to change — i.e., our _internal_ obstacles — while still facing and accepting the external obstacles we will inevitably encounter.

This principle is embodied in Thomas Edison's reaction to a terrible blow to his career. In the early 1900s, when Edison was 67 years old, his research and production campus caught fire. When Edison arrived on the scene, the whole building — including all his prototypes, papers and research — had gone up in flames.

But Edison was unfazed, and saw this event as an opportunity to start over. He realized that he could not undo the fact of the fire, but could change his perspective, and approach a seemingly insurmountable obstacle as a way of starting over — getting "rid of a lot of rubbish," he called it. And at the end of the year, Edison had turned a million dollar loss into a $10 million profit.

### 10. A disciplined will allows us to push ourselves to our mortal limits. 

By learning to accept what we cannot change, and disciplining our will to change what we can, we can master our obstacles — and ourselves.

This disciplined will allows us to persevere in the face of the most difficult obstacles, as shown in one of the founding texts of Western literature, the _Odyssey_, by the Ancient Greek poet Homer. In the _Odyssey_, we find Odysseus departing Troy after ten years of fighting. Little does he know that he will spend another ten years facing many trials and tribulations as he attempts to reach home. Along the way, he is held prisoner, faces temptation, loses all of his men, encounters dangerous whirlpools, and even fights off a cyclops and a six-headed monster!

What allowed him to overcome all the obstacles sent by the gods? His will to get home.

Another way our will can push us to the limits of human possibility is its power to ignore our personal situation in favor of more important goals. This is perfectly illustrated by James Stockdale, a US pilot taken prisoner during the Vietnam War.

Prisoners of war knew that they would be tortured and quite possibly killed. But instead of giving up or simply fearing for his life, Stockdale decided to be a leader for his fellow prisoners. Knowing that some of the tortured might be driven insane, Stockdale formed a support system in the camp to make sure all the soldiers knew they were in it together and should not feel ashamed for having given information. By using his will to concentrate on the greater goal of survival, Stockdale was able to help himself and others endure years of imprisonment and torture — and come out on the other side alive.

### 11. Final summary 

The key message in this book:

**By perceiving obstacles objectively, acting against their weaknesses and persevering in your will, you will transform the obstacles you meet into the fire that fuels your success.**
---

### Ryan Holiday

Ryan Holiday, best-selling author of _Trust Me I'm Lying_ (also available in blinks), is a media strategist and director of marketing at American Apparel. His media strategies are used as case studies by Google, YouTube and Twitter.

