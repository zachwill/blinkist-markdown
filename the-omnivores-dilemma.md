---
id: 51016e81e4b0991857b9260f
slug: the-omnivores-dilemma-en
published_date: 2013-04-13T00:00:00.000+00:00
author: Michael Pollan
title: The Omnivore's Dilemma
subtitle: A Natural History of Four Meals
main_color: 4AB2C3
text_color: 307480
---

# The Omnivore's Dilemma

_A Natural History of Four Meals_

**Michael Pollan**

We face an overwhelming abundance of choices when it comes to what we eat. Should you opt for the local, grass-fed beef, or save time and money with cheap chicken nuggets? Organic asparagus shipped from Argentina, or kale picked from your neighbor's garden? _The Omnivore's Dilemma_ examines how food in America is produced today and what alternatives to those production methods are available.

---
### 1. The huge number of choices available today makes it hard to decide what to eat – this is the omnivore’s dilemma. 

As omnivores, we humans are capable of eating many different plants and animals. This leads to what psychologist Paul Rozin calls the "omnivore's dilemma": with a world of possibilities, how do we know what we should eat?

For early hunter-gatherers, solving this dilemma was very straightforward: they ate the seasonal foods that could be harvested near their homes, such as mushrooms in the fall or strawberries in the summer, and hunted game that was available in the wild. This made for a pretty uniform menu, which made choosing what to eat very easy.

Today, advancements in our ability to preserve and transport food have completely changed the way different foods are available to us. Think back to the last time you were in a supermarket. How many aisles were there? How many shelves? How many items on each shelf? Coconuts, leeks, Oreos, bacon, eggs, rice, broccoli, strawberries — the selection of food available today is mind-boggling, and you can basically have whatever you want, whenever you want it, wherever you are.

This development has exacerbated the omnivore's dilemma, as we must now choose among countless options for each meal. Some are healthy, some are tasty, some are cheap and some are good for the environment. So exactly what _should_ we eat?

### 2. Industrial agriculture makes food cheap, but its environmental, public-health and ethical costs are sky-high. 

Once upon a time, farmers grew crops and raised cattle using nothing but the sun and soil. However, such traditional farming methods only produce relatively small quantities of local, seasonal food and are no longer enough to feed the world's population. Hence, farmers have developed industrial-farming techniques and machines to produce food faster and on a larger scale.

Some people would say this is a good thing. In the past, it was expensive to raise, feed and slaughter livestock for food. As a result, meat was expensive; people didn't eat it every day. Now, however, industrial-farming methods have made raising livestock — and in effect meat itself — incredibly cheap.

Out-of-season produce has also become widely available. You live in Seattle but want fresh asparagus in January? No problem; it's shipped from Argentina. Add to this the fact that the growth seasons of many plants have been extended to unnatural lengths through industrial-farming techniques, and you can pretty much buy any fruit or vegetable whatever the season.

Unfortunately, cheap meat and year-round asparagus come at a cost: in the name of efficiency and mass production, large-scale industrial agriculture pollutes the air and water, pumps chemicals and pesticides into our food, treats animals unethically and spreads diseases.

### 3. Corn is one of the most important crops in the United States, and it's heavily subsidized by the government. 

Corn is very adaptable and genetically robust. It produces large harvests quicker than other crops, so when Europeans discovered corn when they colonized the Americas in the sixteenth century, it quickly became a staple crop. As technology advanced, farmers began breeding corn hybrids to optimize output even further. Advancements included thicker stalks and stronger root systems that could withstand harsh mechanical harvesting and also stand closer together to fit more plants per acre.

As these varieties were adopted by farmers, corn production quickly increased. In 1920, farmers produced 20 bushels of corn per acre; they now produce 180.

In 2005, it cost a farmer about $2.50 to produce a bushel of corn. But due to the abundance of corn, buyers were only willing to pay $1.45 per bushel. Of course, if farmers were to lose a dollar on every bushel of corn they produced, they would go out of business, which is why the US government subsidizes the farmers by making up the difference.

With such subsidies, the system of supply and demand becomes irrelevant. The farmers simply flood the market with corn and still make a (wholly artificial) profit on every bushel. Hence, the price of corn keeps dropping, but the US keeps producing more corn.

### 4. To sell the excess corn farmers produce, companies add processed corn-based ingredients to food. 

These days, corn is less of a food and more of a commodity. In fact, one in four items in the average American supermarket contains corn in one form or another. Chicken nuggets, for example, are usually made of cornstarch, corn oil and chicken that was fed corn.

So why is corn everywhere?

Food industry executives have long faced the _fixed stomach_ problem; every person can only eat about 1,500 pounds of food each year. To grow, food industry companies like General Mills and McDonald's have to convince people to (a) spend more money on those 1,500 pounds of food, and/or (b) eat more than 1,500 pounds of food per year.

In this regard, America's huge corn surplus is particularly problematic, since there is more corn than the population could possibly eat. This is why much of our corn goes to so-called _wet mills_, where it is repurposed to create a wide array of artificial-sounding ingredients, such as "high fructose corn syrup" or "hydrogenated fat" found on nutrition facts labels. These synthetic ingredients are then used in a variety of foods like soda, TV dinners, breakfast cereals and so forth.

These new uses for excess corn are very profitable for the food industry. Heavy processing greatly extends the shelf life of products, allowing food corporations to take a larger slice of the profits and leaving farmers with less. When you buy chicken nuggets, for example, you pay very little for the actual chicken and a lot for the services required to turn the corn into synthetic corn-based ingredients and then those into something that resembles food.

### 5. Meat prices have been brought down by the use of monolithic mass-feeding operations called CAFOs. 

Besides going into processed foods, much of the corn surplus is also used to feed farm animals that we later eat. From the food industry's perspective, animals are like machines that turn excess corn into sellable meat, though machines are usually treated better.

Enter concentrated animal feeding operations, or CAFOs for short.

CAFOs are facilities for raising animals unlike any farm you have ever imagined. They maximize efficiency — and profit — by cramming as many animals as possible into cages or pens while automating and mechanizing as much farm work as possible, such as feeding. This efficiency ethos, as well as the cheap feed due to the corn surplus, has brought meat prices down to previously unheard of levels.

Before the advent of CAFOs, the care, time and resources needed to raise animals on small, local farms meant meat was expensive: a rare treat. But today, a bacon cheeseburger, for example, is so cheap you can eat one every day if you want to — something many people do.

### 6. To keep meat cheap, CAFOs treat animals unethically and cause huge environmental and health risks. 

At first glance, CAFOs might even sound like a good thing: Who would object to cheap and delicious bacon cheeseburgers? Unfortunately, they come at the price of animal rights, sustainability and public health.

CAFOs operate by optimizing output to maximize profit. Animals are forced into crowded spaces without access to pasture or space to move around, leading to suffering and the spread of diseases.

Corn is so cheap that CAFOs use it as animal feed whether the animals have evolved to eat it or not. Even a carnivorous fish like salmon is being reengineered to tolerate corn. Because cattle wouldn't normally eat corn either, at CAFOs they suffer from all manner of illnesses — think bloat so intense that it can press on a cow's lungs and suffocate it, and heartburn so severe it causes ulcers, liver disease and a weakened immune system.

The only way to keep animals alive until slaughter in such conditions is by pumping them full of antibiotics. However, overusing antibiotics to keep sick animals alive can lead to the development of antibiotic-resistant "superbugs," which can wreak havoc in human populations too.

If you thought that was the worst of CAFO offenses, think again. They also contaminate downstream waters with the hormones and heavy metals they use. The fertilizer their animals produce is often used on industrial farms, and could easily spread a new, lethal strain of _E. coli_, born in the dangerously unhealthy conditions at CAFOs.

Ethics, the environment and public health are all secondary concerns to CAFOs. What they really care about is maximizing efficiency and profit.

### 7. Organic food offers some benefits over conventionally produced food … 

Originally, the movement started as a grassroots initiative to solve a lot of the problems caused by industrial agriculture: pollution, pesticides, and the fossil fuel demands of shipping fruits and vegetables around the country. Organic was more expensive than conventional produce, but the process was much better for the environment and the food far healthier for people.

At the beginning of the movement, many farmers started out by selling produce from stands on the side of the road rather than shipping their products across the country. And rather than using pesticides and chemical fertilizer, they used natural, often local compost or manure from nearby farms.

Many studies have compared organic produce with that grown industrially. The results indicate that produce grown without pesticides and chemical inputs is both better tasting and healthier.

When tomatoes are allowed to grow at their natural pace — without chemicals to speed up growth — they develop thicker cell walls, which gives them more concentrated flavors and hence a far better taste.

What's more, other studies have found that organic fruits and vegetables contain more vitamins and cancer-fighting polyphenols than conventional ones.

### 8. … but the organic food system today is far from perfect. 

Picture a happy cow grazing on luscious green grass amid rolling hills. This is probably where you imagine your organic milk came from, partially because the image often adorns organic milk cartons. This idyll is compelling — so compelling that customers are willing to pay a higher price for it and food companies know this.

But "organic" doesn't necessarily mean what you think it means.

As the organic movement gained popularity, small, idyllic farms like the one in your imagination couldn't keep up with demand. They expanded — and that meant sacrificing some of the movement's original ideals. In fact, many problems of conventional agriculture are present on large-scale organic farms, too.

As the organic business grew, the US Department of Agriculture developed lax standards that allowed food companies to cut corners and still get labels like "organic" or "free-range" for which environmentally conscious consumers are willing to pay more. Although smaller producers fought for stricter regulations, big corporations won out.

Under these vague guidelines, you can, for example, cram 20,000 chickens into a shed with two weeks' access to a small yard and call them "free range." Similarly, oddities like "organic TV dinners" and "organic high-fructose corn syrup" have emerged.

Although smaller organic farms do still exist, most organic food in supermarkets comes from big ones that cut corners. This is because supermarkets want to stock a full range of fruits and vegetables year-round, regardless of local and seasonal availability. Unfortunately, small companies usually only produce what grows locally and seasonally, whereas large ones can use industrial techniques to overcome these limitations.

### 9. Management-intensive grazing is a far more natural and sustainable alternative to overproducing corn. 

As we've already learned, corn plays a major role in creating the myriad problems of the conventional food system, not least of which is the havoc corn wreaks on cows' digestive systems. But growing corn also neglects many natural _coevolutionary relationships_ that could be taken advantage of.

One of the best ways to optimize production sustainably is to grow grass instead of corn and to use _management-intensive grazing_ — a farming technique that involves moving animals to different pastures every day to promote optimum grass growth using the plant's natural growth cycle.

This method takes advantage of the coevolutionary relationship between cows and grass that is wholly ignored in industrial agriculture. Cows don't overgraze their favorite kinds of grass, which allows a diversity of species to thrive in the pasture, and at the same time they get to enjoy their natural diet rather than harmful corn that makes them sick and bloated. And healthier cows produce healthier meat.

Management-intensive grazing is also better for the environment. The natural biodiversity of grass that flourishes — unheard of in the sea of corn that is the American Midwest — maximizes absorption of solar energy and carbon. The grass effectively takes thousands of pounds of carbon out of the atmosphere and stores it underground.

### 10. Small, local farms provide an economically, environmentally and ethically sustainable alternative. 

Our current system for producing food values efficiency and profits over ethical concerns, environmental sustainability and consumers' health.

So what can we do? We can buy from small, local farms instead of large, industrial ones.

First of all, buying locally helps reduce the amount of fossil fuels needed to transport food from the producer to the consumer, a distance which today might span countries or even continents.

Also, from an economic standpoint, buying locally helps put profits in the hands of small business owners and farmers rather than huge corporations.

What's more, small, local farms don't rely on pesticides or unnatural farming techniques to produce large amounts of food. Instead, they grow food seasonally, promoting the natural ecosystem rather than interfering with it. This makes them the obvious environmental choice, too.

Finally, local farms are almost always the more ethical choice. Being able to drive down the road to see a butcher or farmer at work creates accountability, which makes them less likely to resort to unethical practices, such as treating animals poorly just to increase profits.

### 11. Final summary 

The key message in this book:

**Much of the food we eat today is produced industrially, which often means unethical practices, environmental damage and that the food contains some processed derivative of corn — a crop produced far in excess of our needs. While organic food does provide some advantages over this system, it is not without its problems. The best solution is to buy from small, local farms.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael Pollan

Michael Pollan is a prominent American journalist and a professor at the UC Berkeley Graduate School of Journalism. His other works include _In Defense of Food: An Eater's Manifesto_ (2008) and _The Botany of Desire: A Plant's-Eye View of the World_ (2001).

