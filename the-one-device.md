---
id: 5a43b400b238e10007128c56
slug: the-one-device-en
published_date: 2018-01-05T00:00:00.000+00:00
author: Brian Merchant
title: The One Device
subtitle: The Secret History of the iPhone
main_color: 455697
text_color: 30438C
---

# The One Device

_The Secret History of the iPhone_

**Brian Merchant**

_The One Device_ (2017) lays out the history of what may be the most important piece of technology on the market: the Apple iPhone. From an interview with an IBM engineer to the frightening depths of a Bolivian mine, Merchant takes us everywhere and explains how the iPhone was born and what it means for the world.

---
### 1. What’s in it for me? Learn the little-known story of the one device to rule them all. 

Apple's iPhone is often seen as the pinnacle of modern consumer technology. It rules modern markets, having replaced MP3 players, cameras and palm pilots with one handheld miracle. But this device's story is deeper and more complex than is immediately apparent.

The iPhone wasn't the first smartphone. It rests on the shoulders of countless other innovations, including phones, voice-recognition tools and touch-screen technology.

And the innovators who paved the way for the iPhone aren't the only unsung heroes in this tale; there are also the millions of workers worldwide who toil in abominable South American mines and Chinese mega factories.

In these blinks, you'll learn

  * why Steve Jobs can't really be credited with inventing the iPhone;

  * the horrible reality behind the raw materials used to manufacture the iPhone; and

  * that the iPhone's roots reach back to an early twentieth-century Swedish inventor.

### 2. The iPhone has changed the technological world and its history stretches far beyond Steve Jobs. 

Unless you've been living under a rock for the last ten years, you know that the iPhone is a colossal success. Indeed, it's so successful that, in 2016, when technology-industry expert Horace Dediu made a list of the world's top products, he listed the iPhone as not only the best-selling phone, but the best-selling camera, music player, video player and computer. The phone has sold one billion units. To give some perspective, that's 550 million more units than the mega-hit _Harry Potter_ series sold.

Not just that, but when Wall Street analysts took stock of the most profitable products in the world, the iPhone was one of the top items on their list as well. It was even one place above Marlboro cigarettes, one of the biggest manufacturers of one of the world's most addictive products.

So the iPhone is ludicrously popular. But why?

Most people attribute its success to Steve Jobs, the man often given sole credit for its invention. However, the history of the iPhone truly begins in the early 2000s with a small group of Apple employees who were secretly experimenting with human-computer interfaces.

The group contained a few software designers and input engineers and one industrial designer, all of whom met, without Job's knowledge, to experiment with unconventional user interfaces. Among them was Joshua Strickon, who'd recently received his PhD from MIT Media Lab. He was a wiz with human-computer interaction and touch-based technology software.

Alongside Strickon were people who pioneered the field itself, like Greg Christie, head of the Human Interface team and a lead force on Apple's handheld mobile device, the Personal Digital Assistant.

Other crucial people in the team were designers Imran Chaudhri and Bas Ording. One member of the original iPhone team described them as "the Lennon and McCartney of user interface design."

The group collectively believed that the traditional keyboard and mouse were outmoded. So they set out to enable more direct interaction with computers and explored motion sensors and multitouch technology in particular.

After months of tinkering, they produced the first, very low-tech prototype of what would eventually become the iPhone. However, they certainly weren't the first to delve into such technology.

### 3. The first mobile phone is a century old, but cell phones didn’t take off until the 1980s. 

You might be surprised to hear that the original precursor of the iPhone is quite old — over a century old, to be exact.

The first mobile phone was made in 1910 by the Swedish inventor Lars Magnus Ericsson, who later founded the tech giant Ericsson. To be fair, this mobile device was a car phone and to function it actually had to be directly connected to telephone lines using a wire.

But it led to the invention of an even more mobile device in 1917. This second iteration was made by a Finnish inventor named Eric Tigerstedt and it was truly wireless. It was a flip phone with certain similarities to modern mobile phones, featuring a thin, minimalist aesthetic.

But despite these early innovations, mobile phones didn't catch on until the 1980s and it wasn't until the 1990s that the first "smartphone" was produced. That's right, decades before the iPhone, Frank Canova Jr., then an engineer at IBM, produced the Simon Personal Communicator, or the Simon for short. It was the first mobile phone that featured a computer.

And therein lay the actual innovation of the product: it boasted a touchscreen and applications, the features that made it "smart."

In fact, Canova's initial goal was to add all manner of apps, like GPS and a stock ticker. He even developed some of them, but in the end the hard drive of the device couldn't support them all. With the few apps and games the phone came with, it was already as big as a brick.

This was a major issue in creating a popular smartphone; the technology just wasn't there yet and the issue of size also explains why the Simon's successor, the Neon, never even went to market. Nonetheless, these early innovations were important inspirations for the inventors of the iPhone, two decades down the line.

### 4. The battery in your iPhone is rechargeable thanks to years of research, trial and error. 

One of the most remarkable things about the iPhone is its battery capacity. Interestingly, the technology behind this super battery has somewhat unusual origins. It dates to the oil crisis of the 1970s, when prices were soaring and the public was getting uneasy — and scientists everywhere were asking whether there might be a way to decrease dependence on oil.

This fear resulted in greater alternative-energy research. Exxon, for instance, hired a brilliant Stanford University chemist, Stan Whittingham, whose job it was to find new energy sources. And his work broke new ground.

At the time, the most common batteries were made of zinc and carbon. Panasonic had recently produced a lithium-based battery, but it wasn't rechargeable.

Today, the lithium battery in the iPhone _is_ rechargeable, and we have researchers like Whittingham and the brilliant physicist John Goodenough, to thank for it. Here's how it works:

When a battery is used, electrical current in the form of electrons travels through an electrolyte from one electrode — the anode — to another, the cathode. If the battery is capable of being recharged, an energy source, say a wall outlet, reverses the flow of current. Essentially, it causes energy to travel back to where it came from, making the battery reusable.

However, in his early experiments, Whittingham was experiencing some serious issues; namely, his batteries kept overheating and catching fire. Luckily, John Goodenough solved the problem. Unlike Whittingham, who used titanium in his lithium battery, Goodenough used cobalt oxide. This chemical combination resulted in a more stable battery that's still widely used to this day in all manner of electronics, the iPhone included.

In fact, in 2015, the lithium-ion battery market was worth $30 billion, and that number is expected to grow to an incredible $77 billion by 2024. This dramatic projected increase is in large part due to the rising interest in electric cars, evidenced by the opening of Tesla's Gigafactory, the world's largest lithium-ion battery plant.

### 5. The selfie is old news, but the iPhone camera popularized it. 

Did you know that, in 2007, the so-called "dumb" phones sold by Nokia had a better camera than that of the original iPhone? It's true and, shockingly enough, the original iPhone camera only had two megapixels, compared to the eight megapixels of the iPhone 6.

Even more interesting is that, when Apple began considering incorporating a camera, the company didn't consider it an essential feature. Today, the camera is almost as essential as the phone itself; it's also seriously complex. The camera module that goes into an iPhone today has over 200 parts and is considered indispensable. It has a sensor, an image-stabilization module and an image-signal processor that sharpens the image.

And that's just the camera on one side of the phone; there's also the _selfie camera_, or _FaceTime camera_, on the phone's front. All in all, its technology is so complex that Apple has a separate camera division with some 800 employees, all of whom are working to improve the iPhone camera.

So what's the deal with this selfie camera?

Well, while selfies have been around for years (more than a century actually), the iPhone selfie camera played a major role in popularizing this style of photo. Here's the history behind the selfie:

Back in 1839, Robert Cornelius took his own picture. Technically, it wasn't a photograph but an earlier form of image-making known as a _daguerreotype_. The camera he used was so slow that it must have taken him at least ten minutes to make his selfie.

Then, in 1914, a teenage Russian duchess named Anastasia Nikolaevna took her own photo in a mirror. She later shared the image with her friends and it became one of the first well-known selfies. But the term "selfie" didn't come into popular usage until the selfie camera was added to the iPhone in 2010. This simple addition made it much easier to take your own photo, and it transformed the culture in the process.

### 6. Early versions of the AI technology used in the iPhone emerged decades ago. 

Off the top of your head, do you know if it's going to rain today? How about who invented the light bulb or where the nearest sushi restaurant is?

Well, Siri does. In 2015, this artificial intelligence gave out one billion answers a week. In 2016, that number doubled.

Because of the computer intelligence and other complex technologies behind Siri, it now assists tons of iPhone users in their daily lives. But how does it work?

Siri is parts artificial intelligence, speech-recognition software and language-user interface. Together, they transform human speech into digital language before sending it to an Apple server where another software understands the speech and translates it into written text. From there, the natural-language user interface analyzes it. When she attempts to answer your questions or fulfill your requests, Siri looks to your phone internally. If no solution can be found, she connects to the internet.

Let's take a minute to see how this technology developed.

An early inspiration for Siri was Hearsay II, a kind of Siri prototype. This technology was developed by a young Indian researcher at Stanford named Dabbala Rajagopal Reddy.

The term "artificial intelligence" had recently been coined by John McCarthy and his colleagues in 1956. They were at Stanford as well and Reddy was drawn in by their research. That context, combined with his interest in language, pushed him toward work in speech recognition.

In the 1960s, Reddy and his team designed a system that allowed a computer to understand words. This computer was the largest device of its type at the time, and it understood around 560 words with 92-percent accuracy.

Then, during the 1970s, while at Carnegie Mellon University, Reddy continued to work on this system. He eventually turned it into a speech interface called Hearsay, which was proceeded by its successor, Hearsay II. The latter system could understand a thousand English words and it was just a matter of time before the technology was further refined to become what we now know as Siri.

### 7. The raw materials used to make the iPhone cause all manner of human suffering. 

Can you name all the different materials in an iPhone? Most people wouldn't even know where to start. And the list is pretty long, with at least 30 materials, including aluminum, iron, copper and even a bit of tin.

That tin most likely comes from the mines of Cerro Rico, right outside the Bolivian city of Potosí. After being mined, the tin is sent to smelters, usually EM Vinto or Operaciones Metalúrgicas, before being shipped to, among other firms, the manufacturers of Apple products. In iPhones, this tin is usually used in the form of solder, a tin-based alloy that connects the various components within the device.

This tin from Cerro Rico comes with costs. For instance, since mining began there in the mid-sixteenth century, between four and eight _million_ people have died there from starvation, freezing temperatures and cave ins. As a result, Cerro Rico has earned the morbid nickname "The Mountain That Eats Men."

To this day, around 15,000 people work there, several thousand children among them, and fatality rates are still out of control. In fact, in a recent incident, two kids died while working in a mine. To cope with the brutal conditions of their work, they had gotten drunk and lost their way in the labyrinthine mine, eventually freezing to death. To make matters more frightening, geologists are now warning that the mountain is on the verge of collapse because of centuries of hollowing it out.

So mining the tin for the iPhone causes tremendous human suffering, but that's not the only negative impact of this product. More than half of the tin smelters used by Apple reside on Bangka Island in Indonesia, another site of deadly mines. On this island, the mining overseers randomly, and often illegally, dig pits with tractors, leaving behind unstable walls. These precarious walls often crash down, killing miners in the process. As a result, in 2014, every week a miner died there.

> _Full-time Cerro Rico miners usually don't live much longer than the age of 40. One of the reasons is that the harmful chemicals they inhale on a daily basis destroy their lungs._

### 8. The parts that make up your iPhone are manufactured by exploited workers. 

In China, just outside of Shenzhen, there sits a massive Foxconn factory that manufactures and assembles the iPhone. Foxconn is the largest employer in mainland China; it employs some 1.3 million people globally, a number only topped by McDonald's and Walmart.

The company's Longhua plant in Shenzhen is roughly 1.4 square miles in size and at one point it housed some 450,000 workers. Today, while fewer people work in the plant, it's still one of the largest in the world.

Perhaps unsurprisingly, the working conditions in this factory are abhorrent. In 2010 alone, 14 workers took their own lives by jumping off the tall factory buildings; another four workers attempted to do the same, but didn't succeed; and another 20 were stopped by officials before they could try.

Each suicide attempt is known to have been motivated by the insufferable conditions of the work, which includes long hours, repressive managers and a common system of unjustified and humiliating fines for even the simplest mistakes.

The response of Foxconn CEO, Terry Gou?

Rather than improve working conditions, Gou had nets erected around the building to block the free fall of workers. Meanwhile, Steve Jobs was entirely dismissive, citing statistics about comparable suicide rates at your average university.

In other words, neither Apple nor its contractors seem to care about worker safety — but they do care about the security of their facilities. In fact, when the author visited the Apple suppliers in Shanghai, he found them highly secured. He wasn't allowed inside the facility and was even prohibited from taking pictures of it from the outside. There were large numbers of guards and cameras, and it was entirely surrounded by barbed wire.

Pegatron, one of the major Shanghai-based suppliers of Apple parts, even makes workers swipe cards and look into cameras that use facial-recognition software before they can enter buildings. While the company claims that such measures are to protect intellectual property, it's clear that this high security also insulates the company from the bad press that the factory working conditions would doubtless generate.

> _Foxconn's 1.3 million employees surpass the total population of the Baltic country Estonia._

### 9. Final summary 

The key message in this book:

**While the iPhone may embody the pinnacle of modern technology, it's actually based on centuries of experimentation and innovation. Though this technological wonder has revolutionized modern life, its production has a number of negative effects on the people who assemble the iPhone as well as the workers who mine the metals from which it's made.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Steve Jobs_** **by Walter Isaacson**

This book chronicles the audacious, adventurous life of Steve Jobs, the innovative entrepreneur and eccentric founder of Apple. Drawing from Jobs's earliest experiences with spirituality and LSD to his pinnacle as worldwide tech icon, _Steve Jobs_ describes the man's successful ventures as well as the battles he fought along the way.
---

### Brian Merchant

Brian Merchant is a journalist of science and technology. His writing has appeared in the _Guardian_, _Slate_, _Fortune_ magazine and the _Los Angeles Times_, among many other publications. He's also an editor for Motherboard — the science and technology department of _VICE_.

