---
id: 543a3c683539360008000000
slug: the-one-minute-manager-en
published_date: 2014-10-13T00:00:00.000+00:00
author: Ken Blanchard and Spencer Johnson
title: The One Minute Manager
subtitle: Increase Productivity, Profits and your own Prosperity
main_color: 335D97
text_color: 335D97
---

# The One Minute Manager

_Increase Productivity, Profits and your own Prosperity_

**Ken Blanchard and Spencer Johnson**

With global sales of over 13 million, _The One Minute Manager_ is a classic that's still changing the workplace. It explains how managers can get outstanding results from their employees while spending as little time actively managing them as possible. A _one minute manage_ r needs just three simple tools to boost productivity — and transform their company.

---
### 1. What’s in it for me? Learn how to make employees happier and more productive in 60 seconds. 

Almost everyone has at one time or another had a horrible boss: someone who should not be in charge of a wall plug, let alone people!

But there's a reason why good managers are so hard to come by: it's no easy feat to manage employees so that they are satisfied and still deliver great results.

What's more, managers are usually swamped in work, which is why they need quick, effective tools to manage and motivate employees.

This is where _one minute management_ comes in: a management methodology where mere one-minute interactions with employees can make them both happy and productive.

In these blinks you'll discover

  * why employees are like whales;

  * how reprimanding someone is like an intervention for an alcoholic; and

  * why it should take no more than a minute to review an employee's performance goal.

### 2. It’s a people business: how managers can deliver top-notch performance by developing their employees. 

If you distilled the job of a manager down to its very essence, what would you say it was? Controlling a company's finances? Developing new products? Selling goods?

Actually, the answer lies in the job title: the main task of a manager is to _manage_ the company's employees.

The purpose of the company is to deliver good financial results, but those figures depend wholly on the efforts of the employees. The quality of their work is the main driver behind creating and selling high-quality products, and thus the company's success.

This makes the employees the most essential capital that a company can own, and so managers need to focus on maximizing their potential.

Unfortunately, this does not seem to be the case: even though companies typically spend up to 70 percent of their budget on salaries, most don't invest even _one percent_ in training their employees. This demonstrates a fundamental lack of understanding.

Most managers don't understand the connection between their employees and financial results, which is why they tend to fall into two camps:

First, there are managers who care greatly about the well-being of their employees, and unwittingly sacrifice the company's performance to keep them happy. These managers would never dare tell an employee that he or she is doing something wrong, and would rather take on almost all of the work themselves. In the long run, this ends in disaster.

Second, there are harsher managers who only care about the performance the company delivers, and are even willing to override any consideration for their employees to improve it. These managers are constantly telling their employees that they are not doing enough and need to improve, and this saps their motivation.

Of course, you really want to be at the cross-section of these two categories, where your employees feel good about working for you, but also deliver high-quality results.

So how can you get there? By taking just one minute to manage your employees! Read on to learn how this can be done, and how it will both increase company efficiency and save you time.

> Did you know that most companies spend the majority of their budget on employees' salaries?

### 3. The one minute goals: how to set employee goals that you can review in a minute. 

What can you do in one minute? Maybe drink a glass of water? Or call someone and leave them a brief voicemail? But surely not much more than that, right?

In fact, managers who know how to conserve their time can achieve a lot in 60 seconds.

How? By defining _one minute goals_ — specific goals for an employee, outlined in measurable performance metrics that fit on a single sheet of paper.

Sound too good to be true? This is how it works.

You and your employee will agree on goals for the employee, each of which is then written down in very brief form: 250 words or less. This way the goal is always readable in just one minute, and both you and the staffer retain your own copies for easy reference.

For example, let's say you want to define a one minute goal for someone on your sales staff. You could say that for the next quarter, the employee's goal is to increase sales by five percent. This is an easy goal to monitor.

Why do these goals work? Because they motivate people.

In most companies, employees don't really have a concrete set of responsibilities, so the only way they can gauge their own performance is through their manager's feedback. And this feedback is often negative, because managers tend to focus on mistakes.

But with one minute goals, the employees have concrete targets to strive for, and they can monitor for themselves how well they're doing. This will boost their motivation.

Of course, even if each goal only takes up one sheet of paper, you could still end up drowning in paperwork if you set dozens of goals.

This is why you need to adhere to the _80–20 goal-setting rule_. Around 80 percent of an employee's work is aimed at achieving just 20 percent of her goals, so you just need to define those 20 percent of goals, meaning three to six concrete achievements at the most.

> Did you know that most of the work an employee does concentrates on just 20 percent of his work goals?

### 4. The one minute praise: how to give your employees positive feedback for great work. 

In most companies, employees only ever get negative feedback because their managers only notice what's happening when they've done something wrong. This is frustrating and saps employees' motivation, as they feel they can never make their manager happy.

To avoid this, you should get into the habit of giving _one minute praise_ where you express your satisfaction with something the employee has done.

To be effective, this praise should be specific and come right after the act that warranted it.

For example, you might say something like, "Johnny, you did great on that presentation yesterday, it was amazingly detailed! It made me feel really proud!"

You may even want to accompany your praise with a physical gesture like a pat on the back, but of course that depends on the situation and the employee.

One minute praise shows the employees that they are valued and appreciated, and it also demonstrates the level of expectations you have for them. This is especially important for recent hires, which is why managers should really try to find something praiseworthy in their work during their first weeks with you.

However it's important for you to tell employees beforehand that you will be reviewing their work, so that they don't feel you've "spied" on them.

So why is one minute praise so effective?

Employees see that someone cares about their work, and this encourages them to try even harder next time, eventually leading to stellar performances.

This is conceptually similar to the way killer whales are trained to jump out of the water at marine parks: They're first rewarded for accomplishing something simple, like swimming over a rope floating in the pool. Then the rope is gradually raised higher until the whales have to jump over it to get their reward.

This works, but if the rope had been placed high over the water from the start, the whale would never have jumped over it. It needed the small accomplishments — the one minute praises — to motivate itself to jump high. The same is true of people.

### 5. The one minute reprimand: how to show your dissatisfaction while still valuing employees. 

Finally, just as it takes only a minute to praise someone, it need not take more than a minute to reprimand them for a mistake.

So when you're dealing with more experienced employees, don't be afraid to use the _one minute reprimand_. These seasoned employees are already able to interpret the one minute goals and praise so they know what is expected of them and if they're doing well. But when they make mistakes or lag behind in their work, you need to give them a one minute reprimand to convey your dissatisfaction.

Similar to the one minute praise, the one minute reprimand should come right after the mistake or misbehavior, and it should explain exactly what went wrong and how it makes you feel as the manager.

However, there should be no acrimony or hard feelings after the reprimand. This is why you also need to include a bit of praise to ensure that the employee knows that though their behavior was unacceptable, they are still valued.

For example, you might say something like "Johnny, that sales pitch was sloppy and unpersuasive. I'm really disappointed in you. But you're still a very valuable employee, so keep up the good work on your other projects."

Again, it helps avoid any hard feelings if you've told the employees beforehand that you intend to review their work.

So why are one minute reprimands so effective? Because the employees feel that their mistakes are being treated fairly.

For one, the reprimand comes right after the mistake, which means neither party has the chance to let any hostility fester. Instead, things are talked through and the air cleared immediately.

Second, the modicum of praise included in the one minute reprimand reminds the employee of their own worth, thus maintaining their motivation.

This is similar to a family intervention for an alcoholic: the intervention participants don't just berate the alcoholic for ruining their lives, but also emphasize that he or she is loved and cared for as a person.

> _"Unless discipline occurs as close to the misbehavior as possible, it tends not to be as helpful in influencing future behavior."_

### 6. Final summary 

The key message in this book:

**Good managers should concentrate on developing the full potential of their people, as it is the best way to improve the company's performance. To do this effectively, managers should use three time-saving management tools: one minute goals, one minute praise and one minute reprimands.**

Actionable advice:

**Onboarding new employees.**

The next time you make a new hire, use the one minute manager's tools. Start by defining one minute performance goals, and then explaining that you will be observing and reviewing the employee's work to assess their performance. Then when reviewing their work, try to find something — anything — they did right or at least approximately right, and give them some one minute praise. It will boost their motivation and encourage them to try even harder next time.

**Suggested** **further** **reading:** ** _The Seven Habits of Highly Effective People_** **by Stephen Covey**

_The Seven Habits of Highly Effective People_ introduces the habits which single out people who deal particularly effectively with the world around them. The author believes that people who lead successful and fulfilling lives do not pursue the state of individual independence as their ultimate goal, but instead align themselves internally with universal principles such as honesty and integrity.
---

### Ken Blanchard and Spencer Johnson

Ken Blanchard is business consultant, speaker and bestselling author, as well as the Chief Spiritual Officer of The Ken Blanchard Companies, who have coached multinational giants from Shell to Merck. Spencer Johnson, MD, is the author of _New York Times_ chart-topper _Who Moved My Cheese?_ and a former Leadership Fellow at the Harvard Business School.

