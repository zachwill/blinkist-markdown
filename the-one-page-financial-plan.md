---
id: 566ebbd4ddd17a0007000003
slug: the-one-page-financial-plan-en
published_date: 2015-12-14T00:00:00.000+00:00
author: Carl Richards
title: The One-Page Financial Plan
subtitle: A Simple Way to Be Smart About Your Money
main_color: E57D47
text_color: 99542F
---

# The One-Page Financial Plan

_A Simple Way to Be Smart About Your Money_

**Carl Richards**

_The One-Page Financial Plan_ (2015) makes budgeting easy: once you know why money matters to you, it's just a matter of making sure you've got enough to do what you want with it, whether it's creating a stable income or saving for the future. This simple planning solution will give you all the tools, tips and tricks you need to realize your financial dreams.

---
### 1. What’s in it for me? Save money with a personal financial plan. 

Even if you're not a dollar-crazed investor or a gambler, you probably think about money more than you'd like to admit. There's rent to pay, kids to feed and suddenly your car needs an expensive repair. And at the end of the month there is nothing left over to do the things you want to do. 

What you need is a financial plan tailored to your life and needs. It might sound scary or boring, but, as these blinks will show you, making your own personal financial plan will not only take the stress out of your life, it can also be a lot of fun. And when you notice that a few simple changes will free up money to do the things that you truly value, you'll find it easy to stick to the program and make even more changes.

In these blinks, you'll learn

  * how to turn budgeting into a game;

  * why you should give your Amazon shopping cart a break; and

  * why you shouldn't rely on CNBC for investment advice.

### 2. First you have to discover why money is important to you. 

The foundation of every successful financial plan is the answer to a simple question: _Why_ is money really important for me? Take a moment and think about this question before reading on, and write down your answer at the top of a piece of paper. You'll need it later.

Asking why money is important to you helps you to define what you value, which in turn will help you develop a financial plan that fits your individual needs. 

As an example, think about the last time you went to the doctor. Your doctor doesn't give every patient the same medication. They first need to ask what's wrong in order to determine a fitting treatment.

The same goes for financial plans. Knowing why you value money will aid in the examination of your financial situation. Only then can you develop a "treatment" that will work for you.

Someone who wants to save money to travel the world and someone who wants to have a secured pension will likely have financial plans that look totally different from one another. Whereas one might want to invest in an RV, the other might want to invest in a good pension fund.

Knowing why you value money also helps you assess whether the way you spend your time reflects your values. Imagine that you value money because it gives you the financial freedom to spend more time with your family.

Now imagine that you run an independent business and spend all weekend on Twitter trying to increase your followers to grow your personal brand. You spend so much time on Twitter that you interrupt dinnertime conversations with your family to reply to tweets and check your followers. Are you _really_ living according to your values?

Now that you've determined why money is important to you, it's time to formulate some goals to guide your financial future.

### 3. Guess what your goals are and fine-tune to reach them. 

Think back 20 years: would you ever have predicted that free apps like Skype or WhatsApp would cause your phone bills to plummet? The future is unpredictable. Your financial plan, and the goals that support it, should be flexible enough to cope with this unpredictability. 

The first step in developing your goals is accepting that it's not always possible to predict the future. You can think of financial planning like planning your vacation. Even though you set some goals, like places to see or things to do, you should leave some room for the unexpected. For example, you can't enjoy a bike ride and a picnic if there's a storm outside. Why not alter your plan and visit a museum instead? 

It's the same with financial planning. Your goals aren't set in stone, so be willing to alter them if you ever get off track. 

Let's say that one of your goals is to pay off your $40,000 student loan in three years. Suddenly, there's a financial crisis that leaves you unable to save as much as you planned. Instead of sticking to your previous plan, you could readjust and settle on paying off $28,000 in the same amount of time.

Once you've come to terms with the fact that the future is unpredictable, it's time to start writing down some goals. But what should your goals be?

Imagine this was your response to the _why_ question: "Money is important because it will help me give my kids the best opportunities and keep them safe."

Some of your goals could be things like:

  * In two years I want to have set aside enough money to co-finance my son's studies

  * Next year I'd like to send my daughter on a three-month foreign exchange trip 

  * I want to set up some emergency funds in case my kids don't find a job right out of college

No matter what your goals are, remember: you can always adapt them as your situation changes.

### 4. Assess your current financial situation by making a simple balance sheet. 

Your values and goals serve as your guide no matter where you want to be financially. However, before you can actually make any decisions about where to take action, you first have to understand your current situation. 

Having a clear understanding of your assets and liabilities will help you take the necessary steps to reach your goals. 

But imagine that you don't know — or don't want to know — your exact financial situation. Without this information, how can you possibly know what you need to do to get where you want to be?

To discern your current financial situation, make a simple balance sheet. Draw a T shape on a piece of paper, putting your assets, such as your investments and savings, on the left, and on the right your liabilities, such as your mortgage and debts. Simply subtract your liabilities from your assets to figure out your net worth. Write that above the T.

Once everything has been laid out on the table, you'll have a clear view of where you stand and can determine your first steps. For example, if you realize that you owe $5,000 in student loans, your first action might be to make a plan to pay them off. 

Aside from being helpful in determining next steps, understanding your financial situation can relieve your financial anxiety and make it possible to take action.

Imagine not being able to pay your mortgage, and hesitating to call your creditors for weeks. But once you finally call them, you discover how much you owe and are able to make a reasonable plan about how to move forward.

Or you might realize that you still owe lots of money on a car you hardly ever use. Selling it could help you inch closer to freedom from debt. 

Now that you understand the basics of setting up a financial plan, the following blinks will offer practical tips on how to spend and save money efficiently in order to free up resources to invest intelligently and achieve your goals.

### 5. Track what you spend using budgeting. 

In essence, budgeting is about taking a good look at how you spend money and making adjustments in order to achieve your financial goals. To budget well, it's crucial that you understand where you're spending and whether your spending is in line with your goals and your values. 

Many of us think that budgeting and tracking expenses is tedious and boring, more like a punishment for those who lack discipline than a tool of the financially successful. In reality, financial success is only really possible if you budget well, tracking how you spend your money, measuring that against your goals and values and adjusting accordingly. 

For instance, if one of your goals is to travel more but you spend 30 percent of your income on partying and fine dining, financing your trip to Vietnam is going to be difficult. You'll have to make some changes to turn this dream into a reality.

Begin by making a list of your fixed monthly expenses, looking for places to cut or shift costs.

Try looking at it as a game or challenge.

Try to make a conscious effort to spend less money. Instead of using public transport, take your bike to work. Instead of eating out for lunch, pack sandwiches. Instead of going to the movies, take a walk through your neighborhood and discover places you've never paid attention to before. Once every two months, tot up how many days you were successful.

Another challenge you could try is to make as few transactions as possible in a week. If you have a spouse or partner, you can even compete against each other. If your vice is impulse-buying books on Amazon, then next time you fill your virtual cart, leave it for a few days and then check if you really want those books after all.

Not only are these challenges fun, they also help you to realign your spending habits and make clear which things you _really_ need to spend your money on and which things are superfluous.

### 6. Save as much as you can and view paying off debts as a form of investment. 

Contrary to popular wisdom, rules of thumb like saving such-and-such a percentage of your income each month aren't all that useful in achieving your financial goals. In the same way that your financial plan is personal, your savings plan should be too. The goal should be to save as much as _you_ possibly can — an amount that will depend on your unique situation.

Once you've determined how much you can save, you can make life a lot simpler by automating the process. Let's say you can afford to save $300 a month. By automating the transaction, you get to spend less time thinking about whether to spend or save. You might eventually forget about it altogether, and be pleasantly surprised that your savings have grown years later. 

In addition to saving, you should be paying off your debts, starting with the one with the highest interest rate.

Paying off debts is essentially an investment in your financial future. If you're racking up debt on your credit card for things that don't align with your goals, then you're ensuring that you'll be busy paying interest on your debts down the line rather than saving for what you actually want to do.

In essence, you're saying "no" to the goals you set and "yes" to something else. If this "something else" is really important, you might want to revise your goals.

You've now identified where your money is coming from, how you spend it, and you have a plan to pay off your debts. The final blink will look at the last step in your financial plan: investments.

### 7. Make your investments like a scientist and diversify. 

There are innumerable businesses, initiatives and institutions that promise a return on investment if you fork out some money. So how should you know where to invest? Should you follow the advice of your friends, family or authoritative-sounding finance people on CNBC?

You could, but then you'd be _speculating_ and not really _investing_, i.e., making investment decisions with your gut rather than your head. To make the best investments, you need to look at investment as a science. 

A good way to avoid financial mistakes and risk is by consulting academic journals and other peer-reviewed publications about investing. People have been trying to make their money grow for a long time, and experts have been studying investment for years. Predicting how a stock will perform in the future is a complex business, certainly not one that intuition alone will help you decipher. 

Before you make any investments, study them as if you were a scientist. Otherwise you might make decisions that you later regret. 

In addition to treating investment like a science, it's important to mitigate risk by diversifying your portfolio, or spreading it over many different types of stocks.

No single stock is going to be your golden ticket to wealth, and if such a stock exists, you can't count on finding it. How many of us could have predicted that Facebook would become this big when it first went public? How many of us could have predicted the financial crisis of 2008?

Instead of trying to find that miracle stock, you should instead spread your risk over as many sectors as possible. While some of them might lose value, others might appreciate, helping to balance out any potential losses. 

Your portfolio should be quite mixed, including national and international companies, big and small in many different sectors. This way, you have the best chance of turning a profit and reaching your financial goals.

### 8. Final summary 

The key message in this book:

**A financial plan begins when you ask yourself** ** _why_** **money is important to you and create goals that reflect your answer. After that, it's a simple matter of assessing your spending, savings and investment and making adjustments that put you on track to achieve those goals.**

Actionable advice:

**Talk about finances with your partner.**

If you share finances with a spouse or a partner it's important for them to participate in financial planning in order to see which of your values overlap and which are incompatible. This way, the two of you can work toward a financial future that's a win–win for everyone. 

**Suggested** **further** **reading:** ** _Young Money_** **by Kevin Roose**

Kevin Roose spent three years following eight young Wall Streeters in an attempt to find how the 2007 crash — and its aftermath — influenced the financial industry. _Young Money_ paints a decidedly grim picture of junior analysts who find themselves on a non-stop rollercoaster of all-nighters and extreme stress, while earning six-figure incomes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Carl Richards

Carl Richards is a leading financial advisor and contributor to Yahoo Finance as well as a weekly columnist at the _New York Times_. He is also the author of the critically acclaimed _The Behavior Gap_.

