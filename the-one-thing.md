---
id: 52934d2d6231360018110000
slug: the-one-thing-en
published_date: 2013-11-27T08:59:18.000+00:00
author: Gary Keller and Jay Papasan
title: The ONE Thing
subtitle: The surprisingly simple truth behind extraordinary results
main_color: 9E2027
text_color: 9E2027
---

# The ONE Thing

_The surprisingly simple truth behind extraordinary results_

**Gary Keller and Jay Papasan**

_The ONE Thing_ (2013) helps you to discover your most important goal, and gives you tools to ensure you can use your time productively to get there. The book reveals that many of the maxims we accept as good practice are actually myths that only hinder our progress. It also provides advice on how to live your life with priority, purpose and productivity without sending other aspects of life out of balance, because this is the way to perform the kind of focused work that leads to great success.

---
### 1. Failing to think big can limit your opportunities. 

When Arthur Guinness set up his first brewery, he clearly had grander plans than producing a few barrels of stout: he signed a 9,000 year lease on the building. Similarly, when J. K. Rowling conceived the idea of Harry Potter, she envisioned seven books about life at Hogwarts before she wrote even the first chapter of the first one.

Both of these people went on to be extraordinarily successful, and this was in no small part because they were not afraid of _thinking big_ : starting out with a grand vision of success before even beginning to work toward it. It's hard to imagine that they would have achieved such great success had they failed to think so big in the first place. 

Yet, for most people, the thought of big ideas or big achievements is daunting and has negative associations, such as feeling _overwhelmed_ and _intimidated_. These negative thoughts often prevent people from thinking big.

When we fail to think big and allow these negative associations to dominate us, our thinking shrinks and we lower our trajectories. We actively limit our potential achievement, condemning ourselves to mediocrity.

Consider science and how much of its progress would have stalled if someone hadn't dared to think of previously unimaginable possibilities, like that humans could breathe underwater, fly through the air or explore space. History tells us that we've done a remarkably poor job of estimating our limits, so we should not let the limits we perceive constrain our aspirations.

Success requires action, and action requires thought. But in order to achieve extraordinary results, our actions have to be based on big thinking in the first place.

**Failing to think big can limit your opportunities.**

> _"Don't let small thinking cut your life down to size. Think big, aim high, act bold."_

### 2. Prioritize your to-dos – they are not all equally important. 

Most people, from time to time, make "to-do" lists to keep track of all the tasks they have to complete. But once you have your list, how do you decide which item to work on first?

Do you start with the most time-consuming ones, or get smaller tasks checked off first? Maybe you just work through them in the order they were written?

These approaches fail to address a key point: all items are not equally important.

In fact, it is likely that only a few of them will have a profound impact, and these should therefore be given the highest priority.

This conclusion can be drawn from the work of Joseph M. Juran, a pioneer of quality-control management. Whilst working for General Motors, he discovered that a majority of the defects in their cars came from only a handful of production flaws. It was clear that fixing these flaws should be their highest priority.

Juran named his finding the Pareto Principle, after an Italian economist, Vilfredo Pareto, who wrote a model for wealth and income distribution in nineteenth-century Italy. In the model, Pareto showed that 80 percent of the land was owned by 20 percent of the people. Juran had noted that these proportions matched his own: 80 percent of the defects came from just 20 percent of the flaws.

Juran realized that this 80/20 principle may in fact be a universal law: 80 percent of your results or outputs are always delivered by 20 percent of your work or inputs.

The implications of this principle are clear: the tasks on your to-do list are not equally important; just a small number of them will make the greatest contribution to your success. Prioritize your tasks to focus on the ones that will achieve the greatest proportion of your results.

**Prioritize your to-dos — they are not all equally important.**

### 3. Asking the “focusing question” will help you to prioritize, create actionable tasks and achieve your goals. 

On the subject of success, Mark Twain once said,

"The secret of getting ahead is getting started. The secret of getting started is breaking your complex overwhelming tasks into small manageable tasks, and starting on the first one."

This is great advice, but knowing where you want to go and what the first task should be to get there can be difficult. This conundrum is exactly where it helps to ask the _focusing question_, a question specifically designed to help you identify both where you want to go and how you can get started on your journey:

"What's the ONE thing I can do, such that by doing it everything else will become easier or unnecessary?"

This question can be asked on two levels, each serving its own function:

First, on a macro level the focusing question can help you to see the _big picture_ and identify your overall goal: _the ONE thing you want to do and achieve in life_. For example, here your ONE thing could be your overall career goal.

Second, on a more practical, short-term level, the focusing question provides you with a _small focus_ to prioritize your immediate options and select the most effective task to start with. Here you are looking for _the ONE thing you can do right now_ ; for example, "Make that phone call."

The first level is about finding the right direction in life; the second is about choosing the right action.

Repeatedly asking yourself the focusing question will not only keep you aimed at your goal but will provide you with actionable steps that build on each other, creating progress and momentum. Keep asking it, and who knows what you can achieve?

**Asking the "focusing question" will help you to prioritize, create actionable tasks and achieve your goals.**

### 4. The secret to a disciplined life is sequential habit forming. 

When we think of someone as successful as Bill Gates, we tend to attribute his phenomenal success to the extraordinary self-discipline that allowed him to learn how to program computers in his formative years. This level of discipline seems like a daunting unachievable goal. How do successful people maintain such discipline?

On closer inspection, we see that the key to their success is not so much to constantly apply a tremendous amount of discipline in order to stay focused and driven but, instead, to use discipline selectively to form enduring good habits.

The success story of Michael Phelps is a revealing example. He is widely admired for his focus and discipline, but in fact as a child he was diagnosed with attention deficit hyperactivity disorder. It was thought that he would never be able to focus on anything.

So how did he turn the tables?

He channeled all of the discipline he could muster into forming one habit: swimming every day. For almost a decade — from the age of 14 through to the Beijing Olympics — he trained seven days a week, 365 days a year.

One habit is just the start. Habits are much easier to maintain than to begin, so once something becomes a habit, you can shift your discipline into forming a new one and then build them up sequentially. For example, you could start by getting into the office 30 minutes earlier every day to clear your inbox before your colleagues arrive. Once this habit is established, you can build on that by channeling your discipline into staying focused on one particular task for longer periods. Once this habit has been formed, you can move on to the next one.

Building up positive habits by selectively applying discipline will give you the appearance and benefits of a disciplined life, without the need for super-human discipline.

**The secret to a disciplined life is sequential habit forming.**

### 5. Multitasking is horribly inefficient: pick one thing and give it your undivided attention. 

Nowadays, it is generally accepted that _multitasking_ is an effective thing to do. We generally understand the term as meaning: to do two or more things _simultaneously_. But it was originally coined to describe a computer using a single processor to work on multiple tasks, _alternating_ back and forth between them in rapid succession. A distinction which, as it happens, is very revealing.

Although we can do some things at the same time — for example, walking and talking on the phone — what we can't do is effectively _focus_ on two tasks at the same time. This means that frequently, when we think we are multitasking, we are actually juggling two or more tasks, switching focus from one to the other just as a computer does.

Research has shown that, for humans, such task switching exacts a time penalty, as it takes time to move from one task and then refocus on another. This time cost may be small in the case of relatively simple tasks, but it increases greatly when the task you are returning to is more complex.

For example, if you are working on a complicated spreadsheet and a co-worker interrupts you to discuss a tricky business problem, time will be lost when you switch back to the spreadsheet and struggle to remember where you were in the process and what you were trying to achieve.

During the day, these time penalties soon add up, particularly in the work environment. It has been estimated that, on average, office workers are distracted every 11 minutes, and spend up to a third of the working day recovering from these distractions. Can you really afford to lose a third of your working day?

Figure out what matters most in the moment and give it your undivided attention.

**Multitasking is horribly inefficient: pick one thing and give it your undivided attention.**

> _"It is those who concentrate on but one thing at a time who advance in this world."_

### 6. Your willpower is like a fuel tank: choose carefully where you use it or you may run out when you really need it. 

Most people are painfully aware that they do not have ironclad willpower. What is surprising, however, is that research has shown that our willpower, far from being a constant resource, actually drains at varying rates throughout the day, depending on what activities we engage in.

For example, our willpower is depleted when we make decisions to focus our attention, suppress our emotions or modify our behavior in pursuit of a goal. When our willpower has been drained, we are less able to exert it should further tasks call upon this resource.

This would explain why you may be unable to resist a tasty snack after a period of making tough decisions or being engaged in tedious work.

Giving in to a guilty pleasure is one thing, but if you are making life and death decisions when your willpower is low, the consequences are potentially much more serious.

For prisoners, few decisions are as important as the ruling at the next parole-board hearing. Could decisions of such gravity be influenced by something as arbitrary as the time of day?

Research involving Israeli parole judges shows that they could: judges were much more likely to give favorable judgments at the start of a parole hearing than towards the end. This is because the judges tended to rely on the default decision of "no parole" as the day wore on and their willpower ran low. The rate of favorable judgments picked up again after breaks and a snack.

Full-strength willpower requires a full tank, so plan your day so that you can avoid making key decisions or judgments when you're running low.

**Your willpower is like a fuel tank: choose carefully where you use it or you may run out when you really need it.**

### 7. Saying no to unimportant tasks is vital if you are to focus your efforts on the most important ones. 

We all struggle at one time or another with saying no to requests, because we want to be helpful. Helping others can be deeply rewarding, but, in order to preserve your time and energy for your biggest goals, you need to say no to lower-priority requests.

Steve Jobs was famously as proud of the projects he didn't pursue as the ones he did. When he returned to Apple in 1997, he reduced the company's output from 350 products down to just ten. That's a lot of no's. At a developer's conference in 1997, he explained, "When you think about focusing, you think 'Well, focusing is about saying yes.' No! Focusing is about saying no."

Saying no to people all the time does not have to be as cold or as selfish as it may appear. You can always try to give them an alternative solution that doesn't require your assistance, or redirect them to someone who can be more helpful.

Also, think about implementing strategies that will cut down on the requests you get; for example, by asking that staff refer to a list of frequently asked questions before approaching you. This may help, but remember: sometimes you will still have to actually turn people down if you are to succeed.

With limited time and resources, you must be prepared to say no to trivial tasks if you want to focus your energy and get the most important ones done.

**Saying "no" to unimportant tasks is vital if you are to focus your efforts on the most important ones.**

### 8. Living with purpose and visualizing the steps to get to your goal will set you on the path to extraordinary results. 

Imagine for a minute that you currently have no concrete goals or ambitions whatsoever. Adrift like this, how would you decide what to do each day? Do you think you would persevere at a difficult and tedious task without knowing why you're doing it?

When we imagine the above scenario, we realize how important it is to have a goal to work toward. It gives your life added meaning and purpose, which leads to greater clarity in your thoughts, more conviction in your actions, and faster decisions. Most importantly, though, knowing why you're doing what you're doing provides inspiration and motivation when problems occur.

With a goal in place, you can start planning the steps to reach it, but it's even better if you also visualize each step along the way, as this motivates you and prepares you for the road ahead. These beneficial effects were shown in a study of students, who, when taking a test, were asked to visualize either the outcome of the test or the process of preparing for and taking it. The students who visualized the process reported higher levels of motivation, were better prepared and subsequently got better results.

Recall the image of being so adrift at the start of this blink? Now imagine that you have found your calling: you want to climb Mount Everest. Your goal is suddenly clear: it's _the ONE thing_ you want to do. You now need to do some research, start training and get the right equipment. Thanks to your goal, you're already making plans, visualizing the steps and progressing toward it.

Defining a goal — the ONE thing we want to do and achieve in life — is something we should all strive toward.

**Living with purpose and visualizing the steps to get to your goal will set you on the path to extraordinary results.**

> _"Success demands singleness of purpose."_

### 9. Never compromise your personal life for your professional goals – instead, prioritize your work time ruthlessly. 

We all strive to achieve a balanced life — to divide our time equally between all of the things that matter to us — but we do this without stopping to ask why, and what it is that we are actually trying to achieve.

It's understandable that we strive for balance because all of the demands in our work life and personal life seem so important, but in fact this vision of a balanced life is unobtainable and undesirable. If you try to do everything, you will end up short-changing everything you do, in both your personal and your professional life.

Author James Patterson encapsulated this dilemma when he said: "Imagine life is a game in which you are juggling five balls. They are called work, family, health, friends and integrity. And you're keeping them all in the air. But one day you finally come to understand that the work ball is made of rubber — if you drop it, it will bounce back. The other four balls are made of glass."

As the quote illustrates, we should never sacrifice priorities in our personal life when the pressure is on at work. The damage done may be irreparable. But if our personal life always takes precedence, how can we succeed professionally?

The trick is to prioritize your work time ruthlessly to focus on professional goals. In your personal life, neglecting any area can be perilous; but in your work life, you have your top priority and everything else is negotiable. From time to time, lesser priorities will have to be minimized, be made to wait, or be managed by someone else until what really matters is done. This approach will allow you to focus on your most important work, giving it the concentration that is required to achieve great results.

**Never compromise your personal life for your professional goals — instead, prioritize your work time ruthlessly.**

### 10. To focus on your ONE thing, you need effective time-management strategies, and to accept some chaos in other areas. 

Let's assume you have discovered your ONE thing, the key priority you need to achieve, and you have a clear plan of the steps needed to reach your goal.

You're all set to conquer the world but with only one small problem: life does not have a pause button. While you are beavering away, working on your masterpiece, the world does not patiently wait for you to finish. Things stack up. There will always be other people and projects that demand your attention.

Imagine you are working on landing a big contract. You'll need to make sacrifices. Your regular work is going to pile up or be delegated to colleagues.

As the chaos builds up in other areas, so does the pressure to attend to them. Learn to deal with this by trusting that the work you are doing on your top priority will come through for you, and in doing so will simplify other areas of your life. In short, let the chaos pile up.

So now that you're focusing on your ONE thing, how do you make the most of your time?

Schedule blocks of time to work on your ONE thing, commit to them and defend them like they are your most important appointments. You also need to ensure that your physical environment doesn't prevent you from using this time effectively. Wherever you work, you need to minimize potential distractions. Consider working away from your office if this is not possible.

Techniques such as these allow you to give your ONE thing the attention it deserves.

**To focus on your ONE thing, you need effective time-management strategies, and to accept some chaos in other areas.**

### 11. Final summary 

The main message in this book is:

**Success comes from focusing on ONE thing, not many things. When working toward your ONE thing, avoid the pitfalls that prevent you from achieving success. Learn how to cut through the clutter and do your best work where it really counts.**

This book in blinks answered the following questions:

**How should I set goals and prioritize my work?**

  * Failing to think big can limit your opportunities.

  * Prioritize your to-dos — they are not all equally important.

  * Asking the "focusing question" will help you to prioritize, create actionable tasks and achieve your goals.

**How do I avoid being diverted from my ONE thing?**

  * The secret to a disciplined life is sequential habit forming.

  * Multitasking is horribly inefficient: pick one thing and give it your undivided attention.

  * Your willpower is like a fuel tank: choose carefully where you use it or you may run out when you really need it.

  * Saying "no" to unimportant tasks is vital if you are to focus your efforts on the most important ones.

**How do I achieve great results in pursuing my ONE thing?**

  * Living with purpose and visualizing the steps to get to your goal will set you on the path to extraordinary results.

  * Never compromise your personal life for your professional goals — instead, prioritize your work time ruthlessly.

  * To focus on your ONE thing, you need to use effective time-management strategies, and accept some chaos in other areas.

### 12. Actionable advice 

**Actionable ideas from this book in blinks**

**Prioritize your to-do lists –** If you want to work much more productively, realize that not all of the tasks on your to-do list matter equally; some will provide a greater proportion of your results than others. With this in mind, you should always try to prioritize the ones that are likely to contribute greatest to your success, and then work on these highest-priority tasks first.

**Ask yourself the focusing question at the start of everyday –** Asking yourself the focusing question — "What's the ONE thing I can do right now, such that by doing it, everything else will be easier or unnecessary?" — on a regular basis will help to keep you focused on your goal, help you to prioritize your tasks, and simplify your life.

**Stop multitasking –** You cannot focus effectively on two or more things at the same time. When we try to multi-task, what we are really doing is switching our focus between the tasks, which comes at a cost. We are more likely to make mistakes and work less efficiently. Decide what the most important thing in the moment is, and give it your undivided attention.
---

### Gary Keller and Jay Papasan

Gary Keller is co-founder and chairman of the board of Keller Williams Realty International, a company he grew from a small office in Austin, Texas, to become the largest real estate company in the US. His previous three books formed the Millionaire Real Estate Series and have all been bestsellers.

Jay Papasan is the executive editor and vice-president of publishing at Keller Williams Realty, and is the president of Rellek Publishing. He has also co-authored numerous bestselling books, including the Millionaire Real Estate Series.

