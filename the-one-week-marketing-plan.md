---
id: 55d0fc75688f67000900007f
slug: the-one-week-marketing-plan-en
published_date: 2015-08-20T00:00:00.000+00:00
author: Mark Satterfield
title: The One Week Marketing Plan
subtitle: The Set It & Forget It Approach For Quickly Growing Your Business
main_color: C03D3D
text_color: B92727
---

# The One Week Marketing Plan

_The Set It & Forget It Approach For Quickly Growing Your Business_

**Mark Satterfield**

Many people think that marketing is difficult, requires tons of time, loads of money and lots of powerful connections, but _The One Week Marketing Plan_ (2014) debunks this myth. These blinks will walk the reader through the process of building a custom-made, cheap and powerful marketing strategy for the information age.

---
### 1. What’s in it for me? Set up and run your own marketing campaign in just one week. 

Every entrepreneur knows that marketing can make the difference between success or failure. But it's such a daunting task. Gurus, social media, advertising — it's enough to get lost in.

But there's another way to approach marketing. Just strip it down to its essentials, give people what they want and win more customers. That's not so complicated, right? 

Right. In fact, you can do it in just five days. These blinks will show you how.

After reading these blinks, you'll know

  * how to use pay-per-click advertising to your advantage;

  * an easy way to find your niche; and

  * why direct mail isn't dead yet.

### 2. Day 1: Define your niche market. 

The first day of designing your business's marketing plan should focus on finding your niche. While niche marketing may appear to limit your options, your specialization will pay off when you're standing out from the competition. 

Why's finding a niche essential?

First of all, it provides visibility. To get new customers you first need to get their attention, and it can help to focus on an area of expertise. By building a niche you send a particular message to a particular target with particular needs. Put yourself in your customers' shoes. For instance, imagine you're a Silicon Valley all-star who, after selling your stock options, is desperately searching for a way to avoid getting reamed by the IRS. You wouldn't choose a general advice book like "Ten Ways to Save Money on Your Taxes" but rather one that suits your specific needs, like "Ten Typical Mistakes You Can Make with Your Stock-option Profits". 

But that's not the only reason a niche is helpful. A niche can also improve your business. Consider JDC Repair, a phone repair chain. They started servicing only Apple products and doubled their sales within a year. How?

The niche route they took made them more credible as Apple specialists. But not just that, their employees also worked faster as they only had to repair one product. In short, they became experts in their niche. 

So a niche is helpful, but how do you find yours?

Your niche could be an industry — for instance, you might only target companies in a particular field. But it could also be a function, meaning your business targets a particular _audience_ regardless of industry. For example, you might offer a course on public speaking for executives from all fields. In this case, public speaking training is your niche function, but there's no limit to your audience.

Start finding your niche by asking yourself questions like: Are there any common industries or functions between my current customers? Do some customers spend more money with me than others? Are some easier to sell to? By following the answers to these questions you'll find your niche!

### 3. Day 2: Convert customers by offering something for free. 

On day 2 you should give something away. Why?

Because in a world dominated by low prices, offering a free gift will put you ahead of the competition. Start by deciding _what_ to give away.

Take Sonny Ahuja, a perfume retailer. He noticed that his customers' primary concern was receiving fake products, so he released a free report called "20 Ways to Spot a Fake Perfume." The gift brought tons of new traffic to his website!

Once you've decided what _your_ gift is, it's crucial to package it in a clever way.

For example, say your offer is also a report. Start by giving it a title that sums up the issue while grabbing the reader's interest. A good strategy is to debunk something many take for granted. For instance, if you're in insurance your title might be "The #1 Health Insurance Myth: The More You Pay, the Better Your Coverage." Or it could take the form of a tutorial, like "Seven Steps for Developing Leaders to Take Your Company Further."

But clever packaging isn't enough. You'll also need to ensure the content of your offer is relevant. When writing a report to give away you should start by choosing a topic that's not only interesting today but will be for years to come. In short, avoid fads and outdated content. 

Instead, consider basic human emotions. When choosing a topic, focus on things that your customers are particularly curious about or afraid of.

Now that you've outlined your idea you're ready to flesh out the content. You'll definitely need to deliver some valuable information that converts your readers into customers but doesn't share too much. Your aim is for your readers to be satisfied but wanting more — and willing to pay for it! So tell them what to do and what not to do, but hold out on _how_ to do it.

### 4. Build a website to deliver your free offer on day 3 and begin marketing on day 4. 

So you've developed a free offer. Day 3 is about building a website that attracts visitors when they subscribe to your gift. In this way you can stay in touch with potential customers. Begin by building a page on your website that advertises your free offer and make sure it entices visitors to sign up by giving their contact information. 

How?

Start with a header that hooks visitors. The title of your free report will do the trick as long as it's catchy enough. Something like "Seven Secrets to Buying Homes for Bargain Prices in Any Area." Once you've got a catchy title, lay out four to 12 points that describe what you're sharing. Keep in mind that your biggest allies in this task are curiosity, fear, pain and gain. Use them to arouse the interest of your visitors.

Lastly, be sure to give visitors the option to sign up for your offer and remember, if you ask for too much information fewer people will sign up. 

Once you've started registering contacts spend day 4 launching what's called a _drip-marketing campaign_, a series of messages sent over a short period. This technique will develop trust and credibility while engraining the name of your business in the minds of your subscribers. 

To achieve this, these messages should show your expertise and personality, communicating that you're both competent and a pleasure to work with. 

While writing to a big group can seem difficult, you can make it simple with a few tricks. For instance, try writing with just one person in mind. Just picture a person you know who resembles your ideal customer and write directly to him. 

You can also make this process easier by using an autoresponder, a tool to collect the data of subscribers and automatically send emails at set times. For instance, you could pre-write two years of messages timed to be sent at a rate of one every ten days.

### 5. Day 5: Increase your website traffic and start advertising. 

Once your website is set up, spend day 5 adding advertising to your marketing system and winning new visitors and customers. 

How? With _pay-per-click_, or PPC advertising. Here's how it works:

PPC ads are internet advertisements that you only pay for when they're clicked on. There are plenty of sites that offer PPC advertising deals like Bing, Facebook and LinkedIn, but the best is Google. 

Here's how to get started.

Create a Google AdWords account and choose keywords for your advertisements. Good keywords are essential because only users whose search criteria include _your words_ will see _your ads_. So when choosing your keywords put yourself in your customers' shoes. What would they type in a search box? 

Once your account is set up and your keywords chosen it's time to write your ad! The good news is it's easy to make your ad relevant as Google will issue it a quality score. This means the more appropriately Google feels your ad corresponds to people's search criteria, the better positioned it will be. For example, say you post an ad for "striped socks." Every time a user's search leads them to your ad and they click on it, Google is notified that your ad is effective. 

But it's also important to keep your ad concise. Google only allows you 95 characters, so you'll need to make them count! That means focusing on _benefits_ instead of _features._ If, for example, you're a personal injury attorney your ad might state "Find out how much your case is worth."

But maybe advertising with Google isn't for you. Don't worry, there are plenty of alternatives. For instance, with Bing you'll get a lower price per keyword but with a catch: Bing only accounts for approximately 25 percent of all search engine requests. Facebook is an effective advertising location for those marketing to women and LinkedIn is great for reaching male and female professionals. 

Now that the basics are in place, where do you go from day 5? Start building on your campaign — starting with social media.

### 6. Boost your marketing strategy with well-timed social media postings. 

It's clear that a few tweets about your business won't bring customers pouring in. But you _can_ use social media to attract potential clients, redirect them to your website and turn them into customers. Begin by creating social media profiles for your business. Google is again the best choice, via its social media platform Google+. 

Why?

Because Google offers the most visibility, because posts on Google+ show up more often than posts on any other social media. 

After you've created your account, write a tagline that's under the ten word limit and remember that keywords are essential. For instance, if you're a dentist a good choice would be "Affordable, pain-free dentistry by Milwaukee dentist." Once your tagline is up, complete the remaining sections of your profile and add a link to your website. 

Again, if Google isn't for you there are many alternatives. For example, a company page on LinkedIn lets you tell the story of your business. If you choose Facebook's business page option just put your keywords in your company description and wait for users to subscribe to your page by _liking_ it. And then there's Twitter, a means to potentially grab the attention of thousands when others retweet your tweets. 

But setting up your profiles is only the beginning. It's also essential to post regularly, keeping your profiles lively.

Here's how.

When posting to social media it's key to use a different hook each time. A good strategy is to switch between questions, like "Which tax saving ideas would save YOU a fortune?", lists like "Ten most common mistakes when filling out a tax form," and advice like "Here are the ideas that always surprise my clients."

But keeping up with social media can be difficult. So make it easier by scheduling it in advance. Third parties like HootSuite offer to manage your social media for you but if you manage your own, keep timing in mind. For instance, avoid scheduling posts during heavy news days — like a presidential election or emergency situation.

### 7. Strengthen your online presence through blogging. 

Once you've got the basics of social media working you can expand to blogging. While the time required to write a blog may seem overwhelming, your investment can produce excellent results.

Why a blog?

Because blogging establishes you as a thought leader, a quotable person within your field. Not just that, but each post you make could bring new customers to your website. Since Google loves fresh new content, your website's ranking will benefit from every new article you blog. And finally, blogging is a great way to keep in touch with your customer base. 

But to build a good blog you'll first need to find your style. That means avoiding corporate gibberish at all costs. There's nothing more boring than technical language and jargon. Instead, go with a one-to-one conversational style and address your readers directly with personal details. 

For example, a piece about your career path or past experiences can quickly build your reputation and trustworthiness. Keep in mind that people tend to forget facts but remember stories.

As with all internet activity, you can't underestimate the power of search engines to optimize the visibility of your blog. So to stay prominent in searches, be sure to include your keywords in the headline and first sentence of every post, putting them in bold typeface for added effect. 

But what about the content of your posts?

A good choice is to go with things your customers should do on a regular basis. If you're a dentist your posts might remind readers to floss twice daily. Or you can debunk widely believed myths and warn about common errors with posts like "Ten mistakes parents make with their first baby." You should then flesh out your point with true stories and testimonials. For example, if you sell skincare products you might share an anecdote about how an acne cream you sold to a desperate teenager turned his life around.

Whatever you write about, keep in mind that blogging for business is about what's valuable to others.

### 8. Reach a wider audience through media coverage and direct mail. 

Maybe social media and blogging aren't enough for you. You want to get your free offer to even more people. You've got two options: either appeal to the masses or target a specific audience. 

How?

Through traditional media. Media coverage is a powerful method for reaching a wider audience. All forms of media from TV to radio and even print can strengthen your image. Because visibility in the media leads to a type of credibility that's unattainable just through advertising. 

But in order to appeal to the media you'll need the right story angles. Because it's not what you want to communicate but what the media wants. And that means marketable stories. 

You can make your offer more appealing by connecting it to current events like an upcoming holiday. For instance, say you're a dentist and it's Christmas time. You could get in the holiday spirit by offering free dental services for underprivileged children.

Another strategy is to latch on to a trend or to build a story that revolves around a celebrity. For example, a tailor could get coverage by offering an analysis of Obama's outfit at the White House Correspondents' Dinner.

And of course there's direct mail. While it may seem old-fashioned, consider how much less competition there is in your mailbox than in your inbox. Also, the tangibility of a physical letter leaves the recipient with a stronger impression.

But an effective mail advertising strategy requires you to find and, if necessary, purchase the right mailing list. You might have heard of _compiled lists_ — cheap options that gather general public information. These can be inaccurate, so a better, and more expensive option are _response lists_, detailing the private information of people who responded to similar offers in the past. 

For instance, the author used response lists to advertise his latest marketing publication to people who purchased a book on self-help for small entrepreneurs and his publication was an instant success!

### 9. Find a partner, build a joint venture and make a win–win situation for both of you. 

So your marketing system is entirely in place and running smoothly. One last push will seal the deal: set up a _joint venture,_ a cooperative effort between you and a partner designed to make money for you both. Your collaboration may even consist of an entirely new business that you build together. The key to getting started is finding the right partners.

How?

Ideally they should be in a similar industry to yourself. For instance, certain pairings seem natural, like a realtor teaming up with a mortgage broker. But less obvious combinations like a chiropractor teaming up with an injury attorney can also produce excellent results. 

Just remember not to assume that the obvious choice is the right one. After all, your competitors are likely to have scouted them out as well. 

Once you've chosen partners the first hurdle you'll have to clear is distrust. For this reason the most effective partnerships are between people with pre-existing relationships. 

How can you build trusting relationships for joint ventures?

A good way is to offer your services to potential partners. For instance, you could volunteer to be a guest speaker at their seminar. 

But in some instances there will be too much competition for strategies like this to work. In certain cases you'll need to buy your way in. For example, say you organize seminars in which the main guest speaker is the most prestigious dentist in California. If you only gave him one percent of the profits, he'd likely question whether he'd do better running his own seminar. Therefore, in order to forge lasting relationships you'll need to create win–win situations — and that means a fair split of profits. 

When implementing your marketing system keep in mind that it requires commitment. You'll need to stick with it for at least six months. But remember, once you've built a big base, converting customers will be all the more easy!

### 10. Final summary 

The key message in this book:

**Powerful marketing campaigns aren't just for big companies with the time and money for complicated strategies. Every entrepreneur has the potential to build a plan suited to the needs of his business. The trick is to focus on a dynamic system instead of investing in unconnected activities.**

**Suggested** **further** **reading:** ** _Permission Marketing_** **by Seth Godin**

_Permission Marketing_ confronts the conflicts and challenges that modern marketers face in the digital age and offers a viable alternative. It explains how the advertising landscape is filling up and how this makes traditional advertising ineffective. The author suggests that smart marketers no longer simply interrupt consumers but invite them to volunteer their time and become active participants in the _marketing process._

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Mark Satterfield

Mark Satterfield is the CEO and founder of Gentle Rain Marketing, Inc. He has been an active force in the marketing industry for 20 years, advising clients in over 75 different industries on how to effectively grow their businesses.

