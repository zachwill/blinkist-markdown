---
id: 57483f68c678450003db711e
slug: the-only-game-in-town-en
published_date: 2016-06-03T00:00:00.000+00:00
author: Mohamed A. El-Erian
title: The Only Game In Town
subtitle: Central Banks, Instability, And Avoiding The Next Collapse
main_color: F4CD45
text_color: 806B24
---

# The Only Game In Town

_Central Banks, Instability, And Avoiding The Next Collapse_

**Mohamed A. El-Erian**

The 2008 financial crisis dramatically changed the global economic landscape. Central banks now play a very different role than they did previously, and we now face a set of new economic risks and problems. _The Only Game in Town_ (2016) outlines the roots of these risks and problems, and what we can do to start overcoming them.

---
### 1. What’s in it for me? Delve into how central banks define the current economic situation. 

Since the 2008 financial crisis, politicians have failed to find any means to foster economic growth. Instead, they turned to central banks — the institutions that oversee monetary systems — and made those institutions solely responsible for creating growth. Somehow central banks have become the "only game in town" when it comes to making the economy prosper again.

These blinks explore the significant role of central banks in the world economy and take you through some of the disconcerting developments that can be understood by looking at the responsibilities of central banks.

You'll also learn

  * why the European Central Bank charges people to put their money in the bank;

  * how central banks have widened the income inequality gap; and

  * why structural reforms need to be prioritized.

### 2. Central banks have gone through a dramatic transformation in recent years and have ventured into unknown territory. 

Few people paid attention to the role central banks played in the global economy before the financial crisis of 2008. Even today, most people only have a loose understanding of what they are. In layman's terms, they're state-owned institutions that manage a nation's money supply.

Central banks have gone through some dramatic changes in recent years, however, and now possess a lot more power and responsibility.

In addition to printing money and managing the currency in circulation, central banks are now tasked with fostering economic growth and employment. They're also responsible for ensuring financial stability by monitoring the banking industry.

Consider the _Federal Reserve System_, or the _Fed_, which serves as the central bank of the United States. It oversees the nation's financial system by supervising and regulating banks.

Because of the problems caused by the 2008 financial crisis, central banks have been forced to venture into potentially dangerous new economic waters. When the US housing market started to collapse in 2008, some central bankers had to interfere by leveraging monetary policies that had never been used before.

In June of 2014, for example, the European Central Bank pushed the interest rate on bank deposits into the negatives: an unprecedented move in times of economic stability. Depositors were thus paying to store their money in the bank rather than earning interest from doing so.

The move was aimed at boosting the economy by incentivizing people to spend more. Instead, it just hurt people who were saving their money by taking it away from them. The full repercussions of this policy remain to be seen.

Where have these changes been leading us? What effects will they have in the long run?

> "_It is now crystal clear the central banks ended up taking an overly relaxed approach to the deregulation of financial activities._ "

### 3. Central banks are associated with worrisome developments, such as a lack of growth and high unemployment rates. 

Central banks managed to restore calm to financial markets after the 2008 crisis, but they failed to create what Western countries need the most: strong growth.

All in all, the central banks' new ventures have created ten major worrisome developments. Let's take a look at the first two now.

First, many advanced economies now lack a proper path to economic growth. They currently have a tough time generating growth, which means it's difficult for them to improve their living standards, reduce poverty and invest in the future.

To compensate, a lot of wealthier nations have been seeking to "steal" growth from other countries by having their central banks print more money than they usually would. A currency's value decreases when there's more of it, so exports get cheaper and imports get more expensive.

In that sense, growth is "stolen" from countries with more valuable currencies because it forces them to import more of these cheaper exports.

Needless to say, this approach of stealing growth hasn't resulted in economic growth in the Western world. Furthermore, the West runs the risk of neglecting certain structural reforms, such as tax reforms, which could have the potential to revive long-term, economic growth.

Central banks have also failed to reduce unemployment, which remains high in a lot of developed countries.

Greece, for example, had a 25 percent total unemployment rate in early 2015 and a youth unemployment rate of over 50 percent. High unemployment is caused by the lack of economic growth, which hinders the creation of new jobs.

It's hard to overstate the dangers of unemployment. A high unemployment rate is dangerous for both individuals and the state, as it prevents the state from overcoming debt and makes the future uncertain. Economic frustration has also fueled the rise of extremist groups like Greece's Golden Dawn, a neo-Nazi party.

> "_… the elusive advanced-economy quest for growth has morphed into a generalized growth deficit for the world as a whole._ "

### 4. Income inequality and political dysfunction are on the rise; institutional credibility is on the decline. 

In recent decades, globalization has been closing the worldwide gap between rich and poor countries. Yet within each country, income inequality is increasing.

In fact, income inequality is now higher than at any point in the past fifty years. The average income of the top 10 percent of the Western world is about nine times higher than that of the lowest 10 percent, according to the _Organization for Economic Co-operation and Development_, or the _OECD_.

A big income inequality gap means a correspondingly big gap in access to education and health care. Unfortunately, central banks have caused this gap to widen thanks to their low interest rates, which help big companies but hit regular citizens especially hard.

_Institutional credibility_ has also been waning. Stable economic prosperity is dependent on institutional credibility, but politicians and financial institutions are causing people to lose their faith in banks and governments.

Before the 2008 crisis, governments allowed banks to take irresponsible risks that ultimately brought the global economy to the brink of a collapse and caused thousands of people to lose their savings. As the crisis raged on, governments proved ready to bail these banks out.

This behavior severely damaged the central banks' reputation and, since they're state-owned institutions, that went hand-in-hand with a decreased trust in the state.

The erosion of trust in the system has also contributed to political dysfunction. Politicians tend to be afraid of making big compromises, as compromises might cost them their careers. But an unwillingness to compromise only prevents us from moving forward out of this global crisis.

In the United States, for example, the two political parties now find it very difficult to agree on trade pacts or basic infrastructure projects.

> "_All in all, inequality has become a deeply entrenched problem whose adverse consequences can easily tip into a self-feeding, vicious cycle_."

### 5. Geopolitical tensions are mounting and significant financial dangers loom. 

We've seen that domestic nationalist political movements are on the rise. Unfortunately, this isn't the only increasing global concern.

Geopolitical tensions are also on the rise because Western economies can no longer manage the international monetary system. The Western countries are the heart of the current global financial system. The United States is the most influential player in both of the world's two most powerful financial institutions, the _International Monetary Fund_, or the _IMF_, and the _World Bank_.

These institutions were founded on the belief that Western economies were stable, therefore making it safe to pin global economic and financial stability on their central banks.

However, advanced countries have failed to deliver that stability and emerging economies are seeking to undermine the Western-dominated financial system.

The BRICS states (Brazil, Russia, India, China and South Africa), for example, are now trying to build their own financial institutions to rival the IMF and the World Bank. Such institutions aim to take power away from the West, which generates more geopolitical tension.

Moreover, the regulation of the banking sector is also presenting new economic risks. Central banks aim to reduce irresponsible risk-taking by regulating the banking system, but that incentivizes big financial players to look for risks _outside_ the financial sector, in areas such as home or health-care insurance.

The financial world, too, has its own emerging set of dangers, like _liquidity risks_ — the risk that an investment can't be sold off quickly enough to prevent the investor from incurring a loss because the market value of the investment falls too fast.

Ongoing liquidity risk is another consequence of the central banks' regulatory intervention. When less risk-taking is allowed, there are fewer buyers in the market to buy up what sellers have to offer.

> "… _squeezing risk out of the economy can be like pressing down a water bed: The risk often re-emerges elsewhere._ " — Greg Ip, _Wall Street Journal_

### 6. Financial risk-taking is high, economic risk-taking is low, and well-managed economies struggle to thrive. 

If you're the head of your company, you take a risk when you invest in training your labor force. You can't be sure it will eventually pay off. If you're an investor, you take a financial risk with your investment: you can't know whether the share price will rise or fall.

In other words, companies take _economic risks_ and investors take _financial risks_.

There's a strong contrast between these two forms of risk-taking in the modern global economy. Financial risk-taking is high whereas economic risk-taking is low.

A lot of companies are hesitant to take economic risks, preferring to save their earnings rather than invest them. Investors in financial markets, on the other hand, often take huge risks by straying far from their area of expertise.

An investor who previously invested in the oil industry might turn to the housing market, even if he has no prior experience in the field.

Low economic risk-taking is bad for the economy because it prevents companies from making long-term investments in critical areas, like developing new equipment or training their labor force. High financial risk-taking is also bad for the economy because it can result in big losses and instability.

Another problem with this risk-taking is that economies, companies and households can't thrive even if they're well-managed. Imagine how hard it would be to maintain a nice house if your neighborhood were steadily deteriorating.

The overarching pessimism and economic malaise also hurts well-managed economies, companies and households. Even if they perform well on their own, they struggle to realize their full potential in a market or industry that's doing poorly.

> "_All of this adds up to considerable headwinds for the better-managed part of the national, regional, and global system._ "

### 7. There are four key measures we can take to improve the ailing global economy. 

It would be nice if there were one simple solution for all the global economic problems of our time. Alas, there is not. But there _are_ four key measures we can take to help regenerate growth.

First off, we need to take the idea of _inclusive growth_ — growth that benefits everyone — more seriously. Central banks have proven to be ineffective at fostering inclusive growth with their financial engineering. Instead, we have to focus on structural reforms, like strengthening infrastructure and improving education systems.

Those reforms strengthen recovery because good infrastructure and a stable labor force help countries adapt to changes in the global market.

Secondly, we have to resolve the disparity between the willingness and the ability to spend. Countries like Greece are willing to spend but not able to, whereas Germany can but doesn't want to.

Strict _austerity_ policies — or policies of minimal spending — such as the strategy employed by Germany, need to be revisited. Austerity hinders the positive effect government spending has on infrastructure, education reform and health care.

Third, _debt overhangs_ need to be removed. High, persistent public indebtedness harms production and discourages people from making new investments. Heavy debt burdens prevent investors from pumping fresh capital into the economy. They need to be lifted.

Finally, we have to improve the design of the global economy. Think of the global economy like an orchestra with different sections — brass, percussion, strings, etc. Each section might be good on its own, but if they're not playing in synch with the guidance of a conductor, the music will be incoherent.

The global economy needs a conductor. The IMF, whose job is to ensure global financial stability and foster economic growth, might be a good candidate.

> "_Rather than just act as referees, central banks have also taken the field in quite a range of sports._ "

### 8. The future economic landscape will be divided into four groups of countries, each with differing economic prospects. 

Where is the current economic course leading us? It's impossible to predict the future, but we do know a few things.

In the future, countries will likely be divided into four groups: those that prosper, those that are slowly growing, those that stagnate and those with a volatile future.

The first group will consist of countries like the United States and India. The US economy will continue to be prosperous, though it remains to be seen whether the United States will regain the global power it had before the crisis. India seems likely to see annual growth rates in the 6 to 8 percent range, which will result in prosperous growth.

The second group will consist of countries whose growth rate has declined because of the crisis, but which have managed to emerge with a new, relatively stable economy. China, where growth will stabilize in the high 5 to 6 percent region, will be the leader of this group.

Stagnating countries will include Europe as a whole, as well as Japan, which won't be able to break out of its low, 0 to 2 percent growth rate. These countries have little hope for growth because of their high debt and high unemployment.

Countries in the fourth group are the wildcards. They have big regional influence but very volatile economies.

The most prominent country in this group is Russia. Western sanctions and low oil prices have pushed the country into a recession, but constructive reengagement with the West might lead to economic recovery. Greece is also a "wildcard" because its ability to recover depends on whether or not it can cut its debt burden.

> "_The road the economy is currently traveling will effectively come to an end soon._ "

### 9. Final summary 

The key message in this book:

**Central banks play a significant role in the global economy, but they have been pushed into dangerous and uncharted waters since the 2008 crisis. Instability and income inequality are on the rise as a result, and geopolitical tensions are increasing. Financial risk-taking is irresponsible and societies are growing more frustrated with their political systems. The global landscape will continue to become more divergent if we can't find a way for nations and central banks to work together in a way that benefits everyone.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _PostCapitalism_** **by Paul Mason**

_Postcapitalism_ (2015) offers a close examination of the failures of current economic systems. The 2008 financial crisis showed us that neoliberal capitalism is falling apart, and these blinks outline the reasons why we're at the start of capitalism's downfall, while giving an idea of what our transition into _postcapitalism_ will be like.
---

### Mohamed A. El-Erian

Mohamad A. El-Erian is the chair of President Obama's _Global Development Council_ and the chief economic advisor at _Allianz_, the corporate parent of PIMCO. He's also a contributing editor for the _Financial Times,_ a _Bloomberg_ columnist and has served as the deputy director of the International Monetary Fund. His writings have appeared in _Fortune_, the _Wall Street Journal_, the _Washington Post_ and a number of other publications.

