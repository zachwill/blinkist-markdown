---
id: 559255356561380007000000
slug: the-opposite-of-spoiled-en
published_date: 2015-07-01T00:00:00.000+00:00
author: Ron Lieber
title: The Opposite of Spoiled
subtitle: Raising Kids Who Are Grounded, Generous, and Smart About Money
main_color: E04C50
text_color: BF4144
---

# The Opposite of Spoiled

_Raising Kids Who Are Grounded, Generous, and Smart About Money_

**Ron Lieber**

_The Opposite of Spoiled_ (2015) is the essential guide to raising patient, generous children through financial education. These blinks will show you how to guide your child's development by talking to them about money, involving them in financial decisions and showing them the importance of generosity and work.

---
### 1. What’s in it for me? The secret to raising well-rounded, generous kids. 

Are you shocked by how much stuff kids have today? With their smartphones, iPods, consoles and designer gear, they seem to have it all. To someone who grew up a decade or so ago, today's kids are materialistic, _spoiled_ even.

Why is this? These blinks show you where this materialism has come from. Incredibly it doesn't come from parents putting too much consideration on money, but from too little talk about it. Rather than educating their offspring about the value — and cost — of money, parents simply buy them whatever they desire.

We need to change this if we want the next generation to be full of more rounded, less spoiled individuals. So let's get blinking and learn how we can turn things around.

In these blinks you'll discover

  * why children should start working as soon as they are able;

  * why if your child can count they are old enough for an allowance; and

  * why you shouldn't pay your children for doing chores.

### 2. Teach your children about money to make sure they don’t grow up spoiled. 

What would you say is the most unlikeable trait of kids today? Some parents would say stupidity, un-exceptionality and even violence. But the most common answer by far is that they're _spoiled._

Oftentimes when people think "spoiled" their next thought is "money." This leads many parents to believe that focusing on money when raising their kids will spoil them. But this simply isn't the case.

By definition, being spoiled has little to do with money. Actually, there are four basic qualities that all spoiled children have in common.

First, they tend to have practically no chores, tasks or responsibilities to others. Second, they don't have to follow rules or adhere to schedules. Third, they receive too much time and attention from their parents. And fourth, they often have many material possessions.

With the exception of the last point, none of these have anything to do with money.

In fact, money, whether an education in it, or responsibility for it, can actually help prevent your kids from growing up spoiled. Because children who learn about money also learn thrift, curiosity, generosity and patience — qualities that most would consider the opposite of "spoiled."

For instance, what better way to teach generosity than by giving the opportunity to give? Not just that, you can also promote patience and thrift by encouraging children to save money or learn to make do with the money they have.

Unfortunately today, talking to children about money is taboo.

Why?

Because we're embarrassed to discuss our wealth or how it compares to that of our neighbors with anyone, much less our children. So we put the topic off-limits. This in turn denies kids the opportunity to learn about money and they become spoiled.

In order to ensure your kids grow up to be well-rounded people it's essential to break down these barriers. But how?

### 3. Answer your kids’ financial questions honestly and show them what things cost. 

So an education in money is essential to raising a well-grounded child, but how can you achieve that?

Oftentimes a child will raise the topic of money themselves. Maybe their classmate made a comment that got them interested or they overheard you and your partner discussing the subject. However it happened, something piqued their interest and they want to know more.

A common reaction to your child asking you questions like "how much do you earn?" is embarrassment. What if they compared your income to those of their friends' parents or started asking for more expensive gifts?

But instead of brushing the question away, the best thing to do is to ask a question in response. Ask them, "why do you want to know?"

Here's why.

Many times children who enquire about money are interested in the subject for a specific reason. For instance, the question "are we poor?" could be related to a fear of moving houses. While the question "are we rich?" could stem from a compassionate desire to buy something for a friend who can't afford it.

Once you've established why the child is asking the questions, it's essential to give them an honest answer, never lying or sugarcoating your response.

But being open with your kids about money isn't enough. You'll also need to involve them in financial decisions.

How?

You can show them bills to explain how everything you do requires money. Or, when you pay for something, ask your child to guess its cost and correct them if they're wrong.

This second tip is helpful because kids have little understanding of money's value. For instance, they might think that a new car costs $100. By showing your children the real costs of things you help them understand what money is actually worth.

### 4. Let your child practice with money to make sure they learn the importance of budgeting. 

Did your parents give you spending money as a child? Can you remember how much? Many kids receive an allowance and their parents are doing precisely the right thing.

That's because allowances are excellent practice for children to learn about money and how to spend it. So as soon as your child can count, start giving them a small sum — for instance, $1 per week.

But never base your child's allowance on the chores they do. Why?

Because it's important for kids to know that household responsibilities like washing the dishes are done because they need to be, not because of a financial incentive. If your child protests, remind them that you don't get paid for these tasks either.

Once you've given your child an allowance that's independent of their chores let them spend it how they wish, allowing them to make mistakes.

If your kid is like most kids they'll spend their money on useless rubbish like too much candy, the latest fads and cheap plastic toys. It's in your best interest to let your child make these mistakes knowing that with every dollar they waste they're learning the importance of budgeting.

But this doesn't mean you shouldn't talk to your kids about their spending habits. A good strategy is to explain to your child the various purchase options they have and the difference between what _they want_ and what _they need._

For instance, they can buy the expensive new boots that everyone at school has, or they can buy the durable ones that will last for years longer. Between your explanation and your child's practice, they're sure to become better budgeters.

As your child gets used to spending their allowance you should raise it slowly, thereby incrementally increasing the amount of responsibility they have.

### 5. Avoid raising materialistic kids by making them wait for the things they want. 

We've all seen the heart-warming joy in a child's eyes when they're handed the toy they wanted so badly. But while giving to children feels good for both parent and kid alike, it can be tempting to give too much.

As parents, it's easy to think that your children always need more things to fit in with their peers. Therefore, spending money on your kids can seem like a straightforward way to help them gain acceptance. This concept is known as _full provisioning_ and its logic works like this:

If your child's friends have TVs in their bedrooms, your child needs one too. If everyone at school has a new smartphone, your kid is entitled to the same. If you don't buy your children what the other kids have won't they be bullied and alienated from their peers?

While the temptation to give your kids everything is palpable, by doing so you're training them to define themselves through their possessions.

On the other hand, children who don't get everything they want at the drop of a hat learn patience through waiting.

But how can you strike a balance between teaching your child patience and ensuring they aren't shunned by their peers?

Try the _Dewey rule_, which says that your child should be in the 30th percentile of stuff. This means that while they won't be the first of their friends to get a new gadget and won't get things as soon as they ask for them, they will eventually get _some_ of what they want. In other words, if they're in the 30th percentile, they'll be the seventh of ten to get that gadget.

This isn't a hard and fast rule but instead works by teaching kids the importance of waiting for things. This will help them think more about what they ask for and enjoy things more when they finally receive them.

While you can't always say no to your kids, it's important to remember that love can't be bought. As a parent, it's your job to find a happy medium.

### 6. Encourage your child to get a job so they can learn while they earn. 

Do you ever consider what life was like for kids in the past? If you go back just a century and a half most children's lives were pretty terrible. Instead of playing or going to school, they worked long shifts at backbreaking jobs. Thankfully, many countries now have laws prohibiting child labor. After all, childhood should be about learning and enjoyment.

But maybe we've gone a bit too far in ensuring kids don't work. Many parents today believe their children shouldn't work at all. And this leads to their kids missing out on beneficial opportunities.

How?

Safe and comfortable work can teach children a variety of skills that will aid them later in life. For instance, just experiencing a work environment builds communication skills as you speak with colleagues and customers. Working also teaches reliability as well as responsibility since you're required to arrive on time and do a good job. Lessons like these will give your child a leg up when they enter the adult workforce.

Remember, giving your child work experience doesn't require them to be in a professional setting. Making your kid responsible for additional household chores can provide the same learning experience.

So working teaches kids helpful lessons, but the other obvious benefit to your child getting a job is the ability to earn spending money of their own. With a regular income your child can purchase their own things, taking the pressure off _you_ to buy them stuff. Not just that, but spending cash they've worked hard for helps instill the value of money in children. Because they're much less likely to waste money they've earned than money they've been handed.

Your child might even help make purchases that you alone could never afford for them, like a college education, a car or even a horse.

But most importantly, experience of work is a major boost to a child's modesty and independence.

### 7. Make sure your kids know the importance of generosity and perspective. 

The most important value to instill in your kids is generosity. That's because people who are willing to give to others certainly cannot be considered spoiled themselves. You can nudge your child in the right direction by encouraging them to be generous when they can.

How?

Just take a look at the example of California mother Olivia Higgins. Her two kids were always asking her about the homeless, why they existed and why the family didn't give them any money. It's actually quite common for kids to ask these questions, and just as common that parents are too embarrassed to honestly answer them.

But Higgins is different. She seized on this as a golden opportunity to teach generosity. Instead of simply giving her kids money to hand out she came up with the idea of donating "giving bags" comprised of various provisions. The kids were involved in planning the contents of the bags and the whole family went around distributing them. Can you think of a better way to get into the giving mind-set?

But kids also need to understand their own privilege and that there are others with much less than themselves. Teaching children perspective is essential, but as we know, most parents are too embarrassed to talk about wealth and social class.

You can give your kids perspective by honestly telling them how much wealth your family has and getting them to volunteer to help the less fortunate. You don't need to do anything huge like sending them to Africa to build schools. In fact, simple efforts like working in a soup kitchen or homeless shelter can offer the same enlightenment and insights.

### 8. Final summary 

The key message in this book:

**Money really does make the world go round, and a financial education doesn't just fill a bank account — it also determines the kind of person one becomes. So, in order to raise well-rounded, generous and enlightened children it's necessary to be open and honest about money. Because the only way children will learn the value, importance and limits of money is through a hands-on education.**

Actionable advice:

**Limit your child's desire for material possessions by limiting their TV time.**

One of the most common ways for children to learn about and desire toys, clothes and all manner of purchasable items is through advertisements seen on television. By monitoring your child's TV consumption you can control the amount of advertisements they see and curb their desire for stuff.

**Suggested** **further** **reading:** ** _How Children Succeed_** **by Paul Tough**

These blinks explore the reasons why some people struggle in school and later on in life, and why others thrive and prosper. Using scientific studies and data from real schools, the blinks dive into the hidden factors that affect the success of children.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ron Lieber

Ron Lieber, in addition to writing the Your Money column for the _New York Times_, is the author of three bestsellers, including _Taking Time Off._

