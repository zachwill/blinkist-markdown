---
id: 55ed24f811ad1a0009000000
slug: the-optimism-bias-en
published_date: 2015-09-07T00:00:00.000+00:00
author: Tali Sharot
title: The Optimism Bias
subtitle: A Tour of the Irrationally Positive Brain
main_color: CAADC0
text_color: 665761
---

# The Optimism Bias

_A Tour of the Irrationally Positive Brain_

**Tali Sharot**

_The Optimism Bias_ (2011) demonstrates the interesting and entertaining ways in which our rose-tinted glasses color our experience of the world — and why it's a good thing that they do. Though they won't enable you to take off those rosy specs, these blinks will at least afford you some insight into why you wear them, and how you can use them to your benefit.

---
### 1. What’s in it for me? Learn why the optimist beats the pessimist. 

So, you're a proud pessimist. You have always told people that that's what you are. You argue that it's the only way to protect yourself against life's many disappointments. It allows you to be joyfully surprised when something great, yet unexpected, happens. While the latter argument is true, the former isn't — pessimism does not protect you from disappointment!

In these blinks, the author introduces the wonders of optimism. The minds of most are convinced that the future is bright, and such an optimistic view of the world and of the future has a clear influence on the lives and choices of those who propound it. 

In these blinks, you'll discover

  * why people are mostly optimistic;

  * how the choices you make in life are based on your level of optimism; and

  * which holiday destination is best for you.

### 2. The human mind is not rational, but prone to bias. 

People like to view themselves as rational beings. In fact, this belief is so central to our self-perception that we even included it in the name of our species: _Homo sapiens_, the "wise man." However, as you'll soon discover, we may have been a bit too optimistic.

Why? The way we perceive reality is simply not rational; often, it's full of bias. To get an idea of just how biased we are, consider the following questions and then rate yourself relative to the rest of the population: Do you play well with others? Are you a good driver? How honest are you?

So, did you rate yourself in the bottom 25 percent, the top 25 percent or somewhere in between?

If you're like most people you will have rated yourself above average. Maybe even in the top 25 percentile!

Of course, it's statistically impossible for this to actually be the case. How can _most_ people be better than the rest? This phenomenon, one of many that shapes our perception of the world, is called _superiority bias_.

It's hard to avoid such delusional thoughts about the world around us. We tend to trust our perceptions, and thus don't realize that the way we see the world is usually misguided. 

To demonstrate this point, consider a study conducted by cognitive scientist Petter Johansson. In the experiment, participants were given pairs of photos depicting different women, and were tasked with deciding which of the two they found more attractive.

They were asked later to explain their choices. But this time they were given pictures of the women they had actually rated lower. Seventy-five percent of the time, the participants didn't even notice the switch, and thus justified a choice they hadn't actually made.

Even stranger, when asked afterward if they thought they would hypothetically notice such a switch, 84 percent of those that had _just been fooled_ confidently exclaimed that they would easily detect such trickery.

### 3. The optimism bias changes the way we view reality. 

Let's start with a quick (albeit macabre) question: What's the likelihood of your getting cancer one day? If you predicted the probability to be lower than 33 percent — the actual statistical probability of getting cancer — then you're in good company. Like most of us, you're an optimist.

In fact, according to a study by Yale psychologist David Armor, about 80 percent of people have what is called an _optimism bias_, that is, the tendency to look into our future optimistically rather than realistically. This bias causes people to overestimate the likelihood of experiencing positive events, such as a job promotion, and underestimate the likelihood of experiencing negative events, like a car accident. 

What's more, optimism bias can sometimes take on extreme forms. Consider, for example, that in the Western world the divorce rate is about 40 percent. Yet, when you ask newlywed couples if they think _they_ will get divorced, they'll usually tell you there is a zero-percent chance.

Even divorce lawyers, whose professional experiences tell them otherwise, greatly underestimate the likelihood that they, too, will get divorced.

Despite our love for optimistic thinking, our optimism has limits. Indeed, we're only optimistic about our _own_ future and that of our loved ones — not about the future of others.

We see this exemplified in a survey revealing that 75 percent of British people are optimistic about their family's future, but that only 30 percent think that families _in general_ are better off today than they were a few generations ago.

The reason for this discrepancy is not a naive belief that fortune will somehow smile upon us. Rather, we tend to erroneously believe that we are capable of making everything turn out okay in our own lives.

For example, we think: "Sure, divorce rates are high. But my partner and I are reasonable adults. If we encounter problems we'll be able to sort it out." As if we are somehow different from all those divorcees.

> _"We can't all do better than the average Joe. Maybe not, but deep down we believe we can."_

### 4. The optimism bias is deeply rooted in our brain. 

The workings of the brain have long been a mystery. However, with the advent of fMRI scanners — _functional magnetic resonance imaging_ machines that measure activity in various regions of the brain — we've begun to unravel that mystery by learning how the brain works.

These scanners have also helped us understand the optimism bias.

There are two areas in the brain that are primarily responsible for the optimism bias, both of which are important for emotions and motivation.

The first area is the _amygdala_, which processes emotions and is responsible for fear. The second is the _rostral anterior cingulate cortex_ (rACC), which regulates the activity in those brain areas responsible for emotions and motivation. One way it does this is by reducing fear and stress responses from the amygdala.

The more tightly these brain areas are connected, the more the brain will pay attention to positive stimuli — and, in turn, the more we adopt an optimistic outlook on reality.

Often this connection is quite strong. Because we pay more attention to positive stimuli, we imagine future events more vividly if they are positive and rather hazily if they are negative.

For example, when you think about next Saturday's barbeque, you'll imagine sinking your teeth into a delicious burger and enjoying a refreshingly cool beer with your friends. In contrast, when you think about tidying up your apartment, a few vaguely unpleasant images will enter your imagination.

The opposite is true for people who suffer from depression. In these cases, the interaction between these two regions is dysfunctional, and the depressed person imagines negative scenarios too vividly and positive scenarios all too dimly.

However, whereas severely depressed people are pessimistic about the future, mildly depressed people simply lack the optimism bias. They exhibit something described as _depressive realism_ : their predictions for the near future are pretty accurate, albeit less optimistic.

Now we understand why so many people are optimists (at least about their own lives). Our following blinks set out to determine whether that's a good or a bad thing.

> _"Could it be that without an optimism bias, we would all be mildly depressed?"_

### 5. Having positive expectations makes us happier and more successful. 

When Magic Johnson and the Los Angeles Lakers won the NBA championship in 1987, their coach, Pat Riley, was asked if they would repeat their success next year. Riley blew everyone away with this straightforward answer: "I guarantee it."

And indeed, not daring to disappoint their fans, the Lakers went on to win the 1988 championship as well.

Strangely, while optimism bias causes us to look at the future with rose-tinted glasses, these positive expectations actually make us more successful. There are a number of reasons for this.

First, optimistic expectations about our endeavors make us more motivated to succeed. This causes us to try harder to achieve our goals, which in turn increases the likelihood of success.

Second, optimism helps us learn from our mistakes, thus increasing the likelihood of our being successful next time.

This was demonstrated in a study by cognitive neuroscientist Dr. Sara Bengtsson. The study required students to complete intellectual tasks. Before doing the tasks, they were primed with either positive or negative attributes, by being confronted with words like "smart" or "stupid."

Students primed with positive words not only performed better, but also learned more from their mistakes. This is because our brain reacts to dissonance between expectations and outcomes. 

If we expect to do well but fail, our brain reacts to this dissonance and, thus, we learn. If, however, we expect to do poorly and have a bad outcome, we learn nothing, as our brain, lacking this positive expectation, has no reason to react.

This goes against the common belief that feeling better about the outcome of our actions is a matter of keeping our expectations low. According to this belief, if we have low expectations, we're less likely to be disappointed.

But this is actually untrue, as demonstrated in a study that monitored students' reactions to failure after taking a psychology exam. The study revealed that students with low expectations felt just as bad when they performed poorly as those with high expectations.

### 6. Optimists have it best when it comes to the important feelings of anticipation and dread. 

It's tempting to try to avoid uncomfortable or dreadful situations, like visiting the dentist. However, putting off these situations is simply counterproductive. 

In part, this is because those feelings of anticipation and dread are sometimes greater than the feelings experienced during the actual event.

When looking to the future, our brain mimics the feeling we expect to encounter, as the rACC and the amygdala will behave similarly regardless of whether you are experiencing something or simply imagining it. 

In fact, sometimes the feelings you have when imagining the future are even stronger than they turn out to be in the real, lived future. That's why excitedly anticipating a single moment in the future can brighten your day time and time again.

Similarly, this is also why we shouldn't postpone dreaded events, like getting a root canal. Your nervous anticipation of this painful procedure causes your brain to mimic the experience of the pain you're about to encounter.

This means that you'll feel bad before the operation has actually been performed. And the longer you avoid it, the longer you will suffer.

With this understanding, it's not hard to see why optimists would have the edge on pessimists when it comes to imagining the future. The intensity of our anticipation or dread depends on how joyful or painful we imagine the event will be — how likely it is to occur and how vividly we imagine it.

But remember: compared to pessimists, optimists imagine positive events more vividly. They also think they are likelier and that they will happen sooner. The opposite is true for negative events: they imagine them less vividly, underestimate the likelihood that these events will occur and imagine that they will happen later rather than sooner.

Whether they are experiencing dreadful or excited anticipation, optimists will have a better experience than pessimists.

### 7. Optimism helps us deal with life and its challenges. 

When making choices, both large and small, in our modern world, we have more options than our parents and grandparents had. If it weren't for the optimism bias, this overwhelming number of options would drive us mad.

Why? Because the optimism bias makes us confident about the choices we make.

In one study, for example, participants were asked to rank a number of potential holiday destinations. Later, the researchers took two destinations that individual participants rated equally (e.g., Greece and Thailand) and asked them to choose between them.

The study revealed that after having made a choice, the participants' _caudate nucleus_, the reward center of the brain, updated its view on the alternatives. For example, if they chose Greece, they would then view Greece as being preferable to Thailand. 

In other words, their preferences changed _after_ their choice was made. While they may have initially rated Greece and Thailand equally, they re-adjusted their preferences based on that choice once they'd been forced to choose between them.

We're probably better off this way. If our preferences weren't influenced by our choices, we would live in constant doubt, ever second-guessing our everyday decisions.

Not only does the optimistic brain help us stay comfortable with our decisions, it also helps out with bad situations.

When we experience an unwanted situation, the rACC plays an important role in coping with it. For example, if a terrible accident causes someone to lose both their legs, they'll encounter some pretty negative thoughts and emotions at first ("I won't be able to go dancing anymore!"). This is totally natural, as these negative emotions themselves help us to avoid such situation in the first place.

However, once it has happened, it can no longer be avoided. Thus, the rACC will adjust these emotions and shift attention to something positive ("At least I still have dependable friends and a clear mind!").

Clearly, optimism is beneficial for our mental health. But is it all sunshine and rainbows? Our final blinks explore the darker side to optimism.

### 8. We are bad at adapting adequately to bad news. 

On June 22, 1941, Nazi Germany invaded the Soviet Union with a force of 4.5 million troops. The Soviet army was caught unawares, despite the fact that the Premier of the Soviet Union, Joseph Stalin, had been repeatedly warned by Soviet spies and had even been told the exact date of the planned invasion.

However, Stalin's brain acted like most brains do: it ignored the bad news.

Normally, we re-evaluate our assessments of a given situation in much the same way that we learn from our mistakes. When we give an assessment of a situation ("Nazi Germany won't attack us") and are confronted with conflicting information ("Nazi Germany is gathering troops at our borders"), an area of our brain — the _frontal lobe_ — signals to us that it's time to re-evaluate our assessment.

Generally speaking, the larger the difference between our assessment and the new information, the stronger the signal. 

However, our brain is only good at tracking these mismatches between expectations and reality when we encounter new _positive_ information.

For example, in an experiment conducted by the author, participants were asked to estimate the likelihood of suffering from negative events, such as getting cancer. She then informed them of the actual statistical risk of the respective events and asked them to re-assess their personal risk.

The results provide interesting insight into our nature. When a person estimated the chance of getting cancer at 50 percent and the real risk was only 30 percent, that individual would correct her estimate, and bring it closer to something like 35 percent.

However, someone who predicted the chance of getting cancer at only 10 percent made virtually no adjustment, despite their new awareness of the actual statistical probability.

The fMRI analysis showed that the "mismatch signal" produced by the frontal lobe was much less powerful if the news was bad, that is, if they had previously underestimated their risk of getting cancer. Despite their new knowledge about the likelihood of getting cancer, these participants simply deemed the statistics irrelevant to their particular case.

### 9. Moderation is a virtue when it comes to optimism. 

As you've learned, optimism has a host of benefits, but it can also come with disastrous consequences. As with many things in life, _moderation_ is a crucial part of optimistic thinking as well. 

Moderate optimists (as opposed to die-hard optimists) make sensible decisions in life. We can see this demonstrated in a study conducted by economists Manju Puri and David Robinson which measured the relationship between optimism and life choices.

Most of the survey participants were moderate optimists who overestimated their statistical life expectancy by a few years. Among the participants, there were also pessimists, who underestimated their longevity, along with extreme optimists who overestimated theirs by more than 20 years. 

Moderate optimists were found to make sensible decisions: they worked longer hours, saved more and smoked less than both extreme optimists and pessimists. Thus, if you want to make the best decisions, you should stick to moderate optimism.

Indeed, moderate optimism is as important for large construction projects as it is for your personal decisions. If you are pessimistic about the costs and benefits of large projects, you will have little reason to engage in them. Conversely, if you are a frantic optimist, your construction project will soon become a taxpayer's nightmare.

Take the world-famous Sydney Opera House, for example. The architects selected for the project estimated that it would take six years and $7 million to complete. However, unexpected difficulties brought the total cost up to $102 million, and the project took more than 17 years to complete. 

This kind of excessive optimism in planning led the British government to publish the _Green Book_, which provides specific guidelines for project appraisals. It requires appraisers to explicitly address their optimism bias by including empirical adjustments to their cost estimates, benefits and predicted duration of the project.

But even these measures didn't prevent the 2012 London Olympics from a subsequent budget increase in 2007. At the very least, however, the budget stayed within this one-time adjustment.

### 10. Final summary 

The key message in this book:

**Common wisdom tells us to keep expectations low, lest we be disappointed. Nevertheless, 80 percent of people maintain an unrealistically positive outlook regarding their personal future. These rose-tinted glasses not only make us happier and more successful, but may also be crucial to our existence.**

Actionable advice:

**Remind people around you of their freedom of choice.**

If you want to increase your employees' commitment to the company or your customers' appreciation of your services, simply remind them that they made the decision to work at your company or use your service. This way, they'll be "re-convinced" that, since _they_ chose your company, it must be better than the alternatives.

**Suggested** **further** **reading:** ** _Predictably Irrational_** **by Dan Ariely**

_Predictably Irrational_ explains the fundamentally irrational ways we behave every day. Why do we decide to diet and then give it up as soon as we see a tasty dessert? Why would your mother be offended if you tried to pay her for a Sunday meal she lovingly prepared? Why is pain medication more effective when the patient thinks it is more expensive? The reasons and remedies for these and other irrationalities are explored and explained with studies and anecdotes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tali Sharot

Tali Sharot is an associate professor of cognitive neuroscience at University College in London, and is a winner of the British Psychological Society's Book Award. She is also the author of _The Science of Optimism_.

