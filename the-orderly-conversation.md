---
id: 58aadac38e98580004e55686
slug: the-orderly-conversation-en
published_date: 2017-02-20T00:00:00.000+00:00
author: Dale Ludwig and Greg Owen-Boger
title: The Orderly Conversation
subtitle: Business Presentations Redefined
main_color: B25D38
text_color: 995030
---

# The Orderly Conversation

_Business Presentations Redefined_

**Dale Ludwig and Greg Owen-Boger**

_The Orderly Conversation_ (2014) is a guide to designing, preparing and delivering a killer presentation. These blinks explain why making a speech is different from presenting in a business context, and why strong presentations are just like a good conversation.

---
### 1. What’s in it for me? Learn the key elements of how to deliver an excellent presentation. 

Just about everyone has at some point (whether forced or voluntarily) carried out a presentation in front of an audience. And almost everyone who has tried has, at least once, failed to capture the audience's attention and interest.

There are indeed quite a few pitfalls that you should seek to avoid if you're hoping to make a successful presentation — and that's exactly what these blinks to _The Orderly Conversation_ are about. They'll provide you with great insights into the dos and don'ts of the world of presentations.

In these blinks, you'll learn

  * how to avoid becoming a dutiful student or a nervous perfectionist while presenting;

  * how to create a connection with your audience; and

  * how best to prepare for a presentation.

### 2. A great presentation is a conversation, not a performance. 

At one point or another, most of us will find ourselves in front of an audience, giving a presentation. But whether it's a sales pitch or a project overview, there's a big difference between a _presentation_ and a _performance_.

A performance is much like a speech in that it's scripted and pre-written. It's carefully put together, rehearsed and totally controlled by the performer. For instance, when an actor gives a performance, he detaches his world from that of his audience, essentially putting up a wall that distinguishes between the performance and the real world.

So, while the last scene of _Romeo and Juliet_ might be utterly convincing, the audience feels no cause for alarm when the actress playing Juliet commits suicide. And just like an actor, when a person gives a speech, he's performing; his words are scripted and prepared, and the audience is well aware of this.

On the other hand, a presentation is unpredictable as there is a constant exchange between the presenter and his audience. In fact, this interplay is essential to bringing the presentation to life, capturing the interest of the audience and fostering a learning environment. There can't be any wall between the presenter and the audience; as such, a presentation is less like a performance and more like a conversation.

But it's not just _any_ conversation. A presentation is an _orderly conversation_, because conversations can easily get off topic and stray in unproductive directions. So, for a presentation to stay on topic it needs some structure — a framework it can follow while maintaining some spontaneity.

This is a bit tricky because it means that as a presenter, you have to speak with a plan in mind, while also responding to changes and adapting to the uncertain trajectory of your presentation.

### 3. The rigid criteria of school presentations can trap presenters in bad habits. 

Looking back on your school years, were you taught how to give a good presentation? Can you remember how your teachers evaluated your presentation performance?

Most students learn how to give presentations and are evaluated according to set criteria, but this method has its flaws. Students tend to be judged on how well they _deliver_ their presentations, based on things like tone of voice, speed, diction and eye contact.

The idea behind this kind of checklist is that it helps the teacher grade all the students fairly. Unfortunately, these same criteria get stuck in people's heads as the golden rules of presenting, when in fact they should be taken with a grain of salt. These rules actually value how well you prepared yourself to deliver a pitch, and not how well the audience actually received your message.

In fact, this approach to presenting so often taught in schools has led to three common but highly ineffective presenter types: the _Dutiful Student_, the _Entertainer_ and the _Nervous Perfectionist_.

The Dutiful Student is far too focused on the classic rules of presentation. This preoccupation makes him concentrate on how his presentation looks instead of on the effect it has. As a result, he becomes disconnected from the moment.

The Entertainer is great at controlling her voice and body language and feels right at home in front of an audience. But since she'll do just about anything to make the presentation exciting, the actual content might not come across so clearly.

Finally, the Nervous Perfectionist will rehearse, memorize and utilize every technique he can to prevent himself from getting nervous. But his excessive rehearsing will backfire by preventing the audience from joining the conversation. What's more, if his presentation changes based on audience participation, which it hopefully will, the Nervous Perfectionist will become increasingly nervous as the conversation veers off its rehearsed track.

So, regardless of what you learned in school, presenting isn't about how you look or sound. It's about how you bring your audience into a conversation while building your confidence and shedding your anxiety, which is exactly what we'll explore next.

### 4. Eye contact and intentional pauses are essential to engaging your audience and returning to the moment. 

Imagine you're presenting in front of a group and suddenly become extremely nervous. You start talking quickly, your words get jumbled and your heart beats faster and faster.

In this kind of situation, the last thing you should do is focus on yourself. Thinking about yourself will only make things worse, leaving your audience unimpressed.

But when you begin a presentation, it's easy to get so caught up with yourself that you entirely forget about your audience. And, unfortunately, the more you think about yourself, the more nervous you'll get and the less connected you'll be to the group.

The authors compare this process to a funhouse. When you enter a funhouse, you encounter tilted floors and strange mirrors that disorient you. As you walk further in, you become even more disoriented and eventually can barely stand on your own two feet, much less think clearly. It's precisely this disorientation and loss of control that occurs when you focus on yourself during a presentation.

So, how can you get out of the _funhouse_? By engaging your audience — and that means eye contact.

When you begin a presentation, be sure to focus on individuals in the audience. Consider what you want to tell them and how you'll grab their attention. Gather the courage to look them in the eyes and speak just as you would if you were having a normal one-on-one conversation.

This works especially well because eye contact helps you respond to the audience as you present, which in turn keeps you focused on the conversation.

But engaging your audience also requires some strategic pausing. While eye contact will help you connect to the group, pausing will give you time to gather your thoughts. So, if you feel like your audience isn't entirely engaged, just take a moment and consider what you intend to communicate.

### 5. Identifying goals and considering your audience are essential to a captivating presentation. 

If you're like most people, you've endured presentations from which you didn't retain a single word. This happens to everyone and the problem lies with the presenter, not the audience. So, how can you avoid giving a useless presentation?

First, when planning your presentation, consider your audience and identify goals that are specific to them. Here, it's crucial to think about how you want your message to be received, and the best strategy for doing so is going _analog_. Before you make slides, you should take a pen and paper and write out the fundamental goals of your presentation.

When doing this, just think ahead to what you want your audience to have thought, felt and experienced after your presentation. If you're giving an informational presentation on a new process, you should consider how your audience will respond to the information you're offering.

You might start out thinking, "I want my team to understand the new process," but end up thinking, "I want my team to _believe in_ the new process!"

The second step is to assess your audience. Think about who they are and how you relate to them. Consider whether the audience contains people of greater importance who you should focus your attention on and then try to empathize with them. Here's where a couple of questions can come in handy:

First, ask yourself "how well will they understand this topic?" This question can help you shed frivolous details. For example, if you're an IT specialist and your audience knows nothing about tech, you might want to cut out some of the technical language.

Second, you should ask yourself, "will it be easy to persuade them?" This question is essential because your audience might hold misconceptions that you need to change. For instance, if your audience thinks that a particular business model is functioning well, but in reality it isn't, you'll need to offer hard facts that will change their minds.

### 6. It’s essential to give context to your presentation before delivering the meat of your topic. 

Whether you're beginning a new slideshow or writing a new text, getting started with a presentation can be overwhelming. To overcome this, it's essential to incorporate visual components in a way that helps the conversation flow.

Here's where _framing slides_ enter the picture as the key to gathering important information and keeping you on task. For instance, when you begin a presentation, you first need to reassure your audience that their participation will be worthwhile.

So, to frame your presentation you should begin by explaining to your audience why they're there, what needs to be accomplished, how your goals will be reached and what they stand to gain. These four points are essential to giving context to your presentation while cutting through the initial confusion — and your framing slides will help make them clear.

Say your team's sales are slowing down. You should show them a slide that presents the current situation, one that states a goal, one that lays out your agenda and a fourth slide that shows them the benefits of your plan.

But conclusions are also a type of framing slide, and an important one at that. In such slides, you want to make clear to your audience that you're approaching an end and give at least one clear actionable step that they can take away.

Framing slides are key to setting the stage, but _content slides_ are the vehicle for delivering the core information of your topic — and these can be much more than just slides. You could use all manner of visuals, from drawings to spreadsheets to posters, as long as they inform your audience about your topic.

And remember, designing picture perfect slides shouldn't be your obsession; there's no proper layout and no essential number of bullet points. What really counts is that your visuals are easily understood!

### 7. Final summary 

The key message in this book:

**There are no strict rules when it comes to preparing for a presentation. Presenting to a group is something that comes to life in the moment, just like a good conversation. It's essential to welcome the unpredictability of your presentation while sticking to your agenda and meeting your goals.**

**Actionable advice:**

**Practice presenting by filming yourself.**

Taking the time to watch yourself presenting on camera is a great opportunity to hear yourself objectively and see how you can improve. After watching your tape you should reflect on your presentation by asking yourself if you look the way you imagined and how you could make your presentation better.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Influencer_** **by Joseph Grenny, Kerry Patterson, David Maxfield, Ron McMillan, Al Switzler**

_Influencer_ (2007) distills the essence of how influence works. In addition to providing examples of real people who are highly adept at affecting change, the authors present information rooted in psychology research and give you the tools you need to increase your influence over others.
---

### Dale Ludwig and Greg Owen-Boger

Dale Ludwig is the founder and president of Turpin Communication, which helps people develop the skills and proficiency to excel in presenting. He holds a PhD in communication and has been a lecturer at the University of Illinois at Urbana-Champaign.

Greg Owen-Boger is the vice president of Turpin Communication. His diverse background includes experience in management and the performing arts. In addition to his day job, he is a frequent blogger.

