---
id: 54479c0c3033610008540000
slug: the-origins-of-political-order-en
published_date: 2014-10-23T00:00:00.000+00:00
author: Francis Fukuyama
title: The Origins of Political Order
subtitle: From Prehuman Times to the French Revolution
main_color: A14147
text_color: A14147
---

# The Origins of Political Order

_From Prehuman Times to the French Revolution_

**Francis Fukuyama**

In _The Origins of Political Order_, Francis Fukuyama delves into the history of modern state-building. Instead of focusing on Ancient Greece or Rome like many earlier scholars, he traces political histories in China, India, the Middle East and Europe. Using a comparative approach, Fukuyama explains how diverse political and social environments allowed Europe to develop many different political systems.

---
### 1. What’s in it for me? Learn the history of modern politics. 

The news is clogged with stories of political turmoil around the world. Every day, we hear stories of failed states, war and corruption. Why are some nations and governments so fundamentally different from others? What factors caused these differing political systems to form?

To answer these questions, we must look to history, and gain an intimate understanding by comparing the histories of many regions, and not just one particular nation-state. Francis Fukuyama does exactly this in _The Origins of Political Order_. In these blinks, you'll learn how our biological predisposition to favor our families has drastically affected the trajectory of political development, and how this has played out in different regions. You'll learn what societal conditions allowed some states to flourish and caused others to fail. You'll also learn

  * how military slaves helped Middle Eastern states maintain power;

  * why China has remained a fairly cohesive state for over 2,000 years;

  * why France and Spain could finance their wars by avoiding their creditors;

  * how parliamentary checks on power caused the English state to grow and the Hungarian state to fall;

  * why the Mongols could build one of the largest empires in history, but not maintain it; and

  * why religion hindered state-building in India but advanced it in Western Europe.

### 2. Humans are social creatures who naturally seek to be part of a group. 

How can we make sense of our evolution? Our progression from hunter-gatherers to life in today's complex, organized societies has been so vast it seems to defy comprehension.

We must first understand that we're social creatures. We thrive by cooperating with our family and others around us.

Our natural sociability derives from our _kin selection_, whereby we help others according to the percentage of genes we share with them. People help their siblings more than their cousins, for example. We share 50 percent of our genes with siblings, but just over ten percent with cousins, and obviously we are less inclined to help those who aren't blood relatives.

_Reciprocal altruism_ also shapes our social natures. It means that, beyond the bounds of kinship, we're kind to people who've been kind to us in the past. In the classic game theory experiment, the Prisoner's Dilemma, two players choose to either cooperate with one another or sell each other down the river. In repeated rounds, players always help those who have cooperated in the past instead of those who have previously betrayed them. If family comes first, friends are a close second.

We also naturally conform to the rules of groups we belong to, because we want to be accepted. This process is quite nuanced, because of our high intelligence as a species.

We developed abstract thinking as our brains became more and more complex. We created theories about our world to give us a sense of control and understanding.

This fostered the creation of transcendent beliefs and religion, which helped make our social groups more cohesive still and gave us something to live for. We didn't just seek material resources, but social status as well.

Eventually, these changes led to the development of politics. Different social groups with separate gods, flags or races fought to assert their place in the world. These bonded groups can be powerfully enduring. Group identity is still very important to us — we are biologically inclined toward it.

### 3. In the earliest societies, families formed tribes together, but they lacked a central authority. 

About 10,000 years ago, humans began permanently settling and growing crops. This increased population density, which compelled people to form more organized social groups beyond immediate families, called _tribes_.

Tribes led to the earliest forms of justice and property ownership. Justice was defined by family and ancestral connections, and there wasn't a higher third party to adjudicate any disputes.

Early tribal law from what is now Germany illustrates this. In 600 AD, disputes were settled with automatic compensation. So if a member of a rival tribe knocked out your teeth, they might have to pay you four shillings per tooth.

The amount of compensation depended on the social status of the victim. So a murderer would pay more for killing a freeman than a servant.

We can still see traces of this system today. In present-day Melanesia, nearly all land is communally owned by groups who share a common ancestor. If anyone wants to extract resources from the land, the entire group must agree — no "judge" or ruler decides it.

Historically, tribes eventually began to fight wars with each other, as people's hunger for power grew. However, they still lacked a central authority that could maintain peace or standardized laws in these inter-tribal disputes.

Tribes could stay united in the face of violence through strong social cohesion. This helped them overpower their enemies, and attain more resources and women from other groups. They were also motivated by the personal loyalty they felt when they helped each other to survive.

The Mongol tribes, for example, came together to conquer Eastern Europe, the Middle East and the entirety of China in just over a century. Though the initial conquest was tremendous, the empire couldn't be sustained because the group of tribes didn't have a centralized authority or a clear leadership succession. The men of Genghis Khan's family waged wars against each other, hoping to gain more power, but they only hastened the empire's decline.

> Fact: According to DNA testing, an estimated eight percent of present-day males in a very large region of Asia are thought to be descended from Genghis Khan.

### 4. The first states emerged because of family power, but they also constantly battled it. 

You might imagine that the first modern states were shaped by the Protestant Reformation, the Enlightenment or the Industrial Revolution. Actually, the first states are much older, and the real story is much more complex.

The earliest states were formed in China. By 770 BC there were 23 of them, but they merged into one after a violent unification process.

A key issue of the unification wars was the conflict between _Confucianism_ and _Legalism_. Confucianists prioritized families and tradition. Legalists wanted power to be based on merit.

Huge numbers of people fought and died in these battles. In order to get the upper hand, states had to centralize their resources and power still further to send out larger and more effective armies.

These wars changed Chinese society dramatically, not least because they were funded by taxes, which forced states to construct civilian bureaucracies. Legalism lent itself more readily than Confucianism to these efforts.

Many other fundamental changes were driven by Legalism. Families were registered, and a poll tax was placed on adult males. Land was taken from noble families and redistributed communally. Measurements and written language became standardized.

In 221 BC, the wars ended with one centralized state having dominion over the rest. It was led by an ultra-powerful emperor, Qin Shi Huangdi.

Qin Shi Huangdi applied Legalist principles. Instead of maintaining the traditional royal families, he created an official class that was loyal to the state. He recruited his administrators through examinations. The bureaucracy featured salaries, promotions and career opportunities. Local tribal ties no longer had much significance.

Huangdi was also a megalomaniac dictator. He had classic Confucian books destroyed, and scholars burned alive. He forced 120,000 families closer to the capital so he could monitor them more easily.

Uprisings and protests eventually led Huangdi's dictatorship to collapse, and people returned to the family-based, more Confucian structure of previous societies.

This early conflict illuminates people's natural instinct to favor their relatives. China's empires would continue to oscillate between kin-based and merit-based social systems.

> Fact: It took 700,000 workers to build Qin Shi Huangdi's tomb, which is known today for the Terracotta Warrior statues that guard it.

### 5. Religion contributed to a strong social order in India, but it hindered state-building. 

Today, India is the world's most populous democratic state, but this is actually a relatively recent development in its history.

In India, social and religious ideas developed before political or economic ideas. Around 900 BC, _Brahmanic_ religion arose in India. It was focused on nature, rather than people's ancestors.

Brahmanic religion led to a new kind of social hierarchy. Priests were at the top, followed by warriors, then merchants, then peasants. Society became segmented into _castes_, which were independent of the state but interdependent with each other.

Shoemakers, for example, only married other shoemakers within their caste. What's more, if a confectioner squabbled with a banker, he could then ensure that tile makers didn't tile the banker's house, because the tile maker and the confectioners were members of the same broader caste and had solidarity.

Religious leaders were seen as the guardians of a sacred law, so they were superior to the state. In fact, many political leaders were illiterate. Illiteracy was high, because Brahmins controlled and limited education.

Priests could also give or take away the state's legitimacy, so even kings were subservient to them. It was nearly impossible for a centralized government to form because of this.

The lack of centralization also meant that India couldn't develop a strong military. The warrior class was a small, privileged elite that was subordinate to the priests. The priests granted them legitimacy, but they didn't organize them.

All in all, the state couldn't mobilize or control the self-governing castes and villages. When other regions took up cavalries and archery, the Indian military still used chariots and elephants. India was soon defeated by the Greeks, and later the Muslims.

Some emperors, such as Mauryan and Ashoka, did manage to unite most of the subcontinent, but never for long. Political offices remained linked to family ties. The state still lacked a central bureaucracy, standardization and infrastructure such as roads or canals. Strong institutions, like the uniform penal code, weren't introduced until the British colonial period.

### 6. Military slavery temporarily counterbalanced family loyalty in Middle Eastern states. 

So what about the Middle East? Two main imperial systems arose: the Mamluks and their successors, the Ottomans. They perfected military slavery, and for a time weakened the strength of family and tribal ties by ensuring that the military power of individuals only lasted one generation.

"Mamluk" originally referred to a kind of soldier-slave used by various Islamic sultanates. Boys with no ties to a sultanate would be captured from places as distant as Armenia or the Sudan and raised as elite soldiers loyal to the sultan. They also were assigned educators who were eunuchs. The soldier-slaves were prevented from having families, so like the powerful eunuchs who had taught them, they passed nothing on to further generations — and were fully devoted to the state.

The sultanates enacted this policy to defeat political tribalism. Power passed from slave to slave rather than from father to son, which helped the Mamluks and later the Ottomans, who adopted the same policy, to become highly centralized world powers. The Ottomans called the soldier-slaves _Janissaries_, and they ran not just the army but also the Ottoman state. They were mainly taken from Christian families and converted to Islam.

But even these practices couldn't fully crush inheritance, or prevent the decline of the empires.

In the Mamluk Empire, the powerful soldier-slaves manipulated the one-generation system by putting their descendants in charge of public institutions like schools or hospitals, and creating a degree of family loyalty as some sultans started to relax restrictions on families and inheritance.

This eventually meant that slaves served the Mamluk Sultanate, but could also contend to become the sultan. There weren't clear rules regarding succession, which led internal confusion and feuding.

The Mongols eventually took advantage of this. In 1399, they attacked the Mamluks and looted Aleppo.

The Ottomans kept the military under civilian control, and later, under pressure from a rising population aggrieved by high grain prices, they relaxed the rules regarding celibacy for Janissaries. Although the Ottomans did manage to control dynastic power among the Janissaries and the sultans themselves for a while, eventually these checks and balances failed, as did the empire itself.

### 7. Only in Europe did religion end tribalism and give individuals property rights. 

We've seen how family-centric tribalism and despotism can stagnate a society. Why didn't these tendencies occur to the same extent in Europe?

In Europe, the Catholic Church became a centralized power and broke down tribalism through a series of social norms or laws that changed the nature of families and property ownership. The church banned close kin marriage, marriage to widows of dead relatives, adoption and divorce. Traditionally, these kinds of social arrangements had helped tribal groups keep their property within their group.

The church also allowed women to own property and bequeath it to the church, which increased the amount of land the church owned.

Pope Gregory officially declared the church independent from the state, which led to the _Concordat of Worms_ in 1122. The Concordat of Worms stipulated that the Holy Roman Emperor could no longer choose the pope.

This was the start of progress toward a _rule of law:_ a standard set of laws for the state. Later, a monk named Gratian founded the first formal universities, and consolidated a single canon of laws — the _Decretum_ — in 1140. These steps centralized the state still further.

Religion-based rule of law also developed outside Europe, but it never counterbalanced the state as it did in the West.

For example, the Mamluks and Ottomans did theoretically cede rule of law to Islamic authorities. Islamic authorities, however, never checked state power as much as their Christian counterparts did. In the fifteenth century, the Ottoman sultan could pass secular laws without the approval of religious authorities.

In 1877, the Ottomans standardized _Sharia_ law as the Catholic Church had standardized canon law. It never gained the same legitimacy, however. Another failed attempt occurred in 1772, when the British East India Company tried to uniformly apply Hindu law to Indian Hindus, as if it were European ecclesiastical law. The law was much misunderstood and eventually collapsed.

### 8. A state needs legitimacy to exist, but it doesn’t require accountability or checks on power. 

How did China remain a unified state, without rule of law or political accountability?

Well, a strong state can persist without rule of law, if society views it as legitimate. In China, the dynasties derived their legitimacy from the _Mandate of Heaven_ — the idea that rulers had complete authority in exchange for maintaining social peace.

There were no written rules or elections that determined who exactly had the Mandate of Heaven. If a ruler was overthrown, however, the next person who assumed leadership would typically gain legitimacy.

In Europe, states developed various institutions and laws. People from different social classes, like aristocrats, religious leaders or townspeople, reacted differently to these changes. This would in turn determine if the state became absolutist or accountable to its subjects.

Russia, like China, also developed an authoritarian state. The Russian state confiscated property and levied high taxes on the poor.

In the sixteenth century, Ivan IV sanctioned extralegal _opirichniki_ — black-clad horsemen who terrorized people and confiscated property. 4,000–10,000 members of the aristocracy were killed in this period.

As a result, Russian elites were dependent on the state by the end of the seventeenth century. In fact, they served in the military. They entered the military at a young age, were promoted on merit, and remained with their unit for their entire life.

So revolutionary spirit wasn't fostered in Russia in this period, because of the lack of unity between non-state social groups. Nobles could exploit their serfs as much as they wanted. Nobility, gentry and state also all depended on taxing the peasants, so they restricted the peasants' movements to bind them to the land. At its uppermost levels, the Russian state was heavily influenced by family ties and patronage.

> Fact: One Russian noble, Count Sheremetov, owned over 300,000 serfs.

### 9. Highly centralized states that couldn’t control their elites used corrupt methods to fund their wars. 

Death and taxes are life's certainties. Well, unless you were wealthy in seventeenth-century France or Spain.

Western European monarchs were constrained by the rule of law in taxation, so some found other ways of raising money for war.

In the seventeenth century, France and Spain had vast amounts of war debt. People in both countries tried to fight taxation, but they couldn't unify to do so. The state became even more powerful and centralized by weakening local elites, governments and judicial courts.

Both Spain and France raised money by selling offices and titles to the highest bidder. Power became private and inheritable: the bourgeoisie could buy official titles and pass them onto their children.

France and Spain were _absolutist_, because they ruled without any truly representative assemblies that could monitor their activity, including their tax systems.

There was also no form of registration, which meant the rich could partly avoid their property taxes. The poor couldn't, however, because the agricultural and customs taxes placed on them were much harder to escape.

The two states didn't repay their debts, either. The French state even forced its citizens to exchange their coins for notes, then reduced the value of those notes. Citizens who didn't accept the reduced exchange rate were threatened with arrest.

Of course, France eventually experienced a great revolution, but the Spanish state managed to avoid one. Instead, Spain passed a legacy of family-based favoritism to their colonies in the New World.

In the New World, these elites had even fewer constraints. Without state intervention, wealthy settlers could get richer and richer by buying new land and passing it on to their families. The new elites became absentee landlords and enslaved the indigenous population. This eventually left a patrimonial legacy we can still see there today.

### 10. Accountable governments can form when elites limit state power for the good of society. 

In the 1215 _Magna Carta_, English nobility forced the monarchy into a constitutional compromise. Seven years later, a similar development occurred in Hungary, but the two country's paths were very different. Why?

Well, the English state was formed _after_ the king's protection of non-elites had already been established. The king's power was already limited.

England had already become highly centralized in 1066, after the Norman conquest. The Normans passed new laws that protected individuals against predatory landlords.

_Common Law_ meant that by 1200, tenants who were tied to their land could buy, sell and lease property without their landlord's permission. Women could also own land and make contracts without a male guardian.

The English Parliament didn't just represent the aristocrats and clergy — it also included townspeople and property owners. It approved taxes and removed incompetent officials. In fact, King Charles I once raised taxes without parliamentary approval, and he was beheaded!

The English government actively tried to reduce corruption. A new treasury bond was created, for example, and bonds were sold on the transparent public market.

After King James II was deposed in 1688, there were even more reforms. The king could no longer impose policies without the consent of Parliament.

However, constitutional limits on state power don't automatically produce political freedom. This was the case in Hungary.

In Hungary, the state was too weak and the non-state actors were too powerful. The state couldn't reform its economy or military, unlike other societies in Europe.

Hungarian peasants didn't have legal protection, so they were dominated by the noble estates. And these elites didn't grant the king authority to extract adequate taxes for defense from them.

Hungary's version of the _Magna Carta_ was called the _Golden Bull,_ but it only led to a powerless king and a ruthless oligarchy. Unable to centralize enough to defend itself, Hungary was taken by the Mongols in 1241, and again by the Ottomans in 1526.

### 11. States fail or prosper through historical accidents, institutional change and social mobilization. 

So what conclusions can we draw from all of this? What lessons does history teach us about whether states will flourish or fail?

Overall, states decay because their institutions are too rigid, too nepotistic, or both. State institutions must keep adapting themselves to changes in their social or physical environment. States that favor family and friends over impersonal meritocracy will inevitably decay.

When Manchu forces invaded Ming China, for instance, the state didn't raise any land taxes to build up their defenses. Such taxes would've mostly affected the elites, who naturally opposed them, and had the power to do so. This is an example of _dysfunctional equilibrium_.

Dysfunctional equilibrium occurs when a state defends the status quo for the benefit of certain small sectors of society, instead of enacting improvements for the _whole_ of society. Sometimes, if a state doesn't undergo serious changes just to benefit the elite, it can lead to their decline, as it did with Ming China.

Ultimately, states prosper through a combination of economic growth, social mobilization, checks, balances and ideas about legitimacy.

In England and Denmark, a balance was finally struck between the components of political order. Denmark, however, saw less prolonged violence, civil war or class conflict than England.

The Protestant Reformation of 1536 was a turning point for Denmark. The Lutheran Church established schools across the country, and encouraged literacy among peasants.

Serfdom was abolished in 1792, and peasants could then own land. Serfdom was abolished so that the monarch could weaken the landlords and conscript them into the army. Led by a Lutheran priest, farmers began a political movement that led to broader voting rights in 1849.

So, a state can "get to Denmark" or have a society that's stable, honest and inclusive through a combination of political development, religious conversion, and lucky historical accidents.

### 12. Final summary 

The key message in this book:

**Since the process of state-building began, states have had to battle people's natural inclination to favor their families. Different regions handled this in varying ways: through dictatorships, military slavery, religion-based hierarchies or other means. Ultimately, the most effective states were those that had balanced checks on state power and provided protection for common people.**

**Suggested** **further** **reading:** ** _Why Nations Fail_** **by Daron Acemoglu & James A. Robinson**

_Why Nations Fail_ revolves around the question as to why even today some nations are trapped in a cycle of poverty while others prosper, or at least while others appear to be on their way to prosperity. The book focuses largely on political and economic institutions, as the authors believe these are key to long-term prosperity.
---

### Francis Fukuyama

Francis Fukuyama is a world-renowned historian and philosopher. He is especially well-known for his 1992 book _The End of History and the Last Man_, in which he argued that liberal democracy represented the end of political evolution — a thesis he has since revised. Fukuyama is currently a senior fellow at Stanford, and he's lectured at John Hopkins, George Mason and many other top universities. He's also completed research for the _RAND_ corporation and the US State Department.

