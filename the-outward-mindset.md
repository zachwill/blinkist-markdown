---
id: 5901bb3fb238e10008b14ec1
slug: the-outward-mindset-en
published_date: 2017-04-28T00:00:00.000+00:00
author: The Arbinger Institute
title: The Outward Mindset
subtitle: Seeing Beyond Ourselves: How to Change Lives & Transform Organizations
main_color: D33E3F
text_color: B23536
---

# The Outward Mindset

_Seeing Beyond Ourselves: How to Change Lives & Transform Organizations_

**The Arbinger Institute**

_The Outward Mindset_ (2016) explains how changing your perspective can transform the world. These blinks discuss how a focus on personal needs obscures the needs of others and how, by identifying collective goals, people can make powerful collective changes in society.

---
### 1. What’s in it for me? Shift your mindset and transform lives. 

Many of us go through life with one aim: What can we do for ourselves? We make decisions based on what we can get out of a situation, often with little regard for how others fair.

However, concentrating on what we each want all the time causes many problems; instead, we should focus on what others want. Imagine you are a manager and you desperately need to improve productivity. Which tactic do you think would work best: forcing employees to put in more hours so that the metric improves or asking them what you can do to make them happier at work?

But how do you develop a mindset that puts others first? These blinks will show you the way.

In these blinks, you'll discover

  * how one police officer reduced crime by offering coffee and public restrooms; and

  * why you shouldn't force your children to play sports.

### 2. Making the right decisions in life means adopting the appropriate mindset. 

Do you ever wonder what determines the actions you take? Many people think that the things they do are just products of their personality, but the truth is more straightforward.

It's your _mindset_ that controls your behavior. Basically, your mindset is the way you think of and see the world. It goes beyond your core beliefs about yourself to encompass the way you think about everything, including how you consider and treat others in society.

Because it's so far-reaching, your mindset is hugely important. As a result, making even small changes to it can result in dramatic differences in behavior and actions.

Just take the Kansas City Police Department's SWAT team. They had long been criticized by locals for their penchant for excessive violence. For example, many citizens had complained that the team would shoot pets during raids.

Confronted by their own poor reputation, the SWAT team had no choice but to change its mindset and begin focusing on the feelings of suspects as well as their relatives.

Suddenly the team began treating people with respect. Instead of acting aggressively, they strove for cooperation. In one example, before rushing off to question the suspects, the team took the time to calm a baby in the house. This new approach really paid off and actually improved the results of their crime fighting.

So, a change of mindset can transform behavior but — best of all — it doesn't even require much effort. Just imagine a company that's in dire financial straits. To keep themselves above water they're micromanaging costs and firing people left and right while slashing employee benefits and customer service.

This mindset is based entirely on saving money, but is it really the right one for them?

Instead, they could focus on making money, which would mean making decisions that bring in revenue. They'd begin looking at developing markets and technological innovations that could potentially solve their financial problems while keeping both their employees and customers happy.

But why does so simple a change of mindset have such beneficial effects? You'll find out in the next blink.

> _"Mindset drives and shapes all that we do — how we engage with others and how we behave in every moment and situation."_

### 3. To achieve success, you must take your focus off yourself and consider the needs of others. 

When something in life goes wrong, people tend to search for excuses. Say someone has an interview for their dream job but doesn't get an offer. The candidate immediately assumes that the failure is a result of, for instance, his parents not teaching him properly, or because his interviewer didn't ask the correct questions.

Such excuses are far from helpful; they're the result of a negative _inward mindset_ that focuses solely on the individual in question and what he wants for himself. So, how does such a mindset emerge?

For starters, many people are too focused on attempting to control the situation they are in. A good example is when people try to make others behave a certain way, as a mother does when she scolds a noisy child. When such tactics fail, they grow upset and pessimistic.

At other times, people lean too heavily on outdated hierarchical structures. Say you're a manager at your company. You might develop a mindset of trying to protect your position and end up constantly seeking out faults in others, blaming them for any mistakes.

Naturally, such an approach can produce serious issues, which is why it's better to develop an _outward mindset_ or one that's focused on the needs and feelings of others. To do so, you need only concentrate on what you need from others to achieve your goals and what they need from you.

To see the difference between these two mindsets in action, imagine being a parent of two children, and every day after work, you go outside with them to shoot hoops.

If you're a parent with an inward mindset, you might think, "I always make sure my kids get a workout every night." But, if you're a parent with an outward mindset, you might ask yourself: "Do my kids want to play basketball? I'll ask them and see what they want. If not, we can find something else to do."

In other words, an outward mindset enables you to work out scenarios that fulfill the needs of both yourself and others. Next up, you'll learn how to adopt such a mindset.

### 4. Developing an outward mindset means listening to others. 

Now that you know how an outward mindset can help you make positive changes in your life, it's time to learn how to adopt one. To do so, just follow three easy steps based on the acronym _SAM_.

The _S_ stands for "seeing the needs of others." That means focusing on other people rather than yourself. This is simple as long as you consider what other people need and want from any given situation.

The _A_ is for "adjusting your efforts." Once you've identified what people need, you should take action to help them achieve it.

And the last letter, _M_, stands for "measuring the impact." This is key to determining how effective you've been. Did your actions help others in the way they actually needed to be helped? If not, change things up to make it work.

Take the example of an NGO that was desperate to bring clean water to a remote area but had no idea how to do it. By starting with S, they saw that people in the area wanted their kids to be healthy and to attend school.

From there, they moved to A, shifting their focus from supplying clean water to a broader approach of making sure children were healthy enough for school. Finally, they moved to M and began tracking the health of children in the area.

By following these three steps, you too can change your mindset. But remember to focus on yourself and avoid forcing others to change themselves.

After all, when you force other people into changing, you're actually displaying an inward mindset that's based on what _you_ want. Instead, you need to make the change for yourself and show others how it works.

Just imagine you're an executive and you want your employees to shift their mindsets. You can't just demand they do what you want — or expect them to change on their own. Rather, you have to alter your own mindset and show them that you care about their needs and desires. In the end, they'll likely catch onto your positivity and follow the example you set.

### 5. One individual with an outward mindset can impact the world positively. 

So, you can effect change in others by leading by example, but how exactly?

Well, a great way to apply the power of your outward mindset is to use it to find a _collective goal_. After all, your outward mindset offers you the opportunity to identify mutually beneficial objectives, and working toward such goals helps produce shared solutions.

Just take Kansas City, where 50 police officers were struggling to stop crime among the day laborers who would gather at a city park. The laborers saw each other as competitors for jobs, and this tension bred violence, theft and vandalism. One officer, Matt Tomasic, used his outward mindset to ask the day laborers what they needed.

All they wanted were bathrooms and hot coffee, which were easy enough to provide. By making these simple changes, the department managed to reduce tensions and dramatically cut the crime rate, and it was all thanks to Tomasic identifying a collective goal.

Both the police and day laborers got what they needed, and all it took was one person with an outward mindset.

It just goes to show that your own position is irrelevant. It doesn't matter if you're a boss, an employee, a parent, teacher or child, anyone can make positive social change by adopting an outward mindset.

Imagine you're a customer service representative. You could improve the lives of customers all by yourself without waiting for your boss.

All it would take is thinking about what customers need and how you can help them get it. They might need you to respond more rapidly or be more polite when addressing their concerns. Whatever it is, you can take an easy step to identifying a collective goal and satisfying your customers.

Once your boss and co-workers see the results of your strategy, they'll start making the same changes. So, start thinking about the needs of others. It could make all the difference in improving their lives and your own.

### 6. Final summary 

The key message in this book:

**The right mindset is key to the success of your actions. To effectively work with others you need to understand what they need and how you can collectively achieve your goals. That means seeing others, adjusting your behavior to match their needs and measuring your impact.**

Actionable Advice

**Treat your employees and colleagues right, especially those that interact with customers.**

The lowest paid and worst-treated employees, whether they work at a movie theater or a call center, tend to be those who interact with customers the most. This is a recipe for disaster. If the employees who spend the most time talking to your customers are dissatisfied, they can't possibly satisfy the customers themselves. So get rid of distinctions within your organization; take an inclusive view and an outward mindset that considers the needs of all employees.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Mindset_** **by Carol Dweck**

_Mindset_ (2006) discusses the differences between people with a fixed mindset versus those with a growth mindset. Our mindset determines the way we deal with tough situations and setbacks as well as our willingness to deal with and improve ourselves. This book demonstrates how we can achieve our goals by changing our mindset.
---

### The Arbinger Institute

The Arbinger Institute was founded by scholars and aims to spread the word about positive mindsets. The institute has produced bestselling books and courses and now has over 300 facilitators, coaches and staff members in offices in 18 countries.

