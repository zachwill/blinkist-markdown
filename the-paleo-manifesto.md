---
id: 56fee6ee7e4784000700001b
slug: the-paleo-manifesto-en
published_date: 2016-04-05T00:00:00.000+00:00
author: John Durant
title: The Paleo Manifesto
subtitle: Ancient Wisdom for Lifelong Health
main_color: F5DF61
text_color: 665D28
---

# The Paleo Manifesto

_Ancient Wisdom for Lifelong Health_

**John Durant**

_The Paleo Manifesto_ (2013) is the go-to guide to going paleo. It explains why the Paleo diet is in sync with our ancestors' diet, and how you can use humanity's anthropological and evolutionary history to get fit, feel great and lead the healthy lifestyle you've always wanted to.

---
### 1. What’s in it for me? Go paleo! 

You've probably already heard of the paleo movement. If you don't know anything about it, you're probably one of those people who thinks paleo means channeling your inner caveman, running around in loincloths with berry seeds stuck in your teeth.

If that's what you think going paleo is about, well, you're wrong! The paleo movement isn't about stone tools or nudity. It's about examining the dietary habits of prehistoric man and applying them to our modern lifestyle. 

In these blinks, you'll learn

  * what your teeth say about your diet;

  * why you should avoid eating cereal grains; and

  * what thermoregulation has to do with weight loss.

### 2. The lives we lead today are at odds with our evolutionary history. 

Despite all the incredible technological innovations of the modern world, we aren't getting any healthier or happier. 

Just compare the typical day of an average person today with that of his ancestors. For instance, before the author became a leading force in the paleo movement, he lived in New York City and was clocking endless hours at a consulting firm, day after day. 

He worked well into the night and went to the office on the weekends. He barely had time to exercise, and subsisted on take-out food. To make matters worse, he replenished his depleted energy by overloading on caffeine. 

But what about the typical day of an ancient human, without all the technology?

Our ancestors dedicated most of the day to the necessities of survival — hunting, gathering and cooking. That lifestyle is still inside of us; we're actually better suited to our ancestors' lifestyle than our contemporary technology-driven one.

For clear evidence of the negative impacts of modern living, let's look at what happened to the gorillas — our nearest primate cousins — at the Cleveland Metroparks Zoo. Even though they were kept on a strict diet according to official health guidelines, the gorillas were always sick. They suffered from obesity, high blood pressure, increased cholesterol and triglycerides and had a marked level of heart disease indicators. On top of that, they had behavioral issues, like yanking out their hair and regurgitating their food before re-ingesting it. 

However, as soon as the zookeepers replaced the processed gorilla biscuits with natural plants, the gorillas' health stabilized and their behavior normalized. 

Why?

Because they were eating what they ate in the wild, that is, what their bodies had adapted to eating through evolution.

### 3. Humans were healthier before the advent of agriculture. 

The organisms we scientifically define as "human" distinguished themselves from our evolutionary ancestors during the Paleolithic Age, which spanned more than 2.5 million years, and ended about 10,000 years ago. During this time, modern humans emerged with bigger brains nourished by a greater intake of roots, tubers and meat. 

These early humans were exceptionally healthy. 

When the author visited Harvard University's Peabody Museum of Archaeology and Ethnology, he saw the 80,000-year-old skull of a hunter-gatherer, one of the earliest modern human artifacts. And guess what? It had perfectly straight teeth, zero cavities and was likely part of a svelte body measuring five foot ten and weighing 150 pounds. 

But human health worsened drastically during the Agricultural Age (8,000 BC to AD 1769) and the Industrial Age (1769 to 1946). 

At the same museum, the author saw the skulls of ancient farmers. Their teeth were flat and full of cavities; their bodies wouldn't have been as tall or robust as the hunter-gatherer's.

How come? Well, tooth decay and stunted growth are classic symptoms of diets high in starches, and with the onset of farming, humans suddenly had copious quantities of starches like cereal grains available. 

Human health only worsened with the onset of industry. It sparked a shift toward urban life and cheap food like refined sugar and grain products. 

Urban living meant people were living in closer proximity to each other. That led to an increase in infectious diseases — from both animals and humans. 

The bad food of the era led to the British reputation for terrible teeth. British people became one of the first groups to consume large quantities of refined sugars and other sugar-rich foods such as bread, potatoes and alcohol. These are exactly the foods that lead to poor health.

The British were also one of the first cultures to move into the great indoors. This meant our once-active hunter-gatherer species was suddenly not so active. Meanwhile, sunless environments prompted diseases like rickets, which is caused by vitamin D deficiency. It was, unsurprisingly, first diagnosed around the time of the Industrial Revolution. It causes skeletal deformities, weak muscles and dental problems. 

So how can we get our bodies back to where they need to be?

### 4. We can return to a natural mode of healthy living by reclaiming the hunter-gatherer diet. 

The contemporary routine of driving to the supermarket and stuffing a shopping cart with food from all over the world couldn't be more different from the eating practices of our Paleolithic, hunter-gatherer ancestors. They consumed a much more diverse diet based on seasonal availability.

Over the course of the year, they'd consume hundreds of different plants and dozens of animals, including mammals, fish and insects. On top of that, they ate animals from "nose to tail," that is, every part of them, including the nutritious innards and bones. 

In today's Western world, however, this kind of diet is generally frowned upon. People are averse to eating organs such as the heart, the brain, the tongue and the kidneys. 

However, for those living in cultures with an unlimited variety of foods, it's essential to return to the diet of our ancestors. That means sticking to meat, fish, plants and water. 

In fact, it's pretty simple to mimic the hunter-gatherer diet from the comfort of your own home. Just be sure that most of your calories come from meat and fish and that the physical bulk of your diet consists of plants. These plants should include leafy greens, roots and tubers such as sweet potatoes, carrots, beets and onions. 

The way you prepare these essential foods is also important. After all, taking the time to experiment with food makes it all the more meaningful and enjoyable. For instance, you can add fermented foods such as sauerkraut or kimchi to your diet to naturally promote healthy gut flora. 

Superfoods like liver, eggs, seaweed and small northern fish that can be eaten whole are excellent sources of micronutrients. But remember to cook them on low heat to maintain their natural nutrients and to only use traditional fats and oils like real butter and olive oil.

### 5. What we don’t eat is almost as important as what we do eat. 

So, you're ready to be a modern hunter-gatherer. Great, but unfortunately most of what we think of as food — the stuff on grocery store shelves — would be unrecognizable to our ancestors. It's all processed!

You might've heard people say that they "don't eat processed food." What they're really saying is that they don't eat "industrial food." After all, "processed" is a misleading term. Any food not in its original, whole form has been "processed" somehow, whether it's been cooked, cured or canned. 

So, obviously, not all processed foods are bad for you. But how can you know which ones are? Here's a rule of thumb: avoid foods made using ingredients and processes that you couldn't easily replicate at home. This includes foods with added sugar, most vegetable oils and anything with corn syrup or artificial coloring or flavoring. 

It's also important to avoid cereal grains and pasteurized dairy. 

Cereal grains like wheat, corn and rice contain plant toxins that make them hard to digest. Plants actually developed these toxic proteins over time to protect their seeds, and they're big trouble for humans. In fact, wheat is the cause of gut inflammation in 80 percent of people. So while some people believe the whole-grain wheat products they eat are "healthy," in reality, such foods are associated with cancer and cardiovascular diseases. 

But avoiding pasteurized dairy is also key. Instead of eating things like skim milk and industrial yogurt, try full-fat or fermented products like real cheese, real yogurt and raw whole milk. 

Also, a diet that uniquely supports your biological makeup and whatever special needs you have, such as allergies, is essential to leading a healthy, meaningful and fulfilling life. Don't be afraid to personalize your diet by experimenting with food and seeing what works for you. 

Finally, don't let your paleo diet prevent you from basking in life's glory every now and then. You can always take a detour from the paleo trail when it's called for, like on special occasions and holidays.

### 6. Many popular dieting trends are founded on misconceptions and ideology instead of hard science. 

Plenty of people follow all kinds of mainstream "healthy" diet fads based on drastically limiting calories, eating low-fat foods or going vegetarian. The problem with these programs is that their adherents often don't understand the science behind them (or the lack thereof). 

The truth is, not all calories are created equal. For instance, lots of people try to lose weight through "calories in, calories out" methods. They figure they can burn every calorie they take in. But then they start to fear high-caloric foods, like fats. But remember, not all calories are equal: a gram of healthy fats from, say, an avocado, is healthier than a gram of carbohydrates. 

There's no need to stigmatize healthy fats, or even saturated ones like those from coconut oil. Just consider the _French Paradox_ : French people eat high-fat foods every day, but have fewer instances of heart disease and enjoy a characteristically fit physique. Knowing what you do now, though, maybe the French Paradox isn't so paradoxical after all!

Another common trend in dieting has to do with meat. Is it really as terrible as some people say?

Well, meat gets a bad rap. Too much of vegetarianism is fueled by ideology and false information. In fact, humans and our ancient ancestors began consuming meat roughly 2.6 million years ago. That's because meat contains tons of essential nutrients that we simply can't get from plants, including vitamin B12, which plays an instrumental role in the function of our brain and nervous system. 

So the reason vegetarians are often thought of as being super healthy probably has to do with their ideology, which makes them more conscious about _all_ the food they consume. For instance, they're less likely to eat industrial, processed foods. 

But when it comes to vegetarianism and all the dieting fads, it's easy to be overwhelmed and confused. Try not to be distracted from the healthiest path: a sustainable, enjoyable food experience befitting our evolutionary biology and history.

### 7. Temperature and activity are also important factors in the paleo lifestyle. 

The paleo lifestyle isn't all about food. It's also about your daily activities. After all, humans evolved in active and diverse conditions. We can — and should! — adopt some of those conditions, starting with small changes in our day-to-day lives. 

For instance, by using _thermoregulation_, that is, the regulation of internal temperatures, you can reduce inflammation and boost metabolism simply by experiencing extreme temperatures. For example, the intense heat of saunas and hot springs has been found to relax the mind, kill external bacteria and lead to lower levels of inflammation. 

On the same note, a freezing shower, outdoor exercise and swimming in cold water can reduce inflammation while burning fat through _thermogenesis_. This is the process through which the body produces heat. It works by transforming body fat into fuel that regulates the body's temperature.

Take the Olympic swimmer Michael Phelps. He eats about 12,000 calories a day. Keep in mind that running a marathon burns around 2,600 calories, so even with an Olympian's training schedule, 12,000 calories is an insane amount to burn off through mere exercise. Well, one former NASA researcher explained this discrepancy by noting the time Phelps spends in cold pools. It turns out that his body burns thousands of calories just to stay warm. 

But we aren't all Michael Phelps and most of us have desk jobs. That's why it's important to remember that our bodies weren't designed to be seated all day long. Hunter-gatherers traveled an average of six to nine miles a day, whereas the average American only walks around 1.5 miles on any given day. 

This modern lifestyle results in slower metabolisms and decreased protein levels for regulating things like cholesterol. That's why it's essential to add standing to your work day. After all, you spend most of your time at work, so adding a standing desk can lead to drastic improvements in both your energy and concentration.

Even just a little change, like standing up for an hour a day, will get you on the path to a healthier paleo lifestyle. Your body will thank you!

### 8. Final summary 

The key message in this book:

**The way we eat and live in the modern world is extremely unhealthy. But there is another option: to eat and live like our ancestors did in the Paleolithic era. If we all adjusted our modern lifestyle to be more like our ancestors, we'd live happier and healthier.**

Actionable advice:

**Take a mid-day walk.**

Getting out of the office is essential to maintaining your sanity, so why not make a habit of going for an afternoon stroll to load up on sunshine and get some exercise? The vitamin D you'll absorb from the sun and the physical activity will trigger natural hormones in your body that wake it up and let you return to work rejuvenated, refocused and more likely to retain information.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Bulletproof Diet_** **by Dave Asprey**

_The Bulletproof Diet_ (2014) shows you how to hack your body to optimize your health, lose weight and increase your mental functioning. How? Maintain a diet filled with the right proteins and fats, and learn to exercise and sleep in the most effective way possible.
---

### John Durant

John Durant is a leading figure of the paleo movement. He is the founder of Paleo NYC and Barefoot Runners NYC, and blogs at HunterGatherer.com. He's been featured on _The Colbert Report_, NPR and in _The New York Times_.

