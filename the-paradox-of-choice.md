---
id: 530c801e3462310008690000
slug: the-paradox-of-choice-en
published_date: 2014-02-25T08:00:00.000+00:00
author: Barry Schwartz
title: The Paradox of Choice
subtitle: Why More Is Less
main_color: FCE362
text_color: 665C28
---

# The Paradox of Choice

_Why More Is Less_

**Barry Schwartz**

The abundance of choice that modern society presents us with is commonly believed to result in better options and greater satisfaction. However, author Barry Schwartz argues that too many choices can be detrimental to our psychological and emotional well-being. Through arguments based on current research in the social sciences, he demonstrates how more might actually be less.

---
### 1. What’s in it for me? 

In today's affluent society, we're faced every single day with an endless array of choices, from the clothes we wear to what we eat for lunch. It's this very choice which gives us fulfillment and frees us to be who we are. Or so we might think.

_The_ _Paradox_ _of_ _Choice_ turns this popular belief on its head, and argues that being confronted with an abundance of choice can be so demanding that it causes psychological distress, making it hard for us to choose.

When we do finally make a decision, the mere existence of other options actually starts to hurt us. In this book of blinks, you'll see how and why they diminish the pleasure we get from our choices.

Thankfully, _The_ _Paradox_ _of_ _Choice_ also shows us how we can avoid the negative effects of choice overload by seeking out some measure of appropriate constraint. Suggestions are offered concerning how to simplify decision making and become satisfied with the choices we make.

### 2. The range of choices people face every day has increased dramatically in recent years. 

Not more than a few decades ago, choice in most areas of daily life was actually rather limited.

For example, just one generation ago, all utilities were regulated by monopolies, so consumers didn't need to make difficult decisions about who was going to provide their telephone or their electric service. And when it came to choosing an education, colleges usually required all students to complete two years' worth of general education, with only some, rather narrow choices available among the courses.

But as society has advanced, the array of choices in everyday life has increased enormously. We now face a demand to make choices that is unparalleled in human history.

Today, for instance, colleges are like intellectual shopping malls, embodying a philosophy that celebrates freedom of choice above all else. Even Swarthmore College, a small school with only 1,350 students, offers about 120 different courses to meet the general education requirement, from which students must select just nine. In fact, in most modern colleges, students are free to pursue almost any of their interests.

Such abundance of choice also applies elsewhere — in utility providers, for example, where deregulation and competition in the telephone and power industries have introduced a dizzying array of options. And we're also now presented with a massive selection of different kinds of health insurance, retirement plans and medical care.

In fact, it seems that no matter which aspect of everyday life we turn to, the amount of choices available to us has increased over the past decades.

So whether we are choosing utility provider or deciding on a career path, contemporary society presents us with a bounty of choices.

### 3. The more options we have, the harder it becomes to make a good decision. 

"Should I kill myself, or have a cup of coffee?" asked the existentialist philosopher Albert Camus, making the point that in every area and every moment of our lives a choice is waiting to be made.

Not only that, but there are always alternatives to our choices. Luckily, however, most of our actions are so automatic that we don't really contemplate the alternatives. Therefore, there is little psychological reality to this freedom of choice: as choices go, putting underwear on and brushing our teeth don't really count.

But today, we are constantly offered new options that require more effort from us than ever before.

New options within finance and healthcare, for example, demand extensive research, and most people simply don't feel they have even the most basic skills or knowledge to make wise, informed decisions about such complex areas of life.

For instance, not long ago the only health insurance you could choose was Blue Cross. But now the choice of health insurance programs and providers has become incredibly complicated, and a person who fully understands what their health insurance covers is a rare bird indeed.

Moreover, being faced with such demanding choices puts a huge burden of responsibility on the individual. Indeed, the last decades have seen burgeoning support for and confidence in the free market, shifting the burden of decision making away from the government and onto the individual.

That's all very well for the more trivial financial decisions in life. But when it comes to choosing the right health insurance, retirement plan or medical care, the stakes for the individual are astronomical. For example, a bad decision by a senior citizen can bring complete financial ruin, forcing the person to prioritize between food and medicine.

The increasing effect of these demanding choices, where we ourselves have the ultimate responsibility, makes it harder to choose wisely and can transform our freedom of choice into a crushing burden.

### 4. The more options we have, the more likely we are to make a mistake. 

Knowing what we want essentially means being able to anticipate how one choice or another will make us feel. Though this sounds simple, it's actually a very difficult task.

Even when choosing from just a handful of alternatives, people's decision making is susceptible to error. This is due to the fact that our choices are partially governed by our memories, which are often biased.

As the psychologist Daniel Kahneman has shown, how we remember a past experience depends almost entirely on how the experience felt to us when it was at its best or worst, and when it ended.

If, for instance, you reminisce about a trip you took, your impression of the trip will likely be dominated by the best/worst experience — for example, fighting with your spouse — and the way the trip ended: for example, the final day's weather.

Also, our predictions about how a choice will make us feel are rarely accurate. This was demonstrated in a study where researchers asked college students to choose a series of snacks to eat during the break of their weekly seminar.

One group got to choose for one week at a time, so they simply had to know what they felt like eating at that moment. The students chose their favorite snack, which remained the same every week.

But another group were asked to pick a snack for each of the next three weeks, and these students picked a variety, wrongly suspecting that they would tire of their favorite.

Consequently, those students forced to predict what they would feel like eating over the next three weeks turned out less happy with their choice.

This tendency to make errors can only worsen as the number and complexity of decisions increases. So if those students in the above example had to choose between hundreds, instead of dozens of snacks, they would've had an even harder time predicting what they'd want.

But not only does having more options make it more difficult to choose well, it also robs us of the satisfaction we eventually get from our choice, as you'll find out next.

### 5. The more options we have, the less satisfaction we get from our decisions. 

Say you're choosing a vacation: What about touring in northern California? Or would you rather spend a week at a beach house on Cape Cod?

Whichever you choose, the decision involves passing up the opportunities the other option would've provided.

This is the so-called _opportunity_ _cost_, and it's an essential part of decision making. For instance, an opportunity cost of vacationing in Cape Cod is being able to visit the great restaurants in California.

Unfortunately, opportunity costs reduce our overall satisfaction in the choices we end up making.

This was confirmed by a study which asked people how much they would pay for subscriptions to popular magazines. Some participants were shown an individual magazine, while others saw the same magazine alongside others. In almost every case, the respondents placed a lower value on the magazine when they saw it next to the others.

So, whenever we have to make decisions involving opportunity costs, we'll feel less satisfied with our choice than we would if the alternatives were unknown to us. And the more alternatives there are, the greater our experience of the opportunity costs, and the less pleasure we'll derive from the choice we ultimately make.

Consider this study: Two groups encountered a variety of jams at a sampling table. One group could sample only 6 different jams, and the other group, 24. The group who could sample from the larger array were much less likely to ultimately buy one of the jams than the group that were presented with just six.

Why?

As the chooser narrows her choice down to a particular jam, the various attractive features of all the jams not chosen accumulate to make the chosen jam seem less exceptional. So, the more jams, the greater the opportunity costs, and the less attractive the chosen jam will seem.

As this shows, increased choice reduces both our power to decide and any pleasure to be gained from what we actually choose.

### 6. We get used to things, and as a result our choices rarely make us feel as good as we expect. 

When was the last time you bought something really nice? Let's imagine it was a magnificent electronic device that you spent a long time deciding on. If you are like virtually every other living being, your satisfaction with the device fell a bit flat after a while.

Human beings, like all other animals, respond less and less to any given event as the event persists — we simply get used to things.

This process is known as _adaptation_, and it's a prevalent feature of human psychology.

For instance, a small-town resident who visits Manhattan might be overwhelmed by the commotion, but a New Yorker, who's fully adapted to it, is blissfully unaware.

Unfortunately, because of this process, any enthusiasm we might have about a positive experience won't sustain itself as long as we think it will.

For example, consider our adaptation to pleasure ("hedonic adaptation"). If an experience boosts our sense of pleasure by, say, 20 "degrees" at the first encounter, it may boost it by only 15 the next time, and by 10 the time after that. Eventually, the experience may even stop boosting it at all.

In a famous example of hedonic adaptation, a study asked both apparently lucky and unlucky respondents to rate their happiness. Some had won between $50.000 and $1 million in state lotteries within the last year, while others had become paraplegic or quadriplegic as a result of accidents.

Results showed that the lottery winners were no happier than people in general, and that the accident victims still judged themselves to be happy (though somewhat less happy than people in general). This demonstrated that people adapt to even the best and worst of fortunes.

So even though you might expect the purchase of a new computer to give you an endless amount of deep satisfaction, any positive experience you might derive from it probably won't last very long.

### 7. The overwhelming amount of choice contributes to the epidemic of unhappiness in modern society. 

It seems that as American society grows wealthier, and Americans become freer to pursue and do whatever they want, they get less and less happy.

Consider the fact that the American GDP — a primary measure of prosperity — has more than doubled in the last thirty years, while the American "happiness quotient" has been in constant decline.

In fact, the number of people describing themselves as "very happy" has nose-dived during the last thirty years, the most dramatic manifestation being the increased prevalence of clinical depression. Indeed, by some estimates, depression was about ten times as likely in the year 2000 as it was in 1900.

So, what's behind such widespread unhappiness?

Put simply, we're spoiled for choice.

When we're presented with seemingly unlimited options but the choices we actually make turn out disappointing, we tend to blame ourselves — which produces genuine suffering.

As psychologist Martin Seligman has discovered, failure or lack of control leads to depression if a person explains the cause for the failure as _global_ ("I fail in all areas of life"), _chronic_ ("I will always be a failure") and _personal_ ("It seems to be only _me_ who always fails").

This type of excessive self-blame thrives in a world of unlimited choice. It's much easier to blame yourself for disappointing results in such a world than in one where options are limited. This is because when we are allowed to be the masters of our fates, we automatically expect ourselves to be. And then, apparently, we have no one to blame but ourselves.

What this amounts to is that the proliferation of possibilities afforded by modern life, coupled with the emphasis on our freedom of choice, can mean that we blame ourselves excessively when we fail to choose wisely.

And since excessive self-blame can lead to depression, there is good reason to believe that our society's abundance of choice is correlated with the modern epidemic of unhappiness.

### 8. Choices are more demanding and less fulfilling if you’re a maximizer: someone who seeks and accepts only the best. 

Imagine that you're shopping for a sweater. If you aspire to make the absolutely best purchase that can be made, and therefore feel the urge to check out the alternatives to be sure that you've found just the right sweater, you might be a _maximizer_.

As a decision strategy, maximizing is an overwhelming task since maximizers aspire to choose only the best. If you're a maximizer, every option has the potential to snare you into an endless tangle of considerations.

For example, since there are endless possibilities out there, and only the best will do, maximizers necessarily spend a long time on product comparison, both before and after they make a purchasing decision.

In fact, studies conducted by the author and his colleagues show that, when faced with a choice, maximizers also exert much effort on trying to imagine all other possibilities — even those alternatives that are only hypothetical. For instance, when confronted with a choice between a warm, light cashmere sweater and a cheap one, the maximizer will be very quick to imagine finding a hypothetical cheap cashmere sweater.

Not only do maximizers overwhelm themselves in this way, but once they've finally overcome the difficulty of choosing, and actually make their choice, they're more likely than others to feel unsatisfied with it.

For this reason, maximizers are especially susceptible to "buyer's remorse." For instance, a maximizer who succeeds in buying a great sweater after an extensive search will nevertheless be irked by the options they didn't have time to investigate. Their imagination of "what might have been" takes over, making the item they have chosen less attractive.

In a world of infinite choices, it is difficult and emotionally exhausting to be a maximizer, never settling for less than the best.

But, as we'll see next, you don't have to continue being a maximizer. There's a simple choice you can make and it will allow you to live a happier existence: become a _satisficer_.

### 9. Choices are less demanding and more fulfilling if you are a satisficer: someone who’s able to settle for “good enough.” 

We all know people who can choose quickly and decisively. These people are _satisficers_ and they're characterized by having a certain standard they adhere to when choosing, instead of having "the best" as their goal.

Satisficing is a fairly simple decision strategy — it means searching until you find the option that meets your standards, and stopping at that point.

A satisficer's world is divided into two categories: options that meet their standard and options that don't. So when making a choice, they only have to investigate the options within the first category.

For instance, a satisficer looking to buy a new sweater will settle for the first sweater she finds that meets her criteria of fit, quality, and price. A satisficer is not concerned about better sweaters or better bargains just around the corner.

But, aside from saving time, what's the advantage in satisficing?

Satisficers are more happy with the choices they make, and — importantly — they're also more happy with life in general.

Because, satisficers don't compare among endless alternatives when choosing, they don't experience the decrease in satisfaction that comes from contemplating what the other options might have afforded them.

And since they don't strive for perfection when making decisions, they won't spend time thinking about the hypothetical perfect world in which options exist that offer complete satisfaction.

This makes it much easier for them to be satisfied with their choices, and with life in general. In fact, in questionnaires measuring happiness and optimism, satisficers are consistent high-scorers.

Faced, as our society is, with endless choices, you'd be fortunate to be a satisficer, since the number of available options won't have a significant impact on your decision making. The good news is that most of us have the capacity to be satisficers, even those who consistently feel overwhelmed by choice. All that's required is to let go of any expectation that "the best" is attainable.

### 10. Our social relations and psychological well-being improve if we embrace certain voluntary constraints on our freedom. 

The unlimited freedom of choice in ever more aspects of life might make us lonely and cause us more distress than we realize.

As a society, we might earn and spend more money than ever before, but we also spend less time with the people around us. Indeed, the political scientist and author Robert Lane explains that our increased affluence and freedom is costing us a substantial decrease in the quality and quantity of social relations, which causes a significant decline in our well-being.

Such relations are essential to our psychological health, even if they bind and constrain us to an extent. In fact, commitment and belonging to social groups and institutions is almost a vaccine against unhappiness.

Consider the tightly knit traditional community of the Amish people. The incidence of depression among their members is less than 20 percent of the national rate — a result of their strong community membership.

But establishing and maintaining meaningful social relations requires letting go of our perceived freedom of choice and a willingness to be somewhat bound or constrained by those relations. For instance, significant social involvement in families, deep friendships, civic associations and the like, means subordinating the self in order to keep the connections strong.

But how can we achieve that? By using rules to constrain ourselves and limit the decisions we face, we can make life more manageable and decrease the likelihood of psychological distress.

For instance, if you adopt the rule that you will never cheat on your partner, you can eliminate painful and tempting decisions that might confront you later on. But you need to have the discipline to live by those rules.

Since unrestricted freedom can impede the individual's social relations and pursuit of what he or she values most, it seems like some degree of constraint would make everyone better off. By working on restricting our options, we would be able to choose less and feel better.

### 11. Final Summary 

The key message in this book:

**Everyday decisions have become increasingly complex due to the overwhelming range of choices that modern society presents us with. As the number of choices increases, so do the negative effects choice can have on our psychological well-being. The more options we have, the more difficult it becomes to make a wise decision, and the less satisfaction we will derive from what we actually choose. Therefore, it seems like some degree of voluntary constraint would make everyone better off. By simply choosing less, chances are that we would be more happy.**

Actionable advice from the book:

**Review your decision making**

A simple exercise can help you restrict your options so you are able to choose less and feel better: First, review some recent decisions you've made, both big and small. Then, itemize the steps, time, research and anxiety that went into making those decision. This will give you an overview of the costs associated with the different kinds of decisions you make and help you establish future rules governing how many options to consider, or how much time and energy to invest in choosing.

**Become a _satisficer_**

Appreciating and embracing "good enough" will simplify decision making and increase satisfaction. Therefore, think about occasions in life when you've settled comfortably for "good enough," and closely examine how you chose in those areas. Then, start cultivating this "satisficing" strategy in more and more aspects of life.
---

### Barry Schwartz

Barry Schwartz is an American psychologist and professor of Social Theory and Social Action at Swarthmore College. He has published several other books, including _The Costs of Living: How Market Freedom Erodes the Best Things in Life_, and his articles have frequently appeared in _The New York Times_, _USA Today_, and _Scientific American_.

