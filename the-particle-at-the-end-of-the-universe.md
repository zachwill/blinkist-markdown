---
id: 53fc92223731620008470000
slug: the-particle-at-the-end-of-the-universe-en
published_date: 2014-08-26T00:00:00.000+00:00
author: Sean Carroll
title: The Particle at the End of the Universe
subtitle: How the Hunt for the Higgs Boson Leads Us to the Edge of a New World
main_color: DF496F
text_color: BF3F60
---

# The Particle at the End of the Universe

_How the Hunt for the Higgs Boson Leads Us to the Edge of a New World_

**Sean Carroll**

_The_ _Particle_ _at_ _the_ _End_ _of_ _the_ _Universe_ gives you a crash course in particle physics by explaining the basics of what has become known as the "standard model." The book also details the fascinating and exciting journey that eventually led to the discovery of the elusive Higgs boson.

---
### 1. What’s in it for me? Learn about the bizarre fabric that holds our universe together. 

Over the last five years, you may have heard news about the awe-inspiring Large Hadron Collider, developed by the European Organization for Nuclear Research (CERN) near Geneva.

But what is it exactly? And why is it important, not only to physicists, but to the world?

To answer these questions, you'll need to understand some of the fundamental principles that define how our universe functions, as well as learn the basics of subatomic physics.

And that's exactly what you'll learn in _The_ _Particle_ _at_ _the_ _End_ _of_ _the_ _Universe_.

You'll submerge yourself in the bizarre world of atomic interactions, all the way down to the very tiniest of particles. You'll also grasp why these particles are important, both in how they give our universe its form as well as how the discovery of these particles has advanced civilization.

With these basics, you'll then be ready to follow the science team at CERN in their recent landmark discovery of one of the most elusive of fundamental particles: the Higgs boson.

In these blinks, you'll learn

  * why Angelina Jolie will never reach the buffet because of her social "mass";

  * why some quarks are "charming" and others "strange"; and

  * how scientists were able to prove the existence of the elusive Higgs boson.

### 2. Atoms, the building blocks of ordinary matter, are made of protons, neutrons and electrons. 

From the very beginning, we've wondered what exactly our bodies are made of. Modern science has revealed that everything — including you — is comprised of tiny particles or building blocks called _atoms_.

And these building blocks are composed of smaller, subatomic particles: _protons,_ _neutrons_ and _electrons_.

Every single atom has a unique number of protons in its nucleus — its _atomic_ _number_. This number can be used to identify the atom on the periodic table, first published by Dmitri Mendeleev in 1869.

For example, the atom _Helium_ has two protons in its nucleus, and is thus identified with the atomic number "two."_Plutonium_, on the other hand, has 94 protons (a "heavier" atom) and can be found on the table at number 94.

In 1913, Niels Bohr made an important contribution to our understanding of the atom with his atom model. He found that electrons "orbit" around the nucleus and its protons and neutrons, much like the moon orbits the earth.

Protons and electrons differ in two important ways: charge and weight. Electrons are negatively charged and relatively light compared to protons, which are positively charged and 1,840 times as "heavy" as electrons.

An atom is the smallest unit for certain chemical elements. Sometimes, however, atoms can join together and form what's called a _molecule_. Many common substances, such as water or carbon dioxide, are actually molecules, or a specific combination of atoms stuck together.

For example, when two hydrogen atoms join with an oxygen atom, they create a water molecule. You can think of this as the tiniest possible drop of water.

As miniscule as atoms are, however, scientists soon came to know of an even stranger, tinier world within the protons, neutrons and electrons themselves.

### 3. In the twentieth century, scientists discovered the tiny particles called leptons and quarks. 

If atoms are the building blocks for matter, what are the building blocks for atoms? As it turns out, scientists have discovered more, even smaller subatomic particles.

But how did they discover something so tiny?

It all started when scientists examined how neutrons decay. When a neutron "dies," it emits electrons. Yet when scientists examined these electrons, they discovered that the energy released didn't match the energy of the original neutron.

In fact, they found _less_ energy, meaning some had somehow disappeared. But where did that other energy go?

In 1930, the Swiss scientist Wolfgang Pauli found the answer. As well as emitting electrons, decaying neutrons also throw off another tiny particle called a _neutrino._

Neutrinos and electrons both belong to a family of light particles called _leptons._ Once they looked closer, scientists began to discover a myriad of different leptons.

In 1936, when American physicists Carl Anderson and Seth Neddermeyer studied cosmic rays, they uncovered yet another lepton — the _muon_.

But it didn't stop there. In 1962, Leon Lederman discovered that there were in fact two kinds of neutrinos: one that interacts with electrons (the so-called electron neutrino) and another that interacts with muons (the so-called muon neutrino).

And then, in the 1970s, the _tau_ particle was discovered along with a corresponding tau neutrino, bringing the family of leptons up to a total of six particles.

Along with these leptons, scientists discovered heavier subatomic particles as well: _quarks_.

Similar to leptons, there are six different kinds of bizarrely named quarks: up and down quarks, charm and strange quarks, and top and bottom quarks.

These quarks are classified by their electrical charge: up, charm and top quarks are all positively charged, while down, strange and bottom quarks are negatively charged.

You can think of quarks as the building blocks of protons and neutrons: just as every atom in the periodic table has a specific number of protons, each proton consists of a specific set of quarks.

### 4. Our universe is held together by gravity, electromagnetism and strong and weak nuclear forces. 

What would happen if you jumped out a window? Would you soar off toward the horizon, or glide down like a leaf?

Well no. Instead, you'd hit the ground with a loud _thud_.

This is because of _gravity,_ a phenomenon first illuminated by Isaac Newton in the 17th century.

Gravity is the most well-known of the four fundamental forces in the universe. The other, less well-known forces are electromagnetic force, along with strong and weak nuclear forces.

If you've ever used a magnet to secure your to-do list on the refrigerator, then you've witnessed _electromagnetism_. One pole of the magnet attracts the opposite pole of another magnet; similar poles repel each other.

Electromagnetic force is important because it is responsible for the structure of the atom. Negatively charged electrons are attracted to the positively charged nucleus of an atom, thus giving the atom its shape.

The next force, the _strong_ _nuclear_ _force_, interacts with quarks, ensuring stability within the atom, and is the reason why atoms exist.

Consider for a moment that the many protons in the nucleus of an atom are all positively charged. Electromagnetic force suggests that these protons should repel each other, tearing the nucleus apart. And yet they don't.

Within each proton are three quarks, all governed and held together by the power of strong nuclear force. In fact, strong nuclear force is around 137 times stronger than electromagnetism, and is thus able to hold the nucleus of the atom together.

The last fundamental force is the _weak_ _nuclear_ _force_, which is responsible for radioactive decay and nuclear fusion. This is the force that causes the sun to burn by fusing hydrogen atoms into helium, resulting in a huge release of energy, which is then carried through the solar system via photons.

These forces are all critical for our understanding of our universe, and indeed for our universe itself. Without them, we simply would not exist.

### 5. Interactions with the Higgs field give every particle its mass. 

Have you ever thought about why some things are heavier than others? The answer lies in understanding _mass_.

You can think of mass as the resistance that you feel when you push against an object. For example, pushing a car up a hill is far more difficult than pushing a bicycle. The essential difference is that the car has more mass.

But where exactly does this mass come from? It is derived from the interactions of particles within the _Higgs_ _field_.

Recall what we learned earlier, that protons have more mass than their counterparts, the electrons. A proton's greater mass indicates a strong interaction with the Higgs field, while electrons, having less mass, have a weaker interaction with the Higgs field.

In fact, the Higgs field is necessary for mass to exist at all. Without it, all particles would have zero mass and life wouldn't be possible. A "heavy" statement — but what does it mean?

Quantum mechanics shows us that heavy particles — that is, those that have lots of mass — can squeeze into small regions in space, while lighter particles take up more space. Reducing the mass of a particle to zero would mean that atoms would be far too large to interact and sustain life.

The Higgs field, as well as the four fundamental forces — gravity, electromagnetism, strong and weak nuclear force — are themselves made up of even tinier particles, called _bosons_.

Much like a droplet of water is comprised of countless water molecules (made up of countless combinations of hydrogen and oxygen), the fundamental forces are made up of countless bosons.

Gravity's bosons, for example, are called gravitons. Those that create electromagnetism are called photons.

The Higgs field is no exception, and has its own boson: the _Higgs_ _boson._ This boson plays a very important role in the composition of our universe.

Unlike the four fundamental forces, which exert no influence on empty space where nothing exists, the Higgs field permeates the entire universe — including empty space — meaning that the Higgs boson is essentially everywhere.

> _"If an electron's mass were turned all the way down to zero, there wouldn't be atoms at all."_

### 6. Think of the Higgs field as a sea of party guests keeping you from reaching the buffet. 

All this talk of quarks, bosons and muons may leave you feeling a bit overwhelmed. Particle physics is not an easy subject, and keep in mind that some of the most talented people on the globe spend their entire lives deciphering how all these elements work together.

To simplify, let's return to the concept of the Higgs field and consider two analogies.

Imagine, for example, that you're at a party where one of the guests is Angelina Jolie. You and Angelina decide simultaneously to grab a snack at the buffet. Who is going to reach the shrimp platter first?

Likely, it will be you. Angelina Jolie will probably be stopped on the way by various guests wanting to talk to her. On the other hand, people are less likely to try and get _your_ autograph. (Unless you happen to be Brad Pitt, for example.)

If Angelina Jolie were a particle, you could say she was more _massive_ than you, due to her stronger interactions with party guests, who represent the Higgs field.

Or think about it another way. Imagine you and a fish are swimming in the sea. Who do you think is the better swimmer?

Obviously, you're probably less agile than your fishy friend. The fish's scales are streamlined and suited perfectly to the water, and thus the fish can glide through the sea with minimal effort.

Your skin, on the other hand, puts you at a huge disadvantage. The surface of our skin creates far more friction with water than scales do. You can try, but you won't swim with as much grace as the fish.

In this example, the sea is the Higgs field: rather than gliding easily _through_ the water, the water slows you down, essentially giving you "mass."

Now that you know what a particle is and how it works, you're ready to start the hunt for the elusive Higgs boson.

### 7. Scientists built the enormous Large Hadron Collider to learn more about tiny particles. 

By the time it was turned "on" in September 2008, the Large Hadron Collider outside Geneva had already generated quite the buzz not only in the scientific community but worldwide.

But what does this machine do, exactly?

The Large Hadron Collider (LHC) is a _particle_ _accelerator_, smashing particles into each other at amazing speeds, giving scientists the chance to observe the crash and measure the results. These sorts of experiments are vital to our understanding of how the universe began, and was constructed.

The LHC, however, was not the first project of its kind. Another particle accelerator, the Stanford Linear Accelerator Center (SLAC), in California, played a major role in our discovering the tau lepton and the charm quark.

At two miles long, the SLAC is the third-longest structure in the world, after the Great Wall of China and the Ranikot Fort in Pakistan! However, it lost its place as the world's most powerful particle accelerator with the inauguration of the LHC.

The LHC is shaped like a ring and works by circulating some protons clockwise and others counterclockwise. Once protons reach a desired speed, they are then smashed into one another.

To prevent the protons from crashing too soon, they are held along a path by incredibly strong supermagnets. The supermagnets in turn are powered by massive electric currents.

The necessary electrical current is transmitted via electrical wire, which, when transmitting so much power, is in serious danger of melting. To keep the wires cool, the LHC uses liquid helium, cooled to -456° F (-235.6° C).

Despite these precautions, even the slightest system issue can cause the helium to heat up, resulting in a "quench." This happened in the early days of the collider, when six tons of liquid helium were released into the acceleration tunnel as a result of a faulty electrical connection.

Thankfully, nobody was hurt.

Considering all the dangers and complexities of such a system, what exactly do scientists hope to gain from building a giant, atom-smashing machine in the first place?

> _"One out of 16 passengers passing through Geneva airport is somehow associated with CERN."_

### 8. By smashing particles into each other, scientists hoped to find proof that the Higgs boson exists. 

What happens when two cars collide? In the crash, the car's parts — screws, shards of glass, pieces of plastic — are tossed about, revealing material usually hidden beneath the car's frame.

This example illustrates what scientists seek when they smash particles together in the LHC. The hope is that, rather than screws and shards of glass, the collision of two protons might reveal a Higgs boson.

But how?

At this point in our story, the Higgs boson exists only in theory. No scientist had been able to verify its existence just yet.

Further complicating the search for the Higgs boson is its short "lifespan." Even if the LHC were to generate one, it would only survive for one ten-billionth of a trillionth of a second, far too short a time for detectors to catch it.

LHC scientists thus hoped to find evidence of the Higgs boson by searching for particles that are thrown off as the Higgs boson decays.

To do this, they designed two large experiments: one called CMS and one called ATLAS. Both experiments were looking for evidence of the Higgs boson, but incorporated different detection mechanisms to minimize the possibility of skewed results based on freak errors.

Within each detection mechanism, scientists created specialized layers to hone in on specific particles, with the hope that some of those particles might suggest the presence of the elusive Higgs boson.

The specialized first layer is called the inner detector, and records the paths of the emerging particles as precisely as possible.

The next layers are two _calorimeters,_ responsible for measuring energy. The first, the electromagnetic calorimeter, is able to capture photons and electrons, while the second, the hadron calorimeter, catches heavier particles, such as neutrons and quarks. The last layer, the muon detector, catches muons.

Once all the technical equipment was built and ready to go, scientists then began the painstaking process of seeking the Higgs boson.

> _"When the LHC is going full steam, there are a total of about 500 trillion protons circulating in two beams."_

### 9. Scientists at the Large Hadron Collider finally discovered the elusive Higgs boson in 2012. 

Once the Large Hadron Collider performed these groundbreaking experiments, scientists could finally begin their search for evidence of the Higgs boson. It wasn't easy at first.

To verify whether the Higgs boson actually had been discovered, scientists first needed to sift through reams of statistical analysis from the experiments.

To do this, they examined the data and asked themselves, "How likely is it that all of the particles we've detected were generated without the decay of a Higgs boson?"

The scientists used a process called _falsification_, in which you actually try to disprove your own hypothesis. This is a foundational part of the scientific method, and is far superior than only trying to prove something affirmatively.

For example, imagine you've only ever seen white swans. If you never see a non-white swan, the hypothesis "all swans are white" will naturally seem true.

However, one day you see a black swan with your own eyes. Your hypothesis has been disproved — and now you know for _certain_ that not all swans are white!

In their search for the Higgs boson, scientists started with a hypothesis they could falsify by determining which particles _weren't_ created by a decaying Higgs boson. After testing and analyzing, they finally felt comfortable "disproving" their hypothesis and declaring the particle found.

This discovery had been a long time coming. In fact, as far back as December 2011, the two project teams announced that they had found _evidence_ for the existence of the Higgs boson, but the data was insufficient to claim an actual discovery.

After years of tests, and more than 50 years after the Higgs boson was first theorized, the LHC teams finally had the evidence they needed to announce that the Higgs boson was found on July 4, 2012.

But now we're left with a more complicated question: what can we actually _do_ with the discovery of the Higgs boson?

### 10. The discovery of the Higgs boson may open new doors in both science and technology. 

You might be asking, "What was the point of all this? Why did scientists sink so much time and money into looking for the Higgs boson?"

Importantly, the discovery of the Higgs boson can help us better understand some of the deeper mysteries of the universe.

All the particles and forces outlined in the previous blinks explain different parts of what physicists call the _standard_ _model_, which helps to account for interactions in "ordinary matter" (leptons and quarks), as well as bosons (the four fundamental forces and the Higgs boson).

However, when scientists calculate the total amount of matter in the universe and the total amount of "ordinary matter," they find that these numbers differ, suggesting the existence of another sort of matter: _dark_ _matter_.

One way that the Higgs boson might help us to detect dark matter is by measuring whether matter is generated as the particle decays. However, scientists would first need to develop instruments that could detect dark matter before such measurements could be made.

What's more, the discovery of the Higgs boson might serve as the basis for new technology. While such applications might not be obvious at first, history has shown that they will indeed be relevant.

Consider Einstein's general theory of relativity. While not immediately obvious, his theory is useful for the GPS technology you might have in your smartphone. Your phone communicates with satellites in orbit, and timing these signals helps to determine your position on the ground.

To make this work, though, you first have to understand that clocks in orbit tick a tiny bit faster than those on earth — a fact we know thanks to Einstein's theory.

What will Higgs boson reveal to us? The myriad possibilities are still to be discovered. Only time will tell.

### 11. Final summary 

The key message in this book:

**The** **discovery** **of** **the** **Higgs** **boson** **represents** **a** **great** **leap** **forward** **in** **our** **understanding** **of** **the** **universe.** **It** **completes** **the** **"standard** **model"** **of** **particle** **physics,** **and** **offers** **us** **new** **ways** **to** **creatively** **approach** **some** **of** **the** **deeper** **questions** **about** **how** **our** **universe** **functions.**

**Suggested** **further** **reading:** **_A_** **_Universe_** **_from_** **_Nothing_** **by** **Lawrence** **M.** **Krauss**

_A_ _Universe_ _from_ _Nothing_ outlines new scientific theories and observations that demonstrate how the universe could have spontaneously arisen from nothing. The author makes the case that this event is not only plausible, but inevitable, and presents evidence to show how the universe began and evolved — and theorizes about its ultimate end.
---

### Sean Carroll

Sean Carroll is a theoretical physicist with a Ph.D. from Harvard University and works at the California Institute of Technology. In addition to his research, Carroll also wrote the critically acclaimed book _From_ _Eternity_ _to_ _Here_, a scientific exploration of the nature of time.

