---
id: 5717a2613f35690007554e04
slug: the-patient-will-see-you-now-en
published_date: 2016-04-26T00:00:00.000+00:00
author: Eric Topol
title: The Patient Will See You Now
subtitle: The Future of Medicine is in Your Hands
main_color: CD4747
text_color: B23E3E
---

# The Patient Will See You Now

_The Future of Medicine is in Your Hands_

**Eric Topol**

The medical world is on the brink of a revolution thanks to new and future technology like Big Data health maps and bacteria scanners that can attach to smartphones. Power is shifting from the doctor to the patient, and self-treatment and self-diagnoses are becoming unprecedentedly powerful. _The Patient Will See You Now_ (2015) outlines these changes and what they mean for both you and the healthcare world.

---
### 1. What’s in it for me? Get insight into the future of medicine! 

We've all been through the routine at the doctor's: spending hours in the waiting room next to sniffling, coughing people, which increases the risk of us getting sicker than we were in the first place. And, when we finally get to see the doctor, we usually get 15 minutes or less of face time and a prescription for an expensive medicine that _might_ work if we're lucky. And that's only if we live in a part of the world with easy access to doctors.

But this doesn't have to be our future. New technology is changing the way medicine is practiced and who is able to do it. It's leading to a power shift and an overhaul in what has become today's medical industry. So let's take a look into a future where we turn the tables and say: "The patient will see you now."

In these blinks, you'll find out:

  * why new technology decreases the need for hospitals;

  * how an app can save you from a trip to the dermatologist; and

  * how mobile phones will improve health care in the developing world.

### 2. Smartphones allow greater access to medical information and will soon give patients much more power to diagnose themselves. 

Smartphones have revolutionized much of our lives. They provide easy access to unprecedented amounts of information with only a simple mobile connection, which is available to 95 percent of the population. They'll no doubt have a big impact on medicine, too. 

Smartphones will soon make _autonomous medicine_ possible, that is, they'll allow people to diagnose themselves. We already have some tools for it, such as the app _SkinVision_. 

SkinVision allows you to send a photo of a skin lesion to a doctor, who can determine whether it's benign or not. 

And photos are just the beginning. Microscopic scans will soon have such powerful zooms that we'll be able to scan ourselves for certain types of bacteria. Tuberculosis is diagnosed by checking a sputum sample for the bacteria _Mycobacterium tuberculosis_. Soon, anyone with a smartphone will be able to test themselves for tuberculosis.

Smartphones also have the potential to radically change the health situation in countries where people don't have easy access to medical professionals. In 2010, the average number of doctors and nurses per 1,000 inhabitants in sub-Saharan African countries was 1.1. In the United States, the figure was 12.3.

Mobile connections also provide people with greater access to health information. In 2013, over 630 million people in Africa had cell phones, 93 million of which were smartphones. Even non-smartphones can have an impact on public health. The South African project Masiluleke, for example, sends millions of text messages every day encouraging people to get checked for HIV/AIDS.

But, of course, smartphones are still more powerful. The biotechnology company _Nanobiosym_ even recently unveiled _Gene Radar_, a tiny chip that plugs into a mobile device and can analyze a drop of blood or saliva for tuberculosis, malaria and HIV. Gene Rader will allow people to diagnose themselves for those diseases at a cost ten times cheaper than the market price today.

### 3. Technology will shift power from doctors to patients. 

In the modern medical world, patients have grown accustomed to following their doctor's orders. But this will change in the future.

Doctors have long been considered all-powerful authorities of medicine. In fact, even Hippocrates, the Ancient Greek philosopher, said that physicians should conceal information from patients for their own good.

Medical students still recite the _Hippocratic Oath,_ which states that only people who've sworn by it should be trusted with medical knowledge.

Recently developed standards still echo this sentiment, like the _American Medical Association's Code of Ethics_, which states that physicians are allowed to treat patients against their consent if that consent would be _medically contraindicated_ — or unhealthy for them.

The "doctor's orders" still have the final say in a patient's treatment. Patients don't determine their treatment themselves and most follow their doctor's orders without question.

Patients will gain more power as technology gives them greater access to their own genetic information, however. When you know more about your genes, you can make more informed medical decisions.

Angelina Jolie did this when she decided to have a double mastectomy. She learned she had a high risk of developing breast cancer and ovarian cancer by analyzing her family's history, and a genetic analysis of her blood confirmed it.

Jolie was found to have an 87 percent chance of developing breast cancer because of a BRCA1 gene mutation, so she decided to have both breasts removed preemptively.

Jolie made her decision public in hopes of educating others on the power and importance of genetic testing. Genetic testing will continue to get more publicity like this as it has become more accessible.

The American company 23andMe already offers a _gene variant report_ of your saliva sample for a mere $100.

### 4. Technology will overhaul our current medical infrastructure. 

As smartphones give people more power, the medical world will change, too. 

We'll make fewer trips to the doctor, for one. Virtual health-care services like Doctor on Demand, MD Live and Teladoc already allow patients to consult a physician without having to go anywhere. A standard Teladoc appointment is roughly the same price as an in-person visit, but Teladoc service is open 24/7 and there's no waiting time. That's much more convenient than waiting an hour to see a doctor for only ten minutes. 

Patients will also spend much less time in hospitals. This is already starting to happen. From 1975 to 2013, the total number of hospitals in the United States declined from 7,156 to 4,995. A lot of treatments that used to require hospitals stays of several days are now treated within 24 hours on an o _utpatient basis_. 

Mobile technology will only further decrease the need for hospitals. Remote monitoring, conference calling and smart pillboxes that track medication use will enable people to monitor their health from the comfort of their homes. 

The Montefiore Medical Center in New York is a good example of this. It has eleven floors, twelve operating rooms and _no beds_, because no one needs to stay there overnight. 

The high cost of health care will also be reduced. Health care in the United States is very overpriced. Some hospitals charge $1,200 for every $100 of their total costs, and $190 billion is wasted in administrative costs each year. 

Few people in the medical world understand the severity of this problem. In fact, a 2014 survey of over 500 orthopedics found that they only accurately knew the price of commonly used devices less than 20 percent of the time. 

Smartphone apps like _Castlight_ and _PokitDok_ make the medical world more transparent by offering precise information on medical costs so patients can compare.

> Seventy percent of Americans would prefer virtual visits to non-virtual ones.

### 5. We’ll soon be able to produce a complex map of everyone’s individual medical information. 

We now have unprecedentedly powerful tools for collecting and storing data. This is another technological change that will soon have a huge impact on the medical world. 

We're currently moving toward developing a kind of Google Maps for humans: a _Graphic Information System_, or _GIS_, that superimposes multiple layers of information onto a single map. 

GISes are already used to analyze traffic, satellite and building information. But soon we'll be able to develop a human GIS that will allow people to superimpose different kinds of important medical information onto a digital map. 

It would include physiological information like your heart rate, genetic information about your DNA and its mutations, and anatomical information about the structure of your bones and organs. 

Some parts of the human GIS are going to be easier to create than others. The physiological layer is already fairly advanced because we can already monitor physiological processes like heart rate and eye pressure with commercially available portable sensors. 

Other layers, like the genetic layer, are much more complicated. We've already collected a lot of important information about genes: the Human Genome Project has identified, or _sequenced_, 90 percent of human DNA. And the cost of getting your genome sequenced has decreased dramatically: in 2004 it cost $28 million, and last year it cost less than $1,500.

There's still a ways to go, however. Each person's genome is made up of roughly 3.5 million variants, and roughly 19,000 of those variants can't be identified with modern technology. We still need to invest a lot in genetic studies before we can produce in-depth genomic layers for the human GIS.

### 6. A human GIS and access to big data would radically improve health care. 

What will we do with all the new information the human GIS will provide?

The human GIS will enable us to use a lot of new treatment methods. The physiological layer will help us keep track of symptoms, such as monitoring an asthmatic child's airways with biosensors. That would allow the child's parents to predict when her next asthma attack might hit so they can make sure her inhaler is on hand. 

The genetic layer will help us with disease prevention. Genetic information already serves as a warning to parents, as most diseases have genetic predispositions. Nearly one in 40 people carry dangerous recessive genes for cystic fibrosis, for instance, and one in 35 carry for spinal muscular atrophy. 

If a couple knows there's a high risk they'll pass a disease on to their child, they might want to consider other options like adoption. 

The range of available medical treatment will broaden even further as the human GIS is combined with Big Data, improving areas such as cancer treatment. 

We currently have a few cancer treatment options, like chemotherapy, surgery, radiation and drugs. What works for one cancer patient might not work well for others, but GIS information could be collected from patients who've been treated in the past and researchers could use that data to determine what treatments are best for a patient's particular genetic makeup. 

Big GIS data could help diagnose cases much more accurately. Today, there's little help available for patients with rare or unknown diseases, but a large GIS database would make it much easier for scientists to gain an understanding of their conditions.

### 7. Big data may even allow us to predict common diseases. 

Do you know anyone with diabetes? You probably do, because it's one of the most common chronic illnesses. Roughly 29 million Americans have it. 

Chronic illnesses are difficult to treat because they often can't be cured. Treatment focuses on managing their symptoms, so patients may suffer from symptoms their entire lives. 

Chronic illnesses also drain medical resources. In fact, about 80 percent of the $3 trillion dollars spent on health care every year in the United States goes toward chronic illnesses.

Big data has the power to change things, however. In the future, it may help us predict and prevent chronic illness. 

Consider _Post-Traumatic Stress Disorder_, or PTSD, for instance. PTSD affects an estimated 24.4 million Americans. If we could analyze that data, we might be able to predict when it's about it set in. 

If health-care professionals could predict that a returning war vet was about to lapse into PTSD, they could treat it preemptively, potentially saving the person from the condition.

This kind of advanced disease prediction might seem far off, but we're already making progress on it. In fact, a computer algorithm called _Healthmap_ predicted the 2014 West African Ebola outbreak nine days before the World Health Organization (WHO) did. 

The _Health Map_ prediction analyzed data from tens of thousands of online media providers, like news sites, social networks and government websites. They mapped search engine results for symptoms and their locations. 

The algorithm was able to conclude that Ebola was the cause of the epidemic over a week before doctors began to diagnose it, by which point it had already spread to some hospitals. 

Predicting an Ebola outbreak is different from predicting an onset of PTSD, however. We still have a lot of work to do to make them useful in individual diagnoses.

### 8. Detailed medical data is dangerous if it falls into the wrong hands. 

Big Data has enormous power to revolutionize the medical world. Unfortunately, that power could also be used for evil. 

Medical identity theft is already a problem in today's world. People can steal your medical identity and use it to obtain treatments like prescription drugs. They can also use your insurance information to pay for their own medical treatment, or treatment for other people. 

Sixty-eight US medical records have been breached since 2009. Hackers can do this in a number of ways, for example, by getting into hospital databases or stealing hardware with medical data stored on it.

Our medical privacy has to be protected. But hackers and thieves aren't the only people we have to protect it from. 

There are also concerns with the privacy of genetic information. Insurance companies could use it to discriminate against people who have predispositions for chronic diseases by refusing them coverage. Some states have already enacted laws to prevent this by forbidding insurance companies to gain access to genetic information.

Genetic information could also be used in unsavory ways by marketing companies. _Data brokers_ like _Acxiom_ earn money by selling people's personal data to marketing companies. Acxiom already has the information on the names, income, home valuation and medical history for over 200 million Americans.

If companies like Acxiom got ahold of our genetic information as well, the marketing possibilities would be even scarier than they are today. Imagine you were to get an ad for cystic fibrosis treatment before you knew you had it or were vulnerable to it. 

Ultimately, your information should be _yours_.

### 9. Final summary 

The key message in this book:

**The future of medicine is upon us. Our technological developments are already overthrowing the current system and many of those developments have only just begun. New technology is improving treatment and giving patients more agency in their own health care. Soon we'll develop a system where every individual has power over his or her own body.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Where Does it Hurt?_** **by Jonathan Bush and Stephen Baker**

America's health care system is failing, and a revolution is needed. Hospital visits are a messy experience, and people don't have fast and easy access to the medical services they need. In _Where Does It Hurt?_, Jonathan Bush argues that we can fix our broken health care industry by making it more open. Entrepreneurs need the space to provide alternative services, so hospitals will be forced to better themselves to catch up.
---

### Eric Topol

Eric J. Topol M.D. is a cardiologist, professor of genomics and the director of the _Scripps Translational Science Institute_. He previously served as chairman of cardiovascular medicine at the _Cleveland Clinic_ and founded the _Cleveland Clinic Lerner College of Medicine_. He is the author of the best-selling book _The Creative Destruction of Medicine._

