---
id: 54d8953c383963000a3f0000
slug: the-personal-mba-en
published_date: 2015-02-10T00:00:00.000+00:00
author: Josh Kaufman
title: The Personal MBA
subtitle: Master the Art of Business
main_color: 44C0F0
text_color: 246680
---

# The Personal MBA

_Master the Art of Business_

**Josh Kaufman**

_The Personal MBA_ contains everything you need to know in business. Today, true business leaders are forged through their own experience and personal research, not in universities. From developing your idea to sealing a deal, these blinks guide you through the entire process of becoming a self-made business success story.

---
### 1. What’s in it for me? Learn why you don’t need an MBA to be successful in business, just some basic guidance. 

When it comes to business, most people think there's an official starting line: A degree, an apprenticeship, or stumbling on the right amount of money.

Wrong!

You don't need an MBA or a winning lottery ticket to start a successful business or improve the one you've got. You just need some common sense and a few basic guidelines. For example, every good businessperson knows how to negotiate, but you don't need a whole semester to learn the key to good negotiation (Just check out blink seven).

And that's just one of the tips that's helped _The Personal MBA_ become the go-to book for anyone interested in business.

In these blinks, you'll learn

  * why an MBA is a big fat waste of time and money;

  * how to confirm if your business idea is any good; and

  * how borrowing money can actually increase your profits.

### 2. Business school: Not only is it extremely expensive – chances are, it won’t pay off. 

Want to go into business, but have no idea where to start? Business school seems like a tempting prospect, a place where all the knowledge and contacts you need are simply handed to you — but is it _really_ your ticket to success?

It has to be, right? Otherwise why else would people pay so much for their MBA? Business school is _expensive_, that's for sure — the top 15 MBA programs charge up to $53,208 per year for tuition. That's not even including fees, loan interest or living expenses!

What if we factor those into the equation, plus the opportunity cost of lost wages too? Turns out there are eight different US business schools where the cost of an MBA exceeds $300,000.

"Wait," you might think, "surely not all business schools are that pricey!" True, however, any student who holds an MBA has an average debt of $41,687 (again, without accounting for additional expenses).

OK, so business school costs a lot. Is this what the educational experience is worth? Short answer: nope.

In the long term, an MBA does nothing for your career. If it did, a master of business administration degree should correlate with measures of success, like an increased salary, or promotions to higher level positions. And it doesn't.

Researchers from Stanford University and the University of Washington conducted an extensive study into the matter. Analyzing 40 years' worth of data, they found that there is no correlation between long-term career success and possessing an MBA, whether you graduated with flying colors or just scraped a passing grade.

So if business school can't make your entrepreneurial career blossom, what can? Well, you can learn a lot from experience, and from your own research online — or from books like this one.

> _"Business school is a big risk...the only certainty is that you will shell out about $125,000." — Wharton School of Business graduate._

### 3. The perfect business idea balances money and passion. 

Now that you know not to waste your money on an MBA, let's get down to business — your business. What kind of company should you start? You might think that since the IT industry is booming right now, starting an IT business would be a smart idea. But what if you absolutely despise IT as a field?

Here's a fact that more people should know: your business won't thrive if you do it for money alone. Even if it'll run itself eventually, your business will still need hundreds of work hours and generous amounts of money to set up.

For example, in order to afford to hire people to deal with mundane tasks like payroll and HR, you'll need to work incredibly hard for a few years. If you're only in it for the money, can you see yourself persevering until your company reaches that point? Probably not!

But if you start a business in a field that you like, not only will you be more likely to stick with it, you'll also be ahead on the decision-making front. If you know a certain amount about the specific market you're entering, you'll be able to make sound decisions, develop an attractive product _and_ tackle the competition.

Say you're into team sports. This may put you in a better position to judge which products meet the needs of sportsmen, making for a _better_ line of business to focus on — even if there's more demand for IT products.

On the other hand, you might feel so passionate about an idea that it impedes your judgment.

Imagine a small town, where expressionist art exhibitions are understandably in no high demand. When you're an expressionist art lover, you may be oblivious to this lack of interest and try to found a museum of Expressionism right there. Sure, this could be a highly rewarding experience, but by no stretch is it a promising business idea — a painful truth that an enthusiastic art lover might be reluctant to acknowledge.

But let's say you're not an oblivious expressionist art fan, or a grudging IT entrepreneur. Let's say you've found just the right mind-set to start your own business. The next question is — how do you actually enter the market? Underneath that there's another question: is there a shortcut to getting all the money you need? Read on in the following blinks to find out.

> _"In all the excitement, it's easy to forget that there's often a huge difference between an interesting idea and a solid business."_

### 4. Leveraging your investments can be rewarding, but it’s also risky. 

So you've found a great new business opportunity, but you're short on the cash you need to pursue it. Should you just stop right there? No way. Instead, _leverage_ your investment.

What is leverage? Simply put, it's any technique that _amplifies_ investor profits or losses. Typically, it's the strategy of using _borrowed_ money to increase your profit potential. In this way, you can make enormous gains with very little of your own capital.

Let's say you want to make a big investment that you couldn't afford without borrowing money: you want to buy four properties at $100,000 each, but only have $20,000 of your own capital. If you borrow $380,000 and the properties double in value, you'll have made $400,000 with just $20,000 of your own capital. That's a 20-times return on your investment!

But if the idea that borrowing more always means earning more seems a little too good to be true, well, you're right. Leverage can be risky because it also amplifies your _losses._

Let's return to our example: imagine that the properties you bought lost half their value — your losses would amount to $200,000. That's right, you'd lose _ten times_ your own capital resources.

For a real life example of this, you need only look to the catastrophic recession of 2008, which was caused in part by the enormous amount of leverage used by investment banks, who were caught with catastrophic losses when the property bubble popped. The lesson? Leverage can be great, but you've got to watch out.

> _"Using leverage is playing with fire. It can be a useful tool if used properly, but it can also burn you severely."_

### 5. We’ve all got needs, and a product that satisfies them will sell. 

Many think that for a product to sell, it just has to be good — not true. Consider this: People will only want something if it fulfills their _needs_. Let's put it this way — a vampire won't pay to use your state-of-the-art solarium (unless you've allowed him to use it to trap unsuspecting customers!)

All our decisions are influenced by our basic needs, including our _buying decisions_. Imagine selling a bottle of stale water to a hiker lost in a desert. He'd be willing to pay just about anything for that lousy bottle, just because it responds to his _predominant need_. The lesson here? Find your own desperate hiker.

Any successful business fulfills one or more of its customers' needs, and Harvard professors Nitin Nohria and Paul Lawrence asserted that there are four needs or _drives_ that are common to all humans.

The first of these is the desire to acquire and collect things, whether it's stamps, shares or social status. Retailers and investment brokerages are the businesses that cater to this.

Secondly, we have the drive to bond with other people, so we can feel valued and loved. Businesses such as dating services and companies that promise to make us more desirable (perfumeries, beauty salons) are the ones that meet this drive.

Thirdly, we want to learn and satisfy our curiosity. Why else would you be reading these blinks right now? It goes without saying that Blinkist is one of the services that fulfills that need.

Finally, we strive to defend ourselves, our loved ones and our property, which is where security firms and companies selling alarms come in.

Consider your business idea in the light of each of these drives. Which of these four drives could your business cater to?

> _"Understanding human needs is half the job of meeting them." — Adlai Stevenson_

### 6. A great product deserves great marketing. 

Say your product meets not just one, but _three_ of your customers' basic needs. It's ready to deliver! Now, where are all those clients who should be drooling over it?

Well, you'll need to get their attention first. But in this age of social media and information overload, this is no easy task. If you want customers to pay attention, you'll need to offer something remarkable _and_ memorable.

And if we want to do this, we've got to think about the _way_ we deliver our message to clients — the medium matters. For example, if the form of your message makes a customer think it's made just for her, you're far more likely to get her attention.

Show your customer that you've made the extra effort, even if it's just with a hand-addressed FedEx envelope. Sure, this can be expensive, but it's worthwhile to inform selected prospective clients in a high-quality way — certainly better than spamming an entire town's inboxes with irritating emails.

If you want to market your product effectively, there's another crucial aspect you should consider: the product's _end result_. Think about it: people don't buy a product for its own sake. They buy it because of the end result they're hoping to attain. For example, a woman won't pay $20 for a lipstick simply because the color is nice. She buys it because she hopes it will make her more desirable.

A great way to highlight your product's end result is through _testimonials,_ in a statement made by an ordinary person who overcame adversity, all thanks to your product. Say you're selling an acne cream — your testimonial could be from a pimply teenager who thought he was doomed to be an outcast until your product cleared his skin right up. That's the sort of story that's going to draw your customers in.

> _"In an attention economy, marketers struggle for attention. If you don't have it, you lose." — Seth Godin_

### 7. Even when clients are reluctant, there are ways to make a sale. 

Picture this: you're visiting a pet store and see a puppy that you're absolutely smitten with. But you're unsure whether you want to buy it. The owner makes an offer to you to take the dog home for a week and guess what? It works its charms and you can't imagine returning it once the week is over.

This is an example of how a sales expert can convince a reluctant prospect to become a customer. But how, exactly?

You can increase sales if you account for your clients' fears. We all hate to make a bad choice, and this makes us cautious customers. Instead of buying the wrong thing, we'd rather buy nothing at all — right? In sales theory, this is called a _major barrier to purchase_. Naturally, it's something salespersons hate.

To encourage prospects to buy something, salespersons shoulder the risk of a bad transaction — by allowing the customer to return that cute puppy if things don't work out. This strategy works for non-adorable items, too. For example, it's often possible to try out a bed for an entire year, _and_ return it for a full refund. This makes people more likely to purchase a bed.

So if you can find out _why_ your customers might say no, you'll be able to talk them into a "yes." In sales, reasons for saying no are called _standard objections_, and there are several of these. A customer might fear the product costs too much, or it won't provide the promised benefits, or that he doesn't need the product yet, and so on.

If you know why your client won't buy, you can devise an argumentative strategy and basically convince him that he's dead wrong. Say he thinks his old laptop will have to do for a while longer. You can convince him that his old laptop is incompatible with essential new software.

> _"People don't like to be sold, but they love to buy." — Jeffrey Gitomer_

### 8. To strike a great deal, it helps to prepare. 

How do you picture a negotiation? You might see yourself sitting at a conference table and discussing a deal, but don't get ahead of yourself! This is just the _last_ phase of a negotiation. A good negotiation actually occurs in several stages.

The first part of negotiation is _setting the stage_. Long before you start exchanging offers with your business partners, you can optimize your outlook in the early decisions you make. You could make sure you're negotiating with the right person, for example: did the company send someone with real decision-making authority? Or just someone who's been assigned to collect information for those in power?

You can also create a conducive environment by choosing the setting you're most convincing in. Will you present your offer in person, over the phone or should you organize an online meeting? An elegant office could make a great impression, but maybe your voice is a lot more confident over the phone. If you're having trouble deciding, research data in your industry or market may prove very helpful.

Once you've prepared, decide on the _terms_ of your proposal. Consider how attractive your offers will be to the other party, and in every case, search for ways to make the offer even more desirable. You could even try to find out the other party's offers. Then you can work out a way to make yours seem superior.

You've always got to consider the worst-case scenario, so anticipate possible objections and create an argumentative strategy to defuse or counter them. And, in case they don't work, decide on the concessions and compromises that you're willing to accept.

The final stage of negotiation is the actual discussion. Because of the effort you put in during the first two stages, you'll be far better prepared. In fact, most of the hard work at this point is already over!

### 9. A good leader is a good communicator. 

Ever had a plan that seemed great until you tried to put it into action? Some bosses have great schemes they just can't implement. Are they unlucky with their employees? Or is it possible they just don't know how to communicate with them?

Here's what they don't know: If you want others to do something, you should tell them _why_ you want them to do it. Harvard psychologist Ellen Langer demonstrated this in a study where her students asked others queuing in front of a Xerox to let them go first. 60 percent of the people agreed, but when a reason for the request was provided, an impressive 95 percent complied.

Moreover, if you tell people about your intentions, everyone can work in ways that support that plan, and know what to do if the situation changes. This way, you won't have to micromanage every step.

Imagine a general telling his field commander to capture a hill, but the situation changes and capturing the hill becomes strategically futile — the field commander will know how to react if he knows the larger goal is to flank the enemy.

There's another way to boost your communication, and it's very simple: stop putting others down. Yet it's surprising how often we find ourselves dismissing our coworkers' comments. It might make you feel superior but it gets you nowhere, causing your partner to withdraw from the conversation, even if you don't notice it.

People can also become defensive and try to save face rather than understand your comments. Think about it: how engaged and cooperative would you be in a conversation with your boss after he said: "This is the silliest idea I've ever heard of."

By contrast, effective communication sees both partners actively and comfortably exchanging ideas in a dialogue that could strengthen the entire organization.

> _"Effective communication can only take place when both parties feel safe."_

### 10. Use your day more effectively by listening to your body. 

Sure, it's no fun to be unproductive, but being too busy can harm you and your work too. The best way to be productive is, in fact, through setting limits.

If you have several things to do at once, your performance can be reduced across all of them. Your brain can only handle so much. Think of it like juggling: the more balls you're trying to juggle at once, the more likely you are to drop them all.

Moreover, with every task come unexpected demands that take extra time and effort to deal with. If your schedule doesn't allow for extra time, you won't be able to cope with those demands, especially if they're coming from several tasks at once.

So how can we stay in tune with what we can handle? Your best bet is to listen to the natural fluctuations of your energy. We're not talking about a far-fetched, new age concept here — it's simply that your productivity isn't distributed evenly throughout the day. Instead, your body has a natural rhythm. Sometimes you can feel highly alert and productive. At other times, you're far too tired to get anything done at all.

Though it varies from person to person, most people are more productive in the morning than around noon. We also know that energy cycles in 90-minute-spans. So within that one-and-a-half-hour frame your energy will both rise and fall.

If you're aware of these natural fluctuations, you can take advantage of the times when you feel energized. And more importantly, you can recognize when you're crashing and take a well-needed break.

> _"All people are created equal, but all hours are most definitely not."_

### 11. Final summary 

The key message in this book:

**If you want a thriving business, your product should cater to the core needs of your customers. By communicating confidently and cleverly, you can convince your clients to buy, your employees to cooperate and your business partners to sign your deal.**

Actionable advice:

**Actions speak louder than words.**

The next time you have to hire a new employee, don't go for the applicants who performed best in an interview. Instead, do some extra research and find out how an applicant has performed in past months or years. Past performance is the best predictor of future performance.

**Suggested** **further** **reading:** ** _The $100 Startup_** **by Chris Guillebeau**

_The $100 Startup_ is a guide for people who want to leave their nine-to-five jobs to start their own business. Drawing from case studies of 50 entrepreneurs who have started microbusinesses with $100 or less, Guillebeau gives advice and tools on how to successfully define and sell a product, as well as how to grow your business from there.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Josh Kaufman

Josh Kaufman is an independent business adviser and the author of two international bestsellers: _The Personal MBA_ and _The First 20 Hours: How to Learn Anything, Fast._ His website JoshKaufman.net was listed as one of Forbes' Top 100 Websites For Entrepreneurs.

