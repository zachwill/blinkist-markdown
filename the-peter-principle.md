---
id: 559bc6f13966610007800000
slug: the-peter-principle-en
published_date: 2015-07-08T00:00:00.000+00:00
author: Laurence J. Peter and Raymond Hull
title: The Peter Principle
subtitle: Why Things Always Go Wrong
main_color: E6922E
text_color: 805119
---

# The Peter Principle

_Why Things Always Go Wrong_

**Laurence J. Peter and Raymond Hull**

_The Peter Principle_ (1969) explains why you feel like you're surrounded by incompetence at work — you are! This wry book reveals promotions for what they really are: a progression to our final level of incompetence. These blinks help us understand how corporate hierarchies really function, as well as offering advice on how to deal with our own incompetence.

---
### 1. What’s in it for me? Learn why you can’t escape a destiny of incompetence! 

There are a handful of unforgettable, brilliant theories that manage to encapsulate our entire lives in a single sentence. Think of Moore's Law, Godwin's Law and so on.

The Peter Principle is another one of those. To sum it up: The great work you're doing now will inevitably get you promoted to a job you can't do. Sorry, but that's what's going to happen.

These blinks show how the Peter Principle proves that we're all doomed to incompetence. And since incompetence is all around us, we have to learn how to deal with it!

In these blinks, you'll find out

  * how to avoid that last fateful promotion to the job you'll be incompetent at;

  * what doctors can prove about your incompetency; and

  * how to speed up your promotions, though it won't make you many friends!

### 2. Incompetence is inescapable. 

Ever wondered why those at the top of your workplace's hierarchy seem so incompetent? Well, it's because they are! But why? Let the _Peter Principle_ explain.

The Peter Principle states that every member of a hierarchy will eventually rise to their _level of incompetence,_ or _final placement_. This position is the furthest you can be promoted with the skills that you possess. In other words, employees continue to receive promotions as long as they're competent in their current position.

To illustrate this further, let's imagine an outstanding elementary school teacher. He receives promotion after promotion until he has lots of responsibility for the students. But eventually, he'll receive a promotion for which his skills don't quite qualify him.

Suppose he lands a position on the school board as a coach for new teachers. If he's unable to engage adults as well as he engages small children, then he's reached his level of incompetence. In this final placement, his performance won't merit further promotion.

As individuals, our skills and competencies vary. You might be a genius software developer, and be very proud of that fact. But this doesn't make you the best fit for a consultancy; perhaps you might simply be unable to cope with the constant pressure of that business.

Some of us are able to rise all the way to the top of a hierarchy without reaching our level of incompetence, so we switch to other hierarchies and find it there. This is called _compulsive incompetence._ Just look at Socrates: A great teacher; not such a great defense lawyer.

> _"You will see that in every hierarchy the cream rises until it sours."_

### 3. To get that promotion, you have to push or pull. 

Now that you're aware of the Peter Principle, do you know how to get yourself promoted? There are two key ways to go about it. You can pull, or you can push.

A _Pull_ refers to a relationship between employee and superior outside the professional context. Remember that guy who wasn't the brightest, but was buddies with the owner's son and got promoted to a senior position before you? He demonstrated an expert use of the pulling technique. Sure, pulling won't make you very popular among your coworkers, but it will speed up your promotion process.

To use pulling for yourself, follow these steps: Find a motivated superior, or patron, who gains from helping you and loses when he doesn't. For the teacher, it might be someone currently on the board.

Next, check if your path to promotion is open.

Our elementary school teacher would check to see if another, more experienced colleague is next in line for the job. If not, be patient or switch to a free promotion channel. You could even obtain multiple patrons — the more the better!

Remember to stay flexible with your patrons, especially once you start receiving promotions. The truth is that they can only help you out up to a certain point. If your patron is a manager, it won't help you to cling to them once you're a manager yourself. Don't have any qualms about dropping them once you've nabbed that promotion!

The _Push_ is the second method you can use to accelerate your promotion. Pushing covers everything related to extra effort you invest in work. This includes coming early and leaving late, extra training in your free time and more.

But beware: promotions almost never occur as a result of pushing. It might impress some of your colleagues that you're always the first one in the office but the others might wonder, "Doesn't he have a life outside work?"

> _"Never stand when you can sit; never walk when you can ride; never Push when you can Pull."_

### 4. There are no exceptions to the Peter Principle. 

Perhaps you're harboring doubts about the Peter Principle, as you can recall several cases where it didn't apply in your own workplace. If someone is promoted even though he obviously reached his level of incompetence, this must be an exception to the rule, right?

Nope. That's called a _pseudo-promotion_.

Pseudo-promotions are just sideways shifts, leaving employees in the same place in the food chain with no real increase in power. _Percussive Sublimation_ is what happens when someone who is already at their level of incompetence receives a promotion in another division. This creates a great incentive for jealous coworkers.

If Tom, an incompetent Senior Marketing Consultant, is promoted to Vice President of Customer Relation Management, this promotion justifies Tom's becoming a Senior Consultant in the first place, because this sideward shift makes it seem like he's still competent. Additionally, his colleagues, knowing the true incompetence of Tom, might think, "Hey, if _this guy_ got a VP position, I can too" — encouraging them to keep pushing for a promotion in vain.

A _Lateral Arabesque_ promotes someone incompetent by offering them a shiny new job title. Take a certain Mr. Schmidt, who is now an incompetent Senior Facility Manager, even though he did the exact same job as a maintenance man. The larger the organization, the more Lateral Arabesques you'll find.

When it seems impossible that someone so incompetent can be promoted, it might be _Peter's Inversion_. For example, suppose a nurse wakes you up to remind you to take your sleeping pill. Though the goal is for you to sleep, her reminding you was more important to her.

This may seem counter-intuitive, but it isn't. Competence is both relative and subjective, as it's something usually determined by your superior. If a rule enjoins nurses to remind every patient of his pills, the nurse followed the rules to perfection. These non-exceptions show that the Peter Principle exists in any organization with a hierarchy.

> _"Competence, like truth, beauty and contact lenses, is in the eye of the beholder."_

### 5. Look out for the common symptoms of Final Placement Syndrome. 

By now you might be wondering just what'll happen to you after you reach your final placement. When you get to your level of incompetence, you simply aren't able to do your work. And this incompetence leads to some unpleasant maladies.

If you've reached your level of incompetence, you might suffer from the Final Placement Syndrome (FPS). Here are some common symptoms: _Tabulatory Gigantism_ — the absolute need to have a bigger desk than your colleagues; or _Cachinnatory Inertia_ — the practice of telling jokes all the time instead of working. No doubt you've seen a few of these cases in your own workplace.

In one survey, some doctors identified high blood pressure, peptic ulcers, insomnia, and sexual impotence as health issues for seemingly successful people — this is classic FPS. But since Final Placement isn't recognized by the medical profession, you might run into some of the following issues.

Some people with FPS confuse physical conditions with their professional incompetence. They think, "If only I could sleep again, I could concentrate better." So they get a prescription for sleeping pills.

But most doctors will simply ignore FPS symptoms and tell their patients to "get over it."

Psychotherapy is also doomed to fail because it doesn't treat the actual problem — being incompetent. Distraction therapy is the only remedy. As incompetence is the root of all evil, you simply need to feel competent in other areas, like bridge or knitting.

Consider an overweight CEO who discovers golfing and begins to spend more time focusing on this calming, outdoor activity. He's working on the source of the problem — incompetence — and the symptoms at the same time; he feels competent in a new sport and loses weight as he strolls around the golf course.

Look out for these symptoms in yourself or your colleagues to check for final placement. Then you can help them out, or recognize who's blocking your next promotion.

### 6. Learn to avoid or cope with your incompetence. 

Given FPS, reaching your level of incompetence is definitely something to fear. But fret not! If you want to avoid the negative impacts of reaching your final placement, there are ways to ensure you never get there. _Creative Incompetence_ is the best way to steer clear of promotions without rejecting them directly.

To be creatively incompetent, you have to create the image that you have already reached your final placement. In other words, start trying to appear slightly incompetent. This will save you from receiving those unwanted promotion offers. And it's easier than you think! Some reliable techniques include parking your car from time to time in your boss's spot, failing to laugh at your boss's jokes and raising uncomfortable topics at the company barbecue.

If you do reach your level of incompetence, _Substitution_ helps you avoid FPS and continue working without the negative side effects of your final placement. One fantastic technique to substitute successfully is _Perpetual Preparation_, where you do everything but the actual work.

As the name indicates, the substituter focuses on preparing for work by, for example, vaguely reaffirming the need for action, by researching other ways to get the job done or by looking for expert support.

Think of a guy in a welfare department known for his fundraising ability who is promoted to lead a program fighting poverty; this is his final placement. But then he begins collecting resources to build a house for the program's staff, and wants to set up an Anti-Poverty Council to receive expert advice.

He's doing everything outstandingly, except actually fighting poverty, because he's incompetent at that. As a very successful substituter he is not in danger of Final Placement Syndrome. Substituting can help you to remain healthy and happy for the rest of your working life.

### 7. Final summary 

The key message in this book:

**Incompetence is all around us — that's just how organizations work! By understanding the Peter Principle, you'll be able to see the reality behind the promotions in your workplace, and determine who has reached their final placement, while avoiding your own level of incompetence with ease.**

Actionable advice:

**Give the ultimate test for incompetence.**

Next time you are wondering if someone around you is already at his level of incompetence, ask yourself this simple question: "Is he still doing something useful for his job?"

If the answer is "YES" then there is still open space for more incompetence. If the answer is "NO" then he has reached his level of incompetence.

If the answer is "Don't Know" then you've reached your own level of incompetence.

**Suggested further reading:** ** _The No Asshole Rule_** **by Robert I. Sutton**

_The No Asshole Rule_ delves into the problem of bullying or aggressive co-workers, who in many cases rise to management positions. Sutton provocatively labels them _assholes_.

The book lays out the effect these employees can have on a business, and gives advice on how to develop an asshole-free environment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Laurence J. Peter and Raymond Hull

Laurence J. Peter has worked as a counselor, school psychologist, prison instructor, consultant, professor and writer. Raymond Hull wrote stage plays as well as articles for _Esquire_, _Punch_, _Maclean's_, among other publications.

