---
id: 53cf7fcd3630650007d70000
slug: the-pirates-dilemma-en
published_date: 2014-07-22T00:00:00.000+00:00
author: Matt Mason
title: The Pirate's Dilemma
subtitle: How Youth Culture is Reinventing Capitalism
main_color: FEE333
text_color: 665B14
---

# The Pirate's Dilemma

_How Youth Culture is Reinventing Capitalism_

**Matt Mason**

_The_ _Pirate's_ _Dilemma_ is an examination of the pirate spirit, its rejection of authority, and the profound ways that this philosophy has changed the world for the better. By adopting the pirate spirit, individuals and businesses have a chance to use open-source methods in order to survive, flourish and be a positive influence in the inevitable shift towards an economy in which seemingly anything can be copied.

---
### 1. What’s in it for me? Learn how to flourish and be a force for good by thinking like a pirate. 

Today's pirates are way different from the pirates of old. They don't have the same hats or the lust for gold, but there are certain undeniable similarities. Both have a sense of adventure, a desire to explore the unknown, and a rebellious attitude that bucks authority.

Today's pirates, however, are not nearly so cut-throat. In fact, their contributions, while often resisted by powerful businesses and other parts of the establishment, have fundamentally changed the way that we interact — socially, economically, and commercially.

It is piracy — perhaps in the form of illegal MP3 downloads, old mixed tapes or remixes — that brought us things like streaming music and iTunes. And it is the stubborn openness of the open-source philosophy that brought us things like Wikipedia and even the internet.

In these blinks, you will learn about the history and relevance of piracy, as well as the way it continues to shape our society. You'll also learn what businesses can do to cope with some of the inevitable, radical changes that the near future holds.

In addition, these blinks will show you:

  * how dodging restrictive licensing fees actually led to one of the largest American cultural exports ever,

  * why a certain rapper is making a killing selling Vitamin Water,

  * why "Yankees" are a bunch of thieves,

  * why the USA would block regulations that would allow cheap AIDS medication to be available to those in need, and

  * how the pirate spirit can ultimately act as a force for good that benefits everyone.

### 2. Piracy means bending the rules to copy, share, and change the world, and create new avenues of expression. 

What do you see when you imagine a pirate? Probably a grizzled, peg-legged, crazy-hat-wearing guy with a parrot on his shoulder going on about rum and treasure and saying things like _argh_ _matey_. In today's world, there is another, more subtle kind of pirate whose influence can be found in nearly all aspects of our lives.

_These_ pirates copy, share, and change intellectual property — both legally and illegally. A pirate could be anyone from a DVD bootlegger to a T-shirt company copying the latest fashion from renowned designers.

Pirates also modify existing things, like the artists who change urban landscapes by adding graffiti to compete with advertising, or the musicians who remix and sample songs they don't own.

Piracy isn't just about manipulating intellectual property; it's also about the creation of new public spaces where these novel changes to other people's ideas can be shared and can flourish.

Indeed, pirates create spaces where traditional rules don't apply. Sealand, an abandoned fort off the coast of England resides in international waters. Thus it is not governed by law, and was consequently ripe for the taking by a pirate radio DJ who broadcasts his shows to England.

By finding an area that was unbound by any sort of laws, he was able to create his _own_ rules.

Using other people's ideas to create new things, legally or illegally, has led to the creation of a whole new world of ideas and media. For example, newsletters circulated among those who created the early internet and open-source culture, such as the Free Software Foundation, arose from the marriage of new ideas and new spaces.

As we can see, piracy is a lot more than mere looting. It's also about creation, reinvention, and the meeting of ideas.

> _"One man's copyright terrorist is another's creative freedom fighter: many forms of piracy transform society for the better."_

### 3. Piracy is just another business model competing with the existing market. 

Have you ever listened to the radio and noticed that you keep hearing the same songs over and over again? It's less likely to be the case today, thanks to the work of pirates, such as pirate radio DJs and MP3 sharers, who have paved the way to give us more options and control over our media consumption.

Often, markets and cultural outlets are tightly controlled, hampered by bureaucracy and the protection of intellectual property, in pursuit of profit above all. Pirates work outside of this restrictive framework, finding their own ways to inspire innovation, creativity, and efficiency.

Piracy cuts through the bureaucracy of the market, allowing new ideas to develop quickly. In fact, this kind of market innovation has a surprising historical precedent:

The Founding Fathers of the USA encouraged the Industrial Revolution, implementing policy that ignored global patents, among other policies that promoted creativity and innovation. Americans were simultaneously notorious for stealing others' ideas, leadings Europeans to refer to them as the Dutch word for pirate, _Janke_ (also known as _Yankee_ ).

Moreover, piracy provides more platforms for creators than the mainstream market offers, giving more people the opportunity to freely express themselves, spread their ideas, and inspire innovation.

For example, pirate radio DJs illegally commandeer the airwaves using transmitters so that they can play whatever kind of music they want. This gives people the option to listen to music outside of the Top 40, and thus gives underground music a voice and an audience.

The pirates' models are successful because they are ultimately better for the consumer and answer market needs. Consequently, after some initial resistance, businesses often adopt the methods pirates use in order to increase their sales and respond to consumer needs.

When Napster started offering MP3 downloads illegally, it shined a light on the demand for digital music. That exposition opened the floodgates for companies like Apple, Google Play, Amazon, and Bandcamp, who all provide digital music services.

Now that you have a solid understanding of what piracy is and how it works, the following blinks will look at how piracy can actually change our lives.

> _"Wherever you tune in, somewhere you will find a pirate pushing back against authority, decentralizing monopolies, and promoting the rule of the people."_

### 4. Digital replication will soon extend to physical objects and all industries will be fair game for piracy. 

Imagine this scenario: it's Christmas Eve and you're ready to print out all of the presents, but there's one problem: your 3D printer is broken! How sad would you actually be if you knew the presents would be there when the printer is fixed tomorrow, the next day, or anytime you wanted anything at all?

Although it might be surprising, this scenario isn't at all far-fetched: 3D printers are _already_ here and here to stay, and will only become more prevalent.

In fact, large companies such as Adidas, BMW, Timberland and Sony, have developed 3-D printer prototypes in conjunction with computer-aided design programs that will cut the production process from months to days.

In addition, Adrian Bowyer and his team at the University of Bath, England have already initiated a project developing open-source, self-replicating 3D printers, which aim to make it possible for _anyone_, even the poorest, to manufacture _anything_.

With these advances, it seems that a 3D printer in every home is very possible in our near future.

The prevalence of 3D printers will expose _all_ industries, not just those of intellectual property, to piracy. The same way MP3s made music more widely available, creating a world where there's no true distinction between producers and consumers, so too the 3D printer will affect all other industries.

Imagine: pirates with 3D printers could scan, copy, and print _any_ physical object, from a designer handbag to a life-saving heart implant, making distributing physical objects as easy as sharing MP3s.

In this inevitable future world, centralized mass production may no longer serve a purpose. Only the creativity and ingenuity of new designs would determine the final product's value, rather than factors like scarcity.

We have to wonder: how much will Nike's Limited Edition Air Jordans be worth when anyone can print them?

Ideally, businesses would start thinking _now_ about how they want to cope with this inevitable, life-changing future. It's not a matter of _if_, but _when_.

> _"What would happen to Nike when kids start printing out Air Jordans at the rate at which they illegally download music?"_

### 5. Technology and a D.I.Y. attitude are shifting work/life balance towards independent and non-traditional forms of employment. 

"On-demand" is in. People are finding everything they want with just the touch of a button or click of a mouse. Could technology help us change work in the same way, allowing you to work "on demand," i.e., how and when you want?

In fact, job structures _are_ shifting more and more towards self-employment and non-traditional work relationships, as people become empowered by _Do_ _It_ _Yourself_ (D.I.Y.) culture, the attitude and set of tools that enable and encourage people to build things on their own, rather than paying someone else to do it.

In the world of work, technology has enabled people to work for themselves, rather than for employers. These new tools include things like publishing platforms and accessible hardware and software, which add to an individual's ability to create value and use new distribution channels.

A 2004 U.S. Department of Labor study predicted that, in the near future, a significant amount of the work force will work in more specialized, decentralized arrangements that are more closely tailored to their individual talents. This shift will then result in more self-employment and freelance working relationships.

The rise of D.I.Y. culture has also sparked a shift in attitude: people want the freedom to control how they work.

Novel workstyles are now commonplace for workers, who can work from home, control their own schedule, and use technology to do what they want, and still earn a living. This emerging work force finds it harder to deal with authoritarian bosses and rigid work schedules, and workplace creativity and individuality are becoming mainstream.

A 2007 private study in the UK found that 45 percent of respondents liked their jobs, down from 58 percent in 1989, despite a 60 percent increase in average income.

Why the dissatisfaction? More people want to work for themselves, and the modern work environment hasn't caught up with this demand.

### 6. Open-source culture benefits everybody by increasing cooperation. 

Imagine if scientists weren't allowed to use other scientists' ideas or discoveries. How could a rocket engineer prepare for liftoff without Isaac Newton's laws of motion? Couldn't we apply this same logic of sharing and accumulation of knowledge to other fields?

We do, and it's called _open-source_. This design method and philosophy promotes access to the source materials or code used to make a product, thus allowing people to use pirates' methods of innovation, legally.

Much like piracy, open-source enables anybody to use or change already-existing ideas and products in order to improve them or create something entirely different. Look at Wikipedia's use of the open-source philosophy: their decentralized, open-source approach, in which anybody is free to add and change information, has produced one of the greatest collections of knowledge yet available.

In addition, the open-source movement set the course of history by ensuring that the building blocks of the internet could be utilized by anyone, and owned by no one. The result is a market in which cooperation between users leads to profits for companies, and opportunities for all.

Pioneered by the Homebrew Computer Club, whose early members included Steve Jobs and Steve Wozniak, the open-source movement gave birth to free software, such as Usenet and Unix, on the shoulders of which the internet was built.

The open-source platform opened the door for companies like Google, Ebay, and Facebook to innovate and profit on the internet's shoulders.

As companies profit from open-source models, new jobs are created for those who can provide the content and services the new market demands.

And just as the internet opened opportunities for companies like Google, we'll see more opportunities for self-employment, as individuals find ways to get paid for doing what they're passionate about within the open-source economy.

### 7. Open-source culture embraces the benefits of piracy to create new business models that help more people. 

It's always nice when people profit from their great ideas. But that doesn't mean the _world_ benefits, especially when one person has control of an idea just because he or she was the first in line at the patent office. In these situations, open-source culture helps the world by providing us with an alternative to privatized ideas.

For starters, it allows people to legally use the pirate's methods to create new things more efficiently. Its solution to the problem of hoarding intellectual property is to keep knowledge in the hands of consumers, while still allowing many different players to compete within the market.

The founders of the internet, for example, worked hard to keep internet browsers open-source, and in doing so allowed many companies access to the online market, rather than allowing it to be hoarded by those who invented or have control over it.

But not all industries adopt the open-source philosophy. In those industries, people choose to pirate products or services because it's the only way for them to reap the benefits of those products.

Sometimes the market fails to provide consumers what they need at an affordable price because one group has a monopoly on certain intellectual property. However, if these products were simply open-source, then there would be no need for piracy and the benefits of the product or service would reach more people, and in some cases can even be life-saving.

For example, despite the fact that 90 percent of those who suffer from HIV/AIDS live in developing countries, drug companies don't focus on distributing AIDS drugs there because the potential consumers can't afford them.

But when Dr. Yusef Hamied of Mumbai tried to offset this travesty by creating generic AIDS drugs for $1 per day, he was called a pirate and a thief, and the USA even blocked a vote in the World Trade Organization that would have allowed countries to import this cheaper drug during health crises.

We know now that the trend toward sharing information, legally or illegally, isn't going away. The following blinks will explore how to stay competitive in a world where piracy is inevitable.

### 8. It’s better for businesses to compete with pirates in the market than to try to squash them and their innovations. 

From pioneering new trends to making ideas more accessible for everyone, piracy has proven to be a valuable force in innovation. The big question for businesses, however, is how exactly to handle piracy: should they squash the innovation or embrace and compete with it? This is the _pirate's_ _dilemma._

Businesses who try to squash piracy run into some immediate problems. Because they ignore market demands — or worse, actively fight against them — they end up losing money. In fact, as a business, you can make _more_ money by embracing the idea that piracy is itself a valuable business model.

When EMI tried (and failed) to block the publication of the _Grey_ _Album_, a mashup of The Beatles' _White_ _Album_ with Jay-Z's _Black_ _Album_, online activists responded with a campaign to get EMI to back off. Eventually, EMI was forced to stand down, but the results were far from catastrophic. In fact, the _Grey_ _Album_ actually helped promote the _Black_ _Album_, increasing its sales!

The _Black_ _Album_ actually generated _more_ money for their distributors because of piracy's free promotion!

The lesson learned is that businesses that view pirates as competitors with valuable ideas, rather than mere nuisances to be squashed, will better adapt to the changing market.

According to Disney co-chair Anne Sweeney, piracy is a business model that competes in quality, price and availability, just like any other competitor. The only way to compete with it is to make the content the consumer craves available on-demand.

Apple's Steve Jobs also believed that the only way to combat piracy was to compete with it. His iTunes store has proved it can successfully compete with CD-burning and MP3-sharing pirates by offering cheap, easily accessible music downloads.

### 9. Punk capitalism is all about doing it yourself, resisting authority, and combining “altruism” with entrepreneurship. 

While piracy has been around since humans first had ideas that could be used by others, the punk generation redefined the ways piracy can be used as a force for change. Since then, _punk_ _capitalism_, the combination of piracy and entrepreneurship, has shown that anyone can say no to the mass market and make their mark on the world.

The most influential idea to come from punk capitalism is the D.I.Y. philosophy and the refusal to be bound by mass market forces. People naturally prefer things that focus on their individual creative power, rather than simply being told what to do.

Punk rockers even invented an entire genre of music, image and accompanying lifestyle that remains popular today as an expression of fierce independence and rejection of authority.

Richard Hell, for example, inspired a new freedom of expression for an entire generation of young people by pioneering a new spiky haircut and clothes with messages like "Please Kill Me." He refused to conform to mass-market images, and was so influential that there is now an entire exhibition in the Metropolitan Museum of Art dedicated to his aesthetic.

Just as punks did with music, _punk_ _capitalists_ resist authority by creating a culture that focuses on the power of individuals empowered by technology. Whereas punks had zines with simple chords that empowered them to create their own individual musical styles, people today have things like editing software and 3D printers.

And when you combine this rejection of authority with, what Mason calls, altruism (giving away your services for the benefit of another artist, company, cause or fans), punk capitalists can be a real positive force for change. Not only is altruism positive in and of itself, but it also has the ancillary benefit of attracting other people to join your cause.

Disco DJs in the seventies knew of both the direct and ancillary benefits of altruism, and so made their careers by promoting others' music, simultaneously attracting others to their movement. In the same way, people support Wikipedia because they feel strongly about sharing knowledge.

### 10. Hip hop entrepreneurs succeed through branding, authenticity, and altruism. 

Unlike the punk aesthetic, punk capitalism isn't limited to one genre or hairstyle. After punk music changed the world with their D.I.Y. mentality, hip hop entrepreneurs showed how punk capitalism is effective in entrepreneurship, succeeding in nearly everywhere they found a niche, from clothing to sports drinks.

In fact, the businessmen who started out in hip hop culture, or _hip_ _hop_ _entrepreneurs_, have proven to be masters of punk capitalism.

Punk capitalists profit by making something new by using pirated copies of something older, changing and sharing it. As a genre, hip hop excels at creatively utilizing available materials to make something novel.

Hip hop entrepreneurs sample parts of older, famous songs, making entirely new beats from them. In this way, they've reinvented the "remix," using technology to make something of their own.

In addition, hip hop entrepreneurs, like punk capitalists, resist authority by communicating their experience of the world, as they see it, rather than how mainstream media or history books might present it.

Where hip hop stars stand out most is in pairing branding and authenticity. Take hip hop artist 50 Cent, for example, who has successfully invested in things as unrelated to music as Vitamin Water.

However, because he is well known for his fitness regimen, fans see an authentic connection between him and his endorsement, and buy both the drink and his music as a result.

And just like the altruism found in punk capitalists, hip hop entrepreneurs show that you can indeed profit while changing the world.

P Diddy, for example, runs the charity Daddy's House. Russell Simmons, as well, founded the Hip Hop Summit Action Network. Jay-Z even partnered with MTV and the United Nations in order to shine the spotlight on millions around the world who don't have access to clean drinking water.

### 11. Advertisers have come to co-opt new styles so quickly that we only respond to substance, not image. 

Advertisers, always looking for a new way to get you to buy, are quick to jump on the bandwagon with every new trend that entices you. It's for no other reason that Burger King would hire P Diddy to hawk their chicken. But after years of trying to keep up and stay cool, are you buying their products?

Maybe not. Today's advertisers are so quick to co-opt new styles that people have completely stopped responding to the images that they tirelessly try to cultivate through their marketing.

Moreover, people are so wary of advertising that there is an inevitable blowback whenever they smell inauthenticity, which negatively impacts the brand's sales. For example, Pepsi produced an advertisement in the 1980s that mashed up the popular trends of break dancing and BMX biking in a commercial called "Bikedance." The result? It actually caused their sales to suffer for _two_ _years_.

Sony, too, suffered some blowback after they hired street artists to make ads for their PSP on apartment buildings in 2006. People responded by tagging over them with messages to cease and desist.

These campaigns were ineffective because people are overloaded with empty advertising and, consequently, have developed a preference for the intent and substance of a brand. People respond to the quality of a product and how companies choose to do business, not merely their brand's image.

For example, American Apparel, whose logo-less clothes attract consumers who are looking for basic wardrobe pieces — that won't make them look like a walking billboard — at one point sold _seven_ _times_ more than the industry average per square foot of store space.

Likewise, consumers care how companies treat their employees. They much prefer companies who, for example, pay employees a living wage and offer perks such as paid leave, subsidized lunches, and so on to one that employs sweatshop labor.

### 12. To compete as a punk capitalist, you must look outside the market, create a new vehicle, and harness your audience through participation. 

From DJing in the high seas to throwing raves at abandoned warehouses, pirates have always succeeded by taking their operations to the places everyone else had previously ignored. If you want to compete in the pirate's market, then you'll have to find your own, previously unclaimed space and bring others there too.

Since established channels have been set up specifically to others' advantage, it's a good idea to look outside the existing market. If you're going to compete as a punk capitalist successfully, you will need to create a vehicle to promote yourself independently.

A good example of this can be found in one of the greatest U.S. American cultural exports: film. Once Thomas Edison had successfully invented the means to produce film, he demanded licensing fees from others who wanted to use his fascinating new technology.

One producer, William Fox, decided that he would rather not pay these fees, and so went out west, where these licensing laws didn't hold. There, he founded the Fox Film Corporation, which paved the way for Hollywood and the Fox TV network today.

Once you've found your frontier space to flourish in, you need to allow your audience to actively participate in the creation of your product and make sure that they benefit.

With the right incentive, audiences will gladly participate in your project and are guaranteed to keep coming back for more.

For example, renowned promoter David Mancuso created the popular private loft party model that continues to be lucrative to this day. In fact, he totally reshaped music by getting early access records before anyone else and launching the first "pre-release parties" — a system DJs continue to use to get and distribute promotional records.

So what exactly made him so successful? He created a space where his audience would want to participate — and he even fed them!

### 13. You can make something viral by bringing it straight to your audience, and letting them decide how it grows and dies. 

With the advent of the internet, "going viral" has gone from a reason to visit the doctor to a cause for jubilation. It is pirates who have long been the masters of this new viral media. What can they teach us?

Firstly, you should speak only to your audience. You should know them extremely well, so you should have no need to get in touch with major record labels or film studios to do your public relations work. Whatever your medium, speak and share content directly with your audience, and they'll let the establishment know that they want more.

Grime rapper Dizzee Rascal, for example, made his career by speaking directly to his audience where he knew they listened: pirate radio. Likewise, you should engage with your audience on their home turf.

Be sure, too, to let your audience make the rules. Let _them_ decide how they want to engage your work, and it will become infectious as they share it.

A great example of this is the song "Dilemma" by So Solid, which popularized Grime music in the U.K. The track has sections that allowed listeners to rap over it every time it was played on pirate radio, thus making the instrumental a must-have for every DJ's collection and spreading the style like wildfire.

Once you've laid down the framework for producing viral content, continue to nourish the virus, but let it go when it's time. Nourish it by building buzz and releasing new, complimentary content when the buzz is at its peak. Once it starts to die down, however, it's time to move on to other projects.

Dizzee Rascal, for example, started selling his first single only after it became popular on pirate radio, and released his first album once his single became popular in the mainstream. However, once he realized Grime had passed its peak, he left the genre to continue his rap career with more diverse styles.

### 14. Final summary 

The key message in this book:

**Piracy** **is** **here** **and** **here** **to** **stay.** **It's** **often** **a** **force** **for** **good,** **and** **businesses** **and** **individuals** **who** **learn** **to** **compete** **with** **pirates** **using** **their** **methods,** **rather** **than** **simply** **trying** **to** **squash** **them,** **will** **be** **the** **most** **successful.**

Actionable advice:

**Explore** **uncharted** **waters.**

It's hard to make it big within existing market environments, since those environments have been carefully calibrated so that they benefit only a few people or businesses. Instead, set your course for uncharted terrain, where you can forge your own environment and make your own rules.

**Suggested further reading:** **_You_** **_Are_** **_Not_** **_a_** **_Gadget_**

Now that you've read about the effects of piracy from a piracy advocate's perspective, why don't you check out an opposing viewpoint? Read Jaron Lanier's _You_ _Are_ _Not_ _a_ _Gadget_ in blinks to understand why he believes online piracy could spell the end of original, high-quality content.
---

### Matt Mason

Matt Mason is a former pirate radio DJ and founding Editor-in-Chief at the music magazine _RWD_. In addition to writing for various publications, such as _Vice_ and _The_ _Guardian_, Mason also writes and produces numerous comics, screenplays, apps, TV series, recordings and advertising campaigns.

