---
id: 5ab4324db238e10005bf5837
slug: the-plant-paradox-en
published_date: 2018-03-23T00:00:00.000+00:00
author: Steven R. Gundry
title: The Plant Paradox
subtitle: The Hidden Dangers in "Healthy" Foods That Cause Disease and Weight Gain
main_color: AF232C
text_color: AF232C
---

# The Plant Paradox

_The Hidden Dangers in "Healthy" Foods That Cause Disease and Weight Gain_

**Steven R. Gundry**

_The Plant Paradox_ (2017) alerts us to the dangers of eating seemingly healthy plant foods. It explores the differences between our diets and those of our ancestors' and tells us which food products we should eat and which we should avoid to improve our digestion and maintain our optimal weight.

---
### 1. What’s in it for me? Don’t fall for the hype. 

Chia seeds, quinoa, açai berries, goji berries, kale — It seems like everyone is into super foods and healthy eating.

But most people are unaware that some "healthy" foods are doing us more harm than good. Just take lectins. Lectins are a protein found in plant-based foods such as legumes and grains; and they can also cause weight gain and damage our health.

These blinks take you back in time to show you how the nutritional values in fruits and vegetables have changed. They explain which plant-based foods are harmful and to be avoided, while introducing you to the Plant Paradox Program so that you can start taking better care of your body.

You'll also learn

  * what the human digestive system and a nuclear power plant have in common;

  * what legumes are and why you must avoid them; and

  * the recipe for a nutritious, body-cleansing green smoothie.

### 2. Defensive plant proteins cause confusion and weight gain. 

If you've ever seen a wildlife documentary, you'll be aware that animals don't just sit around waiting to become another animal's dinner. Instead, prey animals develop defenses like long legs to outrun predators and advanced hearing and eyesight to sense approaching danger. But what about plants? If you have always assumed that plants are any more willing to become your next meal, think again.

Just like animals, the plants we consume also have defensive strategies against predatory attacks. A crucial weapon in their arsenal is a family of plant proteins called _lectins_, which exist in the leaves, seeds, skins and grains of most plants.

When lectins are consumed, they bind to sugar molecules in the animal's brain and nerve endings, inhibiting the predator's cells and nerves from communicating properly. This lack of communication between your nerves and cells results in _brain fog_, a term used to describe moments when you're experiencing memory difficulties or having trouble focusing.

Plants produce these lectins in an attempt to teach predators that they should avoid eating those plants in the future.

Unfortunately for humans, some lectins not only stimulate mental confusion, but also cause you to gain weight.

One type of lectin that causes weight gain is _wheat germ agglutinin_ (WGA). Found in wheat, this plant protein causes sugar to enter our body's fat cells, where the sugar is then transformed into fat, thus making us put on weight.

In fact, these fat-producing properties are the reason wheat was originally favored as the grain of choice by those living in northern climates. It helped them gain and maintain weight in a time when food was far more scarce. Back then, having a "wheat belly" would help you survive the cold winters.

In this day and age, however, with an overabundance of food and the ubiquity of central heating, the weight gained from plant consumption isn't such a welcome side effect.

### 3. Lectins are harmful, but they also saved our ancestors’ lives. 

In the previous blink we learned that even though they can keep us full, lectins cause us serious damage. But if they are not the ideal option, why are these foods in our diet in the first place? Why did our ancestors start eating them all those years ago?

The truth is that they had little other alternative.

Until about ten thousand years ago, which isn't that long ago in the grand scheme of our evolutionary history, the average diet was largely made up of animal proteins. Unfortunately, this was also around the time the last Ice Age ended, when many of the animals living in cold climates died. As a result, there was less animal protein to consume, and humans had to find another food source.

The other food source came in the form of grains and legumes, and with that, agriculture was born. Domesticating plant sources saved humanity from starvation and introduced us to many different types of lectins that we'd never been exposed to before. Quickly, grains and beans became a staple diet in many cultures all over the world, leading to both pros and cons.

For example, in Ancient Egypt five thousand years ago, there were bountiful wheat granaries that held enough stock to feed all the local people, as well as the slaves who built the pyramids. This abundance of food enabled Egypt to rise into a mighty and prosperous kingdom. But according to modern analyses of mummified remains of ancient Egyptians, those whose diets were high in grain were overweight and showed signs of tooth decay.

So, though grains kept our ancestors alive, the lectins contained in those plants were harmful to their health.

> "Mice and rats evolved as grain eaters 40 million years ago and have had far longer to become tolerant of these lectins: four thousand times longer."

### 4. Lectins contained in whole-grain products will drive your body to attack itself. 

What do the human gut and a nuclear power plant have in common? Well, similar to a nuclear power plant, your gut holds energy-producing elements that must be contained to prevent it from damaging surrounding areas.

Your gut or intestinal tract contains trillions of different microscopic microbes. More precisely, 90 percent of our body cells are non-human, meaning that 90 percent of what we consider to be "us" is no more than a collection of non-human microbes!

These microbes are what convert the food we consume into energy. Without them, food would be completely useless to us, and we would quickly starve to death.

We need microbes to help us survive, and our intestinal tract acts as a prison and keeps them in place. If the microbes can escape from the intestinal tract, our body recognizes them as foreign invaders — that is, as an illness — and the immune system is alerted. Thus, our body begins to attack itself.

This self-attack on our bodies is triggered when we eat whole-grain products because the lectins increase the permeability of our intestinal tract, allowing the microbes to break through.

This new health risk only exists today because for centuries societies used to remove _bran_ — the crucial component that defines a food as "whole-grain" — from grain. Think of French baguettes, Italian ciabatta and the white rice found in Asian countries — all bran-free.

Whole grains are now deemed a "health food," so people want to incorporate them into their diets, thereby funneling lots of lectins into their bodies to weaken the defenses of their intestinal tracts, making them more susceptible to illnesses such as Crohn's disease, which is an inflammatory bowel condition.

So, we know lectins are bad news. What can we do to prevent them from causing us harm? Let's continue with the next blink and find out.

### 5. The Plant Paradox Program is concerned with ridding your diet of unhealthy substances, like lectins. 

So now we know about the harmful effects of consuming lectins. The next step is to learn how to regain not only your digestive but also your overall health. It's time to take a look at the author's six-week _Plant Paradox Program_.

Anyone looking to start the Plant Paradox Program should know and internalize the first and most fundamental rule: what you don't eat is more important than what you do eat. That is to say, getting rid of harmful foods will have a more powerful outcome for your well-being than adding good ones.

A patient of the author found that following the Plant Program cured his _vitiligo_, a skin condition. The author could have easily claimed that the favorable results came from a diet of anti-inflammatory, antioxidant-rich foods as outlined by the program. However, he knew that wasn't the case. The patient's recovery was due to him no longer eating the wrong foods — that is, food containing lectins — and adding harmful substances to his gut.

Keeping this in mind, to take care of your health you need to abide by the rules of the Plant Paradox Program and completely cut out a number of unhealthy foods from your diet.

One food group that you must eliminate is legumes, which are foods such as peas, lentils and all types of beans, including soybeans. This is because legumes — especially beans — contain more lectins than any other food group. In 2012, the Centers for Disease Control claimed that one-fifth of all food poisoning incidents in the United States were from lectins found in undercooked legumes.

### 6. Get optimum results with the Plant Paradox Program by starting with a three-day cleanse. 

Similar to how a farmer prepares the soil before he plants any crops, you must also get your gut ready before beginning the Plant Paradox Program. What's the best way to prepare the environment within your gut?

The answer is a body cleanse.

If your gut is already looking worse for wear, then healthy foods won't have much of an impact on you. Therefore, you must first focus on repairing any damage to your gut with a three-day cleanse.

The first step of this process involves removing all possible traces of lectins — we're talking fruit, legumes, grains, dairy, sugar, seeds, soy, eggs, beef, tomatoes and root vegetables. Keeping away from these products will effectively put a stop to inflammation in your body, which is set off by your immune system responding to lectins, thereby enabling your gut to begin healing itself.

Though you have to stay away from several products during a cleanse, this doesn't mean that you have to sacrifice all the foods you love.

There are plenty of other nourishing options from which to pick. For example, you could start your mornings with an invigorating, body-cleansing green smoothie, made from romaine lettuce, mint, spinach and avocado. Or why not have the daily recommendation of up to eight ounces of pastured chicken or wild-caught fish served with a side of vegetables such as cauliflower, cabbage or broccoli?

Regardless of which gut-restoring vegetables you pick, make sure you either eat them raw or cook them in healthy oils like avocado oil or extra-virgin olive oil. Then finish off your meal with some coffee or green or black tea. And don't forget to drink eight cups of water each day!

> _"Vegetables can be fresh or frozen. If fresh, they should be in season and grown locally."_

### 7. Modern-day foods don’t contain as many nutrients as they did historically, but supplements can reduce that gap. 

In 1936 scientists discovered that the fruits and vegetables consumed by American citizens were lacking certain vitamins and minerals. Fast forward 81 years and things aren't any better. Luckily, we're able to make up for this nutritional shortage with supplements.

The fruits and vegetables we eat nowadays are losing their nutritional value. A 2003 report examining the mineral content of fruits and vegetables over the period 1940 to 1991 found that their nutrition content was decreasing steadily. One explanation for this decline in nutrients is that, since the 1950s, we've upped the use of strong petrochemical fertilizers and pesticides on our crops.

The low nutrient content in our fruits and vegetables means that they don't do much for our health. However, you can compensate with supplements during the Plant Paradox Program.

Way back then, our ancestors were hunters and gatherers who only ate plants growing from soil that was organic and rich in minerals, as well as animals which also fed strictly on these plants. Consuming these plants and animals meant that our ancestors' diets also had a pretty high mineral content.

Nowadays, you can substitute these important minerals with supplements such as _vitamin D_, which is crucial for cell regeneration in your intestinal walls, and _vitamin B12_, which is vital in protecting the inner linings of your blood vessels.

Our ancestors enjoyed enriched diets, and by following these nutritional guidelines, we can try to match their healthy way of living.

### 8. Final summary 

The key message in this book:

**For many, many years, the plant foods we think of as healthy have been destroying our bodies. Some plants contain proteins called lectins, which are released to poison the predators that try to consume the plant. In other words, by eating these plants, we end up poisoning ourselves. To reverse this damage to our bodies, the author outlines the six-week Plant Paradox Program, which is designed to eliminate all traces of lectins from our diets.**

Actionable advice:

**Don't eat a yoga mat.**

You probably wouldn't want to eat your yoga mat for dinner, right? So why are you eating at McDonald's, Burger King or any other fast-food restaurant? The food you consume at these eateries contains a substance that is also found in your exercise mat: _Azodicarbonamide_. It's used to bleach flour and condition bread dough. The chemical makes the gluten in bread more immediately available to your digestive system, subsequently irritating your gut. So remember to keep on walking when you come across another McDonald's.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Super Immunity_** **by Joel Fuhrmann, MD**

_Super Immunity_ (2011) reveals the secret to a better, stronger immune system and healthier body: superfoods. These blinks shed light on the shortcomings of modern medicine and teach you how to take advantage of the healing powers of plant foods rich in nutrients and phytochemicals.
---

### Steven R. Gundry

Steven R. Gundry is a former professor of cardiothoracic surgery at Loma Linda University and the director of the Center for Restorative Medicine in California. He is also the author of _Dr. Gundry's Diet Evolution_.

