---
id: 54292e8866656200086b0000
slug: the-poisoners-handbook-en
published_date: 2014-10-01T00:00:00.000+00:00
author: Deborah Blum
title: The Poisoner's Handbook
subtitle: Murder and the Birth of Forensic Medicine in Jazz Age New York
main_color: 60225B
text_color: 60225B
---

# The Poisoner's Handbook

_Murder and the Birth of Forensic Medicine in Jazz Age New York_

**Deborah Blum**

_The Poisoner's Handbook_ details the work of New York City's first scientifically trained medical examiner, Charles Norris, and his partner, Alexander Gettler, the city's first toxicologist. It offers an insider's view of how forensic science really works by walking readers through their investigations into notorious and mysterious poisonings.

---
### 1. What’s in it for me? Learn about the gruesome history of poisons and the advent of forensic toxicology. 

In the Jazz Age of New York City, dangerous chemicals were everywhere: on the streets, in the factories, in the home, and even at the pharmacy. Whether people were heedlessly intoxicating themselves with bootleg alcohol, unwittingly rubbing lethal substances onto their skin, or making deliberate attempts to get an early inheritance, poison was everywhere and at everyone's disposal.

Unfortunately, many poisonings, whether accidental or of murderous intent, were misdiagnosed by the city's bumbling coroners and incompetent justice system. No one knew much about forensic toxicology or chemistry, or even plain old forensics, for that matter. Yet this all changed for the better thanks to two pioneering scientists.

In these blink you will follow the investigations of New York City's first professional medical examiner, Charles Norris, and his "partner in crime," Alexander Gettler, as they look into mysterious deaths by poison, while setting and improving the standards for forensic toxicology.

In reading these blinks, you'll discover

  * that many would rather risk an early death than go another day without alcohol;

  * why not even science could convince a jury to give a guilty verdict; and

  * that beauty products made before 1938 could make you bald, or even kill you.

### 2. Before forensic toxicology became a science in New York, murder by poison flourished in the city. 

We've all seen it at some point on our favorite TV detective drama — someone moves the date of their inheritance forward by poisoning their helpless old grandma. Today, the sophisticated methods of modern forensics mean that the plotters never get away with it, but this wasn't the case in the not-too-distant past.

For a long time in New York, murder by poison was a common way to eliminate those who stood in your way. And due to the utter incompetence of the American coroner system, poisoning could be carried out with near impunity!

Despite the fact that coroners' only job was to determine the cause of someone's death, a report from 1915 revealed that coroners at the time didn't need _any_ medical background or even training. This meant that death certificates were completed without any effort to determine the cause. Reported causes of death ranged from things like "either assault or diabetes" to "act of God."

There were cases where the coroner system made sentencing impossible, even when people confessed to poisonings.

For example, in the winter of 1915 in Manhattan, Frederic Mors confessed to murdering eight people using chloroform. However, when the coroner erroneously stated that this couldn't be true (since he believed it took longer for chloroform to kill a person than Mors had reported) the prosecutor hesitated to believe the confession.

This is just one example; due to the ignorance of coroners, _many_ murderers ended up walking free and leaving their crimes unsolved.

Thankfully, the relative ease by which people could get away with murder by poison came to an end when _forensic toxicology_ — the study of how chemicals adversely affect living organisms — became a fully legitimate science in 1918.

After the weaknesses of the coroner system attracted public attention, New York City implemented reforms that elevated forensic toxicology to a true science, hiring its first _trained_ medical examiner and a chemist who founded the first toxicology laboratory.

### 3. Medical examiner Charles Norris and toxicologist Alexander Gettler revolutionized forensic toxicology. 

The negative public attention placed on the New York coroner system caused the state legislature to pass a bill that established a new medical examiner system for New York City. It required the position to be filled by a qualified forensic scientist — one with experience in pathology, who would need to pass professional tests in order to land the job.

That scientist was Dr. Charles Norris, who would set forensic standards for the rest of the country.

Norris had a lot of cleaning up to do when he was appointed as the new medical examiner in 1918. He fought tirelessly to improve forensic standards, even spending his own money on equipping his department in order to supplement its paltry funding.

Among other things, he developed new rules for handling bodies, and required that files were kept on each case so that testimonies in court could reference recorded information rather than the coroner's "memory," as was the case in the past.

In addition, he harassed other city departments to elevate forensic standards. For instance, he called attention to the bribery that caused police to conceal murders, and badgered hospitals to hasten the transport of bodies to the morgue for examination.

Moreover, he made the wise choice of hiring forensic chemist Alexander Gettler, and together they revolutionized forensic toxicology. Gettler was a passionate believer in the power of chemistry, and if a test or research method didn't exist, he developed one himself.

For example, he once examined over 700 human organs to test the effects of alcohol, discovering that drinking a tumbler of wood alcohol could kill a man within a few hours — a fate that befell many after Prohibition began in 1920.

Together, Norris and Gettler made a wealth of discoveries and tracked innumerable poisons, and in doing so revolutionized forensic toxicology in New York. In the following blinks you'll learn more about what they uncovered.

### 4. The investigation of two cyanide deaths was complicated, but helped build better forensic toxicology. 

The poison death of an elderly couple, the Jacksons, in a Brooklyn hotel in 1922 caused much distress, but also offered an invaluable lesson in toxicology.

The couple's lungs clearly contained the deadly poison cyanide, which was bad news for hotel management, who had been hiding the fact that the basement was being fumigated at the time of the couple's death.

In the 1920s, hydrogen cyanide gas was routinely used for fumigation against pests, making it highly likely that the Jacksons had been overcome by cyanide gas.

In the trial that followed, the defendants — both the fumigator and the manager of the hotel — didn't deny the fumigation. Instead, they focused on the lack of proper scientific evidence put forth by toxicologist Gettler, saying that cyanide could not be detected in a decaying body.

Despite the fact that Gettler had inspected the Jacksons' lungs _and_ found clear evidence of cyanide poisoning, toxicology was such a new science that the jury was at a loss. It proved difficult for Gettler to educate them and convince them simultaneously!

When prosecutors lost the case, Gettler became inspired to do intense research for the next decade and longer, eventually once and for all proving that the defendants had gotten away with manslaughter.

In 1938, he published his findings in _The Toxicology of Cyanide_. This definitive text is still referenced by toxicologists and government agencies nearly a century later.

What's more, the investigation into the Jacksons' deaths also triggered efforts to improve the relationship between medicine and law. Chief medical examiner Norris started a crusade that would force the court to give forensic toxicology the respect it deserved, and even helped to set national standards by forming a committee with representatives from other cities across the United States.

### 5. Arsenic is an ideal poison for murder and its detection is central to forensic toxicology. 

In the 1920s you couldn't set foot in a hardware, drug or grocery store without encountering the subtle but lethal arsenic. Rat poison, bug killer, weed killer, and dyes of all sorts contained arsenic in different variations. This, of course, meant that anyone wanting to use the poison for nefarious ends could acquire it both easily and cheaply.

Besides its easy availability, arsenic has certain properties that make it an ideal murder weapon. As long as it's administered in small enough doses, it can easily be slipped into foods or drinks undetected, since it won't change the appearance or the taste.

It's no surprise then that, of 820 randomly selected deaths caused by arsenic in the eighteenth and nineteenth centuries, almost half were homicides.

Arsenic proved difficult to detect if it was administered gradually, causing doctors to misdiagnose arsenic-related deaths as influenza, heart disease or cholera. As a result, it quickly earned the nickname "inheritance powder," as people would use it to kill those who stood between them and their share of the family funds.

Because arsenic was such a popular murder weapon, learning how to detect it was an important step forward for forensic toxicology and the administration of justice.

Thankfully, an array of reliable tests designed to discover arsenic were developed in the nineteenth and twentieth centuries. Nowadays, arsenic is quite easy to detect: because it's a metallic element, it breaks down relatively slowly when compared to organic compounds, and also leaves characteristic lesions in the body.

In addition, arsenic "mummifies" the bodies of its victims by slowing down the natural decomposition of human tissue, thus creating very well-preserved corpses!

These discoveries made arsenic poisoning a riskier choice for committing murder. However, finding the poison is unfortunately not the same as finding the killer.

> _"In fact, handled with skill by a calculating murderer, the poison seemed to engender a homicidal overconfidence."_

### 6. Mercury was available in many commercial products, and became a popular way of poisoning. 

The industrial innovations of the early twentieth century brought a flood of risky chemicals into people's everyday lives, with each new chemical creating new opportunities for the clever poisoner.

Mercury, an easily procured and highly toxic metal, was among them.

Although mercury is acutely dangerous in its purest form, it was nevertheless available in a huge variety of commercial products and drugs.

Mercury salt — nicknamed quicksilver for its tendency to break into little, skittering balls when touched — was sold as bedbug killer, laxative, antiseptic, and even prescribed for bacterial infections, such as syphilis.

In spite of these uses, mercury is highly dangerous. Our living tissue soaks up the salts, allowing the poison to steadily dissolve our organs in horrifying ways: eventually the stomach erodes into bleeding ulcers and teeth loosen in the gums.

In other words, mercury is extremely poisonous, yet doesn't offer a quick death. In one particular case that Gettler came across, a woman who had swallowed an entire bottle of mercury tablets suffered two weeks of agony before finally dying.

The public was finally made aware of the lethal potential of mercury after a Hollywood scandal in 1920.

Olive Thomas, an actress who starred in one successful film after another, was involved in a stormy relationship with another famous actor of the time, Jack Pickford. One night after a party she accidentally swallowed Pickford's mercury potion (which he used to treat his syphilis sore) instead of her own sleeping medicine. She lingered in hospital for three days before finally succumbing to the poison.

Unfortunately, this was a rather typical way of dying of mercury poisoning at the time. In fact, one estimate says that out of the approximately 20 deaths per year caused by mercury poisoning, most were suicides and unfortunate accidents similar to Thomas's.

### 7. The industrial by-product carbon monoxide proved to be an exceptionally reliable killer. 

In 1925, the New York City medical examiner's office counted 1,272 people run over and killed by cars.

However, there are more ways to die in a car accident than in a head-on collision; toxicologists also worry about the chemicals released by their engines, such as carbon monoxide (CO).

Carbon monoxide can be found wherever humans live and work in modern industrial societies. Streets filled with cars, an increasing reliance on gas stoves or heaters, and pollution from factories have meant that every living creature who is exposed to city air also inhales a steady dose of CO.

This wouldn't be such a big deal if CO weren't so poisonous: Norris estimated that CO was responsible for around a thousand deaths in New York every year. With so many deaths, learning how to detect CO in the body was crucial in separating the accidents from the murders.

One such case involved a man who murdered his victim by forcing a gas tube filled with CO into his mouth. He then placed the victim in a bathtub and reported his death as a drowning. However, toxicology reports showed that the victim's blood was filled with CO, while his lungs contained no water — a clear case of murder!

Yet sometimes verdicts went the other way, and accidents were mistaken for murders. For example, in 1926 Francesco Travia was caught lugging part of a woman's body toward the New York waterfront. Detectives naturally suspected murder, but toxicologist Gettler proved that the CO gas from Travia's stove had caused her death.

Travia had simply panicked, thinking that he would be charged with murder, and tried to get rid of her corpse. He ended up serving time only for illegally disposing of a body.

Without Gettler's revolutionary methods in detecting CO, Travia would have certainly ended up in the electric chair. This case was an important victory for forensic toxicology, as it proved that the sober testimonies of medical experts played a vital role in the courtroom.

> _"So in the dark of early morning, he decided that his only chance was to get rid of the body."_

### 8. Wood alcohol was the best poison at hand, leading to a public health crisis. 

In 1920, Prohibition became law in the United States and alcohol was banned, with the hope of ending drinking. It very soon became clear, however, that rather than obey the law, citizens were drinking more than ever — more recklessly, too.

This flagrant disregard for the law was made possible by an illegal alcohol trade that flourished on stolen industrial alcohol.

Since 1906, the government had required that manufacturers poison, or "denature," their industrial alcohol if they wanted to avoid paying liquor taxes. By the 1920s, manufacturers added numerous poisons to achieve this, the simplest among them being _methyl_, or wood alcohol.

Illegal alcohol suppliers, known as bootleggers, recruited chemists to help them clean up the alcohol and make it safe by filtering out the poisonous additives.

To combat this, the government decided to make drinking industrial alcohol even _more_ risky by adding greater amounts of wood alcohol to the spirits. The process of detoxifying wood alcohol was already complicated, and these new regulations made it even deadlier and more difficult to render safe.

The idea was that if people were going to continue to break the law, then the government would just make even better poisons that would make the alcohol unrescuable, and thus undrinkable.

Unfortunately, making alcohol more dangerous didn't stop people from drinking it, and many were blinded or killed.

In 1926 the consumption of industrial alcohol caused some 1,200 people to become ill or blind, and led to 400 deaths in New York alone. These numbers far exceeded the number of deaths due to alcoholism before Prohibition.

Distraught by the number of alcohol-related health problems, medical examiner Norris delivered a report to the mayor of New York describing the public health crisis resulting from Prohibition. His findings sparked public debate and led many newspapers to label the federal government "a mass poisoner" that should be charged with the moral responsibility for these poison deaths.

### 9. No other poison caused as many deaths or led to as many diseases as ethyl alcohol. 

As you've just seen, Prohibition caused many deaths due to alcohol mixtures not designed for human consumption. However, a great deal more were due to drinking everyday, normal alcohol: _ethyl alcohol_.

Indeed, ethyl alcohol has caused more deaths and illnesses than any other poison, and its consumption only escalated during Prohibition. Interestingly, it was the destructive behavior, sickness and death caused by the overconsumption of ethyl alcohol that led to Prohibition in the first place.

However, Prohibition proved counterproductive — now that alcohol was illegal, people drank just to get drunk, causing heavier and more rapid consumption of ethyl alcohol. In 1930, deaths due to alcoholism were 600 percent higher than they were in 1920, when alcohol had just been prohibited. These numbers indicate that Prohibition helped foster a culture of heavy drinking in the United States.

The increasing threat posed by ethyl alcohol prompted chemists to research its exact effects on the brain and body, and escalating rates of alcoholism during the Prohibition period offered a great wealth of material for building a scientific understanding of alcohol intoxication.

Alcohol-related deaths were an everyday occurrence, leaving chemists with countless test subjects at their disposal. During this period, toxicologist Gettler decided to investigate the way in which ethyl alcohol compromises the body and brain.

After five years of research and tests on around 6,000 brains, he found that the degree of intoxication correlated with the alcohol levels in the blood and in the brain, leading him to publish what many consider the first scientific intoxication grading method.

In addition, Gettler performed a separate series of studies using dogs as test subjects, which showed how the body of a habitual drinker adjusted to alcohol consumption, becoming more efficient at metabolizing ethyl alcohol — although no one developed actual immunity.

### 10. It took many years for radium to be considered a killer as well as a savior. 

During World War I, people discovered that they could make watch faces glow in the dark by mixing radium into the paint used on their dials. This made the watches more suitable for battlefields.

The young factory workers who painted the watch dials with luminous paint were called the Radium Girls. To help apply the paint, they would often wet the tips of their brushes with their mouths in order to get a sharp point.

At the time, no one had the slightest inkling that this highly radioactive element could be considered anything but healthy and healing. At the beginning of the twentieth century, radium was considered a miracle cure: "radium therapy" was used by hospitals in an attempt to shrink cancer tumors, radium water was thought to have energizing effects, and radium facial creams and powders promised rejuvenation.

However, radium is extremely harmful, and unlike traditional poisons, such as arsenic, which poison in a single, direct dose, radium exposure inflicts a lifetime of harm.

After a few years, the Radium Girls began dying in mysterious and horrible ways — their jaws literally crumbled and their bones broke underneath them. By 1924 nine had died.

Luckily, an investigation into their deaths showed the danger of radium exposure for human beings.

In 1928, Norris and Geller were brought into the investigation and were asked to look at one of the young women who had died in 1923. They extracted pieces of her bones and wrapped them in photographic paper to determine whether they were radioactive. Sure enough, this method showed that each piece of bone was still emitting rays even after _five years_.

Though reports on their findings were published, it would be more than 20 years before the health hazards of radium became fully understood by the public.

> _"Hardly a quibble, hardly a doubt was raised, that radium might not really be the golden child of the elements."_

### 11. The use of thallium in cosmetic products caused many unnecessary deaths. 

There was a time when it could have easily appeared as if modern society were addicted to poisons simply because of their prevalence in everyday products. In fact, it wasn't until 1935 that legislation proposed requirements regarding safety testing before a product was introduced on the American market.

Before that, manufacturers and companies didn't even consider providing consumers with basic information about the ingredients in things like household cleaners, medicine or cosmetics.

One particularly poisonous ingredient used in these products was the metallic element thallium. Not only was it used in industrial products and rodent poisons, but also depilatory creams, which were widely advertised in the Jazz Age.

These creams did indeed remove hair as advertised — a little too thoroughly. Though thallium was meant to be completely harmless in small doses, creams containing it caused women to go bald and even blind after applying just a small amount to their face.

Moreover, a lack of government regulation meant that thallium could easily be used as a murder weapon.

Thallium was an ideal poison: the symptoms — including nausea and vomiting, trembling and shortness of breath — could easily be mistaken for an infectious disease. In addition, it mixes easily into liquids, and is tasteless, odorless and colorless.

Gettler became involved in a particularly tricky case of thallium poisoning when, in 1935, a book keeper named Gross was accused of killing his family by mixing thallium into their cocoa powder.

Gettler determined that one would only need a single ounce of thallium salts to quickly kill just about anyone. However, to everyone's surprise, he also found that it wasn't Gross, but his wife that had killed the children, before dying coincidentally of natural causes. Why? Because she wanted to start over in her marriage with Gross.

Despite cosmetic ingredients such as thallium proving to be so poisonous, it wasn't until 1938 that the Food, Drug and Cosmetic Act finally made safety testing and accurate labeling a requirement for manufacturers.

### 12. Final summary 

The key message in this book:

**Chief medical examiner Norris and toxicologist Gettler of New York City worked tirelessly to develop the forensic tools they needed for criminal investigations. They revolutionized forensic toxicology by developing better ways of detecting a wide variety of poisons, and by improving medicolegal standards.**
---

### Deborah Blum

Deborah Blum is an American professor of journalism who writes for the _New York Times_ and blogs for _Wired_. She received a Pulitzer Prize in 1992 for a series of articles entitled The Monkey Wars, and is also the author of _Ghost Hunters_.

