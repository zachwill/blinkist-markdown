---
id: 55daef371f4e6e000900004e
slug: the-politics-of-promotion-en
published_date: 2015-08-25T00:00:00.000+00:00
author: Bonnie Marcus
title: The Politics of Promotion
subtitle: How High-Achieving Women Get Ahead and Stay Ahead
main_color: 2F90A1
text_color: 257280
---

# The Politics of Promotion

_How High-Achieving Women Get Ahead and Stay Ahead_

**Bonnie Marcus**

_The Politics of Promotion_ (2015) offers insights into the ways women can prime themselves for promotion in any line of work. Filled with actionable tips and strategic career advice, it provides the political savvy you need to maneuver within the workplace and secure your next promotion.

---
### 1. What’s in it for me? Learn to maneuver the politics of promotion. 

You know how Fortune magazine ranks the 500 most successful companies and executives every year? If you expect it to reveal anything close to a 50/50 split between men and women in top positions, think again! Currently, women hold only 5.2 percent of Fortune 500 CEO positions. If you see this statistic as something that just has to change, you're not alone. 

These blinks offer valuable insights into the politics of promotion, a particularly useful tool for women. They'll provide essential pointers on the importance of knowing and understanding the internal politics and culture of your organization, and not least on how to proceed in order to get the promotion you so badly want.

In these blinks, you'll discover

  * why performing well is not the only thing you need to do in order to get promoted;

  * why being excluded from Monday night's ballgame has a negative bearing on your career advancement; and

  * why power in certain contexts is better than likability.

### 2. Job advancement is highly political – even more so if you’re a woman. 

You perform well at work. Your projects all receive high praise from your boss. Yet, when your next promotion opportunity comes along, you're left empty handed. Why? 

Workplaces are highly political environments, not meritocracies. Advancement in the business world requires not _only_ skill and competence, but also political savvy.

Politically savvy people know how to subtly work the system. They are conscious of their surroundings, and can easily identify which stakeholders and relationships are important for their success, using this knowledge strategically in order to get ahead.

It all starts with building good relationships with those in power, that is, the ones who have influence over the decisions that will affect your career trajectory.

Without this political savvy, you can deliver great results time and time again and still be passed over for promotion. It could be that your colleague, who, despite being less competent, still got the promotion you think _you_ deserved, simply bonded with the people who had influence over the decision of who to promote.

The need to bond with important stakeholders is clear enough. But that's easier said than done for women in the workplace, as company culture and politics today are still biased against them.

Even if a company has made it clear in its guidelines that all employees are to be treated equally, women still face challenges when it comes to building workplace relationships.

For example, a woman in a team mostly comprising men probably won't get invited to weekend fishing trips or Monday night ball games. It won't necessarily be out of malice, but simply because she is a woman, and her colleagues assume that she wouldn't be interested in these activities or wouldn't fit in. 

But if she's excluded from theses outings, she's also excluded from the important conversations and bonding opportunities that come with them. As a woman, you have to be creative in forging bonds with the men in these networks, even though they might not make an effort to do the same.

### 3. Women need to be aware of the value they create and promote themselves by talking about it. 

Picture that colleague of yours who is always talking about his achievements at work, and you can't help but feel that he's bragging. But is he really? Or is he preparing his promotion by promoting himself? 

Performance in the workplace doesn't speak for itself. Not everyone sees or has the context to understand your work, so you need to actively cultivate your reputation across an organization by talking about it.

If you assume that performance speaks for itself, then the chances of career stagnation increase. In fact, research shows that women who know how to talk about their accomplishments and achievements receive both more promotions _and_ higher compensation. In contrast, those who keep it to themselves are more likely to get neither. 

Imagine that your company finally closes a massive deal after seemingly endless negotiations with another company. These hard-fought negotiations were brought to a close thanks to _your_ convincing argumentation.

After the meeting, what should you do? You should make sure to meet with your boss and tell them about the successful negotiations! Don't just assume that they know a deal was made, and that it was _your_ savvy that finally made it happen.

But sometimes your contributions aren't so cut and dry. In order to be able to promote yourself, you also need to be aware of your _value proposition_, in other words, the ways in which your work contributes to positive business outcomes.

It's important to know your value proposition, because you can only really promote yourself once you know what you bring to the table.

To figure out your value proposition, write down some situations at work in which you were recently involved. Next, ask yourself how you specifically contributed to the success of these situations. This will give you insight into the skills and competencies that you can promote as part of your reputation.

### 4. Understand the decision-making process and the culture of your organization. 

Most people believe the path to promotion involves keeping your head down and powering through your tasks — but this is absolutely incorrect. Rather, promotion depends on keeping your head up and looking around your company.

The first step toward promotion is understanding how decisions are made and detecting the hidden rules that influence the decision-making process.

Decisions in organizations are almost never made by a single person, but instead by many people. You need to find out which people have the most influence on the decisions that affect you — like promotions — and build good relationships with them.

One way to figure this out is through simple observation. Ask yourself questions like who the management consults with before making important decisions.

You should also be aware of unspoken rules. Though your organization's official policy might be to "hire and promote the best people for the job," it's likely that there are other rules, such as tenure, that affect your promotion.

So don't worry if you aren't promoted right away. Just develop an awareness of these informal rules in order to understand the organization and your position in it.

But hidden rules go beyond the promotion process. There are also unspoken rules in the company culture — the personality of your company that defines the codes of behavior that are accepted, rewarded or viewed as favorable. 

If you want your career at a particular company to prosper, you have to behave in accordance with their company culture.

This will vary greatly depending on where you work. Company A, for instance, might have an unspoken rule that the one who barks the loudest gets to speak. In this environment, you have to interrupt others to make your point and thrive.

This same strategy might not work for company B, where patience and listening are more highly valued.

At this point, you've learned some of the things that you can do immediately to start preparing for promotion. The final blinks will examine the kinds of people that can help you along the way.

### 5. Building a strategic network with powerful people is important for your career advancement. 

Imagine that your colleague just got promoted to a position that you didn't even know was vacant. How did that happen? In all likelihood, her network provided her with the right information at the right time. 

You can do the same by building a _strategic network_, a group of people who support, protect and promote you.

Your strategic network doesn't just help you get promoted. It also provides you with valuable information about the workplace, the people in it and its hidden rules. 

For example, if you are applying to a new position, take some time before your interview to consult a trusted colleague within your strategic network about the hiring manager and their decision-making process. This way, you'll have a better idea of who to talk to and how to best leverage your value proposition.

In order to build an effective strategic network, you first have to define your career goals. After all, without these goals, how will you know who can help you achieve them?

These goals should be divided into short-term and long-term career goals. Knowing these goals, you can better analyze the position you want to achieve, as well as the people who might help you get there.

One way to keep track of who can help you, and where, is to list all your important professional relationships on a track sheet, evaluate their level of influence and the quality of your current relationship to them.

Imagine, for instance, that you currently occupy an entry-level position in your company's marketing department. Your goal, however, is to become a team leader in the sales department.

To reach this goal, start by identifying who the current team leader in the sales department reports to, who the decision makers in the hiring process are and which members of the sales team can support you in the hiring process. Connect with all of them, and you'll be on the right track!

### 6. The fastest way to the top is to connect with someone who uses their strategic network to your advantage. 

Imagine that you're looking for a new job. But, instead of making endless phone calls and writing your applications yourself, someone else tells you all about positions that aren't even listed yet, and then goes ahead and recommends you for them. Sounds like a dream, right? 

It's not a dream. In fact, if you find the right _sponsor_, this could be your reality.

A sponsor is someone from within your organization — an influential person who is higher up on the ladder than you currently are and who wants to see you succeed.

Sponsors are different from mentors in that they help you on multiple levels: not only do they offer you their sage advice, they also actively make things happen for you.

If your goal is to hold a top position in the marketing department, your mentor might advise you to build a relationship with the marketing manager. Your sponsor would do the same, but would then actually _introduce_ you to said marketing manager.

So how do you go about finding the right sponsor? That depends on you and your career goals, but one thing's for certain: the right sponsor might not be the one you like the most, but rather the one with the most power.

For instance, while you might admire a top-level manager for their inclusive leadership, their decisions are always undermined by those who push decisions through without worrying about inclusion. This inclusive manager might be likeable, but he has less power, and thus less value for you as a sponsor.

Instead, consider other contacts with a greater level of influence, and make dedicated time in your schedule to bond with them.

But why would anyone be interested in being your sponsor? Well, sponsorship is always mutually beneficial if it's done right. By helping you, your sponsor can expand her impact and reach across the organization.

### 7. Coaches are especially important for support and practical advice. 

We all need help and guidance sometimes. And just like a personal trainer raises your chances of reaching your health and fitness goals, a professional coach can help you reach your career aspirations.

As you learned previously, knowing and setting goals is critical for your career development. Your coach can not only help you to define these goals, but can also provide you with the strategic focus necessary to achieve them, along with the accountability necessary to keep you on the right track.

Imagine that your goal is to become a manager. As you now know, reaching that goal is dependent upon your ability to create or improve your strategic network.

After meeting with your coach and discussing your goal, you decide that the best approach is to spend an hour each week networking with important people in your organization. As time goes by, however, you become overwhelmed by your day-to-day work and stop networking.

Here, your coach steps in, holds you accountable for straying from your plan and encourages you to keep networking!

Finally, your coach can teach you important skills that will help you climb the career ladder.

For example, once you've landed a leadership position, you might realize that your leadership skills are lacking; you don't know how to trust your subordinates to complete tasks as well as you could. You know that if you don't let go, you'll be overwhelmed by tasks that your subordinates should be doing anyway and demoralize them in the process.

Luckily, your coach can teach you the leadership skills that will allow you to trust your team and let go of the desire to control everything.

### 8. Final summary 

The key message in this book:

**Women face more obstacles than men in the workplace, and overcoming these obstacles isn't as easy as just doing a better job. Rather, women need to be politically savvy by understanding workplace dynamics, and identifying the people who can help and support them in reaching their career goals.**

Actionable advice:

**Always ask yourself: How can I add extra value to my work?**

Your value isn't just your ability to complete tasks. It's also the knowledge and attitude you bring to the workplace. By taking the time to do things like sharing interesting articles on topics that are relevant and important for your company or department, you add extra value and people notice that you care and contribute.

**Suggested** **further** **reading:** ** _Lean In_** **by Sheryl Sandberg**

Through a combination of entertaining anecdotes, solid data and practical advice, _Lean In_ examines the prevalence of and reasons for gender inequality both at home and at work. It encourages women to _lean into_ their careers by seizing opportunities and aspiring to leadership positions, as well calling on both men and women to acknowledge and remedy the current gender inequalities.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bonnie Marcus

Bonnie Marcus has more than 20 years of management experience in executive positions in start-ups and Fortune 500 companies. She is the founder and president of Women's Success Coaching, which is dedicated to assisting women in advancing their professional careers.

Bonnie Marcus: The Politics of Promotion copyright 2015, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

