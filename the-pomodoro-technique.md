---
id: 537379cf6434360007680100
slug: the-pomodoro-technique-en
published_date: 2014-05-13T14:29:27.000+00:00
author: Francesco Cirillo
title: The Pomodoro Technique
subtitle: None
main_color: DA2C35
text_color: B2242B
---

# The Pomodoro Technique

_None_

**Francesco Cirillo**

_The Pomodoro Technique_ presents a simple yet effective method of structuring your workday. This method helps to overcome your lack of motivation by cutting large or complex tasks into small, manageable chunks. Using these techniques, you will gain more control over your work, thus making you a more effective worker and work more rewarding.

---
### 1. What‘s in it for me: Get things done without the mental stress. 

_Procrastination_ is a term that has become popular in recent decades. And it's no surprise either. Many of us can relate to the problem of procrastination, and know it intimately:

It's early evening, you're sitting in front of your computer screen, and you know those 50 empty PowerPoint slides won't fill themselves. And yet, instead of tackling this mammoth project, you spend hours perusing Facebook, looking at friends' vacation pictures.

You don't even enjoy it, and in fact feel quite guilty about it, and yet, you just can't stop yourself... Every 5 minutes is marked by opening your email inbox, hoping that you'll have a chance to delete some spam mail and give yourself a short reprieve from the guilt of procrastination.

Hours pass, and after 7 cappuccinos the presentation is still far from finished.

The author Francesco Cirillo faced this very problem at university. His salvation came in the shape of a tomato, more precisely: a tomato-shaped kitchen timer.

Since then, he's been using this simple tool to chop every task into smaller, manageable and motivating units called _pomodori_.

His technique has helped many chronic procrastinators rid themselves of their guilt and learn to enjoy their work instead. Everyone who has checked their email more than twice today should consider giving it a try.

**This is a Blinkist staff pick**

_"This is the one and only productivity system that always works for me. Whomever I tell about this technique immediately applies it to their lives. Its simplicity makes it genius."_

– Laura, German Editorial Lead at Blinkist

### 2. Chop your work down into pomodori in order to make it manageable. 

For many, when it comes to getting work done, time is our greatest enemy: it seems to stretch endlessly while you're trying to finish the first slide of your boring presentation, and then it flies away when you waste time surfing the web.

Before you've even managed to finish that first slide, it's time to go home. You look at your to-do list and see that _nothing_ got done; once again, your day was wasted sipping coffee, talking to your colleagues, and checking your email.

If this is what characterizes your workday, then you likely consider yourself inefficient and your work a burden. The guilt and frustration of not accomplishing your to-dos follows you home, and since the work must be done, you decide to work the "night shift" — even then, you spend most of your time surfing the net and checking Facebook.

After another long night of doing nothing, you go to bed frustrated and fatigued, only to wake up the next morning to repeat this vicious cycle. This is a routine that most of us know.

Thankfully, studies have shown that we can easily overcome these unhealthy work habits: all you have to do is divide your work into small, manageable tasks of 20 to 45 minutes a piece, and work within those short chunks without any breaks or interruptions.

This very concept has been used to create the _Pomodoro Technique_.

The idea is this:

  * Set a timer for 25 minutes (if you want to be true to the method, use a tomato-shaped timer), and spend that time focusing on a single task that you have chosen beforehand — no matter what. This 25-minute chunk is called a _pomodoro_.

  * Once the timer rings, take a 5-minute break to relax, drink a glass of water, or move around a little.

After your break, you'll be refreshed and ready for your next pomodoro!

### 3. Pomodori help you stay motivated so that you get stuff done and get rid of stress. 

Structuring your work into little pomodori is so easy and beneficial that there's no reason to not try it: you don't have to learn any sophisticated techniques nor buy expensive gear, and yet it can help anyone rid themselves of the guilt and anxiety of procrastination while making work rewarding and motivating.

Using the Pomodoro Technique helps you gain back control over your day through focused work.

Indeed, since a pomodoro is so small, it's nearly impossible not to maintain focus during that time. And knowing that you can stop working on your boring presentation after only 25 minutes, you have no reason to procrastinate.

This in turn makes it easier to stay motivated and get more out of your day.

You can't climb Mount Everest in one leap. Trying to accomplish _any_ large task at once is daunting and leads to procrastination: you spend a majority of your time doing nothing, and then, right before your deadline, you try and cram everything in at once.

However, setting a goal of 10 pomodori per day, i.e. merely 250 minutes, makes each step of the climb easier to manage, and will get you a lot further up the mountain than an all-out sprint at the last minute!

Pomodori also help you keep up your spirits, since each pomodoro is manageable by itself to start and you don't have to think about the _entire project_ that you need to finish. In addition, the steady pace of 25 minute intervals keeps you from getting worn out, thus saving you both energy and your sanity.

Using the Pomodoro Technique, you can go home with a feeling of accomplishment knowing that you actually got something done. Rather than working those night shifts on the sofa, you can use your free time to relax, spend time with your family, or do whatever else you want.

### 4. There is no such thing as a “half-pomodoro”: pomodori are always 25 minutes long. 

As simple as the Pomodoro Technique is, it only works if you adhere to the rules. The first and most important of these rules: There are _only_ complete pomodori — no halves, no 80% complete, and especially none that you finish with a minute left before the timer rings.

Once you've started your timer, you're committed to 25 minutes of focused work on that presentation. Do _not_ stop under _any_ circumstances before those 25 minutes are over. That means no giving in to the temptation of grabbing a quick snack, nor checking your email, no matter how briefly.

The only excuse is if you or your house is on fire — otherwise, stick to the rules and keep chugging along for the full 25 minutes.

In the event that you open your inbox to peek at your emails in a moment of weakness, immediately cancel your pomodoro and start over. Incomplete pomodori do not count!

By now you might be asking: Why do I need to be so strict with myself?

Essentially, you want to internalize these habits so that eventually you work pomodoro-style without even thinking about it. Strict adherence to the rules is the only way to discipline yourself to make that change happen.

But then what do you do if you finish a task in only 20 minutes instead of 25?

Simple: you keep going. Ask yourself if there is anything that you could review or perfect in your presentation. Are you sure you used the best font?

If you "finish" early, don't end the pomodoro, don't start your break, and don't even _think_ about checking your email before the 25 minutes are over! If there is truly nothing that you can do to improve your work and there are still 2 minutes left on your timer, then review your work anyway and recap what you've done to engrain it in your memory.

### 5. You must take a break after your pomodori; breaks are not optional! 

Once you've finished your pomodoro, saved all of your new work and crossed off the task from your to-do list, you must observe the second — yet equally important — rule: breaks are mandatory.

You should take this very seriously; if you want to maintain a focused mind, then you need to give it a break. This means that you shouldn't waste your 5-minute break by checking your email or doing anything strenuous.

Instead, relax and distract yourself. Push the task you've just completed out of your mind and enjoy that pleasant feeling of accomplishment. Allow your mind to recalibrate so it can focus on the next task that you need to tackle.

If you for some reason don't stick to this rule, then it will become harder to stay motivated.

In keeping with a schedule of 25 minutes of focus followed by 5 minutes rest, you show your mind that it needs to sustain its focus for short intervals, thus making it easier to stay motivated.

If you dilute that rhythm, then there's no reason for your mind not to procrastinate, because there is no longer a clear division between time to focus and time to relax.

In addition to the 5 minute breaks after each pomodoro, there's another very important kind of break: after you've finished 4 pomodori, treat yourself to a longer break of 15 to 30 minutes.

This method of structuring your time has been proven to work especially well both in terms of being effective as well as sustaining motivation throughout your workday, thus helping you accomplish more with less stress.

As you've seen, the rules of the technique are quite simple. Now let's have a look at the tools you need to utilize the Pomodoro Technique and see if they're equally simple and straightforward **…**

### 6. All you need to get started is a timer and two lists. 

Simple tools are often the best; luckily, the tools needed for the Pomodoro Technique are so simple, you'll find them in almost any household.

First, you need a timer. It doesn't have to be the tomato-shaped kitchen timer of the original Pomodoro Technique — anything from a stopwatch to your smartphone will do just fine.

Ideally, your timer will tick as it counts down and ring once the 25 minutes are over. These acoustic signals help your mind internalize the switch from a focused working state to relaxation and vice versa.

Next, you need 2 simple lists: your "To Do Today" list and your inventory:

_To Do Today_ includes everything you want to get done that day. It might look something like this:

  * outline presentation structure — 4 pomodori

  * find a location for seminar — 2 pomodori

  * pay bills — 1 pomodoro

  * etc.

But where do all these tasks come from? They're from your exhaustive _inventory_, where you've collected _all_ the tasks you want to get done at some point now or in the future.

One thing you will have to learn is how to estimate how many pomodori are needed for a given task. Although your estimates won't be spot-on in the beginning, you'll get better at it with time.

Soon, you'll be quite good at intuitively estimating how many 25 minute chunks you'll need to outline your presentation or sort through your inbox, or whatever else you need to do.

So how do you sustain enough motivation to get through your To Do Today list? You simply vary your tasks: If your presentation on "time management techniques" will take approximately 20 pomodori to complete, just chop it into smaller units.

Working an entire day on this huge, single task without ever finishing is much less rewarding (and therefore less motivating) than finishing the 4-pomodori task "outline presentation structure" after your first two hours of work.

### 7. Your greatest enemies: internal and external interruptions. 

By now, you've seen that time is not your adversary when you use it the right way, i.e., utilizing the Pomodoro Technique. However, there are hidden enemies lurking, waiting to disrupt your productivity if you don't actively protect your pomodori.

These are _interruptions_, i.e., anything that takes your focus away from the task at hand and shifts it to something else.

It might be hard to believe, but most people's workdays consist primarily of one interruption after another — they are everywhere, all the time, waiting to ruin your productivity.

Every phone call or email you receive, every time you give in to the temptation to check your team's results from last night's game, or every time you get a snack or order a pizza: all of these pull you from your focused working mode and interrupt your pomodori, and therefore make you less efficient.

Essentially, there are two main types of interruptions:

There are _internal_ interruptions that come from within, i.e., the stream of thoughts that pop into your mind all the time interrupting your train of thought and pulling you off track. These are things like:

  * a sudden idea for a project that you wanted to work on later,

  * the impulse to order pizza or to look at cat pictures on the net,

  * the spontaneous recollection that you need to buy your significant other a birthday present, etc.

Then, there are _external_ interruptions, i.e., outside forces that demand your immediate attention, such as:

  * the colleague who calls to complain about the weather,

  * incoming spam mail,

  * or the delivery guy who has brought you the pepperoni pizza you ordered during the previous pomodoro.

These interruptions are not only a nuisance, but they also contribute to reduced productivity and those terrible feelings of guilt and frustration that come with them. If only there were a way to get these interruptions under control...

### 8. You can manage any kind of interruption in order to sustain your focus. 

Thankfully, you don't have to be a slave to these interruptions. Just follow these simple steps to protect your pomodori:

Inevitably, something, some _internal_ interruption, will pop into your mind while you are trying to focus on your task and demand your immediate, undivided attention, like a child whining for some candy.

When this happens, simply jot it down somewhere and get back to your pomodoro. As long as you don't smell your pants burning, then it can wait until you're finished with the task at hand.

Once your pomodoro is complete, take a moment and examine the thought that you jotted down earlier; often, what seemed critically urgent while you were in your pomodoro ends up in the trash afterwards.

If it _still_ seems important, then put it in your inventory list or on your To Do Today list.

In the event of an _external_ interruption, try not to get roped into other people's business: take the steering wheel and make others respect your schedule:

  * Whoever is calling can just talk to your voicemail.

  * Turn off email notifications, and check emails only seldom (and _never_ during a pomodoro).

  * If someone comes by to say "hello," they're to be met with a friendly, but firm "goodbye."

  * You may only interrupt your pomodoro in case of _real_ emergencies, e.g., if someone cuts off a finger and is getting blood on your timer. If the same person just wants to borrow a pen, then they'll just have to wait until your timer rings.

If someone tries to interrupt you, then a response like "I'm in the middle of a pomodoro and will call you back," or "Just 10 minutes, then I can help you" are perfectly appropriate. You'd be surprised, but most people will understand and even respect your desire to stay focused...

Except maybe not the pizza delivery boy, which is why you shouldn't have ordered pizza in the first place!

### 9. Final summary 

The key message in this book:

**The Pomodoro Technique is as simple as it is effective: Chop your work into more manageable 25-minute chunks during which you work totally uninterrupted. Afterwards, take a break to relax and recharge your batteries, so you can start your next 25 minutes of focused work without getting tired.**

Actionable advice:

**Don't get caught in a web of distractions!**

If you are in the middle of focused work and your colleagues, who you like and respect, want to chat with you about the game last night, simply politely yet firmly tell them that you'll get back to them shortly. Personal relationships are important, but allowing these sorts of interruptions will simply cascade into another wasted day where nothing gets done!
---

### Francesco Cirillo

Francesco Cirillo developed his world-famous productivity technique in the 1980's while he was studying at university. Back then, he struggled with effectively structuring his workday, ultimately leading to the creation of the Pomodoro Technique. He now runs the website _pomodorotechnique.com_ and offers coaching and training related to his technique.

