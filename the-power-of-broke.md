---
id: 5944e034b238e10005eece08
slug: the-power-of-broke-en
published_date: 2017-06-19T00:00:00.000+00:00
author: Daymond John with Daniel Paisner
title: The Power of Broke
subtitle: How Empty Pockets, a Tight Budget, and a Hunger for Success Can Become Your Greatest Competitive Advantage
main_color: CA282E
text_color: CA282E
---

# The Power of Broke

_How Empty Pockets, a Tight Budget, and a Hunger for Success Can Become Your Greatest Competitive Advantage_

**Daymond John with Daniel Paisner**

_The Power of Broke_ (2016) reveals how starting a business with a limited budget doesn't have to be a disadvantage. With the right perspective, one can harness the Power of Broke to produce astounding creativity. With this fresh look at the business world, you'll stop seeing money — or a lack thereof — as a barrier to entrepreneurship, because the truth is, anyone with the right idea and a can-do attitude can start a business. So what are you waiting for?

---
### 1. What’s in it for me? Discover the power of being broke. 

We have all heard rags-to-riches stories of entrepreneurs who began with nothing but their bare hands. Take someone like Dr. Dre, who started as a DJ in Los Angeles's South Central district, moved on to become a successful producer and is now the owner of a headphone empire, as well as a high-ranking executive at Apple Music.

Stories like his can be inspiring, but are also easily dismissed as something that only a happy few can achieve — specifically, those with exceptional talent and timing. But there is also power to be found in being broke.

These blinks will offer insights into how being broke can help us achieve greatness, based on the experience of self-made men and women.

In these blinks, you'll find out

  * how a poor immigrant turned an ice cream truck into a restaurant empire;

  * why trying to reach new customer demographics can be a bad thing; and

  * which successful computer company was founded by a guy who couldn't operate a computer.

### 2. Being broke can lead to innovation and authenticity, two keys to success. 

Have you ever had to come up with something for dinner when you're stuck with only two or three ingredients? If so, you might remember that limitations like this can often result in some of the tastiest meals.

This is just one example of the truth in the old saying, "Necessity is the mother of invention."

Or, to put it another way, being short on resources can generate real creativity and inspiration that would otherwise never emerge.

Having a million dollars to launch a business doesn't mean it will be successful. On the contrary, already being rich can take away the passion and innovation that long-term success requires. 

But with an empty bank account, it's clear that your only chance for success is to dig deep and become innovative.

It's like being a basketball player who has one shot to win the game, but they're at half-court with only a second left on the clock; there's no other option but to go for it and take the spectacular shot.

It's also important to realize that innovation comes from a singular vision that starts from the bottom up, not the top down.

It doesn't matter if it's entertainment, fashion or technology, ideas grow organically from the ground up — they aren't inspired by executives dumping a bunch of money on a project.

And when an idea really takes off, it's usually because people respond enthusiastically to something real and authentic.

Take _Art Basel_, for example, one of the world's biggest art fairs, which takes place in Switzerland, Hong Kong and Miami Beach. While it has many established artists on display, many visitors get more excited about the unfunded street artists who are seen as being more genuine and "authentic."

This played a key part in coauthor Daymond John's success with _FUBU_, which stands for _For Us, By Us_. The clothing line was inspired by the way real people in the streets were dressing, not by designer studios.

You can also think of your brand as a personal relationship with your customer; like any good relationship, it has to be built on solid foundations. So be true to yourself — otherwise, the relationship is doomed to fail.

### 3. To succeed, be hungry and think like a shark. 

If you're familiar with the _Rocky_ movies, you know that Rocky's career falls apart when he lets his success get to his head and starts throwing money around. To get back on top, he realizes that he needs to rediscover his passion and hunger to win.

A similar principle applies to business, as being hungry can keep you focused on your growth and help you stay realistic about what's within reach.

Just look at Capital One bank's Small Business Confidence Score. This score is determined by a number of questions about a business's hiring plans, future outlook and the current economic situation.

What the scores show are that overall confidence is at its highest rate since the 2008 recession, and that's because small, hungry business owners know how to stick to business plans and use small steps to make great strides.

A great way to stay hungry and successful is to think like a shark by knowing your waters while keeping focused on your target.

On ABC's reality TV show _Shark Tank_, one entrepreneur after another pitches their product or idea to investors. And if there's one thing the show teaches, it's that investors want to hear that an entrepreneur has a clear understanding of both their market _and_ their goals.

In one case, the show's investors rejected an affordable athletic shoe company called Forus because the entrepreneurs had their target market down, but still wanted to expand in a way that took them well outside that market. With this in mind, an investment would have done them a disservice by encouraging them to focus on the wrong demographics.

This is why an entrepreneur who uses the trunk of their car to sell a cheap product might have a better chance with an investor. If they can sell 50 units in under five minutes, it proves that they have a perfect understanding of their market.

### 4. Being at a disadvantage can lead you to find resources others will miss. 

Some people are born with certain advantages while others start out at a disadvantage — but even if you have to work harder than others just to get on equal footing, don't let it get you down.

Being at a disadvantage and having few resources is actually a proven a recipe for success.

In fact, immigrants to the United States are twice as likely as US citizens to start their own business.

Rocky Aoki came to the United States from Japan in the 1960s, and in order to pay for his restaurant management classes, he rented an ice cream truck that he drove around New York City. He eventually saved up $10,000, which helped convince his father to invest in his restaurant, which went on to become the popular Japanese restaurant chain, Benihana.

Rocky wanted his son, electronic dance musician Steve Aoki, to find his own success without relying on Rocky's money. So, when Steve was 19 years old, he used the $400 he had to help start a music label by the name of _Dim Mak Records_ with his friends. Their office was his apartment, which was regularly packed with up to 13 interns.

Steve and his friends would self-produce a seven-inch single and sell it out of the back of their car after Steve's DJ gigs until they could afford to make another one.

While Steve did eventually max out ten different credit cards, today he's a hugely popular musician with a successful label and lifestyle brand.

As the Aokis show us, sometimes you have more resources at your disposal than you may think.

If you're a homeowner, you might have equity in your house that you can invest. But if you've taken out a loan, make sure you put money aside so you can make payments until your business starts earning money.

Extra money can also come from small things you don't need.

If your car is just sitting in a garage every day, maybe you can sell it, like Steve Jobs did with his car to help buy the components for the first Apple computer.

### 5. Being authentic will help you stay focused on your target audience. 

It may be that not everyone will notice or appreciate your authenticity, but it's important to stick to your guns nevertheless.

For instance, Acacia Brinley was recognized for her trendsetting Tumblr page, but her posts received a fair amount of negative feedback as well. Even so, the "selfie queen" of social media didn't let this stop her.

Remember that you don't need to please everyone; it's far more important that you remain true to yourself.

Being broke only means you can't spend money to make things look fancy. Instead, use the _Power of Broke_ and the fact that you're in a _can't-lose situation_ to let your personality shine through.

When Daymond John started FUBU, he knew he was going to be competing for shelf space with other clothing lines. But through his brand, he could show how much his community and clothing meant to him.

And it didn't take any extra money to do so. In fact, John used the Power of Broke, which forced him to spend very little money extremely wisely, to display his authenticity to the black community.

John was one of the first designers to give his clothes away to hip-hop artists, who in turn wore them in music videos and helped solidify FUBU's place in the fashion world.

John then focused on Black Entertainment Television (BET), a US cable network that focuses on the black community.

This way, John could reach his target audience easily, cheaply and directly. Since the Nielsen ratings company used very few black households to measure ratings when John started out, BET appeared to have a very low amount of viewers, so they charged very little for advertising.

This worked in FUBU's favor, as it allowed the brand to reach a wide audience for very little money.

This is the kind of opportunity that you should keep an eye out for when things are looking bleak — one that allows you to stay in your market _and_ stay authentic.

### 6. Don’t let funding or debt steer you away from your vision. 

Too many companies have gotten used to the idea of using "other people's money" to do business. And though the thought of funding might sound appealing, there _is_ such a thing as too much help.

When you consider looking for investors, make sure you don't risk losing control of your business.

Bringing an investor on board means giving up a percentage of your business, which could also mean cutting into your already small profits or growing more quickly than you can handle. 

The better kind of growth is one that is slow, profitable and under your control. Furthermore, bringing in outside capital also means having to rewrite your business plan, which can cause you to lose focus as well as control.

Instead, be like Gigi Butler and stick to your vision, even when funding is nowhere to be found.

Butler was determined to open a cupcake shop in Nashville, even when banks thought she was crazy and refused to give her a dime. Butler's prior employment was as a cleaning lady for Nashville's upper crust, so no one was willing to help fund her dream.

But after maxing out credit cards and emptying her bank account down to her last 30 dollars, Gigi's Cupcakes was finally open for business. And, sure enough, customers lined up out the door.

Not only was Butler able to pay off her credit card debt, she maintained total control over her business and her profits. Today, there are Gigi's Cupcakes locations in 24 states with $35 million in annual sales.

Sometimes debt is impossible to avoid, so don't let it get in the way of your vision.

When you start turning a profit, you might be tempted to put it back into your business, or simply to enjoy it. But the smartest thing to do is to pay off your debts so that interest rates don't eat away at your earnings and your accounts can start looking healthy and prosperous.

Far too many businesses have gone under because of debt that got out of hand, so don't be one of them.

### 7. The Power of Broke can be applied to corporations and entire industries. 

So far, we've seen how the Power of Broke can be applied to give hungry and focused entrepreneurs a leg up in the business world — but these same principles can be used by the major players as well.

After all, most big corporations started out as small businesses, and they should all continue to use the same strategies that got them to where they are today.

Yet, when companies get rich, they tend to start throwing money at problems rather than coming up with creative solutions.

Take marketing, for instance; you could easily throw away millions on ad campaigns and forget about using free or low-cost resources like social media.

One can only wonder why 38 percent of the current Fortune 500 companies don't have an active Twitter account.

Let's look at a smart campaign from years ago, before the era of social media:

General Mills, a company with an enormous marketing budget, wanted to reintroduce its struggling Nature Valley granola bars.

Even though they could have spent millions putting ads in every grocery store, they stayed tightly focused and targeted places with active young clients, such as ski resorts and outdoor gear shops. Since then, Nature Valley has gone on to be one of General Mills' top-selling brands.

The Power of Broke can also be applied to an entire industry.

At the start of 1970, cigarette advertising was everywhere in the United States, from radio and television ads to billboards and magazines, featuring beloved icons like the Marlboro Man and Joe Camel.

Later in the year, however, the US government announced its intention to tighten advertising restrictions for tobacco companies; meanwhile, Chinese cigarette companies were planning on making their first foray into the American market.

Rather than throwing money and lobbyists at this potential problem, the American tobacco industry saw an opportunity that wouldn't cost a penny: they accepted the government's advertising restrictions. The US brands knew they already had a strong hold on the market, and they also knew that without billboards and TV commercials, the new foreign brands wouldn't stand a chance.

### 8. Getting through the four stages of success requires patience. 

Coca-Cola has been around so long that you can be forgiven for thinking it's always been a success. But like any other global brand, it had to start somewhere.

It's possible to pinpoint four different phases that you need to get through patiently before your brand can go global.

The first stage is the _item_, which is your product in its most basic form: no label, no marketing, nothing but a product that satisfies a need. So you might have a coffee maker that doesn't even have a logo or name yet.

The second stage is the _label_, which is when you give your product a name that sets it apart and makes it memorable. This way, after customers see it for the first time, they'll know to ask for it by name the next time they're in the market.

The third stage is the _brand_, at which point you create a logo for better label recognition, as well as an identifiable style around your product. This stage accounts for the money spent on advertising and marketing, ensuring that people will seek out the product and will easily spot it on a crowded shelf.

The fourth and final stage is the _lifestyle_, which is when your brand has grown to the point that customers have come to expect a certain level of quality and experience. This is the stage at which a product can become a status symbol, as is the case with Nike, Apple and FUBU.

By leveraging the Power of Broke in each of these phases, companies have a greater chance of making it.

As your product moves through these stages, you might encounter some sort of crisis, and this is when you'll need to be patient.

As the _Harvard Business Review_ confirms, businesses that survive recessions do so by cutting costs and continuing to invest in growth, which you might do by, say, increasing funding to your research and development department. This way, you'll be prepared to launch when the inevitable upswing comes around.

The office supply chain Staples made it through the 2000 recession by shutting down underperforming stores, while increasing its overall workforce by ten percent. When the recession ended, Staples had actually become more profitable than it had been beforehand.

### 9. There’s no better time than now to embrace your limitations and start your business. 

These days, remarkable technological advances are a seemingly everyday occurrence. It's a great time for small businesses because it's easier than ever for them to get the funding they need.

For starters, the cost of technology is extremely low.

Every year, it gets cheaper and easier to maintain a website and store massive amounts of data online, which makes it less risky and costly to take potentially lucrative chances.

Other great tools for small businesses are crowdfunding sites like _Kickstarter_ and _Indiegogo_, which allow entrepreneurs to raise money for their ideas while maintaining strict control.

Honey Flow is just one example of a crowdfunding success. The company posted a five-minute video to Indiegogo about their dream to start an innovative beekeeping and honey-extracting business. They had hoped to raise $70,000 to make it to the label phase, but ended up raising an astounding $12 million, becoming the most successful Indiegogo campaign ever.

Today, it's easier than ever to turn creativity into success, even if you're not particularly skilled.

FUBU's Daymond John couldn't draw or sew anything more than a straight line, but it didn't keep him from starting a clothing line.

Even short basketball players who seemingly can't compete with their taller peers can end up having successful careers by using their speed and agility to their advantage, or perhaps by managing or coaching a team if they're passionate enough about the sport.

But one of the best paths to success is to come up with creative solutions for those who need help doing something.

Michael Dell wasn't a computer genius; on the contrary, he started Dell Computers with the goal of coming up with a computer that would be more user-friendly.

You don't need to be a rich genius to become a successful entrepreneur. With the Power of Broke, all you need is some creativity to find the right idea, and a willingness to embrace change and overcome the challenges along the way.

With these traits in place, there's nothing to lose — and no limit to your possibilities.

### 10. Final summary 

The key message in this book:

**It doesn't take a lot of money to launch a business. In fact, not having money forces budding entrepreneurs to rise to the challenge with creative and innovative solutions that otherwise may not have occurred to them. There will always be challenges in the business world, but you can use the Power of Broke to stay prepared for anything that might come your way.**

Actionable advice:

**Follow Daymond John's Shark Points for Success:**

  * Define your goal so you know where you're headed.

  * Do your homework: analytics are key, as is knowing the competition.

  * Be passionate about what you do.

  * You are the brand, so be aware of how you carry yourself.

  * Keep moving forward, keep scheming and always be ready to challenge your competitors.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Everything Store_** **by Brad Stone**

Despite being a billion-dollar company today, Amazon was built on humble beginnings in Jeff Bezos' garage. From the get-go, Bezos was driven by the grand vision of creating an _Everything Store_ — which has, in the meantime, virtually come true. Focusing equally on the company and its founder, this book shows how he turned his dream into a reality.
---

### Daymond John with Daniel Paisner

Daymond John is an entrepreneur, investor and one of the panelists on the ABC television show, _Shark Tank_. He is also the founder of FUBU, a successful hip-hop-inspired clothing line.

Daniel Paisner is a prolific collaborator in the publishing world, having cowritten books with celebrities, politicians and athletes. He is also the author of several novels, including _Obit_.

