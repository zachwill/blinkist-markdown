---
id: 5784a6d6d2eac100039f0ee7
slug: the-power-of-ideals-en
published_date: 2016-07-15T00:00:00.000+00:00
author: William Damon and Anne Colby
title: The Power of Ideals
subtitle: The Real Story of Moral Choice
main_color: C7E6E9
text_color: 203A57
---

# The Power of Ideals

_The Real Story of Moral Choice_

**William Damon and Anne Colby**

_The Power of Ideals_ (2016) shines a light on the questionable science behind the new thinking in psychology that holds people are inherently immoral beings. These blinks offer a deeper, clearer look into the nature of human morality and show how our actions are shaped by innate human traits of empathy, humility and honesty, as exemplified by the achievements of society's moral leaders.

---
### 1. What’s in it for me? Discover the human moral sense and see how science fails to acknowledge it. 

In recent years, modern psychology and the social sciences have put forward a new framework of study, calling it the "new science of morality."

This new thinking supposes that human moral traits are similar to physical traits, such as height or hair color, in that we have no conscious control over them. So if we're "good," it's not because we have any deep moral sense to behave well, but that such behavior is just an automatic response to stimuli.

But is this supposition true? In these blinks, you'll discover exactly what's wrong about the "new science of morality." You'll find that morality is an innate human quality and explore how people actively make moral choices and develop a moral sense.

To support these claims, the authors offer examples from the lives of morally exceptional people, like Nelson Mandela and Eleanor Roosevelt. You'll see how their achievements relied on innate moral virtues such as truthfulness, humility and faith.

In these blinks, you'll also learn

  * how modern psychological research relies on mind games;

  * how people can act morally without being aware of it; and

  * why some people believe that self-deception is helpful.

### 2. The “new science of morality” claims that people are immoral. 

The debate over human morality is as old as society itself, and even today it is still hotly contested — just think of the many contemporary religious debates that concern moral choice.

But in recent years, a new thinking has emerged, the so-called _new science of morality,_ with a rather pessimistic viewpoint on human nature.

In 2007, psychologist Jonathan Haidt coined the term, "new science of morality," to describe a renewed understanding of moral behavior, in which humans are seen as intrinsically evil and dishonest.

All fine and good, but where's the scientific proof?

Proponents of this new "science" point to studies such as psychologist Philip Zimbardo's 1971 prison experiment at Stanford University. As part of the experiment, Zimbardo asked students to act either as prisoners or as guards in a make-believe prison.

Once the experiment was underway, the students who were playing guards became so cruel toward prisoners and corrupt in their actions that Zimbardo had to prematurely call off the experiment. His conclusion from his observations was that humans are inherently evil.

Such views are also supported by thought experiments such as the "trolley car problem." This fictional scenario involves a trolley car that's about to run over five people. You're standing nearby and must decide between throwing an innocent bystander under the trolley to make it stop, or doing nothing.

In other words, do you choose to kill one person to save five lives, or choose indirectly to kill five people. It's easy to see how a researcher could paint a participant as immoral, no matter which option is chosen.

These thought experiments are cited to show that when a person makes a moral choice, it's often by accident. In the trolley car problem, you'd have to make a split decision, as you've no time to weigh the pros and cons of action.

But some psychologists hold that when given ample time to decide, a person is more than likely to make an immoral, selfish choice. Or are they?

> _"Academic games about imaginary situations are not sensible proxies for genuine moral commitment and real-life moral choices."_

### 3. A belief that human selfishness is innate leads people to forget the power of moral commitment. 

Do you consider yourself selfish? According to the new science of morality, all humans are.

In an exploration of human decision making, researchers generally examine only one type of motive, and that's self-interest. Why this narrow focus?

Well, self-interest is the motive for action we see expressed most often. From fear of being harmed to greed when economic times are tough — even the actions of an angry mob when people who "aren't like them" enter a community — self-interest is a prominent feature of human behavior.

But the human desire for self-preservation doesn't necessarily make people immoral. In fact, there is an equally powerful force that drives moral behavior: _commitment._

When parents make sacrifices to support their children; when workers stay honest despite the temptations of graft; when civil rights leaders fight for the greater good, these are all examples of individuals who are dedicated to a cause.

And being dedicated to a cause is exactly what increases moral behavior. This is what we call _moral commitment._

Moral commitment is precisely what academic thought experiments _lack_ in any investigation of human morality. Participants in studies are aware that their actions don't have real consequences. It's unlikely then that they'll behave in a way that would accurately reflect the actions they would take when making real-life moral choices. Because of this, it's nearly impossible to get an accurate picture of human morality through such research methods.

So how can we get a clearer picture of morality in action? We'll find the answers in history. In the following blinks, we'll investigate the moral achievements of figures such as Eleanor Roosevelt and Nelson Mandela. In this fashion, we can better grasp the fundamentals of human morality.

### 4. Moral behavior has helped humans as a species survive; empathy is a trait that begins in childhood. 

It's easy to believe that humans are barbaric, based on media reports of war or violence. But when we take a look at human evolutionary psychology, we get quite a different picture.

Evolutionary science has illustrated just how crucial a role morality plays in human development. Moral behavior, while not necessarily beneficial on the level of the individual, does give a species a greater chance of surviving.

By forming groups such as families or tribes, people create bonds of moral commitment. These bonds encourage us to protect one another (and our genes) amid life-threatening situations.

Empathy, for example, is a human trait that is displayed from a young age, illustrating how deeply ingrained morality is in the human mind.

Despite this, proponents of the new science of morality argue that ostensibly, empathic behavior is just a result of cultural conditioning. They say that a Hindu Brahmin, for instance, would act morally and choose to not eat meat, not because of any intrinsic empathy for the animal, but because of his immersion in a culture in which killing cows is forbidden.

The reality is that humans do develop a stronger moral understanding through experience. But our capacity to turn experiences into a foundation for moral behavior is something that is planted within us.

### 5. As your life progresses, your moral disposition can grow and change as dramatically as you do. 

A person's cultural background of course has a significant influence on his or her moral development. But it's also important to consider that a person's sense of morality can change; moral dispositions aren't set in stone. Rather, they are shaped by your experiences over time.

Behavioral economists have illustrated this change with a game that shows how age is bound to a person's understanding of fairness. In the "ultimatum game," two players are given a sum of money to divide between themselves. The first player makes an offer to the second player, who may accept or reject the offer. If the second player rejects the offer, then neither player receives any money.

How this game plays out changes dramatically with the ages of the participants. While kindergarten-aged children might accept an offer in which they receive less money, older participants are increasingly reluctant to do so. They'd prefer that neither player gets any money, rather than receive an unfair share themselves.

What creates this shift? Our moral compass changes not only based on our experiences but also on how we process, remember and grow following such experiences.

While we are in many ways the products of our environments, we aren't passive beings. By interpreting and selecting certain experiences, we play an active role in constructing a moral disposition that just might go against social norms. This is reflected in how a leader from a particular social or religious background might choose to take another path, to fight for morals she believes in.

Social activist and Nobel Prize-winner Jane Addams is a great example of this sort of individual. In the early twentieth century, Addams played an instrumental role in social reforms that radically transformed society in the United States.

Though Addams grew up in an affluent, privileged family, she showed genuine concern for those marginalized or ostracized within society. She departed from the environment she grew up in and its inherent moral biases, to push for a new conception of morality to help poorer families live a better life.

> _"People also can change their minds about their cultural beliefs: Judgments are not frozen in place."_

### 6. We’re born with moral emotions, which we can mold and manage as we fight for a particular cause. 

Humans are not passive when it comes to morality. We are able to construct, change and even grow our own moral codes. But what about deeply ingrained forms of morality, such as empathy? Interestingly enough, humans are able to shape and develop such moral emotions, too.

To put it simply, moral emotions are feelings and responses that are built into all people as a result of evolution. No matter if you were born in Europe, Asia, Africa or the United States, all humans largely share the same feeling of horror when witnessing someone get hurt. A baby will start crying when it hears another baby crying, regardless of cultural background!

Despite the immediacy of such reactions, humans aren't slaves to moral emotions, as these can be reshaped and developed, too. As we grow older, we don't usually cry when we see a stranger cry, as we've learned to separate our feelings from the distress or pain of others.

Many parents know that being too protective won't allow children to grow. Parents thus work to put these evolutionary responses to the side, so that children can learn that actions have consequences.

People can also learn to manage moral emotions in the pursuit of higher goals. Nelson Mandela is famous for his pacifist approach in mending the damage of post-apartheid society in South Africa.

Yet as a young man, Mandela was known for his radical revolutionary opinions and even advocated that at times, force was necessary. But he achieved his long-term goals by channeling his anger toward creating a movement for peace, illustrating the value of reshaping emotional responses when fighting for higher moral ideals.

### 7. Though we question the usefulness of honesty in the modern world, self-deception is far worse. 

Do you believe that honesty is always the best policy? Most of us are happy to tell a "white lie" if it makes life easier for us or for our peers.

Though being truthful is an ancient virtue, how relevant is it to life in the modern world?

Ancient Romans viewed the goddess of truth, Veritas, as the "mother of virtue." Ancient Chinese philosophers believed honesty was the basis of fair human relationships. In the Old Testament, the ninth commandment states, "Thou shalt not bear false witness against thy neighbor."

But today, we sing a different tune. In 2009, _US News_ published an article entitled, "We're All Lying Liars," which claimed that people who lied to themselves were, in general, happier people.

Is this true? If you've ever given a speech, you might have experienced the benefits of tricking yourself into feeling confident to conquer your nerves. But outside such a simple context, deceiving yourself or others can have significant consequences. For one, deception blocks out self-criticism, a trait that you need to learn and grow.

This situation supports what psychologists call the "Dunning-Kruger effect," which says that people who overestimate their abilities tend to be those who are the least competent. Conversely, people with a greater awareness of their limitations are more likely to learn from mistakes, improve and ultimately outperform their peers.

Moral leaders work to avoid self-deception. Nelson Mandela spent 27 years in prison and viewed his time behind bars as a chance to engage in honest self-reflection.

While most of us would cope with those long decades of isolation by telling ourselves stories to justify who we are, Mandela chose to review his life and actions honestly — even though he had no guarantee that he would ever be free again.

### 8. Humility is a multi-faceted character trait that allows us to connect with and mobilize others. 

Humility is often at the heart of spiritual teaching, practices that range across eras and continents. Yet even though humility forms the core of much of religious thought, its virtues are still debated.

How is this the case? Surely when people exhibit humility, this can only be a good thing?

What troubles many philosophers is that humility is paradoxical. While a person may be humble, as soon as he describes himself as such, does he then lose his humility?

Another question is whether humility is a virtue at all. German philosopher Friedrich Nietzsche and Dutch philosopher Baruch Spinoza pointed out that humility requires self-deprecation and passivity to some extent, both of which are hardly admirable character traits.

While some philosophers don't view humility as a virtue, psychologists hold that there's far more to humility than we think. Psychologist June Tangney demonstrated that humility is the result of a combination of six crucial character traits:

An honest evaluation of our own abilities; the capacity to recognize mistakes we make; an openness to ideas, even if they contradict our beliefs; the ability to see achievements from a big-picture perspective; an avoidance of being self-centered; and finally, an awareness of the ways we can make contributions of value are the necessary conditions for humility.

Humility, then, is multifaceted and cannot be reduced to qualities that at first may seem negative. The complexity and power of humility is reflected in the central role this trait has played in the achievements of moral leaders.

For instance, former US First Lady Eleanor Roosevelt was renowned for her rejection of any special treatment offered to her by virtue of her office. Her modesty in manners and dress belied her wealthy background. Her general humility allowed people to see her as an authentic individual, positioning her as an ideal leader in the fight for civil rights.

### 9. Maintaining true faith – an open, hopeful mind – allows moral people to persevere in times of despair. 

When you hear the word "faith," it might inspire thoughts of organized religion. Often too, depending on your background, these thoughts might be accompanied by feelings of skepticism.

In reality, the idea of faith doesn't necessarily refer to a belief in a deity or higher power. Faith means simply to believe in _something_.

You might not be aware of it, but faith is essential to living a modern life, providing people with a source of meaning and direction.

Psychologist Robert Emmons studied people's life goals, such as getting fitter and working harder. He found, perhaps unsurprisingly, that people who combined these aims with _spiritual goals_ were more likely to feel that their lives were purposeful and fulfilling.

We find examples of this phenomenon too in the lives of moral leaders, who often turn to faith or spirituality during times of personal despair.

Dag Hammarskjöld was awarded the Nobel Peace Prize posthumously in 1961 for his work as UN Secretary General. During his tenure, he explained how Christianity, Hinduism and Buddhism all contributed to his personal and spiritual growth, a process that allowed him to make the correct decisions in tense diplomatic situations.

His faith, he said, gave him the patience and resilience he needed to remain calm and balanced during negotiations.

By defining faith as open-mindedness and hopefulness, we can avoid feelings of intolerance and worse, fundamentalist thought, while striving to live with purpose. Morality and faith are the best tools people have to create a better world. We should learn as much as we can from them.

> _"Noted psychiatrist and author George Vaillant recently wrote that the absence of faith is nihilism, not atheism."_

### 10. Final summary 

The key message in this book:

**Despite current trends in the study of psychology and philosophy that reject a person's capacity to live morally, people throughout history have proven themselves able to nurture and fight for the ideals of generosity, empathy and equality in society, whether as a loving parent or a civil rights leader.**

Actionable advice:

**Distance yourself from your ego and be humble.**

Humility is an essential trait for a moral leader, but you don't need to be a Nelson Mandela to be humble. Even "ordinary" people have much to gain by distancing themselves from their ego. Being humble can have a positive impact on health, well-being and personal growth, and humility is essential in building strong relationships with other people.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Just Babies_** **by Paul Bloom**

_Just_ _Babies_ is about the development of morality in humans. It explores the emotions that help us to be moral, the influence family and kinship have on our moral judgements, and how society as a whole fosters morality. The book also shows what happens when we lack the emotions crucial to acting morally.
---

### William Damon and Anne Colby

William Damon is a professor of education at Stanford University and a senior fellow at the Hoover Institution on War, Revolution and Peace. He is also the author of _Greater Expectations: Overcoming the Culture of Indulgence in Our Homes and Schools_ and over a dozen other books.

Anne Colby is a consulting professor at Stanford University.

