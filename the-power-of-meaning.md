---
id: 5a3bb4b7b238e10006e1ac09
slug: the-power-of-meaning-en
published_date: 2017-12-25T00:00:00.000+00:00
author: Emily Esfahani Smith
title: The Power Of Meaning
subtitle: Crafting a Life That Matters
main_color: EDA589
text_color: 2383AD
---

# The Power Of Meaning

_Crafting a Life That Matters_

**Emily Esfahani Smith**

_The Power of Meaning (2017)_ discusses the four pillars of meaning that a person should honor if they hope to lead a fulfilling life. This book encourages readers to discover themselves by searching for a purpose in life, connecting with others, engaging in transcendence and learning from past traumas.

---
### 1. What’s in it for me? Reclaim a sense of meaning in your life. 

It's easy to go through life with a general feeling of "blah." Repetitive work and not enough social contact can make anyone's daily humdrum feel shallow and devoid of meaning.

These blinks make the case that this general malady of meaninglessness comes from our increasingly individualistic lifestyles. We tend to isolate ourselves in our work and look inward rather than outward to try and find meaning.

These blinks will show you where to invest your energy and put your focus in order to live a meaningful life.

In these blinks, you'll learn

  * what the four pillars of meaning are, and why they're important;

  * how to extract positive meaning from negative life experiences; and

  * what the Future Project is, and how it's inspiring current generations to follow their dreams.

### 2. Focus on the four pillars of meaning. 

Take a minute to think about the current state of your life. Would you say that you live comfortably and have enough money in your pocket? Even if this is the case, you're still not guaranteed to live a meaningful life.

Worryingly, suicide rates are actually increasing in wealthy countries. A study conducted by the psychologists Shigehiro Oishi and Ed Diener in 2014 found that even though people from countries such as the United States and Sweden were generally happier than those in poorer countries such as Togo and Sierra Leone, the suicide rate was significantly higher in the wealthier countries. 

As for the reasoning behind this, the study discovered that although modern life has its material and psychological benefits, the constant focus on the individual can sap life of true meaning. In fact, when it comes to the concept of "meaning," the study found that nearly a quarter of Americans couldn't say what makes their lives meaningful.

This is especially concerning because, according to psychologists like Roy Baumeister, who has specifically researched what makes a good life, having a meaningful life is far more fulfilling than having a happy one.

In order to start your journey toward living a more meaningful life, you should maintain these _four pillars_ of meaning: belonging, purpose, storytelling and transcendence. These categories constantly reemerge whenever people describe what makes their lives meaningful.

Once, Mahatma Gandhi explained that living a meaningful life involved having the _purpose_ of serving others, while the filmmaker Carl Laemmle believed that meaning comes from the _belonging_ he felt when bonding with his children.

Philosophers and psychologists also frequently refer to these four pillars. From Aristotle to Baumeister, it has been argued that meaning arises from belonging, having a purpose related to contributing to something larger, making sense of the world and your experiences and connecting with something greater than yourself.

By keeping these four pillars in mind, meaning can be discovered in both fresh and unexpected places. In the following blink, we'll start with the first pillar: you'll find out how a sense of belonging can lead to a more meaningful life.

### 3. Individualism contradicts our need to belong. 

The last time you went out for dinner, were people busy texting and Instagramming on their phones? If the answer is yes, that's no surprise — people are so preoccupied with technology nowadays that they've forgotten the importance of connecting with those around them.

Fundamentally, it's a human need to feel a sense of belonging either in relation to another person or a community. However, in modern society, many people live very isolated lives.

Back in 1945, the psychoanalyst René Spitz found that mortality rates in orphanages were unusually high because, ironically, children were often deprived of human contact in order to prevent the spread of germs and diseases.

Through his research into the matter, Spitz was the first to identify that a lack of belonging can result in death. Modern-day researchers have discovered the scientific reasoning behind this: chronic loneliness can compromise the immune system, which can lead to premature death.

Although having a sense of belonging is clearly essential to human life, social isolation and individualism are both on the rise. Increasingly, people tend to spend less time with their loved ones and more time in front of their phones and computer screens.

To support this claim, take these findings from the General Social Survey. Back in 1985, a number of Americans were asked to recall the number of people with whom they'd had deep conversations in the previous six months, to which the majority answered three. In 2004, that answer dropped down to zero.

This rise in individualism and isolation seems to be a major contributing factor as to why many people feel that they have a lack of meaning in their lives.

That's why it's so important to honor belonging, the first pillar of meaning. Surveys often conclude that people consider close relationships to be critical sources of meaning, and research often shows that those who are lonely consider their lives less meaningful.

By focusing on your relationships with others, you can begin to make your life more meaningful. This doesn't only apply to intimate or profound relationships. Even the relationship that's formed when you smile at a stranger on the street is vital to fostering a wider sense of belonging in society.

So, to find meaning in your life, try to reach out and connect with others around you. In the next blink, you'll learn about the second pillar of meaning: purpose.

> _"If we want to find meaning in our own lives, we have to begin by reaching out."_

### 4. Purpose is found through self-reflection and helping others. 

Have you ever considered what your purpose in life is? Before you panic about the gravity of such a question, you should realize that purpose as a concept doesn't need to be so intense.

When you think about your purpose, simply consider it to be the strengths and opportunities that you possess, with which you can help others.

The developmental psychologist William Damon believes that your purpose should be a far-reaching goal that involves some kind of contribution to the wider world. Don't worry — this doesn't mean that to have a purpose you have to attend every activist event in your city. As long as you have a goal and a make a meaningful contribution, you could live purposefully as, say, a zookeeper or a parent.

Zookeepers, for instance, feel that they have a duty to fulfill by helping animals live better lives; therefore, they have a purpose and consider their lives to be meaningful. As this example shows, an individual with a sense of purpose isn't concerned with personal benefits, but cares more about how to benefit others.

So, by making your work about helping others, you'll discover a purpose.

A survey involving 2 million participants found that those who considered their jobs to be meaningful were involved in careers such as English teachers, radiation therapists, school administrators and other roles that involved serving others.

If your job doesn't directly involve serving others, you don't have to drastically change your career. Instead, focus on the ways in which your work affects other people. A road worker whose job it is to direct the flow of traffic is keeping other people safe, for instance.

In other words, switching your focus to how your work affects others can give your job more purpose, and thus make your life more meaningful.

Up next is the third pillar, storytelling.

### 5. Through storytelling, we create coherence and fresh interpretations. 

You may think of yourself as a more reserved person who doesn't really enjoy being the center of attention in conversations. But guess what: You're still a natural storyteller. In fact, everyone's a storyteller — each person crafts their own life story.

As such, storytelling is the third pillar of meaning: it refers to the way people create stories from their own different life experiences. Humans feel compelled to tell stories, as this is how people make sense of the world. Individuals create meaning from telling their life stories to other people in a certain way.

Psychologist Dan McAdam has been studying the concept of life stories and meaning for over 30 years. He believes that a person's _narrative identity_, or the story that they create about themselves, is constructed by focusing on the most significant events that have taken place in their life and interpreting them in numerous ways.

From his research, McAdam found that those who tell _redemptive stories_ about their lives, or stories that transition from bad to good, often tend to live more meaningful lives.

In order to generate meaning through storytelling, it's best to reflect on how an important event has shaped who you are and the course of your life. The process of doing so is what academics refer to as _counterfactual thinking._ This is when you engage in "what-if" questions such as, "what if I hadn't gone to college?"

Research has shown that counterfactual thinking can make people appreciate the benefits of the path they have taken, as they're forced to think about how their lives would've panned out had that pivotal event not happened.

> _"Stories help us make sense of the world and our place in it, and understand why things happen the way they do."_

### 6. Transcendence can help dissolve the barriers between yourself and the world around you. 

Have you ever looked up at the stars at night and realized that you are just a tiny part of a large whole? Well, then you've experienced transcendence.

This is the fourth pillar of meaning, and it's all about experiencing a higher reality in which everything is interconnected.

Psychologist William James describes the mystical experience of transcendence as being uncontrollable, lasting no more than a few hours and not entirely possible to put into words. But transcendence has the power to reveal truths that will remain with you.

Another psychologist, David Yaden, believes that during transcendent states, a person feels connected to everything that surrounds them. It is in this moment that a person loses any sense of anxiety, feels complete peace and optimal well-being and derives meaning in life.

When you're in a transcendent state of mind, the barriers between yourself and the wider world around you dissolve. Oddly, you experience a paradox in which you feel connected to a higher power but also feel extremely insignificant at the same time.

Some meditators have said that when they've reached a transcendent state of mind, they've felt the boundaries of their own being dissolve. Suddenly, they felt at one with their environment. This self-loss, as it were, is sometimes called _ego-death_ and it's a means of mentally preparing an individual for the final loss of self, which is death.

For most people, the thought of death is a terrifying prospect. But for the individuals who have already experienced ego-death, death now seems like a new beginning.

Buddhists tend to illustrate this outlook on death with the example of a cloud. If you think about the life cycle of a cloud, it doesn't perish when it disappears from the sky. The cloud simply changes shape into rain, which transforms into grass, and grass into cows and milk, and then the milk into the ice cream we eat.

Transcendence serves the purpose of making people feel that everything is interconnected and that they will always exist in the universe in one form or another. It is a state of mind that gives meaning to life.

### 7. Deriving meaning from trauma depends on how an unfortunate experience is interpreted. 

To paraphrase the philosopher Friedrich Nietzsche, what doesn't kill you makes you stronger. The truth of this statement can be experienced when you or someone you know goes through adversity and manages to come out the other side as a better version of themselves.

By drawing upon the pillars of meaning, individuals can grow after experiencing trauma. This process is called post-traumatic growth, and can happen in five distinct ways.

Richard Tedeschi and Lawrence Calhoun, experts in post-traumatic growth, have identified the different ways in which you can grow after a trauma. The first is that your relationships can strengthen; second, you can go on to discover new purposes or paths in life; third, you can discover a newfound inner strength; fourth, you can become more spiritual; and last but not least, you can feel a renewed appreciation for life.

But why does this period of development happen for some sufferers of trauma, but not for others? Well, it apparently has nothing to do with the nature and severity of the trauma, but rather the way the trauma is interpreted.

The social psychologist James Pennebaker noticed that people who had experienced trauma in their childhood and kept it a secret had more health problems than the ones who spoke to others about it. As part of his practice, Pennebaker encouraged his subjects to spend 15 minutes each day, for several days in a row, delving into their deepest emotions and writing about the most upsetting experience they've had. 

The subjects who completed this act of writing down their thoughts and feelings about the trauma found that they didn't need to go to the doctor as much, suffered from fewer symptoms of anxiety and depression and had a stronger immune system, among other benefits.

When recounting their experiences, the subjects used "insight" words in their stories such as "I know," "because," "work through" and "understand." This demonstrates that they were making sense of their traumatic experiences through their writing. The exercise allowed them to take meaning from their experiences, and facilitate their own post-traumatic growth.

So if you ever suffer through any adversity, it's best to try and make sense of your experience, such as by reflecting on the event through expressive writing, and thus derive meaning from your trauma.

### 8. There’s been a societal shift towards exploring cultures of meaning. 

In modern society, the "work-and-spend" mentality is still very much in full swing. People tend to spend less time on fostering social connections and looking introspectively, and more time on constantly commuting and working late into the night. But in some sections of society, this setup is changing.

Political scientists and economists have identified a new trend: people are becoming increasingly interested in "spiritual" rather than "material" concerns, and are prioritizing purpose, knowledge and community over money and consumer goods.

This shift is due in part to several institutions and cultures that work to build connections, celebrate purpose, provide spaces for storytelling and value transcendence.

Take the example of the Future Project, an organization with a mission supported by the pillar of purpose. The project aims to help students pursue their purpose by placing guidance counselors, called Dream Directors, at schools in some of the roughest neighborhoods in the United States. The Dream Directors help the individual students follow their dreams by encouraging them to think big and helping them create a step-by-step plan.

The students who have worked with the Future Project reported that they felt the positive effects of being involved in the initiative many years after. They felt that they were more engaged when it came to learning and had a stronger sense of purpose.

Similarly, the oral history project StoryCorps is supported by the two pillars of belonging and storytelling.The project gives ordinary people the opportunity to tell their life stories in front of an audience in the StoryBooth, an intimate space in which two people meet and honor one another through the act of listening. Their conversation is recorded and the recording is then given to the participants, while also being archived to give the stories an air of immortality.

The author listened to the story that a woman by the name of Mary Anna told in the StoryBooth about what motherhood and being adopted meant to her. Mary Anna was deeply moved by the experience of having been listened to by the author, a complete stranger. She noted that telling your story helps you gain a better understanding of yourself, while also offering support to others dealing with similar experiences.

By listening to other people's stories, you can take the first step toward cultivating meaning both within your own life and within society at large.

### 9. Final summary 

The key message in this book:

**You don't have to travel the world, end world hunger or quit your job to live a meaningful life. You can find fulfillment by guiding your everyday life in accordance with the four pillars of meaning: belonging, purpose, storytelling and transcendence.**

Actionable advice:

**Say hi to the janitor.**

Start getting to know the cleaner or janitor at your work. One organizational psychologist, Jane Dutton, has found that no matter how small or fleeting, establishing high-quality connections in your daily life can contribute to a greater sense of belonging and, therefore, give your life more meaning. Plus, you'll help a group of people who are used to feeling invisible feel more valued and respected.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Purpose Driven Life_** **by Rick Warren**

_The Purpose Driven Life_ (2002) shares the Christian answer to that age-old question: why am I here? From finding moments of worship in daily routines to seeking out a supportive community and letting the Holy Spirit guide you through tough situations, these blinks are an engaging guide to life as a Christian today.
---

### Emily Esfahani Smith

Emily Esfahani Smith is an editor at Stanford University's Hoover Institution, and writes about culture and psychology for the _New Criterion_. Her articles have also appeared in the _Wall Street Journal_, the _New York Times_ and _The Atlantic_.

