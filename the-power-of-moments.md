---
id: 5a3e87fbb238e10006e1ac74
slug: the-power-of-moments-en
published_date: 2018-01-01T00:00:00.000+00:00
author: Chip Heath and Dan Heath
title: The Power of Moments
subtitle: Why Certain Experiences Have Extraordinary Impact
main_color: 339CFE
text_color: 026ACC
---

# The Power of Moments

_Why Certain Experiences Have Extraordinary Impact_

**Chip Heath and Dan Heath**

_The Power of Moments_ (2017) is about moment-making. It explains how brief encounters can change us forever, especially when they're unexpected, and how you can create life-changing moments yourself. Through a wealth of useful examples, you'll see how people have harnessed the power of the moment to take control and change the lives of others for the better.

---
### 1. What’s in it for me? Learn how to leave a lasting impression. 

There are countless self-help books out there preaching the importance of living in the moment. It makes sense, since our lives are really made up of a long string of small moments. But, most of the time, we don't seize these fleeting moments. More often than not, we're like passive actors in our own lives, with these moments happening to us rather than being created by us.

However, once you realize how powerful a moment can be, you may start taking action and generating life-changing moments for yourself and others. The benefits of moments are myriad. They can help you achieve goals by providing motivation at work, for instance. The secret is to make these moments stand out — and that's what you'll learn how to do in these blinks.

You'll also discover

  * that a stuffed giraffe can create a lasting memory;

  * how a filthy hair can change the life of a village; and

  * why _Lord of the Flies_ makes school fun.

### 2. To make learning memorable and engage students, teachers need to elevate the classroom experience. 

If you think back to the most memorable moments of your childhood, what comes to mind? For many of us, our most powerful memories are the result of unexpected moments. And for the most part, they don't involve us sitting in a classroom, listening to a teacher.

This is a problem that every conscientious teacher faces: How to present information in a way that will be both meaningful and memorable?

It's certainly a problem that English teacher Susan Bedford and social studies teacher Greg Jouriles struggled with while working at Hillsdale High School in San Mateo, California.

Back in 1989, the duo felt stuck in the familiar and boring pattern of reading from a book, discussing it with students and then giving them a writing assignment. They knew that sports, prom and school plays were about the only things students were genuinely excited about. And they figured this was because these extracurricular activities provided them with lasting memories. So Bedford and Jouriles began to work together to find a way to make classroom learning something students would remember as well.

The two teachers found that all it took to create classroom experiences that were both memorable _and_ educational was a bit of creativity.

One of the best examples is how Bedford and Jouriles joined forces to make the teaching of William Golding's _Lord of the Flies_ an experience that students would never forget. After reading the book, students received a legal form accusing William Golding of libel due to the way he portrayed human nature in his book.

Students were then divided into prosecution and defense teams, and over the following months they researched and debated the case. It all led up to a public trial, with students playing the roles of lawyers and witnesses, in front of an audience made up of parents, teachers and school staff.

Not only did this teach students a great deal about the legal process; it also transformed the study of literature into an exciting event. In short, it elevated the classroom experience to the level of the unforgettable.

In the end, the whole process was such a success that the public trial was turned into an annual school event. Bedford and Jouriles successfully showed that memorable learning should be interactive and fun. Such an approach both helps students retain information and creates lasting bonds between everyone involved.

> _"There was intensity and excitement and engagement, and extra work that we never asked for."_ — Greg Jouriles

### 3. People and businesses can create memorable experiences by defying expectations. 

One day, a small familial crisis erupted when a little boy suddenly realized that he'd forgotten Joshie, his stuffed giraffe, at the Ritz-Carlton hotel where the boy's family had been staying.

The father quickly contacted the hotel staff and asked them to send the giraffe ASAP, and he also asked whether, in the meantime, they could send a photo of Joshie to show his son that the giraffe was safe.

Well, the Ritz-Carlton did more than send the boy a photograph of Joshie; they sent a binder full of photos — Joshie lounging on a chair next to the pool, Joshie riding a golf cart, Joshie getting a spa treatment and hanging out with the hotel parrot.

By going above and beyond expectations, the Ritz-Carlton created a memorable experience that the boy and his family won't soon forget. Psychologists Roger Schank and Robert Abelson describe this as _breaking the script_, and it's a useful way for businesses to create lasting bonds with customers.

For any given situation, there's a typical script of how events unfold.

For example, let's say a customer orders a cheeseburger at a restaurant, but when it's delivered, the burger is cold and unpleasant. The standard script would be for the waiter to apologize and for the customer to leave a smaller tip than usual.

But if the restaurant wanted to break the script, it could apologize, offer to pay for the customer's meal and give him a free dessert as well. This unexpected offer would create a memorable experience and, instead of losing a customer, the restaurant might gain a dedicated fan.

Companies have shown that there is a variety of ways to go beyond client expectations.

The bakery chain Pret A Manger uses surprise by allowing its employees to give away a certain amount of products to customers each week. So, if a customer comes in who looks like she's had a bad day, the staff behind the counter can decide to cheer her up by giving her a free cinnamon roll.

The unpredictability of this random act of kindness makes it a win-win situation: the customer is happy and, thus, the business increases its chances of seeing her again.

### 4. Memorable experiences can also be used to change unwanted behaviors. 

We've come a long way since the early days of indoor plumbing and public sewer systems. Yet there are still places in the world where sanitation is a daily, life-threatening concern. In 2016, there were still a billion people around the world defecating outdoors — a problem that's proved difficult to solve.

Back in 1999, the WaterAid organization was helping to install public toilets in Bangladesh, but getting people to use them was a challenge that required the use of a memorable experience.

After the toilets were installed, sanitation expert Kamal Kar returned to the villages and found that most people were still doing things the old-fashioned way. So Kar developed a new program to really drive home how dangerous it was not to use the public toilets.

The program, called Community-Led Total Sanitation, involved experts meeting with the villagers and using visualization tools to make the experience more impactful. For instance, they showed the villagers a map that made obvious what was happening: the village was being surrounded by feces.

As a final emphasis, to really give the villagers an understanding of the dangers they faced, the experts would use one of the villagers' hairs, cover it in feces and drop it into a glass of water. They then asked if anyone was willing to drink this water.

Naturally, there were no volunteers, and this is when the experts would point out that the flies in the village were creating a very similar scenario. Since the insects were constantly flying from human feces to the villagers' food, the villagers were eating contaminated food all the time.

With the help of the visual aids, the villagers finally understood the importance of using the public toilets and were motivated to change their behavior. The sanitation experts had given them a memorable demonstration that they couldn't ignore, and this is what made all the difference.

In the next blink, we'll look at another benefit of powerful moments — how they can lead to personal insights.

### 5. Following your dreams doesn’t always lead to success, but the memorable experiences will still carry personal rewards. 

Most people have a dream — something they'd love to do, if only they had the time, money or resources.

As the saying goes, there are no guarantees in life. If you follow your dreams, you may not find success, but you're bound to create meaningful moments that will, at the very least, provide insight.

Lea Chadwell discovered this when she wanted to turn her newfound baking skills into her own small business. She'd only been baking for the past year, and had a second job as a veterinary assistant, but after spending a couple weeks working at a bakery in Portland, Oregon, she realized that this was her true calling.

Chadwell's love for baking resulted in A Pound of Butter, a shop that specialized in handcrafted birthday cakes. It did well, but over the next 18 stressful months, Chadwell slowly but surely discovered that she was not cut out for being the owner and boss of a bakery. Though A Pound of Butter wasn't the lasting experience she'd hoped for, it was a powerful and meaningful experience nonetheless.

Chadwell had to give up this dream, but it wasn't a complete failure since the experience brought her a wealth of personal insight. During those 18 months, she discovered that she doesn't have the right kind of personality to be a boss. She now knows that her strengths lie in being a reliable employee, and this valuable insight helped guide the rest of her career.

So even if following your dream doesn't turn out to be a story of financial success, sometimes insight into personal strengths and weaknesses is its own reward. A Pound of Butter still provided Chadwell with memorable moments that she'll carry with her for the rest of her days.

> _"Action leads to insight more often than insight leads to action."_

### 6. Recognizing talent and hard work creates valuable moments of pride. 

One's teenage years can be a difficult stretch of time, especially if one has a tendency to be nervous or lacking in confidence.

And this general observation brings us to the next defining element of memorable moments: pride.

Being made to feel proud of your talents or achievements is another kind of powerful moment that can transform your life and stay with you forever.

Just such a moment helped Kira Sloop, who, in 1983, was an awkward 13-year-old with a bad haircut, crooked teeth and a lack of confidence that made her an easy target for bullies. One of the only things she had to look forward to in school was choir, but even that was in danger of being taken away from her.

That year the school hired a new choir teacher, and after listening to Kira sing, he told her that her voice sounded strange. After this embarrassing moment, she ceased singing and began mouthing the words during choir practice.

Later that year, however, things turned around entirely. While at summer camp, Kira, still hurting from the choir teacher's comment, continued to only mouth the words to the camp songs. But the camp's choir teacher noticed, pulled her aside after class and convinced her to sing. Sure enough, the teacher was awestruck and told Kira that she had an amazing voice — a true talent.

That summer Kira and the choir teacher harmonized for hours, and thanks to her renewed confidence she went on to sing lead in high school musicals, eventually taking the stage at Carnegie Hall. It was all thanks to that time at summer camp, when a teacher gave her a life-changing moment of pride that she'd never forget.

These moments can, and should, happen between managers and employees as well.

Since the 1940s, studies have consistently shown that regularly displaying appreciation for employees' work is the single best way to motivate them. Yet studies also show that this is something that is lacking at many workplaces. While 80 percent of bosses think they give frequent praise, 80 percent of employees believe that praise is only given _occasionally_.

Even if you're not a boss, you can create powerful moments by acknowledging the good work of others and providing them with some pride.

### 7. Memorable experiences can be used to stay motivated and reach goals. 

It can be downright annoying to have someone tell you to "turn that frown upside down," or to be informed that you should make lemonade when life gives you lemons. If you're in the middle of a painful breakup, this kind of unrelenting positivity can make you feel even worse.

Yet, as is the case with all clichés, those banal comments contain a kernel of truth, since it really is beneficial to use a bad memorable experience as a catalyst for good memorable experiences.

When 25-year-old Josh Clark went through a difficult breakup he began jogging as a way to take his mind off his troubles. But it not only lightened his burden; in fact, he liked his new hobby so much that he began to help other joggers stay motivated.

What Clark came up with was a motivational plan based on instilling pride in other joggers by giving them memorable moments of achievement.

First, he developed a five-kilometer program that encouraged people to increase their fitness level slowly but surely. People would first walk the five-kilometer route and gradually move up to running. Clark also kept a big goal in everyone's mind, an official five-kilometer race, with a big celebration planned when they all made it over the finish line.

This slow, step-by-step process is effective with any goal, and will ensure you stay motivated and proud of your progress.

Say you want to learn an instrument — the guitar, for instance. The first step might be to take one lesson per week, and practice for at least 15 minutes every day.

The second step could be to learn how to read sheet music so you can teach yourself a simple song; and the third step might be to learn that song by heart. The final step could then be its performance in front of friends, family or the audience at a local open mic.

By giving yourself a series of small goals leading up to a big one, you'll have regular opportunities to appreciate your own progress with memorable moments of pride.

### 8. Powerful moments can be used to engage employees and improve performance. 

If you run your own business, the last thing you want to do is create a bad experience that a customer will never forget.

Of course, poor customer experiences can range from simply annoying to downright tragic.

In 1998, Sonia Rhodes went to visit her father in a hospital as he received treatment for a gastric ulcer. Rhodes was shocked by the poor service her father was receiving. Not only was he stuck in a cramped and unpleasant room; the medical staff didn't even greet him when they entered the room or show any sign of concern for his comfort.

Rhodes was particularly upset with the poor treatment since she worked for the very company that organized the management of the hospital where her father was being treated. But this also put her in a position to do something about it, and so she decided to give the staff a memorable experience that would strengthen their connection to the patients.

Rhodes organized a weekend event for the entire staff of the hospital, consisting of a series of meetings, which enabled every employee to attend without the patients being neglected. During the meetings, Rhodes presented a new vision for the hospital — a vision of world-class patient care.

The weekend wasn't about scolding. It actually had more of a party atmosphere and it allowed Rhodes to connect with each and every staff member and make sure they understood the importance of their role in this new vision.

By the end, everyone began to see their role in a completely new light, and it was all so successful that it was made into an annual event.

Hospital staff were now so attentive to the patients that they routinely noticed when a patient hadn't received a gift and would make sure they received flowers or some baked treats.

In the last blink, we'll take a closer look at how you can connect and create powerful moments with others.

### 9. Entire schools can be improved by connecting with parents through powerful moments. 

In 2010, the Stanton Elementary School in Washington, DC, was ranked among the worst schools in America. But that year, Carlie Fisherow was hired as the new school principal, and she was determined to put the school on the path to recovery.

Fisherow began her repairs in the usual way, by refurbishing the rooms and replenishing supplies and hiring a new team of teachers. But she soon discovered that a more creative approach was called for.

Even though the new school year began with sparkling classrooms and fresh teachers, it didn't change the fundamental problem: students were still skipping classes, and even walking out when they were bored. Simply put, the students were out of control, with 28 percent in detention at any given time.

The staff had no idea what to do until, finally, Fisherow realized that she needed to connect with the students' parents. This way, she could create meaningful moments and transform the dynamic from a negative one into a positive one, with a healthy outlook on the future.

For years, the parents of students at Stanton Elementary School thought the school was doing a poor job of teaching their kids, while the school staff felt like the parents were doing a poor job of _raising_ their kids. Fisherow was determined to change these attitudes. And so she had teachers visit the homes of their students and talk about what the parents and children dreamed of for their future.

As it turned out, this is exactly what was needed. The visits changed the parents' attitude, which in turn changed the behavior of the students.

This is precisely the kind of unexpected, powerful and highly memorable experience that people can create when they put their mind to it. Whether you want to change the attitude in your workplace, give a student a boost in morale or improve your own outlook on life, all it takes is one special moment.

### 10. Final summary 

The key message in this book:

**When we look back on our lives, the incidents that changed us and left the biggest impact are moments — special, powerful and memorable moments. When we realize this and keep it in mind, we can start to harness this power and begin to plan moments rather than just wait for them to happen. By putting these moments to work, we can generate meaningful experiences for ourselves and for others.**

Actionable advice:

**Create a powerful moment in your personal relationships.**

This is probably not as difficult as you think. A powerful, unexpected moment can happen by writing a card that says how much you love and respect your partner. Sometimes we underestimate these gestures, but they can transform a standard moment into a highly memorable one for the person you love.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The In-Between_** **by Jeff Goins**

Most people spend their lives impatiently waiting for the next big thing to happen. But what if those moments between big events were actually hugely influential on our identities and lives? What if instead of focusing on those big events, we slow down and savor those in-between moments? _The In-Between_ (2013) comprises a collection of Jeff Goins' life-changing experiences that happened in the in-between, which he would like to share with readers.
---

### Chip Heath and Dan Heath

Chip Heath is a teacher at the Stanford Graduate School of Business. He is also a consultant who specializes in providing support and business strategies for start-ups, helping them to grow into strong organizations.

Dan Heath is a teacher at Duke University's CASE Center. He specializes in supporting entrepreneurs and NGO's who are committed to making positive changes in the world.

Dan and Chip are the authors of the best-selling books, _Switch: How to Change Things When Change Is Hard_ and _Decisive: How to Make Better Choices in Life and Work_.

