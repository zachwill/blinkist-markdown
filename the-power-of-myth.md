---
id: 55a3a0c53738610007440100
slug: the-power-of-myth-en
published_date: 2015-07-16T00:00:00.000+00:00
author: Joseph Campbell with Bill Moyers
title: The Power of Myth
subtitle: None
main_color: 0D2540
text_color: 0D2540
---

# The Power of Myth

_None_

**Joseph Campbell with Bill Moyers**

_The Power of Myth_ (1988) clarifies the origins, evolution and meaning of myths. By comparing stories from different cultures, myth-master Joseph Campbell demonstrates how myths give clarity to universal notions of life, love and death. As spirituality declines in Western culture, he explains that myths are more important than ever, as they help us understand the human experience.

---
### 1. What’s in it for me? Find out why myths still carry the power to unite cultures and make sense of life. 

Talking snakes, magic swords and children jumping fully formed from the thighs of fathers. How could such fantastic myths and stories tell us anything about our modern life?

In ancient times, in the absence of science, myths helped people make sense of the natural world. Yet the truth is, even with today's cutting-edge technology, we need myths more than ever.

Why? Myths still help us make sense of life's more slippery truths, like why we're here and what it means when we die. These blinks explain the impact mythology has had on culture and society.

But most importantly, you'll learn how myths have an uncanny way of showing us just how much we have in common with our fellow man, regardless of culture or background.

Reading these blinks, you'll find out

  * what King Arthur and Luke Skywalker have in common;

  * why eating an apple is one of the oldest acts of civil disobedience; and

  * how horses turned the myths of Native Americans upside down.

### 2. A myth is a story that provides a shared identity for a community and reflects its cultural origins. 

When you were a child, did you enjoy bedtime stories? Tales of the heroic deeds of Robin Hood, or the adventures of King Arthur and his Knights of the Round Table?

Such stories, or myths, are essential in establishing and preserving the identity of a community.

Many myths explain the beginning of life, or how a culture came to be. Through this, myths function as a tool in forming a group identity and in doing so, they also explain how one group is separate or different from other groups.

The concept of a "chosen people" is grounded in myth. Certain religious groups believe the truth of their community is the only truth, and that they have been chosen by God for a special purpose. In contrast, people who exist outside the group need to be converted or fought.

Religious myths foster a connection with other members of a group and create a common identity and shared destiny, while also acting as a boundary for anyone with differing beliefs.

So how do myths happen? Stories tend to mirror the geographical or cultural roots of the place where they originate.

The idea of a god, for instance, is a cultural construct. In religions with multiple gods, each god is responsible for some element in nature, such as the wind or sun, or even animals. The Incas of South America, for example, believed their founding ancestor was the "son" of the sun god, Inti.

And when circumstances in a culture change, so do its myths.

The mythology of Native American tribes in North America was centred around vegetation. The Cherokee's "first mother" and most important goddess was Selu, meaning "maize."

Yet after the Spanish brought horses to the continent, which helped the tribes more efficiently hunt buffalo, the buffalo became an important mythological figure as well.

Myths can adapt or vary over time, but their core message usually endures, allowing members of a group to continue to identify with them.

### 3. Certain myths cut across cultures and peoples. Yet we focus too often on the differences instead. 

Think about the religious myths you know. Which individual could walk on water? You might think this was Jesus. Yet Buddhism tells us that Buddha did the same thing, too!

Across cultures and religions, we share _archetypes_ of common ideas. An archetype is a universal principle or idea that transcends culture. Importantly, we all — regardless of cultural or religious background — _subconsciously_ understand what each archetype stands for.

This is because as humans we all share common fears, desires and hopes. The human psyche is basically the same, no matter where a person comes from.

You may know the Bible story of Adam and Eve, and how a snake coerced Eve into eating an apple that she wasn't supposed to. But you may not know that the Bassari people in Senegal have a legend that tells exactly the same story.

The act of eating "forbidden fruit" represents the original human assertion of will; and in performing this form of disobedience, the old ways of life are abandoned. It's also interesting to note that the serpent symbolizes the power of life in many cultures across the globe.

Yet still people tend to highlight superficial cultural differences instead of seeing and celebrating the similarities between myths! The problem is that so many cultures are convinced that only their beliefs are "true" that they are blind to what we all have in common.

This is particularly true among Judaism, Christianity and Islam. Each religion refers to the same God, but each uses a different name. This fact has inspired violent conflict throughout history.

One African story illuminates this conflict well. It tells of a god walking down a street, wearing a hat that is blue on one side and red on the other. Farmers observing the god from different sides of the street can't agree on the hat's "true" color.

In the end, they start to fight. Yet if they'd only realize that they were looking at the same god from different perspectives, they would realize they didn't need to fight!

### 4. Myths and rituals work as a framework and a guideline for members of a certain community. 

Myths have existed throughout history, in basically every society. But why exactly are myths so important to us as a people?

Myths provide a framework for life, helping us better understand or handle significant stages in our lives, such as birth, puberty and death.

Myths help us make the transition to one stage from another, as these transitions can be scary or confusing. They help ground us and guide us during these times.

Marriage, for example, is often a significant stage in a person's life. All cultures have any number of marriage myths; usually, marriage is seen as a reunion on earth of two spiritual halves.

You've heard people refer to a partner as their "other half" or "soul mate." These metaphors refer to a notion that a pair's soul was once whole, but that it was separated into two human bodies.

Through marriage, these two souls come together again. This comforting myth helps to guide us in this new stage with a life partner.

As myths can be abstract, we often turn to _ritual_, which provides concrete guidelines for people to follow toward achieving what a society expects of them.

This is true for cultures such as the Australian Aborigines, where boys drink men's blood as a rite of passage to becoming men. Symbolically, they leave behind their mother's milk and become hunters.

In modern societies, myths and rituals are no longer this obvious, but we still rely on them to help us make sense of and carry out societal roles.

Consider the ritual of enlisting in an army. Recruits arrive as young men and women, take an oath and wear a uniform. At this point, a recruit is no longer considered an individual but as a member of the armed forces. Personal desires and wishes are left behind, to serve a country faithfully.

### 5. Myths help us appreciate the adventure of life, while taking the sting out of our fear of dying. 

Does the fear of death sometimes keep you awake at night? The positive thing about myths and beliefs is that they can comfort our fears and help us grapple with our own mortality.

In many myths, life and death are seen as two sides of the same coin.

Consider the stories you often hear of people who are diagnosed with a terminal illness, yet who resolve to then live life to the fullest — for the time they have left. Only when faced with their own death, do they manage to embrace life.

In many ancient cultures, the connection between life and death was far more pronounced, and it often was a core component of local mythology.

In Indonesia, among some tribal communities, a young boy would have to kill a person before he was allowed to marry and father a child. The tribe believed that for a new generation to emerge, the old generation had to die.

One significant way myths help us handle dying is by depicting death as just another stage of life. In ancient cultures, no one was afraid of death as death was not the end of life, but a threshold through which your soul had to pass to reach another level of existence.

It is for this reason that ancient peoples, from South America to Egypt, would place gifts in the graves of the dead. They believed the dead would need these offerings for the "afterlife."

Myths are told to help us as a people understand dying not as something to fear but instead as a completely natural, fundamental part of life.

> _"The conquest of the fear of death is the recovery of life's joy."_

### 6. Myths help us better grasp some of life’s transcendent ideas, things like god or eternity. 

What does God look like? A bearded man, sitting on a cloud? A multi-limbed goddess? An elephant, a monkey, or the sun?

We usually prefer to think in terms of the physical to comprehend the abstract, such as the concept of god. What's more, we tend to seek out physical proof for the assumptions we make.

We also like to think in polar opposites, such as male or female, dead or alive, right or wrong, and try to make sense of these notions by comparing them to things that are familiar, such as what we experience with our own senses.

So if a child asks you what God is like, it often is just easier to say that God is like your favorite grandpa; someone who is wise, compassionate and familiar.

Time and space are also concepts we grasp for when trying to explain the abstract, such as identifying where the "soul" or "heaven" exists in the real world.

The trouble is, it's almost impossible to define transcendent concepts through our need for the physical or the concrete. Ideas about god or eternity are simply incomprehensible when using such a framework.

Eternity is essentially abstract, and runs counter to our experience of things having a beginning and an end. Even though by definition, eternity is the absence of time, we can only think of it in terms of real time.

This is where myths can help, by enabling us to get closer to comprehending transcendent concepts and truths. While supreme truths can't be easily explained, myths give us template for understanding these truths in a particular fashion.

For example, the Christian myth of "heaven" and the Buddhist state of "nirvana" don't exactly explain the concept of eternity, but they do offer a sense of it, through describing a state of complete harmony and unity, in which one is oblivious to worry and unconcerned about the past or future.

> _"God is a thought. God is a name. God is an idea."_

### 7. As myth and rituals disappear, society becomes disassociated, and seeks more dangerous substitutes. 

Today it's hard to hear the terms "mythology" or "spirituality" and not think about crystals, incense or other pseudo-spiritual silliness.

In our modern era, myths have been pushed to the cultural sidelines. There are a number of reasons why this is the case.

For starters, our schools are not the defenders of classical education that they once were. If students aren't studying Latin or Greek texts or even examining works like the Bible or Talmud or Koran, all culture's ancient myths and stories and what they can teach us about ourselves will be lost.

Moreover, society's focus is on the individual, rather than the group. Rituals once performed as a rite of passage or that taught us how to behave in society have since lost their significance. Instead, we now prefer to focus on our personal desires and individual career goals.

But really, as a society, it's clear that we are feeling the absence of myths and rituals — and we're trying to compensate for this loss.

Now that myths no longer help us understand the world, many people feel disassociated, and suffer from depression or loneliness.

Feeling a spiritual "hole" in their lives, many people reach for other ways to experience transcendence. Instead of meditation or prayer, however, people seek quick but risky ways of generating awe and wonder through drugs or alcohol.

In contrast, as rituals disappear, certain groups have instead created their own to prove the dedication of new members and separate themselves from other groups.

The hazing rituals of street gangs are just one example, in which a new recruit must carry out a certain dangerous or demeaning task to become a full-fledged member of a gang.

> _"What we're learning in school is not the wisdom of life."_

### 8. As our international connections proliferate, a global mythology will develop. 

If the ancient myths are dying in the face of modernization, will our common understandings, the archetypes that underpin our shared stories, fade away too? Hopefully not.

As globalization takes deeper root, cultures are converging. Worldwide we are consuming the same mass media, sharing food from the same outlets or wearing clothes sold by the same chains.

It makes sense then, that as cultures converge, so do their mythologies.

Through the ages, whenever different cultures merged or one culture was overtaken by another, a new mythology was born.

After the 13 original British colonies declared independence and formed a new nation called the United States, a new myth was born: the myth of "manifest destiny." Christian citizens of this new union firmly believed that they were serving their God by governing the North American continent and spreading their supposedly superior values.

So as the world becomes a global society, we'll probably see a global mythology emerge, too. In fact, this mythology may already be surfacing, through Hollywood.

Why Hollywood? One of the most important cultural players in globalization is the American film industry, as movies made in Hollywood are enjoyed all over the world.

Interestingly, the highest-grossing films, from _Star Wars_ to _The Matrix_, tell the classic hero story of departure, fulfillment and return, themes that are at the heart of many mythological and religious stories, from the life of Jesus or Buddha to the tales of King Arthur.

These references to ancient archetypes are one of the reasons why such films are so successful, as people can easily identify with the hero's trials and tribulations. These story arcs are so universal that everybody can relate to them, irrespective of origin, nationality or religion.

So myths won't vanish from our lives, but adapt instead to changing cultural circumstances. Eventually, a global mythology will develop, with themes everyone will relate to and benefit from.

### 9. Final summary 

The key message in this book:

**Throughout human history, myths have played a vital role in establishing a common identity for communities and helping individuals transition between different life stages. Given the importance myths used to hold for society historically, a lack of mythology and spirituality in the modern day can be problematic. Yet as globalization progresses, we'll see a new global mythology evolving too.**

Actionable advice:

**Follow your bliss!** **  

**

A significant reason behind modern depression lies in the pressure we as individuals feel to live our life according to the wisdom of others. But in contrast, we know deep inside what it is we should be doing and what actually makes us happy. The wisdom of ancient myths tells us that we should follow our own feelings, because if we do what brings us joy, everything else will fall into place. **  

**

**Suggested** **further** **reading:** ** _Rational Ritual_** **by Michael Suk-Young Chwe**

_Rational Ritual_ (2001) offers a profound, game theory-based analysis of the role that rituals, ceremonies and media events play in society. Throughout the ages, these rites have been used to create "common knowledge" that allows people to solve problems such as which ruler to obey and which products to buy. Essential reading for budding Robespierres or Steve Jobses alike.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Joseph Campbell with Bill Moyers

American mythologist, lecturer and author Joseph Campbell (1904-1987) was best known for his exhaustive research and insight into comparative mythology and comparative religion. His best-known works, _The Hero with a Thousand Faces_ and _The Masks of God_, valued by scholars as well as storytellers, influenced films such as _Star Wars._ _The Power of Myth_ was the companion book to a documentary that aired in the United States.

