---
id: 5563c3a06461640007f90000
slug: the-power-of-no-en
published_date: 2015-05-29T00:00:00.000+00:00
author: James Altucher and Claudia Azula Altucher
title: The Power of No
subtitle: Because One Little Word Can Bring Health, Happiness and Abundance
main_color: 1AA2B7
text_color: 137585
---

# The Power of No

_Because One Little Word Can Bring Health, Happiness and Abundance_

**James Altucher and Claudia Azula Altucher**

_The Power of No_ (2014) offers a holistic, intuitive approach towards health, abundance and happiness: Say no to the things that are causing you harm and slowing you down so that you have the energy to say yes to the things that you find uplifting.

---
### 1. What’s in it for me? Enhance your life with the power of saying “no.” 

Have you ever been in the situation where someone asks you for something, and you _know_ that you should decline, that you should just say no, but you can't? Maybe it was the last time a friend asked you to help paint the kitchen during your exams, or when you had to run errands for your sarcastic boss.

The truth is, we've all been there, and learning how to take hold of those moments and just say "no" is one of the greatest feelings imaginable!

But these blinks take it a step further. It's not just about saying no to a particular situation, it's also about saying no to scarcity, and to needless noise. It's about saying no to the things that harm you. In the end, by learning not just how to _say_ no but also the _power of no_, you'll find it's easy to say yes to yourself.

In these blinks, you'll discover

  * how to embrace more silence in your life;

  * why a bookstore can change your future; and

  * what a gratitude diet is.

### 2. Saying no to bad relationships leads to health and true love. 

We all know that there are some things that we should do more often than others, but in the end it all comes down to everyday choices. When faced with these choices, it's easy to get pulled into situations you would rather avoid or to ignore your well-being. But if you want to get the most out of life, you should choose life.

_Choosing life_ means not pursuing habits that quite literally cause you to die, like poor nutrition or smoking. By abstaining from harmful foods and smoking, you are less likely to die from heart disease or lung cancer, and will have chosen life.

Another way to choose life is by ridding yourself of negative and abusive people. Say no to the people who drain your positive energy or stir up feelings of guilt or fear. It frees up resources, like your time and energy, that can be directed towards your _inner circle_.

But how do you know who belongs in your inner circle? This exercise will help you figure it out:

List all the people you engage with at least five times per week. Then rate how these encounters make you feel on a scale from one to ten (ten being the best).

Start focusing on the people you've ranked higher than eight by spending more time with them. Spend less time with everyone else on your list, and distance yourself even farther from those ranked lower than five.

Taking distance from these kinds of people may be a challenge at first, and like any major change in behavior, starting to say no might require you to overcome destructive behavioral patterns. Take one of the authors, Claudia: She had always pursued unavailable men. In the most extreme case, she pursued someone for almost two years, convinced she was in love with him despite having seen him just four times!

Eventually, she realized that she was addicted to unattained love, and started saying no — first to this particular man and then to her addiction.

To say no to her addiction, she went to a support group, which helped her prepare for a real relationship based on love, honor and respect. She's now been happily married for over five years.

### 3. Saying no to other people’s expectations empowers you to follow your dreams. 

Have you ever wondered in the middle of a project: "Is this really me? Or am I just trying to please my boss by laughing at his stupid jokes?" Maybe you've had similar feelings about one of your relationships.

It's time to stop doing things you don't want to do by using your _assertive no_. If you ignore what you really want and keep doing things you don't want to do, you may begin disliking the person you are trying to please or disliking whatever it is you are doing for them.

Not only does the assertive no help you avoid the unpleasantness of doing tasks you don't care for, you can also prevent other people from feeling bad for making you do things you don't want to.

Think about the last time someone was helping you out when you knew they didn't really want to — maybe they were helping you paint your house or babysitting your children. Did you really appreciate their help? Or did you just feel guilty for being a burden?

You don't have to conform to other peoples' expectations. You are unique; choose your personal storyline, one that fits your unique wants and needs. So, if you don't want to go to university, don't. Follow your passions instead and forge your own path.

One way to figure out what that path should be is to go to a bookshop and discover which books you're interested in. If you find yourself continually drawn to coffee table design books, maybe you should look into something like industrial design or architecture.

When you listen to these personal inclinations and start exploring your interests, you'll simultaneously increase your mental muscle as well as your appeal to other people. Remember the last time someone spoke about something with an infectious passion? Enthusiasm is attractive! And when people like you more, they're more likely to trust both you and what you say, as well as enjoy your company.

### 4. Saying no to scarcity and noise allows for abundance and silence. 

More food, more money, more gossip — nowadays it seems like everyone wants more of everything. For most of human history, that desire was fueled by a real scarcity of resources. Today most resources aren't scarce, yet we still suffer from our historical _scarcity complex_.

Saying no to scarcity means shifting your attention away from what's missing and towards the abundance around you.

One way to focus on abundance is to count the blessings in your life, or even something as mundane as the cars in a traffic jam. The important thing is simply to perceive and recognize the palpable lack of scarcity in our lives.

But why should you focus on abundance over scarcity? By focusing on abundance, you also enrich your life. This goes beyond the optimism of seeing the glass as half-full; rather, you realize that there is plenty of water, that you won't die of thirst and that you should be _thankful_ for the water.

You should also say no to noise, i.e., everything that impedes you from being at peace with yourself and the universe. The idea is to replace noise with silence.

Try to be aware of the noise in and around you, like negative thoughts that fill you with fear or regret, or troubling news reports. News of a plane crash, for instance, can make you anxious about your flight the next day. To be more at ease, just switch the TV off and don't watch that two-hour documentary about the dangers of flying.

In addition to allowing for a more peaceful state of mind, silence also allows you to listen with an open heart. One way to practice this is to try giving your full attention to everyone around you for 24 hours, even the cashier in the supermarket or the waiter at dinner. Be with them in silence and make them feel like they have your attention.

The look of a lonely person feeling noticed for the first time in a day is one you won't forget!

> _"God, grant me the serenity to accept the things I cannot change, the courage to change the things I can, and the wisdom to know the difference." - Serenity Prayer_

### 5. Learning to say no allows you to say yes. 

In the previous blinks, we saw how something as simple as saying no to the right things could drastically improve your life. In a way, you could say the word no has its own power.

As we've seen, this _power of no_ is derived from three elements: _discernment_ (the ability to perceive the abundance in your life) _, compassion_ (the basis for listening with an open heart) and h _ealth_ (abstaining from the things that harm you).

But implementing these three elements can be difficult. Thankfully, expressing _gratitude_ can help sustain your commitment to the Power of No.

Start by listing all your problems — your cheating partner, your neighbor's loud parties or your stagnant job search — then list all the good things in your life, from breathing fresh air to having a roof over your head.

Then go on a _gratitude diet_ : Start each day by thinking about ten things you are thankful for. This will help you focus more on the good in your life and say no to the bad.

To get the most from the Power of No, you will need to involve all four of your "bodies": the physical, the emotional, the mental and the spiritual. In order to serve and care for all four bodies, try using the _alien technique_ :

Imagine yourself as an extraterrestrial special agent on a mission to save lives. But to reach your goal, you must possess the body of a human for one day.

On the first morning in your new human body, you become acutely aware of what the body needs: a little bit of physical or mental exercise, a green smoothie or perhaps a resolution to a problem that is causing undue stress.

With this knowledge, your mission becomes clear: "Go for a run," "Read a book," "Get the smoothie and meditate for a while" or "Solve the problem."

In the end, using the Power of No, you'll be better equipped to say yes to yourself. You will shine, be loved and be proud of your contributions to the world!

### 6. Final summary 

The key message in this book:

**We make countless decisions each day that have drastic effects on our lives. We must learn to say no to the things that damage us so that we can say yes to health, abundance and happiness.**

**Suggested** **further** **reading:** ** _Choose Yourself_** **by James Altucher**

Author James Altucher explains that after the 2008 global economic crisis, you can't wait to be chosen; you have to _Choose_ _Yourself._ This means you have to take full responsibility for your own success and happiness by reclaiming control of your aspirations and dreams. To do this, the book gives you both tools and effective practices to stay physically, mentally, emotionally and spiritually healthy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### James Altucher and Claudia Azula Altucher

James Altucher is an entrepreneur, chess master, spiritual teacher and writer who has written twelve books, including the best sellers _Choose Yourself_ and _I Was Blind but Now I See_.

Claudia Azula Altucher is an author as well as a teacher of yoga, meditation and spirituality. She is also the author of _21 Things Before Starting an Ashtanga Yoga Practice_.

