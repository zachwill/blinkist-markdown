---
id: 545021af3831390009000000
slug: the-power-of-noticing-en
published_date: 2014-10-31T00:00:00.000+00:00
author: Max H. Bazerman
title: The Power of Noticing
subtitle: What the Best Leaders See
main_color: F7F646
text_color: 666514
---

# The Power of Noticing

_What the Best Leaders See_

**Max H. Bazerman**

As we muddle through day to day, we often don't see things that are right in front of us. Sometimes we're not paying enough attention; sometimes we subconsciously "blind" ourselves to things. _The Power of Noticing_ is about learning to _notice_, a skill that is crucial in making important decisions as well as in being a great leader.

---
### 1. What’s in it for me? Learn how to notice the important details and be a better leader. 

What do the Challenger disaster, Hurricane Katrina and the British Petroleum oil spill have in common? These terrible disasters all resulted partly because key people failed to notice important details that could have prevented the tragedies in the first place.

We're unknowingly blind to all sorts of information, even when right before our eyes. It's crucial _not_ to miss the important details, especially if you want to be a successful leader.

In these blinks, you'll learn why it is so easy to overlook things, and to realize the damage that can be caused when we fail to notice key details. You'll learn how to hone your noticing skills and better understand why noticing is such a valuable skill to cultivate.

In the following blinks, you'll also discover:

  * why professional baseball coaches didn't notice players' flagrant use of steroids;

  * why the CEO of British Petroleum lost his job over just one sentence; and

  * why Sears inadvertently encouraged employees to scam its automotive customers.

### 2. Focusing too intently on one thing can make you blind to other details around you. 

Horses in busy cities often wear blinders — small shields positioned so that the horse can't see to the right or left, only straight ahead. This way, the horse can ignore the traffic and people, focusing only on what's in front of it on the road.

People too can wear virtual "blinders." We fail to notice the simple things around us as we focus intently on just one thing.

In the 1970s, psychologist Ulric Neisser illustrated this phenomenon in a video. A short clip, only nineteen seconds in length, shows people wearing either black or white shirts passing a basketball back and forth. As you watch the clip, you are supposed to count how many passes those wearing white shirts make.

Most people are able to focus and count the passes accurately. If you ask a viewer if anything strange happened in the video, however, most viewers would say no.

But something strange _does_ happen. As the players pass the ball, a woman dressed in black and holding an umbrella walks through the scene. Most viewers don't recall this as they are too focused on counting passes.

When we focus too much on one thing, like counting passes, we can become blind to other things that might be right in front of us. This is called _inattentional blindness_.

Yet it is possible to overcome inattentional blindness. We have to learn to _notice_.

The _power of noticing_ means being able to see that woman with the umbrella. It means being able to focus on something without going blind to everything else.

In the following blinks, you'll see how your ability to notice can prevent you from making careless mistakes and enable you to become a successful leader.

### 3. The Challenger disaster is a painful lesson in why paying attention to the right details matters. 

So you missed the woman with the umbrella in the video — a harmless omission. Yet missing important details in other situations can have much more serious consequences.

The 1986 Challenger disaster in the United States is one tragic example. Prior to the launch of the space shuttle, many NASA engineers expressed concerns, worrying that the temperature on the day of the launch might play a role in its chances of success.

Engineers ran some simulations and found that seven out of 24 of their tests were _unsuccessful_. This 7:24 chance of failure became NASA's main area of focus. Some engineers felt the risk was too high, while others were confident that the risk was minimal.

In the end, the launch went forward, despite lingering concerns. The Challenger exploded, resulting in the death of all seven crew members and one of the biggest catastrophes in aerospace history.

Even with the odds of success at 17:24, the mission still failed. Yet the disaster could have been prevented had engineers noticed a crucial detail that was right in front of them.

They focused so much on the seven simulations that failed that they didn't pay attention to the temperature conditions of those that _succeeded_.

If they had, they would've noticed a crucial detail: all of the successful simulations were carried out when the temperature was above 65 degrees Fahrenheit. On the day Challenger launched, it was much colder, at 40 degrees Fahrenheit.

If they had focused _less_ on failed tests and _more_ on the successful tests, engineers would have had a clearer picture. As in fact, there wasn't an approximately 30 percent chance of failure on the day Challenger launched — it was actually closer to 99 percent.

The blindness that led to the Challenger disaster was caused by paying too much attention to the wrong details. Unfortunately, this isn't the only sort of blindness from which we can suffer.

### 4. Baseball managers didn’t notice illegal doping among their players – because they were winning. 

What would you do if you saw your boss stealing office supplies from work? Would you confront or report her? Or perhaps do nothing at all?

More often than not, you might not do anything. And you're not alone.

We make ourselves blind to events or details when it is in our benefit to do so. Think about it: reporting on your boss is serious business. It's difficult to remain unbiased in a situation where your own interests may be involved.

This sort of situation is called _motivational blindness_.

Sports offers many examples of motivational blindness. Players will inevitably view an opposing team as more aggressive; you'll notice an opponent's fouls more often, for example, but view your own team's infractions as less severe.

As fans, It's almost impossible for us to watch a game objectively, as we're too motivated by our desire to see our team win.

Even worse, we can become blind to bad behavior when focusing on a larger goal. This was the case with steroid use in Major League Baseball in the United States, from 1998 to 2001.

At the time, one had to be truly blind to _not_ know players were dabbling in performance-enhancing drugs. From 1991 to 1994, the home-run average for the top sluggers was 44. In 1998, this average was beaten by a total of _ten_ professional players. In 1999, eight players beat the new record. In 2000, six players came out on top; in 2001, another nine broke the record, again.

This startling trend should've clearly indicated that something was amiss, but managers and coaches turned a blind eye. They were so focused on winning that they didn't care how their players were increasing their skills, even if doping was both illegal and dangerous.

A key step to becoming an effective leader is learning how to avoid motivational blindness. It's not just about self-interest; you can also be blind to risks you're taking.

> _"How many times can a man turn his head, and pretend that he just doesn't see?" — Bob Dylan_

### 5. Avoid motivational blindness by always asking questions and setting strict rules for yourself. 

As we've seen with baseball managers, if you're winning, it's easy to ignore the risks that may be in plain sight around you. Motivational blindness is powerful, but you can learn to curb it.

JPMorgan CEO Jamie Dimon learned a hard lesson in 2005 when he hired Ina Drew as the company's new chief investment officer. Drew increased company profits significantly, but did so by making enormously risky trades.

Dimon knew about the risks, but ignored them as profits were so high. By the time Dimon finally looked closely at Drew's strategy, it was too late. A series of risky trades ended up costing the company $6.2 billion in just _one month_.

How can we learn from this?

Dimon himself said the biggest lesson he learned was not to get too complacent when you've got a successful track record. In other words, don't become a victim of motivational blindness.

Never stop asking questions. How is your success coming about? Is it sustainable? What are the risks?

A good way to check motivational bias is to set rules for yourself, a practice that auditing firms follow.

Auditing firms have a high risk of motivational blindness as the more money they help companies save, the more they earn. It could thus be in their interest to turn a blind eye to false accounting or fraud. Moreover, if they keep clients happy, they get more work.

To stay on the straight and narrow, auditing firms implement some simple rules. To keep auditors and clients from getting too cozy, auditors aren't allowed to work on the same account all the time and equally, can't be empoyed by a company they just audited before a significant period of time has elapsed.

Rules like this can help us check ourselves and ensure we're noticing the things we should. All good leaders must constantly stay alert to avoid motivational blindness and the problems it can create.

> _"Leadership comes with responsibilities. A critical one is noticing the outlying evidence."_

### 6. Don’t let a salesperson misdirect you; stick to your guns and the rules you’ve set for yourself. 

What do magicians, politicians and salespeople have in common? They are the masters of misdirection.

While you might expect it of a politician or a sleight-of-hand magician, a salesperson isn't an obvious example of someone who deliberately wants to mislead. A salesperson sells items in a shop and often, we take what they say about a product at face value.

But we shouldn't. A salesperson's job is to persuade us to spend as much as possible, and in doing so they can steer us away from what we truly want or need.

To avoid being misdirected, approach a situation in logical fashion. Don't let yourself be seduced; keep your cool and maintain a calm mind. Keep these steps in mind:

First, remember your objectives. Say you need to buy a television. Before you get distracted by a salesperson's rhetoric or the deep discounts on coffee machines, remind yourself: you're there for a TV, and nothing else.

Second, identify your objective's most important criteria. Do you want a television with extra features, such as HD or WiFi? Or do you prefer a lower-priced, basic model to save cash? This will help you avoid being talked into extra features you might not want or need.

Finally, analyze your options. When the salesperson starts delivering his spiel, don't get distracted. If he shows you a computer you can watch TV on, does that fit into your criteria? No — it's not a TV. How about a television chock-full of high-tech gadgets? No, if one of your criteria was affordability.

If you stick to these steps, you can easily avoid being misdirected and thus avoid making decisions you might come to regret.

> _"As John F. Kennedy once said, don't answer the questions you were asked. Answer the questions you wish you were asked."_

### 7. Overconfidence can make justifying manipulation easy; but small mistakes turn massive quickly. 

When we look at examples of corporate meltdowns, such as with energy company Enron, we might assume that one big mistake caused the company to collapse.

Yet most of the time, massive failure is the result of many small mistakes, compounded over time. It's easy to fail to notice small changes, even when they happen right under our noses.

For example, if you put a frog into a pot of boiling water, the frog will immediately jump out to save itself. However, if you put the frog in cold water and gradually heat it to its boiling point, the frog will stay put until it is dead. When the temperature change is gradual, the frog doesn't know it's in danger.

Overconfidence can also blind us to mistakes, or encourage us to make more. Take, for example, a successful executive with a great track record. One day she realizes she's made an accounting error.

She now has two choices: report the error honestly, or make a small change to cover it up. Making a small change, in the executive's mind, is a minor thing and perhaps even justifiable, as she'll simply reverse it with earnings from the next period.

Yet if the executive falls short in the next accounting period, she might try to manipulate the figures again. This time the small change becomes a larger change. Soon enough, what started as a small mistake has snowballed into massive fraud.

Researchers Catherine Schrand and Sarah Zechman spent seven years examining companies that had been investigated for fraud. They found that in 75 percent of cases, the fraud began with optimistic or overconfident executives who manipulated figures when their optimism wasn't confirmed by real data.

Tyco International, HealthSouth and Enron were all companies that were part of the researchers' studies, and prove that turning a blind eye to small mistakes can build up to systemic failure.

### 8. Stay skeptical to see beyond the hype; consider what’s missing when you make a decision. 

It's easy to concentrate too much on what's happening or on what you are doing. Have you ever stopped to consider what's _not_ happening, or what you're _not_ doing?

If you start paying attention to things that _haven't_ happened, you'll be surprised at how much you can learn.

In an Arthur Conan Doyle story, the famous Sherlock Holmes investigates the alleged murder of John Straker and the theft of his horse. The police believed that the suspected murderer entered the stable without disturbing Straker's guard dog and stole the horse. The suspect then unexpectedly ran into Straker and beat him to death, before fleeing the scene.

Holmes eventually concludes that it was the horse that had actually killed Straker; there was no actual murderer. He figured this out by examining what _didn't_ happen.

Why didn't the guard dog bark? It could only be that the dog already knew the person who took the horse. A stranger couldn't have done it — only Straker could have.

Paying attention to details that are missing can also help you determine whether something that seems valuable is indeed so. When you hear about a deal that sounds too good to be true, don't give in to your first instinct and grab it — instead, pause and think about it.

What might be hiding behind the hype? Be wary and keep an eye out for the catch behind the "deal of a lifetime."

Buying a used car is a good exercise in being wary. if the price is very low, and the car seems to be in perfect condition, you need to ask yourself: Why is it on sale in the first place? Why so low a price? Behind its shiny facade, the car could rusting or falling apart.

Being skeptical can help you make better decisions. Always try to notice what's missing; don't focus so much on dogs that bark — notice those that don't.

### 9. Make better decisions by always thinking ahead, and avoid uncomfortable or nasty situations. 

You're on the way to the train station via taxi when the driver tells you that there's a train strike. He then offers to take you to your destination himself, for what he says is a good price. Do you take the taxi?

Before making any decision, think ahead to what the consequences might be. British Petroleum CEO Tony Hayward learned this lesson the hard way in 2010.

That year, Deepwater Horizon, a BP oil platform, suffered an explosion that killed 11 people and leaked oil into the ocean at an alarming rate for nearly three months. Huge numbers of people lost their jobs and entire communities were devastated — in short, lives were completely shattered by the disaster.

Without considering how his words would sound to the victims of the tragedy, Hayward made a grave error. When speaking of the spill, he stated, "No one else wants this thing [the spill] to end more than I do. I'd like my life back." The backlash was so great that Hayward had to leave his job.

Thinking ahead can also help you know when you should trust someone. In our taxi situation, we essentially have two choices: assume the driver is lying, or trust him and take the cab.

You do take a risk if you assume the driver is lying about the strike, as you might be stranded at the station if he's right. But the issue isn't choosing between cynicism or trust.

Instead of making a blind guess about the character of the driver, think about information that might help you get ahead. Try, then, to find out if the strike is real; call the station, or ask someone else.

This will guide you to make the right decision. Aim to understand other people's behavior without destroying any opportunities to build trust.

### 10. Any decision can have an indirect effect, so take the time to consider what those might be. 

Discounter Wal-Mart attracts customers through its very low prices. Yet few shoppers consider the indirect results of supporting an aggressive low-cost retailer like Wal-Mart.

Companies like Wal-Mart put so much pressure on their suppliers to deliver products that can be sold at hugely discounted prices that often, as just one example, safety concerns are disregarded.

In the 2000s, a gas can manufacturer called Blitz USA presented a new model to Wal-Mart that could prevent flames from flowing into the gas can. Blitz was facing a number of lawsuits for this exact problem, thus the product overhaul. Yet Wal-Mart rejected the new model on the grounds that the price was too high, so Blitz redesigned it with lower safety standards, so they could sell it through Wal-Mart.

So the next time you want to choose a cheaper product, ask: what corners were cut to make this product so inexpensively? Were standards compromised? Could anyone get hurt?

Good leaders especially need to identify the indirect harm certain decisions might cause. A decision by US department store chain Sears in the 1990s elucidates the need to consider indirect effects.

Sears management set a quota for auto repair employees to book $147 hourly in sales. Although it seemed like a straightforward, profitable idea, the quota essentially encouraged employees to sell customers unnecessary repairs and equipment just to fill their quota.

Sears should have more thoroughly considered the problem of motivation with such a quota system. Instead, management thought only of money and didn't consider how the policy might push employees to cheat the system.

### 11. Predictable surprises occur when we ignore vital warning signs that a disaster is imminent. 

Sometimes we fail to predict an event even when all the evidence points to it happening. Such events are called _predictable surprises_.

Predictable surprises happen when key people are conscious of a pending disaster and understand its risks yet still don't think action is warranted.

Hurricane Katrina is a classic case of a predictable surprise. In 2001, the _Houston Chronicle_ reported that the city of New Orleans was "sinking" and that its main hurricane buffer was eroding, leaving the city vulnerable to a large storm. in 2004, the US Federal Emergency Management Agency (FEMA) reported that a New Orleans hurricane was one of the top three potential catastrophes facing the country.

They even cited that the US government would not be prepared to deal with such an event.

In 2005, Hurricane Katrina hit New Orleans, a "surprise" that was utterly predictable.

Airport security in the United States is another example of a situation like Katrina that also resulted in disaster. Yet you can work to prevent predictable surprises by following these steps:

First, recognize the threat. In the 1990s, an increased level of security breaches should have triggered warnings that airports needed to implement more thorough security checks.

Second, prioritize the threat. In tandem with a rise in security breaches, the threat of armed groups to the United States during this time had also increased. Vice President Al Gore suggested creating a concrete proposal through a commission to increase security at airports to counter the threats.

Finally, mobilize action. Despite the vice president's best intentions, the US Federal Aviation Administration (FAA) opposed the changes suggested by the commission.

What's more, US airlines spent millions of dollars to prevent the government from addressing security issues, as they feared such measures would scare away customers.

On September 11, 2001, a group of hijackers ran two planes into the World Trade Center in New York City.

### 12. Being a first-class noticer is a key part of being a successful and great leader. 

So how do you start developing your ability to _notice_?

Strive to be a _first-class noticer_. First-class noticers are less prone to being blinded by what they want to see, and more open to what the data truly suggests.

They listen for the dogs that _don't_ bark and they avoid being misdirected. They prevent predictable surprises and are highly suspicious of things that sound too good to be true.

When they approach a situation where they need to make a decision, they view it as if they were an unbiased outsider. They don't let their own interests get in the way of making the right choice.

If you are a first-class noticer, you will also be a much stronger leader to those around you.

Start applying what you've learned to your daily life. Don't give up if at first you don't succeed. When a first-class noticer fails, they'll learn from their mistakes rather than quitting.

Figure out what you didn't notice and think about how to avoid making the same error in the future.

Always keep striving to improve yourself and keep learning from your experiences. Use your capacity to notice to help you become the successful leader you want to be.

Remember that the people you lead might not be as good at noticing as you are, so it's important you use your skills to help others.

We're all taught that _focus_ is extremely important, and indeed it is, but when making critical decisions, _noticing_ is better.

### 13. Final summary 

The key message in this book:

**People place a lot of emphasis on the importance of focus, but what we really need is to learn to** ** _notice_** **. If you train yourself to be a first-class noticer, you'll be much better equipped for making important decisions, especially those that might affect others. Noticing is a necessary skill for anyone who wants to be a strong leader.**

Actionable advice:

**Don't let your hunger for success blind you.**

Having high expectations for yourself is important, but don't let your goals make you fail to notice important things around you. Stick to what the data really says, not what you want it to say. Remember that most large-scale corporate disasters, like Enron, started and continued because leaders were too self-confident and lied to themselves about what was really happening at the company.

**Suggested** **further** **reading:** ** _Just_** **_Listen_** **by Mark** **Goulston**

_Just_ _Listen_ combines time-tested persuasion and listening techniques with new methods to help you get your message across to anybody. By learning how to be a better listener, how the brain works and how people think, you'll be able to motivate people to do what you want because you'll better understand their needs.
---

### Max H. Bazerman

Max Bazerman is a professor of Business Administration at the Harvard Business School. He is the author of several acclaimed books on decision making and negotiation, including _Negotiation Genius_ and _Blind Spots_.

