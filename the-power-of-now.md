---
id: 530c61aa34623100082d0000
slug: the-power-of-now-en
published_date: 2014-02-25T08:00:00.000+00:00
author: Eckhart Tolle
title: The Power of Now
subtitle: A Guide To Spiritual Enlightenment
main_color: F7F646
text_color: 666514
---

# The Power of Now

_A Guide To Spiritual Enlightenment_

**Eckhart Tolle**

_The_ _Power_ _of_ _Now_ (1997) offers a specific method for putting an end to suffering and achieving inner peace, living fully in the present and separating yourself from your mind. The book also teaches you to detach yourself from your "ego" — a part of the mind that seeks control over your thinking and behavior. It argues that by doing so you can learn to accept the present, reduce the amount of pain you experience, improve your relationships and enjoy a better life in general.

---
### 1. What’s in it for me? 

Many of us spend our time either trapped in the regret-filled past or anxious about a future we cannot hope to control. Yet at the same time we also cling to the hope that we can somehow improve our lives, become happier and find enlightenment, even though we have no clear idea of how to do so.

Enter _The_ _Power_ _of_ _Now_, in which bestselling author and renowned spiritual guide Eckhart Tolle provides readers with several methods for navigating the complex terrain of their inner lives and their relationships to the past, present and future.

At the heart of Tolle's (very practical) philosophy is an emphasis on living in the present moment as a way to avoid most of the pain that we usually experience. To this end, the book focuses on the connection between the mind and suffering, offering a variety of perspectives on the numerous self-destructive ways that we use our minds — for example, to trap ourselves in cycles of suffering and stop ourselves from being happy.

_The_ _Power_ _of_ _Now_ has helped millions of people to improve their lives, including their relationships with others, and, crucially, to increase their sense of self-esteem and fully engage with their lives.

### 2. By focusing only on the present – and ignoring the past or future – you can greatly improve your life. 

Many of us want to find inner peace and improve our lives. We're seeking — in a word — enlightenment, yet we don't know which steps to take to find it.

Well, the first step may be easier than you imagine.

We tend to live in the past and in the future. One moment we're reminiscing or regretting, the next we're planning or worrying. Meanwhile, we neglect the one moment that is fully available to us: the present.

Now.

Only the present is important because nothing ever occurs in the past or future; things happen only in a continuous stream of present moments.

Whenever you feel something, that feeling is experienced in the present, because your senses can only give you information about this specific moment. So when we say that something happened in the past, that's not quite true: it actually happened in a single, present moment.

Indeed, what we call "the past" is really a collection of once-present moments that have passed. Likewise, "the future" is made up of present moments that have yet to arrive.

As this suggests, there are no advantages to worrying about the future or dwelling in the past, but there are many to living "in the now." If you manage to achieve that, you'll experience no major problems, just small ones that can be dealt with as they arise.

For example, a challenging task, like writing a scientific paper, often seems too large and complicated to be achievable. If you're anxious about the work that's remaining, or regret missed opportunities to work on it in the past, you'll get nowhere. But if you just solve one little problem after another — gathering the data, developing a structure, writing the first chapter — you'll accomplish it more easily.

So try to live in the present! Stop clinging to the past and stop fearing the future, and you'll see how dramatically your life will improve.

### 3. A part of you needs pain to survive, and it creates most of the pain you experience. 

Let's say you manage to live in the present and to not worry about the past or future. What happens when you then experience pain? If pain is felt in the present moment, how can you deal with both physical and emotional suffering?

Pain is nothing more than a self-created inner resistance to external things that you cannot change. You experience pain when you are unsatisfied with how things are, yet don't feel powerful enough to change them. This manifests itself, on an emotional level, as a negative feeling.

Because you think so much about the past and the future, but can live only in the present, you have no means to change many things that you're unhappy about. So you develop an inner resistance to the way things are — which you experience as pain.

Another aspect of self-created pain is the "pain-body," a part of the self that needs you to feel pain so it can survive.

Since the pain-body is composed of your painful experiences, it grows and strengthens whenever you experience pain. Therefore it will try to make you miserable and sad.

This cycle can continue for a very long time until, finally, the pain becomes an essential part of you: you'll have fully identified with your pain-body. Because pain will by then be such an important part of your life, you'll be afraid to let it go because doing so would then put your very identity at risk.

For example, when something annoys or frustrates you, and you feel yourself getting angry, your pain-body has taken control. The anger clouds your ability to think and act rationally, and simply leads to more pain.

Even when it seems that all pain comes from the external world, it is in fact, for the most part, self-created: it comes from within. The good news is that because it's self-created, you can do something about it — as you'll find out in the next blinks.

### 4. The “ego” is a part of your mind that stops you from being happy. 

Have you ever wondered why some people seem to sabotage themselves? Why, though nobody wants to be miserable, so many people are unhappy?

The culprit is the _ego_ — a part of your mind that controls your thoughts and behavior without you noticing.

Since it cannot easily be observed, most people don't know the extent to which the ego controls their life. For example, if you later reflect on a dispute you had with someone, you may notice (and perhaps regret) that you overreacted. Yet, in the thick of the dispute, you simply weren't aware of anything influencing or controlling your thinking and behavior.

Why does the ego do this? It depends on your misery for its continued existence, so it obstructs happiness at every turn, acting against your own best interests.

The existence of a destructive part of your mind that produces misery would explain why so many people suffer, despite nobody actively wanting to live an unhappy life. For example, some people intentionally sabotage their own happiness by deciding to stay in deeply destructive and painful relationships.

The ego leads you into situations of conflict with others, and makes you unhappy with your current situation so that it can gain control over your behavior and thinking.

For example, whenever two or more egos come together, drama ensues — as can be seen in small offices or households. While people may want to live together peacefully, their egos make them annoyed at trivialities and cause them to overreact. If you find yourself suddenly in an fiery debate over a minor issue, like whose turn it is to clean the kitchen or whether a television program is good or not, this is probably the work of the ego.

The ego is a destructive part of the human mind. It wants to be the most important part of you and knows no limits, so if you let it gain control, it will bring you much suffering.

### 5. If you want a richer and almost painless life, separate yourself from your mind and focus on your body. 

The ego's power is just one of many reasons that it's important to separate yourself from your mind and pay closer attention to your body. Indeed, many great teachers have spoken about the importance of focusing on the body rather than the mind.

Why?

The mind is responsible for pain.

It produces pain by continually bringing up memories of the past, or by planning for the future, occupying your entire life with regretful memories and anxiety-ridden future scenarios. In doing so, it prevents you from living in the present.

The result is that since you can't alter the past or the future, you constantly worry about things you can't possibly change. And that leads to pain.

Clearly, we need to find a way to diminish the mind's power and reduce some of its control.

How?

By shifting our focus from the mind to the body.

Your body knows what's best for you. By listening to your body, you can get a very clear sense of what is important in your life. Jesus spoke often about the importance of the body, and used it in many proverbs and allegories; for example, "Your body is a temple." The stories of his resurrection and ascension to heaven always stress that his body was missing from the grave, and that he ascended to heaven with his body, not just his mind or soul.

Nobody has ever found enlightenment while concentrating on the mind and ignoring the body.

A vivid illustration can be seen in the Buddha's six-year abstinence (including fasting), which he undertook in order to separate himself from his body. The result? He did indeed feel separated from his body, but not more at peace or enlightened. He found enlightenment only after giving up these practices and feeling at one with his body once more.

### 6. Observing the mind without judgment is the best way to separate from it, and thus free yourself from pain. 

After you realize that your mind is causing you pain, preventing you from truly living in the present, you need to detach yourself from it.

How?

To separate yourself from your mind, you must become fully conscious of it and the power it has over you, otherwise you'll never understand the countless small and subtle ways it influences your thinking and behavior — and therefore your happiness.

For example, if you want to observe your mind, ask yourself: "What will my next thought be?" If you focus fully on that question you'll see that it takes a while before the next clear thought arrives. Through observing, you've managed to create a gap in the flow of thinking.

If you do that often enough, you'll start to notice how much you are normally occupied by the continuous flow of the mind. And you'll have found the primary tool for interrupting your mind and thus separating from it.

The second method available to you is to observe your mind without judging. Judging is itself an act of the mind, so if you judge something you are back to using your mind again.

For example, if in the middle of your work you feel like running, just follow that impulse of your body. Your body knows what's good for it, so go outside and run.

Then, listen to the little, nagging voice inside your head that says: "Right now you should be working and not running or wasting time!" But don't judge that voice as good or bad, and make no attempt to follow the advice. Just smile at it, and accept that it exists. By doing so, you'll learn to notice your mind without having to follow it to wherever it's trying to lead you.

### 7. Try to exist in a state of permanent alertness. 

While you get better at separating yourself from your mind, you can try adopting another technique: _active waiting_.

This is a special kind of waiting state, like when you're aware that something important or serious could happen at any moment. In such a state, all of your attention is focused on the _now_.

When you get into a state of active waiting, there's no time for the daydreaming, planning or remembering that usually distracts us from the present. For example, while taking an exam you should waste no time worrying about the results, but instead remain fully present and pay very close attention to the work before you. Entering a state of active waiting shortly before and during the exam can help you to achieve that.

While in this state, you also pay attention to your body because it has to be ready for anything to occur. As we've already seen, this focus on the body is also crucial to living in the present.

For example, Zen masters used to sneak up to their pupils, who had their eyes closed, and then attempt to hit the waiting student. The waiting forced the students to fully concentrate on their body, and thus they were able to sense the approaching masters and evade their "attack."

Many spiritual teachers recommended this state of waiting to their pupils because they believed it would lead to a good life. For example, when Jesus was asked by his disciples what they should do to live a good and peaceful life, he advised them: "Be like a servant waiting for the return of the master."

Since the servant doesn't know at what hour the master will come, he is in a permanently alert state. He does not make great plans for the future and is constantly aware of his surroundings to make sure he doesn't miss the master.

### 8. Living in the present can be hard for your partner, but it can also improve your relationship. 

After following the previous steps, you're now able to live in the present, and are no longer completely dependent on your mind.

But how will that change your daily routines? For example, your relationships?

It's extremely difficult for a "normal" person to share their life with someone who lives fully in the present. The ego of the non-present person feeds on problems, while the person who is present, calm and at peace is experienced as a threat. The non-present person's ego reacts by creating further problems — for instance, by insulting the other, debating a trivial issue to disrupt the peace, or continually referring to past incidents to pull them out of the present.

Why would they do that?

This is best answered with an analogy: just as the darkness cannot survive near the light, it is difficult for a person still controlled by the ego to be near a person living in the present for very long. Strong opposites cannot exist in close proximity. If you place a candle in the darkness, the darkness disappears. If you put water on fire, the flame extinguishes.

But if you do it correctly, living in the present can also greatly improve your relationship: you'll be able to stop judging, criticizing or trying to change your partner, and instead see him or her as an independent person.

Furthermore, the insight you gain by living in the present can be used to disrupt otherwise endless cycles, like debates that never reach a conclusion. The inner peace that being present brings enables you to hear your partner without judging.

If you live in the present, then living with you can be very difficult for your partner. It can even become a new test of your relationship. In the long run, however, it can offer a great opportunity for positive change — for both your partner and for your relationship.

### 9. Not all pain is avoidable: surrendering to the present does not mean ignoring sad or hurtful feelings. 

Even if you live fully in the present, some feelings of sadness and pain are unavoidable.

But what should you do with them? Just suppress them and pretend that everything is ok? That doesn't sound like a very good idea.

While it's true that most pain is self-created, that doesn't mean we create all of it. A good example of unavoidable pain is that inflicted on you by those who are still controlled by their destructive mind. Another example is the death of a loved one. Since you cannot lead everyone around you to enlightenment, and you certainly cannot put a stop to death, this pain is clearly unavoidable.

So what can be done?

When you experience something traumatic, which causes you real pain, you can just accept it for what it is. For example, if you lose a beloved person to death, you will of course mourn and feel sadness. But if you're able to accept this as something that simply _is_ and cannot be changed, then you'll avoid needless suffering.

Being sad is a natural feeling, something you don't need to feel guilty or ashamed about. Things are as they are. Accepting this means that you do not waste your time constantly wishing that things were different.

By being present you will be able to avoid most of the pain in your life, but not all of it. Furthermore, living in the present does not mean ignoring or suppressing pain. Instead, it provides you with the inner strength to accept such difficult and painful facts of life.

### 10. Surrendering to the present does not mean living a passive life. 

Inner peace is a nice thing to have, but when your outer life situation is bad, inner peace is not worth very much.

Does accepting the present automatically lead to a passive way of living in which you are not mindful of or willing to change anything that troubles you?

Not necessarily.

Living in the present is an inner process of feeling and insight, and doesn't entail you having to develop a passive external behavior. For example, if you're stuck in the mud, you don't simply tell yourself that you always wanted to be stuck in the mud. Instead, you can try, without panicking, to free yourself from it.

Living in the present can even provide you with new resources, and new ways to solve problems. It's certainly true that living in the now can bring you new forms of strength and determination, because you don't waste your inner resources creating problems. In fact, by living in the present you actually see _no_ problems — only individual, manageable situations that you can solve, one by one. This makes you much more effective.

Living in and accepting the present does not mean you commit to a passive life, or that you choose to not even try to change it for the better. Rather, by focusing on the present, and keeping the past and future in their place, you are more able to see clearly whatever it is that's actually wrong at any given moment, and you also have the strength to change those things for the better.

### 11. Final summary 

The key message in this book:

**Do not focus on the past or the future! Live in the present and try to separate yourself from the over-thinking mind. Following this method will reduce your suffering and improve your life.**

The questions this book in blinks answered:

**How can I make my life better?**

  * By focusing only on the present — and ignoring the past or future — you can greatly improve your life.

  * A part of you needs pain to survive, and it creates most of the pain you experience.

**What is the connection between mind and suffering?**

  * The "ego" is a part of your mind that stops you from being happy.

  * If you want a richer and almost painless life, separate yourself from your mind and focus on your body.

  * Observing the mind without judgement is the best way to separate from it, and thus free yourself from pain.

  * Try to exist in a state of permanent alertness.

**What effect will this have on my life?**

  * Living in the present can be hard for your partner, but it can also improve your relationship.

  * Not all pain is avoidable: surrendering to the present does not mean ignoring sad or hurtful feelings.

  * Surrendering to the present does not mean living a passive life.

**Suggested further reading: _The Art of Communicating_ by Thich Nhat Hanh**

_The Art of Communicating_ offers valuable insight on how you can become a more effective communicator by practicing _mindfulness_. Drawing on Buddhist wisdom, it outlines ways you become a respectful listener, express yourself well, and ultimately improve your relationships with your loved ones.
---

### Eckhart Tolle

Eckhart Tolle is a German-born resident of Canada who was depressed for most of his life until he had what he called an "inner transformation." In the wake of this, he became a spiritual guide and wrote the bestselling self-help bible, _The_ _Power_ _of_ _Now_.

