---
id: 53a9d57c66656300071c0000
slug: the-power-of-positive-thinking-en
published_date: 2014-06-24T00:00:00.000+00:00
author: Norman Vincent Peale
title: The Power of Positive Thinking
subtitle: None
main_color: EF4030
text_color: BD3226
---

# The Power of Positive Thinking

_None_

**Norman Vincent Peale**

_The_ _Power_ _of_ _Positive_ _Thinking_ is a self-declared manual for achieving happiness and overcoming even the most difficult problems. Focusing on our attitudes toward our life situations as well as our faith, the book acts as a guide to the way we can change our lives through a steadfast focus on positive outcomes — if we're willing to make that choice.

---
### 1. What’s in it for me: Get inspired with a classic self-help book. 

_The_ _Power_ _of_ _Positive_ _Thinking_ is among the seminal American self-help books. It is one of the first books of its kind to explain how changes in your thinking can influence outcomes that you at first perceived to be inevitable, making it a piece of American pop culture history.

The book was wildly popular, selling at least 5 million copies following its publication, and it continues to be widely read.

This popularity did not stop the book from being controversial: both the author's arguments as well as his methods for presenting information were highly contested, and continue to be so. Many, however, still praise the book for its insights into positive thinking. They have changed their lives by using the techniques presented in the book and are thankful for the results they've seen.

Even if you don't find all of the author's arguments convincing, these blinks will nevertheless show you how self-confidence, a focus on positive outcomes, and an appreciation for the people around you are essential to a happy, healthy life.

In these blinks, you'll learn:

  * how your thoughts influence your reality,

  * how taking a walk in the forest can help you to deal with modern life's troubles and

  * how your faith can help you to clarify your goals.

### 2. Believe in yourself: why self-confidence leads to success. 

Feelings of anxiety and inferiority are like a modern plague. Many people feel as though there are problems everywhere they look, then get overwhelmed before trying to shift the blame for their situation onto others or their circumstances, rather than actively fighting to change it.

In some ways, this is true: if you feel inferior, or suffer from negative thinking, then you're more likely to experience negative outcomes.

For example, if you start your new job thinking that you'll never fit in and that no one will like you, then you will have created a self-fulfilling prophecy. Once your feeling of inferiority has been vindicated, you're more likely to have negative feelings about the future, which only leads to a vicious circle that feels impossible to escape.

Yet it's _not_ impossible! If you have self-confidence, then you have the power to shape your life the way you want it. For example, the author knew a man who had lost everything, but was able to regain his energy and the willingness to continue living simply by making a list of all positive things he still had in his life. By focusing on these aspects, he proactively changed his perspective to a healthier one.

But if you want to change your circumstances, then you have to change your thinking instead of passively accepting your situation.

Just like the man in the example, you too can use the power of focusing on the positive and believing in success in order to overcome any obstacles in your life.

One way to do this is by visualizing the possible positive outcomes that you want for yourself, and then visualizing your problems. By comparison, they seem smaller and more easily solvable.

For example, the author's magazine, _Guideposts_, underwent a serious crisis that was solved in a curious way: during a meeting, the staff actively visualized having more subscribers. That's it. And yet, this wishful thinking was followed by a real increase in their readership!

### 3. Give and take: how caring about others will make them care about you. 

The desire to be appreciated is fundamental to our human nature. We are social animals who need companionship, and yet so many care only about themselves. As a consequence, many also suffer from the feeling of not being wanted or needed by others.

Luckily there are steps we can take to become likeable.

First, you must realize that the reason other people don't like you lies within you — it's your attitude. Thankfully, you can actually change your attitude in order to be appreciated by others by learning to become a _comfortable_ _person_.

James A. Farley, the former Postmaster General of the United States, is a great example of a comfortable person. He was well liked by virtually everybody because he had the right attitude: others felt that he was "natural," "honest," and "comfortably outgoing."

Think for a moment about the people you really like: do they talk about themselves all the time, or do they show an interest in you? They probably show interest in you, and you should do the same. When you think positively about others, show them that you care about them and what they do, who they are, etc., then they will want to be friendly with you in return.

For example, the author once counselled a man at his clinic who felt as though everyone disliked him. He realized eventually that this was caused by his exhausting love for himself and self-centered attitude. So he made a list of all the people he met every day and made a concentrated effort to talk to them and notice them as individuals.

By shifting his focus from himself to others, he soon became loved and appreciated.

You will of course find some individuals who are not easy to like. Yet even they have hidden positive qualities you can discover if you put in the effort.

> _"Getting people to like you is merely the other side of liking them."_

### 4. Not alone: why you shouldn’t face your problems on your own. 

Do you ever feel like you can't ever truly take a break because no one could do your job as well as you can? That without you, everything would fall apart? For many, the consequence of this attitude is the heavy burden of mounting problems and depression.

And when you feel depressed and burdened by your problems, do you ever think: "No one in the whole world ever had the same problems as me"? While it certainly might feel that way, this kind of thinking is actually an illusion; millions of people out there have the exact same problems.

There is wisdom in this bit of truth: no situation is hopeless. Just imagine — there are people who have overcome "every conceivable difficult situation," and even when they felt hopeless, they found a way to carry on.

On the other hand, negative thoughts, hopelessness and isolating yourself lead mostly to unhappiness and failure. You can combat this with reminders to focus on the good, by doing things like keeping motivational notes that highlight the power of your positive thoughts.

What's more, sometimes our sense of self-importance can make us feel as if we are carrying the world and all its problems on our shoulders. But if you make _every_ problem your own, then you're less likely to solve any!

This is because we have only limited time to solve seemingly infinite problems. Trying to solve everything leads to haste, tension and a lack of concentration.

However, you can combat this stress with prayer or meditation to reduce tension. In addition, you can delegate tasks and organize yourself in order to achieve better results and better health.

Ultimately, if you accept that you, your abilities and your problems aren't totally unique then you'll experience better outcomes and a healthier life.

### 5. Attitude: why your own thoughts are the key to overcoming your problems. 

Have you ever felt totally overwhelmed by all your problems? While some of them might be real and pressing, such as debt or disease, it's your _attitude_ toward those problems that is of the ultimate importance. Debts take time to pay, and diseases don't disappear with the wave of a wand, but you can change your attitude in an instant.

If you live your life with a focus on positivity and the search for inner peace, you'll have enough energy to overcome your perceived problems. In fact, you can easily organize your life in a way that leads to inner peace.

For example, the author once had a breakfast meeting with two people who had both had very different experiences the night before: one watched the news and slept poorly, the other read the Bible and enjoyed a wonderful sleep.

While this might seem trivial, our sleep is of the utmost importance because it refreshes our energy. Sleeping with "an earful of trouble" — for example, by watching the news while dozing off — will lead to a restlessness that prevents you from tackling your problems. Luckily you have the power to positively influence this vital source of energy.

In contrast, most people think that their situation is determined by outside circumstances and dumb luck. But in fact, your world is nothing more than the thoughts you have about your life experiences.

If you think positively, you set up positive forces that bring with them positive results. Thinking negatively, on the other hand, brings negative results, and this negative thinking can even affect your physical health by making you ill.

We can see this principle in action by examining one young man, whom the author describes as "one of the most complete failures" he had ever met. For the longest time, he couldn't make any positive changes in his life because he wouldn't examine his attitude. However, after realizing that his life was bad because his thoughts were negative, he was able to set everything right.

> _"The world in which you live is not primarily determined by outward conditions and circumstances, but by thoughts that habitually occupy your mind."_

### 6. Don’t worry, be happy: how you can overcome the destructive and unhealthy habit of worrying. 

It's only natural to feel worried or insecure. However, while worrying about things like your finances or health is understandable, it is also unhealthy, and can lead directly to illnesses and undesirable changes in your personality.

Thankfully, worrying is a merely a "habit," and habits can be overcome. This particular habit is one that is especially worth ridding ourselves of, as it is the source of many physical and psychological diseases. For example, the stress from worrying can lead to higher blood pressure, lower life expectancy, or even arthritis.

And yet, breaking the habit of worrying is surprisingly easy: simply believe that it is possible to free yourself from it! If you can imagine a worry-free life, then you can live it. There are a variety of techniques that you can use to get a hold of and ultimately break your habit.

All of these techniques will have one thing in common: they involve the practice of _draining_ your mind, i.e., ridding your mind of oppressive, negative thoughts. This is especially important before going to sleep, as that's when our thoughts sink deeper into our subconscious. Fears, worries, and other negative thoughts can "impede the flow of mental and spiritual power" if you don't drain them before they have a chance to sink into your subconscious.

However, it's not enough to simply drain your mind; you must fill it back up again, this time with positive thoughts to replace the negative ones. These are thoughts that inspire feelings like hope, courage and faith. It may be difficult at first, but you must keep at it by practicing every day if you want to achieve positive change.

### 7. Give yourself a break: why you need to make the choice to be happy. 

It's no secret that modern life can often be hectic and over-stimulating. As a result, sleeplessness, stress and headaches have become common for many. Most feel unhappy under these conditions, but they just can't seem to find a way out of them.

In fact, many people destroy themselves emotionally and physically in their attempts to keep up with the swift tempo of modern life. Today's "feverishly accelerated" pace can lead to serious emotional illnesses, such as chronic fatigue and frustration.

It's therefore important that we are able to find strategies to deal with the stress and anxiety that are part of modern living. The author, for example, often felt the burden of a hectic modern life overwhelming, especially when he was in a big city like New York. He found that by taking the time to go out into the woods, he could relieve that tension.

Ultimately, the unhappiness that we experience because of these conditions is not a given; it is a choice that can be made, or reversed. Indeed, there are real social conditions, such as poverty or unemployment, that can lead to unhappiness.

But these conditions are not as important as our attitudes towards them: they have the power to further entrench unhappiness into our lives _or_ help us choose happiness despite negative conditions. If unhappiness is chosen it can lead to a loop: seeing everything negatively will again produce unhappiness.

Children are the living proof of this concept: they are "experts in happiness," and because they are not "super-sophisticated" they have not yet learned that society expects them to be unhappy.

### 8. Never lose heart: why you should always try to see the positive even when facing problems. 

Have you ever been totally unprepared for an important meeting? It's a horrible feeling, isn't it? It's certainly important for us to prepare for possible obstacles, but when we expect the worst, even only theoretically, then we're more likely to experience negative outcomes.

In fact, when you focus on the worst possible outcomes, you stop the _power_ _flow_, thus cutting off your ability to handle obstacles. Even if you do manage to stand up to these obstacles, your negative thinking will prove you have little strength to deal with them.

How we handle these obstacles is determined by our attitude. If we believe them to be manageable, then they will be!

We can see an example of this in the legendary tennis player from the 1940s, Pancho Gonzalez. Although he was not the most skilled player, and by all means an outsider, he nevertheless managed to win many championships during his career.

How did he manage it? He never allowed himself to become discouraged when he faced defeat, always looking for positive possibilities instead and striving for them.

And we can solve _every_ problem this way because there are no unsolvable problems. You must simply practice positive thinking and see the possibilities available to you.

Surely your subconscious is telling you that there are problems you cannot solve. However, your subconscious can be trained to be more positive. One way to accomplish this is by changing the way you express your possible life outcomes, focusing on the positive aspects and not the negative ones.

Here the practice of mental "draining" is once again important: if you can rid your mind of negative outcomes, then you can focus on the achievable, and thus overcome your problems.

Often there is more than one solution to a given problem, and you may not find it on your first attempt. With a clear mind focused on the positive, you stand a much better chance of finding them.

### 9. The Bible: how faith can help you solve your problems. 

The Bible and Christianity are often seen as offering abstract advice or general guidelines for how to live life. But the teachings of the Bible can build a foundation for your thoughts and actions, helping you in your quest for positive thinking, and consequently changing your life much more radically.

In fact, biblical lessons are quite practical, and they can help solve many problems if you follow them. The Bible teaches us that we should avoid becoming angry, and instead view others in the most positive light possible. Doing so will lead to a healthier and happier life.

For example, two friends of the author, Bill and Mary, felt cheated when Bill wasn't offered his retiring boss's job, as they'd been led to believe Bill would be his successor.

The author offered him advice based on the teachings of the Bible: let go of the anger towards the new boss, and instead deal with the frustration in a positive way, by working even harder. Soon enough, the new boss moved on to a better job, and Bill got promoted, all thanks the teachings of the Bible.

However, your faith can only help you to succeed if you first know _what_ you want, _where_ you want to go, and whether it is _right_ to want it.

In order to answer these questions, you can turn to prayer, which can help release negative energy and lead to creative ideas. If think about your problems from a bird's eye view through prayer, then you have a better vantage point from which to see solutions.

However, your faith can only guide you to these solutions if the success you seek is "morally, spiritually and ethically" right. If the success you want "is wrong in the essence it is bound to be wrong in the result."

### 10. Body and soul: why faith should be part of every form of therapy. 

We know that the mind and the body are intricately related: that when you experience stress, for example, it can manifest as physical pain. Sometimes, too, our body's "complaints" offer no clear medical explanations. In these cases, it's not the body at all that's the problem, but the mind: not only must we take care of the body, but also mind and spirit.

While we shouldn't rely upon faith alone to heal our physical ailments, a combination of "God and the doctor" will lead us to true health.

Indeed, some medical doctors known to the author had begun to prescribe religious and inspirational books in lieu of traditional medicine with the aim of helping their patients help _themselves_ by acquiring a positive mind-set and faith.

In fact, the author even ran a clinic in New York City together with a psychiatrist, where they cooperated with one another to combine their therapies. By pooling their knowledge, recognizing their own unique roles and skill sets as well as the importance of the other, they were able to achieve the strongest results.

The reason that they could work so well together is related to the power of the subconscious mind. While the conscious mind "may suggest sickness, even death," the vast majority of your mind lies within the subconscious. If you "let the picture of health sink into the subconscious," then "this powerful part of your mind will send forth radiant health energy."

This is why faith is so important: by trusting in a higher power, you're better able to truly believe in your positive convictions about your health. Positive thoughts can lead to healing and health, but only when those thoughts are actually genuine. You have to actually believe in your own physical and spiritual healing, and that requires faith.

### 11. Final Summary 

The key message in this book:

**No** **matter** **how** **insurmountable** **they** **may** **seem,** **there** **are** **no** **problems** **in** **your** **life** **that** **cannot** **be** **overcome** **by** **the** **power** **of** **positive** **thinking.** **By** **staying** **calm,** **gaining** **perspective,** **nurturing** **your** **faith** **and** **focusing** **on** **positive** **outcomes,** **you** **will** **have** **the** **power** **to** **create** **a** **happy,** **healthy** **life.**  

 **Remind** **yourself** **to** **stay** **motivated.** Write a note to yourself such as: "The difficult times are only mental. I think victory — I get victory."
---

### Norman Vincent Peale

Norman Vincent Peale was a minister in New York City, famous for such bestsellers as _The_ _Art_ _of_ _Living_, _Inspiring_ _Messages_ _for_ _Daily_ _Living_, and _A_ _Guide_ _to_ _Confident_ _Living_. He was also active on TV and radio, and was the founder of the Positive Thinking Foundation.

