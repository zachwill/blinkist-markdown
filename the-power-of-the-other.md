---
id: 57c948a308dc1b000317fe6f
slug: the-power-of-the-other-en
published_date: 2016-09-06T00:00:00.000+00:00
author: Dr. Henry Cloud
title: The Power of the Other
subtitle: The Startling Effects Other People Have on You, from the Boardroom to the Bedroom and Beyond – and What to Do About It
main_color: D23F45
text_color: 9C2D31
---

# The Power of the Other

_The Startling Effects Other People Have on You, from the Boardroom to the Bedroom and Beyond – and What to Do About It_

**Dr. Henry Cloud**

_The Power of the Other_ (2016) shows you how the relationships in your life profoundly affect how you think, act and feel. These blinks examine why certain relationships harm us while others nurture us. Importantly, you'll learn how to cultivate real connections that fulfill you and help you grow as a person.

---
### 1. What’s in it for me? Learn to harness the power of positive relationships. 

Bad relationships come in many flavors. You may be bullied by a classmate. Or find that you're always cleaning up the emotional messes of a friend. Or perhaps you fear the day you'll let down a loved parent who thinks the world of you.

All of these relationships have one thing in common: they are unhealthy and keep you from being the happy, fulfilled person you can and should be.

Fortunately, there are many healthy, beneficial relationships in the world. Sometimes, with the correct tools, we can improve the troubled ties with which we bind ourselves.

These blinks will help you identify the different types of relationships you're in and show how each affects how you live your life.

In these blinks, you'll discover

  * why President Reagan told his security advisor about squirrels in the Rose Garden;

  * how a relationship might feel great but really be toxic; and

  * how a "corner four relationship" is the positive bond you need.

### 2. Your well-being depends on your brain, your relationships and your mind. 

No bodybuilder strengthens just one or two muscles, but trains to improve his entire body.

Cultivating happiness works the same way. If you want to live a happier life, you have to work on yourself as a whole. This means taking care of your mental health, which means taking care of your brain.

The brain functions through a combination of electrical charges, hormones and neurotransmitters. If something upsets the balance of these elements, you won't feel well. For example, if your brain doesn't secrete a sufficient amount of the chemical _serotonin_, you'll struggle to focus and potentially feel depressed.

Your success and well-being also depend on your relationships.

Each and every person needs close, supportive relationships to thrive. Strong relationships help us cope with stress, overcome setbacks in our lives and even heal pain.

Unhealthy relationships, on the other hand, add stress. They can hamper your ability to deal with tough situations at work, for example. Imagine returning home from a rough day at the office. If you turn to a partner to vent but your partner just makes fun of you, you'll feel even worse.

In addition to taking care of your brain and your relationships, you also have to tend to your _mind_.

Your mind isn't the same thing as your brain, however. Your brain is like a piece of hardware, where your mind is the software that runs it. Your mind is essentially the _mental process_ that works to decode incoming information, and importantly, determines how you react to this information.

Understanding what's happening in your mind is a key element of living well. When you understand your mind, you can stay in control of your thoughts and better manage your emotions.

All three components — your brain, mind and relationships — are crucial in leading a happy life. Now we're going to go deeper and focus on just one of these elements: your relationships.

### 3. Avoid these harmful relationships: those with no connection, a bad connection or a fake connection. 

Every relationship falls into one of four categories or _connection corners_. Let's start with the three kinds of relationships you should avoid.

The first kind is a _corner one relationship,_ in which you can't claim a real connection with the other person. You don't exchange any meaningful information or positive energy with the person, so you don't get anything substantial out of the relationship.

Imagine a manager who feels lonely even though she's surrounded by coworkers, day in and day out. Her teammates don't share important news, and she doesn't involve her team in decision making. This complete lack of connection is why this example is a corner one relationship.

Romantic partners might maintain a corner one relationship if they don't empathize with or support each other. If you feel that your partner doesn't listen to you, for example, or is lacking in empathy, you probably have a poor connection with that partner — a classic corner one relationship.

The second kind is a _corner two relationship_, in which a relationship is harmful.

It's difficult to live alone, so people who struggle to connect with others often end up "settling" for a romantic partner, even if at the core the person is a poor match.

Bad connections, however, aren't always abusive. Perhaps you don't feel well whenever you spend time with a particular person, or maybe a person makes you feel inferior or self-conscious. These are harmful relationships.

The third kind is a _corner three relationship,_ and often doesn't appear damaging on the surface. Yet corner three relationships are bad because they're fake.

While an unconnected or harmful relationship might make you feel bad about yourself, a fake relationship can make you feel good, at least temporarily. Yet corner three relationships are often based on harmful connections, such as a shared addiction, an illicit affair or an ego-based partnership based on false praise.

The problem is, you can easily get hooked on a fake relationship. Passion, admiration or intoxication might feel good in the moment, but they always leave you wanting more.

What's a corner four relationship? The next blink explains.

### 4. In sincere relationships, people thrive as they can be their true selves and openly admit weaknesses. 

Do you have close friends or relatives with whom you can fully be _yourself_? People with whom you don't have to fear letting your guard down?

When you're with people like this, you show your _true self_ — not the _false self_ you often present to the world.

Your false self is like a mask you wear for protection. It makes you feel stronger, smarter or more confident than you are. We use our false selves to earn respect and guard against people who might ridicule or attack us.

Leaders, in particular, tend to hide their true selves because they're always in the spotlight. People admire leaders and place their faith in them, so leaders often feel they can never show weakness.

Former President Bill Clinton, for instance, once told former British Prime Minister Tony Blair about the importance of putting on a "face," or pretending to be strong and optimistic no matter what.

In a _corner four relationship_ — a real connection — you don't have to pretend.

You feel safe sharing everything you think and feel within your relationship. You and your partner understand and care deeply for each other. Your partner won't take advantage of you if you show weakness.

Part of the reason leaders are successful is that they are often people who have overcome challenges and sought advice from mentors.

Virgin Group founder Richard Branson nearly gave up his dream to found an airline because he was inexperienced and didn't have enough capital to compete with giants like British Airways. Instead of quitting, however, he reached out to airline veteran Freddie Laker and admitted that he needed help.

With Laker's help, Branson became a major player in the airline industry. Sometimes confiding in a trustworthy person about a weakness is the most powerful thing you can do.

> _"There's no such thing as a self-made man or woman. Every great leader has opened up to someone who could meet a need..."_

### 5. Meaningful, connected relationships energize us and help us thrive in everything we do. 

Have you ever walked into a party, class or meeting and immediately felt good? Sometimes a space exudes positive energy, even if you can't put your finger on the reason why.

This feel-good energy is generated from _corner four relationships_.

Real connections give you many kinds of energy. You might feel joy and excitement, and be extra-motivated, when you're part of a positive team or on a good date — exactly the energy you feel when you first enter that room — but there's something else, too: intellectual stimulation.

Intellectual stimulation comes in many forms. Maybe you and your friends like to speculate about the origins of the universe, learn new skills together or explore new places.

The author discovered the importance of intellectual stimulation when he became depressed after a golf injury. He recovered thanks to two loving people: a fraternity brother and the fraternity brother's sister.

They fueled his intellect by giving him books, improving his diet, increasing his physical energy and offering emotional, loving care. It is these real, energized connections that we need to succeed.

Think back to that room filled with positive energy. In any successful organization, there are many such rooms. Great leaders take care of employees by encouraging and challenging them to be their best.

Positive communities are also helpful when you're facing a personal challenge, such as overcoming an addiction.

Alcoholics Anonymous and Weight Watchers are successful because these organizations provide people with a space to meet, connect and cheer each other along the road to success.

### 6. A person with whom you're well-connected will offer you freedom and valuable feedback. 

A person who respects you won't necessarily solve your problems for you. In fact, the opposite is usually true.

Let's look at the reasons why.

A corner four relationship offers you freedom, but it comes with responsibility, too. When people are healthy and well-connected, they respect each other's autonomy and intelligence. They don't fight for control; both partners are free to make both positive choices and mistakes.

And when a person respects your autonomy, that person doesn't barge in to solve problems for you. They trust you to handle the problems yourself, which means you need to step up and do so!

Former US National Security Advisor Colin Powell once was briefing then President Ronald Reagan on global hotspots, looking for advice from the president. While Powell talked on, Reagan suddenly said, "Hey look, they're eating them!"

The fact was that Reagan wasn't paying attention to Powell at all. He was instead watching squirrels eating nuts in the Rose Garden.

His interruption sent a clear message to Powell, however. It was as if he said, "It's your problem, and I trust you to solve it on your own."

Powell had freedom in his relationship with Reagan, which meant he also had a lot of responsibility.

A person with whom you share a real connection might not solve your problems, but they'll give you valuable feedback. These people want to see you do well, so they'll give you extra attention.

If you're a writer, a friend might read your new manuscript closely. That friend then will offer specific feedback — detailed enough for you to _act_ on so that you can improve your work.

The feedback that helps you solve problems yourself is always more valuable in the long run.

### 7. Corner four relationships help you to accept failure and bounce back from it. 

Pixar is one of the best animation studios in the world. Their greatest films, like _Up_ and _WALL-E,_ became instant classics when they were released. So it might surprise you to learn that when Pixar starts a new project, the first work they produce is usually terrible!

It's natural to fail and start out less than perfectly. Even the greatest projects often have a bumpy start, and setbacks are simply part of the process. When you begin a project, there's usually some distance between where you are and where you want to be. It's totally normal to make mistakes when learning a new skill, like playing piano or investing in the stock market.

That's where healthy relationships play a particularly important role. They help you overcome challenges and failures. You can only admit to a problem or failure if you feel safe, that is, if you have a real connection.

The more you admit to failure, the more you get used to it and accept failure as a regular part of the learning process. The author, for instance, once felt like a failure when he made a bad decision, but he confided in his mentor who told him, "We've all been there." When he realized that even his mentor had been through struggles like his, he started to realize it was okay to fail.

Likewise, the employees at Pixar also feel safe to discuss their mistakes. They've learned to view setbacks as problems the team can address and overcome together. Environments like that at Pixar foster corner four relationships that allow us to accept failure instead of perceiving it as a threat.

### 8. One person’s values and beliefs in a strong relationship can shape another person’s behavior. 

When you're in a close relationship, you can gain insight that might reshape the way you think and behave through a process called _internalization_.

In short, you internalize the things that your friend or partner tells or teaches you.

Think of a child whose mother tells her not to touch a hot stove. After the child has heard this warning enough times, she'll internalize it. Next time she sees a hot stove, she can warn herself, and won't need her mother to remind her.

This is why real connections are so powerful. When you share a strong connection with a person, that individual can profoundly impact the rest of your life, even after that person moves away or passes on.

The insights such relationships transmit live on as memories and thought patterns that can shape your behavior. What's more, you can use internalization to affect _other people's_ behavior!

Imagine you're a leader planning to retire in a year, and you're afraid your company might fall apart without you. How can you ensure your employees keep everything running smoothly?

You can help your teammates by sharing your values and knowledge with them, so these lessons are internalized over time. Eventually, your team will understand how the company works just as well as you do and adopt your good habits as their own.

You might help employees develop new routines now to continue after you've retired. If you're particular about responding to customer complaints with a personalized message, for example, get your successor to start writing similar letters before you leave.

Child rearing works through the same process of internalization. Sure, your teenaged son won't let you accompany him on a date, but you can teach him how to take precautions with strangers and new partners before that first date even happens.

> _"Internalization starts at birth, when we begin to adopt a self-soothing system in infancy."_

### 9. Use five criteria to assess if you can trust a person in your life. 

Why do people invest money in stocks? They expect their investment to pay off over time.

In a similar fashion, we invest time, effort and goodwill into relationships because we expect them to improve our lives.

So how do you know whether you should trust someone? Let's go over some criteria for assessing trust.

First, trust someone only if you're sure that person understands what's important to you.

Imagine you've hired a hardworking gardener but he misunderstands your instructions and accidentally poisons your whole garden. If someone doesn't understand what you need, that person can be destructive — even if he had good intentions from the start!

Second, make sure the person has your best interests at heart. Does the person want the best for you?

Third, ask yourself if the person is reliable before you turn to him for help. You wouldn't trust an alcoholic to help you get sober, for instance. If the person hasn't been able to overcome his addiction, he's unlikely to offer positive guidance on helping you overcome your problems.

Fourth, assess the person's character. Character isn't just about honesty or integrity, but also about specific character traits. Certain traits can be helpful or harmful depending on the situation. You shouldn't trust a pessimist to help you realize a daring idea, for example.

And fifth, consider the past experiences you've shared with the person. Past experiences tell you in which kind of situations you can trust the person.

Let's say you depended on a friend to pick you up at the airport, but he forgot. If he made the same mistake again at a later date, you probably shouldn't trust him a third time.

Trust is important yet complex, and is one of the key elements of any strong connection — don't take it lightly.

### 10. Final summary 

The key message in this book:

**Your health and happiness depend largely on the relationships in your life. So do your best to avoid non-connections, bad connections and fake connections. Strive to make real connections that nurture you, help you overcome challenges and improve as a person. Choose wisely when placing your trust in people. Relationships can change your thoughts and behavior, so forge them only with people who are right for you.**

Actionable advice:

**Be specific about what you want from a partner.**

The next time you're feeling dissatisfied in a relationship, be specific when telling your partner what you want. Don't say you want to "connect more." Say that you'd like to spend more time together by going on afternoon walks or dinner dates. You can't plan to "connect," but you can plan a date! Concrete goals are easier to meet.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Emotional Intelligence 2.0_** **by Travis Bradberry and Jean Greaves**

_Emotional Intelligence 2.0_ (2009) gives you expert insight into which skills you need to read others and build better relationships. It breaks down the four aspects of emotional intelligence, or EQ, and gives advice on what you can do to improve your own skills.
---

### Dr. Henry Cloud

Dr. Henry Cloud is a psychologist, expert on leadership and bestselling author. _Success_ magazine in 2014 named Dr. Cloud as one of the 25 most influential leaders in the area of personal growth and development.

