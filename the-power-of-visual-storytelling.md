---
id: 56376a8362346500071e0000
slug: the-power-of-visual-storytelling-en
published_date: 2015-11-03T00:00:00.000+00:00
author: Ekaterina Walter and Jessica Gioglio
title: The Power of Visual Storytelling
subtitle: How to Use Visuals, Videos and Social Media to Market Your Brand
main_color: D23F45
text_color: 9C2D31
---

# The Power of Visual Storytelling

_How to Use Visuals, Videos and Social Media to Market Your Brand_

**Ekaterina Walter and Jessica Gioglio**

_The Power of Visual Storytelling_ (2014) is the ultimate guide in showing you how to tell stories through visuals and how to use this technique to your company's advantage. With well-illustrated examples, these blinks explain in detail how you can master today's most important social media platforms through powerful images.

---
### 1. What’s in it for me? Learn visual storytelling, a powerful tool in successful brand marketing. 

Visual storytelling. These are two words that when combined create a powerful method for a company to successfully market its brand — if used in the right way. 

In _The Power of Visual Storytelling_, Ekaterina Walter and Jessica Gioglio share their experience in telling stories with powerful pictures, explaining how you too can use visual media wisely to boost your brand and keep customers coming back for more. 

You'll not only learn why visual storytelling is important in the digital age but also absorb crucial advice in choosing which social media sites would work best in achieving your business goals. 

In these blinks, you'll discover

  * which social media platform's user base is nearly 80 percent female;

  * why a company needs to pretend it's a person when posting online; and

  * how you can grow your salesforce exponentially with smart posts.

### 2. With social media, a picture is worth far more than a thousand words; it can tell your whole story. 

Author J.K. Rowling sketched out the magical world of Harry Potter before she wrote one word of her story. The same is true of author J.R.R. Tolkien, who drew a map of Middle Earth before he wrote his fantasy epic, _Lord of the Rings_. 

Why would an author draw before she writes? Because many authors understand how important images are to the process of creative writing and storytelling. 

Your brain processes images faster than it does text, some 60,000 times faster! This is because the human brain, at least evolutionarily, has processed images to understand the world for much longer than it has grappled with the written word, a relatively new phenomenon. 

Images too have a more direct effect on our emotional state. For instance, consider how you feel when you read the word "girl" and when you look at a picture of a smiling girl. While the word is benign, the photo may trigger an emotional response. 

This knowledge should guide your use of social media. People love stories; yet to be an expert visual storyteller, you need to know the difference between describing a sunset in a postcard and sharing a beautiful sunset snap on Instagram. 

This is the core of _visual storytelling_, where you use images to produce a story around your brand, personality and product. These visuals can take any form, as pictures, videos, infographics or any other media. 

Marketing software Hubspot uses Pinterest, as the site allows the company to present boards, or a collection of pins, on e-books and charts that present business value to its fans. Yet the company also participates in fun boards like Meme-tastic Marketing of Awful Stock Photography — a place where the company can show its funny side, too. 

Now, let's explore the most essential aspects of visual storytelling.

### 3. A poignant visual story is great; yet a visual story shared does wonders for a company brand. 

Even if you have never heard of visual storytelling, you probably have told many a story with an image or video. Every image you post to Facebook, for example, can tell a story.

According to Buzzfeed, some 208,300 pictures are posted to Facebook and 27,800 images are uploaded to Instagram every minute! 

But how can you make a simple picture a moving story? By making your image both personal and helpful. 

To make an image personal, you need to adjust your post to the social media platform you've chosen. This means that your Pinterest pins should relate to your tweets, but not be the same. Doing so makes you appear both engaged and trustworthy, both essential to keeping the attention of your fans.

Making an image useful, on the other hand, is about providing something of value for your fans. Yet what if you don't know what sort of value _you_ can or should provide?

Try _social listening_ — keep track of everything that people say online about your company and products. For example, if you sell backpacks, you might look at the answers to questions you've posted on Facebook or review posts in a travel user forum. 

Be sure to keep your content _shareworthy,_ as a good story is bound to be retold. People should want to share your content because it's cool, funny or touches them emotionally. Transforming your followers into happy sharers turns them into a salesforce for your company!

Coca-Cola sees its customers as independent storytellers, and encourages them to tell their tales. As part of the company's #BestSummerMoment marketing campaign, customers were asked to share their favorite moments of summer online with the campaign hashtag. 

One poignant image was of a newly married couple sharing a Coke on their honeymoon — an amazing experience now linked positively to the company brand.

Let's now learn how to apply the basics of visual storytelling to different social media channels.

### 4. Get familiar with the many social media platforms and find the right combination for your firm. 

If your company is using more social media sites than just Facebook to deliver its message, congratulations! Companies should use many different platforms to engage with customers, in order to provide product information and share brand values. 

Social media can be a great marketing tool. Let's take a look at some main platforms and how to best use them.

When using YouTube, you should focus on video that is entertaining. Consider the wildly successful Volkswagen advertisement, _The Force,_ or Evian's _Baby and Me_ campaign. Both videos went viral across the globe, with tens of millions of views. 

When it comes to Instagram, you should focus on sharing images and videos that are just 15 seconds long. Remember, however, that 70 percent of all Instagram users are female. Campaigns such as Lululemon's _Every Mat Has a Story to Tell,_ which chronicles the different places you can use a yoga mat, were tailor-made for Instagram. 

Blogging site Tumblr focuses on pictures and allows content to be easily shared. It's great for marketing, as many different demographics, such as millennials and wealthy, avid consumers are drawn to its hip, trendy vibe. Coca-Cola runs a Tumblr blog called _Where Happiness Lives_, the goal of which is to boost engagement among teenagers. 

Slideshare is a social media platform for sharing presentations. While information-heavy, it is also great tool for encouraging follower engagement. US space agency NASA operates a universe channel loaded with videos and documents that explain its work, inspiring people worldwide.

But what about Twitter, Facebook and Pinterest _?_ These popular channels are up next!

### 5. Consider your target demographic when choosing which social media site to use. 

If your company is like 83 percent of European companies and 72 percent of US companies, it probably has a Twitter presence. But have you ever considered using Pinterest to promote your brand? If not, you might want to think about signing up. 

Using Pinterest wisely, that is sharing pins with beautiful photos and videos, can draw traffic to your company's website or online store. A fan can click on an attractive or interesting pin, for example, and is then directed to your website. 

Here's how it works. A Pinterest profile is composed of theme boards, things such as "Behind the scenes," "Our company's charity," "Deals & offers," "Tech ideas" and so on.

Is part of your target audience women with families? Keep in mind that 80 percent of Pinterest users are women and about 50 percent of all users have children. If this demographic is attractive to your business, then Pinterest is for you. 

For example, cosmetics brand Sephora reported that its Pinterest followers buy 15 times more products than do its Facebook fans. 

Twitter's huge user base is attractive to almost every company. This platform began as a small microblogging site, but grew quickly. Now some 460,000 Twitter accounts are opened every day and the company's reach is global. 

To use this platform effectively, it's important to know that people tend to read _their_ Twitter _feed_ and not _your_ Twitter _profile_. Thus you need to make your tweets stand out with relevant hashtags — a keyword preceded by a pound sign, such as #summersale.

Technology giant Intel maintains an extremely successful Twitter account, boasting over 4.34 million followers! One of the company's tricks is posting complex, even geeky statistics using the hashtag, #dothemath. One tweet, for example, stated that 675 million smartphones placed end-to-end would wrap around the earth's equator 1.4 times. 

Twitter and Pinterest are just two of the major social media players. Now let's turn to a site you're probably already familiar with, Facebook.

### 6. Facebook is all about connecting and engaging with fans, a great two-way brand conversation. 

With over one billion users, Facebook is by far the most popular social media platform. And with over 15 million brand pages, there are few companies that _don't_ have a Facebook profile. 

But how can your company use this leading social media platform effectively?

Facebook allows two-way communication between a company and its customers. You can post many different forms of content, such as text, video and images, to your profile. Once a fan has "liked" your profile, all your information is visible on his or her newsfeed. 

Importantly, your fans can chat with your company, by sending a private message, writing on your company wall or by commenting on and liking your posts. In fact, 80 percent of all social media users say that Facebook is their favorite way to connect with a company. 

But ensuring the success of your Facebook presence requires _engagement_. Engagement is the degree to which you interact on Facebook and the actions your posts inspire, as calculated by the number of comments and likes each post garners. 

For instance, you might create engagement through _user-generated content_, like Moleskine did with its "What's in your bag?" campaign. The company asked its Facebook fans to upload pictures of their bags' "inner lives" — and in the process, produced tons of engagement through thousands of likes and fan comments. 

And if you want to get technical, you should know about the _EdgeRank_ algorithm.

On Facebook, you're issued an EdgeRank score that depends on your level of engagement. The higher the score, the more prominently your posts are placed on newsfeeds. So while using an engaging photo isn't essential to every campaign or post, it's key to keep EdgeRank scores in mind — as even your best posts are pointless if nobody sees them.

### 7. Just like all other marketing campaigns, visual storytelling on social media requires strategizing. 

Let's say you're the head of a cosmetics company. Knowing that your target audience is 85 percent female, you choose Pinterest, Instagram and Facebook as your social media channels. 

What's your next step?

Well, before you dig in and start generating stunning visuals for your campaigns, you first need to build a social media strategy. This involves three important steps:

First, you need a _vision_ or a _goal_. For instance, your goals might be to build awareness about a product, while increasing customer engagement, fan and community growth, loyalty, referral traffic and sales. More specifically, maybe you want to boost your fan base by 20 percent and your sales by 5 percent. 

Second, you need to assess your current position by monitoring your social media content and analyzing its impact. Your content needs to be in line with your goals. For example, you should monitor your posts' metrics: date and time sent, content type and other details over an extended period of time to make sure you're staying true to your goals, and do this ideally every three to six months. 

And finally, you should conduct a _cross-check_ on the results of your monitoring and what you learned during your social listening, when you monitor what's said about your company online. For instance, you could focus on questions that are frequently asked about your products. 

Now all that's left to do is consider your _company's personality and voice_. Social media relies on a human touch. So ask yourself, if your company was a person, what kind of person would it be?

In the case of a cosmetics company, your company personality might be that of a playful, design-conscious, young, eco-friendly woman. Once you define your personality, you can adjust your language and tone to match!

> _"Visual storytelling is the confluence of art and science..."_

### 8. Infographics and postcards are two great tools for a visual storytelling marketing campaign. 

So you're ready to create stunning visual content for your brand, but are overwhelmed by the variety of media from which to choose. Here's the inside scoop on some important tools that can really make your campaign successful, like infographics, postcards and e-cards. 

_Infographics_ are a great way to give your customers access to valuable information. An infographic is essentially data in the form of a picture, like a chart or a map. Using an infographic lets you merge important information you want to share with a compelling visual element. 

But to make a strong infographic, you need solid data and trustworthy research. An infographic with flimsy data is sure to fail. 

A company that sells ecologically grown coffee to high-end consumers and specialty roasters, for example, might be interested in creating an infographic that shows the origins of its fair trade coffee — information a customer is likely to be interested in. 

And if hiring a graphic designer falls outside your budget, consider using online tools such as Infogr.am, InfoActive, Easel.ly, Piktochart, Visual.ly or iCharts.

Other visual tools that are essential to telling your story are _postcards_ and _e-cards_ — both of which offer great, simple ways to speak to your customers. 

For instance, maybe you want to communicate your gratitude to your customers. Postcards and e-cards are a great choice of media, as they share inspiring words easily. Just remember to keep your text short! 

Our coffee company might consider designing a collection of cards that capture a fan's love of good coffee, or even a collection with a humorous angle, chronicling what happens when a coffee lover misses her first morning cup. 

If you don't want to build your own cards from scratch, explore premade options at sites like _Cards in the Post_ and _SnapShot Postcard._

Now that you've got the tools you need to create your own campaign, it's time to explore what visual storytelling and real-time marketing means for the future, regardless of whether you're a consumer or marketer.

### 9. Real-time marketing is the next step in the future of advertising and visual storytelling. 

Being in the right spot at the right time is more than luck, it's the key to marketing success. Today the fastest and most dynamic form of marketing is called _real-time marketing_. 

Real-time marketing delivers well-tailored messages directly to a target audience with perfect timing, by using social media platforms and available marketing data. 

For instance, during Super Bowl XLVII in 2013, the stadium in New Orleans experienced a partial power outage. Cookie maker Oreo jumped at the opportunity, tweeting a picture of an Oreo cookie under a spotlight with the tagline, "You can always dunk in the dark." The post was retweeted some 15,000 times and was "liked" by nearly 20,000 fans!

So how can your company harness the power of real-time marketing? Two ways are by increasing customer contact and putting your marketing in line with news and trends. 

For the first way, you'll want to use _social customer relationship management,_ or social CRM. This technique improves upon traditional CRM methods that rely on support hotlines. With social CRM, you can use social media channels like Twitter and visuals like infographics to handle customer requests and complaints as they happen.

But social CRM can also be funny. For instance, take Smart Car's reaction to advertising agency guru Clayton Hove's tweet, "Saw a bird had crapped on a Smart Car. Totaled it." The company responded with an incredible infographic showing how much bird poop would be needed to actually crush one of its cars: that of either 4.5 million pigeons or 360,000 turkeys. 

The best part? Clayton Hove loved it. He retweeted, "Outsmarted by Smart Car. Best. Social media response. Ever."

The second route is to align your marketing with trending topics, that is, connect your ads to current events and popular news. For instance, Mini Cooper once posted during a snowstorm a picture of its new Nemo model (colored like the fish from the Pixar film, _Nemo_ ) with the tagline, "Find your way through the storm."

> _"Great marketing is now a conversation based around communities and passions."_

### 10. Final summary 

The key message in this book:

**Visual storytelling is essential marketing in the digital world. That's because effective visuals are key to grabbing the attention of customers and securing their engagement both online and offline.**

Actionable advice:

**Check the pixel size!**

Before posting an image to a social media site, be sure to check the required pixel size, which varies from platform to platform. For instance, Facebook allows cover photos of 851 x 315 pixels; but for the sharpest image, you'll want to double that amount and let Facebook resize it. Instagram feed photos allow 510 x 510 pixels, and Twitter profile pictures are small at just 81 x 81 pixels. Keep these details in mind when posting and your visuals are sure to impress!

**Suggested** **further** **reading:** ** _Lead with a Story_** **by Paul Smith**

_Lead with a Story_ teaches you how to enhance your skills as a great leader by harnessing the power of storytelling. By taking examples from one of the most successful companies in the world, you'll learn how to craft a great story that motivates people and modifies their behavior.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ekaterina Walter and Jessica Gioglio

Ekaterina Walter is the co-founder and CMO of Branderati and the author of _Think like Zuck_. She was previously the global social media strategist for Intel.

Jessica Gioglio manages social media for Dunkin' Donuts and is the founder of _The SavvyBostonian_, a popular Boston lifestyle blog.

