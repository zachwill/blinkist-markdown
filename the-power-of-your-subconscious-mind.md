---
id: 5a5337b9b238e10007298942
slug: the-power-of-your-subconscious-mind-en
published_date: 2018-01-10T00:00:00.000+00:00
author: Joseph Murphy (Ian McMahan revised)
title: The Power of Your Subconscious Mind
subtitle: Unlock Your Master Key to Success
main_color: 8A307D
text_color: 4A0F41
---

# The Power of Your Subconscious Mind

_Unlock Your Master Key to Success_

**Joseph Murphy (Ian McMahan revised)**

_The Power of the Subconscious Mind_ (1963) has helped millions of readers around the world harness their subconscious and find true happiness in the process. These blinks share inspiring true stories and effective techniques that will positively influence your career, love life and overall health.

---
### 1. What’s in it for me? Tap into the power of your subconscious. 

Imagine an iceberg floating along in the Arctic. While you see parts of it above the surface of the water, most of it is hidden beneath. The same can be said of your mind: the part we are aware of — the conscious mind — is visible and known to us, while the other, and at least equally large, part constitutes our subconscious. So how do we tap into such a vast and seemingly inaccessible part of our minds?

That is exactly what we will look at in these blinks of a classic book from the early 1960s. You will discover how to unleash the potential that is hidden in your subconscious to tackle a wide array of issues and problems, coming out at the other end feeling both wiser and happier.

You'll also find out

  * how a doctor used his subconscious to reduce mortality rates;

  * why you need to visualize your house being sold; and

  * why you must learn to love the boss you don't like.

### 2. The subconscious mind is prone to suggestion, which you can use to your advantage. 

Do you remember when you learned to ride a bike? It likely required intense attention and focus from your conscious mind. But after a while, your subconscious probably started to get the hang of things and, pretty soon, riding a bike was a natural, almost automatic task.

This is a great example of conscious to unconscious learning, an incredible tool at your disposal. Using it only requires harnessing the power of your subconscious mind through the repetition of positive thoughts.

Just take Enrico Caruso, the Italian opera tenor. In the late nineteenth century, he performed at famous opera houses throughout Europe and the United States. However, he used to suffer throat spasms and would find himself drenched in sweat moments before he was supposed to sing.

Why?

Well, his mind was filled with negative thoughts; he always imagined the crowd laughing him off the stage or heckling a poor performance. Nonetheless, he got over this stage fright by telling his "small me," or conscious mind, to stop interfering with his "big me," or subconscious mind. By repeating this meditative practice, he eventually gave his subconscious mind the ability to ignore his fears, freeing up more energy for his powerful voice box.

In other words, the subconscious mind is tremendously powerful. In fact, it can absorb and manifest any idea that you suggest to it.

For instance, psychologists have done a number of experiments in which a seasoned hypnotist puts his students into a hypnotic state before suggesting to them that they are cats. The students then go on to act the part with total authenticity; their subconscious minds simply accept whatever their conscious minds believe to be true.

Or consider the Scottish surgeon, Dr. James Esdaille. Between the years 1843 and 1846, he performed some 400 operations, including amputations, well before the development of anesthesia. The mortality rate for his procedures was incredibly low, at just two or three percent, and it was all thanks to his technique of hypnotically suggesting to his patients that they would not contract an infection. This hypnotic tool was sufficient to prompt a response on the part of their subconscious minds and, in turn, their bodies.

> _". . . everything, which you find in your world of expression, has been created by you in the inner world of your mind consciously or unconsciously."_

### 3. You can use the power of positive thinking and visualization to realize your dreams. 

Back in the eighteenth century, priests would heal the sick by convincing them that God would make them well. Incredibly, this approach often worked. But it wasn't the work of some mysterious deity; the subconscious mind was the real healer.

This seemingly supernatural effect can be explained by the fact that positive thoughts give your subconscious the power to heal diseases. For example, after one of the author's relatives developed tuberculosis, the man's son became determined to heal him. He told his father that he bought a cross from a monk who recently visited a healing shrine in Europe — but, in reality, it was just a piece of wood he picked up off the sidewalk.

He said that simply touching the cross had already healed countless people. So, his father gripped the object, praying with it in his hand as he fell asleep. The next morning, he was completely healed.

In fact, the father was so convinced that he was healed by the cross that nobody ever told him the truth; breaking this illusion would likely have caused the disease to resurface.

In a similar way, simply imagining something you desire can help make it a reality. A good example here comes from the author's teachings at the _Church of Divine Science_, which became a weekly radio show with millions of listeners.

In one segment on this show, the author discussed the _mental movie method_ while advising people on the process of selling their homes. In this method, visualization is used to create and hold a mental image until the subconscious mind makes it real.

For a home seller in the example above, the first step was to build confidence that she had set a reasonable price. Then, while in a sleepy state, she would envision celebrating the day the sale finally closed. As she dozed off, keeping this image in her head, her subconscious mind would connect her with the buyer.

This process worked so well that a number of listeners sent in letters, thanking the author for helping them sell their homes.

### 4. Visualization and passion can help you achieve tremendous results. 

You now know how visualization can help you sell your house — but that's just the beginning. This powerful tool can do much more. It can even help attract money and, for it to do so, you need only picture your desired result and rely on your subconscious to make it happen.

Just take the story of an Australian boy who dreamed of becoming a doctor and a surgeon, despite lacking the money he needed to start on the path toward this dream. Every night for months, before he fell asleep, he would imagine a medical diploma hanging on his wall with his name written on it.

Eventually, a doctor who knew the boy saw potential in him and trained him to sterilize instruments, give injections and eventually paid for his medical school tuition.

Another way your subconscious can help you is by overcoming envy, a feeling of inferiority that can obstruct your path to wealth. For example, seeing another person cash a huge check might make you feel envious, but you can easily defeat this response.

The solution is to wish greater wealth upon others, which will signal to your subconscious that you deserve the same for yourself. If you instead remain envious, you'll only preclude your own potential affluence.

And finally, you can reap tremendous benefits by pairing visualization with _passion_. For instance, the author knew a young pharmacist who was in love with his work and often dreamed of having his own pharmacy.

He focused his mind on conjuring images of himself distributing prescriptions and, one day, began a new job at a major chain store, where he worked with the passion he had always imagined. Eventually, he became the manager and, after four years, he had saved enough to open his own pharmacy. Through passion and visualization, he rose through the industry and was able to realize his ambitions.

### 5. Use your subconscious to guide your choices and attract the kind of partner you want. 

Did you know that humans spend a full third of our lives asleep?

But that's not to say this is all lost time. In fact, lots of things happen while you sleep; your body restores its energy, heals faster and digests properly. With your physical body so active during rest, you better believe that your subconscious is working through the night as well, striving to protect you.

Incredibly, these intuitive powers of your subconscious can help guide you. Just take one of the author's _Church of Divine Science_ radio show listeners, a woman from Los Angeles. She was offered a job in New York City at double her current salary but couldn't decide whether or not to take it. In the end, she sought guidance from her subconscious, trusting that the answer would come to her in her sleep.

She practiced meditation as she dozed off and the next morning had a strong feeling that she shouldn't take the job. Months later, her choice was validated when the company filed for bankruptcy. Her subconscious mind's intuition had guided her to the correct decision.

And that's not all your subconscious can do for you in your sleep; it might even be able to find you your dream partner by simply focusing on the qualities that you desire in a better half.

For instance, the author knew a teacher who had three ex-husbands, all of whom were passive and weak, despite her attraction to the opposite qualities. What was happening?

Her masculine, dominant personality had subconsciously attracted submissive partners. However, by mentally constructing her ideal husband every night, she succeeded in breaking the pattern. She took a job as a receptionist in a doctor's office and immediately knew that the physician was the healthy, successful man she had been imagining. To make a long story short, they got married and had a happy life together.

### 6. You can choose to be happy, and lose the negative thoughts that stand in your way. 

Just about everyone has known the glee you can experience upon finding a great deal at a flea market or a $20 bill on the street. But we also know that such happiness is fleeting. Luckily, there are ways to get more happiness into your life. It begins by changing your thoughts.

In order to be happy, you need to _choose_ happiness. For example, a salesman once asked the author for advice because, despite consistently outperforming his colleagues, he had never received recognition for his work.

He blamed this discrepancy on a belief that the sales manager disliked him and therefore treated him poorly. His mind was full of hostility and anger toward his boss and, eventually, these thoughts began to obstruct his progress.

However, he realized that he had the power to choose happiness instead by switching to a positive mind-set. He began wishing health and success upon his boss and practiced visualizing his manager congratulating him, going in for a friendly handshake and shooting him a beaming smile.

One day, his boss called him up and promoted him to sales manager, giving him a huge raise in the process.

The takeaway here is that negative thoughts are highly detrimental and can even prevent you from attaining peace of mind. Just take one of the author's associates, who worked every day until one in the morning, neglecting his wife and two sons and causing himself to suffer from high blood pressure.

Why was he such a workaholic?

Well, it wasn't immediately clear. But when he dug a little deeper, he found that he felt remorse for not doing right by his deceased brother, who had passed away many years earlier. Because he was racked by such guilt, he was punishing himself by working ceaselessly and preventing himself from seeing his family.

To finally end this suffering and heal himself, he first had to forgive himself. Now, he makes plenty of time for his family and works regular hours.

### 7. Replace fears with positive thoughts to overcome obstacles and stay young. 

When you were a kid, were you scared of monsters hiding under the bed?

If you were, as many children are, you probably remember that feeling of relief when your mom turned on the light, revealing that everything was safe. The truth is, all fears are built upon such false thoughts and terror can be eased by countering them.

Some of the most common fears are of failure and powerlessness. Just take the story of Mr. Jones, who couldn't control his impulse to drink excessively. He would at times remain drunk for two weeks on end.

His constant failure to quit drinking had left him feeling powerless, and the fear that he would fail again made him give up trying. Finally, only after losing his family did he decide to face these fears. He put himself into a relaxed state and imagined his daughter praising him for getting sober. Through this process, he was able to gradually give up drinking and eventually reunite with his family.

Another common fear is that of aging, which can also be overcome by modifying your thought processes. After all, you only age when you stop dreaming and learning new skills, which means staying young is really just about staying active.

Consider an executive who lived close to the author. He retired at the age of 65 after spending the vast majority of his life studying and working. Rather than fearing his later years, he saw retirement as an opportunity to pursue the things he had always wanted to; he took photography classes, travelled the world taking photos and eventually became a lecturer on the subject.

His simple enthusiasm for this hobby drove him to continue learning, keeping him young at heart and prompting his mind and body to follow suit.

Or take the author's father who, at 65, learned French and became a specialist on the language by the age of 70. After that, he studied Gaelic, teaching the language until his death at the ripe age of 99.

> _"If you have a keen desire to free yourself from any destructive habit, you are 51 percent healed already."_

### 8. Final summary 

The key message in this book:

**Your subconscious mind is constantly at work, and you can harness its power for your own benefit. This dormant force can help you tackle any issue in your life, improving the way you feel both emotionally and physically. Ultimately, this entire process hinges on visualizing success and banishing negative thoughts.**

Actionable advice:

**Use a simple technique for overcoming fear.**

If you struggle with a fear, no matter what it is, try a simple approach to overcome it. Take five or ten minutes, three times a day, to sit down and imagine doing the thing you're afraid of. Vividly picture the experience as a joyous one, with people cheering you on. By mentally practicing overcoming your fear in this way, the idea will take root in your subconscious mind, even when you're not imagining it, and eventually the fear will disappear.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Success Through a Positive Mental Attitude_** **by Napoleon Hill**

_Success Through a Positive Mental Attitude_ (1960) shows how to achieve the life of your dreams by developing a positive mental attitude. Near the turn of the twentieth century, at the behest of Andrew Carnegie — one of the wealthiest men of his time — Hill interviewed hundreds of famous and successful people in an attempt to uncover the secret to success. This book is one of the outcomes of his findings.
---

### Joseph Murphy (Ian McMahan revised)

Dr. Joseph Murphy began his spiritual journey studying Eastern religions and the I-Ching. Over the course of his career, he has written a number of books on spiritual topics, including _The Miracles of Your Mind_.

