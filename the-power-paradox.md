---
id: 5a43a08fb238e10007128c34
slug: the-power-paradox-en
published_date: 2018-01-04T00:00:00.000+00:00
author: Dacher Keltner
title: The Power Paradox
subtitle: How We Gain and Lose Influence
main_color: 39B79E
text_color: 16705E
---

# The Power Paradox

_How We Gain and Lose Influence_

**Dacher Keltner**

_The Power Paradox_ (2016) draws on a wealth of data from numerous social-science studies over the past 20 years to explore the dynamics of power. Dr. Dacher Keltner gets to the bottom of what power means in everyday life, discusses why so many people lose and abuse their power and explains how it can be used to make the world a better place.

---
### 1. What’s in it for me? Explore the dynamics of power and how to harness it in your life. 

The word "power" gets thrown around a lot. But, despite this bandying about, its meaning is often unclear. Sometimes it denotes something sinister and Machiavellian; sometimes, something enviable and awe-inspiring.

It's true that power can corrupt even the best of us — but this shouldn't make us denounce or shun it.

Power can and should be a force for good. To attain it, we must act compassionately and selflessly toward our peers — otherwise they won't follow our influence — and we mustn't let our power go to our head, for then we lose the ability to empathize with others. Once this ability is lost, we run the risk of losing our power and thus the ability to make others' lives better.

These blinks will help you understand the nature of power and how to use it for the good of all.

In these blinks, you'll also learn

  * how cancer patients empower themselves via storytelling;

  * why writing down what you're grateful for is important for self-empowerment; and

  * how NBA players use the power of encouragement to raise their teams to new heights.

### 2. Power is about changing lives and it plays a key role in everyday relationships and interactions. 

What does it mean to have power? Is it an elusive thing that only belongs to presidents, politicians and celebrities?

The truth is, power is used by everyday people in ordinary interactions — in motivating an employee to do a good job, for instance, or getting your children to eat their vegetables and do their homework.

Whenever someone uses influence to make a difference in the world, that's large-scale power in action.

The exploits of Thomas Clarkson are a great example of power in action. In 1785, Clarkson was a student at Cambridge University when he won a writing contest with an essay that detailed the horrors of the slave trade. At the time, most European economies relied heavily on the brutal slave trade, and millions were forcibly transported for their labor.

Clarkson's prize-winning essay was just the start. He was soon writing more pamphlets and letters on the subject and was convincing many people to boycott the sugar being harvested by slaves in British territories. Eventually, these protests were powerful enough that Great Britain's Parliament outlawed slavery.

Clarkson used power to achieve a huge change, but power also exists within relationships and everyday interactions. For example, the power dynamic between two siblings can also change a person's life.

During adolescence, an older sibling will often be stronger and smarter, with more education; in short, they have power over their younger sibling. Enjoying this power in early life often pushes older siblings to seek out positions power as they grow older, it also leads them to be generally more traditional and conservative in outlook. On the other hand, younger siblings, who lack this experience of power, will become more cooperative and innovative as adults.

Because power is so ubiquitous, scientists have long studied how it is used. In one study, known as "leaderless group discussion paradigm" experiment psychologists looked into the power dynamics of everyday life. The experiment involved the observation of a group of strangers who were asked to cooperate on solving a problem without any assigned roles or guidance being offered. Fascinatingly, the researchers found that some participants naturally assumed power by being the first to offer their opinions or by encouraging others.

### 3. Those who improve the lives of others get power, and those who hold people back lose it. 

There are a lot of opinions on social media these days, but the multitude of Instagram users are all actually rather similar: each is eager to win a good reputation and gain influence within their social circle.

And how can they gain this influence? Research shows that influence is determined by a person's ability to improve the lives of others.

The author made this discovery 20 years ago, when conducting an experiment at the University of Wisconsin in Madison.

Students were asked to fill out a survey that rated how much influence, or power, other students had over them. These surveys were filled out a second time after four months, and a third time after nine months. The students were also asked to rate their own levels of enthusiasm, kindness, focus, calmness and openness — categories that are known as "the Big Five Social Tendencies."

The data revealed that the ability to show enthusiasm and express kindness toward others were the biggest factors in determining someone's influence and social power. In other words, those who benefit their peers are rewarded with power.

It's not all that different in the Arctic, where Inuit tribes bestow respect and power on those who share food. Meat, in particular, brings the biggest benefits, and men who provide the most food have the best chance of landing a mate. For women, generosity to their peers results in help with childcare.

In all these situations, research shows that those who hold the group back by not helping have less power.

Research at an east-coast college followed a team of rowers. The team member with the least amount of influence was, unsurprisingly, the one team member who was frequently late to training and performed poorly.

In the next blink, we'll look at how people with power manage the tricky business of holding on to it.

### 4. Power can be maintained through generosity and gratitude, both of which can also help us heal. 

As Peter Parker learned when he became Spider-Man, great power comes with great responsibility. Indeed, though it's difficult to gain influence and power, it's awfully easy to lose them. That is, unless you take the following advice to heart.

Continuing to be generous and encouraging with others is the first key to maintaining power.

In 2008, the author and his UC Berkeley research team spent seven months observing NBA players. In that time, they identified 25 different kinds of encouraging gestures involving physical contact, including high fives, chest bumps and embraces. Statistics show that the more these physical gestures were used on a teammate, the more appreciated and empowered that teammate would feel, thus improving his performance and making him happy to support the teammate who'd initiated the gesture.

Other ways to maintain power are to use gratitude and tell stories that bring people together.

In a 2003 study, participants were asked to record five things that made them feel grateful, every day, for nine weeks. Afterward, the results showed that the most grateful participants were also the healthiest with the least amount of stress. These participants also had the best social standing, with high levels of influence and power.

Research conducted over several decades examined participants experiencing trauma, such as students with severe stress during exams, couples going through divorce and patients diagnosed with life-threatening disease.

Remarkably, participants who used storytelling skills to describe their intense experiences with great detail saw a reduction in stress and sadness. The grades of the students improved and T-cell levels of cancer patients increased. Meanwhile, those who expressed themselves in flat, emotionless stories remained the same.

The results suggest that the participants who told captivating stories engaged the interest of others, which tightened the social bond between them and their listeners, thus reducing the overall stress of the social group.

In the next blink, we'll have a look at the negative aspects of power.

> _"Stay focused on other people. Prioritize others' interests as much as your own… take delight in the delights of others, as they make a difference in the world."_

### 5. Acquiring power can lead to impulsiveness and a lack of empathy, which we try to justify with stories of exceptionalism. 

What kind of people come to mind when you think of the corrupting effects of power? Politicians, perhaps? Or greedy bankers? Well, the truth is, _all_ of us are susceptible to the dangers of power.

In particular, power can reduce our empathy and encourage our self-serving nature.

One study asked participants to recall times when they felt powerful or powerless; as they were reliving these moments of increased or reduced power, they were shown a video of someone's hand squeezing a rubber ball.

Those who were experiencing powerlessness were shown to be more likely to mimic the squeezing of the ball, while those who were feeling powerful showed no evidence of this mimicry. The researchers believe that the powerless were empathizing as a way to create sympathy and trust. The powerful, on the other hand, simply didn't need to be empathetic.

In another study conducted by a Dutch company, 1,275 participants filled out surveys that asked them to mark their position within their organization on a scale of 1 to 100, with 100 being the company's president or CEO. They were then asked if they'd ever had a sexual affair or considered cheating on their partner. Results showed that 26.3 percent of the participants admitted to cheating, and a majority of these people marked their position of power as high.

As well as being more self-serving, those with power will sometimes rationalize their disrespectful behavior with questionable excuses.

During a study, the author displayed a graph charting the past 30 years of average US family incomes. He then asked participants to offer their opinion on why the wealth has been unevenly distributed to the top 10 percent.

Upper-class participants believed the top ten percent gained their wealth through ability, intelligence and hard work. The poorer participants, in contrast, believed the top ten percent had enjoyed privileged access to resources like quality education and the stability afforded by growing up in an affluent neighborhood.

When it comes to social injustice, the wealthy have ways of rationalizing their situation to feel less guilt.

### 6. The powerless suffer more, which lessens their ability to make a difference in the world. 

There's a good chance that, at one point or another, you've felt stressed out about having to give a presentation. But did you know this is due to powerlessness? During the Trier Social Stress Task (TSST), participants were asked to give a speech on difficult topics such as euthanasia, with only ten minutes of preparation time.

Data shows that an audience of strangers in a situation like this, with their ability to bestow either approval or disapproval, will take on the role of power. As a result, participants instinctively respond with a fight-or-flight reaction: heart rate, blood pressure and stress levels skyrocket, and the immune system crashes.

Unfortunately, the stress that comes from a position of powerlessness is experienced daily by the disadvantaged in society. It's clear why:

Police, occupying a traditional role of power, have been shown to be more likely to search and physically abuse African-American and Latino men in random traffic stops.

Meanwhile, African-Americans have fewer housing options and receive more severe sentencing by the courts. Furthermore, it's been shown that employers are more harshly critical of resumes from African-American job applicants. Naturally, all of this powerlessness gives rise to aggression, stress and ill health.

Neuroscientists have conducted long-term studies that revealed the physiological effects of powerlessness.

When children of low-income families reach 11 years of age, their brains were five percent less developed in areas related to language, rationality and stress control. The researchers noted the negative impact this would have on learning, coping with threats or challenges and making significant contributions to society.

This study concluded that those raised in low-income families are likelier to have stress-related cardiovascular disorders and are also at a 20- to 40-percent higher risk of dying from common diseases.

The issues surrounding the powerful and powerless aren't limited to courtrooms and boardrooms. Understanding the extent of these dynamics can help us battle racism, sexism, homophobia and other inequalities in our society.

### 7. Final summary 

The key message in this book:

**Regardless of our differences, we must learn to respect one another wholeheartedly. By listening with intent, acknowledging other's achievements and practicing gratitude, we can empower others to contribute to the world.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Invisible Influence_** **by Jonah Berger**

_Invisible Influence_ (2016) is all about the effect other people have on the clothes you wear, the music you like and the decisions you make. These blinks explain how your actions, thoughts and preferences are shaped by others, and how by understanding this process, you can have greater control over these influences.
---

### Dacher Keltner

Dacher Keltner is a professor of psychology at the University of California, Berkeley. Over the course of his career, he has published over 190 articles in publications such as the _New York Times_ and the _Wall Street Journal_. He is also the author of multiple best-selling books, including _Born to Be Good: The Science of a Meaningful Life_ and _The Compassionate Instinct_.

