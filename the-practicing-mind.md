---
id: 56b8e3bed119f9000700008f
slug: the-practicing-mind-en
published_date: 2016-02-11T00:00:00.000+00:00
author: Thomas M. Sterner
title: The Practicing Mind
subtitle: Developing Focus and Discipline in Your Life
main_color: FC8E47
text_color: A15320
---

# The Practicing Mind

_Developing Focus and Discipline in Your Life_

**Thomas M. Sterner**

_The Practicing Mind_ (2005) offers a smart and simple solution to handling anxiety when working toward our goals. These blinks show the impact our expectations have on our productivity, and reveals steps you can take to live in the present, enjoy your progress and really get things done.

---
### 1. What’s in it for me? Increase your satisfaction with practice and achievable goals! 

Most of us have felt performance anxiety at some point in our lives. Whether at school, at work, at the gym or in our leisure activities, this anxiety seems to find ways to enter our minds, often making it hard to get things done at all. Where does it come from? And how can we tackle it?

We often set our goals too high, believing we can achieve them. And here lies part of the problem because, as soon as we reach these higher goals, we set new ones that are even higher. As they become harder and harder to reach, we also deny ourselves the gratification of achievement. So let's look at a different approach to achieving the things we want in life with a practicing mind that constantly improves without the anxiety of performance.

In these blinks, you'll find out

  * how cleaning the garage bit by bit is a successful strategy for getting things done;

  * what the _Do, Observe, Correct_ technique is; and

  * why focusing on the process instead of the end result makes us feel like winners.

### 2. Because we raise our expectations of ourselves, we never feel satisfied. 

Sometimes it seems that, no matter how much you practice something, there's always going to be someone out there who does it a little bit better than you. It's frustrating, to be sure. And it can shape our attitude toward several aspects of our lives. But why do we feel frustrated by this in the first place?

If there's one flaw that all humans have in common, it's striving for ideals that simply aren't attainable. We've all got a picture in our minds of what we believe is a perfect life, making our real lives seem inadequate by comparison. Some of us want a better job, some of us want more friends, and some of us want to change our appearance. 

You probably aren't surprised to hear that these high expectations are fed by mass media and marketing. Think of all the polished, perfect and wealthy people that are featured in magazines and advertisements. Nearly all products marketed to us today are made desirable by convincing us that our lives just won't be as good without them. 

Of course, high expectations aren't all bad. We can use images of a better life to inspire us to work hard and achieve more. Unfortunately, we tend to use them as nothing more than objects of comparison. We compare our performance to our colleagues' performance in the workplace. When we look in the mirror, we're keen to compare our appearances with those we see on the street. 

And there's yet another problem with the way we engage with our ideals. Rather than working toward one goal and feeling satisfied when we achieve it, we _raise_ our expectations as we achieve more. This means that our ideals move further away from us, and we're stuck constantly pushing ourselves to reach goals that we don't value once we achieve them. 

In short, the feeling that we're never good enough is a source of constant anxiety for us all. But what if we don't want to feel anxious all the time? Well, then it's time to make some changes. Find out more in the following blinks.

### 3. Cut out anxiety by focusing on your progress in the present. 

Why is it that, when an important deadline is looming, we seem to get better and better at procrastinating? Feeling your focus waver when you know you've got serious work to do can be quite stressful. 

This is often because we're consumed with the thought of our end goal, which seems to be far away no matter how hard we work. This, in turn, makes us feel discouraged and damages our productivity. Rather than fixating on future consequences, we need to focus on the here and now to do our best. 

This is due to a simple change of perspective. Thinking about our goal can fill us with fear and anxiety, which distracts us from the real task at hand. But by simply keeping our thoughts on the present moment, our anxiety will drop significantly. Putting our end goal aside for a moment and concentrating on the _process_ we'll go through to achieve it, we'll see ourselves making progress and feel more motivated than ever. 

This isn't to say that we shouldn't focus on our goals. Goals are vital, but they are of no use to us when we use them as an _indicator_ of our progress. Again, this just discourages us. Our goals are better used as a rudder to steer us in the right direction. 

But, in order to let ourselves be steered, we'll also need to ensure we respond sensibly to problems. Rather than letting our emotions color our reactions, we need to take a step back and objectively assess what is and isn't working in that moment. Once we've done this, we can adjust our next actions accordingly. 

Imagine yourself at a bowling alley: you don't bowl a strike in the first round. How do you react? Instead of thinking "Now the best I can hope to achieve is only nine out of ten," evaluate your actions and try to improve on mistakes you made on the first try.

> _"The practicing mind puts you in control of even the most difficult situations and allows you to work with less effort and negative emotion at any activity."_

### 4. Simplify your tasks and take your time to make your goals achievable. 

If high expectations are making us unhappy and unproductive, should we lower them? Actually, we're free to dream up ambitious goals to our heart's content, as long as we can break them down into smaller objectives. This makes any large goal achievable. 

Anyone that's ever worked on a large, long-term project knows the sheer frustration of working hard while your goal seems farther away than ever. To avoid this, set yourself smaller goals that lead you on a journey to your overarching goal. Keep these tasks simple and you'll find yourself flying through them. 

Cleaning the garage, for example, is a big task that most people tend to avoid. But if you tell yourself that your job right now is just to clean the shelves in one area, you'll have created a goal that's easy to complete and easy to start on. Allow yourself to feel satisfied once completing your first small task, then move on to create the next one. 

Even these simple tasks don't need to be completed in one burst. If you think something will take a little longer, set a time limit for how much work you put into that task on a given day. Cleaning the garage shelves might take longer than you thought, so break it up into, e.g., 45 minutes per day. You'll not only have easier tasks, but you'll also be working toward your final goal every single day!

You might be tempted to work faster to get your tasks completed in less than the allotted time. But this is something to avoid. Though we often think that it's better to do unpleasant tasks as quickly as possible, it is actually rather counterproductive. 

Working at a slower pace allows you to stay in the present, rather than rushing to start working on something else. Rushing creates stress, and stress will only make you less effective. So take your time and allow yourself to complete your tasks thoroughly and efficiently.

### 5. Use the Do, Observe, Correct technique when your productivity wavers. 

We know _why_ we procrastinate when we're under pressure. But what can we do to combat it? The author has developed a three-step technique that can help you maintain calm and focus in any situation. It's called _Do, Observe, Correct_ and it's all about learning to monitor and respond to your own behavior. 

Say you're studying for a big exam and notice that worrying about your results is making it hard to study: apply the technique! You've already covered the _do_ step by noticing that you're straying from the task at hand. 

Now it's time to take a step back and _observe_ the behavior you'd like to change. In this case, it's your worrying. In order to observe effectively, try to not get too emotionally involved or judge yourself for your reactions. Rather, look at your actions as an uninvolved spectator. 

This will allow you to see how your emotions are making your situation seem worse than it really is, and that, by reining them in, you'll be better able to tackle your challenges. Finally, it's up to you to _correct_ the situation by freeing yourself from those negative, fearful emotions as much as you can. 

Initially, it might be a little difficult to do the _do, observe, correct_ technique when you're in the middle of a crisis. But the more you practice it, the stronger your self-observational skills will become. Soon enough, you'll notice yourself applying this technique automatically in any given situation. This will allow you to keep your mind on the present and accomplish the tasks that will lead you to your bigger goals.

### 6. Final summary 

The key message in this book:

**Ambitious aims and high expectations should inspire us. And yet, they become a source of anxiety when we let them distract us from the task at hand. By learning to remain in the present moment, break up our workload and take things slowly, we won't just increase our productivity: we'll also feel more fulfilled as we make progress toward our goals at our own pace.**

Actionable advice:

**It's all about the here and now!**

If you have a task to finish or a skill to master, don't think about the goal, this will only distract you. Tell yourself to work on one smaller task just for a limited amount of time and don't think of anything else during that time. If you manage to fully concentrate on what you're doing in that very moment, you'll notice that you'll get even unpleasant tasks done easily.

**Suggested** **further** **reading:** ** _The Willpower Instinct_** **by Kelly McGonigal**

_The Willpower Instinct_ introduces the latest insights into willpower from different scientific fields, such as psychology, neuroscience, economics and medicine. While considering the limits of self-control, it also gives practical advice on how we can overcome bad habits, avoid procrastination, stay focused and become more resilient to stress. To find these blinks, press "I'm done" at the bottom of the screen.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Thomas M. Sterner

Thomas M. Sterner studied Eastern and Western philosophy and modern sports psychology. Working as the chief concert technician for a major performing arts center, he prepared and maintained the venue's concert grand piano for hundreds of world-renowned musicians. He also produced a radio show about the practicing mind and continues to teach his techniques to businesspeople and at sports clinics.

