---
id: 509cdc6ce4b0a13d41357e10
slug: the-presentation-secrets-of-steve-jobs-en
published_date: 2013-01-13T00:00:00.000+00:00
author: Carmine Gallo
title: The Presentation Secrets of Steve Jobs
subtitle: None
main_color: 6FA5CE
text_color: 2E4B61
---

# The Presentation Secrets of Steve Jobs

_None_

**Carmine Gallo**

_The Presentation Secrets of Steve Jobs_ (2009) __ explains how any presenter can be as convincing and inspiring as the legendary Steve Jobs. From planning to rehearsal and delivery, Carmine Gallo details the anatomy of a great presentation.

---
### 1. To make your presentation successful, plan your storyline and your key message meticulously. 

The audience is captured by the story you tell, not the slides it's told on.

Forget your computer and draft the presentation storyline on paper first. Only when you are satisfied with the narrative should you move to the slides.

Nancy Duarte, the writer and graphic designer behind Al Gore's blockbuster global warming documentary _An Inconvenient Truth_, says presenters should use twice as much time planning their presentation on paper as they do creating the actual slides.

Start your planning by deciding on one _key message_ you want to leave your audience with, and make that the headline of your presentation. It should be like a Twitter message: short, memorable and attention-grabbing.

If you are launching a new product or service, your key message should describe how it will benefit your customers. Repeat your key message multiple times during the presentation to make it stick.

If you are holding a big presentation, the audience and press might well be searching for the right words to describe what they saw afterwards. With the key message, you can give them a memorable phrase of your choosing.

For example, when Steve Jobs unveiled the iPhone, he gave the media a ready-to-use headline: "Today, Apple reinvents the phone."

**To make your presentation successful, plan your storyline and your key message meticulously.**

### 2. To make your presentation great, you must practice relentlessly. 

As in all things, practice makes perfect.

Relentless practice can make anyone into a "naturally charismatic" presenter. Steve Jobs practiced his presentations for hours, even days, to make everything look effortless and smooth.

Even Winston Churchill's "extemporaneous, off-the-cuff" speeches were, in fact, rehearsed thoroughly.

Rehearse your presentation to such an extent that you do not need notes. In cases where you absolutely must use notes like for step-by-step demonstrations, have at most three to four short bullet points with keywords per slide. Place the notes where you can glance at them inconspicuously or even better, mentally connect the keywords with an image on the slide and use that image as the note itself.

To aid in your rehearsal, consider using a video camera to record yourself as you practice. Review the material to see where you stumble, seem nervous or where your voice trails off, and rehearse those parts especially. To gain a valuable second opinion, ask a friend to watch the video with you.

**To make your presentation great, you must practice relentlessly.**

### 3. Don’t let the unexpected ruin your presentation; be prepared for trouble. 

Even the most seasoned presenters run into trouble occasionally: a demonstration might not work, your computer could crash or a slide might be skipped.

If your presentation does hit a glitch, don't get flustered. Unless the problem is highly obvious, don't call attention to it or apologize, just keep moving. If you can't ignore the problem, just laugh it off and move on. Audiences are eager to forgive minor glitches so you can continue with the story, but only if you don't collapse into a sobbing wreck at the first sign of trouble.

Tough questions asked during your presentation are also a potential stumbling block that can be avoided.

Use the _bucket method_ to prepare for questions, and you won't be caught off guard as easily. In the bucket method, first identify common questions you can expect. Next, categorize the questions based on their topic into "buckets" and draft a generic response to each bucket. Make the response broad enough to encompass all the different formulations of questions in that bucket. During the presentation, if a question contains a key trigger word, you can then confidently give the prepared answer for the relevant bucket.

For example, before Hillary Clinton became the US Secretary of State, she held a press conference where she expected journalists to ask about her husband's international foundation and possible conflicts of interest.

Her prepared answer would have suited many possible questions on the topic: "I am very proud to be the president-elect's nominee for Secretary of State, and I am very proud of what my husband and the Clinton foundation... have accomplished as well."

**Don't let the unexpected ruin your presentation; be prepared for trouble.**

### 4. To get your audience’s attention, tell them how you will solve their problems. 

Whether it's a presentation, press release or an advertisement, every communiqué you craft should always answer the one key question every recipient is thinking: "Why should I care about this?"

They should care because you are giving them a solution to their problems.

In order to offer a solution, you must first introduce the problem, _the villain_ of your story: Describe a situation where people are frustrated by the lack of a product like yours (or by a lackluster competing product). Use tangible details and really build the pain in your audience's mind.

Now it is time for your product, _the hero_, to solve the problem and slay the villain. In plain English, without jargon or buzzwords, explain how your product solves the audience's problem. This should be the one main thing your audience will remember from your presentation, so mention it at least twice.

When Steve Jobs introduced the iPod, he first described the audience's problem, listing the various existing expensive and cumbersome ways of listening to music on the move, like the famously bulky portable CD-player. Then he revealed the hero: the iPod, which for the first time allowed people to hold their entire music libraries in their pockets.

What you are truly selling is the promise of a better life, free of the problems you have vividly painted.

This will also help you inject _passion_ into your presentation. Steve Jobs was passionate about the problems his products could solve and passion is the one quality all inspiring communicators have in common.

**To get your audience's attention, tell them how you will solve their problems.**

### 5. Don’t overwhelm your audience; simplify your slides and language. 

The brain is fundamentally a lazy organ, so you need to make things easy for your audience.

Make your slides as simple as possible, introducing only one topic per slide. Remember that you want people to pay attention to what you are saying, not trying to read what is written. Hence avoid using bullet points or long sentences.

Most presenters like cramming their slides as full as possible, not understanding that extraneous information impedes rather than helps learning. In contrast, Steve Jobs often used Zen-like slides with only a single image or word displayed.

You too should build visually engaging and aesthetic slides, preferring simple images over boring text. Associating a picture with verbal information drastically increases people's ability to recall that information.

Apply the same principle of simplicity to your language as well: speak simply and use plain English, avoiding industry jargon or unintelligible buzzwords. Remember, you are speaking for the benefit of the audience, not to make yourself appear smart.

Finally, employ rhetorical devices like analogies and metaphors to really evoke strong images in your audience's heads. 

Steve Jobs loved to compare new products to well-known ones to really drive home the message, for example, "The iPod Shuffle is smaller and lighter than a pack of gum."

**Don't overwhelm your audience; simplify your slides and language.**

### 6. Use data sparingly and make it easy for the audience to comprehend. 

Data can be useful to back up the key theme of your presentation, but nothing puts an audience to sleep faster than an overload of numbers. To avoid this, you should be highly selective with what data you use.

Simplify your data with analogies and metaphors. This is especially important for big numbers, which can be hard to comprehend on their own.

For example, when IBM launched a superfast supercomputer, "The Roadrunner," they did not merely describe its speed as, "one petaflop per second." Instead, they said you would need a stack of laptops 1.5 miles high to match the Roadrunner's speed. This gave the audience a far more tangible metaphor.

Make data resonate with the audience by making it _specific_, _contextual_ and _relevant_ to the audience.

When Steve Jobs introduced the iPod, he did not merely explain it had 5 GB of storage and weighed 6.5 ounces. Instead, he said it could hold 1,000 songs, and physically demonstrated how it was small enough to fit in his pocket. The image of a music player in your pocket was easy for the audience to grasp and remember, while the specific number "1,000 songs" was relevant to every music fan in the room.

**Use data sparingly and make it easy for the audience to comprehend.**

### 7. Use the rule of three. 

The _rule of three_ is a powerful concept in communications theory. It states that lists of three are inherently more effective because they feel more natural than lists of other lengths.

Steve Jobs said the iPhone combines three devices (a touch screen iPod, a phone and an internet communicator), John F. Kennedy listed three main investments needed for the US to win the space race and you're probably expecting a third example because lists of three come so naturally.

Grouping themes, subjects and individual sentences into lists and sublists of three maximizes their impact.

People can usually only recall three important points from any presentation. To take advantage of this fact, you should make a list of all the points you want to make, and then cluster and categorize them until you are left with only three key messages.

Use these three key points as a verbal roadmap, which you outline right at the start of your presentation. The roadmap will serve as a guide for your audience and help them keep pace as you move through the presentation.

**Use the rule of three.**

### 8. Make your presentation stick by including emotional details. 

If you want to make your presentation truly memorable, give your audience a "_Holy Shit" Moment_. Plan and rehearse a revelation which will elicit an emotional response.

People might forget your slides, your product and even you yourself, but they will never forget the way you made them feel. Emotionally charged events stick in your memory like mental Post-It notes.

The "Holy Shit" Moment can be something exhilarating, like Steve Jobs pulling the MacBook Air from a manila envelope to demonstrate its thinness, or something endearing, like an organic produce farmer explaining how he can hug his children right after work with no toxic pesticides on his clothes.

As long as there is a well-rehearsed yet surprising emotional revelation, it will be the most memorable thing in your entire presentation.

Another way to enhance your presentation's emotional content is to use wording which is simple, concrete and emotional. Forget about buzzwords and confusing jargon, use descriptive, "zippy" words and superlatives to convey how excited you are about your presentation.

For example, Steve Jobs said the buttons on the new OS X interface looked so good, "you'll want to lick them."

**Make your presentation stick by including emotional details.**

### 9. How you speak and look can be more important than what you say. 

According to some studies, nonverbal cues and tones of voice are more important than the actual words spoken. They are the reason Steve Jobs' stage presence exudes the authority and confidence it does.

Act like the leader you want to be. Forget standing behind a podium, reading monotonously from note-cards with your arms stiffly by your side.

Instead, stand so there is nothing between you and your audience, maintain eye-contact and use vivid hand gestures to enliven your delivery. Use pauses, varying levels of volume and different inflections to energize your speech.

Remember, while Steve Jobs may have been able to dress in sneakers, jeans and a mock turtleneck, you should aspire to also dress like the leader you wish to be, that is, a little better than anyone else in the room, as long as it's appropriate for the company culture.

To get a better understanding of any deficiencies in your delivery, videotape yourself rehearsing. Remember, even on video you should exude energy and look like you are having fun, both informing and entertaining the audience.

If you seem to be falling short of this goal, try to really go overboard with your energy. Chances are, this way you will get it just right.

**How you speak and look can be more important than what you say.**

### 10. Liven up your show with props, demonstrations and video clips. 

Using many different kinds of media makes your message stick. By using live demonstrations, video clips and props to hand out, you can help every type of learner in the audience understand your message.

People learn in different ways: some respond best to _visual information_, others to _auditory stimulus_, and others still to _kinesthetic experiences_ (touching, moving and doing things). Studies have shown that using multiple media in your presentation can dramatically increase how well the audience remembers your presentation.

Giving a short, simple and impressive demonstration onstage can be a highly effective way of showing the audience your product's hottest features.

When Steve Jobs introduced the iPhone, he used Google Maps to find a Starbucks closeby and then crank-called them to order 4000 lattes to go. All this happened while the audience watched and laughed.

Video-clips are not commonly used in most business presentations, but they are a great way to make your presentation stand out. Steve Jobs loved to show Apple's latest TV commercials in his presentations, sometimes repeatedly.

Use videos and demonstrations as "intermissions" during which the audience can relax and regroup. Research shows that people are usually able to maintain genuine interest in a presentation for a mere ten minutes before becoming distracted.

**Liven up your show with props, demonstrations and video clips.**

### 11. Share the stage and the glory. 

There are three important reasons why you should share the stage with other people during your presentation.

First of all, your audience craves variety. Listening to even the most charismatic speaker in the world grows dull after a while. Introducing partners, celebrities or members from your team often livens up the show.

Consider Steve Jobs, who had the CEO of Intel come on stage wearing an Intel bunny suit when Apple's cooperation with Intel was announced. In 2005, when Jobs declared all of Madonna's albums would be available via iTunes, the star herself appeared via a video link to discuss the news. These made for some memorable moments.

Second, other speakers may be able to speak more authoritatively on certain subjects. For example, when Apple introduced new notebooks crafted from single blocks of aluminum, Jobs let a design executive present the actual design and manufacturing process.

Third, customer endorsements are any company's best sales tool. Customers value objective opinions on products, so you should have satisfied customers and media reviewers endorsing your product as part of the presentation, preferably live or via video.

Also, remember to sincerely thank your employees, partners, customers and audience. This helps build rapport and shows you have the integrity to give credit where credit is due.

**Share the stage and the glory.**

### 12. Final summary 

The key message in this book is:

**To present as impressively as Steve Jobs, make your presentation as easy as possible for the audience to understand and remember.**

The questions this book answered:

**How can you prepare a successful presentation?**

  * To make your presentation successful, plan your storyline and your key message meticulously.

  * To make your presentation great, practice relentlessly.

  * Don't let the unexpected ruin your presentation; be prepared for trouble.

**How can you make your messages stick?**

  * To get your audience's attention, tell them how you will solve their problems.

  * Don't overwhelm your audience; simplify your slides and language.

  * Use data sparingly and make it easy for the audience to comprehend.

  * Use the rule of three.

  * Make your presentation stick by including emotional details.

**How can you make your presentation stand out among the competition?**

  * How you speak and look can be more important than what you say.

  * Liven up your show with props, demonstrations and video clips.

  * Share the stage and the glory

**Suggested further reading: _Talk like TED_ by Carmine Gallo**

In _Talk_ _like_ _TED_, you'll learn about presentation strategies used by the world's most influential public speakers. Author Carmine Gallo analyzed more than 500 TED talks to identify common features that make these talks so influential and appealing.
---

### Carmine Gallo

Carmine Gallo is a former vice president of a major global public relations firm who coaches some of the world's most respected brands in presentations and communications. He has appeared on CNBC, NBC, CBS, as well as in the _Wall Street Journal_ and the _New York Times_.

