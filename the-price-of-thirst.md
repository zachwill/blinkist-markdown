---
id: 555b04106135330007170000
slug: the-price-of-thirst-en
published_date: 2015-05-19T00:00:00.000+00:00
author: Karen Piper
title: The Price of Thirst
subtitle: Global Water Inequality and the Coming Chaos
main_color: BDA994
text_color: 4F453B
---

# The Price of Thirst

_Global Water Inequality and the Coming Chaos_

**Karen Piper**

In _The Price of Thirst_ (2014), author Karen Piper reveals how private water companies have not only failed to offer universal access to clean water but also contributed to environmental degradation and political conflict amid a quest for profit.

---
### 1. What’s in it for me? Find out how privatization is threatening our access to drinkable water. 

The next time you enjoy a glass of water, consider this: of all the water on our planet, only 1 percent of it is drinkable. The other 99 percent? Ice and seawater.

That's right — everyone on earth shares what is already a very limited resource. And what's more, our access to clean water is diminishing year by year, so much so that water scarcity just may be the defining issue of our age.

Yet there _should_ be plenty of water for all of us, whether we live on an Andes mountain top or in the center of Cairo. But thanks to some shortsighted, greedy decisions, we're now facing a crisis.

These blinks will show you how the privatization of water management has led to one of the biggest global threats in our time. So enjoy that glass, as who knows how long our water will last.

In these blinks, you'll discover

  * why some Chilean rivers are off-limits to local citizens;

  * what made water cost 400 percent more in Manila; and

  * why we're pumping water back into Owens Lake in Los Angeles.

### 2. Urbanization, agriculture and climate change – problems we’ve created – are why we’re running out of water. 

Some 70 percent of earth's surface is covered with water. So why has the issue of water scarcity become such a hot topic?

It's not that we lack water in general; it's a lack of _drinkable_ water that has become a global concern.

Much of the water available for drinking is actually polluted; and waterborne diseases, such as cholera and dysentery, are actually the leading causes of death and sickness around the world. The combination of highly populated cities and poor sanitation has led to a rise in waterborne diseases.

Another source of drinkable, or potable, water is groundwater found in natural underground reservoirs, or aquifers. Yet when we pump water out of aquifers faster than it can be replenished, these reservoirs collapse, preventing them from absorbing any more water.

Crucially, when aquifers dry up, the land can actually sink, a process called _subsidence_. In California's San Joaquin Valley, the constant extraction of groundwater to irrigate crops is believed to have caused the land to sink some 40 feet over the past 90 years.

What's more, aquifers in coastal regions are at risk of taking on seawater when groundwater levels fall too low, turning once potable water into non-potable water.

Many freshwater rivers are fed by glaciers. Yet because of climate change, these glaciers are melting at ever faster rates. While the flow of freshwater into rivers might increase in the near term, in the long term as the glaciers melt completely and disappear, so too will the rivers.

The distribution of potable water is also a concern. Importantly, we as a society are distributing water unequally among ourselves.

Increasing urbanization has put pressure on water systems. As some 55 percent of the world's population lives in cities, the demand for water in metropolitan areas far exceeds the supply from nearby ground- and surface water sources. Demand thus can only be met through deeper wells or even transporting water from increasingly remote areas.

Within certain municipalities, water is often unequally allocated by agencies or the government. New Delhi, for example, distributes water to certain regions based on geography. So while some 500 liters per person per day goes to the local army base, some 225 liters goes to official settlement areas. Yet only 50 liters per person per day is delivered to New Delhi's overpopulated slums.

### 3. Corporations have cashed in on our water crisis. The focus is always on profit maximization. 

So how can companies profit from a natural resource that falls from the sky and bubbles up from the ground? Sure, many of us buy bottled water, but that's just the tip of the iceberg.

Corporations earn money primarily from _water treatment and delivery_.

In contrast to public water utilities, private water companies focus on profit. Such firms raise prices on a whim, cutting infrastructure and labor costs through firing employees, neglecting infrastructure repairs and so on.

While public services are ideally focused on the common good, for-profit companies aren't beholden to such standards. In Manila, for example, water costs rose some 400 percent after a private company took over the public water utility.

Although not widely known, the strategy of _water marketing_ — or buying and selling water rights — is another way private companies can generate profits.

Water, whether a river, stream or lake, can be bought and sold independently from the land on which it is located. For example, the owner of a piece of land and the owner of a river that runs through the same land can be two different parties.

Corporations can either purchase rights for _consumptive use_, such as delivery to a private tap, or for _non-consumptive use_, such as building dams and generating hydroelectric power.

Chile was the first country to privatize 100 percent of its water resources. The former public enterprise Endesa, privatized in 1989, controls non-consumptive rights for the Baker River, one of Chile's largest rivers in terms of volume. Although no dam has yet to be built, local residents are nonetheless not permitted to use the river's water.

In regions where water is often scarce, such as in California, water can be deposited in _water banks_. In "wet" years, for example, a farmer can opt to forego a portion of his water allocation and house it in an aquifer. The farmer can then sell his right to use it, or reserve it for when he really needs it.

All in all, there's one thing that all these methods of turning water into cash have in common: profit maximization.

> _"Investment companies describe water as 'the new oil'."_

### 4. The IMF and the World Bank promote water privatization to the detriment of clean water access. 

Without water, humanity as we know it dies. So how exactly did the private sector seize so much control of our most precious natural resource?

The management of public water utilities has often been voluntarily outsourced by municipalities to private contractors to save money on maintenance and development. 

In the United States and Europe in the nineteenth century, privatization was commonplace. Yet after repeated outbreaks of waterborne diseases such as cholera and typhoid in addition to concerns over private firms' failure to provide universal access to potable water, water management was returned to public hands.

Today in the United States, public utilities are responsible for managing 90 percent of water resources. Yet despite private firms' prior dismal record of water management, in the 1990s the International Monetary Fund (IMF) and the World Bank began again to promote privatization.

These global institutions have even advocated for the privatization of the water sector in developing countries by making it a condition for critical loans.

Why would these organizations push such a strategy? Privatizing water management is just one step toward the general deregulation and liberalization of markets and downsizing of federal governments, all general goals of such groups.

What's more, the World Bank also enjoys some financial advantages, as its private sector arm, the International Finance Corporation, owns shares in some of the world's biggest water companies, such as the Paris-based Veolia.

South Africa in 1993 was backed into a corner by the IMF by accepting loan conditions that crushed the post-apartheid government's goal of water safety for all its citizens. As a result, the country's poorest citizens saw their access to clean water diminish.

In sum, water privatization hasn't improved our access to clean water, yet developing countries are being forced to privatize so Western companies, banks and governments can profit.

> _"Cholera killed the private water industry until neoliberalism popularized it again."_

### 5. In water privatization schemes, it’s the private company that wins every time. 

If you're a CEO looking to maximize profits, the poorest people in developing countries are unlikely to be your dream customers. Yet multinational water companies manage to nonetheless generate plenty of cash from people in the most dire conditions.

As privatization projects supported by the International Monetary Fund and the World Bank operate on the grounds of full-cost recovery, it's the private company that reaps the biggest rewards.

_Full-cost recovery_ means a company doesn't have to worry about the cost of laying pipes or updating infrastructure (so-called sunk costs) as such costs are covered by added fees paid by water users.

So in essence, a consumer's water bill also includes fees to the company for installing and maintaining the water supply, in addition to the price the company charges for using water!

When a private company decides to invest in a public water utility, a municipality will often agree to ensure a profit rate of 15 to 30 percent in exchange for paying a lease to operate the utility.

For example, when the Bolivian city Cochabamba relinquished control of its water infrastructure to California-based firm Bechtel, it did so in exchange for guaranteed profits of 16 percent per year. However, to achieve that considerable profit margin **,** the cost of water for citizens increased by 60 percent within just a few months.

What's more, to fulfil their promises of consistent profits, governments often also have to pay subsidies to poor citizens who can't afford the rising cost of water!

Financially speaking, all actors except the private company lose in water privatization schemes. Cities lose a steady income source; citizens are forced to pay higher prices. Corporations alone enjoy the profits.

### 6. The water crisis also has a human cost. Unequal distribution or a lack of access leads to social unrest. 

Imagine living on less than $2 a day and not being able to afford clean water. At the same time, you watch as gallons of water are pumped into swimming pools and golf courses in gated communities.

Welcome to downtown Cairo.

Privatization does not extend access to clean water. In fact, as the 2000s came to a close, the opposite was true.

Private companies have failed to widen water networks and have broken the promise of improving water quality. What companies have done instead is raised the price of water so that it's no longer an option for poorer people.

For instance, when the regime of former President Hosni Mubarak privatized Cairo's water utilities in 2003, within months the price of water doubled. Those who couldn't afford the steep fees were forced to drink from filthy canals.

To make matters worse, the regional water company distributed water unequally, diverting water to wealthy suburbs rather than to poorer farming and fishing towns.

A 2010 study revealed that 69 percent of the residents in the exclusive town 6th of October City had constant access to clean tap water, while 40 percent of Cairo residents had no running water for more than three hours a day. Three large districts received absolutely no water at all.

Unsurprisingly, a knock-on effect of water deprivation is social unrest; unequal water distribution can lead to protests and even riots.

In fact, unequal water distribution and price hikes were some of the key catalysts behind the revolution that eventually overthrew Mubarak's government.

Water conflicts are not limited to national borders. There is mounting tension between Turkey's World Bank-funded Greater Anatolia Project and the needs of the country's neighbors. When finished, the planned 22 mega-dams will drastically cut the flow of the Euphrates and Tigris rivers to Syria and Iraq.

What will be the result of such plans? Depriving people of the most basic human need often leads to conflict.

> _"People can live in poverty for a long time; they cannot live without water."_

### 7. Focusing on profits from water privatization can have a huge environmental cost. 

As we've seen, private companies that control water rights are more interested in short-term profits than long-term environmental damage. If this continues, disastrous consequences will follow.

One concern is that large dams, often built by private energy companies, can worsen the effects of climate change.

One way large dams can affect climate change is through the decomposition of organic materials in dam reservoirs. Such decomposition produces vast amounts of methane, a greenhouse gas that is 20 times more potent than carbon dioxide.

In some situations, dams can even produce more copious harmful greenhouse gases than do fossil fuel-burning power plants!

The building of dams is just one problem among many. For example, when we drain large bodies of water such as riverbeds or lakes, the resulting dry land can release hazardous substances.

Wetlands bind gases, metals and other substances to the soil. When waters are emptied, the resulting dry earth makes gases like carbon dioxide and methane as well as other solid matter airborne.

This was the case in 1913, when the city of Los Angeles began draining Owens Lake for potable water. As the lake dried up, it became one of the largest sources of particulate pollution in the United States. Today, the dry lakebed needs to be irrigated with water from other sources to prevent further carcinogenic metals becoming airborne.

We must remember that the world's water systems are delicate. We can call water an economic commodity all we want, but we can't dam or drag water from one place to another without facing serious consequences.

### 8. Once privatization is the rule, backing out is hard to do. Companies always seem to have the upper hand. 

By now you're probably thinking, it's obvious that the public needs to reclaim its water. Yet it's not easy as you might think, for practical and legal reasons.

States can't just reverse privatization, as such agreements are bound by legal contracts, many of which are valid for a period of 30 years. If a government wants to cancel a contract, a private company can sue — even if the state has good reason to cancel.

The conflict between the state of Tanzania and British-owned Biwater is a case in point. Tanzania felt Biwater did not fulfil its duties as contracted, neglecting to pay its lease fees nor install a single pipe. In fact, the state claimed that water quality actually suffered under Biwater. On these grounds, the government canceled its contract with the company.

Yet it was Tanzania that was sued for some $20 million in damages for breaking the contract. While the country was found to have violated its investment treaty with the United Kingdom in canceling the contract, the International Center for the Settlement of Investment Disputes thankfully ruled that Tanzania did not have to pay up as a result.

And it's not only banks or national economies that rely on the revenues from water companies but also individual citizens who invest privately.

Multinational corporations are often involved with or held by different parent companies and investment portfolios; this makes it complicated to cleanly sever ties with companies that behave badly when it comes to water management or rights.

The three major shareholders of American Water, for example, are pension funds. So if American Water faces a drop in profits, the pension benefits of many people could also be jeopardized. Forcing a city government to choose between the privatization of its water supply and the health of employee pensions is no easy choice.

Clearly there is no easy path to untangling private corporations from water supplies.

### 9. Stopping climate change and the spread of urbanization can help tackle the causes of water scarcity. 

Most of us today live in highly populated cities. Yet with such mega-cities, the pressure on local water supplies is acute as demand continues to rise.

Water pollution can be an issue in high-density areas; and as costs rise to make dirty water clean, we as a society face the ever-tougher challenge of making potable water available for everyone.

This cycle needs to be broken, but how? Simply, we need to curb, if not stop, climate change.

Glaciers are the largest freshwater reservoirs we have on earth. Yet as explained earlier, climate change has accelerated the melting of these glaciers. We just can't construct a dam large enough to capture all this thawed water before it reaches the sea.

In the Andes Mountains in Chile and Argentina, glaciers are melting faster than anywhere on earth. The glaciers in the Southern Patagonian Ice Field have shrunk by about six feet per year since 2000.

If we don't do something soon, scientists estimate that some of these glaciers will completely disappear by 2030!

What else can be done? We need to cut back urbanization and return to more sustainable living conditions.

Constantly supplying cities with water has become unsustainable, as it requires energy-depleting treatments, deep-well pumping and long-distance water transfers that not only damage the environment but also elevate water costs.

Small-scale local solutions, such as rainwater collection or the building of small dams that supply just a few people per square foot, are some alternate ideas. Spreading out human settlement more evenly across countries could well be a sustainable alternative.

The bottom line is, we _can_ solve our global water crisis if we are willing to change our way of life and if we stop treating water as an economic commodity.

### 10. Final summary 

The key message in this book:

**The privatization of water management is not only exacerbating environmental and social problems but also is draining money from the public and redistributing it to profit-oriented corporations and banks.**

Actionable advice:

**Re-think what you eat.**

The food you eat is most likely contributing to water scarcity around the globe. But making a few changes to your lifestyle can help remedy this. Consider reducing your meat intake, for example, and seek out fruit and nuts, as these crops are water-efficient.

**Suggested further reading:** ** _Poor Economics_** **by Abhijit V. Banerjee and Esther Duflo**

Investigating some of the biggest challenges poor people face, this book provides the reader with an understanding of why there still is so much poverty in the world, and why many of the measures usually implemented do not help. Based on these insights, the authors offer a number of concrete suggestions to demonstrate how global poverty might be overcome.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Karen Piper

Karen Piper is a professor of literature and environmental studies at the University of Missouri. She is also the author of _Cartographic Fictions: Maps, Race and Identity_ and _Left in the Dust_ : _How Race and Politics Created a Human and Environmental Tragedy in L.A._

