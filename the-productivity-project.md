---
id: 59aea92fb238e10005ef0077
slug: the-productivity-project-en
published_date: 2017-09-08T00:00:00.000+00:00
author: Chris Bailey
title: The Productivity Project
subtitle: Proven Ways to Become More Awesome
main_color: 5BBAE7
text_color: 1B6A8F
---

# The Productivity Project

_Proven Ways to Become More Awesome_

**Chris Bailey**

_The Productivity Project_ (2016) is a practical guide to how to live a life that's both productive and meaningful. With easy-to-understand techniques and reliable advice, you'll find out how to work smarter and accomplish the work that really matters. Stop wasting time and procrastinating, and pick up some new tools to take control of your life!

---
### 1. What’s in it for me? Become more productive by doing less. 

Most of us would love to be more productive, but struggle to get a handle on our tasks and accountabilities. That's why the author spent a year trying every productivity technique under the sun.

These blinks look at techniques, methods and ways of thinking that are sure to help you in your own productivity project. They might seem simple but, as you'll soon find out, less is more.

You'll also learn

  * what three things you need to be productive;

  * how working more hours makes you _less_ productive; and

  * what a brain dump is, and why you should do it.

### 2. Productivity is about working smarter by managing your time, attention and energy. 

Some people think productivity is about doing more. They're wrong! In fact, productivity is about working smarter and getting the important things done.

Let's look at two extremes of the working spectrum: a frantic Wall Street trader and a Buddhist monk. Ideally, what you want to find is the perfect meeting place between these two.

The prototypical stock trader works at such a frenzied pace that his intentions become scattered and he fails to see the meaning and value of his work. Meanwhile, the monk takes the time each day to meditate and reflect upon what's meaningful, yet he works at such a deliberate place that he doesn't accomplish very much.

So, it's only when you position yourself between the rhythms of the monk and the Wall Street trader that you find true productivity. This ideal working pace is deliberate enough to identify what's important and purposeful enough to accomplish your goals.

To find this perfect pace, you need to manage your time, attention and energy.

After all, you can have all the energy in the world, but it won't accomplish much without a well-managed schedule. And the opposite holds true as well: you might have a perfectly structured plan to finish your dream project, but it's useless if you're wasting all your energy elsewhere.

However, both time and energy can be wasted if your mind is constantly wandering and unfocused, which is why attention is the third key to the productivity puzzle.

In the blinks ahead, we'll explore a number of techniques that will help you strike the perfect balance between your inner monk and Wall Street trader, so that you can be the most productive version of yourself.

### 3. You can increase productivity by recognizing your personal values and motivations. 

So, what's the first step to being more productive?

Well, before you do anything, you need to find your purpose and motivation. They'll help you create the right routines that keep you on pace toward your goals. If you try to skip this step, there's a good chance you'll establish bad habits and poor routines that will work against you in the long run.

The author made this mistake when he first set out to become more productive. He set a 9:30 p.m. bedtime so that he could wake up at 5:30 a.m. every morning. With this schedule, he figured he'd have more time to do things before work, like get to the gym, do some reading, eat breakfast and plan the day ahead.

This went on for months before he realized he didn't care for this routine. It dawned on him that he was just performing his morning ritual so that others would think he was accomplishing a lot — but there was no personal satisfaction in his efforts. In other words, he hadn't thought about the purpose of being more productive, which made it impossible to create routines that matched this goal.

You can avoid this kind of mistake by asking yourself an important question: if you were given two additional hours to your day, what would you do with this extra time?

The answer should point you in the direction of the true purpose behind your desire for greater productivity.

Would you spend more time with friends and family? If so, perhaps you're most strongly motivated by the values of community and personal connections. And, if this is the case, you probably won't benefit from a routine that keeps you away from an active social life.

So, your first step is to consider your reason for wanting to be more productive to begin with. Ask yourself, what's important and meaningful?

Your answers to these questions are central to sustaining a good routine and becoming more productive.

### 4. The Rule of 3 can help you organize your life and be smarter about your work. 

See if this sounds familiar: you wake up with a crystal-clear plan of what you want to get done during the day, only to have your plan fall to pieces by the time you check your first email.

Thankfully, there's _the Rule of 3_, which can help you make more attainable goals.

The Rule of 3 was introduced by J.D. Meier, a Microsoft executive who explains how it works in his book, _Getting Results the Agile Way_.

Basically, you should start every week by identifying three things to accomplish that week. Then, you should start every workday by identifying three goals you want to accomplish by the time you go to sleep.

It might sound simple, but that's part of why it's so effective. By looking ahead at what you want to have accomplished both day by day and week by week, you're already figuring out how best to structure your time, attention and energy on what's important.

So let's say your end-of-the-week goals are to finish the first section of your book, update your website with new content and renew your passport.

With these goals in place, your end-of-the-day goals might be to finish the next chapter of the book you're working on, decide upon the topic for your next blog post and fill out the passport renewal form.

When you're setting these goals, check your calendar.

To prevent yourself from choosing goals that are too ambitious or unrealistic, you need to consult your calendar to make sure you haven't already dedicated your time and energy to another task. For example, even though you want to finish that section of your book, if your calendar reminds you of a big presentation you're scheduled to give on Friday, it may be best to focus on preparing for that instead.

Remember, being productive also means being smart about your schedule, so don't set yourself up for failure by setting unattainable goals.

### 5. Avoid procrastination by making tasks more attractive and rewarding. 

According to a 2014 survey conducted by Salary.com, 31 percent of respondents spent at least an hour a day procrastinating and 26 percent spent two hours or more.

So why is procrastination so hard to overcome? Part of the problem is the specific attributes of some tasks that make them so unappealing.

According to Timothy Pychyl, a professor of psychology at Ottawa's Carleton University, there are six attributes that will make procrastination more likely: _boring, frustrating, difficult, unstructured or ambiguous, lacking in personal meaning_ and _lacking in intrinsic rewards_.

The more of these attributes a task has, the more likely it is that you'll procrastinate. This is why so many of us put off doing our taxes every year until the very last minute, since catching up on a television show is much more appealing.

Therefore, the secret to getting around this problem is to make those unappealing tasks appear more attractive. You can think of it as giving a troubling task a makeover.

Let's say tax day is approaching, but you would still rather get a root canal than fill out those forms. Well, who says it has to be boring and tedious? What if you grabbed a table at your favorite cafe, sat down with a tasty beverage and were able to get in some enjoyable people watching while taking care of business?

And if you feel like a task is lacking in any tangible reward, why not create your own reward system? You can pay yourself an hourly rate for the time it takes to finish the task, and by the end you can treat yourself to a gift that you've always wanted to buy.

### 6. Overworking can lead to inefficiency, so keep your work week under 50 hours. 

For most people, working more equals getting more done. But things are never that simple, especially when you consider that our energy reserves are not unlimited.

So, to make the most of your time, you need to take control of your schedule such that you are using your energy and attention as best you can.

There is no perfect equation that can tell you exactly how much work you'll get done during any given hour. So, the author conducted an experiment to find out how many hours per week produced the best results.

After testing out everything from a 20-hour to a 90-hour workweek, the author found he actually accomplished more or less the same amount of work across the board. As it turns out, the amount of energy and attention you exert is directly related to the amount of time you have.

Therefore, he found himself exerting less energy and attention during a 90-hour week, but remained focused and energetic during a 20-hour week in order to get his tasks done on time.

So it makes sense that the ideal amount of time would be in between these two extremes, at around 35 to 40 hours per week. Anything past this amount will start to negatively affect your productivity.

In 2012, the journalist Sara Robinson compiled 150 years of research to show that when we work beyond 60 hours, any task will take twice as long to accomplish than it otherwise would. So, if something takes a person with a 30-hour workweek an hour to finish, someone who has surpassed the 60-hour mark in their workweek would need two hours to finish the same task.

In a similar article published by journalist Bob Sullivan in 2015, he highlights a Stanford University study that shows how productivity takes a nosedive once a worker crosses the 55-hour mark in a workweek. It clearly shows that someone working a 70-hour week won't accomplish anything more with their extra 15 hours.

It all adds up to suggest that working 35 to 40 hours a week is an ideal schedule for optimal energy and concentration.

### 7. Writing down your appointments and tasks frees up your brain power for better productivity. 

Writing out a list is a deceptively simple act. It doesn't take much to jot down a few items on a piece of paper, but it can make a big difference to our overburdened brain.

You've probably experienced the feeling of not bothering to write a shopping list and then drawing a blank once you're surrounded by all the options at the supermarket. Our brain simply isn't made for storing information in this fashion, so sometimes you need to help it out by freeing up space.

This applies to productivity and completing those tasks, as well. You can free up valuable space and get yourself thinking sharper and more clearly by performing a brain dump and writing things down.

A _brain dump_ is a way of externalizing everything your brain is struggling to hold onto so that it can focus on doing what it's really built for: solving problems and coming up with new ideas.

So, at the beginning of your workweek and each day, do yourself a favor and write down these tasks so that you don't need to waste energy trying to keep track of what still needs to be done.

This simple but helpful technique will give your brain the space it needs to help you take action and get things done.

It's not just grocery lists and tasks, either; you can free up brain space by writing down everything from unfinished projects you want to take care of to books you want to read or a brilliant idea you don't want to forget. And if you don't want to carry around a pen and paper, you can jot it down on a notebook app on your mobile device.

Another way of freeing up some much-needed brain power is to tackle easy tasks right as they appear. Why bother writing something down or trying to remember it if you can take care of it right away? If there's a book you need to get your hands on, order it right away if you can.

### 8. Final summary 

The key message in this book:

**Being productive isn't only about getting more things done — it's about working smarter by finding ways to be efficient and make the most of your time. To do that you have to acknowledge what's most important to you and only then take care of business in the most efficient way possible.**

Actionable advice:

**Up your productivity by avoiding distractions.**

If you want to be productive, you need as much of your attention as you can possibly muster. Unfortunately, there are plenty of distractions out there, ready to hijack your attention. Just think of your phone alerts whenever you have an incoming message or e-mail. It takes time to refocus every time you hear one, so do yourself and your productivity a favor by deactivating as many potential distractions as possible.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Power of Habit_** **by Charles Duhigg**

_The Power of Habit_ (2012) explains how important a role habits play in our lives, from brushing our teeth to smoking to exercising, and how exactly those habits are formed. The research and anecdotes in _The Power of Habit_ provide easy tips for changing habits both individually as well as in organizations. The book spent over 60 weeks on the _New York Times_ bestseller list.

**This is a Blinkist staff pick**

_"Having tried and failed several times to pick up good habits like morning meditation, I found these blinks really helped me understand the root of the problem."_

– Ben H, Head of Editorial at Blinkist
---

### Chris Bailey

Chris Bailey is an expert in productivity who has been hard at work discovering the secrets to living an efficient life since he was a preteen. He's collected the results of his experiments on his website, A Life of Productivity. He's also a popular public speaker with his own TED Talk: _A More Human Approach to Productivity_.

