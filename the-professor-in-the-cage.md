---
id: 56afab65377c16000700008a
slug: the-professor-in-the-cage-en
published_date: 2016-02-04T00:00:00.000+00:00
author: Jonathan Gottschall
title: The Professor in the Cage
subtitle: Why Men Fight and Why We Like to Watch
main_color: A6A99A
text_color: 4D5238
---

# The Professor in the Cage

_Why Men Fight and Why We Like to Watch_

**Jonathan Gottschall**

In _The Professor in the Cage_ (2015), professor Jonathan Gottschall enters the world of mixed martial arts to discover the sources of our fascination with violence. Through the power of modern science and by applying the weight of human history, these blinks reveal how our love of fighting is grounded in our deepest human instincts.

---
### 1. What’s in it for me? Find out why people still enjoy watching violence take place. 

On your average weekend, how many fights to the death do you witness? It would be a fair guess to say none. Yet, if you were alive some two thousand years ago and were a citizen of the Roman Empire, the answer would likely have been completely different. These days, most of us enjoy the benefits of living in what is, relatively, one of the most peaceful periods in human history. We don't witness anywhere near the level of violence experienced by our ancestors. 

But this doesn't mean that violence is no longer a pervasive force in society — you only have to attend a mixed martial arts (MMA) fight for proof. After all, violence is a natural impulse for humans, one deeply ingrained in our DNA. But modern-day humans have learned to channel this violence into less dangerous, more controllable pursuits. These blinks explain just how this happened.

In these blinks, you'll also learn

  * how our ancestors fought to prepare for war;

  * about the violent origins of lacrosse; and

  * why males and females fight differently.

### 2. Fighting is a human act, but over time it has become more and more codified. 

Some people fight for revenge, others for prestige and still others just fight for fun. But one thing is for sure: people have been fighting since the beginning of human history.

However, certain aspects of fighting have indeed changed. In the past, men have fought in ways that would seem brutal to people today. They would batter each other to death with stone axes, slice each other to pieces with swords and burn each other alive.

The reasons why people fought would also seem strange nowadays. For example, honor used to be one of the main reasons for fights, because a person's capacity to defend his honor defined his social status. And without social status, a person was nothing in the eyes of others.

Clearly, our species's past was quite violent. But over the course of many generations, humans collectively learned how to control their violent urges by allowing society to _codify_ violence.

Take duels as a case in point. Instead of a fight breaking out without warning, duels controlled violence by codifying strict limitations and rules. For example, opponents had to agree on a time and place to fight, and only "civilized" weapons, like swords or guns, could be used. And if you could get your opponent to back down instead of fighting, you could win before anyone got hurt.

This pattern of increased codification of violence continues right up to the present.

Even though fighting still happens, and people do fight for honor, most of it is highly codified. Take the incredibly popular sport of mixed martial arts (MMA). Although there are barely any rules regarding the actual combat, the act of fighting itself is highly codified: it's limited to a specific time and place, and is closely supervised by a referee.

### 3. MMA attracts people as a way to fight bullies, but also as a form of ritualized combat. 

At some point during our life, we've all encountered a bully. Most people will have met one when they were still a child, while playing at school. 

But bullying goes far beyond the playground; it happens in societies around the world because it's universally effective. By picking on weaker members of society, bullies can boost their social status at the expense of their victims, without running the risk of facing retaliation.

It's because of bullies that many people want to learn how to defend themselves. If a bully thinks he might not win, he'll be less likely to pick on you. This is why MMA is so popular: it's a very effective form of self-defence that anyone can learn. It's also a less popular sport for elite athletes to engage in, so even the very unathletic don't need to feel intimidated when getting started.

But defending against bullies isn't the only reason people engage in MMA. Such violent sports are also examples of _ritualized combat_, a codified fight that aims to establish the winner while lessening the risk of serious injury through clear rules and limitations as to when, where and how it happens.

The origins of ritualized combat run deep. It is employed by our closest animal relatives, the chimpanzees, to establish societal hierarchies. Chimpanzees will puff up their chests and scream to try and get their opponent to back down. But if no one yields, the ritual breaks into a fight that only ends when someone surrenders — or is seriously injured.

Ritualized combat is actually very similar across species. For example, it typically begins with staring: humans and chimps stare at their opponents to try and intimidate them. Human boxers often try to break each other's confidence in this way before a fight, with Mike Tyson being the obvious master of this.

But why have we barely mentioned women in all this talk about violence? In the next blinks, we'll look at the relationship between violence and gender.

> _"Sustained eye contact is as stressful for other primates as it is for humans. As monkeys pace through their stare-down duels, the stress hormone cortisol floods their blood."_

### 4. Fighting is closely linked to masculinity. 

In recent decades, our conceptions of men and women have changed radically. Gender stereotypes dictating that women stay at home to care for children while men work to provide for their families are being dismantled.

However, there's one thing that definitely separates the sexes, namely the interplay between competition, violence and masculinity. In fact, there's a clear evolutionary reason that males are more competitive and violent than females: reproductive differences.

While women generate around 400 eggs in their lifetime, men produce 3.6 billion sperm. This means that men can parent many more offspring than women, especially when you consider that women cannot conceive during pregnancy or after menopause. 

The consequence is that, at any given time, there are more males than females who want to reproduce, which leads to competition for females — and, in turn, violence. This reproductive disparity is why it's mostly young, unmarried men who commit the majority of violence in society; they are the ones in fiercest competition.

The composition of the male body itself reflects this predisposition toward violence: despite being, on average, only 20 percent heavier and ten percent taller, men have 60 percent more muscle mass than women. 

But although women are generally less competitive and violent than men, it would be foolish to think that they don't also fight. Instead of physical violence, though, women are much more likely to attack indirectly. For example, they tend to attack reputations or spread rumors about their rivals. In doing so, they reinforce traditionally female cultural concepts like fidelity, modesty and honesty. 

In turn, masculine cultural concepts of dominance and honor encourage men to take more risks, like engaging in a fight even though they might get hurt. This risk-seeking behavior can be seen in other realms too: men are more likely to die from lightning strikes, because they're too "brave" to seek refuge. 

In the next blink you'll see that these differences between the sexes aren't only present in adults, but can be observed very early on.

### 5. Boys tend to play more competitively while girls prefer cooperation – and the same is true for adults. 

Often when you see boys playing, it involves some kind of fight or competitive game. In contrast, girls tend to play _together_, rather than against each other. 

This difference in _playing style_ is observable from a very young age. Because of these different styles, the sexes tend to play separately from the get-go. Preschoolers are three times more likely to play with children of their own sex — by the age of six, this likelihood will have increased elevenfold.

The sexes segregate themselves in this way because their playing styles are fundamentally incompatible and unchangeable. Boys tend to play more competitive games than girls; as time goes by, these competitive games turn into ritual forms of combat.

Girls, on the other hand, also play competitive sports, but differently. Although modern-day women are no longer excluded from competing as a result of near-continuous pregnancies and a patriarchal culture that encouraged them to stay at home, they still tend to be more attracted to _cooperating_ and _establishing bonds_ through sport _,_ rather than competition and victory.

Studies demonstrate that this effect is even present in professional sports: male athletes put a higher priority on competition than female athletes. Studies show that even in local five-kilometer races, three times more men than women finish within 25 percent of their gender's world record. In other words, men run to win.

And this trend goes beyond sports — men seek to win in every form of conflict, from physical fights to verbal arguments, which can result in some peculiar rituals. 

Take rap battles, where opponents try to spontaneously come up with the best insults to win. On the surface such rituals might appear ridiculous. But they actually have a vital function: by ritualizing conflict, societies can establish hierarchies and social roles while limiting violence. This allows for society to be hierarchically organized without losing citizens to injury or death.

### 6. Historically, sport helped train for war, but also helped avert it. 

Have you ever felt yourself wishing for the other team to be obliterated while playing a sport or cheering on your favorite team?

That's because many sports are _ritualized forms of warfare_.

For a long time, sports, whether sword fighting, boxing or chariot racing, existed as exercises to prepare for war. For example, medieval warriors would hone their fighting skills in huge tournaments, sometimes fighting to the death. 

And even though the connection between sports and war isn't as apparent today, if you look beneath the surface, you'll see the common root.

Consider the vocabulary: in American football, terms like "blitz" are still used, while an overwhelming victory is called a "massacre."

Or look at the fans. They dress in the colors of their team, roar battle songs and demand that the players give their all to benefit the team. In fact, being a fan has an even deeper historic connection to war: throughout history, people who couldn't defend themselves would take the side of great fighters, supporting them with gifts and praise, and gaining the fighters' protection if they won.

But sports did more than just help warriors prepare for battle. They also helped groups fight for supremacy in a less violent way than a full-blown war.

For example, Native Americans developed a precursor to lacrosse, which could be played among teams ranging from a few players to thousands. It allowed tribes in a given area to see who was the strongest without resorting to explicit violence. 

But sometimes even the sport couldn't prevent a war, like when the Creeks beat the Choctaws in a game of lacrosse in 1790. The next day, the furious Choctaws attacked and murdered some 500 Creeks.

> _"An MMA gym is a man factory. It's where men go to hammer the softness, weakness and timidity out of each other."_

### 7. Humans have always been excited by the spectacle of violence. 

Have you ever seen the film _Gladiator?_ If you have, you'll know just how much people were entranced by violence in the past.

Indeed, there's lots of evidence to suggest people have had a long-standing love affair with the _spectacle of violence_. In the Coliseum, the Romans watched gladiators fight to the death, and prisoners of war battle for their lives against wild animals. And in the Middle Ages, even children were taken to public executions to watch criminals being burned or buried alive. 

But we're much more civilized now, right? Well, not really.

Although we aren't as violent as our ancestors, we've by no means lost our hunger for _violence as entertainment_. And if you look at much of today's popular culture, you'll see that violence lies at its very core.

Take the success of books like _The Girl with the Dragon Tattoo_ by Stieg Larsson, which are packed full of instances of rape and murder. Or look at the _Saw_ film series, which is more or less a clever glorification of torture. 

The big difference between today and the past is that real suffering has been replaced with fictional violence in books and films.

But why do we love to watch violence in the first place?

Because it shows us the most exciting and appalling sides of humanity at the same time.

Consider an MMA fight. Not only is it a stunning display of brute force, it's also an exciting spectacle of two people fighting for supremacy with everything they can muster — their skills, their strength and even their grace.

This excitement is also why violence is often compared to sex.

Some soldiers who have been to war have expressed that they hated and loved it in equal measure. Even though war is hell on earth, it's also one of the most physically exciting things they've ever experienced. So although they don't get sexually aroused by it, many soldiers have indeed compared war to the intense excitement of sex.

> _"Other than boxing, everything is boring." - Mike Tyson_

### 8. Of all martial arts, MMA is the closest to real combat. 

We've seen that sports have a direct link to war and violence. But this doesn't mean that all sports are created equal — in fact, one sport stands above the rest.

MMA is the most brutal, hierarchical and realistic of modern combat sports.

Don't be put off by the name. When some people hear of martial arts, they think of complex choreographies of movement that may look impressive, but are harmless.

MMA is a whole other world. 

The artificial restrictions of other martial arts is one reason why MMA started: there was a desire to recreate the old-fashioned sports used to prepare for war. Fighters from around the world, whether trained in the dojo or in the streets, meet in ritualized combat to see who is the strongest of them all.

And in MMA, the strongest usually prevails. Unlike other sports where the underdog can win, where David beats Goliath, MMA is closer to reality. It's rarely a fair fight, and the best fighter usually dominates.

Another reason MMA started was to allow the different martial arts to prove their respective claims. Each martial art has claimed to be the most powerful, but because their adherents never fought against each other, there was no way of proving who was the best. 

Of course, to accommodate all these different disciplines, this new form of fighting had to have very lax rules. In the beginning, there actually weren't any rules or weight classes, and actions like biting weren't banned, just discouraged. Today, some rules do exist; for instance, competitors must wear gloves and weight classes prevent Goliaths from winning every time. Even so, MMA remains the closest thing to a real, no-holds-barred street fight.

MMA also stands out from most Asian martial arts traditions because of its lack of a religious component. MMA doesn't accept religious concepts of authority and rules, or of ancient rights and artistic purity. Instead, MMA gyms are like laboratories where different martial arts are tested and combined into the ultimate, most effective way of fighting.

### 9. Final summary 

The key message in this book:

**Violence is a fundamental part of human nature. Over time, however, we have learned how to better bring these instincts under control. By channelling violent human tendencies into sports, martial arts and fiction, we have managed to create organized societies that don't tear themselves apart.**

**Suggested further reading:** ** _The_** **_Better_** **_Angels_** **_of_** **_Our_** **_Nature_** **by Steven Pinker**

_The_ _Better_ _Angels_ _of_ _Our_ _Nature_ takes a close look at the history of violence in human society, explaining both our motivations to use violence on certain occasions and the factors that increasingly restrain us from using it — and how these factors have resulted in massive reductions in violence.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jonathan Gottschall

Jonathan Gottschall studies the intersection of science and art as a distinguished fellow in the English Department at Washington & Jefferson College. He is also the author of _The Storytelling Animal_, which was a _New York Times_ Editor's Choice Selection.

