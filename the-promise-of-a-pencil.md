---
id: 53a121dd6230370007040000
slug: the-promise-of-a-pencil-en
published_date: 2014-06-17T00:00:00.000+00:00
author: Adam Braun
title: The Promise of a Pencil
subtitle: How an Ordinary Person Can Create Extraordinary Change
main_color: 3D52A0
text_color: 273E95
---

# The Promise of a Pencil

_How an Ordinary Person Can Create Extraordinary Change_

**Adam Braun**

This book tells the inspiring story of Adam Braun and Pencils of Promise, a charity he founded on just $25, and which has built more than 200 schools in developing countries all over the world. The book is divided into simple lessons that show how everybody can find their passion, make the best use of their potential and live a life full of meaning, joy and inspiration.

---
### 1. What’s in it for me? Let the founder of a leading charity show you how to pursue your dreams. 

_The_ _Promise_ _of_ _a_ _Pencil_ is the story of the genesis of Adam's charity, Pencils of Promise, and how it provides basic education for children in developing countries.

As a college student with his sights trained on a successful career on Wall Street, Adam decided to take some time off to backpack across the globe. On his travels, he witnessed widespread poverty and a lack of basic education in many countries, which led him to the idea of raising money to build a school in Laos.

Though Pencils of Promise was started with just $25 and a group of supportive friends, Adam managed to gradually transform it into a leading worldwide charity. At the time of writing, Pencils of Promise has built more than 200 schools, and has also branched out into teacher training and student scholarships.

The success of these schools, and the charity itself, is — of course — the result of hard work. But this success is also due to the passion, confidence, gratitude and inspiration of Adam and his colleagues.

As these blinks show, _The_ _Promise_ _of_ _a_ _Pencil_ describes and reflects on the steps that Adam took to follow his dream. In an exemplary and accessible way, the book aims to inspire people to start making their own dreams a reality.

In these blinks, you'll also find out:

  * why saying "thank you" can save you thousands of dollars,

  * why leaving your comfort zone is the key to any successful endeavor,

  * why your intuition is an invaluable guide to leading the life you truly want and

  * why success depends not only on your self-confidence, but also on you _expressing_ that confidence.

### 2. Follow your dreams, even if they seem unreachable or if people tell you not to. 

How often have you wondered how different your life would be if, instead of choosing the safe path, you were courageous and decisive enough to take certain risks?

Well, what exactly is stopping you?

Firstly, the dreams you wish to pursue will often contradict the expectations society has of you, which might discourage you from following them.

But, since this is _your_ life at stake, why should you care about the opinions of others?

Consider Adam Braun, who, in 2008, founded the charity Pencils of Promise. When he began, his goal was to raise enough money to fund the building of a school in a Laotian village, but his family tried to convince him to focus on his career instead.

Although Adam's family considered his dream a waste of time, believing that he couldn't possible raise the $10,000 required, Adam ignored their criticisms. Instead, he ploughed ahead in pursuit of his goal, and, within a matter of months, he'd achieved it.

Secondly, we are often discouraged from following our dreams because they appear unattainable at first. But ask yourself this: Will I ever stand a chance of realizing my dreams if I don't even _try_?

By 2013, for example, Pencils of Promise had grown so fast that they were breaking ground in Ghana for their 100th school. When Adam started the organization, no one — not even Adam himself — had believed they'd achieve such overwhelming success.

However, by believing in and committing to their idea, Adam and his team transformed an apparently unreachable goal into a concrete reality.

It's crucial that we allow ourselves to dream more often, and — even more importantly — to not delay the work required to turn our dreams into reality. So take your life into your own hands, and start living your dream today!

### 3. No one can succeed by living in a bubble: Push yourself out of your comfort zone and dare to take a risk. 

Most of us spend our lives in a personal bubble — surrounding ourselves with people who share our opinions, habits and lifestyle — because it feels safe and comfortable.

But this is not the best way to live.

All of us should get out of our comfort zones occasionally, as the new experiences that result might enrich our lives.

Adam, for example, had lived a happy, untroubled adolescence, and was set for a successful Wall Street career. But then, while in college, he decided to take a semester off to backpack alone across the globe.

As you might expect, most of his friends and family advised against it. After all, why would he waste his time on something that was of no help to his career?

But Adam followed through on his plan, and ultimately returned home with fresh insights and impressions that changed his entire life, leading eventually to him founding the charity Pencils of Promise.

Of course, getting out of your comfort zone is not easy: it involves thinking optimistically and taking a risk from time to time. If, on the other hand, you spend your time mulling over what might go wrong, you'll never do anything new or exciting.

For instance, when Adam was travelling in Guatemala, a local man offered him a place to stay in his home for a few weeks in exchange for English lessons.

Although Adam had only just met the man, and was uncertain of what to expect and how safe it would be for him, he ultimately agreed to the arrangement.

In the weeks that he spent in this man's home, teaching his family English, Adam got to know Guatemalan culture and made many new friends.

We never learn so much about ourselves as when we leave our personal comfort zone and are forced to deal with new, unfamiliar situations.

> _"Many of us spend our entire lives in the same bubble — we surround ourselves with people who share our opinions, speak the way we speak, and look the way we look."_

### 4. To find your purpose in life, follow your intuition. 

Have you ever suddenly felt very strongly about something without knowing why?

That's your intuition at work, and in most situations it's our most powerful adviser: it knows what's best for us long before we realize it ourselves.

Because of this, we should listen to our inner voice more often. If we trust our intuition, it will guide us and make us confident that everything will work out.

For example, while Adam was at a concert at the New York Philharmonic, he became captivated by the passionate performance of a pianist. As Adam was admiring the pianist, he had an epiphany: he, too, wanted to do something for which he felt that same level of passion.

Remembering his travels and the joy he'd witnessed in others as he helped them, he suddenly knew that he would found his own charity. When he returned home that very night, Adam sat down and wrote the mission statement for Pencils of Promise.

Intuition also plays a central part when we find ourselves in a particular dilemma, facing a difficult and important decision: should you follow your dreams or continue with your current routine?

When this happens, you should pay attention to certain signs that appear in your life, as your intuition will interpret them subconsciously and reveal what it is that you must do.

For instance, when Adam began the process of building his charity, he did so in the free time he had from his consulting job. However, when his supervisors noticed his lack of commitment, they asked him to decide which was a priority: the job or Pencils of Promise?

On his way home from work, while he was torn between focusing on his well-paid job and his fledgling charity, he came across a cardboard box graffitied with the simple phrase: "Become your Dream."

It was perfect timing: Adam took this as a sign, quit his job and focused his efforts on Pencils of Promise — a decision he never regretted.

### 5. When deciding your life’s journey, don’t be scared to start small. 

Many of us are terrified to follow our dreams, as they're often so big that realizing them seems wildly unrealistic.

But remember: even the longest journey begins with a small step.

Indeed, sometimes even the small things that seem insignificant to you might completely change your life or someone else's.

For example, while Adam was in India during his first backpacking trip, he asked a little boy he'd met what he would like most in the world. His answer?

A pencil.

Surprised, Adam gave the boy one of his own pencils as a gift, making the boy incredibly happy. For Adam, that pencil was a small, cheap item. For the boy, it was the fulfillment of his biggest wish, which allowed him to practice writing and reading.

This experience had such a profound impact on Adam that he took the name for his charity from it.

It follows, too, that starting small also means you don't need a lot of money to effect positive change. Indeed, most successful entrepreneurs started small, working within their limitations, and gradually developed their idea over time.

When Adam decided to found Pencils of Promise, he didn't have much money himself. Nevertheless, he opened a bank account for his organization, depositing the minimum amount of $25.

Soon afterwards, Adam began fundraising among family and friends, and within a short time had collected several thousand dollars in that account, which he used to develop and grow the organization. This (apparently small) act of opening an account and asking close acquaintances for funding was the first step for a fledgling organization that would eventually grow into a major charity.

So, don't think of your dream as one big, unreachable goal. Instead try to picture it as a series of small steps, and follow one after another as well as you can.

### 6. Ensure that you remain confident so you can overcome the inevitable obstacles you will encounter. 

While following your dream, you will almost certainly encounter difficulties that you didn't foresee at the beginning. But you shouldn't worry about these inevitable challenges, as there is one thing that will help you through — confidence.

Confidence is essential because having that unshakeable belief in yourself and your idea will ensure that you won't give up when things get tough.

Adam, for example, experienced one particular obstacle that altered his life's course drastically. Travelling to Africa for the first time, he found his ship caught in a major storm that caused severe damage and terrified the passengers. In fact, they believed they would die.

But while everyone around him panicked, Adam realized that he would not die on that day, as he felt he still had a purpose to fulfill.

This sense of purpose gave him the necessary confidence to remain calm, help his fellow passengers and emerge even stronger from the experience.

But confidence in yourself and your goal isn't the only vital thing: you also need to _act_ confidently to show the world that you're in control.

This is especially true when you're in a dangerous situation, because acting confidently in that scenario will show that you are not weak or afraid.

For example, when Adam was travelling in a taxi in Nepal, he found himself in the thick of a massive political demonstration. The atmosphere in the crowd was very heated, and protesters seemed ready to attack him.

However, although he was scared, Adam exited the car, greeted the protesters and walked away with no hesitation — even though he had no idea where to go! His self-confident behavior probably saved him a lot of trouble that day.

So, while obstacles are inevitable, you _can_ change the way you confront them. Try to stay self-confident, and, in those moments when your confidence is shaken, at least pretend that you know what you are doing.

### 7. Using the right language is essential for success. 

Like confidence, language is crucial to success: indeed, the way you talk to people can mean the difference between success and failure. For this reason, we must learn how to use language to our advantage.

One way is to choose your words carefully when presenting yourself to potential supporters. This is because sometimes even well-established words and phrases can have a negative effect on people.

For example, when Pencils of Promise was just beginning, Adam would refer to the organization as a "non-profit," as this was the best term to describe their objective. But some managers had an adverse reaction to the negative prefix, so Adam changed the phrase to "for-purpose."

By dropping the "non-," Adam instead created a positive connotation for the organization, and managed to increase interest.

Another way we can use language to our advantage is by using it to make our ideas more concrete, as the communication of our ideas is essential to making them a reality.

Often we keep our ideas in our heads, where it's hard to develop them. But simply by voicing those ideas aloud, we increase their chances of being realized.

Following a trip to Ghana, for example, Adam began privately considering expanding the charity's work to Africa. At first it was just a vague idea, but while he was giving a speech at a large fundraising gala, he tried to encourage donations by spontaneously promising the guests that if they raised one million dollars that evening, the organization would open a school in Ghana.

The result was overwhelming: they reached that goal easily, and just a few months later the first Pencils of Promise school opened in Ghana.

### 8. You need to inspire others to gain support for your dream. 

Since it's almost impossible to achieve any goal, or realize any dream, without the help of others, it's therefore important to consider how you might recruit others to your cause.

Firstly, when trying to influence people, remember that you get just one chance at making a good first impression. For instance, when addressing large groups of people, speakers should make every listener feel that they're important.

An important principle that Adam employs throughout his own presentations is "one person, one thought." This means that you establish eye contact with one person for as long as you're talking about a specific thought, and once you've finished that thought, you move on to another person and do the same.

By addressing people in this clear and direct way, you increase the chances that they will sympathize with your cause and be inspired to help.

Secondly, the number of people who listen to you is not important. What matters is that you inspire those who _do_ listen.

Indeed, it's far better to have one truly dedicated person at your side than one hundred people who don't really care.

For instance, at the beginning of Pencils of Promise, Adam and a few friends would tour the country, presenting their project at universities.

On the first occasion that they did this, the group booked a huge classroom because they expected a lot of people to attend. However, just one girl showed up!

Although he was frustrated and tempted to leave, Adam decided to give a heartfelt speech regardless of how many people would hear it — an effort that was not in vain, as his single audience member eventually became a dedicated volunteer for the organization.

### 9. To build and maintain relationships with others, you must find the time to thank them for their support. 

As the saying goes, a joy shared is a joy doubled, and a sorrow shared is a sorrow halved. Indeed, there is nothing more essential in life than developing good relationships.

For instance, have you ever told your loved ones exactly how much they mean to you? Not only will doing this make you feel better, but it will make them happy too and strengthen the bonds of your relationship.

Adam, for example, decided to dedicate the first school his organization built to his grandmother, because he was grateful for her sacrifices and her strength, which had enabled him to live a sorrow-free life: she survived the Holocaust, came to the United States with almost nothing and built a life so that her children could have a better future.

When Adam showed his grandmother the engraving of her name over the school's door, she was deeply touched and moved to tears. So, by creating joy in her life, Adam felt more lucky and thankful himself.

While it's essential to maintain good relationships with family and friends, it's also important to do the same with business partners and acquaintances.

For instance, because you'll be treated by others in the same way you treat them, you should always be nice, thankful and polite.

Two years into Pencils of Promise, Adam realized that he'd never personally thanked the first stranger who'd donated a large amount of money. In order to close the loop, he wrote a belated thank-you note.

Believing that was the end of the matter, Adam didn't expect what happened next: the stranger was so happy to hear from Adam, and so enthusiastic about the organization, that he arranged for the new Pencils of Promise office to be built, free of charge!

In short, simply by sending this letter and expressing thanks, the organization had saved tens of thousands of dollars.

### 10. Self-improvement comes from learning from your mistakes and recognizing your weaknesses. 

Failure is simply a part of life, and weaknesses are part of being human. In other words, no matter how hard you try, you won't be able to avoid either of them.

For that reason, we should instead consider how we can transform our mistakes to our advantage.

Indeed, the only way you'll ever improve is by making mistakes and learning from them. Without experiencing missteps and mistakes, you'd have no way of knowing what doesn't work.

For example, Adam once received an email from a colleague in Nicaragua reporting that he and another person had been robbed.

Incredibly, Adam's first response was not to ask after the colleague's well being, but to inform him that the company wouldn't reimburse them.

But since the colleague hadn't even asked for any money, Adam's response truly upset him.

Realizing that he was in the wrong, Adam vowed that in the future he'd never put money ahead of his staff. In short, the mistake had led him to develop a better style of leadership.

In addition to facing our mistakes and learning from them, we also need to confront our weaknesses so that we can attempt to improve them.

For example, during the first years of Pencils of Promise, Adam never asked anyone personally and directly for money. But this was not a principled stance on his part; he was, quite simply, afraid of rejection.

When Adam realized that this was getting in the way of him doing his job, he decided to confess this weakness to his board of directors.

Once they were aware of Adam's weakness, the board helped him to overcome it by giving him practical advice on how to handle such situations, and encouraging him to practice asking for money more directly.

### 11. Take time to make the right decisions. 

As we've seen, the pursuit of your dreams occasionally requires some hard decisions. As you chase your goal, you'll likely experience moments of doubt and uncertainty when faced with another path that seems more attractive, or just easier, than the one you chose.

In these situations, it's crucial to focus not only on the short term, but also on the long-term impact of your decision on your life, and on how it intersects with your personal values.

For instance, when Pencils of Promise was beginning to take off, Adam received a very attractive job offer — including a high salary and personal benefits — that would've required him to leave his organization.

Just when he was at the point of accepting the offer, a friend advised him to think about his principles and to choose the job he loved, not the one he needed. The result was that Adam decided to reject the job offer and stay with his own organization, which turned out to be the best possible decision for him.

It's also crucial, when you're deciding which path to take, that you make sure you're in a frame of mind conducive to making a good decision.

For instance, if you're working at the same routine, day in, day out, your judgment will be impaired. In such situations, you need time away from that routine so you can gain a new perspective.

Therefore, taking regular breaks from your daily working routine can help you to stay fresh and make better decisions.

An illustrative example of this principle comes from the world of weight training. While the weight lifting itself actually damages the muscle (the weight lifting is analogous to your normal routine), it's the resting period that follows that actually builds the muscle and increases your strength (and the same can be said of the breaks and vacations you take from your everyday life).

### 12. Final Summary 

The key message in this book:

**Don't** **be** **afraid** **of** **realizing** **your** **dreams:** **you** **only** **have** **one** **life** **and** **you** **want** **to** **fill** **it** **with** **love,** **joy,** **meaning** **and** **inspiration.** **If** **you** **believe** **in** **yourself** **and** **are** **thankful,** **humble** **and** **inspiring** **to** **others,** **no** **goal** **is** **unattainable.**

**Take **a** **risk.****

How can you know you're unable to do something if you've never tried? Taking a risk can improve your life in an unimaginable way.

**Don't** **underestimate** **yourself.**

Realizing your dream isn't as difficult as you think. If you believe in yourself, and move step by step, you will succeed.
---

### Adam Braun

Adam Braun founded Pencils of Promise in 2008. He has been named on both _Forbes_ ' "30 Under 30" and _Wired_ magazine's "50 People Who Will Change the World" lists.

