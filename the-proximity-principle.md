---
id: 5d2cdc446cee070008fd878f
slug: the-proximity-principle-en
published_date: 2019-07-17T00:00:00.000+00:00
author: Ken Coleman
title: The Proximity Principle
subtitle: The Proven Strategy That Will Lead to a Career You Love
main_color: 344B69
text_color: 344B69
---

# The Proximity Principle

_The Proven Strategy That Will Lead to a Career You Love_

**Ken Coleman**

_The Proximity Principle_ (2019) presents a proven plan for landing your dream job and turning your career dial from humdrum to awesome. Every bit as actionable as it is original, Ken Coleman's strategy dispenses with shopworn myths about résumés and networking events and tells it like it is. At the heart of his argument is a simple but effective equation: get to know the right people and put yourself in the right places, and the opportunities will start presenting themselves.

---
### 1. What’s in it for me? A proven strategy to launch your dream career. 

On average, we spend over 90,000 hours working over the course of our lives. That's almost 4,000 days, or just under a decade's worth of around-the-clock toil. The kicker? Seven in ten workers report that they're unhappy with their jobs. That's a whole lot of folks spending a whole lot of time being miserable!

But it doesn't have to be that way. Ken Coleman knows a thing or two about that. Having spent years among the disgruntled majority, he decided to take matters into his own hands and pursue his true calling in the broadcasting business. It wasn't always plain sailing, but the journey was worth it. Today, he's one of the best-known radio presenters and podcasters in the United States. 

So how did he do it? Call it the "proximity principle," a tried and true playbook of strategies to get yourself around the right people and in the right places to launch a kick-ass career you love. And the best news of all is that this is something you can start doing right now, wherever you are. 

In the following blinks, you'll learn 

  * why you don't need to move to Los Angeles to work in the film industry;

  * how to get the most out of your network of friends and acquaintances; and

  * why asking for help isn't a sign of weakness.

### 2. The proximity principle helped Ken Coleman get himself out of a rut and launch a new career. 

Do you love your job? If your work is challenging, interesting and aligned with your passions and talents, congratulations — you can stop reading right now! If it's not, you're not alone. According to a 2017 Gallup study, 70 percent of the global workforce is dissatisfied. Millions upon millions of folks get up every day dreading what's ahead of them. 

That's not sustainable. Just ask the author. For a long time, he was part of that miserable 70 percent, toiling away in a dead-end job rather than pursuing his dream of becoming a broadcaster. Sitting on the porch looking at the woods behind his house one morning, he was struck by how far away he was from where he wanted to be in life. 

It was in that moment that he had a revelation. _He_ was the only person in the world who could make his dream a reality. After all, other people weren't sitting on _their_ back porches worrying about how they could help him start a new career. Only he could take the first step on his journey. But how? 

That's where the _proximity principle_ comes in. Here's how it works: If you want to do what you love, you have to be around people who are doing it and in places where it's happening. Ken looked through his list of contacts and hit up an old buddy who worked for a leadership training company in Georgia called Catalyst. 

The firm was just getting into podcasting, and when Ken offered to help develop a concept in return for a chance to gain some hands-on experience in their recording studios, they agreed. Ken's first "sound booth" was little more than a repurposed broom cupboard, but that didn't matter a bit — even if it lacked AC to keep the sweltering Georgia summer at bay, it was a start! 

Over the next few years, Ken established a reputation for himself in broadcasting. It wasn't always glamorous, and there were plenty of moments when he felt like calling it quits, but in the end it paid off. Today, Ken hosts _The Ken Coleman Show_, one of America's most listened-to self-improvement podcasts. 

That's all down to the proximity principle. So how can you start using it to achieve _your_ goals? Well, that's just what we'll be exploring in these blinks!

> _"Until one is committed, there is hesitancy, the chance to draw back, always ineffectiveness."_ — Scottish mountaineer William Hutchinson Murray

### 3. Pride and fear are limiting beliefs that hold us back from pursuing our true goals. 

Pursuing your dream job is like climbing a mountain — you need to be brave. After all, nothing looks quite as daunting as that towering peak when you're standing at the base looking up. Lots of people, however, never even take the first step. 

Why? Put it down to _limiting beliefs_. These come in two forms — pride, and fear of failure. 

Let's start with pride. That's essentially the idea that we're self-sufficient and don't need anyone's help. It's a psychological reflex rooted in the fear of appearing weak. We worry that asking for assistance makes us look incompetent and that others will think less of us than if we'd gone it alone.

Take Steve Jobs. He built a multibillion-dollar company and created products that literally changed the world. Look up "ambitious" in the dictionary, and you may well find a picture of Jobs. But he also knew how to ask for help. 

In a 1994 interview, Jobs remarked that he'd never known people to simply put the phone down or say "no" when he called. He remembered that when he'd asked Bill Hewlett, the co-founder of Hewlett-Packard, for spare parts for a school project, Bill had said yes. Jobs always tried to do the same when people called him. But that, he added, only works if you pick up the phone and make the call! 

Fear of failure is the second limiting belief. It's an instinct, and it can be useful if it reminds us of real risks. The problem is that it often becomes irrational. At that point, we stop focusing on mastering the challenges ahead of us and start ruminating on hypothetical "what-if" questions like, "What if someone gives me a break but I mess up?" That's a recipe for inaction. 

The way to beat that trap is to reframe the way you're thinking about failure. Just think of great inventors and scientists like Thomas Edison, Marie Curie and Albert Einstein. They didn't succeed _despite_ failure — they succeeded _because_ of failure. The fruit of their botched experiments? The lightbulb, three Nobel prizes and the theory of general relativity!

Then there's American hockey icon and Hall-of-Famer Wayne Gretzky. If you find yourself worrying about failure, just remember his famous advice: "You miss 100 percent of the shots you don't take."

### 4. Successfully connecting with the right people is all about asking for a chance rather than a favor. 

The proximity principle has two elements — people and places. Put them together and you get an equation: right people + right places = opportunities. That last word is key. What you're looking for isn't a handout but a _chance_. 

Why's that so important? Well, the people who'll help you realize your dream are out there right now — but they're working for themselves, not you. In fact, they're not even thinking about you — they're busy living their own lives and focusing on their own jobs. That means you need to get their attention. And how you do that determines the likelihood of success. 

That's something the author's friend Joy, a publisher, knows all about. Whenever she opens her inbox, there's someone who wants something from her. That's exhausting — it's all take and no give. As a result, she can spot opportunists who want nothing but a free lunch a mile off. When they come knocking, her door stays shut. 

But when someone approaches who is genuinely passionate about the industry and wants to learn something, it stops her in her tracks. In those cases, she's always willing to take some time out of her day and see what she can do. 

She's not the only industry insider who ticks that way. In whatever industry you're approaching people, remember that enthusiasm is infectious — it makes you the kind of person whom others want to help. 

Pure passion, however, isn't always going to cut it. So what else can you offer? Well, how about giving something back? Take it from Patrick. He hated his accounting job and longed to open a brewery and raise money to build wells in impoverished villages in Africa. There was just one hitch: he didn't know how to brew beer in industrial quantities. 

Patrick didn't let that stop him, however. He approached a local brewery with an offer: if they taught him the tricks of the trade, he'd work for them for free. He spent over a year working a five-day week without earning a dime, and often even came in on weekends. 

Was it worth it? Ask Patrick, now the owner of a successful brewery, who'll never need to do anyone's accounts again but his own!

> _"If you want people to help you, you need to be the kind of person people want to help_."

### 5. Imitating established professionals in your field is a great way to learn and develop your own style. 

The great American poet T.S. Eliot once said that "talent imitates; genius steals." What he was getting at is that the most gifted people in any field rarely develop in isolation; their craft is a grab-bag of skills and techniques they've copied, borrowed and filched from established masters. 

So here's a question for you to think on: Who are the best people to imitate in _your_ industry? Whoever they are, it's likely they fall into a single category — professionals. 

Let's start with a definition. Professionals have two defining features. First off, they have bags of experience. As a rule, that means they have worked in their fields for at least ten years. If getting to the peak of your dream career is like scaling a mountain, then pros are the guys who've already navigated to the summit and know every path, ravine and slope like the backs of their hands. 

The second trait you'll find in every professional is that they learn from each other. Take the actor and Hollywood star Leonardo DiCaprio. If you watch his performances carefully, you'll notice that he often copies the mannerisms and timing of another, older actor — Paul Newman, the legendary leading man from classic movies like _The Color of Money_. 

That covers _whom_ to learn from — now it's time to talk about the _how_. That's a two-stage process, and it starts with picking up the tricks of the trade — the skills, hacks and techniques top professionals use to stay at the top of their game. 

That's just what American basketball great Kobe Bryant did. Growing up in a sports-obsessed household, he watched endless videos of contemporary players and older stars as he was starting out. They influenced his own way of playing. His style was a mishmash of techniques learned from others; he used Oscar Robertson's hesitation moves, Elgin Baylor's footwork and Jerry West's quick-shooting release. 

That brings us to the second step — developing your own method. Bryant used other players' signature moves, but his style was greater than the sum of its parts — after all, no one else had ever combined them in the same way as Bryant. And that's the key: when you're learning from multiple pros, you're much more likely to create your own unique approach!

### 6. Building your dream career takes time, but you don’t have to move a thousand miles to start. 

Ask any experienced climber and they'll tell you that scaling a mountain can't be rushed. Before you even set foot on Mount Everest, you need to set up a camp in the foothills and acclimate. Skipping that step is guaranteed to cause altitude sickness, meaning that when you begin the ascent, you'll feel dizzy and struggle to keep your focus. 

That's also true when it comes to getting your dream job. Just ask the author. When he started broadcasting, he often longed for a shortcut. No wonder; no one actually _wants_ to intern on a cold Friday night, reporting on a high school game to an audience of 26! Looking back, however, Coleman could see that those evenings prepared him for what lay ahead. If you can't handle a bit of discomfort early on, you'll never be ready for bigger challenges further down the line. 

Growth, in other words, can't be forced. Go back far enough and you'll find that today's top professionals all have similar experiences behind them. Take Katie Couric. In 2006, she became the first woman ever to present the CBS evening news show solo. That was the product of thirty years of hard graft, beginning with a lowly desk assistant position at ABC.

But what about folks who didn't get started in the industry they now want to work in a good three decades ago — surely it's much harder for them, right? Well, not quite — in fact, you can start from where you are. 

Let's unpack that. A lot of people put off pursuing their dream jobs because it seems downright unrealistic. Brad, a caller who appeared on Ken's podcast, was one of them. He wanted to work in film, but there was a problem — to get a start in that business, he'd have to move to LA. That would mean selling his house in North Carolina and uprooting his wife and their kids. He just couldn't do it. 

Ken asked Brad a simple question: How many production studios did he think there were in Charlotte, North Carolina? Brad paused before admitting he didn't know — he'd never thought to find out. A little research answered the question: there were over 100! And that's where Brad started his job search. Eight weeks later, he landed a job as a production assistant at a studio just a few blocks from his house. 

Call it the _law of the zip code_. Everything you need to get started is within your reach — you just need to look for it.

### 7. Weak social ties are a better bet than friends and family when it comes to finding your next job. 

Landing any job, let alone your dream gig, can be a tricky and time-consuming business. Chances are you've experienced what it's like to send out hundreds of carefully prepared résumés and fail to get a single bite. If you know the right person, however, it's as easy as pie. When it comes to the job market, connections matter. 

So who is the "right" person? Well, it's not whom _you_ know that counts, but whom _they_ know. That's the conclusion of a 1973 article by Stanford sociologist Mark Granovetter. According to Granovetter, most of the job opportunities that come our way aren't from family members and close friends — our so-called _inner social circle_ — but from people we don't personally know in our _outer social circle_. 

That's hardly surprising when you compare the relative size of these two groups — after all, there are simply far more people in the latter group. But here's what _is_ surprising: connecting with folks in that second category boosts your chances of getting a job by a whopping 58 percent! 

It's a massive resource, but how can you start tapping into it? The answer is relatively simple. Let's break it down into three stages. 

First, you'll want to tell friends and family members about your dream job. These are the people who know you best and care most about your happiness and success. If they know someone who might be able to help you, they're sure to reach out and organize a chat. 

Next, you'll need to make a list of your acquaintances. The easiest way to do that is to create "buckets" into which you can place potential leads. These can be organized into categories like coworkers, schoolmates or people you've met at social events. Think back: Could a former colleague hold the key to your search? How about someone at your tennis club? Or does your neighbor maybe work in an adjacent industry? 

The last step is the most intimidating — actively making connections. But remember what we said about Steve Jobs — if you don't pick up the phone, the person on the other end of the line won't ever have a chance to say "yes." You can also make things easier on yourself by keeping things low-key. You don't have to organize a formal meeting — if you think you've found someone who can help, invite her for a coffee or a light lunch. 

That brings us to the end of our journey — and to the start of yours. Changing your career path and fulfilling your true ambitions are daunting prospects, but it can be done. Start applying the proximity principle today, and there's no telling where you'll be tomorrow!

### 8. Final summary 

The key message in these blinks:

**Landing your dream job doesn't mean you have to move halfway across the world — in fact, everything you need to transform your career prospects is right in front of you. The key? Putting yourself around the right people and in the right places. Call it the proximity principle. And that's something you can start doing right now by making the most out of your social connections, learning from the pros and connecting with your industry's movers and shakers.**

Actionable advice:

**Apply for positions that offer a clear path forward**

Reaching the peak of your dream career is a journey that takes time and a lot of personal development work. That means it's a good idea to plan ahead and focus on positions that can accommodate that future growth. Take the author's friend Sean. His dream was to be an English scholar. He knew he couldn't walk straight into a full-time professorship, however, so he looked for universities with professors who were nearing retirement age and that also had growing enrollments, indicating good future prospects. So next time you're looking at a company, make sure to check out who's above you in the pecking order — after all, _that's_ the job you'll ultimately be going for! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Do What You Are_** **, by Paul D. Tieger, Barbara Barron and Kelly Tieger.**

Right, you now know how to put yourself around the right people and in the right places to land a job you'll love — that's great news! But here's the thing: that only works if you already know where you want to go. And figuring that out is often the biggest challenge of all. 

Sound familiar? Well, don't worry — there's help at hand in the form of a classic career guide that has helped millions of readers find work that suits their unique personality types. If you'd like to join them, why not check out our blinks to _Do What You Are_?
---

### Ken Coleman

Ken Coleman is a career expert, podcaster and radio presenter. He is the host of _The Ken Coleman Show_, a popular program that helps people discover their true vocation and turn their careers around. Coleman is also the author of _One Question_.

