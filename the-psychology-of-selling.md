---
id: 5794a586115d370003373e2d
slug: the-psychology-of-selling-en
published_date: 2016-07-29T00:00:00.000+00:00
author: Brian Tracy
title: The Psychology of Selling
subtitle: Increase your sales faster and easier than you ever thought possible
main_color: CF544C
text_color: B03831
---

# The Psychology of Selling

_Increase your sales faster and easier than you ever thought possible_

**Brian Tracy**

_The Psychology of Selling_ (2004) will school you in the psychology of consumption. These blinks reveal the techniques used by outstanding salespeople — techniques that you can use to increase your sales performance in any market.

---
### 1. What’s in it for me? Use little brain hacks to get ahead in sales. 

Sales: either you're a natural or you can't do it at all. And that's all there is to it, right? Some people are just born with total confidence, perfect communication skills and the uncanny ability to convince others to buy what they're selling. The rest of us just aren't.

But is this true? Of course it's not.

Anyone can be a good salesperson. You just need to learn how. These blinks, based on the thinking of one of the most successful sales gurus in the world, give you a brief overview of the skills every sales professional needs to know. If you want to outstrip your competitors at work and increase your commissions, then check out these blinks.

In these blinks, you'll find out

  * why the subconscious can be your best sales weapon;

  * why a good salesperson is a lifelong learner; and

  * why you won't get much from a fearful prospect.

### 2. Motivate your subconscious for successful sales. 

What's on your to-do list right now? Probably a few mundane errands, like taking out the garbage or remembering to grab toilet paper on the way home. But did you know that the list isn't just a household fixture, but a great tool for salespeople, too?

To-do lists are a surefire way to harness the power of your subconscious and achieve your aims. The subconscious plays an important role in the job of a salesperson anyway — just think of how intuitive reactions to facial expressions and body language allow a salesperson to keep a prospect engaged. By writing stuff down in a simple list, you give your subconscious a _framework_ to work with.

Start by writing a list not of your goals as a salesperson but of the reasons _why_ you want to achieve these goals. The longer the list is, the more motivated you'll be. Why? Well, each reason is a piece of _ammunition_ for your subconscious mind.

Take, for example, a sales manager with just two reasons for achieving his sales goals: saving for a new car and taking a road trip through the mountains. Meanwhile, another sales manager can name dozens of reasons, from renovating parts of his house, to getting a dog for his kids, to taking a family holiday through China, and more. While the first salesperson will find himself easily discouraged, the second will be _unstoppable._

These motivational lists are just one element of a salesperson's toolbox, just one of the techniques he uses to get his subconscious working for him. In the next blink, we'll look at another technique: self-esteem.

> **_"_** _Only about 3 percent of adults have written goals. And these are the most successful and highest-paid people in every field._ ** _"_**

### 3. Boost your self-esteem to improve your sales performance. 

Here's something for you to try tomorrow morning: look in the mirror and tell yourself that you're the best sales manager in the country. Yes, this is totally cheesy. But doing it — even though you'd be embarrassed if anyone saw you — is worth the benefits it brings.

Every statement you tell yourself gives your subconscious mind a mental picture, which it will then try to make real. It's easy enough to see how this could work against you. Have you ever told yourself you're an idiot for messing up, and then found yourself making the very same mistake again and again?

But it can also work in your favor. By simply switching your self-talk from negative to positive, you can improve your mental picture of yourself. A sales manager who repeats positive affirmations — "I am calm, confident and powerful" — will soon come to see himself this way. His subconscious will then encourage him to react to stressful situations as if this were true, further confirming his positive self-image.

This is particularly useful when you're gearing up to handle an upcoming sale. Great salespeople recall the best sale they ever made, getting themselves in the zone where they can repeat that success again. Mediocre salespeople, on the other hand, dwell on their worst sales experience before an upcoming sale. He'll find himself stuttering during the pitch and stressing out; the superior salesperson, in contrast, breezes through the presentation with confidence.

### 4. Surround yourself with people that share your passion and drive. 

Believe it or not, the learning process doesn't end when you graduate from college. We all begin our lives with a limited amount of practical knowledge, so it's our job to teach ourselves as much as possible. Those who fail to do so will quickly fall behind!

So, set yourself the goal of learning one new thing every day, even if it's just something you heard on the radio. Then put this new piece of knowledge to use as soon as you can. By learning new things within the area of your specialty each day, you'll be one step closer to achieving excellence.

One salesperson, a client of the author, listened to an audio program every day on the way to work, which offered advice on how to boost self-esteem, organize a day, lead your career and develop a strategy of self-presentation. Once he arrived at work, he'd apply the new lessons he'd learned during his commute in practical situations. The result? He nearly _doubled_ his sales!

You can also learn from those around you. Find your _reference group_ — a network of people who share your values — and spend time with them. Their accomplishments will lift you up, too. Imagine a sales manager was always mediocre in his role. He might realize that his reference group was filled with other negative and unmotivated people and know that he needed to seek out different company. By associating with top salespeople, asking for their advice and practicing their techniques, his confidence and sales ability would surely improve massively.

### 5. Ask questions to uncover your prospect’s needs and tailor your pitch accordingly. 

Say you'd like to buy a product. Yet, when you go and ask a salesperson some questions about it, they seem to be more interested in talking about everything they know about the product, rather than how the product would benefit you, the customer. Would you buy it? Probably not.

Customers won't be interested in the product's history or any of its special features until they know _what's in it for them_. Great salespeople know that customers will buy if you can demonstrate to them why they _personally_ need the product.

For instance, a salesperson presenting a car could talk about its color or the great features of its engine for hours on end, but you won't feel convinced if he doesn't bring you into the picture, too. Only by underlining how the car's features will benefit _you_ — how little gas it consumes, what a large trunk it has and how practical the parking warning system is — can the salesperson show you that their product has what you want.

But what if a prospect has no idea what they want? Or what if it's not clear to you as a salesperson? Well, you've got to start asking questions. Why are they searching for a particular product? What do they hope to gain from it? If a family is hoping to buy a house, are they interested because of a great price? A great school nearby for their children? Or more space to start their family?

Keep asking questions, and soon enough you'll uncover what your prospect is really after. This in turn allows you to perfectly tailor your presentation to _them_, which will lead to a convincing pitch and a successful sale.

### 6. People buy products in search of social recognition. 

Consumption is what happens when someone goes out and buys something, right? And usually, consumers pick the cheapest product, or at least the one that gives the biggest bang for their buck. Well, actually, it's not that simple.

In order to gain recognition — for our status, influence, power, personality or some recent transformation — we go out and buy products. Many people are more concerned with how a product will communicate their social status than with its monetary value.

For the salesperson, it is important that you recognize what _emotional value_ (the appeal in terms of status) a client wants — then you can offer them the best option.

Imagine a salesperson working in a department store. A client comes in carrying an expensive Louis Vuitton bag, asking to see your watch selection. Recognizing that the prospect probably isn't all that worried about the price of the watch, and is rather more interested in its emotional value as a symbol of status, a good salesperson will take them over to the Rolex section. Then he or she will spend the time explaining just how special, well made and desirable these watches truly are.

### 7. Gain the trust of customers by showing them that you care about their needs. 

Know that feeling right before a making big purchase, when you start to get cold feet? We all feel hesitant from time to time when making decisions with our money. But why?

Because when we buy, we give a quantified amount of our freedom and financial security away. We trade financial safety, in other words, for a product we desire. If you want your prospect to still buy in spite of this, there's another aspect of the buying experience you need to bring to the fore — _emotional anticipation_.

A University of Chicago study illustrated how people tend to buy because of the powerful emotional anticipation of owning and using the product. Ensuring that this feeling outweighs the customer's fear of financial loss gives the salesperson a much higher chance of success. You can also heighten a customer's sense of security by offering money-back guarantees. But whatever you do, it must be _believable_.

It doesn't matter how much you genuinely care about your product, service or brand — the prospect will always doubt your motives. In order to win their trust, you'll need to show that you have credentials, that you're reliable and have made excellent choices in the past.

Say that a family wants to buy a dog. This is a big decision for any family, and requires careful consideration of the type of dog that would suit the family best. An easily trainable and child-friendly dog, one that could spend time alone while kids are at school, would be the best choice.

The family hands over the challenge of choosing the right dog to the salesperson, trusting that they'll do a good job. By demonstrating your trustworthiness and authenticity, you'll gain their trust and gratitude, and they'll doubtless seek out your services — or recommend them to friends — in the future.

### 8. Final summary 

The key message in this book:

**Your subconscious and self-esteem play important roles in your sales ability. By motivating yourself and learning from others, you can start to truly understand your prospects. Demonstrating your trustworthiness and ability to meet their needs will establish trust. Do these things, and you'll be well on your way to achieving top performance in sales.**

Actionable advice:

**Make a list!**

Grab a pen and paper and write down all the things you want to achieve. Include everything. You want to buy some new socks? Write it down. Want to refurbish the kitchen? Write it down. Learn Mandarin. Write it down. Buy a jet? Well, you get it. These are the things that will keep you inspired. These are the things you're working toward. They'll keep you going when times get tough.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Influence_** **by Robert B. Cialdini**

_Influence: The Psychology of Persuasion_ explains in detail the fundamental principles of persuasion that get us to say yes, including how they are used against us by compliance professionals like salesmen, advertisers and con artists. Knowing these principles will allow you both to become a skilled persuader yourself and to defend yourself against manipulation attempts.
---

### Brian Tracy

Brian Tracy, a Canadian-born US author, has written many best-selling nonfiction books, including _Focal Points, Goals, Create Your Own Future_ and _Million Dollar Habits_. He is also a professional speaker and sales trainer.

