---
id: 53032f6e64313100081a0000
slug: the-psychology-of-winning-en
published_date: 2014-02-19T08:07:05.000+00:00
author: Dr. Denis Waitley
title: The Psychology of Winning
subtitle: Ten Qualities of a Total Winner
main_color: CD6662
text_color: 943E3B
---

# The Psychology of Winning

_Ten Qualities of a Total Winner_

**Dr. Denis Waitley**

_The Psychology of Winning_ (1985) reveals the secret to becoming a winner in life. The author argues that there are ten qualities that a person must possess if he wants to achieve this positive, confident lifestyle. Each of these qualities is explained with the help of tangible real-life examples and accompanied by techniques that can help anyone adopt them.

---
### 1. What’s in it for me: Learn how to become a winner in life who's popular and happy 

In today's competitive world, the gap between "winners" and "losers" is becoming ever clearer. This gap does not merely refer to their success in life, but also their attitude toward life and how much they enjoy it. Losers retain so many losing habits that their lives are dominated by fear, regret and aimlessness.

Thankfully, _The Psychology of Winning_ explains how anyone can become a winner.

Firstly, it is important to understand what winning really means: developing a positive lifestyle that benefits both winners and others around them.

To adopt this positive lifestyle and become a winner, you simply have to develop certain winning habits. For example, you must learn to change your self-image, as it will translate into real-life change as well.

Once you have these habits you will find yourself living a more successful and positive life. But still, you must constantly practice your winning habits to retain them and not slip back into a loser lifestyle.

### 2. Winning starts with self-projection: using the power of your mind to achieve what you envision. 

When you were a child, did you ever dream about becoming, for example, a teacher, doctor, or astronaut?

Interestingly, we usually do go on to become the make-believe selves we fantasized about the most. 

For example, since childhood Neil Armstrong dreamed of doing something important in the field of aviation, and he famously went on to become the first person on the moon.

The first step to becoming a winner is _projecting_ the image of yourself that you want to attain. Practice this on a daily basis. Remember, if you feel good, you'll look good and do good. 

This is because our mind has tremendous power and can greatly influence our actions — more so than we think.

For example, one study showed that even the most effective people utilize less than ten percent of their mental capacity. Further studies concluded that the creative capacity of the human brain may ultimately be infinite.

Such findings indicate that we have at our disposal an untapped and limitless resource to become more creative, inventive and successful people.

This means that winning is also mostly about having the _right attitude_ so you can tap into that potential.

Winners are not always the ones who have the best genetic features or live in a good neighborhood — instead, many of them spring up out of adversity.

For example, O.J. Simpson was a just child living on the street who'd been diagnosed with a crippling disease. Yet with his determination he managed to become a football star. 

A winner is a person who manages to harmonize their mental state, their environment and their physiological state. The result is that they usually get what they want from life. 

So the good news is that everybody can be a winner; the process starts in your mind.

### 3. Winners develop a positive lifestyle that helps them and the people around them. 

Winning is a way of living that creates a better life for us. When an individual understands and applies the principles of winning in life to benefit both herself and those around her, she is bound to feel good, think constructively and look great. Great things will therefore happen to her.

Winning is about developing a new, positive lifestyle where so-called _losers' habits_ are replaced with winning ones.

Some examples of losing habits include self-criticism, smoking, procrastination, laziness, anxiety and depression. Winners aim to remove such habits and replace them with winning habits like exercising and enjoying life. They acquire these winning habits through daily practice. 

By acquiring this positive lifestyle as well as setting and achieving goals, winners not only benefit themselves, but also the people around them.

In the blinks that follow, we will discuss some of the methods with which anyone can form the habits of a winner.

### 4. Winners have clearly defined goals as well as a purpose in life, and they tend to get what they expect. 

Most of us know people who seem to be adrift, lacking any clear path or direction. Winners, on the other hand, are like torpedos, homing in on their targets with merciless conviction.

Winners set clear goals for themselves and tend to consistently get what they want. If you ask them, winners are always able to tell you exactly what they are doing and where they want to be.

And how do they do this?

Winners start by setting themselves lifetime goals and breaking them down to specific and prioritized ones. This is because the human brain requires specific data to function properly.

After setting a target for themselves, winners constantly monitor the feedback they get from around them, making the necessary adjustments and corrections to stay on target and score a hit.

Winners also have a definite _purpose_ in life — something they strive for. This purpose gives them strength when they face adversity and challenges in life.

This effect is demonstrated by the findings of Victor Frankl, a psychiatrist who survived the Nazi concentration camps during World War II. He noticed that the men who survived the brutal conditions of the camps were the ones who had a clear purpose for their existence. This purpose was, for example, something they wanted to do, someone they loved or a place they wanted to see. Those who had no such beacon of hope or purpose tended to perish.

Unfortunately, even a strong purpose in life was rarely enough to survive the camps, but in less extreme contexts your mindset really can be crucial: In the long run what people expect tends to be what happens. This is illustrated by a study which showed that mere thoughts can actually have an impact on one's physiology, bringing either disease or good health.

A famous example of this is the concept of "Voodoo Death," where a person dies simply because they expected it. Cases exist where someone has died, seemingly of fright, simply because a fortune teller told them they would die at a certain age.

### 5. Winners motivate themselves by focusing on the positive impact of their actions. 

When facing a challenge most of us tend to feel some inklings of self-doubt. But winners always make sure that their mind is full of positive thoughts.

This is because motivation is driven by your emotional state, and the two key emotions that dominate human motivation are fear and desire.

Winners are motivated most by a desire to attain the goals they have set. Desire is an emotional state that exists between where you are and where you want to be. It produces positive tension. According to Victor Frankl, the concentration camp survivor, people need to strive and struggle for a goal that is worthy of them — otherwise they lose their will to live.

Winners know that whatever fills their mind will influence their actions, so they ensure their mind is full of positive things: solutions instead of problems. 

For example, the golfer Jack Nicklaus ensures he is always thinking about "what should go right" when he starts a swing. He intends to hit accurate, good shots and this idea fills his mind so that there's no room for negatives. 

Winners also tend to see risks as opportunities. They focus on the rewards of success rather than fear the penalties of failure.

This is illustrated by the words of Maurice Chevalier, a French actor who overcame his fear of failure by braving the stage despite his anxiety: "If you wait for the perfect moment when all is safe and assured, it may never arrive. Mountains will not be climbed, races won, nor happiness achieved."

So remember, winners are positive and take their chances fearlessly!

### 6. Winners succeed by understanding that they themselves determine how their lives turn out. 

Have you ever heard someone mutter in a disheartened manner: "I'll just see what happens"? This kind of attitude highlights a crucial difference between winners and losers: losers _let_ it happen, winners _make_ it happen.

Winners believe in their own power of self-determination — that they themselves can shape their own destinies. 

Consider the philosopher Voltaire, who compared life to a game of cards. Each player must accept the cards life deals, but after this, it is up to the player to decide how to play the cards in order to win the game. 

For example, children can learn at a very young age that they can control their parents by crying, whining or throwing temper tantrums. But it is up to the parents to decide whether they will be subordinates of their children or steer their lives in another direction. 

Believing in and strengthening your own self-determination is beneficial because it produces better physical and mental health. 

On the physical side, research in biofeedback and meditation has verified that through high self-discipline and specialized training it is possible to control things like our brain wave frequencies, pulse rate, threshold of pain and other body functions, and this has a positive effect on our health.

On the mental side, people who feel forced to do things usually lose their control in life. They start being irresponsible and lose a sense of their values, which can lead to abnormal behavior, neuroses and mental deterioration. 

Life provides you with many possible paths to follow, but it is you who must decide which one is best for you. Take action and proudly proclaim, "I decide to do this."

### 7. Winners have excellent self-awareness and are able to easily empathize with others, making them very adaptable. 

We all know people who lie to themselves.

But winners don't do this; they are always honest with themselves. They understand and accept their potential, and are honest about the time and effort it will require to achieve what they want. A winner thinks, feels and acts in a manner consistent with who they are and what they want. This is honesty.

Winners are also empathetic to others. This is because they are open-minded, which helps them accept the fact that everyone is unique and also entertain the point of view of other people. Losers, on the other hand, are narrow-minded.

An example of the importance of empathy is the story of a mother who took her five-year old son Christmas shopping. The son was crying the whole time and desperately clinging to his mother, who did not understand what the problem was as she felt that the shopping was fun.

When the mother knelt to tie the boy's shoelaces, she suddenly understood: From her son's perspective, the shopping environment looked terrifying: full of giants scurrying about everywhere.

The mother took the boy back home and promised not to impose her version of "fun" on him ever again.

This kind of empathy is important because it helps winners to be _adaptable_. Adaptability means the ability to cope with or adapt to the rapidly changing environment around us. To be adaptable, we must be able to anticipate the probable actions of others through empathy and by remaining open and flexible.

To develop adaptability we must accept the stresses of life as a normal phenomenon. Famous medical researcher Hans Selye suggested that people need to find their own, individual healthy stress level and try to maintain it. It is important to live so that this level is maintained, because it enables us to challenge ourselves, which is a crucial way to develop one's potential.

### 8. Winners have high self-esteem; start developing yours too. 

The number one difference between a winner and a loser lies in their self-esteem.

Winners have developed a strong sense of self-esteem and have high self-confidence. This is confirmed by several studies: highly successful winners like Benjamin Franklin, Thomas Edison and Hellen Keller all had high self-esteem as their common denominator. This could be seen in the way they were comfortable with their own uniqueness and the way they understood that others should accept them just the way they were.

Such people naturally attract others, gaining friends and supporters easily. They seldom have to stand alone.

So how can you strengthen your self-esteem?

You must practice _positive self-talk_. Winners give themselves constructive feedback like "_I can,"_ "_I look forward to_ " and "_I'm feeling better,_ " to shape and increase their self-esteem. This kind of talk feeds our subconscious self-image positive thoughts about ourselves and our performance. A study on the physical effects of words and images revealed that words can affect your body by, for example, helping you to relax.

Another way to develop and maintain high self-esteem is to concentrate on your own internal reactions to events rather than the external stimuli that cause them. For example, you should not react emotionally to external events, because rational thinking is far more powerful. While winners have emotions and enjoy their intensity, they only make decisions based on logic and common sense.

If you're dissatisfied, you too should endeavor to change your internal reactions rather than focusing on changing your external environment. Try to enjoy your current profession rather that looking elsewhere. For example, if you're unhappy in your current job, you should try to understand and change your internal reaction rather than immediately looking for a new profession.

So in conclusion love yourself, encourage yourself and think logically!

### 9. You can change your own self-image to change your behavior. 

In order to understand human behavior you must understand the fundamental role that self-image plays in it.

Firstly, you must realize that people don't actually act in accordance with reality, but with their _perception_ of reality. Scientists agree that our nervous system cannot tell the difference between an actual experience and experience imagined vividly, emotionally and in detail.

This means that who you really are does not influence your behavior as much as who you think you are. One study showed that when some students in a class were arbitrarily labeled as having greater learning ability, they showed significantly higher achievement levels than their "weaker" classmates. Though not based on fact, their perception of their own abilities affected their behavior.

The implication of this is that changing your self-image can have a real-life impact, because an individual's behavior, personality and achievement level are all usually consistent with the self-image. 

So how can you improve your self-image?

You need to consistently see yourself as a winner. Feed your mind positive and vivid images in which you have achieved your goals. 

Your subconscious will then turn this new and improved self-image into action. This is because the subconscious automatically strives for any objectives or goals we set for it, whether positive or negative, true or false, right or wrong. It has a great influence on our behavior.

Remember, what you see is what you get, and what you see in your mind is what you will become. 

Next, you'll find out how to retain these winning habits.

### 10. Winners strengthen their self-discipline so they can stay committed to their winning habits. 

By now, you're probably eager to pick up some winning habits. But beware — without self-discipline it is impossible to learn or maintain them.

So how can you strengthen your self-discipline?

One way to strengthen self-discipline is by performing concentrated _self-talk_, where you try to inject thoughts and emotions into your mind so that they change the current state of your subconscious.

This will help you change your self-image. An example of time-grown self-image can be seen in a study of amputees: in the first few weeks after their surgery, amputees often "feel" something in limbs that are no longer there. But with self-discipline and self-talk spanning several weeks, they do adopt and accept their new self-image. 

Another method to strengthen self-discipline is _mental simulation_ : imagine the task at hand in your mind before doing it. This technique is much-used by professional athletes, who vividly imagine their entire performance from beginning to end before undertaking it.

For example, French skier Jean-Claude Killy was wildly successful in the giant slalom because he won the events in his imagination before even putting his skis on. He imagined his feet together, weight properly balanced, his knees correctly flexed as he zoomed toward the finish line feeling the air, snow, speed and exhilaration of doing it in real life.

Musicians also employ this technique, sometimes humming as they imagine playing their instruments.

Another prominent example is astronauts: they get used to the feeling of weightlessness by bobbing up and down in a rubber raft at sea and simulate lunar conditions by practicing in the desert. Neil Armstrong actually mentioned this when he first landed on the moon: "It was beautiful, just like our drills."

In addition to these advanced simulators, mental simulation is also a great way for anyone, even a complete novice in the technique, to conquer their fears and strengthen their mental discipline.

### 11. Winners succeed in every sphere of life, live in the present moment and create more winners around them. 

The final thing to understand about being a winner is that as a winner you are not independent of others and your surroundings. Winners see the big picture and understand that they are a part of the universe.

What does this mean?

First of all, a winner is a _Total Person_, meaning that they win in every sphere of life: at home, professionally and spiritually. They win on a communal, national, global and even universal level.

They do this by earning the love and respect of others, starting with their family. Winners build their family into a winning team, valuing their relationships and spending time with them. 

Secondly, winners also create other winners. They do business with other people, benefiting, not exploiting them, thereby making them winners too. Losers, on the other hand, are worried they will be exploited, so they try to exploit others first.

Finally, winners savor each and every moment in life because they have a keen awareness of the value of time. They live in the present. They don't dwell on the past, but rather learn from it, so that they don't repeat their mistakes. They also savor and enjoy the memories that brought them happiness. 

For winners, the future is specific and foreseeable with clearly defined goals that give their everyday activities richness and purpose. Winners do not fear their death in the future, because they understand the mortality of their bodies.

So the final benefits of being a winner are that you enjoy your life and create more winners around you.

### 12. Final summary 

The key message in this book:

**Anyone can adopt the confident, positive lifestyle called winning. You start out by crafting an image of the person you want to be and adopting this as your self-image. You can then adopt certain winning habits to bring out the best in you. It will not only help you lead a better life, but also those around you**

This book in blinks answered the following questions:

**What is winning?**

  * Winning starts with self-projection: using the power of your mind to achieve what you envision.

  * Winners develop a positive lifestyle that helps them and the people around them.

**What habits make a winner?**

  * Winners have clearly defined goals as well as a purpose in life, and they tend to get what they expect.

  * Winners motivate themselves by focusing on the positive impact of their actions.

  * Winners succeed by understanding that they themselves determine how their lives turn out.

  * Winners have excellent self-awareness and are able to easily empathize with others, making them very adaptable.

  * Winners have high self-esteem; start developing yours too.

  * You can change your own self-image to change your behavior.

**How can you retain the habits of a winner?**

  * Winners strengthen their self-discipline so they can stay committed to their winning habits.

  * Winners succeed in every sphere of life, live in the present moment and create more winners around them.
---

### Dr. Denis Waitley

Dr. Denis Waitley is a motivational speaker, consultant and author. He has written over a dozen books and sold over 10 million audio versions of his work. His other bestsellers include _Being the Best_, _The Winner's Edge_ and _Seeds of Greatness_.

