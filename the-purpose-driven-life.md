---
id: 56a6ab95fb7e870007000014
slug: the-purpose-driven-life-en
published_date: 2016-01-27T00:00:00.000+00:00
author: Rick Warren
title: The Purpose Driven Life
subtitle: What on Earth Am I Here For?
main_color: 92A8A5
text_color: 2C524D
---

# The Purpose Driven Life

_What on Earth Am I Here For?_

**Rick Warren**

_The Purpose Driven Life_ (2002) shares the Christian answer to that age-old question: why am I here? From finding moments of worship in daily routines to seeking out a supportive community and letting the Holy Spirit guide you through tough situations, these blinks are an engaging guide to life as a Christian today.

---
### 1. What’s in it for me? Find your life’s purpose – it’s already there, waiting to be discovered. 

What does it mean to be a Christian? What does God want you to do? Maybe you are one of those people who as a child was baptized, went to Sunday school and believed in a benevolent, fatherly god. But, as you grew older, that god somehow disappeared out of your thoughts and your practices, leaving behind a gaping void.

Maybe you spend a lot of time and energy trying to frantically fill that void, creating some artificial purpose for your life. But actually, from the moment you were born, your life has been rich with purpose. These blinks describe the five purposes of every Christian's life.

In these blinks, you'll learn

  * how one young Soviet writer found God on the metro;

  * why, in many ways, you should live your life like Noah (minus the ark-building part); and

  * what the wise King Solomon said about workaholics.

### 2. Let God into your life to discover your purpose. 

Clichéd as it is, just about all of us have stared up at the night sky and pondered the meaning of life. And for good reason — the meaning of life is one of the toughest questions faced by mankind. Why, after all, are we here?

The Christian answer to this question is well-known: we were created by God, to serve him. But what exactly does this mean for you and me?

Well, let's make one thing clear first: God was not _obliged_ to create you. Instead, he happily _chose_ you! The simple fact of your existence is God's delight. You were not created by accident. You were created deliberately, _purposefully._ This applies to every aspect of your life: all your talents and experiences that make you who you are, as well as the people and things that matter to you. You see, God shaped every one of us for our unique ministry to him. And this brings us to one of the five purposes in every Christian's life: to find out about this ministry in God's big plan and to embrace it.

This is something to remember the next time you feel insignificant in the world. When you feel as though you're surrounded by chaos, it often helps to look for God to help make sense of things. This is what Russian novelist Andrei Bitov discovered.

For years, he'd been living in despair. Identifying as an atheist, he saw life as completely meaningless. There was no other way to look at it. Or was there? On an ordinary day when Bitov was riding the metro, a phrase popped into his head: "Without God, life makes no sense."

Bitov describes how suddenly everything became clear. Walking out of the metro and into the light, he was astonished. His misery melted away and he made the decision to dedicate himself to God.

Bitov is among many who serve God each day by worshipping him and the things he has created. Yet, not everybody worships with integrity. Worshipping God isn't as simple as following a formula. "These people come near to me with their mouth and honor me with their lips, but their hearts are far from me. Their worship of me is made up only of rules taught by men." (Matthew 15:8) In other words, you must be _sincere_ and let your innermost desire to serve God shine through.

Turns out there are more ways to worship than sing hymns: scripture reading, confession, reflecting on a sermon or even admiring a beautiful landscape are all ways of worshipping. Find out in the next blinks how you can get the most meaning out of these experiences.

> _"Because of his love God had already decided that through Jesus Christ he would make us his children — this was his pleasure and purpose." Ephesians 1:5_

### 3. Even everyday moments provide opportunities to show our love for God. 

Another purpose in the life of every Christian is to worship God to please him.

And worshipping doesn't just happen in church. It should be the thread that binds the hours of your day, and the days of your week. But in this busy world, it might seem like there's hardly any time to dedicate to God in our fast-paced everyday routines.

Not necessarily! Worship is all about being grateful for what you have. From opening your curtains to welcome the light of a new day into your home to taking a moment to look lovingly upon your family at dinner, even the most mundane of tasks presents the opportunity to give thanks to God in your own unique way.

"Love the Lord your God with all your heart and with all your soul and with all your mind. This is the first and greatest commandment." (Matthew 22:37) By keeping this in mind, opportunities to show your love for God will pop up where you least expect them. These habits will help you keep positive energy flowing in your daily and weekly rhythms.

Of course, it's easy to maintain your devotion when things are going well. But what about times of challenges, suffering and hardship? In moments when you feel it's not worth keeping the faith, you need to hold on even tighter.

Consider the story of Noah. His huge ark took him 120 years of hard labor to build. Not to mention that his peers criticized and ridiculed him all the while. But, when the flood came, his hard work paid off. The story of Noah shows us just how crucial and unshakeable true faith is.

### 4. Strengthen your faith in the company of fellow believers. 

So far, we've explored the role of faith in our individual lives. But Christianity is not something we pursue alone — far from it.

When we were born, we entered into our human families. And as Christians, another life purpose we share is to enter the family of God in the same way, by developing our faith in Jesus Christ and building relationships with God's family. What do you gain from joining God's family? Well, you'll have a way to stay with God forever. You'll have opportunities to learn to live the way Christ did. You'll find yourself in a meaningful role in the community. Finally, you will become closer to Christ than ever.

So how do you join God's family? By attending church regularly, of course. Spending time with fellow believers, you'll strengthen your faith and your ability to remain on the right path. Time is precious, yet we're surrounded by temptations to spend it wastefully, say, by heading to bars to drink on weekends. Why? Because it's what your friends are doing.

But what if you had a friend who spoke of all the great lessons he'd learned at last week's mass? Perhaps you'd prefer to go to church next Sunday with him, too.

> _"Encourage one another daily … so that none of you may be hardened by sin's deceitfulness." Hebrews 3:13_

### 5. Live purposefully by spreading your faith, but stay wary of envy. 

Throughout the centuries, Christian missionaries have dedicated themselves to spreading the word of Jesus Christ. That's no easy task! Nevertheless, God wants you to become a missionary, too. That's another one of the five purposes he has for you.

This doesn't mean traveling all around the world! Rather, being a missionary starts with accepting your role as representative of Christ. Peace between all men and women around the world is God's number one goal — and he can't achieve this without your help:

"God uses us to persuade men and women to drop their differences and enter into God's work of making things right between them. We're speaking for Christ himself now: Become friends with God." (2 Corinthians 5:20-21) In this way, sharing your belief in eternal life with others is at the heart of your mission as a Christian.

With practices of everyday worship, a community of fellow believers and a mission, you're well on your way to living a life of purpose. Unfortunately, there's one obstacle that often gets in the way.

It's none other than _envy_. Envy has posed a threat to the purposeful life since the beginning of mankind: In Ecclesiastes, the wise King Solomon remarks: "I observed all the work and ambition motivated by envy. What a waste!" (Ecclesiastes 4:4)

Solomon's words are even more relevant today: just think of all the people who overwork themselves every day, desperate to have the same new car, the same big house and the same fat paycheck as their neighbors. But none of those things — and especially not the false sense of superiority over others they so often engender — will bring meaning or happiness to your life. Your ambition is better channeled towards becoming like Christ. Find out why in the final blink.

> _"All of us must quickly carry out the tasks assigned us … before the night falls and all work comes to an end." John 9:4_

### 6. The Holy Spirit and your own determination will help you live as Jesus did. 

The Bible tells of how God created us in his image. From the outset, we were put on this earth to become like his son, Jesus Christ. This is yet another one of the five central purposes of every Christian's life. Though it isn't easy, with determination, you can learn to embody the best qualities of Jesus in the way you act and live. And the Holy Spirit is there to help you!

Ever been in a tricky situation and suddenly realized you had the power to make things right just by doing one specific thing? Sometimes it's hard to know the right thing to do but, once in a while, you intuitively know how to make the best decision.

Whether you've helped a lost child to find her parents, even though you were late for a meeting, or stayed behind after work to help out a stressed coworker, these moments show how the Holy Spirit is able to nudge us gently in the right direction.

Of course, we have to put in time and effort ourselves, too. Our personalities are nothing more than the sum of our habits. This means that we can change ourselves for the better by simply collecting good habits and eliminating bad ones.

Finally, take a look at the people you surround yourself with. What are they like? Do you behave the same way as they do? Our cultures inform our idea of normalcy, and we adapt ourselves to them so that we fit in. But you should take care that you don't "become so well-adjusted to your culture that you fit into it without even thinking." (Romans 12:2)

Instead, always reflect on your actions and the actions of others. Are they what Christ would have wanted? Over time, the answer to that question will become clearer and clearer. With your attention fixed on God, you'll know when your surroundings are preventing you from living your life maturely, and with purpose.

### 7. Final summary 

The key message in this book:

**The fact that you exist is no accident. God chose to give you life, so why not thank him by showing and spreading your love for him? By living a life filled with everyday worship and a Christian community, you'll learn to let your good qualities shine, stay on the right path and, above all, experience your true purpose on this planet.**

Actionable advice: 

**Experiment with making intuitive decisions for one week!**

Take a risk and allow your gut decisions to guide you this week. Record the results and pay close attention to your levels of happiness. Chances are things will go better than expected! With confidence in your intuition, you can strengthen your connection to the Holy Spirit and his guiding power. 

**Suggested** **further** **reading:** ** _The Reason For God_** **by Timothy Keller**

In _The Reason For God_, famous New York pastor Timothy Keller defends Christianity and its core beliefs against the most common objections. His fresh approach provides several arguments for continued Christian faith.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rick Warren

Rick Warren has lectured at Harvard, Cambridge and Oxford. He is the founder of the global P.E.A.C.E. plan, which supports the poor and provides education as well as many other resources in 196 countries.

