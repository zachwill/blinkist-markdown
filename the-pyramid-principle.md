---
id: 50d03b44e4b0beaaa7171ad3
slug: the-pyramid-principle-en
published_date: 2013-01-13T00:00:00.000+00:00
author: Barbara Minto
title: The Pyramid Principle
subtitle: Logic in Writing and Thinking
main_color: 628BC1
text_color: 406494
---

# The Pyramid Principle

_Logic in Writing and Thinking_

**Barbara Minto**

Never has clear, convincing communication been as important as in today's information-cluttered environment. _The Pyramid Principle_ (1978) explains in detail how written documents and presentations can be logically structured, and the methods described in the book are used by almost every major management consultancy on the planet.

---
### 1. To assist the reader, organize your thinking into a pyramid shape before starting to write. 

When preparing a document, most people have a pretty good idea of what they intend to write about but no specific plan for what to say or how to phrase it. Usually, they simply begin writing, hoping the structure will emerge on its own from their stream of consciousness. This results in a jumbled narrative, leaving the reader to sort out the mess.

The mind prefers order to disorder, and even imposes imagined order on random data it encounters. Consider for instance the ancient Greeks, who imagined animal shapes in the stars rather than seeing them as mere random dots.

Similarly, it has been shown that when reading, people automatically attempt to organize information in any written document into a certain form, namely a _top-down pyramid_ shape, where conclusions are supported by justifications and arguments, much like a pyramid is supported by its cornerstones.

It is easiest for a reader to digest information if it comes presorted into a logical pyramid shape.

Consider the following statements: "The seats were cold. I almost got into a fight. Italy didn't play well. That really was an awful football match." This "story" is poorly structured, since the actual main statement is withheld until the very end.

A top-down pyramid structure means that the document first introduces a summary statement and then the reasoning behind it. The above narrative would have been much easier to understand if structured in such a way: "That really was an awful football match: the seats were cold, I almost got into a fight and Italy did not play well."

**To assist the reader, organize your thinking into a pyramid shape before starting to write.**

### 2. Build your pyramid: group similar ideas together and then summarize each group with one statement. 

When constructing a pyramid, follow the _bottom-up approach_ : First, list all the points you want to make, clustering together those that argue toward a similar _conclusion_. Then _summarize_ each group with a single _statement_ one level above the group. Each summary statement is like the tip of a miniature pyramid.

Repeat the process on the next level, and the next and so on. Eventually, you are left with one summary statement that crystallizes the key message of the entire document. Your pyramid is ready.

Consider this example: from points like "Our customer-base is growing," "Each customer is buying more" and "We have increased our prices," you might draw the summary statement, "Our sales are growing."

You might then group this summary statement with others such as "Our fixed costs are going down" and "Our variable costs are going down," and summarize this new group with an overall message of "Our profits are improving."

Grouping and summarizing is simple, but you must adhere to the basic rules:

First of all, any idea expressed in the pyramid must always be a summary of the ideas grouped below it. Never use intellectually blank summary statements such as, "There are three reasons why we should expand to Austria." This is lazy writing, as the author has not bothered to summarize the three reasons properly for the reader.

Second, ideas in any grouping must be logically similar and share the same level of abstraction. In other words, a group cannot consist of "apple," "fruits," and "table," since an apple and a table are not logically similar, and as a more abstract term, "fruits" belongs on a higher level of the pyramid.

**Build your pyramid: group similar ideas together and then summarize each group with one statement.**

### 3. Justifying statements: use deduction to derive conclusions from chains of premises. 

Any statement you make within your pyramid structure must raise a question in the reader's mind, which you then answer on the level below. One way this answer can be formulated is through _deductive_ reasoning.

Deduction is a classic logical process where you draw conclusions from _premises_. For example, from the premises "All men are mortal" and "Goliath is a man," you can draw the conclusion, "Goliath is mortal." You could then summarize the entire deduction process on the level above with the statement, "Since Goliath is a man, he is mortal."

When recommending action to your reader, consider flipping the order of your deduction so that you lead with your conclusion. After all, the recommendation is what the reader really cares about.

Consider the example, "We should hire any applicant who knows how to read. Applicant A knows how to read. Hence, we should hire applicant A." As you can see, from a reader's perspective, the premise-premise-conclusion order withholds the most interesting piece of information until the end. As a rule of thumb, recommendations benefit from the opposite order: "We should hire applicant A, because we need someone who can read, and he can."

While deductive reasoning is a very straight-forward and naturally flowing process, it should not be used in complex arguments where you need many backing layers of justifications to support your premises.

If, for example, you try to justify the main finding of a complex document through deduction, your first premise will likely have several levels of justifications below it, which the reader must slog through just to arrive at the second premise. This will make your deduction extremely hard to follow.

**Justifying statements: use deduction to derive conclusions from chains of premises.**

### 4. Justifying statements: use induction to derive conclusions from groups of similar items. 

If a statement in your pyramid cannot be supported with deduction then a more creative form of reasoning must be used: _induction_.

Induction means drawing a conclusion from a set of ideas that are similar in some way; e.g. "Reasons for…" or "Parts of…"

An example of induction would be to conclude that "Einstein was a genius" and defend this statement with "Researched relativity," "Researched gravity," and "Studied cosmological constant."

With induction, the logical order in which to present your supporting points is not as obvious as with deduction. To make it intuitive for the reader, the order should always be determined by the source of the grouping.

If you have grouped _parts of a whole_, e.g. the divisions of a company, you should order them based on their structure, much like an organizational chart. In such cases, use so-called _MECE-logic_, where the parts are _mutually exclusive but collectively exhaustive_.

A MECE-structure of the divisions in a corporation will not contain any overlap, i.e. no activities are repeated in multiple departments, but all the parts put together will describe the entirety of the organization, leaving nothing out.

If your grouping is made up of _recommended actions_, you should order the points chronologically, according to which action you would take first.

For example, if your summary statement is "Hire new assistant," your grouping should be ordered as follows: "Advertise job, interview candidates, make hiring decision."

Finally, if you have classified items together because they _share a certain characteristic_, you should order them based on how strong that characteristic is in each item, e.g. first the heaviest object, then the next heaviest and so on.

**Justifying statements: use induction to derive conclusions from groups of similar items.**

### 5. To formulate recommendations, approach problems methodically and visualize them using logic trees. 

Business writing often involves giving recommendations on how to solve a problem. Usually, the solution is not self-evident (otherwise the problem would have been solved already). To find your recommendations, you can apply a straightforward _problem-solving process_.

First, you _identify the problem_ in clear, measurable terms; e.g., "The factory is losing three hours of operating time each day; how can we avoid this?" Next, you find out exactly _where the problem is_ ; e.g., "The problem is not the people or the raw material but the machines; they break down every day." Third, you dig deeper into the problem to find out _why it exists_ ; e.g., "The maintenance workers receive insufficient training." This allows you to _identify possible courses of action_ to solve the problem; e.g., "Make supervisors responsible for training or purchase training externally."

A useful technique for implementing the problem-solving process above is to visualize the problem using a _logic tree_. A logic tree is a simplified sketch of relationships branching out from left to right.

An example would be a tree of financial structures; e.g. a profit tree. The word "Profit" would form the trunk of the tree and then split into two branches titled "Sales" and "Costs."

The "Costs" branch would then split into "Fixed costs" and "Variable costs," which in turn would split into more and more sub-factors. Eventually, a level of granularity would be reached where the problems and their sources become evident; for example, one might find out that lagging profits are due to the increased costs of certain materials.

**To formulate recommendations, approach problems methodically and visualize them using logic trees.**

### 6. Structure recommendations around the effect they are meant to cause. 

If you have discovered the solution to your reader's problem, it is now your job to convince her to take action. So how do you structure your recommendations as convincingly as possible?

Essentially, when giving a recommendation, you make an _action statement_ that describes the actions aimed at achieving a _desired effect_. This being the case, you should group your actions around the effect you are aiming for and describe the effect so tangibly that you will later know whether or not it has been achieved.

Consider an example where the reader wants to raise profits. A poorly structured recommendation could read as follows:

Examine things:

  * Examine factory efficiency.

  * Examine customer satisfaction.

Increase training:

  * Increase factory staff training.

  * Increase sales training.

The above structure suffers from two main problems:

First of all, although the actions grouped together do bear superficial similarity to each other, they are not structured around the effects they are meant to cause.

Second, the desired effects should be detailed enough to be able to judge whether or not they have been achieved. Thus, a far clearer and more convincing structure would be as follows:

Increase sales by 5% in the next quarter:

  * Examine customer satisfaction.

  * Increase sales training.

Cut production costs by 2% in the next quarter:

  * Examine factory efficiency.

  * Increase factory staff training.

The actions are now grouped under the effects they aim to cause (i.e., increasing sales and cutting production costs) so that later one can easily judge whether or not the desired effects have been reached.

**Structure recommendations around the effect they are meant to cause.**

### 7. Use the introduction to tell the reader your key points within 30 seconds of reading. 

Every document should begin with a brief introduction that serves two purposes: to incite the reader's interest in the document, and to set the stage for the problem to be solved.

The easiest way to pique your reader's interest and help her concentrate on your document is to put the introduction in story-form. You begin by describing the initial _situation_, after which you introduce a _complication_ and finally offer a _resolution_.

The situation is the generally acknowledged current state of affairs; for example, "ArgonEx is considering investing in new mines in Austria." The complication is an alteration to the situation, most often a problem; for example, "ArgonEx is struggling with entry to this new market." Both the situation and the complication should be such that the reader is familiar with them and readily agrees with your statements.

The complication should prompt a question in the reader's mind; e.g. "What should we do about it?" This question will be comprehensively answered in the main body of the document, but you also want to give your reader an overview of your thinking within the first 30 seconds of picking up the document.

Thus, you present your main point as well as your most important backing points in the introduction:

"(Main point) ArgonEx should enter the Austrian market by acquiring an existing player because (1) mining permits are rarely granted to foreign companies, (2) local mining companies are being sold off cheaply and (3) competitors have accumulated huge losses trying to enter similar markets on their own."

**Use the introduction to tell the reader your key points within 30 seconds of reading.**

### 8. Use headings and formatting to show your pyramid structure in the text. 

To guide the reader through your pyramid hierarchy, you need a way of demonstrating the structure on paper.

The most popular method is to use _headings_ to indicate the various levels and groupings of ideas. Use increasingly indented headings to show you are moving toward the lower levels of the pyramid. Government offices and large companies will even add decimal numbers (1, 1.1, 1.1.1) to further emphasize the structure.

For example,

**Title of document**

The introduction and main argument should be stated, and the main section points introduced here.

**1\. First section**

Always introduce any group of headings and the key points you intend to make.

1.1. First subsection

This subsection message is backed up by these numbered paragraphs.

1.1.1. This data backs up the first subsection message

1.1.2. So does this

1.2. Second subsections

**2.** **Second section**

2.1. First subsection

2.2. Second subsection

Readers tend to skim headings, so do not rely on them to communicate the actual idea, but keep them short, only expressing the essence of the idea that follows.

When using headings seems too rigid and formal, consider simply underlining the main point and supporting points, etc., within the text.

Finally, in very brief messages like emails, simple indents may be most appropriate to convey the simple structure. See the example below:

***

For Monday's call I will need the following information on Japan:

Sales

Costs

Market trends

*** 

**Use headings and formatting to show your pyramid structure in the text.**

### 9. Use clear transitions between groups of arguments to keep the reader in the loop. 

Even the most logically thought-out pyramid structure is of little use unless the reader can keep up with it.

It is easy to lose the reader when moving between the various sections and subsections of the pyramid, so you should let her know when you are _transitioning_. One way to do this is to _reference backward_ at the beginning of a new section.

For instance, if you've just used a chapter to support the statement, "ArgonEx has too much inventory," and you're moving on to the next point regarding poor logistical processes, you might start the next chapter, "In addition to having too much inventory, ArgonEx has poor logistical processes." This lets the reader know you've transitioned into a new sub-segment of the pyramid.

If a chapter is particularly long and arduous, you may want to conclude with a _brief summary_ of its main points before moving on. This will effectively make sure the reader's mind is where you want it to be at the start of the next chapter.

If your document consists of recommendations, you might want to conclude it with a simple and tangible "Next steps" section where you outline what should be done next. This brings your reader's mind back to the actionable items at hand.

**Use clear transitions between groups of arguments to keep the reader in the loop.**

### 10. Final summary 

**The key message in this book is to carefully structure your thinking before beginning to write.**

The questions this book answered:

**How can you structure your ideas most effectively in a written document?**

  * To assist the reader, organize your thinking into a pyramid shape before starting to write.

  * Build your pyramid: group similar ideas together and then summarize them with one statement.

  * Justifying statements: use deduction to derive conclusions from chains of premises.

  * Justifying statements: use induction to derive conclusions from groups of similar items.

**How can you make effective recommendations?**

  * To formulate recommendations, approach problems methodically and visualize them using logic trees.

  * Structure recommendations around the effect they are meant to cause.

**How can you put your intended structure into words?**

  * Use the introduction to tell the reader your key points within 30 seconds of reading.

  * Use headings and formatting to show your pyramid structure in the text.

  * Use clear transitions between groups of arguments to keep the reader in the loop.
---

### Barbara Minto

Barbara Minto is a former McKinsey & Co. consultant who now focuses on teaching the Pyramid Principle to some of the world's largest corporations and government organizations.

As a consultant at McKinsey & Co, Minto realized that while most people could get the language of written documents right, many struggled with the clarity of the actual thinking behind them. She developed the Pyramid Principle to teach the foundations required for clear writing.

