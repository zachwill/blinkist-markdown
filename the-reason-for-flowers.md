---
id: 57dd8123f9916500030ea780
slug: the-reason-for-flowers-en
published_date: 2016-09-22T00:00:00.000+00:00
author: Stephen Buchmann
title: The Reason For Flowers
subtitle: Their History, Culture, Biology, and How They Change Our Lives
main_color: E65B4B
text_color: C94031
---

# The Reason For Flowers

_Their History, Culture, Biology, and How They Change Our Lives_

**Stephen Buchmann**

_The Reason for Flowers_ (2015) is about the origin, reproduction and effects of these amazing pieces of evolutionary artwork. These blinks explain how flowers have sex, why they're so beautiful and why humans have become so infatuated with them.

---
### 1. What’s in it for me? Understand our age-old love affair with flowers. 

Imagine someone offered you a deal: you will become fabulously rich overnight _and_ never suffer a single insect bite again — under the sole condition that your life will be devoid of flowers, real or artificial, from now on. Would you accept?

If you're hesitating, you wouldn't be the only one. Humans love and crave flowers, and we always have. In antiquity, people in barren deserts covered their homes with floral-patterned rugs to cheer themselves up throughout their flowerless existence. And today, we buy flowers like crazy: in the United States alone, people buy 10 million cut stems a day!

But what is it about flowers that makes them so irresistible, to us and so many animals as well? How have flowers come to shape our culture? And what are they good for anyway? These are just some of the questions you'll find answered in these blinks.

You'll also learn

  * why some bees take showers in an orchid;

  * about the secret language of flowers; and

  * about the perfect flower for a break-up.

### 2. The reproduction of flowering plants is highly dependent on pollinators. 

When you look out your window, even in the city, you're bound to see some of nature's beauty, and among the most beautiful innovation of the natural world are those remarkable bursts of color we call flowers.

But despite their tremendous beauty, flowers have a major problem:

Flowering plants can't survive on their own, and instead depend on pollinators for their procreation. These are generally flying insects like bees, beetles and butterflies — but to understand the relationship between these winged critters and plants, we first need to learn more about how flower sex works.

Plants that produce flowers are, for the most part, hermaphroditic, meaning that a single plant has both male and female reproductive parts. The pollen grains and ovules, the plant equivalents of sperm and eggs, are held in the plant's flowers, essentially making flowers a plant's sex organ.

But when one plant wants to have sex with another, they encounter an issue: they can't move! And to make matters more difficult, plants, like humans and other animals, are best off mating with unrelated members of their species to increase their genetic diversity and vitality. So, how do you have sex with someone who's far away when you can't move an inch?

Through bees and butterflies! After all, not only can these insects _move,_ they fly. This makes them perfect for the role of transporting pollen grains from one flower to another and impregnating other plants in the process.

But this all doesn't happen by sheer coincidence; it's partly a flower's beauty that helps it coax pollinators into this deal, which is exactly what we'll explore next.

> _"Plants are sexual organisms, but they lead sedentary lives and move on a timescale much too slow for us to notice."_

### 3. Flowers attract their pollinators and make the exchange worth their while. 

So, bees and butterflies lend a hand to their immobilized flowering friends, but this impulse isn't purely altruistic. In fact, flowers reward pollinating insects handsomely for their services.

Flowers are like a kind of café or rest stop for insects where they can eat, drink and take a break. Not only that, but flowers are also an ideal location for insects to find mates and, in colder parts of the world, the inside of a blossom can even be comfortably warm.

Flowers also offer specific perks to certain critters. For instance, for predatory insects, like spiders, flowers offer an ideal hiding place when hunting for food. And in the case of the male orchid bee, its flower of choice provides a sex pheromone-like fragrance that chemically attracts potential bee mates. 

But the most appealing thing flowers have to offer is deeply nutritious food, namely their sweet nectar. This sugar-rich, high-energy food is stored deep inside the flower, forcing pollinators to immerse themselves in the bloom, thereby covering their bodies with the flower's sticky pollen, the grains of which are also edible and a good source of protein.

And, just like the cafés and rest stops that humans frequent, flowers depend on spreading the word about their services. To do so, they've developed an ingenious means of advertisement, which primarily utilizes their vivid colors and enchanting fragrances.

In fact, a flower's color is specifically designed to garner attention. Insects can't see as well as we do, and the brighter a flower is, the easier it will be for a pollinator to recognize and visit it. As a result, flowers boast the most highly saturated colors found in nature.

But smell is another attractive force and the aroma of a flower depends on the type of insect it wants to attract. For example, the African Stapelia emits the scent of rotting flesh to lure in carrion flies, an odor that most humans would find revolting. At the same time, it could make the perfect flower arrangement for a break-up.

### 4. In evolutionary terms, flowering plants are young but tremendously successful. 

Earth wasn't always the lush, green home we enjoy today. Five hundred million years ago, while the sea was crawling with life, the land on Earth was a barren expanse of rock and desert. In fact, plants only came into existence around 472 million years ago, and it wasn't for another 300 million years that they began producing flowers.

So, it took a while for plants to emerge on land and, when they did, it was in or around water. These early plants included a single-celled algae that allowed moss-like organisms to spread across the earth's dry surface, competing for light and space, some of which evolved into ferns.

Then, about 130 to 160 million years ago, plants known as _angiosperms_ produced the first flowers and, as with many evolutionary adaptations, it was a fortuitous accident.

As a result of mutations and natural selection, small leaves began bunching at the tops of plant stems. They then lost their green color, developed floral structures and eventually began producing fragrances.

Flowers' relationship with pollinators accelerated this evolution. When competing for insect companions, the flowers with the most seductive food, smells and appearance quickly won out.

However, from an evolutionary perspective, the development of flowering plants isn't your standard fare. In fact, it represents an unprecedented success story of the versatility of flowers.

Flowers can adapt to nearly any climate, withstanding bitter winters and scorching heatwaves. Beyond this, they're incredibly resourceful in terms of reproduction. For instance, some flowers self-pollinate while others scatter their pollen in the wind or water and, as we know, cooperate with obliging animals.

But another key to flowers' success is their _double-fertilization_. This term refers to a point in the floral reproduction cycle when the sperm divides into two cells, one fertilizing the egg and the other producing an _endosperm_, a kind of protective suit that shields the young seedling during its critical sprouting phase, helping it survive this inhospitable time before it can feed itself through photosynthesis.

> _"Flowers blush. It may seem remarkable to say this, but it's true. Many flowers change colors after they are pollinated."_

### 5. While humans have loved and cultivated flowers since ancient times, crossbreeding is relatively new. 

Enough gossiping about floral sexuality — let's take a look at the special relationship that _we_ have with flowers.

We all know that humans grow, breed, smell, eat, sell and buy flowers, arranging them in bouquets and planting them in gardens. Virtually every culture known to man has held flowering plants in high esteem, and even our early ancestors used flowers in burials and cultivated them in gardens.

Amazingly, the practice of using flowers in human death rites dates back so far that archaeologists even discovered flowers in a 70,000-year-old grave site near the skeletons of Neanderthals. But why do we use flowers this way?

Because they console humans by reminding us of the perpetual cycle of life and death. After all, flowers die every fall only to emerge once again in the spring.

As a result, the first gardens also date back to the very first civilizations, those founded 10,000 years ago in the fertile area between the Euphrates and Tigris rivers. These first gardens were, for the most part, ornamental and designed to please ruling elites who didn't depend on them for food.

After that, millennia passed before the next major advancement in flowers, when botanists began to crossbreed different species in the late seventeenth century, producing incredible hybrids.

The first European botanists discovered that plants are in fact sexual beings that produce both fertile ovules and sperm in the form of pollen grains. This knowledge made crossbreeding a snap. The botanists just took the pollen from a flower and applied it to the sticky stigma of a different species. Then, if they were lucky, a fruit or seeds would appear which they would plant and, pretty soon, a hybrid would sprout.

This was a pivotal breakthrough because crossbreeding often produces large, magnificent flowers, which likely make up most of the inventory in your local flower shop.

### 6. Flowers provide us with food and perfumes. 

Most people enjoy looking at flowers, but would you eat one?

Well, you might be surprised to hear it, but lots of the food you eat is actually derived from flowers, which can come in many different forms. For instance, both broccoli and cauliflower, though posing convincingly as vegetables, are essentially flower blooms that have yet to unfurl.

Another example is honey, which is simply floral nectar processed by bees. And this whole process is all thanks to the wonders of photosynthesis, which enables plants to create sugar from carbon dioxide, water and light.

Plants use this substance to lure bees into their flowers, where the insects drink their fill of nectar before bringing it back to their hives. Once they get home, the bees regurgitate their meal and swallow it over and over, concentrating it into a solution of 80 percent sugar and 20 percent water.

So, honey production isn't the most appetizing process, but it's also thanks to the digestive enzymes of bees that honey doesn't crystallize, instead staying smooth and creamy, perfect for spreading on toast.

Now that you know you're likely eating flowers, let's get back to their lovely smells, which, although meant for the noses of pollinators, are regularly enjoyed by humans as well. In fact, since way back when, people have sought out ways to capture and preserve the scents of flowers — say, by producing perfumes.

For instance, the first scents preserved from plants actually took the form of incenses that our ancestors burned, often for religious purposes. These fragrances were considered very precious and some, like frankincense and myrrh, were as valuable as gold. That's why, in the story of the nativity, the Three Kings brought them as a gift when they visited the newborn baby Jesus.

However, it would be a few more centuries before humans learned how to produce floral-based perfumes. This feat was accomplished by medieval Arab chemists who, in the tenth century, developed a process of steam distillation that they used to make rose water. The technique involved boiling rose petals and harvesting the fragrant steam containing the flower's essential oils.

### 7. Flowers have left their mark on human culture. 

Every year, as Valentine's day rolls around, flower shops see a massive spike in their profits. People flock to buy bouquets that communicate things like love and affection because flowers _mean_ something to us.

But this meaning pales in comparison to the elaborate art practiced during the eighteenth and nineteenth century of using flowers to send secret messages. It's true — there was an absolute obsession with using flowers as secret codes to communicate with lovers or friends. It began in Paris in 1819 with the publication of a book called _Le Langage des Fleurs_, which detailed the symbolic meaning of individual flowers as well as that of bouquets.

For example, to tell a girl that you fell in love with her upon first sight, you'd offer her a rose with the thorns removed. But if you felt uncertain, you'd hand her a rose with the thorns and leaves intact, signifying that you were afraid but still remained hopeful.

And secret communication wasn't the only symbolic use of flowers; they were also prominently featured in texts and paintings as icons rich with meaning and examples of tremendous natural beauty. In fact, flowers are so well suited to figurative speech that even Jesus Christ employed them.

For instance, in his famous Sermon on the Mount, Jesus told his followers to "consider the lilies of the field, how they grow." In other words, he was saying that these flowers don't worry about their food and yet they're provided for and thrive. As such, humans should be less preoccupied with our own needs and instead trust that God will take care of us.

Painters throughout history have also been enticed by the intricate shapes and colors of flowers, striving to capture their natural beauty. This was especially true for the Dutch painters of the seventeenth century, who became obsessed with flowers and managed to produce nearly photo-realistic depictions, many of them still lifes. For these artists, flowers served as the inspiration to strive for new artistic heights.

### 8. Plants sparked scientific discoveries and progress. 

So, everyone from artists to Jesus Christ have been fascinated by flowers and other plants, but plants have also been essential to scientific discoveries. For instance, the observation of the manner in which plants are distributed across the globe led to a major breakthrough in the fields of geography.

In the eighteenth century, explorer-scientists and plant collectors roamed the world, noticing that the flora varied tremendously from one continent to another. In fact, the plants in Australia were so different to the ones in North America that, to their discoverers, they looked as if they'd been created by different gods.

On the other hand, botanists also discovered the plant family Proteaceae, which exists on _all_ continents, including the larger islands. But how was it possible for this family to spread between disparate parts of the world separated by vast oceans?

Well, it was exactly that question that led Alfred Wegener, an early geophysicist, to a revolutionary speculation: maybe the continents weren't always separate. This hypothesis led scientists to discover _continental drift_, and that there had once been a single giant continent, which they called _Pangea_, on which the Proteaceae plant family grew and spread. Then, pieces of Pangea drifted apart, like giant sheets of ice, forming the continents we know today.

And plants also played a major role in our understanding of genetics. It all began 160 years ago when the Augustinian monk, Gregor Mendel, was experimenting with the crossbreeding of pea plants. Through his experiments, Mendel discovered how certain traits were expressed in new generations.

For instance, when he crossed white-flowered peas with those of a violet hue, their offspring would only produce violet flowers. However, if he let the new generation self-pollinate, one out of every four plants in the following generation would produce white flowers.

Mendel's conclusion was that the genetic trait for white flowers was _recessive_. In other words, it was a weak trait that would recede in the presence of a _dominant_ trait for purple flowers.

### 9. Nature, and flowers in particular, enhance our well-being. 

Have you ever wondered why humans are so fond of parks? In cities, for instance, space is hard to come by and tends to be very expensive; the real estate value of New York's Central Park would be an absolute fortune! But city dwellers insist on maintaining their access to parks. Why?

One explanation might be _biophilia_, a term that describes the natural human desire to be in nature and our propensity to gravitate toward green settings and flowers. This trait was inherited from our early ancestors who lived in the African savannah. After all, in a desert like that, an attraction to green space boosted our species' chances of survival because vegetation is a sign of water and food.

But what does this have to do with flowers?

Essentially, we like flowers because they're similar to the things we're intuitively drawn to. Flowers have similar stand-out colors to ripe fruits and many of their scents are chemically related to those of fruits as well. Therefore, they trigger positive feelings by reminding us of the things we need or want, perhaps like a photo of a loved one.

But that's not all nature is good for. Studies have also shown that nature can calm us down and even promote healing. In 1984, the researcher Roger Ulrich was studying postsurgical hospital patients. He discovered that patients in rooms with a green view would recover much more quickly from their procedures than those whose windows looked out onto a brick wall. Not only that, but patients with a nice view also needed less pain medication.

Flowers affect us similarly. In fact, the mere aroma of a flower can drop your blood pressure, and smelling a rose can help you let go of needless aggression by dropping your adrenaline production by 30 percent.

### 10. Final summary 

The key message in this book:

**Flowering plants depend on their beauty, scent and tasty offerings to attract pollinators, which facilitate their reproduction. And while their enchanting means of attraction are not aimed at humans, flowers have captured our hearts nonetheless.**

Actionable advice:

**Prolong the vase life of your cut flowers.**

A few simple tricks will help you make your cut flowers last as long as possible. Most importantly, you should always use a clean vase and clean, lukewarm water, which, if possible, should be replaced daily. Additionally, you can buy flower food in a store to provide nutrition, and be sure to cut their stems before putting them in water.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Hidden Life of Trees_** **by Peter Wohlleben**

Trees are engaged in countless complex cycles and they constantly struggle for water, light and their own survival. This struggle has led to some astonishing abilities: trees communicate with one another, give each other assistance, collaborate with fungi and other creatures, have memories and have even developed their own version of the internet!
---

### Stephen Buchmann

Biologist Stephen Buchmann has been a passionate bee and bug watcher since his youth. He is an adjunct professor at the University of Arizona, with a specialization in pollination ecology. Buchmann is the author of eleven books, including _The Forgotten Pollinators_, a finalist for the _Los Angeles Times_ Book Prize.

