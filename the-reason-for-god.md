---
id: 5550ab0e3737630007050000
slug: the-reason-for-god-en
published_date: 2015-05-11T00:00:00.000+00:00
author: Timothy Keller
title: The Reason For God
subtitle: Belief In An Age Of Skepticism
main_color: AFA931
text_color: 59550E
---

# The Reason For God

_Belief In An Age Of Skepticism_

**Timothy Keller**

In _The Reason For God_, famous New York pastor Timothy Keller defends Christianity and its core beliefs against the most common objections. His fresh approach provides several arguments for continued Christian faith.

---
### 1. What’s in it for me? Learn how some of the most common objections against Christianity can be countered. 

Have you always had doubts about Christianity? Have you struggled to reconcile a world of pain and suffering with a benevolent and loving God? Are you confused by the claims of multiple religions that say _they_ are the only true religion?

These blinks contain some important truths about the Christian faith.

Timothy Keller, one of the most popular pastors of New York, has helped revive the Christian faith with his fresh approach to its core issues. In these blinks he explains the key points of Christian faith — and its most common objections.

In these blinks you'll discover

  * how Christianity helped one German pastor survive the Nazi regime;

  * why the Bible being vague is a good thing; and

  * how the Catholic Church has already accepted the theory of evolution as compatible with Christian thought.

### 2. Contrary to popular belief, the Christian church is not particularly exclusive – but it does have core beliefs. 

People hold many different things against the Christian church. But it's the notion of Christianity as a strict and self-righteous religion that scares the most people away.

This is because Christianity's strict beliefs and claims of being the only true religion make many people suspicious.

For example, some parts of the Christian faith, like the belief that Jesus really is the son of God, are incompatible with other religions. By insisting that this belief is true, Christianity implies that all other religions are wrong.

But for many people, each religion is right in its own way. That's why they reject Christianity: because it claims its own fixed rules and viewpoints are the correct ones. This kind of rigid point of view can make some people very angry.

For example, the author once participated in a panel discussion where he, a rabbi and an imam all stated that not all religions could be true. This angered one of the students attending the discussion, who answered, "We will never come to know peace on earth if religious leaders keep on making such exclusive claims!"

But in actuality, there is nothing wrong or particularly exclusionary in having strong beliefs and core principles. In fact, every single person and society has a set of core principles that make up their identity and differentiates them from others.

For example, if you believe in the right to private property, or that your life has a basic value, then you are upholding a core principle. And every society that has basic rules on how people are allowed to act — for example, a law against homicide — also has some core beliefs about life that create a common moral foundation for their particular social identity.

And even very tolerant groups have certain core beliefs that unite them and form their identity — just like Christianity. For instance, gay rights activists believe that same-sex relationships should be fully accepted by society.

### 3. Christianity is not a threat to your personal or cultural freedom. 

Another typical misconception of the Christian faith is that it has strict regulations. Non-believers commonly ask: how can you live your life freely if there is a rigid set of rules that you have to obey?

Indeed, many people believe that adopting Christianity's strict rules and inflexible regulations would threaten their personal and cultural freedom.

For example, when working as a pastor in New York, the author met many people who argued that Christianity was not an option for them because they wanted to live their life by their own rules, not those of someone else.

But Christianity actually has room for your personal freedom and your own practices.

This is because the Bible is vague on many points, and while it imparts certain necessary core beliefs, it still allows each person to decide exactly how they want to practise their faith.

For example, the Bible does not have a strict set of rules for religious services: it makes it clear that religious practices are important, but the instructions within that can be interpreted in many different ways. This allows every Christian community and believer to decide upon the exact nature of their service — from the loud gospel choirs of Africa to the quiet masses of some Protestant churches.

But Christianity goes beyond allowing for personal freedom — it also allows room for the integration of existing cultural traditions.

Because of its openness, Christianity can be easily integrated into many local cultures and traditions. In this way, Christian beliefs don't override the existing traditions, but instead add a new layer and a new meaning to the culture. For example, African cultures often had a strong belief in good and evil spirits, and ideas of a divine savior. So when they heard about the coming of Jesus Christ, and how he defied the evil spirits while wandering through the desert, they could immediately relate to Christianity, despite their own local traditions.

### 4. Christianity is responsible for much that is good in the world. 

How do you know whether a set of beliefs is valid or not? One way is by looking at those who practice the beliefs and seeing how they behave in the world. In the case of Christianity — as well as many other religions –, it quickly becomes clear that its followers have done and still do many wonderful things.

This is because Christian principles lay the foundation for good beliefs, and encourage believers to help others — especially those who are worse off than us.

Of course, not every Christian in the history has been infallible. But indeed, many of our Western moral beliefs find their roots in Christianity, such as the indisputable worth of human life. This belief comes from the Bible, where God commands that no one shall kill another person, because every human being was made by God. Therefore killing another human would mean destroying what God had created.

And according to the Bible, Jesus, the most important teacher of Christianity, helped the poor and the weak above all others, and encouraged everyone else to do so. He spent much of his time in the company of the poor and disenfranchised, arguing that they needed him the most.

Finally, during the seventeenth century, Christians were among the first people in the United States to speak out against slavery — because it opposed their core beliefs about every human being's worth.

The Christian faith can also give believers the strength to do great things — even during bad times.

For example, the German pastor Dietrich Bonhoeffer's faith compelled him to fight bravely against the Nazi regime, he even left the safety of London to fight against it. After being arrested later in Germany, he explained in his many letters to his family how his faith in God gave him the strength to fight injustice, and concentrate on always helping others, even if it cost him pain and suffering.

### 5. Accepting one’s sins isn’t necessarily a bad thing. 

There is one final common objection to Christian belief: if you become a Christian, you'll have to face all the wrong things you have done, and accept that you are a sinner.

But does this have to be a bad thing?

In fact, it's actually liberating to admit that you've made mistakes and are less than perfect.

For example, Professor Alberto Delbanco writes about an experience he had when he was conducting research in the self-help group Alcoholics Anonymous. One member was a young man who felt cheated by society, and blamed everybody else for his life going wrong. He was very angry and felt like he could not escape from his problems.

But everything changed when one of the other members claimed: "I had the same problem until I achieved low self-esteem." He explained that he finally felt free when he acknowledged that he wasn't perfect, and that he — not society — had made many mistakes. These words helped the young man overcome his belief that his own perfect life had been ruined by society — and he didn't feel so angry anymore.

Another benefit of admitting our sins is that it can help us do something about them.

For example, imagine you are unwell, and keep blaming your poor health on the environment or your genes. Because these things are out of control, you can't do anything about them, and you won't get any better. But if you start thinking about what you have done wrong — maybe you haven't been exercising enough — you can start to behave differently, and make a positive change.

Finally, admitting your sins can also help you focus on more important things — like helping others — instead of money or your career.

Why?

Because if you accept that you've done wrong in the past, Christianity suggests you ask God for salvation. This means you'll have to attempt to please him by following his rules — for example, by helping other people.

In the following blinks you'll learn the author's arguments for why it is good to believe in the Christian God.

### 6. Suffering, injustice and catastrophes don't contradict the existence of God. 

Despite many people trying their best to make the world a better place, the amount of suffering is overwhelming. And for many people, this simple fact is the main argument against the existence of a loving and just God.

But what these people forget is that even bad things can have meaning, and can lead to something good.

A famous case in point is the biblical story of Joseph. Joseph suffered innumerable ordeals, including being sold into slavery in Egypt by his own brothers. Throughout his horrific suffering he must have prayed for assistance, but God didn't intervene. But although painful, his suffering helped him become stronger and wiser as a person. Eventually after being released he used this strength of character to become a leading politician and helped to save many from hunger and drought. God had let him suffer as a slave because it was exactly his suffering that made him a stronger and better person.

On top of the meaning that can be found in suffering, there is a more subtle reason that shows that despite the injustice in the world, we _do_ believe in a higher being — even if we don't realize it!

The fact that we find something unjust shows we have some belief in a higher being. For example, if we think a catastrophe like a tsunami is an unjust event, instead of a random act of nature, we are assuming there's some kind of higher order capable of influencing these events. In other words, an event can't be unjust if no one is responsible for it — so by claiming the event is unjust, we recognize that such tragedies are more than pure coincidence.

One could go even further and ask: if there is no God, where do we get the ideas of "just" and "unjust" from, seeing that we can't measure justice in the physical world? Here is a possible explanation: The mere fact that we have a feeling for justice indicates that there's a God who gave us this idea.

### 7. The idea of a hell doesn't contradict the idea of God also being loving. 

The notion of a fiery hell and an angry, judging God scares away many potential believers. But these people haven't truly understood hell: hell can and should be seen as an abstract principle meaning a deviation from what is right, and a focus on what is wrong.

For example, Christian belief states that if you are dependent on the wrong things, like money and fame, you will live in your own personal hell.

Jesus once explained this point when he told a rich man that his money would prevent him from entering heaven. His words meant that those who only care about money won't find the real happiness and comfort that lies in God, and are instead forced to live in hell on earth.

Anyway, it makes sense that a loving and fair God would sometimes get angry at his creations.

For example, imagine someone close to you was repeatedly making the same mistakes and never reaching their full potential. You would probably be angry, because you care for that person and want to help them take responsibility for their life.

Sometimes, people that are wrong have to be punished. Imagine if there was no sentence or punishment for a murderer in a society. That hardly seems fair — especially from the perspective of the victim and their relatives.

A biblical example demonstrates this: When Adam and Eve disobeyed God and ate the forbidden fruit, he felt they had betrayed him, became furious and expelled them from paradise. But he still showed that he cared for them by making them clothes and watching over them — which highlights the idea that God can judge _and_ be loving at the same time.

### 8. Science can’t disprove God’s existence. 

Have you ever heard someone say that religion is no longer necessary, because modern science now explains all the mysteries in the world — and has proven religion wrong in the process?

What these people don't know is that science and religion aren't as incompatible as they seem.

For example, the theory of evolution is often mentioned as one of the main conflicts between science and religion. But in fact the Catholic Church declared long ago that the theory of evolution is compatible with Christianity.

Furthermore, the fact that many scientists are deeply religious proves there's no contradiction between practicing science and religion. Take Francis Collins, the head of the Human Genome Project, who is a profoundly religious Christian.

Many of these scientists admit that there are still many things that science can't explain, but religion can.

For example, every event needs to have a cause. But what was the very first cause that started everything in the universe? Science can't answer this question without coming to a contradiction, like postulating an infinite chain of causes that started out of nowhere! But religion clearly places God as the first cause that created the universe.

Some people object and say that the Big Bang was the original cause. But if we say the Big Bang was the beginning of all things, we've just created a new question: what caused the Big Bang?

Another misunderstanding of the relationship between science and religion is when scientists try to disprove or explain miracles.

What they don't understand is that miracles work on another level of meaning and proof. When they demand a scientific explanation for a miracle described in the Bible — like when Jesus changed water into wine — they are missing the point. Because by their very definition, miracles can't be explained by science.

### 9. For Christians it is a fact that Jesus actually did rise from the dead. 

The core belief of Christianity is also one of its most discussed topics: the crucifixion and resurrection of Jesus Christ. But can something so miraculous really be true?

Let's examine this claim through what we know from the Bible: shortly after Jesus's death, there was a sudden change in the whole Christian community that can only be explained by real experience. Every Christian simultaneously started to believe and preach that Jesus had risen from the dead. For the Christians, the lack of big disagreements inside the Christian community during that time suggests that the resurrection was real, and there was no need to discuss its validity.

And then there's the fact that the belief in God as a resurrected and transformed human body — in contrast to Ancient Greek mythology, where gods sometimes just took on a human disguise — was completely new in the Jewish community. But since it really happened, no one cared that it was a novel belief.

But even if the story of the resurrection was a fake, it would have been faked pretty poorly.

Why?

Because many of its details are potentially damaging for Christian doctrine, and would have created a lot of doubts at the time.

For example, the first people to meet the resurrected Jesus were all woman. But during that time, women didn't count as official witnesses in court. If somebody had made the whole story up, it would have been much more convincing to claim that the first witnesses had been men.

Finally, the story of the resurrection of Jesus is supported by the long list of eye witnesses named in the Bible.

In an account of the resurrection, the apostle Paul lists over 300 witnesses — often by name — and claims that many are still alive. This was important evidence because the relatively safe roads of the Roman Empire made it possible to visit the listed witnesses and question them directly. He certainly would not have gone to all this trouble unless he was sure that the resurrection was true.

> _"If Jesus rose from the dead, then you have to accept all that he said; if he didn't rise from the dead, then why worry about any of what he said?"_

### 10. Although there is no definite proof, many clues lead us to believe in God. 

Debates about religion can go on forever. But even though there may be no final proof for God, there are several clues that strongly point to his existence.

For example, we have all experienced emotions that seem to connect us to something beyond our everyday world. This indicates that there is a higher plane of existence — and within it there may well exist a higher being.

Furthermore, most people possess moral feelings and a deep sense of beauty — feelings that are hard to explain in terms of our normal, non-spiritual world. For example, we feel some things are simply wrong, like committing murder without reason, while other things are so instinctively sublime, like a breathtaking landscape, that it seems to touch something inside us that is indescribable.

Another argument for the existence of God is that we have the desire for a higher being and a higher meaning for our lives — and where there is a desire, something usually exists to fulfill it.

When people don't believe in God or a higher meaning, they feel what the author John Updike called a sense of "horror." This makes them constantly search for a higher calling in life — and many people wish for a higher being that watches over them to make that call. Seeing that many people have this desire for a deeper meaning and an almighty God, it makes sense to assume that there is such a God who can satisfy these desires.

Why?

Because it would be senseless to have a desire for something that doesn't exist, as we would never be able to satisfy it. For example, the fact that we have a desire like hunger strongly suggests there is something like food to fulfill our desire. And there is!

### 11. Final summary 

The key message in this book:

**The most common objections people have against Christianity, including why miracles are true, how we know Jesus rose from the dead and why science can't disprove religion, can be answered through Christianity's own tenets and principles.**

**Suggested** **further** **reading:** ** _The Biology of Belief_** **by Bruce H. Lipton**

_The_ _Biology_ _of_ _Belief_ describes a revolutionary change in biology and explores a new approach to the connection between mind and matter. Using easily accessible examples and explanations, Lipton offers a radical alternative to our understanding of the influence of genes in determining our behavior and identity.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Timothy Keller

Timothy Keller founded his own church in New York in 1989, and now speaks weekly to a Sunday congregation of over 5,000. A prolific author, he has been described as "the C.S. Lewis for the 21st Century."

