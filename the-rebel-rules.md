---
id: 53f477ed34333900086e0000
slug: the-rebel-rules-en
published_date: 2014-08-19T00:00:00.000+00:00
author: Chip Conley
title: The Rebel Rules
subtitle: Daring to be Yourself in Business
main_color: E8532E
text_color: B53E20
---

# The Rebel Rules

_Daring to be Yourself in Business_

**Chip Conley**

In _The_ _Rebel_ _Rules_, Chip Conley gives us the lowdown on success stories of entrepreneurial rebels. Conley makes sense of the seeming oxymoron of the rebel businessman, who embodies two apparent opposites — business savvy and refusal to play by the rules. He argues that business rebels, i.e., people who connect with their inner passions and dare to use their vision and instinct, aren't only more successful in business but happier in general.

---
### 1. What’s in it for me? Start your own rebel company! 

Many people today realize that their lives — and especially their jobs — no longer reflect their inner passions. At the same time, saturated markets and accelerated market dynamics demand new business approaches to connect to customers' emotions.

The answer to these demands is a walking oxymoron: the business rebel. This relatively new species of entrepreneur is driven by passion and values carried over into their organizations and business approaches. Although loved by customers and employees, the business rebel is feared by the old and clumsy competition.

In these blinks, you'll find out:

  * how to find your inner rebel and incorporate your passions and values into your work,

  * what expert surfers and successful business leaders have in common,

  * why celebrating failure is essential to being successful, and

  * why you shouldn't listen to people with MBAs.

### 2. Dare to reconnect with your “inner rebel.” 

Do you know people who've led a successful career but feel dissatisfied with themselves, like all they've accomplished has been meaningless? One name for this is a midlife crisis, but another way of putting it is that they've lost touch with their inner rebel.

Many people realize their special talents and passions at a young age but let them go over time. Business rebels, however, manage to hold on to them into adulthood.

As Pablo Picasso said, "Every child is an artist … the problem is how to remain an artist once he grows up." So the basic question is, how far behind have you left your inner artist?

Many people gradually adapt to society's norms and rules as they age. They start shedding their naïve dreams and passions — say, to become a Major League Baseball player or a rock star — because they're told that it's extremely unlikely to make money that way.

But this results in a disconnect from a major part of your persona and, later down the road, a potentially unsatisfactory life.

The business rebels, however, fight the expectations of society and hold onto their inner artists. The origins of many successful business rebels can be found in their childhood. Richard Branson, for example, threw together all sorts of businesses when he was a kid, including breeding parrots and growing Christmas trees. He continued his rebel spirit into business ventures as a young adult, creating multiple wildly successful businesses, namely Virgin Records and Virgin Atlantic Airways.

So how do you reconnect with your inner child or inner rebel and dare to follow your passion?

Try to scan through your past: your old diaries, school assignments, photos, or anything else that will help you remember your old self. You can also ask yourself: who did you always worship? What attributes do you admire in other people?

Find that passion and you'll find the source that powers your inner rebel.

### 3. Use your rebel attributes – vision, passion, instinct and agility – but don’t lose traction. 

Are all business rebels thoughtless risk-takers and ruthless leaders? No, but it's in their nature to test the status quo on a regular basis.

Business rebels thus need to strike a balance between four distinct attributes if they want to be successful: vision, passion, instinct and agility.

Having a vision allows a business rebel to establish direction for his or her organization, even when the business faces chaos from all sides. Rebels may come off as naïve or kooky for letting themselves be guided by a vision that looks unrealistic, but it's a powerful force.

And once their vision gives the organization direction, their passion fuels it. Vision and passion in a business rebel often inspire cult-like followers and organizations that have a familial environment.

Then there's the gut instinct. Steve Jobs had just the type of instinct rebel leaders need to tap into, namely, his famous sixth sense of knowing which products, such as the iPhone, were capable of changing the world.

Lastly, rebels need to be agile if they want to do justice to their vision, passion and instinct. Innovations don't last as long as they used to and nobody has time for years of marketing studies before launching a product, so rebels have to trust their instincts, make snap decisions and move quickly.

But, as with everything, there is such a thing as _too_ much.

Rebels who are too extreme can manifest the four attributes to a destructive degree. For instance, instinctive business rebels may enter _denial_ _mode_, i.e., they refuse to accept failure until it's too late to learn from it. And rebels who are too obsessed with their vision can succumb to emotional outbursts and ignore important, everyday tasks.

In order to avoid these situations and keep their feet on the ground, some rebel leaders look to colleagues.

Bill Gates resigned from his position as CEO of Microsoft, seeing that a professional CEO was better suited to realize the company's long-term vision.

### 4. A rebel leads by values. 

Have you come across an executive that's highly productive and effective but totally boring? That's what happens when people leave their passions and values outside of work.

We all want to be part of something bigger than ourselves. That's why great leaders let a set of values guide their management and organizations.

The book _Built_ _to_ _Last:_ _Habits_ _of_ _Visionary_ _Companies_ concludes that, among exceptionally successful companies, there is an unmistakable correlation between sustainable profitability and guiding core values.

Since there is no perfect set of personal values, everyone has to find their convictions and express them as they choose. True rebels integrate them in their life _and_ their work.

So how do you find your own core values?

Start by writing a list of people, characteristics and actions you admire. Try to find connections between them, and use the connections to formulate your core values.

Let's say you admire bravery and humor. Now, think about how these values are represented by the things you do at work and outside of work. You'll probably find that your job isn't incorporating as many values as you'd like, or maybe even making you act against your values.

The next step is to identify the misalignments between the way things work and your set of values. Write down your good habits, i.e., the ones that agree with your values, and the bad ones, i.e., the ones that work against your values; work on eliminating the bad ones.

Then apply your values to your organization. If one of the highest values in your organization is trust and customer satisfaction, then sales representatives who have to ask the headquarters every time they need money to fulfill even the most inconsequential of customer requests indicates a misalignment of your values. Entrusting your sales representatives with a budget to make spontaneous decisions in favor of the customer would be the right move.

> _"Human beings have an innate desire to be both selfish and selfless. Corporate culture and financial incentives must reflect these polarities."_ — George Zimmer, CEO Men's Wearhouse

### 5. Starting a rebel company? Don’t lose hold of your vision. 

Starting your own company is always a whirlwind of emotion and instinct — even more so when you're a business rebel. And when you're just giving birth to your business, you're sure to encounter opposition and obstacles that can easily throw you off course. Following a few guidelines will help you avoid errors.

First, don't start a business with a friend.

A typical mistake for novice entrepreneurs is to start a business with a friend. They think it's a recipe for success because they know and trust each other and it sounds like a good time. Unfortunately, these business endeavours tend to end early along with the friendship.

While trust is always important in a partner, you should also be looking for someone who complements your skill set and your knowledge, and with whom you can work on a professional level.

When you have a rebellious business idea, have faith in it and the talent with which you surround yourself. Your rebel business idea has to question current industry wisdom and look at things from a new perspective.

As Wal-Mart's founder Sam Walton said: "Swim upstream. Go the other way. If everybody else is doing it one way, there's a good chance you can find your niche by going in exactly the opposite direction."

But sooner or later you're bound to encounter people, especially people with MBAs, who will say your idea is impossible to realize. Use the valuable knowledge and skills of such people to help you solve problems, and simultaneously, don't let them make you feel insecure about your business ideas. After all, it's your vision and instinct that makes you the rebel entrepreneur and the MBA-holder your employee or consultant.

And remember Henry Ford's wise words: "Obstacles are those frightful things you see when you take your eyes off the goal."

> _"Great spirits have always encountered violent opposition from mediocre minds."_ — Albert Einstein

### 6. You need to communicate your vision to create a passionate culture. 

As you start building your organization, you have to communicate your vision and values or else it'll get awfully lonely at the top.

There are multiple ways to communicate your vision to your organization. The two most effective are verbal and aspirational.

The verbal approach builds on the vocabulary used in the organization. Certain words or phrases can become a mantra for your rebel organization and spread your vision and the values. For example, at Disney World, it's "guests," not customers; at Joie de Vivre hotels, it's "hosts," not the service staff.

These word choices enforce the friendly, family-like atmosphere that's part of the vision, and the customer-oriented values these companies want to represent

Another powerful method for communicating your vision is by means of aspiration.

When Jack Welch became CEO of General Electric in 1981, his role was to simplify the company's complex problems. He created a concrete aspiration for the company, announcing that it needed to become either the best or second-best in every industry it was in, or it would have to leave that industry.

At the time, GE was in a lot of slow-growing industries and, after Welch's announcement, it divested about $20 billion worth of business. At the same time, it invested about $50 billion in acquisitions that were more likely to be the best or second-best in the industry, matching Welch's vision. During his tenure, the company's value rose 4000 percent, pointing to the success of his vision and strategy.

It's your job to make complex visions and values easily understandable and tangible for your organization. Just remember: everything should be expressed in a way that an intelligent seven-year-old can understand.

When your vision and values live throughout the organization, a vibrant corporate culture will arise almost on its own.

### 7. Implement your instinct inside the company. 

Have you ever noticed that every company has rules of thumb that represent its collective wisdom and experience? Those common sentiments often trickle down from the top.

You can teach those rules of thumb and sentiments to your employees. For example, a business rebel might instinctually know when it's best to oppose conventional wisdom, which is valuable knowledge.

Therefore, rebel entrepreneurs need to create an open environment for learning and sharing in their organizations, enabling every employee to learn to think like the company owner.

One way is to rigorously implement _open-book_ _management_, making decisions and the state of finances transparent to everybody in your organization.

Being transparent and teaching your employees can have a huge impact on your business's overall performance.

The author cites a meeting he once had with his staff, in which he discussed the impact one receptionist could have on the company's annual revenue. Doing his calculations in front of everyone, he explained how if one receptionist could get guests to upgrade their rooms by an average of $8, then — if you multiply that by fifty rooms per night, five nights a week, fifty weeks a year — it would add up to $100,000 annually.

The meeting boosted his employees' morale and the average room upgrade increased to $12.

Now, ask yourself what rules of thumb you use and how you can create an environment of constant learning in your organization.

### 8. The fast eat the slow. 

In the past, the market was dominated by big, tank-like companies with endless resources and economies of scale. Today, in the internet generation, where the additional cost of another electronic page is virtually zero, economies of scale lose their power and it's more important for companies to innovate quickly.

That means there's an intensified need for innovation and adaptability.

In the same way that expert surfers unite agility with instinct to look past the wave in front of them, taking into account the potential effects of the swell and the set, an agile company must also ensure that the strategy it's riding is a sustainable, long-term trend and not a short-lived fad.

One way to implement agility and motivate quick transformation is to celebrate failure.

Gavin Newsom, CEO of PlumpJack Group, a brand of wineries, resorts and restaurants in Northern California, realized his employees don't try new ideas mainly because they're afraid of doing something wrong.

He thus came up with an award of $600 per month for the employee with the most creative solution to a problem, even if the solution failed.

Thanks to that award, which highlights that Newsom valued creativity and innovation over perfection, the company has found many solutions that has led it to stand out. They were the first winery in the region, for example, to use screw caps on super premium wine.

Organizational structures and the bureaucracy surrounding businesses drastically reduces their agility. Reducing your organizational footprint to the bare minimum will help you to fulfill your core values.

So ask yourself: does your organizational structure keep you close to your customers? Does it enable an unhindered, informal communication between key departments? Does it allow project teams to focus on innovation and act with the long term in mind?

> _"It's not the big companies that eat the small; it's the fast that eat the slow."_ — Wall Street Journal

### 9. Customer service: employees as entrepreneurs. 

Service today is more important than ever, because competition is higher in most markets and customers expect better service.

The following three levels of services have an impact on customer satisfaction, from low to high impact: expected, desired and unexpected.

_Expected_ service includes average service and friendliness: customers may come back, but only if they don't find something better. _Desired_ service is what the customer prefers: customers will come back because it's just the way they like it. _Unexpected_ service is special and surprising, like an unexpected gift. Customers form a personal connection to you, will come back repeatedly and will even tell their friends about their experience.

One way to raise employee awareness to service levels is playing a game in which they have to name a place where they experienced extraordinary service and explain what made it extraordinary. They'll probably answer with things like, "The waiters always remember my name when I enter the restaurant" or "They love their product and it shows."

Your organization can also improve its ability to provide an extraordinary experience by determining its customers' _moments_ _of_ _truth_.

_Moments_ _of_ _truth_ are the critical points of contact your customer has with your organization. These are most likely the service points in your store, sales department or customer support center. Your job is to identify these critical points together with your employees, and determine the quality and impact of the current performance.

One good exercise is to go on a retreat with your service staff and let them identify and discuss all possible critical customer relation points in your organization. Then, they should specify steps for managing these moments and create service rules to ensure high quality all the time.

### 10. Figure out your psychographic profile and create buzz around your brand. 

When people buy products today, they aren't just buying stuff: they're buying a representation of themselves in the form of your brand.

That's why successful brands understand that the decision to buy something is highly emotional and often linked to our self-image, our aspirations and what our friends tell us.

So be sure to think of your target market as more than just demographics, study psychographics. Whereas demographics, such as age and gender, describe target markets, psychographics describe the emotional motivations beneath the surface, like the desire to be hip, successful, special, etc.

What could be a potentially lucrative target market for your company and how would you describe it in psychographic terms?

As a rebel entrepreneur, you can connect business's vision to the target market. Phoenix, a rock-and-roll hotel, used an analogy to _Rolling_ _Stone_ magazine, describing themselves as adventurous, hip, irreverent, funky and young at heart.

Psychographic profiles not only help you find your target customers, but also become the framework of your concept, strongly influencing the design, services and profile of your staff.

Once your concept is hashed out, tap into the _early_ _adopters_ to generate buzz. _Early_ _adopters_ are the type of people who try new inventions and styles first and are good at spreading ideas.

When clothing designer Tommy Hilfiger wanted to outpace Calvin Klein in the United States, he realized white teens often emulated the styles of African-American teens, who tended to have a rebellious urban fashion sense.

Thus, Hilfiger started targeting rap stars and urban teens, outselling Calvin Klein in the coveted under-twenties market.

### 11. Be prepared for the most common challenges of rebel companies. 

When your company outgrows its start-up character, you'll eventually be confronted with the problems that stem from more mature organizations, i.e., complexity and bureaucracy. As a rebel entrepreneur, it's your job to foster a little chaos and creativity in your organization. Here are a few tips:

Build a clubhouse for your innovators. If new and innovative ventures start appearing inside your organization, free them from the boundaries of your policies and organizational bureaucracy as soon as possible.

Your goal is to mimic the start-up feeling as authentically as possible, so use the advantage of your resources while preventing the disadvantages of excessively large teams and corporate clumsiness.

For example, when Hewlett-Packard started their internet business, they immediately put their project team in a different building, so their branch would feel small like a start-up, ensuring the freedom and thrill that leads to innovative behavior.

Another effective and easy way to ensure innovation is by setting a tough goal that a high percentage of revenue has to come from new products and services. By doing this — benchmarking their innovative revenue — HP and 3M, for example, have proved to be leaders in innovation.

Finally, you need to celebrate your rebels. Almost every industry is fighting a war for talent, so the rebels in your company are one of your most valuable assets. Your job is to identify them and make sure they stay with you.

One good way is to publicly celebrate your innovators and rebels with prizes and honors. Another good way — and possibly the only way to compete with the entrepreneurial enticements outside your company — is to outrageously compensate your top-rebels (much like NBA teams do with their biggest stars).

> _"The difference between a warrior and an ordinary man is that a warrior sees everything as a challenge, while an ordinary man sees everything as a blessing or a curse."_ — Carlos Castaneda

### 12. Manage your energy and time intelligently and protect yourself from burnout. 

In Japanese, the word _karoshi_ means "death by overwork." And while death is only the outcome of being overworked in rare cases, burnout is a widespread phenomenon. These days, most of us work far more hours than in the past and it seems we have a tendency to want it all.

Rebels are particularly vulnerable to burnout because they're used to pushing the boundaries of their own performance. And since they're always looking for challenges, they have a high tolerance for pain. In addition, most of the rebel entrepreneurs are working towards their life's dream.

That said, there are techniques for preventing burnout and helping you keep your feet firmly on the ground.

We all have our ups and downs, either over the course of a day (think: afternoon tiredness) or a year (think: winter depression). Similarly, your business experiences ups and downs. Now, think about how can you use that knowledge to organize your time as effectively as possible.

After all, the work you do is not only a function of the time you invest but also the amount of energy and creativity you have available at that time. Don't waste lots of time when you're feeling low-energy and uncreative. Schedule your most important and creatively demanding tasks for your up-times and redundant and simple task for your down-times.

But if you still feel you're drowning in tasks and challenges, consider taking some time off. We often need more than just a long weekend or a two-week vacation to ponder deep questions like: "Am I being driven by my ego or my soul?"

Many companies allow staff to take a month, or even half a year, to do the things they otherwise couldn't. Speak with your employer or implement sabbaticals in your own organization to give your rebels the freedom to be who they are. This will keep them by your side instead of burning out or switching companies.

> _"Stressed spelled backwards is 'desserts.'"_

### 13. Final summary 

The key message in this book:

**Connecting** **with** **your** **inner** **rebel** **makes** **you** **more** **successful** **in** **business** **and** **happier** **in** **life.** **You** **and** **your** **rebel** **company** **should** **make** **use** **of** **four** **distinct** **rebel** **attributes** **–** **vision,** **passion,** **instinct** **and** **agility** **–** **to** **keep** **up** **with** **the** **accelerated** **market** **conditions** **and** **spawn** **continuous** **innovation.** **Innovation** **and** **agility** **are** **more** **important** **than** **security.**

Actionable advice:

**Dare** **to** **connect** **with** **your** **inner** **passions.**

It may seem like the most natural thing in the world, but with time many of us lose the ability to tap in to our inner passions, let alone use them in our work. Think about what you loved most as a child and make it a part of your private and professional life.

**Identify** **your** **key** **values** **and** **rigorously** **live** **by** **them.**

Find and eliminate any discrepancies between your ideal values and parts of your life that don't represent them.

**Go** **beyond** **the** **numbers.**

Define your target group with a psychographic profile instead of a demographic profile.

**Suggested** **further** **reading:** **_The_** **_Art_** **_of_** **_Non-Conformity_**

Based on the author Chris Guillebeau's blog and online manifesto, "A Brief Guide to World Domination,"_The_ _Art_ _of_ _Non-Conformity_ deals with ways of pursuing unconventional living and offers tools for setting your own rules, succeeding with your passions and leaving a legacy.
---

### Chip Conley

Conley is an American author and speaker mostly known for his entrepreneurial success story as founder of the Joie de Vivre hotel chain. He has been awarded Northern California Real Estate Entrepreneur of the Year as well as Guerrilla Marketer of the Year by the American Travel Marketing Executives. His widely successful businesses and his renegade, artistic spirit have earned him feature stories in _The_ _Wall_ _Street_ _Journal,_ _The_ _New_ _York_ _Times_, _Time_ and _People_.

