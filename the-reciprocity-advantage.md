---
id: 54f59d64623735000a670000
slug: the-reciprocity-advantage-en
published_date: 2015-03-04T00:00:00.000+00:00
author: Bob Johansen and Karl Ronn
title: The Reciprocity Advantage
subtitle: A New Way to Partner for Innovation and Growth
main_color: FDB447
text_color: A36F21
---

# The Reciprocity Advantage

_A New Way to Partner for Innovation and Growth_

**Bob Johansen and Karl Ronn**

Big changes are coming to the way we live and do business. _The Reciprocity Advantage_ describes the global trends that will disrupt current business partnership models, and explains how you can build advantageous collaborations that'll stand the test of time. These blinks will equip you with the knowledge you need to succeed in the business world of the future.

---
### 1. What’s in it for me? Discover why you have to give in order to grow. 

The Beatles once sang that the love you take is equal to the love you make, and guess what: That's true in business too.

That's the major insight of the _reciprocity advantage_, which shows that the companies who are willing to give something up, share something, or collaborate are the companies that have an advantage over their competitors.

Whether it's old stuff you find in your company's symbolic "basement," or know-how you've built up over the years and aren't using to its full value, this pack will show you how to find that underutilized asset you already own and can share with others!

In fact, anecdotes from major companies and organizations like the US military, TED, Google, IBM, Lego, and many more prove that in an ever-changing world, the reciprocity advantage is the best way to push your organization forward.

In these blinks, you'll find out

  * what Lego pays people for ideas;

  * why Google set up faster wi-fi for the whole of Kansas City; and

  * how TEDx saved TED.

### 2. Locating unutilized assets and partnering in new ways are key to creating a reciprocity advantage. 

Have you got a basement full of junk that you scarcely touch? Many companies have a figurative basement full of assets that go unused. But just because something isn't _used_, doesn't mean it's _useless!_ The stuff in the figurative basement, or _unutilized assets,_ may hold great possibilities for creating new, mutually beneficial partnerships.

IBM is an outstanding example of this. As personal computers shrank in size in the mid1980s, IBM struggled to keep its bigger computers relevant in the market. To address this challenge, the company looked at what else it could offer, and realized that over the years it had built up a lot of in-house competence in managing and analyzing data: an underutilized asset. So instead of falling behind the times, IBM used this know-how as a base for new products and services, helping companies manage and analyze large data sets.

Today, IBM partners with governments, agencies and companies to create solutions that their clients wouldn't be able to come up with otherwise, such as redistributing traffic flow for improved travel within the city of Istanbul.

And their service-oriented approach is highly profitable. In 2000, IBM's profits from software were $2.6 billion, with $3.3 billion in hardware. But by 2012, their software profits had soared to $10.8 billion, while hardware showed very little growth. What if IBM had never thought of turning to services?

Learn from IBM: sometimes admitting defeat in one area can lead to a new, more productive path in your business. In this way, even enemies can become good partners. Take Microsoft's Kinect, a gaming platform that used sensors to track body movement. Shortly after its launch in 2010, the platform was hacked. Microsoft fought back at first but after realizing it was futile, the company decided to partner with the hackers and open up the platform for anyone to modify.

The technology, initially meant only for gaming, has now spawned a wide range of applications, and the software development kit for Kinect has been downloaded millions of times all over the world. Microsoft could not have gained such a wide reach without its hacker partners.

### 3. A willingness to experiment is key to creating a reciprocity advantage. 

Right now, all over the world communal areas where people come together to share resources and knowledge are popping up like mushrooms. These _maker space_ s have proved to be powerful engines for both collaboration and innovation. Companies certainly have a lot to learn from them.

Maker spaces show that sometimes, the point isn't necessarily to create profitable products. Rather, we gain far more by attempting to build and learn together.

TechSpace, a maker space in Menlo Park, California, is brimming with hackers, entrepreneurs and keen experimenters. Not only do all kinds of people help each other with their projects, but successful companies are emerging from the process.

One example is Square, a plug-in device for smartphones that enables credit card transactions. After making prototype upon prototype and using the TechSpace community to test them, Square quickly became a prosperous business.

If companies want to stand a chance in the future, they must experiment with new ways of making money. Google is ahead of the game in this respect.

In 2011 Google selected Kansas City as the first city to get the high speed internet system called Google Fiber, which increased the existing bandwidth a hundredfold. For Google, however, this wasn't primarily about testing new technology.

What Google was _really_ doing was investigating the possibilities of a reciprocity advantage. The experiment was a success. Today, Kansas has become a haven for hackers and makers who are using the increase in bandwidth to develop new products and services. So what does Google get out of it?

Well, in early 2014, Google announced it would take Google Fiber to 34 cities around the United States. The increase in bandwidth and the rapidly growing number of users made it possible for Google to collect usage data much faster, which is extremely valuable as they continue to build new platforms and services.

### 4. Reciprocity advantage is the key to scaling fast. 

Today, the internet allows us to scale a company in ways that would have seemed pure fantasy just 20 years ago. If you're willing to share and co-create with others, there is no limit to how much you can grow.

Sharing speeds up your growth _without_ hurting your core business. A case in point is TED and TEDx. When architect Richard Wurman created TED in 1984, it was intended to be the world's most elite conference.

But when Chris Anderson took over TED in 2001, he wanted to maintain TED's spirit, while also opening it up to new interaction and collaborations. So he started TEDx, allowing anyone to hold a conference. TEDx mushroomed, and the TED brand spread all over the world.

TEDx partners can use the TED brand and logo, and have the freedom to run their events as they please. But TED retains control by promoting good talks and limiting the reach of bad ones. Meanwhile, TEDx works as a spawning ground for new TED speakers. Instead of hurting the core business, TEDx made the "real" TED conferences more popular than ever.

Other new technologies will also allow your business to scale with ease by working with huge networks of partners. Take 3D printing. Since products will no longer be physically produced by a company, but printed at home instead, manufacturing and distribution will both be disrupted and scaled enormously.

Just imagine having partners all over the world who can tweak and improve an existing product, and then uploading it to the cloud, giving the world instant access to it. Quite an extraordinary thought! So what can we learn here?

Well, for one thing, it makes no sense to be afraid of the revolutionary change that collaboration can bring. Rather, you should look forward to expanding your process to include others, so you'll open up a whole new world of possibilities, and success.

### 5. The generation growing up today will challenge how companies make money, interact with their customers, and form partnerships. 

You may have heard the term _digital natives_ being thrown around in reference to people who were 18 years old or younger in 2014. This isn't just some new buzzword — digital natives have radically different attitudes toward companies, and the way they interact with them is bound to create change. They are the key to creating partnerships for the future.

Digital natives are bound to disrupt traditional methods for companies to make money. How? A good example can be found in _intellectual property_. The generation currently growing up will be diametrically opposed to intellectual property rights, and the companies trying to make money by selling them. So what should a company do if it wants to survive? Instead of seeking to _protect_ their intellectual property, it's time to explore what can be gained by _sharing_ it.

Another example of the way the younger generation is changing business is _crowdfunding_. Sites like Kickstarter and Indiegogo demonstrate just how powerful online communities can be in not just selling your product, but also making it possible in the first place. It's becoming easier and easier to start companies without having to look to traditional investors for support.

It's clear these digital natives are the most important potential partners for the coming decade. So how can companies get them on their side? Future companies will have to rethink concepts like "consumer," "advertising," and "commercials." In order to attract the digital natives you have to engage _with_ them, not simply _sell_ something _to_ them.

Relationships with digital natives will also become crucial for international corporations over the next ten years. Major reciprocity advantages will result from partnerships between these corporations and very small players in emerging markets in developing countries.

> _"It is one thing to be hungry and hopeless, but it is quite another to be hungry, hopeless and connected."_

### 6. A connected world will radically change the concept of collaboration. 

Instead of having an innovation department of ten people brainstorming in a stuffy conference room, why not have 10,000 people brainstorming for you? Having access to a global community of connected users won't just affect how and when we work. It will change the very way companies innovate.

Companies today feature employees that perform certain jobs. This is all starting to change. In the future, jobs will be broken down and distributed online. Take Task Rabbit, for example.

Task Rabbit is an online forum that allows individuals to perform all kinds of everyday tasks for you, such as walking your dog or picking up your groceries. One of the most common tasks that people request is putting together IKEA furniture — no surprise there!

Working arrangements in which jobs traditionally performed by one person are broken down into smaller parts and distributed online will become much more common. In turn, future companies can become more flexible and agile than ever before.

Companies are also changing the way they innovate by letting _customers_ do the work. _Open innovation_ has become a vital concept over the past few years. It works to open up your innovation process by inviting your customers to provide new ideas or solutions to a problem.

Customers themselves can be attracted to the task through competitions with cash prizes or a share of the profit when an idea is taken all the way to the product stage.

A notable example is Lego's CUUSOO, an online platform where anyone can submit ideas for new Lego sets, and vote for ideas that they think are winners. The ideas with the most votes are developed further by Lego, and if they're taken to market, the person who contributed the initial idea receives one percent of the net profit.

> _"It's amazing what people are now able to do with no money, no management, and no employees." — Marina Gorbis_

### 7. Gaming will change how companies engage with their customers and how we learn in the future. 

If you think games are just entertainment, think again! They can be much more — like simulations of the future or a means to bond with others. An ever-growing number of companies around the globe are discovering how to use gaming to their advantage.

Games are a proven way to engage more genuinely with customers. Economist Edward Castronova has studied players of large online games to try and grasp exactly why it is that people play. If you'd have guessed escapism, competition or entertainment, you'd better guess again.

People game for the _positive emotions_ that they experience after accomplishing a task, and for being recognized by others for their achievements. Take Minecraft, for example, an online game which isn't about scoring or winning. It allows you to build things with others using simple building blocks, and today has over 100 million registered players. Just imagine the possibilities for companies that learn to tap into the power of people playing and building together.

Not only will games bring us together in the future, they can also aid us in _experiencing_ the future, right now. With games, we can simulate situations at low risk in order to better prepare ourselves.

One of the leaders in this field is the US military, who have long used games to simulate live combat situations. At the National Training Center for the Army (NTC) in the Mojave Desert, soldiers play round-the-clock war games for up to two weeks so that they are as well-prepared as possible for the real thing.

And it's not just the soldiers that learn through playing. The military also get vital information on how to improve their operations. Companies have a lot to learn from this use of video games.

### 8. Cloud computing will radically change the way companies scale their business. 

Just as the railroads opened up a whole new world of commerce in the 1800s, the _cloud_ will open up major new possibilities for reaching more customers and scaling your business. In fact, the cloud is the infrastructure of the future. And we've only seen the beginning.

From what we've seen so far, it's clear that cloud computing will speed up innovation to an astonishing rate. Cloud computing will function as a medium for those shared assets, or _commons_, that we can all benefit from. Not only will cloud computing make it easier to increase the commons, but also for us to access and make use of them.

What's more, improved connectivity through cloud computing will create a shift in business models, as those based on sharing, lending, borrowing and trading will be made both viable, scalable _and_ highly profitable.

Cloud computing will also allow us to connect and partner in spite of language barriers. You might think of the internet as a predominantly English language domain. But in fact, the Chinese language internet is about the same size as the English language one. Multilingual tools will make building relationships between these two domains far easier.

But how? One such tool that can help us get there is machine translation, which, through cloud computing, will become much faster and more intelligent. Many modern machine translation tools have built-in self-learning mechanisms, allowing them to improve over time by analyzing millions of documents. The growth of the cloud will in turn speed up this process.

> _"The network is the computer." — John Gage_

### 9. Final summary 

The key message in this book:

**Sharing and working together with others in new ways will be the key to competitive advantage in the future.**

Actionable advice:

**Don't think product, think service!**

By redefining your business as a service you can locate assets you haven't yet utilized, like know-how and experience built up over the years. The next step is to try and find ways to share this with others to create mutually beneficial partnerships.

**Suggested** **further** **reading:** ** _The Lean Startup_** **by Eric Ries**

The _Lean Startup method_ helps start-ups and tech companies develop sustainable business models. It advocates continuous rapid prototyping and focusing on customer-feedback data.

The method is based on the concepts of _lean manufacturing_ and _agile development,_ and its efficacy is backed up by case studies from the last few decades.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bob Johansen and Karl Ronn

Bob Johansen is a fellow of the Institute for the Future, helping top leaders from all over the world not just prepare for, but also shape the future.

Karl Ronn has a background as vice president of research and development at P&G. Today he is managing director of Portfolio Partners, Palo Alto, and helps Fortune 500 companies create new business.

