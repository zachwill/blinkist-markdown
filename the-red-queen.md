---
id: 58eb3b45a54bbb000464bab8
slug: the-red-queen-en
published_date: 2017-04-10T00:00:00.000+00:00
author: Matt Ridley
title: The Red Queen
subtitle: Sex and the Evolution of Human Nature
main_color: C54136
text_color: AB382F
---

# The Red Queen

_Sex and the Evolution of Human Nature_

**Matt Ridley**

_The Red Queen_ (1993) takes a close look at evolutionary trajectories and how they have been guided more by reproduction than by survival. These blinks describe how the search for suitable mates has produced such remarkable phenomena as the spectacular tails of peacocks and the powerful intelligence of humans.

---
### 1. What’s in it for me? Learn about the role of sex in the evolution of animals – especially humans. 

Do you remember _Alice's Adventures in Wonderland_? In its sequel, _Through the Looking-Glass_, author Lewis Carroll has one character, the Red Queen, explain the workings of nature to Alice. The Red Queen tells Alice, "Now, here, you see, it takes all the running you can do, to keep in the same place."

The Queen's cryptic words are a now-famous allegory within the scientific community to describe what became known as the Red Queen hypothesis: in the natural world, every species needs to "run" — in other words, adapt and evolve — in order to survive.

These blinks take you on a journey through the fascinating facts of evolution. You will see how sexual reproduction is a driving force behind human evolutionary success, how the sexes have evolved and what causes the differences between male and female sexual tendencies.

You'll also learn

  * why polygamy can be beneficial;

  * why a marriage with affairs is best for women; and

  * why there are only two sexes, rather than three, four or more.

### 2. People may love sex, but it’s not very efficient. 

In modern society, sex is part of just about everything people consume, from car commercials to movies. For the most part, people love it — but how did humans come to enjoy sex so much?

Humans' love for sex actually has tremendous evolutionary benefits. For one, sex creates genetic diversity, as parental genes combine in novel ways. This process, in turn, drives evolution, as greater variety helps well-adapted species evolve.

Beyond that, sex also helps repair genes. For instance, a good strand of DNA from one parent can compensate for a bad strand from the other.

Without sex as a way to repair genes, the number of birth defects would grow greater with every subsequent generation; this process would be similar to endlessly photocopying a copy of a copy of a copy. Sex, on the other hand, uses two distinct originals to construct a new document based on both sets of information.

While sex does serve some key functions, other forms of reproduction are actually more efficient. Just take asexual species, which reproduce faster since individuals don't need to secure mates and can reproduce at a moment's notice.

In fact, while people might enjoy the chase of dating and the joy of relationships, having to find a suitable mate is an utterly inefficient process. Just imagine how much easier it would be to simply split in two like most microscopic creatures do, create seeds that are mirror clones of yourself like a dandelion or grow from a cutting like a willow tree.

Also, while sex speeds up evolution, evolution is not necessarily a universal goal, as some species do well with barely any change at all. Consider the coelacanth as an example; this Madagascan fish looks exactly as it did 300 million years ago.

And finally, you don't even need to have sex to repair genes; instead, you could just store a backup copy of them. Most plants and all animals already have at least two copies of every gene and some, like female yams, have eight copies of each!

It begs the question, why do humans still need sex? That's what you'll learn next.

### 3. Parasites would kill us all if we didn’t have sex. 

It's no secret that we're living in a dangerous world. But a much bigger threat to humans than terror attacks or tsunamis comes from other creatures: competitors, predators and parasites.

And while predators, like a bloodthirsty saber-toothed tiger, are indeed scary, they aren't the most dangerous of the lot. It's actually parasites that pose the biggest threat, which makes them a key factor in human evolution.

Although they're small, parasites' adaptability and sheer numbers make them the deadliest force on earth. As soon as a parasite learns to attack the immune system of a particular individual of an asexual species, it can rapidly wipe the organism off the face of the earth by targeting every member of the species in the same way.

This Achilles' heel is caused by the fact that asexual species exhibit only limited differences from one generation to the next. This is precisely why monocultures of corn or soybeans are so susceptible to devastation; they're too homogenous to withstand a specialized parasite.

Luckily for humans, sex helps us fight parasites. Intercourse ensures genetic diversity in offspring, which makes each generation less vulnerable to specialized parasites.

Through sexual reproduction, humans produce different versions of the same gene — a _dominant_ gene that influences appearance and a _recessive_ gene that remains hidden. This genetic system is called _polymorphism_ and it means that while a person might have brown eyes, she could carry a recessive gene for blue eyes, which she then passes on to her kids.

In a sense, polymorphism acts like a library for defense strategies. Every gene a person carries, even the recessive ones, sends information to their immune system that explains how to respond to parasitic threats.

For instance, in the 1980s, the American researcher Curtis Lively proved that sexuality in species is connected to the ability to fend off parasites. To illustrate his discovery, he studied a Mexican fish called the topminnow, which can procreate both asexually through cloning, or sexually through intercourse.

Lively found that the clones were often parasitized, while the sexually reproduced topminnows were almost entirely immune.

> _"Parasites are in a constant battle with their hosts."_

### 4. Evolutionary pressures shape the sex of offspring. 

Have you ever wondered why there are only two, and not three or four sexes?

Well, the answer once again comes back to evolutionary drives and the results of competition and adaptation. In fact, sexes emerged slowly as a result of microscopic competition.

For instance, the fact that most sexually reproducing animals have two sexes is due to the benefit the microorganisms in their bodies derived from their transformation into either males or females.

The DNA in the energy-producing cell structures called _organelles_ can only be passed through egg cells. As such, organelles would benefit evolutionarily when an organism shifted resources, from being hermaphroditic to being an egg-carrying female.

In turn, this benefit led to the production of so-called _male-killer genes_, which cause an individual to become female, thereby increasing the species' ability to reproduce.

With more females around, it became more attractive for other cells to specialize in male reproduction, as doing so would offer them many potential mates and therefore better odds of passing on their genes. In the end, hermaphrodites were no longer sufficiently specialized to compete with the males and females, and the two sexes prevailed.

Even today, the sex of animal offspring is still determined by evolutionary pressures. While many factors influence this process, for the most part, the offspring's sex is determined according to the evolutionary needs of the species.

Just take male opossums, which tend to be noticeably larger than females, as this helps them fend off predators and, in turn, produce more offspring. As a result, female opossums that are well fed, and can thus have heavier babies, tend to produce more male offspring.

Or consider baboons, for whom status is inherited. High-ranking females in baboon societies give birth to more female offspring who will retain their status, as opposed to males who often leave for another pack.

Not only that, but in 1966, the scientist Valerie Grant found that human women who get high scores on personality tests measuring dominance gave birth to more boys. From this finding, she argued that, evolutionarily, boys with such mothers would benefit more than girls would by inheriting traits related to dominance.

> Some snails change their sex depending on their neighbor's sex in order to find mates more easily.

### 5. Men and women may be different, but that doesn’t justify sexism. 

The differences between men and women are the subject of endless debate and arguments, and many societies work hard to minimize gender inequality. Opponents of this approach argue that these differences are simply natural. But are gender equality, on the one hand, and the acknowledgment of natural distinguishing characteristics between the sexes, on the other, mutually exclusive concepts?

For starters, it's clear that women and men are naturally different. In fact, even most cognitive tests can demonstrate gender differences. For instance, girls tend to do better at verbal tasks and object memory, while boys often do better in math and tasks requiring spatial logic.

Interestingly enough, these differences can be explained by biological factors. Boys with one Y and two X chromosomes are stronger verbally than the average XY chromosome boy. Meanwhile, girls with fewer male hormones are worse at spatial tasks compared to girls with average levels of these hormones.

If biology doesn't drive this point home, just consider the fact that several communities have tried and failed to eradicate gender differences. The Israeli kibbutz system is a good example: in the 1920s, these communities began working to eliminate socially assigned gender roles.

Three generations later, men and women in kibbutzim had reverted to traditional gender roles, with women doing housework and men working in business and engineering.

Even so, evolutionary differences between men and women don't justify sexism. After all, these differences are real but relative. For example, the range of differences among male brains is greater than the difference between the average male and female brains.

Beyond that, many of the differences between men and women are socially conditioned, since society also influences evolution.

Over the course of generations, evolution transformed the human brain; as the circumstances of our ancestors' lives changed, so did their brains. At the same time, however, social factors shaped this process.

For instance, a lack of supportive systems for childcare in a society might lead to fewer total births or more children born to wealthier parents. As a result, different genes are passed on to new generations, depending on whether such services are provided.

### 6. Evolutionary imperatives make male animals show off. 

From the tails of peacocks to the love songs of nightingales, males in the animal kingdom sure seem to enjoy flaunting their appearance or abilities. But why?

Well, certain male traits have been passed on through the generations because they were attractive or fashionable. Just take male zebra finches. Females find those with red rings on their legs more attractive than those with green rings. Therefore, a gene that causes such a feature would be likely to increase reproductive success and be passed on.

Or consider the _sexy son hypothesis_, first proposed in 1930 by the biologist Ronald Fisher. He postulated that it's to the evolutionary advantage of females to enjoy traits in males that other females find attractive as well.

For example, imagine a peahen mating with a peacock that's unattractive to others because of his short tail. Their short-tailed sons would also be unattractive and have lower chances of reproducing. On the other hand, a peahen with more mainstream tastes — say, a preference for long tails — would be successful in producing offspring that go on to mate with many females and procreate more.

In a similar respect, traits that reflect the health of an organism also increase its chances of reproduction. In short, if you look healthy, you have access to more potential mates.

This interesting fact is a result of evolution over many generations. Basically, animals that were unconsciously wooed by healthy mates ended up with resilient and fertile partners that passed those traits on to their offspring.

But how can you tell if a mate is healthy?

Well, spectacular or symmetrical features are a good sign. In the case of chickens, colorful combs are an indicator of good health, while the tails of male swallows are more symmetrical if they're fit.

The Israeli scientist Amotz Zahavi's _handicap theory_ suggests an interesting explanation for how extraordinary features signal health. The theory says that males with fashionable handicaps prove their ability to survive while vulnerable. For instance, long tails or loud singing make males more prone to predators, but as they survive this threat, females can infer that they are in good health.

> _"A peacock's tail is a testament to a runaway product of despotic fashion among peahens."_

### 7. Polygamy fosters the spread of quality genes, but it comes at a cost. 

Nowadays, humans are pretty set in our monogamous ways. But do you ever wonder why most people don't have more than one partner at a time?

For some males, polygamy actually offers profound advantages. For instance, it ensures that their offspring inherit quality genes.

After all, men can father more children by mating with multiple partners, and polygamy allows women to team up to share the most appealing men, instead of being stuck with the second-best options. This type of behavior can be seen in the animal kingdom, like in the case of Elephant Seals who engage in massive tournaments known as _leks_.

In these competitions, the males fight each other one at a time, with the winning male mating with the surrounding females, who in turn produce children with his high-quality genes.

In humans, the same behavior can be observed in societies stratified by class. Just take the Kenyan Kipsigis. The women of this tribe prefer to be the third wife of a rich man rather than the sole wife of a poor man.

And men aren't the only ones who practice polygamy — it's just that the sex with less responsibility in child-rearing is more likely to have a greater number of partners. For instance, in the case of a bird called the _phalarope_, the smaller males tend to the eggs while the larger females enjoy polygamous relationships.

However, while polygamy can offer some benefits, monogamy will always prevail. This is due to the _monogamy threshold_, which is reached when there are too many females mating with just one male.

In cases like these, lots of males are left without mates. For these males, and for the less-favored females, it then becomes beneficial to practice monogamy. By mating with just one female, and helping her care for her young, these males can guarantee to pass their genes on to at least one descendant.

Monogamy can also be observed in species like the albatross, the females of which benefit more from parental support than from the genetic benefits of polygamy.

Most legal systems now prohibit polygamy, and such a practice probably isn't in the best interests of your average guy. But when it comes to evolution, which approach is more advantageous for women? You'll find out next.

> _"Mankind is a polygamist and a monogamist, depending on the circumstances."_

### 8. It makes evolutionary sense for women to seek out both long-term partners and casual affairs. 

It's hard to say whether monogamy or polygamy is best for women, but the style of mating they engage in has a major impact on the amount of assistance they can expect when raising children. After all, committed partners can be a huge help in child-rearing for many species. That being said, human monogamy is special.

For instance, when it comes to female apes, there are generally two options: they either live in a multimale group in which they have sex with multiple partners, or they live alone with just one partner. Humans are different in that we're the only monogamous "apes" that live in groups.

Having monogamous partners makes sense for us since a loyal mate doesn't need to divide his time and resources between several families. Beyond that, having multiple partners doesn't necessarily help women in the evolutionary long term, since their ability to reproduce is limited. Studies have even suggested that, on average, women aren't as interested in sexual variety as men.

Indeed, the only reason certain female apes would choose to mate with multiple partners is to protect their children, because male apes will only kill children they know are not related to them.

However, even in monogamous relationships, looking for a lover on the side is only natural. Females of all species are compelled by a desire for a committed partnership, but also by access to the best possible genes. For this reason, the most beneficial system for them is monogamy with the option for affairs.

Consider the findings of zoologist Nancy Burley. She discovered that attractive male zebra finches are much less invested in raising their young. For this reason, the females find it most beneficial to have affairs with attractive males _and_ have a reliable mate back at the nest.

And that's not the only reason affairs are helpful; there's also a higher chance of impregnation when sleeping with a lover. Female orgasms during sex boosts the chances of conception and, according to a 1980 study by the British zoologists Robin Baker and Mark Bellis, orgasms are more likely to occur within extramarital affairs.

### 9. Human intelligence is key to our survival and mating. 

In the modern world, it's clear that humans rule the animal kingdom. After all, when we consider our progress in fields like art and technology, the products of human intelligence are truly astounding. But how did we come to have such powerful brains, and are they really necessary from an evolutionary perspective?

The short answer? Not really. Human intelligence is actually excessive relative to what's needed for survival. In fact, about a fifth of our energy is consumed by our brains.

Until the mid-1970s, it was argued that our brains developed such power to share information and create tools for survival. But even by the 1960s, zoologists had discovered that other apes could build tools, too. There are chimpanzees and bonobos that can build tools quite effectively with brains much less developed than ours.

So, it's clear that our intelligence is about more than just fashioning tools; the real reason for our tremendous brainpower is the need to outsmart our neighbors.

The concept of a race to greater intelligence comes into focus when we consider that humans stopped competing with other animals early on, instead focusing our competitive energies on one another. This shift made sense since living in an intensely social environment makes social status our main resource.

Status can be difficult to determine based solely on looks and strengths, and is instead measured by a person's ability to control her social environment. As a result, the wittiest and most creative people quickly started to become the most desirable partners.

This is supported by the findings of the American psychologist Geoffrey Miller, who concluded that the _neocortex_, the evolutionarily newest addition to the human brain, developed the capacity to help stimulate and entertain others — a key ability when courting a mate in our social world.

In this way, a human's intelligence is just like a peacock's tail in the game of sexual selection. People became smarter because the smart ones always found partners — in other words, we're intelligent because intelligence is sexy.

### 10. Final summary 

The key message in this book:

**Sex is central to human civilizations, and for good reason. It ensures genetic diversity, protects us from parasites and even accounts for our tremendous intelligence. In these ways and many more, sexual attraction among humans is a key driver of evolution.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Evolution of Everything_** **by Matt Ridley**

_The Evolution of Everything_ (2015) argues that the phenomenon of evolution — gradual change without goal or end — reaches far beyond genetics. Evolution happens all around us in economic markets, our language, technology and customs, and is what's behind nearly all changes that occur in these fields.
---

### Matt Ridley

Matt Ridley is a British journalist, businessman and author who has worked for the _Economist_, the _Times_ and the _Daily Telegraph_. Now an editor of the _Best American Science Writing_, Ridley is known for his science-focused books such as _The Rational Optimist: How Prosperity Evolves_ (2010) and _The Evolution of Everything: How Ideas Emerge_ (2015).

