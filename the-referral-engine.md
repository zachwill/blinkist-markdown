---
id: 5a5168afb238e100063370a4
slug: the-referral-engine-en
published_date: 2018-01-10T00:00:00.000+00:00
author: John Jantsch
title: The Referral Engine
subtitle: Teaching Your Business to Market Itself
main_color: F9AB47
text_color: 915E1D
---

# The Referral Engine

_Teaching Your Business to Market Itself_

**John Jantsch**

_The Referral Engine_ (2010) is a practical guide to developing an in-depth referral-based marketing strategy for your business. These blinks explain why referrals are so powerful before taking you through a step-by-step process to build the referral machine that your company wants and needs.

---
### 1. What’s in it for me? Learn the six foundational elements of a referral engine. 

Is your business struggling to grow? Do you see other companies rapidly attaining success as their loyal customers spread their brand, story and product all over the internet and the world?

Well, the difference is in referrals. Referrals are recommendations for your company made by customers or partners to new prospective customers. They're essential to any in-depth marketing strategy and can make all the difference between a floundering start-up and the next big thing.

To make your company "referral worthy," six aspects are essential. Combined, they make up what's known as a "referral engine," an unstoppable referral machine that will propel your company to success. In these blinks, we cover what they are and how you can apply them to your business.

You'll also learn

  * why getting more customers isn't always a good thing;

  * why you shouldn't be afraid of giving away too much; and

  * how you can collaborate with other businesses to your mutual benefit.

### 2. Humans are hardwired to share information they trust, an impulse you can use to your business’s advantage. 

So, you want people to refer your company to others. Well, you can make that goal a reality by tapping into the psychological impulse that compels humans to make recommendations of any kind.

The social nature of humanity means people want to make referrals as a means of earning recognition. This makes perfect sense when you consider that, within tribal societies, maintaining good social standing was a precondition for survival. After all, failing to make people like and value you could mean rejection from the tribe and near-certain death.

As a result, a deep desire for social validation is hardwired into our brains. In fact, the quest for validation from others is so deep within us that it's controlled by the same part of the brain that's responsible for primitive functions like hunger, sleep and sex drive.

To benefit from this impulse to earn social validation, your business should only present relevant and useful information. For instance, telling another tribe member about a good fishing spot during a time of famine would naturally earn the referrer a good deal of respect.

But how do you make your business _referral worthy_? By showing that it can be trusted to provide a stellar solution or experience.

This is a vital step because making referrals always carries with it the risk of losing credibility if what is being referred fails to deliver. As a result, people rarely refer businesses they don't trust.

To make matters more complicated, building trust is a long-term game, requiring commitment and consistency. Take Scott Ginsberg, an author and speaker on the art of approachability, for example. He has worn a name tag every single day for over eight years.

Naturally, this grabs the attention of others in public, who often approach him. More importantly, however, this commitment to standing out has earned him the trust of others as a true expert on approachability.

> _"Your employees probably treat your customers about the same way you treat your employees."_

### 3. Referrals come from standing out and following your purpose. 

Why might a customer refer you? It's an essential question that your referral strategy must address.

To find _your_ answer, you have to offer something different that people will talk about, or identify a way that you already do.

Keep in mind that, while such a difference has to be extraordinary, it can often result from a slight change, rather than a groundbreaking innovation. The aim here is simply to take a product or service that's already in high demand and make it easier to want or need.

To do so, you need to innovate by doing things differently in a proven market or simplifying the product you already offer.

A great example of this _differentiation strategy_ is the Cheeseboard Pizza Collective in Berkeley, California. Unlike normal pizza shops, it only offers a single kind of pizza each day of the week. This fixed menu dramatically lowers costs and allows the business to use higher quality vegetarian ingredients. Such differentiation through innovation has meant that this cooperative has a lineup out the door and down the block, day after day.

While implementing this type of change might be uncomfortable for your business initially, just remember: you'll never maximize your difference unless you're willing to change.

So, apply a differentiation strategy and, once you have, stand out even further by infusing it with authenticity. People want to connect with and talk about businesses that are driven by a powerful purpose, and the wonderful thing about this greater purpose is that you don't even need to communicate it; if it's a true purpose, it'll be present in everything you do.

Businesses with a social mission, like TOMS Shoes, offer an inspiring example. TOMS donates a pair of shoes to a needy child for every pair that it sells, in the process differentiating itself in an authentic way and leading it to great success.

To make your business more authentic, take a look at changemakers in industries other than your own. Just observing what other businesses do will give you a fresh perspective on your own.

### 4. Find your ideal customers and cater to them. 

You may think that getting more customers is always a good thing, but the truth is a bit more complicated. Having the _right_ kind of customers will increase the positive experiences your clients have, thereby producing buzz for your business.

In other words, not all customers are a match for your business, but you can identify which ones _are_ by creating a profile of your ideal one. Just think of a current customer who brings you the most profit and referrals.

From there, be as detailed as possible: incorporate real customer stories, ask questions about the challenges your customers face, the people they trust and, finally, go to the customers who already refer your business to ask them why they do so.

For instance, the customer profile for a home renovation company might be married homeowners with a combined income of over $100,000, who are entrepreneurial and have a long term plan for their lives. They're engaged in their local community, have no intention of moving and have renovated before.

Then, with your customer profile in hand, you can produce a _key story_ that'll engage your customers emotionally. This is essentially a narrative that you convey through your actions, marketing and branding.

When you design it, keep your ideal customer in mind, trying to speak directly to her. You should acknowledge her emotions and focus on what best connects with and captivates her.

Try to keep this story about you and your company concise, no longer than a single page and be sure it's personal, revealing and honest. You'll want to communicate things like who you are, why you do what you do and what you fear. In other words, you should seek to reveal your passion, how you found it and how you want to add to the world.

What you _don't_ want to do is a give a chronology of your company.

Finally, once you have your story, test it out on your friends, neighbors and even your spouse. Figure out what resonates most deeply and revise your narrative accordingly. Once it's polished, you can use it to engage with potential customers.

### 5. Offering valuable content and gathering customer testimonials will provide the foundation for your marketing approach. 

Now that you've set yourself apart from the pack, how can you start creating valuable content that your customers will learn and benefit from?

One way is to write a five- to 20-page educational white paper based on the core principles of your business. This document can serve as your go-to educational tool for all prospects and partners.

In a nutshell, its educational value should demonstrate to others why they should refer your business. For instance, the author distilled his entire previous book, the _Duct Tape Marketing_, into a seven-step white paper that centers on his strategy for differentiation. Since then, it has been downloaded hundreds of thousands of times, serving as his primary lead conversion tool.

Once you have your central white paper, you can create additional content based on it and publish this content across a variety of media. For example, how-to-guides, e-books and case studies can be published in print or online as PDFs, slide decks and recorded audio. Being able to repurpose this content across formats will breathe new life into it while reducing the work of creating new content from scratch.

If your ideas for new content run dry, just take some customer questions and answer them in a blog post or on another medium.

And finally, you should host a testimonial gathering party for your best customers. The stories you acquire from such an event can then be incorporated into all your other value-giving materials. To get people to the party, sell it as a networking opportunity, but also as a chance to play a central role in the creation of your company's new marketing materials.

Then, during the event itself, have guests tell their stories in front of a camera. The pictures, video and audio from the party will comprise a collection of testimonials that you can integrate into your marketing materials for years to come.

> _"Educational content minimizes the need for selling."_

### 6. Leverage online ads, PR and speaking engagements to spread your content. 

In marketing, exposure is the name of the game. Here are a few useful exposure tools to put into practice:

First, take advantage of the powerful targeting opportunities afforded by online advertising. While lots of people believe online advertising has low conversion rates, this misses the point; your goal in using such tools is to direct traffic to your value-giving content. In the end, it's your quality content that will turn interested visitors into customers.

The second tool at hand is press coverage, which is great for generating buzz and awareness, but also third-party validation and trust. A good press strategy begins with compiling a list of journalists that might be interested in your story.

Subscribe to their e-mail alerts and follow their social media accounts; read everything they write and learn what interests them the most; subscribe to their blogs and comment with relevant ideas; and, finally, begin passing on content that's relevant to both your field and their journalistic work. By following these steps, you'll build credibility and reliability before preparing to push your organization's story.

And third, you can get exposure and build your reputation as an expert by taking on speaking engagements and putting together an educational presentation that can be delivered in person or in web conferences.

The presentation can simply be an expanded version of your white paper, or something indirectly related. The key is that you teach your audience something valuable and you shouldn't be afraid of giving away too much.

But in addition to the value you offer, you should also be sure to prompt your audience with a call to action. To do so, begin by letting them know that you'll provide them with helpful information. Then, after building trust, mention one of your paid offerings and how much it costs — say, a two-week program to learn the secrets of recruiting top sales talent.

After your talk, take audience questions and mention that anyone in the audience can bring a guest to your program for free if they sign up today.

### 7. Successful referral-based marketing combines in-person and online tactics. 

Marketing has evolved dramatically over time, and in the modern world, businesses tend to fall into two different marketing categories: those that focus on online marketing and deliver a wealth of content, and more traditional businesses that focus on in-person interactions, in which the customer benefits from a greater sense of connection and community.

However, true success means combining these approaches. The synthesis of online marketing with in-person conversations will forge a powerful trust-building synergy that's vital to any marketing effort. For instance, after you meet a prospective client, you could contact her on LinkedIn, passing along one of your relevant educational blog posts.

Remember, social networking gives you incredible access to millions of prospective customers, and if you can add authenticity to these online connections, there's no limit to the success you can experience. When building trust in your online interactions, these three tips can help:

First, publish links to your educational content through your social media account; second, actively share quality content and engage with prospective customers; and third, focus on partnerships by offering value and letting your educational content sell your product for you.

When navigating through this terrain, blogs can be especially powerful as a means of uniting the online and in-person realms. After all, blogging gives you search engine exposure, community building through a reader base and a connection with customers through comment dialogue. Because of this, blogs are a great place to test and forge new ideas.

The only problem is that some 900,000 blog posts are published worldwide every day, which means you need to put in the effort to produce high-quality content — otherwise, you'll be ignored.

To make sure your content hits the mark, there are a few things you can do. For starters, follow as many relevant blogs as possible and actively comment on them. On your own blog, write a minimum of three posts a week that incorporate relevant keywords identified through Wordtracker or Google's Keyword Tool. And finally, invite guest bloggers to write posts for your site to get a perspective from outside your organization.

### 8. Facilitate customer network referrals through a systematic referral process. 

The previous blinks have given you some great tools for collecting referrals, but it's also important that you make it as easy as possible for customers to refer you when they are inclined to do so.

A good start for this is to give every new client a _customer bill of rights_. This document is akin to an owner's manual, like the one that comes with a new power tool; its purpose is to make sure customers know what their rights are and what they can expect from the relationship with your business.

It should contain information on how to get started, what your agreement entails, how they can get the maximum value out of your product and explain how to resolve problems. Additionally, this kit could be accompanied by a series of automated e-mails that offer tips, or a complimentary phone consultation after purchase.

You might even include other perks that exceed expectations — customers love surprises like this.

All it takes to make one is to add a little something extra to your offering without advertising the added benefit. For instance, if you sell a logo design to a client, you might throw in 200 free business cards featuring the new design.

So, setting customers up to refer your business is essential, but that's just the first step in this process. The second move is to foster customer loyalty.

After all, the majority of your referrals will come from a minority of customers. These are your referral champions, and you need to show them that you care. For instance, you might ask for their input on business matters, or mail them a handwritten note with a little gift, like a pair of movie tickets.

Finally, make sure your customers have the tools they need to refer you easily. A great way to do so is by giving them gift certificates or referral cards that they can hand out to family, friends and coworkers. These cards should be personalized and give credit to the customer for making the referral.

> _"The lifetime value of every single customer is unlimited when you factor in a customer's ability to make referrals."_

### 9. Build a strategic partner network with businesses that share your target market. 

By now you know how crucial customer referrals are. However, another group holds the potential for even more expansive reach: partners. These are businesses with a common ideal customer and reaching out to them is vital.

You can start by listing the high-quality companies you trust and would happily recommend to your own referral stars. It's good to start with businesses you personally know and use.

From there, send a letter of introduction to each of them, communicating that you have customers who might be interested and that you'd like to learn more about the company. For instance, you could request information about their value proposition and post-referral marketing procedure.

After identifying potential partners who are interested, your job is to help them recognize partnership opportunities in which you both win. _Co-branding_ is a great example.

Say you have a white paper entitled, "The Top Five Referral Marketing Tips" that's relevant to your partner's customers. You might offer your partner the opportunity to add their logo and contact information to the paper and allow them to distribute it to their customers for free. In exchange for this valuable content, you'll gain natural exposure.

Similarly, you might offer to give an informational workshop to their customers. Or, you could organize an event with three or more partners, in which everyone has the opportunity to invite customers and present on their area of expertise.

Lastly, joint marketing ventures can be an extremely productive use of your partnerships. For instance, in a three-way partnership between an electrical contractor, a plumber and a heating and cooling business, all three parties could hand out one another's marketing materials and coupons.

One of the best things about partnerships is that those with a certain edge of creativity to them are especially buzzworthy. Just take one IT company that offered free massages during its recruitment presentation. This odd stunt got the company the attention of the top accountants it was seeking, while earning new clients for the massage therapists involved.

### 10. Make a plan to receive referrals and thank those who make them. 

Now that you have your referral strategy laid out, you need a plan for actually receiving those referrals.

The first step is to decide on the best time for asking customers to make a referral. This is usually when key customers express how impressed they are with your offering.

The second step is to get your employees involved in referral-based marketing. To do so, simply encourage them to use every customer interaction to connect the customer with the brand.

Third, you need to start giving referrals in order to receive them. To jump-start this process you might launch a "make-a-referral Monday" initiative, during which employees are assigned the goal of making at least one referral at the start of each week. Making this a tradition will instill referrals into your company culture and strengthen your partnership networks.

Then, once those referrals start rolling in, you of course need to plan how you follow up with them. This process starts during your initial contact, when you should acknowledge that they were a referral and by giving them something extra. Whether that little add-on is free information or a box of chocolate, the most important thing is that it feels special. From there, you should make sure your referral sources feel appreciated for their work, regardless of whether it leads to a sale or not.

But it's just as important to properly handle cases in which a referral isn't a good match for your product or service. When this happens, don't ignore it; instead, make the effort to give a clear idea of who your ideal customer is when contacting your referral source.

Similarly, you should always keep your referrers up to date on the referrals they've made. For instance, you might send an e-mail to a referrer that notifies her when a referral she made becomes a client.

And finally, thanking your referrers publicly is a powerful motivator. This can be as simple as adding a referral page to your website or tweeting out thank yous. Doing so will demonstrate the value you place on referrers and support the image of your business as one of referral quality.

### 11. Final summary 

The key message in this book:

**Getting customers and partners to recommend your business to others is essential to sustained and healthy growth. By forging deep connections with people and spreading awareness of your business, you can make your brand stand out and earn your company a reputation for being worthy of referrals.**

Actionable advice:

**Create a list of** ** _trigger phrases_** **.**

Have you ever heard of a _trigger phrase_? It's a series of words that a prospective customer is likely to say when communicating that he's looking for what you have to offer, but is unaware that you hold the solution to his problems.

For instance, a trigger phrase for an accounting software company might be a complaint about insufficient data management. The prospect could say, "I keep all our finances organized on spreadsheets, but it takes _forever_ to update."

Develop a list of such phrases for your own company by brainstorming common themes with your salespeople, or through conversations with a few key customers. Once you identify them, you can use these phrases to identify new prospects and by putting them in your marketing material, your customers will start using them too, thereby increasing the chances of their referral leading to new business for you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Word of Mouth Marketing_** **by Andy Sernovitz**

These blinks explain how generating positive word of mouth has become such a powerful marketing tool that it's even more effective than traditional advertising placements like television. Through examples and clear guidelines, the following blinks describe exactly how you can facilitate positive word of mouth for your products.
---

### John Jantsch

John Jantsch is a marketing expert who shares his expertise as a consultant, speaker and author. His other books include _Duct Tape Marketing_ and _The Commitment Engine_.

