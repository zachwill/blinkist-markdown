---
id: 5523833939653600071c0000
slug: the-republic-en
published_date: 2015-04-07T00:00:00.000+00:00
author: Plato
title: The Republic
subtitle: None
main_color: 798ABF
text_color: 3E496B
---

# The Republic

_None_

**Plato**

Plato's _Republic_ (c. 380 BCE) is a dialogue in which Socrates and his interlocutors discuss the attributes and virtues that make for the most just person and for the most just form of government. The _Republic_ also examines the relationship between the citizen and the city, and considers how this relationship bears on philosophy, politics, ethics and art.

---
### 1. What’s in it for me? Explore the philosophy of justice with Plato’s best-known work. 

What makes a person just? And how can we define a just city? Answers to these types of questions are what Socrates, the gadfly of Athens, seeks to ferret out over the course of the _Republic_.

Though Plato wrote this dialogue more than 2,000 years ago, it is still the centerpiece of both philosophy and political theory. Indeed, Sir Alfred North Whitehead, the mathematician and metaphysician, once called all of philosophy "a footnote to Plato." An indispensable part of the Western canon, the _Republic_ puts you in the shoes of one of Socrates's students, following him around as he questions various people on the roles played by justice, philosophy and art in the forming of the city and of the soul.

So in these blinks, you'll find out

  * why merely appearing to be just is the worst type of injustice;

  * why the "noble lie" ties people to their community; and

  * why the soul is composed like a speech.

### 2. Socrates questions and dismantles the definitions of justice that his dialogue partners propose. 

How do you define justice? No matter how well considered your response may be, Socrates would probably be able to dismantle your definition. Throughout the dialogue between him and his interlocutors, he examines and questions several definitions of justice.

The first definition comes from Polemarchus, who claims that justice is to give each person what they are owed. In response, Socrates tries to undermine this definition by finding exceptions to it. What if weapons are owed? Although one should return what one owes, one should not offer weapons to someone who is insane and threatening to harm someone.

So, the definition of justice as "giving what is owed" doesn't always hold.

Polemarchus then provides another answer: Being just means assisting friends and harming enemies. To this, Socrates queries whether there are circumstances under which it is moral to do harm. He finds that there aren't. Animal trainers, he says, don't benefit animals they harm; likewise, people become less moral if harmed. Additionally, one can mistake friends for enemies, and enemies for friends, and therefore end up benefiting those one meant to harm.

So, since harming someone isn't beneficial and our judgments cannot be absolutely accurate, this second definition also falls apart.

The third definition, posited by Thrasymachus, is that justice is whatever is advantageous to the ruler.

Socrates questions whether this definition also applies to those in other positions — such as, say, a doctor. The health of the patient, rather than the doctor's benefit, should be the doctor's main concern. A ruler that seeks to benefit himself, instead of his people, is not a just ruler. Like the doctor, the ruler should aim to do good for his "patient," i.e., the city.

This third definition is also inadequate and so the first attempts to define justice come to an _aporia_, an impasse in the dialogue.

> _"So long as I do not know what the just is, I shall hardly know whether it is a virtue or not, and whether the one who has it is unhappy or happy." — Socrates_

### 3. Justice cannot be examined independently of individual and city. 

After this impasse, Socrates proposes his own definition of justice: minding one's own business. This, he says, has both a private and a public aspect to it.

To mind our own business is to responsibly play our appropriate role, and thereby benefit both ourselves and our city. The citizens of a city that functions in a just and well-organized fashion each have their role, perfectly suited to them. Because of this, no one person has to take care of everything themselves.

Socrates specifies that a city should include craft workers, doctors, merchants, rulers and soldiers and that each person should acknowledge their individual role and then ably fulfill it. Knowledge of one's role depends on the city's having just institutions that educate inhabitants on their appropriate duties.

Once they know what their duties are, individuals mind their own business by carrying out their role in a just and appropriate manner. This, in turn, reverberates through the city, making it either just or unjust.

Socrates explains that not everyone is appropriate for every role, however. For instance, someone suitable for being a general won't necessarily make the best horse-trainer.

Each person's job must benefit the community at large — that's its social role. Take the example of the ruler: a just ruler reigns for the city, whereas a tyrant rules for his own gain. So, a tyrant's actions reflect the corrupt society he controls, while a just ruler's actions reflect the just city he rules.

Justice for each person cannot therefore be viewed independently of justice for the city.

Determining one's role is never an individual's decision, but is shaped by the needs of the city and by the individual's skills.

In an ideal and just city, the city's needs and the individual's needs work symbiotically, the city benefitting from its people and its people benefitting from it.

> _"However, in founding the city we are not looking to the exceptional happiness of any one group among us but, as far as possible, that of the city as a whole." — Socrates_

### 4. People and cities have to be just. To simply appear just is the worst type of injustice. 

There is a thread that runs through the dialogues on justice — the difference between essence and appearance. That is, how something appears as opposed to what it actually is. The greatest kind of injustice is for someone to seem just, when in actuality he is unjust.

Plato's brother, Glaucon, now joins the dialogue. Both Glaucon and Socrates attempt to understand justice and present the idea that a just life is more desirable than an unjust life.

Glaucon, playing the devil's advocate, makes a claim that he wants Socrates to disprove. His claim is that the majority of the population considers the mere appearance of leading a just life to be better than actually _being_ just.

Socrates, however, not only refutes this but stresses that such a life is extremely unjust.

It's comparable to someone who seems to be a skilled weapon-maker, when they are actually incompetent, he says. Such false claims would lead to robust-looking shields that disintegrate in battle. The point, here, is that one's true character has nothing to do with appearances. Put someone to the test and you'll learn what kind of metal they're made of.

Finally, Socrates states that one can discern whether someone is just or unjust by studying their environment — the city — and the relations they have with others. Therefore, in order for an individual to be just, their city must be just, and not merely seemingly so.

Socrates then says that without a just city, just individuals cannot exist. So individuals who live in cities whose laws benefit the few, not the many, live in unjust cities, even if they appear to be just.

Such cities are often ruled by tyrants, whose unjust acts are used to build a reputation of justice. The laws of the tyrant always favor him and disfavor all who go against him. Rather than reaching for a common good, the tyrant seeks only to gratify his personal goals.

### 5. Education and a “noble lie” are necessary for justice. 

Socrates postulates that education should instruct individuals to be just. Therefore, a sound education is one that enables individuals to have a healthy mind and body that can shield and strengthen the city.

For example, musical education paves the way to a healthy mind, and gymnastics leads to a healthy body.

Music helps educate the mind and soul through rhythm and harmony, both of which can bestow balanced mental order and lead to a just character. This balanced order is also needed for a variety of arts and crafts.

Gymnastics, on the other hand, promotes physical strength and solidifies group cooperation. In particular, Olympic sports foster both individual strength and group mentality.

Individuals strengthen themselves by running or javelin throwing. Groups train by wrestling and engaging in combat exercises, activities that necessitate cooperation among individuals, and thus improve group mentality.

The benefit of music and gymnastics is that they make citizens healthy in mind and body because they enable the progress and strengthening of a city's culture and military.

While a healthy mind and body are advantageous to the individual, something else is necessary in order to promote justice and make the individual feel involved in the future of her city: a _noble lie_ that connects individuals to their city and their community.

The noble lie teaches citizens that the Earth is their mother and nurse, and that all citizens have risen from beneath the city. As the city's foundation is the Earth, so the citizens also depend upon the Earth, which bore them. According to Socrates, individuals must be told this lie — or an equivalent myth — by their guardians. It's what makes them feel connected to their city.

The noble lie ensures that people will protect the city in times of conflict and reinforce it in times of peace.

### 6. Socrates compares the city to the individual by drawing an analogy between the just city and the soul of the just person. 

It's impossible to study someone without also examining their city, Socrates says. Not only does a city create its citizens, but the citizens also form and develop their city. The just person and the just city need each other.

A city forms its citizens in accordance with its laws and institutions. Then, as citizens mature and take on different offices, they can alter laws and devise new ones, helping the city to progress along with them.

You cannot, therefore, have a just person in an unjust community, or an unjust person in a just community.

To demonstrate his point, Socrates draws an analogy between the city and the human soul.

When Glaucon requests that Socrates examine the soul of the just person, Socrates says the soul is like a speech, because it has reason and logic. The soul of a person can be revealed through conversation with that person and through her explanations of her behavior.

The just city is like a just person, only on a larger scale. Therefore, the speeches, dialogues and laws on which the just city is founded must be examined by way of discussion.

Since one can understand how a person thinks by conversing with that person, one can understand a city by talking about it with others.

If the city is just, it will give rise to just individuals who can offer an account of their actions and debate what constitutes their justness.

Understanding a just person, then, is also a matter of analyzing the just city via speeches and dialogues, such as those between Socrates and his interlocutors.

> _"Then the just man will not be any different from the just city with respect to the form itself of justice, but will be like it." — Socrates_

### 7. City and soul are divided into three parts, and each part of the city corresponds to each part of the individual’s soul. 

How should the just city look and how should it be organized? Socrates uses the noble lie to demonstrate how the city is partitioned, and how the human soul is also divided into the same parts as the city.

The first part of the soul and city is ruled according to reason.

City rulers have, according to the noble lie, gold souls that belong to the guardians who create the laws and are equipped to rule. As rulers oversee the city, the rational part of the soul, informed by reason and logic, must oversee the other parts of the soul, maintaining order and, therefore, justice. This first part also plans out various tasks and ways of accomplishing them.

The second part of the city is the army, which corresponds to the more hot-blooded, "spirited" part of the soul.

The army is made up of those who have silver souls, and it defends the city during battles and upholds the laws during peacetime. This "spirited" silver part acts as a mediator, whenever there's a conflict, between the soul's rational and desirous parts. It maintains order between reason and emotion, striking a balance between arduous calculation and hasty decisions.

The lowest portion of the city consists of farmers and craft workers, and corresponds with the lowest part of the soul — the bronze part — which is the part governed by desire.

Those with bronze souls are farmers, craftsmen, and those who produce goods. This part is controlled by natural desires and needs, such as sexual appetite, that cry for instant gratification. It also lets us know when we need to eat, sleep or procreate.

Although the rulers, soldiers, farmers and craft workers represent the gold, silver and bronze parts of the soul, respectively, their individual souls are also partitioned into gold, silver and bronze. Therefore, the farmers and craft workers, too, have a spirited and a rational part to their souls, just as the rulers have a desirous part to theirs.

### 8. In the perfectly just city, philosophers must be kings, or kings must be philosophers. 

If you had to choose, whom would you want to be ruled by? Socrates posits that philosophers must be made the rulers of the city. This, he says, is the only way that the city's laws will be just and its oversight rational.

For the philosopher-king, philosophy and authority must go hand in hand. To have a philosopher be king, or king be a philosopher, their souls must be governed by reason and their city must be ruled in a rational way.

The philosopher-king desires wisdom; his soul is balanced and harmonious. This means that he must not be a slave to passion. When one's soul is balanced, one's life is also balanced. Philosopher-kings are healthy in body and mind, and epitomize the values imparted to them over the course of their education.

The philosopher-king's thirst for knowledge will also be reflected in the community, influencing them to determine how the city should be run and its citizens educated. In addition, they should decide on the education of the people — which roles fit each individual best and what the people should learn.

Philosopher-kings should also determine the laws of the city, all of which must be written to mirror justice and the common good. Remember: Just laws aren't created for the benefit of the rulers, but for the benefit of all.

Lastly, only the philosopher-kings can determine the common good. That is, the shared good of individuals and the city. This ensures that the city doesn't thrive at the expense of its citizens and that the citizens don't thrive at the expense of the city.

### 9. Philosophers will encounter much difficulty in ruling and educating others. 

Just because something is rational doesn't mean it's popular. Sometimes it can be quite the opposite. Rational arguments often struggle against our well-ingrained habits and prejudices. For example, it can be near-impossible trying to persuade someone exercise regularly. Likewise, the rational philosophers trying to organize a city will often be met with irrational resistance.

Socrates demonstrates this point with _the myth of the cave_. The attempt of philosophers to educate those around them, he says, is like dragging people out of a cave.

Socrates tells Glaucon to picture a cave. Prisoners are chained to seats, their gaze forced toward the wall. They've lived this way their entire lives. The shadows of the movements of the people passing in front of this cave are cast onto the wall by the sunlight behind them. Because it's all they've ever known, the prisoners in the cave perceive the shadows and voices projected onto the wall as reality, rather than a mere shadow of it.

A philosopher is someone who enters the cave to liberate the prisoners and take them out into the light. Socrates claims that most people are like those in the cave, preferring to treat mere shadows as if they were reality.

So, the philosopher strives to reveal the truth, or essence, behind these shadows, these appearances.

In the cave analogy, the sunlight stands for the good — though one cannot look directly into the sun, it helps us see reality.

Socrates calls attention to the fact that while everyone is born into this cave, it is the philosophers who are able to leave and then return to free the others.

### 10. There are five types of government, aristocracy being the optimal form. 

Most of us in the West will only have ever experienced one form of government: democracy. But what are the other forms of government? And which is best? Socrates now puts forth his own analysis.

Socrates argues that the life of cities is circular, cycling from the best form of government to the worst, and then returning to the best. 

The five governments are ordered thus, from best to worst: aristocracy, timocracy, oligarchy, democracy, and tyranny. Movement between these is unavoidable and is spurred by the ruled revolting against the rulers.

The ideal form of government, says Socrates, is an aristocracy, which means 'rule of the best.' The best ruler is the philosopher-king.

The next best government is a timocracy, which is run according to honor. This system is ruled by those who cannot reason well and are thus unable to run an aristocracy. They win support with rhetoric and impassioned speeches about honor, as opposed to the rational lectures given by philosophers, and when a timocratic ruler overthrows a philosopher-king, the aristocracy is overthrown, too.

Next is an oligarchy, where money rules the city. Those with silver and bronze souls are pitted against one another in a bid to rule the city and control money. In an oligarchy, whoever has more money can buy their way into office.

The fourth best government is a democracy, where mixed freedom rules. This begins when the poorer citizens protest against the inequality of the oligarchy. They rule their cities by offering freedom, including free speech, to everyone. In a democracy, everyone may do as they wish, a state of affairs that Socrates compares to a multicolored cloak with no balance or order between its colors.

The worst government is a tyranny. The permissive freedom of democracy affords the tyrant an opportunity to push forward and begin ruling for their own benefit, instead of for the benefit of all.

### 11. Final summary 

The key message in this book:

**In Plato's dialogue, Socrates addresses the question of what it means to be just and what the best form of government is. He examines which institutions are necessary in order to guide individuals toward being the most just. He argues for a city that is just and benefits its citizens, as well as for citizens who are just and in turn benefit their city. His overall objective is to demonstrate that being just is preferable to being unjust.**

Actionable advice:

**Gain clarity by using the Socratic approach**

If you want to uncover previously unexamined assumptions, question other people's knowledge or simply clarify your own thoughts, take a Socratic approach by asking questions that uncover blind spots in other people's, or your own, reasoning.

**Suggested further reading:** ** _Breakfast with Socrates_** **by Robert Rowland Smith**

_Breakfast with Socrates_ whips you through a normal day with commentary from history's most venerated thinkers, explaining exactly how their major contributions to philosophy, psychology, sociology and theology impact your daily routine: wake up with Descartes, brace yourself for a world of Freudian conflict, and when you go to work, either submit to Marx's wage slavery or embrace Weber's work ethic. Argue with French feminists and then slip into a warm bath, bubbling in Buddha's heightened consciousness. Finally, end the day by drifting away into Jung's collective unconscious.
---

### Plato

Plato, Socrates's most famous student, was a philosopher and mathematician during the Greek classic period (5th — 4th century BCE). He wrote over 30 dialogues and philosophical texts on a wide variety of subjects, including love, knowledge, ethics, politics, metaphysics and theology.

