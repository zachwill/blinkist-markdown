---
id: 552e27f43931640007410000
slug: the-reputation-economy-en
published_date: 2015-04-17T00:00:00.000+00:00
author: Michael Fertik and David C. Thompson
title: The Reputation Economy
subtitle: How To Optimize Your Digital Footprint in a World Where Your Reputation Is Your Most Valuable Asset
main_color: 809E5D
text_color: 4A6131
---

# The Reputation Economy

_How To Optimize Your Digital Footprint in a World Where Your Reputation Is Your Most Valuable Asset_

**Michael Fertik and David C. Thompson**

_The Reputation Economy_ (2015) explains how to optimize your digital footprint and establish a first-rate online reputation. The authors identify emerging trends and describe how you can improve your prospect of both professional and financial success.

---
### 1. What’s in it for me? Learn how everything you do on the internet affects your reputation. 

Shopping online is great; instead of traipsing through shop after shop looking for what you want you can get it with one easy click. Yet there is a downside. When you buy something online, say a pair of shoes, you will be bombarded with shoe adverts for the coming weeks. This is because everything you do online leaves a trace.

And it won't just be your shopping and browsing habits that follow you around. We are entering an age where your entire reputation, including your ability to get a job, will be affected by what you do online. These blinks will help you learn (and prepare) for this coming age.

In these blinks you'll discover

  * why a traditional college education may soon be a thing of the past; and

  * why getting your next job may depend on your ability to please the machine.

### 2. Everything you do online is recorded, stored and analyzed to determine your reputation score. 

Before the advent of the internet, people had a fair amount of control over how they presented themselves to the world. Today, however, such tight control is a thing of the past.

This change is partially due to the dramatic expansion of our data storage capacities. Previously, only major institutions, such as the CIA, the NSA and IBM, had the resources to store massive amounts of data. Then came the internet, and now anyone can store huge amounts of data for close to zilch.

For individuals, that means that their digital footprint, the trail of data they leave behind after surfing the internet (photos uploaded to Facebook, credit card transactions and ATM withdrawals), will be recorded and permanently stored by a motley of companies.

Furthermore, emerging technologies are making it easier and cheaper to analyze, quantify and draw conclusions from this data. And companies increasingly rely on data-analysis tools to discern patterns in their customers' behavior.

Amazon, for instance, uses a system called Hadoop to find, among millions upon millions of purchases, patterns that allow them to make customized product recommendations. And LinkedIn uses a similar system to recommend "People You May Know."

These systems organize the cascades of data via numerical scoring. And at this point, pretty much everything is being processed and scored.

So whatever you do online — each click of your mouse — is represented as a number. And many companies are plugging these numbers into an algorithm that computes a _reputation score_ for individual people.

Pretty soon, reputation scores will be ubiquitous. In other words, whatever you want to do in the future, whether it's getting a job or buying a house, you'll first have to reckon with your reputation score — the sum of all your online habits.

### 3. Your reputation score will be updated instantly and used to predict your behavior in all areas of life. 

Our digital footprint and digital reputation will become more and more relevant as technology improves. What's more, the information we share online in the future will go public almost instantaneously.

For example, a surprise compliment from your boss on social media will instantly increase your reputation as a good employee. Accordingly, a grouchy Facebook comment will instantly decrease your score.

And with so much information about you being made available to others, companies you've had no interactions with may even try to attract you.

Imagine that you're the kind of person who always stops at the same coffee shop, eats the same dish for lunch on Fridays and purchases cars from the same dealership. Your reputation score will reflect your loyalty, and a gaggle of companies will come calling, eager to win your business.

Your online reputation will affect other parts of your life, too. Take your professional life: What you do today for one company will be used to predict your behavior in other areas tomorrow. For example, if you build a good reputation as an advertiser, that information will automatically be used to predict your aptitude for social media marketing. In other words, your reputation is "portable."

As you can see, in the reputation economy, people will use data to make decisions even if their information is limited and secondhand. Imagine you want to hire a babysitter: You've probably never observed this person babysitting, and, of course, your children are unique and will pose unique challenges. You have no real way of knowing whether the sitter will care ably for your children.

However, with access to the sitter's reputation score, you'll have a lot of accurate secondhand information (about previous jobs, about education) that will make the decision easier.

### 4. Inaccurate information can powerfully and permanently damage a digital reputation. 

Imagine someone has a grudge against you and your small business. What happens if that person starts spreading lies about you all across the internet? Unfortunately, you can't program a computer to censor such comments. No matter how advanced technology gets, computers will always lack the ability to distinguish fact from fiction.

And so false or inaccurate information about a company can critically damage its reputation. Imagine a competitor leaving fake negative reviews about your company on Yelp, or paying people to create fake accounts from which to write negative comments. By slowly destroying its reputation, they'd sabotage your company, one fake comment at a time.

Similarly, false or inaccurate information about an individual can crucially damage their professional reputation. Let's say someone either admires or dislikes you to such an obsessive degree that they start impersonating you online. Using your name, that person might do or say certain things you would never condone, irreversibly tarnishing your reputation.

So, what would you do if you or your business experienced an attack of this kind? How would you minimize the damage?

Well, it's often better to respond indirectly, because publicly refuting an allegation may be more harmful than helpful.

Why? Because you might draw more attention to the conflict. Also, you may come across as aggressive or defensive.

An indirect response will allow you to be more careful and rational. For example, if someone posts a blog entry alleging that you were fired — when in truth you resigned voluntarily — then you may be able to prove that allegation false by posting photos of your going-away party or of a warm goodbye letter from your department supervisor.

### 5. Be proactive to change the conversation and secure a good reputation. 

It's easy to feel like giving up when your reputation hits a snag. But don't despair — there's a way to bounce back. The key is to be proactive.

It's impossible to change what information is being collected about you, but you can control what information people pay attention to. There are a couple ways of doing it.

One way to challenge a negative reputation is to disrupt the conversation by doing something shocking. Instead of defending yourself, simply get people to start talking about something else.

Say you're tired of being seen as someone who's lazy and eats poorly. Instead of trying to vindicate your habits, go join a gym. And then bombard your coworkers with photos of your weekly workouts.

Another way to change the conversation is to reframe the debate _before_ it happens. That's exactly what Facebook did when it started building a massive energy-consuming data center in the Oregon desert. Since Oregon had long been the heart of the environmental movement, Facebook's PR team dodged a publicity disaster by spreading information about the project's efficient use of energy.

That gets us to another important point: Both corporations and individuals benefit from focusing on their strengths.

So, if you and another manager are both competing for the same promotion, and he's trying to focus attention on his division's revenue growth, you can bet he chose that exact metric for a reason. The best strategy in this case is to shift the basis of comparison to something at which you excel.

Similarly, when the Yahoo! brand deteriorated in the early 2000s, it changed the subject by hiring a new celebrity CEO and spending billions to acquire the microblogging platform, Tumblr.

### 6. Digital reputation will have a huge impact on your career. 

In the Reputation Economy, computers are increasingly making important decisions for us — for instance, who to hire or fire. This trend is called "_decisions almost made by machine_," or _DAMM_.

As the term suggests, these decisions are made with minimal human oversight. Consider hiring: Many companies now use computers to screen job applicants. After all, recruiters are slow and expensive; computers, on the other hand, can make thousands of decisions each second, and they don't need lunch breaks or paychecks.

Consider that the Mars One Organization received over 200,000 applications from hopefuls who wanted to join a mission to Mars. If a human being has to spend five minutes evaluating each application, they would have to spend nine years working full-time in order to process the applications.

Savvy job applicants will anticipate these automated screening procedures and prepare their materials in a computer-friendly way. If you make it easy for a computer to recognize and categorize your accomplishments, you can increase the chances of a favorable outcome.

Most of these automated systems look for keywords based on the employer's requirements. So if a vacancy notice calls for a CPA it's a good bet that someone programed the screening software to scan applications for the word _accounting_.

And keep in mind: DAMM can also vault you forward in your career.

This was the case for Arnel Pineda, a small-time singer based in the Philippines, who loved uploading covers of Journey's hit songs to Youtube. His performances received thousands of thumbs-up votes, and so he started appearing higher in the results when people searched, say, "Journey cover." And this automated ranking system eventually propelled him to stardom: Pineda ended up become Journey's new front man for the world tour.

When it comes to the power of reputation to shape your career — well, don't stop believing.

### 7. Past performance and reputation will increasingly play a role in hiring decisions. 

Hiring valuable talent is a challenge, since there's no way for companies to know exactly how applicants will perform in a role. After all, the conventional approach to hiring, which relies on gut instinct and resume evaluation, followed by a personality-based interview, simply isn't that effective.

This was proven by a study that asked a group of MBA students to choose between two candidates on the basis of traditional face-to-face interviews. The results were grim: These well-trained future managers selected the higher-performing candidate a discouraging 56 percent of the time.

So in order to close the gap between seeming aptitude and actual performance, hiring managers are increasingly relying on real-world tests that simulate the actual job: Candidates are asked to complete certain challenges designed to weed out low-performing candidates.

In the publishing industry, for example, aspiring editorial assistants are asked to demonstrate their ability by editing a manuscript.

Another method companies use to address this gap is the _acqui-hire_ : A company acquires a successful start-up and hires its founders and staffers. This approach is most commonly used by behemoth tech companies, like Google or Facebook, who are looking to hire top engineers and software designers.

And in the near future, improved computer reputation screening will also provide a way for companies to better evaluate future hires based on their past-performance data.

This approach resembles what football recruiters currently do to hire NFL players: An applicant's resume or work history is broken down and analyzed in the same way that NFL recruiters evaluate college football careers on the basis of yards, tackles, receptions and other statistics.

In other words, using available data, computers will scan a list of applicants to find the ones with standout stats. Accordingly, the top performers will be the most valuable players of the employment world.

### 8. The Reputation Economy will destabilize traditional education. 

Although more and more people are going to college, employers have difficulty finding job applicants with relevant skills and training.

Why the discrepancy? Well, part of the reason students enroll in college is to _signal_ to employers that they're likely to be high performers.

However, mounting evidence shows that the traditional, one-size-fits-all four-year model of on-campus education is a poor fit for many students. Some students learn fast; others take longer to master the same material.

Additionally, college simply isn't an effective way to gauge skills, which is why the Reputation Economy is poised to replace traditional signals with new signals that communicate actual learning more clearly.

This will completely change the way students and job applicants are evaluated. Companies will make decisions based on criteria that actually matter. And the central question will be whether this person will be a good employee in this particular job, at this particular company.

So what would an alternative system look like? Well, consider mathematician Sal Khan, who started uploading math lessons on YouTube in 2006. His audience quickly grew, so Khan quit his job as a hedge fund analyst to start his own company. His mission? To make the prestigious schools he attended obsolete, to replace a Harvard diploma with an education anyone could achieve anywhere.

Today, Khan Academy is one of the largest non-profit online schools, with videos on everything from biology to art to economics. This model differs from traditional education in many respects: There aren't mandatory curriculums or semesters; students can advance as quickly as they like; and instead of hour-long lectures, lessons are delivered in short, fifteen-minute units.

As similar approaches become more common, it won't matter whether students learn online, in the classroom or through an apprenticeship. If the learning is measurable and demonstrates the student's ability to succeed in a particular job, they'll be ready to step into today's Reputation Economy.

### 9. Final summary 

The key message:

**Everything you do online today will be recorded, stored and analyzed to determine your reputation score, which will be updated instantly and used to predict your behavior. This will have huge consequences for your career, your business and every other area of your life.**

**Suggested** **further** **reading:** ** _A World Gone Social_** **by Ted Coiné and Mark Babbit**

Social media isn't a temporary fad — it's changing business culture in a big way. _A World Gone Social_ explains why it's important for companies to evolve their own social media tactics, and includes helpful tips for business owners who want to embrace new technologies and build them into their gameplay.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael Fertik and David C. Thompson

Michael Fertik is the founder and CEO of Reputation.com, a pioneering firm dedicated to online reputation management. He also co-authored the bestselling book _Wild West 2.0_.

David C. Thompson is a lawyer and business executive. Previously, he was the general counsel to and chief privacy officer of Reputation.com.

