---
id: 5e4d352b6cee0700079a16f4
slug: the-rest-is-noise-en
published_date: 2020-02-21T00:00:00.000+00:00
author: Alex Ross
title: The Rest Is Noise
subtitle: Listening to the Twentieth Century
main_color: None
text_color: None
---

# The Rest Is Noise

_Listening to the Twentieth Century_

**Alex Ross**

_The Rest Is Noise_ (2011) takes you on a musical journey through the twentieth century, from the game-changing work of Johann Strauss and Igor Stravinsky to the minimalist compositions of John Cale and Philip Glass. Author Alex Ross puts modern classical music into eye-opening perspective, chronicling the revolutionary changes and how they were influenced by the tumultuous events of the 1900s.

---
### 1. What’s in it for me? Rediscover the history of the twentieth century through modern classical music. 

One question that has remained close to musical scholars and critics, is whether a piece of music can exist purely on its own, separate from the time and context from which it came. Perhaps one of the best examples of this question is the work of Richard Wagner, a German composer who died in 1883. Wagner left behind anti-Semitic writing and his musical works were subsequently hailed as the pinnacle of music by Adolf Hitler.

Wagner was also hailed as a genius by many music critics of his day and proved to be massively popular with the average listener, and so his shadow loomed large over music at the beginning of the twentieth century. As you'll discover, the evolution of music from lavish Wagner operas to the minimalist sounds of Steve Reich is also the story of what was happening around these composers at the time. As politics grew more extremely divided and wars raged, the sound of contemporary music reflects these changes and tells the story of a people reckoning with their past and attempting to chart a way forward.

In these blinks you'll find out

  * how a scandal in Vienna ushered in atonal music;

  * how a Stravinsky ballet transformed the music scene in Paris; and

  * how a postwar German music school ended up influencing popular music.

### 2. Under the long shadow of Wagner, modern classical music took a bold step forward with the work of Johann Strauss. 

At the beginning of the twentieth century, music lovers across Europe were still under the spell of Richard Wagner. By 1900, the composer had been dead for over 15 years, but his operas had been so lavish and grandiose that they'd become the height of entertainment.

In Bayreuth, Germany, when Wagner debuted his four-part opera series known as the _Ring_ cycle in 1876, royalty and the upper crust of culture from around Europe attended, including other composers such as Tchaikovsky and Franz Liszt. The event was such a remarkable occasion that the _New York Times_ ran stories on it for days. Wagner was essentially the blockbuster producer of his day.

The next generation of composers, who were hitting their stride at the start of the new century, had grown up under Wagner's considerable influence. This included giants of Austro-German music such as Gustav Mahler and Richard Strauss.

Strauss can be seen as a leading voice in shaping the music that was to come in the wake of Wagner's grandiose and opulent work. Strauss's 1896 work, _Thus Spake Zarathustra_, essentially announces a new dawn arriving, the music rising and shimmering as it uses the natural harmonic series. Trumpets play C, G, and then a higher C; the intervals between the notes conform to natural ratios on the frequency spectrum and so sound pleasing to the ears. If you've seen the movie _2001: A Space Odyssey_, you'll immediately recognize these "rising" notes; during the opening scene of the film, they're memorably used to herald a new beginning for mankind.

On May 16, 1906, Strauss's new operatic work _Salome_ was performed in Graz, Austria, and it heralded a new direction. Like _Zarathustra_, _Salome_ grabs the listener from the very beginning, but this time the choice of notes and progressions is used to an entirely different end. Within the first few seconds, the listener is subjected to a scale that starts off in C-sharp major, but quickly changes to G-major — a completely unrelated key, causing immediate dissonance. What's more, this quick switch involves the interval of a _tritone_, which is also known among music scholars as _diabolus in musica_, or "the devil in music."

Human ears don't respond well to the tritone, since it juxtaposes two very different harmonic spheres. Because of Strauss's liberal use of such _dissonances_ — musical devices which go against traditional musical harmony — _Salome_ was received with shock that was either of excitement or disgust, depending on the individual.

### 3. Mahler's Sixth Symphony approached the avant-garde differently than Strauss's Salome. 

Many big names, such as Puccini and Mahler, were present at the Graz performance of _Salome_. But the emerging composer Arnold Schoenberg was also in attendance with six of his devoted pupils, including Alban Berg. Schoenberg and Berg later became prominent and highly influential forces in what became known as the Second Viennese School of classical music.

At the start of 1906, Mahler and Strauss were quite close friends. But while Mahler was impressed with _Salome_, his own major work — his Sixth Symphony — premiered in the German town of Essen just eleven days later, on May 27. In it, Mahler attempts to push his boundaries as well. The first movement arrives in the traditional sonata form but contains two competing musical themes that eventually merge. The final fourth movement amplifies the idea of competing themes, with cascading waves of joyous fanfare battling chilling, militaristic marching beats.

This all builds to multiple massive "hammerblows," which at the premier involved a meter and a half drumhead being struck by a gigantic mallet. But the final shock is saved for the end — when the final movement appears to have limped to a finale, an extremely loud A-minor chord arrives like a metal crash.

Indeed, after the last rehearsal, Strauss jokingly told Mahler that the performance had caused the mayor of Essen to die of a heart attack. But what mattered more to Mahler was that Strauss had only one thing to say about the symphony, which was that he found the final movement to be "over-instrumented." After the performances of May 1906, their friendship grew strained.

Mahler and Strauss were often jealous of one another, Strauss envying Mahler's popularity and Mahler envying Strauss's status among other composers as an exciting artist. At the time, there were already ideas that what was popular with the general public was rarely the kind of forward-thinking art that would stand the test of time. In the case of _Salome_, however, Strauss's opera was both a hit with audiences and the sort of work that young composers were excitedly discussing in cafes.

As composer Alban Berg said of _Salome_, "_Never were the avant-garde and the box office so well acquainted._ " But the dissonance in _Salome_ would pale in comparison to what Berg's mentor Arnold Schoenberg had in store.

### 4. A radical, dissonant and influential voice emerged in Vienna with Schoenberg. 

In the early 1900s in Vienna, it was the duty of artists to show that not everything beneath the city's gilded, gold, grandiose facade was beautiful. The values of the bourgeoisie were the target for many artists, but for composer Arnold Schoenberg, a radical, revolutionary style was a more personal necessity.

Schoenberg grew up in the Jewish community of Pressburg, in what is now Bratislava, Slovakia, and was mostly self-taught on the piano. In 1901, he moved to Berlin and then to Vienna the following year. By 1904 he'd amassed a group of disciples that included the composers Anton Webern and Alban Berg.

Schoenberg became the leader of a musical movement that embraced atonality, taking the dissonance of works like _Salome_ and pushing it even further away from traditional tonality and melody. Yet his early work isn't atonal. 1899's _Transfigured Night_ shimmers in D-major, and his super-sized cantata _Gurre-Lieder_ can be safely described as Wagnerian. In these works there are only hints of the dissonance to come.

That move into dissonance and atonality coincided with trouble in Schoenberg's personal life. On November 4, 1908, a painter named Richard Gerstl killed himself after having an affair with Schoenberg's wife Mathilde. Schoenberg had been a deep admirer of Strauss, so by that point he'd been adding some dissonance to work that still resided in tonality, particularly his Second Quartet. It finds the composer at a crossroads, where unpleasant dissonances such as tritones abound and the very idea of chords seems to be disintegrating before us. These musical changes, combined with the upheaval in his personal life, led Schoenberg into a prolific period of early atonal works, culminating in a March 31, 1913 program at the Musikverein together with Webern and Berg. 

Truth be told, his protégés were already going farther out into the sonic wilderness than their mentor. During Berg's song "Über die Grenzen des All," a particularly grinding dissonant chord consisting of twelve different pitches caused the audience to break out into a scandalous mix of laughter, catcalls and a fight between the concert organizer and a member of the audience that ended up in a punch being thrown — and a resulting lawsuit.

One composer in the audience called "the sound of the scuffle" the most harmonious part of the musical program, and the press ran wild with full-page articles about the event. No matter the response, though, Schoenberg had no intention of moving backward.

### 5. Stravinsky rocketed to fame with The Firebird and The Rite of Spring. 

While the press may have been laughing about atonality, a "cult of Schoenberg" was developing among young Austro-German composers. Meanwhile, in Paris, a similar devoted following was forming around a composer of a different variety.

The Russian-born Igor Stravinsky was born to an aristocratic family of Polish and Russian descent. His upbringing included summer trips to Ustyluh, on the Polish-Ukrainian border, and it's likely that Stravinsky was influenced early on by regional folk music — not unlike the kind of rhythmic, dance-oriented music that also influenced composers like Hungarian Béla Bartók, Frenchman Maurice Ravel, and Leoš Janáček, from what would become the Czech Republic. 

In the early twentieth century, portable sound recording equipment and listening devices like the gramophone brought the music of both rural and distant lands to the ears of city-based composers. The sounds and rhythms from vastly different societies around the world were beginning to mix.

Around 1910, Stravinsky became something of an overnight sensation when his composition _The Firebird_, which melded Russian folk traditions with French influences, was performed in Paris. In particular, a part of the piece known as the "Infernal Dance," with its jaunty, rhythmically complex use of bassoons, horns, and tuba, raised many eyebrows and began Stravinsky's reputation as a forward-thinking composer.

From there, Stravinsky's use of complex rhythmic patterns became bolder and his sound freely explored the use of dissonance. In 1913, however, he quickly entered the ranks of the most influential composers with his composition for the ballet _The Rite of Spring_, which was choreographed by Vaslav Nijinsky and staged by the Ballets Russes in Paris.

Before its premiere, there were already rumors that the music, as well as the choreography, were going to push the limits of avant-garde, and they did indeed. By the second movement, the combination of dissonant, clashing notes, jarring rhythms, and ferocious dancing caused certain members of the audience to howl with discontent. A few violent altercations between protestors and Stravinsky's fervent supporters even ensued.

This could be seen as a similar "scandal" to the Schoenberg debacle in Vienna, but the difference was that such rambunctious audiences were not uncommon in Paris. In fact, this behavior was expected to happen at least once or twice a year. It wasn't long before _The Rite of Spring_ was receiving standing ovations, whereas Schoenberg's atonal works continue to challenge all but the most adventurous listeners.

### 6. Europe hungered for a new post-World War I sound, which emerged in Paris, along with a jazz influence. 

It wasn't long after the premier of _The Rite of Spring_ that Europe was thrown into the destructive violence of World War I. French, Russian, English, and Austro-German composers alike were caught up in either patriotism or their awareness of the devastation being caused by the war.

Prior to World War I, there was still a sense among the general public that the battlefield could be a noble place where pride and glory could be achieved. But this changed forever in the twentieth century, as war became a far more impersonal and devastating affair and the body count rose to unprecedented levels.

The war greatly changed the evolution of classical music. As large swathes of Europe were turned into bloody battlefields, it brought forth a reckoning. When the war drew to a close in 1918, both composers and listeners were eager for a new sound that had no ties to the past and was free of any associations that may have been tainted by the war.

In Paris, a group of composers known as Les Six emerged. It included Francis Poulenc, Darius Milhaud, Arthur Honegger, Louis Durey, Germaine Tailleferre, and Georges Auric. They were deeply influenced by the early avant-garde stylings of Eric Satie as well as the new aggressive music of Stravinsky.

A new music was emerging, one that looked at the music of Wagner the same way Nietzsche had, as the "lie of the great style." The new post-war classical music shunned the pomposity and grandiosity that had been embraced by previous generations.

Another type of new music that arrived in Europe during the war was American jazz, and its rhythms and textures quickly began to be incorporated into modern compositions. Likewise, elements of Bach and other classical influences began to make their way into the work of American jazz artists like Duke Ellington.

Amidst the changing musical trends in Paris, there was also a push towards formalism. More than just wanting to express emotions, or even anything similar, formalists were interested in the pure, intellectual act of constructing music. At the time, people like Stravinsky suggested that music and dance should express _nothing,_ and that clear form and structure in music should take precedence over an emotional connection with listeners. This kind of formalism would have a long-lasting influence, as we'll see in the blinks ahead.

### 7. In the US, where classical music has European connotations, many composers played jazz or wrote for Broadway. 

Meanwhile, in the United States, classical music has always been at something of a disadvantage. The idea that classical music is a European thing has long proliferated there, and ever since the country broke free of British rule, the average American listener has had a preference for homegrown music.

In the early twentieth century, ragtime, jazz, and blues music certainly fit this bill, both practically and in spirit. But that doesn't mean there weren't American composers creating strong work, despite the disadvantage of working in a European-dominated profession.

At the turn of the century, Charles Ives, the son of a bandleader, was chief among the American composers. Despite early good reviews for a 1902 cantata called _The Celestial Country_, though, Ives still had a full-time job selling insurance. His output would go on to rival Schoenberg in its dissonance.

Granted, there were also many European-born composers working in America, like Antonín Dvořák, who was born in Czechoslovakia and moved to the US in 1892. He became enraptured with African-American spiritual music, rightfully hailing it as the future of American music.

Some American composers, like Carl Van Vechten, switched to working in a more popular musical style: jazz. In fact, American jazz music was being written and performed by many artists with classical training, including the pianist Fletcher Henderson. Other composers, like George Gershwin and Leonard Bernstein, followed their talents to Broadway, where they blurred the line between opera and musical theater with works like _Show Boat_ and _West Side Story_, respectively.

Both Gershwin and the jazz pianist and bandleader Duke Ellington were among the first to successfully blend jazz and classical music. Even the legendary Russian composer Rachmaninoff bestowed praise upon Gershwin's 1924 composition _Rhapsody in Blue_. And in 1934, Gershwin brought the sophistication of Mozart to the Black American experience in his opera _Porgy and Bess_.

Ellington was skeptical as to whether _Porgy and Bess_ was really "a negro opera," as the press were calling it, since it was written by a man born to a Jewish family in Brooklyn. For his part, Ellington was often asked whether classically-trained jazz musicians were diminishing their talents by playing jazz, and he spent much of his career merging the two forms and proving that jazz could be just as sophisticated. In 1943, at a Carnegie Hall performance, Ellington introduced _Black, Brown and Beige_, a work that could accurately be described as a 45-minute "swing symphony."

### 8. In pre-World War II Berlin, the politics of musical styles reflected the deep divides in society. 

In the years following World War I, the always-influential German music scene found itself in an unusual place. In 1918, in the midst of the German Revolution, Kaiser Wilhelm II stepped down from the throne and fled the country, ushering Germany into something like democracy in an era now known as the Weimar Republic. But really, what followed the 1918 abdication was political turmoil that didn't manage to fully stabilize in the decades ahead.

Starting in 1924, the chancellor and subsequent foreign minister Gustav Stresemann seemed to be the only official who could keep the extremist voices on the left and right from tipping the nation one way or the other. Unfortunately, he died in 1929. The music of the time seems to mirror this hostile political situation.

You can think of it as the "politics of style," and it would only grow more contemptuous as the years went on. On one side, you had composers who wanted to make "music of use," compositions that might speak to the humanity of the average person on the street — something they might embrace and feel invigorated by. On the other side were the composers more interested in formalism and making music that pushed boundaries and evolved the form, even if it meant taking the music further into atonal dissonance.

The "music of use" crowd was perhaps best represented by Kurt Weill, a composer who, along with writer Bertolt Brecht, created _The Threepenny Opera_ in 1928. That work won over audiences in Berlin just as Gershwin's music did audiences in the US. Also like Gershwin — and jazz musicians — Weill gave performers room to improvise and leave their own mark on the music.

Weill didn't want music to be something for the precious few. He wanted it to be simpler, clearer, and more transparent. This wasn't something that the Schoenberg school of German composers agreed with. Instead, they were moving towards the _twelve-tone_ form of composing. Perhaps as a natural evolution of atonal and formalist music, twelve-tone essentially turns composing into a rigorous procedure; in it, a composer uses every one of a given scale's 12 notes, or _tones_, before any one of them can be repeated.

If this sounds like extreme formalism, it is. And it proved remarkably durable, becoming a primary influence on classical music from the 1930s to the end of the century.

### 9. Politics also influenced music in Stalinist Russia and an America uneasy about Communism. 

Many composers were stifled by the restrictions and machinations of the Soviet Russian regime, particularly in the 1930s and 40s, when Stalin's powers were at their height. Interestingly enough, like Hitler, Stalin was a music lover.

Certainly, both Hitler and Stalin were well aware of the great musical legacies of their homelands, and both wanted to use music to advance their cause. For Stalin, this meant producing music that not only proved that the USSR was home to world-class composers, but was also consistent with the values of Soviet Communism.

As Russian composers like Dmitri Shostakovich, Gavriil Popov, and Sergei Prokofiev discovered, the values of Communism turned out to be in direct opposition to formalism. The leaders within the Communist Party made it clear that formalist works would be banned and that Soviet music should be the people's music, clear and accessible rather than overly complex or atonal. Art should not be self-serving or indulgent, which is what Party leaders could suspect it to be if a composer were to experiment with something like twelve-tone composition.

In fact, during a three-day conference in 1943, Stalin's right-hand man Andrei Zhdanov announced that 42 prior musical works were being officially banned, including three symphonies by Shostakovich, Popov's First Symphony, and two sonatas by Prokofiev.

It was a conundrum, because to be considered among the best in their field, contemporary composers had to write music that pushed the form ahead; that's what many critics believed formalist trends were doing. So, it's perhaps all the more impressive that Shostakovich and Prokofiev were able to make interesting and provocative music within the limits established in Stalinist Russia.

Shostakovich was at times a favorite of Stalin's. In the mid-1930s, however, the composer was under terrifying scrutiny following a 1936 performance of his new opera _Lady Macbeth of the Mtsensk District_. Stalin was in attendance and left without comment, clearly unhappy with the composer's use of dissonance and jarring rhythms.

His Fifth Symphony, however, was well received. It premiered in 1937, and managed to be emotionally complex while also winning over the censors. In the music, there is an audible journey from tragedy to triumph.

Then, during World War II, Shostakovich composed his Seventh Symphony. He titled it _Leningrad_, and it premiered in the city during its devastating two-year siege by Nazi Germany. The performance was simultaneously broadcast over loudspeakers, bolstering the Red Army's spirits while demoralizing the invaders. Shostakovich continued to have an uneasy relationship with Stalin, but unlike other composers he managed to sustain a remarkable career under extreme circumstances.

### 10. In FDR’s America, composers debated about what kind of music should be made as an influx of European refugees arrived. 

By the time the US joined the Allied Forces in World War II, there were also politics of style arguments underway. What was music's role in society? Should a composer be solely interested in advancing their craft, or should they be making music "to be of use," like English composer Benjamin Britten once said of his ambitions.

The 1930s in the US were also a tumultuous time of trying to rebound from the Great Depression, while the growing popularity of radio allowed for new means of mass communication. President Franklin Roosevelt issued his massive New Deal project, which initially included a generous budget for supporting the arts.

Part of the Federal Arts Project was music-based and designed to support composers as they created their next major works. The music program initially received $7 million and it was dispersed to around 16,000 musicians and 125 orchestras.

But once federal taxpayer funds go to the arts, where that money goes and who receives it is bound to be scrutinized by politicians looking to score points and make headlines. That's precisely what happened in 1938-39, as Republicans looking to chip away at FDR's authority attacked the federal program as a propaganda machine, leading to its dismantling.

One composer who really felt the effects of political scrutiny was Aaron Copland. Unlike other composers, Copland retained populist impulses throughout his career, though he did get dissonant from time to time. Generally, Copland is known for pastoral works that evoke the great expanse of America. His score for the ballet _Billy the Kid_ begins with a piece entitled "The Open Prairie" and it manages to evoke that kind of landscape perfectly.

Copland appreciated clear communication in music and he also believed it could carry a social message. During the Depression, there were strong communist and socialist movements in America, and though Copland was interested in their messages he was never an official member. But he did attend an event called the Cultural and Scientific Conference for World Peace in 1949, which allowed him the opportunity to meet Dmitri Shostakovich and speak out against the way Cold War paranoia could adversely affect art.

The FBI subsequently opened up a file on Copland, and it was apparent that his work was looked at in a different light from then on — perhaps also because Copland was homosexual, and thus seen as a potential KGB blackmail target.

### 11. Post-war Germany saw the creation of the Darmstadt School, while the avant-garde grew more aggressive. 

The post-World War II music scene not only remained divisive, it grew even more so. Just as had happened after the First World War, there was a reckoning following the Second — certain music took on new connotations in the light of the war's atrocities. The kind of bombastic music that was favored by dictators like Hitler, who loved the work of Wagner, were cast aside for new musical trends that carried no historical baggage.

In Germany and France, a new avant-garde rose up during the Cold War era and it was more strident and aggressive than ever before.

While officials in post-war Germany were deciding what music to play on the radio, in 1946, the Darmstadt International Summer Courses for New Music was created. This became the home base for a highly influential group of composers known as the Darmstadt School. At first, twelve-tone emerged as the exciting form of choice for the young composers, but it wasn't long before the forward-thinking group considered this passé. 

In the 1950s and 60s, new advances in electronic devices meant that electronic music and manipulation were quickly embraced for their exciting new possibilities. One of the foremost voices from the Darmstadt school was Karlheinz Stockhausen, who quickly jumped from one style to the next. _Serial music_, _point music,_ and _statistical music_ were all names that Stockhausen used to describe his formally ambitious compositions.

Likewise, in Paris, a new generation of composers were gravitating toward Pierre Boulez, who was perhaps more aggressive than anyone else in calling formalist, atonal music "the only way forward"; all other music was "useless." In particular, Boulez was a proponent of a new technique called _total serialism_, which took many aspects of twelve-tone serialism and plotted them all like coordinates on a grid, from scales of rhythm and pitch to note length and dynamic levels. This could result in the note E Flat always being played for 30 seconds and always at the same dynamic within one piece.

Many composers rallied around figures like Boulez and Stockhausen and drew bold lines in the sand about which techniques and styles were the right way forward and which weren't. Meanwhile, in the States, a different kind of avant-garde scene was taking place.

### 12. Minimalism sprung from the American West Coast and took hold in New York’s bohemian art scene. 

In the 1930s, many European composers fled to the US and some of them, like Schoenberg, found work teaching at universities. Schoenberg, Stravinsky, and other composers found the warm and sunny climate of California to be an agreeable new place to call home.

Many displaced composers found steady work writing music for Hollywood. And perhaps inspired by the flat desert landscape and the long and open roads, other composers were developing a new kind of avant-garde music that would be labeled as _minimalism._

As a teacher at the University of Southern California in the 1930s and 40s, Schoenberg was a direct influence on many up-and-coming composers, including Lou Harrison and John Cage. One thing Schoenberg liked to say to his students was, _"_ Use only the essentials." Harrison perhaps took this advice in ways Schoenberg didn't necessarily intend, by creating sparse musical landscapes involving droning chords and hypnotic patterns.

Cage bounced between the West and East Coasts before settling in New York, settling in among the city's bohemian artists and befriending other musicians like fellow minimalist composer Morton Feldman. Cage also picked up valuable information from trips abroad, visiting Paris and Darmstadt, where electronics and tape manipulation were becoming popular ingredients for technology-savvy composers.

The bohemian art scene in New York was a very open and collaborative environment, where painters, choreographers, musicians, and writers were floating in and out of each other's apartments and cross-pollinating their ideas. Mark Rothko and Robert Rauschenberg were just a couple of the modern artists who drew inspiration from contemporary classical music and likewise influenced composers.

Rothko's big canvases with bold swathes of muted colors are a perfect visual representation of the droning, lulling sounds of minimalist music. It is perhaps fitting that Feldman would create a major work after the artist's passing, entitled _Rothko Chapel_.

In New York in the 1950s and 60s, there was an uptown-downtown divide. European-style classical music was still an influence on Broadway, but a new more experimental mindset was forming downtown, and Cage was at its center.

As we'll see in the next blink, there were some essential differences that set Cage apart from the European avant-garde.

### 13. John Cage deconstructed classical composition as minimalism offered a way forward. 

By the 1960s, techniques like twelve-tone and total serialism had seemingly taken formalism as far as it could go. Music had become as atonal, polyrhythmic, and discordant as it possibly could. So, in this way, minimalism can be seen as a natural response, a resetting of classical music composition that brought back a sense of space, tonality, and a more repetitive rhythm.

Perhaps the most audacious minimalist performance was Cage's _4'33"_, also known as his "silent" piece, in which the musicians performing it simply sit at their instruments and play no notes for approximately 4 minutes and 33 seconds. The "music" is the sound of the environment the performance is taking place in.

In many ways, Cage was like the Marcel Duchamp of music. Just as Duchamp playfully deconstructed modern art, Cage deconstructed musical composition to reveal its inner machinations. This is what really set Cage apart from the European avant-garde and perhaps the reason that people like Boulez expressed dislike for his work.

Both twelve-tone and the serialism that Boulez promoted had an essential element of randomness to them. Choices the composer made at one point would forcibly determine what happened later on. Cage made that randomness readily apparent. He took rules from the Chinese _I Ching_, an ancient divination text, and would roll dice to see what notes, tempos, tones, and duration of sounds would come next in his compositions. The act of composing music was no longer a mystery that only the elite composer was capable of.

Cage's performances often took the form of "happenings" that were more like performance art, and as the counterculture movement of the 1960s took place, more multimedia events were popping up. On the West Coast, minimalist composers La Monte Young organized the Theatre of Eternal Music, which featured performances by Terry Riley, who often used a combination of tape loops and drones to hypnotic effect, as well as the cellist John Cale, who would go on to become a member of avant-garde rock band The Velvet Underground.

In the 1960s and 70s, composers Philip Glass and Steve Reich were also making monumental works. Glass incorporated elements of the work of Indian musicians like Ravi Shankar into his compositions, while Reich took the use of repetition to transcendent heights. These two composers truly found a way to make contemporary classical music not only adventurous but also popular with listeners.

### 14. Twentieth-century classical music’s influence can still be found in today’s eclectic pop music. 

As the end of the twentieth century approached, musical influences from around the world were melding together and the progressive techniques of ambitious composers were making their way into popular music.

The Who gave a tip of the hat to contemporary classical music in their song "Baba O'Riley," which takes its title from the psychedelic minimalist Terry Riley. And on the cover of The Beatles' _Sgt. Pepper's Lonely Hearts Club Band_ album, there's a photograph of the Darmstadt School's Karlheinz Stockhausen, whose compositions and use of tape manipulations would inform the band's more experimental songs. Before passing away in 2007, Stockhausen himself spent 26 years working on a composition entitled _Licht_, which is comprised of seven song cycles, one for each day of the week; he completed it in 2003.

Advances in electronic music, including programmable synthesizers, remained a focus for many composers in both the US and Europe. But after a certain point, new composers were growing up with rock and roll, jazz, and pop music rather than classical. 

The New York collective Bang on a Can's work is a prime example of modern classical music with a pop sensibility. But perhaps better known is Brian Eno, who started out with the rock group Roxy Music before embarking on a career as a producer of ambient music that owes much to the minimalists who preceded him.

In addition to the Velvet Underground, modern rock bands like Sonic Youth have emerged steeped in the experimental music scene of New York that Cage was such an influential part of. Artists like Björk, Radiohead, Sufjan Stevens, and Joanna Newsom have all created works that are deeply informed by the classical music of the twentieth century.

But while your average three-minute pop song excels at making an immediate impact on the listener, it's rarely capable of exploring what Debussy once called the "imaginary country." To fully explore the complexity of the human experience, a composer needs to have the openness of the classical format — a large, sprawling format, which allows the composer to go wherever their muse may take them, from pure silence to pure noise.

### 15. Final summary 

The key message in these blinks:

**Twentieth-century classical music is a reflection of the times in which it existed. Devastating wars prompted composers to break free of the historical baggage that came with prior works and forge ahead with new, increasingly dissonant and formalist compositions. And just as there were heated political divisions across Europe and the US, there were also musical divisions and politics of style, which grew more aggressive during the height of the avant-garde movement in the second half of the century. But once formalism and atonality had been pushed as far as they could, minimalism and electronic compositions emerged to give composers a new way forward.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Ta-Ra-Ra-Boom-De-Ay_** **, by by Simon Napier-Bell**

If the history of twentieth-century classical music has you eager for a similar look at the business side of popular music, then you may enjoy the blinks to _Ta-Ra-Ra-Boom-De-Ay: The Dodgy Business of Popular Music_. This far-ranging account takes you back centuries, to when writers first gained ownership rights over their songs, before moving to the modern day to explain how rock and roll concert tours became such massive, million-dollar spectacles.

Is popular music really formulaic? Is there a single sound that we keep returning to? These blinks to _Ta-Ra-Ra-Boom-De-Ay_ answer a lot of questions about the kind of music listeners are most attracted to and how a huge, not-always-honest, industry has turned that music into big business.
---

### Alex Ross

Alex Ross has been _The New Yorker_ 's music critic for over 20 years. His writing has earned him multiple awards, as well as a Guggenheim Fellowship and a MacArthur Fellowship. _The Rest Is Noise_ is his first book and was a Pulitzer Prize finalist.

