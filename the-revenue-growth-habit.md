---
id: 58da5b4f232de90004a6e635
slug: the-revenue-growth-habit-en
published_date: 2017-03-30T00:00:00.000+00:00
author: Alex Goldfayn
title: The Revenue Growth Habit
subtitle: The Simple Art of Growing Your Business by 15% in 15 Minutes a Day
main_color: AEC276
text_color: 607447
---

# The Revenue Growth Habit

_The Simple Art of Growing Your Business by 15% in 15 Minutes a Day_

**Alex Goldfayn**

_The Revenue Growth Habit_ (2015) is a collection of strategies for boosting your revenue and increasing your exposure to both potential and existing customers. These methods are quick, easy and cost-effective — perfect for delivering your message to the people who matter most.

---
### 1. What’s in it for me? Market your company effectively without breaking the bank. 

Maybe you've always dreamed of advertising your company in a perfect location — on a building in the middle of Times Square, for instance, or on the sides of New York's midtown busses.

Well, you wouldn't be the first person to harbor such a dream. Nor would you be the first to conclude that, sadly, it's just not going to happen. Unless your company is incredibly successful, such an extensive marketing campaign is simply too expensive and time-consuming.

But don't fret. There are plenty of methods for increasing company exposure that only require a small amount of time and money. Some are even free. So let's get into the blinks, which offer the best of Alex Goldfyn's 22 applicable techniques for revenue growth.

You'll also discover

  * that your current customers are a perfect means to expanding your clientele;

  * why the handwritten note needs a renaissance; and

  * how to effectively compose a newsletter.

### 2. Effective marketing doesn’t require a lot of time, effort or money. 

You've probably had this experience: you're watching TV and an expensive-looking commercial comes on, but, when it's over, you're unsure exactly _what_ was being advertised. After such an experience, you may have wondered whether these expensive and flashy ad campaigns are really worth the money.

Many people are under the impression that impressive revenue growth is only ever the result of expensive marketing. But more money spent doesn't necessarily guarantee more sales.

You'll have a better chance of increasing your sales if you focus in on your target audience. So, instead of advertising to an audience of random people, reach out directly to those who are most likely to use your product or service, and to those clients who have already shown interest.

These days, it's trendy to create mysterious ads that obscure the company and the product being sold. But that doesn't mean creating such an ad is the best way to use your time and money. Despite the trends, marketing is most effective when it clearly communicates how a product or service will benefit a client.

Another current trend is marketing through social media. But that's not the most effective strategy. Getting likes on Facebook and retweets on Twitter can be done quickly, but no quantity of likes and retweets will add up to revenue growth, especially if you're trying to target other businesses.

Email marketing is an easier and more direct method — and you can reap huge dividends with just a little bit of time and effort from your customer-service and sales staff.

You can start using an email marketing platform, such as MailChimp, right this very moment. It allows you to send a single email to every member of your target audience, and customizable software makes it easy to tweak the content to match each demographic you're trying to reach.

A lot of businesses fail to market themselves effectively due to the common misconception that doing so requires a lot of time and money. But it doesn't cost anything to send a clear message about what you have to offer to your potential clients.

Next, we'll look at one of the most powerful tools in marketing: client testimonials.

### 3. Testimonials are an easy way to get unbiased praise for effective marketing. 

Advertisements are a tried and true way of boosting sales. But step into the shoes of a customer for a second, and let's say you're on the market for a new car. Are you going to trust the words of the car manufacturer or those of a close friend? Probably the latter, right?

Many business managers get bogged down explaining how their product works and why it is superior to their competitors' products. But this misses the point, because effective marketing is all about explaining a product's purpose.

So an important growth technique is showcasing how your product or service will improve the customer's life. One of the best ways to do this is to offer testimonials.

Customer testimonials provide numerous benefits. Not only are they free; they put a spotlight on happy clients who have volunteered unbiased opinions about your product.

It doesn't take a lot of effort to collect the best testimonials, either. All it takes is the right combination of confirming and probing questions.

Here's how you ask such questions. After a sale is made, simply follow up with the client and tell him how important his satisfaction and feedback is to you.

Then ask some confirming "how" questions, like, "How much has your business grown after you started using our service?" This should lead to an answer that has the kind of specific statistics that look good in a testimonial.

Similarly, questions like, "How has your company benefitted from our product?" will allow the client to provide positive, expressive and useful quotes.

Probing questions start with a "why" and focus on a topic that is important to your product or service, such as, "Why are delivery times important to you?"

So rather than worrying about how to come up with an effective pitch for your business, let your clients do it for you. You might be surprised by how effective a well-placed testimonial can be.

### 4. Phone calls and handwritten notes can strengthen relationships with your customers. 

We've already established that email marketing is effective, but that doesn't mean you should ignore other means of useful and effective communication.

The next growth technique is harnessing the power of phone calls and handwritten notes.

A phone call is a fast and effective way to show that you care about your customers, especially when it comes from an owner or executive.

One of the primary advantages of email as a marketing tool is how effortless it is; however, this advantage can also be a drawback. Email may come off as impersonal, especially when you use mailing lists. A phone call, on the other hand, is direct and personal and it shows customers how important they are to you.

While a call might take more effort than an email, it doesn't take _that_ much effort. Reassuring a client that she is more than just a statistic shouldn't take you more than five minutes.

Imagine you're the manager of a hotel. Think how surprised and pleased a customer would be to receive a brief, five-minute call from you. It would dramatically strengthen your relationship with the client and help guarantee repeat business.

Another effective but forgotten method of communication is the handwritten note, which is a great way to strengthen client relationships.

Much like a personal phone call, a handwritten letter shows your clients that you personally care about their satisfaction and continued business. It's a simple gesture that is nonetheless greatly appreciated by customers, one that will strengthen their commitment to your business.

So to make sure you make the best impression possible, invest in a quality pen and paper. You might think that buying a nice fountain pen and paper with your own watermark is unnecessary, but it's a small cost compared to the revenue that a strong and reliable relationship can generate.

Just think, with handwritten notes being so rare these days, your letter will likely be the only one your client receives that year, making it all the more impressive.

In fact, it could be a big opportunity to distinguish yourself in an increasingly competitive crowd.

### 5. Newsletters can increase revenue by raising awareness about your company’s offerings. 

Successful marketing doesn't require a complex strategy from an overpriced team of experts. Remember, the essential point is to raise awareness of your company and the products or services you're providing.

This brings us to the next growth technique: making sure your customers are aware of everything you have to offer and all the ways you can improve their lives.

Even long-term customers who are happy with their business relationship are usually unaware of the full range of services being offered. In fact, most satisfied customers are only aware of a quarter of a company's products, even though they could also probably benefit from the other three quarters.

This means you could potentially quadruple your revenue, and all you have to do is inform people about your full range of products and services!

It also means a huge boost in sales if you're able to reach even a fraction of that untapped business.

So how do you inform your clients and help ensure that you're the first business they turn to whenever they need the products or services you provide? One of the best marketing tools for this purpose is a newsletter.

By sending out a regular newsletter containing an article, a testimonial and a promotion, you'll always be at the forefront of customers' minds.

The articles should be relevant, commenting on what's happening in the world and on trends that might affect a client's business. This way you're providing valuable and free knowledge — and by adding a client testimonial to the newsletter, you're linking this value to an unbiased endorsement of your company.

Finally, while you have their attention, finish the newsletter off with a promotion for one of your products or services — and then watch as the new sales come in.

### 6. Enhance your exposure and highlight the value of your products with webinars and events. 

As you probably know, YouTube and online videos are about more than just cute cats. They're also a fantastic way to reach a wide audience and show them exactly how valuable your product is.

This brings us to the next great growth technique: webinars.

A webinar is essentially an online presentation for your clients where you demonstrate all the special features of your product. It's an opportunity for you to show your customers how to use your product properly, ensuring that they get the best experience possible.

That's great in and of itself, but once you've recorded and shared the video, it can then be viewed or downloaded millions of times, without any additional cost to you. Creating an effective webinar doesn't require a lot of fancy and expensive gear. Even your phone or computer can record a great presentation, which you can then upload to YouTube. All it takes is the right placement and lighting.

Another effective growth technique is throwing events and corporate functions. This is the perfect way to demonstrate how valuable your services are.

By inviting clients with whom you already have strong relationships, along with some new and potential customers, you can create an event that may result in a significant increase in your company's revenue.

The author hosts his own annual event to which he invites his most important clients and promising prospects — and what's great is that he doesn't have to do any of the selling. By letting them mix and mingle, the satisfied customers do the selling for him.

Other companies will even hire a $20,000 private plane to fly their most important clients to their production facilities. These clients then take a tour and get treated to a fancy dinner. This might sound extravagant, but it's an experience the clients will never forget, and they're likely to generate future revenue that far exceeds the one-time cost of arrangement — thus making it a sound investment.

### 7. Don’t delay! Act now! 

As the owner of a company, one of the most important questions you can ask yourself is what you're going to do today to reach more customers.

Every day, you should spend time thinking up actions, no matter how small, that will help you bring in new revenue.

It's easy to fall into a semi-unconscious routine, or to spend the majority of your day fixing problems rather than being proactive and growing your business. But remember: there's no better time than the present to catalyze exciting new change.

And creating change doesn't require earthshaking actions. As you've learned, there are a number of small steps you can start taking today that will bring big results.

Taking these steps, however, requires a bit of planning. So lay out the exact steps you want to take. This will make it easier for you to hold yourself accountable and follow through.

If you want to start using an email-marketing service along with a newsletter, tell your sales team about it right away. And write down "how" and "why" questions you want them to ask your past clients so that you'll have testimonials to use in subsequent newsletters and emails.

It doesn't have to be perfect at first, but it should happen soon.

If you wait until something is perfect before you take action, it might never happen at all. You could easily spend half a year planning and tinkering with the format of your newsletter or webinar. But meanwhile you'll be missing out on all the potential revenue you could have been bringing in.

Even if your strategy is only 85 percent of what you were hoping for, you're the only one who will ever know that it's missing that other 15 percent. Your clients, on the other hand, will be seeing 100 percent of your effort, which is better than nothing at all.

So determine your strategy and get your message out there as soon as you can. There's a wealth of revenue just waiting to be unlocked.

### 8. Final summary 

The key message in this book:

**You don't have to sink a lot of time and money into creating a highly effective marketing strategy. Rather, you can boost the revenue of your company by simply raising awareness about what you do. Try out the proven growth techniques one at a time, and find out which ones work for you.**

Actionable advice

**Follow up with customers!**

On average, 80 percent of potential customers are never heard from again. So if someone does contact you, follow up! Start off with two emails and then a phone call, one per week for three weeks or until you get a reply. A 10-percent — or even 2-percent — increase in revenue for writing a couple of emails and making a phone call is a good return on the time you've invested.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Scaling Up_** **by Verne Harnish**

You had the idea, you drafted the business plan, you raised the cash, you launched your new venture and you became a success. But now you need to grow. _Scaling Up_ (2014) reveals the most useful tools for doing just that. Use the Scaling Up system of checklists, levers and priorities to establish a strong company culture as your business expands through the right strategic and financial decisions.
---

### Alex Goldfayn

Alex Goldfayn is a marketing consultant, business coach and public speaker. By using his strategies, Fortune 500 companies such as Amazon, Logitech and Virgin Mobile have increased their revenue by an average of 15 percent.

© Alex Goldfayn: The Revenue Growth Habit copyright 2015, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

