---
id: 57c4272e8a12b60003822258
slug: the-rift-en
published_date: 2016-09-02T00:00:00.000+00:00
author: Alex Perry
title: The Rift
subtitle: A New Africa Breaks Free
main_color: F6D846
text_color: 82701A
---

# The Rift

_A New Africa Breaks Free_

**Alex Perry**

_The Rift_ (2015) is a revealing look at Africa's emergence as a continent no longer defined by poverty, war, corruption and dependence on the West. Find out how modern farming methods, solar and mobile technologies and new leadership are creating a brighter future for Africa.

---
### 1. What’s in it for me? A powerful and controversial look at how the West gets Africa wrong. 

From a Eurocentric perspective, Africa often gets a bad rap. Western media, when it mentions Africa at all, brings reports of war and famine, of political corruption and distress. But is this all the massive continent has to offer?

Well, the simple answer is no. Africa is not a continent that can or needs to be saved by the West. Instead, Africa, with its wealth of resources and innovative approaches, could actually be the solution to the problems faced by the rest of the world.

In these blinks, you'll learn

  * how Africa has enough arable land to feed the whole world;

  * why mobile technology is making a radical change to banking in Africa; and

  * how renewable energy is providing the power Africa needs to prosper.

### 2. The West’s own political interests get in the way when it comes to helping Africa. 

Anyone who pays close attention to international news and history knows that Africa has seen more than its fair share of war and famine. But even if you're aware of the turmoil in Africa, you might have missed the news reports in July, 2011, when southern Somalia suffered one of the worst famines in its history.

That month, nearly three million starving refugees poured into Mogadishu, Somalia's capital, hoping to receive emergency aid from the UN. Over the course of the year, 300,000 of them died; the majority of the casualties were children and elderly people.

Many didn't even survive the journey: Khalima Adan, a 38-year-old woman, lost three of her nine children during the long trip to Mogadishu from the countryside. When the author met Khalima, her 7-year-old son Umar had just died in her arms, and she didn't even have the strength to cry or grieve.

Given that efficient foreign aid could have saved thousands of lives, you might be wondering how we can allow tragedies like this to occur.

Sadly, the West, occupied with its own political interests and the war on terror, often fails to help Africa.

In fact, according to Tony Burns, an Australian aid worker on site in Mogadishu, the United States deliberately blocked all aid to southern Somalia during the famine, despite the pleas coming from humanitarian agencies.

The United States denied aid in large part because of a group known as Al-Shabab, which is regarded as an enemy in the war on terror. Al-Shabab was active in the area and they had been known to intercept, tax and steal some of the incoming aid money.

However, by being more concerned with Al-Shabab and the war on terror, the United States ushered thousands of Somalis to their graves.

> _"US policy is no food or resources to southern Somalia. The famine is proof of their success."_ \- Tony Burns

### 3. Even influential, wealthy humanitarians in the West cannot really make a difference to Africa. 

You probably only know George Clooney as a handsome Hollywood star. There's a bit more to the guy than that, however.

Indeed, George Clooney is actually one of the most influential Western humanitarians trying to help the people in Sudan.

Clooney was highly involved in the peace agreement that led to South Sudan's independence. He was a prominent and vocal advocate for its freedom, appearing on television, as well as speaking directly to President Obama and members of Congress.

But Clooney wasn't content with simply campaigning for Sudan on his home turf. In the time leading up to the peace agreement, he traveled to Juba every year, working on site to raise awareness about the massacres against the southern rebels being committed by the Sudanese government.

He even spent his own money to rent a satellite that could track the movements of the Sudanese government troops, which made it impossible for them to deny the crimes they had committed.

And Clooney was there in the newly appointed capital city of Juba in January, 2011, when 98.8 percent of the people voted to become independent from the North.

Despite this work on their behalf, however, Clooney couldn't bring lasting peace to Sudan.

Even after South Sudan gained its independence, it still contained two rival ethnic groups, the governing Dinka and the rebel Nuer.

In December, 2013, the disagreements between these two groups escalated into all-out massacre: after two weeks of conflicts, Dinka officials rounded up and executed 240 Nuer men.

The violence continued until April, 2016, when the government and the rebel groups managed to come to a fragile peace agreement.

All this goes to show how the well-intentioned efforts of the West often fail to bring lasting change, and that, if peace can be reached, it will have to come from within Africa.

Thankfully, as we'll see in the next blink, this process is already under way.

> _"How come you could Google Earth my house and you can't Google Earth where war crimes are being committed?"_ \- George Clooney

### 4. Africa’s budding agriculture industry has the potential to eradicate poverty and feed the world. 

In the previous blinks, we've seen how effective solutions to Africa's troubles can be hard to come by. But there is one area with great potential: farming. Indeed, it may be the key not only to ending poverty in Africa but to world hunger.

After all, when we look back at history, farming is the best way for a country to develop wealth.

For example, the United States-funded International Food Policy Research Institute has shown that when the income of farmers increases by just one percent, extreme poverty diminishes by a factor of at least 0.6 percent to as much as 1.8 percent.

China is a perfect example of economic development through farming.

From 1978 to 2011, as agricultural incomes increased by 7 percent each year, Chinese poverty levels decreased from 31 percent to just 2 percent.

And Africa, with its 1.46 billion acres of farmable land, is in a good position to take advantage of this kind of growth. With more arable land than the rest of the world combined, Africa has the potential to end world hunger.

With this in mind, Africa is moving from simple-survival agriculture to mass-production agriculture.

We can see this happening in Ethiopia. Until recently, Ethiopian farmers only purchased basic necessities and produced enough food to feed themselves. Increasing production was therefore too risky; spending more and having one bad crop would mean bankruptcy.

But this all changed in 2007, when Eleni Gabre-Madhin, a leading Ethiopian economist, created the Ethiopian Commodities Exchange, a central trade organization for farmers that's revolutionizing farming practices in the country.

Inspectors were hired to check produce quality and provide security to buyers and sellers, and the organization also established fair crop prices and sent the information out to farmers via a mobile phone service so they could predict future incomes.

This helped banks feel more secure providing farmers loans to invest and grow their farms. As a result, between 2006 and 2013, overall agricultural production has increased in Ethiopia by 7.8 percent.

### 5. A new urban Africa is emerging as slums are being transformed into planned cities. 

If you've looked at photos taken in urban African communities, you might have seen images of children rummaging through enormous piles of garbage, looking for something to eat or sell. These images can give you the impression that the situation in Africa is beyond hope.

Although Africa is home to some of the poorest, most run-down cities in the world, change _is_ taking place.

In 2009, the Nigerian city of Lagos was crumbling. 65 percent of the city's 20 million inhabitants lived in extreme poverty, surviving on less that $2 a day. The people of Lagos had no running water, electricity or sewage system — a tragic embodiment of modern Africa's failures.

But, in just a few years, even the dire slums of Lagos were transformed into a functional city.

When Babatunde Fashola was elected governor of Lagos, in 2009, he was determined to transform the city into a beacon of hope.

And sure enough, within a few years, two thirds of the city had access to clean water, compared to just one third in 2009. During that time, a large portion of the city was also protected against flooding; green public parks began to appear where garbage dumps once stood; and streets were rebuilt and equipped with lights.

The efforts to rebuild Lagos didn't just create a new city. Forty-two thousand new government jobs were also created, providing an important economic boost to the citizens.

But the new governor didn't stop there. Fashola also wanted to transform the slums on the outskirts of the city, so he hired Peruvian poverty specialist Fernando De Soto.

Together they worked to give residents property rights, which quickly restored order to the chaotic slums. Burglaries decreased by a staggering 90 percent, and incidents of murder and assault dropped by 50 percent. Additionally, these new property owners also helped strengthen the overall economy by becoming taxpayers.

### 6. Not all African leaders are uneducated criminals; some are groundbreaking innovators. 

With news, media and films focusing primarily on the negative aspects of Africa, it's easy to picture African leaders as a corrupt and vicious bunch of despots. This, however, is far from a complete picture.

Some African leaders have more in common with the Queen of England than with criminal thugs.

When the author met North Nigerian leader Lamido Sanusi, Emir to the city of Kano, he was greeted by a graceful nobleman fluent in both English and French.

A student of economics, philosophy and law, Sanusi's fierce integrity ended up costing him his job at the Central Bank of Nigeria, where he had been fighting corruption, much to the displeasure of his bosses.

At one point, he denounced the practices of Nigeria's petroleum minister, who leased planes from her own private company, thereby paying herself each time she took a business trip.

But being fired from the Central Bank didn't diminish Lamido Sanusi's integrity, and, as a leader of the city of Kano, he plans to continue his fight against corruption and bring prosperity to Nigeria.

Sanusi is using technology to his advantage and promoting a biometric system for all money transactions in Nigeria. With this system, Nigerians could use their fingerprints to make cash withdrawals and pay for goods in shops. This method is one of the most secure forms of money transaction imaginable and has the potential to make fraud, theft and forgery a thing of the past.

And with fingerprint technology making all monetary transactions immediately traceable to the registered individual, Sanusi hopes this will also put an end to the corruption that still weakens Nigeria.

> _"If you think of loss as the loss of freedom or loss of life, you miss the point. If you die for a just cause, you are free."_ \- Lamido Sanusi

### 7. Cell phones are connecting Africans to the world and making a new form of mobile banking possible. 

When the first radios appeared in remote areas of the world, people previously cut off from civilization suddenly had access to the music and voices coming from nearby urban populations. But this is a minor development compared to the impact mobile phones are having in Africa.

Cell-phone technology is providing Africans with a direct connection to the entire world.

In 2015, one billion cell phones were operating throughout Africa. This is incredible when you consider that, just 15 years ago, only a few million Africans had a landline telephone connection.

Another astounding discovery is the link between cell phone penetration and national economies. Cell phones allow people in for example remote African locations to work together efficiently, despite being separated by miles of harsh terrain. This saves a great deal of time and promotes economic growth. In fact, a series of studies carried out in various African countries by the London Business School found that a ten percent increase in the number of cell phones resulted in a corresponding rise in national income of 0.6 to 1.2 percent.

Cell phones have also resulted in Africa's adopting brand new mobile banking systems.

The most popular form of mobile communication in Africa is texting, since it costs very little. Sensing an opportunity, Kenya's telecommunications giant, Safaricom, introduced a text-based system allowing people to transfer money.

It works like this: Kenyans give cash to a Safaricom employee who then transfers the money to a specified phone number. The money can then be passed on by sending a PIN-secured text to anyone who also owns a cell phone. This includes local grocers, hairdressers or even someone on the street asking for spare change.

The system took off in 2013. People weren't using it just to do their shopping; by accumulating this virtual money, people were turning their cell-phone accounts into savings accounts. This made sense for a lot of poor people who were being denied an account by more traditional banks.

In 2015, these methods became so popular that 50 mobile-banking firms sprang up across the African continent, making Western nations start to wonder whether they should follow suit.

> _"Africa is the new Silicon Valley of banking."_ \- Carol Realini, CEO of Obopay in California

### 8. The lack of electricity has long acted as a poverty trap in Africa, but solar panels are solving the problem. 

It's easy to take the benefit of a working light bulb for granted. But for many rural and poverty-stricken Africans, living without electricity is an everyday problem, and only recently are people starting to see the light at the end of this long, dark tunnel.

In fact, a lack of electricity can act as a poverty trap, preventing the poor from escaping their situation. 

Gladys Nange is a 39-year-old mother and farmer who lives in a village called Kokete. This village is situated in a remote part of Kenya near the Ugandan border, where the government has never managed to bring power cables and electricity.

Therefore, Gladys's children are unable to finish their homework in the evening and end up with bad grades at school. What's more, Gladys is forced to walk five kilometers to the nearest electrical plug in order to charge her phone and check the day's crop prices.

Francis Morogo, the village chief, admits that no one from Kokete has ever made it to a university, and most people remain in the village their whole lives.

Fortunately, solar panels are becoming a mobile and ecological solution to Africa's power shortage.

In fact, Gladys Nange herself took part in a pilot solar energy project. She now has a small panel on her house that produces enough power to feed two lamps and a mobile charger.

This mobile solar kit, the creation of a Cambridge University project, is affordable, with a payment plan of only $1.20 per month, and it could change the lives of many.

Gladys's children, for instance, can now finish their homework under lamplight and Gladys can save time and earn more money, with plans to install an electrified hatchery for the chickens on her farm.

### 9. Final summary 

The key message in this book:

**The African continent cannot be reduced to unending conflicts, war crimes, tyrannical despots and extreme poverty. Africa is a growing and innovative continent that is developing strategies and technologies to deal with challenges in ways that can only bring inspiration to the West.**

Actionable advice:

**Invest your money wisely if you want to help Africa.**

Aid projects that bring food and water to the starving in times of crisis are laudable, but they will not bring lasting change to the continent as they only make Africa dependent on the West. So only invest in projects that help Africans become autonomous, such as projects to bring solar power to African farmers.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _China's Second Continent_** **by Howard French**

_China's Second Continent_ (2014) is about the mass wave of Chinese migrants who have relocated to Africa in the last few decades. These blinks trace the origins of this migration and outline the profound impact it has on both regions, Chinese-African relations and the world at large.
---

### Alex Perry

Alex Perry, a reporter for _Time_ and _Newsweek_, has traveled extensively in Africa and Asia for the past 15 years. He shares his specialized knowledge about lesser-known parts of the world in his books, which include _Falling Off the Edge_ and _Lifeblood_.

