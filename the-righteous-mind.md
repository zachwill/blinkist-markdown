---
id: 52cc18f76432660011170000
slug: the-righteous-mind-en
published_date: 2014-01-08T19:25:10.000+00:00
author: Jonathan Haidt
title: The Righteous Mind
subtitle: Why Good People are Divided by Politics and Religion
main_color: A6825A
text_color: 694926
---

# The Righteous Mind

_Why Good People are Divided by Politics and Religion_

**Jonathan Haidt**

_The Righteous Mind (2012)_ explores how moral decisions are made, concluding that moral judgments stem from intuitions, not logic. The author draws on his background in social psychology and 25 years of groundbreaking research to explain how morality both binds us and divides us and how religion and politics create conflicting communities of shared morality.

---
### 1. Contrary to popular belief, it is emotion, not reasoning, that drives our moral judgment. 

Western philosophy has emphasized reason and logic over emotions for thousands of years. This tendency still prevails, but a growing body of research proves that emotions should no longer be regarded as secondary to logic.

For example, American neuroscientist Antonio Damasio has demonstrated that emotions play a more important role in processing information and making moral judgments than had previously been thought.

He studied patients with brain damage which had resulted in their emotionality dropping to nearly zero, and found that, at any given moment in their everyday life, every option the patients had at their disposal felt equally right to them.

Imagine that every choice you made in life meant as little to you as choosing a new kettle. This was life for Damasio's patients: they could think about anything at all with absolutely no emotional input, leading to a general indifference in their behavior. Their social lives were severely impaired as they became less thoughtful partners, friends and colleagues.

And not only do emotions have a valid role in processing information, they are also constantly at work. Research has demonstrated that everything triggers emotions in us, and these emotions automatically influence our moral reasoning.

This was demonstrated in an experiment by social psychologist Robert Zajonc, who asked people to view a series of slides of pictures, words and figures. Every single slide immediately triggered a tiny flash of affect in them, even when the images were arbitrary things like geometric shapes and squiggles. This indicates that emotions work instantly and constantly, ready to guide thought, behavior and moral judgment.

So there is no need to distrust your feelings — they always play an important role when you are processing information or making moral decisions.

**Contrary to popular belief, it is emotion, not reasoning, that drives our moral judgment.**

### 2. When making moral decisions, intuitions come first and reasoning second. 

Is it wrong to eat human flesh or copulate with a dead chicken? Most people would instantly and intuitively feel that both actions were somehow wrong.

These kinds of intuitions or "gut feelings" always drive moral decision making. They can be seen especially clearly in infants who have not yet developed a capacity for reason. In one experiment, 6–10-month old infants were shown a puppet show with two puppets: a nice one and a mean one. After watching the show, most infants preferred the "good" puppet. This demonstrates that, to some extent, morality is prompted by intuitions even when the subject is unable to reason.

Furthermore, research shows that these primitive, intuitive moral judgments are also far more potent to adults than rational reasoning. Once that initial moral judgment has been made, we use reasoning to back up, not reject, the judgment. This tendency has been demonstrated in many studies where people were asked to give a moral judgment on a situation and were then questioned about that opinion.

Take, for example, a study where participants were asked whether it is right or wrong for two siblings to have sex. Most people's initial response was that it is wrong. But then participants were given several rational arguments in its favor: a prophylactic was used, the act was done in secret and the siblings very much enjoyed the experience, so no one was harmed. And yet, despite these rational arguments, most participants clung to their initial verdict. That initial intuition proved stronger than rational reasoning.

**When making moral decisions, intuitions come first and reasoning second.**

### 3. People’s self-interest and concern for their own reputation greatly influences their moral reasoning. 

Most people think that moral reasoning is a detached, sterile process, unaffected by self-interest or concerns about what other people might think. However, both self-interest and self-consciousness affect moral reasoning.

Firstly, we are all preoccupied with what others think of us. Research proves that even people who swear that they are completely unaffected by the opinions of others still experience a decline in self-esteem when they're given a low rating by a stranger. This shows that we are concerned about our reputation without even realizing it.

A consequence of this is that when people think they will need to justify their moral decisions to others, they tend to be far more thorough in their moral reasoning than normally.

These phenomena demonstrate how our moral reasoning can be greatly affected by others, even if they are not present.

Secondly, our moral reasoning is affected by selfishness: people tweak their understanding of right and wrong when they have personal interests at stake.

This was shown by a study in which subjects performed a given "job" and were then given a slip of paper and a verbal confirmation of how much they would be paid for the "job." They took the slip to another room to get their money, and when the cashier deliberately misread a digit and handed over too much, only 20 percent of the participants corrected the mistake. But interestingly, if the cashier explicitly asked them whether the payment was correct, 60 percent acknowledged the error and returned the extra money. This indicates that the majority of people will cheat when given the opportunity, but only as long as they can still feel they did nothing wrong.

So, far from being an objective compass, our moral reasoning is easily swayed by our interest in maintaining a good reputation, and our pursuit of self-interest.

**People's self-interest and concern for their own reputation greatly influences their moral reasoning.**

### 4. Even though moral interests vary between cultures, they are based on the same moral foundations. 

If anyone has ever taken advantage of you, you probably experienced a sense of contempt for them and a desire to see them punished. This tendency is the same in all cultures. But where does this moral instinct come from?

According to the _Moral Foundations Theory_, it's the product of a universal moral foundation of _fairness_. Universal moral foundations are the source of all our moral instincts, and they are shared across all cultures.

These moral foundations can be understood as evolutionary adaptations to long-standing challenges in social life. For example, since cheating has always caused social discord, humans have developed a shared moral foundation of _fairness_.

This is how _fairness_ becomes the basis for human behaviors like collaboration and the reciprocation of favors. We cooperate with those who have been nice to us, and we shun those who take advantage of us.

But even though all humans have shared moral foundations, they can be manifested in different ways and to varying degrees in different cultures.

For example, in some Asian cultures, the father is typically considered the head of the family, and it is not unusual for an Indian woman to retreat to the kitchen after serving dinner for her husband and his male guests. The Asian interpretation of _authority_ in the home differs drastically from most Western cultures, where man and wife are considered equal. This indicates that while _authority_ is a shared moral foundation, it manifests itself in different ways depending on the culture.

**Even though moral interests vary between cultures, they are based on the same moral foundations.**

### 5. People from Western, educated, industrialized, rich and democratic (WEIRD) cultures are outliers when it comes to morality. 

Nearly all research in psychology is conducted on a very small subset of the human population: people from cultures that are WEIRD (Western, Educated, Industrialized, Rich and Democratic). But this subset is actually the least representative you could choose if you wanted to make generalizations about human psychology.

For example, studies show that while the moral values of WEIRD people are overtly individualistic, the moral values of non-WEIRD people emphasize things like _the community_ and _the sacred_. This means that non-WEIRD people are more inclined to put their families, nations and religions before their individual needs. WEIRD people, on the other hand, are almost exclusively concerned with protecting individuals and their rights.

Take this example: when they were asked whether a man could be morally justified in secretly having sex with a dead chicken before cooking it for dinner, WEIRD people were happy to argue that "if it's done in private, it's his right."

However, when people from Asia and people from American working-class groups were asked the same question, the answer was emphatically no. These non-WEIRD people would base their moral decision on their sense of the sacred: even if it does no harm, sex with a chicken carcass still "degrades the person and dishonors his creator."

This indicates that people from WEIRD cultures are outliers, compared to individuals in cultures whose sense of morality also includes a sense of community and the sacred. WEIRD people's morality is focused on the freedom and rights of individuals.

**People from Western, educated, industrialized, rich and democratic (WEIRD) cultures are outliers when it comes to morality.**

### 6. Evolution has made us simultaneously selfish and altruistic. 

Many of us would readily agree that most of the time people act selfishly — we want more money, higher status and more happiness than those around us. And yet, at the same time, people are capable of astonishing altruism and selflessness. For example, after the September 11, 2001 attacks, hundreds of Americans drove great distances to New York in the hope that they could help to dig survivors out of the wreckage. Why are we such a strange mix of selfishness and selflessness?

Firstly, people's selfishness can be explained by natural selection at an individual level. Throughout history, individuals have been pitted against each other in a competition that selects and rewards selfishness. In this way, our minds have been shaped by relentless individual competition.

Conversely, evolution has also made people able and willing to cooperate in groups. When groups compete with one another, the side with the most effective team players will gain the upper hand because they work together for the good of the entire group. This is the direct result of evolutionary adaptation at a group level. When early humans began to share their intentions with one another, their ability to hunt, gather, raise children and raid their neighbors increased exponentially, allowing them to thrive better than less cohesive tribes. In this way, shared intentionality is a group adaptation allowing for cooperation, labor division and accountability.

These two levels of evolution explain why humans can exist simultaneously as selfish and altruistic beings.

**Evolution has made us simultaneously selfish and altruistic.**

### 7. Humans’ sense of community and coherence in a group can be heightened by a "hive switch." 

If you have ever lost yourself rooting with the audience at a college football game or dancing at a rave, you have experienced the human ability to become like bees in a hive, a strong sense of belonging to a group and striving for the common good.

This sensation of being part of something larger than yourself is no delusion. We all have a "hive switch" that is hardwired into our brains, thanks to the evolutionary advantage it provides. The passion and ecstasy that group rituals can generate bring collective interests to the fore and bind the group together.

From an evolutionary perspective, the hive switch is a tremendously advantageous adaptation because it makes groups more cohesive and, therefore, more successful. Darwin long ago made the point that the most cohesive and cooperative groups will generally beat groups of selfish individualists.

Surprisingly, there are ways to trigger our hive switch even when there's no one else present. On an everyday basis, the vastness of nature can prompt feelings of awe that give us a selfless sense of connection to the universe.

Additionally, a psychedelic drug called psilocybin can trigger the hive switch. In an experiment, students who took psilocybin described feeling unity, a transcendence of time and space, and a deeply experienced and positive sense of connectedness.

By flipping the hive switch, people gain the ability to cooperate and to regulate their own self-interest when they join a group.

**Humans' sense of community and coherence in a group can be heightened by a "hive switch."**

### 8. Religion can produce cohesive, altruistic and moral communities. 

It is no secret that many scientists feel a strong disdain for religion. But that's because they only focus on one of its functions: the way it _blinds_ its followers by impairing their ability to think rationally. Some scientists claim that religious beliefs are delusions that prevent people from embracing science, secularism and modernity.

These scientists are partially right: religious communities can commit atrocious acts if their moral systems glorify their own "flock" while at the same time demonizing another group.

But what the skeptics seem to forget is the advantage of religion's _binding_ function. Religions have been binding our ancestors into groups for tens of thousands of years, helping them create communities with a shared morality. What is more, religious practices and beliefs have spread only to the extent that they make groups more unified and cooperative. It is therefore important to also take into consideration the cohesiveness and trust that religion can produce.

Look for example at the 200 communes founded in the United States in the nineteenth century. Some were organized on religious principles and others were secular. Can you guess which kind survived longer? Twenty years after their foundation, only 6 percent of the secular communes were still functioning, compared to 39 percent of the religious communes.

The secret ingredient: ritual practices that produced cohesiveness and trust within the group.

Furthermore, religious beliefs and practices help communities increase altruism. Statistics show that religious people give more money to charity and do more voluntary work than secular people. They don't donate only to religious charities: religious people also give more to secular charities, such as the American Cancer Society.

By emphasizing trust and group activities, religion increases the overall altruism of its groups.

**Religion can produce cohesive, altruistic and moral communities.**

### 9. When wooing voters, conservatives can appeal to a broader range of moral foundations than liberals. 

Why do the working class generally vote Republican when it is the Democratic Party that wants to redistribute money more evenly in society? Maybe because these voters are, in fact, voting for their moral interests, which fit better with the conservative morality of the Republicans than the liberal morality of the Democratic Party.

The moral interests of liberals are limited to concerns about _care_, _fairness_ and _liberty_ :

The politics of the Democratic Party urge people to _care_ for innocent victims by protecting them from harm.

Liberals are also overtly concerned with social justice, accusing wealthy groups of exploiting those at the bottom. This concern about _fairness_ was a major theme of the Occupy Wall Street movement.

In addition, their preoccupation with _liberty_ centers on freeing the oppressed by fighting for civil and human rights.

Themes of _care_, _fairness_ and _liberty_ are also part of conservative morality, but in another way. Conservatives _care_ mostly for the people who have made sacrifices for their group. For conservatives, _fairness_ means that people should be rewarded in proportion to what they contribute. _Liberty_ means more freedom and less government, which they fear will use its power to redistribute wealth.

Even more importantly, conservatives also endorse _loyalty_, _authority_ and _sanctity_ in their moral interests.

The conservative near-monopoly on ideas of _loyalty_ becomes clear in their strong appeals to military virtues and their patriotic honoring of the American flag. They also rely heavily on the idea of _authority_, by respecting traditions, the police and elders in their politics. Additionally, their Christian ideas about sexuality give them the chance to appeal to _sanctity_.

This gives conservatives an advantage, since they have a broader palette of moral triggers to work with in their campaigns and political arguments. Hence, working-class people who also feel strongly about loyalty, authority and sanctity will be drawn more toward conservatism than liberalism.

**When wooing voters, conservatives can appeal to a broader range of moral foundations than liberals.**

### 10. Public policy is threatened by polarization; both liberals and conservatives should contribute their strengths. 

Most people are familiar with the heated political debates that rage between liberals and conservatives. Worryingly, it seems there is less and less middle ground between the groups.

In fact, public policy has become more polarized than ever, a development that is noticeably reflected by voters. From 1976 to 2008, the proportion of Americans living in highly partisan counties increased from 27 percent to 48 percent, and today fewer people than ever before call themselves centrists or moderates.

This development threatens the entire world. Take for instance the two parties' failure to agree on raising the debt ceiling in 2011 — a failure that made them responsible for sending stock markets plummeting around the globe.

Contrary to the current state of affairs, the best governance would stem from liberals and conservatives complementing each other's strengths.

For instance, liberals' key strength is caring for victims of oppression. This makes them good at finding and proposing government regulations to help these victims. For example, in the 1990s they a regulation was passed to remove the dangerously high lead content of gasoline, which was polluting the environment and retarding the neural development of millions of children.

Conservatives, on the other hand, understand that you "don't usually help the bees by destroying the hive." They can detect when the overall welfare of a society is being compromised by directing too much help to one subset of "bees," and will oppose changes that weaken other moral communities, traditions and institutions. This was the case a few decades ago when there was an attempt to empower students by giving them the right to sue their teachers and schools. The initiative eroded authority and created disorderly school environments throughout the 1970s, hurting more students than it benefited.

**Public policy is threatened by polarization; both liberals and conservatives should contribute their strengths.**

### 11. Final summary 

The main message of this book is:

**Moral judgments arise from our intuitions because humans are fundamentally intuitive, not rational. In addition, the reasoning that follows our intuitions does not work like a judge, guiding us to sober moral wisdom. It works more like a lawyer, justifying our moral judgments to others and ourselves, supporting our reputation and our self-interest.**

**While our intuition-based morality might divide us into different communities and political parties, by understanding the moral foundations on which all our moral interests are based we might be able to disagree more constructively.**

Actionable ideas from this book in blinks:

**If you want to persuade others, appeal to their sentiments.**

If you want to win a moral argument, never say "you're wrong." You can never win someone over to your principles through purely rational arguments. Instead, if you want to persuade the other person, first of all smile and be a good listener.

We are all fundamentally intuitive, as are the moral judgments we make. So, if you want to change someone's mind about a moral issue, it is important to begin by talking to his or her emotions. Even though empathy may be difficult across a moral divide, you will have to express warmth, respect and openness to dialogue before declaring your own case, or you will have no chance of persuading the other person.

**Remember that happiness comes from between, not within.**

If you want to increase your mental wellbeing, consider getting a more sociable hobby. Group activities have proven to contribute more to people's satisfaction than turning inwards and pondering how to become happy.

We humans evolved to live in moral communities, and our minds are hardwired to help us unite with those in our group. Recent research on social capital even demonstrated that local groups, teams and clubs are essential for the health of individuals. Happiness, therefore, comes not from within, but from getting the right relationship between yourself and something _larger than yourself._
---

### Jonathan Haidt

Jonathan Haidt, PhD, is a social and cultural psychologist at the University of Virginia. He studies morality and emotion and his research has also led to the publication of _The Happiness Hypothesis_.

