---
id: 5bfd77066cee070007f92783
slug: the-rise-and-fall-of-the-dinosaurs-en
published_date: 2018-11-30T00:00:00.000+00:00
author: Steve Brusatte
title: The Rise and Fall of the Dinosaurs
subtitle: A New History of a Lost World
main_color: EDE9DE
text_color: 736232
---

# The Rise and Fall of the Dinosaurs

_A New History of a Lost World_

**Steve Brusatte**

In _The Rise and Fall of the Dinosaurs_ (2018), leading young paleontologist Steve Brusatte takes us on a journey through time and space, detailing the history of the different dinosaurs and the worlds in which they lived. He is guided by his deep knowledge of fossils and geological evidence, and is thus able to bring the fascinating stories of dinosaurs into clear focus.

---
### 1. What’s in it for me? Get to grips with a topic you wish you knew more about. 

Dinosaurs have long captivated our imagination. Just think of successful film franchises like _Jurassic Park_, and who can forget the T. Rex fight in Peter Jackson's _King Kong_?

But that's the stuff of fantasy. How much do you _really_ know about dinosaurs? That's where Steve Brusatte can help. Humans have been around for a just a fraction of the time dinosaurs existed — which makes you realize just how quickly it could all vanish, as it did for the dinosaurs.

For almost 200 million years, dinosaurs roamed and ruled the Earth, long before humans could even be considered a species. Then, 66 million years ago, they were completely wiped out. We've only been able to piece together — sometimes quite literally! — facts about them and their environment over the last few centuries.

Now, thanks to fascinating new research and discoveries, we're closer than ever to knowing about the history of the dinosaurs — even though some pieces of the puzzle are still missing.

Let these blinks take you on a journey, traveling to the land before humankind: the land of dinosaurs!

In these blinks, you'll learn

  * what Disney films _Fantasia_ and _Pinocchio_ have to do with a T. Rex;

  * where half of all recent dinosaur fossil discoveries have occurred; and

  * why Transylvania was the home of dwarf dinosaurs.

### 2. There was life on Earth before the dinosaurs, and it took a cataclysmic event for them to become dominant. 

In the popular imagination, the dinosaurs' rule over the Earth lasted from the first stirrings of life on the planet right up until their extinction.

But, of course, dinosaurs weren't the first inhabitants of Earth.

It was around 390 million years ago that life first crawled onto land. From then until the end of the _Permian Period_, around 252 million years ago, the animal kingdom comprised a range of strange reptilian and mammalian creatures.

The Permian Period, however, ended with the largest mass extinction event in Earth's history.

It began when volcanoes started exuding vast amounts of magma. It continued to flow for several hundred thousand years, perhaps even a few million. There's evidence of the scale of the disaster from geological records; when looking at formations from this period, the rock type changes dramatically and fossils simply stop appearing.

It was devastating: around 90 percent of all species had died out by the time the crisis ended and ushered in the _Triassic Period_.

However, not all life vanished. Tracks from early _archosaurs –_ the early reptilian ancestors of dinosaurs — have survived from around 250 million years ago.

These archosaurs thrived in this new world, and soon divided into two groups. There were the ancestors of our modern crocodiles, the _pseudosuchians_ — their name means "false crocodiles" — and the _avemetatarsalians_.

It was these avemetatarsalians that evolved into dinosaurs, who then split into three groups. There were the meat-eating _theropods_, the plant-eating _ornithischians_ and the long-necked _sauropods_.

These groups didn't just evolve and survive — they thrived.

For instance, 230 million years ago, in what is now Argentina's Ischigualasto Provincial Park, numerous species established themselves. We know this because of the phenomenal quantity of fossils that have been found there over the course of the twentieth century. The hot and humid climate there, which led to occasional flooding, was ideal for preserving fossils.

So, it had taken a near apocalypse to for the dinosaur age to begin, but the Triassic Age had now truly dawned. It was to be a fascinating and diverse evolutionary journey — yet there was still quite a long way to go.

### 3. Dinosaurs weren't immediately dominant, but soon took advantage of ecological conditions to establish themselves. 

Two hundred and thirty million years ago, Earth bore little resemblance to the planet we know now. There were no continents, just a single landmass now known as _Pangea_. It was also _very_ hot; the poles weren't frozen and were as temperate as London or San Francisco today. On top of all of that, so-called "megamonsoons" plagued Pangea, effectively dividing it into different environmental regions.

Each of these new environmental provinces existed essentially as a blank slate. The Permian extinction meant that they became fertile breeding grounds for all sorts of creatures. Nature started experimenting and, soon enough, huge numbers of various mammals, amphibians and reptiles could call Earth home.

Dinosaurs were there too, of course — but they hardly dominated. In Ischigualasto, for instance, they only made up around 10 to 20 percent of the ecosystem.

And even that is comparatively high: the author's own field research in Portugal showed that dinosaurs didn't even live in the hotter equatorial regions of Pangea during this period.

In nearby Spain and Morocco, you only find reptiles and amphibians in the fossil record. It's therefore clear that dinosaurs kept to more humid regions in the southern hemisphere, including modern-day Brazil and India.

However, the limits of the dinosaurs' domain didn't remain static; dinosaurs increased their numbers and soon spread around the world. There were two reasons for this.

First, around 225 to 215 million years ago, the non-dinosaur herbivores that dominated these humid ecosystems began to decline in numbers. Scientists aren't exactly sure why this happened, but it provided dinosaurs with the opportunity and resources to multiply rapidly, eventually comprising around 30 percent of species in those regions.

Then, around 215 million years ago, probably due to changes in the climate, it became possible for some dinosaurs to migrate northward through Pangea.

Evidence for this is found across the US states of Arizona and New Mexico. There, for instance, the Chinle Formation, a rock sequence formed around 225 to 200 million years ago, is teeming with fossils from this period. It shows a rich ecosystem, full of large amphibians and reptiles. But, critically, a few smaller meat-eating theropods are preserved there too.

After 30 million years, dinosaurs had gotten a foothold, but they still existed in the shadow of pseudosuchians — the "false crocodiles" we met in blink one.

However, the dinosaurs were about to get their big break.

### 4. The dinosaurs survived another mass extinction event, diversifying to become some of the biggest animals ever. 

Around 240 million years ago, Pangea started to break apart. Ultimately, from this gradual dissolution, the continents we inhabit today were formed in a slow and barely perceptible process. But then, around 201 million years ago, the Triassic Period came to a violent close.

As Pangea broke apart, magma began pooling just beneath the Earth's crust and eventually burst through. But it was no normal volcanic eruption. Tsunamis of lava obliterated about 3 million square miles of central Pangea. Overall, around 30 percent of all existing species were killed. And it was no one-off event, either; in total, four lava surges, each up to 3,000 feet deep, passed over the Earth.

Incredibly, the dinosaurs withstood this carnage. The fossil record not only confirms their survival but also their dominance in the new ecosystems of the _Jurassic Age_.

This is clear if we look at the North America seaboard. It's replete with rift basins like the Gettysburg Basin, bowls that were formed when the continent split from northwestern Africa.

If you study these basins along the direction of dinosaur migration, you'll see that dinosaur fossils became more abundant and diverse _after_ the eruptions. In contrast, the pseudosuchians effectively vanish. Paleontologists still aren't sure why the two groups had such different fortunes.

The real reasons why the dinosaurs flourished remains a mystery, but flourish they did. This was the age of the sauropods, massive dinosaurs who were the largest animals _ever_ to have lived on land. You're no doubt familiar with the instantly recognizable long necks of the _Brontosaurus_, _Diplodocus_, and _Brachiosaurus._

The oldest fossils — found on Scotland's Isle of Skye — show that the first sauropods were living in the region some 170 million years ago and grew to about 50 feet in length. Sauropods were in fact so large that when excavators first found their bones back in the 1820s, they were flummoxed by their size. On first inspection, they mistakenly thought they were whale bones!

But the size of these sauropods served a purpose. Scientists have postulated that the sauropods' long necks allowed them to seek out and consume larger quantities of food compared to other dinosaurs.

Their efficient breathing mechanisms, light skeletons, ability to expel excess body heat and, of course, their impressive growth rates meant they could reach gargantuan heights, far outstripping lesser dinosaurs.

> _"Imagine Pangea as a giant pizza, being torn apart by two hungry friends at opposite ends of the table."_

### 5. Thanks to geological processes and mercenary fossil hunters, we know an awful lot about the Jurassic Period. 

The Late Jurassic Period, about 150 million years ago, was teeming with dinosaurs. We know this because of the vast fossil record that has been preserved — but why is this the case?

For starters, many different types of dinosaurs lived close to water. This is critical because rivers, lakes and seas are ideal locations for fossil preservation. Layers of sediment build up and become rock, forming and protecting fossils in the process. And it's there where these fossils sit, waiting to be discovered.

One particular such area in the United States, known as the Morrison Formation, is so rich in fossils that it became the battleground of the _Bone Wars._

When fossils were found at several locations in March 1877, many opportunists rushed to the region. They were there to get jobs from one of two bitter rivals: Philadelphia's Edward Drinker Cope or Othniel Charles Marsh of Yale University.

Their teams discovered some of the most famous dinosaurs of all time, including the carnivorous _Allosaurus_, as well as the long-necked herbivores we discussed earlier: the Brontosaurus, Diplodocus and Stegosaurus _._

But it's not just the fossil record that helps us piece together the age of dinosaurs. Further paleontological discoveries have meant that we have a good sense of broader change in the Late Jurassic Period, too.

Pangea was still splitting but was now only doing so at roughly the same speed that fingernails grow. The result was that ecosystems the world over were now broadly similar.

However, around 145 million years ago, as the _Cretaceous Period_ began, the climate began to gradually change. A cold snap and arid conditions had caused sea levels to fall and the amount of landmass to increase. Inevitably, this altered the makeup of dinosaur communities.

Within about 20 million years, most of the gargantuan sauropods were extinct. In their place, smaller plant-eating ornithischians were flourishing. And because there were plenty of them around, it meant that a large variety of carnivorous theropods could survive off this new bounty.

The most impressive of these theropods was perhaps the enormous and terrifying _carcharodontosaur._ It originated in Africa in the Late Jurassic Period and then spread around Pangea, diversifying as the land broke apart. Eventually, by the early- and mid-Cretaceous Periods, it topped the food chain.

The fact that it gets its name from Greek for "sharp-toothed lizard" gives you an idea of just how menacing it would have been.

However, as intimidating as the carcharodontosaurs might have been, another family of carnivores was waiting in the wings, preparing to upstage them.

### 6. The T. Rex is deservedly fearsome and famous, but the rest of large tyrannosaur family were just as impressive. 

If you've heard of one dinosaur, then it has to be the _Tyrannosaurus Rex._ But did you know that _T. Rex_ was actually just one member of a whole family of _tyrannosaurs_?

Thanks to discoveries in the last 15 years, new light has been shed on this fascinating family. In total, 20 new types of tyrannosaur have been unearthed all over the world, from the Gobi Desert to the Arctic Circle.

The author himself was involved in identifying the _Qianzhousaurus sinensis_ after bones were discovered in China in 2010. It even has a cute nickname: Pinocchio Rex! Interestingly, around half of all newly discovered dinosaur species are found in China.

There are some general characteristics that all these family members share.

They were all carnivores and had huge heads, strong athletic bodies and legs and long tails. And, of course, who could forget those comically small and seemingly useless arms?

They were also a pretty tough bunch. Although tyrannosaurs first appeared in the mid-Jurassic Period, they actually hit the peak of their dominance in the Cretaceous Period.

The oldest member that has been identified to date is _Kileskus,_ which was discovered in Siberia in 2010. It lived around 170 million years ago; that's about 100 million years before _T. Rex_. It was only about seven or eight feet long and would have lived in the shadow of the 30-foot-high _Allosaurus_ and other, even larger, carcharodontosaurs.

It might seem odd to associate this pipsqueak of a dinosaur with the massive T. Rex, but there are good reasons for doing so. An evolutionary stepping stone has been discovered: the _Guanlong_ has characteristics in common with both T. Rex and Kileskus, as well as several other tyrannosaurs.

What we don't know, however, is just how or when exactly they grew so large and how they managed to bestride continents. That's because there's a poor fossil record from around 110 to 84 million years ago.

We just know that tyrannosaurs were once smaller and living among other larger predators, but that they eventually superseded the carcharodontosaurs in North America and Asia.

By the time we enter the Cretaceous Period proper, the rule of one king in particular was undisputed.

### 7. Thanks to fossil records we know more about T. Rex than many living animals, and it deserves its monstrous reputation. 

If there's one dinosaur that remains more famous today than any other, it's the dreaded _Tyrannosaurus Rex –_ literally meaning the _tyrant lizard king_!

How did it get to such a position of ignominy?

For starters, we're hardly short of T. Rex fossils. Fortunately, a ton have been discovered, which means we know quite a lot about them.

It was a young fossil collector called Barnum Brown who discovered the first specimens in Montana in 1902. They were put on public display in 1905 at the American Museum of Natural History in New York. So respected was Brown in later life, that he actually helped Walt Disney design the dinosaurs for the film _Fantasia_.

Since then, over 50 skeletons have been unearthed, some of which are almost complete. That's far more than most other dinosaurs.

Thanks to these discoveries, we know that T. Rex lived around 66 to 68 million years ago, and that they completely dominated western North America. What's really interesting, though, is that the T. Rex is actually very closely related to some Asian species.

We therefore think the T. Rex originated in China or Mongolia, and then migrated across the Bering Land Bridge, before heading down through Alaska and Canada to reach its new playground. There were already other tyrannosaurs living in this part of the world, but they were no match for the new Asian arrivals.

The most obvious reason why everyone knows about the T. Rex, though, is because it was absolutely terrifying. T. Rex grew to about 42 feet in length and weighed around seven or eight tons, and its mouth was full of razor-sharp teeth.

It's been shown from experiments that the T. Rex could bite with a force of about 3,000 pounds _per tooth_. By way of contrast, African lions manage just 940 pounds _in total_. Fascinatingly, there's even an _Edmontosaurus_ tail fossil with a T. Rex tooth wedged in it. The Edmontosaurus must have soldiered on and healed after a failed, but no doubt brutal, attack.

The T. Rex was also actually pretty intelligent as far as dinosaurs go. As CAT scans have shown, it had a large brain size relative to its body. We think that it had an IQ similar to that of a chimp — it was certainly cleverer than any dog or cat you might keep at home.

It's also been suggested that they hunted in packs. Imagine how it would have felt confronting that!

### 8. T. Rex may have ruled North America, but that wasn’t the dinosaurs’ only evolutionary success story. 

T. Rex had North America as its stomping ground, but there was no way it could have dominated the rest of the world. After all, by the end of the Cretaceous period, around 84 to 66 million years ago, the continents were about as split and separate as they are today. What's more, because of continental drift, ecosystems had diversified and changed.

This begs the question: What other dinosaurs were there on other continents?

The author has been fortunate enough to spend time examining fossils found in Goiás, Brazil. There, his findings showed that carcharodontosaurs had actually survived and dominated the local ecosystem. This was most likely because tyrannosaurs never made it that far south and thus never toppled them from their position of power.

Interestingly, it also seems that throughout the southern hemisphere, various types of crocodiles thrived in environments where small- to medium-sized theropods had failed to materialize.

Elsewhere, diversity emerged due to localized sets of circumstances _._ In the late-nineteenth century, Baron Franz Nopcsa von Felső-Szilvás dug up various fossils on and around his estate in Transylvania. They all dated from this same period, but they were all miniature versions of species found in the rest of the world. A bit of geological research revealed that his lands had once been an island, and it was this _island effect_ that had ensured the evolutionary development of these dwarf dinosaurs.

Back in North America, as successful as the T. Rex was, there was also another species that had adapted extremely well to the environment.

Lumbering on all fours and bedecked with horns, the _Triceratops_ remains one of the most iconic and memorable dinosaurs of all time. They were herbivores who had evolved beaks at the end of their snout, probably for plucking plants, before their bladed guillotine-like teeth then quickly stripped and sliced this plant matter.

They were almost certainly tasty targets for T. Rexes. But, as they were so strong and large, they were probably able to put up a good fight, no doubt using their horns.

Triceratops certainly thrived though, as did many other dinosaurs. Fossils found in Hell Creek, Montana, indicate the Triceratops made up 40 percent of the population, while the T. Rex peaked at about 25 percent. In fact, this region the was richest ecosystem of dinosaurs that we know of.

### 9. Birds didn't simply evolve from dinosaurs, they are dinosaurs – and their main characteristics are Jurassic relics. 

It's a common myth that birds evolved from dinosaurs. But that's not quite right.

Birds did initially evolve _from_ dinosaurs, but they then evolved _alongside_ them _._ It's best to think of birds as a subgroup of dinosaurs, much as sauropods or tyrannosaurs were.

The initial link between birds and dinosaurs was first put forward in the nineteenth century. But it was a long time before it gained traction and was accepted.

When Charles Darwin published his foundational thoughts on evolutionary theory in 1859, a furious debate ensued. Both proponents and skeptics began searching for proof to demonstrate or disprove the gradual development of species over time.

It didn't take long to find some convincing evidence; a "missing link" was found in a Bavarian quarry in 1861. The 150-million-year-old fossil of an _Archaeopteryx_ seemed to be a creature that was halfway between a reptile and a bird.

It was Thomas Henry Huxley — the grandfather of famed author Aldous Huxley — who cracked it. He spotted the similarities between _Archaeopteryx_ and _Compsognathus_, a small dinosaur that was known from a fossil found in the same area. He therefore concluded that birds had originally descended from dinosaurs.

However, Huxley's theory soon fell by the wayside. A Danish theory from the 1920s argued that birds and dinosaurs couldn't be related. After all, dinosaurs didn't have collarbones, while birds have them in the shape of wishbones, which are essentially just fused collarbones.

It took until the 1960s for this alternative theory to be debunked. Paleontologists uncovered a bird-like fossil in Wyoming, which seemed unquestionably to be a close cousin of _Velociraptor._ Consequently, the older theory was resurrected.

The final proof was unearthed in China in 1996: a fossil for the dinosaur _Sinosauropteryx_ clearly showed it had feathers.

Feathers weren't the only attribute that we associate with birds to develop in dinosaurs during the Late Cretaceous period; fossils from the Gobi Desert clearly show parent dinosaurs that had been killed while tending to a nest of their eggs.

It also appears that feathers didn't initially originate for the purpose of flight. They were most likely there to provide insulation and protection.

And don't forget the role feathers have in attracting mates. Based on pigment analysis, it seems these early wings were very colorful, so it's likely they were used primarily for display. It's also thought that various species learned how to glide independently from one another, and thereafter evolved into the diverse variety of birds we know today.

Based on fossil record, it appears that birds and dinosaurs coexisted for around 100 million years. However, another catastrophe would befall the Earth — and the word "cataclysmic" barely does it justice.

### 10. A catastrophic impact from space killed the dinosaurs, but some paleontologists claim that dinosaurs were already on the way out. 

Sixty-six million years ago, an asteroid or comet — scientists are unsure which — about the size of Mount Everest smashed into Earth. It hit with a force equivalent to a billion nuclear bombs, and set off a global chain reaction. By the time it was over, 70 percent of _all_ species on Earth were extinct, the dinosaurs among them.

The geological evidence is clear enough: there has never been any doubt that a cataclysmic extinction occurred. For instance, in a gorge outside of Gubbio, Italy, a thin layer of clay separates the fossil-rich Cretaceous Period limestone from the barren limestone of the Paleogene Period.

But, critically, scientists were perplexed as to what had caused the event.

Walter Alvarez was one of the scientists who began to search for answers. He set out to determine how long this layer of clay had taken to form. He was sure the solution could be found by measuring iridium.

This element falls gradually from space; therefore, by measuring its presence in the clay, Alvarez would be able to calculate how quickly the layer had formed.

To his surprise, the amount of iridium present was, quite literally, astronomical. It seemed to bear no relation to the amount of iridium in the limestone on either side of the clay layer, whose formation could be accurately dated. So in 1980, Alvarez proposed that something must have brought it from space all at once.

It remained merely a hypothesis until a 110-mile-wide crater was found in Mexico in the 1990s. It must have been caused by something from outer space and, what's more, it could be dated to exactly the right moment. The theory was confirmed.

But we mustn't imagine that everything was hunky-dory for the dinosaurs before the comet hit. It seems that climate change was a problem, and some people have argued that dinosaurs were already facing extreme difficulties.

The author himself has tried to work out what was going on. He analyzed species _diversity_ in the two main dinosaur groups at the bottom of the food chain: plant-eaters — horned ceratopsians similar to _Triceratops_ — and duck-billed dinosaurs.

He identified that diversity was indeed declining; however, overall population numbers remained steady. This seemed to suggest that they weren't becoming extinct at this stage. On the other hand, a less diverse food web is also more susceptible to collapse.

We therefore may never know for sure whether the dinosaurs' extinction was inevitable, but we _do_ know what actually killed them. Traces of their dominance still remain thanks to the few bird species that managed to survive the apocalypse.

But the slate was all but wiped clean — the world that was once brimming with dinosaurs was now free for the rise of humankind.

### 11. Final summary 

The key message in these blinks:

**For some 200 million years, dinosaurs took every advantage of their changing world to evolve and become the rulers of the Earth. From the movies we make starring the foreboding T. Rex, to the actual dinosaur subgroup we see every day — today's birds — the legacy and mystique of the dinosaurs has endured.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Tangled Tree_** **by David Quammen**

_The Tangled Tree_ (2018) provides curious readers with a vital recap of the many scientific twists and turns that have taken place in our understanding of evolution since the days of Charles Darwin. Author David Quammen's lucid explanations will bring you up to speed on all we know and don't know about how life developed on planet Earth.
---

### Steve Brusatte

Steve Brusatte is an American paleontologist at the University of Edinburgh. He has written for _Scientific American_ and has been a consultant for the BBC's _Walking with Dinosaurs_. He has also worked with major figures in paleontology, and his groundbreaking discoveries have established him as a world expert in the field.

