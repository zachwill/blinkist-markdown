---
id: 59f986a3b238e10006f6082b
slug: the-road-less-traveled-en
published_date: 2017-11-01T00:00:00.000+00:00
author: M. Scott Peck
title: The Road Less Traveled
subtitle: A New Psychology of Love, Traditional Values and Spiritual Growth
main_color: DF4151
text_color: C22737
---

# The Road Less Traveled

_A New Psychology of Love, Traditional Values and Spiritual Growth_

**M. Scott Peck**

_The Road Less Traveled_ (1978) is a personal and professional account of how you can live a more fulfilling life by practicing discipline and developing a better understanding of love, religion and grace. Certain pathways in life are less traveled because they're more challenging, but, in this case, the path to enlightenment is also far more rewarding. Find out what steps you can take to grow and become a more balanced person.

---
### 1. What’s in it for me? Find new tools for spiritual growth on a road less traveled. 

"Take the high road," "The road to hell," "Staying on the straight and narrow": the list of sayings in our language that use roads as metaphors for life could go on and on. Many of them are about being a good or bad person. But what about a road that could lead to spiritual growth and exploration, a road less traveled?

In these blinks, we'll take a trip down this road, and learn how a few concepts can lead you down the path of spiritual growth — a path that will help in everyday life as well as with more metaphysical parts of existence.

You will also find out

  * why it's good to delay gratification;

  * how love is something more than a feeling; and

  * why being lazy — not eating the apple — was Adam and Eve's original sin.

### 2. Having a better life starts with practicing self-discipline and developing the habit of delayed gratification. 

Let's not mince words: life stinks. And the sooner you accept this, the better off you'll be.

But don't despair. It's just a simple fact of life that each day comes with the possibility of a new set of problems, and, once you recognize this, you'll be on your way to having a solution for these problems.

Most folks live with the delusion that life should be fair, pleasant or otherwise rosy. This outlook will only lead to disappointment. In contrast, understanding that life is inherently difficult will spur you to assemble the tools you'll need to get by.

Along with this healthy perspective, the best tools you can have are those that help you practice self-discipline; and the first tool is getting familiar with _delayed gratification_.

No one likes to wait for something good when they could have it right away. Many of us would prefer to have dessert before dinner, and we tend to live our lives according to the same philosophy.

This way of life could be called "play now, pay later," and the people who practice it aren't necessarily dumb. They're the sort of intelligent people who nevertheless manage to get bad grades because they'd rather skip class and have fun than deal with the boring business of studying. To put it another way, they're controlled by their impulses.

Let's say you struggle with procrastination. Who among us hasn't tackled the easy work first and then spent the rest of the day struggling with the boring and difficult stuff?

The author had a patient with this exact problem, and he advised her to start practicing delayed gratification by reversing her work habits.

This meant bearing down and dealing with the difficult stuff first. So instead of facing one easy hour followed by six miserable hours of dragging her feet, she could have one miserable hour followed by the reward of six enjoyable hours.

> "Delaying gratification is a process of scheduling the pain and pleasure of life..."

### 3. Discipline also means accepting responsibility, being truthful and striking a healthy balance in life. 

Delayed gratification is just one of the tools that will help you bring discipline to your life.

Real self-discipline also requires that you _accept responsibility_ for your own life.

Do the words "this isn't my problem" sound familiar to you? It's a common way of reacting to the things we'd rather not deal with. But remember: avoidance doesn't solve anything.

When the author, M. Scott Peck, was completing his psychiatric training, the bad habit of avoiding responsibility reared its ugly head. At one point, he was upset about working longer hours than his colleagues. But when he asked the director about this, Peck was told: "It's not my problem. It's your problem with your time."

For months, Peck was furious with the director for avoiding responsibility, when in fact it was Peck himself who was avoiding the problem. No one was forcing him to work more; it was solely up to him to find a solution.

Another tool that can improve your self-discipline is a _dedication to truth_, meaning that you honestly and openly face the reality of your life.

Now, this can be a tough one since it requires that you have a strong sense of self-reflection and be willing to constantly update your worldview.

If this sounds a lot like psychotherapy, you're absolutely right — seeing a therapist is a great way of getting to personal truths. Being open to self-analysis, and reflecting honestly on life, are part and parcel of personal growth.

The final tool for better discipline is _balancing_.

It means letting go and giving up the unhealthy habits and extreme behaviors that are throwing your life out of balance. These are things you may do because they add a thrill to your life, but which, in the end, tend not to turn out well.

The author likens it to an experience he had riding his bike at full speed down a hill. He felt an amazing rush and was reluctant to give up this ecstatic feeling, which is why he wound up crashing into a forest. As he learned, it can be painful to abandon the things that give you a rush, but losing your balance can hurt even more.

### 4. Love is closely linked to our spiritual growth and self-discipline. 

Much has been written and said about the nature of love, but put simply, love is having the will to nurture your own spiritual growth, as well as that of someone else.

In this way, you can look at love as an important part of your evolution, since spiritual growth is about reaching a larger state of being.

It's also important to understand that before you can love someone else, you first need to love yourself. It's similar to a parent teaching a child how to behave in a disciplined manner. For this to happen, the parent must practice this discipline themselves.

To love someone means making an effort. It requires more than a mere desire to love. Most people desire, or want, to love, but few are in fact loving. Those with the will to grow spiritually have chosen to love and therefore they act lovingly.

The difference between the desire to love and the will to love is the difference between saying, "I would like to cook a delicious meal for you" — and then maybe doing it, or maybe not — and saying, "I will cook you a delicious meal," and simply doing it.

Since it requires will and effort, love has what it takes to create discipline in your life.

When you have love in your heart, it's like a storage tank full of energy. This supply can be self-nurturing as well since love allows you to grow and increase your capacity for love.

But sustaining any kind of loving relationship requires discipline.

Once the author counseled a couple who believed that their constant quarrels were simply a natural part of their passionate love for one another, even though the fighting was obviously hurting them. And when they were told that the goal of the counseling would be to improve their self-discipline and balance their emotions so as to stop this, they quit.

Without strengthening their discipline, they were doomed to continue hurting each other.

> "If I truly love another, I will obviously order my behavior in such a way as to contribute the utmost to his or her spiritual growth."

### 5. Love is not a feeling but an action that requires attention and comes with risks. 

More often than not, we think of love as a feeling rather than as an action, and this can lead to problems.

The _feeling_ of love is related to _cathecting_, which means investing emotional energy in something or someone.

If you have a treasured piece of jewelry, for instance, you may cathect that ring or necklace. And when two people meet in a bar and hit it off, they may experience such intense cathecting that the rest of the world falls away.

But this feeling is often fleeting and tied to the specific moment, whereas genuine love comes with a lasting commitment.

Love can even exist without that feeling we always associate with it, since true love is bigger than cathecting.

In a well-functioning marriage, disagreements can happen, and tempers can flare, but the partners continue to work in tandem toward their goals. These are two people who made a commitment through their own volition, and that's more important than any fleeting emotions.

However, for love to flourish it needs more than a commitment — it requires attention and the understanding that it can be lost.

Loving someone means giving them attention and supporting their growth. And this requires putting aside everything else to truly listen and concentrate on what your loved one is saying and experiencing. When you do this, you extend yourself and gain new knowledge about the person you love.

But remember, you can't love someone without the risk of losing them.

There is always the risk that the relationship might fall apart, and you might be left feeling heartbroken and alone. That's part of the price of love, however. Life itself is a risk — and the more love you bring to it, the riskier it gets.

### 6. Religion is essentially a personal worldview that can grow and expand as we gain new perspectives. 

When you think of religion, what comes to mind? For many of us, it's a strict set of rules and ritualistic practices, and being part of a group that believes in and worships God.

But this definition is too narrow.

Religion should be seen as a belief system that is determined by our culture.

In this sense, everyone has a religion, and it may have nothing to do with God or gods. So think of "religion" as just a different word for "worldview," a worldview informed by your upbringing and family culture.

This perspective on religion helped a man named Stewart, a severely depressed patient who considered himself an atheist. But once the author helped him explore his worldview, Stewart began to identify his own religion and how it was affecting his mental health.

When looking at how his family culture affected him, Stewart realized that his belief system painted the world as an evil place, controlled by vengeful forces eager to punish him if he stepped out of line. This came from being brought up by God-fearing and physically abusive parents.

This example illustrates how our worldview is based less on the beliefs that our families conveyed directly and more on the actions and behaviors they engaged in.

Luckily, we all have the capacity for spiritual growth if we allow new experiences to inform our outlook on life.

Think of yourself as a scientist, constantly questioning and exploring the world around you, and using this information to inform your opinions. This mentality will allow you to rebel against the religion of your parents. You needn't adhere to their narrow worldview when you can take in all that life has to offer and form your own perspective.

Don't let past experiences blind you to the beauty of the spiritual realm. Next, we'll look at the phenomenon of grace.

> _"Spiritual growth is a journey out of the microcosm into an ever greater macrocosm."_

### 7. Ordinary occurrences can illuminate the extraordinary phenomenon of grace. 

There's a good chance you know the hymn that starts, "Amazing grace! How sweet the sound…" Because of this popular tune, the word "grace" is most closely associated with "amazing."

But what is grace, exactly? And is it, in fact, all that amazing?

Grace is actually a rather common phenomenon. It's the force that protects people's physical and mental health, even in the most trying of conditions.

Interestingly enough, when you have a neurosis, a good psychiatrist can determine exactly why that condition arose and why it persists. Yet it remains a mystery why some neuroses aren't more severe, and why many people who have suffered tremendous traumas can still go on to live good lives.

One of the author's patients is an incredibly successful businessman who had only a mild neurosis. Yet his upbringing was filled with both psychological and physical trauma, including time spent in cold and unloving foster homes as well as a period, during his teens, locked up in jail.

While it was possible to explain and treat his neurosis, it's impossible to explain how he coped with his upbringing and went on to become such a remarkably successful adult.

The author believes this can only be explained by positing an external force that protects people and their mental health, thereby enabling them to survive the most adverse conditions.

So perhaps grace is more than just an amazing resilience, and rather a force beyond our consciousness that nurtures our growth as human beings. We can see grace as an evolutionary force from a god who wants to ensure we don't keep things as they are and instead feel the urge to grow.

You might think that this idea is naive, but according to the author, there is no theoretical alternative that can better explain our survival instinct.

If you still doubt it, let's take a look at some different "miracles" that better demonstrate grace at work.

### 8. Grace can also be found in our dreams and in instances of synchronicity and serendipity. 

We humans have a strong tendency to categorize phenomena. We know that a sandal is different from a canoe, and to better understand the two, we place them in their respective categories: shoes and boats.

But the phenomenon of grace lies outside the scope of our traditional senses, much like the miracle of our unconscious and dream states.

In his work with patients, the author has come to find evidence that our unconscious can skillfully assist the therapeutic process by providing insightful dreams.

Analysis has revealed certain dreams to be warnings of personal pitfalls. The author believes that dreaming is an example of how grace operates, since dreams seem to help promote our spiritual growth.

According to the author, people have been known to have remarkably similar dreams, or even to psychically transmit images to one another. Psychic phenomena such as these are examples of _synchronicity_, which can be seen as another example of grace in action.

Synchronicity is the frequent occurrence of highly unlikely events, and, like grace, it's hard to find an explanation for it. There are documented occurrences of synchronicity but there's no logical way to explain why two people would share the same dreams or be able to transmit images.

But when events like these happen, they're often beneficial to the people involved, making it an act of serendipity, which _Webster's Dictionary_ defines as "the gift of finding valuable or agreeable things not sought for."

The author experienced this himself when writing a book.

Away from home at the time, he was working in his friend's library and feeling a bit stuck with writer's block. And though his friend's wife was ordinarily cold and unkind to him, on this occasion she came into the room unprompted and handed him a book. Even though she had no way of knowing what he was working on, the book she presented him with was _How People Change_, by Allen Wheelis, and it was the perfect material to get him unstuck and continue working.

We can all be touched by grace, even in the most mundane of situations.

### 9. Humanity’s original sin is laziness, which prevents us from attaining spiritual growth. 

In the story of Adam and Eve in the Garden of Eden, we're presented with the tale of original sin.

Eve is seduced by the serpent into eating the fruit of the tree of knowledge, an act forbidden by God. In this case, we could say that the real essence of this sin was that Adam and Eve didn't discuss with each other or with God _why_ the fruit was forbidden, and therefore didn't understand which law they were breaking.

In other words, original sin can be seen as laziness: not taking the time and energy to have a proper internal debate. If we're to have any hope of avoiding sinful decisions and making good ones, we need to be diligent about questioning ourselves and getting to the bottom of why we do what we do.

Of course, this isn't an easy internal discussion to conduct. Reconciling our inner God and our inner serpent is no simple task and interrogating them means opening ourselves to a deeply-rooted struggle. So it's completely natural for us to be inclined to avoid this conflict.

But along with being our original sin, laziness also is the biggest thing standing between us and spiritual growth.

This kind of laziness isn't related to your family or your responsibilities at work. You might put in 60-hour workweeks, tend to your kids and housework, and still be lazy when it comes to dealing with your spiritual growth.

For many, this lack of energy for growth can come down to a fear of change and not wanting to lose the little bit of comfort they have now.

A lot of people find new perspectives threatening and they don't want to bother with the effort it takes to change their comfortable worldview. It's easier to ignore new perspectives than take them to heart, so through both fear and laziness, spiritual growth can remain stagnant.

But it doesn't have to be this way. By acknowledging your resistance, you can start on your way to overcoming the prejudices and obstacles that stand in the way of spiritual growth.

### 10. Final summary 

The key message in this book:

**Living a life of spiritual growth requires a loving nature and being open to new perspectives, including the helpful and mysterious force of grace. Taking the road less traveled isn't easy — it takes effort, will and a strong sense of self-discipline. Fortunately, there are tools and changes that you can add to your life to bring balance and put yourself on the path to spiritual growth.**

Actionable advice:

**Balance your discipline.**

Too much of anything is bad, even discipline. So don't be afraid to add a little flexibility to your life. If you're overly rigid about things like sleeping and eating habits, make sure you change things up from time to time. Instead of waking up at 5 a.m. every day, try to enjoy sleeping in every once in awhile. As with everything else, healthy discipline requires a balance.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Power Of Now_** **by Eckhart Tolle**

_The Power of Now_ (1997) offers a specific method for putting an end to suffering and achieving inner peace, living fully in the present and separating yourself from your mind. The book also teaches you to detach yourself from your "ego" — a part of the mind that seeks control over your thinking and behavior. It argues that by doing so you can learn to accept the present, reduce the amount of pain you experience, improve your relationships and enjoy a better life in general.
---

### M. Scott Peck

M. Scott Peck was a Harvard-educated psychiatrist who had his own private practice and years of experience in mental-health clinics before becoming a best-selling author. His writing is a unique combination of practical psychiatric experience and religious belief, and it has resonated with millions of people throughout the years. In 1983, he published _People of the Lie_, exploring the nature of evil in humanity. He died in 2005, at the age of 69.

