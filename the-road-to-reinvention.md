---
id: 54fec3e5626661000af10000
slug: the-road-to-reinvention-en
published_date: 2015-03-13T00:00:00.000+00:00
author: Josh Linkner
title: The Road to Reinvention
subtitle: How to Drive Disruption and Accelerate Transformation
main_color: FFEB48
text_color: 807419
---

# The Road to Reinvention

_How to Drive Disruption and Accelerate Transformation_

**Josh Linkner**

In _The Road to Reinvention,_ author Josh Linkner demonstrates the importance of curiosity and willingness to change in today's dynamic business world. Companies that want lasting success need to constantly reinvent themselves, their culture, their products and even their customers.

---
### 1. What’s in it for me? Learn why reinvention is crucial in staying ahead of the competition. 

Success is often compared to climbing a mountain. You start at the bottom, and slowly start to climb your way to the top. The journey may be arduous: you may fall, or slip, or struggle on rocky paths. Yet as soon as you reach the summit, you have made it, and can rest and enjoy your success.

But really, this analogy is nonsense. Success isn't a one-way climb on some pile of rocks, it's a constant struggle to reach the top and stay there. If you relax with your momentary victory, you probably won't be successful for very long.

The key to greatness is to work hard and constantly develop your business. These blinks will show you exactly how you can achieve this and what benefits constant reinvention can bring you.

In these blinks, you'll discover

  * why Borders booksellers had to shelve its business;

  * why that dude riding a Harley Davidson motorcycle is probably a lawyer; and

  * how one company racked in profits from having see-through pants.

### 2. To be and stay successful, you have to reinvent yourself. If you don’t, you’ll become obsolete. 

After you've completed a tough task, such as finishing a major project or closing an important deal, you probably feel like you've earned a bit of a break.

However, in the business world, there is nothing more dangerous!

Continued success means being on constant alert, never taking a break and always being ready to _reinvent yourself_ as conditions change.

The key to reinvention is in staying innovative. In the 1960s, Nike became a household name after inventing the modern athletic shoe. But afterward Nike didn't just rest on its laurels. The company kept pushing, testing boundaries and investing new products.

Two recent examples include the company's Fuel Band, an LED-display wristband that measures physical activity and helps the wearer reach fitness goals; and the Fly Knit Racer, a shoe made of knitted threads rather than traditional materials.

This kind of outside-the-box thinking led the magazine _Fast Company_ to rank Nike in 2013 as the most innovative company in the world. Nike's success confirms this; between 2006 and 2012, company revenue rose 60 percent.

Importantly, if you don't disrupt the market with your own business reinvention, then another company surely will.

At its peak, the Borders bookselling chain had over 1,200 stores and by 2009, annual sales had topped $3 billion. Yet just two years later, the company had collapsed. What happened?

Borders Books failed to see that the market was changing. It didn't embrace the digitization of media through e-books, downloading and streaming, and focused only on physical books, CDs and DVDs, investing heavily in physical stores that the digital market was making obsolete.

Clearly, reinvention is necessary to keep your business relevant. The next blinks will offer a starting point on how to begin the process of reinvention.

### 3. Sometimes staying competitive means ruthlessly re-examining your core business. 

The world is constantly changing. Or as Don Strickland, former vice president at Kodak, so eloquently put it: _shift happens_.

Companies that fail to see which direction the wind is blowing and that aren't ready to act against their own immediate interests will surely fall behind.

Kodak is actually a sad example of this. At its peak, the company dominated almost 90 percent of the film market, and even was a pioneer in digital photography technology, to ensure its market dominance.

In 1992, Strickland announced that Kodak was ready to introduce its first digital camera. However, the company board got cold feet. "Why develop digital cameras," they argued, "knowing that it would clearly undermine our core business of selling film?"

But digital film was the _future_ of film, and Kodak was subsequently overrun by innovative competitors — and barely survived.

Kodak's example shows us that, while it might be difficult, change is necessary for survival. However, by constantly redefining your core business, that change will come much easier.

One great place to start your reinvention is to examine your company through the customer's eyes, following the example of Hawaiian company SNUBA.

When the American economy tanked at the end of the 1980s, scuba-diving companies faced a serious problem, namely that customers couldn't afford such a pricey past time. SNUBA took the opportunity to closely examine its business practices from the customer's point of view, and identified a problem: diving required too much money upfront before a person could experience actually diving.

So the company developed a system that didn't require special training or certification, keeping the air tank on a surface, but connected to a diver through a long hose. The SNUBA experience is like diving, but suitable for beginners and cost-effective, too. It was a great success!

### 4. Redefining the “how” is just as important, if not more, as reinventing the “what” in your company. 

When a company runs into a challenge, it often assumes that the problem lies in its products or services. So the company makes a change, introducing a new product line or snazzy offer.

Sometimes, the way forward isn't to think about _what_ you're doing, but _how_ you're doing it.

Product innovation isn't necessary for success. Often a simple change in how you offer, sell or market your products and services can be enough to revitalize your business.

In the mid 1990s, Dan Gilbert, the founder of mortgage provider Quicken Loans, had a vision to sell his mortgages online to customers all across the United States. Today buying products and services online is commonplace, but at the time, Gilbert's idea was revolutionary.

By simply moving documentation processing online, Quicken Loans reinvented the whole process of selling loans. The _what_, that is, the product itself, remained the same. Changing the _how_, however, helped Quicken crush its competition.

Yet it's not enough to reinvent yourself once. If you want to stay competitive, you must devise a system that allows for a constant reinvention.

Quicken's Gilbert had a similar revelation in 2003. Just as a pilot can't rebuild the plane as he's flying, the people responsible for producing, selling and delivering products and services can't be expected to reinvent company processes while fulfilling their normal duties.

Moreover, the skills necessary for process optimization are often different from those for sales and marketing, for example.

So Gilbert created what he called a "mousetrap team," the sole responsibility of which was to scrutinize and innovate all the company's processes, no matter how small.

As a result, the company grew rapidly, and in 2013, company revenue reached $100 billion; what's more, Quicken won a number of notable awards for its high customer satisfaction.

> _"You can win by reinventing the way you do your work, even if the business remains the same"_

### 5. Remember why you do what you do: for your customers. 

In the rush to create innovative new products and services, a company might sometimes forget the reason it's creating things in the first place: for its customers.

In fact, according to the 2012 Customer Experience Index, only 37 percent of the brands evaluated received good or excellent customer experience scores.

Are you part of that 37 percent? What can you do to improve your customer experience?

One place to start is by improving the customer's sensory experience. By appealing to all your customers' senses, you can get them more engaged in your products or services.

Say you run a karate studio. Try putting yourself in a customer's shoes as she enters the studio. How does the studio smell? What does it look like? How does it feel?

Now ask yourself creative questions. What if you offered cool towels at the end of each session? Or played traditional Japanese music, softly in the background? What other ways could you improve the sensory experience for your customers?

Another way to improve the customer experience is by creating an emotional bond through stories that customers can relate to.

That's exactly what athletic clothing company Lululemon did in 2013, after women around the world began expressing frustration that the cloth used for one of the company's major yoga clothing lines was see-through — potentially embarrassing when you're performing yoga positions!

Rather than issue a standard press release apologizing for the mishap, the company instead decided to appeal to its customers' sense of humor by using a story instead.

In its store windows across the United States, mannequins were put in yoga positions with banners displaying slogans such as: "You saw London, You saw France. We promise no more see-through pants" and "We've got you covered."

Branded as "second-chance pants," the new, improved line of yoga pants continued the story that their customers had created, and in doing so succeeded in turning a problem into an opportunity to bond with customers.

> _"People will forget what you said, people will forget what you did, but people will never forget how you made them feel." — Maya Angelou_

### 6. Changing your company culture can reinvigorate your company’s creative potential. 

All companies have a unique _culture_, one that is often so ingrained in the company that it becomes invisible to employees.

However, just like products and services, a company culture also needs to be constantly reinvented. Taking a fresh look at the values, goals and attitudes that shape the everyday life of your company can help revitalize your team's creative energy and help you reach new levels of success.

One place to start is by ensuring that your company culture empowers employees to take responsibility for their own decisions, as this will unleash huge amounts of productive energy.

Take the Indian tire company Apollo Tyres, for example, which by 2005 had become a successful, profitable company. But company leaders wanted to take Apollo further.

It dawned on managing director Neeraj Kanwar that what had been missing was a common goal, to be shared among the entire company. So he gave employees a challenge: they would try to grow their revenue sevenfold, to $2 billion by 2010.

To do this, the company would have to overhaul its culture. So Apollo empowered team members to make decisions and take risks independently in an attempt to achieve their goal. In doing so, Kanwar succeeded in unleash the team's latent energy and ultimately, Apollo reached its lofty target!

Sharing company rituals among employees can also help reinforce a company's core values.

In 2012, for example, cloud computing company Rackspace made it on _Fortune_ magazine's list of top-100 places to work. The company's shared core value, which it calls "Fanatical Support," is all about fanatically improving the expertise, responsiveness and transparency of the company.

The highest accolade a Rackspace employee can receive is the "Fanatical Jacket," a straitjacket symbolizing — and reinforcing — the core value of the company.

According to the researcher and professor Paolo Guenzi, rituals like these can improve employee productivity, build a shared identity and reinforce desired behaviors.

### 7. Take a hard look at your customers. Is it time to reinvent who they are, or what they want? 

Most companies have a clear idea of who their customers are, and some brands have even come to be identified with what others perceive as their customer base.

Just think of a Harley Davidson motorcycle. What type of person do you see riding one?

For companies looking to reinvent themselves, taking a fresh look at customers and finding ways to cater to new segments is a great starting point.

Between 1973 and 1983, Harley Davidson's market share plummeted from 78 percent to 23 percent, as cheaper Japanese motorcycles flooded the American market.

Realizing that the company could never compete on price, the American manufacturer had to ask itself: How exactly can we compete? The answer was simple: through emotion.

Harley had noticed that a new class of customer had begun to emerge. Wealthy bankers, lawyers and doctors — who saw Harleys as status symbols — had formed biker groups, giving themselves names such as the "Rich Urban Riders" and "Rolex Riders."

Taking advantage of this trend, Harley Davidson _raised_ prices rather than lowering them.

As a result, by 1990, 60 percent of the company's customer base was comprised of college graduates and professionals worldwide.

Another way to reinvent your customer base is by re-examining the scope of customer segments.

If you want to build a prosperous business, it's natural to want to appeal to the broadest possible customer base, by building a product that fits everyone's needs. However, honing in on a narrow customer segment can help you to avoid competition.

The California-based company GU Energy Labs is a case in point. Their GU Energy Gel is made exclusively for endurance athletes, such as marathon runners and personal trainers, who make up only a tiny fraction of the larger sports market. Yet the company is hugely successful and is the market leader in its niche.

We've focused on how to reinvent your business to move it forward. Our final blink will show you how to reinvent _yourself_.

### 8. Success is as easy as following your passion. Take the time to ask yourself: What do you really want? 

Sometimes being successful in business isn't about reinventing your company or your strategy, but rather reinventing _yourself_.

Take a fresh look at your life, starting with this fundamental question: What is your passion?

Letting your passion guide your life and your business will open a world of new possibilities. Your passion and excitement will give you the strength to truly reinvent your career.

In 1975, motorcycle enthusiast Jim Jannard founded the sunglasses company Oakley, long a favorite among bikers and skiers. Yet after selling the company in 2007 for $2.1 billion, Jannard became obsessed with cameras.

His passion for digital photography fueled his idea to create a digital camera that could produce the same results as a regular film camera, but at a much lower price.

With the creation of his RED camera, Jannard managed to improve the quality of digital photography one hundred-fold and convinced many film enthusiasts that the future was digital.

What's more, reinventing the values that drive your life can help you achieve things that you really think are important.

Tennis player Andre Agassi, at his peak, had it all: money, fame and power. Still, he wasn't happy. In 1997, he fell out of the top professional rankings, became depressed and turned to drugs.

But after a period of introspection, he reshaped his life. Now he was guided by new values such as generosity, compassion and thoughtfulness as his metrics of success, instead of tennis rankings and earnings.

Through this, Agassi found new joy in tennis, and in 1999 he was again number one in the world. His success enabled him to pursue his _true_ passion: charity. He founded the Andre Agassi Foundation for Education, dedicated to helping at-risk children, a foundation that has raised over $60 million.

> _"Your time is limited, so don't waste it living someone else's life." — Steve Jobs_

### 9. Final summary 

The key message in this book:

**To stay competitive over the long haul, companies** — **as well as individuals** — **must be open to constant change and reinvention. By rethinking who your customers are, how you manage your processes and how you express your company culture, you can find new, inventive ways to succeed.**

Actionable advice:

**Bring reinvention to your meetings.**

At your next meeting, set a timer for ten minutes and imagine that you've been invited to transform your company for the future. You've got $100 million at your disposal; what will you do? Try coming up with 35 ideas that will dazzle the CEO. Go crazy. Quantity is more important than quality!

**Suggested further reading:** ** _Good to Great_** **by Jim Collins**

_Good to Great_ presents the findings of a five-year study by the author and his research team. The team identified public companies that had achieved enduring success after years of mediocre performance and isolated the factors which differentiated those companies from their lackluster competitors.

These factors have been distilled into key concepts regarding leadership, culture and strategic management.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Josh Linkner

As CEO of Detroit Venture Partners, Josh Linkner has played a key role in revitalizing business in the city. He is the author of _The New York Times_ bestselling book, _Disciplined Dreaming,_ and has received U.S. President Obama's Champion of Change award.

© Josh Linkner:The Road to Reinvention copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

