---
id: 56d5a45094333d000700005e
slug: the-road-to-serfdom-en
published_date: 2016-03-03T00:00:00.000+00:00
author: Friedrich August von Hayek
title: The Road to Serfdom
subtitle: None
main_color: B84544
text_color: B0302F
---

# The Road to Serfdom

_None_

**Friedrich August von Hayek**

_The Road to Serfdom_ (1944) explains the potential of socialist systems to become totalitarian and why this was so significant after WWII. These blinks will show you how socialist planning can lead to a loss of freedom, individuality and democracy.

---
### 1. What’s in it for me? See Western democracies’ potential to become totalitarian states. 

In 1944, World War II was in its final stages. On the eastern front, Russia was gaining momentum against Nazi Germany; on the western front, the United Kingdom and the United States were planning to invade Normandy. The allies were ascendant, and there was reason to believe that the Nazis and their allies would be defeated. 

So why did the author, who would later win the Nobel Prize in Economics, fear that the Allied powers themselves were in danger of turning into totalitarian states?

Well, he saw similarities between the socialist planned economy and the social structures of Nazi Germany, and believed that these structures could lead to totalitarianism. He felt he had to warn the world. 

Let's delve deeper into his fears and see how one of the twentieth century's most staunchly libertarian thinkers viewed the world in such a tumultuous time. 

In these blinks, you'll find out

  * how to rally people in a totalitarian state;

  * why a minority rule is dangerous; and

  * what the best remedy is to fight a dictatorship.

### 2. Nazism may have fallen after World War II, but another dangerous ideology rose to power. 

As World War II died down and the world began recovering from the threat of Nazi Germany, a new and possibly dangerous ideology was just emerging: socialism. Was this another ideology the world should be wary of? 

The thing is, many thought that Nazism had sprung from the upper class's reaction to the lower class's socialism. But in fact, this was not the case. Before Hitler's rise to power, social democrats in Germany increased state control of the economy as a response to the monetary crisis that followed World War I. It was this state-controlled, partially totalitarian system that set the stage for fascism and the Nazi party.

And if it happened in Germany, what was stopping it from happening in other places, too? 

To avoid this threat, it was important to learn a lesson from Nazi Germany, where socialism and the limitation of personal freedoms by state economic control had led to totalitarianism.

But which countries specifically were at risk at this time?

Actually, in 1944, Germany, the United States and the United Kingdom were strikingly similar as all three nations had reduced freedom and equality. For instance, the beginnings of socialism were visible in the United States and the United Kingdom in 1944, just as in Germany before Hitler's rise to power.

So, while US and UK politics certainly bore no resemblance to fascism at this point, these countries ran the risk of turning down a dangerous path toward a totalitarian future. The author believed that these countries, once unfettered by state economic control, were now, by exercising more authority over private and economic affairs, sliding toward totalitarianism.

So why was socialism gaining momentum? Because of a common misconception.

> _"We have progressively abandoned that freedom in economic affairs without which personal and political freedom has never existed in the past."_

### 3. Socialism is incorrectly associated with freedom and equality of choice. 

At the end of World War II, many people associated socialism with freedom and equality of choice. It was considered a democratic way to live a free and equal life, but these notions were utopian. 

How come?

Because the planned economy of socialism eliminates the possibility for personal freedom. For instance, during times of classic liberalism, science and the economy develop freely while personal freedoms reach unprecedented heights. But socialism has the opposite effect.

In fact, socialism was defined by people who theorized that it would be dependent on a dictatorial state. So, while socialism strives for social justice, security and equality, it also calls for the abolition of private enterprise, meaning the means of production could no longer be owned privately. Instead, these things are controlled by central planning that limits individual freedom. 

On the other hand, classic liberalism endeavors to build a legal framework in which individuals can freely compete. So, a classically liberal society allows for freedom of choice and individuality while a socialist one produces a "new freedom" that dissolves the equality of choice. 

In reality, this road to freedom is one of servitude and misery. That's because this path demands an equality of wealth and power, which is impossible in a classically liberal state that values individualism above all else. 

However, collectivism means less competition and, eventually, a loss of choice. So, while collectivism encompasses different types of economic planning, the real danger of socialism was that it effectively tried to plan against competition. 

How?

As industries are centralized, big monopolies end up dominating the market. In order to counter monopolies, you need a central institution to maintain complete control over them. This spells the end of economic competition, and free choice over pricing and production would face a similar fate

### 4. A planned economy has strong implications for democracy and the rule of law. 

Democracy is beyond the economy, right? In theory, yes. But a planned economy has serious implications for the political future of a country. In fact, it can effectively eliminate democracy. 

So, while it might sound counterintuitive, socialism with a planned economy is practically impossible to attain in a democratic way. That's because the majority of society can vote for a planned economy, but then decisions would need to be made about what the plan entails. The problem is that everybody has different interests and values, which they view as the most pressing and important. 

Therefore, planning in a democracy would basically be like a group deciding they wanted to go on vacation, but not being able to decide on where. In other words, complete chaos. 

As a result, the minority would make decisions for the majority. That's because, in a planned economy where the majority can't decide, the minority must. This is a step toward dictatorship, or the complete loss of democracy and freedom. 

On top of that, the rule of law and individual rights are limited or done away with as a result of planning. 

First, let's look at the rule of law, which says that all laws are predetermined and equally applied. In addition to individual freedom and rights, this is one of the most important achievements of the last few centuries. 

But, for a country to plan an economy, it would need to eliminate the rule of law so that it could react to different situations and shifts. So instead of leaving power and direct deliberation in the hands of a parliament, decision-making power would be vested in small, flexible boards. Individual rights would be drastically diminished, and replaced by a duty to the general well-being of all citizens.

> _"When it becomes dominated by a collectivist creed, democracy will inevitably destroy itself."_

### 5. Socialism leads to dictatorship and major reductions in individual freedom. 

Despite its impact on social structures, socialism doesn't stop people from making their own decisions, right? Actually, it does. Having a planned economy means relinquishing control over most parts of life. 

Think about it: most aspects of your life are dependent on your economic situation. Just think of how you earn money and choose what to buy: the choices made by participants in the economy dictate prices. 

For instance, our jobs consume most of our time. Therefore, the ability to choose our place of work is inextricable from our freedom. However, in a planned economy, the planner is in charge of determining who produces what, how it is divided and what things cost. 

That means the planner decides what job you're most qualified to do as well as what types of products and housing you receive. To give people more personal choices would be to stand in direct opposition to social welfare and the greater plan. 

But what's more important is the fact that someone needs to be in charge. In fact, Lenin himself asked the famous question "Who, whom?" In other words, who is in charge of deciding the fates and needs of whom? 

The necessity to make this decision produces a totalitarian state in the long run because a small group, maybe even a single dictator, ends up deciding what everybody else needs and what opportunities they have. For example, architects could earn less than they normally would while farmers would be paid more. 

So, even though socialism promises a more equal distribution of wealth, it can't treat everyone as absolute equals.

### 6. In totalitarian socialism, the worst people inevitably end up on top. 

The fact that somebody is making decisions for others isn't necessarily a bad thing. In fact, the people in charge might be benevolent and nice, making everyone's life better. Unfortunately, this is unlikely for a variety of reasons. 

First of all, the group in charge would need to be a large one that agrees on its goals and tries to represent all people. This is where the trouble begins:

The more educated people become, the more they differ in their ethics, politics and economic convictions. So, uniting a huge group of people is easier if they think the same way or are part of the less educated "masses." The issue is that people without much of an education are often more easily influenced by effective propaganda and can be recruited to fight for a regime that will actually undercut their freedom. 

But another issue is that the dictator needs to focus on the greater good of society, which means restricting the rights of the minority. In fact, totalitarian socialism legitimizes itself by claiming to work for the greater good, for the more equitable distribution of wealth and by instituting a central plan that governs more or less everything.

However, enforcing this requires a dictator to make morally ambiguous decisions. That means people who believe in democracy and individual rights will never rule in such a totalitarian state, while those with lower moral standards will rise to power. 

So, maintaining the support of the majority in a socialist regime requires a dictator to impinge on the rights of the minority. For instance, by forbidding them from openly expressing criticism of the system.

> _"Even the best intentions will be corrupted by absolute power over time."_

### 7. Totalitarian systems seize and hold onto power through conformity, control of information and scapegoated enemies. 

Say a dictator rises to power. Maintaining control will require him to keep every member of society in line with his ideas. How is he able to produce such conformity?

By controlling information and disseminating propaganda. If everyone is going to help execute such a plan, and work toward a system with a single end, they need to believe wholeheartedly in its result. 

So, for socialism to function, people can't just be forced to work toward a shared goal. The result would be unrest and, eventually, revolution. Instead people need to be absolutely convinced that this plan is the right choice. 

Propaganda and the media play a big role here. For instance, if the planner controls all sources of information, there's no vehicle for opposition to his beliefs or plans, making it impossible to hear counterarguments.

Furthermore, if anybody tries to act out against the plan, they're certain to be silenced. After all, doing so would harm the plan's chances of success and the indoctrination of the masses. 

But silencing opposition requires a common enemy. In fact, an essential aspect of human nature is the difficulty we have agreeing on positive goals. On the other hand, it's quite easy for us to agree on an enemy, an _other_ that _we_ can fight against. That's exactly what the Jews were for Nazi Germany. 

Let's take a closer look at that particular situation: 

The post-World War I economy in Germany was transitioning into a more organized, less competitive one. People were getting used to the control of a central organization and struggled with capitalist economic systems and the classically liberal ideas of countries such as the United Kingdom. 

These struggles became especially common for young people in Germany and it was only a matter of time before Jews were being portrayed as "evil capitalists" out to harm the German economy. Jews rapidly became a common enemy of the German people as they came to represent the atrocities of capitalism and, therefore, classic liberalism.

### 8. After World War II, upholding individualist morals was more important than ever before. 

Even before it came to light how much death and destruction World War II had wrought, one thing became clear: rebuilding Europe and recovering from a genocide of such magnitude was going to be tremendously difficult. But, even in 1944, at the time the author published this work, it was obvious that a critical point in the years following the war would be the elevation of individualist morals over collective ones. 

The author's argument ran as follows:

If the United Kingdom chose collectivism, certain moral values, such as self-reliance, independence and responsibility, would be destroyed. People would blindly obey orders, sticking to what socialists called "The Plan." Furthermore, collectivism would hinder the reconstruction of society, leaving the country crippled and war-torn.

So the author proposed an alternative vision — a competitive and individualist market that would foster the rapid recuperation of the country and return UK standards of living to prewar levels or higher in just a few years. Competition would lead to the production of desperately needed goods and services while simultaneously keeping down prices, all of which would strengthen the economy.

The author also pointed out that the adoption of socialism would greatly impact world affairs. 

At a time when it was important for the United Kingdom to appeal to the Germans by stressing individualist morals and ethics such as freedom and independence, opting for collectivism would have been a major blunder.

What's more, other collectivist states, needing to focus intently on their own economies, would neglect their relationships with other countries.

Moreover, having a planned national economy independent of the world market would result in dramatic economic disparities between countries. This would prompt envy and jealousy, thereby endangering long-term peace. 

In hindsight, we can see that the UK didn't become a socialist nation incapable of overcoming the war, but socialism had a negative impact on other parts of Europe and the world.

### 9. Final Summary 

The key message in this book:

**Socialism will grow into totalitarianism as it affords the state too much control over the country's economy and people. To ensure the personal and economic freedom of citizens, it is better to take a libertarian approach that advocates little governmental control whatsoever.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Wealth of Nations_** **by Adam Smith**

_The Wealth of Nations_ is a profoundly influential work in the study of economics and examines exactly how nations become wealthy. Adam Smith advocates that by allowing individuals to freely pursue their own self-interest in a free market, without government regulation, nations will prosper.
---

### Friedrich August von Hayek

Friedrich August von Hayek (1899–1992) was an Austrian-born, British economist and a co-recipient of the Nobel Memorial Prize in Economics in 1974. He was a pioneer of monetary theory, a major proponent of classic liberalism and would later receive the Presidential Medal of Freedom in 1991.

