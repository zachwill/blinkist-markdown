---
id: 55102237666564000a630000
slug: the-sales-bible-en
published_date: 2015-03-25T00:00:00.000+00:00
author: Jeffrey Gitomer
title: The Sales Bible
subtitle: The Ultimate Sales Resource
main_color: 1B4279
text_color: 012D6A
---

# The Sales Bible

_The Ultimate Sales Resource_

**Jeffrey Gitomer**

Considered one of "Ten Books Every Salesperson Should Own and Read" by the Dale Carnegie Sales Advantage Program, _The_ _Sales Bible_ (1994, revised 2015) is a classic tome of sales strategy. The book takes an indepth look at the sales practices and techniques the author himself mastered to achieve lasting success in sales.

---
### 1. What’s in it for me? Hone your sales skills and sell more. 

What makes a great salesperson? Some people seem to just have the knack, or are so charismatic that they could sell a ketchup popsicle to a woman wearing white gloves — truly a natural talent!

But really, no one is "born" a great salesperson. Becoming one requires the right attitude, good communication skills and solid product knowledge.

Sales guru Jeffrey Gitomer gave the world his unique insight into sales when _The Sales Bible_ was first published some 20 years ago. These blinks cover the revised edition from 2015.

Now more than ever, you'll be able to jump right in and become that great salesperson you've always wanted to be!

In the following blinks, you'll learn

  * how to turn a definite "no" into a confident "yes";

  * why a customer complaint is actually a great sales lead; and

  * what you can learn from test driving a car at an automobile dealership.

### 2. Success in sales starts with a positive attitude. Make it a goal to sell with a smile. 

In his book, _The Strangest Secret_, author Earl Nightingale writes, "We become what we think about."

In other words, our thoughts create our _attitude_, or the system of beliefs that inspire what we say and what we do.

In sales, it's not your quick wit or silver tongue that seals the deal, but _your attitude_.

A number of studies performed across the United States have shown that in fact, a salesperson's poor attitude is the primary factor for failing to sell.

According to one study, having bad attitude accounted for some 50 percent of situations where a sales deal failed. Following this, poor verbal and written communication skills accounted for 20 percent; poor management for 15 percent; and improper training at 15 percent.

These statistics imply that a change in attitude alone can dramatically improve your sales success rate. But how do you turn your sales "frown" upside down?

Changing your attitude is a matter of discipline. Developing a positive attitude must become what moves you — not something you casually think about but commit and aspire to.

One way to make this more tangible is to write down a mission statement that incorporates your goals and ideas, or anything else you feel is important to you and how you work.

Your statement doesn't have to be a novel, either. Something like, "My mission is to sell like crazy, help the people around me and have fun doing it" is potent and to the point.

Don't get too sophisticated; the idea is to give yourself a daily reminder of your aspirations and ideas, something short and catchy that motivates you — not something that puts you to sleep!

> _**"** Most people aren't willing to do the hard work it takes to make selling easy."_

### 3. Friends like to buy from friends. Creating relationships outside the office will help you sell more! 

When you were a child, your parents told you to "be nice and make friends." They weren't just trying to make your life on the playground easier; they were also giving you powerful sales advice.

In sales, friendship is important. The ties of friendship are said to be responsible for around 50 percent of every successful sale!

Friends like to buy from friends. There's an old business adage that goes, "All things being equal, people want to do business with their friends. And all things being _not_ so equal, people _still_ want to do business with their friends."

While many aspiring salespeople think that it is the superiority of their product or service that convinces customers to buy, more often than not successful sales are rooted in a trusting, friendly relationship between buyer and salesperson.

Friendly relationships with buyers come with multiple advantages. For example, you don't need fancy sales techniques to sell to a friend. What's more, you can expect a friend to always give you honest feedback about yourself and your product. And competition is virtually non-existent.

But how can you turn a customer into a trusted friend? It all starts with showing your customer that you care. What does this mean in practice? It means getting out of the office.

Unfortunately, many salespeople consider any interaction with a customer that isn't a sales call to be a waste of time and energy. In the short term this might seem true, but having this attitude means you could lose out on the massive opportunities that having a friendly relationship brings.

So if you can, meet your customers outside of the office. Take them to a concert, a sports event or something that isn't work-related. Show them that you are genuinely interested in the person behind the wallet!

Not only will forming a relationship with buyers increase sales, but it will also make your job more personally fulfilling.

### 4. Channel the “WOW-factor” to distinguish yourself and your product from everyone else. 

There are tons of salespeople out there, all trying to grab the attention of your target customers. So how do you stand out to win a sale? Use the _WOW-factor_!

The WOW-factor is something we've all experienced at one point. It's the difference between a person (or thing) that we can't stop thinking about, and everything else that slips from our mind, forgotten.

Basically, you want to use the WOW-factor to imprint yourself inside a potential customer's mind.

While there are many ways to distinguish yourself and put "wow" into your sales presentation, they all share one common thread: _preparation_.

For example, when the author pitched his book to a publisher in the 1990s, he was prepared. He came with a prototype of his book, complete with a computer disk and wallet-sized flash cards to distinguish his book from all the other books on the shelves at the time.

Furthermore, he came with multiple letters of reference and a multimedia presentation that he had practiced for weeks. What's more, he had already trademarked the name of the book, _The Sales Bible_, as a part of his daring marketing concept.

He knew that he had to put himself in the position of the publisher, whose primary concern was whether the book could sell easily. Keeping this in mind, the author designed his presentation around the book's sales potential — fitting for a book about sales!

Although he had set up meetings with ten publishers for his book, his well-prepared concept and presentation convinced the very first publisher to sign on.

### 5. Use power questions to build rapport and get the answers you need to seal the deal. 

Even novice salespeople understand the importance of knowing how to talk to customers, yet few know how to _listen_ to them.

As a result, salespeople miss out on a huge opportunity to understand their prospects' perspectives, to truly know what they're looking for and why.

The key to understanding your customers' perspective lies in _power questions_, which are concise, open-ended questions that don't force a simple "yes" or "no" out of your customer.

Asking power questions demonstrates to a potential client that their opinions are genuinely valued. Showing them this will encourage them to give you the information you need to seal a deal, such as what their preferences are and why they need to buy.

Too often, salespeople feel it's their job to _tell_ their prospects why they should buy a product or service. In reality, it's far better to let the customer decide what they want.

Here's an example. You're selling printers, and you ask a prospective client what seems a deceptively simple, yet powerful question: "How do you choose a printer?"

Her answer will reveal her preferences. For example, she might answer: "Quality, delivery, and price."

You now have an opportunity to dig a little deeper. You ask: "How do you define quality?"

Once again, by probing with open-ended, neutral questions, your prospect will feel that her opinion is genuinely valued, and will reveal the precise requirements for which she's looking.

After some thinking, she might answer: "A high-dissolving, clear printing," to which you may reply, "I see. So you're looking for something that reflects the quality of your company?"

She's unlikely to answer that question with a "no," at which point you can start to close with something like: "If I could deliver this quality within your required time frame and at a fair price, would you consider doing business with me?"

If she answers "yes," it's time start sealing the deal by finding out how many printers she needs and when you can start implementing.

If the answer is "no," however, then you'll need to consult our next blink.

### 6. A successful salesperson is always able to turn a “no” into a “yes.” 

If you're in sales, then you're going to hear the word "no." But what do you do when this happens?

First, know that "no" is seldom a categorical objection to you or your service. Most of the time, your prospect is simply demonstrating that you haven't convinced him to buy, or that there's another issue he hasn't yet told you about.

It's your job to uncover their real objection, which prospects will often keep hidden from you. They do this for any number of reasons. They might have doubts about the product, or maybe they're trying to get you to sweeten the deal. Or maybe they need third-party approval. Basically, there's no end to the reasons people say "no."

No matter their motivations, however, their "no" will often sound like "I need to think about it," or "we've spent our budget" or "I have to talk with my partner."

If you get to this point, then you've made a mistake. Re-evaluate your situation to see if the prospect is completely qualified to make purchasing decisions. At the same time, determine whether you've established enough rapport to truly understand your client's business needs.

The trick to overcoming these objections lies in your preparation. If you run into objections, then it's very likely that you weren't well prepared, hadn't asked the right questions or listened closely enough.

If you can't figure out what the real problem is, listen closely to the objection and ask for clarification. Ask questions like: "You're telling me X, but I think you mean something else. Is that true?"

If they answer positively, and provide you with a bit more information, confirm this information by rephrasing it, like: "So, in other words, if X wasn't the case, you would buy my service, is that correct?"

At this point, if everything has gone as planned, there is only one thing left to do!

### 7. Don’t be shy; ask for the sale. But then, shut your mouth to give your client the chance to say “yes.” 

There are many books and articles that address closing a sales deal. Why? Because none of your preparation, technique or rapport matters if you don't know how to ask for the sale.

Closing questions should be open-ended, avoiding "yes or no" questions at all costs. If you offer your client the opportunity to say "no," then they just might take it!

Instead, ask questions that assume that the prospect is going to buy. Questions like, "Which color hats would you like to buy, then?" or "When do you want these delivered?" focus on critical elements of a purchase without posing a simple "buy or no-buy" question. This way, it becomes harder for the prospect to actually say "no."

But probably, the most important rule when closing a sale is to _shut up_ after you've asked your closing question. If you keep rambling, then you unwittingly relieve the pressure on the prospect that otherwise forces him or her to make a decision.

An easy way to keep your big mouth shut is to remember this simple maxim: "After a closing question, the first one to speak loses."

If your prospect is still demonstrating uncertainty, it might be time for drastic measures. It's time for the _puppy dog close_.

So what's the easiest way to sell someone a puppy dog? Give it to the prospect's kids overnight (for "testing") and try to get it back the next morning. In nine out of ten cases, you won't get the puppy back no matter how hard you try, because everyone has fallen in love with it.

If you've ever test driven a car, tried a trial membership or gotten free issues of a magazine, then you've seen the puppy dog close in action.

It's no small wonder, either. It's among the most effective selling techniques, and can easily turn a "no" (or a "maybe") into a "yes."

### 8. Great and consistent customer service lets you close sales, again and again. 

Imagine all the work you've put in to win a sale, just to lose it because of an offhand rude comment, a too-slow response to an email or simple indifference.

That mistake didn't just cost you a sale, but it cost you a customer who had the potential to bring you future sales as well!

Whatever you do, don't let success turn into complacency when it comes to customer service.

Customer service is the never-ending pursuit of excellence in exceeding your customer's expectations, thus building loyalty. But how could service be more important than the sale itself?

In essence, customer loyalty is one of the most valuable assets you have, as it is virtually a guarantee for future sales and eliminates competition.

But while your loyal customers may give you trust, they likewise will constantly test and re-evaluate that trust. Don't endanger this valuable asset by underperforming.

In practice, this often means taking responsibility for complaints, even if the problem wasn't your fault.

You can't always prevent bad things from happening, but that doesn't mean people won't come to you with their complaints. Take these complaints and consider them a blessing, as each one has the potential to actually strengthen your relationship with your customer and lead to more sales.

If your customer is upset, it doesn't matter who is at fault. In that moment, all that matters is your customer's perception. He just wants the problem to be solved, and if you're the one to solve it, to him you've become a problem-solver.

If you solve your customer's problem, you've proven yourself to be someone who sticks to their word and doesn't make excuses. This creates a basis for a long-term relationship, and perhaps even a testimonial for your services for future prospects.

### 9. A new type of salesperson is emerging: the non-salesperson. 

By now you'll have realized that the lion's share of what constitutes a good salesperson isn't fancy techniques but solid people skills and problem-solving.

More and more, however, salespeople are realizing that a solid understanding of both the product and the needs of the customer are likewise important and deserve attention.

Consider Clarkson Jones, who worked for a company that specialized in the repair and maintenance of parking lots. His job was to supervise workers and the operation of heavy equipment, _not_ sales. Nevertheless, customers turned to him when they wanted to place orders and discuss projects.

Why do you think customers turned to Jones?

Jones naturally knew his product so well that he could solve any customer problem efficiently and effectively. Plus, he was great with people. Customers preferred talking to him over actual salespeople, as he wasn't by default after more sales. Rather, he offered an honest consultation that was aimed at efficiently solving a customer's problem.

Jones represents a new kind of salesperson: the _non-salesperson_.

These are people who aren't out to manipulate the customer but instead consult with them to find a solution that is actually best for them. This sort of salesperson knows her products inside and out, understanding the products' strengths and weaknesses, and thus knows when and how the customer can best benefit from the product. Ultimately, they won't sell something they know won't work.

Many customers feel threatened by the stereotypical salesperson, someone without scruples who will do anything to get the sale whether or not it makes sense for the customer.

You'll have to overcome this hurdle by demonstrating deep product knowledge and a genuine interest in helping solve your prospect's actual problems.

### 10. Social media is a great way to attract sales and showcase your knowledge about your product. 

Today salespeople have a number of new tools at their disposal to raise awareness of their services and to attract potential customers. However, awareness alone is no sales guarantee. 

Maximizing the effectiveness of your social media presence requires you to do two important things: interconnect all platforms and provide value messages.

The primary reason for using social media — or any marketing platform, for that matter — is to reach as many potential customers as possible. With social media, that means using different platforms and bringing them all together.

Your Facebook fans, for example, are more likely to discover your YouTube videos, find them useful and subscribe to your channel if you post the videos on your Facebook page. This same principle of providing content across platforms can be applied to _all_ social media.

But attracting eyeballs isn't enough. You'll also have to provide _value messages_, that is some sort of product insight or special knowledge, testimonial videos or anything else that gives potential customers a reason to look at your content. Providing value messages means they'll spend more time on your pages and be more likely to share your content with their friends and followers.

However, on social media, you don't have the opportunity to take potential customers out for drinks to build rapport. Instead, your value messages will have to do the heavy lifting.

The author does this by providing his followers with articles or testimonial videos, and not ads or sales offers. The idea here is to demonstrate knowledge and capability while also providing something of value to your followers.

Using this strategy, the author has landed sales ranging from a $20 book to a $50,000 sales training program. The important part is that he uses social media to show and prove his knowledge and capabilities.

And remember: you should provide at least part of your value to your followers for free.

### 11. Final summary 

The key message in this book:

**Succeeding in sales is all about developing the right attitude and fostering strong, friendly relationships based on trust with your customers. By being an honest friend and truly discovering what your customers want and need, you'll have created a life-long revenue stream.**

Actionable advice:

**Next time you receive a customer complaint, smile!**

Customer complaints aren't nuisances that you have to take care of but opportunities for more sales. Every customer complaint is a chance for you to solve another customer need and become a problem solver in your customer's eyes.

**Suggested further reading:** ** _The_** **_Ultimate_** **_Sales_** **_Machine_** **by Chet Holmes**

_The_ _Ultimate_ _Sales_ _Machine_ offers twelve key strategies for improving how we do business, as well as other methods and tools to help you work smarter and more effectively in all aspects of your business, from management to marketing and sales.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jeffrey Gitomer

Jeffrey Gitomer is an award-winning author, professional speaker and business trainer, focusing on sales, customer loyalty and personal development. He has written numerous books, including _Customer Loyalty Concepts_ and _The Little Book of Leadership_.

