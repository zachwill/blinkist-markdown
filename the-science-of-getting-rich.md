---
id: 5a3562feb238e100077286d4
slug: the-science-of-getting-rich-en
published_date: 2017-12-19T00:00:00.000+00:00
author: Wallace D. Wattles
title: The Science of Getting Rich
subtitle: Your Master Key to Success
main_color: 4E8553
text_color: 28522B
---

# The Science of Getting Rich

_Your Master Key to Success_

**Wallace D. Wattles**

_The Science of Getting Rich_ (1910) is a self-help classic, where the author presents his guide for attaining a life of abundance. These blinks explain how to use directed thinking to attract opportunity and opulence while accomplishing anything you want.

---
### 1. What’s in it for me? Open your mind to the magic of getting rich. 

How do people get what they want? How do they even _know_ what they want? Do you?

Even though the specifics may differ — some of us desire to learn a skill, others to start a family and yet others to simply grow and harvest a garden — we still all share an underlying desire for growth and abundance.

Most of the time, our desires and wants seem constrained by resources — time, money, opportunities. But what if someone told you that you could create these things for yourself and that there is an inexhaustible amount of them waiting for you if only you'd learn how to think in the right way? According to the author, Wallace D. Wattles, you can.

Along the way, you'll also learn

  * why competition isn't a viable way to happiness;

  * why cultivating an attitude of gratitude is important; and

  * how all the resources for a happy and fulfilling life are well within your reach.

### 2. To live a full life, you must nurture your mind, body and soul, and this requires material wealth. 

Just about everyone wants to be rich. People who don't have money want to acquire it and people who do have it worry about losing it.

There are those who might try to put this desire for wealth in a bad light, but, actually, it's only natural.

The three core components of your life are your _mind, body_ and _soul_, and to live a successful, happy life, you need to nurture them equally, which requires certain material goods.

For instance, you can't fully celebrate your body if you can't afford good food, comfortable clothing and a safe, cozy place to sleep.

Similarly, you can't enjoy your mind without books, education and other intellectual stimulation.

And, finally, you can't celebrate your soul without love and the means to show love to others. This last one may seem odd, but here's the fact of the matter: love is most often demonstrated by a desire to give material gifts. In other words, someone with nothing to give can't enjoy love or relationships to their fullest potential.

So how can you get rich enough to nurture these three aspects of your life?

Well, first of all, you should know that, contrary to what many think, people don't become rich just because they were born to rich parents or with great talents, but because they behave in a certain way that is conducive to accumulating wealth.

Another common misconception is that not everyone can be rich because there's only finite amount of riches in the world. According to the author, everything in the world is composed of the so-called _Original Substance_. The different elements we see in nature are just different manifestations of it. Luckily for us, the Substance is always regenerating, meaning that our natural resources will never run out.

So with this abundance in mind, it's only natural for humans to want to acquire and experience more things.

This also means that nobody is poor because of any inherent dearth of resources, but rather because they don't understand their power and the inexhaustible riches within their reach. Simply put, you and every other person has the right to be rich because you're a part of nature, which abounds with riches.

### 3. Thoughts are the primary source of creation, but gratitude is what keeps it going. 

What do you think the first step to getting rich is? Is it buying stocks? Starting a company?

Actually, the initial step begins with something much more fundamental: your _thoughts_. Thoughts are everywhere, and according to the author, by impressing your thoughts upon the Original Substance, you can cause the object of your thought to be created.

That's how everything on Earth was created; everything you can perceive in nature, including the sky and the universe, is the product of a thought in the Original Substance. That's why, by imagining something using the power of thought, you can bring about its creation.

As you do so, remember that God wants you to become rich so that you can better help others. You're a creator, not a competitor, which means your goal is to get whatever you want in a way that helps and elevates everybody else as well. For instance, God wants musicians to have the best instruments so that they can create music that will also inspire others to strive for greatness.

This all being said, it's not enough to just believe in Original Substance and hope that it will give you all you need. You also need to live by the _Law of Gratitude_. This is a natural principle that states that the more grateful you are for the Substance, the more you'll attract good things.

So if you're a talented musician but can't afford to buy a new guitar, rather than complaining about it, be grateful for the guitar you already have. Know that, in time, the Original Substance will provide you with greater opportunities and better tools.

> _"The mental attitude of gratitude draws the mind into closer touch with the source from which the blessings come."_

### 4. Focus your mind on what you want and don’t worry about other people. 

Now that you know that you can attain your desires through the power of thoughts, it's time to learn how to think the right way.

To get started, use your imagination to imbue the Substance with a clear and specific picture of what you want and a strong purpose to obtain it. For example, say you want a house. Picture it in all its detail. How big is it? What are the rooms like?

From there, keep this clear picture in your mind, like a sailor who harbors a thought of the port where he intends to anchor.

In addition to the image and purpose, you must also have a powerful faith that you can attain whatever you want, that it is there for you and that you need only reach out and grab it. 

Once you've learned how to think correctly, then you can use your will to maintain this thinking and move forward on your path to riches. But remember: on this journey, the only appropriate way to use your will is upon yourself, not on anybody else.

After all, you can't know what is good for anyone but yourself. Never force someone to work for you, and never take things from others through manipulation. The former is a form of enslavement; the latter, of theft.

If you want that big house, your only route is to imagine living in it until it becomes yours. Ignore those who tell you that you're too poor to afford it and keep the clear image of success in your mind.

### 5. Acting in the present, in harmony with your faith and purpose, will bring about the future you desire. 

Alright, so the power of thought is an incredibly powerful tool for creating your own reality. But action is still required to get things moving. While your thoughts can attract the things you want, only your actions will make them truly yours.

And because you can't act in the past or in the future, the present is the only moment you've got, so you'd better act now. That means, regardless of the circumstances you find yourself in, you've got to act to prepare yourself for the future you want. If you merely invest energy in visualizing your ideal future and do nothing to work toward it, success will never actually materialize.

It's common for people to stall here because they believe their environment isn't right for the action they want to undertake. If you find yourself facing this problem, there's a simple solution: just change your environment or improve it through your own actions. In other words, use your present environment as a tool to arrive at a more hospitable one. 

It's also important to do as much as you can during each and every day, so you make quick progress, but without overworking yourself. Doing your utmost doesn't mean completing a ridiculous number of tasks every day, but instead applying all of your faith and purpose to making progress toward your personal goals.

Many people make the mistake of spending their mental energy in one place and their physical energy in another. But if you concentrate your whole being, mental and physical, on whatever act you engage in, then every action will be successful. Having such successes will then attract further success and, pretty soon, you'll get into a rhythm.

So if you already know that you want to earn more money, don't procrastinate. Start working toward your goals today and believe that you will eventually succeed.

### 6. Using your talents will help you to grow and inspire others to do the same. 

Have you ever been really good at something that you also really like doing? If yes, you know what an incredible feeling it is, and you'd probably agree that every person should strive for such an experience.

Finding the sweet spot where talent and enjoyment intersect will help you perform better, thereby helping you succeed. And what could be better than getting rich while doing what you love?

To get started, develop your skills. For instance, if you have a desire to paint, you must first develop the ability and artistry necessary to succeed.

But whichever field you choose, you must have faith in it to attain _increase_, meaning the kind of advancement that everybody strives for. This desire of increase pervades the universe; every little thing seeks advancement and development. And by having faith in what you do, you'll give the impression of increase, which, in turn, will attract more genuine increase.

Therefore, even if your job is something seemingly unimportant, like merely selling candy to children, you should have unwavering faith in its importance in creating increase, not just for you, but for everyone around you too. When you show this kind of faith, it will make people believe that by connecting with you, they too will achieve increase for themselves.

This is why doctors and teachers, who are often filled with faith in the importance of their work, will never be without work, and this attracts more increase.

Just follow the desire to do what you most love doing and know that you _are_ advancing personally. If you can manage that, there is no end to the success you can enjoy.

> _"Do all that you can do in a perfect manner every day, but do it without haste, worry, or fear. Go as fast as you can, but never hurry."_

### 7. Final summary 

The key message in this book:

**The universe wants nothing more than for you to live an abundant and prosperous life, so you should embrace your natural desire to be rich. Material wealth will allow you to nurture your mind, body and soul, while also inspiring and compelling others to do the same.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Think and Grow Rich_** **by Napoleon Hill**

In _Think and Grow Rich_ (1937), Napoleon Hill investigates the methods of the 500 most successful people of his time, including the world's richest men, top politicians, famous inventors, writers and captains of industry. First published amidst the Great Depression, _Think and Grow Rich_ has sold over 100 million copies.
---

### Wallace D. Wattles

Wallace D. Wattles was a writer and thinker within the New Thought movement, a philosophical approach to life that presumes God is in everything, including all human beings. His other books include _The Science of Being Well_ and _The Science of Being Great_.

