---
id: 56654a2ec58abd000700000c
slug: the-science-of-why-en
published_date: 2015-12-07T00:00:00.000+00:00
author: David Forbes
title: The Science of Why
subtitle: Decoding Human Motivation and Transforming Marketing Strategy
main_color: E9B09B
text_color: 804A36
---

# The Science of Why

_Decoding Human Motivation and Transforming Marketing Strategy_

**David Forbes**

_The Science of Why_ (2015) is an insightful and sometimes surprising guide to a question that only the best marketers can answer: why does a customer buy? Read through these blinks to explore the various types of consumers in the marketplace, and give your own marketing greater depth and perspective.

---
### 1. What’s in it for me? Find out what really makes consumers tick. 

What do you really know about your customers? Sure, you couldn't possible know everyone's shoe size, or how many siblings or children each customer has — but there are other things you really should know. For example, what drives your customers to grab _your_ products off the shelf and not your competitor's? Or, conversely, what makes customers pick your competitor's products over yours?

In _The Science of Why_, the author takes a closer look at just such questions. By presenting the findings of his own scientific research, he opens a window into the minds of consumers, and puts forward the _MindSight Matrix_, a tool to help you see the different factors at play when consumers choose what to buy (or not to buy), what they think and what motivates them — in other words, knowledge you definitely want to have when deciding on a marketing strategy.

In these blinks, you'll discover

  * why it's difficult to sell a Gucci handbag to a punk rocker;

  * the importance of distinguishing between three different consumer motivations; and

  * how to turn _a_ marketing strategy into _a_ _successful_ marketing strategy.

### 2. Use the MindSight Matrix to understand why consumers buy. 

So, you've got great marketing, a fantastic business and an ingenious product idea. But your target market doesn't seem to see it this way — because they just aren't buying your product. What's wrong?

If you're at your wits end, there's a tool that might help: the MindSight Matrix. This problem-solving technique maps out the various motivational forces that drive consumers.

There's one main force that motivates action: a desire for change. Whether an elderly lady buying a new pair of slippers or a young professional joining a gym, all customers are looking for change, and the MindSight Matrix helps pinpoint exactly what kind of change is wanted.

There are three categories of desired change: _expectations, experiences_ and _outcomes_. Expectations are what customers want to change in the future. To attract customers hoping to change their expectations, marketers should focus on giving them a vision of the future where the product has opened up new opportunities for them. Experiences are what customers want to change in the present moment _._ And, finally, outcomes reflect how satisfied customers are with their past choices.

There are also three categories of motivations: _intrapsychic motivations, instrumental motivations_ and _interpersonal motivations_. To identify motivation, we have to specify where we want the change to take place.

Change can either be internal or outward-directed. If you want to change how you feel about or perceive yourself, then you're longing for internal change. If you want to change your outward appearance or physical surroundings, or buy a new product, then you desire outward-directed change.

All internal change is driven by intrapsychic motivations. We'll take a closer look at these in the next blink. 

Outward-directed change can be driven by either instrumental motivations or interpersonal motivations.

Instrumental motivations are what inspire you to buy those Nike sneakers or that Chanel handbag, or to take that vacation to Fiji. Interpersonal motivations, on the other hand, are what move you to align yourself with a particular reference group — be it punks, clubbers, sports fans or activists.

This might seem complicated, but it will become clearer through specific examples in the next blinks. Just remember that there are three different types of change and three different types of motivation — with a total of nine different change-motivation combinations.

> _"According to the Council of American Survey Research Organizations, US companies spent $6.7 billion in efforts to learn more about their consumers in 2013."_

### 3. Things we buy reflect how we want to feel about ourselves. 

Here's a question: have you expressed yourself today? And here's another: have you bought anything today? If you answered yes to the second question, then you can probably answer yes to the first. Many of our purchases are driven by _intrapsychic motivations_ ; simply put, we buy things to say something about ourselves. 

Consumers that respond to intrapsychic motivations value how a product will make them feel about themselves. These types of consumers are notoriously hard to satisfy, because only they know their true personal desires. To attract such consumers, marketers develop campaigns that focus on how a product will make an individual feel. 

For example, a single mother of three is tired of cooking for her kids every night, but wouldn't dream of letting them eat fast food for dinner. How does she want to feel?

Well, she'll want to feel like a good mother, one that helps her kids grow up happy and healthy. But she also wants to feel more relaxed, and have more time to herself. By empathizing with this young mother, marketers can create a campaign for a frozen food that highlights what's important to her: healthy food and stress-free cooking.

### 4. Intrapsychic motivations reflect needs for safety, social recognition or even skill development. 

In the previous blink we learned about intrapsychic motivations. Now let's examine three types of shoppers driven by them.

All of us have one of those really indecisive friends. He or she spends ages deciding what to purchase, then gets cold feet at the last minute and leaves the store empty-handed due to some minor worry regarding the product. If you don't have one of those friends, you might even _be_ that friend yourself. But don't worry! There's no shame in it. 

Consumers like this are _security-oriented_ : they want secure, reliable relationships with brands. They don't want to worry about fluctuating quality or sudden price increases. 

To please these customers, marketers need to give the impression that their product will always provide what is wanted. A baby food brand, for example, wants mothers to know that their food is nourishing, has recyclable packaging and tastes good. Nothing more, nothing less!

Another type of shopper is the _identity-oriented_ consumer. Such consumers seek brands that confer prestige or social status on their owners. These consumers buy based on what they associate with the brand. Think about women who walk around in Burberry trench coats or men who drive Lamborghinis: these brands reflect who these people want to be.

Finally, there are _mastery-oriented_ consumers. They love brands just as much as the identity-oriented ones, but don't buy to get social recognition; they buy for themselves. They want to make purchases that help them be the best they can be at their hobby or passion. This means products that are innovative, high quality or professional grade. 

Picture a passionate home cook who is in the market for a new frying pan. Of course, he won't want just any pan! He'll want the one that will take his crêpe recipe to a whole new level — a long-lasting pan that professional chefs swear by!

### 5. Instrumentally motivated consumers want to make the best purchase, and appreciate being informed. 

While intrapsychic motivations emerge from our internal lives, instrumental motivations are primarily focused on the real world. How do these motivations shape buying behavior? Well, it's all about outcomes.

Instrumentally motivated consumers want to buy the product that is most efficient and most effective. In other words, these are the types of people who conduct painstaking online research before they decide to buy anything. 

These customers will be irritated if you waste their time with a product that doesn't do what it says on the box — and you don't want to be subject to their wrath! They'll make you pay for false advertising, either with damaging online reviews or dramatic in-store outbursts. 

What these consumers appreciate is when marketers explain precisely how their product will help achieve the outcome they want. Ad campaigns that are characterized by honesty, authenticity and simplicity are the winners in this regard. 

Dove's "Real Beauty" campaign has managed to achieve this, but with a twist. This landmark piece of marketing was unlike anything else before it in the beauty industry. 

Rather than implying unreasonable promises to women, like the power of a lipstick to make you look like Kirsten Dunst, Dove made it clear that the women in its ads didn't need to look like actresses. Why? Because these women were already much more beautiful than they ever realized.

### 6. Instrumentally motivated consumers like to be empowered, engaged and challenged. 

So what kind of shoppers are driven by instrumental motivations?

For one, there's the _empowerment-oriented consumer_. They care about achievement, improvement and success, and as a result they won't buy unless they're sure a product will help them perform well. It's the marketer's job to make these consumers feel like they're on the right track. 

Sometimes, it's as simple as using encouraging slogans. Phrases like "We can do it" or "Unlock your potential" will make the customer feel excited about learning new things and improving her range of skills with the aid of your product. This will make her more likely to buy. 

There's also another type of consumer that gets excited about possibilities for improvement, but this type has slightly different needs and wants. _Engagement-oriented consumers_ are thrilled by the freshest innovations and the race to obtain the most advanced product before everyone else. 

Marketers can attract engagement-oriented consumers by stimulating their senses, and that's exactly why marketing for tech products puts new sounds, sensations and experiences on show. It's not just a multisensory experience that engagement-oriented consumers are drawn to, but an experience that they can say they were part of. Taglines like "Don't miss the moment!" play on these consumers' desire to live in the present. 

In contrast, _achievement-oriented consumers_ aren't so easy to entice. These consumers are competitive, and don't want products that merely entertain them; they want to be challenged. 

Marketers should approach these consumers more subtly. Messages such as "Let the results speak for themselves" or "Because you've earned it" suggest that even these discerning consumers can feel proud to own your product.

### 7. Interpersonally motivated consumers buy in order to belong. 

If you'd rather pay at the self-serve checkout than have a friendly chat with the supermarket cashier, then you probably aren't a customer driven by interpersonal motivation. For you, shopping means getting in and out as fast as possible. 

In contrast, interpersonal consumers deeply value the loyal relationships they develop with a store, a product and a seller. They seek a sense of community and inclusion in brands, and therefore shop at stores where they feel their needs are understood. 

Because of this, consumers are more likely to buy from sellers that welcome them, so make your customer feel like a member of your family. If you can manage that, cost and competition won't deter them from your product anymore. 

However, some interpersonally motivated consumers aren't just looking for belonging. They seek something deeper from brand relationships: nurturance. These consumers are often responsible for caring for others, and therefore seek brands that truly care about them. Again, marketers often appeal to young mothers with this in mind. 

Other interpersonally motivated consumers care less about belonging to a brand and more about belonging to a certain subculture or _reference group_ in society. Our reference group is the group of friends and peers that we want to belong to. It's not just certain pastimes and attitudes that mark a reference group, but the use of certain products and services. 

Clubbers, for example, really care about techno music. They're more likely to spend money on records and turntables, while keen cyclists would rather spend that same money on bike gear. The way these consumers relate to their reference group can differ widely. 

Some consumers exhibit _low self-esteem motivations_ when they purchase certain items in order to fit in better or compete with others. _High-esteem_ consumers, on the other hand, want to be role models for their reference group. They pride themselves on their strong values, so taglines like "Set an example" will likely draw them in.

### 8. Final summary 

The key message in this book:

**Consumers have diverse motivations. By recognizing the different types of consumers in the marketplace, marketers can equip themselves to improve brand relationships, increase the reach of advertising campaigns and create a sustainable, loyal customer base.**

Actionable advice:

**Don't be too abstract and technical when creating your company's new marketing strategy.**

The most effective way to reach your customers is to appeal to their strongest emotions — their dominant psychological motivations.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Forbes

David Forbes is the founder of Forbes Consulting, an insight-based marketing consultancy. Through psychological consumer insights, the company has increased the effectiveness of several major brands. Forbes also holds a PhD in clinical and cognitive psychology from Clark University.

