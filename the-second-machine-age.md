---
id: 53cf8c1836306500076a0100
slug: the-second-machine-age-en
published_date: 2014-07-22T00:00:00.000+00:00
author: Erik Brynjolfsson and Andrew McAfee
title: The Second Machine Age
subtitle: Work, Progress, and Prosperity in a Time of Brilliant Technologies
main_color: None
text_color: None
---

# The Second Machine Age

_Work, Progress, and Prosperity in a Time of Brilliant Technologies_

**Erik Brynjolfsson and Andrew McAfee**

_The_ _Second_ _Machine_ _Age_ examines how technological progress is drastically changing our society, and why this development is not necessarily positive. It compares the rapid development of computer technology to the advent of the steam engine, which once catapulted the world into an Industrial Revolution.

---
### 1. What’s in it for me? Find out what skills you need to ensure you won’t be replaced by machines. 

20 years ago, if you were lucky enough to own a cellular phone, you probably needed a suitcase to lug it around.

Ten years ago, you may have had a cellphone but it was probably a pretty dull affair that could only be used for calling or texting people.

These days, your phone is a technological marvel that contains your map, your bank, your video player, your social circle — your life!

We're living through a time of astonishingly rapid technological development, and the pace only accelerates.

In these blinks you'll discover what this development means for society and for regular citizens. It turns out that while the benefits to the world could be huge, the biggest rewards will be reaped by just a few people, while many ordinary people may lose their jobs to new intelligent machines.

You'll find out:

  * what skills you should work on to make yourself irreplaceable,

  * how much time Google saves you every day, and

  * why J K Rowling could be considered a more eminent literary influence than Shakespeare **.**

### 2. Human progress is linked to technological development. 

Have you ever considered how lucky you are to be alive in this day and age? In the modern world most of us enjoy a standard of living far beyond the dreams of our ancestors. Even medieval kings and Roman emperors would be jealous of the lives we lead today!

But where has this progress in quality of life come from? From political development? Philosophical pondering?

Not really — humanity's progress has been driven by technological development.

The power of technology can be seen, for example, in the way the Industrial Revolution transformed civilization: In the mid-eighteenth century, Scottish inventor James Watt developed a steam engine that was far more efficient than its predecessors. This development allowed factories to produce goods using steam-powered machines that outstripped the power and accuracy of human workers — which lowered prices dramatically.

This resulted in a leap forward, and an age we will refer to as the _First_ _Machine_ _Age_.

Why the "first"? Because we are now nearing another age of rapid human progress.

Just as Watt's steam engine revolutionized the use of physical force, the advent of the computer and other digital technologies is now revolutionizing the way we use our mental capacity. Jobs and tasks that require intellect and have previously always been reserved for humans are now being performed by machines. In some cases, the machines are even doing a better job than people.

For example, consider Watson, the computer that played the American TV quiz show _Jeopardy!_ and beat two human champions.

Alternatively, ask yourself who has better skills in spelling and arithmetic: you or the smartphone in your pocket?

As machines increasingly take over intellectual tasks, we are being ushered into the Second Machine Age.

### 3. We are entering an era when computers will advance at fantastic speed. 

Legend has it that when chess was invented, the ruling emperor was so pleased with the game that he told the inventor to name his reward. After some thought, the man asked for some rice: one grain on the first square of the chessboard, two on the next square, four on the next and so on.

How much rice do you think he had when all 64 squares were covered?

More than 18 quintillion (ten to the power of 18) grains of rice — a pile that would dwarf Mount Everest!

This story illustrates the power of _exponential_ _growth,_ where the rate of growth accelerates over time. And it just so happens that technological progress also follows this pattern.

This can be seen in the famous _Moore's_ _Law_, according to which the power of a computer chip will double roughly every one to two years. It hasn't failed since it was proposed in the mid-sixties.

We are now at a point where this exponential growth in technological progress is taking us to completely new areas. To illustrate this, consider that in the chess story, after proceeding through the first half of the board, the inventor had reached four billion grains — a lot of rice, but by no means an inconceivable amount. It was only in the second half of the board that the amount of rice grew to absurd proportions.

This is what is happening with technology today; we have entered the second half of the chessboard.

Just look at your smartphone, for example: It can obey your voice commands, pinpoint your location on a map and allow you to access the sum of the world's knowledge online. It could even function as a seismograph or a heart monitor if you download the right app.

This kind of technology was inconceivable just a few years ago, illustrating just how mind-bogglingly fast progress has become.

### 4. The Second Machine Age will produce masses of data, and its analysis will produce great benefits for society. 

Anyone who's ever surfed the internet knows that there is a vast amount of information available. But you may nevertheless be surprised at just how vast it is.

In 2012, there were 2.7 sextillion (ten to the power of 21) bytes of data online. If the amount keeps growing at the current rate, the metric system may soon run out of orders of magnitude big enough to describe it.

Luckily, the accumulation of such enormous amounts of data in digital form has a variety of benefits for society and the economy.

This is because any data in digital form — whether documents, music, tweets, etc. — is very easy to share, copy and, most crucially, analyze. This analysis can in turn produce new insights into a broad range of fields.

For example, Google has digitized over 20 million books, and has thereby given scientists a whole new wealth of data for examining the development of language.

As a result, researchers discovered several interesting trends. Did you know that between 1950 and 2000, the number of English words increased by more than 70 percent?

Another surprising finding was that in the first half of the twentieth century, overall interest in the science of evolution declined until Francis Crick and James Watson discovered the structure of DNA in 1953.

But the benefits of all this data go far beyond mere fascinating findings: when new data are added to old ideas, innovation follows. This is called _recombining_, and a prime example is Google's self-driving car. After all, the car itself is a very old invention, but by adding sensors, cameras and a navigation system, the car can use new geographical data to drive itself.

### 5. In the Second Machine Age, GDP is no longer a good metric for productivity. 

How do you measure the productivity of a society?

For several decades, the generally accepted answer has been to use _Gross_ _Domestic_ _Product_ _(GDP)_ : the total economic output of a country measured in terms of the market price of the goods and services it produces. The idea is that the more a country produces and sells, the healthier its economy must be.

Yet as we enter the Second Machine Age, GDP is becoming a hopelessly outdated measure.

Why?

Because it concentrates only on monetary value, and many of the great innovations in the digital and technological realm cannot be quantified in those terms.

For example, how would you calculate the monetary value of all the knowledge on Wikipedia, which is free for all to browse? Clearly it contributes to the productivity of workers as they can rapidly research new topics, but yet this boost in productivity is not captured by GDP.

Or consider a study that found that when an employee wants to research something, she saves an average of 15 minutes by searching via Google rather than using an encyclopedia or going to a library. And over the course of a year, all this saved time adds up to a substantial benefit to the employer: $500 per employee. And yet this productivity boost isn't recognized in any GDP calculation either.

It seems obvious that we need to define a broader measure of productivity, which can capture the benefits brought by new innovations.

> **Fact:  

** In all the hours people spend looking at Facebook each day added up, you could build the Panama canal.

### 6. The Second Machine Age has brought wealth for some while sapping the income of others. 

Many recent technological innovations have become household names, and the companies that produced them have met with enormous success.

Consider the incredibly successful photography app Instagram. In just 15 months it went from a small start-up with 15 employees to having over 16 billion photos shared on its service. Eventually, Facebook bought the company for over one billion dollars.

The incredible part is that similar success stories are a dime a dozen in the digital realm.

Yet obviously not everyone is benefiting from the Second Machine Age. For example, think of the thousands of people who worked for Kodak, the photography company that filed for bankruptcy in 2012. They lost their jobs because their expertise in analog photography became irrelevant as people turned increasingly to digital.

In fact, despite the huge increases in wealth that technology has brought some people, in little over a decade the average wage of an American has declined by ten percent. Why? Because computer technology is replacing labor so people are losing their jobs.

In this dynamic, the biggest losers are the people whose jobs involve following routines. And this does not necessarily mean manual work like operating machinery or sewing clothes, but any role where tasks are repeated. Professions like accounting and stock trading are also at risk of being replaced by computer technology.

Meanwhile, people with non-routine jobs such as hairdressing and teaching are on more solid ground: it is harder for computers to perform the variable, mostly non-repetitive work they do.

### 7. The Second Machine Age has brought a “winner takes all” economy. 

As you saw in the previous blink, the fruits of the Second Machine Age are not distributed evenly. In fact, we can clearly see an immensely wealthy elite forming at the top of society: in the United States the highest-earning 0.01 percent of society earns a whopping 20 percent of all income.

Now, you probably think that most people in this group are Wall Street bankers and CEOs whose massive bonuses you have read about in the newspapers. They do indeed belong in the mega-earners group, but you may be surprised to find out it also comprises writers, entertainers, and computer programmers.

This is because the internet has allowed the most successful individuals in any given field to become far wealthier than they ever could have been before.

Consider the _Harry_ _Potter_ author J K Rowling. She became a billionaire by writing children's books because the internet gave her _leverage_.

To understand this, let's compare her to another literary great, William Shakespeare.

As popular as Shakespeare was in his day, his reach was limited by the fact that only a few thousand people could fit into a theater to watch his plays.

But thanks to the internet, Rowling's stories are known by billions of people, all of whom are potential customers for books, toys, films and other Harry Potter-related products.

Yet Rowling's scale of success cannot be enjoyed by everyone. In Shakespeare's day, there was plenty of room in the market for other playwrights who could write plays that found their own audiences. But these days Rowling has captured huge swathes of the market, leaving other children's authors struggling to find audiences for their work. Their loss is her gain.

This dynamic is known as _the_ _winner_ _takes_ _all_ _economy_, and it explains why only a few people reap rewards — and they are phenomenal rewards — from the Second Machine Age.

### 8. People should focus on developing their creative skills –  this is where machines lack ability. 

Now you know that the Second Machine Age will not bring prosperity to all, but instead heap riches upon a few lucky individuals while others lose their jobs to machines.

So what exactly does the future of human labor look like?

Well, machines do outperform us in many areas — for example, in analyzing specific data sets. This strength can be seen in competitive chess, where a computer called Deep Blue was able to beat the world champion Garry Kasparov in 1997.

Happily enough, it turns out there are still plenty of jobs that we humans can do expertly, but that a machine would struggle to do.

Machines are bad at innovating, or coming up with something new — they suffer from poor _ideation_. So, for example, while you can program a computer to write a poem with verses that rhyme, it cannot write poems that possess the imagery, power or creativity of human efforts.

So what's the lesson humanity can learn from this?

We should try to build our skills in these creative areas where machines can't compete with us.

Looking at the education system, this means we should focus less on the traditional three Rs (Reading, wRiting and aRithmetic) and instead encourage children to explore their creative sides.

One promising alternative education method is the Montessori technique, where kids are encouraged to play and explore independently. In fact, several successful people in the field of technology have had a Montessori education, including the founder of Amazon, Jeff Bezos, and the founder of Wikipedia, Jimmy Wales. This seems to suggest that the Montessori method fosters the kind of skills necessary for the Second Machine Age.

> _"There's never been a better time to be a worker with special skills or the right education . . . However, there has never been a worse time to be a worker with ordinary skills and abilities."_

### 9. Final summary 

The key message in this book:

**Rapidly** **accelerating** **technological** **progress** **brings** **many** **benefits,** **like** **ever** **cheaper** **and** **better** **goods** **and** **services.** **But** **the** **flipside** **is** **that** **the** **livelihoods** **of** **many** **people** **are** **threatened.**

Actionable advice:

**Get** **an** **education**

In the past if you wanted a good job, the bare minimum requirement was a high school diploma. But these days the bar has risen so that you absolutely need a college degree — so stay in school.

**Use** **technology** **to** **your** **advantage**

Yes, machines may already be threatening your job, but you can still use technology to teach yourself up-to-date, useful skills that will help you get a new career. Leverage the wealth of information online!
---

### Erik Brynjolfsson and Andrew McAfee

Erik Brynjolfsson is the director of the Massachusetts Institute of Technology's Center for Digital Business, while Andrew McAfee is its principal research scientist.

