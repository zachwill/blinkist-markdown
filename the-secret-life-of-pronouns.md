---
id: 56dd866324b39b0007000001
slug: the-secret-life-of-pronouns-en
published_date: 2016-03-07T00:00:00.000+00:00
author: James W. Pennebaker
title: The Secret Life of Pronouns
subtitle: What Our Words Say About Us
main_color: F39F57
text_color: A6662F
---

# The Secret Life of Pronouns

_What Our Words Say About Us_

**James W. Pennebaker**

_The Secret Life of Pronouns_ (2011) shines a light on the everyday language that we seldom pay attention to, revealing the ways in which it serves as a window into our personality and our social connections.

---
### 1. What’s in it for me? Learn about the important messages transmitted by seemingly innocuous words. 

When describing your inner and outer world, you would think that words conveying the most content would easily reveal the kind of personality you have and how you're feeling. But did you know that even more telling than nouns and adjectives are those small words in your everyday language that you rarely even pay attention to?

In these blinks, you'll learn why the pronouns, prepositions and articles we use — such as _I, she, it, the, to, but, for_ and so on — are the words that, surprisingly, say the most about our emotional state, credibility, sex and social status. These seemingly insignificant words actually function like a sort of psychic fingerprint; they can even predict how well groups collaborate or how well a couple is doing!

In these blinks, you'll also discover

  * what pronouns say about mental health;

  * whether it's men or women who use more I-words; and

  * how pronouns can detect lies.

### 2. One of the first language analysis computer programs showed that analyzing language style yields great insights. 

It was over 100,000 years ago that humans first began communicating through spoken language, and some 95,000 years later, they began writing as well. In just the last 150 years, we've developed everything from the telegraph to the telephone, television to e-mail, text messages and social media, all to facilitate communication. Indeed, language is a crucial aspect of what makes us human. But what does the way we use language say about us?

To answer this question, the author created one of the first computer programs for language analysis.

In the 1980s the author became interested in finding out whether people who had experienced serious trauma could improve their mental health by putting their experiences to paper.

He and his research team needed a way to analyze the essays that his patients produced, so they created a computer program called _Linguistic Inquiry and Word Count_, or LIWC.

The idea was to tally all the words in these essays that related to specific psychological concepts. For example, the program might find words that relate to anger, such as _hate_, _rage_, _kill_ or _revenge_, and then tally them up.

Finally, the program would calculate the percentage of words associated with different psychological states. The findings revealed that those who used more positive words, such as _love_, _care_ and _happy_, were the ones whose mental health improved.

Interestingly, this program revealed much more than just the patients' mental well-being.

In the 1990s, one of the author's graduate students had an idea: what if they analyzed the essays differently?

Instead of focusing on nouns, verbs and adjectives to analyze the content of the essays, they could instead focus on the words that revealed the subject's writing style. Those words include pronouns, prepositions and articles.

The subsequent analysis of these words led to surprising, and even bizarre, findings. For instance, the author found that the more people switched between I-words such as _I_, _me_, _my_ and other pronouns such as _we_, _you_, _she_ and _they_, the more their health improved.

> _"Often, the most revealing words that we use are the shortest and most forgettable."_

### 3. Style words are easy to overlook, but nonetheless provide a window into our social skills. 

Words are quite diverse. Some are used to convey informational content and meaning, while others serve a more subtle function as grammatical support.

Generally, words can be divided into two categories: _content_ words and _style_ (or function) words.

Content words include nouns (like, _table_, _uncle_, _justice_, _Peter_, and so on), verbs (such as _to love_, _to walk_, _to hide_ ), adjectives (like _blue_, _fast_, _amazing_ ) and adverbs (for example, _sadly_, _happily_ ). Content words have a culturally shared meaning with respect to an object or action, and are necessary to convey informational content or ideas.

Function words include pronouns (such as _I_, _she_, _it_ ), articles (such as _a_, _an_, _the_ ), prepositions (such as _up_, _with_, _in_, _for_ ), negations (like _no_, _not_, _never_ ) and conjunctions (like _and_, _but_, _because_ ). Function words connect, shape and organize content words, but are rather useless on their own.

If you analyze texts, you'll soon notice that function words make up the greater part of our language. The high volume of function words can be hard to spot, as our brains naturally gloss over them, focusing instead on content-related words.

While style words are easily overlooked, they can reveal quite a bit about our social abilities.

The reason for this has to do with the layout of the brain. Processing function words involves the _Broca's area_ in the brain, located in the frontal lobe, which is also responsible for controlling a number of social skills.

Numerous studies have shown that the frontal lobe is associated with the ability to express and conceal emotions, as well as to read other people's facial expressions. In this sense, function words are linked to our social abilities and can therefore reveal how we see our social surroundings.

> _"Ironically, the quiet words can say more about a person than the meaningful ones."_

### 4. People use different words depending on their gender and how self-focused they are. 

If you were to ask most people about the differences in the way men and women use language, they'd likely tell you that men use more I-words, such as _I_, _me_ and _my_, and that women use more positive emotion words, like _love_, _fun_ or _good_. But do men and women really use words that differently?

Interestingly enough, they do. However, the difference has nothing to do with their use of positive emotion words.

In fact, it's _women_ — not men — who use more I-words. Indeed, in conversations, blogs and speeches, women use first-person singular pronouns, or I-words, at a much higher frequency than men. But why?

Research suggests that women are generally more self-aware and self-focused than men, which is then reflected in their use of I-words.

Moreover, women use social words, such as _they_, _friend_ or _parent_, which relate to other human beings, as well as cognitive words, such as _think_, _reason_ or _believe_, which reflect different ways of thinking, at a higher frequency than men.

In contrast, men generally use articles ( _a_, _an_, _the_ ) more than women, since men talk about specific objects (the broken carburetor, a steak, etc.) more than women do, and specific nouns require articles.

One way to explain these differences could be that girls and boys are socialized differently.

But in addition to gender, our mindset can also influence the words we use.

The author analyzed the diaries of a transgender person who was undergoing hormone therapy, meaning he received testosterone injections every few weeks. Immediately following the injections, the patient's use of I-words increased. This was probably because he was more keenly aware of himself and of the effects of the hormones. But as the effects wore off, he became less self-focused, and a concurrent change appeared in his diaries: he began to use more social pronouns ( _we_, _us_, _he_, _she_, _they_ ).

> The author found that both Romeo and Juliet in Shakespeare's _Romeo and Juliet_ talk like men.

### 5. We can infer people’s way of thinking by analyzing their writing style. 

Words are a fundamental means of human communication. As it turns out, the way people use words, whether it's in e-mails, conversations, blogs or professional writing, can tell us a lot more about a person than just the ideas they are trying to convey.

The author has identified three different ways of thinking that correlate to three different styles of writing.

He did this by conducting an experiment in which participants were asked to write stream-of-consciousness essays, writing down whatever came to mind in the moment. He was then able to discern three unique styles of writing based on these essays.

The first writing style relates to _formal thinking_, which can be considered the opposite of spontaneity. Formal thinkers often appear rigid, and sometimes humorless or arrogant. Their writing is characterized by big words, as well as a high frequency of articles, nouns, numbers and prepositions.

The second writing style relates to _analytic thinking_. People who fit this category strive to understand their world and make clear distinctions. Analytic writing is characterized by exclusives (such as _but_, _without_ or _except_ ), negations (like _no_, _not_ or _never_ ) and causal words (such as _because_, _reason_ or _effect_ ).

The final writing style relates to _narrative thinking_. People who use this style are natural storytellers who just like to talk. Narrative writing is characterized by pronouns of all types, past tense verbs and conjunctions (especially _with_, _and_ or _together_ ).

By categorizing writing styles in this way, we can better infer how people think, how they are likely to organize their worlds and how they relate to other people.

The author also found that descriptions of seemingly irrelevant objects can reveal a person's way of thinking. For example, in another experiment, the author asked participants to describe an ordinary bottle of water. Surprisingly, even the descriptions of something so mundane clearly revealed the participants' unique ways of thinking.

### 6. Language usage can reveal both a person’s emotional state as well as the likelihood of deception. 

For most people, discerning people's emotions from their language seems intuitive. Unfortunately, it's not always that simple, as many people don't intentionally convey their feelings through language at all.

However, we _can_ detect a person's emotional state by examining the pronouns they use.

Consider the case of Rudolph Giuliani, mayor of New York City from 1994 to 2001. Giuliani was often referred to in the media as an angry and self-righteous man, but after he was diagnosed with cancer in 2000, the press noticed that he had become more humble and warm.

The author wanted to know whether Giuliani's pronoun usage changed over the course of his emotional upheaval, so he analyzed the language used in Giuliani's press conferences.

The most striking difference was the dramatic increase of I-words compared to press releases given prior to Giuliani's cancer diagnosis. This makes sense, as people who are in great emotional or physical pain focus inwardly and tend to use I-words at higher frequencies.

Angry people, in contrast, shift the focus away from themselves and onto others. They use the second-person ( _you_ ) and third-person ( _he_, _she_, _they_ ) pronouns at a much higher frequency.

Not only can language usage reveal people's emotions, it can also be used to detect deception.

The author demonstrated this in an experiment in which participants produced essays on both real and imagined traumatic experiences. When reading the essays, it was nearly impossible to discern which essays were real and which were made up.

A computer analysis, however, revealed striking differences.

Firstly, participants who wrote about their own experiences used a greater variety of words, and were able to describe the vivid details of their experience.

Similarly, they also used first-person singular pronouns ( _I_, _me_, _my_ ) more frequently than those writing about imaginary traumas. I-words signal that the speaker is paying more attention to herself, and participants writing about their own traumatic experience were more aware of their feelings.

Knowing this, telling the fake essays apart from the real ones becomes much easier.

> _"Self-attention provokes honesty. I-words simply reflect self-attention."_

### 7. Language usage says a lot about a person’s social status. 

As you've learned, the words you use can tell us a lot about the way you think. Interestingly, these same words can be used to figure out your rung on the social ladder. As with different types of thinking, function words are the biggest indicator of who's who in a given society.

In the case of social status, pronouns are crucial, and their use can be divided into three categories.

First, people with a higher status use first-person pronouns _less_ than lower-status individuals. So, when high-status people talk to lower-status people they use the words _I_, _me_ and _my_ much less frequently.

Second, people with a high status use first-person plural pronouns ( _we_, _us_, _our_ ) significantly more than low-status people.

Finally, people with a higher status are more likely to use second-person pronouns ( _you_, _your_ ) in both written and spoken conversation.

So what is it that causes our use of pronouns to change with social status? The answer has to do with where we focus our attention.

Research has found that, in face-to-face conversation, high-status people tend to look at their audience when speaking, but look away when they're the ones doing the listening. Lower-status people, on the other hand, do the opposite — they look away while they speak.

But where exactly are they looking? In all likelihood, they're looking _inward_, and therefore use I-words more often. This makes sense, as I-words reflect attention to oneself, while you-words and we-words reflect attention toward the audience.

We can see these indicators of status manifested when examining former US president Richard Nixon's language during the Watergate scandal, a major American political scandal that ultimately resulted in Nixon's resignation.

Part of this scandal revolved around the voice recording system that Nixon had secretly installed in his office in the White House. These tapes allowed the author to analyze the private conversations between Nixon and some of his political advisors.

Unsurprisingly, Nixon used far fewer I-words than his political advisors. But when the Watergate scandal unfolded and Nixon's status began to erode, his use of I-words increased.

> "People who use the word _I_ at high rates are focusing on themselves. Those using _you_ are looking at or thinking about their audience."

### 8. Language style matching indicates whether two people pay attention to each other and whether they are compatible. 

Have you ever seen a film that was so compelling that you found yourself behaving and talking like one of the characters afterwards? If you recognize this experience from a film or even a book, then it was probably the character you found most fascinating or enchanting, and thus paid most attention to. But why would we imitate a fictional character like this?

Well, people tend to converge in the ways they communicate when they pay attention to each other.

The idea that people mimic the nonverbal behaviors of people they pay close attention to face-to-face is not news to social scientists; the degree to which people mimic each other reflects how much attention they are paying to their conversation partner.

So it doesn't come as a surprise that this also happens with speech. Just as with nonverbal language, the more two people are engaged with one another, the more their usage of function words — and specifically their usage of pronouns — matches, in a process called _language style matching_, or LSM.

Amazingly, LSM is such a strong indicator of interest that you can even measure it to determine whether two people are compatible for a romantic relationship!

To understand how, consider this experiment in which the author analyzed the conversations of potential couples during a speed-dating event. The couples who showed above-average LSM were nearly twice as likely to want to meet again than those who didn't.

In fact, 77 percent of the couples with the high LSM scores were still dating three months after the initial speed-date, compared to only 52 percent of the couples with low scores.

So, keep this in mind the next time you're out on a blind date!

> _"Conversations are like dances. Two people effortlessly move in step with one another, each usually anticipating the other person's next move."_

### 9. We-words signal group identification and pronoun use predicts group performance. 

What makes for good teamwork? Well, the formula is very simple: like couples, members of successful teams converge in their language use — that is, in their use of function words. And high rates of we-words are often a good indicator of success. Why is that?

When group members use we-words, they are signaling that they identify with their group.

There is an interesting experiment by psychologist Robert Cialdini from the 1970s that illustrates this concept quite elegantly. In the experiment, researchers interviewed students of universities with top-ranked football teams, asking them about the outcome of the previous football games.

If the student's team had won, they usually answered with "_we_ won." However, if their team lost, the answer was usually "_they_ lost." So why the difference?

Basically, we want to be close to groups that are successful and identify with them. We also want to distance ourselves from groups that fail, and thus make the appropriate adjustment in our language.

A similar pattern can be found in the language of couples. Researchers have found that, in general, the more a couple uses we-words ( _we_, _our_, _us_ ) when being interviewed about their relationship, the better it bodes for their relationship. Conversely, conversations with lots of you-words ( _you_, _your_, _yourself_ ) tend to indicate toxic relationships.

Furthermore, the ways group members use function words with one another can predict the group's performance.

While we-words signal group identity, these words alone can't predict how well a group will actually work with one another. However, language style matching between members of a group, which you learned about in the previous blink, _can_.

Take Wikipedia, for example, an encyclopedia project based on hugely collaborative work. One of the author's students analyzed the group discussions among the editors of various articles, and discovered that teams of Wikipedia editors whose language showed similar use of function words produce better, more authoritative articles.

> We-words may save your life. Language analysis of flight crews shows that the more a crew uses we-words, the fewer errors it makes.

### 10. Final summary 

The key message in this book:

**Our use of language — even the most mundane words that we would never give a second thought — reveals a lot about us, from how we think and what we think of our peers, to how we fit into social hierarchies.**

Actionable advice:

**Discover your own status through your correspondence.**

There's a simple experiment you can conduct to figure out where you fit on the social ladder. Look at the last ten e-mails that you've sent to someone, and then compare them with the last ten they sent you. Calculate the percentage of I-words (and you-words, we-words if you have time) you used. Generally speaking, the person who uses fewer I-words ranks higher in the social hierarchy.

**Got feedback?**

**We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!**

**Suggested further reading:** ** _The Sense of Style_** **by Steven Pinker**

_The Sense of Style_ (2014) offers a refreshing and relevant guide to writing potent, readable texts of all kinds. Instead of extolling the same confusing and sometimes counter-intuitive rules found in traditional style guides, _The Sense of Style_ offers simple tricks and heuristics guaranteed to improve your writing.
---

### James W. Pennebaker

James W. Pennebaker is the chair of the Psychology department at the University of Texas at Austin, and is author of _Writing to Heal_ and _Opening Up_.

