---
id: 56c1de12587c820007000063
slug: the-secret-life-of-sleep-en
published_date: 2016-02-19T00:00:00.000+00:00
author: Kat Duff
title: The Secret Life of Sleep
subtitle: An exploration of what happens after we close our eyes
main_color: C4A398
text_color: 785B51
---

# The Secret Life of Sleep

_An exploration of what happens after we close our eyes_

**Kat Duff**

_The Secret Life of Sleep_ (2014) takes an enlightening look at what exactly sleep is. Using cutting-edge scientific research and examples from cultures around the world, Kat Duff explores why and how we sleep, and what makes some Western sleeping patterns particularly unhealthy.

---
### 1. What’s in it for me? Sleep your way to a better life. 

Think of the last time you had a good night's sleep. It felt good, didn't it? But have you ever wondered _why_ you feel so good after a night of deep sleep? 

Recent advances in neuroscience have shed new light on this very question; we now know that sleep not only replenishes your body, but refreshes your mind, improves your cognitive functions and makes you less vulnerable to depression and obesity.

These blinks will show you how sleep actually works, how technological and cultural changes have destroyed our sleep and what we can learn from other cultures' attitudes toward sleep.

In these blinks, you'll learn

  * why a majority of Americans prefer a good night's sleep to sex;

  * why insomnia can make you fat; and

  * the difference between a "big" and a "little" dream.

### 2. Humans have a sleep switch that makes us fall asleep, bringing inspiration and insight into our lives. 

We're all familiar with that murky yet calm feeling we get as we slip into dreamland, but how do we actually fall asleep? Some say the Sandman comes and sprinkles sand into your eyes, the Blackfoot Indians believe a butterfly visits to help you drift off and the ancient Greeks believed in Hypnos, the god of sleep. But what can science tell us about sleeping? 

Well, a _sleep switch_ in humans has been discovered.

In 2001, Harvard University sleep researcher Clifford B. Saper identified a group of neurons in the hypothalamus — the brain region that controls metabolic processes and activities such as hunger and body temperature — called the sleep switch. 

Our bodies have a mechanism to chemically regulate sleep. It functions independently of day and night and maintains internal balance by making you sleep more or less, thanks to a chemical called adenosine triphosphate (ATP). ATP depletes over the course of the day, making us increasingly tired until we fall asleep. Then, during our sleep, other chemicals gradually accumulate until they reach a tipping point, causing us to wake up.

The state when these neurons are shutting down and we begin to fall asleep is known as _hypnagogia_, and its parallel transitional state when we wake up is called _hypnopompia_.

During these transitional states, we often gain insight or feel inspired. We may also relive daytime events, see geometric patterns in our vision, feel like we're falling or floating, or think we're expanding and contracting in size.

As French philosopher Jean-Luc Nancy put it, it's at these times that we "coincide with the world" and have a chance to access other dimensions or realities. Salvador Dalí famously made use of this in his paintings, which were inspired by his dreams.

Thomas Edison also used these transitional states to solve problems by taking naps while holding steel balls in his hands. As he drifted off, his hands would relax and the balls would fall and wake him up, hopefully with a solution already in mind!

### 3. Culture affects when, where and how we sleep. 

We all eventually succumb to sleep. Studies have shown that even the chronic insomniacs among us experience _microsleep_, small bursts of sleep lasting from just under a second to 30 seconds. We all sleep in different ways and culture is a critical factor in determining how we do it.

In the Western post-industrial world, people tend to have a sleep routine, do it in individual rooms and for one chunk of time. These habits are all designed to accommodate the demands of work and school and fit into standardized time.

In contrast, non-Westerners often have more fluid sleeping patterns, sleep in bouts and engage in _co-sleeping_, the practice of children sleeping within an arm's reach of family members. 

In Medieval Europe, sleep was often divided into two, with one hour in between sleeping sessions for drinking tea or having sex. These days, people in warmer climates, like in Spain, prefer an afternoon siesta in addition to sleeping at night, while co-sleeping remains a normal practice in Asia, Africa, the Americas, Southern Europe and parts of Scandinavia. 

Though sometimes viewed as odd by people who don't live in these places, some believe co-sleeping has a positive effect on adult behavior. Mayan mothers say it makes their children feel connected to others and improves their ability to understand and learn from them.

For Anglo-Saxons, co-sleeping mostly died out around the mid-1800s, as Americans were taught that children should sleep in separate rooms to develop their independence. However, leaving babies to cry can make them hypersensitive and hurt their resilience.

Moreover, studies across cultures have found that children who share a bedroom with their parents between birth and five years of age are likely to be happier, more cooperative, more confident and more independent when they grow older than children who sleep alone.

> _"While biology necessitates sleep, culture defines its shape and length, character and conditions."_

### 4. Sleep largely consists of two forms: REM and SW. 

You do it every night, but have you ever stopped to think what sleep really is?

Scientifically speaking, sleep is a fluctuating state consisting of two main forms — rapid eye movement (REM) and slow-wave (SW) — which alternate around five to six times a night, on average every 90 minutes.

Both states are characterized by two features: diminished responses to outer stimuli and the ability to wake up. These features differentiate them from other states, such as a coma. 

REM sleep is named after the way our eyes move under our eyelids when we sleep. It's the most active stage of sleep, and it literally heats up the brain, requiring other stages of sleep to cool it down. In REM, our dreams are vivid, plentiful and easy to recall.

We also use REM to learn, as neuroscientists such as Antti Revonsuo and Patrick McNamara have found. Revonsuo views REM dreams, such as fleeing from tsunamis, as a method of rehearsing for and refining survival strategies; meanwhile, McNamara sees REM dreams as counterfactual versions of past events, which can be interpreted as a learning process.

SW, on the other hand, is a deep sleep typified by slow, high-amplitude, synchronized brain waves that are hard to wake up from. SW is our most restorative form of sleep, and it also burns fat, which explains why not sleeping enough can lead to obesity. It's likely our most necessary form of sleep, and while immersed in it, our dreams are fragmented and vague.

As those around the age of 50 and over know, the amount of SW sleep we get decreases over time. In fact, up to a quarter of 50-year-olds experience no SW sleep at all! This can be a factor in typical characteristics of aging, such as less muscle tone, decreased physical strength, higher body fat, thinning skin, tiredness, lower libido, memory loss and immune malfunction. 

So, it's a good idea to value your sleep. As the Irish say, sleep is better than medicine!

### 5. Though we know sleep is good for us, insomnia is more prevalent and problematic than ever. 

The French call it "dorveille," while to others it's simply insomnia. Sadly, having trouble sleeping at night is a common ailment. According to the National Science Foundation's 2008 Sleep in America Poll, 35 percent of Americans report that they wake up in the middle of the night three or more times a week. The majority of Americans would even prefer getting a good night's sleep to having sex! 

As well as making us feel dreadful, sleep deprivation is also bad for our brain and body. In a sleep-deprived state, your performance suffers, your immunity is compromised and your stress hormones creep up. Your ability to learn, evaluate situations and respond to stimuli are also diminished.

Even more worryingly, studies have found that a week of sleeping only four to five hours a night amounts to a cognitive impairment equivalent to a blood alcohol level of one percent!

So what can you do? 

Try dividing your sleep into two parts. In the 1990s, psychiatrist Thomas A. Wehr carried out an experiment in which eight volunteers spent 14 hours in the dark every night for a month. Gradually, they began to settle into nine-hour sleeps spread over 12 hours, waking up for a few hours between bouts. 

When the study ended, participants longed for this peaceful waking time. Participants also experienced elevated levels of the calming, satisfying hormones melatonin and prolactin, which also support nerve cell growth; these levels fell after the experiment. 

Our own attitudes can also help alleviate insomnia. Modern societies tend to view money, success and achievements as more valuable than getting a restful sleep; a shift in perspective could lead to a much healthier sleep cycle.

In addition to our societal attitudes, artificial light affects us, too. Our bodies are wired to regulate our sleep cycles with the rising and setting of the sun. The Dracula of hormones, melatonin, is only produced at night and is crucial for inducing sleep. Looking at your computer screen until 2:00 a.m. disrupts this natural process.

### 6. The divide between interwoven states of sleeping and waking is cultural. 

Have you ever woken up expecting to see what you were dreaming about in front of your eyes, or later wondered if that conversation with your friend really happened or if you just imagined it? Upon waking up, it's often hard to distinguish your dreams from reality.

Interestingly, every culture has a way of interpreting this division. Central American Mayans, for instance, speak of waking and sleeping as walking with a foot in each world; the Hindu Upanishads understand sleep as more real than our waking life, and African traditions see sleep as a way to communicate with the dead and receive advice. 

We all dream, and some of us even experience hallucinations, syncopes or loss of consciousness. But distinguishing between these states and our waking state isn't so clear-cut. Sleeping and waking intertwine, assist one another and are difficult to isolate. Our daily worries sometimes seep into our dreams, and our dreams sometimes take a while to fade away in the morning. 

In 2008, physiologist James M. Krueger of the University of Wisconsin found that sleep can occur in some parts of the brain while other parts are still awake. With this in mind, a friend of the author compared this process to popping popcorn; you don't know which kernel will pop next, but eventually all will pop, and in this case send you into sleep mode.

So how does this work? When small, independent groups of neurons grow weary from use, they release more of the chemical ATP and shift into the sleep state, while other neurons remain awake. But when a group of neurons enter sleep mode, neighboring groups tend to follow in a rather unpredictable way. This suggests that the transition into and out of sleeping and waking may take longer and be more complex than previously thought.

> _"All that we sense, perceive, intuit and dream — is real and useful."_

### 7. Sleep improves our memory and helps us cope better with emotions. 

Many of us begrudgingly force ourselves out of bed in the mornings, often feeling lazy if we're not up and about at a respectable hour. But getting plenty of sleep is essential for our cognitive, physical and emotional well-being. 

Let's take a look first at memory and cognitive performance, both of which are improved by sleep.

SW sleep preserves our memories by recapitulating information we learn during the day, while REM sleep incorporates these memories into what we already know.

The result of this is what researchers refer to as _memory consolidation_. This is when neural connections we rarely use become weaker and newly formed memories are strengthened as our brain replays them. In this way, as we sleep, short-term memory transitions into long-term memory. 

This process was discovered in the 1990s by neuroscientist Matthew Walker in his experiments with rats. He found that rats "practiced" running through a maze in their brains 20 times faster when they were sleeping than when they were awake. 

These same rats were also better at running through the mazes after having slept. In this sense, we function just like rats; we remember things better after sleeping and often wake up better equipped to solve problems. Because we know this, researchers advise students to take naps to aid their studies. 

Sleep also helps us cope with emotions by highlighting our daily anxieties, a benefit demonstrated by dream expert Rosalind Cartwright. Over five months, Cartwright logged the dreams of 20 recently divorced men and women, half of whom were clinically depressed. Those who recalled their dreams more often would experience longer and more complex dreams, which often integrated recent emotional experiences. This group recovered better than those who had shorter dreams and often couldn't recall them. As Cartwright explained, the dreams were like rehearsals for the patients' recovery.

It follows, then, that our dreams can help with emotional trauma, often showing us multiple perspectives, and thereby helping us understand and process experiences more effectively.

### 8. Everyone dreams because it is innate to human nature. 

Do you know someone who swears they never dream? Well, as you might have guessed, they actually do — they just don't remember it.

Dreaming is innate to humans; researchers have found that we dream around four to six times a night. So the question isn't _whether_ we dream, but why some remember their dreams and others don't.

Remembering dreams has to do with personality, how you wake up, your attitude toward dreams, how long you sleep and the intensity of your dreams. Studies have shown that we tend to forget around half of our dreams in the first five minutes after waking, and a whopping 90 percent after only ten minutes. Our memory gets poorer still if we're jolted awake by an alarm, leap out of bed or don't get enough sleep.

So how can we remember our dreams better? Experts suggest getting enough sleep and reminding ourselves as we're drifting off that we intend to recall them in the morning. Upon waking, you should lay still and acknowledge how you feel, noting any images or sensations, and write them down as soon as possible to revisit later on. 

Although we all dream differently, most of us make a distinction between everyday dreams and extraordinary dreams. Carl Jung, the founder of analytical psychology, called these _big dreams_ and _little dreams_. 

Big dreams are unforgettable and result in considerable life changes or experiences; they not only contain memories, but create new experiences beyond our current knowledge. The most common big dream involves a visit from a deceased person. These dreams are profound, like psychiatrist William Dement's dream of suffering from inoperable lung cancer, which led him to quit smoking the next day. Abraham Lincoln's dream of being assassinated only days before he was killed in real life is another prime example. 

Little dreams are quite the opposite and are characterized by far more mundane concerns.

> _"The more interest we take in our dreams, the more we remember."_

### 9. Waking up is difficult due to social jet lag and sleep inertia. 

People who wake up with a spring in their step are hard to come by; for most of us, waking up is a real chore. But have you ever considered why? It's all due to _social jet lag_ and _sleep inertia_.

Social jet lag describes the discord between our internal biological rhythms and our actual sleep schedules, which are largely determined by school and work.

Our circadian body clocks account for the majority of our daily fluctuations, including blood pressure, body temperature, immune system, hormones, appetite, thirst, arousal and our sleep cycles.

These biologically driven changes affect the length of sleep we need, determine whether we're night owls or early birds and affect how we wake up. However, due to our packed daily routines and increased activities, whether social or work-related, along with the constant use of electrical light, we now sleep and work at hours that aren't always compatible with our natural rhythm. 

Chronobiologist Till Roenneberg, who coined the term _social jet lag_, estimates that 40 percent of the Central European population is constantly two or more hours behind on sleep. In the long run, this can seriously compromise human health, leading to chronic fatigue, digestive problems and weight gain. 

Furthermore, in 2007, the World Health Organization stated that disruptions to circadian rhythm through shift work may even be carcinogenic.

In addition to social jet lag, many of us suffer from sleep inertia. This refers to the struggle of getting out of bed, and is based on Newton's first law of motion, which states that a body at rest tends to stay at rest. Our brains don't just flick on like light bulbs the moment we wake up; instead, some brain regions activate more quickly than others.

Research has shown that memory, manual dexterity and complex decision making are impaired by sleep inertia, with one researcher claiming it to be as bad or worse than being at the legal blood alcohol limit!

### 10. Final summary 

The key message in this book:

**By understanding how sleep works and drawing from different cultural approaches to sleeping, we can all enjoy better quality sleep and live healthier, happier lives.**

Actionable advice:

**Embrace your nightmares.**

The next time you have a nightmare, remember that it may simply be a call for you to rethink some part of your life, such as spending more time with a loved one, or working less. When you acknowledge this and leave sleep to do its job, the images that emerge will be more symbolic and less literal, and your nightmares will fade.

**Suggested** **further** **reading: _The Sleep Revolution_** ** __****by Arianna Huffington**

These blinks are about the importance of a basic human necessity that we often brush aside: sleep. Getting enough sleep isn't just about feeling better in the morning — it improves your work performance, health and even your personal relationships. Similarly, sleep deprivation isn't a by-product of hard work; rather, it _prevents_ you from reaching your full potential. _The Sleep Revolution_ (2016) explains why sleep is so critical, and what you can do to get more of it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Kat Duff

Kat Duff is a mental health counselor and journalist. She is also the author of _The Alchemy of Illness._

