---
id: 5a80ea28b238e100072f703b
slug: the-secret-en
published_date: 2018-02-14T00:00:00.000+00:00
author: Rhonda Byrne
title: The Secret
subtitle: None
main_color: 9E4543
text_color: 93302E
---

# The Secret

_None_

**Rhonda Byrne**

_The Secret_ (2006) is a global media phenomenon that has sold over 20 million copies. In it, the author shares her thoughts on the powers that govern the universe and how you fit within it. Whether you're aware of it or not, you are in charge of whether you experience joy and wealth or despair and poverty. These blinks lay out the author's tools for taking control of your destiny so that you can unlock the happiness that you're looking for.

---
### 1. What’s in it for me? Understand a global media phenomenon and learn the author’s secret to getting what you want in life. 

They say you are what you eat, but according to _the secret_, you are what you _think_. Whatever you want to be, you have the ability to be it, and the secret will tell you how to get there. You will learn that the universe works according to the law of attraction and how you can use that knowledge to your advantage while working toward creating your dream life.

These blinks will also show you why negative thinking doesn't get you anywhere and that creating a positive environment can be achieved in just three simple steps.

In addition, you'll discover

  * how the secret is similar to quantum mechanics;

  * why repeating "I can afford it" is a good thing; and

  * why you shouldn't fight for what you want.

### 2. The secret to life is the law of attraction. 

Once upon a time, the author, Rhonda Byrne, was feeling unhappy and overworked. She thought that there must be a better way to live.

And she was right. Upon discovering old books such as _The Science of Getting Rich_, which was written in 1910, Byrne learned that there was a secret law that governs the workings of the universe.

Continuing her research, Byrne realized that poets like Ralph Waldo Emerson and William Blake, and philosophers like Goethe and Plato, as well as all the major religions, referenced one powerful law: _the law of attraction_.

In fact, this law is the most powerful one in the universe; it informs every aspect of your life. The law of attraction states that what you put out into the universe is what you'll receive in return. So, if you're full of frustration, anger and other negative thoughts and emotions, according to the law of attraction, you will only receive more bad things to be angry and frustrated about.

Conversely, if you're sending love, gratitude and praise out into the universe, positive things will come your way. Whatever you tend to think and feel is what you'll attract to yourself.

Furthermore, this secret — the law of attraction — is affecting your life right now. You can measure how much negativity or positivity you're releasing into the universe by reflecting on your current state.

What occupies your mind the most will influence your state of being. If you're spending the majority of your time thinking about things that make you angry, sad or disappointed, these thoughts will come to define your life. For example, if you spend most of your day worrying about debt, you're pretty much guaranteed to stay in debt.

The good news, however, is that you have the power to change it — and you can start right now!

> "_Through this most powerful law, your thoughts become the things in your life."_

### 3. The law of attraction doesn’t listen to “don’ts” but to the principles of quantum mechanics. 

The main reason that people aren't benefiting from the secret to life is this: they're focusing mainly on what they _don't_ want instead of what they _do_ want.

But the law of attraction doesn't comprehend "don'ts."

If you spend most of your day preoccupied with the thought "I _don't_ want to fail the exam," the message you're actually sending to the universe is "I want to fail the exam," since "don'ts" aren't recognized.

Therefore, to change your current state, the message you send out has to be positive. You need to tell the universe what you _do_ want, like: "I want to pass the exam," or, "I want to live debt-free," for example. The more exact you are in what you want, the better — "I want to live debt-free by the end of the year."

The same ideology can be applied to larger phenomena in the world. Instead of anti-war protests, there should be more pro-peace rallies. Instead of anti-drug campaigns, there should be a bigger focus on pro-sobriety. We need to start thinking about what we want, not what we don't want.

So if the law of attraction doesn't work according to "don'ts," how does it work? Well, it actually follows laws similar to those of quantum physics.

If we dissect the universe until we get right down to its smallest and most fundamental level, what we're left with is energy. Energy vibrates at certain frequencies, and when things vibrate on the same frequency, they are drawn to each other through natural attraction.

Following the same logic, if you change what you think about, you are also changing the vibrations and frequencies of your thoughts, thereby influencing the kind of things that will be drawn to you. So whatever you're spending the most time thinking about, that will be your predominant frequency, and it will determine what the universe sends your way.

> _"You are a human transmission tower, and you are more powerful than any television tower created on earth."_

### 4. There are three steps to creating the universe you want to live in. 

Your thoughts are like a TV transmission tower, constantly broadcasting your requests out into the universe. So if you don't like what you're receiving, you need to change the frequency! Or, put another way, your thoughts are like seeds. The more positive thoughts you plant, the more positive your life will grow.

In both scenarios, the message is the same: _you_ are the creator of your universe, and you can start to shape the world that you want with these three steps:

The first step is to _ask_. Be clear about what you want or desire. The best way to make sure the universe knows what you're after is to write it down according to a specific structure. Use the present tense and ask with gratitude, as if it's already yours, like so: "I am grateful to have [insert your desire here]." It can be an experience, such as, "I am grateful to have a healthy mind and body." Or your wish could be about a product or a person — "I am grateful to own a new car," or, "I am grateful to have a partner who treats me like an equal." Think of it like placing an order from the catalog of what the universe has to offer.

The next step is to _believe_. You need to have unwavering faith, and you need to radiate the feeling and confidence that comes from your faith so that the universe gets the strongest possible signal.

The final step is to _receive_. This is also about sending out the right frequency by imagining the feeling you'll have once you get what your heart desires. You could use a mantra to help simulate that feeling and repeat, "I am receiving now. I am receiving all the good in my life now. I am receiving [insert your desire here] now."

By asking, believing and receiving, you will create the positive environment in which you can achieve what you want.

### 5. A wealthy, healthy and loving relationship requires visualization and a positive mindset. 

When trying to ensure that positive thoughts are your most dominant thoughts, you might find yourself feeling overly anxious about any negative thoughts that threaten to emerge. But you needn't fret. If you worry about negative thoughts, you'll only make them worse. What you should do is let that negativity pass as effortlessly as you can, and instead focus on the good.

For instance, if you want to be wealthy, make that the focus of your thoughts. Grab a pen and write, "I am grateful to earn one hundred thousand dollars a year."

To help you believe and receive the positive thought, you need to visualize the end result.

No matter what you ask for, visualization is key in helping you achieve it. So if you want to get wealthy, you could try printing out your bank statement, covering your current balance with white out and then writing down the amount you really _want_ to have.

Visual aids like this can help you to really believe that you will achieve what you want and that the universe will make it possible to do so. It will redirect your thinking to the right frequency, as will repeating the phrase "I can afford it!" for the next 30 days when coming across things you hope to buy.

Following these steps will help with positive thinking, as will the belief that you deserve good things, especially in your relationships. Remember to treat yourself and others as you would like to be treated. When you learn to love and respect yourself, you'll find the universe sending you someone else to love.

Unlike other wishes, love only exists as a feeling, so you'll only be able to conjure this emotion within yourself. However, the more you think loving thoughts and send out positive vibes, the more likely the same good feelings will come back to you.

### 6. Positive thinking can lead to weight loss and good health. 

Think that our emotions have no impact on our health and weight gain? Think again.

According to the author, to lose weight, the key is to avoid thinking "fat thoughts" and instead start thinking "thin thoughts."

Many diets fail to work because they keep you focused on negative thoughts, such as, "I don't want to be fat," thereby worrying you into thinking that food controls your body. You shouldn't spend your life worrying, and certainly not about every little calorie of food you eat. Instead, you should realize that food doesn't control your body — you do. If you want to be like one of those people who eat whatever they want and don't gain any weight, you need to stop worrying and start thinking like them.

To get your ideal body, the first step is to _ask_ for it. Then you need to _believe_ and trust that you'll reach that ideal body type. After that, you can _receive_ and start feeling good about yourself and enjoy the moment.

Besides weight loss, having a healthy body that can take care of itself is also the result of your emotions. Many ailments derive from stress, which in turn is caused by negative thinking, so you can greatly improve your well-being by changing the way you think.

According to the author, when a person really believes their body can cure itself, that belief becomes a reality, demonstrating the scientifically-proven _placebo effect._

Another scientific fact of the human psyche is that the body is constantly cleaning out old cells and replacing them with new ones; every three years, you essentially have a new body. Remember this fact and recognize that the only thing keeping an illness around is that _you_ continue to hold it in your thoughts.

### 7. Avoid resistance and recognize that you’re in control and will live in harmony with the universe. 

Imagine there's a force in the universe that's responsible for creating the world we live in. It has always been there and always will be. Similar language is often used to talk about God or to talk about energy, but choosing a specific name for this force is less important than realizing that it exists — and understanding your relation to it.

The secret to life is to recognize yourself as one with the universe, that you're an energy field acting inside a larger energy field, where the goal is to be in harmony and not in competition.

One of the main problems that people encounter is thinking that there isn't enough positivity to go around. The truth is that there is plenty for everyone. Don't be fearful or feel like you need to compete with others to get what you want. Besides, we're all part of the same connected universe, so to compete with others is, in the larger scheme of things, to compete with yourself.

Along with fear and competition, another reason for negative thinking is resistance. As the psychoanalyst Carl Jung put it, "What you resist persists." So don't feel like you need to fight for what you want. When it starts to feel like a chore, that's when you know you're on the wrong track and frequency. If you're doing what you're supposed to be doing, on the other hand, it will come easily and naturally and won't feel like work at all, because you're following your bliss. This is what the secret is all about.

Remember, you have complete control over your thoughts, and thus complete control over what comes your way. No one else can create your universe for you.

Sometimes it might feel like your past is controlling your future, whether through regrets over a wrong choice you made or trauma from an unpleasant experience. But if you keep true to the secret and focus only on what you want, the grip of the past will gradually loosen, and you'll realize that you control your fate.

> _"All we are is a result of what we have thought."_

### 8. Final summary 

The key message in this book:

**The secret to life is found in the universal law of attraction. If you send out good thoughts and intentions to the universe, the universe will give you good things in return. Positive thoughts attract happiness, and, similarly, negative thoughts attract bad situations and fuel existing worries and negativity. But you can use this to your advantage since all it takes for a life filled with happiness and wealth is focused concentration and positive thinking.**

Actionable advice:

**Keep a gratitude rock in your pocket.**

To keep your thoughts positive and focused on gratitude — one of the most powerful emotions you can have — you should find a small rock that you like and keep it in your pocket at all times. Whenever you touch the rock, tell yourself something that you feel grateful for. This will help to ensure that your thoughts remain predominantly positive and good things come your way.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Inner Engineering_** **by Sadhguru Jaggi Vasudev**

_Inner Engineering_ (2016) explains how happiness can only be found within yourself. These blinks introduce spiritual wisdom that will make you happier, more fulfilled and at peace with the life you are living.
---

### Rhonda Byrne

Rhonda Byrne was a mother of two whose search for the answers to life's mysteries led her to form a group of like-minded teachers who were all privy to the secret presented in these blinks. From that came a film and a book, both called _The Secret_, and both have proved immensely popular all over the world. Byrne has also written a follow-up book called _The Power._

