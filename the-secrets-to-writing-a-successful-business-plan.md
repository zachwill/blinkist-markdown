---
id: 56e026f826ea840007000018
slug: the-secrets-to-writing-a-successful-business-plan-en
published_date: 2016-03-10T00:00:00.000+00:00
author: Hal Shelton
title: The Secrets to Writing a Successful Business Plan
subtitle: A Pro Shares a Step-by-Step Guide to Creating a Plan That Gets Results
main_color: 363247
text_color: 1F1B32
---

# The Secrets to Writing a Successful Business Plan

_A Pro Shares a Step-by-Step Guide to Creating a Plan That Gets Results_

**Hal Shelton**

_The Secrets to Writing a Successful Business Plan_ (2014) is an expert's guide to crafting a strategy that works. These blinks explain everything you need to know about this process in a detailed, easy-to-follow structure.

---
### 1. What’s in it for me? Become the best of business plan builders. 

Do you instantly get sweaty hands when someone says that you have to create a business plan before you can realize your entrepreneurial dreams and endeavors? Maybe for some, who associate them with too much wasted effort and frustration, they're even a deal-breaker.

But it's really no biggy if you know how to go about it.

These blinks of _The Secrets to Writing a Successful Business Plan_ take you right to the core of the issue — that is, why business plans are so damn important, how to avoid common mistakes in both style and content, and how to craft them successfully.

In these blinks, you'll learn

  * when it makes sense to make your business plan a 30-page roadmap — and when it doesn't;

  * why you should pay extra attention to the executive summary; and

  * which three basic financial statements you simply must include in your business plan.

### 2. A business plan is a multi-purpose tool for refining any idea. 

Most people have heard of business plans but don't know a ton about them. So, what exactly are these tools of entrepreneurship?

Well, a business plan is basically a brief write-up that lays out the basic elements of a soon-to-be company. There are a few things to keep in mind before you sit down to write a business plan, and the first is knowing when you need one.

For instance, say you're forming a new business — like opening a hair salon, buying an existing motorcycle shop or launching a start-up based on your own unique idea. Every one of these actions requires a business plan that details how operations will take place. 

But what does a business plan really do?

Essentially it helps structure your project, confirm its feasibility and crunch the numbers on its projected profits. So a business plan is more than a mere formal, administrative document. In fact, writing one should be a creative process that helps you flesh out your business idea. 

It's this process of thinking that will help you determine how likely your chances of success are and whether your idea is financially sound. Not just that, but in your business plan you'll set reasonable goals, go over your benchmarks and put the finishing touches to your concept. This exercise is necessary because the time you invest here will help you identify and avoid any potential errors, thereby boosting your chances of success. 

And finally, it's important that you clearly state your _value proposition_. You can think of this as your elevator pitch. That is, imagine you step into an elevator only to find Mark Zuckerberg. Now you've got 15 seconds to win him over to your idea by answering the questions, "which problems are you solving?", "how are you better than the competition?" and "why will you succeed where others have failed?"

### 3. Avoid common mistakes to draft an appealing business plan. 

Before we get to the real meat of writing a business plan, it helps to go over potentially deal-breaking mistakes and learn how to avoid them, specifically when it comes to seeking funding. 

First of all, precision is everything, especially when identifying your competition. That means you need to be accurate and state exactly what you intend to do. So, while it's nice to share your dreams with people, your business plan should deal with concrete issues like financing, product development, distribution and branding among others. 

It's also good practice to identify your competition. Because if you don't, your potential funders will assume you lack general business insight and won't want to invest with you. So remember, every business has competition. 

But you'll also need to answer the question, "how will this company make money?" If you take an approach that focuses too much on starting the business now and worrying about profits later, your business plan is unlikely to find much support. 

The next step is to add brief biographies of your core staff members. Remember that when choosing investments, funders are putting their faith in an entrepreneur and his team. Therefore, it's vital to make investors comfortable with you and the people working for you. 

Equally important is making sure that the money you are asking for is in line with your needs. For instance, if you apply for a $100,000 loan, the investor will want to know how every dollar will be spent, whether it goes to inventory, equipment or salaries. So, if you can only account for $50,000 of the total, you'll appear unreliable and likely won't receive financial support. 

Finally, you need to be consistent. That's because overall consistency sends a powerful message that convinces investors of your idea's validity. That makes outsourcing parts of your business plan a dangerous tactic. After all, you might end up with an only partially cohesive document. For example, if you tackle the sales forecast but ask a consultant to draft the marketing plan separately, the figures will likely be out of sync.

### 4. First things first: feasibility plan, feedback, family finances. 

OK, now that you know about common pitfalls, let's have a look at strategies for writing a strong business plan. The first thing to do is to put together a _feasibility plan_, a tool for challenging your assumptions. 

This document, about five pages in total, should cover the key elements of your plan: the business idea, your main advantage over competitors, a profile of your target customer and a few snippets about daily operations. You should also include some figures that approximate your potential cash flows. 

Basically, by looking at a feasibility plan you should be able to quickly determine whether your idea is solid enough to proceed or whether your initial thoughts about the business concept were too loose or naive. 

Once you've got a strong feasibility plan and believe your idea is a good one, you should share it with your mentors and advisors. In other words, discuss your project with objective experts you trust and seek their feedback. This is a great way to further challenge your preconceived notions about your feasibility plan. On the other hand, seeking advice from family or friends who might be too quick to praise you won't help you get an objective perspective on what matters most. 

Then, if possible, begin your business with your own financial resources or those of your family. 

Why?

Because half of all new start-ups fail in the first five years. If your business is one of them you'll be in a better position if you borrowed money from your grandmother than from a bank. 

Now that the basics are all laid out it's time to learn about the core of the business plan itself!

### 5. The first part of your business plan is also the most important. 

We all know not to judge a book by its cover, but when it comes to business plans, the opening section can make or break the deal: it's called the executive summary and it's absolutely essential. 

That's because this section might be the only one your readers even look at. After all, bankers and investors are busy people, often mired in piles of business plans. Since they've got limited time to review them all, it's unlikely that they'll get past the first two pages of your plan if the story you tell doesn't grab their attention. 

But what exactly is an executive summary?

It's a rundown of your entire business plan that should be consistent with everything that follows. So, in the first paragraph state the problem you are going to solve for your customers and how your solutions are unique. Then, in a marketing paragraph, explain who your target is, how much you expect to sell and what competition you'll face. 

The following section should be brief and focus on your team. Give details about all your past experience and expertise in the industry. Finally, the last paragraph should detail your finances through key figures like your expected income over the next three years. Here you should list how much money you need to get going, then split this amount between the sum you're putting down and how much you'll need from outside investors. 

But remember to keep it short and sweet at two pages max. And although the executive summary is the first section of your business plan, it's actually best to write it last. That's because you'll do a better job once you've drafted the rest of the plan and know exactly what your project is all about.

### 6. A strong business plan gives detailed descriptions of the concept, operations and team. 

You might have been working on a project for years and take things about it for granted, but it's all new to those reading your business plan, which means you'll need to present your ideas clearly. 

For starters, you should detail your product or service, the channels through which it will be distributed and any competitors. When it comes to the product, naturally your readers will want you to paint a clear image of how it works. If you invented a new type of electric engine, investors would want to see the technical details of the device. 

But your distribution channels are also important. Because whether you plan to sell your product on the internet, through retailers or on the wholesale market, it's essential to explain why the choice you made is the right one for your business. 

It's also important to highlight your competitive advantages. Maybe you're local, meaning you've got inside knowledge of a particular market or tons of experience in your field. 

Then you should lay out every detail of your operations from your production techniques to your schedule. Readers will want details on your daily business routine in general, everything from your location to working hours, to the equipment you'll use. Maybe your main supplier is on the other side of the globe. In a case like this it would be essential to explain how the two of you will work together across this distance. 

Any potential legal issues should also be detailed. For instance, when opening a bar one usually needs to acquire a liquor license. 

Finally, you should introduce your team. That's because your investors will want to know who is getting paid with their money. Putting your entire family on the payroll will likely raise some red flags. 

So, to be clear about this, answer these questions: "Will you work in a team or by yourself?", "Do you want to hire people on a contractual basis?" and "Do your staff members have the necessary expertise?"

### 7. You must detail your strategy and path to success. 

Once you've got this information laid out you'll need to communicate your business strategy and your plan for reaching your goal. To do so you should break it down into three parts.

First, include a market analysis that details the size of your market and potential customers or business clients. By beginning this analysis with the size of the market, you'll show that there are enough customers to make your idea profitable. 

From there you can profile your ideal customer, a crucial step in figuring out how to reach him. For instance, say you invented a new Facebook-like site for dogs. Your target is likely pet lovers between the ages of 15 and 40 who are excited about new technology. 

Step two is to produce a competition analysis. If you're anything like the average start-up entrepreneur you're likely convinced that your idea is so unique that no competitor even comes close. Well, unfortunately this is simply never the case. 

So, a competition analysis is crucial for showing your readers that you've realistically assessed market conditions. In this analysis, you want to identify roughly five main competitors along with their biggest strengths and weaknesses. For instance, this might relate to their marketing strategies, their delivery policy or their operating hours. 

Then you can try to determine how your competitors will respond when you enter the market. For instance, will they slash their prices, steal your ideas or maybe increase their use of aggressive advertisements?

Finally, the last step is to offer specific marketing techniques, of which there are many, like email marketing, press and media, internet ads, blogging and countless other strategies. To determine which are right for you, keep track of the costs of each and the potential benefits they produce. It'll be clear which gives the most bang for your buck!

### 8. Forecasted financial statements are invaluable tools to prove that your business is profitable. 

There's another aspect of your business plan that's absolutely essential to investors and that's the financial statements. Most of us don't know much about this, so it's helpful to start with the basics.

Financial statements are basically charts that detail key financial figures regarding your business activity, and drafting them is the last step before you tackle the executive summary. In fact, these tables are a kind of summary that uses numbers to wrap up everything you've previously described. 

There are three types of financial statement that every business plan should have: the balance sheet, the income statement and the cash-flow statement. 

First, the balance sheet is simply a two column table divided between assets and liabilities. The former describes what you've got — things like equipment, money and inventory — and the latter what you owe, such as loans to a bank or debts to a supplier. The balance sheet is more or less a snapshot of your company's money and debt. 

The income statement comes second. It's a single column chart that details your sales, minus your expenses, thereby displaying your net profits. This statement should contain everything that has changed your financial position that year. 

Finally, your cash-flow statement is all about — you guessed it — cash. This statement looks very similar to one you'd receive from a bank, simply showing the in- and out-flows of money. 

Accurately producing these statements requires you to rely on solid assumptions. Remember, each figure you present should be supported by evidence. Whether that comes in the form of market research, interviews or even your own hypothesis doesn't matter as long as it's convincing. To be cautious, you can even draft a couple of different scenarios, one conservative and one a bit more optimistic. Just change your central assumptions accordingly.

### 9. Different types of funding work for different financial needs. 

Congratulations, you've got everything you need to craft a stellar business plan! But now it's time to talk about investors. Because knowing how to approach them is essential to your plan's success. 

First off, if you are going to apply for a bank loan, don't go in assuming they'll fully fund your business. In fact, bankers tend not to consider themselves the sole source of funding. That means you can't expect to get more than 80 percent of your funding from a bank and, before you even apply for a bank loan, you should have secured some other funding. Not just that, but a bank will also want you to commit your own money, bringing in collateral of about 125 percent of the loan. 

But if you can't get a bank loan you can go with two other potentially more expensive options. The choice is between _business angels_ and _venture capitalists_, both of whom will offer cash in exchange for shares in your company. 

The first, business angels, are extremely wealthy individuals who invest up to $2 million, expecting a return of between five and ten times their original investment. The second, venture capital firms, tend to invest larger amounts of between five and ten million dollars but expect much higher returns in the range of about 30 times the original. Naturally these funding options will cost you more in the long run than a bank loan because they are assuming a greater risk. 

But regardless of the type of investors you secure, they'll all have shared requirements when it comes to the financial data you provide. All of them will be interested in what's known as your _burn-rate_, that is, the amount it costs you to operate over a given period. 

Investors are obsessed with this indicator because it describes how long your business can stay afloat with their investment. They'll also likely want to see a three-year financial forecast with the first year broken down month by month.

### 10. Final summary 

The key message in this book:

**Starting a business is hard work and over half of those that get off the ground go bankrupt within their first five years. It's crucial to put in the groundwork, and you can only troubleshoot a new business by drafting a rock solid business plan.**

Actionable advice:

**Know your competitive advantage and don't think that low prices are a substitute:**

Before deciding to launch your business take the time to consider your competitive advantages and whether you even have any. If it seems like you don't, you might be tempted to simply undercut competitors, but this strategy rarely pans out. That's because while prices heavily influence consumer choices, people also tend to assume there is a problem with the cheapest option. In other words, rock bottom prices alone are no replacement for a unique market edge. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Write Your Business Plan_** **by The Staff of Entrepreneur Media, Inc.**

In _Write Your Business Plan_ (2015), you'll get a comprehensive overview of how to start putting your ideas to words and getting your dream of starting a business off the ground. A well-crafted business plan is a blueprint for future success, so it's vital that you craft a plan that sells your ideas well.
---

### Hal Shelton

Hal Shelton's 45-year track record with corporations and investment groups has made him the business expert he is today. Through his work with the SCORE Association, an organization that assists and trains small businesses, he has counseled over a thousand entrepreneurs.

