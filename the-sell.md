---
id: 55db30052647b1000900003f
slug: the-sell-en
published_date: 2015-08-26T00:00:00.000+00:00
author: Fredrik Eklund
title: The Sell
subtitle: The Secrets of Selling Anything to Anyone
main_color: BFA66D
text_color: 6B5B36
---

# The Sell

_The Secrets of Selling Anything to Anyone_

**Fredrik Eklund**

_The Sell_ (2015) teaches you how to become a top seller in any business — and how to get ahead in business and life, too! The book is based on the author's own experiences of working his way up to becoming the number one real estate agent in New York City. Being successful isn't just about getting ahead at work — it's about living a positive and healthy life as well. These blinks will teach you how.

---
### 1. What’s in it for me? Discover how to be more successful in business and in life. 

You often hear of salesmen so skilled, they could sell anything to anyone. While that may not be entirely true, some people just seem to have a knack for convincing others to buy whatever they're selling, whether it's the proverbial vacuum cleaner, a dodgy penny stock or a pricy New York City penthouse.

What is it that makes these people so good at selling? What's their secret?

As Fredrik Eklund, who is truly one of these people, will show you in these blinks, what you're selling is not the product — _you_ are the product and the brand. By being genuine and funny, and making sure your appearance is always immaculate, you'll already have taken the first steps to becoming more successful in both business and in life. But there's a few more things you must master to be truly successful, and these blinks will show you just what they are.

In these blinks, you'll learn

  * how Fredrik Eklund became the number one real estate agent in New York;

  * why you should spend ten percent of your commissions on your wardrobe; and

  * why you should bring up your grandmother in your next sales meeting.

### 2. You’re your own brand and product. 

What does _selling_ mean? That might seem like a simple question, but it's more complicated than you might think.

When you're selling something, what you're really selling is "you". You market yourself; you're your own brand and product!

The most important thing in business is to make others _want_ you — whether you're offering a product or a service — and to make them want you _now_.

You can only succeed in business if you know your product, and that means knowing _yourself_. You have to learn what your own strengths and weaknesses are.

The author succeeded in real estate by understanding how to harness the power of his own brand. He's the leading real estate agent in New York City and hosts an Emmy-nominated show, _Million Dollar Listing New York_.

_Million Dollar Listing New York_ was, in fact, the secret to his success. Celebrities now seek him out, because they know from his show that he makes buying an apartment a great experience. They want to buy from a star.

So strive to make yourself stand out like the author did; the worst thing you can be is boring.

People like to be around others who are happy and self-confident. So keep a positive attitude and remember that life is 10 percent what happens to you, and 90 percent how you respond to it.

Never stop learning about yourself, either. Find your trademark, the thing that makes you _you_ ; Eklund makes himself stand out with his famous "high kick" move. Which would you remember more vividly: a real estate agent in a gray suit who hands you an off-white business card, or a real estate agent doing high kicks on the red carpet?

Self-confidence and trademarks are even more important if you work with celebrities. Celebrities are constantly listening to insincere people who tell them what they think they want to hear, so they appreciate a person who's genuine. In the end, business is a social endeavor — successful people rely on their social skills.

### 3. Understand your own motivations and seek out positive role models. 

Why do you work? Some people do it for the money, others do it for the joy. What is it for you?

You can only achieve success if you understand what motivates you to work. This is another crucial part of knowing yourself.

The author, for example, knew he was motivated to get away from certain cultural aspects of his home country, Sweden. Swedish culture has a value called _Jantelagen_, which asserts that you aren't supposed to think of yourself as special. Eklund relocated to the United States because he knew he could get away from _Jantelagen_ there!

So figure out what makes you feel fulfilled. Don't just work to save for your retirement; instead, devote yourself to something you love — and dream big! If you don't, you'll just end up contributing to other people's dreams, rather than your own.

A good strategy for achieving success is to shadow a successful person in your industry; this is particularly important when getting started. Look for a role model, perhaps someone like Eklund, or anyone else you admire.

One good way to do this is to work for free for a time. Find someone you want to emulate and learn as much as you can about them and their environment. How do they dress? How do they speak? What's their office like?

It can be hard to get your foot in the door, however. E-mails won't work because successful people receive too many of them. Be more creative and seek your role models out in person, so you can show them you understand what they're about. Offer to meet for lunch or a drink at their favorite spot!

Always be sure to know the gatekeeper as well. After all, they're the ones who get you in the door, so be sure to make a quick and positive first impression. Practice presenting yourself confidently in 30 seconds or less.

### 4. Stay healthy, dress well and always be positive and charming with others. 

Being successful isn't just about performing well at work. It's also about your attitude, health and the way you present yourself.

First impressions mean a lot. In fact, a study at Harvard Medical School found that people assess your competence and trustworthiness within the first quarter of a second of looking at you!

Think of yourself as a present: it doesn't matter what's inside a gift if it isn't wrapped well, because the outside is what makes you interested. So dress well, avoid wearing black and gray, find your own style and make sure your hair looks nice. When you feel good about your appearance, you'll project more confidence.

If you work in sales, put ten percent of each commission aside for your wardrobe and personal grooming.

Nice clothes aren't enough, however — you also have to be healthy and physically fit.

It can be difficult to schedule in time for working out, sleeping and eating well, especially when your career starts to take off, but it's crucial to make time for these activities every day. Your diet and health are essential parts of your success.

Don't underestimate the value of good sleep, either. The DUKE NUS Graduate School of Singapore found that one's brain actually ages faster when not getting enough sleep (six to eight hours per night).

Finally, work on your charm and sense of humor. Laughter relieves stress and releases endorphins, so strive to use it to brighten people's moods. Use people's names, listen thoughtfully to them and stay interested in who they are — that is exactly what makes you charming.

### 5. Reach out to your customer base through social media. 

Once you have a good understanding of your product (you!), you need to find your customers, because a great product is meaningless if people don't know about it.

Seek out the people who are looking for your service, product or message. The best way to do this is through social media.

Whatever you do, you need an audience, which means you need a strong network of contacts. Social media might seem like the simple answer to this, but it can actually be difficult to leverage it effectively.

One key to using social media is to always be authentic, and not be afraid to be yourself. You won't appeal to everyone when you express yourself, but the right people will appreciate your honesty. Don't limit yourself by constantly seeking approval.

View social media as an opportunity, not an obstacle. These days, social media isn't just about staying in touch with friends — it's also about building a professional network and connecting with your customers.

You attract followers on social media by being active. This takes time and effort, but it's important to maintain a connection with customers because they're also giving back to you, in the form of free publicity when they tell others about your product.

Your social media accounts (you should have more than one) also serve as an official record of your product or service. This also makes you more authentic, because there's no longer a divide between our digital and nondigital worlds. Connect with your customers in both.

Also, be sure to maintain the same account name and style across your different platforms, making your content instantly recognizable. Don't outsource your social media accounts, either — you need to manage them personally.

### 6. Negotiate by accentuating your positive attributes and keeping deals balanced. 

How do you go about selling an apartment, or getting customers to sign up for your service?

The key to making a sale is to accentuate your positive attributes. In any kind of meeting, whether a business meeting or a one-on-one negotiation, the first ten minutes are key. They usually determine what the meeting's outcome will be.

So use those ten minutes to explain what you have to offer, but don't just give a speech. Make it a dialogue — everyone loves to be asked for their opinion.

You can think of it like a first date. Don't just ask the person to "go out" with you, ask them to join you for a pinot noir at your favorite wine bar. Be clear, concrete and personal all at the same time.

Be prepared as well. Plan out how you'll negotiate and have a clear idea of what you want to achieve. And remember: deals only happen when there's equality on both sides. Keep the negotiations slow and stay in control, but make sure the other side is getting something valuable in return. Maintain your position without endangering the balance.

You should also always negotiate face to face. Listen thoughtfully to the other person and engage with them in a meaningful way. 

Body language is another crucial part of negotiating well, so keep track of your nonverbal communication. Whenever the author is excited about getting a high offer on a property, he makes sure he doesn't show it with his body language. He knows that if he does, he might jeopardize the sale.

Sometimes it's also helpful to back away and bring up another topic. If you mention that your grandmother is visiting, for example, you'll bring some cheer to the conversation and make the deal seem less important.

### 7. Take good care of your team members and seek out positive media exposure. 

Building a team is an essential part of growing your business, and teams provide you with more opportunities for media exposure. Media is tricky, however, so make sure you use it to your advantage.

Start building your team by finding the people who want to work with you. When you're interviewing people, remember that you aren't looking for other versions of yourself; you need team members with a variety of talents and experiences, and you need to keep them happy!

So allow your team members to make mistakes and pay them well. Happiness in the workplace is a key part of success in today's business environment. Let your team members know how much you value them, whether you're their boss or are on equal terms. Be generous with your praise and offer criticism in a positive way.

Once you've got a strong team, you can start seeking out positive media attention. Social media is an important way to get exposure but you can't rely on that alone. Earnest praise in the press is more important than any endorsement you could ever buy.

So get your message out. The author did this by creating a reality show, but you don't have to go that far. Use HelpAReporter.com — it sends you daily e-mails about media outlets looking for certain experts. If you find an outlet looking for your area of expertise, you'll be presented as expert in a newspaper, on TV or on the radio. That's invaluable exposure.

Show that you're a part of your business as well. Comment on breaking news in your field and contact reporters to get more attention. You can also create an event — Eklund promoted his book by signing it in the middle of 5th Avenue!

> _"A person who feels appreciated will always do more than what is expected."_

### 8. Keep a positive attitude and remember to reward yourself. 

It's inevitable that you'll pass through difficult times on your way to success. Everyone stumbles, no matter how successful they are. The important thing is to stand back up quickly.

When facing challenges, it's critical that you maintain a positive outlook on life. Live a life of _pronoia_, the opposite of paranoia. Imagine that the world is conspiring to help you — view any obstacles or hard times as opportunities to learn. You'll start to view your life as a journey, where each step brings you closer to success.

Live your dreams _now_ instead of waiting for them, and invest in your own happiness, because it always pays off.

Live your life in extremes. Work hard, play hard and celebrate any success. And don't forget to give yourself vacation time, too! Time off reenergizes you and makes you more effective when you go back to work.

Did you know the United States is the only industrialized country in the world that doesn't guarantee vacation time? Vacations are a positive thing for our mental and physical health. In fact, the National Institute of Health found that people who skip vacations are more likely to suffer heart attacks. You will also invariably run out of steam if you work for too long without taking a break.

So stop living in the future and allow yourself to enjoy the present.

If vacations are unrealistic for you right now, treat yourself in other ways. Go out to a nice restaurant, spend a day at the spa or buy yourself some fresh flowers. Figure out what makes you feel happy and don't feel guilty for giving in to it. When you do, you won't just feel better in the short term, you'll perform better at work, too.

Finally, make other people happy as well: buy a stranger coffee or give a friend an unexpected gift. The more you give, the more you receive, and just as happiness brings you success, success brings happiness in return.

> _"Your business expands in direct proportion to the fun you have and the adventures you take."_

### 9. Final summary 

The key message in this book:

**You're your own brand and product, so be the best person you can be. Stay authentic, take care of yourself, learn from your mistakes and surround yourself with positive team members that you treat well. Don't focus your life on work, either — take vacations and remember to have fun. You won't just advance your career, you'll become a better and happier person.**

Actionable advice:

**Use the** ** _model of alternative costs_** **.**

The next time you're trying to decide if a new car or laptop is worth it, use the _model of alternative costs_. Weigh any new investments against the value you'd gain if you did something else. If you spend three hours on the train every day, you'll lose money if your phone dies and you can't charge it. It's more expensive to hire a driver than pay the train fare, but you would then have a mobile office where you could recharge your phone and relax, while saving the cost of taxi rides in between. Alternative costs are an important model to calculate if investing in an improvement in the office or elsewhere will actually pay off. 

**Suggested further reading:** ** _To Sell Is Human_** **by Daniel Pink**

_To Sell Is Human_ explains how selling has become an important part of almost every job, and equips the reader with tools and techniques to be more effective at persuading others.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Fredrik Eklund

Fredrik Eklund is the leader of the number one real estate team in the United States. He's also the star of _Million Dollar Listing New York_, a reality show that airs in over 110 countries.

Bruce Littlefield is a TV personality and the bestselling coauthor of numerous books, including _Shark Tales: How I Turned $1,000 into a Billion Dollar Business_.

