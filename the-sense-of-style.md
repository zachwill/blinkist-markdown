---
id: 56a6af98fb7e87000700002b
slug: the-sense-of-style-en
published_date: 2016-01-28T00:00:00.000+00:00
author: Steven Pinker
title: The Sense of Style
subtitle: The Thinking Person's Guide to Writing in the 21st Century
main_color: D69081
text_color: A35341
---

# The Sense of Style

_The Thinking Person's Guide to Writing in the 21st Century_

**Steven Pinker**

_The Sense of Style_ (2014) offers a refreshing and relevant guide to writing potent, readable texts of all kinds. Instead of extolling the same confusing and sometimes counter-intuitive rules found in traditional style guides, _The Sense of Style_ offers simple tricks and heuristics guaranteed to improve your writing.

---
### 1. What’s in it for me? Give your writing the style it deserves. 

Think of your favorite books. What makes them so good? Is it the wording, the opening paragraphs, the arc of the story or just how everything fits together? Chances are it's all of these things — or, to phrase it differently, it's how the author used _style_. So how can you hone this crucial writerly skill?

Despite Oscar Wilde's quip — that "nothing that is worth knowing can be taught" — there _are_ concrete steps you can take to make your writing better. These blinks, by examining different types of writing from many different fields, will help you develop a keener sense of style, whether you're an aspiring novelist, a journalist, a communications director or just someone who wants to write dazzling emails.

In these blinks, you'll find out

  * how the curse of knowledge can make texts nearly impossible to understand;

  * why a simple comma can make all the difference; and

  * what malaprops are and why you should avoid them.

### 2. Writers often get a sense of style from reading good writing. 

There are innumerable style guides out there, all designed to help writers master the art of grammar and punctuation. However, most accomplished writers will tell you that they get their skills not from reading style guides but from paying close attention to _other_ works they've read and enjoyed.

Indeed, studying good prose is central to developing solid writing skills. Take, as an example of engaging prose, the opening line of _Unweaving the Rainbow_, by scientist Richard Dawkins: "We are going to die, and that makes us the lucky ones."

Solid prose captures your attention by starting strong and avoiding clichés and banalities. When you want to add a sense of grandeur, take a tip from Dawkins and spice your prose with a bit of poetry.

But say you're trying to communicate an abstract idea. One way to make the conceptual more concrete is to use vivid imagery, which can both make an idea clearer and anchor the reader's imagination. In the same book, Dawkins describes the countless unrealized genetic possibilities as "unborn ghosts" — a potent and evocative image. 

Dawkins is bold from paragraph one, and this makes a strong impression on the reader. And that, ultimately, is the goal of most literature, regardless of genre.

But you don't have to limit your list of "good writing" to literary classics. Even the obituary column has lessons to offer.

_New York Times_ writer Margalit Fox's work in obituary writing demonstrates clearly that you can capture a person's legacy in 800 words or less.

For example, when writing an obituary for Maurice Sendak, author of _Where the Wild Things Are_, Fox wrote of his books: "Roundly praised, intermittently censored, and occasionally eaten." This playful juxtaposition perfectly sums up the broad impact of his books — both on critics, some of whom denounced his work, and on toddlers, who, being too young to read, see books as just another thing to put in their mouths.

### 3. Look to the classic style to develop good prose and clarity. 

There are many writing styles from which to draw inspiration, but when it comes to developing clarity and conciseness, both the author and literary scholars are agreed that no style is better than the classic.

The primary objective of classic style is to present things to the reader in a way that is clear and simple enough for them to understand, no matter how complex the topic. To achieve this, the writer must say goodbye to the pedantic or unnecessarily analytical tendencies that so often crop up in other styles of writing, especially those in academia or business and legal professions.

Physicist Brian Greene, for example, used classic style in his writings for _Newsweek_ to expertly explain an incredibly complex topic: the theory of the _multiverse_. He used clear, everyday images and examples to describe the way in which our universe could simply be one of an infinite number of coexisting universes.

Specifically, Greene compared the multiverse to a well-stocked shoe store, one which can guarantee that you'll find your size, as a means of explaining the perfect conditions of our particular universe to exist within the multiverse. The more shoes there are, the greater the chances of your finding the right pair; the more universes there are, the greater the chances of there being one with the right conditions. 

If written well, classic style can make the reader feel like a genius. Bad writing, on the other hand, can make the reader feel like a buffoon.

Classic style assumes that writer and reader are equals, and the text aims to guide the reader to an understanding of the concept under discussion.

Bad writing, in contrast, does the exact opposite, and often leaves the reader feeling lost. It's often full of unexplained references and intimidating passages that an average reader couldn't readily understand. Whereas clumsy writers often qualify and obscure their contentions with words like _virtually_, _seemingly_, _somewhat_, or phrases like _I would argue_ or _to some extent_, classic style presents its information clearly and confidently.

### 4. The curse of knowledge often leads to bad writing. 

Have you ever stopped reading an article because it was filled with words you didn't understand? Texts quickly become incomprehensible when the writer forgets that her readers aren't all experts on the subject.

This _curse of knowledge_ reflects the writer's inability to place herself in the position of the reader, who may only have a rudimentary understanding of the subject. So what can you do to avoid this in your writing?

One very straightforward method is to avoid jargon, abbreviations and technical vocabulary that will only be understood by people well versed in the subject. 

When you're firmly entrenched in a subject — such as science or sports — it's easy to forget that the language used in these fields isn't necessarily intelligible to everyone. For example, it's still totally possible to write a cogent summary of your rodent behavior study by writing "relating to rats and mice" instead of the more technical term "murine."

In the same vein, while abbreviations might save you time, they might waste the reader's. Don't force your reader to look up abbreviations that he has no reason to be familiar with.

And if you must use technical terms, define them when you introduce them. This will make things much easier for your reader.

In addition, many writers feel the need to prove their expertise. And so, in an attempt to prove their erudition, they fill their writing with high-level vocabulary and complicated definitions.

Don't fall into this trap. If you're writing to prove a point, studies have shown that readers will better understand your arguments when your writing is clear and concise, not bogged down by self-absorbed thoughts or technical mumbo-jumbo.

A good way to ensure your writing is free of technobabble is to imagine a potential reader looking over your shoulder as you write, reminding you when you use words that might not immediately be understood.

### 5. A solid understanding of syntax and grammar will improve your writing. 

If someone were to inform you that "Eating Jerry is sandwich a," you would likely have no idea what was being communicated. That's because the _syntax_ — the order of the words in the sentence — is jumbled. "Jerry is eating a sandwich," on the other hand, is much easier to understand. 

Unfortunately, syntactical errors are usually not as obvious as in the example above, making them somewhat difficult to spot. But a deeper understanding of syntax can help you spot your errors, which, in turn, will make your writing much clearer.

In essence, English syntax is the code that tells us who did what to whom. By understanding how a sentence is structured, and how the words that comprise a sentence are supposed to agree with each other, writers can more easily spot when the code is being broken.

Take the sentence "The impact of the budget cuts have not been felt yet," for example. The word "have" implies that the subject is plural. "The impact," however, is clearly singular, so "have" should be replaced with "has."

A proper understanding of syntax can likewise help you avoid confusing or convoluted sentences.

Indeed, many style guides will advise you to cut the fat from your writing by removing unnecessary words. Doing this is much easier when you have a solid understanding of syntax and a good feel for spotting words that are superfluous, redundant or confusing.

Don't take this advice as a directive to simply axe all unnecessary words, however. It's fine to write page-long sentences — but only if your syntax is solid.

Sometimes a sentence is confusing merely because it's poorly punctuated. For example, if you have some knowledge of syntax, you would avoid writing a headline like this: _Joe Shmoe Finds Inspiration in Cooking His Family and His Dog_. Put a comma after "cooking" and Mr. Shmoe is suddenly no longer a psychopath.

### 6. When used properly, the passive voice can improve your writing. 

There are two major voices in English: the _active voice_ and the _passive voice_. Let's look at both.

One uses the active voice when the subject of the sentence does something to someone else. For example, "A dog bit Jack on the leg." The subject in this sentence, the dog, is biting the object, Jack, and is therefore _active_.

The passive voice is used when the subject of the sentence is being acted upon. So, for instance, "Jack was bitten on the leg by a dog." In this case, the subject, Jack, is _passive_, while the object, the dog, is active. 

As you can see, the passive voice can make a sentence wordier. You'll often see the passive voice being used in academic and bureaucratic writing, which tends to be dull and difficult to read. 

Consequently, editors and style guides have frowned upon the passive voice for ages.

In his 1946 essay, "Politics and the English Language," influential writer George Orwell suggested that the active voice should always be used in favor of the passive voice. And over the years, editors have agreed, often blindly changing sentences written in the passive voice.

But the passive voice has its benefits, and a good writer (or editor) should know when to use it.

For example, sometimes the active voice does an awkward job of getting information across. 

Take this troublesome sentence, written in the active voice: "Another man, he says, whom someone had told to get rid of the puppy, gave the dog to him." 

This can be pleasantly changed using the passive voice: "The puppy, he says, was given to him by another man who had been told to get rid of the dog."

Remember, the goal is to keep your writing limpid and concise, so anything that helps you do that is fair game. The passive voice sometimes allows you to guide your reader's attention in ways the active voice can't.

For example, it's always good to start sentences and paragraphs by focusing on the subject at hand. Let's imagine you're trying to do this in an essay about red-tailed hawks. Using the active voice might result in: "Birdwatchers observed the red-tailed hawk eating…"

Using the passive voice enables you to put your subject front and center: "The red-tailed hawk was observed eating…" After all, you're not writing about birdwatchers!

### 7. Writing a coherent text is about defining your topic and connecting the arguments. 

Making compelling sentences by stringing together the right words is only part of the equation. You also have to string your sentences together to make a coherent text.

A series of well-written sentences may still result in an incoherent mess. To write an intelligible text, you must first have a clear topic in mind and an outline that tracks the flow of your ideas. Your outline doesn't have to be laid out in the traditional style, with letters, numbers and roman numerals; all you need is something that establishes _arcs of coherence_ — clear links from one sentence or passage to the next.

You develop coherence by creating transitions between sentences or paragraphs that make it clear that they are connected. This is called _coherence relation_.

One way to make clear how your sentences or paragraphs relate to one another is by using the right connecting words. Whether you want to show a similarity (with words like "similarly" or "likewise"), a contrast ("in contrast," "on the other hand"), an elaboration ("moreover," "in other words") or an explanation ("because," "as"), the right connecting word makes all the difference.

A coherent text is intelligently designed, making use of these connectors to create an arc of coherence that keeps track of ideas and points of view.

Most fundamentally, you need to make sure that the reader understands your topic from the very beginning. You've surely heard the phrase "Don't bury the lead" — a mantra among writers because of its applications to all kinds of writing. You don't want to leave your reader wondering what your piece is about.

The same goes for your own point of view. The reader should know early on what your stance is on a topic or what you want to say about it.

### 8. When it comes to grammar, purists aren’t always correct. 

You're in the grocery store. There are only nine items in your cart. And so you move toward the sign that reads "Ten Items or Less." 

As you're waiting in line, someone approaches you and says, in a tone full of disdain, "Oh, illiteracy! Should that sign not read 'Ten Items or _Fewer_?' 'Less' is _incorrect_!"

And yet, no one seems to care. Everyone knows what the sign means, and yet grammar purists can't seem to give it a rest, claiming that the word _les_ s shouldn't be used for countable quantities.

In some situations that's true: it sounds bizarre to say _less bills_. "Fewer bills" sounds much better. On the other hand, _one less car_, _less than 21 years of age_, and _one less thing to worry about_ all sound quite natural. In these and numerous other cases, the English language has happily accepted the word "less" where, strictly speaking, "fewer" is correct.

The same goes for the singular "they." There is a lack of gender-neutral pronouns in the English language; opting for the singular "they," however, instead of writing "he or she," isn't a recent invention, nor is it grammatically incorrect. In fact, both Jane Austen and Shakespeare used the singular "they" to manage the issue of gender-neutral pronouns.

But the quintessential false rule is the prohibition of split infinitives — that is, verbs such as "to love" or "to go."

Language purists have long complained that _Star Trek'_ s Captain Kirk committed a grammatical faux pas when he declared that his mission was "_to boldly go_ where no man has gone before." According to them, he should have said "to go boldly" or "boldly to go."

False rules such as these come from a misguided application of Latin grammar rules to English. For example, in Latin, "to love" is one word: _amare_. In fact, all Latin infinitives are one word, making them impossible to split. But English is different, and grammar guides like the _American Heritage Dictionary_ now claim the rule forbidding split infinitives is obsolete.

### 9. Misusing words can keep your writing from being taken seriously. 

Many writers have paused to contemplate whether they should use "who" or "whom," a distinction that confuses even experienced writers.

The distinction between _who_ and _whom_ becomes clear, however, if you do this simple exercise. Take this example sentence: _She tricked him_. Our sentence can be transformed into two distinct questions: _Who tricked him?_, which removes the word "she," and _Whom did she trick?_, which removes the word "him."

When trying to decide whether to use "who" or "whom" you need to decide if the unknown person is the actor (Who tricked him?) or the one being acted upon (Whom did she trick?).

When "whom" is used correctly and sparingly, it's not at all distracting. But even esteemed writers like William Shakespeare and language expert William Safire preferred using "who" in all cases to keep their prose direct and conversational.

But more glaring errors occur when, instead of finding the correct word, people use a word that _sounds_ like it. Such a mistake is called a _malaprop_ and, to avoid embarrassment, it's best to avoid malaprops altogether.

For example, if you aren't interested in something, don't say you're "disinterested" (that is, unbiased). Instead, say you're "uninterested."

Likewise, if you're bored or unimpressed, you're likely to confuse readers if you write that you are "nonplussed," that is, stunned or bewildered.

Similarly, it's in your best interest to know the difference between commonly misused words like "phenomenon" (a singular event) and "phenomena" (multiple events), "tortuous" (twisting or winding) and "torturous" (painful), "proscribe" (to forbid) and "prescribe" (to recommend), and "reticent" (shy) and "reluctant" (unwilling).

If you're ever unsure, just consult a dictionary. The time it takes to look it up is well worth the benefit of not embarrassing yourself with a malaprop!

> _"As far as I'm concerned, whom is a word that was invented to make everyone sound like a butler." - Calvin Trillin_

### 10. Final summary 

The key message in this book:

**It can take a lifetime to master the art of writing, but there are some simple things you can do today to make your texts more eloquent, cogent and convincing. By bringing just a little attention to detail in your writing, you can jettison some bad writing habits and make your writing a joy to read.**

Actionable advice:

**Read your text aloud.**

If you don't have the time to have someone read over your writing before you send it off to the publisher, take the time to read your draft aloud. Clear writing should have a rhythm to it. Confusing writing, on the other hand, will usually sound stilted and clumsy. 

**Suggested** **further** **reading:** ** _The Language Instinct_** **by Steven Pinker**

_The Language Instinct_ provides an in-depth look into the origins and intricacies of language, offering both a crash course in linguistics and linguistic anthropology along the way. By examining our knack for language, the book makes the case that the propensity for language learning is actually hardwired into our brains.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Steven Pinker

Steven Pinker, who serves as Chair of the Usage Panel of the _American Heritage Dictionary_, is an award-winning linguist and cognitive scientist. A professor at Harvard University's Department of Psychology, he is the author of numerous best-selling books, including _Words and Rules_ and _The Language Instinct_.

