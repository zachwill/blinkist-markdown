---
id: 5547789d3666320007690000
slug: the-seven-principles-for-making-marriage-work-en
published_date: 2015-05-06T00:00:00.000+00:00
author: John M. Gottman and Nan Silver
title: The Seven Principles for Making Marriage Work
subtitle: A Practical Guide from the Country's Foremost Relationship Expert
main_color: E3B82D
text_color: 7D6519
---

# The Seven Principles for Making Marriage Work

_A Practical Guide from the Country's Foremost Relationship Expert_

**John M. Gottman and Nan Silver**

_The Seven Principles for Making Marriage Work_ (1999) draws on data from relationship studies and interviews to do exactly what it promises in its title. These blinks take you through the key changes you can make to overcome the common problems that damage relationships and build a supportive, romantic marriage.

---
### 1. What’s in it for me? Discover the keys to a happy marriage. 

As much as we all want to live in perfect relationships, few of us are in a completely stress-free set-up with zero tension, conflict or problems. In fact, if you're married or in a relationship, chances are it can be improved.

Enter relationships expert John M. Gottman. He's been working with and researching couples for years, and has compiled a list of the fundamental problems that every marriage faces — and their solutions. These blinks explain each one of his seven fundamental principles and discuss ways in which you can avoid becoming another unhappy statistic.

After reading these blinks, you'll know

  * how one couple eliminated marriage stress by waking up ten minutes earlier every day;

  * how to talk to your mother-in-law; and

  * why boring conversations are the secret to successful marriages.

### 2. Principle One: Functioning couples have a richly detailed love map. 

If you have a husband or wife, you'll likely be wise to plenty of special, private or sensitive information about them. But did you know this information is actually stored in our minds in a _love map_?

The more developed your love map, the stronger your love.

In several recorded cases of divorce, partners weren't actually that familiar with each other; they didn't dedicate enough space in their brain for the marriage. With no love map, you can't fully know your partner, and without really knowing your partner, how can you truly love them?

Take the example of Rory, a pediatrician who ran an intensive care unit for babies. Being a workaholic, he often slept at the hospital overnight. At work he got along well, but his homelife was a different story. He didn't know the family dog's name or where his house's back door was. In fact, he had become so tangled up in work that his emotional connection to his wife and children had deteriorated. His love map left a lot to be desired.

It's important to be aware of your love map because it contains your own and your partner's aspirations and life philosophies. But it can change. Having a baby can radically alter a woman's life purpose or direction, and thus change her love map.

Take Maggie and Ken, who had been together only a little while before they chose to marry and start a family. They were aware of each others' beliefs, hopes and fears; Ken understood that Maggie was dedicated to her career as a computer scientist. However, when Maggie gave birth to her daughter, she put her career aside in order to stay home and look after the baby. Ken saw that his wife had changed.

This is not unusual; becoming a parent changes your values and identity. Maggie's love map had changed to fit her new priorities, and Ken had to realign his own love map accordingly.

### 3. Principle Two: If a couple has a fondness and admiration system, their marriage is salvageable. 

If your marriage is on the rocks how do you know if it's salvageable? The key is to assess your feelings when you both think of the past you've shared together.

To see if a couple still have what is known as a _fondness and admiration system_ — where both partners share a sense of respect and appreciation toward one another — ask them how they see their history.

If some positive feelings remain within the marriage, partners will speak emphatically about how they first met, their first date, and so on. If a marriage is really struggling, however, recalling beautiful moments with your partner will feel like trying to get blood from a stone.

The way you see your shared past is key: 96 percent of couples who see their marriage history in a positive light are likely to enjoy a happy future.

Remember Rory the pediatrician in the previous blink? During one counseling session, he recalled the early days with his wife with love, admiration and respect. This helped him realize how much he wanted to experience those feelings again. To fix the rut he'd fallen into, he overhauled his work schedule, trained someone to assist him at the hospital, and he now is home for dinner every night.

Rory's fondness and admiration for his partner were also antidotes to growing apart. If you have no fondness or admiration system and don't believe that your partner deserves your honor and respect, it makes having a successful marriage impossible.

So how can you assess your fondness and admiration system? One way is to answer true or false to these statements:

When we're apart, I think of my partner positively.

I can easily list three things I admire in my partner.

My partner is happy to see me when I come into a room.

If the answers are true, your fondness and admiration system is pretty solid!

### 4. Principle Three: During brief and seemingly trivial chitchats, couples turn toward one another. 

Here's an intriguing truth about marriage — no matter how dull they seem, the important moments in a marriage are the daily conversations you have with your spouse.

In marriage, spouses frequently make "bids" for their partner's attention, affection, humor and support.

When you take a brief pause in your work day to give some attention to your partner and his or her worries, you _turn toward each other_, meaning you reinforce your marriage and maintain romance.

For example, when your wife confides in you one morning that she had a horrifying nightmare and you reply with, "I have to get to the office, but tell me about it now and we can discuss it tonight," instead of brushing her off with "I don't have time today," you enrich your marriage with romance. Romance endures when you signal that your spouse is valued during the monotony of daily life.

This is what is meant by turning toward your partner. To do this well, you have to be cognizant of how vital these banal moments in everyday conversation are.

Many couples can improve stability and romance simply by realizing that they should avoid taking their everyday interactions for granted; it's more likely that your spouse's emotional needs are neglected out of mindlessness rather than spite.

If your spouse is thoughtful enough to call you one morning to see how your meeting went, you should be conscious not to take this for granted, even if it becomes a daily habit. Remember these considerate moments and practice some gratitude for the attention your partner gives you.

You can turn toward your partner any time, so the next time you're chatting with them, focus on enjoying it rather than seeing it as mindless nattering.

### 5. Principle Four: Partners should let each other influence their decisions by taking each others’ opinions and feelings into account. 

We all value fairness and equality in society, and marriage is no different. These qualities greatly influence the arguments and discussions you have with your spouse.

For instance, if husbands don't demonstrate honor and respect to their wives, other gender conflicts can be exacerbated.

Although wives can get aggravated by their husbands, they seldom react by _increasing_ negativity. On the contrary, husbands tend to react in a way that intensifies their wives' negativity.

If a husband yells "You're not listening!" the wife often replies with "Sorry, now I'm listening," in order to prevent an argument. But if a wife starts with "You're not listening to me!" the husband likely either ignores her, gets defensive ("Yes, I am!"), becomes critical ("I don't listen because you never make sense.") or displays contempt ("This is a waste of my time."). All these reactions escalate a minor conflict and demonstrate that the husband couldn't care less about what his wife thinks.

It's useful to note that most long-term, stable marriages are those in which the husband treats his wife with respect. A long term, 130-couple study by the author showed that marriages in which husbands allow their wives to influence them are happier and less likely to end in divorce that those with husbands who don't let their wives influence them.

Indeed, there's an 81% chance his marriage will fall apart when a man refuses to share any power with his partner.

For example, Jack wanted to buy a used Honda from Phil. But Jack promised his wife he wouldn't go ahead until a mechanic had checked it. Phil ridiculed Jack for letting his wife advise him about cars. But Jack kept his word, and it's a good thing he did: the mechanic actually found a fault with the transmission. Thanks to his wife's influence, Jack didn't waste his money on Phil's Honda after all.

### 6. Principle Five: There are two kinds of marital conflicts – solvable and perpetual problems. 

It's no secret that conflicts happen in every marriage. But even if they happen frequently, you can still clear them up easily and save yourself some stress.

_Solvable_ problems in a marriage are minor problems that can result in excessive tension and pain.

Just because the problem is solvable doesn't mean it actually gets resolved. This is because couples often haven't learned how to deal with them. Sometimes all it takes is for each spouse to begin a discussion more calmly and monitor themselves as it goes on, keeping alert for alarm bells such as emotional flooding — i.e., when we become overwhelmed by feelings.

To monitor yourself, take notice of gestures, facial expressions and vocal pitch — don't scream at your wife when you see she's already in tears!

Take the case of Rachel, who thinks her husband Jason drives too fast. He claims speeding is necessary because she takes forever to get ready for work. Rachel argues that it takes her so long because he takes ages in the shower every morning. If they just took a broader view here, they'd see this clash is solvable: waking up a mere ten minutes earlier each day could solve it.

Unlike solvable problems, which can be short-lived, most marital problems are _perpetual_, meaning they keep occurring over and over again.

69 percent of couples have a conflict that they have been rowing about for many years. Even though there often is no real solution, they must keep recognizing the problem and talking about it in order to deal with it. Remember, perpetual issues don't necessarily equal a disastrous marriage!

Some perpetual problems can signify more serious issues, but there are ways around them. For example, Tony wants to have his children raised Catholic, yet his wife Jessica is Jewish and wants their children to follow her faith. How do they cope with this perpetual conflict? They have a sense of humor about it, and don't let their religious preferences spoil their love for one another.

### 7. Principle Six: When you feel gridlocked because of a problem that can’t be solved, you have to learn to cope with it. 

Now you know what kinds of conflicts can weasel their way into your marriage, but what do you do if you think they're unsolvable and you feel trapped?

In these cases, you need to get out of _gridlock,_ which is that feeling of being caged by the same problems that come up over and over again. The aim of this is not to eradicate the problem, but to convert it into a dialogue.

Even if the issue seems unsolvable, you should keep working at it and try to target what is actually feeding the conflict.

If you blame _him_ for doing something wrong or _her_ for being demanding, you need to acknowledge that _you_ are contributing to the conflict, too.

For instance, Laura thought Mike was a slob and she had endless housework to do because he never pulled his weight, leaving rooms in complete disarray. Eventually she decided to raise the issue with Mike and it turned out he never actually realized Laura cleaned up after him — he would have helped or done it himself if he had been aware. By having a dialogue, Laura freed herself from gridlock immediately.

Experiencing gridlock is a sign that you have hopes for your marriage that aren't respected by your partner. These hopes can be anything from reaching a certain salary at work, to having a spiritual awakening. Your partner must learn how to deal with your desires and respect that they are a part of you.

John and Amy, for example, quarrel about where to eat on Sunday evenings. Whereas John wants Amy to cook and eat at home, like his mother did when he was a child, Amy prefers to go to a restaurant to feel special. In this case, the conflict is symbolic of what makes each of them feel loved. To settle their dispute, they need to respect each other's wishes and take turns.

### 8. Principle Seven: You don’t have to agree on what is meaningful about your lives together. 

"Birds of feather flock together." Hmmmm, not quite! You can take joy in a long-lasting marriage even if you and your partner see life very differently, but you still need to share things with one another.

Marriage has a spiritual aspect to it, and for that to develop you must build a sense of shared meaning. It's extremely difficult to live harmoniously together without being familiar with each others' values.

Sometimes young couples want to have a relaxed relationship, but a lot of the time they run the risk of not knowing enough about one another's values and aspirations.

Take Kevin and Helen, who wanted a marriage with their own independent careers, interests and social circle. But trouble arose when Helen noticed she didn't feel connected to Kevin. There was no real family feeling to their relationship, so they sat down together and shared their past, childhood and family values with one another. They were then able to connect in a way they hadn't before and find some common ground.

You don't have to be two peas in a pod to have a stable marriage, but the more shared meaning you can find, the deeper and more fulfilling your relationship will be.

Your marriage will further deepen when both of you agree on the roles you play in the relationship.

All of us have a specific idea of who we want to be and which role we want to play in a marriage. To have this work in the long term requires communication and agreement. For instance, Ian and Hilary held the view that the husband should be the protector and provider, while the wife should fulfill the nurturer role. In contrast, Chloe and Evan desired an egalitarian marriage, where they could support each other emotionally and financially.

Whichever way each couple wants to be, the most important thing is that the couples agree on their roles, and enjoy happy and long-lasting marriages, as the couples above did.

### 9. There are various signs which indicate divorce is likely. 

Ever had a sneaking suspicion that your friend's marriage was doomed? Could you pin down why you got this feeling? Well, there are many factors that can indicate that a marriage is heading for the rocks.

Four of the largest indicators, which the author labels "the four horsemen of the apocalypse," are _criticism, contempt, defensiveness_ and _stonewalling._

Let's start with criticism, usually the first of the four horsemen to appear in a marriage. In every relationship, there will be complaints about mistakes, missed chores or forgotten anniversaries. But a warning sign of a troubled marriage is when these complaints turn into criticism. Whereas a complaint focuses on a specific failure, for example, "You forgot to take the trash out again!", criticism highlights a fault in your partner's character, such as "The trash was left again — you're so lazy!"

Mild criticism is common in a marriage, but if left unchecked, it can lead to the next, more dangerous, horseman: contempt.

Contempt is snarling or mocking behavior designed to undermine your partner and make them feel small and useless. Contempt is toxic because it leads to further conflict. After all, if your partner constantly tells you that you're useless, you'll feel compelled to become belligerent and aggressive in return.

And this is when defensiveness, the third horseman, usually enters the stage.

Faced with a contemptuous partner, you respond by getting defensive and arguing that your behavior isn't as bad as they say it is. For example, if your partner sneers at your spending habits, your response will probably be something like "I don't spend that much! I know lots of people who spend much more than I do."

Of course, being defensive won't soothe any problems, it will only lead to more contempt and defensiveness.

Now we turn to the final horseman, stonewalling. When someone has experienced enough contempt and criticism from his partner, he'll disengage from conversation. Rather than becoming defensive, he'll respond to an attack with an "Uh-uh," or "Sure," or by avoiding face-to-face interaction.

Witnessing the approach of each horseman is a sign that a marriage is in jeopardy. So keep an eye out for them.

> _"The more you can imbue your relationship with the spirit of thanksgiving and the graceful presence of praise, the more meaningful and fulfilling your lives together will be."_

### 10. Typical marital challenges which can be solved are in-laws or a new baby. 

There are plenty of naysayers when it comes to the concept of lasting marriage. But let's examine the two most frequent conflicts that marriages face, which are thankfully solvable.

The first conflict is true for many: your relationship with in-laws can wreak havoc on your relationship because both sides seem to fight for the love of the same person.

Parents-in-law sometimes find it tough to share their child with someone else. This is especially hard when they think they know best for a newborn baby, how and where you should live, and so on.

To deal with this, the son or daughter should emphasize to his or her parents that they have their own family now, and it's their new priority.

Consider David. David's parents came for a visit and his wife Janie made a reservation at her favorite Italian restaurant. She was excited about it because her parents-in-law were Italian. However, David's mother showed up with his favorite dish, saying she forgot about the reservation. To handle this, David put the dish in the refrigerator and they went to the restaurant as planned. This showed his mom that Janie was his priority now, and that his mother must adjust to it.

Big challenges also arise with a newborn. Whether or not a marriage endures this depends on whether the husband experiences the transition to parenthood with his wife or not.

Having a baby transforms the new mother. Her love becomes deeper and selfless and her life takes on new meaning. But often the bond between her and her partner can fall to the wayside. For example, when Lucy became a mother she also became a supervisor. Her husband John wanted to be a good father, but couldn't seem to do anything right; Lucy disapproved of everything he did. This meant he did less and less and began to feel excluded.

To avoid this, mothers must remember that the baby is the father's child, too, and that it will benefit from multiple parenting styles.

### 11. Final summary 

The key message in this book:

**A long-lasting marriage is possible! All it takes is becoming conscious of how you think about and react to your partner and your behavior toward them not just in daily life, but also in conflicts. By practicing some straightforward healthy behaviors, you can enjoy a harmonious partnership with your spouse.**

Actionable advice:

**Test your love map with these true/false statements.**

I can name my spouse's best friends.

I know my spouse's current biggest worries.

I can tell you my spouse's philosophy on life.

I can list my spouse's favorite music.

**Respect your stress response.**

A stressed body massively impairs your ability to solve a problem, so take a break for a few minutes until your body has noticeably calmed down before attempting to resolve a conflict with your partner.

**Suggested** **further** **reading:** ** _Attached_** **by Amir Levine and Rachel S. F. Heller**

_Attached_ is all about how to make your relationships work. This book offers you valuable insight into the science of adult attachment and how to use this insight in everyday life, whether you're in a committed relationship or are still looking for love. It also provides tips and tricks on how to find the perfect partner and reveals why some people just aren't compatible.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### John M. Gottman and Nan Silver

John M. Gottman is a psychology professor at the University of Washington and co-founder and co-director of the Seattle Marital and Family Institute. His research into romantic couples has spanned 40 years and he has earned several prestigious academic awards for his work.

Nan Silver is a blogger, journalist and a _New York Times_ -bestselling author. She has also been editor-in-chief of _Health_ and a contributing editor at _Parents_ magazine.

