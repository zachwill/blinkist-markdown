---
id: 57fbe2d470ec6300032cf66f
slug: the-seven-sins-of-memory-en
published_date: 2016-10-13T00:00:00.000+00:00
author: Daniel L. Schacter
title: The Seven Sins of Memory
subtitle: How the Mind Forgets and Remembers
main_color: DF5457
text_color: AB4043
---

# The Seven Sins of Memory

_How the Mind Forgets and Remembers_

**Daniel L. Schacter**

_The Seven Sins of Memory_ (2002) offers a close look at the seven ways our memory can let us down: from why we always lose our car keys to why some people are haunted by recurring negative thoughts. The book also discusses how to mitigate these shortcomings and why they're actually trade-offs for massive memory benefits. By examining how our memory works and its faults, we see that these weaknesses are actually just side effects of a very clever system.

---
### 1. What’s in it for me? Learn about the frailty of human memory. 

We've all been there: you're in a rush to get to an important meeting, when suddenly you forget where you left your keys the night before. You wrack your brain trying to think of where they might be — you could swear you left them right on the kitchen table!

How could such a small, simple memory drift from your mind? Well, this kind of forgetfulness is simply a product of how our brains function. The human mind has evolved with this flaw — and it's not the only flaw in our ability to remember.

These blinks highlight seven frailties of our memories. While these faults are a product of human evolution, we can still overcome them with the right techniques and hacks.

In these blinks, you'll discover

  * why you're more likely to remember a baker's occupation than his name;

  * why traditional police lineups can lead to miscarriages of justice; and

  * how false memories can lead innocent people to admit to murder.

### 2. All memories are liable to fade and weaken over time, but there are ways to keep the memories you want. 

Have you ever rolled up to a party and been greeted by someone you didn't recognize, only to find out that you'd met just a few months before? These kinds of embarrassing moments can happen to anyone and are caused by the first of the seven deadly sins of human memory: _transience._

Memories are transient — many memories disappear over time without us realizing it.

A study conducted by a group of researchers in California demonstrated just how faulty our memories can be over time. The study was carried out after O. J. Simpson's notorious acquittal in his famous 1995 murder trial.

Researchers asked a group of students to provide detailed descriptions of how they came to learn about the trial outcome. Fifteen months on, only 50 percent could give accurate accounts of where they were; three years on, this dropped to 30 percent.

This isn't a surprising discovery, and the fact that our memories lose accuracy over time is something we've known for a while. In 1885, the German philosopher Hermann Ebbinghaus developed the _forgetting curve_ to show how memories fade over time.

Ebbinghaus memorized a list of nonsense words and tested himself. After nine hours, he had forgotten 60 percent of them. By the end of the month, 75 percent were obliterated from his memory.

The good news is that there are techniques you can use to combat transient memory loss.

Memory techniques have been around for a long time. The ancient Greeks developed memory tricks called _mnemonics_, which work by linking new information to places or numbers, making the new concepts much easier to recall than decontextualized bits of info.

In everyday life, a simple way to harness the power of mnemonics is to associate new information to concepts and images that are already meaningful to you.

So, say you meet a well-built man named Bruce. Following in the Greek tradition, imagine how Bruce would "bruise" you in a scuffle. Make the image vivid enough, and next time you see him you'll have no problem remembering Bruce the bruiser!

> "_Thinking and talking about past experiences is the best way to remember them."_

### 3. Sometimes we fail to remember something because the memory never registered properly in the first place. 

You might think that a memory champion who can recall thousands of random words would never forget to pick up a carton of milk from the store. But that's not actually the case — _absent-mindedness_ is an entirely different kind of memory failure _._

_Absent-mindedness_ means you're not paying enough attention to the task at hand, so incoming information is either partially encoded or not at all.

Rather than being a sign of cognitive decline, everyday frustrations like forgetting where you put your car keys often mean that your mind is simply somewhere else.

An experiment carried out by the psychologists Christopher Chabris and Daniel Simons demonstrates the extent to which focusing on one thing can lead a person to totally ignore something else. The psychologists had participants watch a video of people passing a basketball, and asked them to count the number of passes.

At some point in the video, a man in a gorilla suit runs onto the court and beats his chest. Astoundingly, when asked how many people had seen something unusual in the video, only half the participants had noticed the man in the gorilla suit!

At other times when we forget something, it isn't because we haven't stored the information, but rather because we haven't set appropriate cues to remind ourselves. The key to remembering to do something is to set up an appropriate cue that both _informs_ and is _available_.

For instance, it's not going to be very useful to have a piece of string tied around your finger as a cue to feed the cat if you can't remember what the string is there to remind you of. Likewise, writing a detailed reminder in a notebook is completely useless if it's not there to cue you when you need it.

Putting your medication by your toothbrush is a perfect example of an appropriate cue; it's a clear visual nudge at the time you need it, morning or night!

As we'll see in the next blink, though, there are times when you simply can't remember something, no matter how many cues you're given.

### 4. Even when we’ve definitely memorized a piece of information, a mental block can stop our recall. 

We all know the feeling of not quite being able to remember someone's name. You swear you know it, you're positive you do, it's right on the tip of your tongue but . . . !

And, in fact, you do know the answer — the only issue is that it's being _blocked_.

Blocking names is a common occurrence, and it happens more often with proper nouns. For example, we are much more likely to forget that someone's last name is Baker, than we are to forget that a person _is_ a baker. Researchers believe this is because proper nouns usually don't convey any other information apart from the name itself, so only a single connection is made in the brain.

This is different from remembering someone's occupation, like a butcher or baker, which connects to information about where a person works and how they spend their time. This information, in turn, makes far more associations in your brain, which makes the occupation easier to remember.

Another reason why common nouns are far easier to recall is that there are usually a number of synonyms available: a car, for example, can also be referred to as a vehicle or an automobile. Again, more connections makes it easier to remember.

A proactive step you can take is to make more associations for information susceptible to blocking, like the name of someone you see infrequently.

Maybe it's Murray in accounting. You have the most awkward run-ins with him at the coffee pot, because you simply can't remember his name. Well, then imagine marrying Murray, and you'll create enough hilarious wedding day images to cement his name in your head.

And if the tip of the tongue sensation — "His name is on the tip of my tongue, I swear!" — happens to you a lot, rest assured that it is a universal human experience.

A study conducted by cognitive psychologists found that out of 51 languages studied, 45 featured the tongue in their descriptions of this specific phenomenon.

The Korean language has a lovely way of describing the experience: a word is "sparkling" at the end of the tongue!

### 5. We mix up memories of people, places and things more often than we think. 

If you've ever confused the name of your doctor with that of your dentist, you already know about the next sin: _misattribution._

We often misattribute things we've seen in different situations.

The famous manhunt for a second suspect in the 1995 Oklahoma City bombing was a classic case of misattribution _._ A witness claimed he'd seen a second man with bomber Timothy McVeigh when they came into his store to rent a vehicle. The witness gave detailed descriptions of the man, down to the tattoo on his arm.

It was an accurate description of a real person — unfortunately, it referred to an innocent soldier who had rented a vehicle the next day in the company of a man who matched the description of Timothy McVeigh.

This kind of thing can happen easily and often because people remember faces well, but not where or when they saw them. If information is not firmly linked to a time or place, it gets mixed up with other loose memories.

Unlike misplacing the toenail clippers or a belt, though, the dangers of misattribution have serious real-life consequences, especially in the criminal justice system.

A recent study of criminal cases where DNA was used to overturn wrongful convictions found that a whopping 90 percent of the cases involved incorrect eyewitness identification.

A number of steps have been taken to mitigate the effect of misattribution. For instance, police lineups no longer show the witness all the suspects at once, since witnesses tend to select the person who most resembles the culprit — even when the real culprit isn't among them. These days, witnesses signal a thumbs up or down for each suspect. This practice encourages them to scrutinize their memories more carefully.

As we'll learn next, our memories can also change because of our inherently suggestible nature.

### 6. Suggestions from other people can drastically change our memories. 

What is your earliest childhood memory? How much detail do you have of it and how much has it been influenced by what others have told you? It turns out we are highly _suggestible_ when it comes to remembering past events.

Being asked leading questions about past memories can actually change what we remember. This was made crystal clear when a group of Dutch psychologists interviewed their university colleagues ten months after a tragic plane crash in 1992. The crash killed 43 people when a cargo plane crashed into an apartment building in Amsterdam.

The researchers asked the subjects a very clear question: "Have you seen video footage of the plane crashing into the building?" Over half of the respondents answered in the affirmative.

In a subsequent study, the number of respondents who said yes rose to two-thirds. Not only that, but this time they also described details from the footage, such as the angle of the plane's descent and the aftermath of what happened.

You may have already guessed that there never was any such footage of the plane crashing. The study is remarkable because it shows one suggestive question — with its implication that there was video — was all it took for respondents to create detailed false memories.

Naturally, understanding suggestibility is critically important when dealing with the law.

Obtaining false testimonies from people doesn't require anything as drastic as physical torture; simply asking and repeating leading questions can cause suspects to question their own memories.

In one memorable instance, a young man from London was questioned by police about a brutal murder. Soon thereafter, he began seeing visions of the victim and turned himself in. Within a day he went from proclaiming his innocence to giving a full confession. The man spent 25 years in prison before his sentence was overturned in light of new evidence!

Thankfully, the development of recent research has spurred improvements in interview procedures to avoid asking suspects leading questions that could induce false memories.

### 7. We unknowingly alter our memories to fit with our current world views. 

Have you ever had to choose between two objects you liked equally? If someone asked you a day later which one you preferred, you'd probably say you much preferred the the object you chose, even though they were equal a day before. This is a classic illustration of _consistency bias._

In other words, you're likely to believe your views are the same now as they were in the past.

Unless something explicitly happens to make you abandon your original choice, you're likely to align your past beliefs with your present ones.

This consistency bias helps us create easy narratives that make sense to us. Beliefs about relationships are excellent for studying this bias. In one study, couples were asked about their relationships twice over the course of four years. When someone answered both times that the relationship was going well, they easily remembered the way they answered the question the first time (that the relationship was good). But when someone answered first that the relationship was good, and then later that it was not good, consistency bias kicked in. They remembered (falsely) saying that the relationship was not good the first time, too. They thought the relationship was _always_ not good.

Bias works in the opposite direction, too. A _change bias_ occurs when happy couples tend to believe that their love for each other has increased, when in fact it has stayed constant.

The left hemisphere of the brain is responsible for the construction of narratives and biases. Michael Gazzaniga conducted studies at Dartmouth College with patients who had severed the connection between their two brain hemispheres. Since each side connects to one eye, Gazzaniga could show a particular image to a particular hemisphere by using a dividing screen.

What he found was astounding: the left hemisphere will always try to create a narrative to explain our actions, even if it doesn't know what it's talking about!

In one experiment, a patient's right hemisphere was flashed a card with the simple instruction to walk. Once up and walking, the left side of the brain searched for an explanation of the action. The patient would announce they were going to the restroom or to get a sip of water — this was the left side of the brain's explanation for why the body was suddenly walking!

> "_Current knowledge, beliefs and feelings can influence our recollections of the past and shape our impressions of the people and objects in the present._ "

### 8. Highly emotional events are the ones we remember most. 

In 1986, Donnie Moore, a pitcher for the California Angels, was one strike away from winning the American League Championship Series for his team. But instead of tasting victory, Moore threw a pitch that the Boston Red Sox's Dave Henderson launched into the left field stands; the Red Sox would go on to win the series and reach the World Series. Just three years later, haunted by the memory, Moore shot his wife and killed himself. This is an extreme case of memory _persistence_.

Everybody has positive and negative memories that are persistent, but individual thinking styles affect their impact on each of us.

Persistent memories are the result of events that have a big emotional impact. If you're hit by one of these and happen to have a _ruminative_ style, where you obsess about the negative past, you're in danger of getting stuck in negativity loops.

A psychology study done by the University of Michigan assessed the moods and emotions of a group of students after an earthquake. Those who tended to obsess about negative aspects were far likelier to suffer from depression after the earthquake. The more they fixated on the traumatic event, the deeper their depression.

What's the solution, then? Well, trying _not_ to fixate on negative memories actually isn't one. 

Harvard psychologist Daniel Wegner conducted a study that showed just how counterintuitive it is to try to force yourself not to think about something.

Participants were asked not to think about certain topics, like a white bear or an emotionally significant ex-partner. Although some of the participants were able to suppress the thoughts for a while, they would eventually come back in a _rebound effect_, returning with greater intensity.

Fortunately, not all is lost. James Pennebaker at the University of Texas found that talking or writing about persistent memories, which puts stressful experiences into a narrative context, helps us cope with them. Positive effects also include improved mood and a boosted immune system.

This method of getting over disturbing memories has been found to lessen their power. This is especially great news for people vulnerable to debilitating flashbacks, such as soldiers and refugees — but it can work for you, too.

Now that we've gone over the seven sins of memory, we'll see in the last blink that they aren't really that bad after all.

> _"Persistence, then, thrives in an emotional climate of disappointment, sadness, and regret."_

### 9. The negative aspects of our memory are really just side effects of highly successful mechanisms. 

Think of a table, any table. You probably conjured up an image of one you use all the time, maybe even the one you're sitting at now. But now imagine if this simple request had prompted you to remember every single table you'd ever seen. It would be pretty difficult to function, wouldn't it?

This is just one example of the flip side of memory's "sins." What can seem like weaknesses in our memory are actually useful adaptations to our environment.

Consider the mechanism of absent-mindedness: events we neglect to pay attention to aren't registered in our memories. The extreme opposite of this would be remembering every last detail of every single thing you encounter!

Neuropsychologist Alexander Luria studied a Russian patient with an extraordinary memory. While the patient could remember pretty much everything he had ever seen, he suffered acutely in his day-to-day life, because he couldn't organize any of that vast amount of information into useful categories.

Another advantage of absent-mindedness is that it allows us to work on auto-pilot. We often perform tasks like showering or brushing our teeth automatically, which enables us to think about other things at the same time — grocery lists, holiday plans, or whatever it might be!

Without these sins of memory, we would be less successful as a species.

For instance, persistent memories suddenly don't seem so bad in the context of prehistoric man. Think about it: his traumatic memory was probably more closely linked to life-or-death situations, like the location of a deep pit or a predator's sharp teeth. Remembering what caused him physical or psychological harm helped him avoid it in the future, and pass on his genes to us!

Similarly, _positive illusions_, a result of biases, can actually be useful. While a completely out-of-touch perspective of events can be harmful, having a positive outlook helps you avoid the pitfalls of depression and will make you more likely to attempt future challenges that might have a big pay-off.

So for all of human memory's pitfalls, it's actually a pretty brilliant system that helps us remember the past and navigate the present.

### 10. Final summary 

The key message in this book:

**Although there are clear weaknesses in our memory when it comes to recalling information exactly when we want it, these failings are trade-offs for a highly sophisticated system that allows us to function with relative ease in the present moment.**

Actionable advice:

**Make meaningful associations for the memories you want to keep.**

Whether it's the name of a colleague you don't often see or a series of cherished childhood memories, by talking and thinking about them you are more likely to remember them in the future.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Moonwalking with Einstein_** **by Joshua Foer**

_Moonwalking_ _with_ _Einstein_ takes us on the author's journey towards becoming the USA Memory Champion. Along the way he explains why an extraordinary memory isn't just available to a select few people but to all of us. The book explores how memory works, why we're worse at remembering than our ancestors, and explains specific techniques for improving your own memory.
---

### Daniel L. Schacter

Daniel Schacter is the head of the department of psychology at Harvard University. He is a fellow of the American Academy of Arts and Sciences, and has won numerous awards for his research and writing on memory and neuropsychology.

