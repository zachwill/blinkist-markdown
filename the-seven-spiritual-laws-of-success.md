---
id: 576bac11e0dd250003d615d0
slug: the-seven-spiritual-laws-of-success-en
published_date: 2016-06-30T00:00:00.000+00:00
author: Deepak Chopra
title: The Seven Spiritual Laws of Success
subtitle: A Pocketbook Guide to Fulfilling Your Dreams
main_color: 815D43
text_color: 694B36
---

# The Seven Spiritual Laws of Success

_A Pocketbook Guide to Fulfilling Your Dreams_

**Deepak Chopra**

_The Seven Spiritual Laws of Success_ (2007) is about the forces in the universe that can lead you toward the perfect job, increased wealth and well-being, strong relationships and, most importantly, the ability to enjoy what you have this moment. Discover the seven universal laws of success and tap into the endless energy and abundance of the universe!

---
### 1. What’s in it for me? Find success wherever you seek it! 

Success. We all want it, even though definitions of success differ from person to person. Regardless of what kind of success you want to achieve, however, there are certain things you should focus on. We're not talking about getting perfect grades or nailing every job interview. Success depends on factors much deeper and more spiritual than that, and they can all be summed up in seven spiritual laws.

These seven laws have to do with how you connect with your true self and how you interact with others. They are about the flow of energy in the world, about being in the here and now without struggling against change. So let us take a journey and explore how the seven spiritual laws of success will help you reach your goals.

In these blinks, you'll find out

  * why saving money is ruining your flow with the universe;

  * how letting go of your desires will help you get what you want; and

  * that complaining about where you are makes it harder to get where you want to be.

### 2. Discover your true Self and dissolve the ego. 

Have you ever been alone in nature and experienced a moment of profound silence? If so, you may have felt a sense of peacefulness and connectedness with the universe.

It is in these peaceful moments that you can find your _true Self_ and unlock the potential to fulfill your life's desires.

When you feel connected to your environment and the people within it, you realize you are not a separate entity that is cut off from the rest of the world. You can sense that the world is a connected field of energy that unites everyone and everything on the planet.

But not _just_ the planet. Indeed, this field of energy is a boundless source of power for the whole universe.

Once you feel this connectedness and realize that you are not merely a solitary human being, but an integral part of the complex weave of nature, you may discover that your true Self knows no limitations.

By tapping into the energy within your true Self, you can achieve anything you wish, be it wealth and success or spiritual development.

But, in order to connect to your true Self, you must first dissolve the ego.

As long as you identify with your ego, you will continue to think of yourself as separate from the rest of the universe — a mere brain inside a physical body, confined and limited. And due to this restricted point of view, you will continue to be fearful, obsessed with survival and focused on external things such as social status and how other people see you.

When your ego is in charge, you lose your connection with your true Self and, with it, that endless field of potential energy.

Fortunately, you can always reconnect with your true Self by practicing silent meditation.

To do this, calmly focus your attention on your breathing and observe your thoughts without judgment. Gradually, you will become peaceful, and when your mind and body become still, your ego dissolves, allowing you to reconnect with your true Self.

> _"Be still, and know that I am God."_ \- The Bible

### 3. Embrace your current situation and practice defencelessness. 

People spend a lot of time and energy worrying about the circumstances they find themselves in. For instance, a man might fret about being partnerless. But then, when he enters a relationship, new worries arise. Is this relationship limiting his freedom? Is he truly in love?

Thankfully, there is a solution to the problem of chronic fretting. You can simply accept your current situation.

For example, imagine living with a difficult roommate; she never cleans up after herself yet gets angry with you when the place is a mess. This is a tricky situation, one that might make you consider moving out. But moving takes time and money, so what's the best way to resolve the issue?

The answer: Don't fight the problem. Instead, face it and accept it for what it is. There is always a lesson to be learned from difficult situations, and the people who make life difficult are usually there to teach you something.

In this case, the lesson might be about standing up for yourself or learning how to be less affected by the actions of others. Only by accepting the situation can you begin to learn from it and grow.

You can take this acceptance a step further and save energy by practicing _defencelessness_.

Practicing defencelessness essentially means getting rid of the need and desire to convince others that your opinion is right or that theirs is wrong.

For example, you've probably witnessed diet-related confrontations between vegetarians and omnivores. Before long the conversation can escalate into a full-blown argument about the ethicality of poultry farming.

Not only is this a waste of energy; it will inevitably end with both parties more entrenched in their own positions than before.

Defencelessness, in contrast, erases the need to convince others of your viewpoint. It allows for a true exchange of ideas about a topic, without anger or dogmatism getting in the way.

In the above example, defencelessness would mean both parties could openly share their thoughts on their dietary choices without feeling that the other is judging them, and hence without becoming defensive and hostile.

> _"An integral being knows without going, sees without looking, and accomplishes without doing."_ \- Lao Tzu, ancient Chinese philosopher and writer

### 4. To fulfill your desires, you must detach yourself from them and trust the universe. 

Let's say you've always dreamed of owning a Ferrari. You imagine that, once you've saved up and bought your dream car, your life will be happy and fulfilling. Chances are, however, that such ardent attachment to the car will only lead to your feeling terribly distressed.

Attachment to material things implies distrust of the universe — and this inevitably leads to anxiety.

After all, a life spent amassing wealth and material goods as a means to security will end in a disheartening realization: material possessions can never truly provide safety or security.

For example, no matter how much money you save, all can be lost in an economic downturn. Just think of those who lost their homes and savings as a result of the 2008 financial crisis.

Real security can be achieved in only one way — by recognizing your true Self.

By realizing your true nature, you'll find that even fear of death disappears. The body and mind dissolve, and your true Self will remain in the field of energy that gives birth to all new life in the universe.

While it might sound counterintuitive, you'll also find that your desires can only become reality when you detach yourself from them.

For example, single people are often obsessively attached to finding a mate, keeping an eye out for hours on end every day, whether online, at parties or in their circle of friends and colleagues.

Ironically enough, it's usually only when these people, exhausted and discouraged, let go of their ego and give up, that the universe gets a chance to step in. Then, as if by magic, the right people finally cross paths.

This is what happens when you detach yourself from your desires and connect to the field of energy that is the true nature of the universe, which alone has the power to make our desires become reality.

### 5. Give to others what you would wish to receive and let your money flow into the world. 

If you have a large savings account then you may have received praiseworthy pats on the back from fellow materialists. But rather than saving up, a better path to success is to let your money flow into the world.

Hoarding money interrupts the universe's natural flow of give and take.

In fact, the Latin root of the word "currency" literally means "to circulate or flow." This shows that even ancient Greek and Roman civilizations understood that money must flow for a society and its people to flourish. This is why investments are often more profitable than savings.

For example, say you spend your life earning minimum wage, putting the majority of your monthly paycheck in the bank. Even though the balance on your account will increase, you won't become rich.

But if you invest your money in continuing education and take the right classes, you could eventually become the CEO of your own business. By wisely investing in your education, far more will flow back to you, leaving you with more money than a lifetime of petty saving ever could.

This spiritual law of giving and receiving doesn't only apply to money, either. It applies to love, friendship, support and all the things we exchange socially.

If you use this social currency to give others what you hope to receive, you'll be amply rewarded. The more love people give within a society, the more that love will be shared with others and will continue to grow.

But, naturally, this only works if everyone is prepared to give as well as receive.

For example, most people desire a partner who will love them and take care of them. But many forget that a relationship is also about giving love and caring for someone else.

If everyone were focused solely on getting love, and not giving it, we would all end up lonely and unhappy.

> _"Giving and receiving are nothing other than the flow of life."_

### 6. Make conscious and considered life choices to get the maximum benefits out of life. 

We all know the advertising slogan, "Just do it!" Live in the moment, it tells us — seize the opportunity!

But to truly get the most out of life (rather than just _doing_ something), it is important to consider what the most beneficial choice is when making a decision.

This is what it means to be conscious of your _choice making_.

After all, we make choices all the time, sometimes without even knowing we're doing it. In fact, most of our choices are _unconscious_.

For example, when you're shopping, you might buy bags full of junk food without considering the impact they have on your health or the environment. Unfortunately, such unconscious behavior comes naturally to us, since our survival instinct tells us to stock up on large quantities of cheap food.

So, in order to make good choices, pause and consider the impact your actions have, both on you and on others.

How might you avoid habitually purchasing junk food? Well, first off, consider your budget, and then within its constraints choose products that are the most beneficial to your health and that, during the production process, have done the least harm to other people, animals and the environment.

To continue making good choices in your everyday life, simply choose the actions that will bring the most joy and goodwill to others.

In some cases, the choice might seem obvious.

For example, if you're invited to a birthday party but decide that it is too much of an inconvenience to attend, you should realize that you're only considering your own needs. So, instead, recognize the pleasure that the host will enjoy by being surrounded by friends on the special day.

In other cases, the choice might be more subtle.

If someone insults you, you have two options: You can be hurt by the remark and retaliate, or you can refuse to let it affect you. By choosing the second option, you both avoid subjecting yourself to unpleasant emotions and treat the offender as you would want to be treated.

> _"The more you become aware of your choices, the more you will make choices that are spontaneously correct."_

### 7. By focusing intensely on positive intentions, your desires will manifest in the world. 

Did the previous blink about accepting your current situation make you anxious? Perhaps because you're unhappy and don't _want_ to accept how things are? Well, don't worry. The good news is that your thoughts have the power to change the world around you.

You can improve your quality of life by simply introducing positive intentions into your thinking.

The positivity that goes into these thoughts is all part of the infinite field of potential energy that governs the universe. So, by introducing an intention or wish into this field of energy, you affect the universe around you.

Let's say you'd like to have a beautiful home, but you often think about this in negative terms: your emotions are tied up in disliking your current home and thinking that you'll never have sufficient funds to afford a new one.

When you do this, you give negative energy to the universe; naturally, this won't result in the universe giving back positive energy.

So, instead, accept and be grateful for the home you do have, and visualize the changes that could make your living situation even better. The universe will pick up on these positive thoughts and turn them into reality.

To turn these desires into reality, however, you must give the process your full attention.

For example, if you're dissatisfied with your job, you may have negative thoughts about being incompetent or feeling unworthy. It requires constant attention throughout the day to notice these negative patterns and replace them with positive ones.

If you catch yourself thinking that you're not contributing enough at work, stop yourself and consciously think about your accomplishments to find at least one constructive thing you've done. Even if it is as simple as having written a carefully worded and effective email, you can stay with this thought and remain positive.

> _"In the beginning there was desire, which was the first seed of the mind…"_ \- Hymn of Creation, The Rig Veda

### 8. Discover your life’s purpose by helping others and letting the universe support your efforts. 

Every human wants to shine. But, as each star has its right place in the night sky, every human must find their right place on earth. Only by doing this, and by helping others find their place, will people flourish and lead a purposeful existence.

So, ask not what the universe can do for you, but what you can do for the universe.

Let's say you've always dreamed of becoming an artist. After all, it is a wonderful profession, and you admire the special kind of talent it takes to create art that inspires people and makes them happier and wiser.

But the truth is, not everyone is artistically talented, and if this is the case for you, you might end up sad or depressed, feeling that you can't contribute anything of value to society.

But don't despair! Instead of clinging to these dreams, be honest with yourself and discover the truth within you that can serve and benefit others. While a creative person will probably become a good artist or designer, a compassionate person might be better off as a doctor or nurse.

It's never too late to recognize your life's purpose and plug into the endless source of energy that the universe provides.

You might now be wondering whether your current job is providing you with the best way to contribute to the welfare of humanity and the planet. As it turns out, there is an easy way to tell.

When you've found the right job, you will discover that you have a nearly endless source of energy to complete your work.

This is the feeling you get when you align with the cosmic plan that was your destiny from the beginning. The universe begins to support you and provide you with the energy you need, which explains how some people can work effortlessly and happily for long hours, while others feel drained after just a few hours in the office.

What do you think? Have you found your true Self?

> _"Everyone has a purpose in life — a unique gift or special talent to give to others."_

### 9. Final summary 

The key message in this book:

**Human beings are not separate, powerless egos that are lost and alone in a harsh physical world. Every one of us is part of the infinite field of potential energy that gives birth to and governs everything in the universe. When you recognize this, you can use this energy to fulfill your personal and professional dreams.**

Actionable advice:

**Give to everyone you meet.**

When you are invited to someone's home, bring them flowers or some other token of appreciation. Your gifts don't always have to be material. You can give to others by greeting them in a loving and compassionate way.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Power of Now_** **by Eckhart Tolle**

_The_ _Power_ _of_ _Now_ offers a specific method for putting an end to suffering and achieving inner peace: living fully in the present and separating yourself from your mind. The book also teaches you to detach yourself from your "ego" — a part of the mind that seeks control over your thinking and behavior. It argues that by doing so you can learn to accept the present, reduce the amount of pain you experience, improve your relationships and enjoy a better life in general.
---

### Deepak Chopra

Deepak Chopra is a resolute advocate of spiritual values and alternative medicine in the West. He studied medicine in India before moving to the United States, where he focuses on meditation practices and Ayurvedic medicine. His many other publications include _Creating Affluence_ and _Power, Freedom and Grace._

