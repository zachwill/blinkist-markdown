---
id: 5746cd0cb49ff200039c4d79
slug: the-seventh-million-en
published_date: 2016-05-31T00:00:00.000+00:00
author: Tom Segev
title: The Seventh Million
subtitle: The Israelis and the Holocaust
main_color: 97946B
text_color: 6D6932
---

# The Seventh Million

_The Israelis and the Holocaust_

**Tom Segev**

_The Seventh Million_ (1991) is all about the way in which the Holocaust has shaped the Israeli identity. These blinks detail everything from the Zionist response to Nazism and the arrival of the first European Jewish refugees in Palestine to the Six-Day War and Holocaust Memorial Day.

---
### 1. What’s in it for me? Understand the shattering consequences of the Holocaust on Israeli identity. 

The unfathomable genocide during which Hitler's Nazi Germany and its collaborators killed about six million Jews — known today as the Holocaust — took place in Europe between the years 1933 and 1945. And the question driving the author is: How does a tragedy of that scale shape the identity of the "seventh million"? That is, the entire Jewish population of Israel — including those who already lived in former Palestine and all the Jewish survivors who moved there after the war.

These blinks examine the Holocaust's influence on the Israeli nation as well as its politics, culture and identity. You'll learn how the early Jews living in Palestine struggled with the arrival of European refugees and Holocaust survivors in Palestine, and how the young State of Israel dealt with the horrendous events of the past.

You'll also learn

  * what the Nazis and Zionists had in common;

  * how one Jewish Holocaust survivor tried to take revenge; and

  * why Israeli news broadcasters refrain from saying "Good evening" one day every year.

### 2. With the rise of the Nazis, German Jews were “transferred” to Palestine – yet their arrival was fraught with tension. 

Nineteen thirty-three was a turning point in history: the year the Nazis came to power in Germany. The rise of the Nazi state quickly signaled to Zionists, the community of Jews desiring to create a Jewish state in Palestine, that the Jews of Germany were in danger.

Back then, however, the interests of the Nazis and the Zionists complemented one another. That's because the Nazis wanted the Jews to leave Germany and the Zionists wanted them to live in Palestine.

As a result, "transfer" agreements, also known as _Haavara_, were made between the Nazis and the Zionist Jewish Agency in Palestine. Here's what happened:

In the 1930s, the Jewish Agency acted as a government for the future Jewish state, with Zionist officials traveling to Berlin to negotiate the emigration of German Jews and the transfer of their property to Palestine.

As a result of these negotiations, a transfer agreement was reached: any Jews who emigrated to Palestine would be permitted to take $4,000 of their money and to ship goods worth $5,000 to Palestine, a sizable amount of money in the 1930s. In this sense, the agreement was just.

However, the arrival of the German Jews in Palestine was a source of great tumult. The German immigrants were traumatized by the terror of Nazi Germany and from having been uprooted from their home country. Many had also come against their will, in other words, not as Zionists but as refugees. The latter did not have the same beliefs as the Zionist colonists, who sought to establish a Hebrew culture and language in Palestine.

And the Jews who already lived there?

They were not happy about the immigrants. In fact, they lamented the flow of poor people and businessmen with their families arriving from Germany. They would have preferred single men and women to come, as they were considered ideal for building a new country.

Quote/Fact: Eliahu Dobkin, a member of the Jewish Agency in the 1930s, considered German Jews who were arriving as refugees as "undesirable human material."

### 3. Jews in Palestine were focused on building the State of Israel and did not fully acknowledge the extent of the Final Solution in Europe. 

Before World War II, Europe was home to some nine million Jews. By the end of the war, that number was just three million. Of those Jews, just a few thousand owed their survival to the Zionist movement.

In fact, when news of the systematic extermination of European Jews first broke, the Jews in Palestine didn't give it the attention it deserved. In 1942, _Ha'aretz_ published a story about atrocities being committed against Jews in Kharkov, Ukraine, but the story appeared on page two under a single-column headline.

The news right above it?

A piece about the victory of the Jewish soccer team in Damascus — apparently a more newsworthy story.

Then, toward the end of the same year, after a Jewish Agency official announced that there was a massive plan to kill all the European Jews, newspapers began devoting tons of space to the subject. And yet, just a few months later, articles about it were placed further and further from the front page. Palestinian Jews didn't yet recognize the full extent of the genocide taking place in Europe.

Why didn't they see the Holocaust for what it was? Well, having been the victims of endless pogroms, the subject of yet another example of Jewish persecution and murder in Europe simply didn't seem all that remarkable. They wanted to concentrate on the future and building of the state, rather than the present Holocaust.

So, even though money was spent to save Jews — about several million dollars in total — a lot more went into buying land and setting up settlements in Palestine. The assumption was that there just wasn't much that could save the European Jews.

> "_From the second half of 1943 onward, the Holocaust was, again, no big news_."

### 4. After the war, some Jews called for revenge, but many Holocaust survivors were too traumatized. 

When the war came to a close and the extent of the horrors of the Holocaust came into sharper focus, the Jewish people living in Palestine were overcome by a collective sense of guilt because they hadn't done everything they had been capable of doing to save the European Jews.

It was during this period of shock that some Jews called for revenge on the German people. For instance, a few months after the war ended, Abba Kovner, a young Holocaust survivor, arrived in Palestine armed with a plan for vengeance.

He gathered a group of other young Holocaust survivors to poison the drinking water in several West German cities. Calling themselves the Nakam, which translates as "revenge," the group hoped to murder six million Germans to balance the scales, justifying itself by the Bible's tenet: "an eye for an eye, a tooth for a tooth."

While the group did manage to kill a few former SS officers, its gigantic feat of terrorism never came to fruition. In fact, the Jewish Agency did not support this great act of revenge because it would have likely hurt its primary goal: the establishment of the Jewish state.

Moreover, many of the Holocaust survivors who had recently arrived in Palestine wanted only to rest. They were traumatized and struggled to readjust to normal life.

Many Holocaust survivors required psychological care for years after the war. They suffered from intense anxiety, nightmares, bouts of depression, fury and apathy. They also suffered from the guilt of having survived their loved ones and found it difficult to form relationships with others.

Many of the survivors who joined the Jewish communal settlements, or _kibbutzim,_ did not feel right there. The collective communities reminded them of concentration camps. They wanted nothing more than their own space in which to deal with their own problems.

> "_Not even the devil has devised a fitting revenge for the blood of a small child._ " — Haim Nahman Bialik

### 5. Despite their controversial nature, reparation negotiations with Germany benefited Israel. 

In the first few months following the declaration of the Israeli state in 1948, many Israelis called for a boycott of Germany. But such a boycott would have been difficult to achieve and, frankly, counterproductive.

Why?

Well, a boycott would have prevented exports to Germany, yet if Israel were to join international organizations like the United Nations, it would have to network with at least some Germans.

In the same vein, Israel's reparation negotiations with Germany became another heated subject.

On December 30, 1951, the Israeli government decided it would enter into negotiations with Germany to receive reparations for the crimes Germany had committed against Jews. But many Israelis felt that taking this money would effectively be taking ransom from murderers and were therefore opposed to the negotiations.

Their argument was that the German people and their government were murderers, and that all German money was dipped in the blood of Jews. In fact, during a debate in the Israeli Parliament, called the _Knesset_, protests and riots erupted and people even threw stones through the windows of the parliamentary chamber.

So, clearly the political atmosphere was tense, but despite the ardent opposition of Israel's citizens, the Knesset entered into negotiations with Germany in 1952 and the agreement they reached benefited Israel.

The German government agreed to pay reparations of approximately $820 million. Around 70 percent of the money was set aside for goods made in Germany and the other 30 percent for buying fuel.

This settlement was to be paid over a 12-year period, during which time Israel's gross national product tripled. And that's no coincidence. In fact, approximately 15 percent of this growth and 45,000 jobs can be directly attributed to investments made with the reparations money.

Over time, the payments helped forge a better relationship between the two countries.

> "_Suppose they pay you for six million Jews, but when the reparations period is over, … where will you get six million more Jews so that you can get more money?_ " — Yohanan Bader

### 6. The establishment of military links to Germany and the fate of a Nazi collaborator were two more controversial issues. 

In the 1950s, Israel became involved in a number of military conflicts with the neighboring Arab countries. And Israel received some of the military equipment it needed from Germany.

As with the reparation payments, the military connections between the two countries was a highly contentious subject among the Israeli people. In 1959, the German weekly news magazine _Der Spiegel_ reported that Israel was not just buying arms from Germany, but selling them to the country as well.

Due to the conflicting moral and political issues at stake, Israel's decision to forge military connections with Germany was highly contentious David Ben-Gurion, the prime minister at the time, claimed that Israel needed to sell arms to Germany not just because the country needed foreign currency, but also to reinforce the German commitment to do the same for Israel.

Others saw it differently. They argued that it was morally reprehensible to give Jewish weapons to German soldiers who had murdered Jews in the past.

Another source of dispute was the Jewish collaboration with the Nazi state. Likely the biggest controversy of this type was the case of Rudolf Kastner, who was the spokesman for the Israeli Ministry of Commerce and Industry.

During the war, Kastner had been the head of the Aid and Rescue Committee in Hungary, a group that helped Jews flee the Holocaust. In this capacity, he had negotiated with Adolf Eichmann, a senior SS officer, to let 1,684 Jews leave for Switzerland in exchange for a cash payment on what would become known as the Kastner train.

Kastner's negotiations with Eichmann sparked a huge debate as he was accused of collaborating with the Nazis, even though it was to save Jewish lives.

When he stood trial, the judge said that he had sacrificed the mass Jewry for just a chosen few and, in 1957, Kastner was assassinated by right-wing Jewish activists.

> "_Again, … Israelis had to decide between morals and national interest, between emotion and reason._ "

### 7. Israel tried Adolf Eichmann, a prominent Nazi official, forging national unity but also spurring deep criticism. 

On the night of May 11, 1960, in Buenos Aires, Argentina, Mossad agents kidnapped a man whose neighbors knew him as Ricardo Clement. These Israeli secret agents drugged the man, dressed him as an airline steward and put him on a special plane waiting for them at the international airport.

The real name of the man they had kidnapped was Adolf Eichmann and he had been one of the highest-ranking figures in the Nazi party, playing a central role in organizing the transportation of Jews to death camps.

In 1961, Eichmann was put on trial in Israel for being instrumental in the murder of millions of Jews. Eichmann pleaded not guilty to the charges and said he had only done what he was told to do, stating that he was only guilty of obedience and that there was no blood on his hands. Nonetheless, Eichmann was convicted of crimes against the Jewish people, crimes against humanity, and sentenced to death.

What did his trial mean for Israel?

On the one hand, it fostered national unity. Much of the court proceedings were broadcast live on the radio, so people all over the country were listening at home, in offices, cafes, stores, buses and factories. The trial produced a kind of "national group therapy" that sought to give a voice to Holocaust victims everywhere.

But the trial also aroused its fair share of criticism. For instance, the Jewish philosopher Hannah Arendt criticized the court proceedings as a show trial, saying it was obvious that Jewish judges could not impartially determine Eichmann's guilt or innocence.

She was also of the opinion that Eichmann's role in the mass murder of Jews should have only been termed a crime against humanity — not against Jewry — since the distinction between Jews and other humans was exactly the one the Nazis had used to justify their crimes in the first place.

> "_… it was not Adolf Eichmann's deeds that were at the center of the trial but rather the sufferings of the Jewish people._ "

### 8. Driven by existential fear, Israelis occupied the Palestinian territories and discriminated against Arabs in the 1960s. 

Science was always an essential part of the vision for the State of Israel, and the country built a nuclear power plant before many other nations. But their atomic pursuits weren't made for solely civilian purposes and, in the late 1960s, _Time_ magazine reported that Israel was constructing a nuclear weapon.

Why?

Well, during the 1960s, the Israelis lived in a state of perpetual fear spurred by their Arab neighbors. No small wonder, because ever since the founding of the State of Israel, its Arab neighbors had threatened it with war.

On July 21, 1962, when the Egyptian president Gamal Abdel Nasser put twenty ground-to-ground missiles on parade in Cairo, the threat from their neighbors seemed real and present to Israel. In fact, all over the country, you could hear and read about how Arabs threatened to "exterminate Israel."

Comparisons were drawn between Nasser and Hitler, and many Israelis sincerely believed that genocide was a possibility if Israel lost a war against the Arabs. War finally broke out on June 5, 1967. In six days, Israel conquered vast regions of Palestine, including the Gaza Strip and the West Bank, territories the country still occupies to this day.

The Arab civilians living in these regions were relegated to the bottom of the Israeli social ladder and subjected to extreme racism in the 1980s. For instance, in 1984, approximately 25,000 Israelis voted for Rabbi Meir Kahane, who demanded the expulsion of Israel's Arab citizens and the Arabs that lived in the occupied territories.

He wanted to ban all contact between Jews and Arabs, and even have separate beaches for Jews and non-Jews. As a result, there were incidents of young Israelis attacking Arabs and screams of "Death to the Arabs" were commonly heard in Israel, echoing the Nazi slogan "Death to the Jews."

> "_The threat of 'extermination' had not, then, been real. But the fear of it had been real …_ "

### 9. Israel has established a Holocaust memorial culture. 

Since the early days of the Jewish state, the people of Israel have debated how to memorialize the Holocaust. How could the country preserve the collective memory of this event? What was OK and what was off limits?

A consensus was reached in 1951, when the Knesset decided that the twenty-seventh day of Nissan, the first month in the Jewish calendar, would be Holocaust and Ghetto Rebellion Memorial Day. Ever since this decision, the country has all but shut down for one day of remembrance, contemplation and unity each year.

All places of leisure activities — movie theaters and coffee shops included — are closed. The national radio and television stations broadcast an atmosphere of mourning.

The radio plays testimonials of Holocaust survivors as well as long symposiums interspersed with sad music, often by a solo cello. The television news broadcast airs without the music that usually accompanies it and the anchor does not say "Good evening."

Instead of airing the standard programming, the television stations show films about the Holocaust and the print media publishes poems that reflect on the history and culture of memory.

But despite this massive, orchestrated day of remembrance, Israel still struggled to teach the Holocaust to children. That is, until 1980, when Holocaust studies became a standard requirement in all Israeli schools.

As a result, since the early 1980s, questions regarding the Holocaust have made up about 20 percent of the total score on the high school diploma examination for history. And today, the Holocaust is taught in both elementary and secondary schools, meaning an Israeli high schooler will have studied the subject twice.

The author even accompanied a group of students as they traveled to the Nazi concentration camps, Auschwitz included. At some point or another on the trip, every single student broke down and, by the time they reached Auschwitz, they had no tears left, as one student put it.

> "_Here was a Zionist irony. … Israel was sending its children into the Jewish past abandoned by its founding fathers, who hoped to create a 'new man' …"_

### 10. Final summary 

The key message in this book:

**The history of the Holocaust is deeply embedded in the politics, culture and identity of the State of Israel. While early Zionists struggled to acclimate to the arrival of European refugees and Holocaust survivors, the fear of another genocide pushed Israel to invade its neighbors, sparking a climate of racism.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sunflower_** **by Simon Wiesenthal**

_The Sunflower_ (1969) is an important exploration of forgiveness — both its possibilities and its limitations. We join the author as he attempts to find an answer to an extremely complex question: Can a Jewish concentration camp prisoner forgive a Nazi soldier on his deathbed? There is a range of opinions — from people like Primo Levi and the Dalai Lama — but is there a right answer?
---

### Tom Segev

Tom Segev is a columnist at _Ha'aretz_, a leading Israeli newspaper, and the author of _One Palestine, Complete: Jews and Arabs Under the British Mandate_.

