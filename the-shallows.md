---
id: 54102f8536356200086c0000
slug: the-shallows-en
published_date: 2014-09-09T00:00:00.000+00:00
author: Nicholas Carr
title: The Shallows
subtitle: What the Internet Is Doing to Our Brains
main_color: None
text_color: None
---

# The Shallows

_What the Internet Is Doing to Our Brains_

**Nicholas Carr**

_The_ _Shallows_ delves into the seldom-asked question of how technology, and the internet in particular, affects our brains. _The_ _Shallows_ looks at the history of technology and recent studies in psychology and neuroscience to show us how the internet fundamentally rewires our neural circuitry, and why it hasn't delivered the benefits promised by its champions.

---
### 1. What’s in it for me? Learn how the internet isn’t all it’s cracked up to be. 

How good is your memory? Can you remember eloquent passages from your favorite play or book? Recall some of the most important dates in history? Or your favorite recipe?

Most people today struggle to recall any of these facts without the aid of the ever-trusty internet. If you really needed any of that information, it's just a Google search away.

It's easy to let the convenience of the internet overshadow something so apparently trivial as our inability to remember recipes. However, the _reason_ that we can't remember has far-reaching implications for our lives: our growing use of the internet means that the structures of our brains are actually changing.

And these changes aren't trivial. They're the reason we can't concentrate on anything or retain knowledge. Indeed, the marriage of humans and technology poses serious questions about our humanity.

These blinks will teach you exactly how the internet is literally changing our brains, and what that means for us as a society.

In these blinks, you will discover

  * how buying a typewriter had a profound impact on the work of one of the great philosophers; 

  * why you'll probably never read _Anna_ _Karenina_ ;

  * and what happens to a monkey's brain when you chop off its finger.

> _"The net turns us into lab rats constantly pressing levers to get tiny pellets of social or intellectual nourishment."_

### 2. In spite of our previous scientific understanding, the human brain is plastic and ever changing. 

Have you ever considered picking up a new hobby, like playing the violin, only to immediately push away those thoughts, thinking: "I can't do that! I'm just too old to learn anything new."

You're not alone. The idea that you can't teach an old dog new tricks was popular among neurologists until the 1970s.

The thinking was that, once we reach adulthood, the _synapses_ — that is to say, the links between two neurons in the brain — would decay without being replaced. In other words, once the brain was fully developed, it became totally immalleable.

This idea centered largely around a popular eighteenth-century conception of the brain as a mechanical machine. People thought that the brain worked like a clock comprised of several interoperating components, interlocking like cogwheels. Once the machine had been assembled, it's function didn't change, although it might get rusty.

However, in recent years our scientific understanding has changed dramatically. Today, we know that the brain changes with every action we carry out.

Consider, for example, the experiment conducted by Michael Merzenich in 1968, in which he amputated a monkey's finger and observed the brain region that had previously reacted to stimuli from that finger.

At first, the brain was confused: when Merzenich touched the lower joint of another of the monkey's fingers, the brain region responsible for the amputated finger also became active.

However, after a while the brain did something highly unexpected: it _rewired_ _itself_ so that the stimulus caused activity in only the correct areas of the brain.

Later experiments involving humans showed that the plasticity of the brain — _neuroplasticity_ — is not just limited to the sensory system, but actually applies to all neural activities: throughout our lives, synapses are linking, then strengthening through repetition, or fading from disuse.

Indeed, you _can_ teach an old dog new tricks.

### 3. The human brain is deeply affected by the tools we use. 

Suffering from failing vision and hardly able to focus on writing with pen and paper, the famed philosopher Friedrich Nietzsche finally ordered a typewriter. After a short adjustment period, he continued with his writing.

But something had changed. A friend of his noticed that, now that he was using this new technology, Nietzsche's writing style had somehow changed. It had become more dense and telegraphic — almost "iron." Why?

The way that we think actually changes according to the tools used by the society we live in.

For example, the map.

Maps are highly abstract, and thus offer a perspective on our surroundings that is totally different from what we actually see. Using maps now feels quite natural for us — the pervasiveness of maps has caused our brains to adapt themselves to these tools, thus causing our view of the world to become "mapped."

Or think of the clock. The mechanical clock revolutionized the way we think, allowing us to divide our lives into measurable units. Suddenly, we could think in terms of the amount of temporal units that we spend or waste.

Moreover, individual use of technology, even for a short time, can cause substantial physical changes in the brain.

The National Academy of Science demonstrated this in experiments with monkeys. Their research showed that if the monkeys needed to use tools like rakes to reach for food, then the visual and motor areas of the brain that control their hands — which, of course, manipulate the tools — grow significantly.

The same is true for our use of the internet.

For example, when Gary Small, a professor of psychiatry, asked internet novices to search the net for a relatively short time span (an hour a day for five days), he noticed changes in the brain region called the _dorsolateral_ _prefrontal_ _cortex_, which is especially active when we're online.

Now that you know how the internet alters the human brain physically, the following blinks will reveal how these changes affect the way we deal with texts.

> _"What the Net seems to be doing is chipping away my capacity for concentration and contemplation."_

### 4. The internet has dramatically changed the way we read. 

Speaking of tools: Can you imagine trying to read all of Tolstoy's _Anna_ _Karenina_ by scrolling through a web page?

That sounds like a Herculean task, and it's no surprise: the way that we read has always been heavily influenced by the tools we use.

Until the Middle Ages, reading was done almost entirely out loud. In part, this was a reflection of the way early scribes wrote — simply stringing words together like children do when they first experiment with letters.

However, with the emergence of punctuation and spaces between words, texts became more lucid as they directed more toward the eye than the ear. As people no longer had to speak in order to comprehend the words on the page, silent reading, once a strange exception, became the rule.

And thanks to Gutenberg's invention of the printing press around the year 1450, reading became even more individualized. As more people gained access to books, reading changed from a social activity to a highly solitary, meditative one.

These changes led to the style of reading known as _deep_ _reading,_ in which the mind of the reader becomes calm and focused.

However, this has all changed with the birth of the internet.

Unlike the pages of the past, today's texts are littered with images and hypertexts, making us accustomed to constant distractions. To balance these various distractions, we have to develop _jugglers'_ _brains._

Trying to juggle so much stimulus at once results in a loss of concentration, and makes it nearly impossible to read and extract information from long online texts. In fact, one experiment has shown that the more links a text includes, the less information participants could comprehend and remember after reading it.

It's even possible that our adaptation to this new tool will completely erase our ability or desire to read complex novels like _Anna_ _Karenina_!

> _"Once I was a scuba diver in a sea of words. Now I zip along the surface like a guy on a jet ski."_

### 5. Internet reading material was supposed to have lots of advantages, but these have never appeared. 

If you're like most parents, you want your children (or future children) to attend a modern school where they have access to multimedia. You might think that it's important for your child to become accustomed to technology early, and have a fuller, more interactive learning experience.

When the internet first emerged, optimists hoped that the use of hypertext and multimedia would lead to an enriched understanding of written texts. People expected that internet users would be better equipped to compare theories and find diverse perspectives. Without the linear restrictions of a book, users would have a more flexible approach to a given subject.

This meant that if you were reading _War_ _and_ _Peace_ online, you could simply click on a link whenever you wanted more context on a particular aspect of Russian society at that time.

People truly hoped that online use of multimedia, such as embedded pictures and videos, sounds and links, would make the learning processes more interesting and effective.

However, recent experiments have shown that _none_ of these things have actually happened.

For example, a study conducted by Cornell researchers showed that students who surfed the net while listening to a lecture, regardless of whether their searches related to the lecture, performed significantly worse than their peers who kept their laptops shut.

Another experiment published in _Media_ _Psychology_ showed how multimedia might not help in education.

In the experiment, over a hundred participants read through a presentation about Mali. Half of them clicked through pure text pages, while the others were presented with multimedia. Surprisingly, those with pure text not only remembered more about Mali, but also liked the presentation better and found it more interesting and educational.

It seems that, rather than making learning easier and more effective, the internet overexerts its users, forcing them to make constant judgements and decisions.

Now that we know how the internet affects the way we read, we have to ask ourselves whether there are consequences for society at large. The following blinks will provide the answer.

### 6. The internet has changed the way we interact with older technologies, such as books or television. 

Would you say that you spend more or less time with print media than you did five years ago? If you read fewer books and newspapers, was this a conscious decision, or did it just somehow happen? It's very likely that it was the latter.

This is because new technologies don't just add something new to your life — they also change your perspective and expectations of older technologies.

Our exposure to the internet has altered what we expect to find when we turn on the television or radio, or flip open a magazine or newspaper. Of course, if they want to stay in business, producers and publishers of older media must react to this.

_Rolling_ _Stone_ magazine, for instance, began shortening its articles, introducing capsule summaries and spreading "googleable" snippets on its pages in order to account for shorter attention spans.

Even newspapers like the _Wall_ _Street_ _Journal_ and the _Los_ _Angeles_ _Times_ made adjustments for the re-wired minds of their readership by attempting to produce pages that were easier to skim through.

Books, of course, have had to make adjustments too.

E-readers, for example, claim to provide the same reading experience as a book, yet nearly always include features like internet access, which allows users to buy books but also distract them from the deep reading experience.

Some publishers, such as Simon & Schuster, have even published e-novels that include videos.

And just as the internet has altered our reading, it has also altered our writing.

For example, in 2001, Japanese women invented something called "cell phone stories" or novels comprised of a collection of text messages that had been posted and commented upon on a website. Six years later, the top three best-selling Japanese novels were cell phone stories.

So even if you don't surf the net very often, it still influences your experience with other media in ways you might not have considered.

### 7. The internet works as a big interrupting and distracting machine. 

Unfortunately, we are largely unaware of the effect that technology has on us. Just as Nietzsche couldn't have predicted how the typewriter would affect his writing, we couldn't predict how our use of the internet would change us.

Whether we notice it or not, technology does change us. In fact, the internet has dramatically altered our ability to direct and sustain our attention.

Not only do we browse the net with dozens of open tabs, we also have to manage the constant barrage of incoming emails and messages, automated notifications, RSS feeds, etc., that arrive every second.

What's more, Union College psychologist Christopher Chabris claims that the internet encourages our natural tendency to overestimate the importance of things that are happening _right_ _now,_ whether these are major news events or just updates from our friends on social media.

As a result, removing ourselves from the immediacy of the web makes us feel isolated and out of touch.

Moreover, companies actually have an interest in capturing our distracted attention.

Consider, for example, that Google, the number one source for internet browsing, wants users to spend as little time as possible on a web page before jumping to the next one.

Why? Because Google earns money whenever customers click on their advertising, and they make no money when users linger on a page and read. Our susceptibility to distraction is a big moneymaker for them.

This creates a strange paradox: Google helps us to find information for free, but in return seizes every opportunity to keep us from actually digesting that information, pushing us to click on the next personalized ad instead.

What's worse, companies have had a long time to perfect these strategies. In 1997, Jakob Nielsen, a web designer, conducted eye-tracking studies about online reading behavior. The results showed that most web pages are viewed only for few seconds. He was hoping to find out _how_ people read on the internet, and instead discovered that they _don't_.

### 8. The internet age frees us from having to remember things, but has ruined our ability to memorize. 

How many important historical dates can you remember? Can you recall ten examples before having to consult Google?

Probably not. The internet age allows us to outsource our memory, meaning that we need to remember less. Since we can google anything, anytime, remembering facts has become less important than knowing how to find them. Why store information in our brains when it's stored on a server somewhere?

For example, in the past, baking your favorite cake was a hassle if you couldn't find the recipe, because you had to remember not only the ingredients, but also the quantities and the sequences of the recipe. Today, however, you can look it up on the internet instantaneously.

As wonderful as all this may seem, outsourcing our memory comes with massive disadvantages. Namely, it has ruined our ability to memorize things.

If we want to turn a short-term memory into a long-term memory, the human brain requires attentiveness. Unlike artificial and digital information storage, biological memorization requires time in order to literally turn the experience — whether a fact or a story — over in one's head in the area of the brain known as the _hippocampus_.

However, if the hippocampus is constantly overworked by a barrage of various stimuli, as is the case when we surf the net, then no new long-term memories can be formed.

Companies like Google make claims about committing our cultural heritage to _its_ memory, for example, by digitizing books.

Data storage, however, is completely different from the human process of memorization, which requires time, attentiveness, and deliberation. Outsourcing our collective memories in this way actually weakens our culture.

Culture is more than a catalogue of traditions — it's the way we process those traditions as individuals within society. The simple memory provided by data storage cannot replace active engagement with those traditions.

> _"To remain vital, culture must be renewed in the minds of the members of every generation."_

### 9. The internet makes us less human and threatens cultural achievements. 

Technology plays an increasingly prevalent role in our lives. And as the borders between artificial and human intelligence slowly evaporate, we risk becoming "less human."

Qualities that define our humanity, such as our empathy and emotions, increasingly disappear as we become more involved with technology. Instead of having our human characteristics augmented by technology, we instead become more like the machines themselves, allowing them to guide our lives on their terms.

What's more, the distractions inherent in new technology will have a great impact on our cultural achievements. Indeed, all the major cultural achievements of the past were intimately tied to deep reading and meditative thinking.

Great novels like those of James Joyce, Jane Austen, and William Burroughs; the deep philosophies of Descartes, Locke, Kant and Nietzsche; ingenious scientific theories like Einstein's theory of relativity — none of these could have been produced by distracted minds constantly seeking new stimulation.

Finally, the more our problem-solving strategies depend on the use of technology, the more our brain power is diminished.

For every new skill we gain with smarter technology, we lose something in return.

Consider this experiment by Dutch cognitive psychologist Christof van Nimwegen on computer-aided learning.

In his experiment, he divided volunteers into two groups, both of which had to solve a logic puzzle on a computer. One was aided by a very helpful program that would give them clues by, for example, highlighting possible moves, and the other group received no such help.

At first, the computer-aided group performed much better and faster. However, the other group eventually caught up.

Eight months later, the same groups did the logic puzzle once more. The group that had originally done the puzzle without the aid of a computer was almost _twice_ as fast as the one that had computer assistance.

Whatever the consequences, the internet is not going away anytime soon. If we want to hold onto our humanity, we must be aware of how the web affects us.

> _"As we rely on computers to mediate our understanding … our own intelligence … flattens into artificial intelligence."_

### 10. Final summary 

The key message in this book:

**In** **many** **ways,** **the** **internet** **has** **made** **our** **lives** **far** **easier:** **we** **can** **shop** **online,** **consume** **media** **and** **discover** **even** **the** **most** **obscure** **factoids** **in** **ways** **never** **before** **possible.** **But** **how** **many** **of** **us** **are** **aware** **of** **the** **dangers?** **Our** **love** **for** **easily** **accessible,** **instant** **information** **has** **caused** **changes** **in** **our** **brain** **that** **put** **us** **in** **danger** **of** **becoming** **less** **human.**

**Suggested** **further** **reading:** **_You_** **_Are_** **_Not_** **_a_** **_Gadget_** **by** **Jaron** **Lanier**

_You_ _Are_ _Not_ _a_ _Gadget_ examines why the internet tends to glorify the hive mind and devalue the individual. Serving as both a history lesson on the web's origins and a warning of the future consequences of its current path, this book illuminates the hidden design of the web.
---

### Nicholas Carr

Nicholas Carr is a US writer who focuses on themes such as culture and technology. He has written for many publications, including the _Guardian_ , the _New_ _York_ _Times_ and _Wired_ , and has also penned a number of books, including _The_ _Big_ _Switch_ and _Does_ _IT_ _Matter?_

