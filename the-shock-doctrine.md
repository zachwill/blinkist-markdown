---
id: 531f6d576166310008270000
slug: the-shock-doctrine-en
published_date: 2014-03-11T09:00:00.000+00:00
author: Naomi Klein
title: The Shock Doctrine
subtitle: None
main_color: D64250
text_color: B8252B
---

# The Shock Doctrine

_None_

**Naomi Klein**

_The Shock Doctrine_ (2008) offers insights into the dark world of disaster capitalism, in which crises serve as an instrument to undo the trade regulations and national protections which prevent international megacorporations from totally exploiting poorer countries. Rooted in the findings of the CIA-sponsored "MKUltra" psychological torture experiments, economic shock treatment has left behind a legacy of blood and destruction since it first began to be taken seriously in the 1970s.

---
### 1. What’s in it for me? Understand how crises are used to brainwash economists. 

Have you ever wondered why the media today seems so focused on natural disasters, conflicts and economic crises?

No doubt one of the reasons is that disaster headlines sell newspapers, but as you'll see in _The Shock Doctrine,_ the media aren't the only ones benefiting from calamities. Crises like these are exploited by a rich minority for their own advantage.

In these blinks, you'll discover

  * what economic interventions and electro shock therapy have in common;

  * how free-market economics turned Chile into one the world's most unequal countries; and

  * how the Iraqi government was essentially robbed of 95 percent of its revenues.

### 2. In their interrogations, the CIA used psychological shock treatments designed to “recreate” the individual. 

Throughout the history of psychology, scientists and academics have wondered how to best treat those suffering from psychological afflictions. By the mid-twentieth century, most treatment methods were focused on repairing psychological damage.

But Dr. Ewen Cameron decided to take a different approach: recreating the psychologically damaged individual through the process of _shock therapy_. With CIA funding to support his work, Cameron's performed extensive experiments with electro shock treatment, where electric current is passed through a patient to induce seizures.

He noticed that his subjects followed a particular pattern: patients would first revert from a normal state to child-like behavior, until eventually completely regressing to permanent confusion and listlessness.

Cameron believed that at this point the individuals were "blank slates" on which new, healthy identities could be written.

The problem was that it didn't work. While Cameron was able to wear down their identities, he could not recreate them.

So, rather than changing his approach to get the desired results, Cameron opted to intensify his treatments. He began by using methods which were similar to torture: month-long sensory deprivation and isolation, intentional confusion, massive electric shocks and mega-doses of psychedelic drugs.

Meanwhile, the CIA recognized that Cameron's extreme shock therapy could also be used to other ends — for pacification and information extraction.

The extreme isolation (through sensory deprivation) and confusion (through intentionally distorting time by, for example, changing their meal schedules) inflicted upon Cameron's subjects made it impossible for them to make any sense of their experiences. The direct result of this was a heightened impressionability and cooperation, of which CIA interrogators later made great use to extract information.

And by coupling this isolation with sensory overload — for example, with strobe lights or extremely loud music — they could further increase the cooperation of patients and, later, prisoners.

These observations were collected in a document called the _Kubark Manual_, which would serve as the protocol for what would later be termed "advanced interrogation techniques" — or, as some would call it, _torture_.

### 3. Free-market economists developed “economic shock therapy” to force through painful economic reforms in times of crisis. 

The idea that a new order can rise from the ashes of a crumbled civilization is by no means new. What is new, however, is the practice of intentionally wrecking the economies of unwilling societies in order to completely restructure them.

Taking their inspiration from shock therapy, one powerful group of economists — led by Milton Friedman and his colleagues at the Chicago School of Economics — established and used _economic shock therapy_ to enact painful reforms.

The "Chicago Boys" operated according to the maxim that the government which governs best, governs least. And this maxim, of course, could be easily applied to markets.

The Chicago Boys believed in complete free-market capitalism, or at least in achieving a system that was as close to this as possible. They also believed that the free market would be the catalyst for democracy, stability and peace.

However, they realized that getting societies to enact free-market reforms would be problematic because most people wouldn't want to lose their economic rights, such as union protection and social security.

Their solution? Economic shock therapy.

They waited for opportunities such as economic crises or any other kind of great turmoil and then used the shock and confusion to force through sweeping reforms to reshape economic policy.

Thereafter, the Chicago Boys ensured that they were always prepared to take advantage of sudden shocks, and found that the best way was to convert as many economists to their school of thought.

One way they did this was by running exchange programs between Latin American schools and the University of Chicago. These new disciples would then return to Latin America, preaching the wonders of free-market capitalism and making it easier to push through reforms if a shock occurred.

At the heart of the Chicago Boys' actions was the belief that if enough liberalization policies were being enacted swiftly and decisively, there would be no meaningful way to counteract them.

### 4. In practice, the reforms of economic shock therapy are often coupled with authoritarian state brutality. 

It's often simply not enough to economically shock an unwilling constituency into submission during the process of market reform. In order to squash the inevitable public rage towards the unemployment and insecurity that come with austerity and market liberalization, governments must take more drastic measures — like repressing democratic rights in order to push through unpopular economic reforms.

For example, the implementation of an interim government following the 2003 Iraq invasion was a well-executed repression of democratic rights.

Iraqis were promised quick elections after the removal of Saddam Hussein, but since the emerging political powers of Iraq didn't intend to privatize their oil industry, the US administrator, Paul Bremer, had to find a way to circumvent them.

So what did he do? Arguing that Iraqis weren't yet ready for an elected democracy, he simply cancelled the elections.

Here's another example: In South America's Southern Cone, the stage for the Chicago Boys' most violent ideological wars, freedom of assembly and political dissent were banned outright. By silencing their critics the Chicago Boys could control the public discourse around these unpopular economic reforms.

Yet this state brutality also extended into human rights violations. Whatever couldn't be accomplished through economic violence would need to be accomplished through terror.

The most explicit examples of this practice appeared in the Southern Cone.

During Pinochet's rule in Chile, around 3,200 people went missing or were executed, 80,000 were imprisoned and 200,000 fled the country. And within the Southern Cone during this large-scale economic transformation, an estimated 100,000–150,000 people were made to suffer in prisons designed specifically to break their will.

The violence carried out there wasn't the crude brutality of crazed dictators. It was planned terror, designed to force the population to comply.

As these examples show, since economic shock therapy isn't in itself sufficient to implement reforms, many governments turn to state repression and violence to augment it.

But who benefits from economic shock treatment? As you'll see next, there are clear winners and losers.

### 5. Economic shock treatment tends to benefit the super wealthy and multinational corporations at everyone else’s expense. 

With all major economic policy changes there are winners and losers. During the process of economic restructuring, the biggest losers by far are most of the citizens of countries which undergo economic shock therapy.

The immediate removal of price controls, the opening of national markets to privatization and the removal of trade restrictions have drastic effects for the average person. As an example, consider Pinochet's Chile, which saw its unemployment rate hit 20 percent during the implementation of their first economic reforms — a national record. That figure jumped to 30 percent in 1982 following another round of economic reforms, with the economy on the whole shrinking by 15 percent.

The long-lasting effects of these policies are hard to ignore. In 2006, Chile, supposedly a model economy for the Chicago School reforms, ranked as the world's eighth most unequal country.

On the other hand, the biggest winners of economic shock treatment are multinational corporations, as they're able to use their cash to buy up those companies brutalized by hyperinflation and a ruined economy.

In times of economic crisis, the companies and governments of the crisis regions have a difficult time staying afloat. And since no one can buy their products at a rate that can help them sustain operations, many are forced to look for buyers.

After the invasion of Iraq, for example, Iraqis had to privatize their national oil industry, essentially at gunpoint. The legislation that made this possible allowed foreign companies to keep 100% percent of the profits they earned without having to reinvest in the country. In addition, corporate taxes were reduced from 45 percent to a mere 15 percent.

To illustrate just how damaging this was for the Iraqi economy, consider the fact that 95 percent of Iraq's government revenues came directly from its national oil industry.

In sum, it's clear that economic shock therapy benefits the wealthy at the expense of the rest of the population.

### 6. Economic shock therapy requires the annihilation and silence of political and ideological dissidents. 

Although economic shock treatment does not take aim at political dissidents, the general unpopularity of its reforms means that governments are motivated to crush any material or ideological opposition.

The removal of political dissidents prevents the obstruction of economic reform. During periods of unpopular reform, especially in those countries with a strong history of national protectionism and worker solidarity, reform regimes face many obstacles and threats.

One major threat that needs to be neutralized is full-fledged rebellion. For example, under the pretense of fighting rebels, as was the case in Iraq, militaries indiscriminately collected citizens to be brutalized and tortured.

Even more dangerous are the ideological dissidents who generate discord through public scrutiny of these regimes. For example, in the Southern Cone during economic shock therapy, many Leftist cultural icons were abducted, forced into exile or murdered. Under Pinochet's 1976 reforms in Chile, 80 percent of political prisoners were workers and peasants.

Another example is Argentina, where many of those who were abducted and tortured were unionists, farmers who advocated for land redistribution or even social service workers.

Using terror can cause the ideological debate surrounding contested reforms to fall silent. In South America, because abductions, torture, and execution were so common, many were simply too afraid to take to the streets. This meant that reform regimes had complete control over the political discourse at the time, and were then totally free to frame dissidents as terrorists and criticize their political leanings.

With no possibility for political debate, and with no one left to even engage in it, both the reforms and the despots who imposed them at gunpoint had nothing to fear.

### 7. In the 1970s, economic shock therapy resulted in poverty stricken, downtrodden and terrified populations. 

To ensure that the public was sufficiently softened for the purposes of instituting reform, trade liberalization in the 1970s in countries such as Argentina was characterized by the twin terrors of economic shock therapy and immense brutality.

As you know, economic shock therapy was supposed to shock citizens out of their old way of thinking and get them to accept free-market reform.

But these shocks had disastrous consequences. The privatization of state-owned companies, alongside austerity measures, caused large-scale unemployment, and the removal of labor protection laws only amplified this process.

In addition, price-fixing on basic necessities, such as bread, butter and water was often abolished. This would also cause short-term hyperinflation. In other words, what little money the laborers had became essentially worthless, which was crushing for laborers who were now out of work and without food. The objective here was to keep the population from reversing reforms, thus allowing the market to stabilize itself.

Indeed, the _juntas_ of the 1970s used oppression as a means to push through unpopular economic reforms. Doing so allowed them to squash political dissidents as well as heighten the sense of insecurity in their population. 

For example, Argentina's Perón opted for a more clandestine approach to terror, deciding to instill fear using the powers of imagination. Of course, there were a few public killings, but not as a show of brutality. Rather, they were a sign that he meant business.

Afterwards, he would "disappear" people: abduct them, torture them, perhaps kill them and drop them in a river somewhere, never to be found. He left it to the Argentines' imagination as to what happened to their family and friends.

The result of this kind of brutality is a kind of paralyzing fear and hopelessness; many people were simply too scared to act against authoritarian governments or too downtrodden to see the point in even trying.

### 8. In the 1980s, Western nations learned that denying aid to struggling countries forced those countries to open their markets. 

Although the International Monetary Fund and other Western financial institutions claim to ensure the world's financial stability, their actions regarding Asia and Eastern Europe in the 1980s tell a more sinister tale.

That is, instead of helping struggling nations, the Western financial institutions stood by while their economies collapsed.

Consider, for example, Poland in the late 1980s. There, a newly elected (and Leftist) reform government found itself straddled with the debts of the former regime. Without money in the coffers, it was impossible for them to enact the reforms that they'd campaigned on.

So, they went to the Western financial institutions for help. However, those institutions refused because the Polish government was unwilling to enact the neoliberal reforms that they'd requested.

Similarly, the "Asian Tigers" were experiencing a minor financial crisis that threatened to explode unless they received aid from the IMF. But the IMF saw no need to intervene without getting something in return. Their attitude was simply, "let it burn."

By withholding much-needed aid, the West intensified the financial problems of these nations, making them desperate for cash — which put the IMF and the US in the perfect bargaining position.

In order to secure aid from the West, countries would have to adhere to a list of demands focused on trade liberalization, privatization and austerity. In Poland, for example, the new government was forced to privatize national industries, open up stock and capital markets and make budget cuts — exactly the opposite of what they campaigned for.

And in South Korea, the IMF required an immediate 50 percent cut in government employment before it would loan them the money they needed.

The result of these policies was, as always, mass unemployment, plunging wages and a struggling industry that couldn't survive in a volatile economy.

### 9. By privatizing the military and reconstruction effort in Iraq, the United States made large corporations extremely wealthy. 

The close relationship between big business and government is nothing new. However, the reconstruction of Iraq was an unprecedented opportunity for businesses who wanted access to government dollars.

Indeed, the Bush administration wanted to run government using the _hollow shell_ model for corporate organization. In this model, there's a central organization that writes some of the checks and carries out "core competencies" while everything else is outsourced.

For Iraq's reconstruction, this meant lucrative building and mining contracts for select companies.

In fact, even the military was outsourced according to the hollow shell model. Security was provided mostly by private contractors, but security wasn't the extent of the relationship: even operations like raids, arrests, prison security and interrogation were also carried out by private contractors.

The hollow shell model is extremely lucrative for certain corporate enterprises.

For example, in 2003 alone, the Bush administration doled out $327 billion in private contracts, and the Pentagon spent $270 billion per year in private contracts (a $137 billion increase) while Bush was in office.

And this is especially true for those companies who snagged one of the many no-bid reconstruction or security contracts doled out by the administration.

To further aid their profitability, the Bush administration's policy of minimal government meant that corporations weren't accountable.

For example, there was a court battle between the Coalition Provisional Agency (CPA), the US administration in Iraq and the company Custer Battles, who was contracted for the reconstruction effort.

The CPA proved that Custer Battles had created numerous fraudulent invoices that the government paid, but Custer Battles avoided prosecution because they were governed by United States' — not Iraqi — law, and the CPA was itself an institution not governed by the United States.

The US reconstruction of Iraq offered never-before-seen opportunities for large corporations and businesses.

### 10. Final Summary 

The key message in this book:

**Policy decisions and revolution are not nearly as straightforward as they may seem. Whether they are the _juntas_ in South America, the radical privatization in Southeast Asia, or the the restructuring of Iraq, major changes to government and economic policy are shaped not only by the demands of the people, but also the demands of corporations looking to find a way into new markets.**

Actionable advice:

**Look for the profiteers.**

If you want to really understand what motivates government policy, it is a good idea to find out who stands to profit from those policies. Since business and political ideology are inseparable, it is more than likely that the consequences of such policies are far more complex than the simple buzzwords and soundbites to which they are reduced.

**Learn the history of other nations.**

Having a deep understanding of your own country's history requires you to learn about the history of other nations. No nation exists in a vacuum, and no nation will intentionally make themselves look bad. Therefore a complete understanding of history requires that you seek out various contradicting sources of information.
---

### Naomi Klein

Naomi Klein is a bestselling author and political activist who has received the Warwick Prize for Writing and the National Business Book Award. In addition to writing the critically acclaimed nonfiction works _The Shock Doctrine_ and _No Logo_, she also contributed to a number of film productions, such as _The Take_ and _The Corporation_.

