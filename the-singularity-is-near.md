---
id: 55a391373738610007a70000
slug: the-singularity-is-near-en
published_date: 2015-07-15T00:00:00.000+00:00
author: Ray Kurzweil
title: The Singularity Is Near
subtitle: When Humans Transcend Biology
main_color: 2F6C7A
text_color: 175B6B
---

# The Singularity Is Near

_When Humans Transcend Biology_

**Ray Kurzweil**

_The Singularity Is Near_ (2005) shows how evolution is drawing ever closer to a dramatic new phase, in that by 2029, computers will be smarter than humans, and not just in terms of logic and math. This event will not only profoundly change how we live but also pose serious questions about humanity's future.

---
### 1. What’s in it for me? Discover how technology will change your life beyond your wildest dreams. 

Do you remember the scene in _The Matrix_, in which Neo has a program uploaded to his brain and, in an instant, he "knows" kung fu? What would life be like if you could upload knowledge just like a basic software program?

As these blinks will show, technology is evolving ever faster; in just a few decades, technological advances will enable us to transcend our biological shackles to overcome aging and disease.

In this new world, uploading a program to your brain will seem quaint. Technology will have become so advanced that it will mark a whole new era: a "singularity" in which computers will be billions of times smarter than all of humanity combined, and biology and technology will be one and the same.

This era isn't hundreds of years away, either. It's near.

In these blinks, you'll learn

  * how DNA will be used to build supercomputers;

  * why, in the future, you won't have to worry about sunburns; and

  * how a few years from now, anyone can become a Spock.

### 2. Evolution is picking up speed. Each development builds and moves faster ahead than the last. 

Think about how many major technological changes your grandparents may have witnessed during their lifetimes. Now, think about the changes you've seen over just the _last 15 years_.

Stunning how far technology has advanced in such a short period of time, right?

It's evident that, as time passes, things are changing more rapidly. History tells us that the rate of change has been growing _exponentially_.

Roughly 3.8 billion years ago, single-celled life on earth evolved slowly. It took some 2 billion years for multicellular organisms to emerge.

Yet gradually, the process of evolution gained momentum. For example, there were only 200 million years between the first mammals and the evolution of Homo sapiens.

If you were to draw a graph showing major evolutionary development on earth, you'd see that evolution does indeed move faster with the passing of time. The same can be said for the rate of technological evolution.

Technological development is also accelerating. Some 50,000 years ago, discoveries such as making fire were few and far between, occuring every 1,000 years or so. Compare this rate with today, where it's a challenge to list the gadgets and revelations of just last year.

Importantly, the returns of this accelerating process of evolution are _also_ accelerating.

For instance, computer speed in cost per unit doubled every three years between 1910 and 1950, then every two years between 1950 and 1966; today, it's doubling each year.

This exponential development in technology is known as the "Law of Accelerating Returns."

We know that the greatest triumphs in each stage of development help to form the next. For example, biological evolution resulted in Homo sapiens; humans then invented technology, and the best technologies are used to develop even better technologies.

It follows then that eventually, supersmart computers will be able to design superior technologies themselves and in doing so, further speed up technological evolution.

> _"The future ain't what it used to be." — Yogi Berra_

### 3. Computers are making leaps and bounds in speed and processing power. DNA computing is near. 

Computers are evolving rapidly, especially as ever-shrinking silicon components improve their performance. But even silicon has its limitations.

Very thin silicon components leak electricity, and densely wired chips overheat, which lowers efficiency. To counter these problems, new technologies are already in the pipeline.

One such invention is _nanotube technology_, an invention that will significantly speed computer calculations.

Nanotubes are tiny cylinders made out of sheets of carbon atoms. They are excellent candidates as chip components, because electrons pass through nanotubes far more easily than through silicon-based transistors.

The result is that data can be transmitted more quickly. In fact, computer scientist Peter Burke says the theoretical speed limit for a nanotube transistor computer would reach 100 times that of a conventional computer!

Another advantage is that many minuscule nanotubes can fit onto a single chip, speeding performance while avoiding the losses typical of scaled-down silicon chips.

Additional advances in three-dimensional (3D) chips and DNA computing are on the horizon. While chips traditionally contain a single, flat layer of transistors, 3D chips can vertically stack multiple layers of transistors.

Connections within a chip are in essence shorter than connections between chips, such as a processor chip connected to memory chips, which today exist separately. With this stacked organization, data needn't travel so far to execute a computation — and this massively increases processing speed.

Although still in its early stages, DNA computing could transform the future of computers.

The great benefit of DNA is its staggering memory capacity, in that one cubic centimeter of DNA could store more information than one trillion music CDs!

We've already witnessed some of what's possible with DNA computing. In 2002, Israeli scientist Ehud Shapiro and his team developed a DNA-based computer that could perform 330 trillion operations per second — more than 100,000 times the speed of the fastest PC at that time.

### 4. Over the next few decades, computers will learn to do everything humans can do, only better. 

Sure, computers can work quickly and efficiently, yet most of us wouldn't necessarily think of a computer as _intelligent_. A computer lacks insight, social intelligence and consciousness.

But this won't always be the case — and may change sooner than you think. Computer scientists today are exploring ways for computers to emulate what makes humans unique.

Let's take how the human brain works. Given sufficient scanning equipment, scientists may be able to accurately pinpoint exactly how the brain operates when you perform an "intelligent" act.

For instance, a scientist could track which neurons fire when you observe a person's facial expressions, or read a poem. These findings could then be translated into complex algorithms, or formulas that can be understood and processed by computers.

By 2030, these discoveries will give computer scientists the tools to reverse engineer the human brain. That is, they could build and program a computer that could imitate the human brain, and even become conscious — the advent of true _artificial intelligence_.

So what other changes are ahead? Certainly the computational power of artificial intelligence will exceed the capacity required to emulate all the functions of the human brain.

Scientists predict that it'll take between 100 trillion (ten to the power of 14) and ten quadrillion (ten to the power of 16) _calculations per second_ (CPS) to do so.

How did scientists reach this figure? They measured how many CPS it took to simulate specific brain functions, such as the localization of sound, and what percent of the brain was involved in performing such a task. Then they extrapolated the results to make predictions about the rest of the brain.

In the future, it might not be unusual for your smartphone to be smarter than you are today!

But it's not just machines that will reap the benefits of such advances. The next blinks will explain that humans will benefit, too.

### 5. Nanobots will be the new doctors. Patrolling our insides, they’ll cure illness and repair damage. 

Over the centuries, the medical community has made significant breakthroughs in treating afflictions once thought incurable. Yet humans are still vulnerable to many deadly diseases and other maladies.

The good news is advances in robotics and nanotechnology over the next few decades will help people resist and even overcome physical afflictions.

How? Through the use of small, nanotechnological robots, called _nanobots_.

By putting these nanobots in your body, you will be able to alleviate any ailment or sickly bug that threatens your health. As nanobots can self-replicate, just one visit to the doctor's (for an initial nanobot injection) will be sufficient to keep you healthy for the long term.

But what will nanobots do exactly? These small fighters will be able to eliminate toxins, viral DNA and bacteria from your bloodstream. Just as our white blood cells fight pathogens in our bodies, nanobots will do the same, but will be far more effective in doing so.

Nanobots will also be able to clean plaque from blood vessels, the buildup of which poses a risk of heart disease or stroke, and could even cure Alzheimer's disease by eliminating dangerous chemical deposits in the brain.

These tiny robots could even be deployed to deliver medication to individual cells. For instance, a nanobot could deliver an aggressive cancer medication specifically to cancerous cells, thus keeping a patient from suffering common, systemic side effects such as nausea or hair loss.

It might even be possible to control your own nanobots via the internet.

Nanobots could also be used to repair damaged genes by scanning and monitoring a cell's nucleus and ensuring that everything was in order.

Consider the example of sunburn. If you suffered a serious burn at the beach, you wouldn't need to worry about the future threat of skin cancer, as your nanobots would repair the sun-damaged DNA in all your affected cells!

### 6. Genetic disease will be a thing of the past. Gene therapy to repair damage will become the norm. 

Even though gene therapies have been talked about for decades, we still tend to think of our DNA as a fixed foundation of who we are — something that can't change easily, or even at all.

But we're discovering that this isn't the case. Soon, gene therapy will become a commonplace intervention. One method would be using a simple virus to transfer genes into human cells.

When scientists want to replace a defective gene with a healthy one, there's always the problem of how to transfer DNA into a defective cell. But there could be a way around this, using a virus as a sort of "gene taxi."

Many viruses are effective at delivering genetic material to human cells, so all that needs to happen is to exchange the genes the virus would deliver with therapeutic ones. For example, one research team in Glasgow used a modified virus to switch genes in a patient's blood vessels.

Gene therapy may also help to heal many acute illnesses with a straight-forward injection.

Different genes in people can elevate the risk for particular illnesses, such as diabetes. This doesn't mean that everyone with a particular gene will get the disease, but if you do have the gene, you'll need to be wary, managing your diet and keeping physically fit.

Other severe illnesses can be caused by a damaged or altered gene, as is the case with cancer or sickle-cell anemia.

The good news is, once gene therapies are in play, a virus can be injected into your bloodstream to "infect" your cells with therapeutic DNA. Then, once nanobots are able to watch over your health, it will become even easier to repair damaged DNA as soon as it's necessary.

> _"The first half of the...century will be characterized by three overlapping revolutions — in genetics, nanotechnology and robotics."_

### 7. No more donor organs and risky surgery. In the future, we’ll grow our own, from our own cells. 

Every day in the United States alone, 21 people die because of a lack of available donor organs.

In the near future, this will fortunately no longer be the case. We'll be able to _clone_ the organs we need to save lives. Therapeutic cloning has the potential to vastly improve transplantation medicine.

Traditional organ transplant procedures have serious flaws. Usually, transplanted tissues come from an unrelated donor, a situation that often triggers the host's immune system to defend itself and potentially reject the transplanted organ.

To make sure a donor organ survives, the host's immune system must be suppressed at considerable cost, as impairing an immune system escalates the risk of contracting infectious disease or certain cancers.

An ideal transplant could be created using a host's own cells — and amazingly, we're already extremely close to doing just this.

Advances in biotechnology are offering ways to convert one type of cell, such as a skin cell, into another type, such as a pancreatic cell or nerve cell. Soon it will be possible to use these cells to grow custom-made organs _in vitro_.

But even if you don't require a transplant, cloning can provide a great way to revitalize the body in a non-invasive way. Once in the bloodstream, newly cloned cells can find their way to the appropriate organ or tissue, and set to work on replacing aging cells.

Thus, bit by bit, we'll be able to renew our bodies without the need for surgery. This is particularly valuable when it comes to organs like the heart, because as we age, our bodies become unable to replace older heart cells quickly enough.

So nanotechnology and cloning will help us sustain and renew our bodies, yet here we're still talking about a biological human body. These "old-school" bodies are quite different from the radically new types of bodies we'll begin to inhabit in the 2030s and 2040s.

### 8. Ready to become a bionic man or woman? Or perhaps something in between? The choice is yours. 

Retinal implants. Pacemakers. Cochlear implants. It may sound like the stuff of science fiction, but we're already on our way to becoming cyborgs.

Today plenty of people think nothing of hearing aids for the ear or lens implants for the eye. But these small adjustments are just the beginning. Around the year 2030, your body will be more non-biological than biological.

By this time, many of your organs will be replaced by electronic devices with the power to vastly enhance your body's performance.

Here's where nanobots come in again. Your heart, lungs and blood will be replaced by _respirocytes_, or nanobots that deliver oxygen to the bloodstream and remove carbon dioxide. Respirocytes will be so efficient that you could run an Olympic sprint for 15 minutes without taking a single breath!

Likewise, you won't need a digestive tract or kidneys, as you'll have feeding nanobots to deliver nutrients to every cell and elimination nanobots to clean up and leave the body, taking the waste with them. As a result, humans won't suffer from vitamin deficiency or be overweight.

As we push past the 2030s, our bodies will undergo a fundamental, liberating makeover.

We'll still preserve the body's functions as we know them, with nanobots interacting with biological cells. But by the late 2030s, we'll exchange this "body 2.0" with something far more durable and renewable, a "body 3.0."

This new body will be made up of "foglets," or nanobots that can assume any form and can directly manipulate visual information and sound waves, meaning you could create any projection you wish people to see or hear.

These foglets will give you the ability to change your appearance at will. If you wanted to, you could morph from Spock to a supermodel in a matter of seconds!

> _"The essence of being human is not our limitations...it's our ability to reach beyond our limitations."_

### 9. Artificial Intelligence and human intelligence will merge, and humans will become supersmart. 

As developments in brain-computer interfaces improve, the boundaries between artificial intelligence (AI) and human intelligence will begin to blur.

Whether you want to enhance your brain with a memory chip, plug into a _Matrix-_ style simulated reality or steer a car using only your thoughts, you'll need a powerful brain-computer interface.

Scientific labs worldwide are already exploring new possibilities for brain-computer interfaces.

In 2003, German semiconductor maker Infineon, with assistance from the Max Planck Institute, created a "neurochip," or a chip that's directly connected to living nerve cells.

Using around 16,000 sensors, the chip can survey the electrical activity of several cells at once. With such chips, researchers can investigate how nerve tissues react to electrical stimulation, and thus discover more about the future possibilities of neuron-computer interaction. 

Once these neurochip implants have made their way into our brains and our thoughts, it will be nearly impossible to distinguish between human intelligence and AI.

And with implants like these, we humans are going to become incredibly intelligent.

First we'll use memory chips to greatly expand our long-term memories, which means no more frustrating "tip-of-the-tongue" syndrome, as we'll be able to recall information at will.

Chips will expand our working memory capacity, which is strongly linked to intelligence. We'll be able to process data and make calculations far better than we've ever experienced.

By the end of the 2030s, your brain will also use nanobots to connect to the internet and download whatever information you want to know — just like Neo "knew" kung fu in _The Matrix_.

> _"Will robots inherit the earth? Yes, but they'll be our children." — Marvin Minsky_

### 10. Around the year 2045, the world will change beyond recognition. The singularity will have arrived. 

The future we've talked about may seem weird or scary — but it's also amazing. Just think about all those nanobots coursing through your blood and brain! While not yet a reality, such concepts are still based in what we know and can comprehend.

Around 2045, however, the world will experience the "singularity." Then things will really get weird.

The singularity will be a fundamental shift in human history, a change beyond anything we can imagine today.

The developments we've seen so far will expand even further. In 2045, with just $1,000, you'll be able to purchase a computer a billion times more intelligent than all humanity _combined_.

Machines will be able to think and communicate so rapidly that humans won't be able to understand any of it. Machines too will develop revolutionary technologies in a matter of days.

In the future we'll be equipped to fight most illnesses; but by 2045, we'll have the means to become virtually immortal!

In the post-singularity world, intelligence will unfold throughout the universe.

Computer chips will eventually reach a physical limit. Once this limit has been reached, computers will have to increase in size to keep increasing in power, and they'll do this quickly.

Soon, more and more of the earth will be set up as a giant computer. This will likely involve the work of nanobots, turning objects into intelligent systems.

Nanobots require carbon, accessible through dead wood or ashes, for example, to self-replicate. The resultant group of nanobots will then organize themselves into a computer. Afterwards, even the materials of stars and planets will be reorganized to be transformed into computers!

### 11. A future run by sentient robots isn’t perfect. Nanobots could malfunction, and cause havoc on earth. 

Does the prospect of a singularity creep you out? Maybe even terrify you?

Well, the concept itself shouldn't. Thanks to your brain implants, by 2045 you'll be highly intelligent and have no problem keeping up with the rapidly evolving world.

Before then, however, something could go wrong. The most likely scenario involves nanobots and their ability to self-replicate.

Once nanobots are introduced into our bodies, we'll need lots of them to do the jobs we set for them. So it makes sense if nanobots can replicate themselves.

Nanobots assigned to your brain or immune system, for example, will need to be replaced as soon as possible if they're destroyed. If you lose too many, your body may simply stop functioning — just as if in a normal body, too many brain cells are destroyed.

So the most efficient way to replace nanobots is to have them self-replicate.

But this is not without consequence, as self-replicating nanobots could run amok.

Nanobots could be infected with a virus that turns them into destructive bots, tampering with their ability to self-regulate. A worst-case scenario would be if infected nanobots started acting like cancer cells, self-replicating without limit.

Out-of-control nanobots in the brain is no laughing matter. But if nanobots started to multiply outside a body, it could be as catastrophic as a nuclear explosion.

Carbon atoms are the building blocks of nanobots, and they require carbon to survive. Yet if nanobots were to replicate uncontrollably, they might decimate the earth's biomass, or material from living or dead plants, animals _and_ humans.

It would take a mere 130 replications before all biomass — that is, _all life on earth_ — would be destroyed. This would be quick, from between three hours and a few days!

Providing nanobots _don't_ take over, humanity is in for a remarkable future.

Everyone who lives to see 2045 may "live" forever, transform into a powerful cyborg and communicate daily with supersmart, sentient robots.

### 12. Final summary 

The key message in this book:

**Over the next few decades, artificial intelligence will eclipse biological human intelligence. But we will also become "artificially" intelligent. Gradually, our biological bodies and brains will be replaced and enhanced by devices, and humanity will be altered beyond anything we can imagine.**

**Suggested further reading:** ** _The_** **_Second_** **_Machine_** **_Age_** **by Erik Brynjolfsson and Andrew McAfee**

_The_ _Second_ _Machine_ _Age_ examines how technological progress is drastically changing our society, and why this development is not necessarily positive. It compares the rapid development of computer technology to the advent of the steam engine, which once catapulted the world into an Industrial Revolution.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ray Kurzweil

Ray Kurzweil is an inventor, entrepreneur and a director of engineering at Google. In addition to inventing the charge-coupled device (CCD) flatbed scanner, Kurzweil has written a number of books and scripts addressing the integrated future of humans and technology. He was awarded the US National Medal of Technology in 1999 and, over the years, has received 20 honorary doctorates as well as honors from three US presidents.

