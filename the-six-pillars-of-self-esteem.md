---
id: 5475193930386200093c0000
slug: the-six-pillars-of-self-esteem-en
published_date: 2014-11-28T00:00:00.000+00:00
author: Nathaniel Branden
title: The Six Pillars of Self-Esteem
subtitle: The Definitive Work on Self-Esteem by the Leading Pioneer in the Field
main_color: D76E40
text_color: 73684F
---

# The Six Pillars of Self-Esteem

_The Definitive Work on Self-Esteem by the Leading Pioneer in the Field_

**Nathaniel Branden**

_The Six Pillars of Self-Esteem_ shows us that building confidence in ourselves is just a matter of taking a few simple steps. Healthy self-esteem is something we can achieve not just as individuals, but in our relationships and communities too.

---
### 1. What’s in it for me? Learn how to nurture your self-esteem. 

Self-esteem has never been a more important and debated concept than today. There's a wealth of definitions and quick guides to achieving exemplary self-esteem, but what do we really know about this supremely important psychological concept? The author draws on a lifetime of clinical practice and research to offer a comprehensive definition.

These blinks take a journey through the six different practices for daily living that create healthy self-esteem. You'll find out how increased awareness, acceptance, responsibility, assertiveness, purposefulness and integrity will raise your self-esteem.

You'll also find out

  * what self-esteem and calcium have in common;

  * how upsetting your child ruins their self-esteem; and

  * that it's more difficult than you'd expect to assert your own right to exist.

### 2. Self-esteem is the immune system of consciousness, essential for performing at your best. 

"Ten steps to build your self-esteem!" "Five tips to gain confidence!" How many self-help gurus have you heard spout claims like these? Probably many. We hear about self-esteem all the time, but would you be able to define it?

Self-esteem is the _immune system of consciousness_ ; it provides strength, resistance, and the ability to regenerate. Just like our immune system, self-esteem is innate, and we need it to handle life's difficulties.

Calcium is another fitting analogy for self-esteem. Calcium strengthens our teeth and bones and is essential for a healthy body, while self-esteem is vital for strong psychological development. While we wouldn't necessarily die from a lack of calcium, our ability to live life fully would be significantly limited. The same goes for self-esteem: We don't need it to survive, but we cannot live a full life without it. Why is this?

It's all to do with the way our self-esteem works — by creating certain _expectations_ about what we are capable of. These expectations influence our behavior in a way that turns them into reality. Self-esteem becomes a self-fulfilling prophecy.

Consider the story of a recovering alcoholic from the author's psychotherapy practice. He was about to land the biggest commission of his career as an architect, but instead of being excited, he was incredibly anxious and felt that he didn't deserve it. Why? Because of his low self-esteem and the low expectations he had for himself. In order to steady his nerves, he chose to drink, became very drunk, behaved rudely and lost the job. Unfortunately, his low self-esteem led to his downfall.

### 3. Self-esteem is about fighting for your right to happiness and facing challenges with confidence. 

Because self-esteem is so important for our consciousness, it's worth looking into the topic a bit more, starting with the fundamental ideas it is based upon.

Self-esteem really comes down to something quite simple — we all have a _right_ to be happy. From this, it follows that _high_ self-esteem sees us assert this right and take steps to achieve it. On the other hand, when we let our right to happiness be overridden, we have _low_ self-esteem.

Let's explore this difference in real terms. Consider the client who asked the author why she always fell for married guys who didn't care about her. This pattern started to make more sense once the client revealed that her father abandoned her family when she was a child, and her mother blamed her. So how does this relate to self-esteem?

Her father's departure and her mother's negativity _shaped_ her self-esteem by making her feel that she was unworthy of love. Later in life, she began to behave in a way that made her reality conform to this belief: By falling for married men who'd always leave her, she reinforced the feeling that she was indeed undeserving of love. Low self-esteem often manifests itself like this: we make choices that make our negative beliefs about others become reality, often harming ourselves in the process.

Conversely, if we have high self-esteem, we're not only less likely to create problems for ourselves, but also better at persevering in the face of difficulties. This is something psychologists have proven by setting subjects with varying levels of self-esteem the same task. Unbeknownst to the participants, the tasks include several unsolvable problems. Nevertheless, the high self-esteem participants persevere for much longer than those with low self-esteem.

This study shows that our view of ourselves strongly determines how we respond to challenges — this is the power of self-esteem. Read on to find out how you can harness it!

> _"To trust one's mind and to know that one is worthy of happiness is the essence of self-esteem."_

### 4. The first pillar is the mind-set and practice of living consciously. 

So what do we have to _do_ to build self-esteem? It's not as abstract as you might think. Let's examine the six pillars which will guide us.

It begins with a shift in mind-set. If we want to improve our self-esteem, we must first start _living consciously_.

And no, living consciously doesn't entail some sort of zen, esoteric approach to life. Living consciously means simply being willing to distinguish between three facets of perception: facts, interpretation, and emotion. Here's a basic example of how those three can get tangled together: Because you _see_ your boyfriend frowning, you _interpret_ that he is angry, which makes you _feel_ hurt.

But what if you didn't really see him frowning? If you simply realize that you may have misinterpreted his facial expression, you can reassess your interpretations from a distance, without reacting emotionally.

We can retain this mind-set by asking ourselves simple questions throughout the day, such as: "How am I feeling at the moment?" "Why am I feeling this way?" and "Do my actions match my feelings?" This way, we'll stay in touch with our internal world.

However, living consciously isn't just a mind-set — it's a practice too. We've got to keep seeking information from our environment, and adjusting our actions accordingly. Here's a simple example that shows conscious living as a mind-set and practice.

Say you'd like to buy a new outfit. You'll have worked this out by consciously engaging with your "internal world," or your desire to look different. But then you'll also need to check your external world too, or, more specifically, whether you have enough money — the "practice" side of your purchase. As this process takes both your feelings and your situation into account, you can be sure that the decision you reach will be a sound one.

Living consciously in this way provides a fundamental understanding of ourselves, which is a prerequisite to nourishing our well-being.

> _"When we live consciously we do not imagine that our feelings are an infallible guide to truth."_

### 5. The second and third pillars of self-esteem will teach you to accept yourself and take charge of your own happiness. 

Self-acceptance, self-responsibility, self-esteem. The first two are so bound up in the third that it's a little hard to see the difference — the fact that they share the same prefix doesn't help! But the difference is clearer than you think. Self-acceptance and self-responsibility are things that we _do_ so that we can _increase_ our self-esteem as a result.

When we choose to value ourselves, we're practicing _self-acceptance_, the second pillar of self-esteem.

For example, have you done anything in the last week that you regret? Snapped at someone, or neglected something you shouldn't have? Self-acceptance doesn't mean you justify or like these negative actions, rather it seeks to understand the underlying causes that lead to them — perhaps you felt patronized, or were stressed about something else. If you accept why you reacted this way, you will reduce the likelihood of this undesirable behavior recurring.

But if we accept our current behavior, isn't there a risk that we'll grow complacent and lack the motivation to change it? Herein lies the paradox: if you don't accept yourself as you are now, you'll never find the drive to improve, as you'll spend all your energy agonizing over your shortcomings.

Self-acceptance also goes hand in hand with taking responsibility for ourselves. The practice of _self-responsibility_ is the third pillar of self-esteem. It entails taking control of your existence, and your happiness, by being _solution-oriented_. This means asking the following question whenever a problem arises — "What can _I_ do about it?"

Don't try to blame others, take responsibility for your behavior. Instead of saying "He pushes my buttons" or "I would act different, if only she would... " remember that it's not someone else's job to make us happy. Yes, that task belongs to you! Acknowledging this fact will help us empower ourselves — a crucial part of self-esteem, as we'll find out in the following blinks.

### 6. The fourth pillar of self-esteem is self-assertiveness – this entails standing up for yourself, which is more difficult than you might think. 

"I have a right to exist" — it's a statement you can agree with, right? Now here's a challenge for you: say those words out loud. Even when you're on your own, doing this feels a little embarrassing. But why?

Most of us don't realize this, but we often struggle to assert such a basic right. To highlight this, the author asked his class of psychology students if they believed that they had a right to exist. Everyone agreed, but when asked to say that statement out loud, they tensed up and even sounded fearful.

This subtle fear of asserting our rights is actually quite normal. It comes down to the following instinctive thought processes: "If I express myself, I may provoke disapproval," or "If I affirm myself, I may provoke resentment." This mind-set is a direct obstacle to building our confidence, but we can counter it by adhering to the fourth pillar of self-esteem: _self-assertiveness_.

To be self-assertive is to simply openly be who you are. And to practice self-assertiveness, you need the conviction that your beliefs are important. For example, say you're at a party and you hear a racial slur that you find offensive. Having the courage to state your opinion is practicing self-assertiveness. Or perhaps you've just seen a movie with your friends and you felt deeply moved by it. Say so, and don't shrug indifferently for fear of being considered uncool.

Each time you express yourself or stand up for your values, you strengthen your sense of self-esteem. But, there are still two more pillars to follow for a healthy self-esteem.

> _"Self-assertiveness means the willingness to stand up for myself, to be who I am openly, to treat myself with respect in all human encounters."_

### 7. The fifth and sixth pillars are living purposefully and practicing personal integrity. 

How do you want to live your life? Many would say "with purpose and integrity." As it turns out, these two qualities are also vital pillars for healthy self-esteem.

As we build our confidence, it's essential that we first take responsibility for our goals. By asking yourself what you want and where you want to go, you've already started to take the fifth pillar of self-esteem, _living purposefully_, as your guide. But that's not enough. You have to continue to monitor your own progress.

We can see this in the story of Jack. Jack dreamt all his life of becoming a writer, but instead of building his skills, he decided to just "wait until he felt ready." So what do you think happened? Was Jack's desire to become a writer enough to make it happen without any work on his part? Of course not. Years later, Jack was no closer to achieving his dream. Although he'd outlined a clear purpose for himself (being a writer), Jack did not take any proactive steps to achieve it, so he failed to live purposefully.

So our self-esteem depends on making our actions match our goals. But it also entails making our behavior match our words, or having _personal integrity_. This trait can manifest itself in a number of small everyday things, such as keeping promises and telling the truth even when lying would make things more pleasant. A lack of personal integrity, on the other hand, is shown by dishonesty and hypocrisy.

Sometimes, personal integrity can be the most difficult pillar to practice, given that we live in a society where amorality is normal, and cynicism is even considered cool. And yet, personal integrity is so vital for a healthy self-esteem, that you should not let those around you tempt you into sacrificing it.

### 8. Parents and teachers have a central role in nurturing a child’s self-esteem. 

So far, we've explored the self-esteem-bolstering actions and mind-sets that lie within our own power. But there's another factor that can be hugely influential on our self-esteem — the people that have been with us from the beginning of our lives, _our parents._

Parents can make it both easier and harder for a child to develop self-esteem. A study by psychologist Stanley Coopersmith found that while parental factors such as social class, money, education and geography didn't contribute to a child's self-esteem, the quality of the relationship did.

But what creates a positive relationship between parent and child? There are several aspects to it: First, parents need to be accepting of their child's thoughts and feelings, so that she learns that they are valid and valuable. Second, parents themselves should demonstrate their own high levels of self-esteem so that children can learn from them as role models. And third, parents should set clear boundaries for their child, to create a firm sense of security.

This may seem like a lot, but it is crucial for a child's development. If parents cause their children to believe that something is wrong with them, low self-esteem will quickly take root.

Luckily, children who haven't developed a healthy self-esteem in their early years have a second chance at school. A teacher that builds confidence in a child can have an incredible, positive influence. In fact, research shows that a teacher's expectations can be self-fulfilling prophecies. If a teacher has faith in the child's ability to master a particular skill, this belief often becomes reality.

However, a teacher's motivation should never be based on applauding success or punishing failure. Rather, teachers succeed when they nurture a _reality-based_ self-esteem. The key to this approach is making the child feel acknowledged, so that their thoughts and actions are recognized and valued. This is achieved through positive constructive feedback that encourages them as they grow and succeed.

> _"No one was ever made 'good' by being informed he or she was bad."_

### 9. Successful companies foster high self-esteem in their employees. 

So far we've seen how self-esteem can be useful in our personal lives. But what about the workplace?

There's been a surge in entrepreneurship in recent decades: by the late 1980s, up to 700,000 new enterprises were being founded every year, compared to only 100,000 during the peak years of the 1960s. With this entrepreneurial explosion came an urgent need for leaders and employees who were capable of self-direction, personal responsibility and initiative. These are all traits fostered by high self-esteem.

It's no surprise that successful companies are geared toward nurturing self-esteem. But how can an organization foster this precious quality?

One simple rule is to give employees feedback that builds on their strengths. This makes them feel good about themselves and more confident about facing future challenges.

Leaders also play a big part: they can nurture high self-esteem by, for example, constantly finding new ways to stimulate innovation and creativity in their organizations.

Self-esteem and its six pillars aren't just something you can strive for as an individual — they are values that help organizations grow and thrive too.

### 10. Final summary 

The key message in this book:

**Self-esteem is a fundamental human need that makes us better able to cope with life's difficulties. To achieve self-esteem, draw on the six action-based** ** _pillars_** **. These are the practices of living consciously, self-acceptance, self-responsibility, self-assertiveness, living purposefully and personal integrity.**

Actionable advice:

**Use sentence completion to boost your self-esteem.**

"Sentence completion" is a simple but powerful exercise for strengthening your self-esteem. Take a series of sentence stems such as

"If I bring a higher level of self-esteem to my activities today—" or

"If I bring a higher level of self-esteem to my dealings with people today—"

and jot down many different endings to them without pausing for reflection. It doesn't matter what you write, as long as it's a grammatical completion of the stem. Doing this exercise for ten minutes every morning can help you facilitate self-understanding and personal growth. You can find more sentence stems on the author's homepage.

**Suggested further reading:** ** _Learned Optimism_** **by Martin E.P. Seligman, Ph.D.**

_Learned Optimism_ explains why so many people grow up to be pessimistic and what the negative implications of this habit are. Furthermore, it shows how our habitual optimism or pessimism influences us for better or for worse in all areas of life. Finally, it shows how pessimists can learn how to become optimists, thus greatly improving their health and happiness, and presents several techniques for learning this new way of thinking.
---

### Nathaniel Branden

Nathaniel Branden is an American psychotherapist and writer. He has written numerous books on the topic of self-esteem, such as _The Psychology of Self-Esteem_, _How to Raise Your Self-Esteem_ and _The Power of Self-Esteem_.

