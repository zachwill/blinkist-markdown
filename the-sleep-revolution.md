---
id: 5776159b86883200034f4744
slug: the-sleep-revolution-en
published_date: 2016-07-04T00:00:00.000+00:00
author: Arianna Huffington
title: The Sleep Revolution
subtitle: Transforming Your Life One Night At A Time
main_color: 4292C3
text_color: 2773A3
---

# The Sleep Revolution

_Transforming Your Life One Night At A Time_

**Arianna Huffington**

These blinks are about the importance of a basic human necessity that we often brush aside: sleep. Getting enough sleep isn't just about feeling better in the morning — it improves your work performance, health and even your personal relationships. Similarly, sleep deprivation isn't a by-product of hard work; rather, it _prevents_ you from reaching your full potential. _The Sleep Revolution_ (2016) explains why sleep is so critical, and what you can do to get more of it.

---
### 1. What’s in it for me? Sleep your way to the top. 

Be honest: how many hours of sleep do you get a night? Chances are it's less than seven — which is understandable, given how packed daily routines tend to be. Between all the tasks you have to finish at work, meeting up with friends, going to the gym and updating all your social media profiles, there's simply too much to do to find enough time for a proper night's sleep.

But we neglect sleep at our peril. As it turns out, nothing is more important to our health, happiness and productivity than getting enough high-quality sleep. And that's where these blinks can help. They explain exactly why sleep is so important, and how we all can ensure we get enough of it.

In these blinks, you'll also find out

  * why, even on a school day, we should let our children enjoy a lie-in;

  * why couples who sleep together often stay together; and

  * why we all should fill our rooms with lavender.

**This is a Blinkist staff pick**

_"These blinks are_ _a sobering exploration of the dangers of sleep deprivation and workaholism. They're especially poignant today, where our studies and careers seem to demand more and more work and less and less sleep. I loved the tip about putting all screens away by 9 p.m. every night."_

– Clare, Editorial Quality Lead at Blinkist

### 2. Workaholism is robbing Americans of sleep, and the poor suffer the most. 

Sarvshreshth Gupta, a 22-year-old analyst at Goldman Sachs, hadn't slept for two consecutive nights when he called his father from his office cubicle at 2:40 a.m. one night. Although Gupta's father tried to calm him down, Sarvshreshth was found dead on the pavement outside his high-rise apartment building just a few hours later. He jumped to his death, no longer able to bear the pressure of work.

Like Gupta, Americans today are suffering from sleep deprivation because of compulsive working, or _workaholism_.

Workaholism is on the rise. Between 1990 and 2000, the average annual American workload increased by a full workweek. In 2014, when a travel company called Skift conducted a survey to figure out why so few people were booking their holiday packages, they found that over 40 percent of the American workforce hadn't taken a _single_ vacation day that year.

This unhealthy culture of workaholism prevents us from getting enough sleep. In fact, according to a 2010 US government report, 30 percent of all employees get less than six hours of sleep per night, while nearly 70 percent describe their sleep as "insufficient."

And who suffers the most? The working poor.

Lower-class workers often have to take on several jobs just to pay their bills, so they don't have time to make sleep a priority. A 2013 survey from the University of Chicago found that a person's quality of sleep decreases as their wealth decreases, which causes sleepiness and can even lead to sleep-related diseases.

Being overworked isn't the only problem for the poor, either. A professor at Stony Brook University found that poor neighborhoods also tend to be noisier, which makes it even harder to get a good night's sleep.

### 3. Schoolchildren perform better when they sleep in and follow their natural circadian rhythms. 

When you were a child, did you ever struggle to stay awake in school or wish you could take 40 winks at your desk until you were ready to face the day? Well, it turns out it might have been better if you _had_ dozed off!

Sleep deprivation starts at a young age, when children are pulled out of bed for school before their bodies and minds are ready to wake up.

In 1998, a team of researchers from Brown University did a study on the effect of early school start times on children's health. They found that children did well when school started at 8:25 a.m., but suffered as they got older and their schedules changed, requiring them to be in the classroom by 7:20 a.m.

The researchers took these children out of class at 8:30 a.m. and evaluated their sleepiness. Half of them fell into a deep sleep within just three minutes of lying down — something that usually only happens in people who suffer from narcolepsy.

Students are pathologically tired when they're forced to get up this early every school day. It disrupts their natural sleep-wake rhythm, or _circadian rhythm_.

Conversely, when students get better sleep, their performance improves. In 2011, the Technion Institute of Technology in Israel found that their students' attention levels improved considerably when they began starting class 8:30 a.m. instead of 7:30 a.m.

A British high school in North Tyneside even experimented with having classes start at 10:00 a.m., instead of their already relatively late start time of 8:50 a.m., and their students ended up scoring much better on tests.

So, it turns out that less school might actually mean better grades!

> _"Sleep really affects teens' ability to function and make good decisions."_ \- Dr. Maida Chen, Seattle Children's Hospital

### 4. Employees are more productive when they have nap rooms and daylight, and are allowed to sleep in or work from home. 

There's an episode of _Seinfeld_ in which George Costanza buys a custom-made desk that can fold into a bed, and back into a desk at the push of a button. The desk contraption lets him get away with sleeping in his office. Thankfully, this kind of subterfuge is no longer necessary in many modern work spaces!

Work spaces benefit when they have nap rooms for letting their employees catch up on sleep.

Despite prevailing opinions to the contrary, sleeping at work isn't a sign of laziness. When nap rooms were first introduced in _The_ _Huffington Post_ 's offices in New York, a lot of hard-working employees regarded them with skepticism. Four years later, they're constantly overbooked! Other major companies like Ben & Jerry's, Zappos and Nike have followed _The Huffington Post_ 's example.

Companies can promote healthy sleep habits in other ways, too, such as by making sure they have enough windows. Daylight also makes for a more restful atmosphere, and it's important for your body clock. A 2014 study at the University of Illinois found that employees who work in windowless offices lose an average of 46 minutes of sleep per night. Why? Because our bodies need daylight to maintain their circadian rhythms.

And aside from nap rooms and windows, there are even better ways to get more rest: sleeping in or working from home.

Employees get better sleep and save travel time if they're allowed to sleep in, which is why offices with flexible hours are very productive. And according to a Stanford University study on workers in China, working from home is even better!

The study found that employees who worked from home were up to 13 percent more productive than those who only worked in the office.

### 5. Partners that sleep next to each other are usually happier – but when it comes to sex, it’s more important that both partners sleep well. 

People tend to assume that couples have always slept in the same bed, but it's actually a holdover from an era when few families could afford separate beds for every member. So should you and your partner sleep in separate beds if you can afford it? Not necessarily — sleeping together can also have the benefit of strengthening your relationship.

According to a 2014 study at the University of Hertfordshire, 94 percent of couples who sleep with their bodies touching each other are happy with their relationships. For couples who _don't_ sleep with their bodies in contact, the figure is 68 percent.

In fact, the correlation between relationship satisfaction and sleeping close to one's partner proved to be so high that the further away a couple slept from each other, the less happy they were with their relationship.

That doesn't mean couples _have_ to sleep together to be happy, however. It's more important that they both sleep _well_ — especially when it comes to sex.

Think about the above statistics again. The second figure is lower, but it still means that 68 percent of couples who don't sleep next to each other are happy with their relationships. What matters is that both partners get enough sleep, regardless of whether they're in the same bed.

Partners deprived of sleep will rarely be in the mood for sex. In fact, a 2015 study found that a woman's sex drive is directly correlated to how much deep sleep she gets. For every extra hour of sleep, there's a 14 percent increase in the likelihood that she'll want to have sex the next day.

### 6. Athletes often mistakenly believe that sleep deprivation is a sign of toughness, but getting enough sleep can give them a competitive edge. 

The cheetah is the fastest land animal on earth. Cheetahs can go from being stationary to running at 60 miles per hour in just a matter of seconds — but they also sleep as much as 18 hours in a day to rest after reaching such remarkable speeds. Unfortunately, human athletes aren't always as wise.

People often perceive sleep deprivation as macho or tough in the sporting world. Jon Gruden, one of the youngest and most successful coaches in the National Football League, or _NFL_, even wrote a best-selling book called _Do You Love Football?! Winning With Heart, Passion, and Not Much Sleep_.

In his book, Gruden boasts that he's one of the oldest coaches in the NFL if you consider how little he sleeps, because he works the longest hours. George Allen, one of his rival coaches, bragged that he sleeps at his office every night, and that he works 16 hours a day while only sleeping for four or five hours a night.

Gruden and Allen don't realize that these kinds of sleeping habits only harm their performance — and the performance of their players, who also need to get proper sleep.

In fact, getting enough sleep can mean the difference between winning and losing. Cheri Mah, a Stanford University researcher, did an experiment on this phenomenon with basketball players.

Mah observed their performance on the court by measuring their sprinting times and success with three-point shots. Then, she assessed them again after their sleep increased from 6.5 hours per night to 8.5 hours per night. Getting a full night's rest allowed them to lower their sprint times by 0.7 seconds and score over nine more three-point shots!

> _"Just as athletes need more calories than most people when they're in training, they need more sleep, too."_

### 7. Our addiction to electronic devices keeps us up at night. 

What's the last thing you do before you go to bed? Watch an episode of _House of Cards_? Write some emails? Read up on the news? Those activities might seem leisurely, but make sure you don't get addicted to your gadgets.

These days, people are increasingly addicted to electronic devices, and a surprising amount of people take them to bed! According to a 2015 _Consumer Mobility Report_ survey, 71 percent of people keep their smartphones by them while they sleep.

People are also more likely to get addicted to digital media that has an emotional element, like social media; this is especially bad for our sleep.

Heath Cleland Woods, a sleep researcher at the University of Glasgow, found that people sleep less well when they're emotionally invested in social media, and also suffer from higher rates of anxiety.

Emotions and stress aren't the only problems with social media, either; the blue light emitted from our devices also keeps us awake.

The light that comes off your screen suppresses your natural production of _melatonin_, a substance that helps us get to sleep. That's part of the reason so many people stay up later than their natural bedtime.

It's a vicious cycle, according to psychiatrist Dan Siegel from UCLA. When darkness falls, the body usually starts secreting melatonin, so that we fall asleep within a few hours. However, according to Siegel, if you're staring at your screen all evening, your body doesn't think it's time to sleep, even if it's past midnight. When we finally do try to go to sleep, we don't have enough melatonin to help us get there.

So, if you want to get a good night's rest, put your computer or phone away by 9 p.m. Try reading a book instead!

### 8. Acupuncture and herbal remedies can help you sleep better. 

Let's say you've put away your computer and phone, but you still can't get to sleep; it might be time for more direct action. You might not have guessed that sticking needles in your ear can help you get to sleep — but it can!

Acupuncture is an effective, natural way to improve your sleep. People have utilized it for centuries, but now even modern medicine has confirmed its efficacy.

Researchers at Emory University conducted research on previous sleep studies and found that a staggering 93 percent of research confirms that acupuncture is an effective way to cure insomnia. Moreover, several studies found that acupuncture points in the ear are especially useful when it comes to encouraging deep sleep. According to the _Toronto Center for Mental Health_, this is because stimulating these points increases the production of melatonin and helps reduce anxiety.

If you don't have the time to visit an acupuncturist, you can try it yourself by applying pressure to your acupressure points. Put a small amount of pressure on various spots on your wrist, ankles and ears, and take some deep breaths. Just go with your intuition. If you want to get more precise later on, you can seek the advice of an acupressure specialist.

Medicinal herbs like lavender can also boost your sleep quality. Dioscorides, a doctor in ancient Greece, wrote about the soothing properties of lavender as early as the first century. It was commonly used in ancient Greek and Roman bathhouses to help customers relax.

Modern science has confirmed the powers of lavender, too. It has been proven to slow heart rate, decrease blood pressure and lower skin temperature — another important factor in getting to sleep!

So try out some different herbs and scents, like lavender or valerian root, and find out which essential oils soothe you the most.

### 9. We now have new technology we can use to improve our sleep. 

Have you ever heard of wearable technology? Wearable technology doesn't refer to T-shirts with screens or dresses with computer chips, but rather to devices you wear to monitor your biological functions — like sleep!

There are a few sleep-enhancing technological gadgets available now, like smart sleep monitors and sleep-friendly artificial light.

A French company unveiled a new sleep-enhancing system in 2014. It consists of two devices, one of which you keep at your bedside. It controls levels of noise, light and temperature in the room. It also uses soft light and sounds to wake people up when they've slept enough or are at a point in their sleep cycle when they can wake up without feeling groggy.

The second device goes under your mattress. It measures your heart rate, breathing and involuntary movements while you sleep.

We saw earlier how damaging blue light can be for your sleep patterns. Fortunately, tech innovators are working on that problem! The software app _f.lux_, for instance, softens the light on your screen at night so as not to disrupt your melatonin levels.

You can also use technology to access tools for meditating or to help yourself relax. Letting go of your daily stress and anxiety is an important part of getting a good night's sleep, and meditation can help. There are a lot of guided meditations available online, like Louise Hay's meditations, which can easily be found on YouTube.

Methods like paying conscious attention to breathing or _Feldenkrais_ can help get you to sleep. Feldenkrais is a method that allows you to gradually increase body awareness and relaxation by performing small and slow movements.

These devices and methods can work wonders, but they alone won't fix your sleep problems. You should make sure you prepare yourself well for sleep, and make a commitment to getting enough of it.

### 10. Final summary 

The key message in this book:

**Sleep deprivation isn't a hallmark of hard work, dedication or toughness. This widely held belief is a myth, and one that's doing us harm. Everyone, from children to elite athletes to CEOS, is healthier and more productive when they get enough sleep. It's time for a sleep revolution.**

Actionable advice

**Donate one more hour to your sleep and see what happens.**

If you're clocking fewer than seven hours of sleep every night, try extending your slumber to seven or eight hours for a few nights and see if it makes a difference to how you feel and how productive you are the next day. You might be surprised what an extra hour can do for you!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Secret Life of Sleep_** **by Kat Duff**

_The Secret Life of Sleep_ (2014) takes an enlightening look at what exactly sleep is. Using cutting-edge scientific research and examples from cultures around the world, Kat Duff explores why and how we sleep, and what makes some Western sleeping patterns particularly unhealthy.
---

### Arianna Huffington

Arianna Huffington is the co-founder and editor-in-chief of _The Huffington Post,_ and a pioneer of implementing better sleep policies in offices. She is the author of several best-selling books, including _Third World America_ and _On Becoming Fearless_.

