---
id: 543533856663620008960000
slug: the-sleepwalkers-en
published_date: 2014-10-10T00:15:00.000+00:00
author: Christopher Clark
title: The Sleepwalkers
subtitle: How Europe Went To War in 1914
main_color: F2DEB6
text_color: 8A7448
---

# The Sleepwalkers

_How Europe Went To War in 1914_

**Christopher Clark**

Christopher Clark's _The Sleepwalkers_ takes a fresh look at the outbreak of the First World War, focusing on the alliances established among Europe's nations in the years leading up to 1914. In his compelling and masterful account, Clark examines the decisions, both big and small, that led to the outbreak, and investigates the common belief that the war was an inevitability.

---
### 1. What’s in it for me? See for yourself whether or not the Great War was inevitable. 

The year 2014 marks the 100th anniversary of the beginning of the First World War.

But, one hundred years later, do we really understand how the Great War started?

As you'll learn in these blinks, the assassination of the Austrian Archduke Franz Ferdinand and his wife by the Serbian terrorist group Black Hand was the trigger for a far bigger crisis.

Following the assassination, Austria-Hungary issued Serbia an ultimatum. When the demands weren't met, Austria declared war on Serbia. Russia, Germany and then France followed suit by mobilizing as well.

In these blinks, you'll find out what each of these nations did and why to escalate the aggression.

You'll also discover

  * how a single altercation between two countries could lead to a global war;

  * why several countries needed to go to war as soon as possible; and

  * why it was extremely difficult for any country to anticipate the actions of the others.

**This is a Blinkist staff pick**

_"These blinks present a clear and convincing explanation of how the world slid into the First World War. They suggest that the nations and empires of Europe ended up in conflict almost by accident. I found them fascinating, not only because of their description of politics 100 years ago, but also because of what they tell us about the world today."_

– Thomas, English Editorial Lead at Blinkist

### 2. The many alliances and connections between policymakers played a crucial role in the war’s outbreak. 

The First World War was one of the biggest catastrophes of the twentieth century. It pitted the armies of most European countries and their empires against each other and led to millions of deaths.

But why did it start in the first place? Which country was to blame?

Although there is no simple answer, many historians have put much of the blame on the _alliance system,_ a network that aligned each country either _with_ another or _against_ it. Most of the European powers had alliance treaties with others, meaning that if one country in the chain should be attacked, that country could depend on its allies to come to its aid.

For instance, the tiny country of Serbia was allied with Russia, which protected it from an attack by Austria. Austria itself was allied with Germany, which promised to respond if it were attacked. And Russia was allied with France, against the threat of a German attack.

However, while the purpose of the alliance system was to combat the threat of conflict, it actually increased the inherent risk in European power politics. If a war broke out in one region, the alliance system could trigger a chain reaction that would lead to a Europe-wide conflict.

The risk was further amplified because the alliance system had associations with some of Europe's most unstable regions, such as the Balkans.

The Balkans, in the south-eastern corner of Europe, had once been dominated by the Ottoman Empire. But the Ottoman Empire was in the process of collapse, and in the power vacuum, both Austria and Russia aimed to expand their interests to the area and were prepared to use force.

However, their objectives were complicated in that many different nationalities shared the same space: Slavs, Germans, Bosnians, Hungarians, Romanians and Bulgarians all lived, unsegregated, in the area. This made the area hard to control, and volatile.

### 3. The alliance system in Europe split the continent into two polarized camps, greatly increasing the risk of war. 

The alliance system began as an anti-war strategy, but eventually led to large polarized factions that raised the climate for war in 1914.

Before Europe divided itself into two groups, it was possible to prevent wars from spreading. In 1887 there were multiple, non-polarized, interlinked alliances that were designed to contain and de-escalate any conflict. Each alliance included a neutral power whose interests lied in brokering peace.

For example, while the Triple Alliance between Germany, Austria-Hungary and Italy was already established in the 1880s, Great Britain was also tied to Austria and Italy through the Mediterranean Agreements, and Russia and Germany shared a Reinsurance treaty.

But the alliances began to shift from a web of agreements to two blocs, lacking neutral nations, that set the stage for the First World War. On the one side was the Triple Alliance, a pact among Germany, Austria-Hungary and Italy; on the other was the Triple Entente, among Great Britain, France and Russia. Many smaller countries were also allied to each of these camps in some way. Belgium, for example, had an alliance treaty with Great Britain.

The first spark of conflict appeared on June 28, 1914, when the Austrian Crown Prince Franz Ferdinand and his wife were assassinated by Serb nationalists in Sarajevo.

The chain of polarizing alliances meant that, in a matter of months, the conflict between Serbia and Austria spread across the whole of the continent — and the world.

But could the individual countries have done more to prevent war? In the following blinks, we'll look at the roles that each country played in the outbreak.

### 4. Germany and Austria-Hungary are partly responsible for turning a crisis into a world war. 

The traditional historical view is that Austria, in response to the assassination of their Crown Prince and aggressively encouraged by Germany, first bullied and then declared war on Serbia — and in doing so, triggered a fatal chain reaction.

But is this accusation fair? Well, partly.

The Austrian government certainly did provoke war with Serbia, but they failed to consider the broader consequences. 

Following the assassination, the Austrian government gave the Serbians an ultimatum with a list of demands so severe that it was clear Austria was intending to provoke Serbia, rather than propose a fair resolution.

Among this list were unacceptable demands like the forced removal of any military and civil employees of whom Austria didn't approve, and that Austrian security forces would be allowed to work in Serbia. These demands infringed on Serbia's sovereignty and could never be accepted by a nation state. In fact, the diary entries of Austrian diplomats show that this was deliberate: Austria had already set its sights on Serbia. It wanted war.

Yet even though war is exactly what it got — Austrian forces invaded Serbia soon after — their leaders had overlooked that, having an alliance with Serbia, Russia would rush to Serbia's defense.

However, Austria wasn't acting alone; Germany, too, did a great deal to escalate the crisis that led to the war, encouraging Austria to issue the ultimatum and promising its unconditional support. In Germany's defense, it believed that Austria had the right to demand an investigation into the murders at Sarajevo, and had no idea that Austria would issue such a harsh set of demands. Still, Germany didn't impose any restrictions on the composition of the ultimatum, nor did they demand to see it before it was issued. 

As this shows, both Germany and Austria carry their fair share of guilt in the breakout of the war. However, as we'll see in the next blink, they weren't the only countries responsible.

### 5. Russia and France also share responsibility for the war’s outbreak. 

The role that Germany and Austria played in provoking the outbreak of war is a well-known fact.

But two other nations also share the blame — Russia and France.

Both Russia and France had never treated Austria-Hungary as an equal power, and so never genuinely considered Austria's demands. They simply wouldn't acknowledge Austria's right to ask questions about Serbia's role in the assassination of Franz Ferdinand.

So although there were many clues that strongly implicated the Serbian government in the murder, such as the involvement of top Serbian officials, France and Russia condemned Austria's attempts to discuss these issues with Serbian authorities or other governments. 

The lack of respect that both countries showed Austria was largely due to their perception of Austria as a lesser European power, an empire on the brink of inevitable collapse.

But Russia's involvement didn't end there. As the outbreak of war between Austria and Serbia loomed, the Russian government made sure the Serbians would reject the Austrian ultimatum and refuse to enter into further negotiations.

Then, when war finally broke out, Russia did a great deal to escalate it. After Austria declared war on Serbia, Russia promptly reacted by mobilizing its own troops against Austria. And they escalated the larger European conflict by mobilizing on the German border as well.

For its part in the escalation of the Austrian-Serbian conflict, France urged Russia to respond aggressively to the Austrian ultimatum. While Serbia and Austria were locked in their dispute, French President Poincaré visited St. Petersburg, where he promised France's full support in the event of war between Russia and Germany. 

Although France and Russia are not typically considered culpable for the war's outbreak, they clearly played a major role in escalating the conflict and provoking the outbreak.

### 6. Most people believed that, in the long run, a war was inevitable. 

So far, we've looked at the war's development in terms of _nations'_ actions. But, of course, nations are abstractions. It is the _individuals_ and the governments the individuals comprise, who ultimately made the decisions to send their countries to war.

To understand how the Great War came about, we need to examine the popular opinions of this time.

Many people strongly believed that a European war was imminent. Indeed, this view can be found in many diary entries from the time, and in the speeches of diplomats and policymakers.

In 1910, Viscount Esher, an English expert on politics and an advisor to King Edward VII, wrote: "The idea of a prolonged peace is an idle dream."

This pessimistic view was shared by many, and led to the emergence of a "defensive patriotism." In other words, people responded to the threat of impending war not by actively _welcoming_ it; rather, they resigned themselves to its inevitability and strived to be on the winning side.

This popular view of the war as an inevitable fact dictated many political decisions.

In every strategic paper written, the possible outbreak of war was strictly planned and budgeted for. Military leaders often used the fear of the pending war as an argument for increasing the military budgets of their respective nations, thereby reinforcing the popular belief that a war was inevitable.

In Germany, for example, military spending increased suddenly and rapidly so that by 1912 it accounted for 3.8 percent of the country's gross domestic product (GDP).

Also, the France-Russia alliance stipulated many agreements and emergency plans for the onset of war with Germany.

European political leaders were so certain that war would occur that they didn't discuss how they could avoid it altogether.

### 7. Many policymakers in different countries saw an advantage in an early war. 

As the inevitability of war became obvious to all involved, a Europe-wide realization took hold: better that this war occur sooner rather than later.

Why did the policymakers of each country come to the same conclusion? Let's examine each country's.

Germany saw that, in the years leading to 1914, Russia's military strength was growing significantly. Russia has recruited more men for their army and added newer, better weapons to their arsenal.

German diplomats and spies sent reports of these developments to their government, and some reports greatly exaggerated the growth of Russian military strength.

These reports made the German government very nervous, and led them to the verdict that Germany could win a war against Russia only if it would occur within the next few years. After that point, Russia would be too strong.

Russia's growing strength caused some uneasiness with its ally France, too.

The French government feared that Russia, with its rapidly increasing strength, soon wouldn't need an alliance with France anymore, which could lead to the French being isolated in the international community.

So, from France's perspective, if there was to be a war with Germany, it had to happen soon, before Russia could end the partnership.

As for Russia, despite its growing military strength, it was experiencing major problems internationally, mainly because it was fighting on too many fronts.

In addition to Russia's various conflicts in Europe, there also were tensions with China and disputes with the Ottoman Empire over the Russian usage of the waterways, the "Turkish Straits," which were central to Russia's economy.

Many Russian politicians saw war as a swift solution to the problems in Europe, and therefore as positive, because it would give Russia the time and resources it needed to deal with its other problems. 

It's important to note how each country's political and economic climate, compounded with political alliances, weaved together to form what looked like an inevitable war.

Many people did not want a war. But, if inevitable, then they believed it better to happen sooner rather than later. This presupposition played just as much a part in the way the war broke out, and possibly in it breaking out at all, as the actual political climate.

### 8. Anticipating the actions of other countries was difficult, due to disorderly governments and misinformation. 

When we consider the actions of politicians and diplomats leading up to the First World War, one question stands out: Why did certain countries make such great mistakes when anticipating the actions of others? Why, for example, didn't the Austrians anticipate Russia's reaction to its invasion of Serbia? Or why did Russia mobilize against Germany when their dispute was with Austria?

One reason is that, in many countries, the system of government was muddled, to say the least.

For one thing, the role that each nation's monarchy played was never clear, nor was how much power they wielded.

At the time, kings and queens still existed in most European nations, even if they had elected parliaments. The royalty still held a certain power and influence over important persons, but the extent of their authority was often unclear, leading to confusion. 

German Emperor Wilhelm II, for example, continually pursued his own plans, and wrote to the kings and diplomats of other nations expounding his own ideas concerning international politics.

The result of this was that other nations couldn't know for certain whether or not his correspondence and papers reflected the official German line.

The actions of certain countries was also muddled because diplomats and foreign offices often used the press to issue _unofficial_ statements.

Unofficial statements were common. The problem, however, was that the articles did not disclose that they had been written by an official authority, resulting in great confusion over which statements were official, and which not.

Adding to the confusion, newspapers were sometimes used to "test" new opinions, to evaluate the public reaction. So, even if one _could_ be certain that an article was penned by government officials, one could not be sure that it represented the official line.

Again, this misrepresentation in the media meant that foreign politicians found it difficult to assess what other countries were thinking and planning.

The situation in 1914 was an explosive one. Because of the system of alliances, the general feeling that a war was coming, and the many factors that complicated countries' relations, that the war was inevitable seems a valid conclusion.

But was it really?

### 9. The alliances often proved to be fickle and unreliable. 

To answer the question of whether the First World War was truly inevitable, it will help to look at one of the main factors in the outbreak: the alliance system and the resulting polarization of Europe.

While the various alliances appear strong in hindsight, the truth is they were malleable.

That's because the alliances weren't established on a basis of an existing deep bond between countries, but were rather the product of political necessity. Thus, as the political landscape changed, so did the system of alliances.

Russia, for example, initially supported Bulgaria, but later allied themselves with Bulgaria's rival, Serbia, simply because it suited their overarching plan.

Here are some other examples:

First, Serbia was for a long time allied with Austria-Hungary, not with Russia, mainly because Russia had an alliance with Bulgaria, Serbia's rival.

To compensate for breaking the alliance, Serbia made trade agreements and other contracts with Austria. These contracts were terminated only when Austria wanted to establish new trade agreements with Bulgaria in 1906. 

Second, England was unsure about its alliance with Russia in the long term.

Despite some of their common interests in Europe, England and Russia were rivals on an international scale, and in many colonies. For example, Russia was trying to spread its influence to India, which was the British Empire's major colony.

As a result, British politicians were intent on weakening Russia, their supposed ally.

Because of this undeclared rivalry and that many Russian policymakers considered the conflicts at the east border to be more important, it was unclear how long the Triple Entente would last.

So, had the crisis happened earlier or later, the war might not have happened.

But at the time the crisis occurred, is it true that war was still inevitable?

### 10. There were various promising attempts to end the crisis peacefully, even in the last days before it started. 

The assassination of Austria's Franz Ferdinand was a shock to many, and triggered a major crisis.

But while it may appear that all hope for peace was lost, the fact is that several political leaders had tried to end the crisis peacefully.

In fact, certain nations hesitated to join the war. Great Britain, for example, waited a long time before joining, pressing both sides to stop the escalation. British Foreign Minister Edward Grey sent telegrams to Germany and France warning them of the consequences of their rushed actions.

And Germany, despite Russia's full mobilization along their border, hesitated before beginning to mobilize its own troops. Why? Certain German politicians were afraid of the consequences of a Europe-wide war, and wanted to de-escalate it.

Some countries' political leaders tried to use their influence to persuade the leaders of other countries to stop their armies from advancing, and instead to find a more peaceful solution.

For example, shortly after the Russian call to arms, German Emperor Wilhelm sent a telegram to the Russian Tsar, who was also his cousin, urging him to stop the mobilization against Austria and Germany.

Wilhelm proposed that he negotiate between the two countries, without the need for war. Upon hearing this, the Tsar put a stop to Russia's mobilization.

Unfortunately, the Tsar's military leaders persuaded him to change his mind. They argued that any efforts to stop the war at this point were futile since the war was inevitable, and any delay or interruption in the mobilization would leave Russia vulnerable to attack.

It could be argued that the fate of Europe wasn't decided until very late because, even in this state of mobilization and armament, some tried to find a peaceful way forward.

### 11. It’s difficult to make definitive statements from old historical sources. 

In the previous blinks, we looked at many details and arguments on how the First World War broke out.

But many other facts remain unclear. For various reasons, it's difficult to make definite statements about the war, based solely on old historical sources.

First, there are massive amounts of material and literature on the subject, an inordinate amount to attempt to distill into one definitive account.

For example, German researchers published a giant fifty-seven-volume work on the war, containing 15,889 documents. And this is just one of many publications on the subject.

Second, many important documents have been lost or destroyed. In many diaries of political leaders of the time, entries for crucial days such as the early days of the war are missing. Also, some documents were deliberately destroyed to obscure the role that certain politicians and nations played in the outbreak.

Third, there were many different actors, with their own histories and motivations, adding to the complexity of the situation. For instance, the Russian military's growing strength prompted France, Russia's ally, to commit itself to war, because it feared that Russia would abandon it once the alliance was no longer needed. By contrast, the Cuban missile crisis — though very complicated and much debated — had only two major protagonists: the USA and the Soviet Union.

France's motivation is just one small example of the complex ways in which two nations can influence each other. In the crisis that led to the Great War, there were many nations interacting with each other.

Due to various challenges and obstacles to research, we may never fully know all the puzzle pieces that fit together to cause the First World War. Nevertheless, we can do our best to learn from the reasons and facts that we _do_ know for certain.

### 12. Final summary 

The key message in this book:

**The alliance system and subsequent polarization of the European nations played a key role in the outbreak of the First World War. Most people believed that war was inevitable, and on that basis decided to go to war early to avoid various complications down the line. However, as the actions of several key players suggest, many strived until the eleventh hour to avoid going to war.**

**Suggested** **further** **reading:** ** _Man, the State and War_** **by Kenneth N. Waltz**

In _Man, the State and War,_ Kenneth Waltz develops a groundbreaking analysis of the nature and causes of war, offering readers a wide overview of the major political theories of war from the perspective of political philosophers, psychologists and anthropologists.
---

### Christopher Clark

Christopher Clark is a historian from Australia, who is currently Regius Professor of History at the University of Cambridge. Among his other books, he is also the author of _Iron Kingdom: The Rise and Downfall of Prussia, 1600-1947_.

