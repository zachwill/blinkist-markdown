---
id: 543d4e0463633200081b0000
slug: the-small-big-en
published_date: 2014-10-15T00:00:00.000+00:00
author: Steve J. Martin, Noah J. Goldstein and Robert B. Cialdini
title: The Small BIG
subtitle: Small Changes that Spark Big Influence
main_color: EF4576
text_color: BA2550
---

# The Small BIG

_Small Changes that Spark Big Influence_

**Steve J. Martin, Noah J. Goldstein and Robert B. Cialdini**

_The Small BIG_ offers 52 examples on how minor changes in your behavior can make you more confident and successful when it comes to negotiating with and persuading others. Those who read this book will be able to draw value from the examples and make significant improvements to their persuasion style.

---
### 1. What’s in it for me? Learn how persuasion really works! 

We often think that a good message is the only factor that determines whether we win someone over or not. But since every one of us has different values and opinions, the same message can have totally different effects, depending on who the receiver is.

In these blinks, you'll learn some small tips that will make a big difference in your powers of persuasion. Whether it's at home, at the office, or in your own decision-making, you'll find that the _small bigs_ can revolutionize the way you approach persuasion.

After reading these blinks, you will:

  * know how to get people to show up to their doctor's appointment.

  * see why focusing on business failures is better than studying business successes.

  * see how graffiti leads to litter.

### 2. A few small changes in persuasion can have a big impact in getting people to commit. 

Did you know that some simple, small changes can significantly alter how persuasive a message is?

More than a crafty tactic, persuasion is important, and lack of it can have costly consequences. For example, as a society, we lose a lot of money when we don't fulfill our commitments. So persuading others to fulfill their commitments can be very beneficial.

One industry which suffers as a result of lack of commitment is health-care. A popular phrase in healthcare is _DNA_, standing for "Did Not Attend." This is when we schedule an appointment but don't show up. In the UK alone, the problem of _DNA_ has accounted for a £800 million loss.

Governments and organizations are responsible for collecting funds in a prompt manner. Yet, due to late or absent payments, governments, organizations _and_ customers alike are all charged fees.

Fortunately, even slight changes in the approach can have a surprisingly large effect.

You might think that if we were to offer people detailed information and a rational explanation on the subject, it would change their behavior. Take turning off the lights in an office, for example: a common approach is to give your co-workers reasons why it's important to switch them off, such as cost efficiency and environmental friendliness.

But persuasion science studies tell us that these rational methods are likely to fail.

There is a better way to get workers to switch off their lights. It's a _small_ change that has a _big_ effect: put a garbage bin under the light switch. This way, workers will be predisposed to flip the light switch as they throw out their waste when they leave the office.

This is just one of the _small bigs_ that can effect people's behavior.

As the rest of the blinks will show, these _small bigs_ can also solve the aforementioned DNA problem and much more.

> _"… Strategies that simply attempt to inform people into making a change carry a high likelihood of failure."_

### 3. Changing people’s environment can change behavior. 

Here's a negotiating tip: before you hone your strategy, select the right place to hold the negotiation. Your environment is far more influential than you think.

Depending on the situation, certain environments can even influence how responsible we are.

One experiment by Keizer examined this oddity by placing advertisements on customers' bikes in front of a shopping center.

The number of people who threw the advertisements on the ground, instead of putting them in the trash bin changed depending on whether or not the researchers added graffiti to the alleyway adjacent to the shopping centre. When there was no graffiti, 33 percent threw the advert on the ground, whereas when there _was_ graffiti, 69 percent did so.

Another study showing how surroundings influence how we act focussed on creativity in different environments. Participants were better at solving creative problems in a regular room, as opposed to a room with a low ceiling, in which participants were found to have completed fewer creative tasks.

Slight changes to seating arrangements can also change the results of discussions. Scientists found that different seating layouts influenced people's attention in a discussion. Circular arrangements, for instance, made people consider more of what would be ideal for their group. In contrast, people seated in angular (such as L-shaped) or square arrangements, responded more positively to information or proposals that were self-oriented.

Changing the venue itself can also have a significant impact on negotiations.

Much like football games, there is the possibility of a home advantage in places where you negotiate. In one study, researchers assigned groups that were to negotiate with each other to either a home or visitor status. Before the negotiation, the home group was allowed to tailor a room (previously a neutral place) to suit them, such as displaying their names. The researchers observed that groups with the home status outperformed visitors in the negotiation.

### 4. People tend to follow what the majority are doing. 

The decisions we make are based on our own thoughts, right? At least that's what we like to think. But our behavior is strongly influenced by those around us, particularly by those with whom we identify. This tendency is called _social proof._

To take a closer look at social proof, the company _Influence at Work_ decided to address the issue in the UK that many people were not paying their taxes.

Their _small big_ idea was to include a sentence in the reminder letters distributed by the government, stating that citizens from the same area were paying their taxes on time. As a result, the response rate to the letters rose from 67 percent to 79 percent. Even more impressively, when the name of the town was also included, the response rate rose to 83 percent. So, the more similar to ourselves we perceive others to be, the stronger the effect of social proof.

Research in neuroscience has also shown that behaving contrary to a crowd consensus incurs emotional costs. In particular, _fMRI_ studies have shown that areas in the brain associated with emotion are activated when we make decisions that conflict with others' consensus.

Contrastingly, we often _disassociate_ ourselves from groups we don't like. Studies have found that students are more likely to stop a certain behavior (such as wearing a special bracelet) when an "out-group" (in this case a "nerdy" student group) also sported the bracelets. There was a 32 percent drop in wearing the bracelets, in contrast to a mere 6 percent drop when there was no "out-group."

So it is important to couple your desired behavior with qualities which most people want to assume for themselves. Apple uses this small big technique well by accompanying its products with independent, confident and creative people. Because so many of us want to be perceived this way, we are far more likely to buy their products!

### 5. Focus as much on other people’s mistakes as your own. 

We are all scared of making mistakes. But we can learn to play mistakes to our advantage.

One way to do so is by noting other people's mistakes. Rather than emulating what successful entrepreneurs did to make them successful, focus on what led to their failures.

Charlie Munger, investment advisor to _Warren Buffett_, analyzes and avoids the unwise decisions of other companies that led them into some sticky situations. He created a collection of other peoples mistakes which he called his _inanities-list_.

But isn't this contrary to what we've been told? Don't we learn from positive outcomes? Yet researchers assert that this is not the case. Professor Roy Baumeister and his colleagues discovered that we actually focus more on negative information and learn more from it than from positive information.

It's important to manage our own mistakes and we shouldn't necessarily avoid them.

Rather than averting mistakes, organizational scientists found that the _Error Management model_ _(EMT_ ) is more accurate than the traditional error-avoidance slant, which posits that we should try to avoid making mistakes whenever we can.

The EMT-model is similar to the _inanities-list_ in that it involves acknowledging and understanding how other people's failures came about. Secondly, it involves analyzing your own mistakes after the fact and responding appropriately to them.

In the EMT-model, people learn that mistakes are a normal part of learning and so aren't categorized as personal failure.

Interestingly, this model can also improve your customer service. In a study on customer satisfaction, one hotel chain found that customers who had the chance to experience hotel staff responding to a mistake were more satisfied and loyal than customers who had an uneventful stay.

### 6. Being self-confident, or at least appearing self-confident, boosts your persuasion powers. 

Just as our physical environment is key to the art of persuasion, so is the persuader. Some minor changes in how we portray ourselves can do wonders for communicating our message effectively.

Moreover, if you can convince people that you're an expert, you're already halfway to becoming a master of persuasion. For example, recent brain-imaging studies illustrate that we're more likely to make financial choices that are otherwise unfamiliar to us when we are advised by an expert, such as a well-known economist.

In these studies, brain areas connected to critical thinking and counter-arguing were almost inactive. The scientists postulated that the _expert-factor_ ruled over everything, including quality of arguments, when the audience didn't feel strongly either way.

We may not all be experts, but if your self-confidence could use a boost, it may serve you to look back on the times when you were successful.

This is evident in one study where two groups of participants had to attend a job interview. Before the interview, the first group noted experiences in which they felt powerful and the other group noted experiences in which they felt powerless.

The result? The first group out-performed the second. Even compared to a control group, who didn't do any writing, the powerful group was more persuasive.

So, having confidence is ideal, but you shouldn't take it too far.

Researchers found that a review by a well-known food critic was more persuasive when he articulated insecurity, than when he said he was 100 percent positive that the restaurant was the best.

The researchers explained that we often expect experts to be convinced of their opinion, so when they show insecurity, it sparks our attention, which results in the message being more persuasive.

Therefore, to be persuasive, we need to be confident and at the same time aware of flaws in ourselves, our theories or our products.

> _"...if we overstate our know-how and are later discovered to have been deceptive in this regard, we will likely lose the ability to promote our expertise convincingly in the future…"_

### 7. Keep your people motivated and committed. 

If you manage anyone at work, you will want to know how to motivate them. Here's some useful advice.

Often workers don't achieve their full potential because they feel that what they are doing is not significant enough. When this happens, they start feeling detached from their jobs.

Professor Adam Grant conducted a study in a call center where staff called up college alumni and had to persuade them to donate money to the college. Prior to calling anyone, one group was asked to read a text by another employee, highlighting the personal gain (e.g. their wages) the employee gets from doing their job, and the second group read texts written by students about how their situation had improved from receiving their scholarship.

The second group received more than double the pledges than the first group. Why? Because the employees, having seen the meaningfulness of their work, became more motivated to ask for donations and actively persuade those they were calling.

So what else can increase the chances of getting others to behave how we would like them to? Personal commitment and _implementation intention plans_.

Take the aforementioned DNAs in the healthcare industry: when people had to write down their appointment date themselves, the DNA rates fell by 18 percent. This improvement is an example of an implementation intention plan.

Implementation intention plans help us when we want to carry out a certain behavior such as going to the gym regularly. These plans are an agreement, but don't necessarily have to be written down, and they outline where, when and how we intend to reach a particular goal.

We can see the effect of this in one study in which households either filled out a voting plan, detailing the time and location of voting, or didn't fill out any voting plan. Those households who had a plan were more likely than those who had no plan to actually vote on election day.

### 8. The offer itself isn’t as important as the moment you make it. 

As we've seen, good negotiating is about how and where you present your message. Another way to get a solid start on negotiating is to make the first offer and make it precise.

Customers take the first offer from their counterpart and use it as a base or _anchor_ for the negotiation. Even if an initial offer is much higher than they were expecting, they will adjust their expected figure accordingly.

Say you work as a car salesperson and your customer is prepared to pay $2000 for one of the cars. But you pre-empt them and offer $5000 as a benchmark. According to research, your customer will agree to a price tag closer to $5000 than their expected $2000, due to this _anchor effect_.

This phenomenon tells us that customers believe salespeople, who they regard as experts, offer such high figures because there must be something about the product's value that they don't know about.

You could increase your persuasiveness even more if you were to present a precise amount such as $5132 to your customer. This is because it implies to the customer that a lot of time and effort went into researching the car's value. So this price seems more accurate and realistic.

If you want to make an offer even _more_ enticing, you should partner it with unappealing offers.

Take renowned chef Antonio Carluccio, for example. He decided to introduce an unfamiliar item to his italian restaurant menu in which, besides the pastas and salads, you could also order a _Vespa_ scooter.

Although his scooter sales didn't take off, the rest of the menu seemed far more appealing because the other items were much cheaper than the scooter. This is a phenomenon in psychology known as _perceptual contrast_.

A more down-to-earth way to use this idea would be to add a $60 wine to your menu, along with the $15 wines. Suddenly the $15 wine will seem far more attractive.

> _"Parents take note. Be sure you get in the first bid with those bedtimes!"_

### 9. Sometimes you have to give people some time. 

Even the best persuasion tactics sometimes fail simply because people don't want to change their behavior right this minute. In order to be persuasive, it's often a good idea to remember that instant change isn't always the only way.

Sometimes we need to give people some time to act the way we want them to.

Let's take a look at short term decisions. When you think about going out for dinner this weekend, for instance, you think of the _concrete costs_ of the decision, such as weighing up if you can afford it or not.

Events in the future, however, evoke more abstract thinking. When considering the future, we think how the event is connected to our own morals and values. Therefore, if everyone we know is attending a particular Christmas dinner, then we are likely to agree to go, too. If an event fits into our moral values, we are more likely to say yes to it when it's further away in the future.

So how can we apply this technique? Well, for example, if you know that you are going to need help painting your apartment in a few weeks, ask your friends as early as you can. If they don't yet have definite ideas of what they are doing on the date you need to paint the apartment, they are more likely to agree to help you.

Note, however, that if something has to be done in a timely manner, don't give people too much time. It's a mistaken belief that giving people extra time to complete a request or task increases the likelihood that they will do so.

To test this, researchers in one study altered the expiration date of bakery vouchers and observed how often customers redeemed them. They found that those who received vouchers with long expiration dates reported they will likely use them, but they actually redeemed them _five times less_ than customers with vouchers displaying shorter expiration dates.

### 10. Final summary 

The key message in this book:

****Successful persuasion does not merely depend on the message, but on context and environment. By making some minor changes in how you approach persuasion, you can significantly improve your ability to convince others.****

Actionable advice:

**Keep your prices precise!**

Remember, the more exact a price is, the more people believe there are good reasons behind its pricing and will be more tempted to buy the product. So, instead of placing a $400 price tag on your product, try making it $457.

**Change your seating arrangements.**

The next time you schedule a meeting, think about your desired outcome. If a group decision is important, arrange seating in a circular fashion. If individual benefits are more important to you, use an angular or L-shaped seating arrangement.

**Suggested** **further** **reading:** ** _Influence_** **by Robert Cialdini**

_Influence_ explains in detail the fundamental principles of persuasion that get us to say yes, including how they are used against us by compliance professionals like salesmen, advertisers and con artists. Knowing these principles will allow you both to become a skilled persuader yourself and to defend yourself against manipulation attempts.
---

### Steve J. Martin, Noah J. Goldstein and Robert B. Cialdini

Dr. Noah Goldstein is Associate Professor of Management and Organization at UCLA Anderson School of Management and also holds joint appointments in the Psychology Department and the David Geffen School of Medicine. He has won awards in both teaching and research and was on the _Scientific Advisory Board_ of two _Fortune Global 500_ companies.

Dr. Robert Cialdini is the most cited living psychologist in the field of influence and persuasion. His career has centred on researching the science of influence. He is the best-selling author of _Yes: 50 Scientifically Proven Ways to be Persuasive_.

