---
id: 56cdaa3074766a000700004e
slug: the-smarter-screen-en
published_date: 2016-02-25T00:00:00.000+00:00
author: Shlomo Benartzi
title: The Smarter Screen
subtitle: What Your Business Can Learn from the Way Consumers Think Online
main_color: 58B2D6
text_color: 1E6785
---

# The Smarter Screen

_What Your Business Can Learn from the Way Consumers Think Online_

**Shlomo Benartzi**

_The Smarter Screen_ (2015) is a guide both for the start-up and established business to boosting your company's digital presence in a media-saturated world. These blinks apply insights from behavioral economics to explain exactly how people think and respond to digital information on a screen.

---
### 1. What’s in it for me? Declutter your website and turn it into a online paradise for your audience. 

The number of websites is well over one billion, a number that continues to grow by the second. These sites represent the sheer volume of information accessible to people surfing the internet. 

Yet far too much of this information is hidden behind poorly designed websites, pages so cluttered or unclear that no savvy browser not to mention newbie could begin to access the treasures within.

It doesn't have to be this way. If every website and mobile app today would simply follow a few important rules, the internet could be a much friendlier, organized place. If you and your business follow these rules, you'll discover that not only will your users find useful information but also you'll be able to direct users to exactly what you want them to see. 

So what are the magical rules? Read these blinks to find out. 

In these blinks, you'll discover

  * how everyone moves their eyes in the same fashion, regardless of background; 

  * why people prefer less choice than more choice in general; and 

  * how picturing yourself as an elderly person can change how you save cash.

### 2. As the internet offers far more information than people can take in, companies need to fight for attention. 

Say you're looking to book a hotel room in London. You search Google for accommodations, and are immediately given 500 million options. Obviously there's no way you could consider each one — but such is the peril of information overload in the twenty-first century. 

The prominence of computers and the internet have prompted an exponential rise in the quantity of available information. In fact, scientists at IBM have estimated that the data created over just the last two years makes up 90 percent of all the information currently in existence!

In the 1950s, for instance, people could choose among two or three TV channels. But today you can access just about every program ever made, and whenever you want, through cloud services. 

Yet the amount of information that people actually pay attention to is limited by the cognitive capacity of the brain. For instance, research has suggested that a person's short-term memory is only capable of storing about four pieces of information at a time.

So whenever the information presented exceeds your brain's capacity, you start missing out. For example, studies have found that having a phone conversation while driving causes you to stop noticing some 50 percent of your surroundings — red lights and pedestrians included. 

Considering this, it's easy to see how the abundance of online information inevitably leads to the waning attention of users. In short, people surfing the internet face difficult choices every second about what to notice and think about. 

But there's actually a great opportunity for companies to take advantage of the gap between the information that's presented and the information that people notice. For instance, sites such as Booking.com or Hotels.com that offer a simple information platform online are able to charge hotel owners a commission as high as 30 percent. We'll learn why they can do this in the next blink.

### 3. The visual appeal of a website is crucial in both grabbing attention and connecting with a user. 

You probably have heard of the dating app called Tinder. Users glance at profile pictures and in that split second, decide whether they're interested in dating that person. Some 1.2 billion Tinder photos are swiped every single day!

Tinder offers us a great example of how rapidly a person can form an opinion about a certain subject. How exactly do people make such quick decisions?

Most of our choices and judgments are influenced by processes that occur before we're conscious of what we're actually looking at. So instead of weighing pros and cons rationally, our subconscious mind makes a near instantaneous judgment based on a first impression.

So if you run a website, in order to get people to engage, you need to find a way to make your site appeal to a user's subconscious. But most websites are drab, colorless things that rely on long streams of text instead of visual appeal to engage viewers. This sort of design can lead to a site being overly complicated and hard for a person to follow. 

For instance, Bank of America's landing page is a perfect example of a website that is overly complicated. It offers 68 links, 15 asymmetric text blocks using multiple fonts, sizes and colors, as well as three different menu bars.

That's a lesson in what _not_ to do. Building an engaging website requires some _visual complexity_ — that is, a perfect balance of color and detail. For instance, bright, engaging colors are key to grabbing a person's attention. Once that's done, the rest of your information needs to be presented in a clear, easy-to-understand format that doesn't overburden the quick-thinking subconscious.

In certain studies, participants were shown website screenshots so rapidly that they couldn't possibly process them all consciously. Nonetheless, the sites that rated the highest for usability and trustworthiness were those that were visually appealing. 

This is known as the _halo effect_, when one prominent feature of a product is so appealing that it influences a person's judgment about the project as a whole.

### 4. Your website design can benefit from examining human patterns of perception and choice. 

We've all been there: standing in a supermarket aisle, facing ten different brands of a certain product. Some people know exactly what they want, but if you don't, you're likely to pick an option located in the center of the shelf. 

That's because we perceive the world and make decisions based on what's known as the _middle bias_. 

Our eyes don't move systematically from side to side when we see something, but instead jump around in a predictable pattern. As you might have guessed, this pattern tends to reach its end in the "center."

Studies conducted with candy have found that human's middle bias is so powerful that test participants would choose a sweet placed in the center, even if their favorite candy was included in the group but far off to the side. So if you want someone to click on a link, the best place for the link to exist is smack dab in the center of the screen. 

But placing items centrally isn't the only way to get people to notice them. You can also highlight specific options to boost the chances of selection. This tends to work especially well when people aren't on the hunt for something specific, or when they're experiencing a cognitive load such as while multitasking. It's simple: the brightest-colored option will often be the one that's chosen. 

After scanning around jerkily, however, a person's eyes will then move horizontally. To turn this fact to your advantage, it's essential to carefully consider which information you'll organize in rows and which in columns. 

For example, Dell Computer built on this fact to list its computer models in columns on its website, and other information, such as prices, in horizontal rows. 

Yet researchers found that customers were more focused on the horizontal rows and therefore focused their search on the cheapest computer. But when Dell switched the information around, customers spent more time looking at the products, now on the horizontal plane, and less time focusing on price.

### 5. Making your site easy to use is obvious, yet if you want people to read carefully, consider disfluency. 

It's always better for things to be easy, right? Well, maybe not. People are happy when they don't have to struggle to perform a task, yet this might not always be to your benefit. Here's why. 

Easy-to-use websites work well because a user can access content and services simply. For the most part this is a desired outcome, but making your website too easy can create its own issues.

For instance, Uber's simple interface often comes into conflict with the company's policy of surge pricing, during which prices rise with demand. 

In general, with just a few clicks a user can get out of a storm and into a nice, dry car taking them home. Yet at peak times, prices go up as more users log in to find rides. While it's still just a few clicks to reserve a car, that trip during a surge pricing period might cost a customer $150 for even a short ride. And this means unhappy customers and loads of complaints. 

So sometimes making an online service more efficient means making it a bit harder to use. In fact, studies have found that by producing _disfluency_, that is making something more challenging to read, you can actually improve people's attention and their long-term memory. Slowing down makes people process information more deeply, thereby activating more deliberate thinking. 

There are plenty of ways to encourage this, from using an unfamiliar font or unusual layout to even having users manually enter information. For instance, Uber now has a user type in the exact number by which a standard rate is multiplied during peak hours. This simple process forces the user to slow down and ensures he or she knows how much the ride will actually cost. 

The point is, if you want your customers to get something done quickly, make it easy. But if you want them to carefully consider certain information, a bit of disfluency can go a long way.

### 6. To be effective, feedback should be provided in a timely, instructional and emotional way. 

Have you ever thought about how to best provide feedback? It's an important question, because in order to have a lasting impact, comments and criticism need to be presented in the correct way. 

In fact, the proper balance of feedback follows an _inverted u-curve_. Too little feedback will prevent learning, as the recipient won't see how she can improve. It's for this reason that bathroom scales alone aren't an effective way to shed pounds, as you're just given a number, but don't know how to change it.

On the other hand, too much feedback can overwhelm and cause a person to ignore much of the information given. This is why certain smartphone apps that allow you to track everything, from body fat to the ingredients in every meal, haven't really contributed to reducing obesity rates. That much information is simply too much for most people.

But it's not just important to _give_ feedback. It's also key to present the feedback well, and this means giving it to people exactly when they need it. For instance, the Personal Capital mobile app lets users track finances in real time, allowing them to review their balance while in line to buy something.

Feedback should also make clear how a person is to achieve a desired outcome. For example, the Red Alert app was built to warn Israeli residents about potential missile attacks. When an alarm sounds, the app displays directions to the closest bomb shelter. So the app doesn't just warn a user but also tells him exactly how to avoid danger. 

It's also important that feedback produces an emotional response in the recipient. For instance, an internet-connected pill bottle called the GlowCap shines with different colored lights then makes sounds, all with the goal of producing a feeling of urgency in the patient to take his medicine.

### 7. Personalizing information makes people much more likely to act on it and change behavior. 

In a world chock-full of generic information, personalization is key to getting a message across. 

Consider hotel managers concerned about the environment and their laundry bills. Placing a sign in a hotel room's shower that reads, "Please reuse your towels," doesn't do much to motivate the average guest. Yet "75 percent of guests in this room use their towels more than once" is far more effective, as it speaks directly to the person as they dry off. 

The best part is, it's relatively easy and inexpensive to personalize digital content. For instance, international beverage company Coca-Cola ran an experimental campaign on billboards located on certain highways in Tel Aviv. Once a special smartphone app was downloaded, when a person drove past the billboard, it would display that person's name in huge letters. 

In just a few weeks, the special Coca-Cola app was downloaded over 100,000 times, and the campaign overall increased customer engagement with the company brand significantly. 

Or consider the free app called AgingBooth, which adds effects to photos that make a person look much older. Studies have found that when a person is asked to put away money for a hypothetical retirement fund, that person will save twice as much after seeing an image of himself as elderly. It's easy to see how banks and financial service providers could benefit from such software!

So personalizing information is important, but timing your outreach strategy is also essential to achieving a desired effect. One way companies can do this is through the _fresh start effect_, a technique that can help people lose weight, get in shape or save money.

The technique works by urging people to make changes on certain days. For instance, researchers at The Wharton School of the University of Pennsylvania have found that people are much more likely to work on goals toward the beginning of the week, month or year. But important dates like birthdays or retirement are also effective.

### 8. A guided decision-making process can help to increase customer satisfaction overall. 

Are you familiar with the website, Healthcare.gov? This site was built to help American citizens select an insurance plan personalized to their specific needs. 

The website showcases 78,000 different insurance plans and gives people the freedom to choose their favorite. However, studies have shown that on average, users choose a plan that's too expensive for their budget by about $900. How could this be the case?

Well, when given a choice that's complicated by an overwhelming amount of data, people either make incorrect decisions or make no decision at all. 

To better understand this, a study organized by academics from Columbia and Stanford universities compared two marmalade stands that allowed customers to taste and buy jam. One stand offered 24 different choices, while the other offered only six. 

When customers visited the first stand with its many choices, only three percent of people made a purchase. But at the other stand where options were limited, 30 percent of visitors bought jam. 

This stems from a feeling called _buyer's remorse_, or when a person immediately questions a purchase after it is completed, worrying if the product purchased was indeed the "right" one. 

The plethora of choice in the digital world only enforces this dread. While the average supermarket carries about 220 different breakfast cereal brands, online retailer Amazon.com offers nearly 2,000!

So the solution is to establish guided decision making, helping customers to identify the option that's right for them. 

For instance, the author proposed such a strategy for a couple looking to find a house in a popular city. He first had them narrow down all their options to about 16, using different categories such as location, size and access to good schools. 

Then the couple entered a serious decision-making phase. The author set up a system in which four options would appear on a screen; the couple was only allowed to choose one of the four, losing the other options forever. The end result left them with just four houses from which to choose, a much easier amount of choice to manage. 

Remember, strategies like this are effective because customers don't actually want unlimited options, but instead a curated selection.

### 9. Final summary 

**The way humans perceive and respond to digital information is highly predictable. By taking these patterns into account when designing your website, you can ensure that your business will grab the attention of your desired audience as they navigate through a sea of digital options.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Phishing for Phools_** **by** **George Akerlof**

**_Phishing for Phools_** **(2015) reveals the ways in which modern free-market systems, so often praised as the epitome of rational exchange, are fueled instead by willful deceit, with the goal of pushing you to act against your self-interest.**
---

### Shlomo Benartzi

Behavioral economist Shlomo Benartzi is a professor and co-chair of the Behavioral Decision-Making Group at the University of California, Los Angeles's Anderson School of Management. He also holds a doctorate from Cornell University's Johnson Graduate School of Management.

