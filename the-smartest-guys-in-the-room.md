---
id: 55fff3a3615cd10009000040
slug: the-smartest-guys-in-the-room-en
published_date: 2015-09-23T00:00:00.000+00:00
author: Bethany Mclean, Peter Elkind
title: The Smartest Guys in the Room
subtitle: The Amazing Rise and Scandalous Fall of Enron
main_color: BB3625
text_color: BB3625
---

# The Smartest Guys in the Room

_The Amazing Rise and Scandalous Fall of Enron_

**Bethany Mclean, Peter Elkind**

_The Smartest Guys in the Room_ (2003) tells the remarkable tale of energy trader Enron — once a poster child for market innovation on Wall Street — and its dramatic fall from stratospheric heights. These blinks detail a gripping story of financial deceit, while shedding light on the personalities that built Enron's corporate culture and set it up for disaster.

---
### 1. What’s in it for me? Discover what led to the biggest bankruptcy case in US history. 

Enron Corporation, the once-dominant American energy, commodities and services company, has come to be known as a synonym for fraud, corruption and disastrous debts. Enron's fall from grace is probably the most scandalous business story of our times, its skyrocketing profits followed by the largest bankruptcy case in US history. But how does a respected company make it to the top of the business world, only to crumble into economic tragedy?

In these blinks, you'll venture into Enron's past to learn about the fascinating people behind this infamous scandal, and will see how greed and deceit can lead to such a dramatic demise.

You'll also learn

  * why egomaniacs and backstabbers were welcome at Enron;

  * how financial structures sneakily camouflaged existing debts; and 

  * what went on in the company's closed-door meetings.

### 2. Enron, a company whose decades of deceit ended in bankruptcy, nearly met the same fate just two years after incorporating. 

Does it ever feel like history is repeating itself? It sure seemed that way in 2001 when the American energy colossus Enron filed for bankruptcy. The company's demise due to huge debts and fraudulent business practices mirrored problems they had experienced years earlier, when first starting out. 

In fact, in 1987, just two years after it was founded, Enron was already saddled with debt. 

Enron was formed in 1985 through a merger between two pipeline companies, _Houston Natural Gas_ (or _HNG_ ) and _InterNorth._ An intelligent and ambitious man named Ken Lay assumed the role of CEO, and in 1986 the company was renamed Enron. 

Unfortunately for Lay, it wasn't long before Enron was in dire financial straits. By early 1986, Enron reported a first-year loss of $14 million, and by January 1987 its credit rating was at junk status.

What happened?

Enron had been engaging in dishonest business practices that had brought the company to the brink of bankruptcy, and a specific subdivision called Enron Oil was causing the most trouble. Enron Oil wasn't producing or selling oil, but simply speculating on oil _prices_ ; not only that, but the oil traders were manipulating their earnings. 

For example, they would set up deals with fake companies that let them take a huge loss on one contract, only to cancel out these losses with a second contract that generated the same amount in profits. Their fictitious losses enabled them to move earnings from one quarter to the next. 

Enron was trying to prove to Wall Street that it could turn a steadily increasing profit, a trend that the stock market is quick to reward. However, by 1987, Enron's oil trading game had taken such a loss on high-risk bets that the entire company was on the verge of bankruptcy. 

But Ken Lay was prepared to act. He assured analysts on Wall Street that this downturn was just a freak event that could never repeat itself. But as we now know, this kind of deceit and carelessness was at the very core of Enron's company culture.

> _"Although it took place a long time ago, it seems obvious now that the Enron Oil scandal was the canary in the coal mine."_

### 3. Enron was fundamentally transformed when they found their visionary. 

Enron managed to pull itself out of its first crisis, but by the end of 1988 it was in hot water once again. The company's main problem was that it didn't have a business model that could turn real profits, an issue Enron hoped to solve by hiring Jeffrey Skilling. 

Skilling, formerly a consultant for McKinsey and a graduate of Harvard Business School, joined Enron in 1990, taking the position of CEO at a new subdivision called Enron Finance. A very bright man, Skilling was a major force behind Enron's transformation into a successful company — at least temporarily. 

First, Skilling turned Enron into what he called a _Gas Bank._ The idea was that gas producers would sign a contract to sell their product to Enron, who would in turn sign contracts with customers. Enron's profit was the difference between what it paid the producers and what it charged its customers. But Skilling saw another way to make money through these contracts: trading the agreements themselves. 

The second step in Skilling's master plan was to convince Enron to use _mark-to-market accounting_. 

While conventional accounting works by entering revenues and profits from a contract as the money comes in, mark-to-market accounting logs the total estimated value of the entire contract on the day it's signed. So, by immediately posting all potential profits, companies that use mark-to-market accounting _appear_ to be growing quickly. This, in turn, pleases Wall Street speculators and the stock goes up. 

Finally, Skilling forged a corporate environment in which sheer brains were valued more highly than skilled management and hands-on experience. In fact, he used to say that he liked to hire "guys with spikes," meaning that if an executive boasted a single talent, or a spike, Skilling would take him on board regardless of any shortcomings. 

That spawned a company full of egomaniacs, social misfits and backstabbers — which didn't bother Skilling as long as they cunningly did what he needed them to. 

But is it possible for a company to ride a wave of success solely propelled by a brilliant workforce? Not in the case of Enron.

> _"We're a cool company." - Jeffrey Skilling_

### 4. Rebecca Mark was Enron’s poster child – and the person responsible for its toxic deal-making culture. 

It was Jeff Skilling who transformed Enron, but until the mid-1990s, most people outside the company had no clue who he was. The face of Enron to the outside world was in fact Rebecca Mark — the female star in a male-dominated industry. 

Until the 1990s, developing countries were usually overlooked by corporations in the West, even though they had massive populations and matching energy needs. Rebecca Mark was the head of a company subdivision called Enron Development, and her task was to strike energy deals with as many developing nations as possible, thereby flying Enron's flag the world over. 

So, she set out around the globe, and by the mid-90s Enron Development appeared to be wildly successful, with Mark getting credit for making it happen. While she was happy to play up her good looks and dazzling smile, Mark's signature characteristic was her optimism, resolutely believing that everything would work out well. 

But despite her smiles and assurances, Mark gradually tainted the deal-making culture at Enron Development; her subordinates were made to think their task was simply to secure as many deals as possible, a result of Enron's flawed compensation structure.

For instance, developers were paid bonuses on a project-by-project basis, meaning that they got paid when a deal was closed — before a single pipe was laid or a foundation poured. Therefore, developers had no incentive to follow through with deals they'd signed, and there was nobody else at Enron to take responsibility for projects once they were off the ground. 

So, while Mark assumed nothing bad could happen, disasters were taking place every day. For example, in 1995, Enron invested $95 million in a Dominican Republic-based power plant. But as it turned out, the Dominican Republic government wouldn't pay for the power the plant generated. As a result, by mid-2000, Enron's huge investment had returned a mere $3.5 million!

### 5. With Jeff Skilling as president, trading became Enron’s core business – and brought a culture of risk taking and deception. 

In 1996, Enron announced that Jeff Skilling would be the company's new president and chief operating officer, or COO. Newly installed at the top of the company, Skilling quickly began remaking Enron to suit his image. Trading became Enron's central focus, but Skilling also de-emphasized old projects like pipelines and the production of natural gas.

Although Enron had already become the biggest name in the natural gas industry, Skilling wanted to go further. He wanted even more trading, and to deal not just in gas but to expand into the electric power sector as well, even though Enron was still an outside player in electricity. 

While refashioning the company, Skilling took every chance he could to shed its old business operations, even ones that were profitable. As a result, by the end of the '90s, Enron had witnessed a profound shift: trading and cutting deals were now its core business. 

But Skilling's changes had other consequences, a major one being that Enron became a place where risk taking and financial deceit were practically inevitable. Why? Because Skilling's tactic for generating Enron's annual earning targets was absolutely ludicrous: he would set a fabricated number that simply reflected what Wall Street wanted. 

The problem was that a company founded on trading and making deals can't possibly rely on its earnings to steadily rise, since trading desks can earn or lose millions in one fell swoop. 

As a result, the deals Enron brokered came with increasingly greater risks, and Enron's Risk Assessment and Control department, or the RAC, would step aside as long as a deal had the necessary commercial support. At the same time, thanks to the mere existence of the RAC, Enron could paint itself as a company that handled greater risk more carefully than any competitors. 

But to hit the earning targets it promised investors, Enron had to pull a few tricks; the company overstated its future revenue to delay recording the losses it was actually taking.

> _"It was that just about every member of the board seemed to believe that Ken Lay walked on water."_

### 6. Andrew Fastow created the financial structures that helped Enron disguise its debt, making himself rich in the process. 

While Skilling transformed the nature of Enron's business, it was Andrew Fastow who transformed its finances. 

It all began when Fastow, who had been a part of Enron's financial division since 1990, was promoted to chief financial officer, or CFO, in 1998. He and his team went to work masterminding Enron's finances to close the gap between the reality of the firm's business and the picture Skilling and Lay wanted the world to see. 

How? By creating financial structures that hid Enron's debt. 

Take _Whitewing_ as an example. This Enron subsidiary was created in 1997 to purchase and sell off the company's poorly performing assets. Say Enron constructed a power plant at a cost of $8 million and expected the plant to be worth $10 million — the company would mark a $2 million profit in its books. 

When the plant underperformed, drawing a market value of just $7 million, Enron should have erased the $2 million profit it logged and replaced it with a $1 million loss. But instead, Enron would just sell the plant to Whitewing for the full $10 million. Whitewing would in turn sell it for $7 million and be compensated with the remaining $3 million in Enron stock. 

Since the $3 million worth of stock Enron issued wouldn't show as a loss on the books, the company had essentially disguised a $1 million loss as a $2 million profit. 

But Fastow wasn't just making money for Enron — he knew how to pay himself too. For example, in 1999 he started a fund called _LJM_, named for the initials of his wife and two sons, Lea, Jeffrey and Matthew. Not unlike Whitewing, LJM invested in Enron's badly performing stocks, a venture that allowed the company to keep them off its balance sheets. 

However, Fastow heading LJM while also holding the position of CFO at Enron was an obvious conflict of interest, because he could essentially negotiate with himself. In the end, Fastow's dual positions allowed him to quietly make tens of millions of dollars in personal profit.

### 7. Jeff Skilling thought he’d found Enron’s future in electrical energy and broadband, but both failed. 

Enron couldn't ride it's fraudulent accounting practices forever. The company thought of them as a life preserver that would keep their business looking good until Skilling came up with another plan to make _real_ profits. 

Skilling's first idea was to get into the electrical energy business, mainly because, by the mid-1990s, retail electricity was regulated by state governments. Enron was banking on the fact that the market would eventually be deregulated; when that happened, they would be perfectly placed to sell electricity straight to businesses and homes across the country. 

There was just one problem: local energy providers organized a fierce backlash to stop deregulation, leading only a few states to actually curb regulations after all. And in California, a state that actually let Enron sell power directly to customers, Enron spent over $20 million on ads to bring in new business. However, despite the discounts Enron could offer, consumers weren't ready to switch away from the reliable utility they'd always used in the past.

In addition, Enron simply didn't belong in the business of electricity. For instance, to woo customers, Enron promised to make huge improvements to efficiency — the only problem was, they didn't have the first clue how to do it. 

OK, so Skilling's first plan was a bust, but what about his second? Well, it turned out to be a colossal failure, too. 

Skilling pitched broadband as Enron's next big thing, asking why Enron couldn't trade bandwidth capacity just like it did natural gas. So, in 1999, Enron promised that it would soon be capable of delivering real-time bandwidth-on-demand. To do so, it would build a sophisticated new broadband network system. 

At the time, Enron painted its system as "lit, tested, and ready." But in reality, it wasn't anywhere close to operational on a commercial scale, and a great deal of the technology they promised never even made it out of the lab. The fact was, Enron couldn't provide bandwidth-on-demand then, and they never would.

> _"For Skilling, like everyone else at Enron, customer service was little more than an afterthought."_

### 8. Analysts loved Enron, even though many of them knew the company was hiding its debt. 

Given what you now know about Enron, it might be hard to believe that Thomas Kuhn, president of the Edison Electric Institute, said the executives at Enron had "the aura of whiz kids, of golden boys who could do no wrong." In fact, Skilling and Lay were even being mentioned in the same breath as the likes of Microsoft's Bill Gates and Apple's Steve Jobs. 

Analysts had kept blind faith in Enron's performance. Take Enron's annual analysts meeting of January 2000, an annual affair in which Skilling and other executives reviewed each business unit's performance. 

The meeting's highlight?

The company executives unveiled Enron's new broadband strategy by confidently stating the company would become "the world's largest provider of premium broadband delivery service." Skilling even predicted that the new business venture would be worth a whopping $29 billion!

Naturally, the room went mad, with some 200 analysts making rushed calls to their trading desks and telling them to invest in Enron. As a result, the company's stock rose 26 percentage points in just one day. In fact, not one of the analysts in attendance ventured to ask a probing question; the mood was one of absolute praise for Enron. 

So, while it might seem that the analysts themselves weren't in the know, there was a surprisingly large circle of people who knew exactly what lurked beneath Enron's shiny exterior. 

For instance, many of the analysts knew the firm's recorded earnings were well beyond the actual money coming in. In fact, J.P. Morgan analyst Kyle Rudden wrote in mid-1999 that "ENE [Enron's stock symbol] is not a cash story...Enron has significant flexibility in structuring contracts and hence booking earnings."

But that's not all the analysts knew. They also knew that Enron had huge quantities of off-balance-sheet debt, debt that doesn't show on a company's statement of financial position; even so, they only occasionally mentioned this in their reports.

> _"Because the stock was rising, Enron's executives were seen as brilliant. Because they were viewed as brilliant, all their new ideas had to be winners."_

### 9. Concerns regarding Enron’s financial state arose in 2000 and were fueled by strange shifts in the company. 

On December 13th, 2000, Skilling was standing atop Enron's peak: the company had just announced that he would take Ken Lay's position as CEO. In celebration of his promotion, _Business Week_ printed a reverential cover story, naming him America's second-best CEO — just behind Microsoft's Steve Ballmer. Skilling hadn't even held the job for three months. 

But despite the praise for Skilling, skepticism about Enron's success was beginning to surface. On September 20th, 2000, Jonathan Weil — reporting for the _Texas Journal_, a regional supplement of the _Wall Street Journal_ — published a story focusing on energy traders' reliance on mark-to-market accounting, Enron's essential tool for logging profits before the cash actually came in. 

Upon reading the story, an influential hedge fund manager named Jim Chanos began investigating further. He discovered that although Enron was reporting steadily increasing earnings, the business itself didn't appear to be earning much money at all. He took his skeptical views about Enron to _Fortune_ magazine, and in March of 2001 they published a story with the headline "Is Enron Overpriced?"

The piece made note of Enron's lack of cash flow and rising debt, but also revealed the mounting skepticism toward Enron in the investment world. 

That skepticism grew even more when Skilling unexpectedly left his position as CEO.

On August 14th, 2001, the company announced his departure and said that Ken Lay would return to fill the role. Skilling's statement to investors was, "There is nothing to disclose, the company is in great shape...this is an entirely personal decision."

But the move definitely looked strange. Why would such an ambitious CEO resign, just like that, only six months into the job? It was inevitable that Skilling's sudden resignation would raise another wave of speculation into potential troubles at Enron.

### 10. Enron eventually collapsed under the weight of its massive debt and was forced to file for bankruptcy. 

The day after Skilling's resignation, a mid-level Enron veteran named Sherron Watkins mailed an anonymous letter to Ken Lay saying, "I am incredibly nervous that we will implode in a wave of accounting scandals." 

Nonetheless, Lay appeared unperturbed and, according to one Enron executive, "Ken thought there was nothing wrong with Enron that what was right with Enron couldn't fix." But regardless of what Lay said, Enron's massive debt and plummeting stock were poised to send the company into a cash crisis. 

A few of the deals Enron had made in order to bring in cash stipulated the immediate payback of billions in debt if Enron's stock price and credit rating dropped past certain points. Since the media had unveiled how Enron was playing fast and loose with its finances, its stock had already dropped below the allowable limits of these deals, and the company's credit rating was nearing junk territory. 

By way of example, Enron's stock had dropped from an August 2000 peak of $90 per share to below $20 per share in October 2001. 

The company's executives needed to find two or three billion dollars in a hurry, or its panicked creditors were going to shut the company down. But even that was out of the question, as banks had already made clear that Enron's line of credit was bone dry. 

Their only hope to avoid going bankrupt was to merge with _Dynegy_, another Houston-based energy trader that Enron had always held in contempt. Initially, Wall Street traders seemed to think the deal was a good idea, but they soon began to have second thoughts, as did the executives at Dynegy — after all, did Dynegy really know what a merger with Enron had in store?

In short, the deal failed and Enron, without any alternatives, filed the biggest bankruptcy case in the history of the United States on December 2nd, 2001.

### 11. Ken Lay, Jeffrey Skilling, Andrew Fastow and other Enron executives were found guilty of fraud and sentenced to prison. 

In the wake of Enron's collapse, everyone involved — even the board of directors — denied all responsibility. But by 2003, their charade was starting to fall apart, as prosecutors for the United States Department of Justice began issuing a long list of indictments. In the end, 33 people were indicted, and among them were 25 former Enron executives. 

Here's what happened to the people at the top — Lay, Skilling, Fastow and Mark:

In 2004, Fastow pled guilty to all charges. He spent six years in prison and was released in 2011, admitting that he had "engaged in schemes to enrich [him]self and others at the expense of Enron's shareholders." He later added that "I and other members of Enron's senior management fraudulently manipulated Enron's publicly reported financial results. Our purpose was to mislead investors and others about the true financial position of Enron, and consequently, to inflate artificially the price of Enron's stock and maintain fraudulently Enron's credit rating."

Ken Lay passed away before spending a day in jail. He had been indicted on ten counts including conspiracy, making misleading statements and fraud. He pled not guilty to all charges and was found guilty of every one of them. But on July 5th, 2006, before the court had determined his sentence, he collapsed while with his wife outside of Aspen, Colorado — he had died of a heart attack. 

Jeffrey Skilling was indicted in 2004 on 35 counts, including conspiracy, fraud and insider trading. Skilling was found guilty of 19 of his charges, despite maintaining that he "had no idea that the company was in anything but excellent shape." In 2006, he was sentenced to over 24 years in prison and a $45 million fine. 

Oh, and Rebecca Mark?

She left Enron in August of 2000, before the scandal went public. She had sold her Enron stock at its peak, raking in a whopping $82.5 million. Mark was never accused of committing any crimes.

> _"No matter who you asked, it was always somebody else's fault."_

### 12. Final summary 

The key message in this book:

**The energy giant Enron used fraudulent business practices, including a toolkit of accounting scams and a toxic deal-making culture, to artificially inflate its stock. The company's deceitful tactics inevitably led to its collapse, which ended in the biggest bankruptcy case in US history and a slew of indictments.**

Actionable advice:

**Don't let stock prices become an obsession.**

Enron's stock price became all Jeff Skilling could think about. The company's Wall Street standing was like his report card, and when away from the office he would call in multiple times a day to check on the stock's performance. Keeping an eye on the financial health of your company is key, but Skilling's obsession was the spark that ignited criminal impulses, which eventually landed Enron in hot water. Keep in mind that, generally, the quicker a stock rises, the more likely it is to plummet soon thereafter. 

So, instead of solely focusing on ensuring a rising stock price, invest your energy into slow but sustainable business growth!

**Suggested further reading:** ** _When Genius Failed_** **by Roger Lowenstein**

_When Genius Failed_ (2001) follows the rise and fall of Long-Term Capital Management, the world's largest ever investment fund. The book reveals uncomfortable truths about the nature of investment and the fragility of the models we use to assess risk.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bethany Mclean, Peter Elkind

Bethany McLean authored _Fortune's_ March 2001 article "Is Enron Overpriced?" and in doing so became the first person at a national publication to openly question what went on at Enron. McLean now works as a contributing editor at _Vanity Fair_ and a columnist at Reuters.

Peter Elkind, an award-winning investigative reporter, authored _The Death Shift_ and _Client 9: The Rise and Fall of Eliot Spitzer_. He has previously published articles in _The New York Times_ and _The Washington Post_ and is now editor-at-large at _Fortune_.

