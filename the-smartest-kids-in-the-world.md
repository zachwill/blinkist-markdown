---
id: 534e98af6531660007130000
slug: the-smartest-kids-in-the-world-en
published_date: 2014-04-23T11:06:30.000+00:00
author: Amanda Ripley
title: The Smartest Kids in the World
subtitle: And How They Got That Way
main_color: EEA083
text_color: A15A40
---

# The Smartest Kids in the World

_And How They Got That Way_

**Amanda Ripley**

_The Smartest Kids in the World_ takes a look at why South Korea, Finland and Poland seem to have the brightest school children and best education systems in the world. At the same time, problems and potential solutions in the US education system are examined.

---
### 1. What’s in it for me? Discover why you need to be a coach to your children. 

Think back for a second: what was it like when you were in elementary school? Who was your favorite teacher? Do you remember what the classroom looked like? Was it a pleasant experience or did you have to be dragged to school every morning?

Whether we liked it or not at the time, school is an integral part of a child's life. What's more, the education that children get also carries a profound societal significance: well-educated children make for productive employees, and productive employees make for a robust economy.

No wonder then that many countries follow the results of the tri-annual Programme for International Student Assessment (PISA) closely. They are eager to know how their country's education system stacks up against the rest, and more importantly, what can be learned from those that perform best.

In these blinks you'll discover, among other things,

  * Why there's a special police force in South Korea to stop school children studying after 11 p.m.

  * Why you should be your child's coach, not their cheerleader, when it comes to education

  * Why even the students feel that the US high school curriculum is too easy

### 2. The PISA test was a new kind of critical thinking test for comparing nations’ education programs. 

Everyone knows that a nation's future depends on its children and on the education bestowed upon them. This is why countries across the globe have always been interested in comparing their schools and education systems.

In 2000 a new kind of test was introduced for this purpose: PISA.

Developed by the OECD, this test was meant to measure 15-year-old school pupils' abilities in three areas: _reading_, _math_ and _science_. But PISA differed from previous tests in that it was specifically designed to test students' critical thinking abilities rather than what they had learned by heart in the classroom.

For example, students were not required to remember math formulas themselves, but were provided with them in the test. They also had to interpret and analyze new information from, for example, graphs and written texts, and then use that information to solve novel kinds of problems they had not encountered in the classroom.

These kinds of questions were meant to measure the students' problem-solving and critical-thinking skills.

Why?

Because these skills were deemed to be the most important for students if they were to thrive in the demanding, modern workplace.

Indeed, this notion has been borne out by later data: economists have found a near linear correlation between the PISA scores and the long-term economic growth of a nation.

It seems then that better critical-thinking skills translate directly to increased productivity. The obvious knock-on effect of a productive workforce is that it attracts companies seeking good employees, thus attracting investments and further boosting economic growth in turn.

No wonder all eyes are on this new test.

### 3. The PISA test has revealed some surprising differences in the abilities of students across the globe. 

As the results of the first PISA test were unveiled in 2001, a shock wave traveled through the education field.

Countries such as the United States and Germany were surprised at their comparatively low scores, while the top-performing country, Finland, was equally astonished by its high standing.

People in the United States were especially shocked to find that their students only ranked somewhere in between Greece and Canada.

In fact, this lackluster performance was repeated in each later PISA test too: In the latest test, from 2009, US students ranked 12th in the world in reading, 17th in science and 26th in maths.

This was particularly worrying for the United States as they were struggling in the depths of a depression at the time, and of course the PISA results were meant to predict future economic performance.

It is noteworthy, however, that in the United States there is a large gap between affluent and poor students due to the availability of private schools, so many felt these results did not accurately depict the best US students' skills.

Still, it turned out that even an affluent child from Beverly Hills performed worse than an average child from Canada.

Back in 2001 when the original disappointing results were published, the US government under George Bush's response was the No Child Left Behind educational reform act, which aimed to help disadvantaged students.

The results from the 2009 PISA test, however, show that this policy has done little to raise the international standing of America's education system.

Thus it is clear that the United States must re-examine its education system, and, in order to get some input for the necessary reforms, it would seem sensible to examine the countries which have done well on the PISA tests: South Korea and Finland in particular, due to their consistently high rankings, but also Poland due to the vast improvements it has made over the past decade.

In the following blinks we'll examine what lessons the education system of South Korea may hold.

### 4. Korean school children study all day long, under pressure to make it into the country’s top universities. 

The school day in Korea is very different to that in the Western world:

Though classes typically last from morning till around 4 p.m., as elsewhere in the world, in Korea students stay in school for another five hours to study for college entrance exams independently.

And even after this, they don't go home, but instead head off to private tutoring academies known as _hagwons_ to study more. In fact, they would probably keep going all night but for the fact that cities have instituted a study curfew at 11 p.m. in an effort to force the students to sleep for a bit between their _15-hour days_ of study.

Incredibly, even this curfew may not actually be doing trick: despite the law, many _hagwons_ stay open later, so the police have started deploying special "_hagwon_ patrols" to shut down offending academies. And of course there's nothing to stop students from continuing to study at home — something many parents encourage their children to do.

At this stage you may be wondering why Korean students and parents are so obsessed with school.

The answer lies in the next tier of education: universities.

In South Korea, almost every single well-paid or prestigious professional position is filled only with graduates from one of the three most prestigious universities in the country. This means admission into these universities really is the golden ticket to an easy, affluent life.

Naturally enough, competition to get into these universities is beyond fierce, and only the top two percent make the cut. While many parents despise the system and the immense pressure it places on their children, they still play along because the rewards of admission are simply too great to resist.

### 5. In Korea, hagwons provide better quality teaching than schools because they are at the mercy of the market. 

If you were to walk into a class in a Korean school at 10 a.m., you would probably be surprised: at least a third of the students would be asleep.

In fact, many students are so overwhelmed by the demands of the long days of study that they sleep during school classes, preferring to save their energy for their later lessons at the _hagwons_.

Why?

Because the _hagwons_ are generally acknowledged to have higher quality instruction, and so time spent awake there is considered more useful than that spent in the classroom. Indeed, an extensive survey found that Korean students in general felt that their _hagwon_ teachers were more dedicated and better prepared for the job than their public school counterparts.

So from where does this difference between the two kinds of teachers stem?

The simple explanation is that _hagwons_ are private, and hence the teachers are subject to market forces. While regular school teachers receive their base salary no matter what, _hagwon_ teachers' performance dictate what they earn.

This means _hagwon_ teachers are routinely fired if their students' test scores are low, but their salaries can also be phenomenal if they are good at what they do: one _hagwon_ teacher, Andrew Kim, was so in demand that he earned a mind-boggling $4 million a year. These incentives are one clear driver for the high quality of teaching that _hagwons_ provide.

So is this the answer? Should education in the United States be handed over completely to private companies at the mercy of the market?

Even Andrew Kim, the teacher millionaire, feels that this is a bad idea. Why?

Because it promotes inequality: only the richest parents can afford to send their children to the best _hagwons._ In fact, Koreans have already begun to notice that a child's parents' comparative wealth drives their educational outcomes — clearly an unfair situation.

### 6. Korean parents take on the role of the child’s coach, American parents that of the cheerleader. 

As you've no doubt surmised, Korean parents are somewhat notorious for their tendency to push their children hard when it comes to academic achievement.

But more importantly, they take on the role of _coaches_ in their children's lives when they are young. In fact, they consider educating their children to be part of their job as parents.

Studies indicate they coach their children both informally — for example, by quizzing them on their multiplication tables while doing chores — and also in a structured, systematic way (e.g., devoting time every evening to teaching them to read or do maths using a proper workbook).

This is very different to typical American parents, who are often reluctant to engage in such structured learning, and feel their children should learn organically through playing.

What's more, American parents are so worried about crushing their child's self-esteem that they heap praise on them for even the tiniest successes, acting like _cheerleaders_. In fact, one study showed that 85 percent of American parents felt they needed to praise their child's intelligence to convince the child that they are smart.

But studies have shown that the coaching methodology is far more beneficial for the child: children whose parents spend more time coaching them when they're young do consistently better in school.

Furthermore, the cheerleading approach can have negative effects on the child: studies show that insincere or excessive praise can actually discourage children from working hard.

So it seems parents all over the world might benefit from taking a page from their Korean counterparts' book and introducing some structured coaching into their interactions with their children.

However, it is noteworthy that sometimes this approach can go too far.

One horrifying example of this was a Korean boy named Ji, whose mother pushed him so hard that he eventually murdered her. Her demand? That he improve on his current top one-percent national academic standing to become the _top_ individual student in the nation.

In the next blink, let's examine what we can learn from the Finnish teacher education system.

### 7. The Finnish education system relies on the high quality of teachers. 

As noted before, Finnish students have gotten consistently high rankings in each PISA ranking since the test's induction. But in contrast to South Korea, Finnish students do not study all day and night. So what explains these high scores?

In short, the Finnish _teacher education system_.

First of all, the entrance requirements to enter a teacher education program in a Finnish university are highly demanding: only 20 percent of applicants are accepted, and all those who made the cut were in the top third of their high school classes.

Compare this to the United States, where many teacher education programs have no admission standards whatsoever, and only 20 percent of teacher trainees were in the top third of their high school classes.

In addition to the high entry standards, the actual teacher training programs in Finland are very demanding. The rigorous program lasts six years and includes a year of practical training in a public school, where they are mentored and critiqued by three professional teachers.

In the United States, the teacher trainee programs are far shorter and not nearly as demanding. In fact, education is known as one of the easiest majors. Worse still, US teachers spend a mere 12 to 15 weeks actually practicing teaching.

All this results in a grave shortfall in the quality of American teachers.

So what can the United States do about this?

One fairly obvious solution would be to be more selective and demanding when it comes to teachers, especially since every year the number of teachers that graduate is more than double what the nation actually needs. But these kind of suggestions tend to face stiff opposition, not least because all the institutions churning out teachers would lose tuition fees.

The state of Rhode Island has tried to significantly raise its criteria for becoming a teacher trainee in terms of, for example, applicants' SAT scores. It remains to be seen how this will affect the academic results of the state, something other states are no doubt watching closely.

In the next blink you'll discover what the government of Poland did to raise the country's PISA rankings, and how the United States might benefit from these measures.

### 8. Poland drastically raised its PISA rankings by making reforms like the introduction of standardized testing. 

Another interesting country in terms of PISA results is Poland, as it has managed to greatly improve its PISA standing since the advent of the test, now ranking alongside the United States despite high levels of child poverty.

Let's examine how this was achieved.

First of all, in 1998 the Polish government introduced a new, more rigorous national education curriculum with higher expectations for students. The idea was that the curriculum set uniform national goals to be achieved in each classroom, but individual teachers had a lot of autonomy in deciding the details, like, for example, the textbooks used.

At the same time, the teachers were encouraged and in some cases mandated to keep up their own professional development through further education and training.

To track the success of these reforms, the government also introduced a new set of _standardized tests_ that all Polish school children would take at various stages of their education.

The results of these tests were used to identify which students, teachers and schools performed poorly and thus needed more support.

But in addition to this function, the scores in the tests also carried real consequences: they determined which high schools and, later on, which universities the students would attend. Thus the test not only motivated students, but also helped universities and employers better gauge applicants by providing a national yardstick for comparison.

Finally, the structure of the children's academic track was also changed. Previously, students were divided into vocational and academic programs at age 15 based on their academic performance. This method is known as _tracking,_ because students diverge onto different tracks.

But as part of the reform, the government decided to postpone tracking by a year till age 16. This ensured a better education — as well as higher expectations — for students who would later continue on to vocational programs.

### 9. The standards of the United States’ education curriculum should be raised considerably. 

Let's examine how the United States might implement some of the measures used in Poland.

Firstly, the standards of the curriculum should definitely be raised.

Why?

To begin with, a typical eighth-grade mathematics class in the United States features content that would typically be taught in the sixth or seventh grade elsewhere in the world.

Also, when the author surveyed students who had gone on foreign exchanges either to or from the United States, she found that nine out of ten of those who had come to the United States felt the American classes were easier than abroad, while seven out of ten American students who had gone abroad echoed this sentiment.

This view is even held by US students in general: a large national survey found that over half of American high school students felt that their history work was mostly too easy.

The ease of high school often leads to a shock when students go on to college. In Oklahoma, for example, many of the high school students who attend Oklahoma public colleges are promptly placed into remedial classes to bring them up to speed with college standards.

What's more, evidence shows that in states where standards have been raised, improved performance has followed. Fourth-graders in Minnesota, for example, scored below the national standard in a math test in 1997, but then more demanding standards in math were introduced, and in 2007 elementary school students from the state surprised the nation in an international math test by performing as well as their Japanese peers.

Happily, it seems that a consensus regarding the need to raise curriculum standards does seem to be emerging: recently 45 states agreed to adopt a common set of more rigorous standards for reading and math built around those used by leading PISA nations.

### 10. The United States is not reaping the benefits of standardized testing, and is tracking students too early. 

What about the second major reform the Poles implemented: standardized testing?

In short, it seems to be a very beneficial practice.

Take standardized high school graduation exams: every nation that did well in the PISA test had these exams.

However, while many US states also have them, the exams are mostly very easy and carry no real consequences: students can graduate without even passing them.

This means the benefits of the exam are largely lost in the United States.

When it comes to standardized tests in earlier grades, the US system also seems somewhat backward: while in most developed countries the tests are used to determine which schools need more support and resources, in the United States the tendency is to provide less funding for schools that fare poorly in such tests.

In Pennsylvania, for example, the poorest school districts spend 20 percent less per student than the richest ones, whereas in almost every other developed country the opposite logic is applied: the worse off students are, the more money the school gets to teach them.

Finally, when it comes to the third major Polish reform — delaying the tracking of students into separate groups — many countries have begun to follow this trend. The result? Children tend to do better because the delay keeps expectations equally high for all students for another year, pushing them to perform better.

In this regard, too, the United States finds itself at the wrong end of the spectrum: tracking is imposed on students earlier than in any other country. In fact, it can even happen in elementary school as some students are directed to, for example, separate honors classes for high-achieving students.

What's more, in the United States these more advanced programs even teach different content to regular classes, while in other countries all students are expected to learn the same, difficult core content, with the advanced students just being able to dive deeper into the material.

Clearly more must be demanded from American teachers and students alike.

### 11. Final Summary 

The key message in this book:

**To improve the sorry state of the education system, the United States must adopt measures like a more rigorous standard curriculum, more consequential standardized testing as well as delayed tracking of students based on performance. At the same time, more must be demanded of teachers. Parents too must play their part in their children's education by acting as their coaches.**

Actionable advice:

**Coach your children.**

If you have children, try to act as their educational coach: Quiz them on their homework informally while, for example, cooking dinner, but also make time for structured study together. Buy some work books for their school subjects and dedicate a certain amount of time every evening to looking at a few assignments together. Don't heap praise on them if they perform poorly, but instead try to motivate them to do better.
---

### Amanda Ripley

Amanda Ripley is an author and journalist who writes for _Time Magazine_ and _The Atlantic_. Her previous book is the highly acclaimed _The Unthinkable: Who Survives When Disaster Strikes._

