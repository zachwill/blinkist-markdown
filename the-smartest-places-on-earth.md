---
id: 58dbaec7ac33df00047ba94b
slug: the-smartest-places-on-earth-en
published_date: 2017-03-31T00:00:00.000+00:00
author: Antoine van Agtmael and Fred Bakker
title: The Smartest Places on Earth
subtitle: Why Rustbelts Are the Emerging Hotspots of Global Innovation
main_color: A85832
text_color: A85832
---

# The Smartest Places on Earth

_Why Rustbelts Are the Emerging Hotspots of Global Innovation_

**Antoine van Agtmael and Fred Bakker**

_The Smartest Places on Earth_ (2016) tells the story of former industrial powerhouse regions around the world that fell into decline but have since reemerged as centers of innovative collaborations. These blinks describe how countries in the West are using this model to challenge the cheap labor-oriented mass production model that has come to prominence elsewhere.

---
### 1. What’s in it for me? Learn about the smartest and most innovative places on earth. 

Where are the brightest brains on the planet? We would, in all likelihood, instinctively turn toward big cities and renowned universities. But we would be wrong.

Instead, it's former industrial sites, the traditional domain of factory workers, mechanics and builders that are the new centers of knowledge and innovation. Combining the practical skills and experience of technical workers with the creative brainpower of polytechnic institutes and universities, these thriving new hubs are developing solutions to the major global problems facing us right now.

Welcome to the "brainbelts"!

In these blinks, you'll learn

  * why there is still space for manufacturing companies in the West;

  * where the smartest people in the world live; and

  * which projects and ideas the world's smartest people are developing.

### 2. Former industrial sites are emerging as hubs of innovation to compete with emerging markets. 

Have you heard the term _rustbelts_? It refers to formerly industrial areas in the United States and Europe, places that have largely declined in affluence because of the offshoring of manufacturing to cheaper production sites in countries such as China, Bangladesh, Mexico and Turkey.

But that's all starting to change. Some of these rustbelt regions are transforming into _brainbelts_ ; regions that have been revitalized through the formation of hubs that unite knowledgeable workers with smart manufacturing. Just take the new production facility created by General Electric, an iconic company that produces aircraft engines and household appliances, among other products.

Rather than offshoring their production to Asia, GE set up shop in Batesville, Mississippi because of its close proximity to Mississippi State University. In this new location, researchers work with cutting-edge materials to revolutionize the production of aircraft engines.

GE had already been working with the university and, because of the great results the collaboration was producing, decided to become its neighbor. The choice has proven to be a boon to GE, as the work between the two institutions brings together innovation and smart manufacturing.

However, just because some companies succeed with this model doesn't mean that emerging markets in the East are no longer a threat; rather, these markets are just facing stiffer competition. For instance, many Western industrial powerhouses have had to close their doors, overwhelmed by the low-cost production model — built largely on cheap labor — employed in East Asian countries.

That being said, the rise of brainbelts is causing concern for some Asian companies. Just consider the Taiwan-based Mediatek; for years, they've been a powerful force in the global marketplace, designing chipsets for smartphones and other such electronics.

Despite this success, the company's CFO, David Ku, has expressed grave concerns about the increasingly competitive nature of US firms. He pointed to the San Diego-based company Qualcomm as an example, saying that they're way ahead of the game in terms of research and development.

So, you now know why brainbelts are important. Next, you'll learn how exactly they come about.

### 3. Brainbelts share common characteristics including a collaborative ecosystem and a culture of sharing. 

Although each brainbelt is unique, they all share certain basic characteristics.

One common characteristic of brainbelts is a collaborative ecosystem to which countless people contribute. Here's how these structures function:

Generally, brainbelts operate with a research university at their core. From this base, a variety of other institutions branch out, which can include start-ups or more established firms, community colleges and local governmental authorities. Each of these different institutions are closely interwoven into one another's activities.

Just consider the Hudson Tech Valley in Albany, NY, also known as the Albany brainbelt. At the center of this grouping is the State University of New York and the private research university Rensselaer Polytechnic Institute. These core institutions are surrounded by manufacturing businesses and community colleges, all of which collaborate on semiconductor research, which plays a significant role in computer technology.

Another core attribute of brainbelts is an openness to sharing knowledge and expertise. This results in freedom between sectors, such as industry and academia, but also between disciplines, like math and biology. Instead of being hoarded, knowledge and expertise are shared to undertake the complicated projects that come to define these brainbelts.

Beyond that, companies within brainbelts tend to be highly specialized, which means their knowledge rarely overlaps with that of their collaborators. This is key, as it keeps the threat of competition to a minimum.

A great example is the Oregon Health & Science University located in Portland. This institution had collected a wealth of data on cancer patients, but didn't have the computing power necessary to process it. Intel, also a major player in the Portland area, was able to pool its computers to process the university's data.

The two partners were excited to share their knowledge and expertise since they weren't in direct competition. Quite the contrary, each depended on the ability of the other, and they were thus glad to work together.

But these aren't the only key characteristics shared by brainbelts. In the next blink, you'll learn about a few more.

### 4. Focused effort and smart manufacturing are core aspects of brainbelts. 

Do you consider yourself a focused person? Well, not everyone does. Some people want to be doctors one day and geologists the next. But for brainbelts, this isn't an issue.

Another main characteristic of these innovative sites is their focused effort. Instead of taking on a wide range of activities, brainbelts invest their energy in just one or two.

For instance, the Hudson Tech Valley in Albany works primarily on semiconductors like chips and sensors, while the life-science brainbelt in Zurich, Switzerland focuses on biotechnology and biopharmaceuticals.

Another defining feature of brainbelts is their use of smart manufacturing. Unlike traditional manufacturing operations that are focused on cost and worker efficiency, brainbelts' smart manufacturing strategy emphasizes concepts such as customization and automation by using technologies like 3-D printing and robotics.

For example, the authors visited America Makes in Youngstown, Ohio, which was formerly known as the National Additive Manufacturing Innovation Institute. During their trip, they met Kevin Collier, who manages the Innovation Factory there. He told them that 3-D printing enables both rapid production of prototypes and real manufacturing.

So, while aircraft manufacturers can use 3-D printing to create certain engine components, people in medicine can also use this technology to produce customized body parts, like personalized hips and knees for injured veterans.

Not only that, but robotics allows for automation that's both cheap _and_ smart. Just consider Baxter, a humanoid robot used by Scott Eckert, president of the Boston-based firm Rethink Robotics.

Baxter works as an assistant at the company, lifting and rearranging various objects, including other robots. He cost $22,000 to produce, which is a pittance considering he can work for 6,500 hours without getting sick or taking a vacation. That breaks down to an hourly wage of just three dollars!

Now that we're getting a clearer picture of how brainbelts work, let's take a closer look at one in particular to see how it rose from the ashes of collapsed industries.

### 5. Akron, Ohio turned into a rustbelt before being revitalized as a brainbelt. 

If you're searching for examples of industrial hubs that went from great to bad and back to great again, you need look no further than Akron, Ohio. Here's the story of this rustbelt-turned-brainbelt:

Because of its strategic location, Akron used to be an industrial powerhouse. Its convenient location near Detroit and right between New York City and Chicago made the Detroit car industry a key contributor to its supply chain and resulted in tire giants like Firestone and Goodyear basing their operations in Akron.

The city's location also made Akron an essential railway hub in the transportation of cereal grains produced in the region. For instance, Quaker Oats did a great deal of business in the area, reflected by the proliferation of the company's massive grain silo complexes.

But then, in the mid-twentieth century, things began to shift as Akron failed to account for the competitive threat arising overseas. After years of profitable, stable business, the tire companies had become too accustomed to their success; they were so detached and inward-looking that they failed to take notice of their powerful competitors in China and Mexico, and adapt their business strategies accordingly.

When they finally did wake up to this reality, it was too late. Akron withered away into dormancy and the sites were abandoned.

Well, for a little while at least — the brainpower of these industries remained, which was central to Akron's eventual revival.

Many of the skilled workers who had previously worked in the tire industry began their own firms. In addition, the president of the University of Akron, Luis Proenza, began to reposition the university to become a strong central force in the city.

Much of the industrial and educational work in Akron was focused on polymer science, which was quickly broadening into the realm of new materials science. By sticking to a field in which many local residents were already skilled, Akron managed to rebuild its success.

One great example is Akron Polymer Systems. Working with polymers, this firm develops films for the LCD screens used in solar cells, among many other things.

### 6. New technologies developed in brainbelts can help solve humanity’s most pressing problems. 

Climate change is probably the single greatest challenge facing humanity in the twenty-first century. For far too long, we've extracted and burned dirty fossil fuels like coal and petroleum. But there's still hope for the future.

New technology being developed and manufactured in the brainbelts could help mitigate some of the devastating effects caused by human-initiated climate change. After all, our only hope today is to develop new, cheap sources of renewable energy, which is exactly what many brainbelts are focusing on.

Just consider the Centennial Campus in North Carolina. At this site, the company ABB is collaborating with university researchers to improve certain components of the electrical grid. This existing network of lines ensures the delivery of energy from producers to consumers — but it could be made a whole lot smarter.

For instance, consumers could become their own producers, creating, storing and even selling their own energy, as made possible by home solar panels. By helping make such technologies an accessible reality, brainbelts are contributing to counteracting climate change.

Another pressing issue that brainbelts are working on is the need to feed all of the 9 billion people who are expected to live on earth by 2050. To meet this gargantuan goal, we'll need agricultural innovations in areas like greenhouse design and automated milking.

Such technologies could help farmers meet the production needs for their local markets, and a prime example is the brainbelt surrounding the town of Wageningen in the Netherlands. The agricultural university there has partnered with several organizations of farmers and companies to boost productivity and efficiency in Dutch agriculture. Such collaboration has enabled this relatively small country to deliver an incredible 7.5 percent of all global food exports.

### 7. Brainbelts could be greatly aided through national policies and continued funding. 

Unless you were some kind of infant prodigy, you likely weren't born with the ability to ride a bike. Kids need training wheels to get them going before they can ride on their own, and the same goes for brainbelts.

To capture their full potential, these areas sometimes need a bit of help. This is important to note since, when people think of innovation, they tend to think of industry leaders like Apple, Google and Amazon. These companies have done exceedingly well at building immense technology empires and have all the resources and talent they need to keep innovating and succeeding.

Unlike these titans of industry, brainbelts are weighed down by past stagnation, abandoned buildings and out-of-date infrastructure. That being said, they have the brainpower they need to shake off these disadvantages — as long as they can get a helping hand.

One way such assistance can be provided to brainbelts is through national innovation policies and guidelines, as well as continued funding. Innovation policies and guidelines are essentially motivators for innovation and are therefore a big boon to brainbelts. While most countries have such policies on the books, not all of them do.

Consider the United States. Although the US government has funded initiatives to encourage innovation over the years, there are no standing guidelines to support these expenditures. While people often look to Silicon Valley and assume that there's no need for such policies, there are other young brainbelts all over the country — and, indeed, around the world — that would benefit tremendously from a helping hand.

Beyond the need for policy support, continued funding is also crucial to newly emerging brainbelts. Luckily, many funding sources are available, from national governments to private companies. However, the process is far from perfect, since many of these resources are invested in the wrong projects.

Just take the US technology sector. Lots of investors pour their money into Google and Apple, but fail to invest in the cross-disciplinary and innovative projects at the heart of the brainbelts.

> _"Innovation is a marathon rather than a sprint."_

### 8. Final summary 

The key message in this book:

**The power dynamics of market forces are gradually shifting in the global marketplace. Sites of former industrial glory in the West, which have been decimated by the low-cost production methods of Eastern firms, are rapidly rebuilding themselves through the innovative use of shared brainpower.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Start-Up Nation_** **by Dan Senor and Saul Singer**

For a small country, Israel punches far above its weight as a global hub of innovation and tech entrepreneurship. _Start-Up Nation_ explores the country's history, geopolitics and culture to try and understand where this creative energy comes from, and offers stories of companies that exemplify the distinctive ways in which this drive is channelled.
---

### Antoine van Agtmael and Fred Bakker

Antoine van Agtmael is a senior advisor at the public policy advisory firm Garten Rothkopf, based in Washington, DC. A former financier, he coined the term "emerging markets" and founded the investment management firm Emerging Markets Management, LLC.

Fred Bakker is a recently retired Dutch journalist and former CEO, as well as editor-in-chief of _Het Financieele Dagblad_, Holland's equivalent of the _Financial Times_.

