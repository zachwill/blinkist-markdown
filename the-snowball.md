---
id: 58bd17c3c7c53e00041d6602
slug: the-snowball-en
published_date: 2017-03-08T00:00:00.000+00:00
author: Alice Schroeder
title: The Snowball
subtitle: Warren Buffett and the Business of Life
main_color: CEAE75
text_color: 7A643E
---

# The Snowball

_Warren Buffett and the Business of Life_

**Alice Schroeder**

_"By age eleven he'd saved up $120, which was a whole lot of money in 1941. He used that money to make his first investment. He bought six shares of the company Cities Service Preferred — three for him and three for his sister Doris."_

_The Snowball_ (2008) offers a revealing look at the life and times of one of modern America's most fascinating men: Warren Buffett. Find out how this shy and awkward man earned his first million dollars and how following a few fundamental rules enabled him to become the world's wealthiest man.

**This is a Blinkist staff pick**

_"Warren Buffet is one of those people who seems to have a magic touch. It's almost as if he's aware of certain universal secrets that nobody else is privy to! I love learning more about what made him so successful (he memorized textbooks?!) and trying to figure out what makes him tick."_ — Ben S., Head of Audio at Blinkist

---
### 1. What’s in it for me? Get to know one of our age’s greatest investors. 

Warren Buffett is probably the most famous investor of the twentieth and twenty-first centuries — maybe even of all time. Known as the "Oracle of Omaha" or the "Sage of Omaha," he can frequently be seen in the media sharing his opinion on business and politics. But few know what it is he actually does and even fewer how Buffett came to occupy the position he's in today.

In these blinks, we delve into the life of Warren Buffett and follow him from his early life to his first business endeavors to his current status as an American sage of modern business. His story offers an intriguing and educational glimpse into the business of life.

In these blinks, you'll find out

  * how Buffett managed to make his first investments at age 11;

  * Why Buffett's investments are like a snowball; and

  * how friends like Kay Graham and Bill Gates influenced the Sage from Omaha.

### 2. As a child, Warren Buffett found comfort and joy in statistics and numbers. 

Warren Buffett was born in Omaha, Nebraska, on August 20, 1930 — a mere ten months after Black Tuesday, the stock market crash that sent the country plummeting into the Great Depression.

His father, Howard, was a well-liked stockbroker who managed to sell reliable securities and bonds during this bleak period. As a result, unlike many other families, the Buffetts were able to bounce back from the crash and live comfortably during the 1930s.

However, this didn't mean things were easy for the young Warren.

His mother, Leila, was an overbearing parent, quick to fly into a rage, an unfortunate tendency that resulted in her needlessly shaming and blaming her children.

Stuck at home with this abrasive and unpredictable parent, Warren was eager to find an escape. More often than not, his older sister Doris was the target of their mother's rage, but Warren also sought ways to avoid his mother's temper, and he found it in numbers, odds and percentages.

One of the reasons he loved school was that it got him away from home and taught him more about mathematics.

After his first-grade classes let out, Warren and his friend Russ would sit on Russ's front porch and note down the license plates of passing cars. Their parents thought this was so they could calculate the frequencies of each letter and number appearing on the plates, but in reality, the boys secretly believed their notes could help the police catch bank robbers, as the street was the only possible getaway route from the local bank.

Sometimes he was able to get away from home by spending time at his dad's office on the weekends, where he happily wrote the numbers of stock prices on the office's big chalkboard.

The young Warren's interest in numbers, probabilities and statistics was noticed and encouraged by other family members. At the age of eight, Warren received a book on baseball statistics from his grandfather, a gift that delighted Warren. He devoted hours on end to memorizing every page.

He received another gift from his beloved aunt Alice: a book on the complex card game bridge, which sparked a lifelong obsession.

With these wonderful books, Warren could sit happily in his room, away from his erratic mother, and spend time in the company of more comforting and reliable companions: numbers.

### 3. Buffett began earning money and investing at a remarkably young age. 

If there's one thing that fascinated Warren more than numbers, it was money.

When he was nine years old, Warren was already making money by selling packs of gum and bottles of Coke to his neighbors. A year later, he was selling peanuts at football games at the University of Omaha.

Warren's interest in money intensified in 1940, when he spotted a book at the library called _One Thousand Ways to Make $1,000_. The ten-year-old Buffet was instantly inspired by it, and confided in a friend that he planned to be a millionaire by age 35.

He was certainly proving himself to be a determined kid: by age eleven he'd saved up $120, which was a whole lot of money in 1941.

He used that money to make his first investment. He bought six shares of the company Cities Service Preferred — three for him and three for his sister Doris.

In high school, the odd jobs continued; he sold golf balls and bought pinball machines that he rented out to barber shops.

But his wages really went up when he started delivering newspapers.

In 1942, his family had moved to Washington, DC, after his dad was elected to serve in congress as the Republican representative for Nebraska's second district.

It was here that Warren started delivering papers and selling subscriptions on three different routes, one of which contained three popular apartment buildings that were home to many US senators.

Since Warren earned a portion of the collected subscription fees, he was a highly motivated paperboy and a stickler for making sure his customers paid up.

Remarkably, he was earning around $175 per month at this time — more than most of the teachers at his school. Before long, his savings had grown to $1,000.

In 1944, at the ripe old age of fourteen, Warren filed his first tax return. He cited both his watch and his bicycle as deductions, and payed $7.00 in total.

> As a 15-year-old, Warren actually bought a 40 acre farm, splitting the profits with the farm's tenant.

### 4. After studying business in college, Buffett learned the secrets to success in the stock market from his teachers. 

Considering Buffett's all-consuming interest in money and numbers, it's unsurprising that his classmates labeled him as a "Future Stockbroker" the high school yearbook.

It was also unsurprising that Buffett chose to study accounting and business at the University of Nebraska.

As he moved out of his family home and onto the college campus, it became apparent that he was a pretty messy guy. Indeed, his first roommate was so annoyed by Buffett's messiness that he decided to move out after the first year.

But his roommate was perhaps more frustrated by Buffett's ability to effortlessly memorize entire sections of textbooks, which he could then recite back to his teachers verbatim.

This gave Buffett more time to listen to music and not pick up after himself, much to the annoyance of those who had to work harder to get passing grades.

Since Buffett found college rather easy, he was quite surprised when, after applying for a graduate program at Harvard Business School, he received a letter of rejection.

But this proved to be a fortunate failure. Buffett was accepted by Columbia University, where he studied under Benjamin Graham, the author of the book _Intelligent Investor_ and a man whose mentorship left quite an impression on Buffett.

Buffett loved Graham's book dearly, so when he found out he was teaching at Columbia, he forgot all about Harvard. He was also excited about another class that was being taught by David Dodd, the author of _Security Analysis_, yet another book that Buffett had memorized.

Both of these teachers taught Buffett valuable lessons and fundamental investment strategies.

For example, Buffett learned about the importance of investigating a company from top to bottom in order to determine its _intrinsic value_ — the amount of money it's actually worth. This value is then compared to its _perceived value_, which is how much the stock is currently selling for on the market.

When a company's intrinsic value is far higher than its perceived value, it may be what Graham called a "cigar butt" — an undervalued businesses worth investing in. Graham's success rested in large part on the realization that there's a good chance that perceived value will eventually rise to meet intrinsic value.

### 5. After starting a family, Buffett became his own boss by establishing a unique partnership. 

Throughout college, Buffett was uncomfortable around girls. His shyness was so severe that he even enrolled in a public speaking class, hoping it would boost his confidence and make him feel less awkward.

When he took this class, there was one young woman in particular that Buffett wanted to impress.

Her name was Susie Thompson, and though her father took an instant liking to Buffett, it took a good deal of persistence before Susie warmed up to his awkward charm.

A nervous wreck, and too eager to impress, Buffett initially came across as arrogant and privileged. But once Susie gave Buffett a chance, she realized that his posturing was just a symptom of his awkward shyness and, eventually, she fell in love with his charming vulnerability.

The two got married in 1952, and Buffett kept busy teaching classes and working at his dad's old investment firm.

Their first child, Susie Alice Buffett, was born in 1953, the same year Buffett got the job he'd been dreaming about for years — working for Ben Graham's investment firm, Graham-Newman _._

Buffett quickly became the rising star of Graham-Newman, even though he soon realized that he actually hated being a stockbroker.

He couldn't stand the thought of picking the wrong investment and losing someone's hard-earned money. So he soon began plotting his own partnership.

After the birth of his second child, Howie Graham Buffett, Warren's plans to become his own boss became a reality. In 1956, he launched Buffett Associates, Ltd.

The idea behind this partnership was that it would only include friends and relatives, and there would be simple rules behind every investment so that no one could be disappointed or have unrealistic expectations.

At the same time, Buffett's reputation was given a boost by his mentor and former boss, Ben Graham. Shortly after Buffet left his firm, Graham decided to retire and close up shop. But on the way out, he recommended Buffett as a reliable man for his clients to invest their money with.

### 6. In his original partnerships, Buffett stuck to a strict philosophy and it proved successful. 

In his first year as his own boss, Buffett began a series of eight partnerships based on different sets of friends who gave him anywhere between $50,000 to $120,000 to invest.

Each time Buffett started a new partnership, he made sure everyone understood his philosophy.

He would tell his potential partners how he only invested in undervalued stocks, and that any earnings would then be reinvested in these same stocks. In a way it was like rolling a snowball down a hill: what starts as a small handful eventually grows bigger and bigger.

He also made sure they knew he wasn't the kind of investor who would cash out when a stock hit a certain number — he was patient.

And this patient consistency paid off. At the end of 1956, his partnerships beat the market by 4 percent; at the end of 1957, it was 10 percent, and at the end of 1960, it was 29 percent. The snowball was rolling.

By the start of the 1960s, Buffett was already managing over a million dollars. At this time, the stock market was on an upward swing; unlike many other investors, however, this shift in the market didn't change his way of doing business.

He still searched for undervalued companies, and when he found something he wanted, he bought up as much stock as possible.

This often meant earning a seat on the board to make sure that the executives didn't do anything foolish with the investors' money.

Remarkably, while managing millions of dollars, Buffett was still doing all of his own paperwork. But in 1962 he decided to make things less complicated and dissolved all of his individual partnerships into a single entity: Buffett Partnership, Ltd.

Around this time, Buffett's success was starting to expand beyond Omaha to Wall Street, where he was gaining recognition as one of the few major players who didn't work in New York City.

However, some established moneymen remained skeptical and predicted that he'd go broke any day now.

> _"In October 1967,_ [Buffett] _wrote that from now on he would limit himself to activities that were 'easy, safe, profitable and pleasant.'"_

### 7. In the mid-1960s, the partnership grew big enough for Buffett to start buying entire businesses. 

One person who recognized Warren Buffett's talents early on was Californian lawyer and part-time investor, Charlie Munger. The two like-minded individuals became fast friends after a long lunch in 1959 — a friendship that inevitably led to a fruitful business partnership.

Munger's perspective opened Buffett's eyes to bigger possibilities and helped him realize that you could still play it safe while moving beyond those "cigar butt" stocks.

Indeed, the Buffett partnership would soon be taking a huge leap forward, thanks in large part to a certain stock that Buffett picked up at just the right time.

When John F. Kennedy was assassinated in 1963, few people were paying attention to any other story. But, by then, Buffett was a creature of habit, and he continued digging through the back pages of the newspapers, where he came across a story about a soybean scandal involving American Express.

A subsidiary of American Express had certified that certain storage tanks contained soybean oil, but they were later revealed to be filled with seawater. As a result, American Express stocks took a severe hit. But this didn't worry Buffett; he knew the company would bounce back.

So when prices hit rock bottom in January of 1964, he began gradually pouring money into American Express: $3 million at first, and then, by 1966, $13 million.

Naturally, American Express did bounce back, and it brought the partnership unprecedented rewards, enough for Buffett to start buying entire businesses.

One of the first buyouts was a business that would come to define Buffett — the small Massachusetts textile manufacturer, Berkshire Hathaway.

Buffett's research showed that its intrinsic value was $22 million, which meant it should be selling at $19.46 per share. Yet it was selling at only $7.50 per share.

In 1965, after some negotiation, Buffett got controlling interest in Berkshire Hathaway by purchasing 49 percent of the company at a little over $11 per share.

That same year, Warren and Susie earned an additional $2.5 million, thanks largely to the American Express investment, which meant that Buffett had more than met his goal of becoming a millionaire by age 35.

### 8. With larger purchases came larger problems, and some new rules to follow. 

Even though Buffett is closely associated with Berkshire Hathaway now, the company was so problematic that Buffett would come to regret ever getting involved with it.

But Buffett isn't the kind of investor who likes to cut his losses, a philosophy that goes back to before his involvement with Berkshire Hathaway.

In 1958, Buffett made a similar purchase of a company in Nebraska called Dempster Mill Manufacturing, which made windmill and water irrigation systems.

But things quickly fell apart: he put the wrong management in charge, the company went bankrupt and he decided to liquidate the company's assets. As a result, people lost jobs and the neighboring community openly expressed their dislike of Buffett.

Determined to never let this happen again, Buffett wanted to be sure that the right person was in charge of Berkshire Hathaway and that the business stayed alive.

This presented many challenges since the costs of textiles was on the rise during the 1960s and 1970s and the company's machinery was in desperate need of modernization. But Buffett never wasted money, so he was extremely hesitant about injecting additional capital into a company that held no real promise of turning a profit.

All this meant that Berkshire Hathaway would continue to be a burden as a textile manufacturer. Nonetheless, Buffett kept it alive by continuing to purchase winning stocks in its name every chance he got, eventually giving Berkshire Hathaway one of the world's best stock portfolios.

Despite the problems presented by Berkshire Hathaway, Buffett was doing extremely well. His investment partnership was in such good shape that he decided to close its doors to new members and tighten his investment rules.

More and more technology companies were emerging in the late 1960s, which prompted Buffett to make a new rule: he would never buy stock in a company that offered a product or service he didn't fully understand.

Buffett liked things "easy, safe, profitable and pleasant," which led to another rule: no involvement with businesses that had potential or proven "human problems" such as impending layoffs, plant closings or a history of executives fighting with labor unions.

### 9. As the partnership dissolved, the Buffetts began to become involved in more personal, and separate, endeavors. 

Even after Buffett became a millionaire, he remained a notoriously shabby dresser, a man utterly unconcerned with his outward appearance. Far more important to Buffett were the details and characteristics of the people who were in charge of his businesses.

Reliable businesses have reliable management, so when Buffett made his acquisitions, he made sure they were being run by good people.

One of the main reasons Buffett decided to purchase the Baltimore department store Hochschild-Kohn, as well as Associated Cotton Shops, which ran a string of retail stores, was the people behind the scenes.

Buffett got in the habit of sitting down with company managers and getting to know them well. He wanted to make sure they were enthusiastic people that he could trust.

For a while, Buffett had his eyes on an Omaha insurance business called National Indemnity. But it wasn't until he met with Jack Ringwalt, whom he immediately recognized as a great manager, that he decided to buy.

These were smart moves, and by the end of 1966, the partnership was doing better than ever, outperforming the market by 36 percent.

Buffett regarded the managers of these business — as well as his investment partners — as family. And as the 1960s came to a close, Buffett began offering to buy out his partners.

It was a way to wind down the partnership so that he and Susie could focus more on personal endeavors.

Susie continued to hope that her husband might retire, or at least devote more time to his kids, who were quickly growing up without much involvement from their father.

Susie was also busy: she pursued a singing career and got involved in the pressing social issues of 1960s America, attending civil-rights and anti-war protests.

Even Warren, who mostly steered clear of politics, couldn't help but get involved. In 1967, he became the treasurer for the Nebraskan office for Democratic presidential candidate Eugene McCarthy.

Warren's move into politics had a lot to do with the death of his father, who was a devout Republican. After his father died, Buffett could finally express his own political views without worrying about disappointing him.

### 10. In the 1970s, Buffett became involved in the newspaper business. 

Back when Buffett was delivering newspapers in Washington, DC, he dreamed of one day owning his own paper. And when he made $16 million in earnings in 1969, he was finally in a position to realize that dream.

That year, Buffett purchased a controlling interest in the _Omaha Sun._ Not only did this fulfill one of Buffett's dreams; it would eventually lead to a cherished award.

In 1972, the _Omaha Sun_ published an investigative article on Boys Town, a local shelter for homeless boys that dated back to 1913. When the article was written, this shelter had become a huge 1,300 acre compound, with it's own farm and stadium, being run by a priest named Father Edward Flanagan.

Curiously, though, it housed only 665 boys — and there were 600 employees.

It seemed like something fishy was going on, so Buffett helped the _Sun_ 's editor commission an investigation. And Buffett's hunch led to a major scoop. Boys Town had in fact been stockpiling donations, grants and funds, amassing around $18 million per year.

The article — "Boys Town: America's Wealthiest City?" — ran on March 30, 1972, earning Buffett's paper the Pulitzer Prize for outstanding regional journalism. The story immediately went national and led to reform regarding how non-profit organizations were run.

After this success, Buffett then set his sights on a national newspaper: the prestigious _Washington Post._

By the summer of 1973, Buffett owned over 5 percent of the _Washington Post_ and was even developing an extremely close relationship with its publisher, Kay Graham.

The following year, he joined the newspaper's board and started attending Graham's lavish dinner parties. He spent many nights awkwardly attempting to mingle with famous guests like the actor Paul Newman and trying not to embarrass himself in front of respected senators, diplomats and dignitaries from around the world.

### 11. Buffett faced a fair share of early challenges, including a stressful SEC investigation. 

When you're as active an investor as Warren Buffett and Charlie Munger, you're bound to pick up a couple of troublesome companies.

When Munger noticed Blue Chip Stamps, it was 1968 and it was common for housewives to collect trading stamps — which functioned much like coupons — when they were at grocery stores and gas stations.

But with the rise of the women's liberation movement in the 1970s, trading stamps became passé — unpleasant tokens of a less liberal time.

Much like Berkshire Hathaway, Blue Chip was now on life support; it was only alive because Buffett and Munger were buying winning stocks under the company's name.

So, to help Blue Chip, Munger bought 8 percent of Wesco, an undervalued savings and loan company.

Buffett liked Wesco, too — but so did Santa Barbara Financial Company. In fact, SBFC wanted to merge with Wesco.

But Buffett saw SBFC as an overvalued company that would only be bad news for Wesco. So he flew to California to talk with Betty Caper Peters, the surviving member of Wesco's founding family, and convinced her to call off the merger. She did, but the decision sent Wesco's stock plummeting from $18 to $11 per share.

Buffett and Munger felt bad about this, so they offered to buy their majority stake at $17 per share.

But it didn't end there: Santa Barbara Financial filed a complaint with the Security Exchange Committee (SEC) claiming Buffett and Munger purposely overpaid Wesco in order to ruin the planned merger.

1974 was a tense year. The SEC launched an investigation into Buffett and Munger's tangled web of over 30 companies. This included five parent companies, such as Berkshire Hathaway and Blue Chip, that each owned five or more other companies, which in turn owned other companies and so on.

They weren't hiding anything, but the complicated structure made the SEC highly suspicious.

Buffett was extremely anxious. He knew that even being named in a finding of wrongdoing could ruin a reputation forever.

Thankfully, in the end, the SEC only issued one warning for a disclosure violation in Blue Chip's past — and no individuals were named.

### 12. Buffett also faced challenges with his marriage and legal battle between two newspapers. 

The increasingly close relationship between Warren Buffett and the _Washington Post_ 's publisher, Kay Graham, initially upset Susie. But she eventually began a romance with her tennis instructor, and wrote a personal letter to Kay, telling her that she was free to have her own relationship with Buffett.

However, in 1977, with the children out of the house, Susie decided it was time for a radical change and made arrangements to move to San Francisco.

In many ways, Buffett never grew up. Though nearing fifty, he was still a messy man who loved cheeseburgers and ice cream. He also remained far more devoted to his business life than to his family.

Despite all this, Susie still loved him and wanted to make sure he was taken care of.

So when she left, she hired Astrid, a young woman Susie knew from a nightclub, to cook for and look after Buffett.

Buffett was shocked by Susie's departure, but after many tearful phone conversations, he finally understood that she needed a life of her own.

Astrid eventually moved in with Buffett, a development that surprised both Susie and Kay, as well as Howie and Susie, Jr.

Then, in the middle of all this, a nasty legal battle began that would take years for Buffett to resolve.

In the late 1970s, Buffett and Munger had added another newspaper to their collection: the _Buffalo Evening News_. Part of their plan was to add a weekend edition to the paper which they would introduce by offering the first five issues for free, followed by a discount price.

But their competition in the area, Buffalo's _Courier-Express_, filed a lawsuit, claiming that this offer constituted unfair practice.

The judge stunned Buffett by ruling in favor of _Courier-Express_. In his decision, the judge said it was unlawful to sell free newspapers, and if the public wanted to subscribe to the weekend edition they would have to renew their order every week.

Buffett naturally appealed this harsh decision, and, in 1981, he finally won — but by then the paper had lost millions of dollars.

### 13. Being a loyal friend brought Buffet to Salomon Brothers, where he faced one of his most trying tests. 

Another company that Buffett is now closely associated with is GEICO, an auto insurance company that Buffett first noticed back in college. It was actually one of the first stocks he recommended to clients during his brief time at his father's old firm, but it wasn't until the 1970s, when the company was in crisis, that he became closely involved.

In 1976, Buffett joined GEICO's board to help it out of bankruptcy by revitalizing its management.

At that time, an executive named John Gutfreund, who worked at the Wall Street trading house Salomon Brothers, helped Buffett raise funds to get GEICO back on its feet.

Buffett was grateful for Gutfreund's help and he tried to repay that favor when Salomon Brothers faced some problems of its own.

In the early 1980s, hostile takeovers become a standard way of doing business on Wall Street; junk bonds were used to make brokers rich and everyone was racking up debt by making deals based on credit.

Buffett, who always paid with cash, hated these practices, and he didn't like the brokers or analysts indulging in them, either. But when Gutfreund asked for his help in 1986, Buffett agreed to join the board of Salomon Brothers.

Buffett had such a reputation for being associated with stable and reliable companies that having him on your board was a clear sign to everyone that the company was in good hands and not vulnerable to a takeover.

However, Buffett never suspected the trouble that lay ahead.

In 1991, an employee named Paul Mozer was caught in a massive scandal. He'd broken federal laws multiple times by illegally bidding in government auctions.

Making matters worse, management was made aware of this before the news spread but hadn't taken proper action.

During the fallout, Buffett was made interim-CEO and he put together new leadership and reforms. Crucially, he used his contacts to plead his case and successfully prevented the Treasury Department from barring Salomon Brothers from future auctions.

### 14. Buffett’s success has continued without technology investments, despite an eye-opening friendship with Bill Gates. 

Warren Buffett's reputation took a downward turn in the 1990s. As technology stocks became all the rage, Buffett remained utterly uninterested in the NASDAQ. People began saying he was behind the times, an irrelevant old man.

Remarkably, Buffett never concerned himself with these opinions, as he was doing just fine without technology stocks.

Actually, he was doing better than fine: between 1978 and 1991, his net worth jumped from $89 million to $3.8 billion and rising. And since becoming CEO of Berkshire Hathaway in 1986, that company's stock continued to soar, trading at $8,000 per share in 1991 and soon topping $10,000.

His career is proof that you can stay relevant and successful while steering clear of the NASDAQ. Buffett was one of the few people who recognized that the only thing tech stocks did reliably was make investors unhappy in the long run.

Nonetheless, he did make a small investment in one company.

Warren Buffett and Bill Gates agreed to meet at a Fourth of July party in 1991, even though both of them went in thinking that they would have nothing to talk about.

But that meeting led to a conversation that lasted the rest of the weekend, and they went on to remain close friends.

Afterward, Gates began attending Buffett's annual shareholder meetings, which eventually became so popular that scalpers could sell a ticket for $250, and, in the end, Buffett bought 100 shares of Microsoft.

Buffett and Gates also began meeting regularly for bridge games, along with Charlie Munger and Kay Graham.

Throughout the 1990s and the 2000s, these two were neck-and-neck for the title of richest person in the world. But it was their friendship that opened Buffett's eyes to his real place in the larger world.

After taking a trip to China with Gates, Buffett realized how lucky he was to be born in Omaha. He clearly saw that he'd had advantages that many people in the world did not, a realization that only reinforced his humble and thankful attitude toward life.

### 15. In the 2000s, Buffett faced personal losses that made him reevaluate what’s important. 

Buffett's predictions about internet companies being a disappointment to investors were already coming true in the early 2000s, and the publications that called him irrelevant in 1994 were now rebranding him as a prophet.

But this turnaround wasn't much comfort in 2001, when his dear friend and companion Kay Graham passed away.

Their 30-year relationship had been extremely close, and he was devastated for weeks after her death.

Then, two months later, on September 11, 2001, things only became worse.

Buffett took what he learned from both of these events — that we're living in uncertain times — and began investing in companies that offered some sense of certainty. This is what drew Buffett to businesses like Fruit of the Loom and companies that made farm equipment and children's clothes.

However, another period of reevaluation was just around the corner.

In 2003, Susie was diagnosed with stage 3 oral cancer.

Even though they were no longer living together, Susie and Buffett remained close. And though he was often reduced to tears, he recognized how important it was to take this time to care for Susie.

Susie passed away in 2004. Buffett was heartbroken, and he spent days in bed, unable to talk to anyone. But when he finally emerged, he had gotten in better touch with his own feelings and had a desire to be closer to his children.

He now believed he'd figured out the secret to life: "...to be loved by as many people as possible among those you want to have love you."

He also knew what he wanted to do with all his money. He gave 85 percent of Berkshire Hathaway, which was worth $36 billion at the time, to the Bill & Melinda Gates Foundation and he divided another six million dollars between Susie's charitable foundation and the other foundations set up for his kids.

### 16. Final summary 

The key message in this book:

**For much of Warren Buffett's life, he had a one-track mind** — **rolling his snowball. This meant constantly investing and reinvesting the earnings. His relatively simple but thorough method for picking stocks to invest in had nothing to do with business trends or technology. Though he could calculate a company's monetary value quite quickly, he found success by paying attention to the human element of a business.**

Actionable advice:

**Use Buffett's "20 Punches" approach to investing.**

Imagine you have a card that gives you only 20 chances to invest in your lifetime. Each time you make an investment, someone punches a hole in your card and you lose a future investment opportunity. If you use this philosophy, you'll be much more diligent about the investments you do make.

**What to read next:** **_The Intelligent Investor_** **,** **by Benjamin Graham**

Warren Buffett may be one of the greatest investors America has ever seen, but he's not the only American giant in the investment pantheon. Up there with him is the renowned investor and economist Benjamin Graham, who not only survived the 1929 financial crash, but flourished in spite of it.

To benefit from his investment wisdom — and to find out who Mr. Market is and why he's so unpredictable and moody — get the blinks to _The Intelligent Investor._

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alice Schroeder

Alice Schroeder began her career as an insurance analyst before taking Warren Buffett's suggestion and becoming a full-time writer. She is currently a columnist for _Bloomberg News_. _The Snowball_ was named one of the ten best books of 2008 by _Time_ magazine.

