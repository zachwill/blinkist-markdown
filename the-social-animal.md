---
id: 552be2676466340007b10000
slug: the-social-animal-en
published_date: 2015-04-16T00:00:00.000+00:00
author: David Brooks
title: The Social Animal
subtitle: The Hidden Sources of Love, Character, and Achievement
main_color: BD3C3B
text_color: 9C211F
---

# The Social Animal

_The Hidden Sources of Love, Character, and Achievement_

**David Brooks**

_The Social Animal_ (2011) reviews a trove of scientific evidence explaining what actually determines human behavior. Our iceberg-like unconscious mind helps us navigate the world with split-second judgment, while certain inborn traits drive us to be successful in life. You're both a lot — and a lot less — smarter than you think!

---
### 1. What’s in it for me? Find out why you have less control of your behavior than you think. 

Next time you go to a supermarket, try to listen carefully as you're walking through the aisles. Is there a faint jungle sound playing in the background while you're in the fruit section? The soft rush of rolling waves at the fish counter? Research has shown that these kinds of sounds increase customer experience and make customers slow down, which in turn makes them buy more.

Does this sound strange?

These blinks will show you that you're far less in control of your behavior than you think. In fact, research has shown that everything from the way you walk and the wine you choose, to how you look back at your life, are all controlled — in often surprising ways — by forces of which you are unaware.

In these blinks you'll learn

  * why the love of your life is probably closer than you think;

  * why judges should eat more often; and

  * what it takes to resist a marshmallow.

### 2. We subconsciously choose mates who resemble us and conform to certain physical criteria. 

Ask any 15-year-old boy and he'll give you a laundry list of traits his dream girl would possess. But chances are, that kid will grow up and fall in love with someone who doesn't perfectly meet his criteria. So, what actually enamours our teen? And why do any of us fall in love?

It turns out we're subconsciously attracted to people who resemble us and have similar facial features. For instance, if our noses have a similar breadth and there's a comparable amount of distance between our eyes.

Furthermore, we're drawn to people who share our educational, economic and ethnic background. Consider the fact that a 1950s study found that more than half of the couples who applied for marriage in Columbus, Ohio lived no more than 16 blocks apart when they started to date. And what's more, 37 percent of the couples lived within five blocks of each other!

So, generally speaking, we're more likely to fall in love with people who share our attitudes, expectations and interests. However, we also gravitate toward people who have certain generic physical features.

For instance, it won't surprise you that on average, heterosexual women prefer tall men with symmetrical facial features who are slightly older and stronger than the women themselves. But did you know that researchers have also established that women are more sexually attracted to men with large pupils?

And what do heterosexual men prefer in women? According to a massive study conducted across the globe, men unequivocally prefer women with a hip-to-waist ratio of roughly 0.7.

Although hip-to-waist ratio is overwhelmingly the most important factor, men also prefer full lips, clear skin and lustrous hair.

### 3. Context determines our choices. 

We all like to think that we're in control of our behavior. Unfortunately, research shows that's not really the case. Rather, even the smallest factors can have a massive influence in surprising ways.

It turns out that even hearing a few words can set off a whole string of associations, altering our behavior. For instance, one study had people read a series of words vaguely associated with being elderly: "bingo," "Florida," and "ancient." Then, when the test subjects left the room, researchers observed that they walked more slowly than when they came in.

In the same study, other subjects were asked to read words related to aggressiveness, like "rude" and "intrude." Accordingly, the subjects started to interrupt others more frequently.

In a similar vein, the way we judge something greatly depends on how it's presented to us initially. For example, a $30 bottle of wine will appear more expensive when it's presented beside cheaper options. And yet that $30 bottle of wine will seem cheap if it's surrounded by costlier goods, like $149 bottles of wine. And that's precisely the reason wine stores stock very expensive bottles, even though no one actually buys them.

These minor cues also play a major role when it comes to something like prognosis. Imagine a surgeon telling his patient that a procedure had an 85 percent success rate. Now imagine that he puts it differently and says it has a 15 percent failure rate. As you might guess, the patient is far more likely to choose the prognosis which focuses on the success rate.

Now, do you really think that if our decisions were wholly based on rational considerations, these minor cues would really have that much of an impact? Almost certainly not.

### 4. When it comes to decision making, emotions trump rational deliberation. 

Have you ever heard the saying, "Justice is what the judge ate for breakfast?" Well, it turns out there's empirical proof.

In general, people view things differently depending on whether they're hungry or full. However, judges are supposed to surpass average humans when it comes to reasoning skills; we like to think that their decisions are always rational and objective, even though ours aren't.

When a psychologist at Ben Gurion University followed the work of an Israeli parole board, he found that even judges make decisions based on factors that are completely unrelated to the case at hand. For instance, judges show much more clemency after they've just eaten. More specifically, after meal breaks, judges grant two-thirds of all applications for parole, compared to an overall average of just one-third. In fact, judges' clemency actually declines the hungrier they get, plummeting right before meals.

Similarly, the way we evaluate our own lives depends on the weather as much as it does on our experiences. For example, imagine someone who had a traumatic childhood. Of course, sitting in the park on a sunny day doesn't change what happened to them — and yet they might perceive it differently, perhaps as character building rather than devastating.

This also has an empirical basis: When a team of researchers asked people about their overall happiness, the answers largely depended on the weather. If the weather was nice, people tended to describe their lives in positive terms, but when the skies turned gray, life didn't seem all that sunny.

So as you can see, the way we perceive a situation can vary wildly from day to day. But are these just hiccups, slight deviations from our otherwise rational brains? As ethical beings, don't humans exercise reason constantly? Keep reading to find out!

### 5. There are two conflicting theories concerning moral judgment: Rationalism and intuitionism. 

What's the basis of our morality? Is it conscious thought or moral intuition? Philosophers have debated this question for hundreds of years.

And according to one view, moral judgment is based on deliberate reasoning. This is called moral rationalism and its proponents claim that we make moral decisions logically by applying universal principles to a given situation.

These universal principles can be anything from "don't kill other people" to "always act in a way that maximizes the welfare of your community."

According to moral rationalism, there's typically a power struggle between our primitive self-interested instincts on the one hand, and our moral principles on the other. And if we want to act morally, we have to use willpower to subdue the selfish impulses lurking in our subconscious.

For instance, imagine you're having some marital problems when you meet an attractive person who asks you out. Although your instincts may urge you to say yes, your moral principles are there to prevent you from cheating on your partner.

Let's move on to the opposing view, moral intuitionism, which claims that our moral judgment is based on intuition, not reason. Intuitionists stress the point that not all of our impulses and intuitions are wholly selfish, and that humans even have an inner moral sense guiding them. We experience this moral sense as something like compassion — that is, a sense of fairness.

According to intuitionism, we don't necessarily experience a power struggle between our feelings and our reason, but rather between our egoistic urges and our moral sense.

So, for example, if you want to go out on a date with that nice person from the gym, you might feel very guilty, even before you consciously consult your moral principles, which remind you that you're in a relationship.

### 6. When it comes to moral decision making, intuition is more important than rationality. 

So, which view is right: moral rationalism or intuitionism? Well, while it's difficult to say whether sound reasoning leads to moral behavior, it's clear that emotions and intuition play a major role in moral decision making.

Take psychopaths. Although most people experience an intense visceral response when they see a child being beaten — their blood pressure surges and their palms get sweaty — psychopaths keep their cool.

It's curious, because psychopaths are just as intelligent as the rest of us, so you would expect them to have the same moral standards, because they're using the same reasoning skills. But that's actually not the case: Psychopaths typically have low moral standards and are disproportionately more likely to cause great suffering to other people for their own gain. _Their_ intuition results in very different reasoning.

This example shows how conscious deliberation alone doesn't necessarily lead to moral behavior. In fact, sometimes our moral judgment completely precedes conscious deliberation. And in some cases, seemingly rational moral judgments are actually produced via rapid intuitive evaluations.

For instance, when researchers at the Max Planck Institute for Psycholinguistics in Holland read moral statements concerning sensitive topics like euthanasia or abortion, test subjects reacted with evaluative feelings no more than 200 to 250 milliseconds after they heard the statement. In other words, they had formulated a moral stance before they had time to consciously reason through the decision.

And in certain cases, conscious reasoning might not factor into moral decision making at all. Babies, for example, have been shown to automatically favor moral behavior.

In one experiment, six-month-old babies watched a film that showed a puppet struggling to climb a hill. A second puppet was trying to help the first while a third was trying to hinder the first puppet's progress. After watching the film, the babies had the choice to play with one of the puppets. And guess what? They typically chose the helpful second puppet.

### 7. Rational choice is impossible without emotions. 

Mr. Spock's seemingly infinite capacity for rational thinking might seem desirable. After all, if you didn't feel so many distracting emotions, you'd be a superb decision maker, right?

Actually, no. As it happens, people without emotions typically don't make super-rational choices. Instead, they either make foolish choices or none at all.

For instance, certain medical conditions (caused by something like a tumor or a stroke) can wipe out people's emotions but leave their intelligence intact. The neurologist Antonio Damasio famously researched these patients and found out that they aren't just incapable of making good decisions, they also find it difficult to make any decisions at all. Even mundane decisions, like deciding where to have lunch, overwhelmed these people.

And what's more, when they do decide, their choices are consistently bad, leading them to ruin themselves with financial investments or marry an inappropriate partner they don't care about at all.

One reason emotions are so crucial for decision making is that they allow us to evaluate the subjective value of different options, which is a condition for rational choice. In other words, our emotions allow us to feel what kind of impact a decision will have on us.

For instance, what happens when you imagine diving from a high cliff? You probably feel fearful, queasy or even panicky. Well, that's the way your body gives you feedback about risky decisions. We interpret this kind of feedback (like a sinking feeling in the pit of your stomach) as an emotion, and this reaction creates a major incentive to choose or avoid certain decisions.

And that's exactly why Damasio's patients had so much trouble making decisions: Because they didn't experience any emotional feedback, they simply didn't have the incentives to choose one thing over another. Accordingly, a life-threatening jump didn't feel any riskier than deciding to stroll through a park.

> _"Emotion assigns value to things and reason can only make choices on the basis of those valuations."_

### 8. We’re social animals, born to connect with each other. 

As humans, we're born to connect with each other. In fact, in many ways, we couldn't even exist without each other.

Of course this is true on a survival level, but it also applies to matters of self-identity. After all, as children, our personalities emerge from the relationship we have with our parents.

Consider the way a child's sense of self develops through continuous interaction with others, when it finds itself mirrored in the caregivers' behavior. For example, parents typically laugh when their baby laughs, look at the baby when the baby looks at them, imitate the sounds the baby makes — and vice versa.

This kind of mirroring is hugely important to the developmental process because our brains have evolved to catch social cues, respond to them, and look for feedback from the other person.

In fact, when we observe someone take a sip of water or smile, our brains simulate the same action. A specialized set of neurons, called _mirror neurons_, are responsible for this process: When they fire up, they create the exact same pattern that would appear if we actually took this action ourselves.

For instance, when you see someone smile, you feel happier because your mirror neurons simulate that smile in your own mind instantaneously. And this process truly does happen at lightning speed: Studies have shown that it takes an average college student just 21 milliseconds to synchronize her movements to those of her friends.

And this example gets at another fact about human social psychology, which is that we have a strong and automatic tendency to conform to group norms.

This was proven in a famous experiment: Test subjects were shown a set of three lines of obviously different lengths. However, since the subjects were surrounded by a group of people who had been secretly instructed to insist that the lines were the same length, 70 percent of subjects conformed to the group and denied the obvious fact that the lines were different!

> _"If the outer mind highlights the power of the individual, the inner mind highlights the power of relationships."_

### 9. You can’t overestimate the unconscious mind, which absorbs and processes massive amounts of information in seconds. 

Sigmund Freud, the father of psychoanalysis, once compared the mind to an iceberg. We can only glimpse a tenth of what's happening in the brain — that's the conscious mind or the iceberg's tip — while the rest of it's immersed in water, hidden from sight.

But just because it's hidden from sight doesn't make it irrelevant. In fact, the unconscious mind can handle _enormous_ amounts of data — way more than our conscious mind — and we rely on all this information to make quick decisions and perform complex tasks.

To really understand the scope of this, consider the fact that at any given moment, our mind can process 11 million bits of information, but that we can only be conscious of 40 of those! And even at its best, the conscious mind's processing capacity is 200,000 times weaker than that of the unconscious mind.

And this truly is crucial information. For instance, driving a car would be close to impossible if our unconscious mind couldn't handle so many of the necessary motor and perceptual processes. After all, thanks to its enormous processing capacity, our unconscious makes decisions in milliseconds, whereas our conscious mind takes _much_ longer.

It follows that our unconscious is what's responsible for certain remarkable feats. As we've discussed, this part of our brain can absorb and process large amounts of data instantaneously, organizing and interpreting it in milliseconds. So at any given moment, we're perceiving and interpreting a ton of things we're not even aware of.

That's why certain people can make very accurate predictions without being able to explain their reasoning. For instance, many chicken farms employ experts as chicken sexers. These people, who typically have years of experience, can look at a one-day-old chick and diagnose its sex in an instant, with better than 99 percent accuracy. And yet, chicken sexers have no idea how they figure it out!

So ultimately, humans aren't as rational as we may like to think. But that's not a problem, because the non-rational, partly unconscious processes of our mind can do extraordinary things to help us navigate the complex world and make good choices. And yet, if rationality is so overestimated, how does intelligence factor in? And what kinds of abilities and traits actually determine success?

### 10. Conventional measures of intelligence aren’t reliable predictors of success. 

Our society values intelligence and most of us probably think that being smart can have a huge impact on our future success in life. And on average, people with high IQs do much better in school and similar environments. But does exceptional brainpower lead to exceptional achievements in other areas as well?

Well, the first thing to understand is that having a high IQ doesn't mean you'll have a happy and successful personal life. Because clearly, when it comes to relationships, other abilities — like empathy, willpower, agreeability — trump abstract smarts.

Consequently, when you control for other factors, highly intelligent people don't have better marriages or relationships. They're also not superior parents.

In fact, according to the _Cambridge Handbook of Intelligence_, researchers conclude that IQ contributes to, at best, no more than 20 percent of life success.

And although rates like that are, of course, difficult to estimate, it's definitely clear that high intelligence doesn't necessarily lead to superior job performance or material wealth: One study found that only four percent of the variance in job performance can be predicted by IQ.

Similarly, although in some professions (for example, in academia), having an IQ of 120 is an advantage, beyond that threshold, additional IQ points don't translate into greater success or ability. In other words, a chemist with an IQ of 140 won't necessarily perform better than a colleague with an IQ of 120.

Another influential study followed the career trajectory of a group of highly intelligent students who all tested in the uppermost percentile for their age groups. And while these youngsters did just fine in life — becoming lawyers, architects and executives — none of them went on to win major awards or make pioneering scientific discoveries.

But on the other hand, two boys who were excluded from the study because their IQs weren't high enough — William Shockley and Luis Alvarez — went on to become extremely successful scientists and actually ended up winning a Nobel Prize.

### 11. Sensitivity and self-control can have a massive impact on success. 

If intelligence isn't a good measure of future success, which traits can determine whether a child does well later in life?

Sensitivity is an important factor in this respect. And from birth, some children are more sensitive than others. This was established in a study of how 500 children responded to novel stimuli. Researchers found that 20 percent of all newborns startle more easily than others: That is, when confronted with unfamiliar stimuli, their heart rates shot up and they started to cry vigorously.

Another 40 percent of these babies tended to the other extreme: No matter what was dangled in front of them, these kids were unfazed.

Under the right circumstances, the sensitive children fared far better than the others. But in hostile environments, these babies grew up to become vulnerable adults, prone to anxiety and stress-related illness.

On the other hand, less sensitive children tend to become bold and outgoing irrespective of the environment.

Self-control is another factor which influences later success, both at school and beyond. In one famous experiment, a researcher challenged four-year-olds to resist eating a marshmallow which was put right in front of them.

If the children could spend 20 minutes alone in a room without eating the treat, they would earn a second marshmallow (and be allowed to eat the first).

Incredibly, the study found that this simple test of early willpower could predict whether the children would succeed later on in life. Kids who managed to wait the full 20 minutes performed better in school; even 30 years later, this group had high college-completion rates and higher incomes.

Meanwhile, their more impulsive peers had higher incarceration rates and more drug- and alcohol-related problems.

And yet, the study found that self-control was actually a malleable trait. For instance, when the researchers advised children to pretend that they weren't actually looking at a real marshmallow, but rather at something non-delicious, like a fluffy cloud, most managed to resist the temptation.

> _"There is no question self-control is one of the essential ingredients of a fulfilling life."_

### 12. Final summary 

The key message in this book:

**Rationality alone doesn't determine our behavior. Most of the time, it's actually our unconscious mind that informs our decision making. Additionally, since people are hardwired to connect with each other, we aren't — as we like to imagine ourselves — wholly autonomous subjects. Rather, the context and the people around us have a massive impact on our behavior.**

**Suggested further reading:** ** _Thinking,_** **_Fast_** **_and_** **_Slow_** **by Daniel Kahneman**

Daniel Kahneman's _Thinking,_ _Fast_ _and_ _Slow_ — a recapitulation of the decades of research that led to his winning the Nobel Prize — explains his contributions to our current understanding of psychology and behavioral economics. Over the years, Kahneman and his colleagues, whose work the book discusses at length, have significantly contributed to a new understanding of the human mind. We now have a better understanding of how decisions are made, why certain judgment errors are so common and how we can improve ourselves.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David Brooks

David Brooks is an American journalist and political commentator best known as a _New York Times_ op-ed columnist. He has also contributed to the _New Yorker_, _Forbes_, _The Times Literary Supplement_ and many other esteemed publications.

