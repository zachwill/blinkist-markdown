---
id: 552bd9e86466340007380000
slug: the-soft-edge-en
published_date: 2015-04-14T00:00:00.000+00:00
author: Rich Karlgaard
title: The Soft Edge
subtitle: Where Great Companies Find Lasting Success
main_color: FDC133
text_color: B0800D
---

# The Soft Edge

_Where Great Companies Find Lasting Success_

**Rich Karlgaard**

This book (2014) is about the "soft edge" — that is, the human side — of business. If you don't want to be left foundering in the wake of your competitors, your company needs something _more_ — more creativity and flexibility, more passion. The soft edge is the wellspring of such attributes. Your company's soft edge might seem elusive at first, but remember: finding it is absolutely critical for long-lasting success.

---
### 1. What’s in it for me? Discover the forgotten side of business. 

Anyone who has been to business school, or has studied business practice at any length will know that to be successful you need to spend a lot of time working out your strategy and developing the processes to execute it perfectly.

In the real world, however, there is more to developing a successful business than strategy and execution. You need to develop a relationship with people; without people, whether it be your employees or your customers, you cannot hope to succeed.

These blinks explain how successful companies, from Amazon to Northwestern Insurance, have succeeded because they managed to establish meaningful connections with all people.

In these blinks you'll discover

  * why there is no alternative to simple good taste;

  * why only a few people are involved in the making of a Porsche; and

  * why you should never need to buy more than two pizzas for your team.

### 2. Successful companies are founded on three key elements: a strategy, a hard edge and a soft edge. 

What do you think of when you think of successful businesses? Great leaders? Great management? Excellent products?

The foundation of all great businesses is their _strategy_. You can think of a business like an equilateral triangle. The strategy is the base: it's knowing what market you're in, who your customers are and what your competitors are doing. It supports the other two sides of the triangle.

The first of those two sides is the _hard edge_. The hard edge is how the business actually functions. You can't just have a good strategy — you've got to execute it deftly, as well. You can measure the success of the hard edge with raw figures that compute production speed, capital efficiency or logistics, to name a few.

Harder to measure is the second side, the _soft edge_. The soft edge is the deeper meaning that a company communicates to its customers and employees. It's that unquantifiable something that makes a company great.

Good businesses sit on a strong base and adhere to an effective hard-edge operation model. Great businesses, however, always do more than that, which is what gives them their soft edge.

So how do you measure your company's soft edge? Values, innovation, culture, creativity — you can't put these things on a graph and track whether they correlate with your investment returns. However, there is a model that'll help you cultivate and keep track of your company's developing soft edge.

Imagine that your soft edge is a platform supported by five pillars. Now label these pillars: there's the _trust_ you earn from your employees and customers, the _smarts_ that facilitate learning and innovation, the _teams_ that collaborate and share ideas, the _taste_ that gives your product depth and the _story_ that gives your brand its purpose.

Now let's take a look at the five pillars that support the soft edge.

### 3. Trust is a fundamental part of the soft edge. 

You know that feeling you get with your closest friends? That feeling that everything's going to be okay? That's _trust_. Imagine if you felt that way at work.

Trust is at the heart of the soft edge. It's equally important to establish both _external trust_ with your customers and _internal trust_ with your staff.

It's absolutely vital that your customers trust you. In fact, the latest Edelman Trust Barometer — an international study of trust in businesses and other organizations — shows that trust affects corporate reputation more than the quality of products or services.

You earn trust with your customers by showing them you have a secure business. Northwestern Mutual, for example, has built its company on trust. It's the largest insurance provider in the US and it's spent 157 years cultivating trust with its customers.

The ratio of Northwestern's insurance contracts to its assets is 7:1 — $1.5 trillion in contracts and $215 billion in assets. These figures clearly illustrate both how sound and prudent their business is and that customers are unafraid to entrust money to them. Obviously, there's much to be gained from making trustworthiness one of your company's core values.

Creating internal trust begins when you're honest and open with your employees. The network storage company NetApp has been quite successful with this. NetApp is regularly on various lists of the best places to work, including _Forbes_ 's list of the most innovative companies.

Even when the company went through a rough period and had to cut staff, their reputation for honesty and openness endured. Why? They treated their staff with utter respect. The executive management team even traveled to all the affected offices to personally deliver the bad news. Because they trusted their company, the remaining employees, though conscious that they too might get laid off, continued working hard.

> _"Trust is the lubrication that makes it possible for organizations to work." — Warren Bennis_

### 4. A company needs smarts to keep up with the latest ideas and technologies. 

You know those companies who always seem a step ahead of their competitors? What makes them so smart? Do they have a bunch of speed-reading champion employees? From the outside, it's hard to tell what makes a business smart, but in fact smart businesses are defined by something definite: hard work and the constant quest for improvement.

A strong company always has to be ready to change. It needs to work hard and be prepared to make mistakes. That's what makes a company _smart_.

The best way to up your company's braininess is to learn from your mistakes. David Chang, a famous chef and the owner of the Momofuku restaurant group, knows this well. He trained in Japan, where the concepts of _kaizen_ (continuous improvement) and _hansei_ (acknowledgement of your own mistakes) are highly valued.

Chang honors this in his restaurants by giving his staff the freedom to try things out, letting them make mistakes and learn in the process. That's how he improves the dishes he serves.

So smart businesses constantly strive to learn and make improvements. They do this not only by learning from their mistakes but by learning from other companies, too — even those in other industries.

Healthcare giant Mayo Clinic, for instance, uses this kind of _lateral thinking_ to train their employees: They sent their staff to train at two of the world's largest companies in the hospitality industry. They wanted employees to learn to treat their patients as customers, and to treat them with the utmost respect and care. The Mayo Clinic employees learned skills not normally taught in the healthcare industry, like how to put their patients at ease with little more than a greeting.

So think outside the box, learn from your competitors and seek inspiration from other fields. And remember that, in business, having _smarts_ is an ongoing process.

> _"Smarts is about the importance of hard work, of perseverance and resilience."_

### 5. Teams facilitate learning, productivity and innovation most effectively when they’re small and diverse. 

Of course, smarts aren't the only key to success. Your business has to have the right kind of organization, too.

Humans are hardwired to function and excel in groups such as families, tribes and communities. Businesses should emulate the kinds of groups we instinctively work well in.

This means teams should be small. Small teams ensure efficiency and effective communication.

Jeff Bezos, the CEO of Amazon, once said that a development team should be small enough that two pizzas will feed them all. That's about eight to twelve people. In a group that small, everyone has the chance to get their hands dirty by trying out new ideas before discussing the results with the whole team.

Porsche also implemented this strategy. After they created mini-teams focused on improving products and their development processes, their production quadrupled and their profitability increased by 19%.

Another example is SAP, the German software company, which took inspiration from Porsche and also scaled down their development teams to between eight and twelve people. Their development time was reduced by half; on average, it dropped from 14.8 months to 7.8 months.

The most effective teams consist of people with a wide range of backgrounds, beliefs and skills. Diversity ensures a multiplicity of perspectives on whatever project is being worked on.

Nest Labs, a company that produces smart thermostats, believes in diversity within teams. They make sure to put engineers, marketers, designers and other workers together to get a multifaceted perspective on any products they're developing.

Tony Fadell, Nest Labs's CEO, says the reason for this is simple. Their customers are diverse, so their teams should be as well.

> _"When we work together, we make each other better."_

### 6. Taste is a combination of function, form and meaning – it gives customers an emotional connection to a product or brand. 

What exactly gives a great smartphone its greatness? How it looks? How it functions? How it feels in your hand? Excellent design isn't merely a matter of solid interface — there's always something more.

Top-tier products need _taste_. Taste should seem innate, giving the product an aura of intelligence and intrigue; it's that quality that's hard to put a finger on, but makes the customer feel excited and smart. Although taste may seem like an abstract concept, every company needs to push for it.

With taste in mind, Tony Fadell tries to give his customers a Nest Labs experience even if they're doing something as simple as buying a thermostat.

Nest Lab thermostats have a clean design without any buttons and they're controlled remotely through WiFi. They come in bamboo boxes that symbolize Nest Lab's commitment to green production and waste reduction. They also come with a special screwdriver and screws that allow customers to install their device wherever they think it looks best.

Research in aesthetics has shown that people prefer familiar designs and objects to new ones. We feel more affinity for designs we recognize because they evoke all the emotions and memories we might associate with similar objects from our past. So when you create your brand, try to link it with something people already love.

Jony Ive, the famous Apple designer, was inspired by Dieter Rams, a designer for Braun. In turn, Rams was inspired by the Bauhaus school of design. So part of what makes Apple products great is that their design traces a tradition of minimalism.

Howard Schultz, the founder of Starbucks, also understood the value of taste. He didn't just build a coffee company — he reinvented the way Americans drink coffee by bringing Italian coffee culture to the US. He infused his cafés with ambience, complete with baristas and Italian words for coffee sizes.

> _"Taste is the elusive sweet spot between data truth and human truth." — Robert Egger_

### 7. A company gets its purpose from its story. 

Now let's look at the final pillar supporting a company's soft edge. Just think of the first Apple products, built in that garage in Palo Alto. _Stories_ like that one play a large role in shaping our perception of successful companies; it's often what brings us to view them as great.

A company's story helps us understand it. It gives the brand its purpose. It lets us know where it came from, what it is today and where it's going.

Northwestern Mutual, for instance, helps build their story by hosting, in Milwaukee's BMO Harris Bradley Center, an annual two-day meeting attended by more than 10,000 financial representatives. The representatives share their experiences of overcoming problems and finding success. The meeting is part of the 157-years-long story of Northwestern Mutual.

Another key part of Northwestern's story is their _10-3-1_ sales formula: Ten calls should lead to three meetings that lead to one sale.

When Northwestern is hiring, candidates must demonstrate their grit by trying to land sales with the 10-3-1 formula. This increases their recruitment costs fivefold, but it's worth it to maintain the story.

The best stories are always simple, so don't make yours too complicated. People always respond best to simple, straightforward messages; whether it's visual arts, writing or your brand's story, keep it simple.

You should also avoid making predictions about your story, because you'll look foolish if they don't come true. Be open to the unpredictable.

Like teams, taste, smarts and trust, your story is also easy to forget about in today's fast-paced business environment. But as we'll see in the last blink, these pillars of the soft edge can set you apart from your competitors.

> _"The Story is the company and the company is the Story; they can't be divorced from each other."_

### 8. The soft edge is what separates you from your competitors. 

It's important not to think of the soft edge as merely another part of business, like marketing or logistics. The soft edge is much more than that. Remember, it's the human side of business.

These days, companies have an unprecedented amount of data to define their hard edge. It's the soft edge — cultivated by human ingenuity and innovation — that sets some companies apart from the rest.

The Swiss team Sauber, which competes in Formula One _,_ illustrates the significance of the soft edge. Sauber uses systems that gather a great deal of information to help them improve their cars. At the end of the day, however, it's the human driver who has to innovate and perform at 200 MPH. A successful race is a combination of human acumen and helpful data.

If a company wants to continuously innovate, endure rough periods and enjoy lasting success they need to have a business model that covers all three edges: strategy, the hard edge and the soft edge. The soft edge is the easiest to neglect; many businesses don't even bother with it at all.

Wall Street, for instance, is driven by the hard edge. It's all about speed, execution and short-term capital efficiency. Many CEOs, badgered by investors and barraged with stock market information, forget about the values of their employees and customers. This hard-edge approach certainly won't distinguish them from their competitors.

In contrast, companies like Northwestern and Apple stand out because of how much they care for their soft edge. This doesn't only bring them popularity and strengthen their relationships with customers — it brings financial wins, too.

So don't neglect the soft edge. It's too valuable and important to be overlooked, though many companies do exactly that. Just keep in mind: A company with a soft edge is a company that stands out.

### 9. Final summary 

The key message in this book:

**The soft edge is what makes great companies stand out in today's overcrowded business world. So give yours the time it deserves. Build** ** _trust_** **with your customers and employees, use** ** _smarts_** **to keep up and put people in** ** _teams_** **that will be effective. Push your company to have great** ** _taste_** **and craft an engaging** ** _story_** **for it. Your business won't just be more successful — it'll be more meaningful, too.**

Actionable advice:

**Make teams you can feed with two pizzas.**

This rule of thumb has worked for Amazon and Porsche — it'll work for you, too. Teams are at their most effective when they're diverse and small enough for all the members to be fully engaged. That means that two pizzas should be enough to feed the entire team.

**Suggested further reading:** ** _Built to Last_** **by Jim Collins**

_Built to Last_ examines 18 extraordinary and venerable companies to discover what has made them prosper for decades, in some cases for nearly two centuries. This groundbreaking study reveals the simple but inspiring differences that set these visionary companies apart from their less successful competitors.

_Built to Last_ is meant for every level of every organization, from CEOs to regular employees, and from Fortune 500 companies to start-ups and charitable foundations. The timeless advice uncovered in this book will help readers discover the importance of adhering to a core ideology while relentlessly stimulating progress.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rich Karlgaard

Rich Karlgaard is the publisher of _Forbes_, where he also writes the column Innovation Rules _._ His 2004 book _Life 2.0: How People Across America are Transforming Their Lives by Finding the Where of Their Happiness_ was on the _Wall Street Journal_ 's best seller list.

©Rich Karlgaard: The Soft Edge copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

