---
id: 53032c706431310008100000
slug: the-soul-of-a-new-machine-en
published_date: 2014-02-19T08:07:38.000+00:00
author: Tracy Kidder
title: The Soul of a New Machine
subtitle: None
main_color: BE268F
text_color: A6217D
---

# The Soul of a New Machine

_None_

**Tracy Kidder**

_The Soul of a New Machine_ (1981) is a Pulitzer Prize-winning book about one team at the computer company Data General who was determined to put its company back on the map by producing a new superminicomputer. With very few resources and under immense pressure, the team pulls together to produce a completely new kind of computer.

---
### 1. What’s in it for me: 

In the late 1970s, there was a lot of money to be made in the minicomputer industry.

The author, Tracy Kidder got the chance to follow a team of engineers as they attempted to build a next-generation computer, even when the very company they worked for was reluctant to even let them try.

_Soul of a New Machine_ explains how the vice president in charge of the team, Tom West, managed to motivate them to work nights and weekends for no extra pay, all because they felt they "wanted to get the machine out of the door." The book chronicles the dawn of the high tech industry, when engineers felt that the thrill of working on the new generation of machines was the highest motivation possible.

It also explains the intricacies of designing and debugging such a machine, where you have to control the flow of electric current through thousands of chips in timeframes of a couple of hundred- billionths of a second, so that the machine performs tasks consistently without fault or error.

### 2. Data General was a fast-growing, no-frills computer company under pressure to design a new kind of minicomputer. 

In the late 1970s, a few so-called "minicomputer" companies were in fierce competition with one another. (Back then, "minicomputer" meant a device costing under $25,000, that could just aboutfit into an elevator.) The biggest player was a company called DEC, with a circa 85% share of the market.

One company in particular, based in Westborough, Massachusetts, was notorious for its brutally competitive and stingy ways. That company was Data General. It had been founded about ten years earlier by the current president Ed De Castro, who had jumped ship from competitor DEC.

From the outset, Data General was focused strictly on profits, with little ostentation: even their headquarters was a bleak, concrete fort.

The company's practices were infamous in the industry; even competitors warned customers about these dangerous newcomers, which of course only generated more buzz for the company. There was even a rumor that Data General had torched a competitor's factory.

But Data General did indeed make money, generating 20% profits, year after year, until its revenue eventually grew to a whopping $508 million and the company found itself in 500th place on the Fortune 500.

It was around this time that competitors such as DEC were coming out with a new kind of computer — the "supermini" — and everyone wanted one. DEC's new VAX 11/780 machine was considered a breakthrough in this burgeoning industry, and Data General — learning that the market was set to be worth several billion dollars in just a few years — decided it needed to build an "answer" to the VAX. 

To this end, Tom West, an engineering Vice President at Data General, bought and took apart the VAX machine. After examining it closely, he decided Data General could do better.

### 3. Two separate teams began work on a competitor to the VAX: one flush with resources, the other unofficial and on the cheap. 

To pursue, among other things, a contender for the VAX, Data General built a new research and development center in North Carolina.

The North Carolina (NC) department had a huge team, lots of resources and it was supposed to be the primary gamble for the new supermini: A team of 50-people moved to NC to work on the supermini project, also known as the Fountainhead Project (FHP), luring away many of the best and most experienced engineers from Westborough.

Meanwhile, Tom West was left in charge of a team in "boring old" Westborough, where they were supposed to just churn out older computers, such as the Eclipse and the Nova. These computers were important for the company's bottom line, but as one engineer put it, "where's the fun in that?" After all, an exciting and completely new kind of machine was being built by FHP.

So instead, under the radar and with limited resources, West's team decided to build a competitor to FHP: "the Eagle."

Reluctantly, De Castro let them give it a try, but according to West it seemed like everyone was pretending their project didn't exist, as the company "already had it covered with FHP." 

So West's team worked in the basement of Westborough, in a cramped cubicle space that was stiflingly hot in summertime when the air conditioning would go out and the company would be unwilling to spend money to revive it.

Even while telling his own troops that the Eagle was going to blow FHP out of the water, Tom West deliberately kept a low profile toward the rest of the company. The feeling was that the Eagle would be absolutely essential to the company's success in the new age of superminis, but the Westborough team would essentially have to build it all on their own, with little or no outside support.

### 4. A motivated team was assembled by Tom West by getting people to “sign up” – meaning, to fully commit. 

Many engineers in Westborough didn't want to join the Eagle team, because they were convinced it would be a "kludge": a slow, buggy machine that would pale in comparison to the FHP.

Yet, Tom West gradually began to assemble his team.

His first step was to recruit lots of young graduates. They were poorly paid and overworked with no overtime pay, but the possibility of designing a completely new kind of machine that could contend with the VAX was so interesting that they were still very eager.

But West was taking a risk. No one believed that such inexperienced "kids" could design a top-of-the-line computer. West, though, reasoned that they were perfect because "they didn't know yet what was impossible."

To complement such green hires, West also recruited more senior members, sourced from the company by way of West's various ruses. 

For example, West wanted Steve Wallach to design the architecture of the machine but Wallach declined the invitation, as he didn't want to build something that would pale next to FHP. But West knew Wallach was desperate to produce a machine with his name on it, as in many cases he'd had to leave a project before a computer was completed. The FHP was just one example of a project being "snatched away" from him. 

So West presented the idea to Wallach as a way to exact revenge on North Carolina, convincing him that he'd finally get his own machine out into the world. Eventually, Wallach did come on board as architect.

Young or old, every team member underwent a mysterious initiation rite: "signing up." This involved agreeing to forsake _any_ obstacles to the project's success — including hobbies, and even family and friends.

In some cases, stock options were implied as possible rewards, but in actuality, the employees felt there was an inherent reward — and some pride — in getting a new machine out the door and into the world.

### 5. The Eagle team was motivated by fear, pressure and the possibility of “playing again.” 

The team in Westborough worked ungodly hours and under tremendous pressure to produce quality work while getting the machine to market as quickly as possible. But they weren't compensated for overtime, and though in some cases the possibility of stock options was implied, most said they were not doing it for the money.

Sohow did West motivate them? 

Through _fear_, partly. 

West believed in the "mushroom management" philosophy: "Keep 'em in the dark, feed 'em shit and watch them grow." 

This meant West acted as a gatekeeper and insulator to the rest of the company, isolating his team. At the same time, he gave team members individual responsibility for important parts of the machine, getting them to sign up for wildly unrealistic deadlines so that they were in a state of constant worry and anxiety.

Another motivator was the pressure he put on the team to get the product out into the marketplace. In this regard, West's motto was: "Not everything worth doing is worth doing well." In other words, it was fine to cut corners wherever possible. 

But by far the central motivation was the chance to "play another game of pinball."

Much like playing pinball, if the team worked hard at this and succeeded, the implication was that they'd get a chance to "play again": to remain in the project and to play an integral role in the design of subsequent generations of computers.

This worked so effectively with the Eagle team because it consisted mostly of young graduates enjoying their first real taste of responsibility, and older engineers who'd been spurned by FHP's move to North Carolina. Both were excited to be involved at the vanguard of next-generation computer production.

### 6. The young graduates formed two teams – the Hardy Boys and the Micro Kids – which built the “soul” of the new machine. 

Though Tom West was in charge of the Eagle project, most of the work was done by two teams composed of the young graduate hires.

The Hardy Boys were in charge of designing the actual hardware — the circuitry of the machine. This was a crucial role, because computers consisted of various parts which had to communicate with each other seamlessly so that the computer could perform the tasks demanded of it.

The Hardy Boys often battled over including certain hardware components and features in Eagle, but in the end it was West who decided what was worth including and what wasn't.

The Micro Kids, for their part, worked on the microcode for the thousands of chips contained in an Eagle computer.

Essentially, the microcode tells the hardware how to obey the commands of the software. In practical terms, microcode governs how the gates on the circuit board (which control the electrical current) are opened and closed, so that the code is translated into a real, physical flow of current.

These two teams would grind away for an insane number of hours to build, from circuit boards and microcode, the "soul" of this new machine.

In order to encourage the already-underpaid young teams to work harder — and on nights and weekends without compensation — West deliberately let the young graduates feel that they were "the inventors" of the machine. He considered it "cheaper than paying them overtime."

### 7. Deadlines were pushed back because debugging the Eagle was taking too long. 

Once a computer had been designed, what followed was one of the most crucial stages in the entire process:

Making it work. 

Essentially, this meant _debugging_ it. No matter how great a computer's design, there would always be issues, and these, of course, had to be fixed before it was released for public use. 

For the Eagle machines, the debugging took place on two prototype machines named Coke and Gollum — shoulder-height frames full of wires and circuit boards.

This process involved running several programs and task lists on the computers, and trying to fix any errors in functionality and performance whenever they arose.

It was very precise work. The engineers had to examine and understand exactly how the current was being distributed throughout the circuit board by the chips and microcode, working within timeframes of just a couple of hundred-billionths of a second.

To work as quickly as possible, the team had separate day and night shifts, but even minor delays in the components or errors in code could result in days and nights spent searching for the source of the bug.

It quickly became clear that the team would not make their April deadline. In the midst of this, word reached them that the team in North Carolina was going to miss their own deadline too.

This suddenly put pressure on the Eagle team: since Data General's plan A had been delayed, they began to turn their attention to plan B.

### 8. The Eagle was launched and became a huge success; but soon after, the Eagle team dispersed. 

It was already August — the original April debugging deadline overshot by four months — when Tom West asked the head of the Hardy Boys when the debugging might finally be ready.

His answer? 

"I don't know."

It was only in the beginning of October, after an arduous few months of intense work, numerous failures in diagnostics tests and countless late nights to fix them, that the debugging was finally done.

The machine was ready.

The team had created a computer to rival that of FHP. In fact, the Eagle was 10% faster than the VAX.

Following some more work and delays, the Eagle finally debuted in the spring of 1980. 

It was a massive success, a huge win for Data General, raking in large revenues for the company.

It turned out to also have been crucial for the company. Although the Eagle was a late entrant into the supermini market, having missed numerous deadlines, by the spring of 1981 the FHP in North Carolina had _still_ not produced their own machine.

After the launch, the team disbanded, some to new, exciting projects, as promised — another game of pinball.

Having worked on such a phenomenal project, many in the team felt "postpartum blues," especially as they felt their achievements were not sufficiently acknowledged; there was no hero's welcome as they emerged from the depths of the basement.

Tom West took on a new, managerial role at Data General and was rarely seen in the basement anymore. Within a few years, the senior engineers left Data General for other companies offering far better pay.

The younger team members were rewarded with fancy business cards, ski trips, and even, on occasion, the fabled stock options.

Their main reward, however, was "playing pinball" — working on the many interesting projects that West had devised for them before leaving the company.

### 9. Final summary 

The main message in this book is:

**In the late 70s, a team of motivated, totally inexperienced young engineers built a next-generation computer with very few resources or corporate support. The team was built by a vice president named Tom West, who instilled in them a feeling that it was "their machine." Through immense pressure and late nights, the machine eventually became a huge success.**

This book in blinks answered the following questions:

**Why did the team in Westborough start building the Eagle?**

  * Data General was a fast-growing, no-frills computer company under pressure to design a new kind of minicomputer.

  * Two separate teams began work on a competitor to VAX: one flush with resources, the other unofficial and on the cheap.

**How did Tom West build and manage his team?**

  * A motivated team was assembled by Tom West by getting people to "sign up" — meaning, to fully commit.

  * The Eagle team was motivated by fear, pressure and the possibility of "playing again."

**Was the project a success?**

  * The young graduates formed two teams — the Hardy Boys and the Micro Kids — which built the "soul" of the new machine.

  * Deadlines were pushed back because debugging the Eagle was taking too long.

  * The Eagle was launched and became a huge success; but, soon after, the Eagle team dispersed.
---

### Tracy Kidder

Tracy Kidder is a literary journalist who has won both the National Book Award and the Pulitzer Prize for _The Soul of a New Machine_. He attended Harvard University, where he switched his major from political science to English.

