---
id: 5734870ce236900003290249
slug: the-soul-of-an-octopus-en
published_date: 2016-05-20T00:00:00.000+00:00
author: Sy Montgomery
title: The Soul of an Octopus
subtitle: A Surprising Exploration into the Wonder of Consciousness
main_color: 8C758F
text_color: 665169
---

# The Soul of an Octopus

_A Surprising Exploration into the Wonder of Consciousness_

**Sy Montgomery**

_The Soul of an Octopus_ (2015) is about a fascinating sea creature that few people appreciate. Octopuses may appear like alien creatures, with their tentacles and suckers, but in fact, they are brilliant, intriguing creatures.

---
### 1. What’s in it for me? Delve into the fascinating world of octopuses. 

It's easy for people to relate to cats or dogs, as they're often cute and cuddly, and their warm-blooded bodies — ears, eyes, noses and paws — are familiar to us.

Octopuses, on the other hand, appear totally alien. An octopus has no spine, for instance, and its body is squishy. What's more, did you know an octopus's mouth is in its armpit?

Sure, we see cats and dogs every day — but encountering a live octopus in the wild is rare (and poking at a tank at a seafood counter doesn't exactly count). These blinks will get you acquainted with the fascinating world of octopuses, learning about their personalities and how they perceive the world.

In these blinks, you'll also find out

  * why you'd better not let an octopus get bored;

  * how a mood ring and an octopus has a lot in common; and

  * how to tell if an octopus is becoming senile.

**This is a Blinkist staff pick**

_"Before I read these blinks, I had no idea that octopuses were such intelligent creatures. Far from being simple, unthinking blobs, they are superb problem-solvers with unique personalities."_

– Thomas, English Editorial Lead at Blinkist

### 2. Far from being fabled monsters of the sea, octopuses are intelligent, curious creatures. 

Most people find octopuses frightening. The tentacles, suction cups and slimy texture of these sea creatures make them seem quite "alien."

The idea of octopuses as frightening is partly cultural. Many American films have used octopuses or similar shapes that allude to these tentacled animals to create terrifying deep sea monsters.

Society's view of octopuses as scary monsters has its roots in history. An ancient Icelandic saga called Orvar-Odds tells the tale of a tentacled beast that "swallows men and ships and whales and everything it can reach."

Despite the bad rap, octopuses are fascinating and intelligent creatures!

Octopuses are the smartest of all _invertebrates_, or creatures with no backbone. Granted, some invertebrates, like clams, don't even have a brain.

An octopus has a brain, although it is the size of a walnut. Yet with this small brain, these creatures can do incredible things, such as tell humans apart.

Seattle Aquarium researcher Roland Anderson performed an experiment in which he introduced an octopus to two different people. One person fed the octopus; the other poked it with a stick.

After just one week, the octopus could distinguish the two people by sight. The creature would move toward the person who fed it, and back away from the one who poked it.

Octopuses also crave intellectual stimulation. Without it, they look for something to do.

An octopus once flooded a room adjacent to its own by playing with the valves in its tank. These clever creatures search for entertainment when they're restless or bored.

Sometimes octopuses try to escape from tanks and explore. A researcher at the Marine Biological Station in Plymouth once bumped into an octopus wandering down the stairs, after having escaped from its tank!

> _"Octopuses have turned up inside large conch shells and in scientists' tiny oceanic measuring instruments."_

### 3. An octopus can change color depending on its mood, and can taste with its suckers. 

Would you like to be able to change the color of your skin to match your mood? How about learning to become invisible? Octopuses have incredible skills that help them survive in the ocean.

An octopus can change color, to either appear more dangerous or perhaps less tasty if a threat appears. They can also virtually disappear by effortlessly blending into the environment.

If an octopus feels threatened, it can camouflage its head, eyes and sometimes tentacles with colors, spots and stripes. An octopus can also change color depending on its mood. The giant Pacific octopus turns red when excited, and white when relaxed.

Octopuses can also taste things with their suckers. If you feed an octopus a fish, it passes the fish along its suckers before it puts the fish in its mouth, preferring to taste its food before consuming it.

An octopus uses its sense of taste for more things than just food. While working in the New England Aquarium, the author found that octopuses wanted to taste her, moving their tentacles to the water's surface and touching her skin.

One time the author brought a friend to meet the octopuses. An octopus named Octavia wanted to "taste" Liz, but when she did, she immediately withdrew her tentacles. Liz was a smoker; and nicotine is toxic to many invertebrates, including octopuses. Octavia was repelled by the taste of nicotine on Liz's skin.

Even with their amazing camouflage abilities and tentacled "mouths," octopuses aren't so different from humans. You'll see why in the following blinks!

> _"Three-fifths of an octopus's neurons are not in the brain, but in the arms._ "

### 4. Octopuses and humans both exhibit either introverted or extroverted personalities. 

You are original; there's no one on the planet exactly like you. The same holds for octopuses; each creature has its own personality, and no two are alike.

Like humans, an octopus can be either an introvert or an extrovert.

The author during her research at the New England Aquarium in Boston encountered both introverted and extroverted octopuses.

One octopus named Athena moved to cling to her as soon as they met. Another octopus named Octavia kept to herself for a while. Only after a few visits did Octavia warm up and start to greet her.

Octopuses also love to play! They sometimes play with their _funnels_, a tubular part of the body used for locomotion and breathing.

Researcher Roland Anderson once placed empty pill bottles into a tank. The octopuses began to squirt water with their funnels at the bottles, bouncing them around the tank like balls!

And like humans, octopuses also age. As they get older, octopuses become less responsive, paler in color and don't hunt as often.

As Octavia aged, her behavior also changed. She would spend more time at the bottom of her tank instead of greeting people like she used to.

Interestingly, some octopuses may lose their mental faculties altogether as they age, similar to humans that grapple with Alzheimer's or other debilitating diseases.

The author recounts the story of one older octopus that crawled out of its tank to squeeze itself into a crack in the wall, where it stayed until it died.

### 5. Octopus eyes and human eyes share similar hardware, but octopuses can see panoramically. 

Hunters often aim with the same eye when targeting prey, just as most people have a dominant hand with which they write.

Octopuses similarly have a dominant eye they use to focus when they need to see something clearly.

The structure of the octopus eye is similar to the human eye as well. Both human and octopus eyes have a _lens_ for focusing and an _iris_ for regulating incoming light.

An octopus also has a _retina_, the tissue that takes in light and converts it to impulses that are sent to the brain, just as humans do.

Even though the hardware is similar, the human eye and the octopus eye see differently. Octopus eyes are set further back and are more flexible than human eyes. Octopuses have _panoramic vision_ because their eyes can swivel independently.

Human eyes, in contrast, are directed forward. The field of vision for each eye overlaps significantly, so our total field of vision is much smaller. In short, we're good at focusing on what's right in front of us.

Octopuses, however, are colorblind. They have one kind of visual pigment, where humans have three.

Interestingly, researchers at the Woods Hole Oceanographic Institution and the University of Washington have discovered that octopuses may be able to perceive color through their skin. They've found that cuttlefish have gene sequences in the skin that are typically only found in the retina of the eye.

Octopuses and cuttlefish are close relatives, so they may share the same color-detecting ability.

### 6. Final summary 

The key message in this book:

**Though they've been negatively portrayed in folk stories and horror films, octopuses are truly wondrous creatures. They're intelligent, curious and playful, and they have unique personalities just like humans do.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading: The Genius of Dogs** **by Brian Hare and Vanessa Woods**

_The Genius of Dogs_ (2013) uncovers the remarkable intelligence of man's best four-legged friend. By first examining human intelligence, the authors go on to explain exactly what makes dogs so smart, which talents they have in common with humans and other animals, and what sets them apart.
---

### Sy Montgomery

Sy Montgomery is a naturalist who has traveled all over the world researching animals. She's written over 20 nonfiction books for both children and adults, including the bestselling title, _The Good Good Pig_.

