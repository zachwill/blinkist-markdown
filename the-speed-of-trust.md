---
id: 54cf8ec5323335000a550000
slug: the-speed-of-trust-en
published_date: 2015-02-04T00:00:00.000+00:00
author: Stephen M.R. Covey with Rebecca R. Merrill
title: The Speed of Trust
subtitle: The One Thing That Changes Everything
main_color: FFBF48
text_color: 996E1F
---

# The Speed of Trust

_The One Thing That Changes Everything_

**Stephen M.R. Covey with Rebecca R. Merrill**

_The Speed of Trust_ (2006) is about the importance of trust and how it can improve all aspects of our lives, from personal relationships to productivity in the office. Trust improves communication, and in doing so, speeds up efficiency and lowers cost at the same time. Throughout this book, the authors offer us tips on exactly what to do to increase trust in our lives.

---
### 1. What’s in it for me? Learn how to improve your relationships by developing trust. 

"I trust you." What does that mean? For many of us trust is something like love: an abstract, rather fluffy thing. But this isn't true: trust is a concrete, measurable factor. And it's an asset that we should spend more time cultivating.

These blinks highlight the benefits you can expect if you spend time growing trust in your relationships. Whether you run a business or just want to improve your personal interactions with others, reading this advice can help change your life.

In these blinks you'll learn

  * why it's better to behave with integrity than win at all costs;

  * why every time you go through airport security, you are paying a trust tax; and

  * how you should forgive those who betray you...to a certain extent.

### 2. Trust makes everything better by increasing speed and lowering costs. 

Pick up a newspaper and you'll immediately be confronted by headlines about fading trust in banks, corporations and relationships. Trust affects _everything_. But what is it exactly and how does it affect us so much?

Trust can be defined as the confidence you have in someone.

When you trust others, you feel assured in their abilities to do something, such as paying back that $100, or staying faithful to you in a partnership.

Trust is beneficial to us, as it facilitates good communication. For example, it's far easier to understand someone when you trust them. You've probably experienced times when your best friend (whom you trust) understands what you're saying even when you aren't speaking clearly. Conversely, you'll find it harder to understand what people you're not so familiar with are talking about, even if they're speaking in plain terms.

Aside from our friendships, if we can achieve trust through improved communication, it's excellent for business.

Interestingly, trust enables us to do things more quickly at a lower cost. This is known as the _economics of trust._ For example, before 9/11, it took Americans 30 minutes to get through airport security for national flights. After 9/11, as trust rapidly declined, security clearance required an hour and a half. Due to the fear of terrorist attacks (lowered trust), everything started to take longer (lowered speed), and as a result, expenses for security machines and personnel rose (increased costs).

So we see that the traditional economic formula of _strategy x execution = results_ is inadequate. An element of trust must be added, and this comes in two forms.

One is a _trust tax_, for relationships experiencing waning trust. Another is a _trust dividend_, if there's a high level of trust. The formula then would be _(strategy x execution) x trust = results_.

So in the airport security line, the equation would therefore involve a trust tax, which would lead to a slower, less efficient end process.

Distrust generates many problems.

> _"Trust is one of the most powerful forms of motivation and inspiration. People want to be trusted. They respond to trust. They thrive on trust."_

### 3. Learn how to trust yourself by becoming credible. 

Cultivating self-trust is crucial; if you can't trust yourself, how can anyone else trust you? To foster self-trust, you need to make yourself credible by adopting _The Four Cores_.

The first is _Integrity_. This entails being honest, standing by your principles and doing what you say you'll do.

A fantastic example is Andy Roddick, who displayed integrity in a 2005 tennis match. At match point, his opponent's serve was called "out" and Roddick proclaimed the winner. Then he shocked everyone by showing that the ball was actually "in." The game continued and Roddick ending up losing the match, but he had kept his integrity.

One way to increase integrity is to make and keep commitments to yourself, even if it just means consistently getting up when your alarm clock goes off every morning. Sticking with a simple commitment like this will increase your self trust.

The second core is _Intent,_ meaning positive motives and behavior.

For example, research comparing professions we trust showed that we trust NGOs more than politicians, largely due to the common belief that NGO motives are generally conscientious.

You can improve intent by analyzing and redefining your motives, asking questions like "Am I really listening to this person or do I just want to win the argument?"

The third core is _Capabilities._ That is, developing abilities that evoke confidence. For example, a child who learns a musical instrument is likely to possess higher self-confidence, which can benefit other areas in their life.

One way to improve your capabilities is to keep learning. Try reading up on new developments in your industry or doing your own research in an area you're interested in.

The last core is _Results_, where you build a track record of deeds to which you've committed yourself. FedEx, for instance, has a respectable reputation for _actually_ delivering overnight as they promise. Because of this, we trust their delivery methods and so does the company itself.

By working on these core principles, we develop our own self-trust and in the end this helps others to trust us too.

> _"Trust brings out the best in people and literally changes the dynamics of interaction."_

### 4. Improve your behavior to increase trust in your relationships. 

To establish trust in your relationships, you need to _behave_ in a trustworthy way. How many times do we really believe the skirt-chaser who uses charm and excessive flattery?

To make your word hold more weight than the skirt-chaser's, here are two things you can do:

First, tell the truth. Omitting information, double-talking, or spinning stories are all different ways we avoid speaking the whole truth, even if we're not lying outright. The problem is, this behavior erodes trust, whereas being candid increases it. If you've got a reputation for being dishonest, you'll make it hard for yourself to keep good relationships.

Another thing you can do is demonstrate respect for others by showing them you care. When people think you really care about them, they're less likely to believe you want to con them, and will start putting faith in you.

So how can you show you care? You can do the tried-and-true thank you notes, acknowledge the contributions of people around you, be generous in giving acknowledgement, and avoid bad-mouthing people.

Bear in mind, though, that you'll undo all the good that comes from this behavior if you overdo it!

We all appreciate a straight and honest talker, but stating clearly how much you hate your friend's new romantic partner, for example, is simply hurtful. Avoid going overboard, otherwise you will strain your relationships and weaken your trust levels.

The following analogy will help you see clearly if you are creating or destroying trust. Try thinking of your efforts in terms of a _trust account_. When you act in a trustworthy way, you make a "deposit." When you act to the contrary, you make a "withdrawal." The aim here, then, would be to stay in credit! This means asking others for feedback on your behavior. Doing so will help you gauge how you are progressing and where you can improve. This way you'll receive and maintain trust from others.

> _"What you do has far greater impact than anything you say."_

### 5. Build trust with stakeholders through alignment, reputation and contribution. 

In business, you're often told you need to please your _stakeholders_, but what does this mean exactly?

Stakeholders can be divided into three types: internal (employees), external (the market), and societal (other members of society who judge your business). Each of these stakeholders require different approaches in order for them to trust you and your business.

In order for internal stakeholders (employees) to trust an organization, they need to observe _alignment_. This means that the organization must act in a trustworthy way. When they do so, the trust that results is beneficial for all. When employees trust each other, they communicate better, which increases efficiency and results in company growth and even pay raises.

To instill trust in external stakeholders, a good reputation is paramount. Winning the trust of the market involves focusing on your brand or reputation. For example, Chinese products are unfortunately perceived as less trustworthy than British, German or French products, because they have a reputation for being somewhat inferior and unreliable. Perhaps you can recall a time when you bought a Made in China coat, only to have its buttons and threads come loose a few days later.

For the third type of stakeholder — the societal stakeholder — trust is developed through contribution. A company which has a reputation for doing good by contributing to society will be well loved and respected.

In late 1992 a police assault on construction worker Rodney King instigated riots in Los Angeles, California. Amongst all the burned and looted buildings, only the McDonald's restaurants were left unscathed. When asked why this was the case, locals stated that McDonald's had supported literacy initiatives and sports programs in the community and had always offered jobs for youth in the area. Rioters hadn't touched McDonald's because the chain had created societal trust.

> _"Trust is the hidden variable that affects everything."_

### 6. Trust can be restored by extending smart trust to others or by increasing our credibility. 

Once trust has been lost, many of us think it's lost forever. But in fact, trust can be restored. Let's take a look at how.

Regaining and even strengthening trust takes effort, commitment and the extension of _smart trust_. Smart trust lies in the middle ground between distrust and naive gullibility, and combines the following elements: an inherent feeling that others are generally worthy of trust, and a close look at the possible implications of trusting someone.

Say you are about to go into business with someone who has previously betrayed you. Although we'd normally be right to approach every situation with the hope that everyone involved is inherently trustworthy, and to forgive any wrongdoings, diving straight into a joint venture with this person would demonstrate a lack of learning on our part. Practicing smart trust in this case would mean we wouldn't trust this person unless we're confident they've learned from their mistakes. Until then, it wouldn't make sense to trust them completely.

Extending smart trust also shows others that they can trust us in return, and in this way damaged relationships can be rebuilt.

Trust can also be restored by improving your credibility. It isn't possible for us to "fix" someone else's opinions about us and force them to trust us, so we have to try other means of rebuilding trust. Even if you've jeopardized someone's trust, you can make an effort to regain it by behaving in a way that improves your credibility.

The author's son, for instance, was permitted to use the family car on the condition that he didn't drive over the speed limit, yet he was caught doing exactly that. However, he displayed credibility by working hard to pay off the speeding ticket and therefore restored the trust of his family.

> _"Extending trust to others rekindles the inner spirit — both theirs and ours."_

### 7. Final summary 

The key message in this book:

**Trust is a universal asset that can be created by every one of us. Trust changes everything for the better by speeding up interactions and lowering costs. If we conduct ourselves in ways that promote trust, it will flourish and we will witness the difference in our companies and our personal relationships.**

Actionable advice:

**The little things count.**

The next time you want to show others you care and wish to gain their trust, remember that it's often the small gestures that go a long way. Taking a little time to call them, or taking them aside at work to acknowledge their efforts will help build their trust in you.

**Suggested** **further** **reading:** ** _Trust_** **_Agents_** **by Chris Brogan and Julien Smith**

_Trust_ _Agents_ describes how anyone can use the web to become a trust agent — meaning someone who is well-versed in the tools of the web and who is very influential and trusted because of this.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Stephen M.R. Covey with Rebecca R. Merrill

Stephen M. R. Covey is an inspirational speaker, author and advisor on trust. He's the son of the late Stephen R. Covey, the brains behind the smash-hit _The 7 Habits of Highly Effective People_. He is the cofounder and CEO of CoveyLink Worldwide, an enterprise that aims to influence leaders by teaching them about trust.

Rebecca R. Merrill is a writer. She coauthored _Life Matters_ with A. Roger Merrill, and assisted Stephen R. Covey with his bestselling classic.

