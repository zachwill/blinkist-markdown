---
id: 513f5644e4b0569be343764d
slug: the-spirit-level-en
published_date: 2014-05-27T12:33:22.000+00:00
author: Richard Wilkinson & Kate Pickett
title: The Spirit Level
subtitle: Why Equality is Better for Everyone
main_color: 5AC2E3
text_color: 2E7A91
---

# The Spirit Level

_Why Equality is Better for Everyone_

**Richard Wilkinson & Kate Pickett**

This book provides a detailed explanation of how inequality is responsible for many of our present-day problems, including violence and mental illness. It provides detailed explanations and studies to support this and shows how inequality not only hurts the poor but everybody in a society.

---
### 1. What’s in it for me? Find out why inequality is at the root of many of society’s problems. 

If you watch the news these days, you can't help but notice the many reports of violent crimes. And that's just one of many societal problems emerging. Societies the world over are also struggling with obesity, mental illness and climate change. Interestingly, these problems all appear to lead to one major factor: inequality.

In these blinks, you'll discover

  * why the fact that so many problems have their root in inequality actually make them easier to solve,

  * why Japan's homicide rate is less than a tenth of that in the United States and

  * why equality might be more important for your health and well-being than wealth.

### 2. The strength of a country’s economy doesn’t necessarily reflect its citizens’ well-being. 

What causes the biggest problems in our society? One knee-jerk reaction is to blame a weak economy. If we all experienced a flourishing economy, we would all be healthy and happy, right? Actually, there are several arguments that say otherwise.

The truth is that even countries at their economic peak endure severe problems.

Take the United States, which boasts one of the strongest economies in the world: even this beacon of economic power suffers from major social problems, such as high prisoner rates, gang violence and obesity.

China has also enjoyed booming economic expansion over the last few decades. Yet this growth has failed to improve its people's life expectancy or resolve recent problems, such as pollution.

The disconnect between economic prosperity and well-being is particularly evident in the field of national health. To illustrate this, let's examine the fate of two babies born at the same time in different countries with contrasting economies.

One baby is born in the United States, one of the richest countries in the world with a generous health care budget. The other is born in Greece, a poorer Western country that only spends half as much on health care as the United States.

Based on economic factors alone, we might expect the American baby to live longer than its Greek counterpart.

In fact, the opposite is true: the US baby is expected to live 1.2 years fewer than the Greek baby and faces a 40 percent higher chance of dying in its first year of life.

How can that be?

The following blinks reveal that the root cause of illness, violence and climate change lies not with economic strength but with inequality.

> **Be happy with what you have.**   

Make a conscious effort to stop comparing yourself to others. Instead, remind yourself of how lucky you are to have what you have, and stop coveting your neighbor's Ferrari.

### 3. Income inequality stresses people out, which causes health problems. 

Most would agree that one of society's most pressing concerns is health. But what is at the root of our health problems? Surprisingly, recent evidence suggests that health issues, such as obesity, are caused by income inequality.

Take, for instance, the fact that countries with higher overall income differences tend to have more health problems than those with more evenly distributed incomes.

This is evident in nations such as the United Kingdom, Portugal and the United States, where inequality is rife and health issues are widespread.

In contrast, countries with little inequality, such as Japan, Sweden and Norway, enjoy good overall health.

So how does income inequality cause illness?

The answer is _stress_.

In communities where there is a wide income disparity, people are pressured to compare themselves with others and maintain a high social status. This pressure generates stress.

For example, picture yourself in the following scenario: you earn a meager income and live next door to a rich neighbor. As a result, you feel pressured to measure up, perhaps by owning the right status symbol, e.g., an expensive car. However, when you realize you haven't got the money for that Mercedes, you get stressed.

What are some of the ways that this stress impacts your health?

Well, you might try to deal with your stress by _stress-eating,_ which puts a strain on your body. Studies have shown that stress makes us crave more food, which would also explain why many students put on weight when studying for exams.

But it's not just short-term stress that's harmful to our health: long-term stress causes the heart to beat faster and blood vessels to constrict. Eventually, this can lead to elevated blood pressure and the risk of heart attack.

### 4. Income inequality can cause mental health problems and mistrust in society. 

We've seen how income inequality negatively affects our physical well-being. Unfortunately, it doesn't stop there; inequality also negatively impacts our mental health.

One of the ways it does so is by making us egotistical and dishonest.

In an unequal society, we become anxious about how we're perceived by others. When we worry about people's judgments, our anxiety can bring out the worst in us, causing us to become defensive and even dishonest to prove our status.

Imagine you live in a community with uneven income levels and one day you run into some of your old schoolmates. Earning a lot of money is important to you, but you have a low-paying job, so when someone asks you how much you earn, you're tempted to lie about it. You'll likely see no problem in doing so, as the discomfort in lying seems better than feeling like a loser in front of your peers.

Aside from making us dishonest, countries with low levels of equality also have more mental health problems.

The United States, for example, has a high level of inequality and over 25 percent of its population is afflicted by mental illness.

Contrast this with Japan, which has low inequality and under 10 percent of its population suffers from mental illness.

Finally, inequality breeds distrust.

Evidence for this can be seen in a survey that asked people from different nations whether they agreed with the statement "Most people can be trusted."

Countries with high income equality, such as Sweden, Norway and Finland, were among the nations with the highest percentage of agreement. And Portugal, which has a large income disparity, showed the lowest agreement.

We can thus see that inequality is not only associated with mental illness but also affects how we behave and react towards others in our society.

### 5. Income inequality incites violence as people fight to uphold their social status. 

So now we know how inequality hurts our mental and physical health on a personal level. However, it also hurts us on a larger scale: when it's widespread, inequality leads to increased violence.

Interestingly, violence seems to proliferate in places with greater income disparity. According to Harvard University sociologists, neighborhoods with a more equal income distribution appear to want to act together as equals rather than fight each other.

So why exactly does inequality incite violence?

As we have seen, social status is important in unequal societies and an old way to defend social status is through violence.

Imagine you're being provoked in a bar. If you're feeling secure and confident about your own social status, it's easier to resist the temptation to fight, as you have nothing to prove.

However, if you live in a society where social status is important and you're insecure about yourself, you may feel the need prove your status, even if it means acting violently.

Several studies have also shown that countries with a high level of wealth inequality have more cases of homicides than more equal countries.

For example, the United States, with its high levels of income inequality, has one of the highest homicides rates in the world, with about sixty homicides per million people annually. More equal countries, like Japan and Norway, fall below the average in homicide rates with just five to ten homicides per million people annually.

Now we have strong evidence that inequality causes stress, bad health, mistrust and violence in a society. But does that really apply to everybody? Or are there some social classes that escape all this?

### 6. Income inequality hurts everybody in a society, even the wealthy. 

Sure, inequality is a bad thing — but only if you're at the very bottom of society, right? Why should you care about equality if you belong to one of the upper classes? Well, the truth is that inequality hurts everybody.

It's not just those with low incomes who become stressed by social comparison and competition in unequal societies. It affects the rich, too.

Say you have a well-paying job in finance. Even as you climb the ladder, you're still aware that your superiors make far more money and enjoy a higher social status than you. So no matter how well you do, you constantly feel pressure to keep up with them. Not only that, but you also find yourself worrying about _losing_ the social status you worked so hard for. The thought of experiencing this inequality causes you a great deal of stress.

Fortunately, as much as inequality negatively affects us, an _equal_ society does the opposite in that is offers us clear advantages. Studies have found that regardless of what social class someone belongs to, their class-counterparts reap more benefits in societies with more income equality.

Let's take a look at the relation of health and equality, for example. Researchers have found that the more equal a society, the healthier its citizens tend to be, regardless of their income level.

For example, it has been shown that, even if you hold a high-paying job in a relatively unequal society, like the United States, you're still less healthy than if you earned the same income in a relatively equal society, like England. And in an even more equal society, like Sweden, you'd be even healthier with the same income.

Hence it's clear that, even if you're living large with a well-paid job, you could still benefit more from a more equal society.

### 7. Rather being a symptom of wider problems, inequality is the root cause of them. 

So far, we've argued that inequality causes problems. But what if inequality is one of many problems that are caused by other, more deeply rooted factors? This point seems fair, but there's evidence to say otherwise.

One argument reinforcing inequality as the cause of societal problems is the concurrence of these problems; countries tend to do either very poorly or very well in all problem categories, which indicates a common cause.

But what points to inequality specifically?

The United States, for example, has a high level of inequality and fares badly in terms of health, trust and violence, whereas Sweden, which has a low level of inequality, fares better in all these areas.

Furthermore, inequality seems to be the only significant factor that countries with these problems share, making it the likely cause of the problems.

But perhaps wealth is the major factor? Richer nations have fewer problems, right?

Unfortunately not. If that were the case, rich nations like the United States wouldn't perform badly in the aforementioned problem areas.

In addition, the cause of societal problems is unlikely to be local conditions or cultural characteristics. Even neighbor countries that share some similar cultural traits, like Portugal and Spain, have markedly different levels of health and violence.

Another argument for inequality being at the root of other problems has to do with changes in society over time. There tends to first be a change in inequality and only after that do other problems start arising.

We can see this in the years after World War II, where income differences narrowed in Japan yet widened in the United States. Subsequently, the life expectancy in Japan rocketed past that in the United States. 

Based on all this evidence, it seems that our problems stem from one source: income inequality.

### 8. Greater income equality can even help fight global warming. 

Of course inequality is bad for us. But what benefits does _equality_ actually afford us, apart from just being "a good thing"? For starters, it could help us avert one of the biggest dangers we face today: climate change.

One action we can take against climate change is to reduce carbon emissions. But, in order to do this, a society must have certain qualities.

_Trust_ is one of them, for it only makes sense to change your own habits if you trust that other people will do the same. If you're the only one acting, global change won't happen. And, as we've already seen, the more equal the society, the higher the level of trust.

_Creativity_ is another important quality for societies to fight climate change: it helps spawn inventions, such as solar panels and electric cars, which play crucial roles in stopping climate change. Studies have shown that creativity is also more common in equal societies.

So how else does an equal society help the environment?

Well, take a luxury good like the sports car: the production and running of such cars puts a strain on our environment. In an equal society, there's less pressure to own such luxuries because people don't feel the need to bolster their status in front of their richer peers. Therefore, in an equal society, fewer sports cars are bought and used.

Another way an equal society could help combat climate change is that it would leave us with more money and resources to fight it.

For example, if a state passes a law to improve equality, like a tax reform, it would help eradicate problems such as violence and poor health. The money saved by having fewer prisons and affordable health care could then be used to finance new green technologies and fight climate change.

### 9. The problems of inequality are linked and can therefore be changed together. 

Currently, we fail to see inequality as the underlying cause of societal problems and this has prevented us from addressing them effectively.

Like a doctor who only treats symptoms, we are not focusing on the cause of the illness.

One illustration of this symptom-treating is the way we keep building more and more prisons to deal with violence. This logic is fatally flawed because as long as the cause of violence is not eliminated, violence will persist.

Coming back to the doctor analogy, if we find out the cause of the illness, we can treat it and the symptoms will disappear over time.

By the same logic, if we invest our time and resources in achieving equality, violence will decrease, reducing the need for more prisons.

So what can actually be done about inequality?

One solution would be to raise top-level tax rates and redistribute the money to poorer citizens, which would be effective in adjusting income levels.

Another possibility would be to encourage democratic ownership of companies, i.e., when companies are owned by their employees who can then make joint decisions. Studies have shown that, in such companies, salary levels are more equal and employees have a greater tendency to treat one another as equals.

There is some comfort, then, in the fact that our problems seem to be linked to one major factor, which makes the idea of solving them seems less daunting.

### 10. We can change our society into a more equal one. 

When faced with serious societal issues, we sometimes feel that overcoming them is unrealistic or even impossible. Fortunately, when it comes to fighting inequality, this isn't the case.

One promising sign is that the majority of people aren't content with the way things are and want to see change.

In fact, a research report called _Yearning_ _for_ _Balance_ found that three-quarters or more of US citizens shared the opinion that their society had lost touch with what really mattered, thus indicating a desire for improvement.

We can also see this desire through successful political campaigns such as Barack Obama's 2008 presidential campaign, which was based on a promise of positive change.

Since most of us agree that problems exist and need to be solved, the way in which we tackle them — whether by introducing stricter laws or implementing mentoring programs — doesn't matter as much as our unified motivation for equality.

Moreover, the fact that we have differing opinions on how to achieve equality actually gives us a more realistic chance and more opportunities to succeed in doing so.

For example, if a group is strictly against changing the tax system, one could still come to an agreement by concentrating on promoting common ownership of firms, for example, which also leads to equality.

We can see, then, that having a unified aim to create a more equal society and a host of options for doing so changes the notion of equality from an impossible utopia to a state that just may be attainable.

### 11. Final Summary 

The key message in this book:

**The** **main** **cause** **for** **many** **of** **the** **societal** **problems** **we** **face** **today** **is** **inequality.** **Inequality** **affects** **health,** **trust,** **violence** **and** **climate** **change** **in** **various** **ways.** **This** **connection** **can** **be** **clearly** **seen** **over** **numerous** **societies** **and** **countries.**
---

### Richard Wilkinson & Kate Pickett

Richard Wilkinson is an expert in social determinants of health and a professor at multiple universities. Kate Pickett is professor of epidemiology at the University of York. Together, they started the website www.equality.org.uk to further promote the cause of equality.

