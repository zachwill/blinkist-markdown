---
id: 532866723635380008e00000
slug: the-sports-gene-en
published_date: 2014-03-19T08:25:19.000+00:00
author: David Epstein
title: The Sports Gene
subtitle: Inside the Science of Extraordinary Athletic Performance
main_color: FABC63
text_color: 94631E
---

# The Sports Gene

_Inside the Science of Extraordinary Athletic Performance_

**David Epstein**

_The Sports Gene_ takes a look at the physiological traits that are beneficial in various sports, and at their hereditary background. It also examines why people in certain parts of the world have evolved in their particular way, and how this is beneficial in the realm of certain sports.

---
### 1. What’s in it for me? Find out how your ancestry influences which sports you’re naturally well-suited for. 

Did you ever dream of becoming a world-class athlete?

As you probably know, reaching the top demands years of gruelling training, full commitment and an iron resolve to succeed at your chosen sport. But an investigation into the backgrounds of the world's top athletes reveals that physiology and ancestry, too, can play a crucial role.

From your skeletal structure to the tiny red blood cells in your veins, your genes massively impact your baseline physiology, as well as your ability to improve it.

In these blinks, you'll discover how you can blame your lack of motivation to hit the gym on your genes.

You'll find out why if you meet six men over seven-feet tall, you can pretty much ask one of them for an autograph right away.

And, finally, you'll discover how one enclave, founded by the most physically fit runaway slaves, is today producing some of the fastest sprinters on the planet.

**This is a Blinkist staff pick**

_"In comparison to the famous 10,000-Hour rule, these blinks opened my eyes to some of the other ways people become great. Plus, I had a great time talking to Epstein about the book on the Blinkist podcast."_

– Ben S, Audio Lead at Blinkist

### 2. Hereditary factors like one’s height and arm span are key advantages in basketball. 

Have you ever wondered what it takes to become an outstanding athlete, like a NBA basketball player or an Olympic sprinter?

Obviously, hard work and dedication are essential. But it also helps immensely if you happen to be blessed with excellent genes, as they can give you a body type well suited for certain sports.

One of the most often mentioned and obvious physical traits that is genetically determined is _height_. In the industrialized world, it's estimated that approximately 80% of the difference in people's heights is due to their genes.

But there's no single gene that determines height. As yet, even the best study on the topic managed to explain only 45% of the height differences among people, and even that required surveying hundreds of thousands of genetic differences. What's more, genetic influence on other physical traits is similarly ambiguous.

What is certain, however, is that tall people are clearly advantaged when it comes to basketball. Because the basket is ten feet off the ground, the higher one's reach, the greater his advantage. Indeed, at a professional level, this advantage is such that, right now, an incredible 17% of American men aged 20-40 over seven feet tall are in the NBA.

Height is so important in the NBA that shorter-than-average players usually need other attributes to compensate for their short stature. For example, they may have long, stiff achilles' tendons that allow them to jump high — like Spud Webb, who at just 5'7" still managed to win the 1986 Slam Dunk Contest.

Shorter players usually also have a disproportionately long arm span, which gives them a higher reach, enabling them to for example better block shots and get rebounds. In fact, the average arm span of a NBA player is so wide compared to his height that this disproportion would normally lead to a diagnosis of Marfan syndrome, a disorder which affects the body's connective tissue. And just like height, the skeletal structure that produces a wide arm span is also largely hereditary.

### 3. Skeletal structure also plays a big role in suitability in running, sprinting and swimming. 

As with basketball, having the right skeletal structure can be of great benefit in many other sports, too. Of course, how we define "right" is determined by the sport under consideration.

_Long-distance runners_, for instance, benefit from a so-called "Nilotic" body — a very slim torso and thin legs — as evident in today's best marathon runners.

This body type is beneficial because long legs allow for a longer stride and faster top-speed, while the lightness of a thin torso reduces the load on the legs.

The bodies of long-distance runners also tend to be small because it gives them a larger skin surface area in relation to the body's volume. This allows them to disperse heat more effectively; a crucial advantage in a long, potentially exhausting race.

In contrast, _sprinters_ require shorter legs because short legs have less inertia, enabling these runners to accelerate quickly from a standing start.

In fact, the shorter the distance of a race, the shorter that successful runners' legs tend to be. This phenomenon is also seen in American football, where quick acceleration is crucial. Indeed, even while humans have gotten taller over the last four decades, the average height of running backs and cornerbacks in the NFL has declined.

Short legs are also a trait of successful _swimmers_, along with long arms and upper bodies — a skeletal structure that helps them to glide along the water. For example, Olympic swimmer Michael Phelps is 6'4" yet he wears pants with a relatively short 32-inch inseam.

Furthermore, the suitability of one's body type to certain sports can be greatly influenced by ancestry. One study found that, in general, black adults (African ancestry) have longer legs and a higher center of mass than white adults (European ancestry), which would suggest that the body type of black adults is better suited to running, while white adults' lower center of mass makes them better swimmers — a theory that's been confirmed by statistical studies.

### 4. The suitability of your muscles to a given sport is also determined by your genes. 

We now know that inherited skeletal structure is clearly advantageous to playing certain sports — but equally important is muscular structure.

Muscles are made up of two types of fiber: _fast-twitch_ and _slow-twitch_.

Fast-twitch fibers contract quickly, enabling fast, explosive movements, but they tend to tire rapidly. For that reason, these fibers are well-suited for sprinting or weight lifting.

In contrast, slow-twitch fibers have a slower response time. Yet they also have more stamina, making them useful in endurance sports.

We each have a different ratio of these muscle fibers, inclining us to do better at certain sports than others, and there's very strong evidence that this ratio is inherited.

While in most people the ratio of fast to slow-twitch fibers is about 50/50, the ratio in athletes who excel at a particular sport can be quite different. For example, one top long-distance runner was found to have 80% slow-twitch fibers, and the calf muscles of sprinters are, in general, made up of 75% fast-twitch fibers.

In addition, the ratio also determines how muscles respond to weight training, because fast-twitch fibers are able to grow at double the rate of slow-twitch ones. For example, in one study, some participants who followed a four-month weight-training program managed a 50% increase in muscle size while others following the same program saw no increase at all.

As well as determining the growth rate of muscles, our genes also determine the maximum _size_ they can attain. According to some studies, five kilos of muscle requires one kilo of bone for support, which means that people with a small skeletal structure will find it difficult to gain the muscle mass required for power sports.

Furthermore, there are even genetic abnormalities which cause an unusual condition known as "double muscle," as was seen recently when the German media dubbed one infant "the superbaby," born with large biceps and well-defined calves. This genetic condition was shown to be identical with that which causes the unusually bulging muscles of Belgian Blue cows.

### 5. Your aerobic capacity greatly impacts your performance and, though it has a hereditary baseline, can be increased by training. 

When we do any kind of sports, we quickly start panting. This is because our muscles demand oxygen to function. Unsurprisingly, the amount of oxygen that an athlete can take in will greatly influence his or her performance.

This so-called _aerobic capacity_, or _VO2max_, is defined as the maximum rate of oxygen intake over a period of increasingly strenuous exercise — for example on a treadmill.

The rate of intake is determined to a great extent by one's innate "baseline" capacity. For example, it turns out about 0.3% of all people happen to be _naturally fit_ : they have the VO2max of an athlete, despite being completely untrained.

So, what physiological factors determine our maximum aerobic capacity?

One factor is the total volume of blood in our bodies: the more blood we have flowing through our veins, the more oxygen it can carry.

Another factor is the ability of our blood to carry this oxygen, which is determined by the amount of hemoglobin — an oxygen-carrying protein — in our red blood cells, and the number of those red blood cells in our blood.

Both are largely determined by one's baseline VO2max. For example, one professional cross-country skier was found to naturally have 65% more red blood cells than the average male.

Yet, studies have also found that it's possible to increase VO2max through training. In particular, training at a high altitude is an effective way to increase VO2max because the body responds to the lower amount of oxygen in the air by producing more red blood cells and hemoglobin.

In fact, some athletes take advantage of this phenomenon by spending time training at high altitudes before competitions. The ideal altitude is six- to seven-thousand feet; any higher and the increase of red blood cells and hemoglobin begins to slow down the blood flow. And those born at a high altitude are at an even greater advantage, as they naturally develop larger lungs growing up, so are able to take in more oxygen.

### 6. Your genes also influence your desire and ability to train hard, as well as your susceptibility to injury. 

In addition to its influence on muscles, skeletal structure and aerobic capacity, our genetic makeup also affects athletic performance in more subtle ways. For instance, did you know that our genes influence our motivation and desire to train?

As studies have indicated, one's genes greatly determine how much he or she exercises: up to 75% of the variation in the amount of exercise people do is due to their genetic makeup. This has a neurological basis, as the brains of some people don't sense pleasure as readily as others, which means those people have to train harder to feel gratified.

Consider triathlete Pam Reed, for example, who is compelled to run up to five times per day because otherwise she would begin to feel sick. Having such a strong urge to train clearly gives her a great advantage over other athletes.

Another way that genetic makeup influences how much or how hard you can train is in its effect on pain threshold.

When pushing the performance of your body to its limits, pain is always a factor. How sensitive you are to it will affect whether you give up when the pain seems unbearable or keep pushing. Those who are genetically more resilient to pain are more likely to continue training — a predisposition that gives them a massive advantage.

Finally, your genes can also hinder training by making you more or less susceptible to injury.

Indeed, studies show that genetic differences can account for how easily one's bones break or their tissues tear, and even how well their brain recovers from damage. This predisposition towards injury can have a critical effect on athletic success, because such injury can hinder training and, even worse, end careers.

### 7. Two tribes from Kenya and Ethiopia have evolved the perfect build for long-distance running. 

If you follow athletics, it's hard not to notice that a disproportionate number of the world's most successful long-distance runners hail from Kenya and Ethiopia.

What explains this dominance?

First, Kenya and Ethiopia are located on the equator, where the extreme heat has resulted in the local populace having small, "nilotic" bodies to disperse body heat effectively — a body type ideal for long-distance running. Second, both countries have an altitude that's close to the "sweet spot" for altitude training. As a result, the inhabitants have evolved to have larger than average lungs, as well as higher hemoglobin and red blood cell counts.

Moreover, the best long-distance runners originate from even more specific groups within those countries — the Kalenjin tribe in Kenya, and Oromo tribe in Ethiopia. Despite the fact that both are minority groups, the tribes produce a majority of the countries' top runners.

One reason for this is that members of these tribes have thin lower legs which use less energy and thus provide the runners with more stamina in long-distance races. In fact, scientists have calculated the amount of energy a Kalenjin runner saves, in comparison to a Danish runner, to be eight percent per kilometer.

This difference can be explained, in part, by evolution. Both tribes originate from a _pastoralist_ background, where their ancestors often raided cattle from other tribes. To pull this off, they had to run long distances; the best runners seized the most cattle, resulting in them having more wives and producing more children with their genes.

Finally, inhabitants of both countries run more on a daily basis than those of other countries, and they're highly motivated to succeed at becoming top runners. For example, the children of these countries tend to run wherever they go, and thus develop a high aerobic capacity from a young age. And for the people of both countries, becoming a good runner can mean escaping a life of poverty — a strong motivation to train hard.

### 8. Malaria and the hardships of slavery could partially explain why West Africans and Jamaicans excel at sprinting. 

As astounding as the dominance of East African nations in long-distance running is the extraordinary success of sprinters with West African heritage.

For the past few decades, every Olympic finalist in the men's one-hundred meters has his ancestral roots in West Africa.

So, what's behind this incredible track record?

Quite simply, evolution has left West Africans with bodies capable of explosive bursts of energy. The roots of this power can be traced to the pervasiveness of malaria in West Africa.

To cope with malaria, the inhabitants evolved certain traits that made their red blood cells more resilient to the disease, but, at the same time, those traits also decreased the oxygen in the blood.

To compensate for this, the West Africans also evolved to have a higher ratio of fast- to slow-twitch muscle fibers, in addition to a more oxygen-efficient energy metabolizing process. While both traits preserved oxygen, they were also beneficial for the production of explosive force required in short-distance running.

Another source of world-class sprinters is Jamaica — specifically, the tiny parish of Trelawny. While many of the world's top sprinters originate from Jamaica, a disproportionately large number of them can trace their roots to Trelawny — for example, the 2008 Olympic-winning sprinters Usain Bolt and Veronica Campbell-Brown.

What's so special about this _Trelawny population_ as it's known? According to one speculative theory, the physical fitness of its people is due to Trelawny's history of slavery.

Slave traders took slaves from West Africa (those with already strong "sprinter" genes) on an horrifically arduous voyage to Jamaica that only the very strongest could survive. Once these survivors reached Jamaica, some slaves managed to escape to secluded Trelawny, where they could not be found. Naturally, only the fastest runners were able to make this daring escape.

This would mean only the fittest and fastest of the slaves found their way to Trelawny, ultimately passing on their genes to the elite sprinters of today.

### 9. Final Summary 

The key message in this book:

**Although hard work and training are certainly crucial in any sport, one's athletic abilities are also massively influenced by ancestry. From your skeleton to your muscles and even to your motivation, your genes play a large role in determining your athletic fate. Nowhere is this more obvious than in the divide between the specific abilities of runners with ancestry in East or West Africa.**

Actionable advice:

**To find your sport, take at look at yourself and your family.**

Try to get a grasp of what kind of natural physiology you have, by examining yourself, your parents and your siblings. What kind of sports have you or they excelled at? Are they particularly strong or tall? Do they have stamina or rather excel at explosive force? 

Also, think about where your roots are and where you were born? What kind of people thrived in the environment of your ancestors, and how would the place you grew up in have influenced for instance your aerobic capacity? Once you have an idea of your natural physiology, you will understand your natural strengths better, and can find a training regimen that makes the most of those strengths.
---

### David Epstein

David Epstein is a writer for _Sports Illustrated_, specializing in sports science. Epstein also holds a master's degree in Environmental Science.

