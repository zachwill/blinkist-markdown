---
id: 569e7fe7eaed8a0007000028
slug: the-startup-playbook-en
published_date: 2016-01-22T00:00:00.000+00:00
author: David S. Kidder
title: The Startup Playbook
subtitle: Secrets of the Fastest-Growing Startups from Their Founding Entrepreneurs
main_color: FFEF48
text_color: 665F14
---

# The Startup Playbook

_Secrets of the Fastest-Growing Startups from Their Founding Entrepreneurs_

**David S. Kidder**

_The Startup-Playbook_ (2012) gives you business-building tips straight from the founders of some of the world's biggest start-ups. By conducting interviews with the founders of companies like LinkedIn and Spanx, the author uncovers what you need to do to make it big.

---
### 1. What’s in it for me? Let the masters teach you how to succeed with your company. 

In the United States alone, over 500,000 companies are started each month! Needless to say, many of these companies will not make it. In fact, most of them won't. So how do you make sure your company is one of the lucky few? It's simple. You learn from the ones who _have_ made it. 

In these blinks, five founders of highly successful companies share their secrets on how to grow your fledgling start-up business into a successful company. You already know that it's important to be passionate and dedicated. But, as you'll see, it takes a bit more than that to succeed with your company.

In these blinks, you'll learn

  * how to turn unused resources into new products and services; 

  * how Sara Blakely's pantyhose ended up on _Oprah_ ; and

  * why the future is all that matters.

### 2. Enter the market early and anticipate obvious problems. 

You've probably heard of the professional social network LinkedIn. But do you know the story behind it? Most people couldn't tell you how such huge companies get started.

And, contrary to what many people think, the key isn't to be in the right place at the right time — it's to be there _early_.

This is a lesson from Reid Hoffman, the founder of LinkedIn. He's also invested in several successful start-ups, and his secret is to spot a trend before it even starts. For example, when he invests in start-ups, he looks for companies whose value has yet to be fully recognized — a sure sign that a trend has yet to begin.

When Hoffman was starting LinkedIn, many people thought his idea would fail. Newspapers and headhunters already fulfilled the needs of the employment market, so why was his website necessary? Hoffman's idea seemed bad because it came so early. This made it harder to find investors, but it also greatly diminished competition.

Another reason LinkedIn became so successful is Hoffman's ability to anticipate solutions to obvious problems.

If you've got a great idea, chances are someone else has probably already tried it — and failed. That means you have to figure out exactly why the idea hasn't already made it big, and what you can do differently to make it work.

For example, when LinkedIn got started, people were skeptical about the value it could offer its first users; with so few members in the network, what was to be gained? This is a typical problem for many start-ups.

Hoffman had a great solution, though. He developed a function that allowed new users to scan their e-mail address books for LinkedIn matches. They would then be shown a list of who was already on the network, as well as options to invite friends and colleagues to join. This network grew quickly, allowing its users to connect with more and more people.

### 3. Create mental snapshots of the future, and keep your business model secret. 

If you want to succeed in business and life, you need to know where you're going. Sara Blakely, founder of hosiery company Spanx, knew exactly where that was for her.

How did she know?

She created _mental snapshots_ : visions of herself having accomplished ambitious but concrete goals. And even though she didn't know how she was going to accomplish them, she could use her mental snapshots to cheer herself on and renew her faith in her ability to get there somehow.

For example, Blakely already dreamt about being on _The Oprah Winfrey Show_ when she was in high school. She used that dream to fuel her work until the year 2000, when Oprah named Spanx as one of her new favorite products.

But no matter how excited you are about your business idea, be careful about sharing it too openly.

It's normal to want to share your great idea with everyone you know. But instead of receiving the support you need, you may well find yourself in the discouraging position of having to constantly defend your business model against the skeptical remarks of friends and family. 

Think of how Blakely's parents reacted when she told them she wanted to sell footless pantyhose: They were extremely skeptical. Even though she had clearly invested a lot in her idea, her parents simply couldn't see the potential.

And then there's another risk that comes with sharing your idea. Someone might steal it from you. That's why you should only talk with potential investors until your business is up and running.

### 4. Put inefficiently used resources toward creating new products, and react quickly to problems. 

Did you know that many people spend less than five percent of their time driving their car? The other 95 percent of the time, it's just parked somewhere, taking up space. Robin Chase realized how inefficient that type of car ownership was, and so she founded Zipcar — a car rental company that offers cars for an hourly rate.

You too can found a successful start-up by identifying inefficiently used resources and coming up with an efficient solution. Try imagining how we could use what we already have more efficiently, like Chase did.

Another strategy is to come up with new functions for things normally used for one purpose. For example, ten years ago, our phones could only be used to call and text people; in the past decade, however, we've seen them develop into hi-tech computers that can take photos, play videos and send e-mails.

But no matter how brilliant your idea is, your start-up won't survive if you don't react quickly to problems.

So if you discover a big problem with your business model, don't get discouraged. Instead, try to solve the problem as soon as possible, and inform your customers about what is going on. If you're honest, they will be more understanding than you might expect.

For example, after the first three months of running Zipcar, Robin Chase realized that there was a serious revenue problem that would eventually bleed the company dry. After consulting her whole team, she quickly concluded that without a 25 percent increase in the hourly rate, the business would die.

Robin dreaded informing her customers of this. Once informed, however, most of them were prepared to pay more for what they already considered a great service.

### 5. Nurture enthusiastic long-term customers and healthy, highly motivated employees. 

In 1998, Seth Goldman founded Honest Tea, a beverage company that sells organic, fair-trade, low-sugar tea. The company now makes more than $70 million in sales each year. So how did Goldman achieve such phenomenal success?

He realized early on that the company's success didn't just depend on him — it also depended on the enthusiasm and engagement of his employees and customers. 

One way of nurturing healthy and highly motivated employees is to lead by example.

You need to be a role model for managing the stresses of the start-up life. When you're getting a start-up on its feet, you may be tempted to overwork yourself. But if you live an unhealthy lifestyle, your employees will try to keep pace with you, which will make the whole organization unhealthy. So it's crucial to take time off when your stress-levels get too high.

When Goldman started Honest Tea, he felt like he had to work non-stop. But because he wanted to communicate a philosophy of work-life balance to his employees, he did his best to leave the office every day at 5:30 to spend time with his family. That way, his employees could see it was okay for them to leave early, too.

Another thing you'll need for a successful start-up are enthusiastic long-term customers.

No matter how small your start-up is right now, the customers you already have are crucial to your future, because they establish your reputation in the market. Do whatever you can to keep them enthusiastic about what you do, and turn them into customers for life.

Goldman realized that if he could get children to consume his brand from a young age, they would stick with it when they got older. So he started Honest Kids, a fruit juice for children, to encourage them to drink Honest Tea later in life.

### 6. Always keep your eyes on future developments, and focus on the customer. 

Hosain Rahman knows the value of looking to the future. He founded the wearable technology company Jawbone in 1999, long before wearable tech was a trend. And by cultivating a future-oriented mindset, he learned how to keep his company ahead of the game.

A key principle of the future-oriented mindset is that _what worked yesterday won't work today._

The world is changing so rapidly and unpredictably that you must keep moving. If you allow yourself to become satisfied with your success, you'll lose sight of what you need to change to keep your customers happy.

For example, if you sell ice cream and reach your sales goal in three years, you might be tempted to relax. But usually that's the exact moment your customer's tastes change, or a new competitor enters the market — and your company collapses.

Something else Rahman learned early on is the importance of focusing on your customer's wishes and needs. He realized the value of investing in things that directly benefit your customers, like high quality products and excellent customer service. 

Take the Jawbone Up bracelet. It can analyze your health by tracking your day-to-day habits, such as sleep and exercise, and then organize all the collected data for easy access on your smartphone. But on top of this it has a sharp, customizable look: you can choose from various colors and designs to find the Up bracelet that perfectly fits your style.

In a world of almost infinite choice, the only way to stand out is to develop top-notch products. That means not only paying attention to what's needed, but paying attention to what's wanted as well. Develop a product that looks as smooth as it functions, and you'll be well on your way.

### 7. Final summary 

The key message in this book:

**By making sure you enter markets early, with a clear idea of where you want your business to take you and by always keeping the customer in focus, you'll increase the chances of your start-up's success.**

Actionable advice:

**Pay attention to unmet needs — they might be a key to something big.**

If you're looking for start-up ideas, be vigilant for products, apps and services that you or other people would like to use, but can't find anywhere. This often indicates an unmet market need that you can jump to meet.

**Suggested** **further** **reading:** ** _The Lean Startup_** **by Eric Ries**

The _Lean Startup method_ helps start-ups and tech companies develop sustainable business models. It advocates continuous rapid prototyping and focusing on customer-feedback data.

The method is based on the concepts of _lean manufacturing_ and _agile development,_ and its efficacy is backed up by case studies from the last few decades.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### David S. Kidder

David S. Kidder is an entrepreneur and writer. Most recently, he cofounded the online marketing company Clickable, and in 2008 he received the Ernst & Young Entrepreneur of the Year Award.

