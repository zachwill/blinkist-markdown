---
id: 53cf86303630650007230100
slug: the-story-factor-en
published_date: 2014-07-22T00:00:00.000+00:00
author: Annette Simmons
title: The Story Factor
subtitle: Inspiration, Influence, and Persuasion through the Art of Storytelling
main_color: BCB9AA
text_color: 69623E
---

# The Story Factor

_Inspiration, Influence, and Persuasion through the Art of Storytelling_

**Annette Simmons**

_The_ _Story_ _Factor_ explains what a powerful tool narratives can be, and unravels the art of telling your own stories. Stories are far more effective than mere facts or figures when you're making a presentation or trying to inspire people to take action.

---
### 1. What’s in it for me: Influence people by unleashing the power of stories. 

For thousands of years, people have wanted to hear stories, whether from travelling bards in the old days or best-selling paperbacks today.

Why are we so drawn to narratives, when surely we really just need facts to make decisions?

Put simply, a good story helps people interpret the facts and see the bigger picture. Things people would not believe, understand or care about become compelling and meaningful as soon as they are seen through the lens of a simulated personal experience: a story.

And if you're the one telling the story, you can influence the way your audience interprets facts. This makes good storytelling a very powerful tool indeed.

So how do you tell a good story?

> _"When you want to influence others, there is no tool more powerful than story."_

### 2. Stories help us make sense of the world and are effective tools for persuading others. 

In order to know how to tell a good story, we must first know what a good story actually is.

Quite simply, a good story is one that simplifies the world and makes us feel we understand it better.

The world today often seems complicated and chaotic, but a story has the power to make sense of it. It gives listeners a plot to follow, around which they can organize their thoughts.

This not only makes it easier for the listener to understand your argument, but can also help them make sense of their personal problems and frustrations. For example, if someone has lost their job or gone through a tough break up, a story about someone in a similar situation will help them to recover.

In this sense stories — though indirect — are more effective than direct guidance. This is because direct guidance only applies to one situation and then loses relevance, whereas the lessons of a story can be adapted to fit multiple situations.

Let's say you have a colleague who frequently sends obnoxious-sounding emails around the office because he can't be bothered to word them properly, and you'd like him to change his behavior. You could, of course, tell him outright to stop doing this, but it would be even more effective to tell him a story about someone who lost their job due to a misunderstanding in a poorly worded email.

Your colleague would probably remember that story every time he was sending an email. He might even start applying the lesson about being polite and professional in his other communications.

So even though stories are an indirect way of relaying a point, they can be much more effective than the direct, raw truth.

> _"A story is less direct, more gracious, and prompts less resistance than the naked truth."_

### 3. Telling an immersive story requires more than just words – use your whole body. 

Let's say you now know what story you want to tell. So how do you tell it properly?

You have to realize that you have many channels of communication available. Imagine you're not merely telling a story, but enacting a play for your listener: your voice, face, hands and body are the actors, stage costumes, music and props that can add a whole new level to your story.

You can use your hand gestures to add meaning to your words and really paint a picture for your listeners.

Use facial expressions to help people relate to your story. If your story is about something that angers you, show that anger on your face, or smile if you're talking about how happy you were at another point in the plot. This makes people more likely to believe you.

Of course, to fully immerse the listener in your story, what you say is also important. Here you should prioritize things that the listener can imagine vividly.

For example, you could ask your audience to imagine the smell of sizzling bacon. Or if your story involves the wind howling, you could make a similar sound to really make them feel present in the story. This kind of visceral experience creates emotional memories that are particularly powerful.

Another way to create emotional memories is to use irrelevant but concrete details. So if you want to tell a story of a large household, don't focus on how many children are in the family or what their names are. Instead, use concrete details and tell your listeners about the way the house would fill with the scent of freshly baked blueberry pie on Sunday mornings.

> _"Words are less than 15 percent of what listeners hear."_

### 4. A convincing speech or presentation must tell six different stories. 

If you really want to be able to influence your audience, you have to tell them not one, but six different stories.

First, tell them _who_ _you_ _are,_ and second, tell them _why_ _you're_ _there_. This is because people won't trust you until they know the answer to these questions, and if you tell them using stories it will be far more convincing, making you seem more trustworthy.

Third, you have to tell them a story that relates the _vision_ you have — i.e., the long-term goal that you want to move them toward. For example, if you're the CEO of a company, you can't just blurt out, "We must achieve five percent annual sales growth." Instead you should tell them a moving story that inspires your employees to desire that growth — maybe tell them about another company that later became famous.

Fourth, you also need to tell a story that _teaches_ them. For example, if you have just hired a new receptionist, rather than tell him what buttons to push on the phone, regale him with the story of Mrs. Jones, the greatest receptionist you ever met, and how she did the job so immaculately.

Fifth, tell a _values-in-action_ _story._ This means telling a story where the value you want to convey is translated into a real, specific action. For example, if you say to your employees "Integrity is important," it won't mean much. It would be much more effective to tell a story about an employee who once made a huge mistake but was rewarded when she came clean instead of trying to cover it up.

Sixth, tell a "_I_ _know_ _what_ _you_ _are_ _thinking"_ _story_ that will make your audience wonder if you've just read their minds. For example, think about the potential objections your audience members will make, and then raise and deal with those points as part of a story. This will make the audience feel more at ease.

> _"Story lets you instill values in a way that keeps people thinking for themselves."_

### 5. Stories wield a powerful influence because they relax and disarm your audience. 

Now that you know you can influence people with stories, you probably want to know _why_ that's the case.

First, they help you overcome suspicions: people are often distrustful when someone is trying to influence them, but stories allow you to bypass their suspicion because you can frame the audience on your side: your interests and theirs overlap. This will make them more likely to trust you.

Second, stories are effective instruments for making your audience feel as though you know them. These days people crave real human attention, so if you tell a story that touches them and makes them feel acknowledged, they will be more connected to you and more cooperative.

Third, you can take advantage of the fact that people automatically feel more comfortable and relaxed when they hear a story. The instant you say you're going to tell a little story, your audience will relax and become less analytical. It's almost like hypnosis.

And if you tell a good enough story when they are in this state, it could stick in their heads for so long that eventually they won't be sure if they heard it or if it happened to them. If this sounds far-fetched, think about some stories from your early childhood. Are you totally sure they happened to you or did you merely hear about them?

Your story will influence the audience's actions as if it had happened to them.

### 6. You can influence even reluctant or indifferent audiences – if you don’t think of them that way. 

Of course, not all audiences are eager to listen to what you have to say. Often you'll find you have to speak to people that you might find unwilling, disinterested or unmotivated.

So how can you influence them?

The key is to understand that your listeners have good reasons for their opinions, even if they are not in line with yours. It's too easy to think you're in the right and they're in the wrong.

For example, though abortion is a highly polarizing issue, both sides have reasons for their opinions: pro-lifers want to save the life of the unborn child, whereas pro-choicers focus on the life of the mother and the plight of the unwanted baby. If you want to influence either group, you have to acknowledge these reasons.

A second key consideration is that you have to remain positive. If you allow yourself to think that your audience is reluctant or indifferent, that negative emotion will seep into your speech. You want to awaken hope in them, and this can only be done if you yourself are hopeful.

For example, if your story is meant to motivate people to save the planet, don't focus on all the depressing statistics. This will only make your audience feel ashamed, bitter or angry, and none of these emotions lead to action. Only hope does.

Finally, if you find that the audience is very negative, try telling them an extra story that's designed to work around the source of their negativity.

For example, if the audience seems cynical and doubtful of your sincerity, open up and tell them a personal story — this is the closest they can get to first-hand evidence of your sincerity!

Alternatively, if you think your audience resents you for having the spotlight, try to tell a story that highlights the big picture and the goals you share with them.

### 7. You can also influence others by listening to their stories. 

You've seen how important the art of storytelling is, but there's another side to the coin. If you truly want to be able to influence someone, you also need to be adept at _storylistening_. In a dialog you need to genuinely listen to your partner.

This way you'll come to understand not only his opinions and arguments but also his uncertainties and true feelings, because he'll see how closely you're listening and feel comfortable opening up.

Sometimes this is the best chance you'll have to influence someone: listen to their story. Often you'll find that if you just listen to them respectfully, they will begin to reflect on their own opinions and challenge their views, all by themselves. Often they change their position to something closer to yours.

For example, imagine you're a car salesperson, and you want to sell a customer a Toyota, but he says he "hates Toyotas." What story should you tell?

None, yet.

Instead, listen to the customer's story about why he hates Toyotas. This will help him to articulate what concerns him, and will also make him reflect on whether some of his criticisms are unfounded. He may even end with something like, "But that could just be the Toyotas I've driven — the new models might be better."

Once you've listened, you have your second chance at influencing the other person, because he'll want to extend the same courtesy to you. You'll have a focused audience for your own story.

What's more, the customer feels closer to you because you've bonded as he told you his story. This is a great starting point for you to influence them.

### 8. The three don’ts: don’t act superior, don’t be boring and don’t impose negative emotions. 

Though there are many ways to tell a story well, there are also a few surefire ways to make a mess of it. To avoid the latter, keep these three simple don'ts in mind.

Firstly, _don't_ _act_ _superior_ _to_ _your_ _audience._

If you lord it over them, there's a danger that they may see you as some kind of guru whom they'll follow without thinking, and the same perception will make many other would-be listeners turn away. If you're not a guru you'll have a broader audience, so trust your listeners to think for themselves.

It's better to show that you're just like your listeners, so connect to them via shared interests and common experiences. Tell them about your fears, hopes and passions.

Secondly, _don't_ _bore_ _your_ _listeners_.

Everyone knows how boring it can be to listen to a story that either goes nowhere or is far too long. So when you're telling the story, pay careful attention to how it will feel for the audience. Don't go straight to the point and force feed it to your audience. Share some colorful and bizarre details that really entice your audience to follow where you lead them.

Finally, _don't_ _scare_ _people_ _or_ _make_ _them_ _feel_ _guilty._

Negative emotions make people antagonistic and less likely to make lasting changes. Only positive emotions will make people take action or change their mind in the long run.

Abraham Lincoln is one shining example of believing in the power of positivity. When he was told that he should destroy his enemies, Lincoln simply replied, "Isn't that what I do when I make them friends?"

### 9. Becoming a storyteller can change your outlook on life, but it brings great responsibility. 

In the previous blinks, we've discussed how becoming a great storyteller will make you more influential and persuasive in the eyes of others. But that's not all: you'll also notice changes in your own life.

This is because a storyteller sees the world differently. You'll begin to see your life as a story, and you are the person who chooses how the plot unfolds. For example, if you're currently living in a story where you're constantly stressed and frustrated, it's time to rewrite that plot into something more positive.

Once you've found a good story to live, your place in the world will become clearer and your life will seem more meaningful. You'll also begin to look at problems differently, because you've seen how even the most massive problems can be solved.

Being a storyteller will also have a great impact on the relationships you have with other people, because you now carry a great responsibility: The stories you tell will affect the lives of those around you in the long term. So if you tell stories that make the people around you see themselves as victims, or start blaming one another, it can change your family, your company or even your community.

As an example, consider one of the most influential fearmongering storytellers in history: Adolf Hitler. His stories provoked such powerful reactions of fear and hatred in the German people that they perpetrated the Holocaust.

Never underestimate the power and responsibility that come from being a storyteller.

### 10. You need critical thinking, but you also need story thinking. 

These days, it seems the most valued thinking skills in the world are rational and critical. They are taught at school, and they help you get jobs. But in fact there's another kind of thinking that can be very beneficial for a storyteller, namely _story_ _thinking_ : framing problems and situations as stories.

When approaching a problem or situation with purely rational thought, the goal is to remove all ambiguity, anecdotes and emotions from the equation. It's like using a ready-made recipe or formula: you know what you'll end up with, but it definitely won't be anything new or innovative.

Story thinking, on the other hand, actually broadens your horizons and allows you to operate even when there is ambiguity. It encourages you to forget the rules and embrace emotions, which is beneficial when telling stories: you can better interact with your audience by sharing emotions with them.

What's more, story thinking helps you identify stories all around you, and this will improve the stories you yourself tell.

Story thinking also dissuades you from trying to be too objective: our experience of the real world is subjective, after all, so if you try to tell an objective story about it, it won't seem real to the audience.

The fact that story thinking is so free and fluid is what makes it more an art form than a science. It promotes creative intelligence and a better imagination, which is what's needed to enact any change in society.

### 11. Final summary 

The key message in this book:

**Storytelling** **is** **a** **far** **more** **powerful** **way** **of** **influencing** **people** **than** **pointing** **at** **facts** **and** **figures.** **Stories** **can** **help** **you** **reach** **any** **audience** **and** **inspire** **them** **to** **take** **action.** **In** **fact,** **storytelling** **is** **so** **powerful** **a** **tool** **that** **once** **you** **become** **a** **storyteller,** **you** **have** **a** **great** **responsibility** **to** **tell** **stories** **that** **improve** **the** **lives** **of** **those** **around** **you.**

Actionable advice:

**Use** **stories** **in** **your** **next** **presentation.**

The next time you have a presentation or speech coming up, why not base it on stories? Work out the key points you want to make, then try to think of anecdotes that would highlight them for the audience in a clear, relatable way. For example, if you want to promote the benefits of organic food, don't quote dry statistics on phosphates and vitamins. Instead tell a story of how the farmer who grows the produce can hug his children straight after coming in from the fields, because there are no dangerous pesticides on his clothes. You will find your presentation packs a far more powerful punch.
---

### Annette Simmons

Annette Simmons turned a career in marketing and an interest in psychology into a new way of thinking about the age-old art of storytelling. She is an expert in the art of narrative who has given popular storytelling seminars for years. She is also the author of _Whoever_ _Tells_ _the_ _Best_ _Story_ _Wins_.

