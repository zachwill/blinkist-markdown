---
id: 5a6f0c58b238e10009732059
slug: the-story-of-my-life-en
published_date: 2018-02-01T00:00:00.000+00:00
author: Helen Keller
title: The Story of My Life
subtitle: None
main_color: 503341
text_color: 381e2a
---

# The Story of My Life

_None_

**Helen Keller**

_The Story of My Life_ (1903) chronicles the early years of Helen Keller, a young woman who became both deaf and blind at a young age. The book explores the challenges she faced growing up as a child with disabilities, and introduces the amazing people who helped her along the way.

---
### 1. What’s in it for me? Learn about the early years of an influential American icon. 

You're probably familiar with the name of Helen Keller. She's become a touchstone for triumph over adversity, and even featured in contemporary music such as "Helen Keller," by DJ Khaled.

While you may know that Helen Keller was a woman who had a disability, you may not know the details of her story and what made her stick in the hearts and minds of so many people.

Living with disabilities is challenging, and you'll come to understand that it's even harder to deal with when you're growing up.

As a young girl, Keller was already exhibiting remarkable strength of character in the face of adversity. In these blinks, we'll go further into her life as a child and adolescent, which will no doubt inspire and motivate you to achieve what you want in your own life.

In these blinks, you'll find out

  * the struggles involved in growing up deaf and blind;

  * how one teacher saved Keller's life; and

  * Keller's favorite book.

### 2. Since she was a baby, Helen Keller was both blind and deaf. 

Born in 1880 in Tuscumbia, Alabama, Helen Keller would grow up to lead an incredible life that no one had thought possible.

She was just 19 months old when she was struck with a horrible fever.

Keller had started to walk and talk and showed signs of being a fast learner before the illness developed. But when she lost her sight and hearing due to her illness, she had to learn how to communicate by other means.

Basic signals and actions such as nodding her head for yes and shaking it for no allowed her to express herself to others. Fortunately, Keller's mother helped ensure that her daughter understood what was being asked of her, and, at the age of five, Helen was already partaking in daily activities such as folding and putting away the laundry.

You can imagine, however, that growing up deaf and blind can lead to incredible frustration, and understandably, Keller wasn't always the most well-behaved child. She would erupt into fits of rage when unable to express what she wanted. And when something didn't go the way she desired, the consequences were sometimes disastrous.

One day she decided to throw an apron into the fire because it wasn't drying quickly enough. She set herself on fire as a result, though thankfully her injuries weren't too severe.

Keller also acted out in other ways, like when she locked her mother up in the pantry for three hours and refused to let anyone know the whereabouts of the key.

It was at this point that her parents realized she needed to be educated. Yet the difficulty of finding someone qualified in educating a young blind _and_ deaf girl was considerable.

But as we will find out, the right teacher did come along — one who would change her life forever.

### 3. Miss Sullivan was the teacher who transformed Helen’s life. 

After traveling to Washington to meet with the inventor Dr. Alexander Graham Bell, Helen and her parents were told to call the Perkins Institution in Boston, a school for the blind. Although Helen didn't attend the school, she became a frequent visitor, and Mr. Anagnos, the director of the school, was tasked with the job of finding a teacher who would meet her needs.

That teacher was Anne Sullivan.

She came to Keller in 1887 and taught her not only words but the entire concept of language. Miss Sullivan used a method called _the manual alphabet_, which involves using one hand to spell out words onto the other. Helen's first word using this method was "doll." She was instructed to touch her doll, spell the word with her hands, and then repeat the same movements until she perfected it.

Miss Sullivan also taught Keller that both the liquid she was drinking and the substance she felt when she dipped a hand in a stream were the same: water. Once she realized that everything had a name, Helen was eager to learn them all.

But she was soon to discover that things you _couldn't_ touch also had a name.

Keller first encountered abstract concepts when she was trying to work out how to string beads together. Using the manual alphabet, Miss Sullivan touched one hand to Helen's head and spelled out the word "think" into the child's hand.

How would you describe a concept such as love? Miss Sullivan did so using this analogy: even though clouds are out of our reach, rain from those clouds can touch our skin, and the dry ground is surely grateful to receive that rain during an especially hot summer. Without love, explained Miss Sullivan, we would feel neither joy nor playfulness.

It was due to this understanding of what connects us as humans that Keller's world grew monumentally.

Thanks to Miss Sullivan, Keller learned how to communicate effectively, freeing her from feeling frustrated and allowing her to go forth and achieve much in her life.

### 4. Times weren’t always easy, but Helen Keller’s life was full of joy and wonder. 

Keller had many valuable first experiences and loving friendships during her childhood and adolescence, but there were also times of darkness.

At 12, she unintentionally plagiarised a book when writing her homework for the Perkins Institution and was reprimanded for it.

The story she wrote for Mr. Anagnos was claimed to have been copied from another story, though she did not remember ever being told such a story. Keller was placed on a quasi-trial at the Perkins Institution and questioned to determine whether or not the plagiarism was intentional.

She was greatly saddened by the unpleasant experience, especially at having disappointed Mr. Anagnos, whom she loved and for whom she had a lot of respect. It also made her mistrustful of her thoughts and fearful of writing, although, with time, she learned to overcome that fear.

Freed from the doubts and worries, Keller was emboldened to interact with the world, and by age ten she had learned to speak. She had discovered that another deaf and blind girl in Norway — Ragnhild Kåta — could speak, and her determination to do the same made Keller an unstoppable force.

A woman by the name of Sarah Fuller taught Keller to touch her face and feel the shape of her mouth and tongue whenever she spoke, and then have Keller repeat the actions. This gave her a profound sense of liberation as she was now able to connect with others and express herself.

Keller refused to allow her lack of sight and hearing to affect her enthusiasm and joy for everything around her. Unsurprisingly, people wondered whether she could truly appreciate things she couldn't see or hear, like scenic wonders such as Niagara Falls. But Keller explained that she could, in the same way in which we can all appreciate intangible feelings such as love and goodness.

> "_As I talked, happy thoughts fluttered up out of my words that might perhaps have struggled in vain to escape my fingers."_

### 5. Keller’s dream of studying at college came true. 

Keller went to college in 1900. Though her time there wasn't without its struggles, the feeling of reward was everlasting.

First, she went to prep school at The Cambridge School for Young Ladies and, shockingly from a modern perspective, was given almost no special assistance, despite her disabilities. To get by in class, Keller put to use the method of learning taught to her by Miss Sullivan.

During her preliminary exams for college, Keller typed up answers on her typewriter, and then Mr. Gilman, the head of school, would spell them out on her hand. That way she could correct any mistakes she'd made during the exam.

Approaching her final school year, Keller became ill. The school principal saw this as a kind of breakdown and thought it would be too much for her to take her exams with her class, meaning she would have to wait another year before she could attend college. Keller's mother disagreed, pulled her out of the school and gave her a special tutor. She wasn't given any extra time or assistance during her final exams, however, and had to go back and check her answers — if time permitted — just like everyone else.

Furthermore, Keller had been learning a different kind of Braille — the embossed dots made on paper that can be "read" by touch — than the one used in her final exam, and therefore wasn't familiar with the algebraic symbols used.

Incredibly, despite this disadvantage, she passed the college entrance exam.

German and English Literature were her favorite subjects at college.

She was proficient in French and Greek as well and read many books in those languages, but she especially loved German because of its ability to express a story so directly and honestly.

Her most beloved book in English was the Bible, and she loved Shakespeare — particularly _Macbeth_.

She loved literature because when she read, she was just like everyone else. The authors and characters didn't treat her any differently, and their stories brought her to places where she had free rein to explore.

Keller faced many obstacles in her life but, remarkably, she was hardly resentful of them; rather, she found pleasure and meaning in overcoming them.

### 6. Final summary 

The key message in this book:

**Despite starting off on a path much more difficult than most, Helen Keller fought hard to receive the education she wanted. With the help and support of her teachers and others she met along the way, Keller learned to successfully maneuver around obstacles and gained an appreciation of the wonders of life.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _I Can Hear You Whisper_** **by Lydia Denworth**

_I Can Hear You Whisper_ (2014) is about human communication and the phenomenon and culture of deafness. Hearing is a complex process that doesn't function the same way for everyone, and those who are deaf or hard of hearing have developed alternative methods of communication, around which a special culture has grown. These blinks give an overview of that culture and show that it's just as rich as any other.
---

### Helen Keller

Helen Keller (1880–1968) was an American author, speaker and political activist who was part of the women's suffrage movement and spoke up for the working classes as a member of the Socialist Party. She devoted her life to helping others. She also helped found the American Civil Liberties Union.

