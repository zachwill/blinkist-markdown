---
id: 5dfcb4d16cee0700087f97a3
slug: the-storytelling-edge-en
published_date: 2019-12-26T00:00:00.000+00:00
author: Shane Snow and Joe Lazauskas
title: The Storytelling Edge
subtitle: How to Transform Your Business, Stop Screaming into the Void, and Make People Love You
main_color: None
text_color: None
---

# The Storytelling Edge

_How to Transform Your Business, Stop Screaming into the Void, and Make People Love You_

**Shane Snow and Joe Lazauskas**

_The Storytelling Edge_ (2018) is a study of communication by two content strategists who've taken an old Native American proverb to heart — "those who tell the stories run the world." As Shane Snow and Joe Lazauskas assert, it doesn't much matter if you're an individual, a business, or a government: if you want to thrive in today's world, you need to be able to tell your story as convincingly and fluently as possible.

---
### 1. What’s in it for me? A storytelling masterclass. 

In 2012, the American musician Amanda Palmer recorded a video and posted it on Kickstarter. This wasn't your standard appeal for donations, though. 

Standing on a street corner in Melbourne, Palmer held up a series of signs. These told her story — how she'd left her old label and wanted to strike out on her own and actually pay her collaborators for their work. It was an extraordinary success. Over the next 30 days, Palmer received $1.2 million and launched her independent music career. 

Palmer's video is a great example of the power of storytelling. Throughout history, storytelling has driven human behavior — for good and bad. And it's no wonder. Powerful stories make ideas stick and can persuade folks to take action. Whenever you find an effective leader, chances are you've also found a storyteller. 

But here's the thing: creating compelling stories isn't a gift handed down from above. It's a skill anyone can master, and you can start learning now. I In these blinks, we'll unpack the neuroscience of storytelling and look at historical and contemporary examples of great storytellers. 

Along the way, you'll also learn

  * how Renaissance gossip-mongers pioneered the first mass-media content strategy;

  * what American newspaper empires can teach us about storytelling in the digital age; and 

  * why _Romeo and Juliet_ and _Star Wars_ are such compelling stories.

### 2. The human brain is wired to find stories more engaging and memorable than simple statements. 

Many years ago, a French poet named Jacques Prévert happened to meet a blind beggar in the street. Jacques asked him how things were going. Bad, the beggar replied — he hadn't heard a single coin land in his hat all day. Jacques, a poor writer, didn't have cash to spare, but he offered to rewrite the sign that explained the beggar's situation. Two weeks later, the men met again. Things had improved. Folks were generous these days, the beggar said, and his hat was always full. 

So what did Jacques write on the beggar's sign? "Spring is coming, but I won't see it." Why did this help make people more generous toward the beggar? 

**The answer is also the key message in this blink: The human brain is wired to find stories more engaging and memorable than simple statements.**

Imagine a high school health class. The teacher cites statistics about drug-related deaths and concludes that drugs are dangerous. Next door, his colleague takes a different approach. She puts up a slide of a handsome teenager and introduces the class to Johnny. He was a good kid, she says, but he had problems at home and started taking drugs to make himself feel better. She then shows an image of a sickly looking man with missing teeth — it's Johnny ten years later. Drugs, she concludes, are dangerous.

Which class is more memorable? The second one, right? Here's why. 

Neuroscientists have a saying that "Neurons that fire together, wire together." What they mean is that when multiple parts of the brain are working together, we're much more likely to remember due to that cognitive work. 

Logical statements like "drugs are dangerous," for example, engage just two parts of the brain — those responsible for language processing and comprehension. When we hear stories, by contrast, our brains light up like switchboards. Suddenly, we're also processing emotions and images, imagining sensations, and using the part of the brain that's responsible for cognitive planning. 

Now think back to our high school example. In the first class, students processed abstract statements and numbers — tasks the human brain can complete without breaking a sweat but often struggles to recall. In the second class, though, students were giving their gray cells a real workout. They were busy picturing the details of Johnny's life, wondering how his problems compared to their own, and asking themselves whether they would also take drugs if they were in his shoes.

This barrage of neurons makes the second lesson much more "sticky." Whatever their choices, these students will likely think back to poor Johnny if they're ever offered drugs.

> _"When we get information through stories, we engage more neurons. As a result, the story is wired into our memory much more reliably."_

### 3. Great stories are relatable and require both novelty and tension. 

Jack and Jill were next-door neighbors and childhood friends. Later, they fell in love. There were no romantic rivals to come between them, and both were happy to stay in their hometown. Eventually, they married — it just made sense. Their families thought it was a perfect match. They lived happily ever after. 

Is this a cute story? Sure. Is it a great read? Not really — in fact, it's downright dull. 

**The key message here is: Great stories are relatable and require both novelty and tension.**

Most of us are narcissists; we love stories about people like us. 

This is why so many folks read hyper-specific Buzzfeed listicles with titles like, "21 things that could only happen at Stanford" or "25 things you'll understand if you have Asian parents." Millions of people fell in love with _Star Wars_ for the same reason. From the humble kid with big dreams to the bickering robot couple C-3PO and R2-D2, everyone can find a reflection of themselves in the movies' cast of relatable characters. Our Jack and Jill story, by contrast, is pretty unrelatable: things usually aren't that easy. 

Relatability on its own isn't enough, though. Anyone who knows what it's like to be a teenager in a small town or to stack shelves in a store can empathize with Luke Skywalker's life when we first meet him. But no one is going to spend hours watching him working his dreary job on an isolated planet! What this promising setup lacks is _novelty_. 

To keep the audience hooked, Skywalker has to embark on an adventure. The reason we stay hooked is down to evolution. Scans show that our brains light up when we're confronted with something totally new. We owe this response to our ancestors, who lived in a world full of strange dangers. If you weren't alert, chances were you'd end up dead. Millenia later, that same instinct explains why we find films about exotic societies in faraway universes so fascinating — we can't help but focus on the screen 

Then there's tension. Great stories, the Greek philosopher Aristotle argued, open up a gap between "what is" and "what could be." Storytellers close and reopen this gap constantly, and this is what creates tension. Think of _Romeo and Juliet_. The situation resembles Jack and Jill's, but there's nothing boring about Shakespeare's drama. Everything in the starstruck lovers' world works against them realizing what could be. Each step forward reveals new obstacles, propelling the characters and the audience toward the tale's tragic conclusion.

### 4. Great storytellers value fluency above complexity. 

If you run Ernest Hemingway's work through a reading-level calculator, you'll quickly make an odd discovery — he writes at the same level as fourth-graders. That's right, the great Nobel Prize-winning American writer's language is no more complicated than that of the average ten-year-old! Repeat the same exercise with books by J.K. Rowling or Cormac McCarthy, and you'll see similar results. So what's going on here? 

**The key message here is: Great storytellers value fluency above complexity.**

The authors became obsessed with this reading-level experiment. After trying it out on Hemingway and other famous novelists, they started running everything they could lay their hands on through their calculator. Whether they were looking at scientific articles or bestselling fiction, the answer was always the same. On average, the most highly regarded authors on any given subject wrote at a lower level than their peers. 

If that sounds counterintuitive, try thinking about it this way: what these writers value isn't complexity, but _fluency_. 

Fluent storytelling is all about speeding your audience along and discarding everything that gets in the way of that aim. This isn't just something writers do, though. Consider cinema. The editors who worked on _Star Wars_ made George Lucas's story much more dynamic by using a combination of fast cuts and quick transitions. Back in the late 1970s, when the first _Star Wars_ movie was released, science fiction was associated with slow action, ponderous shots, and dramatic pauses. The editors' work changed that for good and introduced the fast-cutting production values we associate with the genre today. 

This change worked for the same reason Hemingway's sparse prose is so effective. It concentrates the audience's attention on what really matters — the story. When you read or watch a fluent storyteller, you're not using your brainpower to think about vocabulary or to work out the significance of lingering 40-second shots. Instead, you're focused on the characters, the dramatic tension, and on relating to a theme. Ultimately, this makes it much easier to absorb information. 

And that's the purpose of storytelling. Whether you're recounting a funny anecdote in a bar, writing a TV script, or telling your story in a blog post, your job is always the same: to move from one part of the story to the next as efficiently and smoothly as possible. This isn't something people will compliment you on if you do it well, either — but that's kind of the point!

> "_When you speak a language fluently, people only notice one thing: what you're saying."_

### 5. Stories make us care about members of our tribe. 

A few years ago, scientists asked a bunch of people to watch a James Bond movie while wearing a medley of gadgets tracking their brain activity, heart rate, and perspiration levels. When the film's hero found himself in a jam, their hearts began racing and their hands became clammy. That was expected. But the scientists also noticed something else. Whenever Bond was in danger, viewers' brains began synthesizing a neurochemical called oxytocin — a kind of "empathy drug."

Oxytocin tells us that we should care about someone. This is an old evolutionary reflex. In prehistoric times, this neurochemical helped our ancestors work out if a figure in the distance was a friend, or a foe who might just steal their mammoth steaks. In other words, oxytocin allowed humans to identify members of their tribe. 

**The key message here is: Stories make us care about members of our tribe.**

So why do our brains start synthesizing oxytocin when we watch James Bond attempting to thwart a dastardly global conspiracy? Well, we see him as one of our own. This is then reinforced by the story being told. The more we watch, the better we get to know Bond, which makes us care more about his fate. That, in turn, keeps us watching. When you look at it in this context, stories help generate empathy. 

This insight isn't just about cinema, though. Consider politics. In 1963, thousands of Americans from different backgrounds congregated in Washington, D.C. They took to the streets after hearing stories about people like Rosa Parks, whose experience captured the injustice of segregation. These stories changed the way folks thought about their fellow citizens and made them care enough to fight for their civil rights. 

The same principle holds in business. In the late 2000s, the Ford Motor Company was struggling. Consumers regarded its once iconic, all-American automobiles as shoddy also-rans compared to the latest Asian imports. The only way Ford could turn things around was to tell a story. 

Ford made a film that began by admitting the company had screwed up — its cars weren't what they had once been. The documentary then introduced viewers to the folks designing and building the next generation of improved automobiles in Ford factories across the United States. These were people anyone might know — they could be your neighbors or family members. In short, they were part of the viewers' tribe. 

It was the first step in Ford's long journey back into Americans' hearts.

### 6. How you publish is just as important as what you publish. 

Sixteenth-century Italy was a turbulent place. Wealthy families vied for power and plotted against one another. Gossip was a useful weapon in these struggles, and it was around this time that the world's first mass media business was born — _avvisi_, or gossip rags. Each day, writers fanned out across cities like Milan to gather rumors, which they then wrote up on fancy new printing presses and posted around town. Avvisi were popular, but there was a problem: writing by hand was quicker. 

**The key message here is: How you publish is just as important as what you publish.**

Milan's inhabitants loved gossip, and that made them impatient. They preferred handwritten pamphlets that same day rather than waiting until the next morning for a neatly-printed version of the scandalous stories. To survive in this fast-paced market, avvisi writers needed to optimize their product. 

And that's just what they did. Soon enough, all gossip rags were being written by hand and posted that same evening. The lesson here is: building a good audience base always follows a pattern, and it has three stages. 

First, you create your content. In Renaissance Milan, this meant rushing around marketplaces, churches, and taverns, collecting salacious rumors, and printing them up. Today, that's as simple as writing a blog article. The second step is to connect with your audience. Think of this as pinning your product to a metaphorical door or lamppost where folks will see it. 

In the last stage, you optimize. Does switching from a complex medium like the printing press to a simple alternative like the quill improve your reach? Or, to use a twenty-first century example, does your audience prefer a smaller number of longer social media posts or a larger number of shorter posts? 

This third step is all about gathering data. Take it from Upworthy, a content-sharing site launched in 2012 that became the world's fastest-growing media company. 

Upworthy's concept was simple: take inspirational content produced by others that had flown under the radar, repackage it, and post it on Facebook. Upworthy would then see whether their version of a story did better than the original. Did more people watch the whole video, or did more people share it? Using that research, Upworthy then tested dozens of different types of packaging until it had the optimal product. It was a perfect strategy to build their audience, and it allowed Upworthy to grow five times faster than any other media company in history!

### 7. When content is everywhere, it pays to go deep. 

America was awash with stories in the late nineteenth century. Big cities like New York were home to dozens of nearly identical newspapers. People's attention, however, was limited, and so publishers resorted to yesteryear's version of clickbait — sensational headlines literally shouted at passers-by on busy street corners. The tactic sold papers, but it didn't win readers' loyalty. It was time for a different approach. 

**The key message here is: When content is everywhere, it pays to go deep.**

Joseph Pulitzer, the owner of the _New York World_, was an ambitious man. He didn't want to operate one of New York's many newspapers — he wanted to run _the_ paper read by New Yorkers. To do that, he needed loyal customers willing to subscribe to his publication and forgo reading those of his competitors. That was a tough sell. This just wasn't a market in which people said things like "I'm a _New York World_ kind of guy" or "I only read the _Journal_." Pulitzer needed to differentiate his product. But how? 

The answer presented itself in the form of a young female reporter named Nellie Bly. Journalism was widely viewed as a man's profession at the time, but Bly didn't let that deter her. One day, she strode into Pulitzer's office and made her case. It was a solid pitch, and Pulitzer hired her. Bly's reporting would be a game-changer for journalism in the United States. 

Unlike her colleagues, Bly wasn't interested in chasing headlines about desperate suicides and dramatic shoot-outs. She wanted something more substantial. In 1887, she pretended to be insane and had herself locked up in one of the city's notorious asylums. For ten days, she maintained her cover and gathered facts. When she was released, she had a stunning multi-part story of neglect, corruption, and brutality. 

It was a sensation and led to nationwide reforms to mental healthcare provision. Desperate to read the latest installment of Bly's account, New Yorkers also began doing something they had never done before — reading just one newspaper. By engaging with a serious topic in depth, Bly helped launch the age of subscription journalism and turn the media landscape on its head. 

In the age of Facebook and the endless churn of superficial content, Bly's example remains as relevant as ever. Her lesson for today's content creators? When your market is dominated by producers focused on _quantity_, your best bet might just be to emphasize _quality_.

### 8. Final summary 

The key message in these blinks:

**We're all hardwired to find stories more memorable than factual statements. That's because storytelling engages more parts of our brains than abstract language does. Whether it's** ** _Star Wars_** **or a Hemingway novel, the best stories tap into this evolutionary trait by emphasizing relatability, novelty, tension, and fluency. But effective storytelling isn't just about what's on the page or on the screen. To influence your audience, you need to pay attention to how you're delivering your message. In today's information-packed world, the best way of doing that is to emphasize quality rather than quantity.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Science of Storytelling_** **, by Will Storr**

Literature and science are often depicted as two entirely different kinds of things. The first, it's sometimes argued, is subjective and "fuzzy," whereas the second is objective and factual. But does this distinction really hold? Is there nothing more to literature than personal taste and the whims of authors?

Will Storr doesn't think so. In a fascinating study of the science behind stories, Storr draws on cutting-edge neuroscientific research to illuminate the inner workings of everything from _Harry Potter_ to Greek drama to _Breaking Bad_. So if you've got the storytelling bug, check out our blinks to _The Science of Storytelling_.
---

### Shane Snow and Joe Lazauskas

Joe Lazauskas is the director of content strategy and editor-in-chief of Contently, a global tech company that helps Fortune 500 companies create compelling stories. Contently was founded by Lazauskas' co-author, Shane Snow. Both are trained wordsmiths with backgrounds in journalism.

© Shane Snow and Joe Lazauskas: The Storytelling Edge copyright 2018, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

