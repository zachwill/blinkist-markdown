---
id: 58540cdee663d400041cec5d
slug: the-strategy-and-tactics-of-pricing-en
published_date: 2017-01-13T00:00:00.000+00:00
author: Thomas Nagle, John Hogan & Joseph Zale
title: The Strategy and Tactics of Pricing
subtitle: A Guide to Growing More Profitably
main_color: E4373B
text_color: BE263A
---

# The Strategy and Tactics of Pricing

_A Guide to Growing More Profitably_

**Thomas Nagle, John Hogan & Joseph Zale**

_The Strategy and Tactics of Pricing_ (2010) reveals the fundamental importance of how you price your products. By exposing common misconceptions, explaining the three dimensions of effective pricing and the five steps you can take to achieve it, these blinks are an essential guide to maximizing your profits through clever pricing.

---
### 1. What’s in it for me? Learn how to price your products in order to maximize profits. 

Have you ever looked at a price tag on a product and wondered whether it was just chosen completely at random? Well, prices result — or at least _should_ result –from thorough studies, which rely on both consumers' psychology and financial analysis, and which are conducted by specialists in pricing.

While this might sound like a lot of work, it's most certainly worth it. Why? Because pricing could be the main driver of success (or failure) when launching a new product. And that's exactly what these blinks are all about: insightful and comprehensive guidance on the art of setting the right price for maximum profits.

In these blinks, you'll discover

  * how Apple managed to overprice their iPhones and still make a fortune;

  * why you shouldn't price your products according to what consumers want to pay; and

  * why understanding the value of your products is crucial.

### 2. The art of pricing is an essential element in today’s economy. 

****You might not have noticed, but pricing has become quite a hot topic over the last few years. Why? Well, as a result of the information revolution, we consumers have become more aware of _and_ more sensitive to pricing.

These days, information on the products or services any company has to offer is at our fingertips. By now, many of us are seasoned online shoppers and are quite accustomed to the process of comparing product after product until we find the one we think is best for our budget.

In light of this, many companies have placed pricing at the heart of their commercial strategy. Take the Apple iPhone, for instance. This revolutionary smartphone was considered overpriced when it was first released.

However, knowing that early adopters and tech-obsessed customers wouldn't mind paying a high price for such a unique product, Apple wasn't deterred. They kept their high pricing and, in doing so, created a reference point for the market sector and for their entire product range. Later on, when Apple slightly decreased the price of the iPhone, customers viewed this as a brilliant bargain, and sales soared once again.

Retail giant Walmart also owes much of its success to clever pricing. Items that everybody needs, such as toilet paper or disposable diapers, are massively discounted, ensuring that Walmart is the first place customers will head to buy them.

Because competitors can't afford to lose their profits on essential products like these, Walmart avoids a price war. And because so many customers are buying their everyday items across a vast network of Walmart stores, the discounts aren't likely to jeopardize their profits. Other items are then priced higher, which allows Walmart to recoup their losses.

Apple and Walmart teach us an important lesson about strategic pricing. It's not about tweaking prices to perfection, nor is it about selling as much as possible; rather, great pricing strategies are focused on increasing profitability in innovative ways.

Unfortunately, many companies fail to grasp this and make plenty of pricing blunders. In the next blinks, find out which pricing mistakes you should avoid.

### 3. Strategic pricing isn’t simple, but it is effective. 

Many companies that use pricing strategies might think they've picked the right approach and have got everything under control. Unfortunately, this is rarely the case; pricing is far more complex than simple strategies seem to indicate.

Take the _cost-plus_ method, for instance. While it's the most common pricing strategy among companies, it's also the hardest to get right. This method involves calculating the costs of production and adding a markup to this cost to determine a retail price.

So, say your company makes T-shirts. Between the rent for your production site at $100 and $100 worth of fabric you'll need to purchase, your total cost is $200. Therefore, if you want to sell 100 T-shirts, they'll cost you $2 each to manufacture.

Through the cost-plus method, you can simply apply a 100 percent margin to your costs and you'll end up with a selling price of $4. But here's the catch: how do you know you'll sell all 100 T-shirts? This is something you can't know for certain.

Other companies create their selling prices based on what their customers have said they're willing to pay. This strategy seems foolproof at first — you're giving your prospects the price they want, so they'll simply have to buy!

But the problem is that most customers have no idea about a product's actual value. When fridges, photocopiers and personal computers first emerged on the market, most people thought they were expendable items, and would have priced these innovations at a price far lower than their real worth.

The final fatal mistake too many companies make is letting competition determine pricing. Undercutting prices seems like a great aggressive tactic to fend off competitors. But it's really a recipe for disaster; competitors can easily match your price cuts, and though your discounts might boost sales, they're unlikely to grow your profits.

Luxury car brands like BMW could decrease their prices to sell more cars, but the increased sales simply wouldn't cover the losses they would incur.

### 4. Value, proactivity and profits are at the core of a successful pricing policy. 

Now that we've identified the methods we don't want to mess with, let's look at pricing strategies that really work. There are three dimensions to successful strategic pricing: _value-based pricing, proactive pricing_ and _profit-based pricing_.

Value-based pricing requires that your prices change _only_ when the value of your product as perceived by your customers changes. So, once the iPhone 7 is launched, for instance, other smartphones won't be able to compete in terms of performance or features. Their value will shrink in the eyes of the customer, and their prices must be discounted accordingly.

While value-based pricing is all about timely reactions, proactive pricing is based on _anticipation_. If you can foresee any major events that will impact prices, then you'll need to develop and implement strategies that can help your company adapt and stay profitable.

You might need to get proactive when, say, a new disruptive technology is about to launch. Anticipating that your customers will expect lower prices as the value of your product decreases, your company could introduce a new loyalty program that rewards returning customers. This adaptive strategy ensures your company has a means to maintain a stable customer base without having to resort to major price cuts.

When it comes to profit-based pricing, the name says it all. This dimension of strategic pricing focuses on generating profits rather than boosting sales. Alan Mulally, former CEO of Ford Motor Company took profit-based pricing seriously. He was adamant that Ford remain profitable, even if this meant the company had to scale down. He gave up market shares and cut the 96 models offered down to 20.

Of course, Ford sold fewer cars as a result — but they also enjoyed great profits. What's more, when the 2008 recession hit, Ford stayed afloat while many of their competitors, such as General Motors, were forced to file for bankruptcy.

Now that we've established the three dimensions of effective strategic pricing, let's go through the five key steps to boost your pricing game.

### 5. To price your product effectively, get to know what it offers to customers. 

What makes a customer willing to pay for your product? Well, it's usually as simple as whether they think it's any good or not! In developing your pricing strategy, your first task is to work out how the features of your product create value for your customers.

But before getting into that, let's take a moment to define _value_. All too often, value is described as the _satisfaction_ a customer receives through purchasing a product or service. This definition describes the _use_ value of a product or service, but it doesn't tell the whole story.

On a sweltering day at the beach, a cold drink could easily have a value of $10. A drinks stand could capitalize on this and charge exorbitant prices for lemonade. But they won't have any luck charging a price based on use value if a grocery store nearby is offering the same drinks for $1.99.

So if use value won't help us in our pricing strategy, what will? Clever pricing strategists use another indicator: _economic_ value. Economic value describes the price of a purchaser's best alternative. In the example above, the best alternative or _reference value_ is $1.99. So, you'd need to price your offer at $1.99 _and_ add a markup based on whatever differentiates your product from the competition.

You could differentiate yourself by lowering your price further, but there are other options too. Through great customer service or striking branding, you can add emotional value to your product to boost buyer satisfaction.

Easyjet differentiates itself from other airlines with its discounted prices. A Rolex watch, on the other hand, is hardly a bargain; but it can offer great satisfaction and pride for conspicuous consumers out there.

Either way, differentiation is complex and can't be calculated with a simple mathematical formula. For instance, a new drug to combat cancer that is 50 percent more effective needn't be priced 50 percent more than its competitors — patients would be willing to pay far more!

### 6. Segment your pricing to ensure your customers all feel they’re getting great value. 

By pricing your products according to their economic value and differentiating them, you'll have completed the first step of the strategic pricing process. The next move is to define an appropriate _range_ of prices.

All markets are made up of _segments_, where consumers are willing to pay varying amounts. Without creating prices tailored to each of these segments, your company will lose money.

Consider a market with three segments: one with a sales potential of 5,000 units where customers are willing to pay $10 per unit; another segment with 20,000 units where customers are willing to pay $15 per unit; and a third segment with 10,000 units and customers willing to pay $20 per unit. A supplier might look at these numbers and decide that by pricing in the middle of the range, at $15, he'd earn the most. But how true is this?

By setting the price at $15, customers only willing to pay $10 will be disappointed and unwilling to buy, while those willing to pay $20 are taking an extra $5 home with them — money that could easily have gone into your profits. So what's the solution?

Instead of offering a single price, you can segment the market by creating different packages suited to each target group.

Airlines give us a great example of segmentation in action. Rather than offering the same offer at a single price, airlines segment by charging more for extras for those who want them, such as more legroom, more attentive service and comfier seats. Those who are willing to pay more for first class will do so, while those who can't afford first class or aren't interested in the extras will buy economy tickets, rather than not flying at all.

In this way, segmenting is effective when certain features are valued by a target group and are unimportant for other customers. A vacation resort might capitalize on this by creating an all-inclusive package for golf players at a higher price for access to the golf course. Meanwhile, families who'd rather just hang out at the pool can enjoy a cheaper family deal with only the features they're interested in.

### 7. Give your customers the information they’re looking for. 

How much effort do you put into researching products you want to buy? Sometimes customers simply have better things to do than read through a long list of specs. So, why not make it easier for them? The third step in the pricing process is all about how you can help clients understand why your product is a cut above the competition.

To do this, you'll first need to assess your customer's _relative cost of search_. This is the effort they have to put in to find out about the pros and cons of your product. If your product is toilet paper, this cost is almost nothing, whereas a new car might take hours of online research.

Relative cost of search is also affected by how much information is already out there about certain products. Information about _search goods_, such as personal computers, phones and cosmetics, is relatively easy to access, whereas information about _experienced goods_ like services requires significantly more research.

Enter the marketer, whose job is to get customers interested in search goods or experienced goods. When it comes to search goods, a marketer will outline the product's main features. These could be the type of camera on a smartphone, or the staying power of a lipstick.

Experienced goods, on the other hand, require more involved marketing. Free samples and trials, such as a free first workout at a gym, allow the customers themselves to assess the benefits and risks of an experienced good.

In both cases, the more information you put out there that demonstrates why your product stands out from competitors, the more customers will get interested. Take Duracell, for example; central to their branding are the stats they use to demonstrate their products' superiority, from how much longer it lasts, to how much money you save when you buy Duracell instead of a competing product.

### 8. Keep in mind that pricing strategies have a powerful psychological impact. 

Did you know that two customers who pay the same price for the same thing can have completely different perceptions of its value? Let's illustrate with an example.

Say there are two gas stations. The first station charges $1 per gallon, plus a $0.20 fee for credit card payments. The second station charges $1.20 per gallon and offers a $0.20 discount per gallon for cash payments. Customers paying cash will pay the same price at both stations — but a customer at the second station will _feel_ like they're winning because of the discount. This psychological dimension of purchasing is not to be underestimated.

Reference prices also have a psychological impact on customers, which price strategists can use to their advantage. For example, an expensive bottle of wine on a restaurant's menu will make all the other bottles look cheap in comparison. So, even if the pricey option may not sell especially well, it will indirectly boost sales of other bottles.

Similarly, smart salespeople always begin their presentations with the most expensive items, even if they aren't in the customer's price range, and then gradually introduces cheaper options. This is called a _top-down selling technique_ and operates according to the same principle as reference pricing.

Even the way you discount products has psychological significance. Studies show that 68 percent of people would gladly leave a store where an item costs $15 and go to one where it costs $10, thereby saving $5. However, if an item costs $125 and $120 in another, less than 30 percent would switch, even though they could save the same amount. Why? Because we think of price differences in relative — not absolute — terms.

Most customers aren't concerned with saving a few dollars if they're already making a big purchase. Hotels take advantage of this when they market more expensive packages as including "free breakfast" or "unlimited access to the pool", which is more attractive to customers than offering $10 discounts, for example.

With the psychology of pricing in mind, we're now ready to take a look at the final two steps of strategic pricing.

### 9. Price sustainably to give your company legs. 

As you get closer to pricing your offer, you'll need a set of rules to guide you; in other words, you'll need a _pricing policy_, the fourth step in this process. Businesses are faced with uncertainty every day. Pricing policies are particularly useful when companies are facing changing situations that are likely to create a need for price adjustments.

Say a company faced with a sudden surge in the price of raw materials is forced to increase their prices. Clients aren't convinced and refuse to pay more than usual. What would you do? You can't risk alienating your clients, and your supplier simply won't budge.

In tough times like these, honesty is really the best policy. This is what airlines do, and while their no-refund policies might seem unfair, they're up-front about it. Customers know what to expect and their rules are accepted.

With the first four steps complete, we're onto the fifth and final stage of strategic pricing: _setting the price sustainably_. This is accomplished in three stages. Start by determining an initial price window with a ceiling and a floor. The floor, or minimum, will be the reference price described earlier.

Next, define the differential value that your price should capture, keeping in mind how long you'll manage to stand out before your competitors follow suit. The higher this value, the higher your initial price can be set.

Finally, start talking to your target market about your prices and demonstrate just how fair they are. Though customers are sensitive to price shifts, they will react sensibly if they understand the reasons why. Don't underestimate the power of an honest explanation!

### 10. Final summary 

The key message in this book:

**Pricing is so much more than setting one "right" price. With the three dimensions and five key steps of pricing in mind, you can ensure that your customers are paying the best price for your business** ** _and_** **feel they're getting the value they pay for.**

Actionable advice:

**Price carefully.**

Take care to ensure you price sensibly when launching a new product or service. You can encourage trials by offering it as a lower price, but you may well lose customers when you raise the price later on. So, look for incentives other than financial ones, such as free gifts for signing up. This keeps the customer happy and maintains the integrity of your sustainable pricing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The 1% Windfall_** **by Rafi Mohammed**

_The 1% Windfall_ (2010) introduces the often-overlooked strategy of price setting and shows how companies can grow even further by making smart pricing decisions. How can a firm not only survive but also thrive amid stiff market competition or even inflationary periods or a recession? These blinks will help you find the path to attracting the customers you want and keeping those you have.
---

### Thomas Nagle, John Hogan & Joseph Zale

Thomas Nagle, John Hogan and Joseph Zale are industry leaders in strategic pricing, having made significant contributions to the theory and practice of pricing, as well as the development of software to aid businesses around the world. Hogan and Zale are partners at Monitor Deloitte, a leading strategy consulting practice based in Cambridge, Massachusetts.

