---
id: 55102d47666564000a920000
slug: the-structure-of-scientific-revolutions-en
published_date: 2015-03-27T10:00:00.000+00:00
author: Thomas S. Kuhn
title: The Structure of Scientific Revolutions
subtitle: None
main_color: 3885AE
text_color: 025C8C
---

# The Structure of Scientific Revolutions

_None_

**Thomas S. Kuhn**

_The Structure of Scientific Revolutions_ (1962) is a groundbreaking study in the history of science and philosophy. It explains how scientists conduct research and provides an interesting (if controversial) explanation of scientific progress.

---
### 1. What’s in it for me? Discover how scientific progress happens through revolutions. 

How does science progress? Through small incremental changes? That's the common view. We think that every scientist — from the philosophers in the ancient world to the hobbyists and tinkerers of the enlightenment, right on up to modern scientists with their computerized models — builds upon, and adds to, the work of those before.

Though this might seem like a nice model of constant, uninterrupted progress, the truth is slightly different. Rather than advancing gradually, science moves forward in revolutions. Each major new idea doesn't build upon previous ones; instead, it supplants them completely. These blinks show you exactly how this happens.

In these blinks you'll discover

  * why two views of science can't share the same world; and

  * why scientists spend a lot of their time mopping up.

### 2. Scientific progress relies on paradigms, the shared frameworks of accepted theories and knowledge. 

Imagine a scientific laboratory. Do you see a frazzled man in a white lab coat, mixing strange chemicals in smoking test tubes, unsure as to how his experiments will turn out?

Well, that's not exactly how the scientific method works. Truth is, experiments are never done randomly; scientists know precisely what they want to test. And often, they can even predict the outcome before they start.

They can make these predictions because of _paradigms_, frameworks of accepted theories and shared knowledge that govern scientific work. For example, Newtonian physics is a paradigm that relies on Newton's laws of motion.

This kind of shared knowledge is crucial, because it gives scientists a foundation to build upon. By accepting some fundamental principles developed by previous research, scientists can concentrate on supplementing established ideas, rather than constantly reestablishing an initial framework.

Because, ultimately, a paradigm is just that — a framework. It can't explain _everything_, since there are always going to be gaps in our knowledge and disjunctions between scientific theory and hard reality.

That's why most scientists spend their time _mopping up_, that is, trying to plug the knowledge gaps and align theory with reality. For instance, Newton's ideas were brilliant, but the equipment he used to conduct his research was rudimentary. And that produced certain ambiguities. In the following centuries, scientists developed equipment and devised experiments to address those knowledge gaps. This rigorous research led to our deeper understanding of Newtonian laws.

And, as mentioned earlier, when they're mopping up, scientists pretty much know what kind of results they can expect. Because the paradigm governs their thinking, they don't really expect to produce a novel outcome that falls outside the theoretical framework.

As we'll discover in the next blink, however, scientists who aren't looking for novelty often stumble upon it anyway.

### 3. Encountering anomalies in their research forces scientists to reconsider existing paradigms. 

We've just learned that when scientists conduct experiments, they typically know what results to expect. However, the outcome doesn't always align with their expectations. But why do unexpected results occur?

Well, in order to address ambiguities in the paradigm, the methodology, instruments and experimental processes become far more sophisticated over time. That's partly because scientists have specialties and devote huge amounts of their time to mopping up relatively small issues in an existent paradigm.

Consider quantum physics, which claims that the universe contains many particles that scientists have yet to discover. Attempting to close this knowledge gap, researchers create incredibly complex and sophisticated instruments. The deeper their understanding of a paradigm, however, the more likely they are to discover anomalies — things that go against their expectations.

And in some cases, scientists will have to delve deeper into these anomalies and make further discoveries that might ultimately disrupt the paradigm.

This process usually occurs in the following way: After an anomaly emerges, a few scientists will examine it and create a hypothesis to explain what went wrong. (Some scientists might ignore the result or be hostile to them, expressing loyalty to the original paradigm.) Then, after a period of experimentation and discovery, new theories will emerge, threatening the credibility of the entire paradigm.

X-rays are a great example of this process. It all started with a mistake: A physicist noticed that an experiment with cathode rays was producing an unexpected shadow nearby. He conducted further research into this anomaly and eventually proved the existence of what we now call X-rays. Since his new discovery shocked the scientific community, a lot of people tried to disregard his discovery. But the evidence was clear, eventually forcing an alteration in the paradigm.

### 4. Scientific crisis occurs when anomalies break down the accepted paradigm. 

We've just seen how new discoveries and anomalies in scientific results can disrupt and alter a paradigm. Such disruptions shift the scientific landscape in a crucial way.

Before the anomaly occurs, scientists conduct _normal science_, which is about working toward closing gaps in an accepted paradigm. During this stage, researchers conduct experiments with specific expectations about the results. In a sense, they're interested in definitively proving what they already know.

This process is a lot like solving a jigsaw puzzle: You already know what the picture will look like when you're finished. The joy and excitement lies in figuring out how all the pieces fit together.

However, when an anomaly emerges in the results of an experiment, a group of scientists may begin questioning the underlying paradigm. The group may be small at first, since most scientists have centered their whole lives round this framework and are reluctant to abandon it. But if it's too difficult to reconcile the anomaly with the current paradigm, then an increasing number of scientists will start looking for alternatives.

And that's when the scientific landscape shifts from normal to _extraordinary science_. At this moment, scientists are no longer treating their work like a jigsaw puzzle, but rather as an opportunity to explore new boundaries and ideas.

For example, Galileo questioned the accepted paradigm of his day, geocentrism (the idea that planetary bodies orbit Earth), after he observed moons revolving around Jupiter. At first, most scientists ignored his views; but then, since his discovery simply couldn't be reconciled with the geocentric model, alternative paradigms were considered (such as Copernicus's theory that the sun was at the center of the solar system).

Whenever an accepted paradigm breaks down, science is in _crisis_. And in the next blink, we'll find out how crisis leads to scientific revolution.

### 5. Scientific change is a revolutionary process: When a new paradigm emerges, the old one must be discarded. 

The amount of scientific progress that's occurred over the course of human civilization is mind-boggling. And most of us believe that this is a _cumulative_ process — meaning, each new discovery simply adds to everything that came before. We believe that, although old ideas are improved upon, they more or less stay the same.

This popular view, however, is actually wrong. Far from being a cumulative process, a paradigm shift is a revolutionary act.

In fact, political revolutions are a perfect metaphor for what happens when a new paradigm emerges. Consider how a political revolution starts: One part of the population observes that their society's institutions no longer meet the needs of the people.

The connection to scientific paradigms is clear: A paradigm shift begins when a group of scientists observe that their theoretical framework no longer accommodates parts of reality. Copernicus questioned geocentric astronomy, for example, because the existing paradigm couldn't explain the movement patterns of planets in the solar system.

Coming back to our political metaphor, revolutionaries understand that, since the change they want won't be permitted by existing institutions, they have to oust them. Instead of resolving their issues within current structures, they reject existing institutions. Thus, there's simply no way for the two sides to peacefully move forward together.

Similarly, there's no way to resolve the differences between two clashing paradigms. Each framework tries to legitimize itself using its own logic. And since this logic contradicts the existence of the other paradigm, compromise or coexistence is impossible.

And at this moment, revolution occurs: A political crisis ends with victory for one side and, for the other, condemnation to the dustbin of history.

The same thing happens in science. For instance, once Copernicus disproved geocentrism, his new theory emerged as the dominant paradigm, and the previous one was discarded. After that, these two contradictory theories simply couldn't coexist. As in political revolutions, one side had to go.

### 6. A scientific revolution radically shifts perspectives, leading to new discoveries. 

After a scientific revolution occurs and a new paradigm emerges, scientists start seeing the world completely differently. Even though they're using the same old instruments they've always worked with, they observe things they never would have noticed in the past.

How can that be possible? Well, paradigm shifts completely upend the scientific community's perspective on reality by making possible new ways of looking at the world. Things that may have once been considered banal or even impossible suddenly become new areas of discovery and significance.

For example, scientists believed for centuries that there were only six planets in the solar system, a paradigm based on what was observable with the naked eye. Even though the invention of the telescope gave scientists the capabilities to see more, the accepted paradigm limited their perception.

So when scientists noticed a strange object in the sky, they identified it as a star, even though it behaved abnormally. But then one day, an astronomer discovered that this celestial object was in fact an unknown planet, Uranus. It was the first new planet that had been discovered since classical times, and it instigated a new astronomical paradigm.

This new paradigm changed what astronomers observed through their telescopes. They discovered dozens of new meteors in the following years, simply because now they knew that there was more out there to discover. In other words, changing their perspective changed what they actually saw.

It's important to note that the scientists aren't interpreting things differently, they're actually seeing new things with their instruments!

This is a crucial point, because interpreting something in a new way isn't a revolutionary shift. After all, interpretation is subjective: Imagine we both see the same thing but I think it's a star and you think it's a planet.

A new paradigm, on the other hand, radically shifts perspectives, leading to completely different ways of perceiving and observing the world, the heavens, and the way they work.

### 7. Final summary 

The key message in this book:

**Scientific progress depends on revolutions. Unexpected discoveries put pressure on old ways of thinking and lead to the emergence of new, more fitting models, consigning the old ones to the dustbin of scientific history.**

**Suggested further reading:** ** _A Short History of Nearly Everything_** **by Bill Bryson**

_A Short History of Nearly Everything_ offers an enlightening summary of contemporary scientific thinking relating to all aspects of life, from the creation of the universe to our relationship with the tiniest of bacteria.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Thomas S. Kuhn

Thomas Kuhn (1922-1996) was a highly influential physicist, philosopher and historian. His most famous work, _The Structure of Scientific Revolutions,_ changed how we think about the history of science.

