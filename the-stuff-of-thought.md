---
id: 5a3bc410b238e10007128b47
slug: the-stuff-of-thought-en
published_date: 2017-12-26T00:00:00.000+00:00
author: Steven Pinker
title: The Stuff of Thought
subtitle: Language as a Window into Human Nature
main_color: EAE744
text_color: 757317
---

# The Stuff of Thought

_Language as a Window into Human Nature_

**Steven Pinker**

_The Stuff Of Thought_ (2007) offers an in-depth look at language and, more specifically, what it can tell us about human nature and the complexities of the human mind. These blinks touch on everything from our ability to unconsciously detect subtle grammatical patterns to the linguistic rules surrounding politeness.

---
### 1. What’s in it for me? Step into the stuff of thoughts. 

Learning languages is a key part of our lives. As infants and children, we all crawled around, pointed at stuff and, at some point, started saying words out loud to describe the things that made up our surroundings.

For most of us, language comes so naturally that, unless we are linguists or language nerds, we take its workings for granted. But, as you'll see in these blinks, not only is language an incredibly complex phenomenon, it can also shed light on human thought and behavior.

Over the course of the following blinks, you'll learn about how humans perceive and act in the world through a close examination of human language. From babies' ability to learn grammatical rules to the historical development of swear words, you'll see why our words provide a fascinating window into our nature. 

You'll also discover

  * why seeing the 9/11 attack as two events instead of one is a distinction worth $3.5 billion;

  * how Bill Clinton used the ambiguity of the present tense to escape trouble; and

  * how the character of Eliza Doolittle scandalized audiences in 1913.

### 2. Even the most tragic events can spur linguistic debates, and words have more practical importance than we think. 

Everybody remembers September 11, 2001, the day two hijacked planes collided with the twin towers of the World Trade Center in New York City. On that day, the two planes crashed in quick succession, the first hitting the north tower at 08:46 a.m. and the second hitting the south tower at 09:03 a.m.

But what does that have to do with language or linguistics?

Well, even highly traumatic events can launch debates about the precise meaning of words. For instance, while it may seem obscure, in the aftermath of this tragedy, a debate ensued about whether the 9/11 attacks on New York constituted a single event or two separate ones.

Here's how the two sides saw it:

The commonly held understanding of the World Trade Center attack as a single event — a coordinated terrorist attack that provoked a series of military and political responses. But it can also be viewed as two separate instances: one attack on the north tower and a second on the south.

Seeing footage of the first tower engulfed in flames while the second remains intact drives home just how separate the two attacks were, at least for 20 or so minutes.

While such distinctions might seem insignificant, they're actually more important than people might believe. After all, words, and the details around them, have a great deal of practical importance in daily life.

And this is particularly the case when it comes to law. Returning to the example of 9/11, the debate between seeing it as a single event or two separate ones held $3.5 billion in the balance; the leaseholder of the World Trade Center, Larry Silverstein, was insured for a maximum of $3.5 billion per destructive event. Therefore, if the attack was considered two separate events, he would have received compensation for both.

So, exploring language and its intricacies isn't just some intellectual game. It can have real-world consequences, and in the following blinks, we'll explore how, using some less tragic examples.

### 3. Babies don’t learn how to speak through imitation, but must instead master the abstract structures of language. 

Have you ever wondered why babies cry so often? Well, one theory is that they're struggling to figure out verbal communication, which is no easy task. In fact, while people often assume that babies and children learn language through simple imitation, that's not exactly the case.

It's true that following the example of others helps infants learn simple words, but it would never be enough for them to learn complex grammar. For instance, you can correctly say "she ate scrambled eggs with bacon" or "she ate scrambled eggs and bacon." But hearing this wouldn't teach a child to ask a question in the form "what did she eat the scrambled eggs _with_?" rather than "what did she eat the scrambled eggs _and_?"

Simply put, language contains too many exceptions and idiosyncrasies for people to remember them all individually, which is why babies have to learn the rules behind grammatical constructions. In English, it's correct to say "I poured wine into the glass," but incorrect to say "I filled wine into the glass." And this difference isn't entirely random either.

A particular type of sentence structure is used to describe that the state of the container has changed. Logically, such syntax can only be combined with a verb that describes the condition of the container changing, in this case being filled. Therefore, we can say "I filled the glass with wine."

However, that same construction doesn't work with a verb that fails to describe a change in the state of the container itself, such as "to pour." After all, you can pour wine anywhere, regardless of whether a container exists to pour it into. That's why you can't say "I poured the glass with wine," but rather have to say "I poured wine into the glass."

All languages are full of such hidden complexities. But can babies really learn these without an innate, pre-programmed ability? You'll find out in the next blink.

> "_In trying to crack a puzzle in how children infer the syntax of their mother tongue, we were forced to reconceptualize what they had to learn."_

### 4. Some people believe that words are elemental and innate, but a closer look shows that they are complex and learned. 

Are you one of those odd people who likes dictionaries? Do you enjoy reading through definitions and learning new words?

If so, you might feel uneasy about the twentieth-century linguistic theory of _extreme nativism_, which states that all words are elemental concepts and definitions can never properly describe them. A major proponent of this idea is the philosopher and cognitive scientist Jerry Fodor.

Let's look at an example to better understand the logic behind this idea:

"To kill" could be defined as causing someone to become not alive. Fodor, however, would argue that such a definition is debatable. He would say you can make someone "not alive" on Wednesday by poisoning him on Tuesday, but you cannot, "kill" someone on Wednesday by poisoning him on Tuesday since there's no active action on the Wednesday.

Definitions often fail in this regard, which is why Fodor assumes that all words are elementary concepts that must be pre-programmed into humans at birth.

That being said, Fodor's theory doesn't quite hold up in that the complexity of the majority of words does seem to originate in more simple concepts.

Just take the verbs to hit, to cut, to break and to touch; they're all quite similar, yet are clearly different from one another. These similarities and distinctions work because all of the verbs are built up from more central concepts, like motion, contact and effect.

For instance, "to hit" always implies motion, which is why you could never use the verb to describe someone leaning against another person, even if a bruise was formed by doing so. In contrast, "to break" implies a particular outcome for an object, without any motion required. That means someone can break a bicycle that she's too heavy for, without hitting it.

The point is that more elemental concepts are essential in explaining and distinguishing similar words. Because of this, it's safe to assume that the extreme nativists were incorrect and that a more plausible explanation is that words are complex structures, built up by combining more simple concepts.

> _"Saying that some concepts are basic, and possibly innate, is not a slippery slope toward saying that_ all _concepts are basic and innate."_

### 5. Simple linguistic devices can be used to many ends, creating ambiguity and getting people into trouble. 

Philosophers throughout history have understood how elusive time is; no sooner have you carried out an action or had a thought than it's in the past, while the future remains forever out of reach. Naturally, this creates issues for language, which, despite describing time with as much accuracy as possible, inevitably relies on imperfect linguistic constructions to do so.

As a result, even something as seemingly simple as the present tense can be used in any number of ways depending on the grammatical constructions with which it's combined. In fact, there are at least two different ways that the present tense is used in common parlance.

The first is to describe something that's occurring in the present. This is actually quite rare and most often employed by sports commentators, as in the phrase "Messi dodges his opponents, he shoots . . . and he scores!"

But the simple present form is also used to describe the habits of people, as in "Shawn runs every day," and to make general statements like "bees pollinate the flowers." This use, rather than describing the literal present, describes a person's tendency to do something, a distinction that creates complications.

That's because while playing with words can be fun, it can also get you into trouble. Consider the famous example of US President Bill Clinton during the Monica Lewinsky scandal. At the time, the president's lawyer gave a deposition, during which he argued that "there is no sex of any kind" between President Clinton and his intern Monica Lewinsky.

When further facts came to light, and it was clear that Clinton had, in fact, had engaged in sexual acts with Lewinsky, the federal prosecutor, Kenneth Starr, charged the president with perjury and obstruction of justice.

However, Clinton argued that his lawyer's statement had indeed been true; after all, when the deposition took place, Clinton was not having sex with Lewinsky.

While this was certainly linguistically correct, it sparked heated debate and did little to restore Clinton's tarnished reputation.

### 6. A clever use of language can produce inconsistencies in human behavior. 

In 2003, when the United States attacked Iraq, some described it as an invasion while others called it a liberation. These variations in word choice were of no small consequence, since the way the action was framed changed people's opinion of it.

This is just one example of how language can deeply affect people's views by framing issues in a different light. In fact, experiments have even shown that people can be easily manipulated through language.

For instance, in 1981, the psychologists Amos Tversky and Daniel Kahneman presented two large groups of doctors with a theoretical dilemma; they said a new flu virus had emerged that would likely cause 600 people to die.

The first group was told that program Z could save 200 lives, while program Y had a ⅓ chance of saving all 600 people and a ⅔ chance of saving nobody. The doctors overwhelmingly voted to go with program Z, the safer of the two.

The second group was told that program X would result in 400 people dying, but that program W offered a ⅓ chance of avoiding all losses and a ⅔ chance that all 600 people would die. The outcomes presented in these options were the same as those for the first group, but had been framed differently. In this case, the majority of doctors opted for program W, the more risky option.

So, why did they make different choices despite being given the same options? The only difference was that, for the first group, the effect of the programs was described as a potential gain, namely that of saving 200 or 600 lives, while for the second, it was framed as a potential loss.

In presenting the options this way, the researchers were playing on the human tendency to avoid losses, rather than seek gains. For instance, if a doctor thinks she can save 200 lives, she'll be satisfied and won't take a risk to potentially save 400 more. However, if she thinks that she's _losing_ 400 lives, she'll suddenly be willing to take a much bigger risk to avoid that loss.

In other words, subtle changes in language can have profound effects. Next up we'll explore this thread further by considering the significance of names.

### 7. Names tend to carry a lot of meaning about a person, but they actually point to something much more basic. 

When you say your mother's name out loud, it likely conjures up all the complexity of her personality and biography, a particular temperament, profession and the opinions you associate with her. But is it her name that's actually pointing to those things?

Well, people certainly ascribe a great deal of meaning to names. Famous people are even listed in dictionaries, as if their biographies were the definitions of their names. Just take _Paul McCartney_. When people say his name, they're not only referring to a British human born in 1942; they're also thinking about a musician, specifically the musician who played in the band The Beatles and wrote famous songs alongside John Lennon.

But that's not to say these are all attributes linked to the name _Paul McCartney_. In fact, when broken down, proper nouns point to something much more core to humans than their accomplishments, and simplifying names in this way is not hard to do.

To understand this, all you have to do is question the assumptions you hold about any given person. For instance, according to one common conspiracy theory, Paul McCartney actually died in a car crash in 1966. After that, according to the theory, a lookalike named Billy Shears replaced him and The Beatles all grew mustaches to improve the disguise.

If you presume this theory is correct, the hypothetical McCartney didn't write all those famous songs, nor was he a part of The Beatles at the peak of their success. And yet it's still intuitively obvious to people that, even if the story were correct, the man who perished in 1966 would still be Paul McCartney. In other words, he wouldn't lose his name simply because it doesn't fit the commonly accepted definition of Paul McCartney.

In this sense, names point to something similar to a person's plain existence, independent of any other qualities.

### 8. Words are dynamic, which means their social acceptability varies over time. 

It's no secret that some words have the power to upset people who perceive them as insulting or dirty. But having a historical perspective means taking a more pragmatic approach to swear words. In fact, some words that were considered acceptable in the past are seen as impolite today.

For instance, in a fifteenth-century textbook, in a section on the female anatomy, the bladder is described as being attached to the _cunt_. Or consider the historian Geoffrey Hughes, who specializes in tracing the evolution of certain words. His studies have turned up all manner of dirty-sounding words like _pissabed, shitecrow_ and _windfucker_. However, in previous epochs, these words referred to perfectly innocent things: a dandelion, a heron and a windhover bird, respectively.

So, words can both lose their taboo and acquire it over time. Another example of the former process comes from the play _Pygmalion_ by George Bernard Shaw, who lived from 1856 to 1950. In this theatrical performance, the character of Eliza Doolittle says, "not bloody likely" during a high-class dinner. At the time it was released in 1913, audiences were scandalized by the phrase, which would strike a modern audience as entirely innocuous.

Because of this change in public reaction, in 1956 when the play was transformed into the much more famous musical, _My Fair Lady_, the playwrights had to add a line in which Eliza Doolittle encourages a horse to "move your bloomin' arse," just to maintain a little touch of the provocative effect Shaw had intended.

And finally, while many contemporary parents get nervous when they hear their kids using words like suck or blow, which they perceive as having a sexual connotation, most adults don't realize the sexual origins of words they commonly use themselves. For instance, sucker alludes to cocksucker, jerk refers to jerk off and even scumbag has a dirty origin, originally being used to describe a condom.

### 9. Politeness is about speaking indirectly and the effects of such speech change as they become common. 

People are generally averse to rejection and it turns out that one of the best ways to avoid it is simply never to ask for anything. But of course, doing so isn't always practical — sometimes you just need something from another person. Luckily, in these cases, you can resort to _politeness_.

It's a commonly used word, but when looked at more closely, politeness refers to a form of indirect speech that couches requests in an apology. For instance, when being polite, many people avoid direct requests; rather, they convey what they desire only indirectly or by implication.

The result is so-called _whimperatives_, which transform normal requests like, "please pass the salt" into vague desires like, "if you would pass me the salt, that would be fantastic." People who use such formulas are in blatant violation of the normal rules of language, since it's obvious that the person who has the salt could _clearly_ pass it and describing the idea of such a thing happening as _fantastic_ is a complete exaggeration.

In this way, politeness is a great way to state the obvious and superfluous, like the observation that there's no salt at your end of the table, which all the other diners can plainly see. So, it's clear that such sentences make an indirect request, but why is doing so considered polite?

Well, indirect requests are perceived as such because they can be ignored. However, the politeness of each question depends on how fresh it is. For example, "can you hand me the salt?" was once considered very polite. After all, the other person had a range of possible options not to pass the salt. She could, for instance, decide the salt was out of her reach or simply change the subject, ignoring the request altogether.

However, as this particular form of indirect question became the standardized polite request, it lost its power. It became just another direct request. That's why truly polite requests have to appear fresh, unexpected and entirely indirect. In this case, a good example would be to say "these mashed potatoes are quite bland, aren't they?"

### 10. Final summary 

The key message in this book:

**Language is largely taken for granted, but it's actually an extremely complex system, the study of which can shed tremendous light on the way human beings perceive and interact with the world. More specifically, understanding language allows us to reflect on identity, perception, morality and etiquette.**

Actionable advice:

**Disempower words by claiming them for yourself.**

Words can, and often have been, used as weapons to attack and humiliate people. Examples include the obvious swear words and slurs like "fuck," "bitch" and many others. However, as history has demonstrated, the perception of these vile insults can be transformed. A great way to do so is to appropriate the words, using them to describe yourself in a positive way. Give it a try and see if you can change the way people perceive the insults they use against you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Language Instinct_** **by Steven Pinker**

_The Language Instinct_ (1994) provides an in-depth look into the origins and intricacies of language, offering both a crash course in linguistics and linguistic anthropology along the way. By examining our knack for language, the book makes the case that the propensity for language learning is actually hardwired into our brains.
---

### Steven Pinker

Steven Pinker is a linguist, psychologist and professor at Harvard University. He's also the author of a number of popular books, including _How The Mind Works_, and _The Better Angels Of Our Nature_.

