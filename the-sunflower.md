---
id: 5746c734b49ff200039c4d6f
slug: the-sunflower-en
published_date: 2016-05-31T00:00:00.000+00:00
author: Simon Wiesenthal
title: The Sunflower
subtitle: On the Possibilities and Limits of Forgiveness
main_color: EEC444
text_color: A18120
---

# The Sunflower

_On the Possibilities and Limits of Forgiveness_

**Simon Wiesenthal**

_The Sunflower_ (1969) is an important exploration of forgiveness — both its possibilities and its limitations. We join the author as he attempts to find an answer to an extremely complex question: Can a Jewish concentration camp prisoner forgive a Nazi soldier on his deathbed? There is a range of opinions — from people like Primo Levi and the Dalai Lama — but is there a right answer?

---
### 1. What’s in it for me? Grasp the complexity of forgiveness. 

Could you forgive someone for colluding in the Holocaust? The thought is staggering, but this is the question that Simon Wiesenthal was confronted with. And he tried to answer it while people were still being led into the ovens, while he was still a prisoner in a concentration camp. What would you do if you were Wiesenthal — forgive or not forgive?

He makes his decision, but the question remains with him and, after the war, he talks and corresponds with more than 50 people: religious leaders, writers and other survivors, trying to understand whether what he did was right, as well as what the true nature of forgiveness really is. Let's explore forgiveness with him, and what it means to give it.

In these blinks, you'll find out

  * the Dalai Lama's ideas on the need for forgiveness;

  * why Judaism cannot forgive murder; and

  * that forgiving sets the forgiver free.

### 2. Some of our experiences and decisions stay with us forever. 

In 1943, many Europeans were dealing with the ongoing death and destruction of World War II. One of these people, a Jewish prisoner at the Lemberg concentration camp in German-occupied Poland, was Simon Wiesenthal.

The possibility of death hung over Wiesenthal and his fellow prisoners every day, who were condemned to hard labor with almost no food. One day, however, Wiesenthal was unexpectedly confronted with a different, more philosophical dilemma.

He was sent to work at a hospital, and when he arrived a nurse pulled him aside and brought him face-to-face with a Nazi soldier named Karl Seidl. Fatally wounded, Seidl lay on his deathbed; he wanted to atone for his sins and confess his crimes to a Jew.

Wiesenthal found himself in a surreal situation. He had to listen to Seidl's life story — a long confession from one of the people responsible for the torture and mass annihilation of his own people.

Seidl was filled with self-pity and remorse as he told Wiesenthal about his idyllic Catholic upbringing and his life story since joining the Nazi party. He was brought up by loving parents, and his father was a Social Democrat who opposed Hitler and the Nazi party. But as a young man, Seidl got caught up in the excitement and joined the Hitler Youth and, later, the SS.

Seidl described the events he felt most guilty about. In a small Russian town, he and his fellow soldiers were ordered to gather 300 Jews — primarily women, children and the elderly — and lock them in a building. The soldiers then burned the building to the ground, shooting and killing anyone who tried to escape.

Seidl was horrified by this event and, soon afterward, received the fatal injury he was now dying from. As he explained to Wiesenthal, he could not die in peace without first being granted forgiveness by a Jew.

The Nazi's plea for forgiveness entailed complex questions about both the power and limits of forgiveness.

So what was Wiesenthal to do?

### 3. Justice and forgiveness are rarely uncomplicated. 

After Seidl had finished his story, Wiesenthal decided to leave the dying soldier without granting him explicit forgiveness.

However, he did offer Seidl understanding and empathy. Despite being repulsed by the atrocities described by Seidl, Wiesenthal held his hand and remained present throughout the confession. Nor did he express anger or spite. And, as a simple gesture of human respect, he helped keep away a fly that was bothering Seidl.

When Wiesenthal returned to the concentration camp, he related the story to his friends and fellow inmates, most of whom assured him that he had done the right thing by not forgiving the Nazi soldier.

One friend said that he would have committed a major offense by granting forgiveness. Another, however, suggested that he should have forgiven Seidl, since the Christian doctrine allows for forgiveness to all who ask for it.

Karl Seidl had also tried to give Wiesenthal his possessions, and though Wiesenthal refused the gesture, he did remember the mother's address from a letter the soldier had written.

So, when the war was finally over, and Wiesenthal was once again a free man, he decided to travel to Germany and visit Seidl's mother.

Once there, Wiesenthal listened as the mother mourned her lost son and departed husband, who had also died in the war.

She described Karl as a kind and helpful little boy, and remained firm in her belief that he had stayed true to his Christian upbringing and done no harm to the Jews.

Wiesenthal made another choice here: He decided not to tell Seidl's mother the truth about the atrocities her son had committed, allowing her to keep this image of her son intact.

But the incident stayed with Wiesenthal, haunting him as he continued to wonder whether or not he had done the right thing.

As we'll see in the next blink, Wiesenthal had to weigh the arguments for and against forgiveness, even in a situation as extreme as this.

> _"No one is bound to forgive. [But]...You showed compassion by not telling the mother of her son's crimes."_ \- Albert Speer

### 4. Arguments against forgiveness emphasize religious beliefs and the choices we make. 

If you've ever been asked for forgiveness, what factors did you take into consideration? This is the question that Wiesenthal asked himself, and he had much to consider.

For some people, simply regretting a wrongdoing isn't enough. And, in the Jewish tradition, the act of murder is never forgivable.

In Judaism, only those who have first been forgiven by the victim can be forgiven by God. Naturally, if someone has been murdered, they are unable to forgive.

Ultimately, Seidl had a choice: to participate in the killings or not. And we now know that Nazi soldiers who refused to kill Jews went unpunished. Also, Seidl offered nothing in his confession to indicate that he would have stopped murdering Jews if he hadn't been wounded.

Furthermore, even on his deathbed and filled with remorse, Seidl failed to see Jews as individual human beings.

Writer Primo Levi pointed out: Seidl asked his nurse to bring him "a Jew, any Jew," as if they're not individuals. And once Wiesenthal arrived, Seidl didn't ask him anything about himself, his personal suffering or even his name. This shows the same mentality that fueled Nazism in the first place: the dehumanization of an entire race.

And even if Karl Seidl wasn't born evil — as some people told Wiesenthal he was — he later performed evil, which makes his acts even more unforgivable.

As Jewish-American writer Cynthia Ozick argues: If someone is born a psychopath, and missing the ability to feel empathy, they can be considered to have more of an "excuse" than others.

Instead, Seidl's path to evil was almost banal: He turned away from the complexities of humanity and goodness and toward the all-answering system of Nazi thought.

So, perhaps the author's silence was the most fitting response; it showed compassion but did not allow Seidl to feel easily unburdened of the weight of his wrongdoings.

> _"I believe that the easy forgiving of such crimes perpetuates the very evil it wants to alleviate."_ \- Herbert Marcuse

### 5. Arguments in favor of forgiveness stress its healing nature and the different levels of accountability. 

So, the arguments against forgiveness were pretty strong, but Wiesenthal also heard plenty of arguments for it.

First of all, forgiveness does not have to mean forgetting or condoning crimes.

And one must also face the question of accountability; the puppets are not the same as the puppet masters, and Seidl was working on orders.

Dith Pran, a witness and survivor of the Cambodian Khmer Rouge genocide, suggests that the Nazi soldier was merely one cog in the machine of death. While Seidl remains guilty of a horrific act, it is not as unforgivable as those who oversaw the evil master plan of Nazi Germany.

It is also important to consider how forgiveness allows us to let go of hate and anger.

Forgiveness has a way of transforming grief, resentment and hate into compassion. If a guilty person shows true remorse and a desire to change, then one can say they deserve forgiveness. Without that forgiveness, the hate and resentment can ultimately create new suffering.

The Dalai Lama shared an example: After serving 18 years in a Chinese prison, a Tibetan monk was asked to name the biggest threat or danger he had faced while incarcerated. Incredibly, the monk's answer was that he was most fearful of losing his compassion for the Chinese.

After all, human history is built upon war and bloodshed, so to build a peaceful future we must learn to forgive one another.

José Hobday, a distinguished Franciscan nun of Native American descent, remembered the anger and desire for revenge she felt when she thought of the genocides and crimes against her people. But one day her mother told her, "Do not be so ignorant and stupid and inhuman as they are… You must learn the wisdom of how to let go of poison."

Forgiveness doesn't just benefit the guilty; it also relieves the person being asked to forgive. Holding on to resentment, anger and hate only keep people emotionally trapped.

> _"Forgiveness is not some nebulous thing. It is practical politics. Without forgiveness, there is no future."_ \- Desmond Tutu

### 6. The question of forgiveness may not have a definitive answer, but it’s important to keep asking questions. 

As we can see, the arguments for forgiveness are pretty strong as well. And what Wiesenthal discovered is that his uncertainty and the range of different answers he was getting was proof that these questions need to be asked.

For Wiesenthal, the best way to get to the heart of an issue like forgiveness is to seek out different perspectives.

For example, Jewish and Christian ideas of forgiveness differ greatly.

Just consider the differing responses that followed a tragic event in New York: a female jogger was raped and beaten in Central Park by a gang of young men who then left her to die.

A Cardinal from the Roman Catholic Church visited the men in prison and told them one thing: "God loves you."

Meanwhile, Dennis Prager, a radio talk show host, asked Jewish rabbis for their response. They said they wouldn't even visit the men in jail, but if forced to, they would certainly not tell them that God loves them. Instead, they would express disgust at their actions.

But whether we adhere to Jewish or Christian beliefs, we must avoid the notion that one can commit sins and then simply ask God for forgiveness.

These different perspectives show that there is no easy answer, and that the best course of action is to keep asking questions.

For instance: How and why did a good person like Seidl come to commit such atrocities against humanity? Why do our societies allow ideologies like Nazism to flourish and atrocities like the Holocaust to occur?

Is there a line between individual and collective guilt? Can Wiesenthal act on behalf of all Jewish people to forgive this man?

In the end, perhaps Wiesenthal's initial silence, his non-answer to Seidl, was indeed the best response. It reflected the impossible complexities of life, humanity and forgiveness.

In fact, life is so complex that maybe we shouldn't always be at peace with our decisions. After all, neither Seidl nor Wiesenthal came to terms with the choices they'd made.

> _"You showed clemency, humanity, and goodness when we sat facing one another, too. [...] ever since that day, it has become much lighter."_ \- Albert Speer

### 7. Final summary 

The key message in this book:

**Forgiveness is complex. Many people have different notions of who has the right to ask for it and who has the right to grant it. Simon Wiesenthal learned that there are no easy answers or pat conclusions. By engaging in many conversations about the possibilities of forgiveness, he also discovered its limits. In the end, the only conclusion is that there is no conclusion to this conversation: We must keep asking questions and find out more about why atrocities happen in the first place.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Man's Search for Meaning_** **by Viktor Frankl**

Originally published in 1946, _Man's Search for Meaning_ details the harrowing experiences of author and psychologist Viktor Frankl during his internment in Auschwitz concentration camp during the Second World War. It offers insights into how human beings can survive unsurvivable situations, come to terms with trauma, and ultimately find meaning.
---

### Simon Wiesenthal

Simon Wiesenthal was a Holocaust survivor who dedicated his life to bringing Nazi war criminals to justice. He has published many books on the topic, including _The Murderers Among Us_ and _Justice Not Vengeance_, and been internationally lauded for this work. The Simon Wiesenthal Center, which fights for Jewish human rights, is based in Los Angeles.

