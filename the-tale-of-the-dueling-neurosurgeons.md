---
id: 56530e9232663500070b0000
slug: the-tale-of-the-dueling-neurosurgeons-en
published_date: 2015-11-23T00:00:00.000+00:00
author: Sam Kean
title: The Tale of the Dueling Neurosurgeons
subtitle: The History of the Human Brain as Revealed by True Stories of Trauma, Madness and Recovery
main_color: B8AA7E
text_color: 78693A
---

# The Tale of the Dueling Neurosurgeons

_The History of the Human Brain as Revealed by True Stories of Trauma, Madness and Recovery_

**Sam Kean**

_The Tale of the Dueling Neurosurgeons_ (2014) is a trip into the fascinating world of the human brain via some of the strangest psychological case studies in history. Until quite recently, neuroscientists could only study the brain by analyzing the thoughts and behaviors of people with aberrant brains. Sam Kean uses these historic case studies to paint a picture of the organ that creates our emotions, personality and consciousness.

---
### 1. What’s in it for me? Marvel at the brain’s mysteries. 

Which parts of your brain are involved in adding two numbers or in recognizing your grandmother? Thanks to modern scanning technologies, scientists don't need to break out a scalpel or even ruin your hairdo to find out. They simply make you lie down in a tube-shaped scanner, switch it on and ask you to do some tests as it runs. Then the scanner's computer translates the results into colorful images that look just like snapshots of your brain in action. 

But things haven't always been this easy. For centuries, many of our brains' secrets were hidden in the dark of our skulls. Yet, modern neuroscience didn't have to start from scratch. Throughout history, physicians have learned about the healthy brain by observing brain-damaged people and tying their bizarre behavior to the cerebral structures that had been injured. Portrayed in historic case studies, some of these unfortunate persons became immortal. You'll learn more about them in these blinks. But more than that, the blinks will help you to understand the intricate and wondrous workings of your own brain. 

You'll also find out 

  * why some people can see with their ears;

  * about liars who can truthfully blame their dishonesty on a vitamin deficiency; and

  * about people who want to divorce their own hand.

### 2. Much of our knowledge about the brain comes from studying the victims of brain damage. 

Throughout most of our history, scientists couldn't see what was happening in people's heads without drilling a hole into their skulls. That's why the pioneers of neuroscience learned about the brain mostly by studying individuals with brain damage.

People with brain disorders often perceive the world oddly or behave in unusual ways. Woodrow Wilson, for example, lost the ability to notice things to his left after he suffered a stroke in 1919. 

Early neuroscientists often conducted their research by performing autopsies on people with brain damage after they died. Autopsies often revealed damage to specific brain regions, so researchers could ascertain what activities those regions were responsible for, based on the person's unusual behavior. 

The famous 1559 autopsy of King Henri II of France was the first breakthrough in this field. It showed that post-mortem examinations could provide us with deeper insight into the inner workings of the brain. 

Henri had suffered a severe blow to the forehead during a joust. He was bedridden for weeks, experiencing hallucinations and painful headaches until he died. 

After the king died, a surgeon named Ambroise Parè and an anatomist named Andreas Vesalius conducted an autopsy on his head — a highly contentious move at the time. This enabled them to uncover the cause of his hallucinations: the king's skull was intact but the injury had caused swelling and tissue decay at the back of his brain. 

Andreas and Vesalius's findings helped establish the credibility of autopsies as a means of scientific research. We owe much of our knowledge about the human brain to unfortunate individuals like King Henri, and the brilliant scientists who studied their brains after their deaths.

### 3. The human brain is made up of three parts: the lower brain, the middle brain and the cortex. 

A human brain might seem like nothing more than a gray lump to the untrained eye, but it's highly organized — down to microscopic level. Let's look at it in detail. 

The _lower brain_ sits at the brain's base and controls basic body functions like breathing, sleeping and circulation. It stretches from the top of the spinal cord up to the brainstem and _cerebellum_.

The cerebellum looks like a "little brain" attached to the back of the main one. It consists of the _pons_ and the _medulla_, which both help coordinate your body movements. 

The _middle brain_ sits at the core and relays information around the brain and body. The middle brain and all other higher brain regions are divided into the _left_ and _right hemispheres,_ which are connected by a set of fibers called the _corpus callosum_. The middle brain contains a set of brain structures known together as the _limbic system_, which plays an important part in memory and emotion. 

More complex cognitive functions like planning and decision-making are carried out in the _cortex_, the wrinkled tissue that covers the brain's surface. The cortex is further subdivided into four main regions that each specialize in different tasks.

Planning and strategizing occur in the _frontal lobes_, whereas language and recognition are processed in the _temporal lobes_. The _occipital lobes_, located at the back of the brain, process vision. The _parietal lobes_ are located on the upper sides of the brain and they process a variety of sensations, like vision, hearing and touch. 

So why was King Henri II hallucinating? His occipital lobes had been damaged.

### 4. Neurons and glial cells are the building blocks of the brain. 

Just like the rest of your body, your brain is made up of cells. Brain cells are called _neurons_ and they build a sensory network that spans your entire body. Neurons collect information to send back to the brain for processing and send orders from your brain out into your body. 

Neurons pick up and transmit information with electrical signals. They consist of a _cell body_, an _axon_ and _dendrites_. The dendrites extend out of the top of the cell body like branches, collecting information directly from your sensory organs or other neurons. 

If the intensity of a dendrite signal passes a certain threshold, the cell body triggers an electrical signal in response. So when you touch something, for example, the nerve endings in your skin send a mechanical signal to your neurons. The neuron's cell body then turn that signal into an electrical impulse and send it down the axon. 

The axon branches out, connecting to the dendrites of other neurons. The signal keeps getting passed along the neuron chain until it reaches your brain. When your brain sends an order to your hand to move, the same process happens in the opposite direction. 

In addition to neurons, there are also _glial_ (or "glue") cells in the brain. Glial cells are less complex than neurons but they're equally important: they feed and stabilize the massive neuron network. 

Charles Guiteau, the man who thought God had commanded him to shoot US president James Garfield in 1881, suffered from a syphilis-induced brain infection that killed off his glial cells one by one. The neurons and blood vessels in his brain couldn't get enough nourishment without them, and it drove Guiteau to the point of insanity.

### 5. Neurons communicate with chemicals and form elastic connections in the brain. 

Neurons translate information from the outside world into electrical signals so that our brains can process them. That's how we have sensory experiences like taste, vision and smell. 

Neurons aren't directly connected to each other, however. There are gaps called _synapses_ between them.

When an electrical signal reaches the axon of the neuron sending it, the axon releases chemicals called _neurotransmitters_ from its tips. These biochemical messengers cross the synapse to the dendrites of the receiving neuron, causing a change in its biochemical makeup.

This change causes the receiving neuron to behave in certain ways. Some neurotransmitters _excite_ the neuron, causing information to be passed on, whereas others _inhibit_ the neuron, blocking the flow of information. 

There are approximately 100 billion neurons in your brain, held together by about 1,000 trillion synaptic connections. The basic structure of these neuronal pathways is in place when you're born, but it remains elastic throughout your life. As you get older, some synaptic connections die off, others grow stronger, and new ones form as well. 

If a person loses one of his senses, his brain often makes up for it by improving the neuronal pathways of the other senses. Some blind people, for example, can use echolocation to "see" their surroundings. They tap their canes or click their tongues and listen to the echo that bounces off the surrounding objects. 

Brain scans of blind people who can use echolocation have revealed that when they hear sounds, their _visual_ cortex is stimulated. The nerve cells in their ears have connected to the brain regions responsible for vision and spatial orientation, providing them with a new kind of super hearing.

> _"Neurons that fire together, wire together."_

### 6. Damage to any region of the brain can have very specific consequences. 

What would life be like if you couldn't tell your mother from the supermarket cashier? People who suffer from _face blindness_ experience that problem every day. They can see perfectly well but they can't distinguish between faces. That's because they have a problem in their _fusiform face area_ (or FFA), the region of the brain located in the visual cortex of the occipital lobe that discerns between faces. 

Brain disorders like face blindness can have highly specific symptoms because most neuron clusters have highly distinct specializations, like the FFA. 

For example, there's a neuron cluster called the "_where-stream"_ of the visual cortex. It processes the location and movements of the objects around you. It's also connected to parts of your brain responsible for sophisticated hand–eye coordination, like drawing a picture or picking up a safety pin. 

The "_what-stream_," on the other hand, connects the visual cortex to the temporal lobes and helps you identify objects. The neurons in the what-stream fire very selectively: most of them only respond to specific lines at specific angles, processed in specific locations of your visual field. This creates a special pattern of active neurons, allowing your brain to recognize something like an apple or your mother's car. 

The _somatosensory cortex_ monitors your body parts. It initiates their movements and monitors how they respond to various sensory inputs. It contains an area connected with each of your body parts: there's a strip of gray matter dedicated to your right leg, another to your left leg, another to your tongue, etc.

So when an amputee experiences sensation in a missing or _phantom limb_, it's because the absent limb is still represented in their somatosensory cortex.

### 7. Our limbic system controls our emotions and helps us make rational decisions. 

Have you ever started crying before you realized what was wrong? That happens because the brain sometimes generates feelings before it processes a rational thought. Feelings are highly useful: they allow you to immediately label incoming information as pleasant, unpleasant, harmless, threatening, important or unimportant. 

Emotions and memories are formed in the limbic system, which is made up of other parts like the _thalamus_, _hippocampus_ and _amygdala_. 

The _thalamus_ sits at the very center of the limbic system. It's responsible for recognizing and labeling sights and sounds. The _hippocampus_ is involved in the process of forming both short-term and long-term memories. The _amygdala_ orchestrates attention, fear and aggression; it also plays an important role in our hunger and sex drive.

Most of this activity is managed by the _frontal lobes_, the area of your brain that helps you make choices and plan your actions. 

We depend both on our emotional limbic systems and rational frontal lobes to make good decisions. The case of Elliot, a man who suffered from a tumor that severed his limbic system and frontal lobes, illustrates the significance of the connection between these regions. 

Elliot's intelligence, emotions and memory remained intact, but he lost the ability to make simple choices like what to eat or which tie to wear. The rational part of his brain couldn't communicate with his limbic system. 

The limbic system also helps guide our common sense and morality. Researchers realized this when an iron pipe shot through the eye socket of a construction worker named Phineas Gage, blowing out part of his brain. Gage survived the accident but transformed from an honest laborer into a reckless gambler. His personality change was probably caused by severe damage to his thalamus and prefrontal lobes.

### 8. Hormones are the brain’s chemical communication system. 

The brain structures that make up the limbic system are special because they don't communicate exclusively with neurotransmitters. They also use _hormones_, a class of biochemical molecules that act less quickly than neurotransmitters but are more far reaching. Hormones travel through the bloodstream rather than across neurons, and they can interact with all kinds of cells. 

Hormones are released by glands in the brain and body like the thalamus, _pituitary gland_ or amygdala. They help regulate your body functions and behavior. 

One of the most important hormones released by the pituitary gland is the growth hormone _somatropin_, which travels through the bloodstream targeting organs and stimulating their cell production, growth and regeneration. 

There are many diseases associated with the overproduction or underproduction of growth hormones. In fact, most forms of dwarfism and gigantism result when the pituitary gland malfunctions by producing too much or too little somatropin. 

Hormones released in the limbic system also play a big role in regulating our moods and emotions. The amygdala, for instance, regulates your adrenalin levels, aggression and fear. 

Patients with _Klüver-Bucy syndrome_ have damaged amygdalas and temporal lobes. They suffer from memory loss, unusually low levels of fear and aggression, and unusually high sex drives. They also tend to have a compulsion to put everything into their mouths. 

The _thalamus_ is another crucial part of our impulse control. Some thalamic lesions cause spontaneous outbursts or laughing or crying, and damage to the thalamus can even change a person's sexual orientation! There are reports of patients with thalamic tumors who suddenly begin to have pedophilic fantasies or urges.

### 9. The human brain is highly delicate and susceptible to all kinds of problems and malfunctions. 

The human brain is so delicate that even a healthy one doesn't always function properly. Any tiny neurological shift in the process of recording, storing, relaying or producing information can have a huge ripple effect, causing something significant like _sleep paralysis_, which many otherwise healthy people experience. 

Ordinarily, when you sleep, many of your muscles are effectively paralyzed by your brain to stop you acting out your dreams physically. When you wake, the paralysis is lifted. During sleep paralysis, however, the brain stem — which is responsible for regulating sleep patterns — brings your dreaming brain into consciousness but fails to unfreeze the muscles. This causes a few frightening seconds or minutes in which you feel like you're locked inside your own body.

Epilepsy is another common form of brain malfunction. There are many variations of epilepsy, but they're all characterized by seizures, muscle rigidity, thrashing and foaming at the mouth. These seizures are caused by damaged neurons that misfire and short-circuit the entire brain. 

Seizures can have very specific triggers, depending on the location of these faulty neurons. They can even be provoked by the smell of a certain perfume or the sight of a Rubik's cube.

Epilepsy is often genetic, though numerous other brain pathologies develop as a result of vitamin deficiencies and malnutrition. 

A lack of vitamin B1, for example, can cause a disease called _Korsakoff's syndrome_. B1 helps digest glucose, which the brain uses to build protective myelin sheaths for neurons and certain neurotransmitters. Alcohol prevents the intestines from absorbing B1, so Korsakoff's syndrome is common among alcoholics. 

Symptoms of Korsakoff's syndrome include heart failure, anorexia, swollen limbs, memory failure and compulsive lying.

### 10. A tiny brain structure called the hippocampus processes all three types of memory. 

Can you remember your first day of school? How to tie a shoe? What about the thirteenth president of the United States? The last word of the previous blink?

The process of forming, storing and retrieving memories involves many areas of the brain, but the hippocampus is the most important one. However, we have different types of memory, and they use distinct neural networks to function. 

Short- and long-term memories, for instance, are formed and processed in the hippocampus, a small structure in the center of your brain. The neurons of the hippocampus ensure that information coming from your immediate surroundings can be remembered for short periods — your short-term memory, in other words. So your hippocampus processed the last word of the previous blink for a short time, but it was then quickly replaced by new information.

The hippocampus also plays a big role in forming long-term memories. Its neurons produce special proteins that strengthen neural connections, so we can consolidate the information our brain deems important. Memories become even stronger when they provoke a strong emotional reaction or we recall them over and over. These consolidated memories are then moved to the back of the cortex for storage. 

Our memories are further subdivided into three basic types. 

First, we have _semantic_ or _declarative memory_ : memory that pertains to facts, like the thirteenth president of the United States or the meal you had for lunch yesterday. 

Then there's the knowledge for performing certain automatic procedures, like tying your shoe or riding a bike. That's called _procedural knowledge_ ; it involves your motor centers in the cerebellum and striatum _._ You can call on most of your procedural knowledge without thinking about it.

Finally, we have _episodic memory,_ which is linked strongly to the limbic system. Episodic memory is the memory of our personal experiences.

### 11. The left and right brain hemispheres specialize in different tasks but work together. 

You've probably heard the folk knowledge that certain people are "left-brained" while others are "right-brained". "Left-brained" people are supposedly better at mathematics and problem-solving whereas "right-brained" people are more artistic.

There's no such thing as a "right-brained" or "left-brained" personality, but it's true that the right and left hemispheres of your brain specialize in different tasks.

The left hemisphere, which is usually more dominant, specializes in language, logical thinking and theorizing. It controls the right hemisphere and serves as the "master interpreter" of all sensory input.

_Broca's area_ sits in the front center of your left brain hemisphere. It helps process language, and damage to it can cause a disorder called _aphasia_, whereby victims can't articulate themselves even though they can understand others.

_Wernicke's area_, located near the back of the left hemisphere, is also critical for understanding language. People with _Wernicke aphasia_ can still produce sounds, but can only talk gibberish.

The right hemisphere, on the other hand, gathers sensory data and specializes in spatial skills, movement detection and facial recognition. It often works in tandem with the limbic structures.

If a person has a problem with the connection between their fusiform face area and limbic area, they may develop _Capgras syndrome_. A victim of Capgras syndrome can still recognize their loved ones, but they don't _feel_ familiar anymore — the emotional connection is gone. Sufferers often come to believe their loved ones have been replaced by duplicates or alien imposters.

The two hemispheres are connected by a set of fibers called the _corpus callosum_. The corpus callosum is crucial because the left side of the brain controls the right side of the body, and vice versa. Everything is connected. In fact, our higher cognitive functioning rests on the ability of the brain hemispheres and body to cooperate.

### 12. Consciousness is a complex process that involves nearly all of your brain. 

The ancient Egyptians believed the heart was the seat of consciousness. That's why they carefully preserved it during the embalming process — while tossing out the brains. 

Two-thousand five hundred years later, we now know that consciousness is seated in the brain, but we still don't fully understand how it emerges there. 

Your consciousness and personality derive from several brain structures and functions. Your memory, emotions and sense of personal agency are all equally important for making you feel like _you_. 

That's why the tiniest problem in any of these areas can disrupt your sense of self. People with a damaged corpus callosum, for example, sometimes suffer from _alien limbs_. They feel like a parts of their bodies aren't actually part of them, because the limbs don't communicate properly with the hemispheres in charge of them. 

Victims of _Cotard's syndrome,_ another bizarre neurological problem, become convinced they're dead, even though they can still talk and walk around. They often even report they've lost their "glow" when they look into a mirror. 

Strangely, a person's sense of self can still remain intact in the face of severe brain conditions. Most amnesiacs, for instance, can still describe their own personality even if they suffer from severe memory loss. 

All in all, the human brain is extremely delicate but also highly adaptive. Scientists are truly only just beginning to unravel the mysteries of the mind. Throughout scientific history, we've learned a lot about our amazing brain structures by studying the consequences we face when those structures have flaws, but there's still a great deal of research to be done.

> _"Consciousness isn't localized; it emerges only when multiple parts of the brain hum in harmony."_

### 13. Final summary 

The key message in this book:

**The field of neuroscience — the brain's attempt to study itself — is still in its infancy. Though we have a lot of research to do, we've learned much about the brain and its various complex structures and processes by examining individual cases of head trauma, madness and disease. Overall, the brain is a delicate organ, but it's highly resilient and has been shown to adapt itself in extraordinary circumstances.**

**Suggested further reading:** ** _Do No Harm_** **by Henry Marsh**

_Do No Harm_ (2014) is the memoir of leading London neurosurgeon Henry Marsh, whose anecdotes and recollections provide an intimate look into the operating room. Marsh has learned that much in his vocation falls within a moral grey area — and that much in life does, too.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Sam Kean

Sam Kean is a contributor to numerous scientific journals, the _New York Times_ and _Psychology Today_. He's published two other bestselling science history books: _The Disappearing Spoon_ and _The Violinist's Thumb_.

