---
id: 5a140003b238e100065296f4
slug: the-tao-of-physics-en
published_date: 2017-11-23T00:00:00.000+00:00
author: Fritjof Capra
title: The Tao of Physics
subtitle: An Exploration of the Parallels Between Modern Physics and Eastern Mysticism
main_color: 832B37
text_color: 832B37
---

# The Tao of Physics

_An Exploration of the Parallels Between Modern Physics and Eastern Mysticism_

**Fritjof Capra**

_The Tao of Physics_ (1975) explores the relationship between the hard science of modern physics and the spiritual enlightenment of Eastern mysticism. These blinks lay out striking parallels between relativity theory and quantum theory on the one hand and Hinduism, Buddhism and Taoism on the other.

---
### 1. What’s in it for me? Discover the link between modern physics and ancient Asian wisdom. 

Science and religion are often considered entirely disparate concepts and a clear line is often drawn between Western "rational thinking" and more intuitive or spiritually oriented Eastern schools of thought. But as you'll see in these blinks, when we compare some of the fundamental notions of the major Eastern religions — namely Buddhism, Hinduism and Taoism — with modern science, especially modern physics, we see many similarities.

Whether it's the basic interconnectedness of all particles or the nature of space and time in Einstein's theory of relativity, it seems that Eastern religions have been presenting similar concepts for millennia.

Although science has made huge strides since the book's first publication in 1975, _The Tao of Physics_ remains a source of inspiration for believers and atheists alike.

In these blinks, you'll learn

  * what sort of paradoxes are found in both quantum physics and Eastern religions;

  * how particles and waves can be seen as a parallel to yin and yang; and

  * why there is no space without time, and no time without space.

### 2. Western science and Eastern mysticism may have clear differences, but they also share striking similarities. 

What do modern physics and Eastern mysticism have in common? At first glance, it may seem like nothing at all. Physics is a science expressed through the highly precise and rational language of mathematics, while Eastern mysticism, encompassing the religious philosophies of Hinduism, Buddhism and Taoism, is a spiritual discipline based primarily on meditation.

That being said, the two domains have compelling parallels, all of which begin with the human notion of "knowledge." Generally speaking, knowledge can be broken into two forms, the _rational_ and the _intuitive_.

While they're clearly distinct, both forms can be seen in Western science and Eastern mysticism alike. For instance, science is widely considered the realm of rational knowledge. It's a practice of measuring and quantifying to classify and analyze material reality.

By contrast, Eastern mystics are more interested in intuitive knowledge that goes beyond intellectual positions or sensory perceptions. They seek a nonintellectual experience of reality that can be obtained through meditative states of consciousness.

Nonetheless, the rational side of physics also enjoys an intuitive component; scientists would never get anywhere without the creativity needed to develop theories and gain new insights. Similarly, there's a rational element to Eastern mysticism.

Much like physicists, Eastern mystics learn through observation. The only difference is that, while a physicist observes through scientific experimentation, mystics observe through introspection.

Yet despite this overlap, there's also a fundamental difference between the fields, specifically that Western philosophy is based on a separation between the body and the mind. After all, Western philosophy, and therefore science, stems from ancient Greek learning, which was premised upon precisely this difference.

Just take the ancient Greek philosopher Democritus, whose atomist school made a clear delineation between matter and spirit. Such an idea became core to Western thought, leading to a dualism between mind and matter.

By contrast, Eastern thought has at its core a basic assumption of the oneness of all things.

> _". . . there is an essential harmony between the spirit of Eastern wisdom and Western science."_

### 3. Physics has transformed over time to reach a conclusion long held by Eastern mystics. 

For Eastern mystics, deep spiritual experiences shape the way the universe is perceived. Interestingly enough, physicists in the early twentieth century felt much the same way as they explored the atomic world.

In fact, modern physics, in general, has revolutionized the worldview put forward by classical practitioners of the field. But first, let's consider what physics was like in its classical incarnation.

In large part, classical physics was formulated by Isaac Newton. The field was based on the idea of absolute space and time, the existence of elementary solid particles, the notion that you can determine the behavior of physical objects and that nature can be described objectively, regardless of the observer's perspective.

Modern physics, however, takes a dramatically different tack, essentially demolishing the classical theory. For instance, according to Einstein's 1905 theory of relativity, time and space are relative. That means we can't speak about one without speaking about the other. Or take his famous formula _E=mc²_, in which _E_ is energy, _m_ is mass and _c_ is the constant speed of light. This groundbreaking discovery established that mass is a mere form of energy.

From there, _quantum theory_ rocked the foundations of classical physics even further by describing the behavior of atoms and particles. Quantum theory holds that the movements of subatomic particles can't be determined with certainty, but rather show "tendencies to occur."

Quantum theory also put forward the revolutionary idea that objective descriptions of nature are actually impossible, since atomic particles are affected by the very act of observing them.

With such new discoveries in mind, modern physicists had to contend with a number of paradoxes. But Eastern mystics also recognize such quandaries. This is most apparent in the _koans_ of Zen Buddhism, a series of nonsensical riddles that are impossible to understand through thought alone. A good example is the question, what is the sound of one hand clapping?

Such koans are in fact designed to stop thought processes altogether, thereby preparing the subject for a higher state of awareness. It's only by entering this elevated state that the riddle can truly be solved.

So, now you know how physics bears some resemblance to Eastern thought. In the next blinks, you'll learn about the specific schools of Eastern mysticism in greater detail.

### 4. Hinduism and Buddhism share many key features, from the oneness of life to spiritual enlightenment. 

You've likely heard about Hinduism and probably don't know much about it. That's only reasonable, since Eastern religion and philosophy generally have a rather esoteric aim: the attainment of deep mystical experiences.

Hinduism emerged in ancient India and uses as its main spiritual guide the _Vedas_, a collection of scriptures written between 1500 and 500 BC by a variety of anonymous sages. The religion's core message is that everything and every event in the world is simply a different expression of the same ultimate reality.

This reality is known as _Brahman_, which approximately translates to the soul or inner essence of all things. However, Brahman is actually quite incomprehensible intellectually; it eschews any concept or image and can only truly be experienced through liberatory practices like yoga.

Liberation in this sense refers to realizing the unity and harmony of nature — understanding that all Earthly phenomena are part of the same reality. In this way, the Hindu view of nature is one of a dynamic world, in which all shapes and structures are fluid and ever-changing.

Similarly, in Buddhism, there's a notion of unity, the interrelation of all things and of constant flow and change. For centuries, this religion has been the dominant spiritual practice in much of Asia, including China, Korea and Japan.

While it's similar to Hinduism in this sense, it differs in that it traces back to a single founder, Siddhartha Gautama, or the "historic" Buddha. In Buddhism, the _Dharmakaya_ is similar to the concept of Brahman in Hinduism.

There's also a concept similar to that of liberation. In Buddhism, this experience is known as _nirvana_, a state in which false notions of separateness vanish, laying bare the oneness of all life. And just like in Hinduism, this state of consciousness evades intellectual description.

Buddhism even shares the notion that the world is perpetually changing. Or, to put it in the words of the Buddha, that "all things arise and pass away."

So, that's Hinduism and Buddhism. Next up you'll learn about ancient Chinese spiritual thinking.

### 5. In old Chinese thought, notions of unity, as well as of continuous flow and change, also predominate. 

There are two basic aspects of ancient Chinese thought. First, since the ancient Chinese were a practical people, their philosophies centered around social life, morals and government. But to complement this pragmatic focus, ancient Chinese philosophy also has a mystical side, which offers transcendence of the social world into a higher state of consciousness.

In the sixth century B.C., these two sides developed into distinct philosophical schools. The first, _Confucianism_, revolves around social organization, while the second, _Taoism_, is all about the observation of nature.

Since Taoism is the more mystical of the two, it lends itself more readily to comparisons with physics. Created by the "Old Master," Lao Tzu _,_ Taoist thought teaches that there's an ultimate reality underlying and unifying the world. In this respect, the religion is very similar to Hinduism and Buddhism.

In Taoism, this ultimate, undefinable reality is called the _Tao_, or "the Way," which is basically the equivalent of the Hindu Brahman and the Buddhist Dharmakaya. Similarly to those concepts, the Tao is considered the essence of the universe.

However, as the name "the Way" suggests, it's also perceived as a cosmic process that describes the inherently dynamic nature of all things. Also similarly to the other two religions, continuous flow and change are essential concepts.

That being said, in Taoism, there are patterns to this change, and a principle characteristic of the religion is its cyclical nature. For instance, Lao Tzu said, "going far means returning."

This cyclical focus becomes yet more apparent in the opposing concepts of _yin_ and _yang_, both of which are core to the religion. In fact, the Tao is generated by the dynamic interplay of these polarized forces, representing light and dark, male and female, firm and yielding and heaven and earth.

And finally, just like Hinduism and Buddhism, Taoism sets out to attain intuitive wisdom, rather than hard, rational knowledge. Such a similarity brings us back to the comparisons between Eastern mysticism and modern physics, which we'll dive back into in the next blink.

### 6. Eastern mysticism and modern physics both believe that the universe is bound by a basic unity. 

People tend to divide things into distinct experiences, objects or events. We distinguish between table, chair and soul to help us cope with and navigate our everyday environment.

However, for the Eastern mystic traditions of Hinduism, Buddhism and Taoism, such distinctions are illusions, none of which are a fundamental characteristic of nature. Rather, for these religions, the universe is characterized by a basic unity.

Interestingly enough, such a notion of basic oneness is also central to modern physics. One of the most famous examples comes from atomic physics and its concepts of particles and waves. In this context, a wave is a periodic vibration pattern that exists in time and space. The motion of a wave can be described by how rapidly it oscillates. In contrast, a particle has a well-defined location at any given point in time and its motion is described by way of its velocity.

However, according to modern physics, matter has a dual aspect at the level of the atom, combining to be both a particle _and_ a wave. As a result, depending on the situation, matter can act like either of the two.

For instance, light exists as _photons_, or light particles. But when these photons travel through space, they are observed as vibrating electric fields, evidencing the key characteristics of waves.

In other words, in atomic physics, reality appears to transcend opposing concepts; similarly, in Eastern mysticism, yin and yang are opposing aspects of a unified whole.

And that's not the only similarity between the two. Modern physics, just like Eastern mysticism, recognizes how interconnected nature is.

For physics, this comes in the form of a realization that the properties of atomic particles can't be defined independently of the process of observation itself. This implies that modifying the way observation or measurement occurs will change the properties of a particle. In this sense, the observed particle is a manifestation of the interaction between an observer and the object of her study, a fact that points to the relational nature of all things in the universe.

> _"What we observe is not nature itself, but nature exposed to our method of questioning."_ \- Werner Heisenberg

### 7. Einstein’s theory of relativity unifies space and time, further connecting physics to Eastern mysticism. 

In classical physics, space and time are clearly delineated concepts. According to this school of thought, humans live in three-dimensional space, which is independent of time.

In modern physics, however, no such separation exists; Einstein's theory of relativity unifies both space and time, leading to a new view that all measurements of the two concepts are relative.

This was a groundbreaking discovery. After all, the idea that spatial observations depend on the observer's relative position in space has long been accepted. For instance, a tree looks small from a distance, but huge when it's right in front of you.

But the same was not accepted of time. Einstein broke this assumption wide open by showing that time is relative as well, since light takes time to travel from its source to the human eye. Therefore, the further away from an observer an event takes place, the later it occurs.

Beyond that, relativistic physics has now shown that a fourth dimension of time must be added to the three dimensions of space. This dimension, rather than being independent from those of space, is relative to them.

As a result, it's impossible to speak about time without speaking about space and vice versa. Now, space is no longer a three-dimensional entity, independent of time, but rather inseparably joined with it in a concept called _space-time_.

This unifying concept clearly ties modern physics to Eastern mysticism in yet another way. In fact, Eastern mystics have a powerful intuition for space-time.

Consider the _Avatamsaka_ school of Buddhism. The _Avatamsaka Sutra_, upon which this school was founded, describes enlightenment as an awareness of the "interpenetration of space and time." The famous Japanese Buddhist author D.T. Suzuki even explains that, in enlightenment, "there is no space without time, no time without space."

### 8. According to physicists and mystics alike, the universe is a dynamic, constantly transforming place. 

To the Eastern mystic worldview, the notion of the world as a dynamic, ever-changing place has long been an essential feature. And now, for modern physics, and quantum physics in particular, the universe can be understood in a largely similar way.

For instance, according to _quantum physics_, or the study of the atomic and subatomic world, particles are in perpetual motion. The reason for this is that whenever subatomic particles find themselves confined to a small section of space, they begin to move around. Not only that, but the smaller the area, the faster they move.

Such a reaction indicates what can be called a _fundamental restlessness_ of matter at the subatomic level.

Furthermore, modern physics also shows that the universe itself is constantly expanding and is in continuous motion. This expansion was first observed in 1929 by the American astronomer Edwin Hubble, and has since become one of the foundational discoveries of modern astronomy.

As a result, physicists today believe that the universe was born around 10 billion years ago, emerging out of an explosive event now known as the _big bang_. Some physicists assume that this expansion will go on forever, while others hypothesize that it will slow down, and that the universe will eventually begin to contract. The former of these models describes an oscillating universe — one that both expands and contracts.

And finally, the theory of relativity states that subatomic particles have an inherently dynamic quality to them. Let's return to Einstein's famous formula, E=mc², which states that energy and mass are the same.

But it also implies that we can observe a continuous performance of energy transforming into matter, and matter into energy. For instance, when a tree is burned, the wood is turned into energy in the form of heat, while also producing matter in the form of carbon dioxide, water vapor and a number of other molecules.

> _"The whole universe is thus engaged in endless motion and activity; in a continual cosmic dance of energy."_

### 9. Modern physics unifies matter and space, in yet another parallel to Eastern mysticism. 

The full and the empty, matter and space — in classical physics, these two concepts are clearly separated. However, in the nineteenth century, with the discovery of the concept of the _field_ by Michael Faraday and James Clerk Maxwell, such an understanding began to shift.

The first field to be discovered was the _electric field_, a condition in space induced by a charged body that exerts force on any other charged body in the same space. However, the real rupture came when this concept of the field became associated with the force of gravity.

As a result, for modern physics, matter and space are no longer separated. Here's why:

According to relativity theory, a gravitational field occurs naturally in the curved space around a massive body like a planet. That being said, the field doesn't "fill" this space and "curve" it; rather, the field _is_ the curved space.

In other words, in Einstein's theory, matter is one with its gravitational field and gravitational fields are inseparable from the curved space in which they exist. In this sense, matter and space cannot be divided, but are rather a single, unified whole.

Beyond that, this concept of a field can be applied to both massive objects like the sun and to the subatomic world of particles. In the latter case, physicists speak of _quantum fields_ in which the distinction between particles and space is transcended. In such fields, particles are simply local, temporary concentrations of energy and disturbances of the field itself.

And incredibly, even the quantum field has a parallel in Eastern mysticism. In the Eastern concept, the underlying reality of all phenomena is a formless, empty void. For Hinduism, the ultimate reality of Brahman is described as "the void," as is _Sunyata_, its equivalent concept in Buddhism. Meanwhile, for Taoists, the Tao, or essence of the universe, is described as formless and empty.

However, Brahman, Sunyata and the Tao aren't just the void — they also embody endless creative potential. Just like the quantum field of physics, they give rise to an innumerable variety of forms.

### 10. Final summary 

The key message in this book:

**The hard rationality of modern physics bears some striking similarities to the spiritual flow of Eastern mysticism. Although the two have clear differences, they also share two fundamental, underlying motifs: the basic oneness of all things and events, and the inherently dynamic nature of the universe.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Physics of the Impossible_** **by Michio Kaku**

Just how unrealistic is the technology we see in sci-fi novels and television shows? In _Physics of the Impossible_ (2008), renowned physicist Michio Kaku takes a mind-bending look into how far away we really are from such fantastical notions as starships traveling faster than the speed of light or teleporting to different planets.
---

### Fritjof Capra

Fritjof Capra holds a PhD in theoretical physics from the University of Vienna. He is a prolific author and lecturer on the philosophical aspects of modern science. Capra's other books include _The Turning Point_, _Uncommon Wisdom_ and _The Web of Life_.

