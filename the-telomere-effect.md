---
id: 593485fab238e10008fda20b
slug: the-telomere-effect-en
published_date: 2017-06-05T00:00:00.000+00:00
author: Elizabeth Blackburn and Elissa Epel
title: The Telomere Effect
subtitle: A Revolutionary Approach to Living Younger, Healthier, Longer
main_color: E69E34
text_color: 996923
---

# The Telomere Effect

_A Revolutionary Approach to Living Younger, Healthier, Longer_

**Elizabeth Blackburn and Elissa Epel**

_The Telomere Effect_ (2017) explains why some people look and feel younger than others. These blinks walk you through the science of telomeres, which are at the cellular root of the aging process. You'll learn how it's possible to do right by your telomeres and live a longer life.

---
### 1. What’s in it for me? Understand why we age and how we can slow the process. 

According to the Old Testament, Methusaleh and the early patriarchs lived for many centuries. But with each new generation, the patriarchs lived shorter and shorter lives until they reached the life expectancy we know today. We have long been obsessed with aging, staying young and living long past the normal lifespan — stories about it crop up in our holy books and folklore. But, short of finally locating the mythical Fountain of Youth, what should we do to live longer and healthier lives?

These blinks cover the greatest breakthrough in our understanding of aging: telomeres, a component of cell structure that affects how and at what rate we age. We will learn where they are found and how they work.

In these blinks, you'll also find out

  * how we can help our telomeres extend our lifespan and improve our health;

  * why some types of stress are good for telomere health while others are not; and

  * how obsessing over weight can have more negative impacts on telomeres than actually being overweight.

### 2. The pace of aging depends on a specific cellular structure. 

Lots of people fear getting older, but is this just a product of the youth culture we live in or a valid fear of aging's effects on the body?

For many people, it's probably the latter. A quick overview of the process of aging shows why it's justified.

The cells that compose our bodies have to regenerate regularly to keep us healthy. Some cells, however, can only be renewed a limited number of times. These are known as _senescent cells_. When they're damaged, they send out inflammatory signals to other cells and body parts, damaging healthy structures, causing aging and making the body function poorly.

This dynamic can be likened to a barrel of apples: just one piece of rotten fruit can affect all its healthy neighbors.

That being said, how old a person looks and feels is also determined by the length of the _telomeres_ in her cells. Telomeres are compound structures attached to the ends of chromosomes that help protect the cell. They get shorter every time a cell divides.

This is especially problematic when it comes to _stem cells_, which have the potential to become all manner of different specialized cells in the body. They can divide continuously throughout a person's life. In adults, they can be found in many different tissues and play a vital role in repairing the body by replacing damaged cells. Stem cells thus keep people feeling healthier and looking younger. However, if the telomeres of these cells shorten, they go into early retirement, meaning they can't replace unhealthy cells as necessary.

Because of this, shortened or damaged telomeres affect how old a person looks. For instance, damage to the telomeres in skin cells due to UV exposure from the sun or through genetic mutations, can cause hair to gray prematurely. Ultraviolet radiation can even damage the stem cells in a hair follicle itself, thereby killing _melanocytes_, which add pigment to the follicle.

In general, people with shorter telomeres in their cells are sicker and weaker. Now that we've learned what telomeres are, it's time to learn precisely how they work.

### 3. Telomeres protect cells when they divide and, with the help of an enzyme, can regrow themselves. 

Without the support of her defenders, a soccer goalie would be prone to easy attacks, and cells are basically in the same situation.

For cells, the goal keepers are telomeres, which protect the chromosomes as cells divide. The DNA of telomeres is structurally different from the genetic DNA on the chromosomes, which determine a person's physical traits, like eye color and height.

Telomeres defend the ends of the chromosomes they're attached to, enabling cells to divide and copy their DNA safely.

So, telomeres protect cells, but what happens when they themselves get damaged in the process?

Thanks to a simple enzyme called _telomerase_, they can actually be restored. This process began to be understood in 1978 when one of the authors discovered that the telomeres of the _tetrahymena_, a single-celled organism that lives in pond scum, could actually grow. This realization in turn led to the discovery of the telomerase enzyme.

Here's how it works:

Telomerase builds new telomeres by restoring DNA lost during cell division. It also has the ability to slow, prevent and even reverse the shortening of existing telomeres.

To do so, it includes proteins and RNA, which enable this enzyme to form the correct sequence of DNA necessary for the cell in question.

However, while humans produce some of this enzyme, they don't produce anywhere near as much as those microscopic pond dwellers. Beyond that, the limited supply we _do_ have decreases as we age.

Because of this, scientists are researching new ways to artificially boost telomerase levels and, even without scientific proof, there are a handful of supplements already on the market. It's important to approach these products with caution since too much telomerase isn't such a great thing. An excess of the enzyme can actually fuel uncontrollable cell growth, a key characteristic of cancer.

But what's maybe more important is that telomeres can only protect us so much. We have to also help protect them, and thus our health, from many harmful external factors. Like stress.

### 4. Stress is a killer but you can protect your body by simply responding to it differently. 

Have you ever found yourself stuck in a nasty traffic jam, with your heart beating like mad?

Such stressful situations put your body on high alert as stress hormones like _cortisol_ and _epinephrine_ flood into your bloodstream, causing your heart rate and blood pressure to rise.

Meanwhile, the _vagus nerve_, which is responsible for helping the body deal with physiological reactions to stress, stops functioning properly. As a result, it becomes harder to breathe.

As you have this same experience repeatedly over time, your telomeres become shorter and shorter. In this way, stress and telomeres have what's called a _dose-response relationship_.

Small amounts of typical stress don't pose any danger to telomeres. In fact, little bits of stress can actually help build the muscles that cope with stress and increase cellular health in a process known as _hormesis_. Even high doses that only last a short while — like a month-long crisis at work — are unlikely to be harmful in the grand scheme of things.

However, high doses over years definitely are. It's just like drinking alcohol; a glass of wine every once in a while might be good for you, but a bottle a night certainly isn't.

A good example of this type of toxic stress is that caused by being a longterm caregiver. People in such roles are frequently woken up in the middle of the night, while also having to deal with disrupted schedules and multiple medical appointments — a basic recipe for the premature shortening of telomeres.

Another factor in this equation is challenge versus threat. Or, to put it differently, "good stress" versus "bad stress." Simply put, you can protect your telomeres, and your health, by simply viewing stressful situations as challenges, instead of threats.

Just take a recent decade-long study by the psychologist Wendy Mendes. The experiment found that the body rallies its forces to deal with good stress, but shuts down when faced with bad stress. For instance, when you're experiencing good stress, your heartrate goes up, leading to more oxygen in your blood, which, in turn, boosts your performance.

### 5. The way you think affects the health of your cells. 

It's only normal to blow problems out of proportion, but doing so regularly could actually shorten your life. That's because negative thought patterns are bad for telomeres.

Just take _cynical hostility_, an inclination toward anger and a distrustful mentality. It often results in bad habits of overeating, drinking and smoking, thereby causing disease and shorter telomeres.

In a 2013 study published in _Biological Psychiatry_, British civil servants who ranked high on the scale for cynical hostility were found to have shorter telomeres. What's more, they also had elevated levels of telomerase. In other words, the telomerase wasn't doing its job correctly.

Being pessimistic puts you at risk for shorter telomeres because it makes you feel threatened in stressful situations. As a result, when such people develop diseases of old age, like heart disease, the illness progresses more rapidly.

Another negative thought pattern is mind wandering, which occurs when people aren't happy with whatever it is they're currently doing. This also causes stress in the body, which in turn leads to shorter telomeres.

And of course, depression and anxiety are terrible for your cellular health. They don't just affect telomeres, but also _mitochondria_ — the cell's "power plants" — and triggers inflammatory processes in the body, causing diseases to progress more quickly.

Just consider the cellular aging of the brain, which is one result of depression. Scientists have found a link between depression and decreased brain volume. The cells of the hippocampus, which plays a key role in forming memories, are primarily affected by this process.

But what causes anxiety and depression?

Primarily major life events, and the more recent the event, the greater the threat to telomeres. According to the Netherlands Study of Depression and Anxiety, published in 2008, big events from the past five years correspond heavily to telomere length, while those from further in the past do not as the telomeres will, by then, have had time to recover.

### 6. Exercise and rest are key to building up your telomeres. 

The next time you're contemplating a trip to the gym, keep in mind that there's more at stake than your waistline. Exercise is also essential to the health of your telomeres, but too much of it can cause problems.

In other words, healthy telomeres depend on moderate workout regimens. Exercising correctly will lower your chances of having high blood pressure, experiencing a stroke, or falling prey to dementia.

Just take a study done at the Saarland University Medical Center in Homburg, Germany, on three different types of exercise. Two of them were found to promote the activity of telomerase: high intensity interval training and moderate aerobic exercise. The third, resistance training, had basically no effect on telomerase.

But it's also important not to overdo it. That's because exercising too much can create _oxidative stress_. In this process, a free radical molecule that's missing an electron, thus making it unstable, can steal an electron from a healthy molecule, thereby destabilizing it in turn and damaging telomeres.

The result can be _overtraining syndrome_, caused by too much exercise with too little rest and recovery. It disrupts sleep, causes a susceptibility to illness and a tendency toward moodiness.

In fact, in general, poor quality of sleep can result in shorter telomeres. Here's how:

The brain, which regulates the body's internal clock, requires regular sleep patterns. When you're asleep, your cells enter a restorative process that includes DNA repair. If this restoration is thrown off balance, your cells function less efficiently and become more prone to stress.

Beyond that, deep sleep is also crucial for regulating appetite. Without it, cortisol and insulin levels can skyrocket, essentially putting your body into a temporary pre-diabetic state.

Because of this, longer sleep means longer telomeres, and at least seven hours is needed. A 2012 Whitehall study found that men who slept five hours or less per night had shorter telomeres than those who slept more than seven hours. This correlation means that insufficient sleep and chronic insomnia can damage telomeres while causing you to be hungrier and more emotional.

### 7. Metabolic health is much more important than overall weight. 

Everybody knows that being overweight isn't great for your health, but as long as you're not obese, a few extra pounds won't affect your telomeres. Instead, the real killer is poor metabolic health.

That includes things like high cholesterol, high blood pressure and insulin resistance. Such conditions put you at increased risk for heart problems, cancer and diabetes, which makes them way more important to telomere health than your weight.

Just take belly fat and insulin resistance. Your _Body Mass Index_, or _BMI_, doesn't say much about how much fat you have compared to muscle, or where that fat is being stored. In this way, it's a poor indicator of telomere health. Belly fat, on the other hand, is a prime sign of poor metabolic health, no matter how much you weigh.

It can even cause the biggest health problem of the century, diabetes. This epidemic has reached such heights that around 9 percent of people globally suffer from it.

Belly fat is a big culprit here since people with more fat around their mid-sections have shorter telomeres that lead to _insulin resistance_ — a condition that occurs when cells fail to respond to insulin, a molecule that's responsible for harvesting glucose from the bloodstream, resulting in high blood sugar. Just take a 2016 Danish study of twins, in which the twins with the shorter telomeres were found to have higher rates of insulin resistance.

Beyond that, refined carbohydrates and sweet foods cause spikes in insulin, resulting in inflammation that harms cells. That makes sweets a definite no-go, while a whole food diet can actually nurture your telomeres by reducing inflammation and oxidative stress.

For starters, sticking to a low-sugar diet, rich in non-starchy vegetables, will increase your metabolic health. Additionally, eating primarily antioxidant rich foods like fruits, vegetables, nuts, whole grains and green tea, will also rein in oxidative stress.

Foods like blueberries, kale and broccoli contain anti-inflammatory nutrients, while foods rich in omega-3 fatty acids, like oily fish and flaxseed, also reduce inflammation and support telomere health.

> Antioxidants in nutritional supplements are not recommended for telomere health as the evidence supporting their use is inconclusive.

### 8. A safe neighborhood promotes healthier living. 

Would you feel comfortable walking home at night in your neighborhood? Well, it's an important question for your health for a variety of reasons and your answer could even affect your telomeres.

That's because the safety and social cohesion of the place where you live have profound effects on your health. A sense of safety impacts stress levels and can therefore affect telomeres.

A 2015 study published in the _Journal of Health and Social Behavior_ found that people who feel stuck in an unsafe neighborhood tend to have shorter telomeres.

Beyond that, healthy habits are just plain difficult to keep up in certain neighborhoods. People who live in dangerous neighborhoods with regular police helicopter activity might have a hard time sleeping through the night.

Your social environment is also key to your health and, when it comes to this, what truly matters is how friendly and trustworthy your neighbors are, not what social class they come from. A 2014 study published in _Health & Place _found that people in neighborhoods of low social cohesion experience more rapid cellular aging than people in safe, trusting neighborhoods.

Another factor in telomere health is the physical environment. Just consider San Francisco. Despite its many shining qualities, it's a dirty, litter-filled city, which is bad for residents in general, but especially harmful for children.

_Social Science and Medicine_ even found that children living in neighborhoods with lots of litter and vacant buildings have shorter telomeres. On the other hand, the more green space, like parks, in a neighborhood, the healthier the telomeres of the residents.

It just goes to show how nature plays a role in the restoration process by clearing the mind and fostering relaxation.

And finally, certain molecules have also been linked to shorter telomeres, like carbon monoxide, a colorless, odorless and flavorless gas. Another is cadmium, which is predominant in cigarette smoke, but also found in household dust and fossil fuel emissions.

### 9. From pregnancy through childhood, a healthy lifestyle benefits children and adults. 

Between saving for college, weddings and other major life events, it's hard not to worry about the future of your children. But even before a child is born, it's important to consider how to take care of her cells.

That's because cellular aging starts in the womb. In this process, known as _direct transmission,_ the length of the parents' telomeres at the time of conception is passed on directly to their baby. This means a baby could easily start life with shorter telomeres.

That being said, quitting smoking before conception and increasing your intake of nutrients like _folate_, a B vitamin, are great ways to help your baby's telomeres get a good start. Another good strategy is to reduce stress through activities like pre-natal yoga and nature walks.

Taking such precautions is key since the early years of a child's life shape her telomere health for life. A rough childhood can manifest in the body through a process known as _biological embedding_, which has a dose-response relationship to telomere length.

In other words, the more traumatic events a child experiences, the shorter her telomeres will be as an adult. Just take a study published in the _American Journal of Preventive Medicine_ in 1998, which asked ten questions about things like the alcohol consumption and depression levels of parents to show that the more difficulty a child had experienced, the more likely she was to suffer from early adult diseases like heart problems and depression.

Not just that, but neglectful parenting can also negatively impact telomeres through the emotional difficulties it causes. After all, children who feel nurtured by their parents get a rush of _oxytocin_, a hormone that's released when you feel close to another person. It reduces stress and lowers blood pressure.

So, while a bit of childhood stress can be beneficial, especially in dealing with greater stress down the line, if a child bumps his head his parent should respond calmly, thereby making the child feel calm as well.

### 10. Final summary 

The key message in this book:

**A singular cellular structure has profound effects on aging. It's called a telomere and the shorter it gets, the older we feel. By attending to the health of our telomeres through changes in diet and lifestyle, it's possible to look and feel both younger and healthier.**

Actionable advice:

**Set an aging goal and pick an aging role model.**

Take a moment to imagine a future in which you enjoy telomeres that enable you to live a longer and healthier life as you cross into your 60s and 70s. Then, plan how you'll spend your days at this ripe age. Consider what exercise you'll do, what social activities you'll partake in, where you'll travel and what books you'll read.

Finally, find an older role model who fits the image of vitality you want to attain at their age. It might be Judi Dench, Supreme Court Justice Anthony Kennedy, or even your own grandfather.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Blue Zones_** **by Dan Buettner**

_The Blue Zones_ (2012, first published in 2008) whisks you through the regions of the world with the highest concentrations of healthy centenarians. By examining how people in these regions live and interact, we gain insight into how to extend our own lifespans.
---

### Elizabeth Blackburn and Elissa Epel

Dr. Elizabeth Blackburn, PhD, shared the Nobel Prize in Physiology or Medicine in 2009 with two colleagues after uncovering the molecular nature of telomeres. She is now president of the Salk Institute and a professor emeritus at University of California, San Francisco.

Dr. Elissa Epel, PhD, is a leading health psychologist who studies stress, aging and obesity. She is a professor in the Department of Psychiatry at University of California, San Francisco, and a member of the National Academy of Medicine.

