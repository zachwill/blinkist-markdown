---
id: 53ea0ffc31373700070e0000
slug: the-thank-you-economy-en
published_date: 2014-08-12T00:00:00.000+00:00
author: Gary Vaynerchuk
title: The Thank You Economy
subtitle: None
main_color: F26432
text_color: D95A2D
---

# The Thank You Economy

_None_

**Gary Vaynerchuk**

_The_ _Thank_ _You_ _Economy_ (2011) describes how the advent of social media has changed the relationship between companies and their customers. It shows just how critical online engagement is for companies who want to succeed, and offers tips on how companies can use social media to influence their public image along the way.

---
### 1. What’s in it for me? Turn your genuine interest in your customers into customer loyalty. 

Businesses today face more competition than ever before. And the presence of the internet means that they now have a much larger arena to compete in.

Today's successful businesses not only have to navigate the world of print media, television and radio but also the complex and highly controversial world of online marketing and social media.

_The_ _Thank_ _You_ _Economy_ unravels this complexity by presenting an approach to customer engagement that draws its inspiration from the "good old days" when companies were personally involved with their customers and genuinely interested in their well-being.

But this is more than mere nostalgia or sentimentality: social networking has changed the rules of engagement for doing business, and a new approach is necessary to cope with the new reality influenced by social media.

In these blinks, you'll learn

  * how social media forces multinational companies to think like small-town butchers;

  * how one company pulled in a $250,000 deal by answering a single tweet;

  * why the author decided to switch deodorant brands; and

  * why you can say goodbye to your business if you refuse to interact with your customers through social media.

### 2. Social media brings old-fashioned values back into businesses. 

Have your grandparents ever reminisced about the times when businesses were courteous, lamenting that those times have since passed? And did you brush it off the same way you brushed off their telling you they used to have to "walk a mile to school — uphill, both ways"?

While it's safe to say that they may have exaggerated the arduousness of their trek to school, the courtesy found in small local businesses did indeed exist, but was forgotten in the urban lifestyle of the twentieth century.

In the rural settings of the past, local shop owners _had_ to care about their customers because they depended on a steady customer base. If, for example, the town butcher upset one customer, that customer would tell the whole neighborhood, and the butcher would soon have a real problem.

However, around the mid-twentieth century, people began flocking to the cities and courteous service became less important.

In highly populated cities, the chances of going to the same butcher more than once became slim, so the butcher no longer had to earn your trust to stay in business. If you didn't like his service, no one would care.

But social media has in some ways brought back the old-fashioned ways of small-town living.

Online networking sites, such as blogs, Facebook and Twitter, allow people to share their thoughts instantly, and if someone isn't satisfied with your service, they can tell the whole wide world.

Just consider Giorgio Galante, a regular AT&T customer who was unhappy with the company's service and expressed his dissatisfaction in two emails to AT&T's CEO, Randall Stephenson. However, instead of sympathy, Galante received a legal threat to cease and desist!

In the recent past, the frustrations of a single customer might not have mattered. But Galante shared the story on his blog, where many other people read and shared it, causing a huge scandal for AT&T.

What does this teach us? If companies want to survive in today's world, they'll have to rediscover the courteous ways of their small-town predecessors.

### 3. Successful businesses have to build trusting relationships with their customers. 

With so much competition nowadays, a high-quality product and service with a smile can only get you so far.

For starters, there are real limitations to how low you can price your services and how high you can go on quality. And no matter what you sell, there will always be someone out there offering it cheaper, better or faster.

And even if your product is perfect, it won't mean much until you win your customers' loyalty.

For example, the author's liquor store offered great quality and better prices than one competitor in his area, but that wasn't enough to entice his competitor's customers to make the switch — they still felt loyalty to his competitor.

When it comes to your customers, a long-lasting, trusting relationship is more important than the short-lived relationships made by simple, attention-grabbing advertisements.

Unfortunately, however, many companies rely on generating short-term interest to boost their profits. But why settle for the short term? Developing a trusting relationship with your customers will guarantee that your company survives long term.

For example, during the 2010 Super Bowl, Old Spice introduced a series of highly praised commercials. At the same time, the company actively engaged with consumers online by having them ask questions of Isaiah Mustafa, the star of these commercials.

However, once the campaign ended, Old Spice simply stopped interacting with its fans and followers.

How did fans react? The author, for example, bought a stick of Old Spice deodorant during the campaign, but not afterwards. Because the company's interactions with fans stopped so abruptly, he felt that the company's interest in him was that he buy a stick of deodorant and nothing more.

Old Spice's mistake was allowing the interest they generated with their commercials to drop. Had they continued to engage their fans and nurture the relationship they built, they would have been able to save money in the long term by not having to invest so heavily in new campaigns.

> _"All the numbers prove is that you've made contacts, not connections."_

### 4. A successful business aims for the customer’s heart. 

Think for a moment about your greatest friendships. Did they develop overnight? Probably not!

Just as building personal relationships requires time and engagement, so does building a trusting relationship with your customers — you have to show them you care.

If you can make your customers feel special and appreciated, they'll be loyal not only to your products, but to your entire brand.

One great way to do this is to personalize your high-quality products and services for each customer.

The Joie de Vivre hotel chain, for example, came up with an initiative they call the Dream-Maker program, which encourages employees to learn as much about their guests as possible in order to make their stay a one-of-a-kind experience.

When reservations manager Jennifer Kemper learned that one of her hotel's frequent guests was actually visiting her son, who was undergoing chemotherapy during his studies at Berkeley, Jennifer arranged a special, emotionally supportive welcome basket for her.

As a result, that mother continued to stay at the hotel until her son graduated.

The catch when personalizing your products and services is that you have to be genuinely interested in your customers. If customers suspect that you're feigning interest in order to push a product, they'll lose interest.

Consider the website Quirky, which used to stream updates about its products using its Twitter account. But they eventually switched tactics, deciding to use the platform to engage in personal conversations with each customer rather than simply announcing product updates.

By showing its customers just how important they were, Quirky received a huge boost in interest.

Why? Because scripted communication to push a certain message or ad campaign isn't enough. Customers aren't fooled by it — especially on websites like Twitter. They expect companies to communicate with them directly, like they would with friends or acquaintances, and they quickly get fed up with pre-packaged lines.

We've now seen how social media has been a complete game changer for customer relations. But let's say you're the more conservative type: Can your business thrive if you ignore social media?

### 5. Many businesses mistakenly reject social media as unreliable or unimportant. 

Are you the type of person who thinks that social media isn't necessary for your business to succeed? If so, you're not alone. Many companies dismiss it as unreliable because it's hard to measure its benefits in a precise way.

It is, indeed, difficult to be precise about the way social media affects both customer behavior and, more importantly, the subtle emotional changes that take place there.

For example, a 2010 Customer Service Impact Report revealed that 76 percent of the customers surveyed appreciated it when companies demonstrated a personal interest in them.

But what does that actually mean? Did those consumers actually buy more products? While we can't say that for certain, we can say that some of them were positively influenced by these companies' strategy of personal engagement.

And due to its personal nature, social media represents an opportunity to engage with customers and make them feel truly valued as individuals.

Even though it's clear that social media does have _some_ impact on consumer behavior, companies that rely on exact metrics to guide their strategies will surely find it hard to make a large investment in social media.

Plus, given that social media is still a relatively new phenomenon, many companies believe it'll fade into obsolescence and thus isn't worth the investment. However, even if it is a passing fad, it's one that has huge potential _right_ _now_, so it's foolish to ignore its possibilities.

Just look at the business communications development company Avaya. They decided to take their communications to Twitter, and by answering a _single_ tweet, they were able to win a new client who eventually signed a $250,000 deal with the company.

### 6. Businesses that don’t adapt to social media will lose customers. 

Before the advent of social media, people had serious doubts that doing business online would be of any great benefit. In fact, the author even recalls being ridiculed at a conference in 1997 for talking about his desire to sell wine online.

This turned out to be a huge foot-in-the-mouth moment for all the doubters. Since then, social media, with its ability to share new sites, products and shops via images and links, has helped make online shopping extremely popular.

In fact, by using social media, the author now enjoys the loyalty of more customers than Costco on the local market and Wine.com on the national market.

Not only does social media help foster online shopping, it also serves as a way for customers to learn about your business, making an online presence critical to fostering a positive perception of your brand.

When people buy online, they get information on the company through social networks. Therefore, if a company has a negative social media presence — or even _none_ at all — the customers will assume that it's not trustworthy and buy their goods elsewhere.

Consequently, companies that don't engage their customers online, and on social media in particular, will lose popularity. Just look at what happened to Barnes & Noble.

At one time, they were among the leading bookstores. However, they've since been beaten back by the online-only Amazon, founded in 1997.

Barnes & Noble also went online in 1997, but didn't use its online presence as a way to outcompete Amazon. Having already established itself with shops all over the country, it was too sure of itself to put an effort into a solid online presence.

What they _should_ have done was initiate an online campaign including an online store. And they should have made a major social media push once the technology was available. But they didn't, and in 2010, B. Dalton, one of Barnes & Noble's critical chains, was forced to close its doors.

As you can see, ignoring social media can threaten a company's very existence. Using it properly, however, can bring companies to new heights. The next few blinks, you'll learn how.

### 7. Combine social media with traditional media to get the best out of both. 

If you've been in business long enough, you've surely learned that _every_ strategy, no matter how good, will always have its drawbacks. Social media is no different: it's not a silver bullet that will obliterate all your marketing and customer service problems. That said, sometimes online interaction alone just isn't enough.

While social media is powerful, so are other types of media. If you want to broaden your appeal to as many people as possible, you still have to use more traditional media in order to reach those who aren't active online.

For example, when the author was advertising his book _Crush_ _It!_, he used both billboard and social media advertising. Although some thought it paradoxical to advertise a book about building brands with social media platforms through a traditional print medium like a billboard, he felt it was the best way to reach as large an audience as possible.

That's because you have to use every opportunity available to you to engage customers in active conversations. In fact, extending the conversation from traditional media to social media can raise interest in your product.

That's because transmedial advertising — especially when ads from traditional media extend into more personalized social media — can build tension and excitement.

If you leave your fans wanting to know more about you or your product, then they'll follow your brand onto social media platforms, where you'll have the opportunity to interact with them more personally.

Consider this example from Reebok, which made a television ad starring Stanley Cup champions Sidney Crosby and Maxime Talbot.

In the ad, the players stood in a garage, taking turns shooting pucks into an old dryer and tallying their points on a scoreboard. The viewers saw only snippets of the match, and if they wanted to see who won, they had to follow the brand on Facebook.

Needless to say, Reebok won lots of curious Facebook followers at the time!

### 8. You can’t manage social media like traditional media – it follows different rules. 

So you've decided to engage your fans across both traditional and social media — good for you! However, managing all this can be tricky. Unfortunately, you don't get to use the same strategies with social media that you would with traditional media.

For one, traditional media _sends_ messages and social media _receives_ them.

In other words, with traditional media, you're advertising a particular product or trying to garner attention for your company in general. Social media works in an entirely different way, as it's based on two-way communication.

If you make the decision to go online and interact with social media users, you'll have to _listen_ to what people tell you, rather than accost them with neatly packaged messaging.

The author personally tweets answers to his followers' questions. Sometimes it's just a phrase or a short sentence, but even this small gesture shows that he's listening to his fans and genuinely cares about their concerns and input.

And since social media is all about two-way communication, you'll have to learn to speak your customers' language. This means being open to learning new things, trying out new ideas and — above all — staying genuine.

AJ Bombers, a burger joint in Milwaukee, has generated a very active online following, and fans actively participate in the company's decision-making processes. But how did it get people so invested in the first place?

They engage their customers across a number of social media platforms, including smaller ones like Foursquare, and give fans the opportunity to follow every aspect of their preparation process. In return, their fans can take part in that process in ways they find interesting — for example, by mixing up menu items and even suggesting promotions for the employees.

### 9. A caring culture starts from the top of the company. 

Once you've decided to take a customer-oriented approach to your social media campaigns, you'll need to find a way to ensure that this approach is actually adhered to. While the CEO can decide how far a company will go for its customers, it's ultimately the employees who'll go the distance.

Consequently, in order for the CEO's values to reach the customer, each employee must be able to demonstrate them. And as the employees that interact directly with customers are the most visible part of the company, it's crucial that they manifest the company's values.

Consider for a moment that, before Amazon bought Zappos in 2009, Zappos was outselling Amazon in spite of the fact that its prices were higher. How was that possible? Well, Zappos offered an amazing customer experience and its employees offered stellar customer service.

While Tony Hsieh, the CEO of Zappos, understood the deep value of customer service, _he_ wasn't the one who actually communicated with the customers. That was done by _his_ _employees_, so it was critical that every one of them shared the values of the company.

And the best way to ensure that your employees care for your customers is to show your employees that you care for _them_. Overworked and stressed employees are less likely to care about customers and, consequently, less likely to make customers feel welcome and well-tended to.

In contrast, when employees feel good at work, they become personally invested in the company and are therefore more concerned about the impression they make on the customers.

What are some ways of keeping your employees happy?

Zappos offered its employees a cafeteria with free food and a library. And the author introduced a new vacation policy at his company Vaynermedia: his employees can take as much vacation as they need. In return, he expects his employees to give their absolute best when they return to work.

### 10. Final summary 

The key message in this book:

**Social** **media** **has** **ushered** **in** **the** **return** **of** **small-town** **courteousness** **by** **empowering** **people** **to** **share** **their** **thoughts** **with** **a** **few** **keystrokes** **and** **the** **click** **of** **a** **mouse.** **With** **this** **knowledge,** **companies** **who** **want** **to** **foster** **a** **good** **public** **image** **will** **have** **to** **take** **special** **care** **to** **show** **genuine** **interest** **in** **their** **customers'** **well-being.**

Actionable advice:

**Reallocate** **some** **of** **your** **PR** **budget** **to** **social** **media.**

Social media allows you to get instant feedback on your products, services and advertising strategies without having to shell out for focus groups. Just asking your customers directly what they want from your company and listening and responding to their concerns gives you an opportunity to create a strong bond between your customers and your brand.

**Combine** **traditional** **media** **with** **social** **media.**

While it social media is an essential part of any successful business strategy, it isn't capable of capturing every aspect of the market as not all customers use social media. The best strategies are those that engage customers across a number of media and attempt to extend the conversation on social media platforms.

**Suggested** **further** **reading: _Crush it!_**

_Crush_ _it!_ by Gary Vaynerchuk is a motivational text and a guide for those who want to translate their passion into a business. Using the author's life as an example, this book details how everyone can "crush it," i.e., realize the possibility of living their passion, determining their livelihood and making a living off of what they love to do.
---

### Gary Vaynerchuk

Gary Vaynerchuk is a public speaker, wine critic at the video blog WineLibraryTV and author of the bestseller _Crush_ _It!_ (also available in blinks). In addition, he is the founder of the wine retail store Wine Library and co-founder of the social media consulting agency Vaynermedia.

