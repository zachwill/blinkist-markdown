---
id: 5a3a35a3b238e1000765bb52
slug: the-third-chimpanzee-en
published_date: 2017-12-28T00:00:00.000+00:00
author: Jared Diamond
title: The Third Chimpanzee
subtitle: The Evolution and Future of the Human Animal
main_color: 177164
text_color: 177164
---

# The Third Chimpanzee

_The Evolution and Future of the Human Animal_

**Jared Diamond**

In _The Third Chimpanzee_ (1991), Jared Diamond explores the evolution of _Homo sapiens_, which started out like any other animal and gradually became a unique creature capable of producing speech, making art and inventing technology. The book reveals some extraordinary insights about the nature of human beings.

---
### 1. What’s in it for me? Get to know your close cousins and learn some human history. 

Our fascination with chimpanzees and other primates is nothing new. King Kong, Tarzan, the army of primates in _Planet of the Apes_, the orangutans at the zoo — when it comes to apish things, we humans seem unable to get enough.

And maybe it makes sense, since they're our closest evolutionary cousins. In fact, we might be far more closely related to other primates than was previously thought.

_Homo sapiens_ is a complex and fascinating species — a species that, in this age of technology and automation, has never been more important to understand. These blinks explain a revelation that has opened up new opportunities for understanding human essence and human evolution — an understanding that has both its beautiful and its brutal side.

In these blinks, you'll find out

  * why chimpanzees are counted as humans by some taxonomists;

  * how human language may have emerged; and

  * why we might all have been better off as hunter-gatherers.

### 2. Science shows that humans are more genetically similar to other primates than previously thought. 

The similarities between humans and other primates aren't hard to spot. But exactly how genetically similar are we to our wild cousins, and which ones are our closest relatives?

Scientists are now able to look at the human genome and calculate exactly how alike humans and apes are. The results are pretty striking. We share 96.4 percent of our genes with orangutans, 97.7 percent with gorillas and an incredible 98.6 percent with chimpanzees.

Effectively, that means that a mere 1.4 percent of our DNA distinguishes us from chimpanzees. Of that percentage, only a small amount contains the genetic tools that helped us develop those attributes we consider uniquely human, such as language, art and technology.

In fact, we're so genetically close to chimps that some scientists even consider us part of the same family.

In most encyclopedias, you'll find humans and chimpanzees classed in the same order, the _Primates_, and the same superfamily, the _Hominoidea_, but in separate families: the _Hominidae_ for humans and _Pongidae_ for chimpanzees.

However, one school of taxonomy — known as the _cladistics_ — arranges species based on relative genetic distance. Using this method, humans and chimps are part of not only the same family, but even the same genus.

They conceive of not one but three separate species within the _Homo_ genus: _Homo troglodytes_, the common chimpanzee; _Homo paniscus_, the bonobo; and us, _Homo sapiens_.

Being in the same genus means the species are very closely related, so much so that sometimes they might be distinguishable only to experts. Take the _willow warblers_ and the _chiffchaffs_ as an example. These European birds share 97.7 percent of the same DNA and look virtually identical. But they're less closely related to each other than humans are to chimps.

So we're a lot like our chimp cousins — but it's our few different characteristics, as well as art, technology and language, that really mark our species out.

### 3. Humans were always evolving, but it was the ability to speak that really changed things. 

According to many anthropologists, human development underwent a "great leap forward" about 40,000 years ago in Europe.

But to really understand human evolution, we've got to begin a lot earlier than that — at the first stage of human evolution.

Around 3 million years ago, two distinct species of early humans could be identified, quite separate from apes. These were _Australopithecus robustus_, which died out 1.2 million years ago, and _Australopithecus africanus_.

_Australopithecus africanus_ later evolved into _Homo habilis_ and _Homo habilis_ evolved into _Homo erectus._ _Homo erectus_ had larger brains and bodies and, soon enough, they began to expand their territory from Africa into Asia and Europe.

_Homo erectus_ underwent further anatomical changes and, around 500,000 years ago, evolved into _Homo sapiens_.

Anatomical changes could only get us so far, however. It was the development of language, unique to humans, that really allowed us to make major strides.

To speak, you need the right larynx, tongue and associated muscle structure. Humans use this to produce a range of sounds that, in turn, can be manipulated to produce language.

Chimpanzees, gorillas and orangutans can't speak because they don't have that structure. Scientists believe that proto-humans also lacked this structure, and that even _Homo sapiens_ wasn't physically equipped for speech for the first 460,000 years of its existence.

As _Homo sapiens_ developed language, the great leap forward began. A subtle change in anatomy meant we suddenly had the vocal control and range suitable for speaking a language. We could now communicate images, ideas and instructions more quickly and effectively.

Language also enabled us to develop art and technology. So what exactly was this early language like?

### 4. Language development in humans can be understood through both historical and comparative scientific models. 

For centuries, sophisticated human language and communication seemed utterly unlike the grunts and howls of the lower beasts. But scientists have recently analyzed animal communication methods and have shown that they're actually not so distant from human language.

The animal that has probably come closest to developing a "language" is the _vervet_, a small African monkey. Electronic analysis of vervets has shown that they react with different vocalizations to different environmental stimuli. Attacks from leopards, eagles or snakes, for instance, cause the vervets to produce varying sound-alarms. They also produce distinct sounds in various social contexts and they can communicate with grunts to indicate whether something is edible or inedible.

So it seems that a general speaking ability is not so unique to humans.

There's even a way we can imagine how humans first developed languages. In the fifteenth century, the first wave of European colonization began, and, over the course of the next four centuries, countless trading posts were established. But, at first at least, it was very difficult to get any trading done, because there was no way for traders from different lands to communicate and work together.

And so simple languages known as _pidgins_ emerged. These contained elements of the various traders' different native languages.

Later generations spoke this or that pidgin as a native language, and enriched them with more vocabulary and a more complex grammar. These new languages are called _creoles_.

The fascinating point is that although pidgins and creoles have been created all over the globe — and all independent from each other at that — they still share features, like similar grammars.

The subject-verb-object order is used for statements; prepositions are common; double negatives are frequent; and monosyllabic words are often toneless.

These common features indicate that human languages may have developed in much the same way that creoles developed.

Let's turn away from language now and examine another aspect of human diversity: racial characteristics.

### 5. Natural and sexual selection gave rise to “racial characteristics”. 

As you've doubtless noticed, not all human beings look the same. There are tall humans and short humans, humans with lighter skin and humans with darker skin. These are known as _racial characteristics_, and there are two scientific theories that posit how they developed: natural selection and sexual selection.

Most biologists are proponents of natural selection. This theory claims that racial characteristics emerged in order to improve a population's chances of survival. For instance, the large chests of Andean Indians enable them to absorb more oxygen from the thin air found at high altitudes.

But natural selection doesn't really explain other characteristics, such as skin color.

Advocates of natural selection suggest that darker skin emerged in areas of the world that get the most sunlight. But that doesn't explain why some native peoples have dark skin in places like Tasmania or in parts of equatorial West Africa. And as for differences in hair and eye color, or genitalia, natural selection has no explanatory clout.

In addition to natural selection, Charles Darwin proposed the theory of sexual selection. This theory can fill in some of the gaps that natural selection leaves open.

Sexual selection is simple enough in theory. Say a female has a set of physical characteristics that males find attractive. Since this female's characteristics are attractive to males, she'll have an easier time finding a mate and procreating. Her offspring may then inherit some of her attractive characteristics, thus raising their chances of finding a mate and procreating. Over the course of generations, the physical traits — such as hair color, eye color and the size or shape of genitalia — that increase the likelihood of finding a mate within a given region are perpetuated, and those that diminish it are weeded out.

This means that sexual selection spreads characteristics that don't necessarily increase survival rates in and of themselves, but do result in the proportional increase of these sexual characteristics in a given population.

Most likely it's a combination of these two theories that has led to the gradual development of distinct racial characteristics.

> _"As long as the female has it and the male likes it, sexual selection could lead to any arbitrary trait, just as long as it does not impair survival too much"_

### 6. What you thought about agriculture’s benefits is most probably wrong. 

Most historians and anthropologists believe that humanity's embracing of agriculture increased quality of life and gave us the free time to develop art and new technologies.

Nowadays, however, that logic is being turned on its head.

Hunter-gatherers actually had a higher quality of life than later farmers. Studies of the few remaining hunter-gatherer populations indicate that they had plenty of leisure time and worked no harder than farmers. Today, Kalahari bushmen only spend twelve to nineteen hours a week gathering food. How many farmers can claim to work as little as that?

Additionally, archeological evidence shows that hunter-gatherers were much healthier than farming populations who lived in the same regions at a later date.

Paleontologists looked at skeletons from Greece and Turkey dated to the end of the last Ice Age. In that region, the average height of hunter-gatherers was 172 cm. Once agriculture was adopted, though, this average dropped to just 157 cm. This change clearly tells a tale: these communities were less nourished than those who had lived there before.

Farming only supplanted hunting and gathering because agriculture was able to support populations larger _in number_. Simply put, agriculture supplies more food per capita than hunting and gathering.

Those populations that took up agriculture increased rapidly. Once they were greater in number, they were able to use that advantage to push the healthier, but fewer, hunter-gatherers to the margins.

From that point on, agriculture took hold and became the primary method for supplying food.

The population boom that followed the adoption of agriculture was the reason why agricultural societies progressed further in terms of technology and culture than hunter-gatherer societies. They simply had more people, more minds that could busy themselves with societal advances.

### 7. Genocide is terribly human. 

You probably think that only perverted psychopaths resort to genocide. But you'd be wrong. History shows us that genocide is no one-off phenomenon, and that each of us could potentially commit it.

For starters, many instances of genocide have simply been forgotten by the vast majority of humanity.

Consider, for instance, the nearly forgotten mistreatment of the native population of Tasmania by British settlers. In about 1800, the Brits arrived on the island, where they "discovered" a native population of roughly 5,000 hunter-gatherers whose technology had advanced no further than stone tools. By 1869, only three native Tasmanians remained. The rest had been killed or kidnapped.

In the twentieth century, there were at least 26 cases involving the genocide of racial, national, ethnic, religious or political groups.

Some of these were smaller in scale, such as the abuse of the Aché Indians in Paraguay in the 1970s. Others, like the Armenian genocide in Turkey, were much larger. In this particular case, roughly a million people were killed between 1915 and 1917.

In fact, genocide — or attempted genocide — is so common that it must be considered a part of human nature. Though we might find the idea repugnant, we are all potential participants in genocidal massacre. And those who do attempt it will always find ways to explain away their actions.

Some, like the Hutu people, claim self-defense. They murdered more than 500,000 Tutsi during the 1994 Rwandan genocide.

Others rationalize their killings by claiming them as necessary for advancing what they consider the "correct" religion, race or political belief. That sort of justification is exemplified by the Indonesians who killed close to a million supposed communist sympathizers between 1965 and 1966.

Comparing the victims to animals is also a recurrent justificatory gambit. French settlers in Algeria, for instance, portrayed local Muslims as rats.

It's debatable whether humans will be able to quell genocidal urges in the future, but maybe — just maybe — we can.

### 8. We have always exploited the environment to satisfy our needs. 

Ever since the Enlightenment in the eighteenth century, primitive peoples have been portrayed as noble savages — as a purer kind of person living in harmony with nature.

However, we now know that human societies have always posed a threat to the environment.

It's emerged that the Māori, shortly after they first reached New Zealand, exterminated the moa, a large, flightless bird. When European settlers arrived later, they found the bones and eggshells of the already-extinct moa. Ever since, scientists have been puzzling over just what happened.

Initially, they rejected the idea that the Māori had killed them, assuming that the Māori had had a deep respect for nature.

However, more than a hundred archaeological sites have shown that the Māori used to kill moas for meat, eggs and bones. It's they who probably drove the bird to extinction. In fact, an estimated 100,000 moa skeletons lie in these field sites.

It's no one-off case. The exploitation of the environment is simply part and parcel of human existence.

Consider the advanced indigenous civilization whose environment exploitation backfired on them.

When Spanish explorers reached New Mexico, they found towering, uninhabited structures built in the middle of the desert. They belonged to a lost civilization that the indigenous Americans had termed the "Ancient Ones."

Paleobotanists — scientists who reconstruct environments and landscapes from the past using plant fossils and remains — have demonstrated that, at the time this civilization started building its cities, woodland grew all about. However, the people exploited this resource for timber and firewood until it became the barren wasteland that exists today.

By cutting down all the trees, the Ancient Ones also essentially ruined the natural underground water-storage system on which they'd depended. Eventually, their irrigation water dropped below the level of their fields, and the subsequent drought led to the collapse of their civilization.

It's pretty clear now that the belief that past societies were strident champions of environmentalism is a prime example of romantic blather. On the contrary, our ancestors didn't know how much devastation they were capable of causing. But we do. And if we end up destroying our planet, it won't be out of ignorance, but out of catastrophic negligence.

### 9. Final summary 

The key message in this book:

**Throughout our long history, humans have shown a capacity for both remarkable achievements and terrible mistakes. Now, in an era when technology has maximized our potential for both good and evil, a deeper understanding of human nature and human history may play a crucial role in preventing humanity's downfall.**

Actionable advice:

**Walk on the wild side.**

Have you ever wondered what life might be like as a hunter-gather? It might be fun to spend an afternoon in some forest of field in the countryside near you. Take a nature guide with you and see what plants and birds you're able to identify.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Guns, Germs, and Steel_** **by Jared Diamond**

_Guns, Germs, and Steel_ explains how today's unequal distribution of wealth and power between people is based on factors of their respective environments rather than innate characteristics. It is an explicit refutation of any racist theory concerning current inequalities. The book uses a mixture of case studies, statistical analyses and deduction to support its arguments.
---

### Jared Diamond

Jared Diamond is an exemplary scholar. His career has taken him through several fields and he is now professor of geography and physiology at the University of California, Los Angeles. He's also published a series of popular science books, including the best-selling _Guns Germs and Steel._

