---
id: 56d5ab9394333d0007000076
slug: the-third-plate-en
published_date: 2016-03-03T00:00:00.000+00:00
author: Dan Barber
title: The Third Plate
subtitle: Field Notes on the Future of Food
main_color: 675550
text_color: 675550
---

# The Third Plate

_Field Notes on the Future of Food_

**Dan Barber**

_The Third Plate_ (2014) is about food: the way we cook it, eat it, produce it and the ways in which all these things are intertwined. Barber examines the dangers of monocultures and presents a powerful argument for sustainable food. He not only explains how we can go about making food more sustainable, but how we can make it even more delicious, too.

---
### 1. What’s in it for me? Get a sneak peek at the sustainable cuisine of the future. 

As you munch away on your French fries and slurp your soda, you're probably aware that it isn't very good for you. But it's not just unhealthy for you, it's doing great harm to the environment. 

These blinks will take you on a journey of the life of food: how it is produced, how monoculture wiped out former agricultural practices, how the oceans are being destroyed, and why we need to rethink the way we eat to create a healthier, sustainable and better-tasting cuisine.

In these blinks, you'll learn

  * how monoculture caused the Dust Bowl; 

  * how you get your carrot to burst with sweet flavor; and

  * what the risotto of the future looks like.

### 2. Our eating habits need a makeover to ensure sustainability: chefs can help. 

What springs to mind when you think of a quality restaurant meal? Perhaps a large cut of meat or fish and some vegetables on the side drizzled with a special sauce? Sounds delicious, but what does a meal like this mean for the environment?

Most of us in the Western world are lucky enough to be able to eat just about any kind of food we want. However, most of us also fail to realize that it's environmentally unsustainable. With increasing populations and changing food habits, our ecology is being threatened. And while population control is extremely difficult to put into action, we _can_ change the way we eat. And chefs can help us do it. 

Chefs create food trends and influence how and what we eat at home. For example, many restaurants focus on meat and fish, products that require a wealth of natural resources to produce. Vegetables, meanwhile, only play a supporting role. 

Restaurants also often center on eating _high on the animal._ In other words, eating the cuts of meat that are literally high on the animal's body, like beef or fish filets. These cuts have become popular for their tenderness and because they look great on a plate.

The problem is, each animal can only provide few such cuts. As a result, we use less of the animal's meat than ever before. To make matters worse, as the meat market grows, so does the need to feed these animals. For example, one pound of beef requires a whopping 13 pounds of grain feed to produce. This puts a massive strain on our environment.

So we're faced with the need to find a sustainable cuisine. This cuisine, let's call it _the third plate_, needs to use more food varieties in order for it to be sustainable.

Having the world turn vegetarian isn't the answer, as our ecological system is based on animals and plants existing alongside one another. Rather, we need to understand the complexity of our ecosystems and the diversity required to sustain them.

> _"A gastronome who is not an environmentalist is stupid."_ — Slow Food founder Carlo Petrini

### 3. Modern wheat is a perfect example of how crops have changed over the last 100 years. 

Bread, pasta, pastries, couscous… They all have one thing in common: wheat. But where does all this wheat come from?

Historically, wheat was processed in gristmills that ground the whole kernel of the wheat, producing a wheat rich in minerals, protein and fatty acids. But this method also meant that the wheat only kept for about a week. Modern production techniques have changed that.

When roller mills were introduced in the late nineteenth century, the germ of the wheat, which contains the minerals, protein and fatty acids, was removed in the milling process. As a result, wheat not only had a longer shelf life, but could be grown and milled far from where it was consumed.

World War I and legislation exacerbated the circumstances. During the war, Europe ran out of wheat.

But the new properties of roller-milled wheat enabled the United States to export wheat to Europe without it rotting first.

In addition to roller mills, the Enlarged Homestead Act of 1909, which made it possible to obtain 320 acres of free land in the new western territories, led to record-high wheat yields. In 1917, a total of 65 million acres of wheat was harvested. To make that number a bit more tangible, that's equivalent to about 50 million football fields. In 1919, this number climbed to 75 million acres. The Midwest was now a vast monoculture of grain production.

Today, wheat is one of the most consumed foods in the United States, with the average American devouring 130 pounds of it per year. That's more than beef, lamb, veal and pork combined, and more than any other grain — aside from the sweeteners extracted from corn.

Because we eat so much of it, and because we need so much to raise animals, grain is now the United States' most important agricultural product. And it has a huge impact on our environment.

> _"Focusing only on fruits and vegetables is like planning a new house but designing only the doors and windows. It misses the big picture."_

### 4. Monoculture has severe consequences on soil quality and the robustness of ecosystems. 

Picture boundless golden stretches of corn and wheat, the wind billowing through it. Doesn't sound so bad, does it? Unfortunately, there's a nasty downside to it.

Monoculture, or the practice of growing one type of plant in one area, wreaks havoc on the soil and drains its nutrients. But there is hope: namely, traditional farming techniques. Native Americans, for example, used the _three sisters_ technique, where corn, beans and squash were all grown together, each playing vital roles in replenishing the earth's nutrients.

In this method, beans bind nitrogen from the air and into the ground via their roots, which is ideal for the nitrogen-loving corn, which in turn gives the beans something to climb on. Finally, the squash helps bind carbon, a nutrient for soil, and provides the plant roots with shade, which helps keep the water in the soil.

This is a far cry from what happens in modern agriculture. Modern agriculture is based on monocultures and this unnatural process literally turns the soil to dust. And yet, going back to the three sister method and other similar techniques isn't ideal either, as the crops are much harder to harvest mechanically. 

With the expansion of the roller mill and increased wheat demand, hard wheat emerged as the preferred type to use in production. However, since hard wheat roots are shallow in comparison to older kinds of wheat, they are also more susceptible to drought. In the 1930s, this resulted in the _Dust Bowl,_ when the previously healthy prairies that had been turned into farmland became so dry that their soils blew into the air, forming dust clouds over America, blocking the sun and forcing settlers in the Midwest to pick up and leave.

Techniques to make chemicals for bomb production during the war were then repurposed to produce fertilizers, which in turn refertilized the soil and maintained these frail monocultures. But there's an adverse trade-off with this method: it stuffs our rivers and waterways with excessive amounts of chemical fertilizer residues.

### 5. Depleted soil results in food with less taste and fewer nutrients – which is harmful to our health. 

Since soil is food for the plants that grow in it, imagine what happens to them if they only eat junk food. That's what's happening today: food grown in monocultures lacks nutrients, which affects our health. 

When plants can't get what they need to thrive from the soil, they become stressed. Just like us, this makes them more susceptible to sickness and pests.

In the mid-twentieth century, agronomist William Albrecht shed some light on how health and soil quality correlate with one another. He reviewed the draft records for WWII in Missouri and discovered that out of 1000 recruits from an area with poor soil, 400 were rejected, compared to only 200 in a rich soil area.

During the last 50 to 70 years, up to 40 percent of nutrients have been lost in what researchers call _biomass dilution_. Some scientists argue that a lack of micronutrients might be the culprit behind higher obesity rates, since we now need to consume more calories in order to get enough nutrients and feel satisfied.

Another argument for changing the way we grow food is that it simply tastes better when it's grown in nutrient-rich soil. At Stone Barns, the author's restaurant and farm, they grow carrots in healthy soil. These carrots contain 16.9 percent sugar and are bursting with flavor. A stark contrast to industry-farmed organic carrots, which, when tested, registered at 0 percent sugar.

Plants living in good soil absorb micronutrients in a process similar to making coffee. Think of it like this: when hot water drips slowly through ground coffee beans, the coffee will be richer than if it is made by pouring hot water over whole coffee beans.

So it's clear that bad soil equals bad food for us, but what about the animals that also live off it?

> _"Healthy soils brings vigorous plants, stronger and smarter people, cultural empowerment, and the wealth of a nation."_

### 6. Despite large-scale meat production, we eat less of each animal than ever before. 

Picture-perfect scenes of chickens frolicking freely around the barnyard were a reality a few decades ago. But times have drastically changed since then. What happened? The American broiler chicken: a prime example of how we've industrialized meat production.

It started with The Perdue Chicken Company, founded in the 1920s to produce eggs; chicken meat was only a by-product. But when Frank Perdue, the son of the eponymous founder, took over the company, he saw the chance to focus on meat production.

With the help of new, specialized breeds, grain-fed chickens could now reach their slaughter weight in just seven weeks — half the time it took before. Frank boosted his profits by making his own grain feed, which reduced the cost of chicken rearing. This cheap feed, combined with the rapidly growing broilers, helped make chicken rearing a large-scale production. This same process can be seen in all meat production.

The story of the farmed chicken is also an example of how we now consume less of the animal. Ever notice how most of our chicken is sold in parts? Up until the 1970s, 80 percent of chickens were sold whole, with bones and skin. However, Perdue and their competitors spotted an opportunity for profit if the chicken was cut into smaller pieces. 

This idea was supported by the low-calorie health trends of the 1980s and 1990s, when boneless, skinless, lean chicken grew by 50 percent. At the end of the 1990s, pared down, processed chicken accounted for 80 percent of purchased chicken.

So we now eat less of the chicken, even though we produce 37 _billion_ tons of it in the United States per year. And what happens to all the parts we don't eat? They're turned into fertilizers, dog food and the like. And the same process applies to the other animals.

So what's the alternative to such wasteful farming? Read on to the next blink to find out.

### 7. The Spanish dehesa: a biodiverse culture that produces some of the best food in the world. 

The Spanish word _tierra_ is often translated as _land_. But it's far more than that: it's the earth, roots, climate and so on — a complex, biodiverse place. The _dehesa_, in the Southern region of Extremadura in Spain, is one example of this. Once filled with flocks of Merino sheep, the land is sectioned off by stone, crops and oak trees. On the surface it might seem barren, but it's home to one of the world's most beloved foods.

The world's best ham, _Jamón Ibérico de Bellota_, hails from the dehesa. This ham comes from the Iberian pig, whose food of choice is acorns, which give the ham its signature nutty flavor. As the pigs are free to roam and exercise, their muscles make room for oleic acid, which comes from the acorns and is the same unsaturated fat found in olive oil.

The pigs do not receive any additional food — they feed only off the field when there are no acorns. Waiting for the acorns increases the pigs appetite for them, so when they finally fall to the ground, they gorge on them. This gorging forms the fat cap, i.e., the fat surrounding their muscles, for which _Jamón Ibérico_ is so famous.

But ham isn't the only delicious food that the complex ecosystem produces. Merino sheep still graze the grass in the dehesa. And although they aren't traditionally a milking sheep, Merinos are milked to produce two of the world's finest cheeses: _Torta del Casar_ and _La Serena_. 

The dehesa also houses geese which produce _foie gras_, or fatty goose liver. The geese roam freely and when they feel the winter chill approaching, they overeat, creating a naturally fatty liver.

The complexity of the dehesa means that these products are a product of the entire land. Now let's take a look at another complex system: the ocean.

### 8. Fishing and agriculture have depleted the ocean of fish. 

If industrialized farming on land wreaks havoc there, why not turn to the resources of our seas? Bad idea. We've already taken so much from our oceans that overfishing has reached critical levels.

Over the past 60 years, fishing increased fourfold, from 19 million tons in 1950 to 87 million tons in 2005. Now, over 85 percent of the world's fish are endangered.

Just as WWI technology gave rise to monocultural agriculture, WWII wartime technology, like sonars, were used to increase fishing yields. Together with the practice of bottom trawling, this has almost emptied our oceans of fish. Bottom trawling also hauls out fish that are too small to use or otherwise unwanted.

To make matters worse, eating fish high up in the food chain intensifies the problem. Many of the most sought-after fish, such as bluefin tuna, halibut and cod, are high on the food chain; they eat other fish and take longer to reach full maturity. However, people like Spanish chef Ángel Léon have come up with a more sustainable approach to fish consumption. At his restaurant, Léon makes good use of small fish and fish that are usually discarded, even using pureed fish eyes to thicken his sauces. Such thrifty ideas are better for our environment and have earned him the title of "Chef of the Sea."

Our monocultural grain farming also affects our waters. Many of the chemical fertilizers we use on our plants are never totally absorbed and flush into the sea. This saturates our oceans with nitrogen and other chemicals, making it impossible for plankton and other sea life, which other fish need to feed on, to survive.

One example of this can be seen at the mouth of the Mississippi River, where an 80,000 square mile dead zone lies off the Gulf of Mexico. This was created by the fertilizers from the Midwest.

### 9. Traditional fish farms are bad for the environment, but there are alternatives. 

Fish farming presents an alternative to fishing in oceans and rivers. But this is nothing new: China has been doing it since 500 BC.

The annual growth in fish farming, which hovers around 8.8 percent, is just enough to keep pace with the global increase in fish consumption. Some even argue that, by 2018, all the fish we eat will have been farm-raised. While this doesn't seem problematic at first sight, the environment is suffering for it.

Most fish farms are situated on shorelines or estuaries. These are complex systems where saltwater meets river water. It's also the most active part of the oceans, where fish spawn and breed. The problem is, a shoreline fish farm functions like a monoculture in that it undermines biodiversity.

Farmed fish require food. Often, it's other fish caught in the wild. To raise one pound of farmed fish, three to five pounds of wild fish are needed. It's either that, or using grains from our seriously flawed agricultural system.

But all's not lost, though: fish can be farmed in a better way.

Remember the dehesa? Well, the Spaniards have also come up with a different kind of fish farm: enter _Veta la Palma_.

The land here was once drained by channels to make pasture for cattle, and these channels have been used to flood the system with estuary water to create fish farms. Merged with the water from the Guadalquivir river, it's become an ideal breeding ground for the phytoplankton that shrimp feed on. These shrimp then become feed for the fish that are farmed there. 

The canal fish farm also functions as a filter for the overfertilized water that runs off the farms and flows into the river. As a result, this natural mimic of a shoreline produces high-quality, natural-tasting fish.

So, once again, we see how complex systems often mean a better environment for creating healthier and tastier foods.

### 10. It all begins with seeds: they hold the key to the future of our food. 

Whether it's grains for animals or for human consumption, they exist thanks to seeds. Seeds hold the answer to our food future. And, as we know, being ready for the future requires learning from the past. 

Let's look back to the proliferation of hybrid plants, which created today's monocultures. Hybrids are essentially two plants which are crossbred to create a new plant. The plus side of hybrids is that, for example, a corn plant can become more resistant to drought or a wheat plant can produce larger yields.

Dwarf wheat, invented by Norman Borlag, was a revolutionary hybrid. After 15 years of experimenting with hybrids, in 1952, Borlag discovered a Japanese breed called Norin 10, which caught his attention due to its shortness. He went on to create hybrids of Norin 10 and other wheat types. When used with fertilizers, this breed produced three times the yield of normal wheat.

Borlag's hybrid was a huge success. By 1963, 95 percent of wheat in Mexico was of this variety. It also helped a famished India triple its wheat production and start exporting wheat. In a sense, this was fantastic progress. But it also had some serious side effects. 

This kind of seed monoculture is the basis for the world's monocultures. As we've seen, monocultures have grave impacts on our environment and our health. Some people argue that cases of certain types of cancers, diabetes and cardiovascular disease have increased due to these monocultures.

The future of food, then, is to go back to collecting our own seeds. Rather than using hybrids, we should base our cuisine on several varieties of grains, vegetables, beans, etc. This is, after all, how cuisine was created: by using what the land has to offer and creating delicious dishes with it; not by every person in the world eating the same thing.

> _"The fertilizers made the new varieties possible. The new varieties made the fertilizers necessary."_

### 11. Food production affects what we eat, so let’s look at the menu of the future. 

We now understand that monocultures have a negative impact on our environment, our health and our cuisine. So what might sustainable cuisine look like? Let's take a look at two dishes.

First, the beloved risotto: a classic first course at many high-end restaurants. However, it's also the quintessential monoculture dish of rice, stock and cheese.

This could be tweaked by creating _rotation risotto_, combining different grains grown in rotation to replenish the soil. Rye could be used to infuse the soil with carbon, barley to suppress weeds, buckwheat to cleanse the soil of toxins and legumes to give the soil nitrogen.

To get the perfect consistency, you could add a puree of brassicas, such as kale and broccoli. These brassicas have the added benefit of also providing the soil with nitrogen. This very dish is available on the menu at the author's New York City restaurant Stone Hill.

Next, let's rethink a meat dish. How can we make the most of the meat we eat? The nose-to-tail method, where the whole animal is used, has been en vogue in many restaurants. But let's take it one step further with the _blood-to-bone_ method.

With a view to creating a dehesa-like environment, the author introduced the Ossabaw pig breed to his Blue Hill farm. With similar characteristics to the Iberian pig, it helped shape the land. It also bred with other pigs on the farm, giving rise to a breed they call _Crossabaws_.

The dish they make from these pigs consists of grilled meat to infuse extra flavor, and the pig bones are made into a charcoal that seasons the meat. To ensure nothing goes to waste, the blood of the pig is then used in a blood sausage to complete the dish.

### 12. Final Summary 

The key message in this book:

**The way we eat affects the way we produce our food, and vice versa. But the way we farm and produce food today is unsustainable. Still, there is hope. We are capable of growing, producing and cooking food that is better for our environment and tastes great, too.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _In Defense of Food_** **by Michael Pollan**

_In Defense of Food_ is a close examination of the rise of nutritionism in our culture, and a historical account of the industrialization of food. An expert in food ecology, author Michael Pollan takes a look at the way in which the food industry shifted our dietary focus from "food" to "nutrients," and thus narrowed the objective of eating to one of maintaining physical health — a goal it did not accomplish.
---

### Dan Barber

Dan Barber is a chef and co-owner of Blue Hill in Manhattan and Blue Hill at Stone Barns, just north of New York City. He is a key proponent of the farm-to-table movement, has been featured in several TV shows and is famous for his TED talk _How I Fell in Love with a Fish_.

