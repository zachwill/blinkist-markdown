---
id: 57754c9386883200034f4727
slug: the-third-wave-en
published_date: 2016-07-08T00:00:00.000+00:00
author: Steve Case
title: The Third Wave
subtitle: An Entrepreneur's Vision of the Future
main_color: ECE82F
text_color: 87851B
---

# The Third Wave

_An Entrepreneur's Vision of the Future_

**Steve Case**

_The Third Wave_ (2016) explains that, at this point in the information age, entrepreneurial relevance and success depend on awareness about the current environment. Find out how the next revolutionary innovators will be disrupting the status quo by using the internet to change mainstay industries like healthcare, food and education.

---
### 1. What’s in it for me? Glimpse the future of the internet. 

No more than a fad 20 years ago, the internet has become a central part of most people's everyday life. And it looks like it'll only continue to permeate our lives in ever new ways. We are approaching the third wave of the internet revolution, a time when our use of information-age technology will change and enter a new stage.

Let's look at how we reached this third wave and what we can expect from it. We'll explore the fields that'll be revolutionized, how this may happen and what types of obstacles entrepreneurs will face in the new era.

In these blinks, you'll find out

  * how the third wave will change the healthcare industry;

  * what "impact investing" is and why it's a key feature of the third wave; and

  * that cooperation is sometimes more effective than competition.

### 2. The history of the internet has had two distinct phases, and a third phase is beginning now. 

In 1980, futurist Alvin Toffler wrote _The Third Wave_, a book describing how the next "wave" of evolution in the information age would transform the world and create an electronic global village.

But to understand this "third wave," let's first take a step back and look at what came before.

The _first wave_ began with the creation of the foundation and infrastructure of the online world.

Companies like Sprint, Cisco, Apple, Microsoft, AOL, and IBM created crucial hardware, software and networks, making the internet available to everyday users.

This wasn't exactly an easy task. These online pioneers faced an uphill battle, struggling to convince people that the internet was relevant and worthwhile. Indeed, many saw it as a fad. In a 1995 interview on PBS, the author was asked, "Why do people need [the internet]?"

By the turn of the twenty-first century — the beginning of the _second wave_ — the answer to that question was more than apparent.

By this time, search engines like Google were helping people navigate ever vaster amounts of information; online marketplaces like Amazon and eBay appeared; and social media platforms like Facebook and Twitter emerged, connecting more and more people.

The second wave further revolutionized the world when mobile devices like Apple's iPhone and Google's Android enabled people to access the internet wherever they went.

This brings us to the _third wave_, on the threshold of which we currently stand.

The third wave is about integrating the internet into everything we do. No longer limited to PCs, tablets and other devices, the internet is becoming part of our homes, automobiles, agriculture and cities. This technologically-integrated age has also been referred to as the _Internet of Things_.

The third wave will find entrepreneurs taking advantage of this new era to challenge major industries such as healthcare, education, food and commuting.

So, let's dive in and discover what these future fields of entrepreneurship may look like.

### 3. Healthcare and food will be a major focus for third-wave entrepreneurs. 

The third wave of the internet isn't about creating the next viral game or social media app; it's about innovatively addressing real-world problems.

Consider health care, an industry that offers great potential for entrepreneurs.

Health care is a huge market, making up one-sixth of the US economy, and since it suffers from longstanding problems, there's plenty of room for technological improvement.

The US healthcare system is notoriously slow, inefficient and expensive; many hospitals still use fax machines and hard copies of medical records, and, due to a lack of coordination and accessible data, mistakes are constantly being made.

For instance, in 2011, researchers at the cancer center MD Anderson found that one out of every four patients was getting a new diagnosis following a second opinion.

Many entrepreneurs have already jumped at this opportunity: between 2010 and 2014, the amount of money raised by digital health-care start-ups quadrupled.

So, before long, smartphones will diagnose your symptoms, check your medical background and warn you _before_ you have a stroke or heart attack. This constant monitoring of high-risk patients from home could potentially reduce health-care spending by 30 percent — money that would otherwise be drained into misdiagnoses and incorrect treatments.

Another enormous opportunity for these entrepreneurs is the five-trillion-dollar food industry.

The third wave will revolutionize how we grow, raise, store, transport and deliver food.

For example, a report in _The Guardian_ revealed that innovative researchers are already finding ways to improve bee colonies by connecting their hives to the internet. Bees are vital in the pollination of crops and their numbers are declining. But internet technology can monitor hive temperatures and create heat in order to deter the mites that lead to colony collapse.

Entrepreneurs are also developing sensors to monitor food safety, even going so far as to imagine ovens that will refuse to cook spoiled meat! Such technology is badly needed in processing plants today, where USDA inspectors check less than one percent of processed meat.

As we can see, the third wave represents a serious change to the old ways of doing business.

### 4. Disruption is an inevitable part of the third wave that companies must embrace in order to succeed. 

In the wake of the third wave, ideas will continue to come from unexpected places, causing disruption that entrepreneurs should embrace. These ideas will lead to old paradigms being shifted or completely destroyed, which means success will only come to those who take risks and recognize future opportunities.

The self-driving car is a perfect example of an innovative idea that emerged from a surprising place — in this case, the agricultural sector.

Companies like Google and Uber are now embracing this technology, but it actually goes back 20 years, when the Illinois-based tractor company John Deere developed GPS navigation systems for self-driving tractors.

Unfortunately, the John Deere executives didn't see the true potential of their innovative idea.

Even though it's unlikely that John Deere would have had the means to build self-driving commercial cars from scratch, they ignored the opportunity to license the technology to a partner company or branch out into new territory. Had they been willing to take a chance and disrupt their business model for something new, who knows what level of success they would have achieved.

In other cases, _self-disruption_ might be the key to future success. This entails being aware of your customer's current preferences and not being afraid to get rid of tried-and-true products or services in favor of new ones.

Apple has done this multiple times. Some of their biggest successes have come at the expense of their previous products. Apple was not afraid to hurt iPod sales by coming out with the iPhone, or limit MacBook growth by releasing the iPad.

Amazon is another expert at self-disruption. After building its reputation selling physical media, it successfully transitioned into streaming movies and TV shows, and introduced the Kindle for customers to buy and read electronic books.

Successfully riding the third wave means getting out of your comfort zone and not underestimating how fast those tidal waves of change can come. But, as we'll see in the next blink, staying on top also means looking beyond the profit margins.

> "If you don't cannibalize yourself, someone else will." - Steve Jobs

### 5. Investors in the third wave will look at both the profit and the social impact of their investments. 

Historically speaking, corporations and CEOs have long adhered to economist Milton Friedman's old dictum: "The social responsibility of business is to increase its profits." Investors have also been happy with this philosophy, but as the third wave rolls in, we're seeing a change.

There's been an increase in _impact investing_, a third-wave megatrend that merges traditional business and philanthropy.

Through impact investing, corporations recognize the importance of creating both financial return and social good. This results in a "best of both worlds" scenario where companies can combine profit and purpose. And this is what we see today; major institutions such as investment banks, asset management firms and private equity investors are all embracing impact investing.

But this kind of practice is still in its early days:

In 2013, global wealth reached over $150 trillion, but only $50 billion went toward impact investing. However, current projections suggest that by 2020, this amount will increase twenty-fold to exceed $1 trillion.

This is mostly due to millennials, who are committed to creating positive social change, both as employees and consumers. This new generation wants to support companies that have a positive impact on society.

And this trend has been supported by corporate policy-makers, too.

In July of 2013, the G8 governments created the Social Impact Investment Taskforce to help foster a profitable, purpose-driven marketplace made up of _benefit corporations_, or B corporations. As a result, many US states have created laws to support such companies.

These organizations include Patagonia, Kickstarter and Etsy, all of which are focused on issues like climate change, job creation and supporting the arts — in addition to making a profit.

Kickstarter is a perfect example of a public-benefit company. Their mission statement is to use a global crowdfunding platform for the sole purpose of helping bring creative projects to life.

### 6. The third wave will encourage innovation from geographically diverse places. 

For some time now, technology hubs like Silicon Valley, and creative business meccas like New York, have maintained the status quo when it comes to brilliant ideas. But this will change as the third wave engenders "the rise of the rest."

This rise in innovation will come from on-the-ground experts in diverse geographic regions.

Removed from the usual insulated areas, their location will afford them direct access to the businesses they aim to affect and provide them with the kind of experience that brings about useful innovation.

After all, it makes more sense for a new food company to operate out of the Midwestern United States, where there's agricultural infrastructure, or for a health-care company to be based in Baltimore or Nashville, where you can find many medical institutes.

Take Baltimore's Johns Hopkins University. With its extensive research programs, it's often considered one of the best medical schools in the world. So, it stands to reason that you'll find a more innovative idea for your health-care business here than across the country in Silicon Valley.

This rise of the rest will also create new opportunities and improve economic growth.

This is because start-ups are the engine of America's economy: over the past 30 years, new companies less than a year old have created an average of 1.5 million jobs per year.

In fact, The Kauffman Foundation, a non-profit organization focused on entrepreneurship, found that new businesses like these are almost completely responsible for the overall increase in new jobs.

There's the Detroit-based Shinola, for example — a company that employs hundreds of former autoworkers to build such products as watches, bicycles and handbags. Their motto is "Where American is Made," and they've found success exporting their goods around the world.

Venture capitalists would be wise to start paying attention to what's going on outside of the usual locations. But, as we'll see in the next blink, these third-wave entrepreneurs won't be revolutionizing the economy completely on their own.

### 7. Building strong partnerships and credibility will be essential for third-wave entrepreneurs. 

Second-wave companies, like Google and Amazon, managed to become overnight sensations by single-handedly creating massively popular products. While this method proved successful in the past, it won't work in the third wave.

Like companies during the first wave, innovative third-wave companies need to build strong partnerships with industry gatekeepers. Otherwise, these big corporations may regard new entrepreneurs as competition and block their path.

So there are lessons to be learned from the first wave:

When Apple was developing iTunes and the iPod, they needed to license music content that people could download. As a result, they needed to build partnerships with companies that might see Apple as a business threat.

But at the time, iTunes was only a Macintosh program, and the computer only had a two-percent share of the market. This allowed Apple to pitch iTunes to the music corporations as a risk-free experiment, where they could safely test out online music sales. For Apple and the record labels, this was a win-win proposition.

So, to follow this proven path to success, third-wave entrepreneurs will have to make early partnerships to build credibility and gain momentum in order to win over the gatekeepers.

And to find the right partners, they will need to look in diverse places:

For instance, when AOL was starting out, it hustled non-stop before finally landing a willing partner with the Commodore computer company. But they didn't stop there — they kept the momentum going, with Commodore leading to a second partnership with Tandy, then a deal with Apple and finally a contract with IBM.

So, even if you have a great product or a spectacular idea, you might never get them off the ground unless you make partnerships a core part of your strategy.

> _"If you want to go quickly, go alone. If you want to go far, go together."_ \- African proverb

### 8. Working closely with the government will be especially important for future entrepreneurs. 

Success in the third wave of the information age doesn't only depend on finding like-minded business partners. Entrepreneurs will also need to work closely with the government.

After all, the government has always played a crucial role in innovation.

Government organizations routinely take risks by funding research, supporting projects and ideas that the private sector won't or can't touch. In fact, without federal assistance, many of today's technologies wouldn't exist.

For example, the GPS technology in our cars and mobile devices is due to the Department of Defense, which created a satellite-based global positioning system as part of its nuclear deterrence program.

But the best example might be the internet itself, which was created in 1972 as part of the Advanced Research Projects Agency (ARPA), a government agency created by President Eisenhower in 1958. The internet's potential in private and commercial use only became apparent when the government passed the Telecommunications Act in 1996.

Therefore, it's important to understand that the government's role will only become more crucial in the future, and in order to succeed you'll have to work alongside it — mainly because third-wave entrepreneurs will be targeting highly regulated industries.

Just think: Would you want to ride in self-driving cars that didn't receive government approval?

So, to ensure both short-term and long-term success, future innovators must work hand in hand with the government to make sure that they're operating within the bounds of legislation. This kind of relationship can even help pave the way for future laws and policies.

Certainly, it's not uncommon to think of the government as a terrible tangle of bureaucracy and red tape. But if you have revolutionary ideas about changing the way we lend money, or the way we deliver government services, such as food or new forms of energy, the government must be a partner, not an adversary.

### 9. Final summary 

The key message in this book:

**The Third Wave is the next phase of the internet and information age, and it is fast approaching. This next step in our technological evolution will be marked by businesses and entrepreneurs who possess strong partnerships, purpose-driven missions and a clear understanding of government policy.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Industries of the Future_** **by Alec Ross**

_The Industries of the Future_ (2016) gives a sneak peak at the effects information technology and the next wave of innovation will have on globalization. These blinks explain how people, governments and companies will need to adapt to a changing world driven by big data.
---

### Steve Case

Steve Case is the cofounder of AOL and the CEO of Revolution, an investment firm in Washington, D.C. that supports entrepreneurs and companies such as Zipcar and Sweetgreen. He is a member of the Presidential Ambassadors for Global Entrepreneurship and a founding chairman of the White House initiative Startup America.

