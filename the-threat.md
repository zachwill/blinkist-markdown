---
id: 5ca283ec6cee070007a590e0
slug: the-threat-en
published_date: 2019-04-03T00:00:00.000+00:00
author: Andrew G. McCabe
title: The Threat
subtitle: How the FBI Protects America in the Age of Terror and Trump
main_color: CDA09E
text_color: 804B49
---

# The Threat

_How the FBI Protects America in the Age of Terror and Trump_

**Andrew G. McCabe**

_The Threat_ (2019) offers an inside look at America's famous nation-wide law enforcement agency, the Federal Bureau of Investigation or FBI. Written with the lucid precision you'd expect from a high-ranking former FBI official, this book depicts the organization's inner workings, details the methods it uses to protect the public, and explains why terrorism and President Donald Trump are currently the nation's biggest threats.

---
### 1. What’s in it for me? Get a glimpse into the FBI’s inner workings. 

"Fidelity, Bravery, Integrity." That's the motto of America's Federal Bureau of Investigation (FBI), and these words are just as important for the Bureau today as they were when it was founded back in 1908. In this period, the threats facing America have evolved, and so has the Bureau itself. In the 1920s, mobster violence was the main threat; in the 1970s, the FBI was in the vanguard for the "war on drugs." And after September 11, 2001, the nation and the FBI entered a new era: that of the "war on terror."

As former deputy director Andrew McCabe argues, the FBI is not the same organization it was on September 10, 2001. This war on terror necessitated new tactics and approaches, and old methods and structures needed to be updated. To do this, the Bureau was reorganized and revamped.

And along with the ongoing issue of terrorism, a new threat to the American people has emerged since 2017: President Donald Trump. His attempts to influence the FBI and tip the scales of justice in his favor are a new and unprecedented threat to democracy.

In these blinks, you'll learn

  * why terrorism and organized crime investigations are much the same;

  * how terrorist suspects are interrogated; and

  * how President Trump is undermining an important FBI investigation.

### 2. The FBI has changed dramatically since 9/11. 

Even though almost two decades have passed, most people vividly remember September 11, 2001. When New York's Twin Towers collapsed after being struck by hijacked aircraft, everybody watching knew this was a momentous event. Even so, few could've predicted just how radically it would alter American society and the wider world.

One organization, in particular, changed irreversibly and almost overnight: The Federal Bureau of Investigation, or FBI — the US federal law enforcement agency.

To be clear, the FBI has always investigated terrorist threats. The Counterterrorism Division and its criminal and counterintelligence equivalents have long been the Bureau's three main branches. The agency mainly concentrated on high-level criminal activity, however, like organized crime and drug trafficking.

This is still a crucial function of the FBI, but 9/11 completely changed the rules of the game and the Bureau shifted its central focus to protecting American citizens by preventing acts of terrorism.

Overnight, counterterrorism operations took precedence, and funding and human resources for the division responsible grew quickly.

This meant, among other things, a growth in the number of units within the Counterterrorism Division. Before September 11 there were only two within the Counterterrorism Division — an Osama bin Laden Unit and a Radical Fundamentalist Unit. After September 11, dozens of counterterrorism units were created, including those dedicated to the financing of terrorism and weapons of mass destruction. But these changes went further than simple resource allocation or restructuring: 9/11 transformed the Bureau's internal culture.

Before the attacks, the FBI was riddled with division along both departmental and interpersonal lines. There was a fierce rivalry between the criminal and counterterrorism divisions, for example, with each believing they did the "real" work. And the FBI employs over 30,000 people, which requires a diverse range of skills, from bulky strongmen who excel in hand-to-hand combat to computer science experts working on cybersecurity. Often, those with different specializations couldn't find common ground.

Again, 9/11 changed this. The day after the attacks, it was like a gust of air had blown through the stuffy corridors of the FBI's Washington headquarters. There was a new sense of unity — everyone there was FBI, and everyone was in it together. The attacks were a watershed moment: The Bureau would never be the same again, and neither would its methods.

### 3. Enterprise theory is one of the Bureau’s primary investigative techniques. 

The FBI has a mammoth task: Keep the peace, uphold the US Constitution and keep over 350 million American citizens safe. Threats to that safety can come from solitary, "lone wolf" perpetrators, but some of the most dangerous arise when individuals band together to form crime families or terrorist groups.

Thankfully, the FBI employs a number of sophisticated techniques to identify and prosecute these syndicates. One of these is an investigative technique called _enterprise theory._

Enterprise theory was developed after the passage of the Organized Crime Control Act in 1970. Before this, tackling organized crime was difficult because criminal investigations could only focus on prosecuting a single individual and his suspected crimes. But the Crime Control Act allowed agents to probe entire criminal organizations in a single investigation, allowing a gang member to be prosecuted for _all_ the crimes committed by his crew.

Figuring out how to structure an investigation into a vast criminal network can be incredibly challenging — the size of the networks alone can be overwhelming. That's where enterprise theory comes in. The first step involves proving the existence of the criminal enterprise in a legally irrefutable way. To do this, agents gather evidence which links individuals together, building up webs of association.

Imagine that the FBI search the house of a mobster named Vito. Before enterprise theory, agents were solely looking for evidence that Vito had committed a specific crime. After enterprise theory, it became equally important to search for photographs of Vito hanging out with other suspected mobsters.

The second step involves proving that a particular mobster has participated in two or more of the enterprise's crimes. This is notoriously tricky, so enterprise theory was used to develop new forms of evidence gathering and enhance existing forms.

Take witness testimony, which has always been a powerful form of evidence. This can come from former criminals who cooperate with the police and provide information on their enterprise — these are _cooperating witnesses._ Another type, which the FBI specifically developed to tackle organized crime, involves inserting an undercover agent into a group to gather evidence.

These are called _undercover employees_, and they are some of the most highly skilled agents in the FBI. They have to be — their job involves gaining the trust of a criminal organization by convincing them that they are a career criminal. If an undercover employee is successful, the firsthand evidence they can gather is usually devastating.

### 4. Slowly, the FBI has transitioned from muscling to targeting. 

Brains or brawn? People often see the two as mutually exclusive, but things are more complicated than that in a field like law enforcement. Intelligence and strength are both crucial to the FBI, although there has been a shift in the way agents strategically employ them in recent years.

Over the last 15 years, agents have started to prefer _targeting_ techniques rather than a _muscling_ approach _._

Muscling tactics ruled the roost in the old FBI. This doesn't mean that agents used excessive violence — it just describes their methods for gathering intelligence and developing cases. Muscling means taking a blanket approach, exploring every single lead and analyzing every minor piece of intelligence until the data is exhausted.

This approach was especially popular after 9/11 when thousands of calls would come into Bureau headquarters every week about suspected terrorists. In this climate of heightened fear, the agency would throw resources at every single inconsequential lead: Agents worked punishing overtime hours, analysts scrutinized every line of every spreadsheet, and administrators opened case after case.

For a hypothetical example of this approach, imagine a phone call has been intercepted between two known terrorists. In the call, they make repeated references to the "brothers in California," and the FBI wants to identify these brothers.

A muscling approach would require reexamining every case in every FBI office in California. It would require investigating every male within a target age range who had traveled to California from certain countries. It would probably involve sending agents out to look physically for suspects on the street, too. Needless to say, muscling is extremely resource- and time-consuming. A better approach, and the one preferred by the FBI today, is targeting.

Targeting doesn't try to be everywhere at once. Instead, it takes a much more specific and precise approach to investigations and employs intelligent techniques for gathering and organizing data.

To identify the "brothers in California" using targeting, agents would build a database of terrorists and their associates in California and its surrounding states. Then analysts would query the data and search for sibling relations or other significant familial ties. If they found a match, that could justify surveillance of the suspected individuals to confirm they are the brothers mentioned in the original intercepted call. Usually, this approach would be sufficient to identify our "Californian brothers."

Muscling and targeting techniques are normally used to identify suspects. But what happens once a suspect has been detained?

### 5. On President Obama’s orders, the Bureau changed the way it classified interrogation targets. 

One consequence of President George W. Bush's "war on terror" was the need to hold and question a large number of suspects. Many suspected terrorists were held in the now-infamous Guantànamo Bay detention facility in Cuba. At this facility, suspects were treated brutally, deprived of sleep and subjected to inhumane interrogation practices. The United States came under harsh international criticism for its techniques at Guantànamo, and President Obama decided it was time for change.

In 2009, he ordered the creation of a new group — the High-Value Detainee Interrogation Group (HIG).

This group involved three government agencies — the FBI, the Central Intelligence Agency, CIA and the Department of Defense — and was a clear attempt to move away from the abuses at Guantanàmo. The HIG was strictly controlled and accountable to the National Security Council in the White House.

This organization aimed to professionalize the questioning of terrorist suspects and ensure that all their interrogation tactics were legal and humane. From then on, the only techniques permitted were those listed in the US Army Field Manual or those already used by the FBI when investigating domestic criminals.

The HIG also established a new protocol for categorizing targets for interrogation, identifying them as one of two types: _predesignated_ or _pop-up._

Predesignated targets are the highest value targets — well-known suspects at the top of the terrorist food chain. If former Al Qaeda leader Osama bin Laden had been captured alive, he would have been a predesignated target.

But a pop-up target is far more common: They are suspects not previously known to law enforcement. Sometimes, pop-up targets are nominated — anyone in law enforcement can contact the HIG to nominate someone as a pop-up target. In other cases, a pop-up target becomes obvious through their actions.

A famous example of a pop-up interrogation target is Umar Farouk Abdulmutallab. On Christmas Day 2009, Abdulmutallab attempted to detonate explosives hidden in his underwear on a flight from Amsterdam to Detroit. His device failed to detonate, and he was taken into custody upon landing.

Before Christmas Day, Abdulmutallab wasn't known to intelligence agencies; afterward, he became the most important person to speak to in the United States and a pop-up interrogation target for the HIG.

Whether the subject of interrogation is a captured terrorist or a suspected criminal, all FBI interrogations are part of investigations. In the next blink, we'll explore exactly how these investigations work.

### 6. There are three types of FBI investigations, and each has a different level of regulation. 

Thanks to exaggerated plots in action movies, it's easy for us to believe that government agencies like the FBI are somehow all-knowing and all-powerful.

But the almighty FBI, allowed to investigate whomever and search whatever it pleases, is a Hollywood myth. The operations of the Bureau are kept in check by highly structured rules, constrained by law and accountable to the government. There are strict rules that govern _why_ someone can be investigated and _what_ can be searched for and used as evidence.

The FBI has the power to open three different types of investigations, each with different rules and permissions.

The first and most basic level is an _assessment._ Any agent can begin an assessment, as long as she has a clearly defined objective. Most assessments begin when the FBI receives unsolicited information about someone. So if an anonymous caller reports you as a car thief, an agent can open an assessment on you with a clearly defined objective: determine whether you have committed grand theft auto.

As assessments are the most basic type of investigation, the tools that an agent can use in them are limited. She is permitted to review information that government agencies hold on a suspect, query online resources like social media sites, interview people and physically follow your movements.

The second level is a _preliminary investigation._ To initiate this, an agent must have information which suggests you have committed a crime or will do so in the future. In addition to the investigative tools of an assessment, an agent conducting a preliminary investigation can use CCTV to monitor your movements. She can also order internet providers and mobile phone companies to provide her with your account information. She isn't allowed to read the contents of your electronic communications, though.

The final level is a _full investigation._ For this, an agent must have factual evidence that a crime has taken place or will take place. A full investigation allows FBI agents to search a suspect's home, providing they have applied for and received a warrant from a court. It also allows the FBI to monitor electronic communications, but they need a warrant for that too.

Rules that structure the behavior of law enforcement agencies are crucial in safeguarding citizens' rights and delivering justice fairly and impartially. Traditionally, you could count on the US president to respect this system, but as we'll see in the next blink, that's no longer the case.

### 7. The FBI is being undermined by the Trump administration. 

Each section of a state in a healthy democracy needs to be able to carry out its responsibilities free from internal or external influence. Government and law enforcement should work seamlessly but remain independent of one another.

So things clearly aren't working as they should when we can say, quite seriously, that the President of the United States is compromising the work of his own law enforcement agencies.

Why does he do this? Well, he doesn't trust the FBI, for a start.

President Trump sees the Bureau as a pillar of the Washington establishment. Built on fiery anti-establishment rhetoric, the president's campaign promised to "drain the swamp" of Washington D.C., which he viewed as full of corrupt career politicians and slimy civil servants. And the president's mistrust extends to government institutions like the CIA and FBI.

Consider his reaction to North Korea's missiles. In July 2017, the country made headlines for completing a series of intercontinental missile tests. But Trump claimed these tests were a hoax — Vladimir Putin had told him so. When intelligence officials tried to explain that this claim was completely inconsistent with existing evidence, Trump insisted that he still believed Putin.

And then there's the investigation into Russian interference in the 2016 presidential election.

Shortly after the election, it became clear that someone had tried to influence its outcome. Voting databases were hacked, politicians' private communications were released, and social media was manipulated to taint public debate. And all evidence unambiguously points to one perpetrator: the Russian state.

The FBI opened an investigation, and it quickly became more complex: numerous members of Trump's campaign team had close ties with Russian officials. For example, Trump's former campaign chief Paul Manafort met with Russian officials several times during the campaign and has a history of dodgy business deals with Russian and Ukrainian oligarchs.

Most shockingly though, President Trump is constantly attempting to use his power to influence the outcome of the Russia investigation. He's asked multiple FBI directors to confirm that he isn't under direct investigation and regularly takes to Twitter to call the Russia investigation a "witch hunt."

In this way, the president communicates his partisan views on an ongoing criminal investigation and signals his desired outcomes to judges and juries, potentially influencing their decisions. This is corrosive to democracy. No president should attempt to influence an ongoing investigation to suit himself — a president should uphold the rule of law, protect the constitution and support his own law enforcement agencies.

The current president poses a serious challenge to the FBI. But, having faced organized crime syndicates and deadly terrorists before, this is undoubtedly a storm it can weather.

### 8. Final summary 

The key message in these blinks:

**The world changed when the Twin Towers collapsed on September 11, 2001. Nowhere was this change more keenly felt than inside the FBI — the Bureau had to adapt, reposition and refine its techniques to keep Americans safe from acts of terrorism. Almost 20 years on, this remains the United States' biggest threat, but a new one has developed from the most unlikely of places: the White House. With a president distrustful of his intelligence agencies and influencing ongoing investigations, the Bureau will again need to stay vigilant to continue protecting the American people.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Ghost in the Wires_** **, by Kevin Mitnick**

After these blinks, you should be pretty well informed about the FBI's efforts to uphold American law and order. But this is only one half of the story — what happens to those on the wrong side of the law?

_Ghost in the Wires_ is the thrilling story of Kevin Mitnick, one of the greatest criminal hackers in history. As a teenager, Mitnick used his technical mastery to gain access to companies' computer systems, copying their software and downloading personal data. In 1995, Mitnick was arrested by the FBI with over 100 cloned cell phones and dozens of fake IDs.

It's a larger-than-life story of hacking, identity fraud, police chases and jail time. And with your inside knowledge of the FBI, you'll be able to see Mitnick's deadly dance with the law from a unique angle.
---

### Andrew G. McCabe

Andrew G. McCabe spent over two decades of his law-enforcement career working for the FBI. Initially working as a street agent investigating Eurasian organized crime, McCabe transferred to counterterrorism after September 11 and then continued to rise up the ranks. He served as deputy director — the FBI's second-highest position — for two years and is now retired.

