---
id: 56fe4ced8249800007000003
slug: the-three-marriages-en
published_date: 2016-04-04T00:00:00.000+00:00
author: David Whyte
title: The Three Marriages
subtitle: Reimagining Work, Self and Relationship
main_color: 1E8F94
text_color: 1E8F94
---

# The Three Marriages

_Reimagining Work, Self and Relationship_

**David Whyte**

In _The Three Marriages_ (2009), David Whyte combines his worldly experience and talent as a poet to explore the three great loves we cultivate throughout our lives: the love of a vocation, the love of our own deeper self and the love of a special person with whom we choose to share our lives.

---
### 1. What’s in it for me? Discover how to nurture and enjoy all three of your life’s marriages. 

Did you ever consider marriage to refer to anything other than what you would share with your life partner — that is, matrimony? Well, now is the time to reconsider.

These blinks show that, aside from our relationships with our romantic partners, we also have marriage-like relationships with our life vocations and our inner selves. Each of these three relationships demand a great deal of time, struggles and, above all, love. Drawing from a wide array of historical examples, these blinks draw insightful conclusions as to how these three love stories work, and how we can get the best out of them.

In these blinks, you'll learn

  * which author jumped through an open window to declare his love to a married woman;

  * which poet found inspiration from his visit to a zoo; and

  * why we should be happy that Jane Austen never married.

### 2. Love brings foolishness, unfamiliar paths and deaf ears. 

Have you ever fallen so deeply in love with someone that you've found yourself doing something crazy, like writing a terrible love song or performing a reverse striptease?

Don't be embarrassed. For some of us, foolish behavior is how we express our truest feelings.

Consider the love declaration of Robert Louis Stevenson, the famous author of _Dr. Jekyll and Mr. Hyde_. In 1876, aged 26, he was walking through a French town when he caught a glimpse of a woman through a window — and instantly fell in love. 

Without thinking, he jumped through the open window and declared his love for her, astonishing her and her friends. This bold move was not only dramatic, but also effective. The woman in question, Fanny Osbourne, would later become the love of his life.

But love isn't as simple as a spontaneous declaration. As the next phase of Stevenson's story shows, we often need to let go of our familiar and comfortable lives for the sake of love.

Stevenson had one major problem: Osbourne already had a husband and two children in the United States, and when her husband called her back to America, she returned. But a year later, in 1879, she fell ill and sent a telegram to Stevenson asking him to come and join her. 

He didn't hesitate. The penniless author travelled in squalid conditions across the Atlantic Ocean and the continental United States to reunite with her in San Francisco.

In the end, Stevenson's love story would prove to have a happy ending. But the blind pursuit of love can also make you ignore wise advice from others.

The author experienced this himself. In his early twenties, he hitchhiked to London to surprise his holiday girlfriend with a visit. He was so enthusiastic that he ignored the advice of all the older drivers who told him he shouldn't go without warning her. 

When he finally arrived, her expression alone told him that the drivers had been right all along — their affair was over.

### 3. Committing to a romance is a harsh test of love. 

There are few things more confusing than romance. For a while, it seems like the easiest and most wonderful thing on earth; but soon things start getting messy and complicated.

People don't often realize how hard it is to commit fully to a romance. And if lovers want to make it all the way to marriage, they have to be prepared for some tough times. Contrary to popular belief, love does not grant instant happiness; it requires sacrifices and compromises. 

Robert Louis Stevenson again provides a case in point. He endured a life of severe poverty in San Francisco while Osbourne hesitated about divorcing her husband. Osbourne had her reasons for feeling uncertain: being a divorcée had severe social consequences at that time, and marrying an author like Stevenson carried substantial financial risks.

In fact, it's normal to have doubts before making a final commitment. It's wise for lovers to take a period of pause before they get married, so they can be sure they've chosen the right spouse.

If they don't, they might end up like Charlotte Lucas and Mr. Collins in Jane Austen's famous book _Pride and Prejudice_. Lucas rushes into a marriage with Collins for wealth instead of love, and the couple ends up living in separate apartments.

In contrast, the road to marriage was rockier but much more fruitful for Osbourne and Stevenson. Osbourne eventually divorced her husband and committed fully to Stevenson — and to his work. They created a partnership of labor and love that laid the foundation for Stevenson's success. Osbourne helped Stevenson with everything, offering editing advice for his writings, nursing him when he was sick and protecting him from the friends whose wild lifestyles would put his health at risk.

In the next blinks, let's take a look at what it takes to pursue another kind of love — our love for a vocation.

> "_Men marry because they are tired; women because they are curious. Both are disappointed._ " - Oscar Wilde

### 4. To discover your vocation, follow your passions and listen to clues in the world around you. 

Over 600 years ago, a poor, illiterate peasant woman had a vision in which three saints who commanded her to drive the foreign invaders from her country and return the crown to the land's rightful king. And while not all of us have experienced as clear a vision as Joan of Arc, most people have felt a dream or calling deep within themselves. 

But what many of us fail to do is pay close attention to this call. 

One way we block out our calling is through fear of commitment. Too often we think that if we follow one passion, we will have to neglect all others for the rest of our lives. However, one passion may well turn out to be the gateway to another, greater one.

For example, when he was a child, the author was inspired to study marine biology by Jacques Cousteau's ocean-exploring documentaries. But many years later, after working for some time for an environmental conservation NGO, he realized that he had lost this childhood passion and was being guided by a false sense of importance. So, he started exploring what was lacking in his life; after writing a poem one day, he discovered that he had a new vocation: becoming a writer.

If you are still looking for your calling, remember that you can discover the work you love in both the beautiful and unpleasant aspects of the world. The poet William Wordsworth, for instance, was inspired by the rugged, wild beauty of his homeland, Cumbria.

On the other hand, Charles Dickens's inspiration had a darker source: as a youth he worked grueling shifts in a boot-blacking factory. These harsh experiences gave rise to his desire to portray the lower classes of British society, and to help better their lives. His passion made him into one of the most articulate voices fighting for social change in Victorian society.

### 5. When things get tough, embrace and push through the frustration and depression. 

Have you ever spent a whole day trying to motivate yourself to do something creative, and found that no matter what you do, nothing works?

Then you know what it's like to feel blocked: it's frustrating and depressing.

But, paradoxically, we need to learn how to embrace these negative emotions. In fact, it's part of the creative process.

The German poet Rainer Maria Rilke captured this feeling beautifully: he compared being stuck on something to trying to pass yourself through solid rock. But he also realized that such moments were no less valuable than the ecstasies of creative expression. When the going gets tough, we're forced to dig deeper into our inner resources than ever before — and that's what makes us grow.

So what are some practical methods for turning these negative experiences into positive ones? 

You can start by surrounding yourself with inspiring friends like Rilke did.

In 1902, he was suffering from severe writer's block while staying with the French sculptor Auguste Rodin. Tired of seeing the young poet brood around the house, Rodin instructed him to head to the zoo and observe an animal until he felt compelled to write something.

The result was one of Rilke's masterpieces, "The Panther". It describes the imperial panther circling in its cage, bewildered by the nature of its strange prison — an appropriate metaphor for the creative forces trapped inside the author's mind.

Another way of revitalizing our inspiration is by diving into long-suppressed aspects of ourselves. Rilke's mother used to dress him up in dresses when he was a child, because she had always wanted a girl. And as a reaction to his mother's feminizing behavior, his father sent him off to a very authoritarian Prussian military school — a nightmare for the sensitive poet. 

Pressured by both his mother and father to be something he could not be, Rilke felt profoundly inadequate during all of his life. But when he was struggling for inspiration, he could come back to the tragic richness of these childhood traumas.

### 6. Despite its virtues, marriage is not for everyone. 

Many of us grow up with the belief that marriage is a central goal in one's life. But, as the life story of Jane Austen shows, you can't always fully commit to both your partner and your job.

Jane Austen's classic novels, like _Pride and Prejudice_ and _Mansfield Park_, are perfect fantasy material for modern singles longing for a time when finding a partner was so much easier. But many readers are amazed that Austen could so accurately depict loving relationships when they learn that the novelist herself never actually married.

It's not that she didn't try. Austen was born at the end of the eighteenth century, a time when marriage was the most important event in a young lady's life. Austen wrote wildly excited letters to her sister Cassandra about every social gathering where she could potentially flirt with suitors. And eventually, she did fall in love — but the times were against her. 

At her aunt's house, she met a young student named Tom Lefroy. Unfortunately, as was typical for the era, genuine love between two young people wasn't the main consideration when planning a marriage. In Austen's case, family ties and financial considerations ruled out the possibility of ever marrying Lefroy.

But every cloud has its silver lining. If Austen had gotten married, it's unlikely she would have blessed the world with her novels. Back then, a married woman was expected to devote herself to taking care of her many children and supporting her husband's ambitions. Austen would never have found the time or privacy necessary to write her novels.

Even so, people don't just feel love for others and for their vocations. They are also able to experience love for themselves, which we'll explore in the next blinks.

### 7. The first step in encountering the self is to preserve or rediscover the innocence of youth. 

Have you ever marveled at the simplicity, courage and honesty of children? They seem to have many of the attributes we admire in enlightened adults.

This is largely because they are still in touch with their _inner selves_, a point of contact that most adults are sorely lacking.

The inner self is the compass that guides you toward what you love. But as we make our way further and further into adulthood, most of us lose sight of our inner self.

Why?

Because education teaches us to follow _other people's_ _goals._

Take competitive sports as an example. All too often, coaches and parents don't care about what children want — mostly to have fun and explore their capabilities — and instead urge them to compete and win. 

By imposing their adult feelings of ambition and jealousy onto a promising young athlete, adults guide the child further and further away from his inner self. Soon enough, the child has completely lost sight of what he really wants.

But it's never too late to get back in touch with your inner self. All you need is to relearn how to ask your _own questions_.

This is the opposite of what happens in school, where young people are taught to think through the questions and answers of the great philosophers, artists and scientists of the past. While these questions may be interesting, they limit our growth because they are not our own.

For example, the author was asked as a student whether there was one god or several. The problem with this question, one which many thinkers have grappled with in the past, is that it only offers two answers. The author discovered that his answer responded to an entirely different question.

He composed his thoughts in a poem and realized that he didn't care whether there is one god or several gods. The question he needed to answer was: do I feel safe or abandoned? Discovering this question allowed him to connect deeply with one of the core feelings of his inner self.

> "_It doesn't interest me if there is one God or many gods. I want to know if you belong or feel abandoned. If you can know despair or see it in others._ "

### 8. As an adult you have to face the negative realities of the self, including suffering and anxiety. 

Do you sometimes get annoyed with the perpetual optimism preached by self-help books? Don't worry; despite what these books might say, there's nothing wrong with you.

It's natural to feel negative emotions; if you learn to embrace them, it can lead to a positive transformation. For instance, Buddhist philosophy doesn't understand pain or depression as negatives. Instead, if you learn how to adopt the right perspective, these feelings will lead to a deeper understanding of the self.

One high school teacher, Deirdre Blomfield Brown, experienced this transformation firsthand. She was depressed and felt caught in a pit of despair when she discovered an article on negativity written by Tibetan guru and teacher Chögyam Trungpa. 

Trungpa's writings helped her learn that it wasn't negativity that was ruining her life, but rather her negative reaction to it. This insight struck her so forcefully that she eventually became a Buddhist monk.

So does this mean we can live without anxiety and worry? Unfortunately not — these are universal, natural and even useful feelings.

Anxiety and worry drive us to create stability in our lives by succeeding in the world. And, indeed, success does lessen our anxieties. But we often believe that if we only had enough success and money, our anxiety would disappear forever. 

In fact, the truth is often the reverse: the more success we achieve, the more we stand to lose, and the more anxious we become.

Luckily there is an alternative path to overcoming anxiety: _meditation_. Meditation allows us to find the part inside us that is not anxious about survival. This place doesn't eliminate anxiety, but is instead a perspective from which we can observe our negative emotions and, in turn, disempower them. 

From the peaceful and eternal place meditation allows us to reach, we can observe all our emotions — including our negative ones — as nothing more than transient feelings. The more we meditate, the more we can identify with this inner spirit in our day-to-day lives. By doing so, we can set ourselves free from the bonds of anxiety.

### 9. You must first face yourself before you can truly welcome others into your life. 

Many of us have dreams of becoming more popular, stronger and smarter than we currently are. But if we're not careful, these dreams can do more harm than good.

If we ever want to accept the self, we first have to accept our current situation. This includes accepting painful truths, like our ultimate loneliness in the world. To do so, we have to allow ourselves to feel the depths of our isolation by spending time alone and doing absolutely nothing.

Yes, nothing.

Deirdre Blomfield Brown experienced this on her way to becoming Pema Chödron, her Buddhist nun name. One day she was struggling with the feeling that one of her teachers didn't like her. So, when night fell, she sat alone with this pain and meditated on it all night long. 

She simply listened to the pain, gradually relaxing into it, until she was suddenly struck by an insight: these negative feelings were only a transient part of her, like waves on the surface of the ocean. If she could accept her true self — the infinite depths beneath the waves that couldn't be affected by other people's opinions — she could liberate herself from the power of her anxieties.

People who have faced and accepted themselves like Pema Chödron radiate such positive energy that their mere presence makes other people feel good.

For example, one evening the author was greeting guests he had invited for dinner. His daughter hid behind his legs and shyly waved to each guest as they arrived. But when monk and educator Satish Kumar arrived, his joyful face dissolved her shyness and she spontaneously ran towards him with outstretched arms.

### 10. Final summary 

The key message in this book:

**Don't be afraid to throw yourself into pursuing your beloved, even if it means crossing the Atlantic ocean. At the same time, be sure not to neglect the work you love, whether it is running a company or writing an epic poem. When you encounter difficulties, use them to nourish your soul and get closer to your true self.**

Actionable advice:

**Take time to reflect on your life.**

Take some time for yourself and reflect on your relationships, work and personal happiness. Explore what you like and what you would like to change. If you have a partner or a good friend, talk about it with this person. Not only will it help you in your thinking process, it's also a good bonding exercise. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Happiness_** **by Richard Layard**

In _Happiness_, economist Richard Layard examines what it is that makes us happy and how anyone can achieve greater happiness. Basing his studies on insights from such diverse fields as psychology, philosophy and neuroscience, Layard presents compelling arguments that are great food for thought, encouraging readers to question their daily habits and practices.
---

### David Whyte

David Whyte is an English poet renowned for bridging the gap between poetry, the art of living and business. His other non-fiction publications include _Crossing The Unknown Sea_ and the best seller _The Heart Aroused_.

