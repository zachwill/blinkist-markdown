---
id: 55e462002852cc0009000063
slug: the-time-paradox-en
published_date: 2015-09-02T00:00:00.000+00:00
author: Philip Zimbardo & John Boyd
title: The Time Paradox
subtitle: The New Psychology of Time that will Change Your Life
main_color: E22F2D
text_color: E22F2D
---

# The Time Paradox

_The New Psychology of Time that will Change Your Life_

**Philip Zimbardo & John Boyd**

Time matters. Most of us are slaves to time, whether we're planning, multitasking, postponing or making to-do lists. But it's our attitude toward the concept of time that really affects who we are and how we live. _The Time Paradox_ (2009) shows you how you can start to think of time in a way that helps you to lead a better, happier and more fulfilled life.

---
### 1. What’s in it for me? Live a better life by discovering your relationship with time. 

Apple's Steve Jobs once said, "It's clear that the most precious resource we all have is time."

If this is so, then why do we understand so little about the nature of time? It's not just that time is an abstract topic, one that can make you feel a bit Star Trek, exploring wrinkles in the universe or space/time continuums. Time is also something we perceive in slightly different ways; that is, time influences each of us differently. 

What we do know is that time affects the way we think, the way we act and even how healthy we are.

Stanford psychologist Philip Zimbardo — the researcher responsible for the infamous Stanford prison experiment — in these blinks explains how you perceive time differently than other people and why this is so important. Crucially, you'll discover how you can make the time you have even better. 

In these blinks, you'll discover

  * why memories aren't like watching a video of your past;

  * how people who are too focused on the present can be danger to themselves; and

  * what governments can do to harness the power of time more effectively.

### 2. My time is not the same as your time; how we perceive time explains how we live and who we are. 

Time is an abstract concept; and as we can't touch, see or smell it, it's often extremely hard to understand. This is just one reason why our perception of time has changed — well, throughout time.

Our prehistoric ancestors, for instance, lived in the "now." Being focused on the present helped them escape immediate threats, and thus avoid death. They didn't even have the vocabulary to talk about the future, so everything was about the present.

Later, people began to recognize the change of seasons and solar and lunar cycles, which helped further the development of agriculture. Slowly, our ancestors started thinking and talking about the future.

Today, however, we're obsessed with time. Our lives are built around school semesters, financial quarters and 24-hour news. The word "time" is one of the most used in the English language.

Yet time is still a subjective concept. But how exactly does each person experience time differently? 

There are two main ways we perceive time. First, there's time as it is tracked on a clock. When we observe the passage of time as the ticking of a clock, for example, it feels like it exists outside of us and thus we treat it objectively.

Second, there's _psychological time_, which describes how we subjectively perceive the passage of time. Are you more concerned with the past or future? Do you see this positively or negatively?

Because of these different perceptions, we each have different _time perspective_ s. A time perspective is how you think of time, and how your attitude affects how you live your life.

There are six different time perspectives. _Past-positive_ and _past-negative_ perspectives relate to the positive and negative ways of perceiving past events.

_Present-hedonist_ means you live for the moment, experiencing as much pleasure as possible; _present-fatalist_ means you're anxious and sad, as you feel fate has already decided your hand.

Finally, _future-orientation_ involves planning ahead, while a _transcendental_ attitude means you believe that life extends beyond death.

### 3. Sad memories shape who you are today. Yet rose-colored glasses help make today more positive. 

To better understand time, let's start with the past. Why does the past matter? Because the memories we have essentially create the life we're living now.

Psychologist Alfred Adler used to ask his patients to recall their first memory, as he believed it was a window into their current lives. He then used this information to try to understand present troubles. For instance, anxiety may stem from a childhood fear of being abandoned by a parent.

Most people assert that their recollections of the past are accurate, and that memories are fixed. But memories cannot be compared to a video recording, as they actually change over time.

Rather than being recalled, memories are reconstructed in accordance with our current attitudes and beliefs.

This explains why beautiful memories can become painful, if for example you're breaking up with a partner. The sadness you feel now alters the way you view the good times you spent together.

Memories can also be repressed, only to surface later on. Childhood victims of sexual abuse sometimes can't recall events until they've reached adulthood. Sigmund Freud spent most of his career digging into his patients' repressed memories to interpret them in the light of the present.

What matters most about the past, though, is your attitude toward it.

People who view the past positively — a _past-positive_ time perspective — are more inclined to be happier, healthier and more successful than people who are _past-negative_.

Surprisingly, past-positive people are still happier even when their positive attitude is based on an _inaccurate_ recollection of events. Psychologists have found that looking at the past through "rose-tinted lenses" is the key to cultivating the gratitude necessary to appreciate life today.

How can you do this? Take some time every day to jot down things you were grateful for. While you can't change the past, you can change how you feel about it. A bit of re-framing may be enough to change the whole picture, and transform a past negative into past positive.

### 4. There’s more than one way to live in the now. Are you a hedonist or a fatalist? 

Do you live in the "now?" Just focusing on the present creates an interesting paradox. While it's great to be engrossed in the moment, overdoing this may jeopardize your long-term happiness. 

So what can we do about it?

As humans, we're born with an innate tendency to focus on the present. Biological needs demand this, from eating to sleeping to going to the bathroom. 

But we aren't usually raised to remain focused on the present. Society tells us to delay gratification, including the fulfillment of biological urges. Schools discipline us to study; even though it's no fun, we know that studying now will improve our future prospects. 

Thus we sacrifice some present-orientation perspective in favor of future-orientation. Let's look at the different kinds of present-orientation perspectives.

In the present, you might be a hedonist or a fatalist. _Hedonists_ enjoy all things that offer pleasure, and will do anything to avoid pain. Teens are classic hedonists, seeking novelty, excitement and short-term gains. They're life of the party, to be sure; but teens also have little impulse control.

In contrast, _fatalists_ believe their lives are controlled by forces outside of their influence. They might think, "Why should I worry about the future when it's already decided for me by God?"

So what are the pros and cons of living for the present moment?

Pros include not getting hysterical about your long to-do list. Yet if you can't review your past and learn from it, a lot can go wrong.

People primarily focused on the present are often not terribly concerned about the consequences of their actions, and this makes them more likely to indulge in drugs and risky behavior.

### 5. So what’s next? Being future-oriented can help you earn more and be more successful overall. 

The past isn't so different from the future, insofar as you can't directly experience either. Each are essentially constructed mental states around which we shape our hopes, fears and expectations. 

So how does having a future-orientation affect your life?

Being future-oriented can help you check impulses and positively influence present behaviors. 

A future-oriented student will prepare for exams rather than play video games, if she wants to become a successful doctor one day.

But we aren't born to naturally plan for the future; this perspective requires certain conditions. 

Stability is one such condition. When you think about the future, being able to rely on the present gives you the necessary foundation to plan ahead and create goals. A chaotic family life, or even living in a war zone, for example, makes thinking about the future almost impossible.

Even living in certain climates affects how we think about the future. In temperate zones, you need to plan holidays or outdoor work projects to avoid the winter freeze, for example. 

So what is the advantage of developing a future-orientation perspective?

Well, _futures_ — that is, people with future-orientation — actually get better grades. They are able to solve problems and get things done. Generally, they're also more likely to be more financially successful than those people who don't plan ahead.

The people who look further into the future, even beyond their own death, are those with a _transcendental_ future time perspective.

While conventional goals such as graduating from college or having children extend to the point of imagined death, a transcendentalist's perspective stretches into eternity. This perspective is naturally connected with spirituality and religion, particularly in Christianity and Islam.

But beware of developing an exaggerated fixation on the future, as you may turn into a member of the _time-press generation_.

As _USA Today_ reported back in 1989, many Americans suffer from a feeling that they don't have enough time to do everything they want, so squandering time becomes a huge source of emotional distress.

### 6. Our relationship with time impacts how we think and how we feel. 

Animals and plants seem to be able to keep track of time — consider bud break or the shedding of autumn leaves, bird migration patterns and hibernating bears. 

But we too can also track time with our "internal clock."

This clock is found in the base of the brain, an area called the suprachiasmatic nucleus. Its primary job is to regulate bodily functions, such as blood pressure and fertility cycles.

You know when your clock falls out of sync — think about the last time you suffered from jet lag, and how exhausted and irritable you felt. It takes a while for this clock to "reset."

When we think about time, however, as opposed to feeling it, this happens in the prefrontal cortex in the front lobe of the brain. You engage this part of your brain when you're identifying a goal or predicting a future consequence.

Organizing tasks that are tightly bound to time can be stressful, and this is actually what the lobotomy — a surgical procedure to remove the brain's prefrontal cortex — was supposed to cure.

It was thought that this procedure would relieve anxiety and feelings of being overwhelmed. Yet lobotomies also made patients completely lethargic and apathetic.

So what's the use in thinking about time anyway? Well, it helps us form emotions.

Fear, for instance, stems from imagining a threatening future, which forces you to become more present-oriented to overcome the threat. Our prehistoric ancestors knew they had to stay close to home and not wander off when hungry lions were on the prowl.

But being preoccupied with the future or the passage of time can also have negative consequences.

Depression sufferers often get stuck in a loop, thinking about things that depress them. This is called _depressive rumination_, an obsession with past negative experiences, which keeps a person depressed in the present.

### 7. Do you have impulse control, or are blinded by love? Your behavior is tied to how you perceive time. 

It's probably no surprise that your time perspective shifts as you get older. But what remains the same is that your perception of time will always influence your behavior.

How we experience time influences our development. If we can't perceive a future, we also can't work toward it. This is why anything that limits our sense of a continuing future shifts our focus from long-term goals and encourages us to focus instead on present satisfaction.

Researchers at Stanford University demonstrated this in a study with four-year-old children. They offered the kids either one marshmallow immediately, or two marshmallows an hour later.

Naturally, the children were interested in the sweet snack now! Years later, however, the same children were tested again. Interestingly, those who showed the ability to control their impulses for immediate gratification achieved on average much higher scores on standardized tests.

Impulsivity is a characteristic of present-hedonist children, and this trait can be carried into adulthood — and with it negative consequences, too.

Your time perspective also affects your love life. According to Sigmund Freud, falling in love is much like suffering from a mental illness: you lose sight of goals to become a present-oriented simpleton!

Remember getting to know your romantic partner? You didn't share a common past and hadn't yet planned a future together, yet you were quite happy enjoying the present. But as time marched on, the past and future slowly reasserted themselves.

This transition can be tough, as women are usually more future-oriented and men more present-focused. Having children, for example, can be a source of real conflict.

Your personal happiness is also affected by time, but the bad news is that nothing can make you happy permanently. And although we don't need much time to get used to the good, it can take much longer to shake off the bad.

Sure, a lottery winner might be ecstatic over a win, but the joy doesn't last. Research has shown that after hitting the jackpot, a winner usually reverts back to the state of happiness he felt beforehand.

### 8. Society and business would benefit if we thought more seriously about the impact of time. 

How can we apply what we've learned for the benefit of society as well as ourselves individually?

Our education system can provide a good framework for encouraging a future-orientation perspective. Governments, for example, could create institutions that teach a time perspective necessary for positive economic development.

In the nineteenth century, the Industrial Revolution necessitated an ever-growing workforce. Children, preparing for a lifetime of factory work, were taught in the so-called _Prussian way_, stressing obedience, punctuality and a tolerance for monotony. 

Not exactly a method conducive to forming future freethinkers! Instead, it was a way to tame present-hedonist kids and mold them into future-oriented adults.

Business could benefit if we paid more attention to how companies and people handle time. When an entrepreneur loses sight of the future he envisioned when he first founded his company, instead turning all his attention to the present, the consequences can be devastating.

Consider Enron, a high-flying company lauded by Wall Street that suddenly collapsed in 2001. What happened? Enron executives spurned long-term goals in favor of princely quarterly earnings, even cooking the books to do so!

Time also raises questions about our legal system. People with high levels of present hedonism and low future orientation are more prone to break the law, and future consequences do nothing to impede such impulses.

Why, then, do we punish people by locking them up, depriving them of a future that they have no concept of in the first place? The future threat of jail time doesn't deter crime.

And although we know what doesn't work, what does is a complex matter that is not easily fixed.

### 9. Work to find your ideal combination of time perspectives and get the most out of life. 

What if we could all develop an ideal time perspective? One that made us happier, more productive and more in control of our lives? 

There is an ideal for which you can aim. And it's actually a combination of many time perspectives.

First, it pays to be past-positive, as your past gives you roots and is at the heart of your understanding of yourself. Positive associations with family and tradition can give you a sense of continuity, which in turn is more likely to help you establish a rosy outlook on life.

In addition, you should steer clear of past negativity and present fatalism. Studies have shown that nothing comes out of this except low self-esteem, anxiety, depression and anger.

If you do get caught up in past negativity, you're probably stuck in a cycle of self-blame. So if you're spending too much time reviewing painful memories, see what hidden positives you can discover and create your own successes. 

The fact is that your past is over. Now is the time to get positive and start afresh.

It's also good to have a dose of present-hedonist, but in moderation. You can enjoy feeling alive and be passionate without becoming a drug addict or ending up broke!

Lastly, if you're competitive and realize you've been choosing personal success over friends, you may be over-obsessed about the future. Stop adding to your to-do list before you've tackled a few personal items.

Do keep an eye on the future, though, as it can give you hope and negates fears that keep you in your comfort zone. Being optimistic about the future means you'll grab opportunities that life offers.

Ultimately, by balancing your time perspectives, you'll allow yourself to get far more out of life.

### 10. Final summary 

The key message in this book:

**Time is precious. To make the most of it, you should develop a balanced time perspective and stop obsessing about the past or future or over-indulging in the present. Understanding how you perceive time and learning to shift between perspectives as needed will help you enjoy a much happier life.**

Actionable advice:

**See it to be it.**

Just like a basketball player visualizes shooting free throws with perfect form before the game, you should visualize your successes to increase your chances of living them for real. This will help enforce a healthy future-time perspective. Start with a goal. Now, visualize step-by-step the actions you need to take to achieve this goal. Baby steps are key. Don't overwhelm yourself by trying to do everything at once!

**Suggested further reading:** ** _Time Warped_** **by Claudia Hammond**

_Time_ _Warped_ is about that enduring mystery: our perception of time. Using research from the fields of neuroscience, biology and psychology, Claudia Hammond investigates the many reasons why, on one day, time appears to pass rapidly, while on another, it seems to grind to a halt. In addition, _Time_ _Warped_ suggests ways in which we can control our individual experience of time.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Philip Zimbardo & John Boyd

The head of the notorious Stanford prison experiment in 1971, Philip Zimbardo is a distinguished figure in modern psychology. Formerly the president of the American Psychological Association and a Stanford University professor for nearly four decades, Zimbardo has published over 50 books, including _The Lucifer Effect_ and _The Time Cure_.

In addition to his psychology doctorate from Stanford University where he studied with Zimbardo, John Boyd holds a degree in economics and works as a research manager at Google.

