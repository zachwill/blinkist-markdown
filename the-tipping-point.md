---
id: 52822ff13334640018300000
slug: the-tipping-point-en
published_date: 2012-10-16T13:11:30.805+00:00
author: Malcolm Gladwell
title: The Tipping Point
subtitle: How Little Things Can Make a Big Difference
main_color: E22F2D
text_color: E22F2D
---

# The Tipping Point

_How Little Things Can Make a Big Difference_

**Malcolm Gladwell**

_The Tipping Point_ discusses why certain ideas, products and behaviors spread like epidemics and what we can do to consciously trigger and have control over such epidemics.

---
### 1. Ideas spread like epidemics. 

The spread of ideas, products and behaviors can be compared to the spread of a viral infection: for years, only a few people are affected (or infected), but then, within a short period of time, it becomes an epidemic.

Take the suede shoes made by Hush Puppies, which remained mere shelf-warmers until the mid‑1990s when suddenly they became a must‑have. Within just one year, sales figures jumped from 30,000 to 430,000 pairs; the next year, around two million pairs of Hush Puppies were sold.

The company itself had nothing to do with the epidemic. It all started when a couple of hipsters in Manhattan started wearing the shoes, which "infected" others with the idea and set off a trend.

Social epidemics share several of the same recurring characteristics as viral infections.

For example, subtle external changes can often strongly affect the transmissibility of a social infection, just as viral infections can spread more easily in the wintertime when most people's immune systems are weaker.

In addition, both will eventually reach a _T_ _ipping Point_ : the point at which the critical mass has been reached and the spread can no longer be stopped.

**Ideas spread like epidemics.**

### 2. It is only an epidemic once the Tipping Point threshold has been crossed. 

The Tipping Point is the moment at which a trend turns into an epidemic and spreads like wildfire.

Take a flu strain, for example; it might spread through a population slowly at the start, but, day by day, it infects more and more people until finally it reaches that magical moment when the transmission rate soars dramatically and the epidemic spins out of control.

Represented in graphic terms, the growth would be a curve that is slightly inclined at first, and then juts up at almost a right angle. This dramatic turn is the Tipping Point.

The same kind of growth is also seen in the spread of technological innovations. When the electronics company Sharp made the first affordable fax machine in 1984, it sold around 80,000 of them in the first year and saw that number rise steadily every year, until 1987, when it reached the Tipping Point and sales skyrocketed.

At that particular Tipping Point, so many people owned a fax machine that anyone who didn't decided they'd better get one, too.

In other words, a fundamental change takes place at the Tipping Point, causing the "infection" to suddenly spread rampantly.

**It is only an epidemic once the Tipping Point threshold has been crossed.**

### 3. A certain few key people are often the cause of epidemics. 

The _80-20 Rule_ describes a sociological phenomenon found in many groups of people in which 20 percent of the people tend to influence 80 percent of the final outcome. For example, in most societies,

  * 20 percent of employees carry out 80 percent of the work,

  * 20 percent of criminals commit 80 percent of the crimes,

  * 20 percent of drivers cause 80 percent of all accidents,

  * 20 percent of beer drinkers drink 80 percent of the beer.

Virus epidemics, for example, are sparked by a few key people, but the ratio is even more extreme: only a small percentage of the people infected perform a majority of the "work" in promoting the spread.

Many of the early AIDS cases in the United States can, for example, be traced back to one flight attendant who, according to his own account, had sex with more than 2,500 people in North America and in doing so significantly contributed to the spread of the virus.

Likewise, in the case of social epidemics, it is typically a certain few who speed up the rate of transmission. Most of the time these are the people with special social connections or a remarkable personality.

**A certain few key people are often the cause of epidemics.**

### 4. Ideas spread particularly fast with Connectors, or people with a vast social network. 

Ideas are often spread by people with many social ties. The remarkable part is that these _Connectors_ usually are not only well-connected in one area, but in many different areas.

Connectors are the nodal points and idea propagators of social networks. They know, and like communicating with, lots of different people. Their most important asset is having many so-called _weak ties_ at their disposal. In other words, having a vast network of acquaintances from all different walks of life is more valuable to them than having close ties with friends.

Contacts that transcend social milieus are especially important when it comes to spreading epidemics: if a virus or idea only spread within a closed circle, it would not become an epidemic.

That's why Connectors with networks made up of people from different social groups are vital to the emergence of epidemics.

In a social experiment of the 1960s, scientists found out that every person in the world is connected to everyone else through just a few people. But the connections aren't necessarily distributed equally. Crossing the borders of milieus generally occurs by way of a small group of particularly well-connected individuals.

Anyone who wants to spread an idea by word of mouth would therefore do good to focus on these Connectors because they are the ones who can set off social epidemics.

**Ideas spread particularly fast with Connectors, or people with a vast social network.**

### 5. Some people are born with the gift of persuasion and a knack for selling ideas. 

There are some people who are born _Salesmen_.

Usually they are people who think positively and have a lot of energy and enthusiasm, qualities that help them persuade others of new ideas.

Studies have shown that outstanding Salesmen differ from others most noticeably in their non-verbal communication. They can feel out the right rhythm of a conversation and create a palpable harmony, thereby establishing a sense of trust and intimacy in very little time.

In short, Salesmen manage to synchronize themselves with others. Through their nonverbal communication, they get others to perform a kind of dance in which the Salesmen set the pace.

Born Salesmen also have a special way of expressing their feelings: emotions are contagious, and Salesmen show them so clearly that others empathize with them immediately and, as a result, change their own behavior.

Salesmen are in a position to influence people on the inside from the outside, which makes them ideal people to spread ideas.

**Some people are born with the gift of persuasion and a knack for selling ideas.**

### 6. In every network there are Mavens who amass information and pass it on to others. 

A final type of person that plays a key role in spreading social epidemics is the _Maven_. Mavens have two distinguishing characteristics:

  * They know a lot about many different things and assimilate information constantly — often about new trends or specific products and how much they cost, etc.

  * They have social skills and constantly pass on their knowledge to others.

Mavens do not have exceptionally big networks, but they have a major influence over those in their own network. Others trust the Maven because everyone knows the Maven has insider knowledge.

Mavens are highly communicative and socially motivated to be helpful and pass on information to others. If they are confident about a product or service, they recommend it to their friends and acquaintances — and their friends and acquaintances follow those recommendations.

Therein lies the power of the Maven.

**In every network there are Mavens who amass information and pass it on to others.**

### 7. An idea has to stick before it can spread. 

If you want an idea to spread, you have to make sure it sticks first.

An idea needs something special, something catchy — something that makes it stand out from the rest of information that inundates us every day.

In order to stick, a message has to be appealing. Usually, tweaking something — even a small detail — in how the message is presented is what makes all the difference.

For example, in 1954, the cigarette brand Winston advertised for its new filter cigarettes with the slogan, "Winston tastes good like a cigarette should." They purposefully incorporated a grammatical error (using "like" instead of "as") which caused a small sensation. The message stuck and ended up spreading like an epidemic. In just a few years, Winston became the most popular cigarette brand in the US.

Another example is from the world of television. Sesame Street's huge success was largely based on the fact that the makers of the show introduced an innovation. When the show first aired, they always stuck to the convention of keeping scenes with "fictional" characters (the Muppets) separate from the scenes with real actors filmed on the street.

But as soon as the makers found out that children were bored with this separation, they decided to bring the Muppets into the real scenes. This small but crucial change is what made Sesame Street so appealing to its audience. And the rest is history.

**An idea has to stick before it can spread.**

### 8. External circumstances have a much greater influence on our behavior than we think. 

Our behavior is strongly dependent on external circumstances. The tiniest changes can have a huge impact on the way we behave in any given situation.

For example, one study showed how being pressed for time can influence our willingness to help. Students were sent to a lecture hall to give a talk; half of them were told there was no rush to get there, and the other half were told not to be late. On their way, they all encountered a man who had collapsed. In the first group, 63 percent of the students stopped to help the man; in the second group, it was a mere 10 percent.

In another study, the _Stanford Prison Experiment_, 24 healthy males were selected to spend two weeks in a mock prison where each of them was assigned to play the role of either a guard or a prisoner.

The experiment quickly spiraled out of control. The "guards" exploited their power and grew more and more cruel and sadistic, leading many of the "prisoners" to suffer from emotional breakdowns. Because of this, the experiment had to be called off completely after only six days.

The change in their circumstances — despite being a mock prison with artificial roles — turned the participants into completely different people and had a huge impact on their behavior.

**External circumstances have a much greater influence on our behavior than we think.**

### 9. Even the smallest changes in a context can determine whether an epidemic takes off. 

The emergence of epidemics depends largely on external circumstances and can frequently be traced back to small changes.

This was apparent to the authorities in New York City when, in the mid‑1990s, the city's crime spiraled out of control, and they blamed a number of seemingly harmless details. They thought that things like graffitied subway cars, or subway-fare evaders going unpunished, sent out signals to people that nobody was taking care of the decaying situation — and that anyone and everyone could do whatever they wanted.

In order to get a handle on this crime epidemic, authorities began focusing on these more minor details. Graffiti was removed, seemingly overnight, and fare-evasion became a punishable crime. By showing _zero tolerance_ for what seemed like trivialities, it became clear to the public that reckless behavior was no longer acceptable. The crime rate dropped rapidly in the following years; the epidemic was reversed thanks to these small interventions.

Another subtle factor that plays a role in the emergence of social epidemics is the size of a group. The _rule of 150_ states that only in groups of no more than 150 people can a dynamic develop that can later extend beyond the group.

In other words, if you want groups, for example, clubs, communities, companies or schools, to be incubators for contagious messages, make sure to keep them small.

**Even the smallest changes in a context can determine whether an epidemic takes off.**

### 10. Final summary 

The key message of this book is:

**There are several crucial factors that play a role in triggering epidemics. They are easy to recognize and can be consciously used to spread ideas, products or behaviors.**

The questions this book answered:

**Why do ideas spread like epidemics and what part does the Tipping Point play?**

  * Ideas spread like epidemics.

  * It is only an epidemic once the Tipping Point threshold has been crossed.

**What sorts of people have the most influence on the spread of ideas?**

  * A certain few key people are often the cause of epidemics.

  * Ideas spread particularly fast with "Connectors," or people with a vast social network.

  * Some people are born with the gift of persuasion and a knack for selling ideas.

  * In every network there are "Mavens" who amass information and pass it on to others.

**Which other factors play a part in the spread of ideas?**

  * An idea has to stick before it can spread.

  * External circumstances have a much greater influence on our behavior than we think.

  * Even the smallest changes in a context can determine whether an epidemic takes off.
---

### Malcolm Gladwell

Malcolm Gladwell is a journalist and the author of five best-selling books, including _Blink: The Power of Thinking Without Thinking_ and _Outliers: The Story of Success._

