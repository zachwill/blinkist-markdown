---
id: 55189bb43961320007570000
slug: the-total-money-makeover-en
published_date: 2015-03-31T00:00:00.000+00:00
author: Dave Ramsey
title: The Total Money Makeover
subtitle: A Proven Plan for Finance Fitness
main_color: 9A8A57
text_color: 665B3A
---

# The Total Money Makeover

_A Proven Plan for Finance Fitness_

**Dave Ramsey**

_The Total Money Makeover_ (2013) is a step-by-step guide to turning your financial situation around, no matter how nasty it seems. By following these seven simple steps, you can put financial security back into your life and begin planning for a comfortable, contented retirement.

---
### 1. What’s in it for me? Learn how to revolutionize your financial health. 

Seeking advice on how to be financially secure can be stressful; there is a glut of financial gurus telling you how you can follow a few simple steps and become a millionaire.

Most of this advice is hogwash, but some of it isn't. These blinks present a simple and straightforward plan based on advice from distinguished financial expert Dave Ramsey. If you follow his plan, you'll stand a greater chance of getting your finances in order and becoming financially secure — that is, reaching _financial fitness_ — something we all dream of.

In these blinks, you'll discover

  * why you aren't as comfortable as you think you are;

  * why it might not be worth it to attend college; and

  * why you shouldn't eat an elephant in just one sitting.

### 2. Your financial security is an illusion, and it’s time for you to take action. 

Would you say you're financially comfortable? Many of us feel secure enough in our finances, even though we all could use a few extra dollars. You probably have a job, and perhaps a car and house, and financial difficulties therefore seem far, far away.

But no matter how secure you may feel at this moment, financial insecurity may be closer than you think.

Imagine you suddenly lose your job: What would you do? Would you be able to pay your bills? Unlikely. Financial security is often more illusory than we think.

Take the author's client Sarah, for example. After her marriage she and her husband calculated their joint annual income to be over $75,000, and they had only a few debt obligations between the two of them. Feeling comfortable with their earnings and their debt, they proceeded to take out a large mortgage on their house.

But that's okay, right? They could afford it. Or could they?

One day Sarah discovered that she was losing her $45,000-per-year job. Suddenly, they faced foreclosure on the house. 

As in Sarah's case, sudden financial setbacks can put us quickly in dire straits. What can we do? One way to avoid unpleasant surprises is by taking action before it's too late. Change things _now_.

It's easy not to feel this sense of urgency, to think instead that you can simply carry on until things turn sour, and _then_ change. But this is totally irresponsible. Financial difficulties sometimes creep up on you, and before you know it, you're in trouble.

It's like the proverbial frog in the pot of water: As the water slowly heats up, the frog doesn't realize that it's being boiled alive and will let itself die.

This is what is happening to you _right now_. Your financial security could be slowly collapsing around you, and you have no idea.

It's time to make a change.

### 3. Debt is treated like a fact of life, but we need to recognize its constraints and dangers. 

In our modern world, we are constantly encouraged to go out and buy stuff — a house, a car, a huge TV, you name it. And how exactly are we supposed to pay for all these things that we simply _must_ have? With credit, of course.

Debt today is so ingrained in our lifestyle that it's hard to imagine living without it. You probably have a decent amount of debt yourself, whether in the form of student loans, a mortgage, credit card debt or car payments.

In fact, debt is so embedded in our society that one of the author's clients felt okay with having $72,000 in debt on a rental property and $35,000 on credit cards and student loans.

Though debt seems to be ever-present, it is absolutely not the path to financial happiness, and instead leads straight to financial difficulty.

Take one of the most common forms of debt: the credit card. Credit cards, which feel essentially like free money, give users incredible spending power. In the long run, however, they can hurt our financial strength.

In fact, according to the American Bankruptcy Institute, 69 percent of people who file for bankruptcy say it was caused by their credit card debt.

Interestingly, while many people use debt to create the appearance of wealth, genuinely wealthy people tend to avoid debt entirely.

In fact, 75 percent of people on the _Forbes_ 400 list said the best way to build wealth is to become and stay debt-free. And some of the most successful companies, from Walgreen's and Cisco to Harley-Davidson, are run completely debt-free.

If these companies and individuals are able to find success without the burden of debt, then can't we do the same?

> Fact: The typical millionaire lives in a middle-class home, drives a car that's paid off and is at least two years old and buys jeans at Wal-Mart.

### 4. Step one on the path to financial fitness involves creating an emergency starter fund. 

So far, we've established one way _not_ to establish financial security: trying to spend our way to success using credit. So what _can_ we do?

You'll need to start by creating a step-by-step plan that charts the route to _financial fitness_. Although you know you need to change the way you approach your finances, you must also realize that you can't change everything at once. Instead, you should do yourself the favor of proceeding slowly, one small step at a time.

Just think: if you had to eat an elephant, you wouldn't even think to try and do it all at once. You would start maybe with a foot per day, moving eventually to the trunk, then the body, eating bit by bit.

You want to take the same approach with your finances. If you try to attack different areas all at once — for example, your mortgage, credit card and 401k — then you'll dilute your efforts and, ultimately, fail. So, go slow and take small bites.

But where do you start? The first step on your Total Money Makeover is to grow a _starter emergency fund_, a $1,000 chunk set aside in case of a rainy day.

In fact, _Money Magazine_ estimates that 78 percent of us will experience a major negative life event, such as an unexpected pregnancy or car problem, in any given ten-year period. You'll want to be prepared for when this happens.

And while $1,000 won't cover that much, it's nevertheless a useful start and will reduce the likelihood of having to go into debt.

But remember: this fund is _only_ for emergencies, and if you have to take anything from it, you should replace it as soon as possible.

### 5. Steps two and three: Start paying off your debts one at a time and, when you’re ready, grow your emergency starter fund. 

Once you have your emergency starter fund set up, you're on your way to turning your finances around. Now it's time to get your debts sorted out.

Step two of the Total Money Makeover is to create a _debt snowball_.

Everyone knows that if you start rolling a small snowball along the ground, it will, in no time at all, turn into a veritable snow boulder. The same thing happens when you pay off your debts. Here's how:

Start by listing all your debts in order of size, from your tiny phone bill to your massive mortgage. Then it's time to get serious about paying them off, starting with the smallest. As the small debts start to disappear, you'll get inspired to tackle those bigger, trickier debts.

After you've started rolling your debt snowball, it's time to turn your attention back to your emergency fund.

The goal of step three is to grow your emergency fund so that it could cover your expenses for a three to six month period.

Of course, everyone has different spending needs, so this number isn't fixed. However, it often ranges from $5,000 to $25,000. To make things more concrete, if your family earns $3,000 per month, aim to save $10,000 or even more. 

Now let's say you succeeded and have a bigger emergency fund. You'll find that this gives you the confidence to continue on the path toward financial freedom. If, as you pay off your debts, you have to use some — or even all — of your savings and retirement funds, you'll have an emergency fund that'll cover you for half a year. That allows you to get on with your life securely and confidently.

### 6. Step four: Invest 15 percent of your income in mutual funds to provide for yourself in retirement. 

Everyone has big worries about the financial situation they'll face after retirement. We ask ourselves: Will we have enough money to live comfortably in our golden years?

In order to overcome such fears, we turn to step four of the Total Money Makeover:

A dignified and secure retirement will require you to invest 15 percent of your income.

Although this might seem like a lot, there are a number of reasons why it's worth setting aside that kind of money.

For starters, old age simply wouldn't be any fun if you had to rely on others to maintain a comfortable living. This is especially true if you are hoping to live off government pension plans. By the time you reach retirement age, the chances of our inept government providing for a dignified life are negligible.

It can be tempting to put aside less for retirement so that you can focus on things like your children's college fund or quickly paying down the mortgage. But your kids' degrees won't feed you after you retire, and too many senior citizens live in a paid-for house with no disposable income.

Once you have committed to putting away 15 percent of your income, where exactly should you invest it? For the best returns, the author recommends mutual funds.

Throughout history, the stock market averages just below 12 percent in returns. Mutual funds take advantage of this trend, and thus make for an excellent choice for long-term investment. One tip is to select funds that have a solid track record of winning for more than five years, ideally for over ten. Make sure to diversify your investments across various funds to ensure profitability.

Here's a another good rule to follow: allocate 25 percent to growth and income (or blue chip) funds, 25 percent to growth (or equity) funds, 25 percent to international funds and the final 25 percent to aggressive funds, i.e., ones that are riskier but can give higher returns.

### 7. Step five: if you want your kid to go to college, plan for them to get there debt-free. 

Nearly every parent dreams of sending their child to college, and many parents are prepared to let themselves and their children go into debt to fund this dream.

But as we've already discussed, debt is to be avoided at all costs. Funding college with debt should not be considered a tenable option.

A college loan will hamper your child for a long, long time. The current generation of students has earned the nickname "generation debt" with good reason — they graduate from college with an average of $25,000 to $27,000 in debt, and it's not going away anytime soon.

So how _should_ you pay for college?

One way, of course, is to win a scholarship, or to simply save up enough in cash to foot the bill.

Yet, there is another way: using an Education Savings Account (ESA) and funding it in a growth-stock mutual fund.

If you were to invest $2,000 a year in a prepaid tuition plan, from the birth of your child until their eighteenth birthday, you would end up with $72,000 worth of tuition. However, if you instead used an ESA funded by mutual funds (which average 12 percent), you'd have $126,000 to spend on education and living expenses.

And as long as you use this account to pay education expenses, the money is tax-free.

But, even with this option, you must ask yourself whether a college degree is the right thing to invest in for your child.

In his book _Emotional Intelligence_, about successful people, Daniel Goleman states that only 15 percent of success can be attributed to training and education. The remaining 85 percent is attributed to attitude, perseverance, diligence and vision.

These latter qualities will take you much further in life than some piece of paper with the words "degree" scrawled on it.

So does your kid need to go to college? If getting there means going into debt, then certainly not.

### 8. Step six is becoming debt-free by paying off the biggest loan of them all: your mortgage. 

So how long have you been paying off your mortgage? Often, they take decades to finally pay off.

Step six of the Total Money Makeover is about paying it off as soon as possible. For most people, this is the final hurdle on their path to financial fitness, and paying it off will leave them absolutely debt-free.

However, there are many pitfalls that can prevent you from finishing off your mortgage. It's your job to avoid them.

For example, you'll hear it suggested that you should borrow money against your home, taking advantage of low interest rates and investing your money in the stock market.

But this is terrible advice. Imagine, hypothetically, that you borrow $100,000 against your home at 8-percent interest and invest it in stocks with 12-percent return. In this scenario, you stand to make $12,000 in profit. After paying the interest on your mortgage, in this case $8,000, you'd have a healthy $4,000 left over. Not so bad.

But that doesn't take into consideration all the taxes and fees that go hand in hand with playing the stock market. In the end, you're left with around $1,000. Hardly worth it for such a huge risk.

Another misconception is that it's possible to take out a 30-year mortgage with the promise to pay it back in 15 years. But you will inevitably run into expenses that take you off course — high heating bills, dog vaccinations, sick children and so on.

And if they aren't compelled by law to do so, virtually no one ever makes the extra payments necessary to pay off a loan that quickly.

However, it's often better to simply t _ake a shorter mortgage out anyway_. In comparison to a 30-year mortgage at 7 percent, a 15-year mortgage will earn you a savings of $150,000 over the course of the mortgage. Think about what you could do with that kind of money.

### 9. Step seven: follow your plan and spend (and give away) your money if you have it. 

At this point, you're right on the brink of financial fitness. You're on the last leg of your journey, with just one final step to go.

Once you are debt-free and have begun to save for your future, it's time to start building up your wealth.

Surround yourself with experts, people like tax advisors, CPAs, estate-planning attorneys, etc., who can offer you sound advice on what you need to do with your money. 

And no matter what, stick to your plan. As you grow older, you'll find yourself more inclined to react to small changes in the market, especially if you fear a downturn is looming. But don't fret! These small blips are nothing compared to the market's trend of long-term growth.

Finally, understand that financial fitness doesn't mean living like a Scrooge. Have fun with your money _when you can_.

Fun is a crucial element of the Total Money Makeover. Should anyone wear a $30,000 watch? Drive a $50,000 car? Or live in a $700,000 home? Absolutely. But only if they can _actually_ afford them.

You have to learn to spend your money only on what you can afford, and forget the rest.

When the right opportunity presents itself, you should also be prepared to give your money away. Giving money away is just as fun as spending it, and possibly even more rewarding. It feels good being generous — but you need to have before you can give.

At last, you've completed your journey to financial freedom. Now it's time to enjoy it, living in comfort, happiness and security.

### 10. Final summary 

The key message in this book:

**For most people, financial security is little more than a comforting illusion. However, you can turn your financial situation around by following seven "baby steps" that put you on the path to a debt-free and financially successful life.**

Actionable advice:

**Learn to live your own life.**

You probably have some friends who appear to be pretty well off. Yet, a lot of people appear to be doing better than they actually are. Debt allows for this illusion, but it will eventually crumble beneath them. So don't measure yourself against your peers.

**Suggested** **further** **reading :** ** _MONEY:_** **_Master the Game_** **by Tony Robbins**

Do you want to master money, and make it work for you? In this book you'll discover the steps you need to take to achieve real _financial freedom._ Whether you're just starting your career or moving toward retirement, _MONEY_ offers sound advice from seasoned professionals on saving and investing so you can live the life you want.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dave Ramsey

Dave Ramsey is an American author, radio host, television personality and motivational speaker who focuses on finance and debt freedom. He is best known for his radio show, _The Dave Ramsey Show_, on which he offers financial advice to callers from all across the United States.

