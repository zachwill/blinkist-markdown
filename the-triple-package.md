---
id: 56afb098377c1600070000c4
slug: the-triple-package-en
published_date: 2016-02-05T00:00:00.000+00:00
author: Amy Chua and Jed Rubenfeld
title: The Triple Package
subtitle: How Three Unlikely Traits Explain the Rise and Fall of Cultural Groups in America
main_color: 1F6399
text_color: 1F6399
---

# The Triple Package

_How Three Unlikely Traits Explain the Rise and Fall of Cultural Groups in America_

**Amy Chua and Jed Rubenfeld**

_The Triple Package_ (2014) is a sweeping account of the rise and fall of different cultural groups in America. These blinks explain the traits essential to success, how they are at odds with American values and the unintended side effects they often have.

---
### 1. What’s in it for me? Discover why some cultural and ethnic groups do better than others. 

Although Jews make up only a fraction of the world's population they account for around 20 percent of all Nobel Prize laureates. Why is that? And how did the Mormons go from being viewed with suspicion by many to becoming an extremely wealthy and influential group in America?

As these blinks will show, the factors behind the success of many prominent faith-based and ethnic groups in America are in fact neither religious nor genetic. If you look at these groups a bit more closely, three distinct forces — _the Triple Package_ — stand out. Together they create the magic formula for success. But what are they? 

In these blinks, you'll learn

  * the connection between marshmallows and achievement;

  * why you should "eat bitterness;" and

  * the real reason the United States has lost its position in the world.

### 2. Successful groups in America often share common characteristics. 

It's clear that certain groups in America excel above others. And while your personal success can depend heavily on that of your parents, the American Dream of rising from rags to riches is still a reality for some, and for immigrants in particular. 

Of course, this depends on your definition of "success," but if by success you mean having an important role in society and plenty of money, then it's possible to compare the relative accomplishments of different groups. For instance, Indian Americans have the highest median household income of all census-tracked ethnic groups, clocking in at $90,500 per year, but the Taiwanese come a close second. 

Another group that's garnered a high standard of achievements are Jewish Americans, who are continually awarded the Nobel Prize, Pulitzer Prize and Tony Awards, and earn huge amounts from hedge funds disproportionate to their population size. 

However, the overall success of a group can also decline. For instance, in the early 1900s, Protestants were on top of the American economy, but today they hold a less than average amount of the country's wealth. 

So, how do some groups rise and others fall?

It's got a lot to do with the _Triple Package_, a combination of three forces that all successful groups in America share, each of which contradicts a fundamental aspect of American thinking. 

They are:

A _superiority complex_, or the deeply held belief that your group is somehow better than others. This could be based on religious supremacy (Mormons), the glory of the group's history (the Chinese), or being told that you are the "chosen people" (Jews). 

The second aspect is _insecurity_ or the feeling that you aren't good enough. For example, immigrants can be particularly insecure as they deal with many economic and social anxieties. 

And the third element is _impulse control_, or the strength to resist temptation. This is especially key when facing difficult conditions.

### 3. The key to the package’s potency is the tension between its parts. 

So, the Triple Package both produces success and is contrary to American culture. That means that every successful group in the country is, in one sense or another, an outsider. 

For instance, the superiority complex is antithetical to liberal teaching, which says that no individual or group is better than any other. In fact, thinking otherwise was used to justify such atrocities as racism, colonialism and genocide. 

The fact that insecurity is a trait shared by successful groups contradicts psychology, which says that feeling insecure is caused by low self-esteem, a deterrent from a successful life. But nonetheless America's most successful populations don't just suffer from insecurity, they promote it. 

And finally, impulse control is contrary to contemporary youth culture. That's because it's viewed as outmoded and restrictive, while impulsive living in the present is considered a goal worth fighting for. 

But it's not the individual parts of the package that give it its strength. It's the tension between them. For instance, it's the combination of superiority and insecurity that produces the will to succeed. That's because when they're paired they grow into a need to prove oneself — perfect fuel for a successful life. 

Just take the many Chinese, Korean and South Asian immigrants who perfectly exemplify this fact by expecting their children to earn the highest grades, a symbol of honor for the whole family. It's this "can't you score higher than 99?" mentality that supports the superiority complex because it's implied that you should be able to do better than everyone else. But it's also fueled by the insecurity that the student hasn't done well enough.

So, drive is key but what's more important is the strength to persevere. This ability is in part the result of psychological armor built by the superiority complex, but it's more due to the interaction of superior feelings with impulse control. It's produced by a belief that superiority comes from controlling your impulses. That's because this logic can produce a self-fulfilling cycle that leads to increasing endurance.

> _"Superiority plus insecurity is a formula for drive. Superiority plus impulse control is a formula for hardship endurance."_

### 4. The Triple Package produces more than success and its absence is not the cause of poverty. 

Although the Triple Package leads to success, it can also produce a variety of other consequences.

First, the Triple Package groups face _disappearance_ as they assimilate into American culture and intermarry — two consequences of success. For instance, the hugely successful Huguenots, French Protestants who faced persecution by the Catholic Church in the seventeenth century, have entirely disappeared. 

The second consequence of success is _decline_, as success often chips away at insecurity. In the case of the American Jewish community this could be a real threat as the group becomes more secure. In fact, there's already evidence to suggest declining academic performance among young American Jews. 

The third effect is _reinvention_. That's because groups adapt to some American ideas without fully Americanizing. This results in members feeling like outsiders both in their cultural group and in broader US society. However, this effect can mean a huge boost in creativity that frees members from conventional limitations while retaining the Triple Package. 

So, some of the package's side effects can have downsides, but there's at least one way to avoid these — by rejecting a common definition of success.

The Amish avoid the disappearance, decline and reinvention that affects Triple Package groups because they have a belief system that keeps them isolated from the mainstream and disinterested in a very American type of success through conspicuous consumption. This sets them apart from other high-achieving groups who are also outsiders. 

Now, you might be thinking that if the Triple Package produces success, then chronically low-income groups do badly because they don't share the package. However, that's simply not the case. The real reasons for the impoverishment of certain American minorities are systematic exploitation, discrimination, denial of opportunity and macroeconomic factors that are entirely separate from their culture. 

For instance, although the Appalachian region was geographically blessed with superb reserves of raw materials like salt, timber and coal, these assets were mercilessly exploited — as were local workers — by corrupt but powerful actors, and subject to further economic fluctuations and twists of history. The result? Rampant poverty and an inequitable distribution of the wealth generated by those resources.

### 5. Cultural groups like the Mormons are particularly successful. 

So, now you've seen how different groups defined by religion, ethnicity and country of origin can be more successful than others. But what really distinguishes these groups from others is the fact that they're _cultural groups_, which means they share specific values, beliefs and practices passed down through generations. 

These include Jews, Cuban exiles, Iranians, Armenians, Kurds, Turks, the Lebanese, Asians — particularly the Chinese and Indians — West Indians and African groups like Nigerians.

But one group that's particularly successful with regard to income, academic achievement, corporate leadership and professional development are Mormons. In fact, over three decades, the public image of Mormons transformed from a polygamous, isolationist, racially discriminatory tribe rarely seen on Wall Street to one of corporate, financial and political success. 

That's because while they comprise just 1.7 percent of the country's population — compared to Protestants at 51 percent — Mormons now occupy powerful positions in the upper echelons of American business and government. Just look at 2012 presidential nominee, Mitt Romney, and the chairman of Marriott International, J.W. Marriott, both of whom are Mormons. 

However, the real symbol of the Mormon ability to concentrate wealth is their church, known as the _Church of Latter-day Saints_ or the _LDS Church._

While the church's finances are kept secret even from members, a 1997 estimate put its assets in the ballpark of $25-30 billion. Compare that to the Church of England's $6.9 billion valuation and the American Catholic Church's $7.5 billion in 2008. Furthermore, its current annual revenue is estimated to be $5-6 billion!

But being part of a successful culture doesn't guarantee you'll be a winner. Some subdivisions of Triple Package groups undermine the plus points of being a member of that group. For example, by 2003 "prophet" Rulon Jeffs, an insular fundamentalist Mormon leader, had married 75 women, had at least 65 children _and_ been excommunicated by the church. His sect isn't doing fantastically well, despite still sharing many cultural similarities with mainstream Mormonism.

### 6. Most successful groups have a superiority complex that belies America’s mantra of equality. 

However distasteful it may be, successful people often believe that they're better than others in one way or another and the same goes for groups, some of which have very pronounced superiority complexes. 

So, while some groups truly believe in equality, others claim they're unique. The most famous example is probably that of the Jews, a group that lays a 3,000-year-old claim to being God's "chosen people" — although many Jewish thinkers have been deeply uncomfortable with this notion.

Cuban exiles have similarly identified as an exceptional people who were forcibly removed from their promised land. As a result, they consider themselves special and privileged. In his memoir, the historian Carlos Eire writes that students were consistently taught that, upon arriving in Cuba, Columbus said, "this is the most beautiful land ever seen by human eyes."

In fact, many Cubans maintain that their island was home to the Garden of Eden as well as Adam and Eve. 

However, while such bracing self-belief runs contrary to American egalitarianism, and all ethnic groups achieve a superiority complex. Sometimes it's because they reject the notion of superiority outright. In the case of black Americans, it's because since slavery began in America, the country has routinely denied its black population any claims to a narrative of superiority. But this doesn't necessarily apply to recent black immigrants. 

In fact, some African immigrants, like those from Nigeria, are shielded by what sociologists have termed an "ethnic armor," a defense created by their homegrown superiority complex which helps them deal with the discrimination and exclusion that black people face in America. For instance, the Nigerian Igbo and Yoruba tribes have experienced disproportionate success in West Africa, a fact that has reinforced their superiority complexes. Igbos are at times even referred to as the Jews of West Africa.

### 7. Scorn, fear and family are powerful sources of insecurity – and success. 

While insecurity is a trait shared by all humans, it comes in a variety of forms. So, what kind of insecurity do the most successful groups experience?

One caused by public scorn, fear and family. Each of America's most successful groups has been scorned in some way throughout history. As Voltaire said, everything but contempt can be borne. Scorn motivates people to climb the social ladder. 

For instance, Iranians are constantly portrayed in a negative fashion by the US media. During the post-9/11 wave of Islamophobia they have been irrationally dubbed terrorists, suffered hate speech and generally been humiliated. 

The result has been a desire to distinguish themselves. That's why many Iranians for whom Southern California is home live in shacks so they can drive a Mercedes. 

But scorn isn't the only thing that augments insecurity. The fear of being unable to survive is also a major factor. This fear can be a mix of material worries and the anxiety caused by being an outsider, whether it's real or imaginary, rational or irrational. For example, the talk-radio host Dennis Prager has made the dramatic assertion that every Jew has the hidden fear of being persecuted and killed because of their religion. 

And what about the third fear, family?

Well, the demanding Jewish parent is just one deep-seated stereotype. It's not unusual for children to feel that success is imperative to not disappointing their families. In Chinese, Japanese and Korean families in particular, a B grade in school is seen as a disgrace to the entire family. This insecurity can be exacerbated by a feeling that the only way for the child to be freed from the conviction that they haven't done enough is to repay their parents for the sacrifices they've made.

### 8. Impulse control is crucial and can be strengthened through culture. 

Have you ever noticed that if you ask a successful person how they have accomplished so much they'll bring up all the ways they failed instead? Well, it's no coincidence that they have this tendency and it's got everything to do with the importance of perseverance. 

Ever since the Stanford psychologist Walter Mischel conducted his famous "marshmallow test" in the 1960s, a growing body of empirical proof has emerged showing that impulse control is an essential factor in success.

Here's how it works.

Mischel placed a marshmallow in front of children ranging from three to five years old. He told them that if they abstained from eating the treat for 15 minutes, he would give them a second treat. Unsurprisingly, only a small percentage waited the time necessary to double their reward. 

He kept an eye on the 650 participants as they grew up, and found that the ones who had refrained from indulging were doing better in school, had fewer social problems and better exam results than the ones who hadn't been able to resist. In fact, impulse control even proved to be a better indicator of college success than SAT scores or IQ!

Luckily, impulse control can be developed through culture, but not without problems — in a vacuum, it may even produce seriously negative consequences. For instance, the Amish are great exemplars of impulse control because they isolate themselves and deny themselves many modern comforts, but they aren't particularly academically or economically successful. 

Impulse control only leads to success when it's combined with the other two elements of the Triple Package, like feeling superior because of your ability to control your impulses and striving to prove yourself because you sense that your group is looked down on by the mainstream. Many of America's most successful cultural groups do just that: they take pride in their self-restraint and pass this value on to their children, expecting them to defy mainstream expectations.

For instance, the Confucian idea of _Chi ku_, or "eating bitterness," is essential to Chinese child-rearing. It means enduring all hardships without complaint and learning from them. As a result, Chinese parents raise their children to believe that they'll need to work harder if they want to be taken seriously in America.

> _"How people respond to failure is a critical dividing line between those who make it and those who don't."_

### 9. The Triple Package has downsides: psychological problems and harm to others. 

Now you know how the Triple Package can propel people to success, but not every aspect of it is a blessing. The success that the package affords you comes at a hefty price, and depending on how much you value things like standing and money, it might not be one you're willing to pay.

For instance, impulse control can prevent you from enjoying life. But it can also help you break free from the constraints of the world by giving you the ability to decide whether or not you want to devote yourself to conventional success. 

However, there are other issues with the Triple Package and the biggest one is that parenting in this way can cause psychological problems. That's because Triple Package parenting, with its unnatural focus on success, is contrary to the American parental norm that sees youth and childhood as a fun, free time. Not just that, but it can make children feel that their parents are using them.

Studies have found that Asian-American students have the lowest self-esteem of all racial groups, even when they are consistently academically superior. After all, it's precisely this feeling of not being good enough that the Triple Package uses to achieve success. The problem is that these pathologies, often the result of extreme insecurity or impulse control, become internalized. 

And the superiority complex can cause damage to others. Throughout history, feelings of supremacy have caused war, oppression, slavery and genocide. 

So, while a superiority complex obviously won't _inevitably_ lead to these things, it does lead to racist attitudes. For instance, West Indian and African immigrants to the United States are often seen as looking down upon black Americans. As Nobel Prize-winning novelist Toni Morrison has written, "the move into mainstream America means buying into the notion of American blacks as the real aliens. Whatever the ethnicity or nationality of the immigrant, his nemesis is understood to be African American."

### 10. America is no longer a Triple Package nation. 

So, some groups have the Triple Package, but what about America itself?

Simply put, it doesn't. In fact, since the Triple Package runs contrary to many emblematic American beliefs, groups who have it do well precisely because the rest of the country doesn't have it. 

However, the reality is a bit more complicated. First of all, nations aren't groups and their sheer size and diversity mean that they can't be analyzed in the same way. Despite this, there's still an overarching American culture.

Secondly, the country has changed. When the United States was founded it had a Triple Package of sorts. The new nation had to deal with the scorn of the "Old World," Europe. For instance, an influential French Enlightenment writer named Guillaume-Thomas Raynal said that America could never be a great country since it had no great animals like the elephant, rhino or tiger. 

The response?

Well, when Thomas Jefferson, then in Paris, heard this, he enlisted some hunter friends in America to send over a gigantic moose, which he presented to the curator of the royal natural history collection. In his _Notes on Virginia_, Jefferson also boasted that the "tremendous" mammoth that used to live in the state was "the largest of all terrestrial beings", at least "five or six times the cubic volume" of any elephant in a European menagerie!

Clearly Jefferson was demonstrating both a superiority complex and insecurity.

But despite that early Triple Package, America has always valued life in the present over impulse control. In the late twentieth century this spirit won over. 

Why?

Because as communism collapsed, globalization was accelerated and capitalism boomed, resulting in American brands and policy dominating the world. This prosperity and success meant America lost its sense of inferiority and gave way to instant gratification.

In the wake of this change, America needs to recover its Triple Package, but it's important that this new American Triple Package embraces tolerance, equality and opportunity.

### 11. Final summary 

The key message in this book:

**Certain groups in America have found success by embracing a Triple Package of a superiority complex, insecurity and the ability to resist temptation. However, not having these three traits doesn't mean you** ** _can't_** **succeed, they just make the process a lot easier.**

Actionable advice:

**Feeling a little bit inferior is good motivation.**

If you want to raise successful children it's important not to always tell them how perfect they are. While it may seem counterintuitive, you need to show disappointment with their abilities and instill in them the desire to prove you wrong. The result will be a child who craves success over all else. 

**Suggested** **further** **reading:** ** _Battle Hymn of the Tiger Mother_** **by Amy Chua**

Amy Chua was born in the United States to strict Chinese immigrant parents who pushed her to work hard and succeed instead of coddling and encouraging her. _Battle Hymn of the Tiger Mother_ (2011) is about her experience of raising her third-generation kids according to her parents' old-school beliefs. Chua offers not only an insightful and often controversial take on parenting, but also a memoir of a very stern yet loving tiger mother.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Amy Chua and Jed Rubenfeld

Amy Chua and her husband Jed Rubenfeld are professors at Yale Law School as well as published authors. Chua penned the controversial internationally bestselling parenting book, _Battle Hymn of the Tiger Mother_ and Rubenfeld has written two mystery novels.

