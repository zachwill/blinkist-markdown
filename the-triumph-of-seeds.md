---
id: 583af7a2d4507000044fa5c8
slug: the-triumph-of-seeds-en
published_date: 2016-11-30T00:00:00.000+00:00
author: Thor Hanson
title: The Triumph of Seeds
subtitle: How Grains, Nuts, Kernels, Pulses & Pips Conquered the Plant Kingdom and Shaped Human History
main_color: D5A955
text_color: 8A6D37
---

# The Triumph of Seeds

_How Grains, Nuts, Kernels, Pulses & Pips Conquered the Plant Kingdom and Shaped Human History_

**Thor Hanson**

_The Triumph of Seeds_ (2015) tells the amazing story of the influence of seeds. Find out how plants have managed to endure and evolve over the course of Earth's long history and how they manipulated both man and animal into doing their bidding.

---
### 1. What’s in it for me? Find out what’s so great about seeds. 

Seed plants are the winners of the plant world. They make up the vast majority of our flora, although other plants — such as those that grow from spores — have been around for millions of years longer.

And these seed plants shape our everyday lives, too. Just imagine an ordinary morning. You wake up in cotton sheets. Cotton starts out as the soft, fluffy stuff that surrounds the seeds of the cotton plant. Your soap or shampoo and your toothpaste contain oils that stem from seed plants — like olives and mint. Your coffee is made from the seeds of the coffee plant, and your granola, well, you get the picture. 

Interestingly, seed plants owe much of their success and their popularity to something very unassuming: their seeds. These blinks will tell you all about them.

You'll also find out

  * how trees have made us their servants;

  * why the size of a seed is so crucial; and

  * about one of the oldest organisms on Earth.

### 2. Every seed consists of three parts and, though they vary in how they develop, all seeds rely on water. 

The world is full of an immense variety of plants that spring from a small seed — plants that then go on to produce their own seeds. Just compare a walnut to a peanut to see how different seeds can be.

Yet, despite their different appearances, all seeds consist of three parts: an embryo, a nutrient tissue and a coat.

To better understand these parts, you can think of a seed's embryo as the baby, the nutrient tissue around the embryo as the baby's lunch and the coat as a sort of protective shell.

Seeds begin to show their differences when it's time to germinate. Germination is the phase that begins when a seed takes its first drink of water, or _imbibes_, and ends when the embryonic root sprouts forth.

Now, this process can vary depending on the seed. The common germination procedure is for the coat to open up, the baby to slowly eat its lunch and then grow roots and sprout upward.

But in other seeds, the baby may eat its lunch before the coat opens. In this case, the baby uses the energy from its meal to form embryonic _seed leaves_, or _cotyledons_, to keep the young plant healthy during difficult times, when sun and water are in short supply _._

And if you've eaten a peanut or a walnut, then you know what seed leaves look and taste like. When you crack open a peanut's shell and remove the thin layer around it, you'll see that the peanut is made up of two distinct halves; these are the seed leaves.

But regardless of variety, seeds need water to grow.

Different seeds may imbibe at different times during the germination process, but every seed is designed to grow roots — and this important step requires water.

So, if the seed is unable to imbibe, the seed will lie dormant.

### 3. Seeds evolved from spores to overcome difficulties in reproduction and adapt to dry climates. 

Did you know that most of our coal originates from a single geological period in Earth's history? It's called the _Carboniferous era_, and it began about 359.2 million years ago and lasted for the next 60 million.

Experts used to think that Earth's entire climate was warm and rainy during this time, which created swampy landscapes that were dominated by the spore plants that thrive in such conditions. This theory suggests that seed plants didn't arrive until the climate dried up, which they believe happened during the following period, the _Permian era_.

However, new research shows that only a small portion of the landscape was swampy during Carboniferous times, and seed plants could thrive on hills and in the vast, dry upper regions.

Since the uplands provided great conditions for seed plants, they were in a perfect position to quickly "take over" and dominate the lowlands once the Permian era began.

But this still doesn't answer the question: Where did seed plants come from?

During the early days of the Carboniferous era, plants evolved and developed seeds as a superior way of reproducing.

For a spore plant to reproduce, two stages have to take place: First, asexual spores carrying the plant's genetic material need to be cast off. Then, each spore needs to produce an egg and a sperm that have to fertilize before a new plant can grow. And this step can only happen when there's enough water around for the sperm to reach the egg.

So, since dry areas don't provide good conditions for spore plants, the plants evolved. Sperm became pollen, which could be carried by wind instead of water. And plants like spikemoss emerged that were no longer asexual, allowing female plants to hold on to their own fertilized eggs.

Eventually, plants began developing a hard shell to keep the egg safe and — voilà! — the seed was "born."

> _"When spore plants have sex, they usually do it in dark, wet places, and quite often with themselves."_

### 4. All seeds die, but some are able to lie dormant for years – even centuries. 

Hibernation isn't just a way for bears to make it through a cold winter. Seeds do something similar. But instead of just one season, seeds can lie dormant for years, or, given the right conditions, much, much longer.

This is what the seeds you buy at the store are doing. Whether they're grass seeds waiting to be spread around your backyard, or herb and flower seeds for your garden, all of these seeds have matured and are awaiting germination.

But even though they have an impressive ability to lie dormant, they are all capable of eventually dying, which usually happens within a couple of years, or decades.

However, if a seed is given a perfect environment, it could stay alive for centuries.

One such seed lasted almost two thousand years and eventually developed into the date tree known as Methuselah. During the First Jewish-Roman War, around the year 73 AD, Romans captured the Masada fortress in the Judean desert and discovered the lifeless remains of a thousand men, women and children.

Remarkably, the inhabitants had all committed suicide, but, before they did, they'd burned all their food and possessions, depriving the Romans of any plunder.

During this fire, the stone walls collapsed, burying many of the goods and stored foods deep in the soil beneath the walls.

It wasn't until the 1960s that archaeologists working at the site of the temple unearthed some of these goods. Among their discoveries was a sealed jar of date seeds — and, surprisingly, when one of the seeds was planted, it germinated and sprouted right up out of the ground.

They called it Methuselah, after the oldest man in the Hebrew Bible, and it is now a healthy, ten-foot-tall palm date tree.

> Methuselah is one of the oldest organisms on Earth.

### 5. Seeds evolved to defend and disperse themselves – with the help of unwitting creatures. 

It would seem that seeds are pretty well protected if they can last thousands of years. But it's not just the normal wear and tear of age that seeds are designed to withstand. They also have defenses against the mice, squirrels and other rodents and animals that love a tasty seed treat.

These creatures actually represented an evolutionary dilemma for seed plants. On the one hand, the plants need to defend themselves, but, on the other, these creatures can provide a helpful way to disperse their seeds.

So the hard coat of a seed generally needs to be just protective enough to keep from being eaten on the spot, and attractive enough to be desirable for animals to collect and carry around.

With this criteria, seed plants evolved to attract the right animals and get them to help their species thrive and survive.

This is what determined the thickness of a seed's protective layer. By having shells that are thick enough for it to take time and effort to open them up, a rodent is compelled to carry them to a safe place in order to get the job done when the time is right. And when this happens, a seed will often get lucky: the rodent will either forget where it's hidden or never get the chance to eat it.

Still, for this scenario to work at all, the plant has to attract the appropriate animals.

In the case of the almendro tree, it attracts its ideal creatures, squirrels and pacas, with fruit that contains thick-shelled seeds that are coated in a protective resin.

The fruit is too big for small rodents to carry away, but it's ideal for medium-sized creatures like squirrels and pacas, with seeds that are just tough enough to be worth the time investment. Plus, when they're not unintentionally dispersing the seeds, the animals have just the right kind of tools and strength to break through the shell's defenses and enjoy the rewards of their work.

### 6. Many seeds travel and disperse due to fruit that attracts both animals and humans. 

Rodents and birds aren't the only ones who enjoy the fruit that a plant keeps its seeds in. It's difficult for anyone not to be attracted by the sweet scents, appealing texture and wide variety of flavors offered by cherries, peaches, apples, nectarines and so forth.

This is no accident. Since their purpose is to act as bait for animals to disperse the seeds, fruits need to be attractive and appealing.

Sometimes, all it takes is just a tiny bit of fruit. A bat will fly thousands of feet, through dangerous territory, avoiding pythons and owls, just to get a seed from an almendro tree that has some fruity pulp around it.

Since dispersal is crucial for survival, plants don't want to rely on just the seed itself being attractive to a couple of animals. With bright, flavorful and aromatic fruit, plants make sure a wide variety of animals will help their cause.

After all, if a fruit falls from a tree, it needs to get away from the mother tree in order to get the sun and water it needs. By appealing to as many animals as it can, it increases its chances tremendously. 

Of course, humans are just another servant, helping disperse the seeds of plants.

Like many other animals, we pick fruit from trees and carry them with us for a snack. And even when we eat the seeds, we're only holding onto them temporarily since the seeds remain undigested and get dispersed when they're inevitably disposed of.

But, most of all, we enjoy fruit so much that we purposely plant the seeds around the world.

In the case of apples, there used to be only one main variety in the mountains of Kazakhstan. But, due to our love and efforts, the apple now comes in thousands of varieties found around the world.

As we'll see in the next blink, however, human agricultural efforts can have their drawbacks.

### 7. Seed manipulation, intended to benefit companies and consumers, has some questionable consequences. 

As science has progressed, one of the more fascinating developments has been the controversial ability to manipulate genetic material. While this makes news all the time when it comes to animals and humans, it's also a complicated subject for seeds.

Scientists actually began manipulating seeds decades ago, and in the mid-twentieth century they created _seedless young_ : a method of breeding seed plants to create infertile offspring.

You may have tasted the results of this engineering: seedless watermelon makes up more than 85 percent of the watermelons sold in the United States.

Some consumers find seedless fruit convenient. They don't need to spit out seeds when eating watermelon or worry about the seeds getting into freshly squeezed juices. And seed companies benefit from this development as well.

But that doesn't mean everyone's happy. 

While the ability to produce seedless plants provides seed companies with more control over their market, this can end up hurting farmers.

A standard practice for farmers and gardeners was to gather and store their own seeds to sow the following season. Now that there are seedless variants, though, farmers have to buy their seeds from profit-seeking seed companies. 

Not only is this more expensive, but since companies have patents on their genetically modified seeds, some farmers and gardeners have been taken to court for using a seed from their own plants.

These developments have to led a wide variety of concerned critics raising issues relating to the environment, general health and the moral questions surrounding the manipulation of genes to create new plants and food. A primary concern is that it might take generations before we know the full effects and possible risks of growing and eating genetically modified foods.

But one thing is for sure: seeds have come a long way over the past 300 million years.

### 8. Final summary 

The key message in this book:

**Seeds are ubiquitous and vital, not only in nature, but also in the everyday life of people. Over many millennia of evolution, they have developed ingenious ways to endure difficulties, to attract exactly the right kinds of creatures to help them multiply and to defend themselves against greedy predators.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Reason For Flowers_** **by Stephen Buchmann**

_The Reason for Flowers_ (2015) is about the origin, reproduction and effects of these amazing pieces of evolutionary artwork. These blinks explain how flowers have sex, why they're so beautiful and why humans have become so infatuated with them.
---

### Thor Hanson

Dr. Thor Hanson is a conservation biologist from the Pacific Northwest and an award-winning author. A Guggenheim Fellow, his other books include _Feathers_ and _The Impenetrable Forest_.

