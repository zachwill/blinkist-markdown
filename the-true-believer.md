---
id: 55a36c153738610007140000
slug: the-true-believer-en
published_date: 2015-07-13T00:00:00.000+00:00
author: Eric Hoffer
title: The True Believer
subtitle: Thoughts on the Nature of Mass Movements
main_color: EA5050
text_color: B83F3F
---

# The True Believer

_Thoughts on the Nature of Mass Movements_

**Eric Hoffer**

_The True Believer_ (1951), published in the aftermath of World War II, is an exploration of mass movements and the means by which they attract followers. These blinks will take you on a walk through history — showing how, under certain circumstances, be they right or wrong, anyone can become a _true believer_.

---
### 1. What’s in it for me? Discover how mass movements are formed and why some lead to lasting change, while others only lead to chaos and destruction. 

The twentieth century, for better or worse, saw its share of mass movements. Millions of people put on boots and uniforms and marched to the drum of Communism, Nazism, Stalinism and other doctrines, claiming they knew the way to a better future.

Although each mass movement is based on different ideas and doctrines, they all have a lot in common. A strong belief that a better future is possible; a strong leader that can channel the discontent of the masses; a clear common enemy — these are the cornerstones of any mass movement.

However, there are important differences, too. Some mass movements truly change the world in a positive way; others lead to chaos and destruction. So what's the difference between them? These blinks will show you.

In these blinks, you'll learn

  * why all mass movements need a strong leader;

  * why mass movements are like plants; and

  * why the revolutions in France and America were so successful.

### 2. Mass movements are spurred on by a belief in change and the hope for something better. 

If you had a job you loved with flexible hours and a good salary would you go looking for another one? Probably not. But what if your job was so bad that you dreaded waking up in the morning? Finding a new one would be a top priority.

People who are discontent or lack hope want change and so are open to messages that promise it. In Germany after WWI, for instance, people were at the end of the line; after losing the war, they felt, the future held nothing. And it was from this fertile soil that the Nazi movement grew.

In fact, the biggest cause of despair among the unemployed isn't a lack of money but a lack of _hope_. Unemployed people are more likely to follow someone who gives them hope than someone who offers them money. When you have hope — even just a shred of it — you start feeling self-efficacy.

That's why all mass movements start by making people feel that a better future is within their grasp — to inspire hope. Consider the French Revolution, which was sparked by the idea that man has an infinite capacity for reason and is not soaked in sin. By spreading this new, more upbeat vision of mankind, the revolution instilled in people a new sense of power and fueled the engine that drove democratization.

But hope isn't the only thing that makes people desire change. Knowing what it means to have something has a similar effect. For example, "the new poor" (people who used to have money but lost it) are the most powerful force for change, because they know what can be achieved. Just look at the Puritan Revolution that catalyzed the English Civil War; it was started by people who'd recently been driven from their property so that landlords could turn the fields into grazing land. People who've lost something will fight to get it back.

### 3. Strong mass movements build unity by replacing individuality with a commitment to something greater. 

Have you ever considered what makes a mass of people — participants in a demonstration, for instance — so powerful? It's certainly not a result of each person's individuality, of each personal decision and doubt. It's because these people act collectively. Accordingly, mass movements are strongest when many people function together, forming a single unit.

Why?

Because members of a group feel empowered and supported by other members. For example, those incarcerated in Nazi concentration camps were much more likely to survive if they were affiliated with some kind of group. Group members do better because they look out for each other when they fall ill or need other forms of support.

But how do groups work?

They eliminate individuality and form a united whole. Consider how the army strips away individuality, assigning uniforms and regulation haircuts.

A change of outfit isn't enough to eliminate the sense of self that prevents group unity, however. To do that, members need something bigger to identify with. For example, nationalist movements form by telling citizens that they're not merely people; they're German, or French, or Italian.

Hitler did this in Nazi Germany by dressing up 80 million people to play parts in a larger-than-life opera, a performance in which the German people themselves played the part of the heroes. Such spectacles, performed in Nuremberg during the heyday of National Socialism, were filmed and later screened in cinemas. But Hitler wasn't the only one to use these tactics. Communist countries continue to put on similar spectacles, intended to display the greatness of Communism and their nations.

Whether it's Nazi Germany or the Communist Soviet Union, the fanaticism of all mass movements offers a way for people to be a part of something greater than themselves; the ramifications, however, can be disturbing. Belief in a doctrine can drastically affect an individual's rationality. For instance, many years after WWII, Japanese refugees in Brazil maintained that the Japanese Empire had never fallen!

> _"The true believer, no matter how violently he acts, is basically an obedient and submissive person."_

### 4. A mass movement needs a strong leader capable of channeling the hatred of many toward a common enemy. 

Have you ever been to a football game and, when booing the opposing team, felt totally united with the crowd of like-minded fans, each of whom is, like you, following every move of _your_ team's star player? Well, something similar plays out in mass movements.

Having a common enemy to unite against is a crucial part of mass movements. In fact, the more powerful their enemy, the more united the members of a movement will feel. Consider the Nazis. Rallying their followers against the Jews, who they perceived as wealthy and influential, was crucial to their success.

This was well understood, by both Nazis and others.

In 1932, for example, a Japanese research group came to Berlin to study the Nazis. When asked their opinion, one researcher responded, "It is magnificent! I wish we could have something similar in Japan, but we don't have any Jews." In fact, Hitler himself once said, "It is impossible to exaggerate the formidable quality of the Jew as an enemy."

But who is the _ideal_ enemy?

Foreigners are a sure bet. During the French Revolution, the enemy — the aristocracy — was depicted as descendants of German tribes. And during the English Civil War, the Puritans referred to the Royalists as "Normans," implying they were French.

But picking a good enemy is only the beginning. In order for a mass movement to rise, it needs a fanatical leader who can channel the hatred of the masses. Without a leader, the currents of discord will never swell into the tsunami of a mass movement.

If not for Lenin, for instance, the Bolshevik Revolution may have never begun. In fact, without this leader it's likely that other high-up Bolsheviks, people like Leon Trotsky, would have joined a coalition of democratic parties. In the same way, without a Hitler or a Mussolini, Nazism and Fascism probably would never have existed. These leaders were the necessary force that channeled the discontent of the people toward a fabricated enemy.

> _"Mass movements can rise without a belief in a God, but never without belief in a devil."_

### 5. Mass movements need people who can put their goals into words and leaders who can act on them. 

To build an IKEA shelf you of course need all the necessary materials. You also need a manual that tells you how to put them together. The same is true for mass movements: in order to start one, someone needs to put its goals and doctrines into language.

The best people for this task tend to be "men of words," people like philosophers and scholars who are trusted for their knowledge. For instance, the French Enlightenment philosophers Voltaire and Rousseau set the ideological foundation for the unborn French Revolution. Even Jesus Christ was a man of words, and his teachings began one of history's largest mass movements, Christianity.

But it takes more than the right words to spur a mass movement. Successful doctrines are usually written by men capable of channeling the frustration of the masses. During the Napoleonic Wars, for example, Prussian losses inspired German philosopher Johann Gottlieb Fichte to call on his countrymen to join together and form one massive nation state.

Once a doctrine has been established, a fanatical leader comes into play to put ideas into action. In Germany, for instance, Hitler took Fichte's ideas and used them to build an empire that he named "The Third Reich." Similarly, Josef Stalin employed the ideas of Karl Marx.

Sometimes a leader starts out as a man of words only to later transform into a fanatic. For example, the prophet Mohammed began as an intellectual, writing the word of Allah in the Quran. From there he went on to lead a fanatical quest aimed at conquering the world for his new religion, Islam.

> _"When the moment is ripe, only the fanatic can hatch a genuine mass movement."_

### 6. All mass movements are of the same nature, but their goals and results can differ. 

Just as two plants can have a similar appearance, be related and yet have drastically different properties, so too can mass movements. For instance, tomatoes, a fruit loved by many, are in the same family as the nightshade, a lethally poisonous flower.

So the fact that two mass movements are of the same nature doesn't mean they share the same goals. For example, fanatical belief in a doctrine is a common impulse that can have many different results, depending on what that specific doctrine says.

Take Czarist Russia, where a desire for change and frequent pogroms, mobilized some Russian Jews to join revolutionary, others to devote themselves to Zionism. In fact, Chaim Weizmann, the first President of Israel, had one son in a revolutionary movement and another who was a Zionist.

Because of this, mass movements can sometimes act as substitutes for each other since they're composed of the same parts. For example, before WWII, Italian businessmen promoted Fascism, fearing that Communism would cost them business. But in reality Fascism was very similar to Communism and had the same effect.

Another fact about mass movements?

The shorter ones tend to result in better outcomes. That's because when a mass movement is active people are preoccupied by it. This causes people to forget about everything else and focus on the movement, meaning nothing else gets accomplished.

For instance, the revolutions both France and America, which both had fairly clear goals, changed things in a short span of time. The result is that we remember them as the first modern democracies. On the other hand, Nazism and Communism, while they had clear goals, just went on being mass movements once they gained power. This resulted in stalled creativity and development, which ultimately led to their demise.

### 7. Final summary 

The key message in this book:

**Whether right wing or left, political or religious, all mass movements share the same characteristics. Under the right circumstances, it's possible for any one of us to get hypnotized by group mentality and become a true believer.**

**Suggested further reading:** ** _Going Clear_** **by Lawrence Wright**

_Going Clear_ offers a rare glimpse into the secret history and beliefs of Scientology as well as the conflicted biography of its founder L. Ron Hubbard. It also details some of the Church's darker qualities: a tooth and nail method of managing criticism and systematic approach to celebrity recruitment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Eric Hoffer

Eric Hoffer was a working-class American autodidact who authored a variety of treatises on moral and social philosophy. After writing his first book, _The True Believer,_ he went on to publish over ten others. He received the Presidential Medal of Freedom, in February, 1983.

