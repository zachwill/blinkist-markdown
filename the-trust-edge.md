---
id: 53285b143635380008aa0000
slug: the-trust-edge-en
published_date: 2014-03-18T09:42:13.000+00:00
author: David Horsager
title: The Trust Edge
subtitle: How Top Leaders Gain Faster Results, Deeper Relationships and a Stronger Bottom Line
main_color: D96377
text_color: 8C3242
---

# The Trust Edge

_How Top Leaders Gain Faster Results, Deeper Relationships and a Stronger Bottom Line_

**David Horsager**

_The Trust Edge_ (2012) explains why trust is not only essential for a happy personal life, but the fundamental factor in successful businesses and business relationships. It outlines and discusses eight so-called pillars of trust and argues that these principles should be at the basis of your daily actions, in order for you to gain what the author terms the "trust edge." The book also discusses how to rebuild trust once it's been damaged.

---
### 1. What’s in it for me? Gain the trust edge, your strongest competitive advantage. 

No matter how charming you are or how good your product is, if people don't trust you, you lack the most important asset anyone can possess.

In _The Trust Edge_, David Horsager explains why generating trust in others is essential to establishing and maintaining long-term, successful relationships. In particular, he focuses on trust in the world of business, where, to a large extent, the trust that customers and partners have in a company determines whether that company flourishes or perishes.

Presenting the reader with a kind of "anatomy" of trust, _The Trust Edge_ identifies eight pillars of trust on which all successful businesses are founded: clarity, compassion, character, competence, commitment, connection, contribution and — perhaps most importantly — consistency.

In the following blinks, you'll find out how to establish trust, and — once you've done that — how to sustain it. You'll learn why people trust the opinions of friends over those of professional critics. And you'll find out why extending trust to others can save you a lot of time, money and effort.

### 2. Being trusted is great for business: it gets you loyal customers, free advertising and a safety net. 

Would you give your money to a bank you don't trust? The breakdown of credit markets in 2008 showed that the sudden and dramatic loss of trust can destroy centuries of work in the blink of an eye. 

This also holds true for any business.

But there's a positive side: if mistrust can be the death of a business, then being trusted can improve the success of a business tremendously.

One reason for this is that being trustworthy leads to a loyal customer base.

And one way to build trust is to deliver on your promises consistently. If your business has established a reputation for being reliable, your customers will not find it easy to abandon you for a cheaper competitor. Indeed, they'll have a good reason to remain loyal, as being able to trust you means they can get on with their business without the need for follow-up calls or double-checking, which saves time and money.

Furthermore, a loyal customer base allows for a more efficient use of your resources, which increases profits and keeps your prices low and your customers happy.

And happy customers is what you want. They'll promote and champion your business, and act as a safety net during hard times.

For instance, most of us are more trusting of recommendations from friends than from ads because our friends don't have a stake in a particular company's success. Money can't buy this kind of promotion — it's earned by establishing a trusting relationship with clients.

Consider, for example, that in 2009, in the thick of the financial crisis, a small software company called Passlogix was able to thrive, even outperforming giants like IBM.

How? The company built their business on a culture of trust and good customer relations. For example, if any of their customers encountered problems, they were invited to call the CEO directly.

### 3. Clarity: Know what you want and let others know, too, so they can trust you. 

It's difficult to trust someone with ambiguous expectations and unclear plans. Such a person seems unpredictable, and thus untrustworthy.

Companies can fall into a similar trap. To avoid this, you must make sure you're honest and clear in your communication. Most likely, you'll see a positive result in your business's bottom line, as well as in your relationships to your customers and employees.

For one, clarity of this kind makes for more effective work. Indeed, many failures result from the miscommunication of expectations, so you shouldn't expect others to read your mind. You need to be as specific as possible, since the more specific you are about what you want, the more likely it is that you'll get it.

Another way that clarity improves effectiveness is that it brings focus to your business activities. With a clear plan, for example, you'll find it much easier to structure daily tasks. So, every night, write a list of the six most important tasks for the next day. Chances are you'll see a tremendous increase in your productivity.

In addition to boosting effectiveness, clarity can also drive business success more directly, in the form of a _clear vision_. Many of the world's most famous companies started out as underdogs yet succeeded precisely because they had a clear vision of their company's direction. For example, although Google wasn't the first search engine on the market, it swiftly became a market leader, acquiring an 80-percent market share.

The reason for its success? A simple and clear vision: "To organize the world's information and make it universally accessible and useful."

It was this clarity of purpose which guided the design of every single aspect of the Google we all know and use — from its simple homepage presentation to its search algorithm.

### 4. Compassion: To gain trust and respect, think beyond yourself and care for others. 

One of the most important triggers for trust is the perception that a company is not only motivated by profit but also concerned about the greater good of its customers and employees. This ability to show empathy and care — in a word, _compassion_ — can therefore be a huge driver of success.

There are two main reasons for this.

First, compassion sustains loyalty. According to a study by California's Saratoga Institute, workers will remain with a company which they believe cares for them, even when a competitor offers them a higher salary. In contrast, employees are very likely to leave if they don't feel respected or appreciated by superiors, regardless of pay.

For example, Starbucks is famous in the industry for having a very low annual turnover rate. The company could accomplish such a "culture of loyalty" by providing the majority of their employees with benefits, like healthcare and retirement options. As a result, Starbucks saves millions because of its low costs for training and recruitment. 

Second, compassion increases your reputation. Consider, for example, Indra Nooyi, the CEO of PepsiCo since 2004, whose vision as a leader was to "bring together what is good for our business with what is good for the world." To that end, under Nooyi's supervision the company produced healthy food products, such as oatmeal and low-calorie Gatorade, and engaged in programs against obesity. As a result, PepsiCo's profits and influence have soared.

### 5. Character: Live by your moral values and be consistent in thought, words and actions. 

Of all the features of a great leader, character is the single most important, since people who demonstrate character consistently generate trust.

But what does "having character" mean? In terms of gaining trust, it means displaying two qualities: integrity and morality. Integrity is consistency in thought, words and actions. In other words, the very opposite of hypocrisy. For example, if a grossly overweight doctor strongly recommends that you improve your diet, would you follow his advice? Or imagine Martin Luther King, Jr. voicing his pacifist message with a gun strapped to his side. Would his message have been at all credible?

Morality means striving to do the right thing, ethically speaking, even when it involves making a sacrifice. Take Mother Teresa, for example. A Nobel Prize winner and one of the most beloved and respected leaders of recent times, Mother Teresa's influence was so strong that, today, one million workers in forty countries continue her work of helping the poor. The reason? People admired her readiness to sacrifice her own needs in order to help others satisfy their own.

Of course, doing the right thing is not always easy. But it will help you in the long run.

Telling a white lie, for instance, is usually more comfortable than facing a conflict directly. But if you do this, reality will eventually catch up with you. For example, in 1986 the space shuttle Challenger exploded due to an O-ring failure. Prior to the catastrophe, the O-ring manufacturer, Morton-Thiokol, was aware of the risk, yet, in order to meet the deadline and avoid criticism, the company agreed to the launch.

The result? Morton-Thiokol took much of the blame for the disaster.

So make sure you act according to your moral principles, always — even if it seems like no one's paying attention.

### 6. Competency: Instead of resting on your laurels, preserve your competence by continuing to learn. 

If you say you trust your doctor, what you probably trust are her intentions to help you and her ability to prescribe the best treatment for you. You probably wouldn't trust her ability to prepare your tax return or to fix the brakes on your car.

That's because trust is essentially inseparable from competence and ability. Therefore, to hold onto the trust you've gained, it's crucial that you maintain your competence. One way to do this is to keep yourself up to date on the newest developments in your field.

According to financial expert Joe Murtagh, most industry-leading companies aren't able to sustain their position for longer than ten to fifteen years. Established companies don't feel much urgency to stay fresh and innovative, so, sooner or later, competitors catch up with the company or even outperform it.

That's what happened to the American automobile industry. In 1979, the three largest American car manufacturers — Ford, GM and Chrysler — sold nine out of ten cars on the domestic market. Today, their share amounts to less than 40 percent. 

Why?

The main reason is a decline in competence. The American manufacturers resisted innovation for too long, which left enough time for German and Japanese companies to improve and eventually become robust competitors.

Take Bayer, for example, who'd been leading the painkiller market successfully for 50 years. Bayer was aware of the potentially big demand for alternative, cheaper painkillers, and yet it didn't dare to create a product that might compete with its cash cow, Aspirin. As a result, Johnson & Johnson were able to swiftly take over the market with Tylenol in the 1990s.

Clearly, it's important to keep learning even if you've already achieved success; this will enable you to foresee market trends and react to the market's sudden and unpredictable changes.

### 7. Commitment: When it counts, stick to your promises even if it involves sacrifice. 

It's often said that you don't know who your real friends are until you face a major crisis.

Those who stand by your side even if doing so demands their making an effort and sacrifices will be the most valuable relationships you'll ever have.

This is equally true of leaders. For instance, a manager who is committed to her company is able to lead it even when times are hard.

Lee Iacocca, for example, is known for his masterly performance in guiding Chrysler through a major crisis in the 1980s. To save the company from bankruptcy, he applied drastic measures, such as laying off employees and selling unprofitable divisions in Europe.

But he also made sure the company's best talents didn't leave in this difficult period.

How?

He gained their trust through a powerful symbolic gesture: for as long as Chrysler was struggling, he took a salary of just $1 a year. This sign of commitment earned him great respect and raised the morale and team spirit essential to reviving Chrysler.

Another example of the benefits of commitment is the motorcycle company Harley Davidson, which was bought by a group of determined, business-savvy motorcycle lovers in 1981.

At that point, the famous brand was in a terrible state: the motorcycles were priced too high, they were manufactured badly and the name alone conjured up images of tattooed gang members in most people's minds.

Still, the new management team was committed to the brand and worked tirelessly to reach practicable solutions. These included outsourcing the manufacture of specific components to ensure higher quality, advertising campaigns to improve the company's image and the adoption of certain management principles, such as quality circles.

The result of their commitment?

Today, Harley-Davidson owns 45 percent of the American heavy motorcycle market and the value of its stock increased by more than 17 percent over the last 25 years. 

Commitment is not only a tool for gaining trust, but one of the most desirable qualities we can find in our friends, lovers and, certainly, our leaders. Keeping your promises and being ready to sacrifice for your convictions will make you a highly respected and trusted person in all areas of life.

### 8. Connection: Know the people around you and build relationships. 

Would you trust a friend's recommendation of, say, a new laptop computer more than a positive review by a professional critic?

One study showed that almost 90 percent of people would.

Although critics are usually considered experts, your friends have knowledge about something even more crucial — what you like and how you judge things. In other words, they know _you_.

As a business, knowing your clients, employees and other stakeholders as if they were friends will give you a crucial advantage over your competitors. 

For instance, if you know your customers well, you'll know how to satisfy them. And the closer you are to them, the more readily they'll provide you with feedback and suggestions about how to improve your product or service.

Being able to connect well is especially important for leaders. For instance, knowing the strengths and weaknesses of your employees makes it easy to assign the right tasks to the right people. And knowing what kind of appreciation they prefer helps you to raise morale and keep everyone motivated.

Another advantage in knowing the people around you is that it can reduce misunderstandings and help to manage conflicts. Interpreting and understanding the behavior of those around you becomes much easier, as does avoiding conflicts or resolving them if they do occur.

But, of course, it's not always easy to connect with people.

The important thing to remember is that you don't need to be a charismatic, networking genius to it. Just because those with magnetic personalities can connect with others easily doesn't mean you need to be an extrovert or an overly charismatic person. In fact, many magnetic characters are actually introverts who've developed the ability to listen and empathize, and to be optimistic, honest, encouraging, inquisitive and grateful.

Trust is all about relationships. And to build relationships, you just need to be genuinely interested in what others have to say.

### 9. Contribution: Deliver and reward results. 

Contributors always deliver. They know that a good plan isn't worth very much if one never acts on it and sees it through.

Completing projects and tasks is extremely important, but perfectionism often paralyzes us and stops us from completing them successfully. But while achieving excellence is important to generating trust, it's always better to produce good-enough, completed work than to produce perfect-yet-unfinished work.

Imagine that your customers are consistently waiting for you to deliver. In such a situation, you're not only costing them money and time, but also destroying their trust in you.

Although aspiring to produce great work is a worthwhile goal, you should, at the same time, try to remain conscious of advancing that work toward completion.

To that end, here are two basic principles which can help you to complete projects as quickly as possible. First, make a plan: write a checklist of the most urgent or important tasks for the day, and make sure you do them. Second, keep your email inbox clean and your desk free of clutter, as this will help you minimize the distractions that hinder progress. In particular, you shouldn't put off making important decisions since this will increase clutter and confusion, and lead to stress.

But contributors are not only doers. They're also _givers_ : first considering what they can do for others, not what others can do for them. In other words, they operate according the maxim: "The more you give, the more you receive."

Indeed, givers are usually happier than people who look only to receive, as they've developed many strong relationships and enjoy having a lasting, positive impact on the lives of others.

And since many of the people in givers' lives are grateful to them, givers will be supported readily when they themselves need help some day.

Building trust means _doing_ and _giving_. Each of these will reward you with a feeling of purpose and human connection.

### 10. Consistency: Practice the pillars of trust every day and in everything you do. 

Consistency is the final, and most important, pillar of trust. That's because it underpins the other seven pillars: if you don't practice clarity, commitment and character _consistently_, their effect will be diminished, and any trust you've established may be broken.

But you should be aware that, of all the pillars, developing consistency requires the most patience. It will be worth the effort, though, since building trust in any relationship is never harder than when you have to _re_ build it after it's been damaged.

For businesses, being consistent is the only way to build a reputation and a brand. Most people won't be willing to take a risk each time they buy one of your products, so to gain their trust you have to consistently deliver products of the same quality.

The fast-food giant McDonald's is a great example of a consistent company. Wherever you buy their burgers — New York, Tokyo or Berlin — they will always taste the same.

Another example is Walmart, which has continued to grow despite being publicly criticized for its outsourcing practices, resistance to union representation and gender discrimination. The reason for the company's continued growth is that it delivers consistently on its promise of "Always Low Prices."

As these examples suggest, consistency is at the heart of trust because trust can only emerge over time. There's no one, single opportunity in which you can gain the trust of the people in your life, or of your customers. Rather, there are thousands of tiny opportunities for you to earn it by behaving consistently.

Trust is like the oxygen in the air we breathe — we hardly ever think about it yet it's an essential part of our life. Just as breathing is essential to our survival, the consistent application of the trust pillars is essential to maintaining trust.

So remember: Every interaction counts.

### 11. Trusting in others brings out their best qualities. 

Getting others to trust you certainly is crucial to success, both personally and in business.

But what about the reverse? How much should you trust others? Isn't there some truth to the saying "trust is good, control is better"?

Not really. Often, the potential rewards of trusting others outweighs any possible harm. And by extending the right amount of trust to others, you can provide them with the opportunity to be the best they can be.

Indeed, one huge benefit of extending trust to others is that it can be extremely motivating. We've all been in situations where we've been controlled so totally by others that our creativity and sense of initiative seem completely out of reach. Furthermore, those doing the controlling impair themselves, too, since exerting control consumes a lot of time and energy; not only do they need to communicate _which_ tasks need to be performed, but also _how_ they should be performed.

By contrast, if you entrust an important task or project to others, you'll make them feel valued. And by delegating significant tasks to subordinates who've already proven themselves trustworthy, you both acknowledge their achievements and give them more room to grow. But you should keep in mind that trusting others means taking a risk. For that reason, you should think strategically and entrust people with the tasks and responsibilities you believe they can handle. 

Another benefit of extending trust is that it reduces transaction costs. In an environment without trust, it takes a lot of resources to monitor people's actions effectively. But if you create a culture of trust, this can improve efficiency and effectiveness.

Consider, for example, a popular doughnut cart in New York whose owner let his customers take their own change. Compared to other similar carts in the same area, he was able to serve twice as many customers in the same amount of time, and therefore more than make up for any losses resulting from customers' poor math skills or self-interest.

### 12. Trust can be rebuilt – if you’re patient and willing to go the extra mile. 

Trust resembles a forest in the sense that it takes years to grow but can be completely destroyed by just one small fire.

Fortunately, trust _can_ be recovered, but it's difficult to achieve and demands great patience, since it can only happen if you persist in following certain steps.

If you've let down someone who trusted you — say, by failing to deliver a product by the agreed upon deadline — you must first accept full responsibility for the failure and apologize. This is a crucial step because if you deflect the blame, you'll simply destroy any trust that remains.

Next, after you've apologized, you have to give those you've let down enough time and space for them to recover their trust. In that time, you can try to find out if there's anything you can do to solve the problem. This can often entail making a much greater effort — say, working faster — the next time you have to deliver on a promise you've made.

Unfortunately, however, sometimes trust can't be rebuilt: the people you've let down may be so deeply hurt that trust can't be regained, no matter what you do. When this happens, the only option available to you is to accept the reality of the situation, learn from your mistake and move on.

When a company loses customers' trust, those customers tend to project their mistrust onto every single thing they associate with that company: its products, employees and even its logo.

In this situation, it might be sufficient for the company to change its name or logo, or restructure its management. But this will only be effective if the company actually learns from its mistake and consequently makes profound changes to ensure the problem doesn't occur in the future.

It only takes a few seconds to destroy a reputation; rebuilding it can take years and years.

### 13. Building trust globally requires you to magnify your efforts. 

A globalized world represents many great things such as easy access to foreign goods or the opportunity to travel to exotic countries. However, such a world also represents a massive challenge to building trust.

One such challenge stems from the diversity of people's backgrounds, as this can undermine trust.

While it's true that teams comprising members with diverse backgrounds can generate great ideas, because each member approaches a topic from a different angle, it's also a fact that such diversity can be a source of conflict and misunderstanding. That's because the more closely other people resemble us, the more we tend to trust them.

This doesn't mean that global teams are an inherently bad idea. In fact, they're often far more productive than homogeneous ones.

But they could be improved upon: by remaining conscious of a group's cultural differences and the misunderstandings that could arise from them, collaboration could become even more fruitful.

Compared with gaining the trust of those with a similar cultural background, gaining foreigners' trust is usually far more challenging, requiring extra caution.

In particular, you should be careful of the following:

First, don't ignore another culture's customs. Ignorance can lead to skepticism about your intentions and abilities. So, if you travel abroad and your host is trying to give you an insight into his culture, try to be an attentive student.

Second, don't pretend that cultural differences don't exist. If something in their culture appears strange to you, speak openly about it but remain objective. Your hosts will be interested in hearing your perspective.

Finally, never stereotype foreigners. Instead, attempt to get to know them as individuals. This is especially important in economically significant countries (like China, Russia, Japan and India) as relationships there play a much larger role in business than in Western countries.

Doing business in other countries can be extremely lucrative. However, international business relations often aren't established due to a lack of trust between the individual parties. In this situation — as in many others — your "trust edge" becomes especially important.

### 14. Final summary 

The key message in this book:

**Trust is crucial to every aspect of our lives, in business and personally. For that reason, we should employ the eight pillars of trust: clarity, compassion, character, competency, commitment, connection, contribution and consistency. The last of these, consistency, is especially important because every interaction we have with others has the potential to reduce or increase trust. Living according to these guiding principles will give you the "trust edge."**

Actionable advice:

**Make realistic promises and deliver on them.**

Never make promises you can't keep. No matter how insignificant your promise seems to you, breaking it will always make you appear less trustworthy.

**Care and connect to reduce misunderstanding and conflict.**

Caring about people will make you a better leader, salesperson, co-worker and more. If you are truly interested in those around you and are able to connect with them, less misunderstandings and conflicts will occur.

**Don't let fear guide your life.**

Often, you will need to take a risk and trust someone. In some rare cases, your trust might be abused, but in general people will feel honored that you trust them and will aim to do their best to meet your generous expectations of them.
---

### David Horsager

David Horsager is a business strategy consultant and keynote speaker. From his firsthand experience as the director of K-Life Inc., and founder of Special Delivery Productions, he learned how crucial the ability to gain trust could be to the success of a business. Horsager also works as an adjunct Professor of Organizational Leadership for Bethel University's graduate program.

