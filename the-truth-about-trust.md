---
id: 533b0c9139613700072e0000
slug: the-truth-about-trust-en
published_date: 2014-04-01T09:03:57.000+00:00
author: David DeSteno
title: The Truth About Trust
subtitle: How It Determines Success in Life, Love, Learning, and More
main_color: D5FFAD
text_color: 6B8057
---

# The Truth About Trust

_How It Determines Success in Life, Love, Learning, and More_

**David DeSteno**

_The Truth About Trust_ not only explores what trust exactly means, but also how it impacts almost every single aspect of our everyday lives. The author's own extensive research and progressive experiments from the fields of psychology, economics and biology reveal the surprising ways in which trust deeply matters.

---
### 1. What’s in it for me? Discover how trust is the foundation of all relationships. 

You'd be wrong to think that trust only matters in the big moments of life: the weddings, the business deals, the heart operations.

Matters of trust influence practically every aspect of our everyday lives and occupy a huge amount of our mental energies when it comes to learning, romance, business and our behavior on the internet.

In these blinks, you'll find out about the roots, reasons and risks of trust.

You'll explore how other species reveal the evolutionary foundations of trust, and read about how monkeys get so upset when their trust is violated that they throw food at their researchers!

You'll also learn that, counterintuitively, trusting yourself is just as big a gamble as trusting anybody else — and why trusting someone more powerful than you can be a big mistake.

Finally, you'll discover why the most common cues for identifying untrustworthy people, like shifty eyes, are totally unreliable. Luckily, you'll learn something crucial about body language that'll help you make more reliable judgments.

### 2. Trust allows people to work together towards greater rewards – but short-term benefits of betrayal are always tempting. 

Every day we make decisions based on trust: if a friend wants to borrow $100, do you give it to her? Yes, you like her, but you haven't known her long — will you ever get your money back? This risk is the whole essence of trust: when you trust someone, you take a gamble that they won't only indulge their short-term selfish desires, but also think of the long-term relationship you build together.

So why do we trust?

Because, despite being risky, trusting others can bring great rewards.

Trust is necessary to get resources and benefits we can't obtain on our own. We achieve more by working together than by working alone, which results in financial, physical and social gains.

For instance, we send our kids to school, trusting that someone else will give them a good education so we can focus on earning money.

But we are also hard-wired to be drawn to "selfish" short-term individual rewards.

In fact, our minds are always balancing opportunities for short-term selfish gains, like cheating on a fiancé to have an exciting fling, against longer-term communal ones, like resisting short-term temptation and opting for a less-exciting but loving long-term relationship.

These urges are due to evolution: we have evolved to be reproductively prosperous "winners" — not saints who always do the right thing.

In prehistoric times, the world was filled with dangers, so life expectancy was short, and our ancestors didn't think much about the long term. This has left us with a penchant for occasionally disregarding long-term relationships in favor of selfish short-term behavior — especially when we know we won't be caught.

But evolution also exerted another influence: once humans started to live in tribes, they saw the benefits of cooperation. It became clear that those who helped others — who helped those people in return — were better off in the long run because it benefited both parties.

### 3. The human mind and body have built-in systems that guide who we trust. 

Think of the last time you decided whether to trust someone. Could you feel it in your stomach or in the beating of your heart? There's a good chance you did, because we feel trust as much in our bodies as in our minds.

That's because our feelings of trust have a physiological base.

Humans and other social primates like chimpanzees share something called a _vagus_ _nerve_, a key nerve that runs straight from the brain to our chest. When activated, it has a calming effect on bodily functions, like our heart rate and perspiration, and only when the vagus nerve has brought us to a sufficiently calm and assertive bodily state can we feel trust toward others.

Also, our body uses hormones like _oxytocin_ to affect how much we trust others.

In experiments where participants had to make financial decisions in teams, it demonstrated how spraying oxytocin into participants' noses increased trust and cooperation among those on the same team, while increasing distrust toward other teams.

This physiological base is the foundation of our primal instinct for detecting and rejecting unfair and untrustworthy behavior.

In fact, experiments have shown that people will instantly refuse money offers that seem unfair, even if it means walking away with nothing. In their gut, people know they aren't being treated fairly and will sacrifice their own profit to make the point that the other person is acting in an unfair — and thus untrustworthy — manner.

This instinct is so deeply rooted in our evolutionary past that we share it with our primate cousins. Studies with monkeys have shown that they also hate unfairness. During one experiment, monkeys received food rewards each time they completed a task and reacted furiously if they saw another monkey being rewarded with a tastier food for the same task. The unfairly treated monkey would refuse to accept the inferior reward or even throw it back at the experimenter!

So next time you suspect somebody, listen to your body — do you feel tense, or relaxed?

### 4. From nearly the moment they’re born, children judge whether an adult is trustworthy or not. 

We often think of children as easy to fool, but when it comes to trust, children are a lot less gullible than commonly believed.

In fact, due to their vulnerability, there is a strong evolutionary reason for children to figure out early upon whom they can rely.

Earlier than most would imagine, the minds of children are already calculating who they feel they can trust. Experiments have shown that six- to ten-month-old babies can differentiate between who can and can't be trusted. In one experiment, infants were shown a puppet show with two puppets: a "good" one and a "mean" one. After watching the show, most infants preferred reaching out for the "good" puppet.

This shows that, to some extent, even babies are able to sense who they can and can't trust.

Another experiment shows that children decide whether to trust an adult based on their _competence_.

In the experiment, three to five year olds were asked to watch two adults naming familiar objects out loud. One adult would always name the objects correctly, while the other intentionally made mistakes, for example calling a hammer a fork. Afterwards, when the kids had to learn about other unfamiliar objects, they directed their questions three times more often to the adult who hadn't made mistakes — the "competent" adult.

The effect proved to stick, since even three weeks later, when the children were brought back to learn about more unfamiliar objects, they continued to prefer asking the competent adult. This shows that children make a strong impression of an adult's competence and place more trust in competent adults.

So, since learning comes down to whether or not you can trust what you're told, children's minds are already categorizing their teachers, i.e., adults, as trustworthy or not — almost from the moment they're born.

### 5. Trust is fundamental to fostering long-term romantic relationships. 

All types of adult relationships, from friendship to employment, involve some level of mutual dependence. But when love enters the equation, trust takes on a fundamental importance.

From an evolutionary perspective, the trustworthiness of a potential co-parent is more important than that of anyone else.

Since the origins of humanity, parenting partnerships have been key to the survival of children. Having two parents to feed and defend the child greatly raised its chances for survival. That's why our minds have been shaped by evolution to need to be able to trust our romantic partners — unconsciously, we need to trust that our partner will stick around and help care for the child.

And things haven't changed — trust is still at the center of successful, modern-day romantic relationships.

This was shown in a recent study examining the role of trust in romantic relationships. Couples were invited to first individually complete questionnaires about trust and then discuss a desired goal that would require major sacrifices from the other partner.

The recordings of the discussions revealed that individuals entering the discussion with more trust in their partners showed a much greater desire to collaborate and compromise to find a solution. And it was also found that the more trust one partner had in the other, the more their partner's responses were appreciated as noble sacrifices.

What's more, by helping to sustain long-term relationships, high levels of trust also support your general well-being. In fact, study upon study has shown that there are economic, social, physiological and psychological benefits to being in a long-term relationship.

### 6. The more status, power and money we have, the less we trust others – and the less trustworthy we are. 

We all need to place our trust in others to obtain what we can't obtain alone. But what about someone who is completely self-sufficient, who has everything they need, and doesn't need the support of others?

We're talking about people with high socioeconomic status — those who have lots of money, power and influence. Research indicates that since they already have most of their needs fulfilled and are less dependent on others, they're more likely to be selfish.

This was shown by an experiment conducted at a busy intersection where a researcher crossed at a crosswalk and observed which cars stopped as required. The results were clear: the more expensive the car, the greater the chance that the driver would cut the pedestrian off and keep driving — in other words, the higher their socioeconomic status, the more likely the drivers were to put their own needs first and break the law.

This shows that wealthier individuals are likelier to disregard your trust in them if it's to their advantage — in this case, they were prepared to endanger a life to save themselves a few seconds.

And higher socioeconomic status isn't only associated with less trustworthy behavior, but also with a reduced willingness to trust others.

Scientists observed this when studying individuals of varying socioeconomic status playing _the_ _trust_ _game_. In the game, each participant is given some money they can then choose to give to a "trustee" who will triple the amount of money in the pot — and isn't required to give any of it back. As a participant, you either trust the trustee to act fairly and split the profits with you, in which case everyone benefits, or take the smaller pot home without the risk of trusting a stranger.

And just as the researchers had expected, the higher the socioeconomic status of the subject, the lower their willingness to trust.

### 7. Contrary to what you might think, you can’t always trust yourself. 

If you claim you can always trust yourself to make the right decision, think again.

Actually, trusting yourself is a lot like trusting someone else: you're placing a bet on what another person, "the future you" is likely to do — and experiments show that the future you rarely acts predictably.

The author himself showed how little people can trust themselves by asking one hundred people if they believed they would cheat in the following scenario:

You're left alone in a room to flip a coin, and the outcome (heads vs. tails) determines whether you or another subject in the adjacent room will be assigned a difficult and long task versus a fun and short one.

Every single person answered that it would be wrong to lie about the outcome of the toss, and that they wouldn't engage in such untrustworthy behavior. Yet, when they were put in this exact situation, a hidden camera revealed that 90 percent of them cheated!

But we don't only overestimate how much we can trust our future selves, we also rationalize our own untrustworthy behavior away:

We humans have such a strong desire to view ourselves as good people that when we fall short of our ideals, we try to explain it away.

After the coin-flip experiment, when the cheaters were asked about their behavior, they didn't even view their actions as untrustworthy. Instead, they offered rationalizations arguing that they had no choice or that it was right to do so, like, "Well, I'm sure the next person isn't as swamped as I am right now, so they should take the longer task."

Therefore, you shouldn't consider trusting yourself any less of a risk than trusting someone else. You'll be surprised at how good we all are at abusing both our own and each other's trust.

### 8. There is no single clear sign, or “golden cue” for trustworthiness – trust signals are complicated and context-dependent. 

Shifty eyes are a reliable indicator that a person is lying, right? Actually, contrary to what many people believe, isolated gestures and expressions will tell you nothing about whether a person is trustworthy or not.

To interpret cues about trustworthiness correctly, context is key and, more specifically, two facets of context: _configural_ and _situational_.

First, looking for a configuration or a set of cues rather than relying on single cues that might be ambiguous on their own allows for greater accuracy in detecting other people's intentions, including their trustworthiness.

Research on this shows that basic facial expressions often prove useless in isolation for figuring out other's intentions. For instance, when people are shown pictures of only the faces of athletes experiencing intense winning or losing emotional states, they are very poor at correctly inferring which state the athlete is in without seeing the rest of their bodies. An open-mouthed, furrowed-brow expression looks aggressive in itself, but if accompanied with one or both fists pumping, it becomes the nonverbal equivalent of "Yes, I nailed it!" This shows that accuracy in detecting emotional states and intentions, including trustworthiness, comes from looking for a collection of signs, not just one "golden" cue.

And, second, we also need to consider the situational context when detecting trustworthiness.

For example, the same cue can signal different intentions depending on the specific situation, like who is showing it.

We see this in studies that show that our brains can interpret someone's smile as a signal of support or malice depending on how we categorize that person socially. Your best friend's smile will make you feel happy and safe, but your sworn enemy's smile will likely arouse your suspicions.

### 9. Technology and online social interaction have opened up new ways to manipulate trust – for better and for worse. 

No one can deny that technology has radically changed how people interact with each other.

And today we're using technology to be social just like in real life — but with a twist.

In 2011 over half a billion people spent more than twenty hours per week interacting with others as _avatars_ — graphical representations of themselves controlled in real time. These are used, for example, to play on the Nintendo Wii.

Interestingly, this virtual world of avatars also seems to be governed by the same norms that apply to our daily real-world interactions.

This was demonstrated by researching a virtual world called Second Life where users navigate avatars through everyday interactions. Scientists showed that the normal rules of social interaction of the real world continue to apply in this virtual realm. For example, just like in real life, male avatars maintained a significantly greater distance between themselves when talking than female or mixed-gendered pairs did.

However, internet social interaction is different — there is no direct body language to deduce intentions from — and this brings a greater potential for manipulation.

For instance, we normally judge a person's trustworthiness by visual cues: if, for example, they constantly fidget or touch their face, they might be untrustworthy. But, on the internet, we can't access that information, and are therefore deprived of reliable signals about the intentions of others.

Just as technology can be used to hide information and con people, it can also simulate trustworthy behavior in new forms of technology created to help us.

For example, a virtual nurse called Louise helps people who struggle to follow the procedures prescribed in their after-hospital care plan. The scientists designed Louise to convey medical advice using the body language and tone of voice of a real-life trustworthy person. This illusion of trustworthiness actually made patients feel more at ease — and more successful in following medical advice — than with real nurses.

### 10. Final Summary 

The key message in this book:

**Trust is fundamental to all relationships. When we trust someone, we take a gamble that they'll prioritize the long-term relationship we've built together instead of indulging their immediate desires. Despite this risk of betrayal, trust is worth it as it allows people to work together towards the greater rewards that only cooperation can give.**

Actionable advice:

**Accept that you can't trust yourself to always do the right thing, and make use of the different techniques that help communicate with your future self.**

If post-its with friendly reminders or intimidating warnings don't do the trick, consider downloading one of the many apps designed to help control the actions of your untrustworthy future self. There are apps that can urge you to exercise and keep your diet; then there are the very specialized ones that can, for example, prevent you from going on Facebook all the time or from calling certain numbers on your phone during preset periods of time.

**Make sure your mind and body are in the necessary state to be open to trust.**

If you enter a new situation feeling angry or nervous, you've already limited your ability to trust, and if you enter a situation feeling extremely calm, you might end up trusting someone you shouldn't. Therefore, if you want to get a "realistic" view of someone's trustworthiness, don't rush into a first meeting with them after an emotional event. Rather, take a moment to calm down. And when the stakes are high, remember to calmly pay close attention.
---

### David DeSteno

Professor David DeSteno directs the Social Emotions Research Group at Northeastern University. He also co-authored _Out of Character_, a _Wall Street Journal_ psychology bestseller.

