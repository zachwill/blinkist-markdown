---
id: 56e6de059854a500070000e4
slug: the-truth-en
published_date: 2016-03-16T00:00:00.000+00:00
author: Neil Strauss
title: The Truth
subtitle: An Uncomfortable Book About Relationships
main_color: BEB085
text_color: 736A50
---

# The Truth

_An Uncomfortable Book About Relationships_

**Neil Strauss**

_The Truth_ (2015) takes a close look at relationships, sex and trust, unpacking our assumptions about sex and arguing that monogamy may not be right for everyone.

---
### 1. What’s in it for me? Find out why love doesn’t have to be confined to a monogamous relationship. 

What is a "model relationship"? Traditionally, a monogamous couple: two people so deeply in love that they wouldn't even consider looking at another person, let alone having sex with them. 

This traditional view still dominates, despite the fact that it isn't at all true. In fact, there is simply no such thing as a "model relationship." All that matters is what works for you. You can love as many people as you want; the only thing that should concern you is what makes you, and your partner(s), happy.

These blinks tell the story of those who have embraced other forms of sex and love. They also explain that to enjoy such an alternative lifestyle you need to get yourself into the right frame of mind: one centered on the concepts of respect, trust and self-understanding.

You'll also learn

  * why swingers refer to their activities as "The Lifestyle";

  * why not everyone agrees on what constitutes a sex addiction; and

  * why you can blame your relationship problems on your parents.

### 2. Polyamory – the act of loving multiple partners – can be enjoyed spiritually or by swinging. 

Have you ever heard of _polyamory_? Many haven't. Nonetheless, this word, brought to prominence by New Age writer Morning Glory Zell-Ravenheart, refers to the practice of loving multiple people simultaneously. It developed as a reaction against monogamous society and the stresses of marriage within it. 

After all, would you rather be monogamously but unhappily married or in a partnership that allows you to share your love with others and gives your partner freedom to do the same?

But polyamory isn't just about sex. In fact, many people stress its spiritual side. So, instead of viewing it simply as a way to have sex with multiple partners, you should see polyamory as a way of concentrating on the finer, more spiritual aspects of love. 

It can foster compassion and understanding between partners, utterly banishing the judgment and shame that often surround self-exploration and desire in monogamous culture. If a relationship _does_ become sexual, this spiritual base enables complete openness between partners, making sex a holy experience that transcends the word's smutty connotations. 

But embracing the spiritual side of polyamory can seem weird to some. That's why many people choose to explore this world through swinging. The problem is that swinging has earned itself a bad reputation as a seedy practice and members of the polyamorous community tend to instead call their life practice _The Lifestyle_, thus asserting their commitment to living a life of sexual adventure. 

However, that doesn't mean people living The Lifestyle should view it as a free pass to have sex however they please. In fact, boundaries and rules are essential because they let people know what they can and can't do. With clear rules in place, each partner is free to enjoy The Lifestyle through a trusting, shame-free relationship that they know is strong.

> _"The thing about swinging is that it strengthens good relationships and destroys bad ones."_

### 3. Polyamorous relationships can be fixed or open. 

So, you now know what polyamory is. Polyamorous relationships, however, come in many different forms.

Some people choose relationships with multiple, but fixed partners. That's because operating in complex relationships is no easy task and such limitations help simplify things. For instance, how do you establish boundaries and equally divide the time you spend with your partners?

Naturally, open communication is of the utmost importance. Each person must feel comfortable sharing their feelings as honestly as possible. And to build this atmosphere, everybody needs to get over their natural jealousy. 

How?

By thinking of your relationships in terms of giving and not just getting. That means thinking about what good you can give to the group, not simply what you want in return. 

However, in many cases, relationships of this type have a _fulcrum_. That is, a member who has a relationship with every other person. Because of the trust they enjoy with all other members, this fulcrum often occupies the role of "benevolent dictator" and so controls all decision-making. 

At the other end of the polyamorous spectrum is the _open relationship_. In this arrangement each partner can sleep with whomever they like. But such an agreement requires both partners to let go of their fears of loss, insecurity and jealousy, and that is easier said than done. 

For instance, all humans, men in particular, are naturally wired to compete for sex. In fact, studies have found that men ejaculate more and with greater force when sleeping with a partner who recently slept with a rival male. 

But jealousy can be banished if you separate the intimacy engendered by sex and love from your more general feelings of sexual desire. Only after you do this will you be able to enjoy sex with others. 

The danger is that this ability may amount to avoiding love, and that is sure to leave you unhappy. 

So, polyamory comes in different forms. But what differentiates it from a mere sex addiction?

### 4. Making sex your highest priority could be a sign that you’re a sex addict. 

Most people have heard stories about sex-obsessed people. But what really constitutes a sex addiction? Serial affairs? Watching too much pornography? Constant masturbation?

Actually, a sex addict is simply someone who places the opportunity to have sex over all else. They can't control their behavior when presented with the opportunity to indulge in their addiction, even if they don't enjoy it. 

Just like other addicts, sex addicts are trapped in a cycle that begins with preoccupation, proceeds to ritualization — that is, the acting out of the addiction — and ends with shame. Then it's back to the beginning. But sex addiction is different from other types of dependencies. Unlike alcoholics or drug addicts, sex addicts harm others rather than their own bodies. 

However, though sex addiction is a recognized condition, the boundaries of diagnosis are unclear. For instance, when raised in the context of therapy, the practice of masturbating while "using" fantasies of female bodies is considered sex addiction by some. To others, this doesn't amount to an addiction. 

Another controversial matter is how therapists connect sex addiction to a fear of intimacy. For instance, sex therapists tend to view addiction to sex as an escape from intimacy. This theory suggests that intimacy only exists in deep, long-lasting, "true" relationships; those who steer clear of such connections are therefore steering clear of intimacy. 

But many see this assumption as incorrect. For example, when the author was diagnosed with sex addiction following an affair, he began considering his condition's connection to intimacy. For him, it's simply not clear-cut. 

After all, what about couples who swing? Are they trying to escape the intimacy they share or simply exploring their sexual desires?

So, what really matters is that the relationship gives those involved what they want. If both partners want to sleep with other people, that's fine. It doesn't make them intimacy-fearing addicts.

> _"Perhaps marriage is like buying a house: You plan to spend the rest of your life there, but sometimes you want to move — or at least spend a night in a hotel."_

### 5. Sexual and relationship-based problems are often rooted in family situations. 

Have you ever experienced a traumatic event? Just about everyone has. During childhood, most people experience some sort of trauma, be it neglect or loss or abandonment. But while most people can overcome the unconscious feelings of worthlessness such experiences produce, others are driven to external stimuli to make them feel better. 

For some people, this develops into a sex addiction. For instance, those who experienced neglect during childhood through physically or emotionally distant parents tend to grow into adults without much sense of self-worth. 

In relationships, these people often become anxiously attached, worrying that they're not worthy of their partners. They come to require constant reassurance and affection — and so are known as _love addicts_. Though they desire constant love, it's unlikely they'll become sex addicts. 

On the other hand, those raised by overbearing parents often have the opposite issue. Kids who are weighed down by their parents' problems and desires grow up to mistrust deep connections with others, which makes them _love avoidant_ — and this is the foundation of sex addiction. In fact, 80 percent of sex addicts were raised in emotionally disturbed families.

It's the goal of therapy to overcome such traumas. But the first thing to understand is that these problems aren't caused by one person in a family. They're caused by the family as a whole, which makes children unable to detach from their parents or escape the role they played while growing up. 

Therapy works by helping the addict to see this and move away from it. The intention is to rebuild the true self by establishing an honest and functional relationship with it, because only by building a healthy relationship with oneself can one build such a relationship with others.

### 6. Love avoidance and sex addiction are issues with accessible solutions. 

So, when it comes to intimacy there are two extremes: love addicts — for whom it's nearly impossible to live without someone to love — and the love avoidant, who struggle to accept intimacy and love. And while we don't all occupy one extreme or the other, we do tend to lean to one side. 

In fact, relationship difficulties often arise between people from different sides of the spectrum. For instance, when a love avoidant person finds himself trapped in a relationship, he will search for intensity to replace the intimacy he can't handle. However, this is often just a way of running away from himself, a cycle that he is bound to repeat from one partner to the next. 

So, if you're love avoidant because your parents overburdened you emotionally, you will feel responsible for the feelings of others and guilty about your own. Luckily, you can overcome this by understanding yourself and accepting your true feelings. If you're able to concentrate on what makes you happy, you'll stop running from intimacy and be able to enjoy it. 

On the other hand, if you're love avoidant _and_ sex addicted — that is, truly incapable of controlling your behavior — it's essential to get yourself help. Because sex addiction often has physical causes as well. For instance, most sex addicts have a weak prefrontal cortex, meaning it's difficult for them to control instincts and emotions. 

Luckily, this can be overcome through diet and medication. For example, herbs that improve frontal lobe function and a healthy diet that balances blood sugar. 

And finally, it's very normal to fantasize about sleeping with others. In fact, a University of Vermont study found that 98 percent of men and 80 percent of women have had fantasies about people other than their partners. If you're one of them, it might be time to think of different relationship forms. Who knows, maybe polyamory is right for you. Just be sure you and your partner have each other's trust.

### 7. Building a healthy relationship with yourself is essential to moving on from your traumatic past. 

Do you feel like your relationship to yourself is a healthy one? If not, you could benefit from this two-step technique:

It begins with _emptying out_, a phase in which you take space from relationships and sexual partners in order to better understand yourself. This is helpful because it's important to see that it's not your relationships that are broken, but _you_. That means there are problems you can't solve simply by forming a relationship with someone else. 

After you've emptied out, the next step is to _fill up_ again. Because once you've gotten rid of the feelings your traumas produced, you can begin remaking yourself based on responsibility and honesty. 

So, instead of jumping from relationship to relationship, leaving all your partners unhappily in the dust, it's important to remember your responsibility to your feelings and those of others. But it's also key to keep in mind that how you feel _now_ is what matters the most. 

But overcoming your traumatic past experiences is only half the battle; you'll also need to make sure your fears and expectations of the future don't distract you. Concentrate on the here and now and cultivate the patience to nurture your relationship to yourself.

During this time, you might be attracted to others, but just remember that indulging in unhealthy sexual habits will give you a moment of pleasure and, potentially, a lifetime of shame.

> _"Only after you've learned how to be alone without loneliness will you be ready for a relationship."_

### 8. Final summary 

The key message in this book:

**Monogamy is not necessarily for everyone. However, it can help some people find intimacy. If you're struggling to maintain relationships, it might be because of traumatic childhood events. The only real solution is to get to know yourself better and learn what you really need.**

Actionable advice:

**Learn which segments of your brain correspond to which segments of your relationship!**

Did you know that different parts of your brain correspond to different aspects of your relationship? It's true. In fact, there's one segment for sex, one for romance and one for attachment. While the first two sections might start to be activated by other people as your relationship goes on, you can keep the romance in your relationship by doing things together and having sex regularly. This will keep the sexual part of your brain in good shape! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Game_** **by Neil Strauss**

_The Game_ (2005) gives readers an inside look into the "pickup community" frequented by men desperate to convince women to sleep with them. These blinks share the advice of a leading seduction guru, and the less than great consequences of his successes.
---

### Neil Strauss

Neil Strauss is a journalist who writes primarily for _The New York Times_ and _Rolling Stone_. Especially well known for his undercover journalistic investigations, he has performed with musicians like Marilyn Manson, hosted his own TV show and written several books.

