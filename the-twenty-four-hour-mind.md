---
id: 53a9933e6462310007060000
slug: the-twenty-four-hour-mind-en
published_date: 2014-06-24T00:00:00.000+00:00
author: Rosalind D. Cartwright
title: The Twenty-four Hour Mind
subtitle: The Role of Sleep and Dreaming in our Emotional Lives
main_color: 2F466A
text_color: 2F466A
---

# The Twenty-four Hour Mind

_The Role of Sleep and Dreaming in our Emotional Lives_

**Rosalind D. Cartwright**

_The_ _Twenty-four_ _Hour_ _Mind_ illuminates the mysteries of sleep, dreams and sleep disorders. The author posits that the main purpose of sleep and dreaming is to help us cope with the negative emotions caused by new experiences by linking them to older memories.

---
### 1. What’s in it for me? Understand your 24-hour mind. 

Have you ever wondered what actually goes on in your mind while you sleep? Or what those bizarre images in your dreams might mean? Were you ever so curious to figure that out that you found yourself looking through old college textbooks on Jung and Freud?

_The_ _Twenty-four_ _Hour_ _Mind_ explores these questions and more, shedding light on the many activities the brain carries out while we sleep. As it turns out, sleep is a very active time. In fact, our mind is active constantly during the alternating states of waking and sleep. What's more, the waking and sleeping mind are intimately bound in their functions — so much so that your health and even life can depend on their cooperation.

In these blinks, you'll learn

  * why not getting enough sleep can make you fat, 

  * how your dreams can actually alter your personal identity,

  * why you feel so cranky after a bad night's sleep and 

  * why getting a good night's rest should be one of your top priorities.

### 2. Sleep helps us stay physically and mentally balanced. 

We've all experienced the effects of a bad night's sleep: we're tired, can't concentrate and are generally unpleasant to be around. But why?

There's nothing puzzling about it: science can explain why a sleep deficit affects our body and mood.

For starters, deep sleep is an important part of the way we release tension. Our sleep is divided into two states that work together in a cycle, quiet sleep (NREM) and deep active sleep (REM), which both need to run through their cycles before waking in order to replenish the body and brain, a process which takes seven to nine hours.

Light sleepers — those who are REM-deprived — and people who don't sleep enough are prone to be more tense than people who get enough deep sleep. For example, employees who work the nightshift and sleep during the day don't sleep as deeply, and as a result show more signs of stress.

In addition, sleep reduces our negative emotions. One study showed that, of the 31 participants who were struggling through a divorce, 20 worked through their emotions in their sleep. In their dreams, they dealt with their anger and sadness, playing through scenes with their ex-spouses and slowly improving their "dream reactions," helping them to develop strategies to move on.

We take strong, unresolved emotions with us into our sleep, where our minds continue to work through them and help us to come to terms with them. For example, when we get upset by a nasty comment someone makes about our looks or weight, those emotions feel less powerful after a proper night's sleep.

Finally, our endocrine system functions best when we get enough sleep because it affects the hormones that control our appetite. In fact, most people who sleep fewer than six hours a night will experience an increase in appetite, and studies have even shown that you're 7.5 times more likely to be obese if you're a short sleeper.

> _"Sleep is a busy time."_

### 3. Dreams store our experiences and help us deal with emotional problems. 

Many people consider their dreams mystical — they scrutinize them looking for patterns, prophecies or some deeper meaning. In reality, dreams are simply a way to help deal with our everyday lives.

When we dream, we're evaluating the experiences we've had throughout the day, linking them to similar, older experiences and filtering out new, important information that should be stored in our memory. This information is then matched with an emotional experience from the past, thus creating a _dream_ _image_, which represents memories in relation to this new information.

For example, if our normal routine is somehow broken by a traumatic and unexpected event, such as a bad argument with a close friend, we'll revisit this experience in our dreams and match it with previous situations that caused the same feeling. This helps us contextualize these negative emotions so we can wake up with a more positive perspective on it.

In this way, dreams affect our waking world: they forge our perspective on life, influence our behavior and give us a sense of who we are. To better understand this, let's take a look at how our identity develops.

Our identity is formed by the organized structure of our thoughts. Whenever we have new emotional experiences, they either fit into that structure and reinforce what we already know, or they challenge that structure. In this case, we literally change our identity through the process of managing our emotions in our dreams.

For example, someone who feels like an outsider in a new group will take these uneasy emotions with him into his sleep, where his mind will try to come to terms with them while he dreams. When he wakes up, his thoughts, and thus his identity, will have been altered as a result of his dreams.

### 4. The waking and sleeping minds process different information, interacting to guide our behavior and keep it flexible. 

By this point in your life, you've probably realized that your experiences in dreaming and waking life are radically different. But why? It all comes down to the fundamental differences in how we process information when we're asleep or awake.

When we're awake, our minds receive signals from and respond to the external world almost automatically. In fact, we run largely on autopilot: once we've developed the behaviors that take us through the day, our perspectives, attitudes and habits — things we'd often ascribe to our personal identity — are produced without almost any conscious input.

And there's good reason for this: if our basic thoughts and behaviors required our undivided attention, we'd be way too stressed out to do much of anything! Just imagine trying to think through every decision you made throughout the day. It would be far too much to handle. When we're awake, we need these unconscious behaviors so we can pay attention to and interact with the outside world.

On the other hand, when we're asleep, our minds work with much less information. The sleeping mind concentrates on managing and balancing our emotional troubles, focusing only on the day's important emotional experiences and leaving out the rest.

These two unique, separate processes work together in order to help us function in this world: when we're asleep, our brain modifies the "software program" that derives our identity based on the experiences of the waking mind so that the waking mind can better cope with the world around it.

This is one of the things that makes sleep so incredibly important. We need sleep in order to help us negotiate and learn from unanticipated misfortunes so that we don't keep making the same mistakes again and again and suffer for it emotionally.

### 5. A malfunctioning waking-sleeping cycle can lead to severe emotional disorders. 

So far, we've learned a great deal about the importance of sleep for our waking life. But what happens if our sleeping and waking minds don't work together as they should?

For starters, if you have difficulties sleeping, it might be a sign that you're suffering from the beginning stages of depression. In fact, studies show that depression always starts with a malfunctioning sleep cycle, which results in crankiness and irritation. Depressed individuals stay in a sort of REM mode even while they're awake — in other words, they aren't able to leave the emotional state of their nighttime mind.

According to the author, depression can actually be combated simply by adjusting the REM time during sleep. She claims that 60 percent of patients who do this for just three weeks will be able to recover without any additional medical help.

Another dangerous consequence of a lack of sleep is _sleepwalking_, where people unconsciously perform actions that would normally be performed with full consciousness. The consequences of sleepwalking can be devastating.

For example, in severe cases of sleepwalking, sleepers can exhibit aggressive behavior — and sometimes even commit murder! One such case, that of Scott Falater, opened up a large debate about the question of our responsibility for acts committed while we sleep.

Falater was seen by a neighbor pushing a dead body into his pool before returning to his house. That body was Falater's wife, whom he had stabbed 44 times.

Falater was then arrested coming down the stairs from his bedroom, bloodstained and in pajamas, with no memory of what transpired after going to bed and before being awakened by the sound of police sirens.

Considering that Falater had no motive, was clearly in mourning over his wife, and had a history of sleepwalking and troubled sleep, the police determined that he killed his wife without knowing whom he was attacking when she tried to bring him to bed as he was unconsciously fixing a pump in the pool.

### 6. Too little sleep is not a minor problem: it leads to many of our world’s dominant health problems. 

Often, things like poor nutrition or a lack of physical exercise are blamed for many modern-world health problems. But this comes at the cost of overlooking one of the main causes of poor health: bad sleep.

The greatest risk of not getting enough sleep is obesity, which has reached epidemic proportions in the Western world. This was demonstrated in one study in which participants were allowed only a few hours of sleep per night: the decrease in sleep caused an increase in appetite due to malfunctioning hormone regulation, and resulted in weight gain within only a week!

So, if you're one of those people who are constantly sleep-deprived, you stand a great chance of putting on weight and increasing your risk of obesity.

Another result of sleep deprivation is diabetes. This was confirmed in one study in which 11 young men started showing early signs of diabetes after sleeping less than four hours over a period of only six nights.

In general, short sleepers are 2.5 times more likely to become diabetic than those who get the right amount of sleep.

Finally, sleep deprivation is not just a matter of health. It can also be a matter of life and death. In its most extreme form, poor sleep can actually cause death. Its effects can be so severe that many societies have actually used sleep deprivation as a method of torture.

For example, in one study, rats that were deprived of sleep quickly began to suffer from weight loss, fur loss and sore tails, and began developing aggressive behavior before finally dying after a three-week period.

One reason for this is that our body temperature goes down while we're asleep, thus allowing our system to cool down. If we're deprived of sleep, we don't get a chance to lower our body temperature, thus resulting in a metabolic burnout.

So make sure to get a full night's sleep: your life depends on it!

### 7. Final summary 

The key message in this book:

**Our** **dreams** **are** **much** **more** **than** **just** **a** **masquerade** **of** **strange** **images** **in** **the** **night:** **they** **keep** **us** **healthy,** **regulate** **our** **negative** **emotions,** **and help us** **cope** **with** **the** **difficulties** **of** **waking** **life.**

Actionable advice:

**Let** **your** **dreams** **do** **the** **talking.**

Our dreams are far less mystical than we might like to believe, so there's no reason to run to your bookshelf to pick up your dusty college textbooks on Jung and Freud in order to find some hidden meaning. In fact, don't try to analyze them at all! Just trust that they're doing what they do best: keeping your emotions in balance.
---

### Rosalind D. Cartwright

Rosalind D. Cartwright is a sleep researcher and the former director of psychology at the University of Illinois College Of Medicine where she studied the function of dreaming and REM sleep. She later opened a Sleep Disorder Service, where she diagnosed and treated patients with various sleep difficulties. In addition, she has authored many books, including _Night_ _Life:_ _Explorations_ _in_ _Dreaming_, _A_ _Primer_ _on_ _Sleep_ _and_ _Dreaming_, and _Crisis_ _Dreaming:_ _Using_ _Your_ _Dreams_ _to_ _Solve_ _Your_ _Problems._

