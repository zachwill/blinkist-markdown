---
id: 5750027f7bea3d0003c7158d
slug: the-tyranny-of-email-en
published_date: 2016-06-09T00:00:00.000+00:00
author: John Freeman
title: The Tyranny of Email
subtitle: The Four-Thousand-Year Journey to Your Inbox
main_color: 269ABD
text_color: 2185A3
---

# The Tyranny of Email

_The Four-Thousand-Year Journey to Your Inbox_

**John Freeman**

_The Tyranny of Email_ (2011) is about the rise and unprecedented power of email as a form of communication, and the profound effect it has on our lives. We have more access to communication now than at any other time in history; but not everything about email is positive.

---
### 1. What’s in it for me? Overcome the tyranny of email. 

Hand on your heart, how many times do you pick up your smartphone every day to check your email? If you knew the real number, you'd be shocked. Don't feel bad though — you're far from the only one.

Since its advent, emailing has completely changed the way we communicate and has made life easier in countless different ways. But, as you'll see in these blinks, all of this has come at a price. More and more people are feeling the side effects of never being more than a ping in the pocket away from total distraction.

So, how do we overcome this tyranny of emails?

In these blinks, you'll learn

  * what camels have to do with sending letters;

  * what spamming looked like before the internet; and

  * why you should only answer emails twice a day.

### 2. Only powerful institutions had access to long-distance communication in the early days of mail. 

Powerful groups of people have sought to control the flow of information throughout human history. For a long time, only governments and the Catholic Church had the tools to dispense information on a large scale.

Empires and governments have been using communication as means to consolidate their power since antiquity. In the Persian Empire, for example, messages could be sent by horse at the speed of 100 miles per day — as early as 600 BC! A new horse had to be switched in along the route at every postal stop.

The Abbasid Caliphate had over 900 postal stops in 860 AD, and Caliph Abu Jalbar Mansur once said in a speech that a faithful postmaster was just as important as the Chief of Police or the Minister of Finance.

Meanwhile, the Roman Catholic Church issued major doctrinal rulings or political decisions by disseminating scrolls all across Europe.

Common people, on the other hand, didn't have access to long-distance communication until the end of the nineteenth century. In fact, in in the year 1500 in Britain, only five to ten percent of the adult population could even read or write.

Sending mail was very costly, too. For just one letter, you needed paper (which was expensive at the time), silk to wrap it in, wax to seal it and access to an official sign. Sending a letter over 400 miles cost about a fifth of the average daily wage as late as 1800.

Things were further complicated by the fact that people didn't have fixed addresses. An American woman once addressed a letter to her brother by writing, "To my most noble brother, Mr. John Miles Breton, at Ye barber shoppe which lieth in the land hard against ye tavern of Ye Great Square in shadow of Ye Towne Hall." With addresses like that, it was often hard for letters to end up in the right place!

### 3. Communication styles changed as mail services became more democratic and accessible over time. 

Postcards quickly took off as a hot new form of communication after the first one was sent in 1871. Not everyone liked the change, however. In fact, _The_ _New York Times_ said the country was suffering from a postcard-sending "epidemic" later that same year!

Letter writing, postcards and mail services became much more accessible to common people in the late 1800s, thanks in large part to education reform and massive literacy campaigns in countries like the United States and the United Kingdom. Large governments also started investing more in their postal systems to build them up.

The British Royal Mail, for example, employed 42,000 people and had opened over 12,000 offices by 1873. The US Congress even experimented with using camels to deliver mail from Texas to California.

By that point, mail had gradually started to become a more regular part of people's lives. In 1840, the average American only sent three letters a year; by 1900, that number had risen to 69.

People used mail for various different purposes, too. Emigrant groups wrote letters to stay in touch with friends and relatives, and some people even started using it for fun. Members of the "Nonsense Correspondence Club", founded in 1903, sent random items to each other, like leftovers from their dinner parties!

Communication styles also changed as mail became more widely available. In 1871, _The New York Times_ published a complaint about what we now call "flaming" when it happens online: aggressive, violent insults sent out to people anonymously.

There were even early forms of spam! Some unlucky people got tricked into making false investments as early as 1887, when they received letters asking them to claim the estates of supposedly deceased distant relatives.

### 4. The telegraph provided the first means of real-time, long-distance conversation. 

Letters were becoming much easier to send at the turn of the century, but they still took a long time to get where they were going. Overall, communication was still fairly slow. But the telegraph changed that completely and irrevocably.

The telegraph was an incredibly powerful communication tool — so powerful that it changed the way people perceived space and time. For the first time, people and information in far away places were quickly accessible. Before the transatlantic cable was laid in 1870, it took five weeks to send news from London to New York. With the cable, it only took a matter of hours.

As the world grew smaller and communication became easier, even enemies started making long-distance small talk with each other. Generals in the American Civil War of 1860 sent each other short, provocative messages like, "I see your condition through my telescope" and "We have intercepted your supplies. Give in like a good fellow."

The introduction of the telegraph even brought on the first era of _information overload_. People now had unprecedented access to news and communication. In fact, the second message sent by Samuel Morse, the inventor of the telegraph, was, "Have you any news?"

William James, the famous philosopher, coined the term _Americanitis_ to describe the perpetual fear of not being on time, and nervousness about missing out on something — all sorts of stress caused by the telegraph.

Newspapers also started printing larger editions as the telegraph granted people more access to news. Soon they were printing every day instead of once a week, covering news from all over the world; but not everyone liked this development. The Michigan newspaper _Alpeno Echo_ even shut down its telegraph service because it felt it was becoming the voice of the world, rather than the record of its community.

### 5. Emails are fundamentally different from any prior form of communication. 

The telegraph peaked around 1945, when about 240 million telegrams were sent per year. In 2007, the number of emails sent globally hit 35 trillion — a figure more than 10,000 times higher.

So how did email become so successful and what does it mean for us?

Email has made communication much easier and faster than ever before. Not only is it instantaneous, it also costs virtually nothing!

Before email, people had to write addresses (or descriptions of a location!) on paper or envelopes and send them out individually. Now, we can send messages to people, or even large groups of people, with just one click. You can forward a message or share a piece of news without thinking about it at all.

Email is also free. Anyone with a computer or phone and access to the internet can use it anytime, from any location. Telegrams, on the other hand, used to cost quite a lot. In 1860, it cost the equivalent of $18 in today's money to send a message from New York to London.

Communication has become a lot more efficient — but it might actually be _too_ efficient.

Now that it's so easy to send and receive messages, we're often expected to reply immediately. Online messages constantly interrupt our day. In fact, one 2006 study found that the average American worker was interrupted 11 times per hour, resulting in an overall loss of $600 billion.

Another danger of constant email access is that it can create a never-ending to-do list for you. When you can receive new tasks or information at any second, your friends, family or boss might expect you to be available at all times, always ready to change your plans.

It goes without saying that email is efficient. But it creates a lot of stress for us too.

### 6. Emailing is highly addictive and alters your brain chemistry. 

Email is a digital form of communication, but it still generates problems in the physical world. In fact, email functions a lot like a drug. Email is addictive and causes behavioral problems just like any other addiction.

When you get a positive email, you feel a sense of validation and recognition. This, in turn, induces you to keep checking your inbox again and again so you can get that feeling over and over. The resulting addiction is powerful: in one survey designed to measure email response time, the average time was only 104 seconds. Seventy percent of the participants responded in just _seven_ seconds.

Email withdrawal can cause distress and anxiety, too. In 2007, one real estate agent even said that his "blood ran cold" when the Blackberry network went down for a few hours.

Our brains simply aren't designed for the new challenges presented by email. Brain imaging has proven that repetitive behavior, such as compulsive email-checking, causes imbalances in dopamine levels. Your dopamine increases when you check your email, so you start to crave it.

Compulsive email-checking also has a negative effect on your memory. When you try to do too many tasks at once, your brain's attention is directed away from your _hippocampus_, which is responsible for storing information. It focuses instead on your _striatum_, which deals with repetitive tasks. This is why it's harder to remember what you were doing if you were multitasking.

Emails also send very little information to our _prefrontal cortex_, the part of the brain responsible for empathizing with others and addressing them in an appropriate tone. So, they can generate a lot of misunderstanding. The prefrontal cortex is particularly vulnerable and impressionable during adolescence, so young people who develop email addictions may also develop permanent problems with communication.

### 7. Email disrupts our everyday lives. 

Our email habits have a major impact on our emotions and everyday behavior. They even change the way we interact with our families, friends and children!

Why? Because email is a major disruption in our daily lives. The speed, frequency and anonymity of email can leave you feeling exhausted at the end of the day. Moreover, emails are depersonalized, draining and can diminish your sense of accomplishment. According to psychologist Christina Maslach, these are the symptoms of _burnout_.

We're also reading fewer books these days because of email, and our eye movements have changed; we're now prone to skimming instead of reading. In fact, the percentage of adults scoring "proficient" on prose reading scores dropped considerably between 1992 and 2003.

Email has an impact on our sleep patterns as well. Studies have shown that people are now sleeping less than they did 20 years ago, largely because of our need to always be connected.

Our social lives and relationships have also suffered in the modern age. Madonna even admitted in 2008 that she and Guy Ritchie slept with their smartphones under their pillows, so they could grab them whenever they needed. "It's not unromantic," she said, "it's practical." The pair is now divorced.

You also spend less time with your loved ones when you're always looking at your phone or computer. Bruce Mehlman, the Assistant Secretary of Commerce for Technology Policy under George W. Bush, even claimed he could play with his son while checking his email.

He always had to make bad trade-offs, however. He once destroyed his son's Lego figure in a play fight so he could answer an email, leaving his son to rebuild the figure on his own.

### 8. The tyranny of email can be overcome. 

We've seen that email is highly addictive and alters our brains. Even so, it's still possible for us to overcome this tyranny. Let's go over some tips for controlling your email so it doesn't end up controlling you.

First off, you need to stay in control of when you're actually using email. Try checking your email only a set number of times per day, or only using it during work hours. Figure out what amount of email is healthy and necessary, and cut out any excess use of it. In this way, you avoid unnecessary interruptions and can react to a long email thread without having to check every single message as it arrives.

You make matters worse if you check your email just after you wake up or just before you go to sleep — it means you're subjecting your mood to other people's messages at key points in your day. So, aim to have a few email-free hours on either end.

We also need to change the way we write our emails. Give your email an efficient subject line that summarizes the most important parts, and be sure to keep your emails short and only write information that's absolutely necessary. Then, encourage others to do the same. Let them know you don't always need a "Thank you message" and keep your inbox free of any messages that aren't useful.

Strive to find a balance between email and other, offline tools for planning and communication. Phone calls and meetings are better than email if you're discussing something sensitive. Group discussions should be held in person, as group emails can become endless and disorienting.

Try rearranging your desk, too. Desks are usually organized around computers, but make sure you still have enough space to write on paper and make phone calls comfortably. Don't cut paper and pens out of your life!

### 9. Final summary 

The key message in this book:

**No other form of communication has affected our lives as profoundly as email. It offers free, instant access to communication from anywhere with internet access. Though its benefits are manifold, email has its downsides too. It's addictive, hampers our attention span, causes us stress and is changing the nature of our relationships in some negative ways. Email is powerful, so make sure you control it — or else it will control you.**

Actionable advice:

**Use your voice when you need to discuss something lengthy and complicated.**

Nuances can be lost in emails, creating unintentional confusion or ill feelings. If you need to discuss something important, it's best to do it in person. If that isn't an option, make a phone call — otherwise, emails might actually be counterproductive.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Pomodoro Technique_** **by Francesco Cirillo**

_The Pomodoro Technique_ presents a simple yet effective method of structuring your workday. This method helps to overcome your lack of motivation by cutting large or complex tasks into small, manageable chunks. Using these techniques, you will gain more control over your work, thus making you a more effective worker and work more rewarding.
---

### John Freeman

John Freeman is an author, poet, book critic and the former editor of _Granta_, the renowned literary magazine. His work has been featured in several publications, including _The_ _New York Times Book Review_, _The_ _Los Angeles Times_, _The_ _Guardian_ and _The_ _Wall Street Journal_. He won the _James Patterson Pageturner Award_ in 2007.

