---
id: 570970a18b5d6e0003000066
slug: the-ultimate-introduction-to-nlp-en
published_date: 2016-04-12T00:00:00.000+00:00
author: Dr. Richard Bandler, Alessio Roberti and Owen Fitzpatrick
title: The Ultimate Introduction to NLP
subtitle: How to Build a Successful Life
main_color: 23AEAD
text_color: 187A7A
---

# The Ultimate Introduction to NLP

_How to Build a Successful Life_

**Dr. Richard Bandler, Alessio Roberti and Owen Fitzpatrick**

_The Ultimate Introduction to NLP_ (2012) offers a fascinating primer of Neurolinguistic Programming _,_ or NLP, a novel approach to the ways your thoughts and language can "program" your emotions, behavior and communication. When you learn to master NLP, you'll connect better with the people around you, have a healthier outlook on the future and lead a happier life, too.

---
### 1. What’s in it for me? Learn how to program your thoughts and feelings to enjoy life more. 

Programming is a word we tend to associate with computers and technology; it can be used for anything from complex operating systems, to smart life-hacking apps with summaries of books, to incredibly detailed computer games. But wouldn't it be great if we could program ourselves in a similar way?

It turns out we can.

_Neuro-Linguistic programming_, or NLP, is a way to change how we act and behave. It's based on the connection between the neurological processes in your brain (neuro), the language you use (linguistic) and your behavior, such as your fears or beliefs (programming). Each of these parts can be altered, ultimately changing the way you act, feel and think. So, let's look at how these processes work and what techniques you can use to program yourself.

In these blinks, you'll discover

  * how emotions resemble yogurt;

  * what the meta model is; and

  * how your breathing can help you connect with other people.

### 2. Everyone creates a mind map they use to understand the world. 

What causes humans to disagree with each other? The answer is simple: we see the world in different ways.

Everyone creates a map in their mind that they use to make sense of the world. This concept is central to the idea of _Neuro-Linguistic Programming_, or _NLP_ : a person's understanding of the world is based on their internal map, not the actual world around them. Your personal map is a composite of your ideas, values, knowledge and preconceptions.

When you create your map, you employ three basic processes.

First, you delete some of the information around you. Just think of a city map: it doesn't have cars or trees on it. Your personal map is also missing certain things, but they can be much bigger. Have you ever walked down a familiar street and noticed a shop for the first time? That's because your map didn't include the shop before you noticed it.

Second, you generalize. Physical maps generalize too: roads are all drawn the same way and water is always the same shade of blue. Making these generalizations on your personal map can get you into trouble, however.

Sometimes it's good to generalize information, like "hot things shouldn't be touched," for example. However, if your partner cheats on you and you become paranoid that _everyone_ will cheat on you, this can become a serious problem.

The third thing we do when using our maps is to distort information. A map of a city distorts information because it's flat and smaller than the actual city. On your personal map, you might distort information by attributing meaning to things when you shouldn't.

Imagine one of your colleagues doesn't greet you one day. Your map might tell you to believe that they're angry with you — but they might just be late for a meeting.

### 3. Updating your map now and then is a key part of staying happy. 

Conflict arises when people's mind maps have them interpret the world differently, which is exactly why it's important to expand your map as much as possible.

Expanding your map is vital to living a happier life. When you broaden your map, you're able to view the world from more perspectives. Your relationships also improve when you're better at empathizing with other people's feelings and opinions — and you'll feel better, too.

So it's a good idea to check in with your map every now and then to make sure it's up to date. Much like ordinary maps, mind maps can become outdated. When new roads are built or others are removed, a city map has to be changed; your personal map works the same way.

Two common problems arise when you don't keep your map up to date.

First, you might imagine that you have constraints or limitations when you really don't. Some people limit themselves out of fear, for instance. Maybe they don't travel because they're afraid of flying.

These kind of fears only exist in our heads. We're only born with two fundamental fears: a fear of loud noises and a fear of falling. All other fears are learned — which means they can be unlearned!

Your map might also interfere with your life by making you believe that something works when it doesn't. This often leads people into counterproductive behavior.

Imagine your business partner betrays you. Your map might then tell you that it would be safest to never trust any business partners again. However, if you follow this assumption, you're sure to always work alone. Do your best to avoid this sort of thinking; instead, reconsider how you choose your business partners.

### 4. You can alter your emotions, like happiness and fear, by changing the way you think. 

Have you ever thought about something scary and suddenly felt anxious? Not surprisingly, your thoughts have a major impact on how you feel, both physically and emotionally.

Our thoughts are like images of a film reel — and they can profoundly alter our emotions. And, sticking with the same analogy, you can change your emotions by adjusting the colors, brightness or noise level of the images in your thoughts. Think of it like watching a film in a cinema instead of at home: it's the same film either way, but the experience is better in the theater.

So you can choose to minimize your negative feelings or amplify your positive feelings. But how?

You can intensify a feeling by _anchoring_ it to another form of stimulus you can control. Think of a happy experience, for example; it should be so good that you smile when you remember it. Now, imagine this experience is attached to a lever labeled "fun," and pull the lever.

This might feel silly, but when you come to associate that lever with a happy feeling, you can imagine pulling it whenever you're feeling down. Your brain remembers the happy sensation and you'll start feeling better.

You can also use anchoring to quell your fears. One of the authors had a client who was able to use anchoring to overcome his fear of talking to women. He was afraid to approach them, but he was very into ski jumping, a sport where one skis off a mountain and flies off a ramp into the air at high speeds.

The author helped the man anchor that feeling of excitement to the experience of talking to women. Once he leveraged the happiness he got from ski-jumping, he was much more comfortable with the opposite sex.

### 5. Your feelings have an impact on how other people feel. 

Have you ever noticed that happy people improve your mood, while depressed people bring you down?

Emotions are contagious. Other people's emotions influence you, and your emotions influence other people in turn.

One of the authors conducted an experiment on this phenomenon using yogurt. He separated a yogurt culture into two containers and attached one of the containers to a device that could measure the yogurt's electrical activity. Then he poured milk, which yogurt "eats," over the half that wasn't attached to the device.

Interestingly, when that half received the milk, the other half of the yogurt somehow picked up electrical activity. It "knew" the other half was getting the milk added to it.

The researchers tried putting barriers between the yogurt, such as metal, wood or even electromagnetic fields, but the results were always the same.

The author then placed a fish tank full of a different kind of yogurt between the first two containers of yogurt — this stopped the transmission of electric activity between them.

The researchers asked the author how the first batch of yogurt could "tell" that the second batch was there. His reply was simply, "yogurt knows yogurt."

People work the same way: people know people. We spread our feelings like yogurt spreads electrical activity.

Your mental state has a significant impact on how people interact with you. So, if you want to change someone's feelings, you have to change your own feelings first. You can't expect people to feel happy around you if you're feeling depressed. If you're in a bad mood, you'll spread it to others around you.

So, work on improving your own mental state through anchoring before you try to help with anyone else's. You can't make other people happy until you're happy yourself.

### 6. You communicate better when you match your conversation partner’s body and verbal language. 

Have you ever clicked with someone as soon as you met them? If so, it was probably because your communication styles matched, both verbally and nonverbally.

We naturally adjust our communication style to suit the person we're talking to; it's called _matching_. Matching is an important part of communicating effectively.

You match with someone in different ways, such as through your breathing. You build a strong connection with another person when you breathe at the same rate as them.

Synchronized breathing is more powerful when you do it subtly, so don't go overboard. Matching your breathing should eventually become an unconscious behavior, like walking or driving a car through a familiar neighborhood.

You can also influence people with _pacing_ and _leading_. Humans naturally mimic each other's body language, so when you have someone's attention you can influence their behavior. The author once attended a presentation in which, at one point, the presenter took a deep breath — and nearly all of the audience repeated the action.

A person's communication style also reveals a lot about the way they think. People tend to fall into one of three categories, depending on which sense they base their thinking on.

Some people use visual imagery to describe the world and explain their thoughts. They use phrases like "It _looks_ like" or "I _see_ it."

Others rely more on their ears for understanding and communication. They say things like "That _sounds_ okay" or "Stop _telling_ me..."

The third group communicates and understands the world through their feelings and physical sensations. They use phrases like "I feel great" or "I don't grasp the idea."

You can strengthen your bond with someone by matching with them on their speaking style. If you mismatch, however, by using visual language when they use auditory language, you'll distance yourself from them.

> _"NLP is largely about how to pay attention to what's going on around you."_

### 7. Gain a better understanding of a person by asking questions to find out what their generalizations are. 

We've looked at how our internal maps cause us to make generalizations about the world around us. But we also make generalizations about people, which can be a problem for communication.

The _meta model_ is a good tool for avoiding this, and challenging other people to expand their maps as well.

The easiest way to understand the meta model is through an example. Imagine you come home from work and your partner says, "There's been an accident." What exactly do they mean? Was it a car accident? Did they burn dinner? Is someone hurt? Ask questions about the situation using the meta model: What? How? When? Where? Who? Each answer deepens your understanding.

You also help enrich other people's maps when you use the meta model. Imagine a person says, "Everybody hates me." This, of course, is an absurd generalization and there's no way it could possibly be true. Even so, this kind of generalization can completely undermine a person's happiness.

So if a friend says something like this, ask him who "everybody" is. After he gives a more specific answer, ask how he knows everybody hates him, or what makes him think so. Ask if there are any other possible interpretations of others' behavior.

The more you question this friend's beliefs, the more likely he is to doubt them. If you do this the right way, you may be able to guide him into thinking more positively about himself. He might start to question and rework his own internal map.

NLP doesn't only help you overcome your own negativity, it can help you bring out positivity in others, too.

> _"There is so much confusion and misunderstanding in business, and in life, because people fail to clarify what the other person means."_

### 8. Creating a wonderful life starts with learning how to feel good. 

Do you ever make yourself worried about the future by dwelling on the problems you've had in the past? Negative thoughts are powerful — so don't let them control your future!

Not letting your past control your future is a crucial aspect of maintaining your happiness. The author once did an exercise related to this in an NLP seminar. First he told the audience to breathe slowly and steadily, then he asked them to think about five wonderful events from their past that they could remember vividly.

Then the author asked them to think of a future event they were worried about, and imagine it as the sixth event in the chain. This helped them feel more optimistic about it. Once they associated the future with happy memories in their past, they were more at ease with it.

Another good way to overcome your negative feelings or bad habits is to make yourself feel stupid (strategically!).

The author used this method to help a client who was endangering his life by eating too much chocolate. Instead of confronting the addiction head on, the author put some chocolate on a chair and had the man look at it. Then he said that the chocolate was smarter than him, and had more willpower and self-control than he did.

The man started to feel foolish about his desire to eat the chocolate. Once he associated that feeling with his craving, he didn't crave the chocolate anymore. After all, no one likes to feel stupid.

Readjusting your thoughts isn't only about feeling better about the future and overcoming cravings; when you have a positive outlook, you live a happier and more fulfilling life.

> _"If you begin to look at things as if they're difficult, they will be."_

### 9. Final summary 

The key message in this book:

**Taking control of your life starts with taking control of your mind. Most problems are rooted in your thoughts, whether it's stress, fear, anxiety or a lack of self-discipline. Start making meaningful changes in your life using the principles of Neurolinguistic Programming. Your thoughts are powerful — use them to your advantage!**

Actionable advice:

**Ask thoughtful questions.**

If someone tells you they're depressed, don't ask "Why?", ask "How do you know?" If someone tells you they're struggling with shyness, ask them "What would you do if you weren't?"

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Activate Your Brain_** **by Scott G. Halford**

Despite all the scientific breakthroughs made in recent decades, we still don't fully understand the human brain. However, we have discovered some important neuroscientific facts. Backed by research, helpful examples and exercises, _Activate Your Brain_ (2015) shows you how you can use this knowledge to make the best use of your brain and live a more fulfilled and mindful life.
---

### Dr. Richard Bandler, Alessio Roberti and Owen Fitzpatrick

Dr. Richard Bandler is the creator of _Design Human Engineering_ and the co-creator of NLP. He's written several books, including _Get the Life You Want_ and _Make Your Life Great_.

Alessio Roberti is a Master Trainer of NLP and the director the largest NLP organization in the world. He has trained presidents, executives and the CEOs of some of the world's major companies.

Owen Fitzpatrick is a psychologist, author and international speaker. He became the world's youngest licensed Master Trainer of NLP at the age of 23.

