---
id: 537c616b3465300007210000
slug: the-ultimate-sales-machine-en
published_date: 2014-05-20T08:59:15.000+00:00
author: Chet Holmes
title: The Ultimate Sales Machine
subtitle: Turbocharge Your Business with Relentless Focus on 12 Key Strategies
main_color: EF3930
text_color: EF3930
---

# The Ultimate Sales Machine

_Turbocharge Your Business with Relentless Focus on 12 Key Strategies_

**Chet Holmes**

_The_ _Ultimate_ _Sales_ _Machine_ offers twelve key strategies for improving how we do business, as well as other methods and tools to help you work smarter and more effectively in all aspects of your business, from management to marketing and sales.

---
### 1. What’s in it for me? Learn the twelve strategies that will take your business to the next level. 

Do you ever feel like you aren't getting the most out of your sales team? Or that there must be something you can do to increase the effectiveness of your advertising campaign but you just can't figure out what?

If so, you're far from alone. In fact, most businesses would benefit from some fine-tuning in areas like organization, hiring and sales strategies. _The_ _Ultimate_ _Sales_ _Machine_ will help you to give your own business a "tune up" by helping you forge the best strategy possible in order to maximize sales and retain loyal customers.

In these blinks, you will learn which of the many available potential clients you should set your sights on, as well as how to grab their attention and close the deal.

In addition, you'll learn how to strengthen your front lines by assembling a superstar sales team through effective recruitment, training and incentives.

In these blinks, you'll discover

  * how your negative thinking is affecting your bottom line,

  * how playing it cool can help you to sneak past the gatekeeper,

  * why you should target CEOs in their own neighborhoods,

  * why it's best to teach your customers something new and

  * how a cheap flashlight pen could seal the deal for a life-long business relationship.

### 2. Ensuring the highest standards for your staff through repeated training can help your business run effectively. 

The lumberjack may not seem to bear any relation to sales, but consider the following example: a lumberjack's job is to cut wood every day, so if he wants to cut more wood, he could add a few extra hours to each workday and thus boost his productivity.

Or: he could spend a few extra hours sharpening his saw _one_ day, which would enable him to cut more wood every day after that without having to work the extra hours.

You, too, can follow the lumberjack's example and increase your organization's productivity by "sharpening" your team through training.

Training enables you to set and raise the standards of staff performance and be proactive. Most obviously, training shows your employees where you want their focus and what areas of performance they should improve upon.

Furthermore, training helps you proactively prepare for difficult situations before they arise.

Would you, for example, want to perform first aid for the first time _ever_ if someone's life depended on it? Of course not! You'd want to be confident in your first aid skills before you put them to use, and the only way to do that is by training.

In fact, the same is true for your business. If, for example, you want your team to effectively handle an important — but disruptive — client, then you must train them in managing this particular situation first.

For training to be effective, it has to be repetitive so that important concepts are constantly reinforced and reiterated.

For example, if your customer acquisition training is only a one-time, hour-long course, then you might only learn the basics. But, after a couple of weeks, you'll probably have forgotten most of what you learned!

However, follow-up lessons that continue for a few weeks give you the capability to actually _use_ and _reuse_ the skills you learned, even months after.

Like the lumberjack, cutting wood becomes much easier when you keep your tools sharp. With regular training efforts, your staff will learn how to deal with tricky situations more effectively and proactively.

### 3. Use workshop training to develop the best strategies for your sales team. 

Imagine this scenario: a salesman from a carpet-cleaning company visits an old lady's house. She asks him to take her dirty rug to be cleaned and, as he rolls up the rug, he notices that the the pad underneath is dirty and threadbare. Surely, it needs replacing, but the salesman doesn't want to come across as pushy, and so he doesn't make the pitch to sell a replacement rug.

This sales approach sure won't bring in many customers. To avoid lost opportunities like this, you should ensure your team is well-trained.

The best kind of training for sales teams is _workshop_ training, where groups of professionals are brought together to learn new skills. In fact, workshops offer participants the perfect environment for sharing ideas and opinions with one another.

In this way, that salesman who didn't want to appear pushy may learn some tactics from other salespeople for up-selling without being rude. Because workshops make use of collective knowledge, you can gather ideas to build a great sales strategy for your entire team to follow.

Furthermore, workshop training is best forum for testing and improving sales strategies.

One way to test out new ideas is through role-playing. Have your sales team act out the greatest pitches or performances. Once the team has decided on the most effective approach to address that old lady, have someone role-play as _her_ and try it out.

If it feels like it could work, give it a try in the real world by having one of your top salespeople test out its effectiveness in the field.

If it's successful, then make sure this new valuable strategy is taught to the rest of your team at the next workshop.

In this way, training gives your sales team the opportunity to try out techniques in the workshop _before_ having to react to unfamiliar situations in real life, therefore increasing their chance of success.

### 4. By educating your potential customers, you can reach more people and sell more goods. 

Imagine this scenario: two furniture stores open in the same city and, within four years, the first has grown by 10 percent annually and remains a single-store location. The other store, however, grows into a six-store firm within the same time span. What did they do differently?

Furniture Store 2 engaged in the more effective marketing approach: namely, _education-based_ _marketing_. In other words, rather than just trying to sell its products, Furniture Store 2 actually educated its customers about everything involving its brand.

For example, while Furniture Store 1 only tries to sell a couch to those looking for couches, staff at Furniture Store 2 tries to sell the _whole_ _store_. They tell prospective customers about the store's history, their dedication to customer service, their knowledge in furniture craftsmanship and about how all these things directly benefit the customer.

Doing this will not only help you sell couches but also create brand loyalty: people will feel connected to your brand, and your salespeople will be viewed as trusted experts in their fields.

In addition, education-based marketing allows you to broaden your scope and target as many people as possible. For example, imagine you had the opportunity to make a sales pitch to an entire stadium packed with potential customers. How could you do this while maintaining everyone's undivided attention?

It wouldn't be easy considering that when you make your sales pitch, only about 3 percent of the population is interested in buying now and only 6 to 7 percent is open to the idea of buying at all. The vast majority, however, is totally uninterested.

Consequently, you'll lose 90 percent of your audience if your sales pitch only focuses on your couches.

However, if you inform your audience about things that directly benefit them, such as "reasons why choosing adequate furniture enhances productivity," the vast majority of them — not just those interested in couches — will start listening.

By educating rather than selling, you can attract the interest of a much larger audience and thus increase your success!

### 5. Hiring superstars: accelerate your growth by only hiring top performers. 

Have you ever had to work with someone who didn't fit into your team? This isn't just bad news for them; in fact, the average "bad hire" costs a company a whopping $60,000.

It's therefore vital that your business attract and find the right candidates — which is especially true for your sales team. So how can you ensure that you acquire the best people for the job?

You have to recognize the qualities that characterize sales _superstars._

Essentially, sales is all about personality, not background:

  * Sales superstars are _dominant_, i.e., they have the strong ego necessary to close as many deals as possible without taking rejections personally.

  * They also exhibit _high_ _influence_, i.e., they love people, are empathetic, fast, energetic, persuasive and communicative, and therefore bond better with prospective customers.

So when you're looking for sales associates, what you're really looking for are strong egos with high influence and _nothing_ _else_. Other factors, such as age, industry knowledge or previous roles, are not worth considering.

Once you know what constitutes the superstar personality, you then need to discern who actually has that personality. But how?

One way is to pre-screen candidates via short phone interviews to weed out people without strong egos or high influence: test them by forcing them to them sell themselves as superstars.

You should grill them relentlessly and attack their ego during the interview; sales superstars will be prepared to fight back! You can also detect if they have high influence by seeing whether they share childhood stories or other important life experiences during the conversation.

And, once you've found your superstar, make sure they're adequately rewarded. This means offering a mostly performance-based salary, which will motivate them to close deals.

In addition, pay should be structured based on the performance of the highest-performing salespeople in order to ensure that everyone reaches their maximum potential.

Now that you have some strategies in your tool belt that will help you bolster your team, you need to put that team to good use. The following blinks will show you how you can win the attention — and business — of your dream clients.

### 6. Maximize sales by targeting decision-makers in big companies. 

Imagine you're a company that sells office equipment. To attract clients, you'll probably advertise your services by sending mail to all the companies in your area.

However, this strategy isn't very productive. In fact, chances are that only a small minority of people will actually contact you. So what can you do to improve your advertising?

First, rather than targeting everyone, target the _best_ _buyers_ in your industry instead. Best buyers buy more, buy faster and buy more often than other buyers, and are therefore the most attractive buyers. For example, selling to larger businesses with 100 to 300 employees is obviously more valuable than selling to smaller companies since the larger business will likely buy more.

Then, refine your focus on larger firms that have older computer systems. These companies are most likely to buy from you in high volumes, so you should target them specifically with your advertising.

Next, sharpen your focus even more by targeting the specific staff responsible for the purchasing decisions of your target companies.

For example, while the research software you're selling might ultimately be used by librarians, they're likely not the ones who make the actual purchasing decisions.

However, if you contact the managers or directors of the library and explain to them how your research software will enhance the librarians' work processes and productivity, the likelihood of closing a sale increases.

Finally, adopt the _best-neighborhood_ strategy. This means sending special direct mail to the neighborhoods where the best buyers live.

For example, one real estate broker focused her efforts on a "dream neighborhood" with 2,200 upscale homes. Each month, she sent a simple mailer with a list of homes sold in this area and their selling prices. This way, those interested in selling their homes could see first-hand how much they stood to profit.

And it paid off: the commission on just a single, million-dollar home more than than paid for the $1,320 per month spent on fliers!

### 7. Streamlining your marketing and educating people will put you in the spotlight for potential customers. 

Many companies spend loads of money marketing their various products. Yet, their marketing strategies often aren't connected to one another. Instead, they use isolated campaigns without any formal unity, such as radio ads, direct mail, public relations efforts or internet advertising.

This kind of marketing strategy won't give you the best bang for your buck, so you'll need to find another way, for example:

Start by coordinating _all_ your marketing activities. This means sharing information and strategies across all your marketing campaigns in order to increase their effectiveness.

For example, if your public relations team has compiled a list of your company's stellar accomplishments for press releases or newspaper articles, you should distribute this valuable information across your teams. For instance, your sales team could use this list when interacting with clients to demonstrate how trusted and outstanding your company is!

By creating a more unified marketing message, you also increase the likelihood that people will see your messages more often, and in many different media.

Additionally, you can utilize education-based press releases to improve your marketing campaigns.

In fact, by producing press releases with an educational element, you will garner more media attention and thus greater access to potential customers.

One example of this can be found in the strategy of two women in Burlington, Vermont, who run a small farm offering fruits with cartoon characters painted on them in order to entice children to eat healthy food.

In order to spread awareness of both healthy eating _and_ their service, they write regular press releases with hard facts and study findings about obesity in the United States, along with the benefits of healthy eating habits.

Because of the regularity of their press releases and all the interesting facts for journalists to write about, their company benefits from growing media attention. And because their marketing provides interesting and relevant consumer information, potential customers find them more trustworthy!

### 8. Make your sales pitches more memorable by using compelling visuals and headlines. 

If you plan to pitch your services to potential clients, you'll likely be asked to make presentations to audiences at conferences and marketing events. Your success on the podium depends on how compelling your presentation is.

Unfortunately, though, the majority of the audience will only remember about 20 percent of what you said. That's why you need to find other ways to hold their interest.

One way is by using visual aids. It's in our nature as humans to only remember 20 percent of what we hear and 30 percent of what we see; however, we remember 50% of what we both see _and_ hear. You should accommodate this by ensuring that your audience gets both visual and auditory messages.

It's a good idea to make use of colored graphics to deliver your message. Color images can better attract our attention the than black-and-white ones, and can affect the viewer in different ways depending on the color.

For example, a software company targeting lawyers displayed an infographic showing the significant rise in the number of lawyers in the United States. To highlight the drastic growth between two of those years, they highlighted the highest number by making it larger, using a red font and circling it.

This created a clear signal to the audience that competition in their field had increased _and_ that that fact was important — using colors made sure the point stuck.

Additionally, every visual in your presentation should have a catchy headline summarizing its most important points.

For example, instead of using plain titles for your slides like "State of the Industry" or "Five Trends in Our Industry," make them more interesting and informative with titles like "State of the Industry — How Times Have Changed" and "Five Trends That Could Ruin Your Business or Bring It to New Heights."

In doing so, you convey a clear message and make your audience aware of why it's important to them.

### 9. Make sure best buyers know who you are by persistently and personally communicating with them. 

Unfortunately, your dream clients are not out there just waiting for you to make a sales pitch. In fact, they probably have no idea who you are. So how do you create awareness and interest in you and your company?

One method is to send letters and gifts to your _Dream_ _100_, i.e., the clients you wish you had, but don't. In order to attract their attention, send them a letter every two weeks offering them something they'll value. For example, if you're a media consultant, you could offer them a free web seminar about industry trends and challenges in the media industry.

Accompany these letters with small gifts, like pens and keyrings, that match the message of your letters. For example, if the future of the media industry seems dark and obscure, the pen flashlight might assist your message best.

After sending your letters and gifts, follow up with a phone call to the decision-makers, e.g., CEOs.

Getting in contact with CEOs isn't always easy: you'll have to circumvent the _gatekeepers,_ such as personal assistants, who decide which phone calls to put through to their bosses.

One way to do this is by acting like the CEO is familiar with you or your company.

For example, when gatekeepers put up walls and ask about the nature of your call, play it cool and say "Just tell him it's John" or whatever your first name is.

When faced with such confidence, assistants will likely go straight to their boss and tell him it's you on the line. Of course, their boss won't know who you are and will probably ask for more information.

When this happens, respond with something like "Hm. Just tell him I'm from Company ABC. That ought to jog his memory."

As long as you continue to sound confident and important, gatekeepers will eventually cave and put you through.

### 10. Build rapport with clients to ensure that they buy your services. 

Although you've now made contact with your dream clients, you haven't made the sale just yet. They might still hesitate even after you've convinced them that it's in their best interest to buy.

So what can you do now to seal the deal?

Well, you should lay the groundwork by establishing rapport with your clients, which will help you close more sales later. Be sure to get to know your customers and let them get to know you; it's a lot harder for competitors to steal your clients if your clients are also your friends!

One way to build this rapport is to create opportunities in which you can bond with your client, such as parties and trips.

But creating this emotional bond with clients requires trust. Often, you can achieve this if the prospective customers trust you as an expert. For example, if your telecommunications company sells high-tech equipment, then you can invite potential clients to seminars in order to educate them on how equipment matters in their industry. Doing so helps you bond with your clients, who will regard you as an expert on equipment and trust your advice.

You also shouldn't be scared of helping clients make a decision to close the sale.

Once you've identified the buyer's need and created a desire to buy, they might still hesitate or object. However, if you truly believe that your product or service will offer a significant value to your prospective clients, then you have a _moral_ _obligation_ to help them decide.

Imagine that a couple has been looking for a home theater system for four months after having visited several stores. Don't you think they'll be happier if you sell them the system that you actually think is best for them?

If you think it's the right choice for your clients, don't be scared to push them to buy now!

### 11. Keep your clients forever in order to increase your profits. 

Getting a sale is cause for celebration, but if you want to build the _Ultimate_ _Sales_ _Machine_, your job's not over yet: you have to follow up post-sale to keep your clients. But what's the best way to go about it?

They key is to develop superior follow-up procedures that will produce more business with your client.

After the sale, your clients will be aware of you, but it's like they say: out of sight, out of mind. To make sure they don't lose sight of you, grab their attention by writing them letters, calling, sharing amusing stories, or hosting an event or party so that you're always at the back of your clients' minds.

Once you've closed the sale, send your first follow-up letter quickly and make sure it's personal. Try following this formula:

  * Start with something personal: if they told you a funny story about their dog, be sure to mention it in your note.

  * Next, compliment them while staying focused on the benefits you provide. For example, you might write something like: "You really seem to understand the challenges your company faces in this competitive industry. It is clear that our research software will help keep you ahead of the competition by reducing research costs and increasing productivity."

  * Finally, close your letter with another personal note. You could write something like: "Once again, it was great doing business with you. I have some more ideas about other challenges you are facing, which I am sure will be of interest to you."

You should never apologize for taking up their time, as it implies that you don't believe that your services actually benefit your clients. In addition, focus on your clients' perspective and how you can offer solutions to their specific issues without focusing on yourself.

It may seem like some work, but think about it like this: it takes _six_ _times_ _the_ _effort_ to acquire a new customer than to sell additional services to an existing one.

Now that you have the tools to turn your business into the ultimate sales machine, you have to develop the mindset necessary to operate it.

### 12. Reprogram your mindset to follow your goals with iron discipline. 

Picture yourself wandering through a busy train station. There are a million things happening all around: advertisements vie for your attention and a thousand conversations take place simultaneously, yet you can ignore all of it when, suddenly, somebody calls your name and your attention is immediately drawn to them.

How is this possible?

We're able to pinpoint interesting things in our environments by using our _reticular_ _activating_ _system_ (RAS). Our RAS focuses on whatever is occupying our thoughts and sifts through the countless bits of information that bombard us everyday until it finds something that fits.

However, many of us think with a negative mindset that calibrates our RAS to filter the wrong type of information, which then leads to _failure_ _reinforcement_, i.e., emphasizing the negative. For example, many people think that they "just can't remember names" and, lo and behold, their RAS responds by failing to focus on people's names.

Luckily, we can recalibrate our RAS by focusing on positive thoughts. By actively seeking out the positive elements of a given situation, the RAS can be reprogrammed to see opportunities where it once saw only problems. For instance, if you think to yourself, "I'm great at remembering names," your subconscious will focus on them more and help you remember.

But recalibrating our RAS doesn't just help improve our memories: it can also make us better salespeople. For instance, one of the most difficult tasks for many salespeople is cold calling.

However, by continually affirming that it's actually our _favorite_ activity, we can actually learn to enjoy it. You can reinforce this by simply placing "I love cold calling in the morning" in big letters on your wall and looking at it every day.

In addition, you can focus your RAS by setting specific goals. In fact, having short- and long-term goals and using affirmative wording will actually make the goals easier to achieve. Just telling yourself "I will make ten sales every day" makes it more likely that you'll actually make ten sales every day.

### 13. Create short task lists and manage your emails to maximize your productivity. 

When you enter your office every morning, you're probably confronted by stacks of emails and memos waiting for your attention. As the day progresses, more of them accumulate in your inbox and you react to these new memos instead of finishing what you wanted to do.

How can you avoid this?

It's actually quite simple: if you touch it — whether it's a document, email or letter — take action!

This means not opening or reading an email until you actually have the time to deal with it immediately. When you open an email, ask yourself, "Can I deal with this right away?" and, if you can't, leave it for when you have more time.

You're probably thinking: "How on earth am I supposed to know what's in an email _before_ I open it?!"

In order to be able to do this, you need to make sure that everybody in your organization clearly labels their emails.

Imagine, for example, that your team is communicating via email to follow up on your last strategy meeting. As the conversation develops, it migrates towards the implementation of a new product, and yet, the subject line is still "Re: our last strategy meeting."

As you could probably guess, that's the wrong way to go about it.

Instead, your team needs to apply relevant labels to their emails. In this case, the subject line might read "Four steps to product implementation." This way, you get an idea of what it's about before you commit yourself to opening it and taking action.

In addition, you should stay organized by making short task lists to ensure that the important tasks are completed.

Try listing the six most important things you need to get done that day. Having such short lists keeps things manageable and prevents you from getting caught up with tasks that don't need your immediate attention, thus helping you to focus on the things that will actually move your business forward and ultimately make it more profitable.

### 14. Final Summary 

The key message in this book:

**According to Chet Holmes, every business can double its sales by applying just one of his twelve key strategies with iron discipline and determination. These strategies give businesses the tools they need to attract their dream clients, hire superstar salespeople and outpace their competitors.**

Actionable advice:

**Hire** **superstar** **salespeople.**

You can only perform best in sales if you know what characterizes a superstar sales employee: the best salespeople have big egos and the high influence necessary to close the deal and increase your bottom line.

**Sharpen** **your** **team's** **tools** **with** **regular** **training.**

Don't wait for your sales team to encounter a difficult situation before giving them the tools necessary to tackle it. Be proactive in your training and make sure that that you schedule training often. This way, you can engrain important skills into your employees before they have to use them in the real world.

**If** **you** **have** **a** **sales** **superstar** **in** **your** **team,** **manage** **him** **or** **her** **carefully.**

Superstars are original, intelligent and self-confident. They tend to criticize and seek challenges. When they come up with new ideas, let them take the reins and challenge them to overachieve.
---

### Chet Holmes

Chet Holmes was a sales consultant and marketing guru who advised more than sixty Fortune 500 companies, owned fourteen businesses and educated millions of business owners and employees through his seminars and articles in his lifetime.

