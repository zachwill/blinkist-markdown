---
id: 53c63b703839340007de0000
slug: the-undercover-economist-en
published_date: 2014-07-15T00:00:00.000+00:00
author: Tim Harford
title: The Undercover Economist
subtitle: None
main_color: 87AD72
text_color: 607A51
---

# The Undercover Economist

_None_

**Tim Harford**

_The_ _Undercover_ _Economist_ explains how economics defines our lives. From the price of a cappuccino to the amount of smog in the air, everything is tied to economics. The book shows us how economists understand the world and how we can benefit from a better understanding of economic systems.

---
### 1. What’s in it for me? Make better choices by thinking like an economist. 

How many times have you moaned and groaned about how your shopping bills are becoming more expensive? Or how you could've _sworn_ that broccoli was only two dollars per head, and not four. Or how someone has sold you a shoddy product?

Yes, it's frustrating! But do you ever stop to think why any of this happens in the first place? Why we continue to buy expensive products, even when we can't be sure of the quality?

_Undercover_ _Economist_ sets out to answer those questions, and more crucially, provides us with an understanding of how economics shapes our lives and our purchasing decisions. It also explains how, by understanding the economics behind everything, you can start to make better purchasing decisions in your day to day, and no longer fall victim to the tricksy strategies of marketeers.

Along the way, these blinks will show you how entire societies can be defined by their economies. If you've ever wanted to know why some countries are poor while others are vastly wealthy, these blinks will offer you some critical insights.

In addition, you will learn:

  * why it's so hard to discern peaches from lemons,

  * how shopping at a train station can be hazardous for your bank account,

  * why one company would intentionally make one of their products less effective, and

  * why it's not always best to go to a discount store when looking for a discount.

### 2. The economy has a huge impact on the simple choices you make every day. 

Sipping your morning cappuccino, do you ever stop to think how that cappuccino got there? Probably not. Yet, perhaps you should, as it reveals crucial insights about our economy, and thus our lives.

Even something as simple as a cappuccino is the result of the economy's ability to bring many professions together.

Imagine trying to make that cappuccino all by yourself. Where would you even begin? You'd need to first grow the coffee beans, harvest, dry and blend them. You'd also need to raise a cow, gather its milk, and then fashion your coffee mug out of clay. Finally, in a spark of engineering genius, you'd have to build an espresso machine.

Could you tackle all this by yourself? Not likely. You rely on an entire economic system, especially the division of labor across the world, to produce your favorite morning beverage.

Maybe you decide to buy your morning coffee, instead. Even the price that you pay is tied to an entire economic system.

Generally, the more scarce a resource is, the more it will cost, but this isn't always true. For example, you might think that if every coffee shop uses the same resource, then every cup of coffee would be priced the same. But they aren't.

In the UK, a chain of train station coffeehouses called ATM has far higher prices than its competitors. Is it because they sell a rare kind of coffee? No. ATM can charge high prices because the space they occupy, i.e., railway stations, is extremely scarce.

Lots of people pass by during their morning commute, and there is consequently a high demand for ATM's space. Without competitors who occupy the same space, this demand for coffee pushes the price up.

It is thus the intersection of convenience for customers and the high rent that makes ATM's coffee more expensive.

It is these sorts of insights that allow you to think like an economist, and thus better understand the world around you.

### 3. Companies use many strategies to make us pay as much as possible for their products. 

Every company's goal, no matter how nice they seem, is to get you, the customer, to pay the maximum amount that you're willing to pay for a given product, and they use several pricing strategies in order to accomplish this.

Obviously, they can't just ask you what your maximum payment would be. They'd never get a straight answer. So, companies have to employ sneakier methods.

One way is to offer you a range of slightly different products that all cost roughly the same amount to produce, yet are priced differently.

Companies like Starbucks, for example, do this. Rather than offering a single type of coffee, they offer a variety of caffeinated products at different prices. For instance, you can get a large coffee with extra cream for one dollar more than a small coffee with no frills. By providing options, they ensure that each customer has the opportunity to pay their maximum.

However, not everybody can afford to pay the same maximum amount. Companies compensate for this variation with _group-targeting_ strategies. Examples of these are things like "seniors discount" or "student offers" for events like theater performances or public transport.

The idea is to ensure that those groups who have less money to spend can still afford a company's products or services, while making sure that "normal" customers, who have _more_ disposable income, still pay their own unique maximum price for the product.

Even if companies offer more than one version of a product, they will try to discourage you from buying the cheaper one.

For example, IBM sells two printers: the low-end "LaserWriter E" and the high-end "LaserWriter." The first is cheap and the latter expensive, but that isn't the only difference. IBM intentionally installed a chip in their cheaper version to make it slower in order to entice wealthier customers to buy the more expensive printer.

> _"Any well-run business would seek to charge each customer the maximum price he'd be willing to pay."_

### 4. Companies try and get you to pay more than you need to. Being aware of their tricks can help you avoid them. 

Companies might be cunning when it comes to chasing your money, but you are not totally at their mercy. There are ways to save your money, and it's up to you to practice good consumer habits.

First, be aware of where you buy your stuff. Consider, for example, that companies often use a strategy called _price-targeting_, where they sell the same goods or services at varying prices, depending on the market or the location.

In London, there are two Marks & Spencer Simply Food stores only 500 meters apart. In the one located at a metro station, _every_ _product_ is about 15 percent more expensive!

They get away with this because people in stations often have little time to shop, and just want to get in, grab their groceries, and get out. Consequently, they are less conscious of price.

Second, don't assume that products in discount stores are necessarily cheaper than elsewhere. While those stores may be cheaper _in_ _general_, if you're looking for a specific product, chances are that they will have the same product for exactly the same price at a higher-end store.

The trick, therefore, is not to search for a cheap store, but to shop intelligently by finding specific deals.

Finally, some stores, like supermarkets, often price their goods randomly, so try to be aware of how prices change so you don't get tricked.

For example, many supermarkets will randomly triple the price of their vegetables, just to see how it affects purchases. Customers that take notice will simply buy another type of vegetable. Those that don't notice or don't care end up paying more than they otherwise would.

Ultimately, it's up to you to make sure that companies can't take advantage of your desire for convenience — or your laziness.

Now that we've learned a bit about how the economy functions, the following blinks will examine what happens when it doesn't function properly.

> _"Supermarkets often charge ten times as much for fresh chilli peppers in a package as for loose fresh chillies."_

### 5. A lack of information can seriously distort the market. 

In the media and in the halls of university economics departments, there are many who constantly extol the genius and fairness of the free-market system, which they believe is the most efficient method of ensuring that everybody gets what they want and need at the right price.

But, the market has a major problem: it can easily break down when people are dealing with limited (or concealed) information. This is known as the _information_ _gap_.

One popular example of this is found in the used-cars market. When buying a used car, you might end up with a "peach" (one that works well) or a "lemon" (one that is basically junk).

As a prospective buyer at a used-car dealership, there is no way to tell which is a peach, and which is a lemon. The seller, on the hand, knows _exactly_ which is which.

If a buyer's budget is small, say $1,500, then he is essentially guaranteed that the seller will offer lemons only. However, if you offer a lot more, say $4,000, your chances of getting a peach are still only 50/50, as only the seller would know which is which.

Faced with these odds and a lack of important information, a prudent buyer will simply not offer _any_ money, as there is no situation in which she will definitely end up with a peach. When this happens, the market simply breaks down.

This is true only when information is one-sided or asymmetrical. If, however, both the buyer _and_ the seller didn't know which cars were peaches and which were lemons, then the buyer could take a 50/50 chance at a lower price.

For the market to work smoothly — and fairly — there must be mutual exchange of information. Without information exchange, it's impossible to do good business.

### 6. We need to ensure that a product’s harmful side-effects are included in the price. 

Does the market really provide the most efficient means of getting everybody what they want? That depends on what you want. If you value clean air or public transit, then you probably don't feel you've gotten what you want by being stuck in smoggy traffic on your morning commute. So how can we account for this?

The free market theory suggests that if we all pursue our individual desires, then everybody will benefit. However, this fails to take into consideration the possible negative consequences of our actions.

In other words, if you want to buy a car, then the market is supposed to provide you with what you want for a fair price which also benefits the seller. But there are social costs hidden in the equation that aren't included in the retail price.

Every city in the world suffers from air pollution as a result of high density of gas-powered vehicles. Not only is the high density of gas vehicles harmful to your health, but it also prevents people from using cleaner methods of transportation, such as cycling.

In order to curb these social costs, the government should step into the market to levy _externality_ _charges._ Generally these are taxes on things that add costs to the wider society in order to ensure that the steps to manage these problems can be paid for.

London, for example, introduced the congestion charge, that people had to pay when driving through a certain area of town. The effect was astonishing: a significant and immediate drop in traffic.

When driving had no additional costs, people hopped in the car to go even short distances. Since the introduction of the congestion charge, people started to choose walking or cycling instead.

However, you can't put taxes on everything. For example, if someone's behavior is simply _annoying_, and not truly _harmful_, then it doesn't make sense to tax it.

> _"Around seven thousand people a year die prematurely because of traffic pollution in Britain."_

### 7. Malfunctioning institutions and corruption restrain economic development. 

One of the most heavily discussed economic questions is why some countries are poor and others manage to develop and thrive. Is it because of their access to resources? Because of the markets they choose to enter? Not quite; one of the strongest reasons for poverty is simply having the wrong kind of government.

Lack of democratic control and response to constituent demand is economically unhealthy. Often, an authoritarian leader's primary goal is to seek personal fortune, even at the expense of the people. In these cases, money that comes into the country is invested neither in infrastructure nor the constituents, thus causing the economy to flounder.

Cameroon, for example, is one of the poorest and most corrupt countries in the world, governed by authoritarian leader Biya, who is interested mainly in maintaining his position of power and furthering his self-enrichment.

Adding to the problem, dictators need dependents in order to secure their power. So they tolerate a culture of corruption, further damaging economic development.

For example, because Cameroon is hardly governable, Biya must tolerate corruption to satisfy his powerful police and army. His soldiers therefore support him, as they are better off with him and the power and corruption that come with his office, than with democratic leadership.

The result of this corruption is an economic decline: to set up businesses, you have to pay bribes to a corrupt bureaucracy. Infrastructure and the educational system are also struggling, spiraling downwards due to lack of governance or support.

The solution seems simple. You simply have to remove the crusting layer of corrupt bureaucracy in order to free the flow of money and ideas. The free market would fix the rest.

An accountable government would be needed, however, to carry out this shift, and the lack of an accountable government is the initial problem.

### 8. Poor countries can thrive if they open their markets to international trade. 

There are many examples of once-poor countries that now are wealthy. Think, for example, of Taiwan and South Korea. History has shown that the key to their success lay in opening their borders to international trade.

The economic growth is because it's far more efficient to engage in international trade than trying to stay self-sufficient.

Protectionist stances toward trade, in which imports are forbidden in order to support domestic trade, ultimately cripple a country's exporting industries, as other countries will immediately stop importing goods to them in response.

Doing the opposite and opening up to international trade can allow countries to take full advantage of the vast and varied global market.

For example, it took mere decades for South Korea to become a wealthy nation after opening itself up to the world market. Meanwhile, North Korea, its totalitarian sibling, severely isolated itself with the hope of being self-sufficient. The result has been incredible poverty, in which the average North Korean suffers from hunger.

However, it's not enough to just trade on the international market. Once there, you have to specialize and concentrate on doing what you do best. By focusing on your most refined skills, you can reap the benefits of _comparative_ _advantage_.

Imagine, for example, that Britain is best at making televisions and produces one unit per hour. China might be able to produce a TV in only half an hour, but their specialty is in manufacturing DVD players. While you might think it best for Britain to stop trading with China to protect its own TV production, the opposite is actually true! If Britain, producing what it's best at, sells its TVs to China, then China can concentrate on selling DVD players to Britain. This way, both countries gain from the trade.

> **Fact:  

** North Korea is so poor that it is hard for us to accurately measure just how poor it is.

### 9. Final summary 

The key message in this book:

**You** **can** **learn** **a** **lot** **about** **the** **world** **if** **you** **look** **at** **it** **through** **the** **eyes** **of** **an** **economist,** **and** **doing** **so** **will** **help** **you** **to** **make** **better** **decisions** **day** **to** **day.** **It** **will** **also** **help** **you** **gain** **a** **better** **understanding** **of** **why** **societies** **across** **the** **world** **behave** **the** **way** **they** **do.**

Actionable advice:

**Shop** **cheaply,** **not** **in** **cheap** **places.**

Don't assume that a trip to the discount store will mean any big savings for your weekly bills. In fact, when comparing specific products, discount stores often offer the same product at the same price as higher-end stores. Instead, you have to carefully think about what products you buy, no matter where you shop.

**Don't** **buy** **out** **of** **ignorance.**

Remember that the seller might be willing to sell you a dud product just to get your money. If there is no free flow of information between you and the seller, or if you aren't sure about something's quality, then walk away.

**Read** **_Freakonomics_** **by** **Steven** **D.** **Levitt** **and** **Stephen** **J.** **Dubner.**

Author Tim Harford is not the first economist to realise that the principles and statistical analyses used in economics can also be applied to a number of everyday phenomena. In _Freakonomics_, authors Levitt and Dubner examine such mundane acts as purchasing life insurance and picking up your kids from daycare through the lens of economics. The key insights from this book is available in blinks.
---

### Tim Harford

Tim Harford is an English economist, journalist and bestselling author. His long-running column "The Undercover Economist" illuminates the underlying principles of everyday economics. His other bestselling books include _The_ _Logic_ _of_ _Life_ and _Adapt:_ _Why_ _Success_ _Always_ _Starts_ _with_ _Failure_.

