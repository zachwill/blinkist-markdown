---
id: 5e74f3336cee0700068c8f1b
slug: the-unexpected-joy-of-being-sober-en
published_date: 2020-03-25T00:00:00.000+00:00
author: Catherine Gray
title: The Unexpected Joy of Being Sober
subtitle: Discovering a Happy, Healthy, Wealthy, Alcohol-Free Life
main_color: 2298AA
text_color: 186B78
---

# The Unexpected Joy of Being Sober

_Discovering a Happy, Healthy, Wealthy, Alcohol-Free Life_

**Catherine Gray**

In _The Unexpected Joy of Being Sober_, Catherine Gray persuasively argues that sober living offers a far more intoxicating high than the short-lived pleasures of drinking. Pushing back against socially and culturally ingrained stereotypes that equate sobriety with joylessness, Gray demonstrates that going sober creates opportunities for health, wealth, and, above all, fun!

---
### 1. What’s in it for me? Flip the script on sobriety! 

Look up the word sober in the Oxford dictionary, and you'll find two definitions. The first: unaffected by alcohol; not drunk. The second: serious, sensible, and solemn. For many people, the first meaning of the word sober is inextricably entangled with its second meaning. Perhaps you're one of those people yourself. Perhaps you've toyed with the idea of adopting a sober lifestyle, but abandoned it because sobriety strikes you as dull and joyless. 

But plenty of people who've embraced sobriety know something you don't...

Sobriety has a bad rap. The truth is, sober living can be joyful, surprising, and fulfilling. Going sober means giving up alcohol, not giving up fun. You don't need a drink in your hand to live your life to the fullest. In fact, sobriety can lead you to a far deeper enjoyment of life's pleasures than you ever thought possible. Curious to learn more? Then why not mix yourself a mocktail and settle back to enjoy _The Unexpected Joy of Being Sober_. 

In these blinks you'll discover

  * why cigarettes are frowned upon but drinking is encouraged;

  * why the phrase "moderate drinking" is a red herring; and

  * how to navigate awkward situations (yes, even sex!) while sober.

### 2. Sobriety is a fresh start – albeit one that brings challenges. 

In 1914, Thomas Edison — inventor of the light bulb and motion film, among other things — suffered a serious setback. His studio burned to the ground. All of his work was destroyed.

If you're a serious drinker, you'll know that alcohol can flame through your life, too, burning some parts of it beyond recognition. 

But don't get defeated and reach for another drink. Channel Edison, who reportedly surveyed his damaged studio, then said, "Thank goodness all our mistakes were burned up. Now we can start fresh."

**The key message here is that sobriety is a fresh start — albeit one that brings challenges.**

If you're ready to break up with bad drinking habits, you should know that the road to sobriety isn't an easy one. And the first few steps you take down the road are often the most difficult. So you need to prepare yourself for the challenges of sobriety. 

Here are a few tips that might come in handy — just remember that getting sober is different for everyone, and your mileage may vary.

If you're a physically-dependent alcoholic — that's a drinker who experiences physical withdrawal symptoms when they don't drink — then quitting will be tough on your body at first. You may experience problems sleeping, nausea, headaches, burning skin, and other nasty symptoms. It can be a good idea to talk to your GP before you quit. But don't worry: the first few days of sobriety are not your new normal. This period lasts approximately ten days. When the clouds of withdrawal fade, you'll see the world afresh from your new, sober perspective. 

Whether you were a moderate or a chronic drinker, buckle up for a ride on the hormone roller coaster. Alcohol dulls experience and numbs emotion. Particularly in your first few weeks of sobriety, you should be prepared to experience higher highs and lower lows than you did in your drinking days. Once your mood settles, though, you'll learn the joy of experiencing the world filter-free.

Sharing your sobriety-mission with others is a great way to keep yourself accountable. Why? It all comes down to the psychological phenomenon known as the Hawthorne Effect — essentially, people perform better when they know they're being observed. You're less likely to relapse with others cheering you on.

Finally, stay alive to the surprising bonuses of being sober. Say goodbye to lost phones and wallets, empty bank accounts, and regrettable late-night kebabs. Say hello to productive Sunday mornings, boundless energy, and a new zest for life!

### 3. The negative impacts of alcohol go way beyond your hangover. 

A splitting headache. A tongue the texture of dirty carpet. A churning stomach. An unshakeable urge to spend the day inside with the curtains drawn, watching the worst that Netflix has to offer.

Yes, the symptoms of a hangover are pretty unpleasant.

But, in a day or two, when the hangover's faded, you bounce back as healthy as ever. Right?

Wrong.

**The key message here is that the negative impacts of alcohol go way beyond your hangover.**

Alcohol means different things to different people. For some people, it's a shortcut to relaxation after a stressful day at work. For others, it's the bottled confidence they need to navigate social situations, or the liquid anesthetic that lets them drink away their pain. Regardless of your relationship with alcohol, there's one undeniable fact: when you drink, you are ingesting a harmful drug.

In fact, you are ingesting the most harmful drug in the world. That's right — in 2009, a team of British scientists released a report ranking the health dangers posed by various drugs, by giving each drug a score out of 100. Alcohol had the dubious honor of ranking highest, with a score of 72. It came in ahead of heroin (55), crack (54), and crystal meth (33). It was deemed almost three times more dangerous than cocaine and tobacco.

Professor David Nutt, who spearheaded the report, stated, "If you want to reduce the harm to society from drugs, alcohol is the drug to target at present."

The World Health Organization concurs. In 2011 they released a report noting alcohol as a contributing factor in over 60 diseases and concluded that, on an international scale, alcohol was the world's number one killer.

Still think the worst thing drinking can lead to is a hangover? Well, going sober will make you realize exactly the damage alcohol was doing to your body.

When your body isn't working overtime to clear the neurotoxin that is alcohol out of your system, you'll soon notice an uptick in energy, increased mood stability, and fewer problems sleeping. Other positive health effects you could expect to see include clearer skin and healthier hair. You'll kick facial bloating, bloodshot eyes, and flushed cheeks to the curb, too.

### 4. Even moderate drinking has harmful consequences. 

Everything in moderation, or so the saying goes. But do these words of wisdom apply to drinking? 

For some drinkers, the phrase "moderate drinking" is oxymoronic. The way their brains and bodies react to alcohol make it impossible to stop at one. But other people are perfectly able to drink in moderation. They can enjoy a glass of wine with dinner or a pint at the pub with friends, and then simply stop.

Moderate drinkers, it would seem, have the best of both worlds. They're able to enjoy a drink or two without experiencing any of alcohol's harmful side effects. Aren't they?

Actually, no.

**The key message here is that even moderate drinking has harmful consequences.**

Drinking, even in moderation, is always harmful to your health. In 2016, a report from the UK's chief medical officer warned that "there is no level of regular drinking that can be considered completely safe." There are plenty of facts and figures to back this recommendation up. Alcohol is not only a carcinogen, a substance that's been found to cause cancer in human tissue — it's a first-class carcinogen, right up there with tobacco and asbestos. It directly contributes to eight different cancers. And, according to researchers at Boston University, drinking as little as one and a half alcoholic units a day can set you on the road to a cancer diagnosis. Up to 35% of people who die from alcohol-related cancers are so-called moderate drinkers.

But what about the health benefits of alcohol? Every so often, studies touting the health benefits of moderate drinking spread across the media like wildfire.

A 2015 Canadian study, for example, found that drinking a glass of red wine was the equivalent of spending an hour at the gym. Drinkers everywhere clinked glasses to the scientists' findings. Unfortunately, a closer look at the study reveals the "fat-burning" effects of a glass of wine are all attributable to an antioxidant called resveratrol. Resveratrol is present in red wine. It's also present — in more concentrated amounts — in blueberries, grapes, dark chocolate, and peanut butter. And don't forget, despite containing trace amounts of resveratrol, red wine also contains harmful toxins and carcinogens. So, drinking red wine for the antioxidants is like diving into shark-infested waters because swimming is good for you.

Why do these dubious studies get so much airtime? Simple. We're all looking for ways to justify our drinking. But the only safe and healthy amount of alcohol to drink is none.

### 5. Alcohol is a dangerously addictive substance, yet society likes to pretend it isn’t. 

If you've ever got so drunk that you made out with a complete stranger, lost your shoes, or woke up with the remains of a Big Mac meal in bed beside you, then you'll know that excessive drinking has negative consequences.

But society acts as if it doesn't.

**The key message here is that alcohol is a dangerously addictive substance, yet society likes to pretend it isn't.**

Consider how drinking is portrayed on television. In shows like _The Good Wife_ or _Scandal_, Alicia Florrick and Olivia Pope frequently swig from goblet-sized glasses of red wine, yet their drinking never impacts their high-powered careers. Characters like Tyrion Lannister on _Game of Thrones_ are high-functioning alcoholics — yet their addictions only serve to make them more lovable.

On Facebook, popular memes bear captions along the lines of "Friends don't let friends stay sober." Even upmarket boutiques sell fridge magnets with slogans like, "Good friends offer advice; real friends offer gin."

Imagine if _The Good Wife_ showed Alicia snorting cocaine, consequence-free, every time she won a case. Imagine a fridge magnet that reads, "Good friends offer advice; real friends offer crystal meth."

Alcohol is just as addictive as cocaine and crystal meth. And, despite the media's insistence that it's a harmless substance, many of us will suffer its ill-effects.

In the UK, one in six drinkers will suffer health problems directly linked with their alcohol use. In 2002, 200, 000 British women were hospitalized in alcohol-related incidents. Flash forward to 2010, and that number more than doubled. In 2014, 8,697 deaths were attributed solely to alcohol use. That's 24 people dying from alcohol's effects every day. Does this seem like the work of a harmless substance?

Other addictive substances, like cigarettes, are subject to strict advertising regulations. In the UK, cigarette packets are emblazoned with graphic pictures of tar-addled lungs to highlight the health impact of smoking. The government runs campaigns to warn the public about the dangers of drugs like heroin.

Why hasn't alcohol had the same treatment? Well, look at the figures. Alcohol-related disease and injury costs the UK government roughly 3 billion pounds a year. At the same time, it rakes in over 10.3 billion pounds from taxing alcohol sales. Why not keep those figures in mind the next time you see an advertisement that makes drinking look glamorous, sociable, and harmless?

### 6. Going sober might mean relearning confidence. 

A blind date where you have nothing in common? There's nothing like splitting a bottle of wine to banish awkward chit-chat.

A house party where you don't know anyone? Down some tequila shots with a stranger. Instant best friend!

A wedding dancefloor when you have two left feet? Grab another flute or three of free champagne, and you'll be moonwalking like Michael Jackson.

Many of us reach for a drink when we're not feeling confident. In fact, some of us might have learned confidence through drinking. Alcohol is a confidence-crutch.

**The key message here is that going sober might mean relearning confidence.**

Let's say you started drinking at 15. You found that drinking helped you shed your insecurities and inhibitions. You put up with the hangovers and the clouded judgment because you found drinking smoothed paths to romance, facilitated friendships, and gave you the courage to act more boldly than you would dare to sober. Drinking gave you faux confidence — you might never have learned to cultivate _real_ confidence.

Without alcohol, you'll need to relearn social confidence. Many people find that their sober selves are shyer, quieter, less confident — at least to begin with. So, how do you get that confidence back? Well, confidence comes more naturally in situations where you're comfortable or where you're enjoying yourself. While under the influence, those situations — viewed through the prism of alcohol — might have been pubs and clubs. But sober, you might find that, actually, that dank pub booth or that loud club isn't your happy place. You just thought it was.

So, find your real happy place. Maybe it's walking beside the sea, browsing bookstores, or viewing the world through a camera lens. Spend time here, cultivating your confidence and learning to enjoy the world on new, sober terms.

Going sober might also mean getting reacquainted with yourself. Under alcohol's influence, lots of us are extroverts. Sober, you might find that you were an introvert all along — after all, half the population are.

If the thought of a group holiday makes you grimace or you need to grit your teeth to play games like charades, you might well be an introvert.

This doesn't mean that you're a wallflower. It just means that you need to recharge your batteries after socializing — unlike extroverts, who recharge their batteries _through_ socializing. If drinking has blinded you to your introvert tendencies, you might need to learn some simple self-care techniques, like scheduling solo-time into your calendar or making sure you have one night off socializing every weekend.

### 7. You need to build up your sober social life gradually. 

Imagine you're hitting the gym for the first time in years. You wouldn't start lifting the heaviest weights first. You'd take it slow, build your strength. You wouldn't start pumping iron until your muscles were conditioned.

Well, think of your sobriety as a muscle. In the early days of sober living, it might be too weak to manage a boozy pub session or an open bar.

But that doesn't mean you can't socialize when sober.

**The key message here is that you need to build up your sober social life gradually.**

In the first days and weeks of sobriety, many people find socializing in traditional — read: booze-soaked — settings, like pubs and restaurants, too filled with the temptation to drink. But there's good news for the newly sober. More and more alcohol-free social spaces and events are popping up around the world. Try dancing at a sober morning rave, or meeting friends at an alcohol-free bar or restaurant.

Real talk, though: whether it's a wedding or a work function, sooner or later, you'll probably have to socialize in contexts where alcohol is freely available. And it's probably going to be tough.

Before you attend an event with alcohol, build up that sober muscle by fighting fantasy with reality. Fantasy? You're in a sunny garden, drinking rosé. Reality? Those afternoon drinking sessions often extended into the next morning. Fantasy? You'll just have one drink. Reality? For every time you successfully stopped at one, there are three more times when you lost count of how many you had.

It's depressing, but true — drinking is so normalized in contemporary culture that it's remarkable when someone over the age of 18 abstains in social settings.

Telling people you're not drinking can create awkwardness. It can be helpful to drop the "sober bombshell" ahead of time — let your friends know, via text or email, that you won't be drinking. And remember, no-one is entitled to know why you're not drinking.

Even when you come clean about your clean living, some people will push you to drink, dig for reasons why you're not drinking, or totally dismiss you. Congratulations — you've just activated one of sobriety's secret superpowers: it's a failsafe jerk-detector. Know that if someone isn't at peace with your sobriety, they're probably not at peace with their own drinking. Either that, or they're a jerk.

True friends will support you in your sobriety. And, when you're surrounded by true friends, socializing really isn't so hard after all.

### 8. Sobriety can rewire your neural networks for the better. 

Here's why sobriety is probably going to be more fun than drinking: your drinking stopped being fun a long time ago.

If you're a habitual drinker, over time, your brain has learned that whatever the problem — be it boredom, anxiety, or stress — the solution is alcohol. In fact, this so-called solution becomes coded right into the _prefrontal cortex_, the part of your brain that controls decision-making and regulates behavior. Over time, it becomes the only thing that can trigger a significant release of _dopamine_ — a neurotransmitter that opens our brain's pleasure centers.

Simply put, drinking can rewire our brain's neural networks. But sobriety can repair that damage.

**The key message here is that sobriety can rewire your neural networks for the better.**

Think of your brain as having a series of pathways running across it. The most well-worn pathway to dopamine release in a drinker's brain is alcohol. A dopamine rush can trick you — and your brain — into thinking that you've solved your problem. And your brain is nothing if not a problem solver. So it uses this pathway again and again and again.

The more the pathway is used, the easier it is to use. It's wide and accessible like a well-trafficked road. That's why it triggers alcohol cravings in so many "problem" situations. Feeling pain? Alcohol is the path to pleasure. Bored? Alcohol is the path to fun. Sad? Alcohol is the path to happiness.

In chronic drinkers, the brain sees alcohol as a super-highway from problem to solution.

But just because there's one well-traveled pathway in a drinker's brain doesn't mean that other pathways don't exist. They may be more like narrow lanes or back alleys or overgrown forest paths than six-lane highways. But they are there.

In the early stages of sobriety, you're trying to access these hidden neural paths. A 2013 UK study found that drinkers showed much less synaptic activity in their prefrontal cortexes than non-drinkers — meaning, drinker's brains made less use of neural pathways.

Encouragingly, that same study showed that a few months of abstinence was enough to restore a diversity of synaptic function in the brain. And the best news of all? Drinkers who'd gone sober often exhibited more synaptic activity than baseline participants who had never been dependent on alcohol. So, all the work your brain needs to do to adjust to sober living might make it stronger than ever!

### 9. Dating and sex can be very compatible with sobriety. 

Imagine you're on a first date, smiling at your romantic interest across a table for two.

Imagine you're introducing your new partner to your friends at a cozy Sunday dinner.

Imagine you're celebrating an anniversary, splurging on a sunset meal at a seaside restaurant.

Now, be honest: in how many of those imaginary scenarios were you holding a drink in your hand?

From first date drinks to champagne toasts at weddings, we're socialized to associate alcohol with dating, romance, and sex. 

**The key message here is that dating and sex can be very compatible with sobriety.**

First, let's dispel the myth that dating needs to involve drinking. It doesn't. In fact, many people don't view drinking as desirable at all. A recent study found that Tinder profile pics that show someone boozing are the least likely to be met with a rightwards swipe. Sobriety won't make you any less attractive to prospective romantic partners — though, without your beer goggles on, _you_ might be a little more discerning in your choice of partner.

Navigating a sober date is much like navigating any other social occasion. It's worth considering sending your date a text ahead of time to tell them you don't drink and let them know whether or not you're OK with them drinking, too.

But what happens when sparks fly, and you want to take things to the next level?

Many former drinkers are, understandably, unnerved by the idea of initiating sober sex. Alcohol makes initiating sex much easier — it loosens our inhibitions and artificially amps up our confidence.

Here's a secret, though: sober sex is far better.

For starters, it's more memorable. As in, you can remember it, once it's finished.

Even better, it's far more sensual. Alcohol may loosen our inhibitions, but it also dulls our senses. When you have sex sober, you'll be far more attuned to small, subtle thrills, like the feeling of your partner's fingers in your hair, or the sensation of their breath on your skin.

Last but not least, you'll likely perform far better. When you're having sex sober, you can act deliberately, thoughtfully, and with finesse.

Basically, drunk sex is like watching television in SD. When you switch over to HD, everything initially looks too bright, too clear, too well-defined. But, as soon as your eyes have adjusted to the superior viewing experience, switching back to SD feels like a major step down in quality.

### 10. Final summary 

The key message in these blinks:

**Many drinkers are afraid to take the leap and go sober because sobriety, frankly, has a bad rap. But what sober people know that drinkers don't is that sober living can offer far more fulfillment and pleasure than drinking ever could. It's a bold move to give up booze, particularly when we live in a society that normalizes drinking. But your boldness will be met with great rewards.**

Actionable advice: 

**Be an anthropologist of cocktail hour.**

When you're newly sober, you'll soon be hit with an epiphany: drunk people can be really, really annoying. Nevertheless, to maintain a social life, you'll probably have to spend time with them now and again. Don't let them irk you. Try observing their strange behaviors, David Attenborough style. It makes for interesting viewing!

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Sane New World_** **by Ruby Wax**

Feeling excited to start your journey to a joyful, sober life? Congratulations! Be aware, though, that quitting alcohol might not magically solve all your problems. In fact, alcohol dependency is often entangled with, and driven by, other mental health issues — and without booze as a numbing agent, it can be confronting to face up to them. Luckily, the blinks to _Sane New World_ by Ruby Wax are the perfect primer on how to reclaim your mental health. Wax is a comedian, best-selling author, and mental health campaigner who also happens to have a Masters in Mindfulness Therapy from the University of Oxford. There's no better guide through the mental health minefield, and these blinks will have you thinking clearer and feeling freer in no time.
---

### Catherine Gray

Catherine Gray is an accomplished author and journalist, whose features have appeared in magazines from _Stylist_ to _Grazia_. She's also a recovering alcoholic, whose frank debut, _The Unexpected Joy of Being Sober_, was a _Sunday Times_ bestseller. Gray has followed up her debut success with _The Unexpected Joy of Being Single_ and _The Unexpected Joy of the Ordinary_.

