---
id: 54e35758303764000a030000
slug: the-up-side-of-down-en
published_date: 2015-02-17T00:00:00.000+00:00
author: Megan McArdle
title: The Up Side of Down
subtitle: Why Failing Well Is the Key to Success
main_color: F05330
text_color: F05330
---

# The Up Side of Down

_Why Failing Well Is the Key to Success_

**Megan McArdle**

_The Up Side of Down_ presents the case for failure as a valuable part of our growth process, and something that we should embrace. In addition, the book offers sound advice for how we, as individuals and as a society, can better manage and mitigate failure.

---
### 1. What’s in it for me? Find out why you need to fail first to finally reach your goals. 

When you first don't succeed, try and try again. Sure, it may seem like wise advice, but is it true?

It turns out that success is not just about the trying, but also about the failing. While you might think that failing to meet your goal the first or even the second time makes you a loser, think again!

These blinks will show you that failure isn't a bad thing; in fact, you need to fail to reach your goals. Creating an environment where failure is not just okay but encouraged will help improve how you experiment with ideas to achieve success sooner than you think.

In the following blinks, you'll discover

  * what Swiss cheese has to do with avoiding disasters;

  * the major problem with bankruptcies; and

  * how anchor Dan Rather lost his job at CBS.

### 2. Learning how to fail gracefully is truly the only way to progress, and eventually, succeed. 

Sometimes it feels as though some people crash into every possible hurdle, while others never seem to even stumble.

The truth is that we all fail. It's _how_ we fail that separates the ordinary from the extraordinary.

One way that we struggle with failure has to do with how we conceptualize it, incorrectly assuming that success comes from an innate talent or intelligence, rather than through hard work and commitment.

Take Carol Dweck's experiment, for example, in which two groups of children took a simple test and all passed. The children then got to choose from a collection of follow-up tests that varied in difficulty.

One group was told they did well on the first test because of "how smart they were." This group was likely to choose an easy follow-up test.

The second group was told they did well "because of their hard work." This group, in contrast, was much more willing to take on a challenge with a more difficult test, and succeed in the face of adversity.

When children are praised for their innate skills, they find it harder to face challenges. "Naturally gifted" people will prefer to take an easier road over a bumpy one.

Such people can't _fail gracefully_. When coddled children finally end up in college or the "real world," they don't understand the importance of hard work, how to take criticism or how to fail.

It's not innate talent but graceful failure that leads to success, even if it's a long and painful process. Those who see failure as normal and surmountable, who have a _growth mentality_, see that they have nothing to fear from failure.

Mark Twain, for example, was a largely unsuccessful writer before he wrote _Huckleberry Finn_. Isaac Newton made blunders, too. Before his breakthrough discoveries in physics, he was drawn to alchemy, a pseudo-science that is now the laughing stock of the scientific community.

> _"Work begins when the fear of doing nothing exceeds the fear of doing badly." — Alain de Botton_

### 3. Our predictions won’t always help us avoid failure. Sometimes you just have to experiment. 

When you're trying to make up your mind to engage in a project, you may try to predict whether you'll succeed or fail — mostly to see if the effort is worth your time.

However, such predictions are often laughable in hindsight.

Take the 1995 film _Waterworld_, for example. With a leading star actor, a grandiose set and a summer release, the film was anticipated to be a phenomenal success. The film instead was a major flop.

Two years later, another film with an extravagant water-based set but a less-famous cast, a less-popular release date and a three-hour run time showed all the signs of a critical flop. And yet, _Titanic_ turned out to be one of the most successful films ever made.

We're not always the best at making predictions. In fact, the _only_ way to find out if something works is to try it out, to put the idea into the world.

In the 1980s, for example, executives at Coca-Cola were brainstorming ways to stave off competition from their biggest rival, Pepsi. So they spent huge sums of money developing a new product — New Coke. The marketing research, including surveys and taste tests, indicated that New Coke would be a hit.

What happened? New Coke was a disaster.

Coca-Cola hadn't understood the limitations of their testing, as people liked New Coke better in trials, but that preference didn't translate to the "real world."

There were other hidden factors too, such as the strong association between people who lived in the South and the Coke brand. Southerners were absolutely indignant about New Coke.

Coca-Cola did eventually rebound, but it learned a valuable lesson. It's always possible that things will turn out contrary to your best predictions.

### 4. Safety systems may reduce the chance of failure, but failures can and will still happen. 

Have you noticed that the more you rely on spell check, the worse your spelling becomes?

Indeed, our reliance on such _safety systems_, made possible by increasingly sophisticated technology, actually can cause us to make mistakes. What's more, they also delude us into thinking we're invincible.

Every day, we rely on multiple layers of safety systems to make failure less likely, something psychologist James Reason named the _Swiss Cheese Model_. Only in rare occasions, when the holes of each layer align, does failure occur.

For example, we rely on many safety layers to avoid traffic accidents. We go to driving school, police look out for drunk drivers, our cars are outfitted with anti-lock brakes, and so on.

And it's a good thing that we have so many safety systems, considering that three-quarters of Americans admit to driving while on the phone, while one in five admit they've even fallen asleep at the wheel! Despite this, only 0.02 percent of drivers are killed in car accidents each year.

Compare this with work in the medical field. Medical staff consult procedural checklists, wash their hands, use antibiotics and so on, and yet an estimated 100,000 people die each year from illnesses contracted during treatment — _three times_ the number of fatalities due to car accidents.

Clearly, safety systems aren't enough to always protect us from failure. What's worse, failure is unpredictable.

Before the Great Recession, economists believed that the Federal Reserve's expert management of banking institutions and fiscal policy was proof that they had reduced volatility in the business cycle. This belief was vindicated by a low number of defaults and the shrinking severity of recessions.

And yet, despite its regulatory power and successful track record, the Fed was powerless to stop the Great Recession and its complicated unraveling.

### 5. It’s hard to admit you’re wrong. Yet it doesn’t help to try to cover up mistakes, honest or otherwise. 

You might have the bad habit of trying to create a reality that matches your expectations, but in the process fail to see what is really in front of you.

Just before the 2004 U.S. presidential election, for example, journalist Dan Rather broke the news that George W. Bush had gone AWOL during the Vietnam War.

His source was a series of photocopied memos obtained from Bush's former commanding officer. Although CBS said that experts had verified the authenticity of the memos, they were later found to have been written using technology that didn't adhere to the standards of 1970s typewriters.

But why did CBS's experts miss the evidence that the memos were fake?

Modern psychology tells us that we all have a tendency for showing _inattentional blindness_, whereby we focus on one thing at the expense of others.

As part of an experiment to demonstrate inattentional blindness, students watched a video of people passing a basketball around after being told to count the number of times the ball was passed.

In their intense concentration, the students failed to notice a person in a gorilla suit enter the space, beat his chest and then casually leave.

The experts hired by CBS might have been so focused on looking for ways to verify the story and the memo's authenticity that they missed all the damning evidence against it.

Often when we do make honest mistakes, we sometimes act disingenuously to cover them up.

After the memos were revealed to be fake, Rather and the CBS team could have easily admitted their mistake. But they didn't. Instead, Rather defended the memos, leading to his team being fired from their jobs at the station, and eventually, Rather's departure as well. 

The following blinks will offer some tips on how to better cope with failure.

### 6. Don’t be an ostrich when failure hits; be strong and change course before everything is lost. 

Have you ever stayed in relationship you felt was unhealthy, or at a company that you knew was doomed? The reason for doing so lies at least in part in human nature.

People often act as if nothing is wrong, even when all signs point to disaster.

Consider General Motors, a company which in 1960 finally matched Ford's stature earlier in the century by commanding half of the American automobile market.

Whereas Ford had offered one make of car in one color, GM offered a greater variety while also developing innovations such as the first electric starter and an automatic transmission. GM even paid their workers double the national average!

But from 1965 to until the Great Recession 40 years later, GM continually lost market share. The company focused only on the positive aspects of its business, refusing to acknowledge its failures — which ultimately ended in disaster.

General Motors was presented with a number of intersecting problems from the 1970s onward that simply weren't dealt with. For instance, the union ensured workers earned above-inflation wage increases as well as generous health and pensions benefits.

What's more, policy made it almost impossible to punish unexcused absences, so much so that the company simply closed plants on the first day of hunting season, knowing full well that no one would show up anyway.

As a consequence, GM's costs were much higher, making it difficult to compete with cheaper foreign cars. In 1998, GM decided to discontinue its Oldsmobile brand, which had lost 75 percent of sales over 15 years. Adding to this failure was the fact that GM was required _by law_ to reimburse dealers once it closed a brand, costing the company over $1 billion.

In the face of such disasters, GM decided to focus on its past successes, the production of SUVs and expanded customer financing. Unfortunately, a spike in oil prices in the 2000s and a rise in the cost of credit threw cold water on the company's plans.

GM lost its advantage because it didn't address the actual issues the company was facing, instead burying its head in the sand until it was too late to act.

### 7. One of the more successful global franchise owners was for most of his life a complete failure. 

Unemployment is the pits. Having less money causes stress; your skill atrophy; and the economy as a whole suffers.

So what's the best way to deal when you find yourself without a job?

Try to learn from your mistakes, widen your job search and keep your forward momentum.

Follow the example of Harland Sanders, who spent his first 20 adult years drifting between jobs, getting fired from a few and running (and failing at) some independent business ventures.

At 65, Sanders found himself unemployed again. But he didn't despair.

Instead, he developed a fried chicken recipe — not because it was a lifelong dream, but because a newly built highway diverted customers away from his restaurant and he needed a new idea. So, he took his recipe on the road, eventually franchising the now global KFC.

As a job seeker, you should approach your job search like salespeople approach sales.

First, set specific _goals_ for approaching potential clients. Second, make sure to _record_ your attempts so that you can learn from them later. Employ a _script_ that you can hone through your experiences.

Finally, surround yourself with _people in the same boat_ who can offer you their insights.

Not only will this make the job search automatic, but it will also make it easier to cope with rejection.

As a society, we can also work to alleviate unemployment by taking steps to reduce the average unemployment time. One way is to make it easier to hire — and fire — employees.

Consider, for example, that U.S. unemployment is near the lowest in the OECD (Organization for Economic Cooperation and Development).

While European unemployment benefits are much more generous than in the United States, Europeans are also unemployed for longer periods of time. Likewise, European labor laws often make it difficult to fire employees after a probationary period, which serves as a disincentive to hire.

Failure will inevitably occur, and that means that we need to be prepared for it. Our final blinks will deal with approaching wrongdoing and failure in society.

### 8. When things go wrong, the simple explanation is not always the correct one. 

It's only natural that we try to identify the reasons for failure or disaster. Unfortunately, we all too often rush to judgment once we've decided who the culprit is or what's to blame.

Take the Great Recession. Some people blame the government, which, by encouraging access to easy credit, allowed people to spend beyond their means, or trade in subprime mortgage bonds.

Others blame the financial industry, which, after financial deregulation, took to selling expensive mortgages to naive, low-income home buyers.

The reality, however, is far more complex. Banks kept the worst deals on their books, indicating that most had no idea of the catastrophe to come. There was also nothing specific with regard to deregulation that could have been a smoking gun. The 1999 Gramm Leach Bliley Act, for example, did allow commercial banks to own investment banks, as critics claimed. However, the crisis started in investment banks _not owned_ by commercial banks, such as Lehman Brothers.

Most crises are made up of many single factors; yet no one event could bring the world to a halt. Yet when trying to grasp how or why things happen, we often prefer an easy-to-understand story to a messy, complex event that is beyond our control.

The rising rates of autism in children is another interesting example. It's easier to believe that the chemicals in vaccines are to blame, rather than a yet-undetermined mix of genetics, pollutants and infections.

We must learn to accept, however, that randomness and complexity are what rule the world, not some simple, imaginary puppetmaster.

### 9. The most effective punishment for crime is consistent and certain, not draconian. 

Sometimes people fail in ways that require society to intervene through the criminal justice system. So what is the best way to deal with people who break society's rules?

The answer doesn't lie in putting more people behind bars. In fact, the U.S. prison system is damaging to society in the long term.

We put so many people behind bars that it has become impossible to evaluate whether we're actually safer as a result. With overzealous prison sentencing, certain communities take it for granted that at some point, one will do jail time. And those who are eventually released are unable to rebuild their lives, which often leads to more crime.

Compounding this problem in the United States is the mix of mandatory minimum terms, "three strikes" laws and _revolving doors,_ where offenders are released directly to their old community after imprisonment.

Moreover, this system often entails _randomized draconianism_, where offenders can be handed lengthy jail terms for trivialities, such as missing a probation meeting or drug test.

We would be much better served by enacting swift, predictable punishment rather than the seemingly random and drawn-out process we have now.

One example of this is a pilot project called Hawaii's Opportunity Probation with Enforcement (HOPE). The program so far has had promising results.

For example, people on probation receive random drug tests. After six months, however, only four percent still test positive. In addition, offenders form relationships with probation officers, a feat which is normally impossible due to the ratio of offenders to probation officers.

This extra effort means that the HOPE program is more expensive than other handling for repeat offenders, but the cost is easily offset by long-term savings. Indeed, only 9 percent of offenders have had their probation revoked and have returned to jail, compared to 31 percent in the standard probation system.

It's programs like these that make it clear how much better rehabilitation is at curbing crime than the traditional prison system. As a society, we would all be better off with more of such programs.

### 10. Bankruptcy is not failure. U.S. laws allow entrepreneurs to get quickly back on their feet. 

Imagine you've been kidnapped by gangsters, who have tied cement blocks to your feet and thrown you in a river. A nightmare situation, for sure!

Yet to some financial advisers, this scenario is the legal equivalent of being shackled by long-term debt.

The easiest way to avoid this situation is to not get yourself in debt in the first place. But that's not always easy.

Take financial adviser Dave Ramsey, for example, who in 1986 at the age of only 26, was worth $5 million and had a booming real estate business.

Unfortunately for him, a new law was introduced that revoked tax deductions for investor-owned real estate. His lenders called in their loans, but he couldn't pay so he had to sell his assets at a loss. He then foreclosed on his home, filed for bankruptcy, and as a result, lost friends and almost his marriage.

Since then, Ramsey has rebuilt his fortune and now advises people to learn from his mistakes and avoid debt and bankruptcy at all costs. His advice is to spend less than you earn and save 15 percent of the difference.

Those who can't adhere to this financial plan sometimes have to file for bankruptcy. However, this isn't necessarily bad, as it allows debtors to start over, which is absolutely vital for entrepreneurs.

Unique to the United States' Chapter 7 bankruptcy law is the fact that debtors aren't bound to a repayment plan. Contrast this with Germany, for example, where a stipulation of bankruptcy is continued employment, or in Scandinavia, where your spending is scrutinized.

The American concept of bankruptcy allows both labor and money to be reallocated as quickly as possible, which is crucial for a healthy, entrepreneurial economy.

Indeed, compared to European countries where bankruptcy comes with punishing stipulations, the United States has markedly higher entrepreneurial activity.

> Fact: 75 percent of U.S. citizens have less than six months' expenses saved.

### 11. Final summary 

The key message in this book:

**Failure is natural; not just for you, but for everyone. Only by trying out new things, learning from mistakes and sharing risks and rewards with others can you ensure success.**

Actionable advice:

**Play video games.**

People may think video games are a waste of time, but the opposite is true. Today's games are realistic and complex, and have just the right balance of challenges and rewards to allow us to learn from mistakes and keep problem solving.

**Suggested** **further** **reading:** ** _The Rise_** **by Sarah Lewis**

Through a broad range of anecdotes and stories, _The Rise_ illustrates how some of humanity's greatest achievements arose out of what initially appeared to be failure. The author shows how setbacks are an inevitable — and in fact, _necessary_ — part of anyone's journey to mastery.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Megan McArdle

Megan McArdle is a journalist whose writings on politics and economics have appeared in _The Economist_, _Bloomberg View_ and _The Atlantic_. _The Up Side of Down_ is her debut.

