---
id: 5746c320b49ff200039c4d65
slug: the-upcycle-en
published_date: 2016-05-30T00:00:00.000+00:00
author: William McDonough and Michael Braungart
title: The Upcycle
subtitle: Beyond Sustainability – Designing for Abundance
main_color: D8DA49
text_color: 727326
---

# The Upcycle

_Beyond Sustainability – Designing for Abundance_

**William McDonough and Michael Braungart**

_The Upcycle_ (2013) explains that eco-consciousness and economics needn't be at odds. In fact, ecological sustainability is _good_ economics, and humans can nurture the planet by learning from nature and starting a green revolution.

---
### 1. What’s in it for me? Designing a more sustainable world. 

Next time you're walking around a shopping mall, take a look at how many stores advertise themselves as "eco-friendly." In all likelihood, there'll be many displaying their environmental credentials.

And yet, by and large, these are meaningless claims, mere window dressing. The companies don't really care about the environment; they just want to seem to care.

A truly eco-conscious company is possible, however, and these blinks make clear that such a business is defined by ecological thinking. It's not a matter of appearance; you must truly care about the environment, and that means thinking ecologically, designing ecologically and, most of all, operating ecologically.

Any company that does this will in fact be more efficient and more profitable than ever.

In these blinks, you'll discover

  * what airplane wings should be made of;

  * why there is no energy crisis; and

  * why sustainable thinking helps NASA stay in orbit.

### 2. Nature teaches environmental care. It all begins with upcycling. 

Climate change is perhaps the biggest problem ever caused by human activity. We've all heard about or witnessed the effects: rapidly shrinking rainforests, cataclysmic weather, melting ice caps.

So, how to solve this problem?

Some people argue that humanity needs to disengage from nature — a strategy that, actually, isn't very effective. In fact, a hands-off approach to the natural world is not an ecological way of engaging with the world.

That's because influencing nature doesn't necessarily mean destroying it. There are countless ways that we can live in harmony with the environment.

What if we think of the entire natural world as a garden. Just as gardeners care for and cultivate each plant, helping it survive and flourish, we can care for and cultivate the entirety of the natural world.

It's just a matter of building stable, productive environments that give flora and fauna the best chance of survival.

And we can learn to do this from the best teacher out there: nature herself. One lesson is of particular importance. We've got to _upcycle_ — that is, recycle waste products to produce something new. Think of how the natural world deals with feces, for instance: once they hit the soil, they're acted upon by micro- and macroorganisms and, eventually, they're turned into humus, a nutrient-rich substance that feeds other forms of life, like mushrooms.

Not just that, but nature can even make productive use of dangerous gases. For instance, take CO2, the gas that plays a fundamental role in global warming. Well, it's actually totally normal for animals to produce CO2, and many organisms produce it simply by breathing out. Plants then turn it into oxygen for animals to breathe back in, thereby completing the cycle. So, some CO2 emissions aren't dangerous. In fact, they're a key part of nature's process.

### 3. Well-designed, eco-conscious products are more efficient and cost-effective than conventional ones. 

When people hear "environmentally conscious products," usually one thing comes to mind — pricey. But this common assumption is far from true. That's because, when done right, ecological product design is more efficient, and therefore cheaper, than more mainstream production.

For instance, we're working toward technology that will make buildings extremely energy efficient, which means even more savings down the road. Soon, buildings will be able to use _artifactual lighting_ — lighting that's part artificial, part natural; buildings equipped with this technology would require artificial lighting for a mere 40 days out of the year. Obviously, this means major energy savings.

So, while building such a structure would cost about as much as constructing a conventional building of the same type and size, the eco-conscious one gives a _much_ better bang for your buck.

However, when implementing eco-friendly designs, it's not safe to assume that traditional materials will do the trick. In fact, you'll need materials that are both efficient and aligned with the project. It's therefore key to remain open to any and all materials. If it fits the desired design, then it's golden.

Just take Paul MacCready, designer of the first entirely human-powered airplane. He was also the first person to use Mylar, an incredibly strong polymer film, to construct airplane wings. The result was airplane wings that were incredibly durable for their time, earning MacCready his place in history as an aviation pioneer.

Or consider Thomas Edison, who searched long and hard for a natural substance to use as lightbulb filament. He was even open to using human hair! But, eventually, he concluded that bamboo would work best, and even had a botanist grow and import it for him.

Obviously, excellent product design enables the seamless combination of economic and ecological efficiency!

### 4. We don't have an “energy problem”; we have an efficiency problem. 

If you listen to politicians or so-called "experts" in the media, you'd probably think that we're facing an energy problem. But are we?

Actually, nature abounds in energy. We just need to learn how to harness it in a sustainable way. For example, wind energy, an omnipresent source, is becoming more and more accessible thanks to ongoing research. Not just that, but some states are even trying to help their citizens profit from it.

Consider the University of Maine, which is developing floating offshore wind turbines, the combined potential of which would exceed the output of 150 nuclear power plants! Or take Minnesota, where the laws are being tweaked to encourage investment in wind energy. As a result, there are now incentives for individual ownership of turbines and tax breaks for producing wind energy.

But wind isn't all there is! Hydropower is another key source of clean energy that's available worldwide. However, it's still essential when building hydro that every aspect of the construction is as green as possible.

For instance, when the Icelandic government built the Kárahnjúkar Hydropower plant, they intended it to produce 100 percent of the country's electricity. For the first few years, the project was considered hugely successful. Then a problem became apparent: the surrounding area had been negatively impacted and it's no longer the second largest unspoiled wilderness in Europe.

So it's important to remember that "renewable" isn't necessarily "green."

However, the energy problem could be drastically diminished in another way: reducing the insane amount of power that we waste every day. For instance, there are countless examples of inefficient transport routes that waste tons of energy. You might be thinking of trucks carrying oil on winding roads, but you're probably not thinking of meat.

That's right, producing meat for human consumption uses over 70 percent of the agricultural sector's energy. In fact, growing a single kilogram of meat produces over 30 kilos of CO2, uses several thousand liters of water and over a dozen kilos of grain.

### 5. Putting upcycling into practice means defining principles and sticking to them. 

Naturally, a greener economy is appealing, but it won't just descend from on high. To get our economy green, we'll need guidelines and principles that help us meet that goal. That's exactly what the _Hannover Principles_ are — a rulebook for people or businesses aiming to support a green economy.

First established for the 2000 World's Fair in Hannover, Germany, these guidelines provide an orientation for designers of all types on how to support nature and human welfare while also advancing technology. Crucial to the list is the need to concentrate on the interdependence of the economy and ecology. That's because it's always discouraging to see companies put their bottom line first only to arrive at solutions that are only slightly less detrimental than the ones they had before.

So, while the principles touch on various important points, one of the most essential is number six: "eliminate the concept of waste." This point is crucial because it pushes people to search for ways of upcycling literally every by-product on Earth.

In fact, only by sticking to principles that serve a company's values can the intentionality necessary to achieve one's goals be established. Even the largest organizations out there have to challenge themselves time and time again by refining their principles. They do this because they know it's the only way to turn good results into stellar ones.

For instance, NASA built a space station that endeavored to consume 90 percent less fossil fuel than older stations. They set a huge goal because they were determined to do the best for the people and the planet.

And this determination, coupled with concrete strategies and a commitment to fuel efficiency — they used water for cooling and air for temperature control — really paid off.

So it's crucial that we don't shy away from big design challenges. It's easy to compromise, and merely design something that's just a little less bad. Staying ambitious, however, is the only way to make true progress.

### 6. Final summary 

The key message in this book:

**Protecting the environment goes hand in hand with a strong economy, and the future is one of harmony between humans and nature. That's because ecological products are actually** ** _more_** **economical. By thinking creatively and ecologically, humans can build an advanced world that nourishes, instead of depletes, the natural world.**

Actionable advice:

**Take note of your next big purchase's ecological footprint.**

Whether it's a smartphone or a new jacket, a little research can help you calculate just how much your purchase will affect the natural world. These environmental effects could be anything, from the CO2 emitted, the energy used or the toxic conditions company workers are exposed to.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Cradle to Cradle_** **by William McDonough and Michael Braungart**

_Cradle to Cradle_ (2009) exposes the fundamental flaws of manufacturing and the damage it inflicts upon our environment, even as we attempt to be eco-friendly. These blinks also introduce you to ways in which you can make a positive impact on the planet, and guide you through the process of rethinking your business in order to become eco-efficient.
---

### William McDonough and Michael Braungart

William McDonough and Michael Braungart entered the international stage with their first book, _Cradle to Cradle_, which explores how economics can complement a holistic view of nature. They've advised multiple powerful corporations, and Bill Clinton was such a fan of _The Upcycle_ that he wrote the foreword.

