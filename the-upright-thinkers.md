---
id: 57f20b8156f9f60003d4b4f4
slug: the-upright-thinkers-en
published_date: 2016-10-06T00:00:00.000+00:00
author: Leonard Mlodinow
title: The Upright Thinkers
subtitle: The Human Journey from Living in Trees to Understanding the Cosmos
main_color: FDA74A
text_color: B07433
---

# The Upright Thinkers

_The Human Journey from Living in Trees to Understanding the Cosmos_

**Leonard Mlodinow**

_The Upright Thinkers_ (2015) takes you through the fascinating evolution of science, tracing the footsteps and influence of major figures along the way — from Galileo to Einstein to Heisenberg. These blinks will start with a trip back in time to the first moments humans learned to control fire, and will leave you with a brief summary of quantum mechanics.

---
### 1. What’s in it for me? Learn how the sciences became what they are today. 

Science plays an incredibly important part in our everyday lives, so it might be easy to think that the field of science itself has been around for as long as humans have. This is far from being the case.

In fact, science has developed in countless tiny steps, from the most basic observations of the natural world to the incredible technological advances of the modern world.

So how did it all start? How did humans start to think in scientific terms? And how did it all get this far? These are the key questions that these blinks will address. They will take a journey back in time, telling the story of science, how it came about and how it has evolved over the past 5,000 years.

In these blinks, you'll learn

  * which piece of clothing a seventeenth century chemist thought mice were made of;

  * how arithmetic was used in Babylon in 2000 BCE; and

  * how the Dane Niels Bohr revolutionized the model of the atom.

### 2. The foundation of scientific thinking was laid when curious-minded humans began working together. 

As the saying goes, Rome wasn't built in a day. Similarly, scientific thinking didn't spring up overnight; it took eons for human beings to develop scientific methods and analysis.

We can thank human nature and our inherent curiosity for this eventual breakthrough. Throughout human evolution, curiosity and inventiveness have been crucial to the survival of our species.

The tools that early humans created and the fire that we learned how to control were vital to keeping ourselves fed and keeping predators like saber-toothed tigers at bay.

This kind of curiosity and knack for problem solving has always been an innate and unique aspect of human nature.

Unlike chimpanzees, we can see how human children play with toys while diligently trying to figure out how they work, or why a tower of wooden blocks keeps toppling over.

With this curiosity already providing an advantage as compared to other species, innovation really took off once humans started living and cooperating with one another.

Around 11,500 years ago in modern-day Turkey, people formed the earliest communities to worship gods and exchange ideas.

Cohabitation came with a lot of advantages, chief among them being that it enabled people to share past experiences with one another, create a pool of knowledge to work with and use this knowledge to come up with early innovations such as irrigation systems.

This is essentially the same type of brainstorming and problem solving that happens among the employees of creative companies like Google and Apple.

Another advantage of cohabitation was that it allowed for the distribution of work. In Mesopotamia around 7000 BCE, people began to assign tasks within the community, which allowed individuals to focus on one job each day rather than fretting over all the daily activities that are essential to survival.

This eventually led people to have occupations, such as bakers, brewers and blacksmiths.

In turn, this division of labor gave rise to schools, which appeared roughly 5,000 years ago, giving people a place to learn professional knowledge from experts in their field.

### 3. Writing, reading and mathematics were central to the advancement of society and the sciences. 

Shared knowledge was a huge leap forward for humans. However, since all this information was only being shared orally, people soon realized that they needed a standardized way to document it. This way, they could carry and share knowledge when they traveled, and the information could be preserved for future generations.

In order for this to happen, humans developed reading and writing, a process that took considerable time to advance. The first documented efforts at writing date back to Mesopotamia at around 3000 BCE.

This earliest, primitive form of writing was a pictorial script that consisted of symbols such as rudimentary drawings of animals. However, simple as it may have been, it allowed people to start keeping records of transactions; soon, communities and businesses were able to grow and develop economically.

After 2900 BCE, humans began combining various symbols in order to describe the kind of complex concepts a pictorial script couldn't capture.

This was of huge benefit to schooling, as scientific concepts, in fields such as geology and medicine, could be written down for generations of students.

The earliest evidence of mathematics being used to make simple calculations and predictions dates back to approximately 2000 BCE.

While it was a very limited form of math, it was useful in many professions. For example, people could figure out how much time and labor a project might take by calculating how much dirt had to be removed and dividing it by the amount a single worker could move per day.

The first signs of geometry can be traced back to this time as well. For example, the Egyptians developed a way to measure the surfaces of large areas in order to form the boundaries of property lines.

While these were certainly significant advancements, it would take another 2,000 years before Indian mathematicians invented the standard system of addition, subtraction, division and multiplication that would make modern science possible.

### 4. The ancient Greeks were among the first to start thinking rationally and develop scientific theories. 

Mathematics isn't the only important building block of modern science; scientific advances also required rational minds that asked the right questions. And this is where the people of ancient Greece played a vital role.

Around 600 BCE, some important thinkers in Greece began developing an entirely new form of inquiry by questioning the common belief that the gods were responsible for the world's natural phenomena.

It was a major scientific breakthrough when people such as the philosopher Thales of Miletus began to speculate that it perhaps wasn't the gods who produced natural disasters, such as earthquakes.

While Thales wrongly theorized that the world was floating in water, and that earthquakes were the result of a disturbance in that water, it still signaled a major breakthrough to even ask this rational question.

Along with this entirely new way of thinking, the ancient Greeks were also among the first to study physics and start scientific experiments.

Philosopher and mathematician Pythagoras is a perfect example, as he strove to develop a better understanding of nature and the world through science and mathematics.

In one of his many experiments, he was the first to analyze the strings of musical instruments to find a scientific reason as to why shorter strings produced a higher pitch; he then put his results into a mathematical equation.

Aristotle also played a role in the scientific breakthroughs of this era. He's even credited with introducing the term "physicist" to describe people like himself and Thales, who were posing these rational and scientific questions.

And around 350 BCE, Aristotle came up with one of the first theories of physics when he considered the difference between natural change and violent change.

He believed all things have an inherent potential and that they are reached through a natural change, such as a person aging normally or a bird flying through the air.

But violent change also occurs, such as a rock being thrown and knocking the bird out of the air. In Aristotle's view, violent change must be caused by something, and he referred to this simply as "force."

### 5. Physics emerged by expanding on previous theories through abstraction and hard work. 

The march toward modern science began sprinting ahead after 1450, when a new breed of scientific thinkers starting expanding upon old observations in bold and exciting new ways.

By this time, the physicist Galileo Galilei was conducting his own precise experiments while looking at the results of the primitive experiments of the past with a more modern and abstract scientific mind.

Rather than making generalized observations, Galileo designed his own experiments and was one of the first to give his observations of speed and movement exact measurements.

While he conducted many experiments himself, such as poking a hole in a bucket full of water and measuring the time it took to empty, there were some observations he couldn't make directly.

However, this didn't stop him: Galileo used abstraction to expand upon one set of results and make predictions about another. And by doing so, he paved the way for modern scientific methods.

For example, Galileo discovered that you could tell how fast a bronze ball would free fall through the air by first determining how fast it would roll down a smooth surface at various angles and comparing those results.

Isaac Newton was another man who was interested in falling objects — but despite the popular myth, his breakthroughs in science weren't the result of an apple falling from a tree.

Newton was a dedicated scientist and his success was the result of years of research and hundred-hour work weeks. Legend has it that he worked so tirelessly, he didn't find the time to eat; instead, his cat would end up eating his meals and grew quite fat as a result.

Sometimes Newton would use his own body to conduct experiments: contrary to what we now know to be extremely dangerous, Newton once stared into the sun for as long as he could and documented the results. He discovered that after a certain period of time, colors would appear distorted, which left him wondering if light might be a product of our imagination and inspired him to continue his studies.

### 6. Chemistry began as an experimental craft before turning into a science that changed the world. 

Unlike physics, chemistry didn't really take off as a popular science until the seventeenth century. While chemistry might seem like a fundamentally important part of science today, in the early days of science, chemists were a lot like kids playing with test tubes and hoping for an interesting result.

In fact, rather than being the work of scientists, chemistry was once the work of craftsmen.

This goes back to the invention of mummification, a craft that people discovered after combining certain substances in an effort to preserve bodies.

These efforts progressed and scientific reasoning was soon applied when people discovered that, with the help of chemistry, one substance could be turned into another. And since water could suddenly become air, it wasn't long before people were wondering if there was something that could be turned into gold. Alas, as hard as these _alchemists_ tried, they could never manage to create gold.

In the sixteenth century, the revolutionary Swiss alchemist, Paracelsus, thought to be one of the first true chemists, introduced the idea of applying systematic procedures to chemistry and using elements to cure diseases.

After Paracelsus, chemistry had earned a reputation as a legitimate field of science, as people began to understand how elements interact with one another and how they are structured.

This led to more progress being made in the seventeenth century, when the philosophical physicist, chemist and wealthy earl Robert Boyle became the first to actually apply scientific thinking to chemistry.

After studies and experiments, he challenged the widely held belief that the only elements of boiling water are fire, air and water. He was the first to suggest that these substances were actually made up of other elements.

These advancements paved the way for the French chemist Antoine Lavoisier, who pushed chemistry even further in the eighteenth century.

While precisely weighing different substances, Lavoisier discovered that different elements like gases and solids can either merge with each other or repel each other depending on the conditions. Lavoisier made another significant discovery when he heated mercuric oxide and found that oxygen was released in the process.

> _"There is nobility in chemistry, the nobility of the quest to know and conquer matter but there has also always been the potential for great greed."_

### 7. The rudimentary science of biology finally evolved with the hard work of Charles Darwin. 

As the sciences continued to evolve and advance into the seventeenth and eighteenth centuries, discoveries were beginning to influence the field of biology and our understanding of animals. Today, Darwin's theory of evolution is considered common knowledge and modern genetics has answered many mysteries — but centuries ago, our understanding of the animal world was rather primitive.

For a long time, one common theory for the development of species was _spontaneous generation,_ the idea that simple species could emerge out of other substances.

This is what seventeenth-century Flemish chemist Jan Baptist van Helmont suggested: he believed that you could create mice by letting a mixture of wheat grains and dirty underwear sit for 21 days.

But theories like this began to change after the microscope was invented in the early seventeenth century. Scientists discovered that even small animals like maggots have reproductive organs and are more complex than they'd ever suspected.

It was clear there was still much to learn about biology, and a major development came when Charles Darwin's _The Origin of Species_ was published in 1859.

Darwin's understanding of evolution was the result of scientists being able to explore new lands and conduct painstaking observations.

Darwin visited South America and noticed that when one kind of species within a genus became extinct, it was succeeded by others of their kind. He determined that it was through competition and the ability to adapt to one's habitat that the dominant species ended up surviving and, eventually, thriving.

It took Darwin several years to make his observations and during this time he also found that some traits occurred purely by accident.

For example, he noticed that in rare instances, some birds in the _zebra finch_ family would be blue rather than the normal red. He called this phenomenon _random variation_.

Darwin also used the growing network of scientists and a new "penny post" mail program to collect and integrate data from his peers, allowing him to produce his theory of how natural selection and random variation worked together.

### 8. When science embraced the world of atoms, the field of physics took a huge leap forward. 

When groundbreaking discoveries were being made, it wasn't uncommon for people to think that these revelations marked an end to the secrets that could be unlocked.

This was especially the case in physics after Isaac Newton's findings: applicants to Harvard University were even told that there was nothing important left to study.

But this all changed at the dawn of the twentieth century when a few bright scientists started taking a closer look at the previously unknown world of atoms.

German physicist Max Planck was initially skeptical about the importance of atoms, but he was soon convinced otherwise: it became clear that there was no way to explain his findings without considering the idea that matter was made up of atomic particles.

Planck was studying the radiation produced by things like light and heat, and discovered that this energy could only be represented by going beyond and smaller than the elementary level.

However, Planck's theories weren't greeted with open arms after they were published. His research contradicted commonly held beliefs in the scientific community and it would take 20 years before he finally received his Nobel Prize.

By 1905, Albert Einstein was ready and willing to take Planck's ideas to the next level and be one of the first to find proof for the existence of atoms.

Einstein did this by observing the movements of small particles, such as pollen dust.

He discovered that the seemingly random movements of a tiny piece of pollen in water could be explained once you looked at the atomic level and accounted for the effect of the water molecules hitting the pollen from all sides.

Einstein also elaborated on Planck's work on radiation, looking closer at light energy and finding that this too consisted of particles called _photons_.

### 9. Physics continued to advance as the disciplined work of two passionate researchers revealed the structure of atoms. 

Planck and Einstein's work was essentially telling people that their everyday lives were being affected by things they couldn't see. This took some getting used to, and scientists were eager to continue unlocking the secrets of this atomic world.

In finding out what the atom is made of, physicist Joseph John Thomson discovered the electron and theorized that the atom consisted of a positively charged fluid in which a number of electrons circled.

Meanwhile, in a dingy and cramped basement with exposed pipes, physicist Ernest Rutherford was shooting radioactive particles through gold foil and marking where the particles landed on a second sheet behind.

Rutherford had noticed something strange: after firing many particles through the gold, many were moving straight through, but a few were being deflected.

To explain this, Rutherford suggested that the atoms in the gold foil consisted of free floating electrons orbiting a central, heavy nucleus. Most of the time, the particles he was firing would pass through the gaps between the electrons and the nucleus, but the deflections were the result of the rare occasions when a particle hit a nucleus.

Rutherford's model of the atom is essentially what would go on to be taught in schools today. 

However, Danish physicist Niels Bohr would make an important contribution to the model of the atom in the 1920s.

Bohr was a passionate scientist whose work and findings were so important to him that he even canceled his honeymoon in order to dictate a paper to his new wife.

Bohr saw a flaw in Rutherford's model: he believed it failed to account for the interaction between the electrons as they orbited the nucleus. He suggested that the electrons would influence each other and that this would disrupt their orbit.

This led Bohr to make a crucial improvement by proposing that atoms could lose or gain energy when an electron jumps to an outer orbit.

### 10. The challenge of measuring the invisible led to the important development of quantum theory. 

With the revelations of the early 1900s, people were getting a good idea of what was going on in the world of atoms — but the question remained: how can you measure something that's invisible?

To overcome this challenge and properly describe what goes on at the atomic level, a new theoretical approach to physics was created.

German physicist Werner Heisenberg reasoned that concepts such as position or speed would be unobservable and therefore unmeasurable at the atomic level. So, when he launched _quantum theory,_ it was designed to rely solely on factors that would be measurable, such as radiation and frequencies.

Heisenberg also invented _spectral data,_ a whole new language to describe this information.

This was important since it gave the things we can't see a whole spectrum of possible values. Instead of just one value that might be used to describe the color of an observable light, with spectral data you have an array of values to describe every possible color of the theoretical light emitted by an atom.

Sure enough, quantum theory has proven to be very useful and has spurred numerous inventions, particularly because it can explain and predict results on both the atomic level and the observational level.

However, Heisenberg's methods weren't everyone's cup of tea, since they were still quite abstract and difficult to apply in the practical world.

So, Austrian physicist Erwin Schrödinger developed a friendlier system for applying quantum physics, which was similar to the Newtonian notations that people were more familiar with.

This not only made quantum theory more approachable, it also allowed other scientists and inventors to apply these principles to their own work. Over time, this is what has made it possible for you to reheat your pasta in a microwave or have a three-dimensional scan made of your brain.

More than a hundred centuries ago, people in Mesopotamia came together in their belief in the mystical power of invisible forces. Now, through the benefit of quantum science, we have harnessed different mysterious forces, connecting us through the power of, say, fiber-optic cables.

> _"If that sounds complicated, don't worry — it is."_

### 11. Final summary 

The key message in this book:

**Historical advances made in science are sometimes depicted as the result of a miraculous moment of inspiration, such as Isaac Newton and his famous apple. In reality, the virtues of hard work, curiosity, cooperation and rational thinking have driven the remarkable progress of science throughout the ages, from the most basic writing systems all the way up to modern-day scientific innovations.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Drunkard's Walk_** **by Leonard Mlodinow**

This book is about the role randomness plays in our lives. It explores the historical roots of modern statistics and delves into fundamental mathematical principles to explain how — like a drunk person struggling to walk — much of our lives is dictated by pure chance.
---

### Leonard Mlodinow

Leonard Mlodinow, PhD, is an American physicist, author and a leading researcher in the field of quantum theory. He has written books alongside colleagues such as Stephen Hawking, and is the author of _The Drunkard's Walk: How Randomness Rules Our Lives_, which was a _New York Times_ notable book of the year and one of Amazon's choices for best science book of 2008.

