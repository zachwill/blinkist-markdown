---
id: 569e33cef16f34000700009c
slug: the-upside-of-irrationality-en
published_date: 2016-01-21T00:00:00.000+00:00
author: Dan Ariely
title: The Upside of Irrationality
subtitle: The Unexpected Benefits of Defying Logic at Work and at Home
main_color: F89932
text_color: AB6922
---

# The Upside of Irrationality

_The Unexpected Benefits of Defying Logic at Work and at Home_

**Dan Ariely**

In _The Upside of Irrationality_ (2011), Dan Ariely uses behavioral economics to show us why we behave irrationally, how it affects our decision-making processes, and what we can do to make better choices.

---
### 1. What’s in it for me? Find out the truth behind your bizarre behaviors. 

We'd all like to behave in the most rational way possible. Wouldn't it be great if we always knew how to make the right decision? Imagine how much better our lives would be if we always got the best deal in stores, in love or at work.

But of course we can't. We're pretty lousy at being rational. Still, it doesn't mean all is lost. These blinks will show you some of our most common irrational behaviors and the ways you can take advantage of them. If you want to rise to the top in our upside-down world, read on.

In these blinks, you'll discover

  * why dating sites almost never lead to happiness;

  * why, if you're a steady seven, you'll never get the perfect ten; and

  * why we actually like having to work hard for our money.

### 2. A hefty bonus isn’t always the best incentive. 

Most of us assume that the higher the incentive, the more time and effort we put into a job — and the better the quality of our output. Based on this logic, CEOs and stockbrokers receive eye-wateringly generous bonuses every year. But the logic behind this notion has been seriously called into question by several studies.

In one experiment, rats were put in a maze and given electric shocks. The higher the intensity of the shock, the more the animals struggled to find the escape route. Instead of being stimulated to find their way, they froze and completely forgot the layout of the maze.

If you replace the shocks with money and the rats with people, you can imagine how challenging it is to concentrate when a fat bonus package is hovering over you at work. Just like rats, people don't give their best when they're placed under intense pressure.

That said, there _are_ times when high incentives foster high performance, but it's only when the task at hand is primarily mechanical or manual. When it comes to innovation and creation, it's a different story.

CEOs aren't paid for manual labor, and bonus-based incentives may even be detrimental when it comes to problem-solving, creating and innovating. In fact, the potential of a huge bonus can actually make them perform worse. 

We can clearly see the impact of stress when we give a speech: our preparation goes well behind closed doors, but everything can suddenly fall apart when we're faced with an audience. Here, the culprit is our _hypermotivation_ to impress.

So what can we do?

One solution could be to offer employees average bonuses, i.e., the average of an individual employee's bonuses over the past five years. Performance would still be rewarded, but the pressure to perform before a deadline would be reduced, which would lead to less stress and better results.

### 3. Our motivation at work is a complex phenomenon that can’t be reduced to a paycheck. 

Living creatures aim to maximize rewards while minimizing effort. Although it may seem obvious, this notion is contradicted by the concept of _contrafreeloading_. The term, coined by animal psychologist Glen Jensen, describes the preference displayed by many animals — such as fish, birds and monkeys — to earn their food as a reward for performing a task rather than receiving it for nothing.

This observation can be applied to humans in the workplace, where the meaning found in work — not just the money earned by doing it — affects our willingness and motivation to do it.

In one study, two groups of people were paid to build Lego constructions individually and they were told they could quit any time. When individuals in the first group had completed their structure, it was carefully examined by a researcher. In the second group, everyone's structures were promptly dismantled. Seeing the destruction, and thus the futility, of their work, those in the second group were more likely to quit building earlier than those whose structures were evaluated by the researchers.

In short, when we feel our work is devalued, it has a huge effect on our motivation. We don't put in the work when there's no meaning in it! 

We also struggle to see the value in our work and are consequently demotivated when we're assigned highly simplified tasks. Adam Smith was first to come up with the idea of the division of labor, i.e., putting people in charge of smaller parts of a larger project. 

Karl Marx denounced it, chiding it as the _alienation of labor_, because it separated workers from their activities and prevented them from gaining a sense of identity or finding a purpose in their work. This is evident in workers who mindlessly screw bolts onto a piece of metal day after day, without feeling involved in the end product, e.g., a car.

This could be why so many of us are demotivated at work.

### 4. We tend to overvalue what we do compared to what others do. 

How proud did you feel after assembling that IKEA bookcase by yourself? Pretty satisfied, right? 

That's because the amount of effort we put into a given task determines the pride we get from it. But we only need to exert a paltry amount of effort in order to feel pretty good about ourselves.

Take cake mixes that were first introduced to the home baking world back in the 1940s. Women weren't exactly gushing to tell their friends and family that they used a packaged mix but this all changed when Pillsbury decided to leave the dried egg out of their mixes, instead instructing women to add a fresh one themselves. The result? Sales skyrocketed. The simple effort of adding an egg was enough to turn the cake into a genuine homemade dish!

This also demonstrates the power of the creator's bias when evaluating work. 

We tend to be rather oblivious to the fact that we overvalue our creations. If you have children, for example, you probably think of them as the best children on the planet. Well, most parents do! This subconscious, often irrational, attachment applies to the business world, too. In fact, brands exploit it when they give clients the opportunity to customize their products.

For instance, Converse.com gave their customers a chance to design their own sneakers by picking out the material, colors, etc. They played on the creator's bias to have the customers view their creations as special and unique.

But sometimes effort isn't enough — we also need completion in order to evaluate something in a positive light. Without it, the pleasure in achieving our goal disappears. Think about your love life: if someone you desire puts obstacles in your way when you're trying to woo them, you'll place a higher value on this person. Yet if this person keeps rejecting you — i.e., giving you no sense of completion — you'll lose all positive bias and your interest in that person will fade.

### 5. Our adaptability explains why we may become happy citizens or addicted shopaholics. 

Are you a creature of habit, set in your ways, confident about your long-standing preferences? Well, you might be more flexible than you think.

The reality is, we humans have an extraordinary ability to adapt to almost anything. Every emotion we experience that leads us away from our normal state usually subsides with time, and we eventually bounce back to normality.

As one famous study on lottery winners showed: winners reported that, after their initial euphoria, their happiness levels eventually evened out to their pre-jackpot days. This is adaptation at play.

Adaptation is actually an important novelty filter, helping us to focus on change — or potential threats — in our environment. For example, if you've been on the couch all day and you suddenly smell smoke, you'll get up immediately to find out where it's coming from.

In the same way, we adapt to expectations or experiences and emotionally level out. This is known as _hedonic adaptation_. Say you just moved into your new house and are thrilled with its polished, hardwood floor. But, after walking on it for a couple of weeks, you don't even realize it's there anymore.

Hedonic adaptation also applies to our shopping habits, as we get bored of the things we have. Our failure to recognize our adaptation explains why we keep buying new things and expecting them to make us happier.

We have a propensity to constantly adapt to both positive and negative things. So to use adaptation to our advantage, we need to let the adaptation process continue without interrupting negative activities, and interrupt pleasurable activities to keep them exciting.

Minor interruptions — _hedonic disruptions_ — stop us from adapting. So it's best _not to_ interrupt painful experiences: next time you're cleaning the garage, don't take a break every five minutes — it'll make it more tedious. Conversely, if you've adapted to a long- term relationship that turned boring, try "interrupting" your adaptation by trying radically different activities with your partner.

### 6. Our adaptability helps us find our place in the dating hierarchy. 

You probably know the adage "birds of a feather flock together." This is particularly evident in the dating world, where beautiful people date each other, and the more aesthetically challenged of us date those with similar levels of attractiveness.

This phenomenon is known as _assortative mating_.

Here's a more concrete example: you arrive at a party where the host puts a sticky note on everyone's head with a number they can't see ranging from one to ten, indicating attractiveness. You are then told to select the highest-numbered person willing to make out with you. Let's say you were labeled as a six. Naturally, you'd start approaching the tens, then the nines, then the eights. But finally, you would end up with a six, as your numbers would match. This illustrates adaptation.

One way to adapt is to lower our aesthetic standards. That is, to place more value in a lack of perfection.

By rebuffing the nines and tens and switching to numbers more like your own as a "six," you may start to find seemingly unappealing traits desirable, for example, big ears or slightly crooked teeth. You may even end up rolling your eyes at a photo of Charlize Theron, saying "Ugh, I hate her perfect nose."

Another option is to seek out non-physical qualities, thus valuing attributes other people don't necessarily think about first. Kindness, for example, may replace physically appealing characteristics. Actually, this option is the one most frequently used: the less physically desirable of us learn to prioritize non-physical attributes.

The author proved his hypothesis during a speed-dating session. Less beautiful people requested another date with those displaying a sense of humor or some other non-physical quality, while the more attractive people preferred those who they evaluated as good-looking.

### 7. The online dating market is set up to fail. 

Found your true love on that dating site yet? No? Here's why...

Let's first look at markets. Markets are hubs that allow people to find what they need while conserving time and effort. Take your local supermarket: it offers you everything you need in one convenient place, without having to traipse all over town to the butcher, the baker and so on.

There are markets for just about anything, and love is no exception.

Particular circumstances contribute to the success or failure of the singles market. For instance, young people in jobs and schools are relocating more than ever, which means they don't settle and develop a steady social life where they can ultimately meet the right partner. Then, once they graduate, they have little free time to invest in finding their soulmate. 

In these circumstances, online dating _should_ flourish. Yet, online dating services often fail miserably in pairing up their clients. 

The author discovered that dating website users spent 5.2 hours per week scanning profiles, 6.7 hours emailing potential partners, and a mere 1.8 hours on meeting people face to face, often with poor results.

What's wrong with this system? Well, we're decidedly irrational when trying to meet romantic partners online and these websites reduce people into lists of searchable attributes like hair color, favorite movies and annual income. The problem is, we don't naturally evaluate potential partners using such absurd logic. Multiple-choice questions, checklists and characteristics cannot precisely represent the person and what it feels like to spend some time with them.

We are more than the sum of our parts, and we should keep this in mind when we venture into the world of dating both virtually and in person.

### 8. Our empathy is severely biased, but it’s actually not based on our irrationality. 

Why is it that many of us wouldn't think twice about lending money to a loved one, but don't bat an eye when it comes to donating to relief efforts that help thousands of displaced or injured people after natural disasters or political upheaval?

More often than not, we're moved to tears by a news report about a little girl that fell down a well, yet remain distant when we hear about the brutal murder of 800,000 Rwandans. It was actually Stalin who famously noted our irrationality toward tragedy when he said "One man's death is a tragedy, but a million deaths is a statistic."

Social scientists refer to this response as the _identifiable victim effect_.

Simply put, this means that we feel for someone when we see their picture, know their name and have some information about them, but have far less empathy when the information is broader and not attached to one person. Thus, in the latter case, we fail to act.

But there are reasons for this. The first is _closeness_ to the victim. This isn't just about physical proximity: it can also relate to those within the same social group as us.

Another reason is _vividness_. If someone tells you about their broken leg, you probably wouldn't empathize much. But if she relays to you in detail the excruciating pain that shot through her body as the bone shattered, you'll probably empathize with her a lot more.

Maybe you're thinking: What if becoming more rational is the answer to caring more about others?

Unfortunately, turning into Mr Spock won't help. Our logical attitude is as such: a rational human wouldn't care about anything that doesn't directly concern, threaten or profit him. As living beings, we're not naturally designed to care about catastrophic events that happen far away to people we've never met.

So, if we want to make a positive impact on society, we need to exercise some _ir_ rationality to see the point in helping those not in our immediate surroundings.

### 9. Short-lived outbursts lead to long-term negative feelings. 

Did you raise your voice the last time someone aggravated you? Here's a good reason to resist the urge to blow up next time: even when short bursts of negative emotions fade, we may be developing a bad habit without even noticing it. 

Imagine someone cuts you off in traffic on your way to work and you subsequently burst into a temper tantrum. The next time a similar situation occurs, it's highly likely that you'll have the exact same reaction. The reason for this is that we tend to look at ourselves in the past to determine how to act in the present moment. That is, in order to know what to do, we take cues from ourselves, just as we do from others. Our past behavior then turns into indications of what we should do next and we start to believe that we had a good reason for acting the way we did. This is known as _self-herding_.

It boils down to the fact that we have a poor memory for our emotional states. For instance, do you recall how you felt last Friday at 6:00 p.m.? We tend to remember our past actions over our past emotional states. The failure to recall the emotional state you were in when you swore at your friend or were rude to a colleague prevents you from _not_ doing the same thing next time!

This is why short-term decisions based on emotions may affect long-term decisions. We should therefore give them more consideration than we usually do.

So, the next time your kids are driving you insane, know that keeping your cool can prevent both short-term and long-term damage. Shouting at them may make them cry, which will already make you feel terrible, but it may also instigate a lasting pattern of you regularly losing your temper.

### 10. Final summary 

The key message in this book:

**We humans love to think we're rational and objective, yet this is often far from the truth when we have to make difficult decisions. On such occasions, our biases become hard to dispel, sometimes making us behave undesirably. In order to make good choices, we need to be aware of our irrationality and how to work with it.**

**Suggested** **further** **reading:** ** _Predictably Irrational_** **by Dan Ariely**

_Predictably Irrational_ explains the fundamentally irrational ways we behave every day. Why do we decide to diet and then give it up as soon as we see a tasty dessert? Why would your mother be offended if you tried to pay her for a Sunday meal she lovingly prepared? Why is pain medication more effective when the patient thinks it is more expensive? The reasons and remedies for these and other irrationalities are explored and explained with studies and anecdotes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dan Ariely

Dan Ariely is a professor of psychology and behavioral economics at Duke University. He is also the author of two international bestsellers, _Predictably Irrational_ and _The Honest Truth About Dishonesty_.

