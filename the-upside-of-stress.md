---
id: 5637732c62346500075d0000
slug: the-upside-of-stress-en
published_date: 2015-11-04T00:00:00.000+00:00
author: Kelly McGonigal
title: The Upside of Stress
subtitle: Why stress is good for you and how to get good at it
main_color: 236FAD
text_color: 236FAD
---

# The Upside of Stress

_Why stress is good for you and how to get good at it_

**Kelly McGonigal**

_The Upside of Stress_ explores the power our attitudes have when it comes to dealing with stress. These blinks explain the inner workings of our biological and psychological responses to stress, and open up new perspectives on how stress can help us grow.

---
### 1. What’s in it for me? Think about stress in a completely new way. 

Deadlines to meet, problems to solve, traffic to wait in, bills to pay: the list of things that stress us out is long. In fact, modern life seems to be filled with stress from the time we get up in the morning until we go to bed at night. But what if the hectic pace is actually good for us?

Though we usually think of stress as something negative, there's another side to the story: through the ages it seems like our bodies have developed ways to handle stress in much more complex and ingenious ways than we realize. It may even help us grow stronger and healthier. 

In these blinks, you'll discover

  * why being positive about stress increases longevity more than exercise does;

  * how holding hands reduces the impact of stressful situations; and

  * why people who are retired have an increased risk of depression.

### 2. Stress is harmful when you believe it is. 

We've all heard a million times that stress is bad for us and that it's the cause of most illnesses. So why should we all of a sudden embrace it?

Think about what stress really is: a reaction that occurs when something you care about is at stake. This could be your frustration over a traffic jam or your grief over the loss of a loved one. 

In one 2006 US study, researchers discovered that high levels of stress increased the risk of death by as much as 43 percent. But this was only in people who believed stress was harmful. Those who reported high stress levels but didn't believe it was harmful had the lowest risk of death of all participants, leading to the conclusion that stress is harmful — when you believe it is. 

One study at Yale University showed that people who looked positively upon old age lived 7.6 years longer. That's a lot more than the extra four years you earn by exercising and not smoking! 

Positivity is a form of belief so powerful that it can influence your body's health. Such beliefs can be considered _mindsets_ : superior to preferences or learned facts. Usually based on your own understanding of how the world works, mindsets affect how you think, act and feel. 

Your attitude toward stress is a central part of your mindset which shapes the choices you make in everyday life. If you view stress as harmful, you tend to try and avoid it at all costs. People who view stress as helpful, on the other hand, are more likely to come up with strategies to cope with the source of stress, seek help and make the best of the situation. 

Are you a person who faces stress head-on? Chances are you'll feel more confident about handling life's challenges. In this way, the belief that stress is helpful becomes a self-fulfilling prophecy. But what if stress makes you want to head for the hills? No need to fret! The following blinks will help you shift your mindset.

> _"Be able to see both sides of stress but choose to see the upside."_

### 3. A range of stress responses encourage us to engage, connect and grow. 

In the late 1990s, the trauma center of a hospital in Akron, Ohio, carried out an experiment on survivors of traffic accidents and found that they were able to accurately predict who would develop post-traumatic stress disorder. How? They based their predictions on the urine samples they collected from the survivors right after the crash. 

Forty-six of the 55 patients had higher levels of stress hormones such as cortisol and adrenaline in their urine, and did not suffer from PTSD. The remaining nine patients, with lower levels of stress hormones, did develop the disorder. This experiment made for powerful evidence that stress could lead to a better long-term recovery, even in deeply traumatic events. 

So, despite what you might hear, stress isn't all about a fight-or-flight response. It just doesn't work that way in modern life. We can't flee our relationships every time we have a disagreement, and we can't break out into fistfights at the office when a deadline looms! 

A fight-or-flight response is, of course, still valuable in the case of an assault or a burning building. But we also have the capacity to use stress to _engage_ with challenges, _connect_ with those around us and _grow_ from the experience.

There are several stress responses that help us to face difficulties with a positive outlook. One of these is the _challenge_ response. It's similar to the fight-or-flight response, but suited to situations that, while pressing, do not threaten your survival. The challenge response releases cortisol and adrenaline to generate a feeling of self-confidence and the motivation to learn from a tough experience. 

Another positive stress response is the _tend-and-befriend_, which is where we talk to a close friend or loved one when feeling stressed. This action causes the release of hormone _oxytocin_. Commonly referred to as the _love molecule,_ oxytocin encourages us to connect with others through caring social relationships. 

As well as helping us deal with stress in the here and now, positive stress responses also leave an imprint on our brains, rather like a vaccine. These experiences teach our bodies and minds how to handle similar stress in the future. That's why going through stress makes you better at coping with it in the long run.

### 4. A stressful life is often a meaningful one. 

Sounds counterintuitive right? But just listen to this. In 2005 and 2006, researchers from the Gallup World Poll asked more than 125,000 people aged 15 and over if they had experienced a great deal of stress the day before. 

On average, around a third of each country's population said they experienced stress. The Philippines topped the poll with 67 percent of their population suffering from stress. The US wasn't too far behind with a hefty 43 percent. Populations of other countries reported far less stress, with as low at 5 percent in the African nation Mauritania. 

So how did these statistics line up with other data? Results surprised researchers. Nations that exhibited higher levels of stress were also likely to have a higher GDP, longer life expectancy and improved quality of living. Conversely, countries like Mauritania with low levels of stress were prone to high levels of corruption, poverty, hunger or violence. 

This is what the author calls the _stress paradox:_ happy lives contain stress, and stress-free lives don't guarantee happiness. How can we explain this?

Well, perhaps because a meaningful life is also necessarily a stressful one. According to a 2013 study at Stanford and Florida State University, people who reported the greatest number of stressful events in their past were most likely to consider their lives meaningful. 

And what about you? Do you consider your life meaningful, and why? We often derive our sense of purpose from the different roles we play and responsibilities we have — in our jobs, as parents, and in our relationships. The activities that feel the most meaningful are precisely the ones that are the greatest sources of stress in our lives. 

This was evidenced in two recent surveys in the UK and Canada. Thirty-four percent of participating UK adults stated that having a baby was the most stressful experience of their lives. And, 62 percent of participating Canadians deemed their careers to be their biggest source of stress. 

Research also tells us that lives with less stress may also lack happiness. Humans tend to be happier when they are busy. This might explain why retirement, a time in life when we suddenly have less to do than we're used to, increases the risk of depression by as much as 40 percent.

### 5. Just thinking about stress differently helps us cope. 

With hundreds of articles, radio talks and editorials on the topic, stress seems to be one of the greatest conundrums of this century. Nearly everyone we know finds stress a struggle. 

But what about those people who just seem to breeze through life? We all know at least one of them. Every so often, we come across people who cope with stress far better than others. So what do they do differently? It's all to do with how they think about stress. 

Your resilient coworker, neighbor or acquaintance is able to roll with the punches because they see stress as a _normal part of life_. Which it is! Without stress, we'd have far fewer opportunities to grow and learn. Additionally, accepting that stress is normal makes us less likely to view every challenge as a catastrophe. 

These resilient people recognize that no matter how stressful things are, life does go on. They continue to make choices that will change the situation or themselves. But where does their reserve of strength come from? Interestingly enough, it often develops as a result of experiencing hard times in the past. 

Theresa Betancourt, Associate Professor of Child Health and Human Rights at the Harvard School of Public Health, observed in Sierra Leone in 2002 that child soldiers who had been used as human shields, sex slaves, or had been forced to kill family members or commit rape, were very resilient. Why?

The horrors they'd experienced in the past allowed them to see the bigger picture when facing problems on a day-to-day basis. Many even dreamed of becoming doctors, journalists and teachers. Even in extreme cases, past experiences of stress can endow us with strength and a positive outlook.

### 6. Embrace your anxieties to benefit from them. 

Do you ever get so nervous that your palms begin to sweat like crazy? Or perhaps your heart feels like it's pounding right out of your chest as your mouth gets drier than the Sahara. These are all very clear signs of your nerves, and usually you'll berate yourself for letting your stress get the best of you! But is this the right way to go?

Not always. Embracing anxiety actually helps us perform better. It just depends on how you channel it. To demonstrate this, Harvard Business School professor Alison Wood Brooks told some students who were about to give a speech to say "I am calm" to themselves, while others were instructed to say "I am excited." 

A simple trick, but it worked: the second group felt more confident in their presentation skills, handled the pressure more gracefully and were rated as more persuasive and competent speakers than those from the first group. Simply by shifting their mindset, the second group of speakers channeled their anxiety into an energy that boosted their performance enormously. 

Repeating _positive mantras_ like "I am excited" is one great way to channel your stress into strength. _Embracing your anxiety_ is another strategy for shifting your mindset, and it's quite an important one. Why? Because without it, you run the risk of falling into an anxiety-avoidance cycle. 

The author, for example, was deeply afraid of flying. She resolved to never go on a plane, so she wouldn't have to go through the anxiety. But by avoiding anxiety in this way, she let it get the better of her. Finally, the author got tired of missing out on seeing relatives or getting to explore new cities, so she decided to grit her teeth and face her anxiety instead. She still gets nervous on planes, but she knows that it's worth it.

> _**"** If you notice tension in your body, remind yourself that the stress response gives you access to your strength."_

### 7. Authentic exchanges with other people transform our stress into bravery, confidence and wisdom. 

Stress isn't just something we have to deal with. It's also something that can help us learn to care, cooperate and show compassion a little better. So why not embrace it!

Think back to the tend-and-befriend response we learned about earlier. This gives us a welcome opportunity to practice being social, brave and people-smart. How? First off, it fires up the _social caregiving system_ by releasing oxytocin while inhibiting the fear centers of the brain. As a result, we feel more empathy, connection and trust toward one another.

Simultaneously, the release of dopamine activates the _reward system_. This makes us feel confident and optimistic about our own abilities, and increases motivation while diminishing fear. Finally, _the attunement system_ is switched on by the neurotransmitter serotonin. This enhances our perception, intuition and self-control, making it easier to understand what action we need to take to get the best results. 

That's a lot of biology to remember! The most important thing to keep in mind is that you activate the tend-and-befriend response every time you choose to help others. Even if it's as simple as squeezing your partner's hand during a scary movie! Simply showing care for someone can turn fear into hope, just like that. 

A study at UCLA proved just this. Participants were told that their loved ones would be administered painful electric shocks. They were offered two different coping strategies to choose from: to squeeze a stress ball or hold a loved one's hand. 

Holding hands was shown to increase activity in the reward and caregiving centers of the brain, and decrease activity in the amygdala, a center responsible for fear and avoidance. The stress ball had no effect on the amygdala, confirming that most avoidance strategies cannot dispel distress or anxiety. Connecting with others during stress is in this way a scientifically proven way to respond to stress effectively.

### 8. Seeing the positive side of stress makes you more resilient. 

Think of a time in your life that led to positive changes, a newfound purpose or significant personal growth. It's not unlikely that these times followed periods that were very stressful. That's the paradox of stress! Although it's frustrating, the paradox of stress makes a lot of sense, too. 

The idea that what doesn't kill us makes us stronger is far from new. It can be found in the teachings of religions and philosophies for centuries. In fact, 82 percent of respondents in one study, when asked how they cope with stress in their lives, cited previous stressful experiences as sources of strength. In other words: adversity helps us learn and grow. 

Living a sheltered life can actually do us more harm than good in the long run. Psychologist Mark Seery found that individuals unfamiliar with adversity were the least resilient to it. 

He asked participants in a study to dunk their hands in ice-cold water. Participants unfamiliar with adversity found the cold to be the most painful and unpleasant, and took their hands out the fastest. What was going through their minds? Most of them reported thoughts about how "they couldn't stand it." By already discrediting their ability to cope, they only distressed themselves further. 

Even if you're currently facing a situation that's a lot more stressful than sticking your hand in ice water, remember that your attitude toward stress will shape the outcomes as well as future challenges. It's well known that men who find an upside to their first heart attack, e.g., a chance to change their priorities, have greater appreciation for life. Plus, people with better family relationships are less likely to suffer another heart attack. 

The reality is that seeing the upside of things makes a dramatic improvement in our coping abilities. Rather than relying on ineffective avoidance strategies, people who see the upside are able to take proactive steps to deal with their stress. Even their bodies demonstrate a healthier physical response to stress: they have faster recovery times and a reduced risk of depression, heart attacks and diseases.

### 9. Final summary 

The key message in this book:

**Despite what you may have thought, stress isn't always bad for you. If you approach stressful experiences as something you can learn from, then you certainly will. Even our biological responses to stress allow us to deepen relationships, boost our confidence and increase our resilience.**

Actionable advice: 

**Learn from stress today for more resilience tomorrow.**

Next time you're in the middle of something stressful, try to see the benefit of such a situation. How can you put this stress to good use? Simply by considering the answers to this question, you'll be better able to overcome not only that situation, but future stressful situations you face, too. 

**Suggested further reading:** ** _The Upside of Your Dark Side_** **by Todd Kashdan and Robert Biswas-Diener**

_The Upside of Your Dark Side_ looks into the darkest depths of the human psyche, only to discover that the painful emotions that we often wish we could just make go away — anger, anxiety, guilt — are sometimes the key to our success. Backed by many fascinating scientific studies, _The Upside of Your Dark Side_ makes it clear that psychological health means wholeness rather than happiness.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Kelly McGonigal

Kelly McGonigal is a psychologist at Stanford University and a leader in the field of "science help," which applies research in psychology, neuroscience and medicine to situations in everyday life. She is the author of international bestseller _The Willpower Instinct_.

