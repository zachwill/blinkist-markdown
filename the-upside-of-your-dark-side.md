---
id: 547c8ed06363300009840400
slug: the-upside-of-your-dark-side-en
published_date: 2014-12-04T00:00:00.000+00:00
author: Todd Kashdan and Robert Biswas-Diener
title: The Upside of Your Dark Side
subtitle: Why Being Your Whole Self – Not Just  Your "Good" Self – Drives Success and Fulfillment
main_color: E74F2E
text_color: E74F2E
---

# The Upside of Your Dark Side

_Why Being Your Whole Self – Not Just  Your "Good" Self – Drives Success and Fulfillment_

**Todd Kashdan and Robert Biswas-Diener**

_The Upside of Your Dark Side_ looks into the darkest depths of the human psyche, only to discover that the painful emotions that we often wish we could just make go away — anger, anxiety, guilt — are sometimes the key to our success. Backed by many fascinating scientific studies, _The Upside of Your Dark Side_ makes it clear that psychological health means wholeness rather than happiness.

---
### 1. What’s in it for me? Discover why it’s good to be a little bad. 

For many decades, clinical psychologists have explored human weakness and the causes of emotional suffering. With the rise of positive psychology, the focus began to shift from avoiding pain to pursuing happiness through attempts at obtaining the _good life_. 

As you'll soon discover, happiness isn't everything. In fact, by obsessing over happiness at the expense of our other, darker emotions, we actually do ourselves a great disservice.

Indeed, the so-called "good life" can never be achieved without the benefits that come from emotional pain, bad feelings and the dark sides of the human condition.

In these binks, you'll learn all about how negative emotions can actually lead to greater insights, stronger leadership and a more successful life.

In these blinks, you'll learn

  * why you'd be much better off with an angry lawyer by your side than a happy one;

  * why an anxious person is more likely to notice a fire on a train; and

  * how a dash of psychopathy is one of a potential president's greatest character traits.

### 2. Your happiness can interfere with your performance and accuracy. 

Nowadays, management goes by the mantra that an upbeat mood translates to business success. But while mirthful and content people _do_ get better customer and supervisor evaluations, that doesn't objectively mean that they are performing better.

For instance, while they might be likeable, happy people are less persuasive.

If you want to persuade others, then you have to communicate your message in a detailed and concrete way. However, happy people tend to focus on the big picture at the expense of the tiny details. Consequently, their arguments are less nuanced and concrete.

Indeed, this can be seen in a number of studies on the persuasiveness of upbeat people:

In these studies, "happy" and "unhappy" people were asked to create arguments about issues ranging from the allocation of tax dollars to the existence of soul mates. In all these studies, unhappy people's reasoning was judged as 25-percent more convincing than those of happier participants.

What's more, happy people are more easily deceived and more likely to "recall" false facts than others.

As we've already seen, people tend to be less interested in details when they're happy. But if you want to spot deceit, then you'll have to look for very subtle cues. This became obvious in one study in which participants were asked to identify liars and honest people in a series of videotapes:

The tapes showed people denying that they had stolen something, only half of whom were telling the truth. Happy participants were able to identify the liars only 49 percent of the time, while unhappy people detected 62 percent of the frauds.

In other experiments in which people had to recall facts or words they'd been presented with before, the happiest among them were the most prone to "recall" items they hadn't even been shown!

Clearly, happy people are both less persuasive and more gullible. So, how happy do you want your lawyer to be?

### 3. The pursuit of happiness can keep you from being happy. 

Happiness feels so good that it's only natural to do whatever you can to increase it. Unfortunately, the more you try to brighten your spirits, the worse your mood becomes!

Indeed, the pursuit of happiness can prevent you from feeling happy in the short term, as people who focus on achieving happiness get less joy out of the pleasurable experiences that are supposed to create happiness.

Take this experiment, for example, in which people were divided into different groups before listening to Stravinsky's _Rite of Spring_. One group was asked to raise their happiness level while listening, or in other words, to wilfully increase their pleasure from listening, _and_ estimate how well that worked. Another group was instructed to just listen.

Participants who only listened got the most out of the experience, enjoying it 7.5 times as much as those who were instructed to try to be happy and evaluate their happiness.

This study suggests that happiness slips away if you try to grab it — just like a bar of soap in the tub.

What's more, striving to be happy can also make you unhappy in the mid-term. The pursuit of happiness over all else is inherently self-centered, and this myopic focus on your own happiness can distract you from other people's needs. This, in turn, can interfere with your relationships.

For instance, if you're dedicated to increasing your own happiness, you might avoid contact with a troubled friend in order to prevent her problems from negatively affecting you. As a consequence, this person may consider you less of a friend, and thus choose to keep her distance.

The longer this continues, the more likely you'll end up alone! In fact, research indicates that people who value the pursuit of happiness actually feel lonelier than other people.

Paradoxically, relationships — like friendships, romance and family — are among the things that make us happy. Happiness-consciousness, therefore, in valuing happiness over relationships, only makes us unhappy in the long run.

Apparently, happiness itself can impede your success. Our following blinks will look at more negative emotions, such as anger, anxiety or guilt.

### 4. Anxiety enables us to react quickly in the face of danger. 

Wouldn't it be great to rid yourself of your anxiety, once and for all? Maybe not! Without anxiety, you'd lose your love for roller coaster rides and scary movies. Apparently not _all_ anxiety is undesirable.

In fact, anxiety is quite beneficial to us in that it helps prepare us for dangerous situations. As you'll see, this has consequences in many areas of life:

For starters, anxious people are far more vigilant than their peers. In part, it's because anxiety stimulates the brain, thus making you more alert. Whereas non-anxious people tend to ignore ambiguous and even overt signs of potential danger, anxious people are highly attentive to these cues.

Not only do anxious people take greater notice, their anxiety also heightens perception:

For example, research has shown that people can see greater distances when they're anxious, and even enjoy a sharper sense of hearing.

Furthermore, anxious people are more likely to react swiftly and to try to find solutions when they perceive trouble.

Typically, anxious people suffer the most from a threat, as their anxiety makes their perception of negative events more intense. As a result, they're more motivated to gather important information and persist in finding a solution.

Imagine, for example, that a group of students is returning by train from a school trip. Suddenly, a slight but ominous scent of burning plastic begins wafting through the cabin. An anxious student or teacher would waste no time in searching for the source of the scent and alerting other passengers if needed.

So, if something is amiss, the class has a better chance of survival as long as there are anxious students and teachers among them!

> _"You need an anxious person on your team."_

### 5. Anger can enhance your creativity and increase your authority. 

Angry teens can come up with strikingly original (and sometimes offensive) epithets for the objects of their anger. As you'll soon see, the source of this originality is anger itself! As a matter of fact, you can even reap benefits from _someone else's_ anger.

For example, another person's anger can crank up your creativity. Specifically, receiving angry feedback increases both the quality of your ideas and their originality — that is, if you're the kind of person who needs to feel in control of situations.

We can see this in action in one experiment, in which participants were instructed to come up with as many possible uses for a brick as they could. However, before beginning their task, some of the participants had received very angry feedback on another assignment.

The results of the experiment showed that this angry feedback enhanced the creativity of those participants who preferred to feel in control. They found more uses for the brick than those who had previously been given a neutral feedback. What's more, their ideas were much more original — ideas such as carrying the brick around in a backpack as a way to exercise.

These encouraging results contrast with those from the non-control-freaks, who fared slightly worse after receiving angry feedback.

In addition, anger can enhance your authority, as angry people are often viewed as being more powerful than their happier counterparts. Consequently, demonstrating anger can give you leverage during negotiations.

Take this experiment, for example, in which participants were supposed to sell mobile phones for the highest price they could manage. Interestingly, the results varied dramatically depending on whether their negotiating partner, i.e., the buyer, appeared angry or neutral: participants were willing to sell their phones at a much lower price to buyers who appeared angry.

Moreover, occasional outbursts of anger can be an effective means of strengthening your authority. For example, in a study of construction managers, many interviewees reported that selective angry outbursts had proven effective at motivating an ineffective team to cooperate again.

> _"Anger is a powerful tool that is maligned by the mistaken notion that a healthy society is an anger-free society."_

### 6. Guilt can turn us into better people. Shame, on the other hand, is not so helpful. 

Can you describe the feeling of guilt? For most, it's not an emotion that we enjoy exploring. And yet, it's also an emotion that offers us all great benefits:

Guilt motivates people to repair the damage they've done. In a study about prison inmates, researchers found that criminals who were prone to experiencing guilt suffered more from what they had done than those who didn't feel guilty.

As a result, they were more likely to attempt to make amends and fix the problems they caused where possible.

In addition, the propensity to experience guilt seems to help us to respect moral boundaries. Guilt is a terrible feeling, and consequently, we have a strong motivation to avoid it and thus avoid the actions that might result in feelings of guilt.

In fact, researchers have found that adults who are prone to react with strong feelings of guilt are less likely to demonstrate certain harmful behaviors, such as using illegal drugs, drunk driving, assaulting others or theft.

This same aversion to harmful behavior can be observed in the guilty prisoners mentioned earlier: once released, they were much less likely to be arrested again in comparison to their less conscientious peers. 

Shame, on the other hand, does not provide us with the same benefits as guilt.

Depending on the context, every emotion can be a blessing or a curse. However, shame tends to cause us problems more often than not.

When we feel shame, we tend to want to hide the source of our shame. Whereas guilt causes us to want to take responsibility and repair the damage we've done — even when there is no one looking — shame makes us want to sweep our misdeeds under the rug in order to preserve our reputation.

Yet, while it can be good to feel bad, there are some character traits that are just irredeemably bad, right? Our next few blinks will answer that question.

### 7. Narcissistic traits can make you productive, creative and successful. 

What are some character traits that you consider "bad?" How about excessive admiration-seeking or a grandiose sense of self-importance and entitlement? To many, these traits are less than admirable, and all of them are hallmarks of a narcissistic personality.

However, these "bad" traits can also be beneficial to your success!

For instance, narcissistic people are more likely to reach the ambitious goals they've set for themselves. When narcissists want something, they feel entitled to pursue it without regret or second thoughts. For them, the mere desire to obtain something is a sufficient reason to fulfill that goal.

A narcissistic reporter, for example, who learns about a scandal may be so intent on getting the scoop that he won't bother wasting a thought on the privacy rights of the people he'll expose. 

In addition, narcissists' grandiose vision of themselves makes them unlikely to become impeded by self-doubts, even in the face of an imposing goal or when the odds are stacked against them.

Finally, believing that they're entitled to success, narcissists are willing to give anything and everything to succeed, stand out and win the admiration of others.

These character traits, while certainly off-putting, can be great motivators when approaching spectacularly difficult tasks, such as deciphering the human genome or interviewing for a very demanding role.

Furthermore, narcissism can enhance your creativity. In order to find novel solutions to complex problems, you need to be able to challenge assumptions about how things are supposed to be.

Since narcissists are so sure of their individuality and brilliance, they pay little attention to common conventions and the assumptions of other — i.e., lesser — people. As a result, they're more likely to consider ideas that others might dismiss as strange, eccentric or inappropriate, which then provides them with considerable creative freedom.

Clearly, narcissistic behavior can offer at least some benefits, both to individuals and societies. But what about other, more seemingly sinister traits, such as psychopathy? Aren't psychopaths those cold-hearted, cannibalistic _Hannibal-Lecter_ -types?

### 8. Psychopathic traits can make you a “better” leader. 

Popular culture portrays psychopaths as cruelty incarnate — they're the Hannibal Lecters and Patrick Batemans, whose bloody antics haunt our dreams. In real life, however, the village psychopath is likely to be a policeman, doctor or even toy developer, with absolutely no inclination to kill, threaten or abuse.

In psychology, psychopathy is characterized by overconfidence, a lack of shame and remorse, shallow emotions, blaming others for one's own faults and a willingness to lie or manipulate whenever it serves selfish interests.

That's quite a laundry list of what most would consider negative personality traits. However, psychopathic behavior can actually save lives:

Psychopaths, for instance, are better at handling crises.

In difficult situations — a terrorist attack, fist fight, natural disaster or the like — non-psychopaths tend to become overwhelmed with strong negative emotions such as fear or anger which leave us in a state of paralysis, unable to do what must be done.

Psychopaths, on the other hand, have no problem emotionally detaching themselves from a situation. Not only are their emotions more shallow to begin with; they also have an uncanny ability to dial down their emotional responses.

For example, in a messy hostage situation, it may be a psychopathic policeman who rescues the hostages. His fearlessness will help him to keep a clear head and work out the best strategy — as long as it is in his self-interest.

We can even measure the success of psychopaths by examining the success of American presidents:

In a study, 121 experts were asked to evaluate the presidents' personality traits and leadership performance. Afterwards, the researchers took this data and estimated both the leadership performance and psychopathic traits from each president's biography.

Interestingly, those with more psychopathic traits were better leadership performers, including historical icons such as Theodore Roosevelt and John F. Kennedy. These presidents proved to be more persuasive, more willing to take risks, more likely to keep a positive relationship with Congress and more adept at handling crises than their non-psychopathic counterparts.

But today's pop-psychology doesn't just focus on pursuing happiness. In our final blinks, you'll learn about whether the oft-extolled practice of "mindfulness" is actually conducive to success and happiness.

### 9. We can’t be mindful about everything. 

Mindfulness seems to be today's self-help buzzword. So many of today's books teach us how to "live in the moment" and observe what's happening without judgment. Moreover, research shows that practicing mindfulness improves your physical and mental health, makes life feel more meaningful and might even make your brain grow.

So why aren't we more mindful?

In short, our conscious minds have limited resources: our brains offer many more pathways for unconscious processes than for conscious, mindful processes. As a result, our brains can process large amounts of data unconsciously, but can't handle much more than a few bits of information consciously.

You can think of your consciousness as a bank account: you can only invest so much at any given moment.

Consequently, if you want to mindfully engage a complicated task, you'll have to _pay in installments_. In other words, you'll have to go very slowly, step by step.

Yet, the world is full of intricate information. In fact, each and every situation is comprised of multiple layers of interoperating data.

Just think about the vast amount of sensory data that your body is processing at this very moment: there's your mood and physical surroundings, the people around you, their moods, the ads on the wall, etc.

Even something as simple as looking at someone's face is a deeply intricate task:

Your subconscious checks whether that face belongs to someone you know and whether it appears happy or sad, friendly or menacing. Then you have to deal with the constant stream of new information as her expression changes over time.

If you know the person, you'll recall her name, the nature of your relationship and whatever else you know about her. You may also draw up a quick list of topics you could talk about.

In short, there's simply too much data out there to make continued mindfulness a realistic strategy. Certainly there are moments when you can be mindful, but it comes at the price of your limited conscious resources.

### 10. Mindless processing can lead to superior performance and better decisions. 

So, what's the opposite of mindfulness? That would be _mindlessness_, which includes everything from absentmindedness to impulsive behavior to intuition. In other words, it's acting without preliminary consideration.

But why would anyone want to act mindlessly?

Paradoxically, when faced with complicated situations, mindless processing actually leads to better decisions.

As you've already seen, our unconscious minds have far more computing power available than our conscious minds do. Thus, mindless processing allows you to handle larger amounts of data more quickly, which in turn can lead to better decision-making.

This conclusion was verified in a study in which trained psychologists were asked to diagnose mental patients based solely on their case files. After reading through the files, half of the experts had the chance to contemplate the data from the file for four minutes, while the other half was distracted by a crossword puzzle before making their diagnosis.

The results: the distracted psychologists were _five times_ as accurate as those who'd had the chance to deliberate on their diagnoses!

Furthermore, mindlessness also promotes creativity.

Your conscious mind tries its best to fit information into pre-defined, sensible categories based on your experiences. As a result, it may consider unusual ideas to be either off-limits or useless.

However, when your conscious mind is "off duty" or focusing on something else, your unconscious mind has the opportunity to combine information in interesting, new ways. This novel synthesis of information can lead to inspiring new ideas even your conscious mind deems acceptable (but could not come up with on its own).

For example, research into the most stunning PR ideas revealed that most of these ideas popped up while the interviewees weren't even focusing on work, and were instead doing mindless things such as commuting home or taking a shower.

Clearly, mindlessness deserves our respect and consideration. Without it, we wouldn't be able to have the creative breakthroughs that are so often responsible for our success.

### 11. Final summary 

The key message in this book:

**While happy, mindful and nice people are often held as shining examples for how to behave, this portrayal only tells half of the story. In fact, our negative emotions are there for a reason: they give us guidance, motivate us to pursue our goals and keep us out of the way of trouble.**

Actionable advice:

**Make snap judgments.**

The next time you're at the store and can't decide between brand-name or off-brand cereal, give yourself no more than 15 seconds to decide. By giving yourself a deadline, you force yourself to make an intuitive, "mindless" decision — exactly the kind that's been shown to deliver the best outcomes. By getting in the habit of making snap judgments on the small decisions, you are training yourself to use this method on bigger, life-changing ones, too.

**Suggested further reading: _The_ _Wisdom of Psychopaths_ by Kevin Dutton**

Not all psychopaths are locked away in maximum-security prisons and mental hospitals. Many of them live among us, in the midst of society. Indeed, a great number of highly successful political and financial leaders exhibit psychopathic traits. This book investigates why they are so successful, what makes them different from psychopathic criminals and what all of us can learn from them.
---

### Todd Kashdan and Robert Biswas-Diener

Professor Todd Kashdan is a widely recognized expert on anxiety, self-regulation and well-being, who has published more than 150 scholarly articles. In addition to working as a public speaker, he has also won numerous academic awards and wrote the critically acclaimed book _Curious_.

Dr. Robert Biswas-Diener is a positive psychologist who has published numerous scholarly articles and conducted research all around the world. He is currently the managing director of _Positive Acorn_, and his book _Happiness_ won the PROSE Award in 2008.

