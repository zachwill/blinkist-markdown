---
id: 59a301a2b238e10006a129af
slug: the-upstarts-en
published_date: 2017-09-01T00:00:00.000+00:00
author: Brad Stone
title: The Upstarts
subtitle: How Uber, Airbnb and the Killer Companies of the New Silicon Valley are Changing the World
main_color: 27AEC2
text_color: 1D808F
---

# The Upstarts

_How Uber, Airbnb and the Killer Companies of the New Silicon Valley are Changing the World_

**Brad Stone**

_The Upstarts_ (2017) is about the new generation of start-ups in tech, in particular, Airbnb and Uber, and the impact they've had on the world. It takes a deep look at the people behind the companies and how they got to where they are today, as well as where they might go from here.

---
### 1. What’s in it for me? Learn the history of two huge tech upstarts. 

As you probably know, start-ups tend to be new, small companies with innovative business ideas. Google, Facebook and eBay were all start-ups once. In the past few years, new start-ups have begun popping up everywhere. But at some point, a business stops being a start-up and becomes something else, something bigger. We call them _upstarts_.

In these blinks, we'll look at two upstarts that have become household names: Uber and Airbnb. Both have challenged and pioneered new ways of doing business, taking the personal transportation and hospitality industries by storm.

So what has made these upstarts what they are today, and where will they go from here?

In these blinks, you'll find out

  * how the personality of the CEO can influence the way a company does business;

  * how a competitor forced Uber to reinvent itself; and

  * what Uber and Airbnb should do in the future.

### 2. The term “upstart” is used for mega start-ups like Airbnb and Uber. 

Companies like Airbnb and Uber that have grown rapidly to become phenomenally successful are hardly start-ups anymore. They've shown they like to tussle with the big boys, and they've paid scant respect to the usual ways of doing business. We can call such companies _upstarts_.

Both Airbnb and Uber are renowned all over the world and business is booming. And that's despite the fact that unlike many other major companies, they don't actually have any physical assets.

Airbnb might at first glance appear to act like the world's largest hotel company. Except, of course, it doesn't own the rooms that get rented out.

Equally, we think of Uber as the world's largest car service. But it doesn't even have its own fleet.

In part, the freedom granted by this lack of assets helps explain how these companies have been able to expand so far and so quickly. In the world of tech, speed and flexibility are crucial to success.

We also shouldn't underestimate just how massive these businesses are. They're what are known as _unicorns_, start-up giants worth more than a billion dollars.

In 2016, Airbnb was valued at $30 billion, and Uber at $68 billion.

Airbnb's meteoric rise was largely assisted by venture capital firm Sequoia Capital, one of its first big investors. In turn, Sequoia Capital has reaped huge returns. In fact, the amounts it earns from similar investments in Google and WhatsApp are chickenfeed in comparison.

But of course, to become this successful requires a lot of effort and sometimes a rather ruthless approach to business _._

### 3. Uber’s constant reinvention has made it a global success, even when it doesn’t win contracts. 

Before Uber became successful, the idea of a business that encouraged you to ride in strangers' cars seemed ridiculous. So how did Uber manage it?

The company actually began as a town car service in San Francisco, which had a notoriously terrible transport situation. Finding a taxi there was almost impossible, as there wasn't enough supply to meet demand.

Uber, therefore, started a reliable car service for the city's young professionals, and style was a key part of the business model. It seemed cool that you didn't need to hang around waiting for a cab.

In time, other ride-sharing services, like Lyft, entered the market. At the time, Uber had already established itself in a few cities, but now under threat, the company realized it needed to branch out into the taxi service too. Thanks to its excellent app and strong user base, this expansion proved successful.

This was when Kalanick realized that Uber should keep its image fluid, responding to changing trends to stay fresh and relevant.

But Kalanick dreamed bigger still and lead Uber to success abroad too, especially in Europe. Incredibly, he even almost succeeded in getting a monopoly in the challenging Chinese market.

However, he was thwarted by Uber's Chinese equivalent, Didi Chuxing. Didi Chuxing had more capital and a broader reach in China than Uber, operating in 400 cities compared to Uber's 100. Seeing he couldn't win, Kalanick agreed to a deal where Uber withdrew from the Chinese market and sold its Chinese operations to Didi Chuxing in return for a 17 percent stake in the company.

So although Kalanick didn't actually get Uber into China, he still made billions of dollars from the Chinese market, which cemented his reputation as a phenomenon in tech. What's more, this added capital meant that he now had the means to grow Uber even faster.

Clearly, Kalanick's risk-taking was a major factor in his company's success. But as you'll find out later, it also meant Uber sometimes courted controversy.

### 4. Airbnb has grown from one man’s idea into a powerhouse in the hospitality industry. 

Just as climbing into a stranger's car went from being unthinkable to routine, so has paying to sleep in a stranger's house become commonplace. Nowadays, thanks to Airbnb, many tourists see this as a perfectly acceptable way to find accommodation.

Airbnb arose from necessity and humble beginnings. One of the company's founders, Brian Chesky, was busy working on other projects and simply couldn't afford to pay his rent. So he decided to instead sleep on a friend's sofa and pay him just a bit.

That's when a lightbulb went off in his head. Hosts could rent out spare rooms in their houses and apartments, making some money while visitors saved on accommodation costs compared to hotels.

For guests, another attraction is the authenticity of staying at an Airbnb. They can connect with locals and see and experience things that most tourists miss.

The founders felt this notion was particularly inspirational, and Airbnb's slogan became "Belong anywhere," implying you'll always feel at home, wherever you go.

Eventually, Airbnb grew into a major player in the travel industry, and in August 2016, the company had its biggest night on record, with 1.8 million guests staying in Airbnb rooms all over the globe. That's more than all the rooms of the entire Marriott Hotel Group, the world's largest hotel company!

Almost no one had predicted this.

In fact initially, no investor wanted to go near the concept, fearing that safety issues would discourage people from using the service.

But, even though there have been a few mishaps, broadly speaking Airbnb has proved that we can trust in our fellow humans after all.

Clearly, Airbnb and Uber have brought consumers some great innovations. But it hasn't always been a smooth path.

### 5. The CEOs of Uber and Airbnb are determined, but sometimes court controversy. 

Bill Gates and Mark Zuckerberg will be remembered as two giants of the entrepreneurial world. But both are shy and softly spoken, not the stereotypical brash and fast-talking CEOs.

The CEOs and co-founders of Uber and Airbnb, Travis Kalanick and Brian Chesky, are a different kettle of fish. Both are charismatic and sharp speakers.

Famously, Kalanick clashed with Christiane Hayashi, then director of San Francisco's Taxis and Accessible Services Division, who tried to block Uber's early expansion in San Francisco. The taxi drivers Hayashi served were up in arms because while they were heavily regulated, Kalanick's company seemed to dodge every rule in the book. At one particularly heated meeting, Hayashi felt the Uber executives were "obnoxious," while Kalanick described Hayashi as full of "deep anger" and "screaming." His willingness to wage this war made it clear that Kalanick was not afraid to court controversy.

On the other hand, his style can also be inspiring. Fans of Uber rallied in his support when the company was locked in battle with local government bodies in places like New York City.

Meanwhile, Airbnb's Brian Chesky was so charismatic that after speaking once at a conference hosted by the investment bank Allen and Company, he found himself invited to the bank's yearly soiree. There he worked his magic and could promote Airbnb to real powerbrokers.

However, although Airbnb and Uber have connected people in bold new ways, their methods are controversial.

Uber, for instance, doesn't actually employ its drivers, who are technically independent contractors. This means the company avoids having to provide benefits such as health insurance, which employees of large companies are entitled to.

And on one occasion, Airbnb publicly promised to compensate a host known as "EJ," whose apartment had been wrecked by Airbnb guests. However, the company didn't actually deliver on this promise, and so EJ mounted an online campaign to put public pressure on it. Eventually, the controversy forced Airbnb to change its insurance conditions for hosts.

The lesson is clear. A CEO's ability to charm and force a deal can get them to the top. But it'll likely make a few enemies along the way too.

### 6. Upstarts should strive to stay true to their early idealistic visions. 

It's easy to be idealistic when you start a company because you have nothing to lose. The real challenge is to stay idealistic once you've attained fame, fortune and success.

So can Airbnb and Uber stay human and not turn into heartless money-making machines?

In their early days, Airbnb was targeted by the notorious Samwer brothers, who made a living by cloning apps and cannibalizing the original companies' market, thus forcing them to buy the clone.

But Airbnb didn't take the bait and instead reasoned that the best way to beat the competition was to make the best app. And it worked — eventually, the Samwers' clone, Wimdu, went under.

Airbnb also works hard to maintain their unique working atmosphere, which originally arose from principles like always carefully discussing decisions and the idea that everyone counts. But the company should still keep in mind their encounter with the Samwer brothers so that it never becomes as ruthless as them.

Meanwhile, Uber's reluctance to let its workers unionize or enjoy proper working conditions is disturbing. It shows that the company has become disconnected from its origins and earlier community advocates. It owes its drivers fair treatment.

As the saying goes, with great power, comes great responsibility.

These two companies now essentially represent the future of business. Kalanick and Chesky have the opportunity to shape a new generation's way of thinking, and their wealth allows them to influence future decision making.

The risk is, of course, that they, like old-school corporations, may succumb to greed or a desire for more power through conformity.

We don't yet know what the lasting global effects of Airbnb, Uber and the other upstarts will be. But we can hope that they will stay true to their original ethos.

> _"It's up to us to hold them to their promises . . . They are the new architects of the twenty-first century, every bit as powerful as political leaders."_

### 7. Final summary 

The key message in this book:

**Airbnb and Uber are the product of two driven, charismatic entrepreneurs and their equally impressive colleagues. These two companies have the potential to do great things on a global scale, but we must be careful that they don't abuse the power they have garnered.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Raw Deal_** **by Steven Hill**

_Raw Deal_ (2015) reveals the ugly truth behind the new sharing economy and the harm that companies like Uber or Airbnb are inflicting upon societies around the world. There's a major crisis on the horizon, and it will affect not only these companies' exploited employees. We're all at risk, and we'll need to choose our next steps wisely to prevent an economic collapse.
---

### Brad Stone

Brad Stone is the senior executive editor of the global technology group at Bloomberg News and a _New York Times_ best-selling author who frequently writes about Silicon Valley. He also wrote _The Everything Store: Jeff Bezos and the Age of Amazon_.

