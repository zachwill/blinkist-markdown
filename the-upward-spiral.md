---
id: 58c6ad6c8d900300040725f3
slug: the-upward-spiral-en
published_date: 2017-03-16T00:00:00.000+00:00
author: Alex Korb PhD
title: The Upward Spiral
subtitle: Using Neuroscience to Reverse the Course of Depression, One Small Change at a Time
main_color: EB682F
text_color: D15D2A
---

# The Upward Spiral

_Using Neuroscience to Reverse the Course of Depression, One Small Change at a Time_

**Alex Korb PhD**

_The Upward Spiral_ (2015) details the ways your brain can fall into a progressively worsening depression and what you can do to reverse the process. These blinks explain the material conditions that cause your mood to slump and offer concrete steps for overcoming depression, anxiety and worry.

---
### 1. What’s in it for me? Escape the downward spiral of depression. 

Have you experienced moments when, no matter what you do, your bad mood only worsens? Part of you starts to wonder if it would be best to go directly back to bed, shut down mentally and chalk up the day as a loss. Since you have things to do you try to carry on, but with every action you take you fall deeper and deeper into a vicious cycle of fretfulness.

Such downward spirals can be the cause of depression. We get locked into them out of fear, anxiety and worry. And once we're in one, our brain does its bit to drag us further into distress.

These blinks are about escaping your downward spiral and getting back on your feet again. You will learn how your brain is wired and what you can do to escape depression. They will even teach you to create an upward spiral instead.

You'll also learn

  * about your reptile brain;

  * who (in your brain) is responsible for your bad habits; and

  * why hugging is so good for you.

### 2. The structure of the human brain explains how people fall into deep depressions. 

If you tell a friend that you're feeling depressed, he'll probably assume there must be an external cause for your mood and ask "why?" But if you want to understand the dark spiral of depression, you need to start somewhere else entirely.

To begin with, you can divide the brain into the _feeling_ brain and the _thinking_ brain.

The feeling brain is the _limbic system_. Also known as the _reptile brain_, it's composed of several older brain sections. These areas are responsible for making people feel emotions — particularly stress and anxiety.

Then there's the thinking part of the brain or the _prefrontal cortex_. It's located behind the forehead and is the newest development in the evolution of the human brain. In addition to playing a key role in determining human intelligence, it regulates the limbic system.

In normal conditions, the prefrontal cortex lets us think in an abstract way about negative emotions like guilt, shame or worry, and this creates some distance that helps us process them. When a person becomes depressed, the prefrontal cortex can no longer do its job properly, and emotions start getting out of control, triggering greater stress and anxiety.

So, a malfunctioning prefrontal cortex leads to depression and a downward spiral.

Just consider the author, who has a tendency toward loneliness, especially when he spends all day writing. He could make plans to meet a friend after work, but planning causes him stress. The worse he feels because of his loneliness, the harder it is for him to make plans, which only locks him deeper into the downward spiral.

In the worst case, such downward spirals can cause chronic depression, but the factors triggering them vary from person to person. So, while the author requires social contact to stave off a vicious circle of low moods, a friend of his falls into a downward spiral when she doesn't get enough exercise.

That being said, for many people, anxiety and worry are the main factors that cause a downward spiral. In the next blink, you'll learn more about these primary culprits.

### 3. Worry and anxiety can trap you in a downward spiral. 

Have you ever had a great plan but lost faith in it as you realized all the things that could go wrong? You start to worry and the more you think about the possible deficiencies in your plan, the more worried you get.

It's easy to see how you can get trapped in a fretful spiral, in which every plan you make is tinged by negativity. Interestingly enough, your prefrontal cortex, which is responsible for planning, is also in charge of worrying.

For instance, the author was once planning a dinner party. Everything was going fine until he remembered that he had to clean his apartment. So, he added that to his to-do list. Then he realized he needed to take a shower and that's when he began to worry: "What if dinner isn't ready when the guests arrive? What if I don't have time to shower? What if they think I live in a pigsty?"

By the time he'd snapped out of it, he had spent 20 minutes worrying and missed a message from his friends saying they would be 30 minutes late.

This story just goes to show that, once you fall into the loop — oftentimes triggered by the question, "what if it doesn't work out?" — it's difficult to pull yourself free. At this point, the prefrontal cortex is incapable of doing its important job of making plans and considering potential problems, because it's been disrupted by the stream of worries emerging from the limbic system. As a result, you only see the bad things that could occur.

Worry is caused by thinking about potential problems, but anxiety is different. In anxiety, these potential problems are experienced as though they're actually happening. Nonetheless, it too involves the limbic system — especially the parts responsible for fear. Anxiety only differs from fear in that fear is caused by real danger while anxiety is prompted by the potential for danger.

So, there's a distinction between anxiety and worrying, but they also exacerbate one another, which keeps people locked into the downward spiral.

### 4. Depression causes people to focus on negativity, prompting a downward spiral that bad habits exacerbate. 

Some people say that, for every negative comment you receive, you need several positive ones to smooth things over. This holds especially true for depressed people.

How come?

Well, when people are depressed, they focus primarily on the negative aspects of any event, which keeps them trapped in the downward spiral.

To understand this, it first helps to know that our brains pay greater attention to emotional events than to those with no personal resonance for us. For instance, if you see a picture of an apple, your brain likely won't respond strongly. But if you see a picture of a gun, aimed directly at you, it definitely would.

Some people are genetically wired to focus more on the negative aspects of events, a tendency that only increases when they are depressed. Take a 2014 study by the psychologist Robert Maniglio, in which participants looked at pictures of people with neutral facial expressions. The depressed people in the group were more likely to interpret them as sad.

This tendency to see the negative side of things keeps depressed people trapped in their downward spiral, but bad habits can also worsen such a situation. A good example is the author's friend, Billi. Billi is obese and eating helps him deal with stress. Of course, this habit makes his obesity worse, which makes him feel bad, thereby compelling him to eat even more.

The part of the human brain responsible for habits is called the _striatum_ ; it makes us repeat actions we find enjoyable. Unfortunately, this mechanism also works for bad habits like Billi's comfort eating.

How? Pleasurable activities like eating junk food, taking drugs or gambling release a chemical called _dopamine_ in the brain. A depressed brain has a reduced dopamine reaction, which means it requires more repetitions of those bad habits to get its chemical fix. And so the striatum prompts its owner to go on eating, drugging or gambling.

> _"The striatum is perfectly happy carrying out bad habit after bad habit with no regard to the long-term consequences."_

### 5. Exercise is a great way to combat depression and create an upward spiral. 

We're constantly being told how excellent exercise is for the body — and with just cause. But working out isn't just good for our waistlines, it's good for our emotional health as well. In fact, exercise fights depression on three levels: physically, mentally and socially.

Unfortunately, when people are depressed, they rarely feel like exercising. As they see everything through a negative filter, they assume physical activity won't help. But in reality, exercise combats all the symptoms of depression.

For instance, depression disrupts your sleep patterns, leaving you feeling lethargic and physically exhausted. Exercising will both improve your sleep and give you more energy.

On a mental level, exercise sharpens your mental acuity while reducing anxiety and stress, both of which are contributors to depression.

And finally, on a social level, exercise gets you out into the world, whether by running through the park or going to the gym.

But, if you can't manage to motivate your depressed mind to go for a lengthy run, don't sweat it. Every little bit of exercise will help you create an upward spiral.

When the author was working at UCLA, he was offered the opportunity to work from home. At first he loved it, but after a while he found himself just slobbing around on the couch, falling into a negative mind-set prompted by his loneliness.

When he noticed this trend, he started making small changes: he would take a walk after breakfast or go to his office on campus and use the university running track while he was there.

Every short spurt of exercise released positive chemicals that made the next step that much easier for him. In no time at all, he was pulling himself out of the downward spiral.

> _"Exercise is possibly the most straightforward and powerful way to start an upward spiral."_

### 6. Setting goals and making decisions reduces uncertainty and leads the way out of a downward spiral. 

Life is full of major questions about things like career changes and mortgages, and it's not uncommon to experience worry or anxiety when faced with such choices. The good news is that we can escape these spiraling emotions by making decisions and creating certainty.

Just consider an example from _Touching the Void_, a classic book about mountain climbing. The author, Joe Simpson, explains that when you're on the mountain, you need to make decisions, even if they're the wrong ones.

So, if there are two ways to go, you've got to pick one, despite the fact that it might mean backtracking later. If you stop making progress, you'll be lost when it gets dark or starts to snow. By making decisions, you open windows for success.

This logic also applies to life off the mountain and might be just the thing to guide you out of your downward spiral. Furthermore, making decisions means setting goals, which is another wonderful tool for creating an upward spiral.

That's because goal-setting releases chemicals in your brain that can lift you out of depression.

When you hold a long-term goal in your mind, your nerve cells release dopamine, which makes you feel good, not just when you achieve the final goal, but also when you move closer to it. However, for this to work, your goals need to be very precise. Only when your goals are dialed in will your brain know that you're moving in the right direction.

For example, if you want a new job, don't just set this as a goal. Instead, commit yourself to sending out five resumés per week. A smaller goal is specific, as well as manageable, and will boost your sense that you can actually achieve your main aim through incremental steps.

### 7. Improving your sleep will help you reduce stress and shake off depression. 

Sleep affects just about every aspect of your health, both physical and mental. Poor sleep causes increased blood pressure, worsens mood and impairs memory as well as the ability to learn. It's not surprising that it's a common symptom of depression.

If you get a good night's sleep, you'll feel better and think more clearly the next day. Great sleep increases your focus and improves your ability to make decisions. It even reduces stress, which in turn helps you sleep even more soundly.

So, how can you optimize your sleep?

The way your bedroom is arranged, the amount of exercise you undertake and the time at which you go to sleep — as well as wake up — all affect the quality of your sleep. Experts refer to the way you control your surroundings and actions before you clamber into bed as "sleep hygiene."

So establish a sleep routine that works for you and follow it consistently. Steering clear of screens not just when you're in bed but also for a while before you go to your bedroom will help. For example, your routine could be that you switch off all your devices, brush your teeth, wash your face and have a relaxing cup of herbal tea before hitting the sack.

It's also wise to steer clear of caffeine later in the day and avoid using alcohol to help you nod off. And remember, while the quality of sleep is essential, the quantity is also key. Most people need around seven to eight hours per night, so make sure you get at least that much.

### 8. Being grateful and seeking out social interactions activates positive circuits in your brain. 

When you were little, your parent or guardian probably taught you to always thank people. As an adult you can take this a step further and say "thank you" every day to express the sheer gratitude you have for life.

This is a powerful strategy for combating depression because, as you now know, depression makes everything seem negative, and gratitude is the polar opposite of negativity. Gratitude isn't dependant on your life situation; it's a state of mind. So, whether you're rich or poor, there's always something to be grateful for.

To practice gratitude, try keeping a gratitude journal. Each day, write down three things you're thankful for and make a habit out of doing so.

Many studies have illuminated the positive benefits of gratitude, but one of the most important things it does is to lift your mood. The brain has a limited focus, which means that when you pay attention to things that you're grateful for, negative thoughts like anxiety and worry are simply replaced by positive emotions.

If you're grateful for your relationships with other people, your social life will become more enjoyable and rewarding. Social contact triggers circuits in your brain that improve your mood; being around friends or family will make you feel better and even contact with strangers can lift your spirits. Just surrounding yourself with other people without even interacting directly with them can be a powerful antidote to depression.

Beyond that, there's also a social circuit in your brain that's responsible for releasing oxytocin, a hormone that promotes feelings of trust and reduces anxiety. You can easily trigger it by hugging, shaking hands or getting a massage.

### 9. Final summary 

The key message in this book:

**Depression is a complicated process caused by a variety of factors. Many people who experience depression find themselves in a downward spiral, in which everything they do makes them feel worse. Luckily, there are several things you can do to counter this and reverse the direction of this cascade.**

Actionable advice:

**Consult a therapist**

There's a lot you can do to improve your mental health on your own, but if you find that you can't pull yourself out of a depression, you should speak with a professional. Some people stigmatize seeking out mental health services but there's no reason to. After all, if you wanted to build a new house, you'd hire an architect. Similarly, depression calls for professional help.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Rise from Darkness_** **by Kristian Hall**

_Rise from Darkness: How to Overcome Depression through Cognitive Behavioral Therapy and Positive Psychology_ (2015) details techniques anyone can use to help overcome depression. These blinks explore the causes of depression and map out the near- and long-term strategies readers can use to develop a recovery program.
---

### Alex Korb PhD

Alex Korb PhD is a neuroscientist who studied at Brown University and UCLA. He's now conducting his post-doctoral research at UCLA and working as a scientific consultant for pharmaceutical and biotech companies.

