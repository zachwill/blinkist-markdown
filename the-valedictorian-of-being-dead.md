---
id: 5d9dce0e6cee070008284ce7
slug: the-valedictorian-of-being-dead-en
published_date: 2019-10-10T00:00:00.000+00:00
author: Heather B. Armstrong
title: The Valedictorian of Being Dead
subtitle: The True Story of Dying Ten Times to Live
main_color: DCCF2C
text_color: 8F861D
---

# The Valedictorian of Being Dead

_The True Story of Dying Ten Times to Live_

**Heather B. Armstrong**

_The Valedictorian of Being Dead_ (2019) explores one woman's battle with depression and the radical treatment she underwent to cure it. Shining a light on the realities of living with this debilitating mental health condition, these blinks chart Heather B. Armstrong's journey through hell and her inspirational recovery.

---
### 1. What’s in it for me? Join Heather B. Armstrong on the toughest journey of her life. 

In 2016, Heather B. Armstrong's life was falling apart. As if being a recently divorced single mother wasn't bad enough, she was also grappling with the most severe bout of depression she'd ever experienced. Overwhelmed with sadness and anxiety, she found getting out of bed a struggle and was plagued by constant thoughts of how nice it would be simply to slip away and be dead. 

And as morbid as it sounds, Heather eventually got her wish. In fact, in a bid to escape her depression once and for all, Heather would die a total of ten times. Luckily, these deaths were only temporary — part of a groundbreaking experimental treatment for depression that saw Heather put into a deep coma in an effort to reset her troubled mind. 

In these blinks, you'll join Heather on her journey of dying to live. At once a searingly personal memoir as well as a fascinating scientific study, these blinks lift the lid on depression, its causes and a potential cure. 

In these blinks, you'll discover 

  * what it feels like to experience near brain-death;

  * how severe depression impacts daily life; and

  * whether anesthesia can treat mental health disorders.

### 2. Heather had wished she was dead for 18 terrible months. 

As Heather B. Armstrong sat in her psychiatrist's office in early 2016, he could tell just by looking at her that she was seriously ill. Slumped in her chair, Heather resembled a zombie, with unwashed hair, a blank expression and dirty clothes. 

Heather, as her psychiatrist already knew, was seriously depressed. More worrying still, she had been feeling this way for a long time. In fact, Heather had spent the previous 18 months in the grip of a severe depressive episode. 

Her suffering would begin each morning when her alarm went off. As soon she heard it, a rush of anxiety would jolt through her body, making her gasp for breath. She likens it to waking up to your brain being on fire. As soon as she was awake, she would start panicking about every task that needed doing that day. Would she be able to get everything done on time — and do it all perfectly? 

After divorcing her husband two years earlier, Heather had struggled to cope as a single mother to her two young daughters, Leta and Marlo. Whenever something went wrong — whether her girls forgot their lunch boxes or their homework — Heather's negative inner voice would start whispering. She was a terrible mother, the voice said, whose children would be happier without her. And once these thoughts began, they usually spiraled into the most destructive thought of all: wouldn't it be better if she was dead? 

Overwhelmed with the demands of childcare and work, Heather spent almost every night crying down the phone to her mother. Often she made these calls from her bedroom closet, so her children couldn't hear her sobs. Heather cried on her own too — usually in the shower, when the mere sensation of the water hitting her skin felt like too much. 

Cruelly though, Heather's depression didn't just make her miserable, it sapped all her energy too. Most days, she couldn't muster the strength to comb her hair or put on fresh clothes. So she lived in the same outfit for days at a time, not even caring if it was stained with food. 

Heather's psychiatrist, Dr. Bushnell, thought her vicious cycle of depression had to change. And after making a quick phone call from his desk, he made an unexpected suggestion.

### 3. Heather was offered a radical treatment for her severe depression. 

Sitting with her psychiatrist that afternoon, Heather felt as she had for the last 18 months — utterly worthless. But ironically, to Dr. Bushnell and his colleagues, Heather's advanced mental illness made her a valuable asset. These doctors were conducting research into a new cure for depression. And in Dr. Bushnell's opinion, Heather was a perfect candidate for a treatment that invited patients to start dying to make life worth living again. 

When Heather asked for another prescription for her usual medication of valium that day, Dr. Bushnell had a more radical idea instead. How would she like to take part in a groundbreaking experimental treatment for depression, he asked — an experiment so groundbreaking, in fact, that it would require Heather to die. Not just once, but ten times over. 

Of course, Dr. Bushnell hastily explained, this dying would only be temporary. 

Heather would have an anesthetic called Propofol administered to her on ten separate occasions over a period of several weeks. This anesthetic would put her into a very deep coma, during which the electrical activity of her brain would be suppressed to the point of near brain-death. 

Think of a computer that you turn off and on again to get rid of a glitch: the doctors hoped that switching the brain "off" with the anesthetic would allow it to reset itself. When it switched back on again, in theory, the patient's depression would be cured. 

This approach might seem far-fetched, but it has been proven to work in another treatment for depression: _electroconvulsive therapy_ (ECT). During ECT, an electric current is passed through the brain, causing a similar suppression of brain activity. Sadly, ECT can also have devastating side effects, such as migraines and even irreversible loss of memory. The doctors hoped that in this experimental treatment, the anesthesia would provide similar relief to ECT but without the nasty side effects. 

Heather wasn't immediately sure that she wanted to die ten times. But later that evening, as she had her nightly cry down the phone to her mother, she decided she had to take action. The next day, she called Dr. Bushnell and agreed to the treatment.

### 4. Heather’s first treatment session was a painful and frightening experience. 

As she drove to the hospital for her first appointment, Heather was famished. Because she was receiving a general anesthetic, she had been instructed to fast for 20 hours beforehand. But if she thought the most uncomfortable thing about her first near-death experience would be hunger and thirst, she was mistaken. Because, unfortunately, during that session, things went very wrong. 

Heather's ordeal began when the clinic staff inserted the needle which would later be used for the anesthetic into her veins. It was several times bigger than an ordinary needle, and the staff struggled to put it in. Not only was it painful, but it also left Heather with severe bruising on her inner arms. 

When the gigantic needle was finally in, Heather was taken to the treatment room where she lay down on a gurney. Here, the experiment's lead researcher, Dr. Mickey, explained that the anesthetic would put her into a deep coma. Once she was unconscious, they would monitor the extent to which her brain activity had been suppressed by using something called the _bispectral index_. 

Patients being anesthetized for surgery would normally have their brain activity reduced to around 40 on this index, as this is the point at which one can no longer feel anything. However, Heather was being taken all the way down to just above zero — a whisker away from total brain death. Dr. Mickey explained that she would also be given a drug called Fentanyl, a potent opioid that would minimize the risk of her waking up from her coma with a headache. 

Heather nodded that she understood, and the staff administered the anesthetic and the Fentanyl. But in the moments before unconsciousness, she suffered terrifying hallucinations. As the drugs took effect, she saw the people around her melting like wax figures, their faces gruesomely distorted. She tried to scream, but her voice box had already been disabled by the anesthetic. 

Finally, the black curtain fell, and the coma took hold. 

To make matters worse, Heather awoke to find that the Fentanyl hadn't worked. She had a throbbing headache that only subsided after she had returned home from the hospital and collapsed into her bed to sleep for the rest of the day. 

Heather later found out she was one of just 4 percent of people in the world for whom Fentanyl causes delusions.

### 5. Heather’s depression has its roots in her childhood. 

Despite the horrors of her first treatment, Heather pushed on with the second and third session later that same week. Although she did not have hallucinations again once Fentanyl was removed from her drug regimen, the extreme tiredness continued in the hours and days after the next two sessions. During this first week, despite her exhaustion, Heather also took time to reflect on the roots of her depression. 

This bout of depression had begun just 18 months previously, but Heather had suffered from sporadic sadness and anxiety throughout her life. Even as a 16-year-old, she remembers her teacher warning her that she needed to learn to relax, or she would end up having a breakdown. 

But Heather couldn't relax.

Ever since childhood, she had felt an all-consuming need to be the best — the valedictorian — at absolutely everything. If she wasn't at the top of her class, she would worry that she was going to lose her family and end up living on the streets as a result. Looking back, Heather realizes that these destructive thoughts indicated that she had a depressed state of mind, even then. When you're depressed, your mind always goes straight to the worst-case scenario. 

Motivated by her fears of losing everyone and everything, Heather ignored her teacher's advice and pushed herself to the limit during her school years, eventually graduating as her high school valedictorian. But by her second year of college, the anxiety and the pressure she was putting herself under all became too much, and she dropped out. 

But where did this valedictorian urge come from? Heather feels that it may be partly due to her strict Mormon upbringing. In Heather's childhood home, expectations were high, and the penalty for mistakes was severe. 

Though her mother was loving, her father was an intimidating presence. He was extremely strict and had an explosive temper; one of her worst memories is of him storming into the room, pushing her against a wall and shouting that she needed to learn to talk to adults with more respect. 

Her crime? She had answered a question from her mother by saying "what" instead of "yes, Ma'am." Heather was just eight years old at the time. Another memory is of her mother crying and begging her father to stop as he ruthlessly disciplined Heather's brother. Perhaps unsurprisingly, Heather's brother has also suffered from severe depression as an adult.

### 6. Heather noticed big changes halfway through her treatment. 

Waking up from her third treatment session, Heather felt as tired and depressed as ever. But as she climbed into the back of her mother's car to go home, something amazing happened. Catching sight of her reflection in the rear window, she realized she had put makeup on that morning. This might seem unremarkable, but to Heather, it marked a major turning point. 

After all, during the last 18 months, Heather hadn't taken any care over her appearance at all. Not only did she lack the energy, but her depressive thoughts also dictated that, as she was doomed anyway, there was no point in looking nice. 

Thus, this small act of applying makeup was already highly significant. But things would really start to improve after the fifth session. 

After her fifth near-death experience, Heather arrived home without feeling tired. On the contrary — she felt more energetic than she had in months. When her mother offered to take her daughters out for dinner, Heather did something she hadn't done in a long time — she reached out to a friend. Then she drove over to his house and spent the evening drinking beer and chatting with him. Pretty amazing, considering she'd spent the last 18 months barely making any social contact at all, fobbing friends off with the excuse that she was "too busy" to see them. 

When her friend put some music on that night, Heather was in for another shock. For the first time in years, she felt like she could actually hear the music and feel it striking an emotional chord within her. Slowly, the numbness of her depression was lifting. As she headed home again, the thought struck her: maybe being dead wasn't what she wanted after all. In fact, she had never felt so alive. 

Visiting the hospital for her sixth treatment, the change in her mental state hit home. During her previous visit, she'd described herself as feeling sad nearly all the time. Now she suddenly couldn't remember having ever felt that way. Whereas before she had reported feeling that life was barely worth living, now she felt so happy that she had leaped out of bed that morning, showered, carefully styled her hair and sprayed herself with perfume on her way out.

> _"I was Dorothy opening the door of her house when it landed in Oz. The colors were nearly blinding."_

### 7. Even though she’s an adult now, Heather finds that her relationship with her father remains troubled. 

As Heather's depression began to lift, she found herself reflecting on her relationship with her parents. Her mother and stepfather had provided steadfast support throughout her treatment. Not only did they drive her to and from the hospital, but they also helped out with her kids. 

As Heather's relationship with her stepfather deepened, she considered how different her real father's response to her depression had been and how his attitude affected her. 

In essence, Heather's father always made it clear that he did not believe in depression. When Heather's mental health problems had seen her drop out of college, for example, he had wasted no time in telling her to stop feeling sorry for herself and just shake it off. 

Her father's own upbringing might shed some light on his cold attitude. Born into humble beginnings in a deprived neighborhood in Kentucky, Heather's father had pulled himself up by his bootstraps to become a successful manager at computer firm IBM. His attitude was, "If I can overcome hardship, so should the rest of my family." 

Tragically, while it grated on him that Heather claimed to suffer from depression, he found it even more unacceptable that his son did too. In his eyes, it was the height of weakness for a man to claim to be suffering from sadness. 

Unfortunately, despite everything she has been through, Heather thinks her father still does not believe that the brain can become ill in the same way that the body can. Like many people, he is simply skeptical about mental illness. 

Nonetheless, her father did come to watch one of Heather's treatment sessions and was in the room while she was put to sleep, and when she came round from the anesthetic. When Heather woke up, groggy and disoriented, she saw her father and mother watching her. As she fought to regain full consciousness, she was disappointed that her father sat there in silence the whole time without offering a single word of encouragement or sympathy. 

Nonetheless, her mother told her something later that moved her deeply. Though her father may not have said anything in the treatment room, he had actually spent the hour before Heather woke up gently stroking her forehead. 

Some people, it seems, struggle to put their empathy into words.

### 8. Heather now wants to bring wider attention to mental health issues. 

As of August 2018, 18 months after completing her experimental treatment, Heather B. Armstrong no longer wants to be dead. In fact, she hasn't suffered from depression at all since her ten near-death experiences. Furthermore, as soon as she got well again, Heather felt a calling to share her experiences with others in the form of a book. 

Heather thinks it's crucial that more people understand how it feels to suffer from depression — especially for single mothers. 

All too often, single parents are afraid to admit to mental-health problems because they're worried about having their children taken away from them. Heather stayed away from her psychiatrist for much of her depression for fear of being labeled an unfit mother and losing custody of her girls to her ex-husband. 

Heather's book was also a way for her to share the most inspirational part of her journey: her recovery. But was her recovery really as miraculous as it seemed? 

Although Heather puts her remarkable improvement down to the experimental treatment she received, the lead researcher on the experiment, Dr. Mickey, is more cautious. 

Dr. Mickey believes it's possible that Heather's episode of depression would have come to an end on its own, even without treatment. The first point he makes is that, just as bouts of depression can rear their ugly head suddenly, they can also dissipate rapidly again. 

Secondly, if his treatment really did help Heather, it might have been due to all the care she received during the sessions. After all, her family, friends and doctors were giving Heather extra attention during this period, and it's possible that this made Heather feel so good about herself that her depression lifted as a result. Lastly, he argues, it's even possible that Heather's routine fasting before each anesthesia may have had a beneficial effect on her mental health. 

One thing's for sure, though. These days, Heather has a new lease on life. She no longer feels that the world, or her children, would be better off without her. And that can only be a good thing. So to everyone struggling under the weight of depression and anxiety, Heather wants you to know that there can be a brighter tomorrow. It all starts with having the courage to reach out and ask for help.

### 9. Final summary 

The key message in these blinks:

**Heather B. Armstrong's life was almost destroyed by the depression and anxiety that had plagued her since childhood. She eventually plucked up the courage to try a radical new treatment. Incredibly, after being put into a medically induced coma ten times over, she is now free of depression and able to enjoy life once again.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _T_** ** _he Upward Spiral,_** **by Alex Korb.** ****

If you enjoyed reading about Heather B. Armstrong's experiences, then you're in good company! Author Alex Korb, PhD, had nothing but praise for _The Valedictorian of Being Dead_. And he should know. If you look at the blinks to his own book _The Upward Spiral_, you will understand why. These blinks offer key insights into Korb's understanding of how to escape the downward spiral of depression.

Detailing the myriad ways your brain can fall into progressively worsening depression, these blinks explain how you can take control and reverse this process. So for compelling insights into the causes of mood disorders and concrete advice on overcoming them, head on over to the blinks to _The Upward Spiral._
---

### Heather B. Armstrong

Heather B. Armstrong is one of the world's most prolific "mommy bloggers," and _Time_ magazine has named her website, dooce.com, as among the best 25 blogs in the world. In 2009, Armstrong featured in Forbes's list of the 30 most influential women in media. She is also the _New York Times_ best-selling author of _It Sucked and Then I Cried_ (2009).

