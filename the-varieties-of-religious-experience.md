---
id: 55e6130b7a20120009000000
slug: the-varieties-of-religious-experience-en
published_date: 2015-09-02T00:00:00.000+00:00
author: William James
title: The Varieties of Religious Experience
subtitle: A Study in Human Nature
main_color: 24B4B2
text_color: 1A8281
---

# The Varieties of Religious Experience

_A Study in Human Nature_

**William James**

Based on a series of lectures given by William James between 1901 and 1902, _The Varieties of Religious Experience_ (1902) is an in-depth exploration of how we experience religion and how a personal approach to religion can be profoundly useful to us.

---
### 1. What’s in it for me? Discover what can be learned from the many facets of religious experience. 

Worldwide, atheism is on the rise. However, an article published in the Washington Post, in 2012, article states that about 8 out of 10 people still identify with a religious group. Religion is an integral part of modern life — not only in people's daily lives, but in politics, too.

But if you strip away the churches, mosques and temples, the dogma and the holy books, what is religious experience? What is it on a deeper, more personal level? 

These blinks, based on a series of lectures given by the author near the turn of the 20th century, take you on a journey through the multifaceted world of religious experience. Among other things, the author explains why we simply can't understand human psychology without studying religious experience, and that these experiences can help cure mental illness.

In these blinks, you'll learn

  * what we can learn from the psychotic;

  * how religious experience cured Russian writer Leo Tolstoy's depression; and

  * what laughing gas can teach you about religious experience.

### 2. Studying religious experience sheds light on otherwise mysterious parts of human psychology. 

What springs to mind when you think of religion? A church or mosque? A prayer chanted or sung? Psychology may not be the first thing one thinks of, but it turns out that religious experiences has much to teach about the mind. Of course, science and philosophy have their place, but they simply can't explain everything in the world.

In the author's time, science and philosophy were burgeoning, and today we're witnessing even more developments in these fields. Yet science and philosophy simply aren't enough to explain everything that we experience and perceive. For instance, the way we see the world is subjective; no two people view it in exactly the same way. As we shall discover in the following blinks, we're filled with emotions and judgments that inform our unique perceptions.

Furthermore, religious experiences expose people to new emotions and guide them toward new conclusions. Such effects clearly bear on human psychology.

Some people might regard certain religious experiences as symptomatic of mental illness or instability, but regardless of whether they are or aren't, such experiences can still be immensely valuable and educational.

Take, for example, the founder of Quakerism, George Fox. Fox experienced revelatory visions; he saw the streets running with blood and had bizarre epiphanies that some would consider psychotic. But by drawing on all his experiences, Fox founded a religion that is alive and well today.

So what exactly is the value of religious experience? In order to answer this question, we have to separate the history, constitution and the origin of religion, and then extrapolate its importance and significance to us.

### 3. There is institutional religion and personal religion, and the latter is the basis of religious experience. 

Religion is all about churches, temples and people donning special garments, right? Well, there's more to it than that. Let's first take a look at the two main facets of religion: the institutional and the personal. 

Institutional religion involves systems of religious participation and is often not concerned with choice; rather, it's a result of the culture in which you grow up. For example, many of us recite the Lord's Prayer at a funeral because convention says we should. But the prayer itself isn't necessarily deeply significant to us.

In contrast, personal religion represents the source of our deep-rooted beliefs, those things we consider to be true. The world's great religions — such as the ancient Christian sects that were precursors to the institutional church — all aim to tap into this vein of personal truth. However, personal religion doesn't necessitate a belief in God or gods. For example, substituting a belief in science for a belief in deities is a personal religion of sorts, since non-belief, in this case, functions as an overarching personal truth.

But isn't personal religion similar to morality and philosophy, then? Well, no, as personal religion is rife with religious experience. From this point, we'll examine personal religion as the experiences, feelings and behaviors individuals have and act out when they feel connected to what they consider the divine.

Religion differs from morality also because morality is a product of logical reasoning and has no use for emotions. If we compare how Christians and Stoics handle life's challenges, this becomes clear. For the faithful Christian, problems are signposts of divine purpose and are therefore to be embraced gladly. The moral Stoic, on the other hand, deals with problems without complaint, yet doesn't see any purpose in doing so.

### 4. The unseen can be as real as the seen – and can affect the way we think. 

You likely have a notion of goodness, but can you picture it? Perhaps not. But the unseen can be just as real as the seen. Here's why.

We all believe in things that can't be seen. In fact, sometimes pictures in our mind seem even more real than what we actually see in front of us. For example, the memory of being insulted is often far more visceral than the actual experience of affront.

Religious belief may also be viewed as a belief in an _unseen order_ — something that we should adjust to, that shapes and informs our behavior. This means that even if we can't prove the existence of God, we can act _as if_ there was a God.

But this isn't only true for religion. For example, we act _as if_ we were free, perceive human nature _as if_ it was bursting with potential and, regardless of whether these beliefs are true or not, this unseen order guides our behavior. Our belief in these things makes them real.

Along with our actions, abstract and unseen ideas also affect the way we think.

All the things that we see give rise to (and are influenced by) our ideas of beauty, strength, truth, justice and so on. These ideas allow us to define our visible world.

Plato explained this in his _idea of forms_, which posits that each physical thing is but a copy of its absolute form. These forms can't be witnessed first hand; instead, we see them indirectly, like shadows cast on the wall of a cave. 

It's true that we sometimes believe in things even when rationality disproves them, and our sense of truth is based on how we construct our reality. So if someone believes in God, no rational argument could convince them that He isn't real.

> _"Instinct leads, intelligence does but follow."_

### 5. Religion can lead to a happy, healthy mind that can help cure its own illnesses. 

We've all heard the expression _mind over matter_. But what if this was absolutely accurate? And what makes you a healthy-minded person anyway?

Let's consider two types of people who achieve healthy-mindedness in different ways. The first are happy and healthy-minded by nature. Let's call them the _once-born_. Their minds simply don't experience bad in the world and so they struggle to find reasons _not_ to be happy. 

The poet Walt Whitman is a perfect example of this kind of person. His readers gush over the almost childlike joy he described when strolling through nature and the connectedness he felt from being with other people.

In contrast, the second type see the world as divided between good and bad. We'll refer to them as the _twice-born_. For these people, being healthy-minded means they need to defy the negative parts of the world, either by rejecting them, overcoming them or ignoring them.

While some belong to the first group of intrinsically happy and healthy-minded people, the majority of us are members of the second group and must _adopt_ a healthy-minded view of life.

Doing this has its merits. For example, many physical illnesses can be healed by healthy-minded perspectives on life. This idea goes against the scientific view of humans as passive objects in the physical world who fall ill due to external things like bacteria and viruses, and can in turn be cured by external things, such as medicine.

Yet, the alternative viewpoint is that there is a force within each of us that we can use to heal ourselves if we only open up to this through belief. This notion was already popular in the author's day; the mind-cure movement of then is similar to today's self help and mindfulness movement.

> _"What is called good is perfect and what is called bad is just as perfect"- Walt Whitman_

### 6. A sickly soul can lead to depression, but this can be alleviated by religious experience. 

The people who see both good and evil — the twice-born — don't all reach happy-mindedness. This is because experiencing the evil in life can lead to a _sickly soul_ filled with sinister thoughts. But how does this happen?

Well, first there are two types of sickly souls.

The first believes that there is evil in the world and that this makes us commit evil deeds. The way to deal with this and be absolved of guilt is to pray and hold onto hope. To the author, this is exemplified by the attitudes of people in Southern Europe and the Catholic Church. We may also confess our sins to find mercy.

The second type of sickly soul believes that sin is with us from birth and is inescapable. To the author this is related to Northern Europe and the Protestant way of life. On this view, we're fundamentally flawed and our negative actions cannot be exonerated. Believing this can lead to depressive illness.

There is hope, however, as religious experiences can help alleviate depression.

One example is author Leo Tolstoy, whose book _My Confession_ tells of his return to health after depression via his religious experience. He describes the deep despair he felt (he had suicidal tendencies to begin with) and how he was gradually able to overcome these feelings. First, he concluded that his idea of a meaningless life stemmed from the idea of a finite life; he tried to find meaning in anything that was finite, but always came up with nothing, and so this was also the value of life to him — zero.

His breakthrough came when he entertained the idea of an infinite, spiritual life. Reason alone couldn't solve the puzzle of existence; only belief and faith provided an answer: he wasn't tired of life itself, merely of the superfluous, artistic and higher-class life of which he was a part.

### 7. The soul of the twice-born is divided and that division can be bridged through religious experience. 

So twice-born people can't help but see the darker side of life. But is this necessarily problematic? 

Once-born people maintain the simple view of life as complete; they see the world, and themselves, as part of a benign unity.

The twice-born, however, acknowledge the division of good and bad outside and within themselves. The world, as well as the twice-born psyche, are divided into lower, base and higher feelings, and these are in continual conflict with one another. 

This conflict can be viewed as between the _ideal self_ — how we wish to be and act — and the _actual self_, the way we perceive ourselves acting in daily life.

However, this division can be healed in two ways. 

Just as the Ancient Greek medical teachings showed us, there are two ways to overcome illness or division: _lysis_, meaning gradual change, and _crisis_, an abrupt change.

We have already seen the gradual overcoming of the divided self in the story of Tolstoy and the healing of his soul.

So what about abrupt change? Saint Augustine of Hippo is a great example. Growing up in Carthage, Tunisia, in the 4th century, he became a follower of the religion _Manicheism_, a rival to Christianity at the time. 

Manicheism was centered around the idea of a world split between a good realm and an evil realm.

Saint Augustine struggled with the idea that everything was either good or bad, and one night, while in a garden, he heard a voice. It said, "read me." As he heard this, he stumbled upon a Bible. This was the beginning of the reunification of his divided self and his conversion to Christianity.

### 8. Conversion happens in two main ways and leads to new religious experiences. 

Saint Augustine's conversion arose from a feeling of conflict and crisis, but it's not the only way conversion happens. So how else can it occur?

First, we must understand that conversion comes from transferring power from one set of ideas to another.

Remember the ideas of beauty, truth and strength that we noted earlier? These ideas develop in our inner selves in a way that allows some ideas in particular to dominate over others. The author calls this _the habitual center of personal energy_ and when this center shifts from one group of ideas to another, conversion occurs. This is the process for any type of conversion, including, for example, changing one's political views. And so when new religious ideas are drawn to the habitual center, religious conversion occurs.

Although conversion is based on this shift, there are two ways it happens.

The first, which the author calls _volitional_, is voluntary and conscious. This is the kind of experience Tolstoy relates in _My Confession_.

The second is involuntary, subconscious and is known as _self-surrender_.

The way the two shifts happen can be illustrated by the way our mind works when we need to remember something, such as where we put our house keys.

If we go about it volitionally, we'll pause, think and retrace our steps until we find our keys.

The self-surrender method would be to not think about the keys for half an hour, letting our subconscious mind figure out where we put them.

In some select cases of self-surrender, conversion can also be instantaneous. This happens when thoughts and emotions have been trapped in the subconscious and are suddenly triggered by something, leading to a simultaneous shift.

Lastly, religious conversion brings new emotional experiences. These include a sense of well-being and genuine happiness, and the feeling that one understands new truths.

### 9. One result of religious experience is the development of characteristics we attribute to saints. 

After conversion through religious experience, what happens? One significant outcome is something we'll call saintliness.

This characteristic arises when our spiritual emotions become _the habitual center of personal energy_ — that is, the collection of ideas that dominate our behavior.

Saintliness comes with four distinct attitudes and beliefs about life.

The first is a feeling of being part of something bigger, along with a belief in an _ideal power_. In the popular monotheistic religions, this power is identified as God, but it could also be an idea such as beauty or truth. Second is a sense that this ideal power is in alignment with your life and that you can surrender to it. The third change involves a powerful feeling of bliss and freedom as the confining limitations of the self dissolve. Lastly, your attitude toward life becomes one of positivity and affection.

The feelings above set the foundation for the four respective characteristics in the saintly individual:

_Asceticism_ — surrendering to a higher power and finding pleasure in sacrifice and poverty.

_Strength of Soul_ — life expands as personal inhibitions fall away. Fears and anxiety are replaced by joyful calm. 

_Purity_ — a newfound dedication to seclusion and the avoidance of sensual experiences.

_Charity_ — an increased compassion for one's fellow beings and an embracement of everyone as family.

To see how these elements come together in one person, consider St. Francis of Assisi. Francis lived in voluntary poverty and _asceticism_ ; showed _charity_ when helping leprosy sufferers with the _strength of soul_ to risk infection and kiss them, and a strong desire to find solitude and _purity_, either in the hermitages or in nature.

> _"The saint loves his enemies, and treats loathsome beggars as his brothers."_

### 10. We ascribe value to saintliness and this value changes depending on time and place. 

Who would we call a saint today? Would modern saints be people like Francis of Assisi, or would they demonstrate different characteristics?

What we value does indeed change over time and these changes are based on their usefulness. For example, pagan gods were needed in order to explain natural phenomena like thunderstorms and earthquakes, but, as people became more educated, this gave way to Christianity.

From a modern point of view, the characteristics comprising classic saintliness may no longer apply. Excessive purity, for instance, is no longer a virtue. 

In addition, modern morality places involvement in human affairs at the forefront, while solitude and hermitism have lost their importance.

We also find that charity taken to an extreme is rarely useful and that loving and being kind to your enemy may even be dangerous. And yet charity inspires moral improvement and, in this way, has played a vital role in our social evolution. 

Then we have asceticism. This may feel like a quality suitable only to the distant past, when impoverished monks begged barefoot in the streets. But we can still gain wisdom from asceticism. Poverty, for instance, could teach us important lessons. In the modern age, society often frowns upon those who choose a simpler, more spiritual life over one dedicated to monetary gain. But such a choice should be celebrated.

We can indeed learn a lot from the saints and their saintliness. Even though some of the characteristics aren't held in as high esteem as they once were, they can still inspire us and embolden us to foster saintliness in ourselves.

### 11. Mysticism and mystical experiences are key ingredients in personal religion. 

Have you heard of people fainting in church, being touched by God, or even seeing Him? These are examples of mystical experience. But what exactly is a mystical experience? 

It can be explained by mystical states of consciousness, which consists of four traits.

The first is _ineffability_, or the impossibility of communicating what you experienced to others. 

The second is the _noetic quality_, the fact that these mystic states seem to impart some profound knowledge and understanding.

Ineffability and noetic quality are always a part of mystic experiences. Martin Luther experienced this after hearing a monk reciting, "I believe in the forgiveness of sins." Suddenly, he saw the Bible in a completely new light, as if he'd been reborn, and felt that the doors to paradise had been flung wide.

The third trait is _transiency_, meaning that mystical experiences usually don't last long.

The fourth is _passivity_, which gives the person having the experience a feeling of losing control.

An example of these two traits combined could be a trance state or somebody speaking in tongues; such actions are involuntary and usually don't last long. 

So why are mystical experiences important?

Mystical experiences offer a valuable glimpse into another reality. Anyone who has experienced a mystical revelation knows that it feels absolutely true — but, as we've seen, communicating the meaning of what's been glimpsed is challenging. Still, these states should make us aware of possible alternative realities.

The author himself experimented with inducing mystical states by using nitrous oxide, known as laughing gas. In these altered states, it became clear to him that another consciousness existed, separated from the known world by a thin veil.

### 12. Religious philosophy is secondary to religious experience. 

Have you ever done something bad and then justified your actions with an argument? We often reason out experiences after the fact; theology, for example, could be said to be an attempt to explain religious experience.

Religious philosophy, or theology, is based on a deeper feeling of religious experience and has historically attempted to prove the existence of God.

The sense of an intangible, higher power drives the desire to translate these feelings into something that other people can comprehend and sympathize with.

In an attempt to translate individual experience into general religion, theology finds arguments for religious conviction, defining things like faith and work. For example, scholastics like Thomas Aquinas attempted to prove God's existence through philosophy. He tried to create universal, intellectual statements that proved his personal conviction that God created the world. 

However, such abstract discussion of religion in philosophic language doesn't hold any practical implications for believers. That is, it doesn't change the way they behave and, in the author's view, it's therefore meaningless.

So, does this mean the study of religion is pointless? Not at all.

We should rather focus on a critical study of religion.

If we were to transform theology into a critical study of religion, for example, it could assist us in many things.

First, it could help us eliminate those parts of religious dogma that natural science has proven to be nonsensical; it could remove individual misconceptions of religion and help remove historical misconceptions that have been ingrained in religion over time; it could weed out religious concepts that are unlikely or impossible; and, based on whether we can verify the beliefs attached to them, it could ascertain which religions are most favorable.

This kind of religious science would ultimately help us more than classic religious philosophy or theology.

### 13. Prayer and access to the subconscious are important aspects of religion. 

The word "religion" tends to invoke images of prayers or revelations. Sure enough, these facets play integral roles in religion.

First let's examine prayer — a cornerstone of religion. The act that defines religion, prayer connects the person praying to the higher power in which that person believes. However, prayer shouldn't be thought of as a mere recitation of words or traditional sayings, but instead as a true movement in the soul.

In fact, prayer can be a source of energy.

Many who pray feel a transfer of energy, from the higher power they believe to themselves. Whether this process actually occurs or not is beside the point; prayer seems to awaken energy in those who pray.

Prayer can also be a means of finding solace. By surrendering to and being supported by the power she believes in, the devotee can experience a kind of proof of the higher power.

Prayer also causes destructive feelings like fear and egotism to weaken and fall away. This process opens new pathways for the soul.

The subconscious is another integral part of religion and has served as the basis of many ideas we hold about religion today.

Entering into the subconscious has offered us visions, revelations and voices throughout the history of humanity. One example of this is Mohammed, the prophet who founded Islam. In addition to receiving instructions from the angel Gabriel and from God directly, it's said that he became inspired by the Holy Spirit in his heart and received instructions in his dreams.

The subconscious, in addition to being the sources of our dreams and visions, is also perhaps source of inspiration.

### 14. Final summary 

The key message in this book:

**There is profound value in religious experiences. In order to learn from and understand these experiences and religion as a whole, we should adopt a more critical study of religion.**

**Suggested** **further** **reading:** ** _The Structure of Scientific Revolutions_** **by Thomas S. Kuhn**

_The Structure of Scientific Revolutions_ (1962) is a groundbreaking study in the history of science and philosophy. It explains how scientists conduct research and provides an interesting (if controversial) explanation of scientific progress.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### William James

Trained physician William James (1842-1910) was an American psychologist and philosopher. Considered by some to be the father of American psychology, he played a key role in the development and both pragmatism and radical empiricism.

