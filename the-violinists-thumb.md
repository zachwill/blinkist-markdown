---
id: 5369fdc66631340007430000
slug: the-violinists-thumb-en
published_date: 2014-05-06T09:40:31.000+00:00
author: Sam Kean
title: The Violinist's Thumb
subtitle: And Other Lost Tales of Love, War and Genius, as Written by Our Genetic Code
main_color: 814A26
text_color: 814A26
---

# The Violinist's Thumb

_And Other Lost Tales of Love, War and Genius, as Written by Our Genetic Code_

**Sam Kean**

_The_ _Violinist's_ _Thumb_ is an exploration into DNA, the double-helix of life itself. The following blinks chronicle the scientific discoveries that led us to understand DNA and the major role it's played in the emergence of life on earth.

---
### 1. What’s in it for me? Discover why, thankfully, we won’t see whether humans and chimpanzees can be crossbred into humanzees. 

At the turn of the nineteenth century there lived one of the most celebrated violinists of all time: Niccolò Paganini. He was so gifted with his instrument that rumors began to circulate that he'd sold his soul to the devil for his extraordinary talent. For decades after he died, the church in his hometown even refused to bury his body due to these allegations.

But it turns out that Paganini's skills were bestowed upon him by a much different force: his DNA. It's almost certain that he had a genetic disorder that made his fingers incredibly flexible: he could contort his pinky finger to the side so it formed a right angle with his palm, which is impossible for most people.

And it was this dexterity that allowed him to play the violin with such incredible skill and precision.

So what other stories can DNA tell us? It has always been an integral part of the development of life on earth, influencing the path we humans have taken from single-celled microbes into the complex, intelligent beings we are today.

In the following blinks, you'll find out

  * how thousands of fruit flies buzzing around in milk bottles helped unlock the secrets of genetics,

  * why our origins probably lie in one failed attempt by two microbes to kill each other and

  * how one enterprising Soviet scientist almost crossbred humans with chimpanzees.

### 2. The groundwork for genetics was laid by two unrecognized scientists in the 1860s. 

In Tübingen, Germany in the 1860s, Swiss biologist Friedrich Miescher was hard at work studying white blood cells at the behest of his mentor, Felix Hoppe-Seyler.

After analyzing the white blood cells gleaned from pus in hospital bandages, Miescher discovered an entirely new kind of substance: deoxyribonucleic acid or DNA.

Unfortunately for Miescher, nobody appreciated the importance of this discovery at the time. Even Hoppe-Seyler only mockingly lauded him for "enhancing our understanding of pus."

Meanwhile, not 400 miles away from Miescher's lab, a monk by the name of Gregor Mendel was pursuing agricultural experiments to increase crop yields.

Mendel was studying the heritability of traits in the common pea plant because the plant seemed binary in many of its traits: for example, it either produced yellow or green peas, and had either long or short stalks, but nothing in between. This made experimentation simpler.

Through his experiments, Mendel discovered first of all that when plants with opposing traits, say yellow and green peas, were crossed, the end result was never a mix of the two. This alone was significant, since scientists had always assumed the traits would blend together into a yellowish/greenish pea.

What's more, Mendel found that some traits were _dominant_, while others were _recessive_. In the case of peas, yellow color was dominant, meaning that when purebred yellow peas were crossed with purebred green ones, the offspring in the second generation were all yellow.

However, in the third generation of plants, Mendel found that one out of every four plants did have green peas again, so it seemed the recessive green gene lurked in the background even though it wasn't visible.

Finally, Mendel also found that the traits were separate from each other: one plant could have the dominant yellow pea trait and the recessive short stalk trait. Though he didn't use the term "gene," this is basically what Mendel had discovered: discrete, separate factors that controlled traits.

Disappointingly, the scientific community didn't recognize the importance of these findings in Mendel's time.

### 3. Fruit flies in milk bottles eventually helped explain the importance of DNA and genes. 

The importance of Mendel's and Miescher's discoveries began to emerge around the turn of the twentieth century.

Some first hints were gleaned when scientists discovered that cellular components called _chromosomes_ seemed to play a part in inheritance: parents passed their chromosomes intact to their children. What's more, the chromosomes turned out to be made of DNA.

More missing pieces in the puzzle were discovered when a Dutch botanist named Hugo de Vries found mutations in his primroses and began speculating that it was how new species evolved: species went through rare but intense mutation periods where they produced new individuals with very different traits that eventually became new species. This theory was meant to challenge Darwin's theory of evolution, which was unable to explain how new species arose.

American biologist Thomas Hunt Morgan heard of de Vries' mutations and began experimenting with mutations in fruit flies. After growing thousands of fruit flies in milk bottles for months, Morgan and his team finally found mutations, the most interesting of which was a male fly with white instead of red eyes.

When Morgan bred this white-eyed male fly with red-eyed females, he got only red-eyed offspring, but then, in the third generation, white-eyed flies popped up in Mendel's one-to-three ratio again. This lent credence to Mendel's work.

What's more, Morgan found that all the white-eyed flies were males, so it became apparent that genes actually resided in chromosomes, because the white-eyed gene evidently resided in a chromosome that only male flies had.

Eventually, Morgan was able to explain how mutations fit in with Darwin's theory: genes dictate the traits that creatures have, and mutations in those genes produce small changes in the creature's traits, say, in speed or height.

Contrary to de Vries' theory, these mutations aren't large; nor do they instantly produce new species: they're small, subtle changes. According to Darwin's idea of natural selection, it results in creatures that have beneficial mutations for survival and procreation, eventually spawning new species.

### 4. DNA produces proteins, the building blocks of the body. 

So now that the importance of DNA is clear, the question of why it's so important still remains.

This matter was illuminated in the 1940s when scientists discovered that DNA makes proteins, the building blocks for cells, tissues, organs, muscles and so forth. In fact, each gene — a stretch of DNA — stores the "recipe" for making one protein.

This protein-manufacturing process was better understood in 1953 when James Watson and Francis Crick famously discovered that DNA was shaped like a double helix. They found out that to manufacture a protein, a double strand of DNA splits in two and one of the remaining strands is transcribed into a similar strand called ribonucleic acid, or RNA. This RNA is then augmented with the amino acids we get from food to produce a protein that's ferried off to wherever it's needed in the body.

Interestingly, only about one percent of the DNA strand performs this function and can thus be called "our genes." The rest is known as "junk DNA" because it was originally thought to serve no function. Later studies suggest, however, that this is not the case: some stretches of junk DNA regulate genes by switching them on and off at certain times, making them very important indeed.

The phenomenon of _mutation_ also has its roots in DNA: a change in this blueprint obviously also produces changes in the body it's building, which gets manifested in different traits.

The vast majority of mutations will be bad for an organism. This can be seen in, for example, survivors of the atomic bombings of Hiroshima and Nagasaki, who suffer from tumors and other ailments because radiation is a major cause of DNA mutations.

But in some rare cases, a mutation may actually produce a trait that's beneficial for an organism's survival. As stated before, through natural selection, this trait will probably become more prevalent in the population, resulting in evolution.

### 5. Life on earth likely evolved from microbes that began cooperating with each other. 

Life on earth likely began deep in the ocean billions of years ago near volcanic vents that pierced the ocean floor. The heat energy from the vents melded simple molecules together into the first organic molecules.

These first lifeforms were very simple, single-celled organisms called _microbes._ Though a variety of microbes evolved, they could not get much more complex than this because they lacked an efficient means of _generating_ _energy_.

In fact, microbes spent some 75 percent of their total energy on building proteins as per the DNA's instructions, so even if some complicated, beneficial DNA mutation had occurred, building it would have completely exhausted the microbe's total energy.

Things continued like this for roughly a billion years. What happened next is a topic of debate, but the leading theory is one proposed by biologist Lynn Margulis.

The microbes had begun to evolve in different directions, some becoming large and predatory so they could absorb and "digest" smaller ones, while others shrunk and, like viruses, got inside larger microbes and killed them.

Then, so the theory goes, there was a breakthrough: one large, predatory microbe swallowed a small virus-like one, but it turned out neither could kill the other.

And so the two began to co-exist in the body of the larger organism and, as they both replicated, more such duos were created.

Eventually, the smaller microbes began to specialize in producing energy from oxygen and the larger ones specialized in providing raw materials and shelter for the smaller ones. This combination was so efficient at generating energy that it enabled more complicated features to develop.

The theory is that the legacy of this smaller microbe can still be seen in our cells, which contain _mitochondria,_ the power plants of the cell.

Margulis' theory, challenged vigorously at first, may seem pretty far-fetched, but over the years, all available evidence has come to support it. It's the likeliest explanation for what happened.

### 6. Embryonic development must be closely regulated or things can go disastrously wrong. 

While the single-celled organisms did start becoming more complex by cooperating, it would be another billion years before the next leap in evolution to multi-celled organisms would occur.

This multicellularity was achieved some 550 million years ago, probably by mistake, when sticky, single-celled creatures found they were stuck together. They soon began to specialize in different functions, which opened a gateway to the vast array of life forms we see today.

In humans, for example, there are over two-hundred different kinds of cells, each specialized in a specific function.

Interestingly, though, they don't start out that way: in a human embryo, all the cells are _stem_ _cells_, capable of transforming into any of the over two-hundred specialized ones.

How does this happen?

Each cell finds its role by having chemicals "mute" all the other genes in a cell's DNA except the ones required for that role. Thus, cells that are meant to become skin cells, for example, have the genes switched off that would make them liver cells or brain cells.

Of course, there's more to embryo development than having the right building blocks, and they also all need to go in the right place. This regulatory job is handled by so-called _hox_ genes.

Hox genes can be hundreds of millions of years old, and are therefore shared by most organisms in the animal kingdom.

This explains why insects, reptiles, fish, mammals, etc. all share similar characteristics: a trunk-like body, with a head at one end and an anus at the other, sprouting different appendages in between.

As you might have guessed, when something goes wrong with hox genes, disaster ensues: animals can develop multiple jaws or have genitals on their heads, for example.

And it does not even need to be a problem with the genes themselves because they also need other chemicals to perform their function. Too little or too much vitamin A, for example, can cause fetuses to develop monstrously: they can grow fused legs, a.k.a. mermaid syndrome, or have only one eye like a cyclops.

### 7. Not only do we share DNA with animals, we’ve also borrowed a lot from viruses. 

As seen in the previous blink, many of our current human genes are shared by other animals. Indeed, if you look at pictures of human, bird, fish and lizard embryos, you'll see that they look very similar to each other in early stages of development.

After a few weeks, a human fetus begins to mute the reptilian, avian and other genes, while turning on the mammal-specific genes, and the similarities fade.

On occasion, however, something goes wrong with this muting process, and this results in _atavisms:_ evolutionary throwbacks, whereby a trait that has disappeared ages ago reappears.

For example, some children are born with tails up to five inches long, a clear indication of our animal history.

But it turns out that animals aren't the only creatures we share genes with: the same applies to _viruses_. Viral DNA can jump into the DNA of other organisms, which has happened more than a few times over the course of evolution. Scientists have found that around 8 percent of the human genome isn't human but viral in origin.

Most of this viral DNA is junk DNA, but in some cases it has become essential.

One example is the placenta that connects the fetus to the mother in the womb, which is the trademark of nearly all mammals. The fact that placental mammals can be found all over the world is a testament to how great an evolutionary benefit it is. This is probably due to the fact that it's much easier to care for your future children if they are inside you, instead of them being vulnerable to predators in a nest or on the ocean floor.

And it seems we have previously acquired viral DNA to thank for the development of the placenta: the embryo latches onto the wall of the uterus using exactly the same DNA that a virus uses when it latches onto cell before injecting its DNA into it. We've simply reappropriated this parasitic DNA for our own use.

### 8. Many animals can be crossbred but, perhaps thankfully, humans and chimps never have been. 

So if some segments of DNA are shared across the animal kingdom, can different species breed together?

The answer is yes, several crossbreeds can produce viable offspring: scientist have blended zebras with donkeys, lions with tigers and even dolphins with killer whales.

But what about us humans? After all, we share 99 percent of our DNA with chimpanzees, so shouldn't it be possible to crossbreed our two species into some kind of _humanzee_?

Welcome to mad scientist territory.

In the former Soviet Union, a Russian scientist by the name of Ilya Ivanovich Ivanov had this same idea.

In the 1920s, Ivanov asked officials for funding for one such dubious experiment and received an excited approval. Communists had always seen religion as their enemy, and felt that crossing a creature "made in the image of God" with a simple primate would be a perfect way to insult religion.

Ivanov promptly left for Africa, where he tried for months to inseminate a female chimp with human sperm. Due to procedural and technical errors, none of of the attempts succeeded, and he soon found himself running out of funding.

So the enterprising scientist decided to turn the tables and inseminate a female human with chimp semen, and he found a female volunteer who was willing to participate in the name of science.

Just before the experiment, though, Ivanov was detained by the Soviet secret police on obscure charges and died of a brain hemorrhage just a day before he was set to be released.

Naturally, the ethics of science have forbidden retrying the experiment, but debates have raged about whether it _could_ have been done.

Though there is evidence for and against it, these days, most respectable scientists would say that such a crossbreed wouldn't be possible. The two sets of DNA are still too different to cooperatively produce a viable organism.

### 9. The size of our brains and our artistic tendencies also have their roots in mutations. 

One of the things that sets humans apart from other animals in the animal kingdom is the size of our brains. Not to brag, but how exactly did they get so big?

Well, thanks to a mix of mutations: some of which strengthened our neurons and others that expanded our cortex, the part of the brain involved with conscious thought.

But other, less-obvious mutations had a part to play as well: for example, a mutation of the jaw allowed our skulls to grow thinner, which in turn freed up more space for our brains.

Some mutations were even kinds of "evolutionary gambles," which produced positive and negative effects. For example, one mutation let us lose a DNA segment which hindered exorbitant neuron growth. While this increased the size of our brains, it also raised the risk of brain tumors.

One area where we often think the power of our gigantic brain is manifested is that of _artistic_ _endeavor_.

But, in fact, artistic pursuit may lie in our DNA, and might even pre-date the human species: animals too have been known to be artistic. For example, in a laboratory setting, chimps can be taught to paint, and can become so passionate about it that they even skip feedings to indulge their artistic desires.

But a good question is, why have we become artistic? What evolutionary advantage does art convey?

One hypothesis is that it has a social role: we can better bond over shared images, dances and songs, increasing social coherence and thus overall survivability.

Another theory is that art is the human equivalent of a peacock's feathers: a way of drawing attention to ourselves so we can attract mates. Singing and dancing demonstrate physical fitness, while painting and poetry show off our mental prowess. This would make artistic performers "sexy" and help them pass along their artistic genes.

### 10. The human genome was sequenced a decade ago, but no major medical breakthroughs have yet followed. 

Perhaps one of the biggest recent strides in the understanding of DNA came through something called the Human Genome Project (HGP), a project aimed at recording or "sequencing" the entire human DNA chain.

The project held the promise of curing all kinds of diseases, as their genetic causes could be pinpointed and suitable medications applied.

HGP was completed in 2003 and confirmed a long-held belief by scientists that humans actually have very few genes compared to other species — a mere 22,000 — and very little genetic diversity. This is taken to mean that, at some stage thousands of years ago, modern humans went through a "bottleneck" event that killed off almost all of them, leaving a relatively genetically uniform bunch as our ancestors. One theory points to a volcanic eruption some 70,000 years ago that darkened the sun, and may have left as few as forty humans alive to continue the human species.

But, in other ways, the HGP did not deliver: virtually no gene-based cures for diseases have emerged since the completion of the project. The genetic mechanisms causing diseases are simply too complex to target effectively.

Another problem in curing diseases through genes lies in _epigenetics_ : the environment's ability to switch genes on and off, and thus muddy the waters of what genes exactly cause what diseases. This effect, now being furiously researched, explains why, for example, identical twins are not totally identical despite having the same genes.

Of course, curing gene-based diseases is not conceptually far off from _manipulating_ genes to cure disease. This brings us into the controversial realm of _genetic_ _engineering_, i.e., splicing DNA from one species into that of another to induce a desired trait.

With the completion of the HGP, manipulating our own genes to improve our health, appearance, intellect, etc. seems closer than ever. But just how we will approach these exciting, yet potentially disconcerting possibilities remains to be seen.

### 11. Final Summary 

The key message in this book:

**Our DNA carries within it the history of life on earth: many of our genes are shared by the whole animal kingdom and we've even borrowed some from viruses. Yet DNA is not the whole story of life, for its effects are also modulated by other factors like the environment. Hence, the quest to better understand our DNA, and perhaps to manipulate it, continues.**

Actionable advice:

**Find your hidden artistic tendencies.**

If you're thinking of starting an artistic hobby, say painting, sculpting or playing an instrument, ask around in your extended family about their hobbies first. It may turn out that your grandmother was quite a ballet dancer in her day, implying that you may well have good genes for dancing, and should perhaps give it a go!
---

### Sam Kean

Sam Kean is an American writer whose stories have been featured in publications like _The_ _New_ _Scientist,_ _New_ _York_ _Times_ _Magazine_ and _Psychology_ _Today_. His previous book, _The_ _Disappearing_ _Spoon_, was a bestseller and named one of Amazon's top five science books of 2012.

