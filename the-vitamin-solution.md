---
id: 57bb524d003a4b0003f1a896
slug: the-vitamin-solution-en
published_date: 2016-08-25T00:00:00.000+00:00
author: Dr. Romy Block & Dr. Arielle Levitan
title: The Vitamin Solution
subtitle: Two Doctors Clear the Confusion about Vitamins and Your Health
main_color: ECBE31
text_color: A18121
---

# The Vitamin Solution

_Two Doctors Clear the Confusion about Vitamins and Your Health_

**Dr. Romy Block & Dr. Arielle Levitan**

_The Vitamin Solution_ (2015) offers a clear picture of the world of vitamins to help you determine whether you need them, why you might need them and what they can do to improve your everyday life. Cut through the clutter and confusion and find out which vitamins are essential for a healthy body and mind and how you can put yourself on the path to better living.

---
### 1. What’s in it for me? Learn why supporting your diet with vitamins can be good for you. 

Professional athletes take vitamins to balance their diet and reach optimal levels of physical performance. But do the rest of us — we who go jogging every once in a while or who play soccer or tennis on the weekend — really need vitamin supplements?

Well, you may not _need_ vitamin supplements, but they may still be good for you. These days, it's becoming more and more apparent that the right intake of vitamins is good for the body and overall health.

In these blinks, you'll learn why vitamin supplements are useful. You'll discover that vitamins can cure or prevent common ailments, and you'll learn whether vitamin supplementation might be right for you.

You'll also learn

  * why vitamins are an efficient hangover cure;

  * how vitamins can prevent depression; and

  * why you should consult a doctor before taking vitamin supplements.

### 2. Although the value of vitamins is often questioned, there are good reasons to supplement your diet with them. 

Have you ever been at a family gathering and had to avoid an eccentric family member who is always trying to convince everyone to use vitamin supplements? Such zealous relatives are just one of the many reasons people tend to be skeptical about vitamins.

Even the medical community isn't sure how effective vitamins are.

Much of this uncertainty is due to scientific studies' inability to provide definitive results.

For example, in 2000, biologist Patrizia Mecocci conducted a study on the effects of vitamins on the human lifespan. Afterward, however, there was no conclusive evidence that vitamins prolong life, and many studies since have also concluded that vitamins are largely ineffective.

On the other hand, there are other researchers who argue that the problem isn't with the vitamins, but rather the science behind the studies.

They suggest that these studies often fail to show conclusive results due to participants not taking continued doses of the vitamins for long enough periods.

Another important contributing factor is that the effects of vitamin supplements are highly dependent on the individual. Therefore, studies where every participant spends years taking vitamin C are unlikely to show uniform results. That's because, in all likelihood, not every participant needed vitamin C; some were probably in need of a different supplement.

Despite these disputes, one can say with confidence that most patients are likely to benefit from a form of vitamin supplementation that suits their lifestyle.

After all, no matter how healthy your diet, you are probably missing out on some key nutrients that can easily be supplemented.

For example, a largely vegetarian diet can lead to deficiencies in vitamin B12 and iron. And if your diet consists of a lot of meat, you might miss out on other essential nutrients such as vitamin A and vitamin C.

Achieving the perfect balance through diet alone is nearly impossible, and some nutrients are simply hard to come by. Vitamin D3, for instance, is important for bone health, but only exists in wild salmon.

> _"Should I be taking vitamins? Yes, you should, but perhaps not the typical concoctions you find at your corner pharmacy."_

### 3. Electrolytes can fight dehydration headaches and vitamins are an efficient hangover cure. 

Life is filled with headaches. Working overtime to meet a deadline, fighting with your partner about overdue bills, the endless noise of construction work outside your apartment — any of these can give rise to a bloom of pain.

While supplements can't solve all your problems, they can help cure headaches.

One leading cause of headaches is dehydration, and electrolytes are here to help.

Certainly, drinking plenty of water is a good way to stay hydrated, but your body also needs plenty of electrolytes. These electrolytes are key because they're the charged particles that actually hold on to the water that enters your body. Without them, no matter how much water you guzzle, it will just be urinated out and you'll remain dehydrated.

So, make sure you get enough electrolytes like magnesium, sodium and potassium, which are found in mineral waters, sports drinks or tablet supplements.

Vitamins can also help you battle your next hangover.

Actually, hangovers are also linked to dehydration. One of the side-effects of drinking alcohol is that it dehydrates the body's cells, especially brain cells.

So, in order to avoid a painfully throbbing headache the next morning, make sure you get enough water and electrolytes before, during and after you party.

There are also a few other vitamins you can take to help you recover. Before going to bed, or first thing the next morning, take a combination of vitamin B1, magnesium and folic acid. And, just to be sure, remember to prepare this dosage before you've had one too many drinks.

These vitamins will help by settling in on some of your brain receptors and protecting them from the harmful effects of the alcohol in your system. This way, you'll be guaranteed a less agonizing morning.

> _"We can't hook up an IV every time we have a cocktail, but we can work to keep our own brain cells nice and hydrated."_

### 4. Iron supplements can help raise your energy levels, but you must be careful to get the right dosage. 

It happens to the best of us: you wake up and, for whatever reason, the last thing you want to do is go to the gym or put in another long day's work. Some might call this laziness — but maybe your body actually just needs some iron.

An iron deficiency is no laughing matter. In fact, it can lead not only to a lack of energy but also to depression.

Your red blood cells use iron to bond with and carry oxygen throughout your body. So, a lack of iron means the body's system is being compromised since less oxygen is getting circulated, which leads to your feeling exhausted and lethargic.

Women, who lose a lot of iron during their menstrual period, tend to suffer iron deficiencies more than men. But men may also struggle with this, especially if they follow a vegetarian diet since one of our primary sources of iron is red meat.

Nor is being tired the only sign of an iron deficiency. Other symptoms include confused thinking, depression and hair loss, all of which can appear even if your iron levels take just a small dip. With this in mind, it's worthwhile to get your iron levels checked and find out if supplements are necessary.

And if you're thinking of starting iron supplements, it's important to be very careful with the dosage: too much iron can be dangerous, and a serious overdose can be fatal.

Our bodies are sensitive to iron; too much can result in unpleasant side effects such as constipation and dry coughing. So, to make sure you get the right amount, it's recommended that you consult a doctor.

It's also important to make sure that you don't have a condition called _hemochromatosis_, which makes people intolerant to even the smallest amounts of iron. With this condition, iron can't be processed and ends up getting dangerously deposited in vital organs.

> _"Wouldn't my doctor know if I was iron deficient? Not always. Early stages of iron deficiency require special testing."_

### 5. Vitamin D deficiency is a growing epidemic that may be at the root of seasonal depression. 

If you've ever raced outside to enjoy the first warm and sunny day of the year, you're not alone. One reason for this common reaction is that our bodies are trying to recover from being deprived of the benefits of sunlight.

Actually, lack of sunlight is seen as a leading cause in the spreading epidemic of vitamin D deficiency.

It's difficult to pinpoint exactly why vitamin D deficiency is so common. Indeed, there are a variety of reasons which might explain it.

A primary one is the original source of vitamin D: the sun. So, whether you live in a cold and cloudy environment, or simply work long hours in the office, it's likely you're in need of some vitamin D.

Also getting in the way is the coating of sunscreen we apply when we do get time outdoors. Yes, it's an important preventive measure against skin cancer — but by blocking out the sun's rays, sunscreen also prevents the body from metabolizing vitamin D.

Also at risk are people with dark skin, whose natural color prevents their bodies from absorbing a lot of sunlight.

The consequences of this epidemic are serious.

In fact, many practitioners who consider the benefits of vitamin supplements are discovering a link between vitamin D deficiency and depression. This is especially true for the winter depression that commonly affects people in cold climates.

Unfortunately, though, you can't just pop a couple of vitamin D pills and kill your winter blues.

Vitamin D is unique because it needs to get stored in the body's fat, which is a slow and time consuming process. This means it can take six months to a year for your body to replenish its vitamin D levels.

So, to prevent those winter blues, the best course of action is to plan ahead and start taking vitamin D at least as early as summertime.

> _"In 2015 some evidence was found to suggest a link between vitamin D and serotonin production, which alleviates depression."_

### 6. Magnesium helps keep our muscles and bones healthy and is especially important for athletes. 

You might have heard about the health benefits of magnesium for the ailing and elderly; in reality, however, magnesium is vitally important for everyone, whether young or old.

Magnesium is especially crucial in helping prevent muscular problems and bad sleep.

Magnesium is a mineral central to the functionality of our cells. It's an important part of our nervous system, enabling it to send signals to all of our muscles, including our heart, telling them when to contract and release.

So, with a magnesium deficiency, you can end up with symptoms such as muscle aches, cramps, osteoporosis and sleep trouble.

Today, magnesium deficiencies are partially caused by the food industry, which uses pesticides and overworks the soil, which can lead to depletion of mineral content in farming areas. This then results in plants' lacking full nutritional value and important minerals.

Though magnesium can still be found in many nutritious foods such as bananas, nuts, green vegetables and whole grains, having these foods in your regular diet might not be enough. So, it's worth considering a magnesium supplement.

This is especially true if you are athletic since magnesium supplements accelerate bodily regeneration. Through sweating and physical exertion, athletes are vulnerable to magnesium deficiency and the resulting muscle cramps and tensions that can lead to poor performance.

Magnesium is also a key element for strengthening and regenerating both bone and muscle structure, which makes it crucial for athletes to have a steady supply of it coursing through their system.

Finally, in addition to easing tense muscles, magnesium also provides you with a better night's sleep by stimulating specific brain receptors that help the body relax. While anyone can appreciate the benefit of this, it's especially crucial for athletes, whose performance often depends on a good night's sleep.

> _"Magnesium is an electrolyte we can't live without and certainly can't sleep (well) without."_

### 7. The right amount of iodine promotes a healthy thyroid gland, but healthy diets often don’t provide enough. 

Most of the time, we think of salt in terms of its taste benefits rather than its health benefits. But the truth is that salt is very beneficial to our well-being. And, interestingly, so-called "healthier" salt is actually less beneficial.

The importance of salt comes down to the iodine it contains, which prevents your thyroid gland from getting over- or under-stimulated, as well as allowing it to produce the hormones the body needs.

Therefore, there are a range of symptoms that can occur when an iodine deficiency interferes with the proper function of the thyroid: your heart rate slows down and you may feel depressed, tired, cold and constipated.

But too much iodine is also a bad thing and can cause the thyroid to get overexcited and send the body into overdrive, making you feel nervous, overstimulated and hyperactive, with trembling hands, hot flashes, diarrhea and trouble sleeping.

Therefore, getting the right amount of iodine is key. Unfortunately, however, healthy diets often don't contain enough iodine while supplements can go too far in the other direction.

Thyroid problems weren't always so common. In the past, most people seasoned their food with a fair amount of table salt, which always used to be fortified with iodine. These days, diets suggest eating less salt and processed food. This has lead people to eschew these processed foods, which generally contain iodine, and use sea salt or Himalayan salt, which, though healthy, are both iodine free.

These modern trends have resulted in people having iodine deficiencies and an underactive thyroid. And since we know that a consistent lack of iodine can cause goiters and developmental deficiencies in children, it is certainly worth considering iodine supplements.

But, as with iron, you must be careful of the supplemental dosage. Many products contain too much iodine, which will overstimulate your thyroid and lead, potentially, to tremors and hair loss.

> _"We are comfortable with the recommendation of between 150 and 220 mcg of supplemental iodine daily."_

### 8. Vitamin B12, along with lifestyle adjustments, can help the fight against neurological diseases. 

You, along with everyone you know, certainly don't want to experience the moment when a close friend or relative can no longer recall who you are. And thankfully, discoveries are being made that will help battle the brain disorders that have puzzled us for so long.

Currently, however, illnesses involving memory loss and cognitive degeneration are on the rise.

As a significant portion of our population grows older, ailments related to memory loss, such as dementia and Alzheimer's, will only become more prevalent.

Fortunately, there has been substantial research in this area and we've seen that patients who've made some simple adjustments to their lifestyle have been able to counteract the symptoms of memory loss and cognitive impairment.

These adjustments include keeping the brain active by doing crossword and Sudoku puzzles and by staying socially active.

And while a medicinal cure is still missing, vitamins can also play a role in reducing symptoms.

The most helpful vitamin is B12, which can counteract issues related to memory loss.

In fact, acute B12 deficiencies have been observed in patients suffering from memory or attention deficits, establishing a clear link between the vitamin and neurological disorders. And these observations don't only apply to the elderly; benefits have also been seen in school children and young adults experiencing trouble with memory and focus.

Plus, there is a variety of ways to take B12 supplements in order to fight these symptoms. Depending on the appropriate dosage, there are pills, injections or prescription nasal sprays.

However, while the effectiveness of B12 in combating these neurological issues is clearly established, there is no evidence suggesting it can _improve_ the functions of an already healthy brain.

So, as we've seen, there are many ways and reasons vitamins might be able to help you live a healthier life. But finding the right vitamin and the right dose for you is not an easy task, so make sure you seek the advice of your physician before you start a supplemental regimen.

### 9. Final summary 

The key message in this book:

**Taking vitamin supplements is a useful way to improve your body's health and functions, but taking them is not always as simple as you might think. It is important to find the right vitamin for your specific problem and to take the appropriate dose over the necessary amount of time in order to reap the benefits.**

Actionable advice:

**Talk with your doctor or health counselor.**

If you are unsure about which vitamins and what dosage to take, talk with your doctor. You can even get tests measuring the current level of vitamins in your body. This way, you'll know which deficiencies you're experiencing and whether or not you do in fact need supplementation.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Bulletproof Diet_** **by Dave Asprey**

_The Bulletproof Diet_ (2014) shows you how to hack your body to optimize your health, lose weight and increase your mental functioning. How? Maintain a diet filled with the right proteins and fats, and learn to exercise and sleep in the most effective way possible.
---

### Dr. Romy Block & Dr. Arielle Levitan

Arielle Miller Levitan is a doctor of internal medicine, and Romy Block is a doctor specializing in endocrine medicine. After years of practicing together and treating patients, they developed the Vous Vitamin brand to promote the vitamin supplements that they feel are best for their patients' health.

