---
id: 56faa52edf81420007000088
slug: the-wandering-mind-en
published_date: 2016-04-01T00:00:00.000+00:00
author: Michael C. Corballis
title: The Wandering Mind
subtitle: What the Brain Does When You're Not Looking
main_color: EAD98F
text_color: 857B51
---

# The Wandering Mind

_What the Brain Does When You're Not Looking_

**Michael C. Corballis**

_The Wandering Mind_ (2015) reveals exactly what is going on in our brain when our mind starts to lose focus. We explore the areas of the brain that remain active as concentration drifts and uncover the connections between our memory, creativity and the rewards of daydreaming.

---
### 1. What’s in it for me? Stroll through the fascinating field of mind wandering. 

We all cherish a little daydreaming for escaping a dreary situation like a traffic jam or a long day on the assembly line. On the other hand, a drifting mind can be extremely annoying when you're desperately trying to focus on a task. But why do our minds meander off in the first place?

As you'll learn from these blinks, we really can't help it, because our brain comes with an in-built wandering mode. 

On the face of it, this seems like a great evolutionary disadvantage — wouldn't it be better to be alert all the time, considering all those saber-toothed tigers and poisonous snakes so eager to kill our ancestors? The following blinks will tell you about advantages that override the pitfalls of being less than constantly vigilant.

In these blinks you'll also find out

  * about a man who was stuck in an eternal present;

  * why some of your precious childhood memories might be wrong; and

  * how you don't have to be a psychic to wander into another person's mind.

### 2. A wandering mind isn’t a bad thing; your brain remains active when it woolgathers. 

Sometimes our minds don't cooperate. Have you ever found yourself sitting at your desk with the intention of getting some work done but instead your mind keeps going in other directions? When this happens, don't assume that your brain isn't still hard at work.

When your mind wanders, it is using nearly as much energy as when it is focused and concentrating. 

This is because only certain regions of your brain are active when focused on a specific task. But when your mind begins to wander, the _default-mode network_ is activated. This network, named by neurologist Marcus Raichle, is spread out across the brain in regions that are not involved with the more direct interaction focused on what's in front of you.

But despite being spread out, there is still plenty of activity going on.

You can think of your brain as a small town: When there's a big event at the town square, all the people show up in one location. But afterward, when everyone splits up and goes about their own business, there's still nearly as much activity, it's just distributed around town. This is what happens when your mind wanders.

So the wandering mind may have some drawbacks, but it isn't all bad.

It's true that a wandering mind can distract you from finishing a task, and research also suggests that it can lead to less happiness and premature aging. 

But wait! Mind wandering is also vital to creative thinking and allowing inventors and artists to make the world a better place. Because when your mind drifts it can free-associate and find connections and solutions to problems that might otherwise stay hidden.

Maybe you've had inspiration strike while you were spacing out in the shower or, perhaps, on a hike through nature. This is where George de Mestral was inspired to invent Velcro when he was walking along and noticed how burrs stuck to his clothes. 

In the next blink, we'll see how our memory dictates where the wandering mind goes.

> _Blood flow in the brain is only 5–10 percent lower during wandering, showing it is still quite active._

### 3. When our mind wanders, it makes use of the three levels of our memory. 

You will often find that when your mind drifts it is thinking of a past experience or a future event that you're anxious about. As it turns out, the destination of this wandering hinges upon three levels of our memory.

The first level is made up of our basic skills.

Our wandering minds are often linked to simple abilities like walking, talking and writing. It is also common to daydream about either a skill we'd like to have, such as playing a musical instrument, or a talent we have lost, such as fluency with a foreign language. Obviously, when our mind stops reflecting on these, we can be unhappy about not having them.

This leads us to the second level of our memory: knowledge.

This level contains all the words and languages you do know as well as the data and information about people and places you've learned about. This is where most of the creative mind wandering happens, where we draw on and make connections from this vast pool of knowledge.

Whether you are writing a book or simply daydreaming, your mind can wander and create worlds from your storage of knowledge. You can even imagine being somewhere you've never been, such as climbing up one of Hawaii's volcanoes.

This brings us to our third and final level: _episodic memory_.

This is a specialized layer containing specific and personally relevant moments. This is where your mind wanders to recall an event that you have experienced. Since these memories are deeply personal, they are the ones that most define your sense of self.

These are also memories that tend to grow fuzzier the older we get, requiring a trigger in order to be revived. You might feel this happening when an old photograph causes your mind to wander to a past event. 

With all these different levels to wander off into, it isn't surprising that our mind takes these frequent journeys.

> _"It seems we are programmed to alternate between mind-wandering and paying attention."_

### 4. Strange memory phenomena have an impact on our mind’s wanderings. 

Have you ever taken comfort in allowing your mind to wander back to a cherished childhood memory? Can you imagine the pain of losing access to those memories?

People with amnesia experience this inability to recall past events.

Some cases of amnesia also result in the failure to create new memories. But in both cases, the mind is impaired and unable to wander between the past and present.

One famous case is Henry Molaison, who underwent an operation when he was 27 years old. Afterward, he was unable to learn anything new, even though he could remember the events prior to the operation. His mind was stuck in a permanent present: Even when he was 60 years old he would assume that he was around 34 years old. 

But mental health issues aren't the only cause of memory problems.

Everyone is capable of creating false memories that can affect the way our mind wanders. This is partly because our memories aren't stored on a permanent record like a movie we can rewatch. Each time we bring up a memory we have the ability to distort it.

An experiment conducted by American psychologist Elizabeth Loftus revealed that it's even possible to implant false memories in people. A quarter of her test subjects were able to "remember" and answer questions about events in their life that never actually took place. In these cases, it appeared that these "memories," such as getting lost in a supermarket, were actually stories from other family members.

So, since our memories are constantly being reshaped, it is possible that our mind can wander back to something that hasn't even happened. These memories can feel so real to us that they can even fool a lie detector!

In the next blink, we'll see how we can also wander into someone else's mind, and how this can be good for us.

> _"People with amnesia typically have as much difficulty in imagining future events as they do in remembering past ones."_

### 5. We can actually wander into others’ minds, but it has nothing to do with psychic powers. 

We don't have to visit a mystic with a crystal ball in order to figure out what someone else is thinking. In fact, we do it all the time when our mind wanders in a particular direction.

It's not psychic powers that tell us what is on someone's mind; it's our powers of observation and cultural experience that give us this insight. And these abilities come from the same part of our brain that is active when our mind wanders: the default-mode network.

One example of this comes from a study in which researchers offered a scenario to a group of people: When John met Emily, he told her that he drives a Porsche, but he actually only drives a Ford. How will Emily, who knows nothing about cars, react when she finds out?

The people in the study thought about it, and the majority came to the right conclusion: Emily would have a false belief and think that the Ford was actually a Porsche.

Brain scans revealed that it was the default-mode network that was active when participants were thinking of their answers. So we can say that it is entirely possible to wander into someone else's mind to figure out what they're thinking.

This sort of mind wandering also allows us to safely navigate potentially awkward social situations.

By wandering into someone's mind, we can determine what they may or may not know about a subject, which lets us respond appropriately. Perhaps, like Emily in the previous case, someone is mistaken in their beliefs. Knowing this allows us to avoid confusion when we're talking to them.

We can also determine whether someone might take offense to something we say. In Emily's case, she might be humiliated if it's made public that she mistook a Ford for a Porsche. So we could then discreetly inform her of the mistake and help her avoid embarrassment.

### 6. Storytelling is an essential form of mind wandering and probably goes back millions of years. 

Though we share many traits with other mammals, including social behavior, the use of tools and an aversion to fleas, humans are the only species with a long history of storytelling.

We let our minds drift and come up with stories for a variety of reasons.

Back in hunter–gatherer times we likely shared stories as a way of passing on important information to one another, helping to ensure our survival. Some tribes still do this today, sharing different hunting and gathering techniques, where they found food or danger and telling stories about other tribes. The storytelling format also helped children learn these life-saving stories.

The importance of these stories can be seen in the social structure of tribes, where storytellers are highly respected.

It's also likely that this creative daydreaming led to the development of language. 

Many believe that mime was originally used to tell stories, but it is naturally not the most effective way to share important information. Language developed as a better way to share knowledge as well as create more complex and elaborate stories. This also allowed people to communicate more abstract ideas like "living beings" and to give a name to new inventions or concepts. 

While some stories are based on an exciting or important experience, other stories require fantasy and gripping narrative. As the next blink shows, a wandering mind can enhance this sort of creativity.

### 7. Many people use drugs to get creative, but mind wandering might work just as well – with fewer side effects. 

Creativity is a huge asset in both the business and entertainment world. In the search for inspiration, many people have turned to drugs, because substances like LSD, marijuana and opium all have a mind-altering effect that can lead to the wandering mind that results in ingenuity. Dreaming can have the same effect, but is unpredictable — and few people remember their dreams.

From Charles Dickens and Edgar Allen Poe to The Beatles and Steve Jobs, many artists and creative people have used drugs to help them find inspiration and come up with innovative ideas. 

Alcohol was used by Winston Churchill, James Joyce and Ernest Hemingway for the same reason.

But these drugs also have many negative side-effects such as addiction.

A safer path to inspired creativity is through the wandering mind and a process called _incubation_.

This means developing our best ideas in our center of creativity — the default-mode network — while we are focusing our attention elsewhere. 

For example, one experiment revealed that people performed best at a creative task after they were given a break involving a mundane task. This break allowed their mind to wander, activating the default-mode network, and they returned to the creative task inspired.

On the other hand, participants who were given a task requiring them to use their memory during the break performed poorly when they returned to the creative task. This is because they did not have the time to allow their minds to wander and let their ideas incubate. 

In the end, you don't need drugs to be creative. Let your mind wander and reap the rewards.

> _"Creativity depends on the very mechanisms of mind wandering itself — the default-mode network."_

### 8. Final summary 

The key message in this book:

**A wandering mind isn't a bad thing.**

**When your mind wanders it isn't shutting down, it's actually still quite active. By letting your mind wander you can inspire more creativity and become more understanding of others.**

Actionable advice:

**Let your mind wander.**

Don't try to be productive every minute of your time. It's OK to sit back, relax and let your mind wander. It just might inspire your next creative breakthrough!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Organized Mind_** **by Daniel Levitin**

_The Organized Mind_ provides an insightful explanation into the way our brain handles incoming data — a process particularly relevant in this age of information overload. It's also a practical guide to coping with the multitudes of decisions we're required to make in everyday life. By learning well-thought-out strategies that will help you organize your life, you'll become a more productive and effective worker in any task. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Michael C. Corballis

Michael C. Corballis is a psychologist and author as well as a professor emeritus at the University of Auckland in New Zealand. His main fields of research include cognitive neuroscience, memory and the evolution of language. His other books include _A Very Short Tour of the Mind_ and _The Recursive Mind._

