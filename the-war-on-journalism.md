---
id: 5852d0aa58b1350004ed1711
slug: the-war-on-journalism-en
published_date: 2017-01-05T00:00:00.000+00:00
author: Andrew Fowler
title: The War on Journalism
subtitle: Media Moguls, Whistleblowers and the Price of Freedom
main_color: A04556
text_color: A04556
---

# The War on Journalism

_Media Moguls, Whistleblowers and the Price of Freedom_

**Andrew Fowler**

_The War on Journalism_ (2015) explores the challenges journalists face while seeking the truth amid increasing state control and private sector criticism. Even though the internet has allowed those in the media unprecedented access to people and information, equally technology and new rules of the game have made fact-seeking a far more problematic pursuit.

---
### 1. What’s in it for me? Learn why democratic governments are fighting the power of journalists. 

Democracy is under attack. And the attackers aren't foreign terrorists, but the very government agencies which are supposedly protecting citizens — and democracy as a way of life — from those terrorists.

So what exactly is happening? In short, many governments are doing everything they can to keep "inconvenient truths" away from the prying eyes of journalists and society.

These blinks will show you why, even in our internet age, we as a society must continue to depend on independent journalists to keep our governments in check.

In these blinks, you'll discover

  * why whistleblower Edward Snowden reached out to a freelancer rather than a top newspaper;

  * how just a few news reports triggered the final days of the Vietnam War; and

  * why so many journalists felt the need to criticize the actions of WikiLeaks.

### 2. In a democracy, the media need to be free to conduct investigations and report the news. 

Do you read the news? Have you followed major developments such as Edward Snowden's revelations about the US National Security Agency or the so-called Panama Papers?

If so, you're familiar then with both whistleblowers and investigative journalists, and understand how the activities of both groups can affect society at large.

The media is often called the _fourth estate_. Coined in Britain in the eighteenth century, this term set journalists and their craft as a check on the power of society's other "estates," namely the clergy, the nobility and the common people.

Today, journalism's role as the fourth estate is still important. When a government enacts harmful policies or officials pursue controversial plans, journalists are tasked with letting the public know, in effect working to curb the influence of the too-powerful in society.

The reasoning is simple. Citizens have the right to know the facts, so that they can make informed decisions.

The _Guardian,_ for example, has a long track record of writing about controversial issues and has often collaborated with whistleblowers such as Edward Snowden on stories. The information Snowden released on the National Security Agency (NSA) in the United States — a secretive agency that was spying on US citizens and leaders abroad — sent shockwaves across the globe.

Journalists and publications such as the _Guardian_ can only function if they can freely investigate issues on an independent basis. A free press is a crucial part of any democracy; if the state controls the press, it can limit the public's access to information and cover up deeds about which it doesn't want society to know.

Snowden's NSA revelations, for instance, wouldn't have been possible if he hadn't had access to a free press.

While there are still many TV stations and newspapers run by governments in countries around the world, most media outlets are privately owned. This separation allows journalists to report on issues without fear of losing their jobs or being censored — or worse.

### 3. The media wield a power so great that it can determine the fate of nations. 

Journalists, through their work, can wield powerful influence among their audiences. The stories they write act to mold public opinion, which in turn can shape a nation's government.

Society at large can experience a major upheaval when the public gets wind of information that a government or ruling clique would rather keep secret. The revolutions of the Arab Spring are one poignant example of this situation in action.

When the media began to divulge details of pervasive fraud and corruption within the Egyptian government, citizens rose up against President Hosni Mubarak and effectively ended his rule.

And even if scandals revealed by the press don't lead to a public outcry, such revelations can still result in government agencies closing or political figures abdicating.

Just take former Icelandic Prime Minister Sigmundur Davið, who was forced to resign in 2016 after the so-called Panama Papers (leaked documents from an offshore law firm) revealed that he had hidden millions of dollars from his country's tax agencies.

Media reports can even make the difference between victory or defeat in war.

In 1969, RAND Corporation employee Daniel Ellsberg got access to a 7,000-page secret report that two years earlier had been commissioned by the US defense secretary.

The report, which later became known as the Pentagon Papers, revealed that the US government was lying to the public about the Vietnam War. Despite the persistent, positive narrative that government officials continued to push, the administration no longer expected to win the war.

Ellsberg decided to go public with the confidential document. He was arrested and prosecuted for the leak after the _New York Times_ published stories based on information from the report. Ellsberg was later released, however, after it came to light that the prosecution had obtained evidence illegally in the case.

Americans, as well as citizens from many nations, were appalled by the US government's deception, and protests to end the war became even more strident and widespread.

To this day, the release of the Pentagon Papers is credited as a decisive event that helped bring about an end to the Vietnam War.

### 4. Freelance journalists are more willing than mainstream publications to explore sensitive information about the state. 

Brave investigative journalists in recent history have risked incarceration to disclose secrets a government might have wanted to hide. In the United States, however, such days are behind us.

Today many mainstream journalists and editors engage in self-censorship when considering the stories that may be critical of a government, fearing such stories could hurt their careers.

Editors aren't just reluctant to publish critical stories, but sometimes even contact officials involved to ask for permission to go to print!

Why would journalists compromise their independence in this fashion? These journalists are afraid of the political consequences if they publish stories that anger those in power. Reporters could even be threatened with arrest — and whistleblowers even more so.

While career journalists cower in the face of increased state scrutiny, organizations such as WikiLeaks have garnered a lot of publicity but also equal amounts of criticism. Despite the risks, the site still publishes secret reports and documents from whistleblowers around the world.

Many journalists are embarrassed by WikiLeaks' activities, feeling that the organization makes mainstream media look "bad." After all, WikiLeaks is essentially doing the investigative work that many journalists should be doing themselves.

Whistleblowers with sensitive information now often turn to freelance journalists, as mainstream newspapers are often hesitant to publish controversial stories. When whistleblower Chelsea Manning took her collection of US Army intelligence to the _New York Times_, for example, the revelations weren't published for weeks.

Whistleblower Edward Snowden revealed his secrets in a different fashion. He contacted Glenn Greenwald, a freelancer for the _Guardian_, and Laura Poitras, an independent filmmaker who had made a film about Julian Assange.

When _Guardian_ editor Alan Rusbridger received Greenwald's article about the activities of the National Security Agency based on Snowden's revelations, he sat on the story for a few days, fearing that he might lose his job, or worse, be prosecuted for treason.

> _"The Snowden case revealed once again the timidity of newspapers on both sides of the Atlantic when offered an astonishing story."_

### 5. The internet has become both a blessing and a curse for investigative journalism. 

Before the advent of the internet, doing investigative work was a tougher job. Journalists would spend days traveling to conduct interviews or sifting through newspaper archives, looking for relevant pieces of information.

The internet offers a journalist today a massive amount of information as well as instant access to billions of people around the globe. Archives of some 400 million websites, media reports and articles are now online, so anyone can access them.

While the internet has become a great source of information for reporters, it has at the same time made it increasingly difficult for newspapers and other media outlets to earn money.

Today many readers expect to read the news online for free. Media organizations in the face of dwindling revenues have been wrestling with this basic reality for years.

Should news outlets offer content for free and earn money through advertising, or put all or some published content behind a paywall? Major papers such as the _New York Times_ and the _Guardian_ are still struggling with this question, and both have incurred large revenue losses in recent years.

The financial reality of journalism is bleak. It can take years to uncover and accurately report a story as big as the Panama Papers, for example. Journalism is still a job, and reporters need to earn money to support themselves and their families.

Yet sales of printed media and paid access to online media continue to fall as readers simply no longer want to pay for them.

In sum, the internet is eating away at the economic realities on which print media was founded. Yet balance sheets aren't the only challenge for investigative journalists these days.

> _"Revelatory and investigative journalism in the West is in a state of crisis."_

### 6. The US government is trying to gain control of information by going after whistleblowers. 

How far does "freedom of information" actually extend?

In the United States, journalists are protected by the First Amendment of the US Constitution, which guarantees freedom of speech and freedom of the press, in essence offering journalists legal cover if they choose to report on government secrets or crimes.

Whistleblowers revealing sensitive or secret information to the public, on the other hand, are guaranteed no such cover and run the risk of prosecution.

Edward Snowden, for example, left the United States for Hong Kong before releasing his information on the NSA. He is now living in Russia to avoid prosecution in the United States.

In 2009, when former State Department contractor Stephen Kim gave Fox News reporter James Rosen secret information on North Korea's nuclear arms program, Kim was eventually found out and was jailed for 13 months.

The US government is intensifying its spying to prevent leaks and catch whistleblowers. Governments spy on people every day. In fact, it's become an open secret that the US National Security Agency (NSA) has spent millions of dollars and countless man-hours listening in on government leaders and American citizens.

Pre-internet snooping was limited to wiretaps and the like. Today the government, with advanced technology, can sort through internet searches, Facebook chats and other online activity. Government officials, however, denied that agencies were monitoring citizens until the Snowden leaks.

All this means that investigative journalists have to do a lot to protect their sources, especially whistleblowers. At the very least, reporters need to use encryption software for any online communications, be careful about their footprints online and conceal real-life meetings.

James Rosen neglected to take such measures when he met with whistleblower Stephen Kim. He didn't cover his tracks or find ways to obscure visits or repeated phone calls to the State Department, leaving obvious patterns for investigators to pursue.

Thus when a government investigation over the leak was opened, Kim was quickly identified as the source of the information.

> _"Paradoxically, at a time when information flows around the world more freely than ever before, the controls are tightening."_

### 7. Rather than decreasing, government surveillance has expanded in recent years. 

Do you think the Snowden revelations changed the way the NSA conducts its surveillance activities?

If you think they did, you're about to be disappointed.

While the US government has decreased surveillance of its citizens, it continues to spy on other nations. Snowden's revelations showed that the NSA was even spying on US allies such as Germany. NSA operatives had even tapped Chancellor Angela Merkel's cell phone!

Even after officially ending certain NSA programs, US government officials were still involved in the spy game. US spies even recruited a member of the German secret services, an individual who was caught shortly after the relationship was established.

This revelation put a further strain on Germany-US relations, especially after US officials pressured Germany to deny Snowden asylum. Both US and UK officials threatened to keep German intelligence out of the loop regarding jihadist and terrorist movements if Germany helped the whistleblower.

Other US allies such as Australia have _increased_ surveillance activities since the Snowden leaks.

While the United States failed to renew the Patriot Act, a law that initially enabled the country's current intelligence efforts, several other countries have changed laws to increase spying. Australia even introduced emergency laws enabling the government to gain access to private information such as phone protocols.

Many people assume that freedoms of speech and information have increased since Snowden's revelations, but in fact, the opposite is true. Governments monitor people more closely and foreign governments monitor each other even more.

All this spy-versus-spy activity will undoubtedly have a negative impact on the field of journalism. Only time will tell if the future is to offer a transparent world of open information or one where government secrets stay behind closed doors and citizens are kept in the dark.

### 8. Final summary 

The key message in this book:

**A free press serves as a counterweight to state power. The public can't be informed unless journalists are free to research and publish stories to benefit democracy and encourage transparent government. Today, however, free media is under threat. The internet has given governments additional tools of repression against journalists and whistleblowers and has rocked the economic foundations of the industry at large. Whether journalism will survive this period remains to be seen.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _No_** **_Place_** **_to_** **_Hide_** **by Glenn Greenwald**

In _No_ _Place_ _to_ _Hide,_ author Glenn Greenwald details the surveillance activities of secret agencies as according to information leaked by American whistleblower Edward Snowden. Rather than serving as a means to avoid terrorist attacks, as the US National Security Agency (NSA) claims, Greenwald explains that these dubious activities instead seem to be a guise for both economic espionage and spying on the general public. _No_ _Place_ _to_ _Hide_ also brings to light the media's lack of freedom in detailing certain government and intelligence agency activities, and addresses the consequences whistleblowers face for revealing secret information.
---

### Andrew Fowler

Andrew Fowler was an investigative journalist for (Australian) ABC TV's _Four Corners_ program and was also foreign editor and chief of staff at _The_ _Australian_. His series of interviews with Wikileaks' Julian Assange led to the publication of his award-winning book, _The Most Dangerous Man in the World_. Fowler is retired from daily journalism and spends his time between Sydney and Paris.

