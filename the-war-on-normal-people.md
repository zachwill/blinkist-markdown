---
id: 5e6505226cee07000601bcaf
slug: the-war-on-normal-people-en
published_date: 2020-03-11T00:00:00.000+00:00
author: Andrew Yang
title: The War on Normal People
subtitle: The Truth About America's Disappearing Jobs and Why Universal Basic Income Is Our Future
main_color: None
text_color: None
---

# The War on Normal People

_The Truth About America's Disappearing Jobs and Why Universal Basic Income Is Our Future_

**Andrew Yang**

_The War on Normal People_ (2018) reveals how America is on the brink of economic and social collapse due to technological advances such as automation and artificial intelligence. Painting a portrait of the reality that average Americans will increasingly face if mass unemployment is left unaddressed, author Andrew Yang proposes a vision to transform the economy by adopting a human-centered form of capitalism that begins with a universal basic income.

---
### 1. What’s in it for me? Consider Andrew Yang’s radical plan for the future of America. 

As the founder of Venture for America — a nonprofit organization focusing on training entrepreneurs and creating jobs in American cities — Andrew Yang often traveled to cities like Baltimore, Detroit, Cleveland, and New Orleans. He noticed that in contrast to the successful people in his Manhattan and Silicon Valley bubbles, average __ Americans are facing major difficulties accessing healthcare and education, while many communities are struggling with issues related to drug addiction. On top of this, he realized that our economy is on the brink of a mass unemployment crisis driven by unprecedented technological advances.

In these blinks, you'll learn about the reality that most Americans are facing today and what it will take to create an economy that creates hope for everyone.

Along the way, you'll find out

  * how Richard Nixon was behind a failed plan for a guaranteed income in the 1970s;

  * why empty malls lead to broken cities; and

  * why a capitalism that values humans doesn't need to be utopian thinking.

### 2. Advances in technology and market-driven capitalism have left America on the brink of mass unemployment. 

Think about your five closest friends. Did they all go to college? If so, chances are you're part of a privileged socioeconomic group. For most in the United States, not finishing college is more the norm. In fact, based on a random sampling of Americans, the likelihood that all five of your closest friends finished college is about 0.3 percent.

In contrast to the educated class, average Americans tend to live from paycheck-to-paycheck. And while our system of market-driven capitalism has raised the standard of living for many, giving us widely-enjoyed technology like smartphones, this system doesn't value the lives of most people. Instead, it values efficiency and cost-cutting measures that enable the market to perform better. 

According to Yang, things are about to get a lot worse. Market-driven capitalism has propelled unprecedented advances in technology — including artificial intelligence, automation, and robotics — which are beginning to eliminate jobs by the millions. Currently, a whopping 95 million working-age Americans aren't in the workforce, making the US labor force participation rate lower than almost every industrialized economy.

Yang calls this phenomenon the Great Displacement. And unlike many economic situations, it's one that the market isn't set up to solve. According to a report published by the Obama administration in 2016, 83 percent of jobs that pay less than $20 per hour could face automation or replacement.

This displacement has already begun — since 2000, automation has eliminated an estimated five million American manufacturing jobs. With the advent of self-driving vehicles, a projected two to three million car, bus, and truck drivers will lose their jobs in the next 10 to 15 years. Factory workers, cashiers, customer service representatives, and fast-food chain employees will also soon suffer the consequences of automation.

With today's technology, even therapy and drawing are skills that can be automated. Even if white-collar workers such as paralegals and wealth managers are also forced to find new work in the coming years, low-income earners without a college degree or other skills will inevitably be the least able to find new work prospects.

### 3. Job losses lead to broken cities, broken communities, and broken families. 

When it comes to addressing the impending crisis of mass unemployment, many economists look to the Industrial Revolution of the nineteenth century as a model. They claim that in the future economy — which we haven't even imagined yet — new jobs will materialize, just as they did in the Industrial Revolution.

Not only does this notion overlook the comparative sophistication of new technologies to the combustion engine or other Industrial-era inventions, it also fails to acknowledge that, despite its great technological leaps forward, the Industrial Revolution was a time of significant turbulence. The reality is that job loss has an effect that ripples through society.

About one in ten American workers who earn an average income of $11 per hour are sales and retail workers. These people are increasingly at risk of losing their jobs, which is largely the result of the rise of e-commerce giants like Amazon, and the growing prevalence of self-service checkout scanners. Mall anchor stores such as Macy's and JCPenny have been closing locations, while chains like Payless and Aeropostale have declared bankruptcy, forcing dozens of malls to close their doors.

For each mall that closes, approximately 1,000 people lose their jobs. Assuming an average yearly salary of $22,000 per worker, this equates to $22 million in lost annual wages for the area the mall is located in. Since a mall's sales tax feeds directly into state and county budgets, the loss in tax revenue leads to cutbacks in municipal funding, including school budgets. And without the capacity to hire sufficient security, these dying or empty malls sometimes become crime havens, endangering the local community. And what about all those former mall workers? In most cases, retail workers don't have a college degree, limiting alternative employment options.

The burden of ghost malls is just one way that job losses lead to cultural breakdown. The same cascade of negative effects happens on an even larger scale when an entire town faces mass unemployment. Consider the fate of cities such as Youngstown, Ohio. Youngstown was a bustling industrial steel town until the strain of global competition forced its steel mills to shut down in the late 1970s. In spite of government loan programs that attempted to help ex-steelworkers retrain, the town was plunged into turmoil. Bankruptcies, foreclosures, child and spousal abuse, drug addiction, and alcoholism all increased over the next decades, as did crime and psychological breakdowns.

Youngstown's story is an extreme example, though a version of this story is already playing out on a national scale.

### 4. A mind-set of scarcity has caused many desperate Americans to apply for disability insurance. 

Losing your job doesn't have to be catastrophic, right? After all, as recently as September 2017, the unemployment rate was only 4.2 percent, so new jobs must be readily available. 

Well, some facts suggest otherwise. The unemployment rate doesn't account for people who have given up on finding a job altogether. And a 2012 Department of Labor survey found that, of the five million workers displaced from manufacturing jobs since 2000, 41 percent were either still unemployed or had dropped out of the labor market entirely.

But what does "dropped out" mean exactly? The recent rise in suicide and drug overdose rates — the latter a product of the nationwide opiate crisis — gives us one answer. Another answer might be that a growing number of people are applying for Social Security Disability Insurance, or SSDI. Beneficiaries, who in 2017 received an average of $1,172 per month, apply for SSDI citing issues like "mental disorders" or "musculoskeletal and connective tissue." Unsurprisingly, in places where the number of people out of work is high, so too are the numbers of people on disability benefits.

Applying for SSDI is a relatively straightforward process. Of the roughly 9,500 applicants per business day, 40 percent will be approved on first application or through appeal. That might explain why there are currently more Americans on SSDI than American workers in construction jobs.

Once someone claims disability, it's unlikely that they'll look for another job, since that would mean losing their benefits. Unfortunately, the system wasn't built to support masses of unemployed Americans, and the SSDI fund was recently forced to merge with the greater Social Security fund. 

It's hard to blame beneficiaries for cheating the welfare system when they've been left little other choice, an idea that is backed up by studies on financial scarcity. In the early 2010s, Princeton psychologist Eldar Shafir and Harvard economist Sendhil Mullainathan worked together to study the effects of scarcity on impoverished people. They found that on a test that measured IQ, poor people and wealthy people performed similarly. But when the two groups were asked to consider how they'd manage an unexpected $3,000 car repair bill, the poor group underperformed significantly.

Shafir and Mullainathan also found that scarcity uses up our intellectual bandwidth, reducing our capacity to be rational, forcing us to behave more impulsively. Given these results, we can easily empathize with desperate Americans trying to make ends meet, and should work to create a system in which scarcity plays no role in people's futures.

> "_After one gets on disability, one enters a permanent shadow class of beneficiaries_."

### 5. Income inequality in the United States could lead to political upheaval. 

As normal Americans experience marginalization without prospects for future employment, wealth in America is becoming increasingly concentrated. The majority of high-achieving graduates from top universities are moving to a handful of cities, including New York City, Boston, San Francisco, and Washington, DC. With educated people leaving their hometowns behind in pursuit of status and high-paying jobs, top talent is effectively pulled away from the rest of the country, as the income equality between these cities and everywhere else deepens.

Given that income inequality is a product of market-driven capitalism, Yang fears that growing discontentment and desperation might lead to widespread upheaval. For example, frustrated truckers and unionized Teamsters — legendary for their aggression in the 1950s — could trigger mass protests and nationwide havoc if they are laid off when self-driving vehicles become available. Inspired by social media, truckers across the country might gather and block highways. This could inspire the support of white nationalist groups and anti-government militias, spiraling into violent conflict while hundreds of thousands refuse to pay their taxes.

Though a truckers' uprising is a hypothetical scenario, there's reason to believe that something along these lines could play out. Scientist Peter Turchin has likened the current state of America to France before the French Revolution, and anticipates increasing turmoil with potential violent conflicts.

Though mass unemployment hasn't explicitly been the cause of recent conflicts, some experts believe that it may have been an underlying factor in the 2015 Freddie Gray riots in Baltimore. The riots began with the death of 25-year-old African American Freddie Gray at the hands of police officers, and led to the injury of 20 police officers and 250 arrests. But Baltimore-based writer Alec Ross suggests that the protests were also driven by a sense of hopelessness shared by many in the Black community in the wake of industrial and manufacturing layoffs.

Given this state of civil unrest, it's not hard to imagine similar cases playing out in the future. And with racial minorities projected to collectively form the majority of the US population by 2045, it's likely that in the case of a revolution, underprivileged white people will blame immigrants and people of color, rather than automation and our capitalist system.

### 6. The idea for a universal basic income is neither new nor impossible to finance. 

If you've been thinking that this vision of America sounds particularly bleak, you'll be relieved to hear the author's solutions. At heart, Yang is an optimist and believes that there's still time to change the fate of America, as long as the federal government takes action.

The first solution that Yang proposes is the implementation of a universal basic income, or UBI, which Yang calls the Freedom Dividend. This dividend would allot each American between the ages of 18 and 64 an annual income of $12,000 regardless of their work status or income. The dividend would be indexed to rise with inflation and, since the poverty line is $11,770, would effectively raise Americans out of poverty en masse.

Though the dividend would replace most welfare programs, a universal basic income would inevitably come with a hefty price tag. To cover the $1.3 trillion cost, Yang suggests that the government implement a value-added tax, or VAT, for goods and services. This would mean a slight increase in prices, but the only way you would lose money — given your $12,000 guaranteed income — would be to spend more than $12,000 on goods and services per year. If that still sounds unreasonable to you, consider that the United States is the only developed country without a VAT. In fact, it would only take half of the average 20 percent VAT used by European countries to cover the costs of a universal basic income for every working-age American.

Costs aside, UBI might sound like a radical idea to many. But UBI has actually been around for centuries, and it was suggested by some of the greatest figures in American history. Those who have proposed some version of it include the eighteenth-century American activist and political theorist Thomas Paine, the economist Milton Friedman, Martin Luther King Jr., and Bill Gates. You may also be surprised to learn that President Richard Nixon proposed a guaranteed annual income called the Family Assistance Plan. As part of this plan, eligible families would receive $10,000 per year. The 1970 and 1971 versions of a bill to establish the program passed 245 to 155 in the House of Representatives before stalling in the Senate because of resistance from Democrats, who favored an even more extensive plan.

### 7. Universal basic income will alleviate poverty while boosting the economy. 

Skeptics often dismiss the idea on the basis that people will be irresponsible with the money, arguing that UBI will squander incentives to work and that people will waste the money on drugs and alcohol. While it might be reasonable to believe that some people will stop working, when it comes to how people handle the extra cash, real-world examples of guaranteed income paint a different picture.

In 1974, the Canadian government spent $56 million on monthly checks to raise everyone in Dauphin — a 13,000-person town in the province of Manitoba — above the poverty line. The program was discontinued when a conservative government came to power four years later. In 2005, an economist at the University of Manitoba, Evelyn Forget, analyzed the results. What she found was that the only groups of people who worked less during the program were new mothers and teenagers. At the same time, birth rates decreased for women under 25, high school graduation rates increased, and hospital visits were reduced by 8.5 percent.

The benefits of a guaranteed income have been proven beyond short-term studies. The Alaska Permanent Fund has been paying residents between $1,000 and $2,000 annually since 1982, with funds taken from state oil revenue. Republican governor Jay Hammond touted the plan in 1982 for its "conservative political purpose," since it distributed money directly to citizens while downsizing government spending.

Effectively reducing poverty by 25 percent, the Alaska Permanent Fund has created more than 7,000 jobs annually through increased economic activity. Studies show that the dividend helps rural Alaskans stay solvent, and many Alaskans attest to saving parts of this extra guaranteed income.

According to a 2017 analysis by the Roosevelt Institute, a federal UBI would have economy-boosting benefits similar to those of the Alaska Permanent Fund, but on a national scale. A $12,000 dividend could grow the economy by 12.56 to 13.10 percent per year by 2025, and bring 4.5 to 4.7 million people into the workforce. The simple explanation for this is that most Americans are so broke that the money from a dividend would be spent locally on things like paying bills, feeding children, or car repairs. 

In other words, UBI would bolster local industries and maintain the consumer economy in the face of automation.

### 8. Successful healthcare reform will require transforming the incentives of healthcare providers. 

Between 2002 and 2005, while working as head of client engagement at a New York-based health software start-up that digitized paper medical records, Yang noticed how reluctant doctors were to financially back the new technology. In part, this was because high-earning surgeons were uninterested in investing their profits.

Yang's observations reflect how, in spite of new technologies, American healthcare providers continue to put profits before the interests of patients. By 2016, healthcare costs had risen to account for 17.8 percent of the economy, with the United States spending around twice as much per capita on healthcare as other industrialized countries. At the same time, US healthcare ranks lower than all other major industrialized nations in equity, efficiency, and health outcomes. What's more, for many Americans, unpredictable costs often result in high bills, leaving people with a financial burden at a time when they're least equipped to deal with it. 

As jobs with full healthcare benefits become increasingly rare, healthcare reform will become more urgent. The most immediate solution is a single-payer healthcare system for all Americans in which the government guarantees coverage at fixed prices. One way to establish this, as suggested by Sam Altman, the former president of the startup incubator Y Combinator, would be to gradually lower the eligibility age for Medicare. This is the national insurance program that provides healthcare for Americans aged 65 and older. Though a "Medicare-for-all" movement has gained headway, it will inevitably face resistance from high-paid doctors reluctant to accept lower incomes. So, to make it work, we'll need to transform how doctors get paid.

One approach would be to pay doctors flat salaries instead of wages that fluctuate according to their activities. This has already been successful at the Cleveland Clinic in Ohio, where doctors are paid a flat rate, and are involved with hospital purchasing decisions. While in most hospitals, doctors are often concerned with billing and take on more work than necessary due to financial incentives, doctors at the Cleveland Clinic can instead focus all their attention on patients. It's no wonder then that it consistently ranks as one of the top hospitals nationwide.

### 9. The government should pioneer a human-centered form of capitalism and help citizens find value in the automated era. 

Yang's healthcare reform ideas are a testament to his belief that UBI is not an end-all solution; rather, it's just the first step toward what he calls _human capitalism_. Unlike our system of market-driven capitalism that values money, human capitalism would be a system that values humanity and uses markets to serve our common goals and values.

To institute human capitalism, the government would begin holding companies accountable for abusing the market for financial gain and against humanity's interests. Yang proposes that the government could create a law, a Public Protection Against Market Abuse Act, which would require a company's CEO and largest individual shareholder to spend one month in jail for every $100 million fined by the Justice Department or accrued in federal bailout costs. 

For example, this could apply to the Sackler family and their company Purdue Pharma, which in 2007 was fined $635 million by the Department of Justice for advertising OxyContin as non-addictive. The fine hardly made a dent in the Sackler family's $14 billion fortune. Though six months of prison is a small price to pay when Americans will have to face the opioid crisis for decades to come, it might prove more effective at discouraging companies from abusing the system than inconsequential fines.

For human capitalism to succeed, the government would not only need to deal with market abuse, they'd also have to ensure that people lived flourishing lives in the face of long-term unemployment. Though for some, UBI would enable the flexibility of practicing filmmaking, sports, or other passion pursuits, the government could also create a _time banking_ system to inspire meaningful community engagement.

Pioneered in the 1980s by law professor and anti-poverty activist Edgar Cahn, time banking enables people in an organized network to accrue credits by offering time-based tasks such as dog walking, cooking, or handiwork. Credits can be used to buy other time-based services. The system is more than just utopian thinking: at the time of writing, time banking was already in place in 200 US communities.

In Brattleboro, Vermont, a single mother named Amanda Whitman signed up to the Brattleboro Time Trade. She hoped to find assistance repairing her house while she balanced a part-time job and homeschooling her children. Amanda was surprised at both the number of people who offered to help her and the many ways she found to give back to her community. Once, she even earned credits by playing music at a garden party with her children. 

Considering the crises that many Americans find themselves in, Amanda's story offers a hopeful image for what the country's future could look like. If, that is, the government takes action to revolutionize our economy in a way that prioritizes normal people.

### 10. Final summary 

The key message in these blinks:

**Market-driven capitalism has led to advanced technology that is eliminating US jobs and setting regular Americans up for an economic and cultural crisis. The damage has already begun to destroy communities and cause desperate Americans to apply for disability benefits. Starting with a universal basic income, our government can still create a human capitalism that values human health and well-being above financial gain.**

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next: Utopia for Realists, by Rutger Bregman.**

As you've just learned, a universal basic income funded by a VAT could immediately raise Americans above the poverty line and cultivate an economy that works for everyone. But Andrew Yang isn't alone in seeing UBI as a pragmatic step toward solving income inequality. Rutger Bregman's _Utopia for Realists_ (2014) recognizes the paradoxes of our capitalist system and calls for a radical transformation of the way we think about life, work, and society.

In _Utopia for Realists_, Bregman suggests that in spite of unprecedented wealth and material comfort, we continue to suffer from inequality, poverty, and soul-destroying jobs. By embracing utopian thinking, we can solve these problems and attempt to build a better future. To find out what Bregman has to say about the benefits of a shorter working week, as well as UBI, head over to our blinks to _Utopia for Realists_.
---

### Andrew Yang

Andrew Yang is an entrepreneur, lawyer, philanthropist, author, and Democratic candidate in the 2020 United States presidential election. After helping to develop several companies and start-ups, Yang began Venture for America, a nonprofit organization focused on creating jobs throughout the United States.

