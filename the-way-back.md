---
id: 5846b6f0714e5f00047cdc89
slug: the-way-back-en
published_date: 2016-12-09T00:00:00.000+00:00
author: F.H. Buckley
title: The Way Back
subtitle: Restoring the Promise of America
main_color: CFC5BE
text_color: 696360
---

# The Way Back

_Restoring the Promise of America_

**F.H. Buckley**

_The Way Back_ (2016) offers an intriguing explanation of why the American dream is a long-lost concept. These blinks take a close look at the fundamental systems of the United States, from education to criminal justice, and reveal exactly how these systems have broken down and why they are in desperate need of repair.

---
### 1. What’s in it for me? Restore the American dream! 

We all know about the American dream — the idea that hard work and moral conduct are enough to ensure a good life, or even to bring great success. Arguably, this desire for social and economic mobility is what has driven American citizens to keep on striving, which, in turn, has resulted in the great wealth and power of the United States.

Today, however, the American dream is an empty one. If you are born poor, you will stay poor no matter how hard you try, and if you're born rich, you will stay rich regardless of how poorly you perform. The system is rigged. And that's not all. American society also punishes the people who try to live the American dream. Those who work hard to achieve something great are usually thwarted by the current system. So how can we find a way back?

In these blinks, you'll find out

  * how getting US school results up to Canadian levels would solve the US debt crisis;

  * what impact having certain license requirements for jobs would have; and

  * how the American judicial system negatively impacts social mobility.

### 2. America’s poor public education system perpetuates social inequality at high economic cost. 

Here's a straightforward question: If a boat were traveling from England to the Philippines via the Suez Canal, what are the names of each body of water it would pass through along the way? Today, most adults would be hard-pressed to come up with an answer, but in 1912 it was something an eighth-grade student in the United States was expected to know.

Since then, the standards of American public education have drastically declined, and not just in geography: math skills and literacy rates have plummeted in the United States while those of other countries have improved.

According to the Programme for International Student Assessment (PISA), the United States, one of 65 countries under review, fell from 25th to 31st place in mathematics between 2009 and 2015.

This is extremely important since the quality of a nation's public education is directly related to its social mobility.

In 2011, a study by Stanford University economist Eric Hanushek found a near perfect correlation between education and social advancement: Children who experience inequality in their education grow up to experience social inequality in the professional world.

Out of all the countries in Hanushek's study, America was most guilty of perpetuating inequality, tending to provide the best education to the richest people and an inferior education to the poor.

A poor public-education system like this isn't a problem that only affects the poor, either; it also comes at a high economic cost.

When a country fails to produce educated workers with the right skill levels, the entire economy suffers.

In 2013, Hanushek followed up with another study that showed how, by raising public school standards to match Canada's, the United States could balance out its current debt crisis. And if that weren't incentive enough, higher education standards could also result in every US worker receiving an average pay increase of 20 percent.

As it stands now, private schools help the economy by producing a few great scientists. But the majority of the population receives a significantly inferior education, which is little help to them or to the economy.

### 3. America’s school admission systems – both wealth- and merit-based – favor the rich. 

A look at higher education paints a pretty clear picture: a good education in America is reserved for those who can afford it.

In the United States, rich families can put their children on a fast track to a prestigious college by making financial donations to that college.

Sadly, this system can result in many undeserving students being admitted, a practice that Daniel Golden, a writer and Harvard alumnus, condemns in his book, _The Price of Admission_. Golden lists many famous children with poor grades who would never have been admitted if not for their parents' money.

Among these unqualified students are Albert Gore III, who was granted admission to Harvard, and Harrison Frist, who got into Princeton with the help of his father, a US Senator. George W. Bush and John Kerry both had high-school grades that failed to meet the requirements of the elite universities they attended.

But even if colleges based admission strictly on standardized tests, such as the Scholastic Aptitude Test (SAT), the rich would still have an unfair advantage due to the poor public-education system.

By looking at the results of SAT scores and comparing them to the income of the student's family, you can see that those who could afford a private education fared better:

Children from low-income families that earn less than $20,000 scored an average of 1,326 out of a possible 2,400. Meanwhile, middle-class families earning less than $60,000 rise to an average score of 1,461. And upper-class families earning over $200,000 hit an average of 1,714.

As you can see, it's hard to deny that the US education system strongly favors the rich. And this doesn't even take into account the cost of college and the fact that, even if children from low-income families were accepted to a prestigious school, most would be unable to afford the tuition and fees.

> _"While legacy college admissions perpetuate social immobility, so too do meritocratic admissions."_

### 4. The American immigration system is not picky enough and costs the country a lot of money. 

True, America is a nation founded and built by immigrants. But, as they stand today, America's immigration policies are in need of repair.

The current US immigration system is not built to get the best out of migrants.

Two-thirds of America's immigrant population are accepted based on the _family preference_ policy, which admits people with family already in the United States without considering their level of education.

Some believe the Canadian immigration system would be a better model to follow.

Canada evaluates immigrants using a point system that rates their language skills, work experience and education. Those with a high score are considered skilled workers and are likelier to be admitted, which has led to Canada having two-thirds of its immigrants classified as skilled workers.

Without a policy similar to this, many unqualified immigrants will continue to enter the United States, and the economy will continue to suffer.

It's no secret that immigrants earn far less than American-born citizens: In 2006, sociologist George Borjas found that US immigrants earn 20 percent less than non-immigrants and that this discrepancy diminishes by 50 percent every generation. So, it would take an immigrant family four generations to finally close the gap.

Meanwhile, by earning less, they have less money to spend and therefore also contribute less to the US economy.

In contrast, it only takes two generations for Canadian immigrants to catch up with Canadian-born citizens.

Furthermore, the Canadian immigration system takes less time to process its applicants, which gives more time for immigrants to settle down and establish a permanent residence. US immigrants often wait years to know whether their application has been approved, which stalls their search for steady work or continuing education.

### 5. Requiring unnecessary licenses cripples the economy and market competitiveness. 

So far, we've looked at the faultiness of America's education and immigration systems. Both are working against the best interests of the United States. But there's another threat to the economy: the licensing system for US businesses.

Take the ridiculous licensing requirements needed to teach yoga. As yoga became more popular in the United States, states like Virginia began introducing licensure requirements for teachers. As a result, anyone hoping to teach yoga had to work through a mountain of paperwork and pay a $2,500 fee. On top of that, to meet the requirements, many had to take continuing-education courses that added up to thousands of dollars more in fees.

These licensing requirements are overseen by the State Council for Higher Education, which normally takes care of matters involving universities and colleges. But now they're also in the strange position of evaluating how well someone teaches a yoga pose. Arguably, such evaluations could safely be left in the hands of the yoga community.

Enforcing these restrictive licensing requirements only hurts the economy by deterring entrepreneurs who have the initiative but lack the money and resources to jump through all the hoops.

Unfortunately, more and more businesses are being subjected to licensure requirements and it's continuing to hurt their chances of being competitive in the open market.

It was only 20 years ago when one out of every 20 American workers needed to obtain a license for their job. Today, that number has jumped to one out of every three workers. As a result, you now need a license for almost all jobs, even if all you want to be is a Washington, D.C., tour guide.

And all these licenses aren't necessarily there to ensure quality work is being done. Most of the new requirements come from a group of established insiders trying to beat back the competition.

To keep safe their positions as industry leaders, they erect these hurdles to deter people from entering the profession and challenging their status. And, of course, there's the added financial benefit they receive from those who can afford to pay all the licensing fees.

> _"There's usually a cartel of insiders protecting their turf from competitors who can't afford the licensing fee."_

### 6. The US tax code offers many loopholes to big corporations and their top executives. 

If you're an American hoping for a good education or harboring dreams of entrepreneurial success, then there's one thing that'll help you above all else: being rich. Furthermore, if you're rich, then you'll also reap the rewards of the federal tax system.

The US tax code is full of arcane details and loopholes that wealthy Americans and the richest corporations can exploit — a fact that continues to hurt the federal budget.

Between 1952 and 2009, corporate taxes, which once made up 32 percent of the government's overall tax revenue, fell to just nine percent.

With a good tax advisor who knows which loopholes to take advantage of, a wealthy company can even get by without paying any taxes at all.

In 2010, General Electric earned $14.2 billion, yet it paid absolutely nothing in taxes. First of all, it sheltered money in offshore accounts, but, more importantly, it invested in deductible causes such as renewable energy, low-income housing and research.

And just like their companies, corporate executives also take advantage of the different ways the tax code can be exploited.

Many of them have an apparent salary of under one million dollars; they point to this as a way of avoiding public scrutiny. Meanwhile, they earn more significant sums through performance bonuses and stock options, which are taxed at much lower rates.

Remarkably, corporations can even use the salaries they pay to executives as a tax deduction, which is a perfect example of how the federal tax system is set up to help the rich stay rich.

### 7. US criminal law makes arbitrary imprisonment easy, and businesses are often raided without reason. 

Have you broken a law today? If you really think about it, how sure can you be?

There are a lot of random and seemingly arbitrary laws in the US legal system; accidentally breaking one of them may be easier than you think.

Take Krister Evertson, for example. He was arrested in Alaska while working on a way to create clean-energy fuel cells. But he ran into trouble when he mailed a package of sodium without including the required sticker that ensures such packages get shipped by ground transportation.

Believe it or not, this is a federal crime and Evertson was taken to criminal court where the jury saw that he'd made an honest mistake and acquitted him.

But, unfortunately, it didn't end there: The federal agents found another obscure law to arrest Evertson once again.

This time, he was charged with violating federal law by not having his sodium properly stored while he was under arrest for the first charge. Ultimately, Evertson was sentenced to two years in prison for the abandonment of hazardous waste.

Similar legal technicalities in the United States can lead to small businesses being unjustly raided by inspection agents.

In 2010, the Florida Department of Business Regulation sent two licensing inspectors to an Orlando barbershop where they found that everything was in order and that the business was in compliance.

However, the inspectors returned two days later, this time with eight armed policemen. Even though their only jurisdiction was licensure requirements, they searched the entire premises and told the barbers that there were so many federal regulations that they were bound to be breaking one of them.

The case eventually went to court, where the inspectors and police were reprimanded by the judge and warned against performing illegal searches.

### 8. US criminal law gives prosecutors great power, which they use against corporations. 

As we saw in the previous blink, sometimes the judicial system helps keep order and sometimes it sends someone to jail for a simple mistake.

Many of the problems with this system stem from prosecutors being given far too much power.

US criminal cases often begin with a grand jury, which is made up of 12 to 23 people who are left with a prosecuting attorney behind closed doors. The prosecutor can then make their case to the jurors, unobserved by a judge or defense attorney — in other words, unburdened by counterarguments or counterevidence.

The prosecutor is also free to present any evidence that might convince the jury of a guilty verdict. This can include testimony from witnesses, including whoever's being investigated as well as unsubstantiated rumours and hearsay. And at no point do they need to tell the witnesses what the case is actually about.

Grand jury proceedings can last for years and they almost always end in the prosecution's favor, and with damage done to the accused's reputation.

Prosecutors are well aware of the power a grand jury can give them and they use it to gun down corporations.

In a 2008 study of legal systems around the world, scholar Edward Diskant found that no other developed country files criminal charges against corporations as often as the United States.

And when charges are brought, the defendants usually make a large cash settlement rather than have their reputations ruined and see their business destroyed in a court battle.

One of the most high profile cases occurred in 2002, when charges were brought against the energy corporation Enron. The company rightfully went bankrupt due to their fraudulent practices, but the prosecutors didn't stop there.

They also went after Arthur Anderson, Enron's accounting firm, which had shredded some evidence of Enron's wrongdoing. For this, the accounting business was brought before a grand jury and, in the end, 28,000 employees were put out of work.

> _"A grand jury would indict a ham sandwich." -_ leading American judge, Sol Wachtler

### 9. US prosecutors even target the rich and famous and are not afraid of bending the rules when they do. 

Arbitrary laws and the power of prosecutors recently came together in a remarkable case that shows just how corrupt things can get.

In 1999, Martha Stewart had built her own empire around cooking, decorating and homemaking. Her magazine, _Martha Stewart Living_, was selling two million copies annually and she was all over television, being celebrated as one of the first women in the United States to become a self-made billionaire.

However, even though she was loved by millions, prosecutors were merciless when Stewart's name came up in a case against ImClone Pharmaceuticals.

It turned out that the CEO of ImClone was a friend of Stewart's and had called her to inform her that he was selling many of his shares in the company and that she might do the same, which she did.

Stewart didn't break any laws, but when she was interviewed by federal prosecutors without a lawyer, she made a mistake and gave a false story to protect her friend. Now, lying to a federal agent is indeed a crime and the prosecutors sent her to prison for five months.

This just goes to show that even celebrities aren't immune to the broken legal system in the United States.

In other cases, prosecutors will even bend the rules to send certain businessmen to jail.

Michael Milken was a superstar on Wall Street when he helped start a whole new way of doing business by getting people to invest in portfolios that contained several unconnected high-risk stocks. It started the high-yield bond market of the 1980s, and though the overall risk wasn't bigger than a standard investment, the returns were much, much bigger.

With the system Milken helped create, the US economy soared, but the prosecutors had a different point of view when they put him before a grand jury. To make his strategy illegal, they created new rules for stockbrokers. Under these new laws, Milken was found guilty of a remarkable 98 counts of racketeering and fraud, getting fined $600 million and sentenced to two years in prison.

We need to protect free enterprise, rework the legal system and rethink the entire education system. Do that, and America will be back on track.

### 10. Final summary 

The key message in this book:

**If America wants to survive and thrive, fundamental changes need to be made. Public schools need to be improved to meet the higher standards of private schools and the legal system needs to be reviewed, especially the tax code and criminal law.**

Actionable advice

**Lobby your government to get rid of tax incentives and loopholes.**

The United States and other countries need transparent tax codes to ensure that the rich really do contribute to federal revenue. Otherwise, tax lawyers will continue to help them evade taxes altogether.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Beyond Outrage_** **by Robert B. Reich**

_Beyond Outrage_ provides a sobering analysis of what has gone wrong in American politics and economics. Looking at the distribution of wealth and income imbalance, it convincingly argues that we must wrest government from the hands of the regressive right.
---

### F.H. Buckley

F.H. Buckley is a Canadian writer who specializes in constitutional and contract law. A professor at George Mason University in Virginia, his writing has appeared in the _Wall Street Journal_ and the _National Post_. His other books include _The Once and Future King: The Rise of Crown Government in America_ and _The Morality of Laughter_.

