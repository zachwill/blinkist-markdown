---
id: 5369f94366313400070b0000
slug: the-way-of-the-seal-en
published_date: 2014-05-06T12:37:37.000+00:00
author: Mark Divine
title: The Way of the SEAL
subtitle: None
main_color: E1C157
text_color: 947F39
---

# The Way of the SEAL

_None_

**Mark Divine**

_The_ _Way_ _of_ _the_ _SEAL_ draws upon the experience of Navy SEALs to outline the principles that enable them to cultivate mental toughness and excel. It offers the mental exercises necessary to deal with any challenge on the path towards achieving your goals.

---
### 1. What’s in it for me? Learn how to accomplish any mission using Navy SEAL tactics and strategies. 

_The_ _Way_ _of_ _the_ _SEAL_ offers you insights gained from a former Navy SEAL during the course of his training in duties. Drawing from the experiences of his own training, the author has devised a set of strategies that anyone can use to define their missions, sharpen their focus and develop the tactics necessary to accomplish their missions, whether personal or business-related.

In these blinks, you'll learn

  * what it takes to develop personal discipline, take an ethical stand, and shape your life accordingly,

  * how hanging upside down from a mountain helped one executive take tighter control of his life,

  * some simple tricks for deciding whether your decisions are tactically sound, and

  * how to discipline your "fear dog" and nourish your "courage dog" to maximum health.

### 2. In order to make your life meaningful, you need to define your purpose. 

Each individual is unique, but it isn't our color, shape or language that differentiates us: it's our _soul_ — our sense of who we are.

Inside our souls, there are fixed points unique to us, and it's these points that give our lives meaning. When we're unsure of what to do, for instance, what job to pursue, they act like our GPS, guiding the way for us.

So how can we find these unique, fixed points?

The first thing you must do is make a _stand_, deciding which things you absolutely cannot compromise on.

For example, the author's stand is: "I will find peace and happiness through seeking truth, wisdom, and love, and not by chasing thrills, wealth, titles or fame."

Knowing which values cannot be compromised helps him to shape his life in a way that is meaningful to him.

For example, if he were offered a job with a great title and a huge paycheck at the expense of sincerity, then he wouldn't accept it because that would mean compromising his values.

Second, you must know your _purpose_.

Your purpose answers the question: What do I really want with my life? Knowing your purpose enables you to determine steps necessary to achieve it and overcome any hurdles along the way.

However, if you don't follow your purpose, it will be difficult to achieve _anything_.

For example, on his first day of SEAL training, the author was able to enjoy the beauty of the day despite the extreme challenge of his training. His buddy, however, didn't share his positive outlook, struggled with the challenge, and ultimately quit.

The author asked him afterwards what had happened, and his friend confessed that he didn't even want to be a SEAL. His dream had always been to be a veterinarian.

So why was the author successful with this challenge but his friend wasn't? Quite simply, because the author had identified his purpose and his friend hadn't.

### 3. Gain control over your mind to sharpen your focus and avoid distractions. 

During their training, Navy SEALs are taught that, when they wield their weapon, they should place enormous concentration and focus on their aim, utilizing something called _front-sight_ _focus_.

Not only is front-sight focus absolutely essential in combat, it's also applicable in our everyday lives.

In fact, all goals require focus if we're to achieve them, and great accomplishments are only possible when we identify our goals and achieve them in succession, _one_ _goal_ _at_ _a_ _time._

Imagine, for example, that you run a business and you have several goals to achieve simultaneously: you need to reduce your costs, be environmentally conscious and introduce new products onto the market.

You could try to accomplish all your goals at once, but the lack of focus ultimately leads to inefficiency. It would be better to employ front-sight focus and concentrate on one single, immediate goal at a time in order to achieve each of them more efficiently.

But front-sight focus doesn't always come naturally. In order to cultivate front-sight focus, you must first gain control of your mind.

You should start by learning to ignore outside influences, thus allowing yourself to focus on a singular goal.

One way to do this is through deep breathing and silence. Deep breathing forces you to concentrate only on the present moment, thus avoiding any distractions. Practicing deep breathing will help you focus on the front-sight.

Silence provides you with a space to think more clearly. If you work in an office, for example, your mind gets overloaded by the distractions of office chatter and ringing phones, which ultimately saps your concentration.

One way to overcome this might be to go outside and sit in a quiet place, like a park. The stillness will allow you to regain control of your mind, thus offering you the chance to focus on your goal.

### 4. Challenge your weaknesses to grow stronger. 

Undoubtedly, you'll be confronted by many things in your life that make you uncertain: things that make you feel afraid or like you can't succeed. What's the best way to handle these kinds of challenges so you can create the kind of life you want?

You have to work hard to overcome your weakness and, in doing so, build a stronger character.

We naturally fear anything that might cause us harm, so we often do whatever we can in order to avoid those things. While this strategy is understandable, it's also limiting.

In fact, we can accomplish so much more without the limitation of staying in our comfort zone.

One executive demonstrated this during a mountain climb during which he overcame his fear of heights. As he climbed the mountain, he panicked and flipped upside down, hanging on only by his rope.

Instead of rushing to his aid, his coach told him to "stop screaming" and focus on overcoming his fear. In spite of the panic, he was able to calm down and right himself. Ultimately, he learned that if he challenges his fears, he will be stronger for it.

In order to excel at overcoming your weaknesses, you must first cultivate your _discipline_, _drive_ and _determination_ :

  * Discipline creates habits, e.g., the habit of confronting (and not fleeing) fear

  * Drive is the motivation behind the habit, i.e., the belief that you can reach your goals

  * Determination is your commitment to your mission, i.e., working extremely hard to accomplish your goals

You can use these principles to overcome fear and expand your possibilities. If you're afraid of heights, for example, you can develop the _discipline_ to create the habit of looking down from a new height every day. You can also cultivate your _drive_ by telling yourself that nothing can stop you. Then, use your _determination_ to enact your new habit every day without exception.

### 5. Cultivate mental toughness by banishing fear and nurturing positive thoughts. 

Those who have _mental_ _toughness_ are the first to jump into any challenge and the last to throw in the towel; they never quit, and they face challenges with a smile. But how do you cultivate mental toughness?

To forge your mental strength, you need to have control over your thoughts and mind.

One way to accomplish this is through positive thinking.

Try this trick: imagine there are two dogs inside you, a courage dog and fear dog, and the idea is to to feed the courage dog and discipline the fear dog.

The fear dog is the source of your fear and unease, and thus weakens you mentally. Therefore, you want to be sure to appropriately discipline him.

The courage dog, on the other hand, is the source of your strength. He thrives on positive thoughts, such as "you can do this" or "I'll never give up," so you want to be sure to feed him as often as you can.

By banishing your fear and cultivating courage, you gain the strength to react appropriately in any situation.

But in order to cultivate this mental toughness, you also need control over your emotions.

Emotions don't only affect our minds, but also our bodies. For example, when we get angry, our muscles tighten; when we're afraid, we hold our breath, thus making it harder to control our performance.

One way to overcome this is by transforming negative emotions by feeding them with positivity.

For example, during his military training course, one of the author's classmates made a mistake that cost him his front tooth. He became so angry that his muscles began to tighten and his performance got even worse.

However, after having noticed these negative emotions, he was able to transform his anger into determination, thus giving him the strength to complete the course even faster.

### 6. When you’re connected with your subconscious, you achieve even greater accomplishments. 

Have you ever had the feeling something bad was going to happen, and it turned out that your intuition was right? This feeling is your subconscious and it can be used to your advantage.

In order to harness the power of your subconscious, you must be aware of all the various information gathered by your senses.

You'll need to slow down and collect the information through all your senses: from your ears, nose, eyes, mouth and skin. Your subconscious collects all this sensory data and uses it to help you better understand your environment and thus make better decisions.

For example, when you're having a conversation with someone, your ears collect large amounts of sensory data. But that doesn't just help you to understand your conversation partner's words — assessing their tone of voice also helps you gain a deeper understanding of what they're trying to convey.

In order to best access your subconscious, you should employ the two following techniques.

The first is _focused_ _awareness_. This type of awareness involves laser-sharp focus where your conscious mind is fully engaged in processing information.

The second is _relaxed_ _awareness_, i.e., not focusing on anything in particular and gathering a bigger picture of your surroundings.

Employing these two types of awareness in succession helps information pass from the conscious to the subconscious mind, where it will remain until your brain recalls it in the form of inspiration or strokes of genius.

In fact, Einstein employed a similar technique by napping after a period of deep focus. His "aha" moments often came to him during or after his naps.

Indeed, your subconscious can be a great source of inspiration, so long as you make use of all of your senses and correctly store the information they've gathered.

Now that you've gained some knowledge into how to structure your mind for success, the final blinks will focus on the tactics and strategies you can use to accomplish your mission.

### 7. Build an enterprise that resists failure by diverting resources to where they’re needed. 

Why is it that 95 percent of startups fail within their first five years? Because the business world is volatile: new technologies can make products obsolete, investors can abandon projects, etc. But is there any way to mitigate these risks?

While you can't avoid risk altogether, you _can_ make your business more resilient by _selecting_ _high-value_ _targets_. This process helps you optimally allocate resources by knowing what's most important.

You can assess a target's value by using _FITS_ (fit, importance, timing and simplicity):

  * Does your target _fit_ the specific skills of your team?

  * Is it _important_ in order to achieve your overall mission?

  * Is it a prudent _time_ to tackle this target?

  * And is the target _simple_ and clear or rather complex?

For example, imagine you're running a factory and your goal is to adopt carbon-neutral technology. You can use FITS to assess the practicality of your goal. You should ask questions like:

  * Is my team qualified to install and implement this new technology, i.e., are they _fit_ for the job? If not, do the benefits of this new technology outweigh the cost of training my workforce?

  * Will it help you be a better company, i.e., is it _important_?

  * Do I have the resources available to achieve my goal, i.e., is it the right _time_ to make these changes?

  * Finally, is the project _simple_ enough to be realized efficiently?

If the answer to any of these is no, then you should seriously consider whether to begin the process of becoming carbon neutral.

If you decide to start the project anyway, you run the risk of being unable to cope if something unexpected happens due to the fact that you've diverted your resources elsewhere.

### 8. If a product is not working well or to capacity, don’t patch it up: break and improve it. 

Sometimes we've invested so much of ourselves in our ideas and inventions that they're hard to let go, even when it's necessary to. However, rather than tinker with a defective product until it works, sometimes you have to break and totally rebuild it.

Whenever your product "breaks," you shouldn't get upset, but instead welcome the failure, as it's a welcome chance to learn from past mistakes.

For example, the body-building product Body Rev failed because it was too complicated to use and targeted the wrong market. Instead of tweaking the product to make it simpler or seeking out a new market, the inventor decided to simply build an entirely new product: the Perfect Pushup. The inventor learned from his mistakes by creating a simple product from scratch and targeting a more lucrative market.

However, it's better to not have to wait until something fails before improving upon it. Instead, you should constantly think up innovative ways of making your products and ideas better.

One way to do this is by critically examining your own work as well as studying rival products and assessing their strengths and limitations. Indeed, an unwillingness to actively look for problems with your own product will leave you stuck in the status quo, and you won't be able to think outside the box and stay innovative.

Imagine, for example, that you've created a reasonably successful product that works fine. Sure, it has a few issues, but you've only had a few customer complaints and finding a solution might be difficult.

You may choose to not take action, but your competitors certainly won't. They'll probably spot even this tiny defect and develop their own innovative solution. Before long, they'll have stolen all your customers and left your company in the dust!

### 9. Don’t get stuck in defense mode: always be on the offensive. 

It's a fact of life that there will always be conflict both in our personal lives and in business. You should always be ready to deal with these conflicts. However, that doesn't necessarily mean preparing your defenses. Instead of reacting when conflict strikes, you should always be in _offensive_ _mode._

One way to do this is rework your defensive language vocabulary and turn it into part of your offense.

Given that the words we use affect people's attitude towards a given situation, we have control over whether they function defensively or offensively.

Words like "good," "can't," "try," "maybe," "failed," etc. produce images of uncertainty and doubt. However, if we substitute them with words like "great," "do," "definitely," etc., then you can change your language in a way that uses stronger, more decisive and positive words.

If you make a new habit of using assertive, positive language, you can gain the upper hand in both your personal and business relationships.

Furthermore, "offensive" mode helps you to use surprise and speed to get an edge over your competitors.

In any business in this modern world, the ability to swiftly react is essential — technology and the business environment are constantly changing.

Indeed, a business's very survival depends upon quickly reacting to these changes, lest they be overtaken by those businesses that can adapt more quickly.

Take Apple, for example. They constantly receive praise for their creativity, yet they aren't the quickest in their field. That award goes to Samsung, which, after being faced with the iPad, went on the offensive and produced a rival tablet so swiftly that even Apple was caught off guard. Thus, Samsung was able to use its speed to quickly adapt and get ahead.

By remaining on the offensive, you too can catch your competitors off guard and take advantage of their slow reactions.

### 10. Final Summary 

The key message in this book:

**If you want to achieve your goals, you must first define your purpose and then live by it. In addition, you should carefully sculpt your mind with positivity so that you can attain maximum focus and convey strength.**

Actionable advice:

**Feed the right dog.**

Imagine two dogs living in your soul: the courage dog and the fear dog. Each time you succumb to fear, you feed the fear dog, and each time you overcome fear, you feed the courage dog. The more you feed them, the more strength they have and the more influence they have on your life. If you want to overcome your fears, then you need to nourish your courage dog while disciplining the fear dog.
---

### Mark Divine

Mark Divine began his professional career as a public accountant, later completing his Navy SEAL training and retiring with the rank of commander in 2011. In addition, he has started a number of entrepreneurial ventures, such as SEALFIT and NavySEALs.com.

