---
id: 53c631363839340007710000
slug: the-wealth-and-poverty-of-nations-en
published_date: 2014-07-15T00:00:00.000+00:00
author: David S. Landes
title: The Wealth and Poverty of Nations
subtitle: Why Some Are So Rich and Some So Poor
main_color: None
text_color: None
---

# The Wealth and Poverty of Nations

_Why Some Are So Rich and Some So Poor_

**David S. Landes**

In _The_ _Wealth_ _and_ _Poverty_ _of_ _Nations_ (1998), author David Landes explores the origins of economic thinking, illuminates the factors that influenced the rise of capitalism and explains why capitalism found fertile ground in certain parts of the world and not others. The book provides a detailed description of nations that have been extremely successful and explains why the Industrial Revolution was destined to occur in Europe.

---
### 1. What’s in it for me? Discover the origins of modern capitalism and the reasons for economic inequality. 

Why is it that some nations are extremely rich while others remain incredibly poor? Where did this inequality originate, and why is it not as easily abandoned as some people claim?

Ultimately, much of what is happening in today's national economies is dependent on what happened hundreds of years ago, when cultural foundations and attitudes toward work, money and prosperity were established. 

As it turns out, geographic and cultural factors play a far bigger role in the accumulation of wealth than is commonly thought. In these blinks, you'll find a fascinating explanation of the world's economic structures.

You'll also discover:

  * why the invention of the mechanical clock was one of the most important developments in human history,

  * how the Japanese car industry was able to become a world export leader within just thirty years,

  * how Protestantism was a key influence in the rise of capitalism, and

  * why the Industrial Revolution began in Britain and not elsewhere in the world.

### 2. The concept of property rights made economic development a Western notion. 

Many of us think of the Western world's economic success as the result of sheer luck. But, in fact, its foundations were laid over 1,000 years ago, when European cultures began to establish the concept of property rights.

This was in stark contrast to other cultures, such as the despotic empires in Asia and ancient Greece. Political power then was based on oppression: if a ruler wanted something, he simply took it from those beneath him — peasants or slaves — who could create wealth, but not own it themselves.

This lack of property rights was an obstacle to economic development. After all, why would one put effort into acquiring wealth that one would not be allowed to keep?

In the nomadic Germanic tradition, on the other hand, each warrior looked after and defended his own possessions. Because these individuals had an established right to own and hold property, they had an incentive to maximize their wealth: whatever they acquired would belong to them.

Because property rights could be defended and secured, gains based on productivity were preferable to those derived simply from taking from the weaker members of society.

The first place where these ideas were put into practice was in the western European _commune_ — a semi-autonomous city, with a government by, for and of the merchants. In a commune, all citizens were allowed to conduct their business free of interference from outside the city and regardless of their social status.

In fact, when a medieval count of Flanders attempted to reclaim an escaped serf in the markets of Bruges, the townspeople drove him out of their city.

Although these developments were often accidental rather than planned, they were the inspiration for the accumulation of riches throughout society. Of course, equally crucial to economic success is _productivity_ — as we'll see in the next blink.

### 3. The Middle Ages were a period of technological innovation, which enabled gains in efficiency. 

Although the Middle Ages are often seen as a dismal, dark interval between the fall of the Roman Empire and the blossoming of the Renaissance, the period was actually one of the most inventive times in human history, leading to gains in efficiency and overall technological development.

For example, the efficient use of water to power mills resulted in greater productivity. Specifically, the invention of gears made it possible to use the power of water sources located at a distance from the mill, and even control the direction of the water's flow. Mills helped automate grinding grain, pounding cloth, hammering metal, mashing hops for beer and pulping rags for paper. In fact, while China invented the manufacture of paper, the process was first mechanized in Europe.

Another significant and pivotal invention of the Middle Ages is the mechanical clock, which enabled the coordination of collective work or business, and which crucially brought with it the concept of maximizing production per unit of time.

Previously, it was the church that determined the day's structure, with individuals organizing their lives by its bells: the church told them when to wake up, when to start work and when to open the markets.

And although the church for more than a century resisted the clock as a new way of keeping time, the clock quickly became a standard in cities. Clocks were installed in public places so they'd be visible to everyone.

This public display made possible the organization of personal and collective labor, and enabled individual autonomy in business.

In contrast to Europe, other civilizations — such as in China and the Middle East — had developed water clocks long before the European invention of the mechanical clock. However, unlike the Europeans, they had no interest in creating a public sense of time.

This was because, until the late sixteenth century, the religious and imperial authorities of these regions considered public time, and the autonomy it would allow individuals, a threat to their sovereignty. In short, they wanted to be "in charge of time," so they kept the technology from the people.

Though these innovations might seem trivial today, they were both key to economic success. Since prosperity is influenced directly by an increase in labor productivity, any tool that aids productivity is a tool to accumulate wealth.

### 4. Protestant values encouraged the accumulation of capital. 

After the technological advances of the Middle Ages, Europe — and later America — benefitted from another change in the seventeenth century: the rise of Protestantism, which in turn prompted the rise of industrial capitalism.

What was it about Protestantism that was so conducive to economic growth?

Protestantism defined a work ethic that was at the foundation of economic success. Specifically, Calvinism — a predominant form of Protestantism in Germany and the Netherlands — encouraged hard work, honesty and thriftiness. These traits constituted the foundation of the Protestant work ethic.

Also, the Calvinist doctrine of _predestination_ — that is, that God has determined your life's path — led people to pursue goodness in order to show that they indeed were the "chosen" ones. To have acquired wealth through your own hard work and honest ways was thus considered a further sign of divine favor, so people were encouraged to act likewise so they too could accumulate riches.

However, this is not to suggest that _before_ Protestantism no one wanted to be rich. Rather, it means that the doctrine produced a type of businessman who pursued his life in a certain fashion — with wealth as a by-product. What was previously considered an exceptional personality type was now encouraged by religion.

And although the original doctrine of predestination didn't last longer than a few generations, the success-oriented mindset it created remained.

With their work ethic, Protestants rose quickly to leading positions in society, with Protestant merchants playing a leading role in trade, banking and industry.

For instance, the Swiss Protestant cantons became centers for the production of watches, machinery and textiles, while, in contrast, Catholic regions remained agricultural. Furthermore, in the factories of western Germany and France, the employer was usually Protestant while employees were Catholic. And later in England, industrialization was in large part supported by the activity of Calvinist merchants.

With private property, technological efficiency and a new work ethic, the Western world was primed for economic success. Yet the eighteenth century would bring with it even greater success: the Industrial Revolution.

### 5. Britain’s distinctive social situation made it fertile ground for the Industrial Revolution. 

The Industrial Revolution changed everything: it substituted machines for human handiwork and used new raw materials in production. But, while many countries had sufficient resources to support such a development, it was in Britain where the Industrial Revolution began.

Why?

By encouraging national and not only personal growth, Britain provided a common, national identity for its citizens — particularly in contrast with the divided Germans. As a result, British citizens were much keener on combining social purpose and a stable, safe environment with individual goals such as accumulating property.

One key aspect of this identity was the freedom and safety of industry and enterprise, which Britain provided for the ventures of its citizens.

In contrast to other nations, Britain included almost no religious and social constraints as an aspect of membership in craft and trade guilds.

In this way, they were able to benefit from the knowledge of refugees — among them Dutch, Huguenots and Jews — who came from families of merchants, craftsmen and bankers with a large number of business connections.

Most importantly, the high consumption patterns of all citizens regardless of class encouraged technological progress and, in turn, an increase in wealth.

Traditionally, products were manufactured exclusively for the rich, who had the means to purchase expensive goods. However, the mechanization of labor during the Industrial Revolution led to two crucial changes.

First, workers were paid higher wages as factories generated higher profits. This meant that workers could then consume more, which, in turn, injected more cash into the economy. Furthermore, this meant greater profits, even more jobs, higher wages and, in turn, greater consumption.

Second, products aimed directly at middle- and lower-class female consumers — such as combs, buckles or buttons — were introduced. Due to the mechanization of labor, these could be produced and sold more cheaply and thus appealed to all classes, not only the rich.

### 6. Other places, such as India, lacked a crucial incentive to replace human labor with machinery. 

The world's cotton industry in the seventeenth and eighteenth centuries was centered in India, so — at least in theory — there should have been a keen interest in automating cotton production there.

However, this wasn't the case. But why?

There are several reasons:

First, it was far easier and more convenient to hire more workers than to introduce new technology. From an economic standpoint, hiring women or "untouchables," individuals outside India's rigid caste system, for spinning or weaving was simply a lot less expensive. Thus there was no financial incentive to invest in technology when increased productivity could be achieved more cheaply by hiring more laborers.

Also, in contrast to Europe, Indian merchants had no involvement in the production process. Their only role was to pay workers' wages and let the spinners and weavers do their jobs. As they didn't need to consider the particulars of the production process, they had no personal interest in transforming it.

Second, workers thought of hard labor as their fate, so they had no interest in remedying a situation that they essentially had no social or political power to change.

The introduction of equipment or machines — the single most-important factor in the genesis of a revolution in industry — is the direct result of a desire to make tasks easier to perform. However, because workers were socially molded by India's caste system, they accepted hard labor as entirely appropriate to their lives.

Finally, Indian craftsmen avoided using iron. Traditionally, the Indian system depended on wood, rope and pots, using iron nails rarely, and screws never. But without iron, precise work in mechanization was impossible. Interestingly, one of the best Indian pistol factories of the eighteenth century actually manufactured each component individually. Yet Indian workers simply couldn't — and wouldn't — work with iron-forged precision instruments.

> _"The Industrial Revolution fragmented the globe by estranging winners and losers."_

### 7. Technical knowledge is the single most-important factor in the creation of a nation’s wealth. 

We have already seen how important the new techniques of the Industrial Revolution were to the accumulation of wealth. But not all countries developed technologies independently of each other. Indeed, much of this knowledge was transferred — or even stolen.

Why? Because building on the experiences of others can save a lot of time and money.

Certain knowledge can be acquired only through experience. A good example of this can be seen in the manufacture of crucible steel, which was invented in Britain in 1740.

Like all steel, this was a very popular material for industrial purposes, but the way it was made offered a major advantage: its carbon additive is mixed in completely during the manufacturing process, for optimal hardness.

Having an obvious interest in the technology for the manufacture of guns, knives, files or edge tools, the French attempted to copy the technique, but to no avail.

Finally, in 1820, after having spent several years and a lot of money on research, a British expatriate taught the French how to make crucible steel.

However, while there's a clear disadvantage to being a latecomer to technical innovation, there's still a chance for success, as latecomers start with the latest and most efficient techniques.

In most cases, early technologies require a lot of fine-tuning and adjustments to be economically viable. This explains how industrial concerns in underdeveloped countries are able to quickly catch up with modern, capital-intensive industries in other countries — by emulating the latest techniques instead of inventing them from scratch, companies save money and avoid time-wasting errors.

Moreover, latecomers have more to gain in terms of competitiveness than the original inventor. By jumping directly into the fray using an industry standard, a latecomer is able to quickly close the wide competitive gap between itself and competitors, an advantage it would not have if it tried to innovate slowly on its own.

As we've seen, technical knowledge is the single most-important factor in the creation of a nation's wealth. Yet there are other aspects which are crucial to ensuring that industries actually flourish from the knowledge they have gathered.

### 8. Industries are born from the right balance of talent, technology and research drive. 

There is more to economic success than pure technical know-how, and the decline of the British chemical industry is a great example of why this is true. While the British possessed the proper techniques and technologies, a culture of research which would have fostered innovation didn't exist.

Indeed, without research facilities, industrial enterprises cannot flourish.

For example, when William Perkin discovered a synthetic method to produce dye in 1858, Britain quickly became the market leader in the new industry for artificial colors. Not only did Britain have ample quantities of the necessary raw material, tar, but it also offered the biggest market for textile dyes.

However, within twenty years, Germany was producing half of the artificial dye in the market, and by 1900, the country's production claimed a massive 90 percent of the market.

What was behind this change?

Britain simply didn't have enough top chemists engaged in dye research. Moreover, the chemical industry was too far removed from universities and colleges to tap into research resources there, in order to encourage further innovation.

In contrast, in addition to enterprise, a pool of talent with access to top-notch research facilities will stimulate healthy competition and cause the rise of new industries.

This can be seen again in Germany, where major companies such as BASF, Agfa, Bayer or Hoechst flourished because of their close affiliation with universities and world-class chemists with in-house laboratories. The means to experiment, competition and profit were incentive enough: in just seventeen years, 152 patents for the synthesis of indigo dye were registered, effectively halving the price of the product.

By 1918, German chemists had become so successful in modern chemistry that even the confiscation of German industrial patents did not help their overseas competitors. In fact, British and American companies were forced to hire German chemists so they could learn their techniques and put them into practice.

### 9. By underestimating the value of high-quality cotton mills, Egypt cannibalized its world-class raw cotton industry. 

Egypt's investment in cotton mills seemed foolproof. After all, cotton was a sought-after product, and Egypt was home to the best long-fiber cotton in the world. Furthermore, by processing the cotton themselves rather than selling raw cotton, Egypt's mills would be able to rake in greater profits.

In 1822, a French expat developed a new strain of cotton for Egypt, suitable for the finest yarn and cloth. By 1845, the country had exported 15.5 million kilograms of it. Furthermore, the funds generated from the export of raw cotton were poured back into education and industry.

It seemed like an auspicious start.

However, following World War II, foreign weavers invented better mills that improved the quality of their cloth, even with lower quality cotton. Egypt, meanwhile, maintained its traditional mills. 

Because the country had failed to take these technological changes into account, Egypt's industrial transition from cotton farming to manufacturing did not result in the establishment of a global industrial standard — and with fatal consequences. The Egyptian mills produced yarn that simply wasn't of international quality anymore.

Egypt now had inferior cloth and less raw cotton to export. Since merchants weren't able to sell the cloth internationally, they flooded their home market with it. Worse still, the cotton used in the manufacture of their own cloth was now costing them part of their export market, as raw cotton was in high demand, but they had less of it to sell.

So although Egypt started their enterprise amid ideal conditions, their industry crumbled.

### 10. The Japanese made a virtue of their country’s relatively small size by coming up with customization and “just-in-time” stock systems. 

One of the most successful conversions to modern capitalism happened in the Japanese automobile industry. This is surprising because their home market — where economies traditionally begin to grow — was too small for mass production of the kind seen in the American car industry.

So, how did it happen?

First, by focusing on demand, Japanese companies were quicker to introduce new models and became more competitive than other car manufacturers.

In contrast to the American car industry, in which many identical cars are produced and sold cheaply, the Japanese catered to specific needs and thereby evolved to design and test their cars faster. For example, it took a Japanese manufacturer just forty-six months to produce a new car model, compared to sixty months in the United States.

This speed enabled companies to identify successes and eliminate the flaws and mistakes of previous designs.

In 1950, after the war when many of its industrial areas had been destroyed, Japanese companies produced just 32,000 cars. Yet by the 1980s, Japan was shipping over 6 million cars annually, surpassing West Germany as the largest automobile exporter and the United States as the largest car manufacturer worldwide.

Second, a focus on conserving resources led Japanese innovators to establish a system of "just-in-time" production. This system ensures that the right material is available in the right place at just the right time, and in the exact amount required — which effectively reduced costly inventory.

While the American consumer market is large enough that profits can be made even in a market saturated with products, the Japanese market had to become profitable by eliminating wasteful inventories and improving efficiency in production.

Once again, this advance was merely a result of the size of their country. Japanese companies simply couldn't afford to maintain huge stock and have everything available at all times — as American manufacturers did — because demand was, at the beginning, not high enough to finance increased storage costs.

As this shows, with the right mindset and by making a virtue of one's situation, one can achieve unrivalled success.

> _"The Japanese […] built their recovery on work, education and determination."_

### 11. Final summary 

The key message in this book:

**We** **can** **explain** **economic** **success** **and** **failure** **by** **investigating** **the** **conditions** **under** **which** **certain** **cultures** **developed.** **Historic** **accidents** **based** **on** **geography** **or** **power** **structures** **came** **to** **establish** **a** **specific** **mindset** **and** **attitude** **in** **certain** **parts** **of** **the** **world,** **which** **then** **contributed** **to** **gains** **in** **productivity** **and** **the** **accumulation** **of** **wealth.**

**Suggested further reading:** **_Civilization_** **by** **Niall** **Ferguson** **and** **_The_** **_Bottom_** **_Billion_** **by** **Paul** **Collier**.

To even better understand the underlying causes of poverty, be sure to read Niall Ferguson's _Civilization_, in which global differences in economic development over the past 500 years are examined.

Another solid read on the topic is _The_ _Bottom_ _Billion_ by Paul Collier, a book that focuses on the most pressing problems faced by poor states today. The key insights from both titles are available in blinks.
---

### David S. Landes

A professor of economics and history at Harvard University, David Landes was a preeminent scholar of his generation. He also penned the critically lauded _Revolution_ _in_ _Time_ , which analyzes the links between capitalism and the invention of the mechanical clock. Landes passed away in 2013.

