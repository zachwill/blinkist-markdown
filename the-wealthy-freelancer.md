---
id: 55a39ced3738610007070100
slug: the-wealthy-freelancer-en
published_date: 2015-07-16T00:00:00.000+00:00
author: Steve Slaunwhite, Pete Savage, Ed Gandia
title: The Wealthy Freelancer
subtitle: 12 Secrets to a Great Income and an Enviable Lifestyle
main_color: FC3239
text_color: C9282E
---

# The Wealthy Freelancer

_12 Secrets to a Great Income and an Enviable Lifestyle_

**Steve Slaunwhite, Pete Savage, Ed Gandia**

_The Wealthy Freelancer_ (2010) reveals the secrets behind a successful career in freelancing. These blinks outline how you can focus, structure, manage, organize, market and price your freelance business in the most effective and fulfilling way possible.

---
### 1. What’s in it for me? Maximize your freelancing potential. 

Would you like to be your own boss? Work your own hours? Only have clients you want to work for? Many do, and this is why so many people are becoming _freelancers_.

And yet, for all these plus sides, being a freelancer can be a struggle. All freelancers know the pain of not being able to go out with friends because of a late running project, or begging for rent money from parents because work is going slow.

But it doesn't have to be this way. You can have the freedom _and_ security, but how?

The following blinks contain 12 secrets that every freelancer, no matter what their expertise might be, should commit to memory.

In these blinks you'll discover

  * why every good freelancer needs a buzz piece;

  * why you should turn your day into a jigsaw; and

  * why you need to live in the middle of a wealthy triangle.

### 2. A successful freelancer accepts that there are times when business isn’t great. 

Every freelancer, no matter how relaxed they seem, is _constantly_ under pressure. Even when business is booming and money is flowing in, certain fears linger in the back of the mind. What if work dries up next month? What if a competitor undercuts me next week?

However, these fears aren't a reason to reject freelancing. Rather, by accepting the realities of constant pressure in freelancing, you'll unlock the first secret to success.

Because it is always possible that work will dry up, or a rival will snatch your clients, you'll need to prepare yourself for the moment catastrophe strikes. Use the acronym _IDEA_ to memorize the four techniques that will help you think positively, no matter what the situation.

First, you'll need to _Invest_ in your success. There's a whole host of coaching services, books, teleseminars and online resources available on how to make your freelance business prosper. So use them! Learn from the achievements of successful freelancers, and learn from their mistakes. In short: invest in learning and get as much information as you can.

Next, _Develop_ an unshakable belief in yourself. Think of J.K. Rowling, Warren Buffett and Stephen Spielberg. If they strike you as people who've achieved what you'd never be able to, think again. They're normal people too, and just like you, they had to start somewhere. Nobody ever got anywhere without a big dose of self-belief.

But you've also got to _Expect_ that this belief will be tested. There are always times of self-doubt. Before her worldwide success, J.K. Rowling found it hard to get published at all, and money was very tight to say the least. Just remember that these dark moments are always _temporary._ Tell yourself that they'll pass _,_ and they surely will.

Finally, be sure to _Absorb_ that satisfying feeling of success. If your business is going well, enjoy it! It's exactly what you've been working so hard for.

With the IDEA acronym in mind you'll always have something to guide you when the going gets tough. Now that you've uncovered the first secret to success in freelancing, it's time to dive in. It all starts with finding the right clients.

### 3. To get the clients you want, carefully market your services to the right people. 

As a freelancer, you take on the challenge of finding your own work. Which of course means finding clients who want your skills and expertise. But who are these clients? Where are they? And how can you attract them to you?

Enter the second secret of freelancing: the _Master Marketing Formula._ This formula will lead you right to your desired clients in five steps.

It starts with some list-making. Compile 150 to 200 names of organizations and industries where your skills, experience and knowledge will give you the best chance of attracting clients. It's vital that you get specific with this.

Suppose you're a freelance marketing consultant who is highly experienced in working for marketing managers in midsize, up-and-coming software firms. This narrowed focus will allow you to determine which potential clients suit your skills best.

On to the second step, in which you start generating leads. Start working your way through the list of companies that you've drawn up and making contact with those parties. If they seem interested, provide them with additional compelling information. Outline your area of expertise and highlight your previous successes in a way that demonstrates just how suitable you are for their needs.

If your lead is genuinely interested, then it's time to make step three happen: turn them into clients. Provide them with any extra information they request, and give them more. Offer as much help and assistance as you can so they can see how great it would be to work with you.

Prospect still with you? If so, let's move on to the fourth step of this formula. Give your client a quote for your services. If you and the client are happy with the agreed price, then close the sale!

And what if you don't get the job in the end? Follow step five and don't give up! Instead, stay in contact with the prospect. This way you'll be the first person they think of when they have a new project and need a freelancer.

This formula is a powerful tool for finding the work you want to do. But if you want to open up your options even further, then you might want to put the third secret of successful freelancers into action. What is it? Find out next!

### 4. Create a brilliant buzz piece to position yourself as an expert in your field. 

As we found out back in the first blink, there are inevitably times when work is scarce. If you want to give yourself the best chance possible of always having a project on your hands, then the third secret is something you should consider: writing a _buzz piece._

Usually a short report between five and ten pages long, a buzz piece will help you to position yourself as an expert in your industry. You've got a whole range of options when it comes to format: a workbook, checklist, toolkit, how-to guide or a simple list of tips are all great choices. It's choosing the right _topic_ for your buzz piece that is more of a challenge.

Take Dave, for example. As a graphic designer, he put time and effort into a buzz piece that described how to design an effective logo. But after sending it to 350 companies, he didn't receive the response he was hoping for. In fact, he hardly received a response at all!

His structure and content were great, so what was the problem? He'd chosen the wrong topic. Think about it: Who'd be interested in a special report on drafting logos? Other graphic designers.

Dave had created a buzz piece that did indeed demonstrate his expertise, but he was addressing his colleagues when he should have been addressing potential clients instead. By changing the topic to choosing the best logo for a business, Dave received a far better response from prospects.

Of course, a buzz piece with a great focus is nothing without a great title. All you need is a headline that makes a prospect think something along the lines of "Wow, that sounds interesting, I could really use this information." The title of Dave's second buzz piece was "A Five-Step Strategy for Selecting the Perfect Logo Design," a perfect example of how your title can draw clients in.

Put time and effort into a well-focused buzz piece with a title that does it justice and you'll have something to be proud of. Next time you want to give a prospect your business card, give him a free copy of your buzz piece, too.

### 5. Employ high-impact marketing tactics that are efficient and bring you returns. 

Finding clients is one hurdle for freelancers, but keeping them is a whole new challenge. With the right strategies, however, maintaining existing client relationships while building new ones will be a breeze.

One great strategy that every freelancer should be making use of is the _Marketing Effectiveness Matrix._ By assessing your MEM, you'll know exactly which marketing strategies are the best for attracting and maintaining your clients.

We can think of marketing as comprised of four quadrants. The first quadrant contains tactics that are highly effective and time efficient. This includes encouraging work with existing clients and direct mailing. The second quadrant entails tactics that are effective, but take a little more time to develop, such as public speaking, networking or e-newsletters.

Third quadrant strategies require some effort, but give you little in return most of the time. For example, posting on online job boards or developing a social media presence. The fourth quadrant represents tactics that take up lots of time, but don't give you much in return — cold calling is a typical example of this.

Now that we know which quadrants do what, which should you focus on? That's right, the ones that give you the best returns. A smart freelancer will therefore use a combination of strategies from quadrants one and two.

The way you approach quadrant two depends on your specific area of expertise. Techniques from quadrant one, however, can be applied to any discipline. Every client you gain is an opportunity for repeat business, but only if you treat them right. It's vital that you develop strong personal relationships with clients.

For example, let's say you've started working as a freelance copywriter for a company. Instead of just doing your job and saying goodbye as you flee the office, you could ask to be introduced to other employees, then strike up a few friendships. If you stay in touch with coworkers even after your project together is completed, you'll be the first person they think of when there's another project on the horizon.

### 6. Avoid lean times by nurturing prospects perpetually. 

If there's one thing freelancers complain about, it's the fact that their jobs are either incredibly busy with lots of work, or exhaustingly slow with every prospect out of reach. Little do they know that this problem is quite simple to solve!

The easiest way to avoid those slow periods in your freelance work is by keeping in touch with previous clients. That means both email and phone, so you can maintain your relationship after the project is over. But what if you've already dropped out of touch? It's not too late! You can add them to your monthly e-newsletter, or even just drop them a message that looks a little something like this: "Hi, came across this article recently. Thought you'd find it interesting." Sent with your business card, it's a gentle reminder to your client that yes, you do still exist!

Encouraging repeat clients isn't the only way you can minimize the time spent twiddling your thumbs. Another way is to use the _Discover_, _Identify and Position (DIP) Technique_ to focus your work _._

Imagine you're a US freelance photographer of Egyptian descent, and with lots of experience.

First, _discover_ your passion. What is it that sets you apart? For you, your Egyptian heritage, love of traveling to that country and strong knowledge of its art and culture.

Next, _identify_ the market you could work for. In your case, you might want to focus on companies that offer services and products related to Egyptian culture or history.

Finally, think of your _position_ in that market. By looking at what you've discovered and identified, you can position yourself as skilled freelancer for importers and exporters of Egyptian goods, Egyptian tourism firms and Egyptian-to-English translation services. With the DIP technique, you won't have to scramble for work, because you'll already know exactly where and how to get it!

### 7. Price your services for success in your freelance business. 

How much is your work worth? That's a question that scares many solo professionals. Why? Well, as an emerging freelancer it can be very difficult to find an appropriate fee.

Charging an hourly rate often seems like the easiest option, but don't be fooled! There are some significant disadvantages to it.

Firstly, clients don't like it. One freelancing expert even noticed that when he told clients that he charged services per hour, they began speaking faster on the phone immediately!

When you bill an hourly rate of, let's say, $45 per hour, your income is determined by the number of hours you work each week. So if you spend 35 hours per week working on projects for clients, your income will be around $45,000 annually. That's not bad, but you will never get much better, and you have to consider that you won't get paid for hours you spend on bookkeeping and marketing.

Being a _project pricer_ is far more advantageous. Project pricing means that you provide your client with a fixed price for the project work. One of the benefits of this type of pricing is that your client won't know the amount of hours you spend on the project, and you won't have to prepare tedious timesheets for him. You can charge far more for your expertise. This means that as you get better and faster at your job, prices go up, instead of going down as your hours shrink.

### 8. Get more done by working in focused bursts and carefully organizing your tasks. 

Who wouldn't want their commute to work to be as short as walking into the room nextdoor? Working from home is one much-coveted benefit of freelancing. But working at home can make it incredibly hard to concentrate.

Luckily, your lack of focus is easily solved with two clever techniques. The first of these is the _50 Minute Focus_, which will help you structure your workday as efficiently as possible.

The 50 Minute Focus means selecting the project you need to work on for the day, setting an alarm for 50 minutes and getting to work. Don't check your emails, don't take a break and don't let your mind wander. If that sounds hard, remember that you only have to do it for less than an hour. When your timer rings, stop working and do something that lets your mind unplug completely. When you're ready, do it all over again.

One freelance graphic designer and mom found that the 50 Minute Focus took her productivity to a whole new level. She used to spend all day working, getting distracted constantly. But after adopting the process she found she could do all her work in just _three_ 50 minute focused sprints!

Another invaluable technique to keep yourself productive is the _Jigsaw Puzzle Visual._ Picture the tasks on your to-do list as pieces of a larger puzzle. Less urgent tasks are smaller puzzle pieces, while more important ones are larger. As you work on these tasks, keep updating how your pieces fit together.

For example, suppose your puzzle has four pieces: your direct-mail campaign, a current client project, a commercial real estate opportunity, and preparing the proposal for an article you want to write. After finding out that your proposal has been accepted, you'll need to reprioritize your time so that you can spend a few hours each week writing it.

Thus the article piece of the puzzle will grow, which means other pieces have to shrink. This technique is incredibly helpful, not just because it keeps you aware of your priorities, but also because it reminds you that using your time productively isn't about working more. It's about fitting things into the bigger picture, so that you get things done without burning out!

### 9. Create alternative streams of income and live in the Wealthy Triangle to ensure a strong work–life balance. 

There comes a time in your freelancing career when you spend almost all of your time on work, while your personal life suffers as a result. But it doesn't have to be this way! If you want to maintain a strong cash flow but also catch a break when you need it, then start thinking about how you can maximize your _passive income._

Active income is everything you earn while you work. Passive income is money you can earn in your sleep. Sounds crazy? Let's look at an example. A scout sells lemonade every hot Sunday afternoon. He earns ten cents for each beverage he sells to his neighbors. But when his mother calls him in for dinner, he has to stop selling. As a result, the dimes — his active income — stop rolling in.

Now let's imagine this scout is clever, and publishes a neat little recipe book on how to brew the perfect lemonade. Assuming the book sells well, this scout can earn money even while he plays football with his friends. That's the benefit of passive income.

Start brainstorming what alternative, passive streams of income you can create from your skills, and you're on the way to becoming a wealthy freelancer.

A very common problem freelancers face when it comes to work–life balance is that the more they earn, the less free time they have. Living and working in the _Wealthy Triangle_ means you're at the point where you have a) great income, b) freedom to choose the jobs you want, and c) the flexibility of a real freelancer.

By following all the steps in these blinks, you'll reach your maximum earning potential. Then compare this with how much you need to enjoy your life. Earn that amount as efficiently as you can, and you can then enjoy the free time you've worked for!

### 10. Final summary 

The key message in this book:

**Be sure of the goals, income and lifestyle you want to have as a freelancer and stay focused on every project, without losing track of future prospects, projects and streams of income.**

**Suggested further reading:** ** _The_** **_Fire_** **_Starter_** **_Sessions_** **by Danielle LaPorte**

_The_ _Fire_ _Starter_ _Sessions_ is a spiritual guide to how you can achieve success on your own terms. It will show you how to become successful — without sacrificing your true self — through practical exercises that will set your full creative potential free.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Steve Slaunwhite, Pete Savage, Ed Gandia

Steve Slaunwhite is a famous marketing coach, professional copywriter and author of books such as _The Complete Idiot's Guide to Starting a Web-Based Business_ and _Fast Track to Great Clients_.

Pete Savage and Ed Gandia are successful copywriters, marketing consultants and coaches.

