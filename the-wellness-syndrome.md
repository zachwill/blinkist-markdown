---
id: 587de6f2850310000464e842
slug: the-wellness-syndrome-en
published_date: 2017-01-19T00:00:00.000+00:00
author: Carl Cederström and André Spicer
title: The Wellness Syndrome
subtitle: None
main_color: EDE6BB
text_color: 877F50
---

# The Wellness Syndrome

_None_

**Carl Cederström and André Spicer**

_The Wellness Syndrome_ (2015) explains why a health craze that's sweeping the world may not be all that healthy for you. These blinks get to the root of why we're obsessed with making ourselves happier, healthier and harder-working — and who is reaping the benefits of this obsession.

---
### 1. What’s in it for me? Learn the truth about modern society’s obsession with “wellness.” 

We're all worried sick about our health. Even if you're fit, you may feel you need to diet. We don't sleep enough; we're constantly jumping on the next trend, be it superfoods or juicing.

What is causing society's unhealthy obsession with health? Previously only people who were sick made health a central issue. Healthy people, on the other hand, thought about politics, relationships or philosophy — in short, they focused on life in general, not the latest sniffle.

So what's different today? These blinks help you get to the bottom of the wellness craze to figure out how we can heal ourselves in the right way.

In these blinks, you'll find out

  * how being health conscious can harm a budding philosopher;

  * why eating cookies makes you feel guilty; and

  * how an obsession with wellness distorts government welfare policy.

### 2. An obsession with wellness has become an ideology, limiting freedom of thought and action. 

Many people consider the pursuit of wellness a lifelong goal. To stay on track, these people avoid "unhealthy" influences, whether it be eating fatty pork chops or smoking cigarettes. They head to a Pilates class daily and occasionally enjoy some pampering at a fancy spa.

Eating correctly and exercising daily — what's wrong with that?

Well, we first have to consider that "wellness" means more than exercising regularly and eating healthily. Wellness is an ideology; it holds that a healthy body is a necessary state for success and happiness in life.

Thinking this way marks a significant societal change. If you cared about your looks and health decades ago, people would think you vain and superficial. Today, the cult of wellness stresses that to be successful in everything in life, you need a fat-free, fit body and a clear, capable state of mind.

The problem is that adhering to such an ideology compulsively can limit your freedom of thought, causing you to miss important experiences.

The wellness doctrine is based on a narrow way of thinking that revolves exclusively around health while forbidding many activities such as drinking or taking recreational drugs. Such activities society used to view as not only enjoyable but also important rites of passage.

Many American universities now require students to sign a wellness contract upon enrollment. Students pledge to abstain from alcohol and drugs and devote themselves to living healthily.

Yet these straight-edge students will undoubtedly miss out on youthful experiences that once inspired society's greatest thinkers. French philosopher Jean-Paul Sartre famously indulged in coffee, cigarettes and alcohol while at school, his circle of friends enjoying discussions of absurdity and revolution.

Such experiences were mind-expanding! Contemporary students of philosophy pushed to follow the tenets of wellness might miss out.

### 3. Slim and fit are “good,” fat is lazy and bad. Society now equates healthy living with morality. 

So it's clear that being obsessed with health isn't healthy. But shouldn't this at least mean the end of people judging a person's character based on physical appearance?

Well, not exactly. People now face a moral imperative from society to be both healthy and happy.

Slovenian philosopher Alenka Zupančič has dubbed this _biomorality_ — people must take care of their bodies because _not_ doing so means they are stupid or irresponsible. It is a moral judgment — healthy people concerned about their bodies are good people, and unhealthy people are bad people.

Such a mentality explains why people view those who are obese as lazy or not trying hard enough, if at all, to improve their health. In doing so, obese people are evading their moral and social responsibilities.

Television has capitalized on this tendency to equate "fat" with "bad" and "healthy" with "good." English chef Jamie Oliver has a show called "Jamie's School Dinners," on which he has been critical of unhealthy eating among school children.

The show demonized parents for letting school children have food like potato chips and sugary soda.

While it is, of course, not healthy for children to consume processed foods, Oliver jumps at the opportunity on his show to shame offending parents, calling them indifferent to their children's health and welfare — an unfair assessment, as Oliver at no point takes into consideration a family's income or social situation.

Similarly, smoking has become another way to judge a person's social morality. Smokers are thought of as more "stupid" and "selfish" than people who don't smoke.

The crucial point is that biomorality has led us to focus all our attention on the physical body, socially shaming people into following a wellness lifestyle and in doing so, keeping them occupied with diets and fads as to not question the moral assumptions of such obsessions.

It's a never-ending cycle. The quest for a perfect body is eternal, and you can always "do better."

The irony is that, by striving to meet unreachable goals, eager to be on the side of the "good," we end up with less time to engage with people socially. Doing so might help us rediscover the things that make our peers morally upstanding, like being helpful or caring.

> _"When health becomes an ideology, the failure to conform becomes a stigma."_

### 4. The pressures of wellness can make you feel anxious or guilty, leading you to overexert yourself. 

Wellness as an ideology works against itself. When we try to follow its tenets and "feel better," in reality, our quest for wellness can make us feel worse.

We put intense pressure on ourselves to adhere to a wellness doctrine by eating correctly and exercising. All of this stress compounds, leaving us full of _guilt_ and _self-blame_ anytime we might slip off base and "cheat" with unhealthy foods or skipping a workout session.

When being "well" is synonymous with being "good," eating a cookie becomes a criminal act, inspiring frustration and self-hatred.

Worse, trying to hide your seeming moral weakness from others can be isolating. For instance, if you feel guilty for quitting a diet, you might avoid friends to hide the extra pounds you may have gained as a result.

Following such a judgmental doctrine also leads to overexertion and feelings of anxiety when we fail. We conflate our successes and general happiness with the ability to follow strict exercise protocols, even after a long, exhausting day at work.

Within such a system, failure is inevitable. Your body will eventually force you to rest! When this happens, wellness devotees can become overwhelmed with anxiety, as clearly this failure will lead to others, and a good life will remain out of reach.

This becomes a vicious cycle, sapping energy and leaving you less capable of meeting goals and, in turn, making you more anxious.

Some people even try to fight through exhaustion by doing even more — spending all their free time at the gym and spare cash on life coaches.

In this way, the wellness doctrine works contrary to our natural instinct to rest and indulge occasionally.

What sort of society does an obsession with wellness create? As you'll see in the next blink, we've built a community of highly functioning neurotic machines with a single life purpose — to work.

### 5. Companies use wellness to shift responsibility to employees and make everyone work harder. 

Most major companies today offer employees courses to learn how to relax or quit smoking. Many also have set up private gyms for employee use. Such caring gestures are appreciated; who doesn't want a boss who clearly cares about your well-being?

There is, however, a dark side to wellness at work.

Wellness programs reinforce the idea that only _you_ are responsible for your well-being, workplace satisfaction and success. It is certainly bad for your health to work long hours for poor wages, concerned about being fired at any moment. But the wellness doctrine maintains that employees can thrive under any conditions as long as they think positively, live a healthy lifestyle and know how to relax.

For example, Google offers its employees mindfulness courses; they are popular. Workers learn to relax by focusing on the present moment and breathing. This practice ideally helps employees deal with stressful situations that arise at work or in general.

While being mindful can help in life, this also carries the implication that each employee takes it upon himself to manage often overwhelming workloads; a busy schedule isn't to blame for employee stress.

Beyond that, a wellness culture can create competition among employees, ensuring that everyone work as much as possible. Wellness teaches us that we can do anything if we optimize our minds and bodies — clearly more beneficial to a company than its overworked employees.

These practices are further supported by health apps which encourage people to monitor performance and compete with others.

> _"Ever since the prestigious Cleveland Clinic stopped hiring smokers in 2007, other hospitals in the United States have followed suit."_

### 6. Politicians have used the tenets of wellness to justify cutbacks in welfare and other social benefits. 

A wellness ideology is also a powerful tool in politics, especially for politicians who necessarily want to distract or divert attention from one issue to another.

After all, wellness is a spiritual quest for health and happiness or personal well-being. Politics, on the other hand, requires collective thinking to improve the state of a nation.

So while the wellness movement might not seem overtly political on the surface, it can serve a political function: drawing the attention of the middle class away from the needs of society. Politicians can act without facing blowback from a public largely distracted by personal concerns.

A profound political effect of the wellness doctrine is that it helps justify cuts to the welfare state.

In the 1990s, both British Prime Minister Tony Blair and US President Bill Clinton pushed through massive welfare reforms, claiming that people who expected money from the government would just become lazy and stop looking for work.

Such arguments are intimately related to the wellness ideology. What politicians here were doing was holding people responsible for their success or failures — regardless of economic conditions.

So if a person lost his job and couldn't find employment, the wellness ideology says it's not the job market's fault but the fault of the individual for having the wrong mind-set. Maybe the person didn't want a job badly enough or didn't have the stamina to work harder.

People already employed and financially comfortable bought into this sort of thinking, and viewed the unemployed and poor as a lazier class. As a result, people became reluctant to help others in need or stand up to government policy on their behalf.

So, while the craze for wellness might seem to push people to become healthier, it's dividing society and increasing and legitimizing injustices.

### 7. Final summary 

The key message in this book:

**Wellness, self-improvement and healthy living have been used as a ploy to make people slaves to work, oblivious to society's needs and so focused on personal success that they can't see how others are suffering. This obsession has blinded us to the facts and the power we have to change them.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading: 30 Ways to Reboot Your Body** **by Ben Greenfield**

_30 Ways to Reboot Your Body_ (2015) gives you the keys to repairing and regenerating your body so you can take your health and fitness goals to the next level. These blinks explain the importance of digestive health, light exercise and solid routines. Get ready to have your ideas about diet and exercise challenged.
---

### Carl Cederström and André Spicer

Carl Cederström is an associate professor of organization theory at Stockholm University. His work has been published in the _Guardian_, the _New York Times_ and the _Harvard Business Review_.

André Spicer is a leading thinker on subjects such as organizational behavior, leadership and corporate responsibility. He is a professor of organizational behavior at Cass Business School at City, University of London and the founding director of ETHOS: The Center for Responsible Enterprise at the University.

