---
id: 58506baa3e967900044cb0cc
slug: the-whole-brain-child-en
published_date: 2016-12-28T00:00:00.000+00:00
author: Daniel J. Siegel & Tina Payne Bryson
title: The Whole-Brain Child
subtitle: 12 Revolutionary Strategies to Nurture Your Child's Developing Mind
main_color: 24A6B2
text_color: 24A6B2
---

# The Whole-Brain Child

_12 Revolutionary Strategies to Nurture Your Child's Developing Mind_

**Daniel J. Siegel & Tina Payne Bryson**

_The Whole-Brain Child_ (2011) is a parent's guide to understanding children's minds. These blinks explain how to help your child integrate various aspects of his or her brain and develop into a mentally well-rounded human.

---
### 1. What’s in it for me? Raise your child to face life’s challenges with his or her whole brain. 

Few of us would choose to stand on just one leg if we can use both.

But strangely, many of us — especially children — use only some of our brain's capacity to deal with life's challenges. Of course, children don't _choose_ to eschew important functions of their brain; the problem is that brain functions develop at different speeds, and, as a result, your child isn't always entirely familiar with each one.

So, it's your task as a parent to help your child explore the newer functions of her brain — such as its reasoning ability — and show her how to use these capabilities along with more familiar properties of the brain.

This very process is what these blinks are about. Learn about the different parts and functions of your child's brain and pick up lots of useful advice on how to make them work together.

You'll also find out

  * how to calm your child down when she suspects that there's a monster in the wardrobe;

  * why a temper tantrum in a fancy restaurant can be a good thing; and

  * what happens in your brain when you try to name a feeling.

### 2. Raising healthy kids means teaching them to deal with their experiences in a constructive way. 

Every new parent is showered with advice, from the best potty training tips to the safest cribs. But there's one area of knowledge essential to raising a happy kid that no one ever explains: how should you nurture your child's brain? Doing so requires you teach your kids how to interpret and deal with their experiences.

After all, our brains determine who we are and what we do, and they're molded by our experiences. Experiences change the brain. For instance, whenever an event occurs, such as a temper tantrum, certain neurons fire in our brains; when the same neurons fire over and over again, they connect to one another.

So, dealing with experiences is a central aspect of parenting, but that doesn't mean you should protect your child from difficult experiences. Rather, it's your job to make sure your child uses his entire brain when dealing with everything that happens, regardless of whether it's enjoyable or painful.

The key idea here is _integration_. The brain has lots of different parts — which you'll learn about later on — and for a child to thrive, these parts need to work in harmony to tackle whatever comes his way. This concept is at the root of what's called _whole-brain parenting_.

But how can you guide your child toward using his whole brain? Start by using all of yours. 

If you use your whole brain, your child will emulate you. For example, when your child throws a tantrum, instead of losing your temper or becoming cold and detached, use your empathy to connect with your child and learn what's bothering him while using the other parts of your brain to keep your anger under control. 

But to do that kind of whole-brain parenting, you'll first need to learn how your brain _works_, which is what we'll explore in the coming blinks.

### 3. Our brains comprise two different hemispheres that need to be balanced. 

Have you ever tried arguing with a two-year-old? If you have, you know it's almost always a lost cause, and here's why:

The human brain has two hemispheres, often referred to simply as two brains. Each has completely different functions from the other. The left hemisphere takes longer to develop and is devoted to order, specializing in language and logic, while the right side is geared toward the big picture, not details, with an expertise in nonverbal signals, images and feelings.

Since the right brain develops faster, it dominates the logical left side until a child is about three years old. This is precisely why it's impossible to reason with younger children — they're actually unable to see the rational side of things.

Young children are right brain-dominant, but once both hemispheres have developed, it is also problematic to rely too much on one of them. For instance, someone who is overly reliant on his left, logical hemisphere will be blind to feelings, while someone who uses his right brain more might act like a toddler, struggling to understand basic societal rules or logic.

As such, teaching your children to use both hemispheres is crucial and, when your child's third birthday rolls around, two strategies can help. The first is called _connect and redirect_, and it's designed to help your young one when he gets carried away by illogical concerns, like a monster in his closet.

Start by _connecting_ with his feelings. Soothe him and show your empathy to calm his right brain. Then, _redirect_ your child to his logical left brain by addressing his ability to reason. In this case, you might open up the closet and prove that there's no monster there.

The second strategy is called _name it to tame it_. Have your child retell his experiences, _naming_ the feelings that come along with them. This will connect left-brain functions like language to the emotional memories and thoughts of the right brain. Whenever we name emotions, our brain decreases activity in the areas responsible for emotion, thereby _taming_ our feelings.

But the brain has more specific parts than just these two hemispheres. Learn about these areas in greater detail in the next blinks.

> _"When we can give words to our frightening and painful experiences (...) they often become much less frightening and painful."_

### 4. The human brain has lower and higher functions, and it’s your job to help your child strengthen the latter. 

When your toddler throws a tantrum, are you in control or is she? Well, the answer has to do with balancing the _higher_ and _lower, or primitive,_ parts of the human brain.

The primitive part of your brain controls basic functions that keep you alive, like breathing, impulses and strong emotions such as anger. When this aspect controls your behavior, you're more likely to end up throwing a temper tantrum just like your toddler, or doing some other foolishly impulsive thing like telling a friend that they're ugly.

This is where the higher part of your brain comes in to maintain a counterbalance. Also known as the _cerebral cortex_, this part of your brain is responsible for impulse control, thinking, planning and self-understanding. You might have already guessed that, in children, the primitive part is dominant; the higher part of the brain takes much longer to mature, making it easier for lower parts to take control, especially the _amygdala_.

This almond-sized region processes emotions and can seize control of the upper part of a person's brain, especially a child's, flooding her with stress hormones and making her act before she thinks. This can obviously lead to some awful situations, but there are three strategies to help your child balance the different parts of her brain.

First, ask your misbehaving child what's happening and if a problem caused her anger. Then ask her to offer a solution. In this way, you _engage_ her higher brain, instead of _enraging_ her lower brain with, say, a punishment.

Second, encourage her to _use_ her higher brain whenever she can, then let her make decisions and ask her why she behaved in the way she did. This will strengthen the upper brain while connecting it to the feelings and impulses of its lower counterpart.

And finally, soothe your child's lower brain through exercise. For instance, if she feels overwhelmed by homework, have her run around the block to calm her stressed-out lower brain and improve her mood.

Now that you know how to balance the upper and lower brains, it's time to learn how you can help your child handle memories — especially difficult ones.

### 5. Memories can haunt children, but you can help them work through their past. 

Does your child ever freeze up in harmless situations? If so, a negative _implicit memory_ may be to blame. After all, our memories influence our actions — even those memories we aren't aware of.

For instance, when we talk about memories, it's usually about those we can consciously access. These are our _explicit memories_, like the time your friend found a dead mouse in her salad. But there's another type of memory called _implicit memory_. These are memories that we're not consciously aware of, but which guide our actions.

So, say your son had a painful medical treatment as a child, which he can't remember, but when he wants to go to the bathroom at his school he can't get past the door. His brain is associating the faint smell of disinfectant and the checkered tiles of the restroom with the hospital he was treated at as an infant, and he anticipates pain.

This fear can be paralyzing. But there are two strategies to help your child alter and control his memories. Memories aren't fixed and can be changed by focusing on a positive aspect, like a happy ending. Perhaps your daughter got lost in the supermarket one time, but a kind old lady helped her find you.

And if your child refuses to talk about a troubling memory?

It may help to suggest that she narrate it as if watching a movie with a remote control. In this way, she can pause or fast-forward whenever it gets too scary, or just skip to the happy ending.

But to alter and control memories, your child first needs to become aware of them and make them explicit. To help her do so, you should have your child talk about experiences in great detail; the _hippocampus_, or "search engine" of the brain, will fill in any gaps.

For instance, instead of asking "how was your day?" try, "what did you play today?" Subtle changes like this will help your child build a detailed picture of her actions and commit them to memory.

### 6. Our brains aren’t whole until we understand the multiple facets of our being. 

You may be just one person, but your individual self contains lots of different parts, such as your dreams, thoughts and sensations. These aspects are products of your upper brain and other cerebral regions that surround it like a _wheel of awareness_.

For your child to stay flexible and develop every facet of his personality, he needs to develop his own awareness of his mind. Many children can get stuck focusing on particular notions or goals, like the ambition to be the fastest runner in their grade, while forgetting about other aspects of their personal growth.

After all, when a person focuses on a distinct part of themselves, neurons fire in that direction, fostering new connections. So, if your child is always focused on the same part of their personality, it will develop this aspect at the expense of all others.

But the flexibility to shift his focus will only be available to your child if he develops _mindsight_, an awareness of every aspect of himself, and learns that he can choose where to place his focus. To help him get to this point, three strategies can help:

First, teach your child that emotions will come and go by themselves and that the average emotion only lasts about 90 seconds. This will prevent your child from confusing temporary states of mind like loneliness with permanent traits like being a loner.

Second, make your child aware of his _SIFT_, or the bodily _sensations_, _images_, _feelings_ and _thoughts_ that comprise his experience. To do so, keep asking your child about every one of them to show him that they all matter. In this way, he'll learn to focus on his inner landscape.

And finally, let your child exercise his mindsight by teaching him to calm himself down and guide his attention at will. A good way for him to practice this is by focusing on nothing but the sounds around him or by visualizing a place where he feels safe.

Pretty soon he'll be on his way to understanding his own mind. But that's just the first part of mindsight.

> _"The danger is that the temporary state of mind can be perceived as a permanent part of their self. The state comes to be seen as a trait that defines who they are."_

### 7. The brain is a social organ and you should nurture your child’s ability to connect with others. 

So, mindsight is essential for integrating the different aspects of oneself — but it's also a tool for understanding the minds of others. The brain is a social organ, designed to be shaped and reshaped through interaction with other people. In fact, we only thrive by learning to attune ourselves to other humans.

After all, our brains are equipped with a special type of neuron to help shape them through social interactions. These are called _mirror neurons_ and they come into play when we see people acting with intention. Pretty soon, our mirror neurons cause us to do or want the same thing as the people we're observing.

For instance, you might get thirsty when watching someone else drink water. In this way, you're not just understanding what others want, but actually feeling what they feel.

Naturally, such a socially inclined organ relies on interaction to remain healthy and it's no wonder that humans don't do well in isolation. But children don't yet have the skills to navigate social situations in appropriate ways, and if they don't learn them at an early stage, they might end up feeling alone or having few friends. 

That's why it's important to give your child sufficient opportunities to become socially adept, and a child's relationship to her caregivers is among the most determinative of how well she'll be able to empathize and communicate. Beyond that, such relationships will also decide whether she seeks contact and thinks of herself as part of a group.

So, to support your child's social brain, make family life fun. A great way to do so is through _playful parenting._ Act silly and play games! Prepare your kids for relationships and show them that being with others is fun.

As conflicts arise, seize the opportunity to teach your child empathy by asking her to consider the other person's perspective. But before doing that, be sure to acknowledge your child's own feelings so that she doesn't feel attacked, and draw her attention to body language to teach her about nonverbal cues.

### 8. Final summary 

The key message in this book:

**Most parents never learn how to nurture their children's brains — but this useful knowledge is an essential aspect of childrearing. Only through an understanding of the whole brain can you help your child integrate the different parts of their mind to become a self-aware and controlled person.**

Actionable advice:

**Play "what would you do" games to build the higher brain.**

Ask your preschooler to imagine a difficult situation like, "What would you do if your uncle gave you $10 and you really wanted to buy a new toy but your uncle said you had to share the money with your sister?" Posing such a quandary will help your child anticipate situations in which his lower brain urges him to do something he knows he shouldn't and push him to control these urges with his higher brain.

**Make sure that your child develops positive memories.**

Memories are associations between our current experiences and those of the past. If you give your child a candy after piano lessons, she'll connect sweets and the piano, thereby forming what we know as a "memory." So, do your best to make your child's experiences positive ones. After all, it won't just make for a pleasant present, but also for a fond memory down the road.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Happy Kid Handbook_** **by Katie Hurley, LCSW**

_The Happy Kid Handbook_ (2015) explains the key components of a happy childhood that allows children to thrive as unique individuals. Whether your child is introverted or extroverted, these blinks will guide you through helping them understand stress, negative emotions, social relationships and the importance of finding calm in their lives.
---

### Daniel J. Siegel & Tina Payne Bryson

Dr. Daniel J. Siegel teaches psychiatry at UCLA and leads the Mindsight Institute, an educational organization that works to make people aware of the processes within our minds. He is the author of several best-selling books about mindfulness and brain development. 

Dr. Tina Payne Bryson works as a clinical psychotherapist in Arcadia, California and is the Child Development Specialist at Saint Mark's School in Altadena. She also works for the Mindsight Institute and _The Whole-Brain Child_ is the second _New York Times_ best seller she has coauthored with Daniel J. Siegel.

