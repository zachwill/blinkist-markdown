---
id: 5a92795ab238e10006d0820f
slug: the-whole30-en
published_date: 2018-02-27T00:00:00.000+00:00
author: Melissa Hartwig & Dallas Hartwig
title: The Whole30
subtitle: The 30-Day Guide to Total Health and Food Freedom
main_color: AD3B26
text_color: AD3B26
---

# The Whole30

_The 30-Day Guide to Total Health and Food Freedom_

**Melissa Hartwig & Dallas Hartwig**

_The Whole30_ (2015) is a dietary program aimed at improving your health and general well-being. It's based on the idea that, by cutting out certain harmful food groups and reintroducing others, you can find a diet that works for you.

---
### 1. What’s in it for me? Find your personal diet for better health and well-being. 

Have you ever had a skin issue that never really cleared up? Do you go through your days feeling tired and without the energy you'd like? The culprit may be your diet. The fact is that many of us eat food daily that not only fails to deliver the nourishment we need, but is actually wreaking havoc on our health and overall well-being.

The Whole30 is a program designed to address this issue. Unlike many other dietary programs it takes individual differences into account. For 30 days, the aim is to eliminate potentially problematic ingredients like dairy and sugar-rich foods to monitor the effect they may be having on your health. When the program is complete, you'll be able to reintroduce the foods that you've found harmless, and your diet will be more nourishing and beneficial because of it.

In these blinks, you'll learn

  * the negative impact of sugar on your bloodstream;

  * how to cook simple, healthy meals; and

  * how to manage diet-related mood swings.

### 2. The Whole30 will reset your health and well-being. 

Do you find that you're already tired by eleven? Do you frequently experience odd aches or pains? Well, this could all be due to the detrimental effect of certain foods on your body — and quality of life. The _Whole30_ program may be exactly what you need.

The Whole30 will help you completely reset your health. It's an innovative way of helping you figure out what's good for you and what's not.

The aim is to rid your diet for 30 days of all products that have been scientifically shown to provoke either hormonal imbalances or inflammations in a large number of people.

After 30 days, your body should be set back to neutral. Then, you'll be able to start slowly reintroducing potentially harmful ingredients such as alcohol. In doing so, you'll be able to monitor the impact that each ingredient has on your body — whether it affects your moods, energy levels or your sleep patterns.

Eventually, you'll be able to categorize each ingredient into foods that have a positive impact on your health and foods that don't. In time, you'll be able to develop your nutrition plan as a step toward greater well-being.

The technique has been in use for many years, and it has received largely positive feedback from those who have tried it. In fact, 96 percent of people who have taken part in the program have experienced both weight loss and an improvement in their energy levels.

In addition to this, some people enjoy better sleep, feel that they have a greater ability to concentrate and experience better moods. For people who have lifestyle diseases, such as high blood pressure, or allergies, infections, arthritis and ADHD, the program has had a positive effect as well.

However, you don't need to have a chronic illness to reap the benefits of Whole30. If greater energy and a healthier body are what you seek, then the program is a great fit. So let's get started.

> _"If you feel helpless and out of control with your food and cravings, the Whole30 is for you."_

### 3. The Whole30 revolutionizes your relationship with food and reinstates your metabolism. 

When most people decide to start eating healthily, it's usually because they want to lose weight — but there are a multitude of good reasons to take a closer look at what you're putting in your body.

The Whole30 program was designed to remedy the complicated emotional and psychological issues we develop around food.

Take a bag of potato chips. They're high in caloric content but low in nutritional value. Consuming this type of food stimulates the reward center of your brain without really supplying any of the nutrients you need.

Eventually, your brain's reward center is rewired to constantly crave these foods. This is why cookies, chocolate bars and other comfort foods are so detrimental, leading to overeating.

As a result, you end up giving in to the craving and feel guilty for doing so. And as guilt is a negative emotion, this simply spurs you on to quash the feeling with yet more salty, sugary treats.

That's why it's so important to find alternative ways of comforting yourself, like spending time with a friend, hopping into the bath, or even going out for a run.

Aside from rectifying your emotional response to food, the Whole30 also sets your metabolism back on track.

Regularly consuming sugar and certain carbohydrates is damaging because these substances eventually slow your metabolism down. Over time, your body starts to rely on the steady stream of sugar and slowly forgets how to burn fat.

That's why, if you're eating too much sugar, you find that you need to eat every two hours or so — your body needs fresh fuel to power your metabolism. Otherwise, you end up irritable and struggle to concentrate.

Your blood sugar is another issue to monitor. The body's blood sugar levels are usually managed by the hormones _leptin_ and _insulin_, but they are rendered ineffective if your body is supplied with too much sugar.

In the next blink, you'll learn how to utilize the Whole30 program to work on specific aspects of your diet.

### 4. The Whole30 means eliminating foods from your diet – although you may be able to reintroduce them later. 

It's a difficult task, but to begin the Whole30 program, you first have to eliminate certain foods — these include sugars, sweeteners, alcohol, grains, legumes and dairy.

Generally speaking, the negative effects of sugars, sweeteners and alcohol are widely understood, but what about the other ones?

Grains can negatively impact the body because they tend to include inflammatory substances such as gluten. Less well known is the fact that the carbohydrates they contain ferment in your body, which can cause an imbalance in the bacteria present in the gut; this fermentation can also lead to inflammation throughout your body.

Another element to watch out for in grains is _phytic acid_. This acid makes it difficult for certain micronutrients to be absorbed by your body, meaning that grains provide you with very little nutrition.

Pulses, like chickpeas and lentils, pose pretty much the same issues. Another food to be wary of is peanuts, as they contain proteins that the body simply can't digest.

Dairy products are also highly problematic and have to be eliminated from your diet, too. The sugar, or _lactose_, in milk provides a surge of high energy, as it's intended to encourage the rapid growth of a small mammal. When humans consume milk, this high energy and certain proteins overstimulate the body's insulin response. This can lead to inflammation or other problems such as obesity or diabetes.

By undertaking the Whole30 program, you'll learn to what extent these foods are having a negative effect on your individual metabolism — it's not that these foods are fundamentally bad, but rather whether they disagree with your body.

Initially, it may feel like a drastic change, but after 30 days you may be able to start reintroducing some of the foods you've eliminated.

Shortly after you've removed these ingredients from your diet, whatever ailment you've been suffering from — whether that be low energy or bloating, for instance — will most likely start to improve. To identify which foods are causing you problems, sample each ingredient you removed and monitor how your body reacts. If you notice a certain food is not working well for you, remove it from your diet indefinitely. You can reintroduce the foods that cause no trouble.

### 5. The Whole30 can cause anger, fogginess and exhaustion. 

Forewarned is forearmed. Although you'll probably experience energy peaks and positive changes in your mood as you delve into the Whole30 program, there will also likely be a host of negative side-effects to contend with.

Fogginess, for instance, is a common side-effect. It can feel a bit like a hangover. The more unhealthy foods you gorged on before you started the program, the higher the likelihood that you're going to feel awful.

This is a normal reaction to your body detoxing. On certain days, you can end up feeling sore and ache in places. It could take several weeks for your body to be completely free from toxins and return to its regular functioning. As trying as it can feel at times, it's important to soldier on as the benefits of completing the program are well worth it.

Two other side-effects that you're likely to endure are anger and exhaustion. Your body is initially going to react negatively to the drop in sugar due to you cutting out lactose, carbs and other ingredients. Think about all the cookies and donuts you used to consume to assuage your feelings of anxiety. Now that you no longer eat them, you're bound to feel stressed and angry as there's nothing to pacify your brain.

A shortage of sugar can also cause exhaustion. This happens because the cells in your body which generate energy — known as _mitochondria_ — have to adjust and relearn how to transform the fat in your body and the food you eat into energy, rather than relying on the sugars that used to routinely course through your bloodstream.

To make it easier for your friends and loved ones as you're feeling the full force of these side-effects, you should tell them beforehand to anticipate mood swings — that way, they'll expect odd behavior from you.

Now that the adverse side of the Whole30 is out of the way, it's time to get familiar with the tasty recipes and cooking tips that are part and parcel of the program.

### 6. Certain kitchen utensils make it easier to prepare vegetables and meat while doing the Whole30. 

The Whole30 program has a range of easy, fun recipes to prepare. However, if you want to make your life easier, there are some kitchen tools you might want to get.

First up, there's a selection of utensils that are great for preparing delicious vegetable-based dishes.

Everyone knows what an arduous task it is to chop garlic into small pieces, so bypass the fiddly stuff and get yourself a garlic press. You simply hit the garlic clove with the flat of a knife, peel off the skin and slot the clove into the garlic press. Et voilà!

Another useful tool is a high-quality food processor. A food processor will allow you to transform raw ingredients, like chunks of vegetables, into sauces or mashes without requiring dilution with liquid, which is often the case when using a blender.

A julienne peeler will also make preparing vegetables a lot simpler. This peeler allows you to create long, thin vegetable strips — so making vegetable noodles is a cinch.

Now that you've got your vegetables sorted, consider the tools you'll need to prepare your meat. It's essential to cook meat to the correct temperature but it takes a lot of practice to know when it's cooked all the way through, so why not cheat and be on the safe side with a handy meat thermometer? To make your job even easier, the Whole30 recipes all include the right temperatures for cooking your meat.

Lastly, it's a smart idea to buy yourself a meat tenderizer. It looks like a hammer, and you use it to pummel a cut of meat that you're planning to either fry or grill.

This relaxes the muscle fibers in the meat, making it more tender and also flattening out the meat so that it'll cook evenly. You don't want a thin section that's overcooked and a thick portion that's undercooked!

> _"Don't stress about outfitting your kitchen all at once. You have your whole life to build it up."_

### 7. Meticulously prepare your Whole30 meals and make plentiful portions. 

In life, it's always a sound idea to have a plan in place — that way, you'll improve your chances of success. When it comes to cooking, this attitude can work wonders.

It's vital to take the time to plan out your recipes. Before you begin cooking, lay out on your kitchen counter all the ingredients, containers and utensils you'll need. Leave any meat in the fridge for the time being. And remember, it's also wise to arrange the ingredients in a logical order to make the process of cooking your meal seamless.

Say you want to make chicken soup. First, place the chopped onions, spices and garlic together in one bowl. Then, grab another bowl and sweep in the chopped peppers and tomatoes. Now take the chicken breast from the fridge, cook it and put it in another bowl that you've already set on the counter. Add the ingredients from your first bowl to a pan, then add the second bowl's content, and, finally, the meat. You'll find this is a simple way to cook that requires little washing up.

It's also important to make sizable portions of your Whole30 recipes.

The point of the program is not to deprive yourself of food, so feel free to be generous when it comes to quantity. For instance, a Whole30 recipe may suggest a 150-gram steak which may be too much for one person and not enough for another. Therefore, adapt the recipes according to your requirements.

It's often better to cook larger portions because even if you don't eat them all in one sitting, you'll have plentiful leftovers to eat the next day. The Whole30 program is centered around providing nourishment through protein, so it's essential to make sure that you're consuming enough of it.

You're now ready to start cooking, so in the following blinks, you'll find two basic recipes.

### 8. The protein salad is a cornerstone Whole30 recipe. 

Having a quick protein snack in your bag is great for when you need something on the go. Many people are tempted to just grab a power bar, but it's important to have something more nourishing for meals.

This is where the Whole30 protein salad comes in. Generally, a salad is considered to be healthy by most people — but this isn't true if you're just going to eat a bowl of lettuce. Again, the goal is to nourish, not deprive yourself. Make sure to add plenty of protein to that bowl!

Eating a protein salad for lunch should keep you full throughout the afternoon, so you don't have to reach for any treats to satisfy your hunger.

The trick is to make a large portion that will last you for several days. Be wary, though — protein salads are delicious, so you'll probably be eating a lot more than you planned!

To make a big portion, select 450 grams of a protein of your choice and slice it into chunks. Eggs and smoked or canned fish, for example, are good sources of protein.

Get yourself a big bowl and mix four tablespoons of mayonnaise or guacamole with two tablespoons of vinegar and a pinch of salt and pepper. Make sure you mix it all well before adding in salad leaves and the chopped protein. This is the base of your salad.

Now you can create many variations — maybe go Mediterranean with some olives, dried tomatoes, figs, celery, pine nuts or parsley. Or, why not take it a step further and add some more unusual ingredients? Think raspberries, blueberries or cashew nuts. For an autumnal spin, try including some apple slices, roasted pumpkin, raisins or walnuts.

Stock up on these ingredients and select a favorite dressing so you can throw this meal together at short notice. There are plenty of variations to try, and you can add as much or as little of your chosen ingredients as you like. There are absolutely no restrictions when it comes to compiling this meal — just make sure to include 450 grams of protein.

### 9. The Whole30 braised beef brisket includes a slow cooking option. 

If you're missing devouring a big bowl of pasta, this braised beef brisket may be just the substitute.

This recipe is for a brisket that's around the 680-gram mark. You'll need salt, pepper, a cooking fat that's not butter, half a chopped onion, four garlic cloves, fresh thyme and 1.25 liters of beef broth.

To begin, preheat your oven to 180°C. Place three tablespoons of your chosen cooking fat in a thick, ovenproof frying pan, and once the fat is hot, add the beef. Sear the meat on both sides for a couple of minutes before removing it from the pan and setting it aside.

Reduce the pan heat to medium and cook the chopped onion for two to three minutes. After one minute, add the garlic; wait a further minute before putting in the thyme and the broth. Turn the heat up until the liquid starts to boil.

Place the brisket back in the pan, but this time put the lid on and place it in the oven. Leave your meat to cook for four hours — don't forget to turn it over every hour.

Once it's cooked through, place the meat in a bowl. Pour all the liquid from the pan into a mixer and blend it into a thick sauce. Add this to the pan, and cook for a further five minutes. Combine it with your brisket, and your beef is ready to be served.

The braised beef brisket can also be made using a slow-cooking process, which is ideal if you want to come home to a delicious warm meal after work.

To prepare it, take all the ingredients and put them in a slow cooker in the morning. You'll need less liquid using this method — use just enough to cover the meat. Allow the brisket to cook for eight or nine hours while you're away at work — just quickly make the sauce with a mixer and a pan when you get home in the evening.

As these two recipes prove, not only will the Whole30 do wonders for your health, it'll also introduce you to a variety of wholesome, delicious meals.

### 10. Final summary 

The key message in this book:

**As the famous adage goes, you are what you eat. So, if you've ever suffered from an unexplained ailment or just felt less energized than usual, the Whole30 program may be just what you need. For 30 days, you purge your diet of any food groups that may be causing health issues. Through conscious eating and slowly reintroducing non-problematic ingredients, you could be on the road to better general health and well-being.**

Actionable advice:

**Abide by the Whole30 when eating out.**

When you're at home, it's fairly easy to stick to the program, but what about dining out? To help you stick to the diet plan, why not do some research on the restaurant beforehand? When it's time to order, feel free to request what you need. Be confident with your approach but remain respectful and polite toward the staff for going that extra mile for you.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Nutrition and Physical Degeneration_** **by Weston A. Price**

Based on the author's meetings with many of the world's indigenous people, _Nutrition and Physical Degradation_ presents a comparison of the health of those who consumed only local whole foods and those who had begun to include processed foods in their diet. The author found that the latter suffered from problems with their teeth, bodies and brains, while the former remained strong and vigorous. Having investigated the differences between processed and local whole foods, the book argues that diets made up of processed foods lack the requisite vitamins and minerals for maintaining a healthy body.
---

### Melissa Hartwig & Dallas Hartwig

Melissa Hartwig is a nutritional therapist who specializes in helping people adjust their dietary habits. She is a co-creator of the Whole30 program, as well as a successful author and speaker.

Dallas Hartwig is a physical and nutritional therapist. His area of focus is the treatment of hormonal and digestive imbalances. He is a co-creator of the Whole30 program, a successful author and a practiced seminar leader.

