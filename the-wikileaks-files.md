---
id: 593e5c4ab238e100051fddc7
slug: the-wikileaks-files-en
published_date: 2017-06-14T00:00:00.000+00:00
author: Julian Assange (introduction)
title: The WikiLeaks Files
subtitle: The World According to US Empire
main_color: 81B0C4
text_color: 81B0C4
---

# The WikiLeaks Files

_The World According to US Empire_

**Julian Assange (introduction)**

_The WikiLeaks Files_ (2015) provides fascinating and digestible insights from WikiLeaks, the organization that came to worldwide prominence with the release of 251,287 US State Department cables in 2010. These blinks paint a bleak picture of an American empire and its machinations.

---
### 1. What’s in it for me? Find out what the Wikileaks files reveal about the United States and its foreign policy. 

Since WikiLeaks went online in 2006, it has published hundreds of thousands of pages of classified material about international warfare, diplomacy and policy — especially related to the United States.

This, of course, has come to the great displeasure of the US government, which denounces WikiLeaks as a subversive organization that spreads lies and has taken the leaked files out of context. So what exactly do the WikiLeaks files reveal, and what do they tell us about the United States?

As it turns out, the WikiLeaks files have nothing positive to say, and these blinks will cover key findings like the United States' war crimes and torture of prisoners.

In these blinks, you'll learn

  * which magic word gets e-mails blocked by the Pentagon server;

  * how the CIA tried to make its practice of torture seem legitimate; and

  * how the United States avoids juridical investigations into its crimes.

### 2. The US government has attempted to contain and vilify WikiLeaks. 

If you've heard of WikiLeaks and its founder Julian Assange, you likely also know that it's a very controversial organization. But what exactly is WikiLeaks and why is it so divisive? Let's delve in.

The story begins in 2006, when a network of hackers, programmers, activists and journalists, led by Australian hacker Julian Assange, established the WikiLeaks website. It was designed to be a platform for whistleblowers, which would allow them to upload documents anonymously. Squarely in WikiLeaks' sights were shady, corrupt or illegal practices by governments, institutions and corporations.

Since then, its global profile has grown immensely. The website has disclosed some momentous revelations, with some of the most famous including information about mass electronic snooping carried out by the US National Security Agency (NSA).

WikiLeaks has, as of 2016, published 2,325,961 US State Department records — that's about 2 billion words in total, which, if printed, would fill around 30,000 volumes. The records also showed that the State Department, the heart of American diplomacy, was placing a positive and optimistic sheen on policies that were causing devastation in other parts of the world.

What's more, they had also budgeted more than $1 billion each year for "public diplomacy;" in other words, they were investing in propaganda.

For its part, the US government has actively worked against WikiLeaks. Both the George W. Bush and Barack Obama administrations have condemned WikiLeaks, with former Vice President Joe Biden going as far as proclaiming Assange to be a "cyber-terrorist." Since it first came to prominence, US governments have sought to suppress WikiLeaks and stop the public and researchers from using it.

For instance, the Library of Congress blocks access to the website, while the National Archives blocks searches for the term "Wikileaks" in its databases. In 2012, the Pentagon blocked e-mails containing the word "WikiLeaks" on its servers through an automated filter.

The International Studies Association (ISA) even forbade its members from using WikiLeaks material. With 6,500 members worldwide, including many professors in political science departments at major universities in the United States and abroad, that's a pretty serious clampdown — and it's all because WikiLeaks' revelations about US policies are so damaging and revealing.

### 3. The United States has no respect for the rule of law and its conduct in war is dubious at best. 

As renowned anthropologist Talal Asad says, war is a conflict waged by people of good character, for benign motives, when all other possible alternatives have been exhausted. In other words, it's a last resort.

Successive US governments have always claimed their wars are just. And yet, as WikiLeaks has proved, the United States has time and again shown little respect for international law and has even committed war crimes to advance its agenda.

And WikiLeaks has proven this.

For example, the United States has bombarded and executed civilians on numerous occasions. In 2006, in the Iraqi town of Ishaqi, at least ten Iraqi civilians were executed, including a woman in her 70s and a five-month-old infant. The civilians were handcuffed and shot in the head, and the United States employed airstrikes to destroy the evidence thereafter.

The United States has also assassinated civilians and journalists. A classified US military video from 2007 shows US helicopters killing a dozen people in Baghdad, including two Iraqi Reuters news staff.

And, of course, the United States has tortured detainees at its military prison in Guantanamo Bay, Cuba, even though very few of the detainees there have ever been charged with any offense.

The evidence is clear; the United States has shown no restraint, respect for the rule of law or morality in military matters.

For example, although the US claimed that its actions in Guantanamo were righteous, the WikiLeaks files proved this to be completely false.

Government officials told the public that the Guantanamo detainees were basically the scum of the earth, terrorists who posed a clear and present danger to Americans. In reality, however, the WikiLeaks files showed that many prisoners were still held even though they posed no risk whatsoever!

Ultimately, this all boils down to the discourse favored by the George W. Bush administration, which sought to readjust the moral compass of the United States according to an "us versus them" mentality.

Whatever "we" Americans would do was seen as "good," while whatever "they" did was considered bad. Dick Cheney encapsulated this mentality when he called the possibly innocent Guantanamo detainees "bad guys."

### 4. The United States has used torture but has consistently denied doing so. 

You might think that the definition of what constitutes torture is pretty straightforward. But the US Central Intelligence Agency (CIA) has found ways of getting around such definitions.

Let's be clear: under George W. Bush, the United States tortured people.

And the evidence is there to prove it. In 2002, the CIA captured Abu Zubaydah, a Saudi national who was allegedly associated with al-Qaeda, in Pakistan. The CIA publicly celebrated his capture, claiming that he was one of their highest-profile arrests; Zubaydah was then promptly sent to Guantanamo.

WikiLeaks files show that he was forced to spend two weeks inside a coffin-shaped box. He was subjected to sleep deprivation, stress positions and slappings, and insects were even scattered inside the box.

During this interrogation period, the bullet wound he'd received during his capture was allowed to fester and rot. Officers and medical staff at the prison complex didn't treat the wound.

He eventually admitted he'd associated with armed jihadi groups, but denied connections to al-Qaeda — and appeared to be telling the truth.

But then, the United States decided to make Zubaydah "disappear," which meant that he was unable to get access to the International Red Cross. Zubaydah was never charged with a crime — and yet, unbelievably, he continues to be held in Guantanamo.

Looking at the facts of the case, you might think the CIA was torturing Zubaydah. But the agency has scoffed at any such suggestion.

In fact, when the torture program was established in 2002, then-CIA director George Tenet even claimed it "saves lives" by helping procure information that prevents terrorist attacks.

The CIA has since set about trying to prove the legitimacy of its torture practices. At one point, CIA officials produced a document listing every occasion they had briefed members of Congress about the organization's interrogation methods.

This is just a way of burying accountability for the CIA's — and, as a consequence, the United States' — actions, so that the country could continue to present itself as a sentinel of the liberal world order rather than admitting the dark truth of its actions.

### 5. By weakening the International Criminal Court, the United States can avoid accountability. 

In 2002, the Rome Statute established the International Criminal Court (ICC), which was ratified by 122 signatory countries.

The ICC has the authority to investigate individuals for war crimes, crimes against humanity and genocide. The idea was originally developed during a diplomatic conference in Rome in 1998.

From the start, American negotiators sought to limit the ICC and its independence. Most countries were in favor of "universal jurisdiction," which would imply that the ICC could prosecute war crimes committed anywhere.

However, the United States didn't like the idea and forced a compromise: the ICC could only prosecute crimes if an individual of a ratifying state committed them, and only then if committed on the territory of a state party to the Rome Statute.

Although displeased even with this compromise, President Bill Clinton nonetheless signed the Rome Statute, which served as a symbolic, but not legally binding, endorsement of the treaty. Clinton had wanted the United States to be able to veto prosecutions against its citizens; instead, the ICC prosecutor (elected by the member states) would have the final word on which cases would be taken forward.

In 2002, George W. Bush withdrew the United States' signature from the already weak treaty, which meant that American citizens were free from the threat of prosecution for war crimes.

John Bolton, Bush's Undersecretary of State for Arms Control and International Security, claimed that the United States would instead "follow its values when measuring the legitimacy of its actions."

It also means that US citizens can only be indicted if they commit a crime in a state that had ratified the ICC. But there's little chance of that happening either since the court depends heavily on Western financial and political support. Consequently, in its 13-year history, the court has only indicted citizens of African countries.

On top of this, the ICC has no enforcement officers of its own and depends on governments' cooperation to detain and extradite indicted individuals.

### 6. The United States has made its economic interests those of the entire world. 

Transnational forces govern the global economy. Big banks, the finance sector and the market all play their parts, but what determines _their_ behavior? The simple answer is American imperialism.

The United States has long defined its imperial mission. First expressed in the country's nineteenth-century doctrine of "manifest destiny," the United States has since its earliest days aimed to spread and institutionalize capitalism throughout the globe.

This behavior is typified through the establishment of international trade agreements, which seek to ensure American capitalism becomes the world's prevalent economic model.

In 2014, WikiLeaks released drafts of two obscure free trade treaties: the Trade in Services Agreement (TISA) and the Trans-Pacific Partnership (TPP).

These drafts revealed the nuances of the US model. They promised huge benefits for American service firms, for instance by eliminating trade barriers in the hopes of increasing US services exports by up to $860 billion, to a whopping $1.4 trillion.

The drafts also bypassed environmental and labor protection standards set by the World Trade Organization.

Meanwhile, the finance sector wields a powerful influence over US policy.

Between 1973 and 2007, profits from the financial sector grew from 16 to 41 percent of total profits in the US economy. And today, Wall Street accounts for just over a third of financial transactions worldwide.

This scale gives financial communities power, and it's from them that the US government collects technical expertise, training, legal knowledge and attitudes. The government then adheres to this advice blindly, with other nations following its lead.

Through this relationship, American financial institutions are basically free from democratic oversight and can maintain an iron grip on politics and decision making. Thus the financial sector ensures its centrality in the world, with US administrations championing and protecting these financial interests on a global scale.

This is how the financial interests of the US have merged with global economic interests to virtually become one and the same.

### 7. Final summary 

The key message in this book:

**The extent of United States' hegemonic power and its machinations are no secret. However, the WikiLeaks files offer a more precise picture and help flesh out the true nature of the country's political dealings. Upon examining leaked top-secret State Department cables, it becomes clear that by manipulating language and regulations to pursue its economic goals, the United States has expanded its global hegemony — and continues to do so.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _No Place to Hide_** **by Glenn Greenwald**

In _No_ _Place_ _to_ _Hide,_ author Glenn Greenwald details the surveillance activities of secret agencies as according to information leaked by American whistleblower Edward Snowden. Rather than serving as a means to avoid terrorist attacks, as the US National Security Agency (NSA) claims, Greenwald explains that these dubious activities instead seem to be a guise for both economic espionage and spying on the general public. _No_ _Place_ _to_ _Hide_ also brings to light the media's lack of freedom in detailing certain government and intelligence agency activities, and addresses the consequences whistleblowers face for revealing secret information.
---

### Julian Assange (introduction)

The contributors to this collection of essays include scholars, journalists and activists. The introduction was written by Julian Assange, the editor-in-chief and founder of the whistle-blowing website WikiLeaks.

