---
id: 590e586cb238e1000792caff
slug: the-wild-diet-en
published_date: 2017-05-08T00:00:00.000+00:00
author: Abel James
title: The Wild Diet
subtitle: Go Beyond Paleo to Burn Fat, Beat Cravings, and Drop 20 Pounds in 40 Days
main_color: D87839
text_color: A65C2C
---

# The Wild Diet

_Go Beyond Paleo to Burn Fat, Beat Cravings, and Drop 20 Pounds in 40 Days_

**Abel James**

_The Wild Diet_ (2015) is your guide to using the biology of fat-burning to lose weight. These blinks explain what makes high-intensity exercise, plant-based, protein-rich diets and hydration so effective, and provide you with health hacks that you can start applying right away.

---
### 1. What’s in it for me? Lose weight fast without spending all your spare time exercising. 

If you want to lose weight, you'll have to make sacrifices. You'll have to stop eating your favorite foods and, what's more, you'll need to go jogging three times a week for at least an hour. Feel the burn!

Sound like a nightmare? Well, that's because it is one.

Welcome back to reality, where despite what you've heard, you can do away with the myth that only time-consuming exercise regimens and self-sacrificing diets lead to weight loss.

These blinks will show you how you can continue eating what you like, limit exercise to conveniently short periods and still lose weight.

In these blinks, you'll learn

  * how to limit your daily exercise to seven minute periods;

  * why eating lots of protein is good for losing weight; and

  * why you'll drink water at your next party.

### 2. Short and intense workouts are the key to burning fat and building muscle. 

There's one key maxim when it comes to taking on new challenges: quality over quantity. And this is definitely the case when it comes to working out. Your success depends not on how long you exercise, but on how hard you push yourself. In other words, intensity trumps endurance every time.

Compare the physique of a marathon runner to that of a sprinter and ask yourself which athlete has the type of body you want. Chances are, it's the latter. While endurance athletes tend to be thin and wiry, sprinters are tough, toned and powerful.

There's a biological explanation for this. Research shows that exercising in rapid, explosive bursts triggers the release of growth hormones and testosterone that burn fat and build muscle, while simultaneously inhibiting the release of stress hormones.

Along with sprinting, high-intensity exercises include pull-ups and squats. Squats are a great example of what's called a _compound full-body exercise_. This means that they make muscles in several parts of your body work hard, from your legs and buttocks to your arms, shoulders and core. Sit-ups and bicep curls are examples of _isolated exercises_ which, by contrast, only train muscles in a certain area, burning fewer calories as a result.

Structuring workout sessions around high-intensity, full-body exercises allows you to work out for a short time but still burn an impressive amount of calories. And when we say short, we mean it. The author's "seven-minute wild workout" barely takes any time out of your day but is a real challenge for your body.

The workout begins with a one-minute warm-up — jumping rope or a boxer's shuffle are good options here. This is followed by a set of 20-second sprints, with a ten second pause between each interval, for five minutes — that's ten sets altogether. The workout concludes with a one-minute cooldown, which can be something like jumping rope or jogging at a more relaxed pace.

Once you've got the hang of the seven-minute wild workout, you can customize it to your heart's content. Try doing your sprints up a hill to raise the intensity, or push your body in a different way by applying it to swimming. Whatever you do, be sure to exercise to the max during your 20-second sets. This is what will trigger the release of the hormones needed to burn fat.

### 3. Swap sugary processed foods and starchy staples for nuts, legumes and greens. 

We've been on this planet for around 200,000 years. In this time, the world has changed a lot. You might think that our bodies would have evolved and adapted to this change, but the impact of diet on our physique says otherwise.

Humans still thrive on the plant-based diets that sustained our ancestors. Greens, raw nuts and fiber-dense fruits and beans provide our bodies with the right balance of nutrients to keep us healthy and lean. Why? Because the foods themselves trigger the release of fat-burning hormones and keep us full for longer.

However, modern diets of sugary, starchy foods have the opposite effect. Excessive sugar is found in all sorts of processed foods, even seemingly healthy items such as granola or fruit juice. Starch dominates our diets too — just think of how much pasta, cereal, bread and potato you eat each month.

Sugar and starch fill our bodies with more energy than we need. They also trigger the release of huge amounts of insulin into our bloodstream. This, in turn, prevents our bodies from burning off body fat, and even converts blood sugar into more of it.

By reducing the amount of sugary and starchy foods in your diet, you can turn your body back into a fat-burning machine. Creating an energy deficit — that is, expending more energy than you eat — triggers a cascade of enzymes and hormones that encourage your body to convert stored fat into energy.

Fat reserves are transported throughout our body in the form of fatty acids to the places where they're needed most, such as our muscles, our vital organs and other tissue. Once these fatty acids have reached their destination, they're broken down and converted into energy. All that's left is carbon dioxide and water, which your body gets rid of in exhalations and in the form of urine and sweat.

### 4. Protein is a must, but make sure you pay attention to its source. 

Our bodies need protein like cars need fuel. In fact, eating lots of protein is one of the best things you can do for your body. Why? Because protein requires so much energy to digest that you actually burn fat while your body processes it.

During digestion, your body breaks down food to convert it into energy. This process itself requires energy, but the amount varies depending on the food you eat. Proteins found in meat and eggs require three times as much energy to digest as rice or pasta, causing the body to burn off fat stores to meet this energy demand.

In this way, consuming more protein helps you burn fat and build muscle. So eat as much as you like! And be sure to get a minimum of 50 to 100 grams of protein into your meals daily.

Just make sure you choose the meat you eat carefully. When you eat a piece of steak, you're not just eating the meat of a single steer, but meat that's shaped by the entire food chain on which the animal relied. Unfortunately, cattle are often fed antibiotics and corn to fatten them up faster. Because of this, meat from commercially reared beef cattle can be far from healthy.

It's best to eat organic meat from naturally raised, grass-fed animals. If your budget doesn't allow for premium organic cuts, you can save money by purchasing offcuts instead, such as liver, heart or stewing meat.

No, this doesn't sound particularly appetizing, but these cuts actually have the highest concentration of nutrients. In fact, ancient hunters used to eat the raw liver of their killed game right away to replenish their energy. Thankfully, there are plenty of delicious recipes for organic offcuts if you're looking for a less extreme meal.

### 5. Staying hydrated will help you burn fat even faster. 

The earth is often referred to as "the blue planet" because of the sheer amount of water covering its surface. In this sense, humans are "blue" too — 90 percent of our blood is water, 78 percent of our brain is water and 70 percent of our muscles are comprised of water too.

Water is essential for our health. Failing to drink water for a couple of days is enough to kill you since our organs require a certain amount of water to function. Even moderate dehydration has a profound impact on your body. Losing 5 percent of your body weight in water reduces oxygen transportation and muscular endurance by as much as 30 percent.

Drinking lots of water allows our bodies to function correctly. It helps one organ in particular: the liver. The liver is our main fat-burning organ and is responsible for breaking down toxic substances like smoke, alcohol, food additives and even air pollution into smaller molecules that can be dissolved in water for disposal.

The liver functions best when we're well hydrated. In fact, the more water you drink, the more efficient this detoxing process is. This boost in efficiency frees up your liver to burn more fat.

So how can you tell when you need to hydrate? Surprisingly, feeling thirsty isn't a very helpful indicator. By the time you start feeling thirsty, you're already dehydrated. To prevent dehydration, you need to drink regularly over the course of the entire day.

The exception is at mealtimes. Drinking too much water while eating a meal can dilute the digestive juices in your stomach. Try not to drink more than one glass of water while eating. Otherwise, carry a small bottle of water with you at all times and take sips frequently. Finally, remember that fruit juice and soft drinks won't cut it. Water is the only source you should rely on for hydration.

### 6. Healthy, organic substitutes for meat and dairy will make a vegan diet healthy. 

These days, veganism is all the rage. Plant-based, raw, superfoods — all sorts of labels are used to win over the health-conscious customer.

But meat-free diets aren't always as healthy as they seem.

Animal foods provide us with crucial nutrients. These include collagen for healthy skin and hair; iron for transporting oxygen in our blood; and choline for a sustainable metabolism. A lot of vegan meat substitutes simply can't provide these nutrients. On the contrary, many products even contain GMOs, artificial flavors and gluten, which are as harmful to your health as a McDonald's Big Mac.

If you abstain from eating animal products for ethical reasons, be sure to eat healthy and nutritious substitutes. If you're comfortable getting your protein fix from eggs and seafood, go for the least processed produce you can find. Mussels and oysters are a great option; they contain comparable amounts of copper and iron to meat. Also, don't forget that purchasing organic and free range eggs also helps support farmers who treat their animals humanely.

If you eat an entirely plant-based diet, you'll need to stock up on substitutes. Unrefined coconut oil is a good replacement for grass-fed butter. Coconut cream, soy milk and unsweetened almond milk are sound alternatives for milk and cream. Instead of chicken, you can eat tempeh, an Indonesian soy-based product similar to tofu. Instead of ground beef, you can eat whole or mashed black beans. The flax plant even provides a substitute for eggs in baking — one tablespoon of flax seeds mixed with three tablespoons of water replaces one egg.

### 7. Final summary 

The key message in this book:

**The biology of fat-burning helps us find new approaches to exercise and diet that work with our bodies, not against them. Through high-intensity exercise and a diet of organic protein, plant-based foods and constant hydration, we can shed fat and return a healthy balance to our bodies.**

Actionable advice:

**Drink only water and lots of it!**

Sugary drinks like sodas, juices and hazelnut lattes contribute an insane amount of calories without filling you up at all. Because you can drink them and still be hungry, you might not realize how much they contribute to your weight. So try drinking nothing but water for a month. You might lose up to 20 pounds without changing anything except your drinking habits!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Paleo Manifesto_** **by John Durant**

_The Paleo Manifesto_ (2013) is the go-to guide to going paleo. It explains why the Paleo diet is in sync with our ancestors' diet, and how you can use humanity's anthropological and evolutionary history to get fit, feel great and lead the healthy lifestyle you've always wanted to.
---

### Abel James

Abel James is the creator of _The Fat-Burning Man_, an internationally successful podcast on healthy eating and lifestyles. The podcast has been rated as the number one food podcast in eight countries.

