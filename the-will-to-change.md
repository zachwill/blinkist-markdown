---
id: 599c3c1eb238e10005eeb5fb
slug: the-will-to-change-en
published_date: 2017-08-25T00:00:00.000+00:00
author: bell hooks
title: The Will to Change
subtitle: Men, Masculinity, and Love
main_color: B3C299
text_color: 6C755D
---

# The Will to Change

_Men, Masculinity, and Love_

**bell hooks**

_The Will to Change_ (2004) is about the social norms that cause men to dominate society. These blinks explain the social basis for masculinity, offer insight into why men can't get in touch with their feelings and explain why it's important for them to do exactly that in order to heal.

---
### 1. What’s in it for me? Understand how patriarchy limits men. 

Over the last century, feminism has become a hot topic in political debates. From an early focus on equal rights and pay, feminism has grown to take a big picture perspective on the patriarchy: a social system where men have dominant roles in politics and the economy as well as moral authority and social privilege.

But the patriarchy is not only bad for women, it's also bad for men.

Just as patriarchy subjugates women, it represses men. Rather than losing political and economic equality, men in the patriarchy are deprived of love and other emotions. Here we look into the patriarchy's devastating effect on men and society as a whole, and how the time is ripe for change.

In these blinks, you'll find out

  * why the saying "boys don't cry" is emblematic of the problems of patriarchy;

  * why men are taught that sex is a natural desire for men, but love isn't; and

  * how the family is the place where a real change in men's behavior will begin.

### 2. Patriarchy prevents men from loving and getting in touch with their feelings. 

Most people know that boys just don't play with dolls, but do you ever wonder why this is such a deeply ingrained social norm? It's an assumption that comes from how the _patriarchy_, a system that instructs the behavior of men, affects our thinking.

The patriarchy is everywhere you look. It's a power structure that tells men they should be in charge, dominate those around them and feel superior, especially toward women.

Indoctrination into this system begins in early childhood when parents teach their kids behavioral rules. During this time, gender determines the norms that kids are taught. Girls are taught to serve and nurture while boys are taught to suppress their feelings and provide financially for their families, but not to care for others in different ways.

Because of this early teaching, every social setting is afflicted by patriarchal logic. No matter if it's religious, educational and so on. In every instance, both kids and adults are reminded that breaking the rules of patriarchy will result in punishment.

Just consider the author, who would always win when she played marbles with her brother. Her father considered it inappropriate for her to be better and more aggressive than her brother, so he forbid her from playing. When she rebelled, he responded by beating her and sending her to her room.

So, the patriarchy affects everything, and the situation is dire. After all, as long as men are trapped in this system, they won't truly be able to love or be in touch with their emotions.

That's because the patriarchy denies men access to their feelings and therefore to emotional well-being. Just consider the common saying, "boys don't cry." Many people haven't seen a man cry, and that's a result of the patriarchy telling men it wouldn't be manly to feel emotions.

As long as men are disconnected from their feelings in this way, they'll be deprived of love, both for themselves and for others.

Next up, you'll learn more about the consequences of this emotional denial.

### 3. Boys are taught to suppress their feelings, and this leads to anger and violence. 

Can you recall the last time a male colleague talked to you about his innermost emotions? Men are taught from the youngest age that feelings are to be avoided at all costs.

As a result, patriarchy doesn't just affect adult men, but boys as well. Boys are taught that, to become a man, they need to be strong and never weak. Feelings are associated with weakness and are therefore off limits.

For generations, parents were even scared to comfort male babies in case such attention made them "wimpy." It's only recently that this ludicrous logic has been challenged. And that's not even to mention the mass media, which inundates boys with images of what it means to be male.

So, boys are told not to express their feelings, and, when they're denied this option, they act out instead.

This is pretty simple really: boys are taught that the only acceptable emotion for them to express is anger. That means when they don't get something, they become furious.

The mass media also plays a role in teaching boys to act out, whether in the form of kicking, hitting or screaming to garner attention. This, of course, has major social implications.

While not all men will exercise violence during their lifetimes, men are certainly raised to be violent. Patriarchal society, after all, teaches that rage and violence are manly. We tell boys to suppress their feelings, and that isolates them. Once they're isolated, they forget their worth and develop aggressive inclinations.

And finally, boys are angry because they lack the necessary connections to their fathers. Fathers are often extremely strict, using shame to assert their dominance. Since boys are told not to feel, they have no tools to grieve this lack of emotional connection in a healthy way and instead their anger leads to bad behavior.

> _"Since patriarchal parenting does not teach boys to express their feelings in words, either boys act out or they implode."_

### 4. Patriarchy teaches men to crave violence and sex, but not love. 

The majority of people weren't beaten as children, but many were shamed by their fathers — a form of emotional violence.

This is a good example of how patriarchy teaches men violence, even if it's not necessarily physical. From childhood, boys are told that pain will make them into men, whether it's the result of emotional abuse by their fathers, a lack of love or the consequence of using violence themselves to achieve their material desires.

The images boys see in the media play a key role in this process. Just consider interviews carried out by a sociologist in which young boys were asked what they would do if they had the powers of comic book character The Hulk. Many of them answered that they would "smash their mommies."

So, boys are told that violence will make them men, and, as adults, men use violence to prove their manliness if they can't assert it through wealth and success.

This drive to assert their value is a result of men never learning that they have innate value in the first place. Because of this, most of them attempt to define their worth by becoming rich or successful and, if that doesn't work, they simply lash out violently to prove their status.

In a similar sense, many men seek out sex as they believe it will fulfill their need for love. Men are taught to think of sex as a natural desire determined by their biology, but not to consider love the same way. They believe they can have sex without love, but not the other way around.

The problem here is that men expect to feel emotionally satisfied from sex, a feeling that would normally come from love and simple intimacy. Since sex only brings temporary relief to these yearnings, no amount of sex can actually fill the void.

And what's worse, the more intense a man's emotional pain becomes, the more he craves sex or pornography.

### 5. Working women, dull jobs and unemployment threaten the patriarchy, but work culture represses men. 

During our grandparents' generation, men were the main providers for their families and rarely faced unemployment. But that's all changed today.

Women entered the workforce in increasing numbers, and changes in the economy meant that men had to work mindless jobs and experience unemployment. As a result, men lost their sense of importance and any sense of personal fulfillment in the workplace.

Losing their patriarchal role left men with little to hold onto, and many of them turned to alcohol, pornography or gambling to deal with their emotions.

Unemployment challenges the patriarchy because it gives men more time to think about emotional issues, which is precisely what patriarchy evolved to prevent. After all, in patriarchal society, a man's value lies purely in his work, which leaves him no time to understand his inner self.

Work drains men of energy to such an extent that they're left with no will to invest in emotional issues. Either out of economic necessity or to complete a difficult job, most men work long hours. They're compelled to do so, since patriarchy teaches us that work is far more important than emotions. Because of these ideals, men who stay at home are still considered strange.

If men could access their feelings more, they'd be more likely to find work that they actually enjoy, but society has a different plan. Just consider the movie _American Beauty_, in which the main male character falls into a depression and stops taking his work seriously. He starts to notice his feelings, but can't find redemption and ends up dead.

Movies like this send the message that when men get in touch with their feelings, they lose. But while society is opposed to men noticing their emotions, it's likely the only way they can find freedom.

### 6. Men must become aware of their feelings in order to heal – and women must welcome this. 

Now you know how society dictates that men be strong and how this prevents most men from getting in touch with their feelings. But how can men overcome this emotional barrier?

By simply acknowledging that their feelings exist. After all, the majority of men have been told that they shouldn't have feelings at all, and therefore constantly mask their emotions. They create a false self and struggle to build self-esteem or feel their emotions, living every day as a lie.

They play this game because they fear exposing their weakness, but the pain that most men have pushed away is exactly what they need to feel to heal themselves. It might cause suffering, but it is only this suffering that can bring them back in touch with their emotional selves.

Women also have a role to play in making this transformation, because they often urge men to show their feelings but then shut them down. This is to be expected, since women are indoctrinated into the patriarchy as well. When men are suffering, they often turn to women, who can't handle an emotional man and turn their backs.

The author recalls a time when she did just that. She asked her partner to share his feelings and, when he did, she began interrupting him and making him stop as she began to cry. In this way, she essentially communicated to him that it's better for him to hold his feelings inside because they're too painful for others.

In short, women also need to overcome patriarchal barriers and learn to let men talk about their feelings.

> _"To heal, men must learn to feel again."_

### 7. By practicing integrity and finding space to express love, men can change for the better. 

Have you ever done something for work that contradicted your values? It probably didn't feel great.

Most men live their entire lives that way, which is why it's essential for them to practice integrity. By doing so, they can learn to shed their masks and become whole people again. After all, integrity is about being one undivided person, which is exactly the opposite of what men are under the patriarchy.

In patriarchal culture, men are taught to hide their true selves to aspire to an ideal that they will never reach. They split their personalities, dissociating from their real selves and causing inner suffering. But their true self can be rediscovered through the practice of integrity.

But of course, since men have been taught to tell themselves lies and compartmentalize every aspect of their lives, practicing integrity will be painful.

This is another necessary pain, because only by mourning the loss of patriarchal influence will men begin to heal.

So, while men need the will to change, they also need space to do so; family provides the perfect environment for this. Instead of being seen as a place of resistance, the family must be transformed into one of love. In so doing we can create a culture in which the function of the family is re-envisioned.

Boys will then learn that building connections with others and not practicing violence and domination will make them men. Men will be able to love, and women will be able to love them back.

### 8. Final summary 

The key message in this book:

**Men are taught to be dominant, violent and unfeeling. This social expectation causes them and the people around them to suffer, but there is another way. Men can learn to acknowledge and share their feelings, and in so doing, learn how to love others and themselves.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Raising Cain_** **by Dan Kindlon, Ph.D. and Michael Thompson, Ph.D.**

_Raising Cain_ (1999) explains how boys have to navigate a society rife with misguided ideas about masculinity and filled with cruel classmates who are ready to pounce on any sign of weakness. Discover how these conditions can create emotionally stunted and suicidal young men, and find out what can be done to help remedy their situation before it's too late.
---

### bell hooks

bell hooks has written many influential works of cultural criticism and feminist theory. She is the founder of the bell hooks institute in Berea, Kentucky and lives in New York City.

