---
id: 53444b856432320007070000
slug: the-willpower-instinct-en
published_date: 2014-04-08T12:30:55.000+00:00
author: Kelly McGonigal
title: The Willpower Instinct
subtitle: How Self-Control Works, Why it Matters and What You Can Do to Get More of It
main_color: FFE75F
text_color: 807330
---

# The Willpower Instinct

_How Self-Control Works, Why it Matters and What You Can Do to Get More of It_

**Kelly McGonigal**

_The Willpower Instinct_ introduces the latest insights into willpower from different scientific fields, such as psychology, neuroscience, economics and medicine. While considering the limits of self-control, it also gives practical advice on how we can overcome bad habits, avoid procrastination, stay focused and become more resilient to stress.

---
### 1. What’s in it for me: discover how you can better wield your willpower to achieve your goals. 

Why is willpower important? Research shows that people with stronger willpower are better off in almost every aspect of life: they're happier and healthier, have more satisfying and long-lasting relationships, are more successful, make more money — and even live longer. In a nutshell, if you want to improve your life, your willpower is a good place to start.

In _The_ _Willpower_ _Instinct_ you'll read why you shouldn't deprive yourself of your favorite foods when dieting, and you'll discover why thinking that you hold no prejudices will make you act in a more prejudiced way.

Finally, you'll learn how you can improve your willpower by placing that bowl of sweets right in front of your nose — it's for your own good.

### 2. Willpower consists of three forces: I will, I won’t and I want. 

Life is full of temptations: you may be offered a chocolate chip cookie right after you've started a diet, or find a pack of cigarettes just as you've resolved to quit smoking. These situations are _willpower challenges –_ a challenge in which your immediate desires fight with your long-term goals. 

So what makes you able to exert self-control in these situations? 

The strength of your willpower, which consists of three powers: "I won't," "I will," and "I want."

First, your "I won't" power is the ability to say no even when your whole body wants to say yes.

This power covers the common conception of willpower: the ability to resist temptation.

Temptation comes to each of us in different forms, be it chocolate, cigarettes or a sexy stranger. And each temptation can be seen as an "I won't" willpower challenge that asks: Do you have the strength to say no?

You can determine your most important "I won't" challenge by asking yourself: Which habit that is hurting your health, happiness or career would you most like to give up?

The second element of willpower is your "I will" power — the ability to do what you dislike now for a better future.

Your "I will" power helps you accomplish those tasks that are both unpleasant _and_ necessary to achieve your goals — for example, studying to pass exams and get a degree.

You can find your most important "I will" challenge by asking yourself: Which habit should you stop putting off in order to improve your life? 

Finally, there's your "I want" power — the capacity to remember what you truly want.

What you truly want is what is best for you in the long term — despite present temptations. To resist the present you need a clear long-term goal that guides your actions. It's this goal that fuels your "I want" power by reminding you what's at stake.

You can find your "I want" challenge by asking yourself: What is the number one long-term goal you would like to focus more energy on? Which immediate desires are keeping you away from it?

### 3. Meditating increases awareness and helps avoid distractions – which in turn boosts self-control. 

These days, distractions are everywhere: there are links to click, series to watch, parties to go to. I'll just check my email one last time, we say to ourselves.

But you're running a larger risk that you think.

That's because when you are distracted, you are actually more likely to give in to other temptations.

When your mind is preoccupied, immediate temptation can more easily overshadow your long-term goals.

This was shown in a study where students were instructed to remember a telephone number while making a choice between which snack to eat during the experiment: chocolate or fruit. 

The students with their mind distracted chose the chocolate 50 percent more often than a group of students who were given no memorization task.

But there is a way to deal with distractions — by raising your awareness through meditation.

Neuroscientists have discovered that people who meditate have more grey matter — indicating higher levels of performance — in the regions of the brain responsible for self-awareness.

Meditation cultivates a moment-to-moment self-awareness which helps us realize when we are being distracted and refocus our energy back on the task at hand.

In fact, scientists have shown that it only takes three hours of regular meditation to improve self-control and develop a higher attention span, and after 11 hours of practice the changes are already observable in the brain.

But sometimes distractions feel overwhelming, and you feel like you just can't stop watching that clip.

In such situations the fruits of meditation can help again: by taking a breath and refocusing your concentration on the long-term goal at hand, you can break the distraction cycle and regain control over your impulses.

When your mind is preoccupied you lose significant amounts of willpower. Avoiding decision making when distracted and increasing your self-awareness through meditation can help to save you from willpower failures.

### 4. Willpower is a biological instinct that protects us from harm in the long term. 

What does a saber-toothed tiger have in common with a chocolate cookie? Each of them can interfere with your goal of a long and healthy life.

That's why evolution has given us the instincts to fight both the saber-toothed tiger and the temptation of a chocolate cookie.

You have probably heard of the _fight-or-flight_ _response_, an instinct that kicks in when we face scary or life-threatening situations. Basically, it's your body's built-in ability to devote all its energy to getting your butt out of an emergency.

What most of us don't know is that willpower itself is based on a biological instinct.

One study showed that facing a willpower challenge can activate a specific state in your brain and body that gives you a willpower boost.

This state is called the _pause-and-plan_ _response,_ and as its name suggests, it is very different from the fight-or-flight response:

While the fight-or-flight response heightens your awareness of an external threat and increases your speed (to avoid the tiger), the pause-and-plan response shifts your focus to the internal conflict between your rational and impulsive selves, and slows you down to help control your impulse (to avoid the cookie).

So how can we strengthen this _willpower_ _instinct_ to better slow down our minds and make the best decisions?

By paying close attention to everything that places stress on our minds and body, like anger, anxiety, chronic pain and illness.

All the things that stress you out interfere with your ability to get into a state of self-control by keeping you in that fight-or-flight state — and preventing you from achieving that slow, rational state of mind.

However, there are plenty of ways to improve your stress-resistance and thereby your willpower. Meditation, exercise, a good night's sleep, healthy food and quality time with your family and friends can all help reduce your levels of stress.

And getting active outdoors for only five minutes a day will give you a quick willpower boost too — so get out there!

### 5. Willpower is like a muscle – it can be trained but also overused. 

Ever helped a friend move? At the end of the day your muscles are so tired you couldn't carry anything more even if you wanted to. You might say to yourself, "I should be hitting the gym more often, I would be less tired". For your willpower, it's just the same: after flexing your willpower muscle too much you become exhausted and can't control yourself anymore. And if you hit the willpower gym, you could improve the strength of your willpower muscle. 

So why does overusing your willpower cause you to run out of it?

Because every successful attempt to exert self-control draws from the same limited source. 

This means that resisting a temptation will not only weaken your ability to avoid other temptations, but also prompt procrastination and other willpower failures. 

And this _willpower exhaustion_ happens all the time. 

This is because many daily tasks you would not think of as willpower challenges — having to commute, sit through a boring meeting or choose between 20 brands of shampoo — all draw from our limited daily willpower reserve. 

But although we are constantly draining our willpower, we can do our best to maintain it at a high level by keeping our blood sugar steady and our energy levels high. 

Low-glycemic foods such as nuts, cereals, fruits, vegetables and high-fiber grains all contribute to resourcing our willpower.

But there is another way of improving our willpower — by training the willpower muscle.

Just as it's possible to train your arm muscle through weight lifting, it's possible to train your willpower muscle with willpower challenges.

By performing small but regular willpower challenges you can gradually improve your self control. 

For example, keep a forbidden candy jar in an easily visible location you're never allowed to touch — no matter how tempting it looks. Regularly practicing with this small temptation will train your willpower muscle, which will help you cope when it comes to larger willpower challenges.

In the next blinks, you'll discover why we often succumb to our desires.

### 6. Don't indulge in the present because you think you've done well in the past. 

Hardly a week passes without news about the moral failures of upstanding citizens — politicians, athletes or religious leaders who have done something wrong. Why do these supposedly virtuous people make such large mistakes?

Actually, thinking that you are "virtuous" lowers your self-awareness and discipline.

This was shown in a study where a group of university students were asked to agree or disagree with strongly sexist statements. Not surprisingly, very few agreed.

A control group then received milder versions of the same sexist statements, and this time many more people agreed.

But when both groups had to make a decision in a hypothetical hiring situation, it was paradoxically the students who disagreed with the highly sexist statements who were more likely to discriminate against female candidates than the students from the group who had agreed to the milder versions. And the exact same pattern emerged when racist statements were used.

This is because when we feel we are being virtuous enough, we see less need to control ourselves.

And this is exactly what happened in the experiment. After the students proved to themselves that they were not sexist by rejecting a statement, they then paid less attention to their actual behavior in the hiring task.

Another example of using good past behavior to excuse present bad behavior is when we give ourselves something "bad" as a reward for being good — for example, eating a doughnut after a long workout.

However, this is counter-productive: giving yourself a reward which undermines your long-term goal is not a promising strategy for success. 

So don't allow your successes to loosen your self-discipline. Otherwise you might find yourself nullifying your progress by allowing your self-indulgent behavior. Instead, stick to a rule which serves your goal, but is not so challenging that you can't stick to it every single day without failure.

### 7. When the reward system of your brain takes over, temptation becomes almost irresistible. 

Why do we often feel bad and guilty after satisfying our immediate desires, like buying a new sweater we don't need, or spending a lazy evening in front of the TV? And why do we do it again and again, despite knowing better?

Because your brain's _reward system_ is not always your friend — and sometimes it leads you in the wrong direction.

So what exactly happens in the brain when you crave something?

First you see or smell something you desire — and just that is enough to activate the reward system in the brain. 

The system releases a _neurotransmitter_ called _dopamine_ which activates the areas of the brain responsible for attention, motivation and action. These dopamine releases can be triggered by anything we have associated with feeling good: a 70-percent-off sale sign at a mall, the smell of a rib-eye steak (or a vegan burger), or an attractive face smiling at you. 

And when this dopamine is released, the object that pulled the trigger immediately becomes very desirable — even if it's against our long-term interest, like unhealthy food, internet binges, binge drinking or one night stands. This is why we engage in activities that seem to be irresistible at first glance, but afterward leave us feeling guilty and dissatisfied. 

Our prehistoric ancestors, however, were not troubled by this reward mechanism. In fact, being attracted to sweet things was to their advantage, as sweet fruit and berries were a crucial part of their diet. Our ancestors were also more free to pursue sexual impulses without modern-day societal constraints. 

But even though this impulsive mechanism isn't as useful in our day and age, it's still there, and we have to make sure it doesn't push us toward unhealthy or unwise choices. 

So what can you do? You can actually make this weakness your strength by combining unpleasant tasks with something that gets your dopamine firing. For example, bring your boring paperwork to your favorite café and finish it over a delicious cup of hot chocolate.

### 8. Feeling bad undermines willpower by triggering cravings and high expectations. 

Stress is a common source of unhappiness. It can be caused by professional or personal worries, but also by external events, like bad news in the media.

Stress is one of the biggest threats to your willpower because it induces dreaded cravings.

How?

Stress makes you feel bad about yourself, which motivates you to do something to make yourself feel better.

Unfortunately sometimes the easiest way to feel better is by doing the very thing you'll later feel bad about.

For example, losing money at the casino can make you feel so upset that you continue gambling in order to win a game — and thereby relieve the stress.

But this impulse might actually lead you to greater and greater risks, and eventually lose you a fortune.

So how can you overcome this? When you feel stressed, don't give in to immediate cravings. Instead try out stress-relief strategies that have a more sustainable effect, like exercise or meditation. These activities might involve more effort, but will leave you with a feeling of satisfaction, not guilt.

But don't make unrealistic resolutions to counter stress — you're more likely to give up early.

When people reach a low point in their lives, like facing an enormous mortgage, they often decide to drastically change their life.

For example, we might resolve to cut back all our expenses by 25 percent to get our finances back on track. That's a big change, and such big resolutions often feel like they can completely transform our life: we imagine being free of problems, people treating us completely differently and so forth — just because of this one change. This boosts our self-confidence.

However, this can backfire, for the higher we set our goal, the more difficult it is to stay on track. Failure to meet our expectations then leads us to frustration, guilt and self-doubt, and soon we typically abandon our efforts altogether.

To avoid this fate, remember: when you fail to achieve your goals, don't despair. Just forgive yourself and try again.

### 9. When we are too focused on the present moment, we make bad long-term decisions. 

Do you ever overcommit yourself to responsibilities and later find yourself overwhelmed?

Do you sometimes regret your past choices when confronted with their actual costs?

Both phenomena are caused by our inability to imagine the future clearly — and especially to imagine our _future_ _selves_.

We don't see our future selves as ourselves, but as distant, different people. Our brain perceives them as strangers due to our inability to observe their thoughts and feelings.

This can lead to us putting off tasks, hoping that our future-self will have more willpower to deal with them — or even worse, racking up debt and hoping our future selves will be able to pay.

These hopes lead nowhere because your future-self is not different from your present self, and will also struggle when facing challenges, be it mustering the willpower to do an unpleasant task or balancing the budget.

So what can we do? A good method for becoming more familiar with your future-self is _visualization_ : imagine your future-self thinking back on the decisions you are making today and their consequences.

So what else makes us neglect our future selves?

Our vulnerability to instant gratification.

When a tempting object is staring right at you, resistance often feels futile because the reward system in our brain reacts so strongly to visible rewards.

Why?

Because visible rewards make us overestimate the benefits of instant gratification and underestimate the value of exerting self-control. This leads us to make decisions that our future selves later regret.

But temptation becomes weaker if you then create some distance between you and the object — for example, by making it less visible or more difficult to reach.

This was shown in a study where office workers had access to candy. When the candy was placed out of sight inside a desk drawer instead of on the top of the table, the subjects' candy consumption was reduced by one third.

### 10. Attempts to push aside unwanted desires actually make them stronger. 

Here's a challenge: for the next five minutes don't think about white bears. Can you do it? Most people fail in this task. Even though we never usually think about white bears, if you actively try to _not_ think about them, it becomes almost impossible to stop. 

The same is true for your cravings: though suppression might seem to work at first, it actually makes them worse.

This was shown by one researcher who believed that thought suppression compels us to do the very thing we are trying to not think about.

To test his hypothesis, he invited women for a tasting test of two similar chocolates. Before bringing in the candy, he asked the participants to think out loud for five minutes. One group was instructed to suppress any thoughts about chocolate, while the other participants were free to think about whatever they wanted.

And as expected, the group that received the instructions not to think about chocolate reported fewer thoughts about chocolate — but also ate twice as much of the candy.

This is also the reason why most diets simply don't work. The more dieters try and resist a certain food, the more their mind becomes preoccupied by it.

So how can you overcome cravings without pushing them away?

When you're on a diet, don't deprive yourself of your favorite foods because it will only increase your cravings.

Instead of deciding "you won't" eat fast food or cupcakes, devote your energy to the idea that "you will" eat more healthy food. A decline in unhealthy food will automatically follow and you'll have a much easier time sticking to such a positive challenge.

Another way to overcome cravings is by merely _observing_ them: When the unwanted urge appears, allow yourself to notice it. Observe your breath and what you are feeling. Then imagine the urge is a cloud which dissolves and passes on by.

This technique, inspired by mindfulness traditions, is especially useful if you want to rid yourself of an unpleasant habit like smoking.

In the final blink, you'll discover the key environmental factors that affect our willpower.

### 11. Willpower is contagious: our social environment can increase and decrease our willpower. 

Have you ever noticed that you behave and think differently depending on who you're with? In fact, who we interact with influences our beliefs, goals and actions to a remarkable degree. And even characteristics like a strong or weak willpower can be "picked up" from our social context.

For example, studies showed that if we observe other people acting impulsively, we are more likely to be impulsive ourselves and neglect our long-term goals for a pleasurable moment. What's more, the more we like the person observed, the stronger this effect is, and the more willpower we lose.

Luckily this mechanism can also be harnessed for good, for example, with dieting: research shows that having a close friend or family member who recently lost a lot of weight increases your chances of also losing weight.

So how can you take advantage of this?

Ask yourself, do you know someone you admire for their willpower? Try thinking about them more often — because research shows that just thinking about someone with good self-control increases your own willpower.

Another way of harnessing the force of willpower contagion is to get friends and family involved with your willpower challenges.

The power of this approach was shown in the weight-loss intervention at the University of Pittsburgh that requires people to enroll with a friend or family member. The participants were then instructed to support each other in pursuing their goals — for example, by writing encouraging messages or sharing a healthy meal from time to time.

The results were impressive: 66 percent of the participants had maintained their weight loss when checked on ten months later. In contrast, the success rate of the control group — the participants that did not join with a partner — was only 24 percent. 

So if you and your loved ones share a willpower challenge, make it a group project!

### 12. Final Summary 

The key message in this book: 

**By learning how to concentrate on our long-term goals, maintain our willpower supply and train our willpower muscle, we can gain greater control over our bad habits — and live more fulfilling lives.**

Actionable advice:

**Keep close track of how you are managing your willpower challenges.**

For at least one day, try to observe your decisions very closely. Were there situations you could have avoided to better maintain your willpower supply? Were there times when you gave in to an impulse because you lost sight of your long-term goal? Pinpoint your weaknesses and visualize yourself overcoming them.

**Regularly refill your willpower with mindfulness.**

Try the five-minute brain training meditation: focus on your breath using the words "inhale" and "exhale" in your mind, and when your mind wanders, just notice it and bring it back to your breath.
---

### Kelly McGonigal

Kelly McGonigal, PhD, is a health psychologist and lecturer at Stanford University. She has received several awards including Stanford University's highest teaching honor, the Walter J. Gores award. She is also the author of _The Upside of Stress_, which deals with the ways in which stress can be beneficial for us and how we can better manage it.

