---
id: 529c9e1862643300110f0000
slug: the-wisdom-of-crowds-en
published_date: 2013-12-04T10:51:01.000+00:00
author: James Surowiecki
title: The Wisdom of Crowds
subtitle: Why the Many Are Smarter Than the Few
main_color: FC8D4C
text_color: B06235
---

# The Wisdom of Crowds

_Why the Many Are Smarter Than the Few_

**James Surowiecki**

_The Wisdom of Crowds_ explores why, and under which circumstances, groups of people can come up with better solutions to problems than any one person — even if that person is an expert. By analyzing the way individuals and groups make decisions, the book gets to the bottom of _the wisdom of crowds_, and shows how this wisdom can be used to make reliable decisions.

---
### 1. Big groups can solve problems better than individuals. 

At a livestock fair in 1906, scientist Francis Galton observed visitors taking part in a contest to guess an ox's weight in pounds. The results were surprising: although no individual, including cattle experts, could guess the ox's exact weight, the mean value of all the visitors' estimations was a mere pound more than the animal's actual weight. In other words, the _collective_ estimation was far more accurate than any individual one.

Galton had discovered how a group of people was, indeed, wiser than each of its individual members (including experts).

The television show _Who Wants to be a Millionaire?_ is another example of this phenomenon. Candidates on the show have to answer round after round of multiple-choice questions to win the prize and, in the process, can turn to "lifelines" for help. They can use the Ask-the-Expert lifeline to call up an expert for advice, or the Ask-the-Audience lifeline to find out the audience's opinion. One analysis showed that only 65 percent of the experts were right, while 91 percent of the crowd's votes were right.

However, a quick glance at history could confirm that not all groups are wise: we witness raging crowds, violent mobs and mindless herd behavior all too often. As Friedrich Nietzsche once wrote, "Madness is rare in individuals — but in groups, parties, nations and ages it is the rule."

Of course, there are also errors in collective intelligence that can lead a group to make faulty decisions. But if the group manages to stay diverse, work as a team and foster open dialogues, the "wisdom of crowds" is not a meaningless cliché, but actually — as the examples above have shown — the surprising truth.

**Big groups can solve problems better than individuals.**

### 2. The more diverse the group, the wiser its decisions. 

Heterogeneous groups are often better at tackling problems than homogeneous ones. In a heterogeneous group — that is, a group of people with different ages, genders, religions and professions — each individual introduces new ideas and perspectives that would have never seen the light of day in a homogeneous one.

Homogeneous groups, such as those made up of experts, are often handicapped by the fact that their members' skills and approaches are too similar. In other words, it is more difficult for them to consider counterarguments and think unconventionally.

America's unsuccessful Bay of Pigs invasion under the Kennedy administration is one notorious example of how a homogeneous group's opinion can have unfortunate consequences. The administration implemented its strategy for invading Cuba without consulting anybody who was skeptical of it. The historian Arthur Schlesinger, one of the experts present at the strategic meetings, stated later that the meetings had taken place "in a curious atmosphere of assumed consensus."

Simply put, a homogeneous group's weak variations of the same idea can't compete with the heterogeneous group's abundance of ideas. With its manifold perspectives and wider vantage point, a heterogeneous group can more easily identify and discard bad ideas.

However, this doesn't mean that groups should exclude experts from joining their ranks. On the contrary: experts are indispensable because they bring to the table knowledge that others don't have to the table. The danger lies in groups entirely made up of experts — especially if they are all experts in the same field.

**The more diverse the group, the wiser its decisions.**

### 3. The bigger the group, the more intelligent it is. 

As a general rule — as long as we're talking about people and not clones — the bigger the group, the greater its diversity. And the more diverse it is, the more valuable ideas and skills it brings together.

When it comes to decision making, big groups are usually better at taking account of unusual perspectives and minority opinions because, quite simply, their members come from many different standpoints.

Big groups do not avoid conflicts: instead, they confront them head on and openly debate the issues at hand. Hence, there is a much lower risk that a big group will make a quick yet **false** compromise, in comparison to a small group.

When it comes down to it, it's far easier for people in a small group to influence one another. In such groups, one person will often monopolize the discussion, which can heavily sway the groups' decisions. That's why a small group's decisions tend to be more volatile and radical than those of a big group. To make matters worse, people who see themselves as leaders are prone to overestimate their own abilities — even if they lack solid expertise.

For instance, in experiments where military pilots and navigators were given the same problem to solve, the higher-ranking pilots defended their solutions far more convincingly than did the lower-ranking navigators. The same behavioral pattern persisted even when the pilots' solutions were wrong and the navigators' solutions were right. The navigators went so far as to voluntarily hand over their control to the pilots, who they assumed must be right because of their higher rank.

A big group inherently decreases the likelihood of such risks. The bigger the group, the smaller the chance that irrational opinions will prevail.

**The bigger the group, the more intelligent it is.**

### 4. Herd behavior and hierarchies weaken a group’s intelligence. 

Since social interactions abound in big groups, their decisions are more likely to be influenced by their members' social behavior. This can bring about several dangers — _herd behavior_ being one of them.

Herd behavior refers to a widespread social-behavioral pattern within a specific group. It is a defense mechanism by which people support group decisions or actions in order to reduce their own personal risk. Human beings are inherently social creatures, and, as such, they feel safer in the crowd. Take teenagers, for example, who want to do exactly what everybody else is doing, for fear of not fitting in. Whose parents didn't respond to that by asking, "If all your friends jump off a cliff, are you gonna follow them?"

Groups can also be plagued by fallacies such as the "social proof" — i.e., when people observe that a number of other group members do or say certain things, and figure there must be a good reason for it. What people do and say _produces_ the proof that what they are doing is right — even if it isn't actually founded in truth. That's why, however irrational a belief might be, if enough people voice it, it usually won't be long until they find supporters. After all, if you were to suddenly find yourself in a group of people assuring you that 2 + 2 = 5, at some point you too might wonder if they're not on to something.

This type of behavior is bolstered when social rank comes into play: in clear hierarchies, only certain members of a group will have a chance to speak. The highest-ranking members can thus steer every discussion in a direction that reflects their own views, and the rest of the group follows.

In order to prevent these negative repercussions, strict hierarchies should be avoided and all members of a group should be encouraged to express their own views rather than blindly following the herd.

**Herd behavior and hierarchies weaken a group's intelligence.**

### 5. Groups can be intelligent only if they are made up of many independent thinkers. 

The irrational tendencies that arise in every group can be contained only when all its members are able to _think independently_.

In a group where nobody thinks independently, people usually end up having the same opinion, as this avoids conflicts — even though such conflicts should probably be discussed openly. Contradictions are ignored, or they are reinterpreted to mean something else. The group members take on other people's views without questioning them, and their ways of thinking grow more and more similar.

If a group needs to make an intelligent decision, it must prevent all of that. The key lies in allowing the group to be made up of individuals who have their own diverse opinions and sources of information — i.e., individuals who think independently.

Diversity is important too. If the members of a group are diverse, it is easier for individuals to stand by their own opinions. On the contrary, if a group is dominated by a certain set of beliefs, individuals with deviating views tend to hold back and go with the tide.

This kind of behavior can have grave consequences. Take, for example, the _Columbia_ space shuttle disaster in 2003. Just after the shuttle's launch, a piece of insulating foam broke off and made a small puncture in the shuttle, which caused its disintegration as soon as it reentered the Earth's atmosphere.

Although many employees of the Air Force Space Command recognized the piece of insulating foam as a safety hazard early on, they did not speak up, allowing things run their fatal course. That was because the team's dominant leader imposed her opinion on everyone else, thereby nipping any debate in the bud. As a result, the team missed the opportunity to analyze the damage done to the shuttle before it reentered the atmosphere.

If the NASA team had been allowed to think independently — and not been forced to accept one person's opinion — this disaster might have been prevented. Every decision would have been thoroughly considered, everybody would have been able to contribute their own personal judgments, and the group could have worked together to reach a consensus.

**Groups can be intelligent only if they are made up of many independent thinkers.**

### 6. The most intelligent groups have decentralized structures but pool their information into one central place. 

Big groups are most successful when they have a decentralized structure — that is, when all members can organize themselves and act in small subgroups without being controlled by an omnipotent central leader.

The more invested that members of a group are in solving a problem, and the more independently they work on it, the more likely it is that they will come up with good solutions.

This idea was put to good use in ancient Athens: citizens could negotiate most cases involving petty crimes in local courts, and they were allowed to organize public festivals themselves.

However, if a group with a decentralized structure is to act intelligently, its various operations, results and pieces of information must be collected and analyzed in one central place where people have an overview of the bigger picture. In other words, decentralizing power incurs the risk of not seeing the forest for the trees.

This is precisely what happened among the various intelligence branches of the United States prior to the World Trade Center attacks on September 11, 2001. The organizations' decentralized structure hampered the flow of information among them, making it impossible to predict (and potentially prevent) the attacks. The collective wisdom of the specialists working for the FBI, the CIA and the NSA could not be used intelligently because there was no central figure analyzing the available information.

**The most intelligent groups have decentralized structures but pool their information into one central place.**

### 7. Coordination comes naturally to big groups – as long as everyone doesn’t think only about himself. 

The more complex the challenges a group faces, the more it must rely on its members to coordinate themselves.

The key lies in the members' ability to agree upon their intentions, expectations and actions. Individuals shouldn't think and act only in their own interest, but keep an eye on the bigger picture and put themselves in the shoes of other group members.

People experience this every day on city sidewalks: pedestrians practice a sort of mutual anticipation of each other's movements as they decide how and where they want to walk. We slow down our pace to avoid collisions with oncoming traffic, and quicken our pace to avoid making traffic for others.

Such traffic rules can be applied to many other areas of life, too: People who observe where others want to go, and what they do, end up reaching their goals faster. And those who think only about themselves will cause traffic and accidents.

Nobel Prize winner Thomas Schelling illustrated one interesting example of this coordination phenomenon in an experiment. He asked a group of his law students to imagine that they had to meet a friend in New York City on a certain day, but that no specific location or time of day was set. Interestingly enough, most of them solved the problem in the same way: by showing up at the information desk of the central train station at noon on the date they'd agreed upon. Schelling's experiment thus shows how people can coordinate their intentions and expectations with one another and reach a collectively beneficial result.

**Coordination comes naturally to big groups — as long as everyone doesn't think only about themselves.**

### 8. People cooperate in groups when they believe they will gain long-term benefits. 

As social creatures, human beings are used to working in groups. But such groups function only when all of their members put aside their selfish, short-term interests for the long-term benefit of the group.

One way to encourage such behavior is to establish laws and rules. States can punish their citizens if they evade taxes or murder their neighbors, and companies can let their employees go if they do not fulfill their duties or if they harass their coworkers.

Ideally, people behave cooperatively and prosocially even without sanctions. Groups that function well allow members to _trust_ one another. This means that members will not only pursue their own interests but the interests of the group as a whole.

But trust is not the only reason for prosocial behavior: people cooperate because they profit from belonging to the group. For example, everybody wins when people donate to charitable organizations, make honest products that others can buy withouthesitation, or leave tips even if they don't have to.

Simply put, individuals experience far less stress when making decisions if they don't have to think about optimizing their own selfish gain.

This type of helpful, voluntary cooperation exists, above all, when the members of a group believe that the group will last, and the affiliation with it will pay off for everyone in the long term.

**People cooperate in groups when they believe they will gain long-term benefits.**

### 9. Final Summary 

The key message of this book is:

**Under the right circumstances, groups of people can not only make surprisingly intelligent decisions, but even produce better results than could a group of experts or the smartest individuals in those groups.**

The questions this book answered:

**Why does it make sense to have groups made up of diverse people to solve problems?**

  * Big groups can solve problems better than individuals.

  * The more diverse the group, the wiser its decisions.

  * The bigger the group, the more intelligent it is.

**What dangers does it entail, and which prerequisites must be fulfilled?**

  * Herd behavior and hierarchies weaken a group's intelligence.

  * Groups can be intelligent only if they are made up of many independent thinkers.

  * The most intelligent groups have decentralized structures but pool their information into one central place.

**How can we encourage groups to work together towards the best possible outcome?**

  * Coordination comes naturally to big groups — as long as everyone doesn't think only about themselves.

  * People cooperate in groups when they believe they will gain long-term benefits.
---

### James Surowiecki

James Surowiecki is an American journalist. He writes a column on financial matters for _The New Yorker_, and has published articles in the _New York Times_, the _Washington Post_ and the _Wall Street Journal_.

