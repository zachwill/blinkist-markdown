---
id: 5829cfc192e9210004940bac
slug: the-wisdom-of-insecurity-en
published_date: 2016-11-17T00:00:00.000+00:00
author: Alan Watts
title: The Wisdom of Insecurity
subtitle: A Message for an Age of Anxiety
main_color: C95228
text_color: B06235
---

# The Wisdom of Insecurity

_A Message for an Age of Anxiety_

**Alan Watts**

In _The Wisdom of Insecurity_ (1951), author Alan Watts discusses the paradoxical nature of modern life: we pursue goals and covet material goods that promise happiness, but which leave us feeling empty and more anxious than ever. As we indulge in unproductive thoughts about the future or the past, we tend to forget about what is most meaningful — the present moment.

---
### 1. What’s in it for me? Learn why consumerism will never give life meaning. 

There are many things that money can buy: swanky cars, palatial houses, high-tech gadgets. But there many other, more important things that cash can't get you, things such as happiness and purpose. And when you think about what you want out of life, what seems more important: a Ferrari or a purposeful existence?

Bluntly put, a life of consumerism and pleasure-seeking won't be fulfilling.

So, what will be?

These blinks guide you toward finding meaning in life. After reading them, you'll stand a better chance of living the life you want.

In these blinks, you'll also discover

  * why the decline in religion has left a gap in society;

  * that the best ideas come when you're not thinking about them; and

  * how pain is closely related to pleasure.

### 2. As the power of religion and social norms diminishes, life becomes more uncertain. 

Do you ever feel anxious or insecure because of a lack of fulfillment in life? You're not alone: this feeling is more common now than ever before.

Not long ago, the average person's life was guided, or at least influenced, by the strict doctrines of religion. It might sound counterintuitive, but these strongly held beliefs about morality and the afterlife actually helped people feel more fulfilled.

The promise of an afterlife is especially grounding and reassuring.

Humans can put up with many of life's harsh realities as long as there is something to look forward to. The devoutly religious can remain positive through the worst hardships because an infinitely blissful afterlife awaits them. But the waning influence of organized religion has shifted people's outlook on life.

The twentieth century saw societies around the world moving away from many religious concepts. The myth was losing its strength. Suddenly, with the afterlife in question, people were struggling to make sense of the pain and suffering of life. Where is the reward that makes it all worthwhile?

So people began to fill this gap with the cheap thrills of modern society.

Deprived of the meaningful narrative provided by religion, many people felt an inner emptiness. And so, in an attempt to fill this void, more people turned to stimulants, like drugs, partying or overwork. These methods can provide distraction from the bigger existential questions kicking around in the back of your mind.

But what this constant stimulation is really doing is desensitizing you.

It can also lead to addictive behavior. Using alcohol to fill the void can quickly take you from beer to hard liquor as your tolerance levels rise. And this desire to increase intake mirrors what is happening in today's society. We are chasing down more and more stimulation to make up for a lack of meaning in our lives.

### 3. Consumerism promises happiness but leaves you unfulfilled. 

Have you tried to convince yourself that you'll find happiness once you get that promotion, buy that fancy new car or own that perfect house in the suburbs? Many people spend their valuable time working toward such goals. And many find only emptiness once they achieve them.

This is the essential problem of consumerism: the chase for happiness is never over.

Maybe you've seen the image of a donkey being led by a carrot that's always dangling just out of reach. Well, people living in Western societies work much the same way, forever chasing what they'll never catch.

We start down this path at an early age. Children are told they'll find happiness by getting good grades in school and going to college; then it's going to graduate school; and, finally, getting a well-paid job with a good pension for a happy retirement.

But what do you do once you have that comfortable job and nice house? Emptiness might start nagging at you again if you don't distract yourself by coveting your neighbor's nicer car or that bigger house down the block. And so the anxieties and the hunt for happiness continue.

This way of thinking can push you into making terrible decisions.

It is especially common for this mindset to lead you down the wrong career path. Parents, teachers and authority figures might convince you that attaining a prestigious job should be the goal of any young professional. But following this advice could result in a lifetime of unhappiness.

What if you knew that your life's calling was to help people — a mission that brought you joy and fulfillment? But the influential people in your life steered you away from becoming a nurse or a social worker, and you entered law school instead. It's far too easy to end up spending your life doing unfulfilling work in the hunt for someone else's idea of happiness.

### 4. There is no pleasure without pain, so stop worrying and let go of negativity. 

It's completely natural to want to experience the joys of life. We want the highest highs without any of the lows. Unfortunately, though, life just doesn't work that way.

In order to really appreciate the intense pleasures of life, you need to process the painful parts, too.

There is joy and beauty in life, but it always comes at a price. For example, think of the joy of meeting someone, falling in love and forging a lasting bond. But, of course, there's a potential price: your partner may fall ill or out of love. Experiencing pleasure is exactly what makes it possible to experience pain; the two emotions fall on different parts of the same spectrum. Experience one, and you will experience the other.

So what can you do?

You can change your perspective and stop looking at either emotion as being positive or negative. Instead, think of them as temporary events. You can then recognize pain as simply being a necessary part of life.

For instance, just as the agonizing feeling of thirst can lead to the sweet satisfaction of a glass of water, you can only savor the best moments once you've experienced the worst.

And it is with this perspective in mind that you can break the vicious cycle of worry that can take over your life.

A nice house with a mortgage usually comes with a slew of worries. What if you get sick or injured and can't make the payments?

Even if you acknowledge the irrationality of such worries, they can nevertheless lead to _other_ anxieties. For instance, why are you worrying about whether or not you're worrying about things that haven't even happened! It is a vicious cycle.

But if you adopt the right perspective you can reconcile yourself with life's ups and downs and insecurities. You can break the cycle, let go of the worries and stop trying to control everything.

### 5. To truly experience life, have awareness in the present moment. 

Has this ever happened to you: You're feeling particularly unhappy or hopeless, and someone, instead of taking pity, tells you that you're thinking too much? Might seem a bit callous, but, actually, that person was probably right.

Many people are disappointed with life not because of what they're doing but because they're fixated on what they need to do or what they regret not having done.

So, instead of planning out the future or bemoaning the past, focus on the experiences of the present.

Simply put, thinking about something is never as profound as actually feeling it. Take music, for example. Thinking about a Mozart symphony is nothing compared to being in the audience and letting the music wash over you.

The same can be said for any experience. Don't think about how it will impact you later. Enjoy it now.

For instance, rather than taking a picture of a delicious bowl of pasta and thinking about how cool it will look on Instagram or Facebook, focus on enjoying the meal. Nothing compares to the experience of actually savoring the taste of good food.

You might be wondering: What if the present moment is making me miserable? Shouldn't I be focusing on pleasant prospects or comforting memories?

Actually, resisting or denying present emotions will only make matters worse.

Life is like a long and powerful river. From time to time, there will be some rapids. All you can do is ride it out. Resist or try to get back upstream, and you might drown. Stay calm and ride the waves, and the river will carry you to a safer place.

Furthermore, this is exactly how your mind works. It _wants_ to give in to the emotions you are experiencing. It's only when you try to resist or escape these painful emotions that anxiety takes hold. If you embrace frightening emotions and experiences, the tensions will subside.

### 6. Understand that your mind and body are the same entity. 

These days, we spend so much time in our head that we lose touch with what's going on elsewhere in the body. But constant cogitation subordinates the wisdom that our body and subconscious has to offer.

Have you ever spent hours struggling with a seemingly insoluble problem — and then, while in the shower or on a walk, not consciously thinking about anything at all, struck upon the perfect solution?

It's a common experience, and it reflects one of the benefits of a holistic, total-body approach to life. Once you recognize that mind and body are one and the same, you can unlock your full potential.

One way to facilitate this process is to allow yourself to fully engage with your environment and slow down the constant analytical nagging of your brain. This will allow your mind to subconsciously process information that can lead to creative breakthroughs.

Consider what goes on during the creative process. Whether cooking up strategies for your company's marketing plan or formulating a thesis for an important term paper, you probably rarely hit upon tenable solutions during bouts of strenuous thought. Inspiration tends to come out of the blue, when we aren't consciously seeking it.

And tapping into the wisdom of the unconscious mind can work for all aspects of life.

Take eating, for example. Many indigenous tribes give special attention to their body's wisdom and let their stomach dictate how much they eat. They actually _listen_ to their body; once it tells them it's satisfied, they'll stop eating.

On the other hand, we often don't listen to our stomach. We eat with the head, foregoing the actual pleasure of the food and eating as much as possible.

So attune yourself to other parts of the body and let these inner voices guide you. If your body doesn't approve of something you're doing, it'll speak up! Sometimes it's wise not to second-guess your gut feeling.

### 7. Final summary 

The key message in these blinks:

**Understand that there is no such thing as true security in life. So, rather than falling into a state of anxiety, accept insecurity and pain as a part of life by being aware in the present moment. This will help you find tranquility and contentment.**

Actionable advice:

**Meditate!**

Ever tried meditating in the morning? In addition to its many health benefits, meditation will help you tune into your body and shift your focus away from unproductive worry.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Daring Greatly_** **by Brené Brown**

_Daring_ _Greatly_ explores how embracing one's vulnerability and imperfection is necessary for achieving real engagement and social connection. Through explaining our deep-seated reasons for shame, and showing how to embrace our vulnerability, the author aims to provide guidance for a better private and professional life, and to initiate a fundamental transformation in our shame-based society which, according to the author, needs to adapt a new culture of vulnerability.
---

### Alan Watts

Alan Watts is considered one of the most influential interpreters of Eastern philosophy in the Western world. An advanced student of theology, his enlightening and compassionate writings continue to influence thinkers today. His other books include _The Way of Zen_ and _Eastern Wisdom and Modern Life._

