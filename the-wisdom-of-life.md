---
id: 5ab43b16b238e10005bf5853
slug: the-wisdom-of-life-en
published_date: 2018-03-23T00:00:00.000+00:00
author: Arthur Schopenhauer
title: The Wisdom of Life
subtitle: None
main_color: A04CA7
text_color: 86408C
---

# The Wisdom of Life

_None_

**Arthur Schopenhauer**

Schopenhauer's _The Wisdom of Life_ (1851) is a short philosophical essay about what constitutes human happiness. Starting with ancient Greek philosophers' ideas on human happiness, Schopenhauer develops his own thoughts on what people need to be happy.

---
### 1. What’s in it for me? Get a surprisingly modern perspective on what it takes to be happy. 

The question of what happiness actually _is_ has puzzled humankind since time immemorial. After all, it was one of the first issues that the early Greek philosophers turned to. They called it _eudaimonia_, a term that encompasses prosperity and good fortune as well as happiness.

The question troubled Arthur Schopenhauer, too, and in this essay he presents his own thoughts on the matter. He attempts both to define what happiness is and to ascertain how life should be lived so as to achieve it. According to him, that is the only way to gain the wisdom of life.

In addition to explaining how to build a foundation for happiness, these blinks also reveal

  * why you shouldn't worry what others think;

  * just where pursuing fame will get you; and

  * how one Frenchman wanted to look on the day of his execution.

### 2. Life's blessings can be divided into three categories. 

Ruminating on the qualities and nature of human life is nothing new. But after the rumination comes the practical part. How, exactly, are you supposed to live your life for maximum benefit and happiness?

Of course, the Greek philosophers got there first. Aristotle thought human blessings could be classified into three categories: blessings that are external to the self, blessings of the soul and blessings of the body.

Aristotle was on the right track. There _are_ three categories of blessings, but they don't align with his conception of them.

First and foremost, there's _personality_, or "what a man is."

Personality isn't just your character; it also covers your health, strength, beauty, temperament, moral outlook, intelligence and education.

These attributes are generally determined by nature, and as such they're very significant in governing human happiness.

Most importantly, a person's _inner constitution_, or "what he is made of," plays the biggest role in shaping his well-being. Just think of health: it's axiomatic that a healthy beggar is happier than a sick prince.

Needless to say, for Schopenhauer, the greatest pleasures are those of the mind. As he puts it, "An intellectual man in complete solitude has excellent entertainment in his own thoughts and fancies, whilst no amount or diversity of social pleasure […] can ward off boredom from the dullard."

The second category is _property_, or "what a man has."

Material wealth can satisfy real and basic needs, but it won't get you any further than that. It's never going to truly satiate you or compensate for a lack of inner wealth. Happiness comes from elsewhere. That's why rich people, though materially well off, aren't particularly happy.

Finally, there's _position._ In other words, how you're thought of by others.

An inwardly rich person, unlike a fool, will pay little heed to others' opinions. She'll just live her life.

That's the basics covered. Now let's look at each blessing in more detail.

### 3. Human happiness depends on physical health and the gifts and pleasures of the mind. 

Let's begin with the first of the three distinct categories that make up the blessings of life: personality. You're always going to carry it with you, no matter where you go or what you do. Therefore, who you _really_ are matters a great deal.

A critical part of personality is _health_ and it accounts for nine tenths of happiness. If you're healthy, you're more likely to find pleasure in things. If you're unhealthy, nothing is enjoyable.

Aristotle put it very well indeed when he said, "Life is movement." That's to say, if you want to stay healthy, a little bit of exercise each day will go a long way.

What's more, a mind engaged in constant introspection needs an external counterpart. Consider a tree. Every now and again, the wind needs to shake it up a bit, so it can thrive.

In addition to health, the gifts of the mind are significant in determining human happiness.

A famous phrase from the Old Testament — "The life of a fool is worse than death" — sums this up well.

If you're lucky enough to have been gifted with intellectual abilities, then you should lead an intellectual life. That way, your mind will be kept busy and you needn't ever worry about boredom.

A rich and fertile mind will see beauty in the commonplace, while a fool is stuck with what's in front of him. Cast your consideration upon Goethe or Lord Byron — the fertility of their minds provided them with inner wealth and happy self-sufficiency.

Conversely, if your mind is empty, then you're more likely to search for entertainment, diversion and luxury to stave off boredom. Schopenhauer is pretty damning. For him, "a man is sociable just in the degree in which he is intellectually poor and generally vulgar."

> _"Because people have no thoughts to deal in, they deal cards, and try and win one another's money. Idiots!"_

### 4. Your wealth will determine what you expect in life, and maybe it’ll bring freedom too. 

It's time to examine the second category, _property_, or "what a man has."

Once again, the Greeks got there first. Epicurus divided human needs into three parts. Specifically, these are possessions that satiate or quell certain feelings.

First off, there are _natural and necessary_ needs. These include food, shelter and clothing. Without them, we'd be in pain.

Second, there are the _natural but unnecessary_ needs — that is, all things that gratify the senses. These can be tough to satisfy.

Finally, there are outright _luxuries,_ which are neither natural nor necessary. And as they aren't actually needs per se, they're the hardest to fulfill.

Naturally, there's a bit of overlap among the three categories, because we're all different. What one person considers a luxury, for instance, might be considered a natural but unnecessary need by someone else.

Once we're fed and clothed and safe from the elements, we all have different ideas of what is and isn't "necessary."Moreover, we tend not to expect more than we think it possible to obtain. For instance, you're not going to notice the loss of a fine coat if you never had one in first place. But if you're accustomed to finery, then you may be pained by the lack of it.

This explains why someone born with a great fortune usually takes better care of it than someone who happens into wealth. If you're wealthy from birth, you'll see riches as a necessity that you can't afford to lose. But if you've lived your life without it, you won't worry about losing it.

If you're lucky enough to have been born into wealth — what the author calls a "favorable fate" — then you'll probably be freer and feel more in charge of your time. Ultimately, it'll result in you having a more independent mind-set, too.

> _"Riches, one may say, are like sea-water: the more you drink, the thirstier you become; and the same is true of fame."_

### 5. Worrying about reputation is pointless, and impedes your path to happiness. 

Position is the third category of life's blessings. It's all about how you appear to others. Let's begin with its first aspect, _reputation_.

Generally speaking, we worry too much about other people's estimation of us. We can't seem to help it, though our concern is completely unnecessary.

The story of a man named Lecomte illustrates the point. After conspiring to murder the French king, he was sentenced to death in 1846.

At his trial, he seemed more concerned about his outward appearance than his fate.

He complained that he hadn't worn his finest clothes before the Upper House. Even on the day of his execution, he was more distraught about not being permitted to shave than his impending death. Instead of devoting himself to finding some sense of peace in his final hours, he worried about the opinions of complete strangers.

Excessive attention to other people's attitudes is a folly we're all susceptible to. We call it vanity.

More than being an undesirable trait, vanity is a real impediment to happiness. The opinions of others will distract you from finding peace of mind and inner contentment, both of which are crucial to a happy existence.

Roughly half of life's anxieties can be traced back to overconcern about other people's opinions.

It's a mighty task to reduce this natural impulse, and to tell ourselves not to listen to or think about the views of others. But the moment we see it for what it is — complete folly — we'll be one step closer to being rid of it.

### 6. Pride is pointless, and rank is a sham. 

Coming to grips with understanding position can be hard. In fact, there are two more aspects of it that we really need to talk about: _pride_ and _rank_.

Pride, like vanity, is a common foible. It's also absurd.

Pride comes from within and is best thought of as _"an established conviction of one's own paramount worth."_

But, of course, this conviction may well be misplaced.

Pride actually operates in a similar manner to vanity. But while pride is an internalized opinion, vanity acts externally to the self.

Vanity loves praise, and the vain individual only gains a sense of self-worth by winning the good opinion of others.

In contrast, a proud person has an unreasonably inflated opinion of himself regardless of what others think.

The basest and most unproductive form of pride is national pride. If you're reduced to celebrating your country, you probably have few admirable qualities of your own. To trumpet the superiority of your nation is silly at best, and it's only done by people compensating for their own inferiority. After all, not every nation can be superior to all others, though each would have you believe in its superiority!

Now let's take a look at social rank, which is just as bad as pride.

Rank is a weapon of the state. It falsely informs the opinions of the people at large and keeps them in their place. As such, the institution of rank saves the state a lot of money. It means, for example, that public servants get paid a lot less than their monetary due, because they're supposedly also "compensated" by rank.

Rank is really a fraud, however, because its value depends on arbitrary, artificial convention. You should respect a person because of who she is, not because of what theoretical rank she holds.

### 7. The notion of honor stems from a primitive human characteristic. 

We'd all like to imagine that we're useful members of society. Indeed, if some impolite busybody marched up to you and said that you were useless, you'd probably feel that your _honor_ was under attack.

There are two sides to honor: one is objective; the other, subjective.

Objective honor is the opinion others have of your worth. Conversely, subjective honor is your own self-estimation.

As you doubtless know, society tends to judge people depending on how useful they are to society. One's opinion of one's self counts for very little. So we're taught to focus on the opinions of others, and thus favor objective honor over subjective honor.

In addition to the two categories of honor just mentioned, there are four subcategories of honor.

First up is _civic honor_. Civic honor is universally applicable. The idea is that we should unconditionally respect other people's rights. In short, civic honor makes just, lawful and peaceful social coexistence possible.

Then there's _official honor_. It's associated with people in public service such as doctors, lawyers, teachers or soldiers.

Specifically, it's the notion that people in public service should be suited to the job. If it's generally believed that the person in office is truly qualified for the position and able to manage its responsibilities, then that person has official honor.

Thirdly, there's _sexual honor._ This depends on the division of the sexes. Female honor dictates that a woman give herself to a man only in marriage. On the other hand, male honor requires that he look after the business of the marriage itself.

Finally, there is _knightly honor_, which depends on expressed opinion.

For instance, if someone praises you, then your knightly honor remains intact. However, if someone insults you, your honor takes a blow and you must restore it, either with violence or by forcing your insulter to retract the insult.

This last example demonstrates the fundamental issue with honor. Although it may appear to have some use for society as a whole, it's just an element of primitive human nature, and does nothing for happiness.

### 8. Deserved true fame lasts an eternity, but it’s only a product of underlying personality. 

Honor has a twin: _fame_. But, like the famous twins of Roman mythology, Castor and Pollux, who were immortal and mortal, respectively, fame and honor have utterly different relationships to time. Honor is ephemeral, a mere flash in the pan. Fame is undying and eternal.

The thing about true fame is that you have to work for it. Unlike honor, fame must be won.

There are many lesser forms of fame, but true fame is like an oak. It grows slowly but gains strength with every passing year. Lesser fame is but a mere fungus. It shoots up overnight, it blooms, then it disappears as quickly as it came.

Fame also precedes the person to whom it's attached. We needn't meet the famous to know of them.

What marks fame out is that it's won through merit. Honor comes quickly, but it's also easily lost. You can't just be credited with fame. It takes real effort. But, once acquired, it sticks. Or, as Schopenhauer puts it, "Fame is something which must be won; honor, only something which must not be lost."

What's interesting about true fame is that it barely has any connection to actual achievement.

Rather, it's more like an echo or a reflection of a great personality.

This is the critical point. It's therefore not the fame itself that makes you happy. Instead, it's the underlying merits that make you happy. Fame is only an articulation of these deeper personal qualities.

Similarly, people won't admire you for your fame alone, but because of those attributes that made you famous.

Don't worry if you never achieve fame. You still possess attributes that could contribute to your happiness. Besides, if someone's main goal is merely to achieve fame, then there's really not much in him worth admiring.

So focus on your inner life. If you have sound intellectual abilities, you should work as hard as possible to surpass the ignorant masses. And if you've been gifted with the highest mental faculties, then your studies may bestow a greater gift, and put you among the poets or philosophers.

### 9. Final summary 

The key message in this book:

**There are three aspects of happiness: personality, property and position. Personality, or what is contained in ourselves and what gives us undisturbed pleasures of the mind, is the most important of the three. Property is of lesser significance. It determines how much happiness we expect in life, and may even give us some degree of freedom. Position, or how others view us, impedes happiness. It places too much weight on the opinion of others. It's also a folly to worry about it.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Free Will_** **by Sam Harris**

In _Free Will_, author Sam Harris explains that the concept of "free will" is essentially an illusion. While it might be hard to believe, what we think and what we do lies mostly out of our direct control. This book explains why exactly this is, and what implications it has for society at large.
---

### Arthur Schopenhauer

Arthur Schopenhauer (1788-1860) was a prominent German philosopher. He rejected the idealism of his time in favor of a more practical materialism. Schopenhauer greatly influenced other intellectuals, including Richard Wagner, Friedrich Nietzsche, Leo Tolstoy and Thomas Mann.

