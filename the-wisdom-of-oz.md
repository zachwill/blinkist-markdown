---
id: 546bc9d93435300008380000
slug: the-wisdom-of-oz-en
published_date: 2014-11-20T00:00:00.000+00:00
author: Roger Connors & Tom Smith
title: The Wisdom of Oz
subtitle: Using Personal Accountability to Succeed in Everything You Do
main_color: AD2326
text_color: AD2326
---

# The Wisdom of Oz

_Using Personal Accountability to Succeed in Everything You Do_

**Roger Connors & Tom Smith**

_The Wisdom of Oz_ helps you unleash your power to make change in your life by following the authors' four-step path toward personal accountability. The book is based on the authors' previous bestselling title, _The Oz Principle,_ which was inspired by the 1939 classic musical fantasy film, _The Wizard of Oz_.

---
### 1. What’s in it for me? Take charge of your life and create the change you want to see! 

We'd all like to have something better — whether a new job, warmer family relations, closer friendships or improved health.

But all too often, it seems there are obstacles keeping us from our wants at every turn. We might even feel like victims, stuck in a situation that we have no power to change.

What if the solution to our problems was simply a change in thinking? These blinks show you how to live "above the line" and leave behind the victim mentality.

Through a four-step process, you'll learn how to recognize and achieve your goals and in turn, change your mind and your life. Personal accountability will become a principle that defines not just one, but all aspects of your life, improving yourself and the lives of those you care about.

In the following blinks, you'll also discover:

  * how Alexander the Great motivated his greatly outnumbered army to fight;

  * how a struggling young man from Detroit became one of the world's top neurosurgeons; and

  * how the parents of a murder victim are creating positive change in the lives of hundreds.

### 2. Only you can unleash the positive power of personal accountability. 

Let's say you work in a job that you _really_ hate. The hours are long, the work is tedious and mind-numbing, and worst of all, the pay is poor.

But instead of seeking a new job, you simply moan and complain, saying that there aren't enough jobs in the area, that only people with the right connections get the good jobs — that nothing is going right for _me!_

Many of us have felt this way at some stage. Yet as common as this mind-set may be, it does _nothing_ to improve our lives.

If you really want to make a change for the better, you can't blame others. Instead, you have to take responsibility for yourself, your job and your happiness: this is _personal accountability._

Often, however, being accountable for something feels like a burden. We often seek to shed responsibility as much as possible, by passing it on to our bosses, the government or relatives.

But if you delegate your responsibilities, you become reliant on others. You are _giving away_ the power to change yourself.

The only way you can improve your life is to become accountable for it. This is a truth you must accept; and only then can you find a new job, and gain more qualifications and experience.

It's true that sometimes, situations can seem too overwhelming. But getting up after you've been knocked down is a _choice_ that you make.

With personal accountability, you can achieve your aims and control your circumstances: your choices are yours to make. But what exactly should you choose? Read on to find out.

> _"The solution to most of life's problems and opportunities is a strong dose of personal accountability."_

### 3. To accept responsibility and in turn succeed, you can’t see yourself as a victim. 

Think back to when you faced a tough challenge — a mountain of overdue work, or bills you couldn't pay. When searching for a solution, how did you feel when you thought about the problem?

Typically there are two ways to think about a challenge: _above the line_ and _below the line_.

When we think _below the line_ we consider ourselves victims, and point the finger of blame at others.

Yet when we think _above the line_, we look constructively at a problem and try to solve it.

Let's look at one example. As vice president of sales, Dennis faced the challenge of no new products for the third year in a row. He called a meeting with his other sales managers, who were frustrated that they had to sell the same old products in which customers had no longer any interest.

For the first 30 minutes of the meeting, Dennis allowed his staff to think below the line, to get their complaints and frustrations off their chest. But then he got down to business and encouraged his staff to think above the line.

By shifting the tenor of the conversation from complaints to real change, Dennis and his team developed creative sales strategies that they eventually turned into record sales.

Thinking below the line can be a perfectly natural response: you often see yourself as a victim when you are not to blame for your troubles, as during a natural disaster or when you're a victim of crime.

But how you _react_ to these events remains _your choice_.

In 1989, Adam Walsh, the six-year-old son of John and Revé Walsh, was kidnapped and shortly thereafter found dead. Yet even in the wake of such a brutal act, the couple managed to think above the line and took action to help others. They have since worked to create support systems, develop preventative measures and improve legislation for cases of missing children.

So how can you turn thinking above the line into positive change? Learn in the next blinks.

### 4. Don’t let blind spots keep you from seeing what’s really going on around you. 

When two people who have witnessed the same event are asked to describe what they saw, you'll unquestionably get two different versions of the event.

It's the same with common problems we all face. While one person may see a challenge as life-ending, another may see the situation as an opportunity to change their life for the better.

What you see is not reality; it is just _your_ reality. And in many cases, what you might see may not reveal all the things you need to know.

We all have _blind spots_, areas of our lives that we don't view in enough detail or pay enough attention to.

Luis thought he was a successful businessman and a loving father and husband. Then one day his wife left him, seemingly out of the blue. He couldn't believe it!

The problem was, Luis had blind spots. He only saw success in his life, but didn't realize he wasn't spending enough time with his family, and that he and his wife were drifting apart.

If you want to be personally accountable, you cannot let blind spots hide your problems. But how do you find these blind spots, if you're "blind" to them?

The best way is to constantly seek feedback from others. This is especially important if, like Luis, you think everything's great and you think you have few if any problems. Others may be able to see your situation from a different, often enlightening perspective.

Consider 13-year-old Jessie. She is a soccer player who is unhappy as she's always sitting on the bench during games. Finally one day she asked her coach why she wasn't playing. He told her simply that she wasn't good enough — and this changed everything! Instead of getting angry or sad, Jessie decided to up her game by practicing and building her skills. She soon became a regular player.

By seeking out blind spots through awareness and feedback, we strengthen our vision and are able to "see it," the first step toward personal accountability.

### 5. If you want to fix a problem, you’ve got to “own” it – only then can you improve. 

People who rent their homes are more likely to overlook repairs or things that are broken. Yet people who own their homes are more conscientious about the state of their property.

Does this surprise you? Once again, the reason why lies in accountability.

To be personally accountable for a situation or problem, you have to take _ownership_ of it.

Owning something, whether it be a house, your job or even your health, means you become totally committed to it, and don't act in half measures!

Let's explore some statistics that support this idea. Some 70% of working Americans don't feel engaged at their work. They don't feel personally accountable; instead, they only want the time to pass quickly so they can go home. This means they aren't doing the best job they could do.

If this 70% decided instead to _own_ their job, they would approach it in a radically different way.

With every failure or criticism, they'd hold themselves personally accountable and do whatever they could to improve — working a little later, or even doing more than what's expected.

In this way, they'd perform better and better all the time, soon standing out from their colleagues and having a better chance of receiving promotions or rewards.

Only if you own something can you start to take responsibility for it — an important step toward making real change. Yet there are still two more steps to come!

### 6. Though you may fail at first, you’ve got to persevere to create the change you want. 

You're well on your way to identifying problems in your life — but what use is this if you don't know how to solve those problems?

Unfortunately there's no formula that works like a charm every time. Instead, you'll need to create your _own_ way to overcome obstacles, and this may require perseverance.

Developing your own route to success can be daunting, and you may at first fail. But solving a problem is all about keeping on, even when times are tough.

Remember that each failure is a learning experience, in which you figure out what doesn't work, to come closer to solutions that will.

Perhaps you've seen "Gold Rush" on the _Discovery Channel_, where groups challenge each other to find buried treasure.

To find the gold, each group has to work hard and dig up huge amounts of empty land, often to no avail. The successful teams are those that don't give up.

It's the same with life's obstacles — a lot of attempts that end in failure may be necessary, but in the end, you'll get the gold!

Another great example of the power of perseverance can be found in the story of Heather Dorniden.

Dorniden competed as a junior athlete in a crucial 600-meter track race, and during the race, in front of the crowd, she fell flat on her face. What did she do? She got right back up again, kept running and — would you believe — won the race.

So how can you find this kind of perseverance? One useful method is to think of what you're doing _as if your life depends upon it._ Literally, think that failure would be the end of the world. This attitude will help you bring all your energy into play.

### 7. The final step to achieving personal accountability is to take action and make that change. 

So far you've learned how to recognize obstacles, overcome them and own what you want to achieve. Now, you're finally ready to take action and realize your desired change.

So how can you do this successfully?

One useful method is to convince yourself that there's _no way back._ Thinking this will eliminate failure as an option.

Alexander the Great used this strategy to great success. To motivate his soldiers to fight the Persian army that vastly outnumbered his own, he burnt his troops' ships after landing. There was _literally_ no way back, no place to retreat to — fighting and winning was the only possibility.

Granted, you need not be so drastic, but the same principle applies. So if you want to meet new friends, as one example, go to a place where you hope to meet new people and commit yourself by shutting the door behind you. You will then have no choice but to introduce yourself to others!

Whatever change you decide to make, know that all big changes can be difficult to sustain. You might start quickly and run out of steam, or fail to plan and end up going round in circles.

How can you avoid this?

One simple tool is to ensure you choose the right company. Many "friends" won't encourage you to keep going. What you need are friends that will help you move forward and achieve your goals.

> _"Reasons become excuses as soon as you start using them to stop trying to solve the problem."_

### 8. Put accountability into practice! Make following the four steps part of your everyday life. 

Personal accountability isn't just something you should strive for in one aspect of your life. Instead, it should touch everything, from your family life to work life and friendships.

Make personal accountability a habit. The four steps, or seeing the problem, owning the problem, solving the problem and doing what is needed to change it, should be how you live your life, not simply a one-time lucky moment.

Remember that the world is better _above the line_ — here you have more influence, more enjoyment and more achievement. But you have to invest in this sort of positive thinking, otherwise you may quickly lose yourself in the self-pity blame game.

Once you start thinking and living above the line, you'll see the benefits of accountability blossom in other people's lives, too.

People who choose personal accountability have better relationships with their partners, colleagues and family. With the ability to notice problems previously masked by blind spots, such people can address and resolve issues with partners earlier and maintain healthy relationships.

However, accountability can only go so far. You are not responsible for everything, and you can't change all situations. Remember, personal accountability isn't about changing what's already happened, but about how you _respond_ to the challenges life hands you.

Ben Carson lived a life full of disadvantages. He grew up poor in a problematic area of Detroit, and was raised alone by his mother. While he knew he couldn't change his past, he could take responsibility for himself to create a better future. Carson committed himself to his education, and became one of the leading neurosurgeons in the United States.

If you accept full accountability for your actions, you can create amazing change and build the future that you want.

> _"Once you realize that nothing behind the curtain can help you get what you want in life, you will have discovered The Wisdom of Oz."_

### 9. Final summary 

The key message in this book:

**Even though it may seem that you are a victim of your situation, by changing how you think, you can instead assume personal accountability for your life. This new perspective not only helps you realize what you really want and where your problems lie but also helps you achieve your goals through a four-step path: seeing it, owning it, solving it and doing it.**

Actionable advice:

**Let your frustrations out; then get to work!**

When facing adversity, such as being fired from a job, it's perfectly normal to think below the line, and see yourself as a victim of fate. Give yourself time to express your frustration, and then prepare yourself to think above the line and take charge to create positive change!

**Suggested** **further** **reading:** ** _The Truth About Trust_** **by David DeSteno**

_The Truth About Trust_ not only explores what trust exactly means, but also how it impacts almost every single aspect of our everyday lives. The author's own extensive research and progressive experiments from the fields of psychology, economics and biology reveal the surprising ways in which trust deeply matters.
---

### Roger Connors & Tom Smith

Roger Connors and Tom Smith are the co-founders and CEOs of _Partners in Leadership_, and together they have produced three _The New York Times'_ best-selling books. As experts in workplace accountability, Connors and Smith have organized workshops and advised many leading businesses.

