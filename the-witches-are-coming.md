---
id: 5e21ad216cee0700073abcee
slug: the-witches-are-coming-en
published_date: 2020-01-22T00:00:00.000+00:00
author: Lindy West
title: The Witches Are Coming
subtitle: None
main_color: None
text_color: None
---

# The Witches Are Coming

_None_

**Lindy West**

In _The Witches are Coming_ (2019) writer and feminist activist Lindy West applies her scalpel-sharp intellect to today's political landscape. She uncovers the ideological agendas behind everything from Adam Sandler movies to the wellness movement, abortion rights to Louis C.K.'s comeback. This is feminism for the post #metoo era.

---
### 1. What’s in it for me? Get educated on gender politics. 

The future is female and feminist! But the present is...complicated. In the wake of the #metoo movement, there was certainly a shift in gender power dynamics. But the fight against the patriarchy is far from over. 

These blinks chronicle a misogynist sitting in the Oval Office, a religious right who are working overtime to repeal women's rights, and men claiming that they're marginalized — all because women are being "nasty" to them on Twitter.

There's never been a more confusing — or a more hopeful — time to be a feminist. Tackling everything from politics to pop culture, these blinks lay bare the complicated gender and power dynamics that underpin our everyday lives. 

Read on to find out

  * what Donald Trump and Joan of Arc have in common;

  * what Adam Sandler movies say about contemporary masculinity; and

  * how _South Park_ has inflected the alt-right's discourse.

### 2. We need a reality check on the phrase “witch hunt.” 

What do Joan of Arc, the Sanderson sisters from the movie _Hocus Pocus_, and the 45th president of the United States of America all have in common? They've all — allegedly — been the victims of "witch hunts."

Joan of Arc was tried as a witch and burned at the stake in Rouen, France in 1431. The fictional Sanderson sisters met a similar fate years later in the town of Salem, Massachusetts — the site of the most notorious witch trials in United States history. And Trump is a self-professed victim of a witch hunt. Or, make that plural: witch hunt _s_. Since he's taken office, Trump has tweeted the phrase "witch hunt" over 200 times. Yet despite this, Donald Trump is still alive and well — unlike the victims of real, historical witch hunts.

Before 2016, "witch hunt" generally referred to the witch trials that took place in early modern Europe and America, roughly between 1450 and 1750. These trials usually concluded with the accused being hanged, burned at the stake, or thrown into a lake to drown. The accused witches were overwhelmingly women, their accusers overwhelmingly men.

But recently, the way the phrase "witch hunt" is used has shifted somewhat. In 2006, activist Tarana Burke spearheaded the #metoo movement, calling on women to share their experiences of sexual assault. The movement gained serious traction in 2017, when powerful producer Harvey Weinstein's many sexual assaults came to light. More and more women named their male harassers. And some men started to feel very worried.

Worried about the systemic abuse and harassment women suffered? No. They were worried that these women's accusations amounted to a "witch hunt." But do they?

Here's what these so-called "witch hunts" look like. A prominent man is accused of harassment. The accusation is corroborated. The man experiences public condemnation and is punished — though generally not tried in a court — for his behavior. His punishment entails resigning from his job and fading into the background before reappearing to tape a comedy special or run for office…you know, sort of like getting burned at stake. Except not like that at all.

When men refer to themselves as victims of witch hunts, they're implying that the tables have turned. Women hold all the cards, now. Men are oppressed. Simple as that.

But they couldn't be more wrong, no matter what the Trumps and Weinsteins of the world would have us believe.

### 3. Women have a “patriarchy problem,” not a “likeability problem.” 

What can a series of comedy movies from the 1990s teach us about gender politics? Well, if the movies in question are from the Adam Sandler ouvre…quite a lot, actually.

Billy Madison, Happy Gilmore, Little Nicky, and others might not be cinematic masterpieces. But they are some of our culture's best-loved examples of male mediocrity. Take Billy Madison.

Billy is an adult man who enrolls in elementary school, talks in a baby voice, engages in a series of inane pratfalls, and somehow ends the movie in a romantic relationship with his attractive elementary school teacher anyway. Billy is the epitome of a mediocre male failing upwards.

Now, there's nothing wrong with mediocrity or even failure. But there _is_ something wrong when only one gender's mediocrities are celebrated and only one gender's failures are forgiven. Adam Sandler movies exemplify the benefit of the doubt that society gives to white men.

Let's illustrate the point with another, more extreme, example: Ted Bundy. This American serial killer confessed to the rape and murder of 30 women between 1974 and 1978. Forget mediocrity — Bundy was evil personified.

And yet, society still manages to give him the benefit of the doubt. Bundy has been the subject of countless books, movies, and podcasts, all of which emphasize how his victims were "attracted" to him on account of his "good looks," "charm," and "brilliance." Even Edward D. Cowart, the judge who sentenced Bundy, told him: "You're a bright young man and you would have made a good lawyer." And this was after Bundy was convicted of his crimes.

Still don't believe that our culture bends over backwards to give white men the benefit of the doubt? While society seems to agree that serial murderer and rapist Ted Bundy is "charming," newspapers in 2016 devoted countless column inches to debating whether or not Hillary Clinton — an experienced and accomplished politician — was likeable enough to be electable. Democratic presidential candidate, respected lawyer, and principled activist Elizabeth Warren is now facing the same treatment.

To recap: our society likes serial killer Ted Bundy and think he might have made a good lawyer. But Elizabeth Warren — who is, among other things, a very good lawyer — is difficult to like. There's definitely a problem here. And it's not women's inherent likeability.

### 4. Forget civility. Sometimes we can’t all just get along. 

"Why can't we all just get along?" It's a common refrain in today's sometimes hyper-critical political discourse. Surely, the question implies, things would be easier if we stopped quibbling about pronouns and deleted tweets, and instead agreed to live and let live.

And yet, "Why can't we all just get along?" hides a multitude of sins.

Have you ever heard of Chip and Joanna Gaines? If you've switched on a television recently in North America, odds are that you have. Chip and Joanna are the likeable married duo that, until last year, helmed HGTV's home renovation series _Fixer Upper_. Their own television channel, the Magnolia Network, is coming soon to US screens.

In 2016, Buzzfeed published a report that tarnished the Gaines's teflon image. It criticized the Gaines for their involvement in a megachurch called Antioch Community Church, which preaches that LGBTQ children should be sent to conversion therapy. Conversion therapy is the pseudoscientific practice of trying to turn LGBTQ people straight; it's abusive and traumatic for the people who undergo it. 

Some of the Gaines's supporters were outraged that the Buzzfeed report had revealed this. Why couldn't the left-wing fake-news media just let Chip and Joanna keep their private lives private? _Why can't we all just get along?_

The Gaines themselves responded with a semi-apology that echoed this question with the phrase, "We are not about to get into the nasty business of throwing stones at each other."

And the Gaines are right. Throwing stones at each other is nasty. It's a kind of nastiness some conservatives just won't tolerate. But what about the nastiness they _do_ tolerate? Things like a government that detains migrant children in cages. A climate in which trans black women are murdered at disproportionately high rates and LGBTQ children are — by many accounts — tortured in an effort to repress their sexual and/or gender identities.

Invoking civility to deflect legitimate criticism is a tired trick. We may all live in glass houses. Some of us even live in artfully-renovated-for-television glass houses. But does that really mean we shouldn't throw stones? Looking at the news, it seems there's never been a better time to be uncivil.

### 5. Wellness doesn’t benefit all women equally. 

Goop, the lifestyle brand founded by actress Gwyneth Paltrow, has become notorious for the wellness products it peddles. Just google "jade yoni egg," and you'll soon see why. But Goop's jade eggs and crystal-infused waters are far from New Age fads. Wellness and self-care are concepts that have firmly entered the mainstream. 

These days, there's a whole industry in selling wellness-related products to women, like bespoke juices and organic face masks. The science behind these products may be questionable. But the fads themselves are relatively harmless. Aren't they? 

Actually, the wellness trend neatly illustrates the pressures women face under _patriarchy_, a social system that privileges and empowers men.

Think about it---. The modern man may be targeted with ads to buy beard oils or artisanal colognes, but this pales in comparison to the infrastructure that has sprung up around selling "wellness" to women. That's because, under patriarchy, women are primarily valued for their youth and beauty in a way that men simply aren't. Of course, wellness products are ostensibly sold for their health benefits. And some of these products are genuine health boosters. Their underlying promise, though, is the enhancement of women's most precious asset — their appearance. When you think about it, the price of a rose-quartz face massager isn't so high if it maintains the only real asset you have.

Yet one group of women enjoy the benefits of the trend far more than others — those who are wealthy and white. And wealthy, white women are already doing pretty well. They're sheltered, by virtue of their race and class, from the problems that disproportionately affect other groups of women. Problems like discrimination, poverty, malnutrition, addiction, and incarceration. The fact is, if your most pressing concern is whether a scented candle can balance your chakras, you can probably stop with the self-care and start caring for others, instead.

Self-care is a pretty solipsistic endeavor in a country like America, where not everyone can afford quality healthcare. A juice cleanse isn't really helpful for someone who doesn't have comprehensive health insurance. By looking past structural reform and perpetuating the pretence that candles and crystals are essential to women's wellbeing, women like Paltrow fail to reckon with their own privilege.

### 6. Edgy humor isn’t always subversive. 

For more than 20 years, the foul-mouthed elementary schoolers of a fictional Colorado town have been entertaining television viewers. When _South Park_, the animated series developed by Trey Parker and Matt Stone, premiered in 1997, it was lauded as the last word in cool, subversive humor. Decades later, _South Park_ still exerts a cultural influence — only, it's not a positive one.

When portraying the antics of Cartman and company, _South Park_ makes a point of tackling topical political issues. A trademark of _South Park_ 's satire is its creators insistence that when it comes to politics, everyone — whether they're on the left or the right — is fair game. 

According to Parker and Stone, it's cool to laugh at both sides of politics equally. But this creates a false equivalency, suggesting both sides of politics are equally bad. 

This false equivalency is particularly insidious when, in the United States, one side of politics is currently denying millions of people basic healthcare, telling women what to do with their bodies, campaigning to roll back LGBTQ rights, detaining migrant children…and the list goes on. 

What's more, the show takes its equal-opportunity satire to nihilistic extremes. In the world of _South Park_, issues are intrinsically uncool. The only thing that's cool is using humor to undermine these issues. The edgier the humor, the better. And if you take issue with any of that humor? Well, that's uncool.

Things that are cool, in the world of _South Park_, include a sneering caricature of a disabled child and a token black character named Token. What's uncool is taking offense at any of this. And the idea that being offended is unequivocally uncool, regardless of whether that offense is merited, has caught on. Specifically, it has caught on in certain alt-right corners of the internet, like online chatrooms and forums where users complain about PC culture and the ultimate insult is "snowflake" — someone who's hyper-sensitive and easily takes offense, instead of being cool, edgy, and irreverent.

Here's the thing. When you're irreverent about everything, you believe in nothing. Believing in nothing just means that you tacitly endorse the status quo. And what's so edgy about that?

Forget about killing Kenny. This line of thinking kills the discourse — and it needs to stop.

### 7. Political correctness isn’t killing comedy – male comedians are. 

In the 1990s, there were few shows funnier than _The Simpsons_. These days, we can still appreciate the show for its sharply drawn characters and keen satire. Yet many people find it hard to watch the character of Apu without wincing at the crude South Asian stereotype. In 2018, responding to growing public outcry, _The Simpsons_ creators acknowledged that Apu's characterization was outdated.

Humor changes. Society progresses. And art needs to keep pace with society's evolving standards. It needs to respond to valid critique if it wants to stay relevant. 

Yet some male performers have failed to grasp this concept. Worse still, some promote a progressive image, even while their offstage behavior couldn't be more backwards.

In 2013, there were few male feminist allies more prominent than the comedian Louis C.K. He had just released a comedy special on HBO titled _Oh My God_, in which he spoke frankly about rape culture and acknowledged his male privilege. In one oft-quoted line, C.K. admitted that men were "the number one cause of injury and mayhem to women."

The punchline, of course, was that for many women, C.K. himself was the cause of injury, mayhem, and worse. Behind the scenes, C.K. was a serial sexual harasser. His victims were typically less established, less well-known female comedians . Many were too intimidated to share their experiences. Those who did were dismissed and silenced.

Then #metoo happened, and C.K. was outed. He apologized and stepped back from the spotlight, saying he planned to "take a long time" to reflect on his behavior.

A long time, in C.K.'s world, is apparently about nine months. Less than a year after making his public apology, C.K. popped up onstage in a New York comedy club. His new material had taken a disturbing turn. Despite the fact that he was an acknowledged abuser, C.K. painted himself as a victim of politically correct culture run amok. In doing so, he joined a growing list of male comedians — like Ricky Gervais and Adam Carolla — who portray themselves as marginalized by a hypersensitive, permanently triggered society. 

Post #metoo, many male comedians have gone on the defensive, with truly disturbing results. In his comeback set, C.K. even made fun of the teenage victims of the 2018 Parkland School shooting. Rather than taking valid critiques of his act and his behavior on board, C.K. has merely pivoted to a less critical audience. It's a lazy move — and hardly the stuff of great art.

### 8. We need to normalize abortion. 

A US network once refused to air a teen drama that dealt with the topic of abortion. It sounds like something that might have happened decades ago, but this was in 2004. The show in question was an episode of _Degrassi: The Next Generation_, and PBS pulled it from the air that year. 

Now, PBS didn't decide to pull the episode simply because the topic of abortion came up. Rather, it chose to pull the episode because the character who opted for an abortion didn't express sufficient remorse over her decision.

Abortions, PBS seemed to say, are fine — just so long as the women who get them feel very bad about them. And this attitude is widespread. Liberal politicians are fond of saying that, while they support women's right to abortion, they believe the procedure should be safe, legal, and rare. Even people within the pro-choice movement sometimes share this view. 

All of this adds up to a shameful stigma around what should be a normal, non-shameful medical procedure. And it's detrimental to the pro-choice cause. If we want women to have access to abortions, we need to take a leaf out of the pro-life playbook. Think about it. The pro-lifers settled on an argument and stick to it with conviction. Whether they're on the Senate floor or chanting outside a clinic, they're always on message. Abortion, they contend, is murder. They don't qualify that statement. They don't add caveats or loopholes. If the pro-choice movement wants to compete, they need to excise the caveats and loopholes from their arguments, too. And that means jettisoning the idea that abortions are shameful, or only acceptable in certain circumstances.

So, how do we destigmatise abortion? Just like it did for the #metoo movement, social media offers a platform for women to share their abortion experiences. In 2015, the hashtag #shoutyourabortion went viral. Women around the globe used it to share their stories of abortion publically, countering the idea that they should be shameful and secret.

In the United States, the current administration is chipping away at women's reproductive rights. Abortion clinics around the country are shutting down. And the current President has publically stated his intention to repeal Roe v Wade, the landmark supreme court decision that guarantees women access to abortion. Now more than ever, women need to shout about their abortions as long and as loud as they can.

### 9. Women alone can’t fix the problem that is the patriarchy. 

The patriarchy poses many problems for women. Under patriarchy, women are at much higher risk of sexual violence than men. In the workplace, women are vastly underrepresented at management level. In the home, women still perform far more domestic duties than men do.

How do we fix this? There are lots of so-called solutions floating around, like: 

"If women want to avoid becoming victims of sexual violence, they should carry pepper spray and take care not to wear revealing clothing."

Or, "If women want to get ahead in the workplace, they should negotiate raises and promotions, and stop men from talking over them in meetings."

And of course, "If women want their male partners to take on a bigger domestic role, they should create chore rosters and assign their partner household tasks."

These "solutions" are all hopelessly inadequate because they completely sidestep the people that created the problem: men. Women didn't create the patriarchy — men did. And yet gender inequality is often treated as a women's issue, rather than an issue that affects women. If we want to fix these problems, men need to get involved. So…where are they?

In general, men responded to #metoo in one of two ways. Some complained about witch hunts and doubled down on their misogyny. 

But other men were truly distressed by the revelations the women around them shared. These men wanted to do better. They asked women to guide them and tell them how they could be better feminist allies. This was more constructive, but still problematic. When men ask women to show them how to fix the problems caused by patriarchy, they're casting themselves as bumbling novices and suggesting that women are the experts.

Women aren't experts on patriarchy. Men are. Men created it, and men continue to perpetuate it every day. Here are a few suggestions for how they might dismantle it:

Male employees should refuse to show up to work until the pay gap is closed. Men should strike on International Women's Day. Men should take on an equal domestic load. Men should point out misogyny wherever they see it in the media and on Twitter and in politics and…

Too ambitious? Then, start smaller. Men should back women up, believe women when they share their experiences of sexism, and call on other men to do the same. And if you're not a man, you're not off the hook. Cis people, do this for trans people. Straight people, do this for queer people. White people, do this for people of other races. It's time for each of us to use the privileges we have to break down the systems that oppress everyone.

### 10. Final summary 

The key message in these blinks:

**The #metoo movement was a watershed moment for contemporary feminism, but the fight against the patriarchy is far from over. Women and other marginalized groups are still disadvantaged by oppressive social structures. Men claiming that they've been oppressed by feminism and political correctness distract us from the real fight — working towards a truly fair, equal society.**

Actionable advice:

**Be an ally on social media.**

Social media enabled the message of #metoo to spread. But many women that speak out about their experiences on social media are trolled relentlessly. If you're scrolling through Twitter or Facebook and catch sight of a misogynist comment, don't ignore it. Report it! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _She Said,_** **by Jodi Kantor and Megan Twohey**

Now that you've taken a closer look at gender dynamics in a post #metoo world, why not get inspired and go back to where the movement began? The blinks to _She Said_ by Jodi Kantor and Megan Twohey — the two journalists who broke the Harvey Weinstein story — uncover the shocking stories of sexual assault and abuse of power that brought down some of the world's most powerful men, and sparked a global feminist movement. You understand the problem; to hear about how women around the world have fought back against it, head on over to the blinks to _She Said_.
---

### Lindy West

Lindy West is an American writer, comedian, activist, and — above all — outspoken feminist. As a cultural commentator she's written extensively for outlets like the _New York Times_, the _Atlantic_, and _Gawker_. Her first book, 2016's _Shrill: Notes from a Loud Woman_, was a _New York Times_ bestseller and adapted for television by HBO.

