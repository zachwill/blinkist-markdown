---
id: 57618f7c0beabd0003161b6b
slug: the-wonder-weeks-en
published_date: 2016-06-24T00:00:00.000+00:00
author: Hetty van de Rijt, Ph.D., and Frans Plooij, Ph.D.
title: The Wonder Weeks
subtitle: How to Stimulate the Most Important Developmental Weeks in Your Baby's First 20 Months and Turn these 10 Predictable, Great, Fussy Phases into Magical Leaps Forward
main_color: None
text_color: None
---

# The Wonder Weeks

_How to Stimulate the Most Important Developmental Weeks in Your Baby's First 20 Months and Turn these 10 Predictable, Great, Fussy Phases into Magical Leaps Forward_

**Hetty van de Rijt, Ph.D., and Frans Plooij, Ph.D.**

_The Wonder Weeks_ (1992) is all about the major changes babies go through during their first year and a half of life. These blinks explain the huge developmental steps that every infant makes as they discover their own identity, learn how to move, talk and relate to others.

---
### 1. What’s in it for me? Get a better understanding of the inner life of your baby. 

Chances are that you remember absolutely bubkes from your first year and a half of life. However, you might be present in a hell of a lot of family photos. And in those pictures, your parents were probably lucky enough to have snapped you just as you were smiling.

But surely you didn't smile all of the time. In fact, you're likely to have wailed and turned to your parents with bewildered eyes on more than one occasion. But that's normal. In fact, it's a positive thing!

If you're wondering why, these blinks will tell you. They cover those first rollercoaster 18 months when you underwent so many changes in communicating with and perceiving the world around you.

In these blinks, you'll discover

  * at what age your baby will start flexing her muscles;

  * why you might find your baby shaking a kitten; and

  * why even babies can be sneaky.

### 2. Babies experience regular phases of intense development during which they can be fussy and difficult. 

It's natural for the parents of a newborn to despair when their little one cries, but a crying baby might not be such a bad thing after all. In fact, crying is often simply a sign that an infant is making a developmental leap.

Babies go through regular developmental jumps during their first year and a half, when they experience ten distinct changes in how they perceive and relate to the world.

Most of these changes are neurological, and they involve the rapid growth of the child's brain as new neural pathways are formed. All babies go through these changes around the same time, although the exact timing can vary by a few weeks.

Basically, this means that your baby's brain experiences a growth spurt that moves it one step closer to being an active participant in the world. For instance, when a baby reaches eight weeks, it will learn to recognize shapes and patterns, like light and shadow.

So, when a baby cries, it's often signaling that it's confronted with learning something new. It makes sense then that parents are often shocked by the regular intervals with which their peaceful, sleepy infant bursts into unstoppable wailing. In such instances, the child is simply gaining new skills and interacting with the world in new ways — an exhausting task for such a tiny creature.

For example, when a baby is first born, it usually cries because it's no longer in the warm, safe, liquid environment to which it has grown accustomed. In just the same way, when an older baby all of a sudden finds that it's an individual, separate from the world around it, it will cry again as it adjusts to this shift in reality.

> _"I always noticed that my baby was difficult for a few days before I realized that he was making a leap." (Maribel, young mother)_

### 3. At four to five weeks, babies gain more control over their senses and their experience of the world. 

New parents often stare in astonishment as their newborn opens its eyes for the first time and cracks what looks like a smile. But soon afterward, their baby's smile takes on a whole new dazzling quality. That's because at just four to five weeks babies start to control their senses.

Here's what happens:

Newborns are capable of reacting to a movement or noise, but if you watch them carefully, you'll see that their reactions are rather automatic, almost reflexive. However, around five weeks, that all changes. At this point in your baby's development, you'll notice that your infant exercises more control over its senses.

For instance, when it looks at you, it will be clear that the glance is not an automatic response to a stimulus, but a concerted effort to see you. Similarly, your baby will no longer automatically listen to all sounds, but will pick out the ones it finds most interesting.

So, this newfound control over the senses is enjoyable for babies to pursue, but it also wears them out quickly. In fact, an experiment found that children at this age will make a serious effort to enjoy sensory stimulation, but can only do so for a short time before they get tired.

In the study, babies were given a special pacifier that allowed them to adjust the focus of a color image projected on a screen in front of them. By simply sucking on their pacifiers with more force, the children could make the image sharper, and they all worked hard to achieve this result.

But just to be sure, the experiment was reversed so that the image came into focus when the children sucked more gently — and they quickly adapted to the new system. However, for a baby, watching a screen and sucking on a pacifier is an exhausting endeavor, which meant they could only keep it up for a few seconds at a time.

> _"Now suddenly, she's totally preoccupied by noises of any kind when she is awake. If . . . I sing to her, she stops crying." (Hannah's mom, week 6)_

### 4. At seven or eight weeks, another leap in external and internal sensory perception increases autonomy. 

With so many skills to learn, it's a good thing infants come equipped with an oversized skull that their brain can grow into. In fact, as a baby's brain increases in size, the infant also increases its perception and exploration of the external world.

That's because, around seven to eight weeks, babies start to see differences between their bodies — which they can move and feel — and the rest of the objects and people in the world. For instance, at this age, your little girl might all of a sudden find that her hands belong to her, that she can move them, wave them around and look at them whenever she wants.

And once they discover their hands, babies quickly move on to exploring everything they can do with them. At this age, they love touching and grasping anything within range.

But it's also at this time that babies begin to be more aware of the inside of their bodies. For example, they start to notice the muscles under their skin and the different muscle tensions necessary for particular movements. Simply put, they realize that it's harder to hold their arm up in the air than to let it flop by their side.

Your baby will try out all kinds of positions using every part of her body from her head to her torso to her toes, slowly but surely beginning to flex all her muscles. This experimentation will even include her facial muscles and vocal cords, which explains why your baby might make hilarious faces and ludicrous sounds during this time.

It's also at this time that babies begin to let go of their reflex to grip. Since they can now consciously grasp an object using their newly controlled muscles, they no longer need to do it reflexively.

> _"A baby this age loves to be picked up, caressed and cuddled. You can never give him too much of a good thing."_

### 5. The next big change comes in week 19 when babies learn to sequence movement and sound. 

Have you ever been horrified to see a small child catch a kitten and manhandle it? Well, the root of this behavior lies in the next phase of a child's development. That's because at 19 weeks babies learn movement sequences.

You'll see that babies of this age are capable of both executing fluid motion and repeating it several times. That's a big deal for them, so they do it _constantly_.

For instance, you might see your child of 19 weeks pick up its toys, or that kitten, and experiment by shaking them around. But that's just the beginning of the ordeal for your baby's object of choice. As you've no doubt witnessed, babies also love to press things down and bang them against other objects repeatedly.

So, this is a good age to ensure your baby has plenty of sturdy, indestructible toys. Your kitten will thank you for it.

It's also at this stage that babies begin to utter a sequence of sounds. In fact, up until this point, your infant was probably more or less monosyllabic, and she's about to discover how to switch between vowels and consonants to form a series of sounds.

It's during this phase that the famous baby talk of "baba zaza abba tu" has its heyday, but words like "mama," "dada," or variations on them might pop up, too. The interesting thing about this baby talk is that it's universal. That means that children all over the world start with the same standard babble that slowly gives way to the specific language of their region.

And this process tends to begin pretty early because babies are praised and encouraged when they make sounds that are important to their native tongue.

### 6. Around week 26, babies start to understand how relationships work. 

Do you ever feel like your baby is regressing instead of developing? Maybe he suddenly grows very shy and afraid, whereas he used to approach any situation fearlessly. Well, don't worry about it. It's just because he's beginning to notice new overwhelming and frightening things, like human relationships.

First and most important is the relationship with their main caretaker, or how physically close Mom, Dad or Gramps is to the baby. That's because during a baby's 26th week, his awareness of the distance between people and objects increases dramatically. As a result, he begins to care a whole lot more about the proximity of his caretaker.

For instance, it's common for babies of this age to no longer want to be in their playpen when it's out of sight of their parents. That means they'll often be much more content lying on the floor near Mom or Dad where they just need to twist around to make sure that their caretaker is close by.

Then, as this relational awareness grows, babies start exploring how objects, events and people relate to one another. This is the phase when babies start playing with objects in space, experimenting with the ways in which they correspond.

For example, they might spend hours piling objects on top of each other, placing them side by side, or pulling things out of a cupboard only to put them straight back in. Thankfully, this makes babies of this age very easy to entertain.

But it's also during this phase that babies start noticing how sequences of events between people and objects are connected. For instance, when a key turns in the front door, your baby will know that it means someone is coming home, or they'll know that a noise in the kitchen means food is on the way.

> _"He clears cupboards and shelves and is thrilled by pouring water from bottles and containers into the tub." (Matt's mom, 30th week)_

### 7. At week 37, babies learn to recognize categories of things and give them names. 

Have you ever seen a kid running around pointing at strangers and screaming "That's a man!" or "That's a woman!"? Well, this basic ability to simplify and group begins around week 37. As babies experience their next cognitive leap they start to recognize categories.

At this point, babies will understand the meaning of words as abstract concepts that name similar things. For instance, they will recognize a horse as such, regardless of the color of its coat. In fact, they'll even understand that a photograph or painting of a horse belongs to the same general category as a living animal.

But for your baby to develop these categories she needs to spend a long time carefully observing the world and, for people who aren't used to children, this newfound inquisitive gaze can be disconcerting. So just remember, your baby isn't being rude, she's just trying to make sense of all the different things in her environment.

Later, children begin giving new names to qualities and categories that they see around them. This ability comes once your baby can speak more fluently, but many children will come up with unique names for things they observe.

For instance, your baby might dub the garage "car house" or, more poetically, name a fern "feather flower." At the same time, babies will learn how to tell objects apart by assigning them different qualities like soft, hard, tall, small, sweet or salty. At this point, since your baby is increasingly beginning to see and describe the world as an adult would, it will become a lot easier for you to interact with her.

### 8. At week 64, babies find they can assert their will and make strategic choices. 

Do you remember that fateful day when your baby decided it was time to throw its first ever tantrum? Well, this horrific scenario is all thanks to week 64, the phase during which your baby develops autonomy and willpower.

As babies start to think more abstractly, they find that they can strategically choose between different options. It's at this point that babies start to develop their own action plans and strategies.

For instance, they might begin thinking about such questions as "Who's the most likely to give me candy?" Up until this point, if a baby's parents refused them a sugary treat that would be the end of the matter, but now a child might turn to its grandparents to see if the older generation will be more sympathetic!

At this stage, parents often see their babies being extremely indecisive and contemplative as they tease out the confusing array of options. That means a decision to watch TV might take as long as 30 minutes to make.

But as babies start to find out that they can make choices, they both assert their will more powerfully and begin learning about boundaries. As a result, babies of this age will grow much more possessive. They might no longer be willing to share their toys because they've discovered that monopolizing them is an option.

And if the toys are forcefully pried away? The result will likely be a temper tantrum.

As a result, this is a natural time for babies to explore boundaries. Your baby might try to spill its milk on the table or throw food against the wall just to see what happens. That means it's important to give babies of this age clear signs about what is acceptable and what isn't.

> _"He now realizes that the whole day through he has to make all kinds of choices. He chooses very consciously and takes his time." (Luke's mom, 67th week)_

### 9. All these upheavals are stressful for babies and parents, but remember, it’s all normal. 

Everyone expects a teething baby to be fussy and easily accepts the wailing that results. But your baby can find it equally challenging and difficult to experience the growth of its brain.

That means it's essential to be understanding and supportive of your baby as it works through this major change. For instance, when a baby sees his experience of the world suddenly expand, he might become scared and clingy. In fact, parents often describe how toddlers going through such changes can be quite a handful, hanging on to their clothes and desiring constant bodily contact.

So, it's key to be patient and supportive as your baby explores his new, developing skills. To make it fun, for each major change he experiences, you can come up with appropriate games to help him along. For instance, when your baby is discovering categories around week 37, he'll love games that let him compare and categorize different shapes.

However, it's equally important for caretakers to have patience and kindness for themselves during these stressful phases of development. After all, it's easy for parents to grow frustrated and be provoked by a clingy, whining baby. To make matters worse, babies also tend to sleep poorly during their developmental phases.

But just remember, these phases are normal. So, while you might not be able to avoid all the crying and fussing, you can at least be relieved that you're not doing something wrong. All babies go through these difficult periods.

### 10. Final summary 

The key message in this book:

**The first 18 months of a baby's life are a roller coaster of intense neurological changes that occur in sequence and according to a universal pattern. Understanding what your infant is going through will help you stay calm and supportive during these transitional phases.**

Actionable advice:

**Keep an eye on your baby around week 26, when his mobility increases dramatically.**

When you find yourself approaching week 26 of your baby's life, it's a good idea to make sure your house is sufficiently baby-proofed. That's because babies of this age love to explore and don't realize how dangerous some objects and situations can be. It's essential to be mindful of any electrical appliances or outlets as well as drains, pets and anything that's left lying around on the floor.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Children Succeed_** **by Paul Tough**

These blinks explore the reasons why some people struggle in school and later on in life, and why others thrive and prosper. Using scientific studies and data from real schools, the blinks dive into the hidden factors that affect the success of children.
---

### Hetty van de Rijt, Ph.D., and Frans Plooij, Ph.D.

After studying psychology, wife and husband team Hetty van de Rijt, PhD, and Frans Plooij, PhD, became deeply interested in infant development. In the 1970s, they studied young chimpanzees with Jane Goodall in Tanzania, before turning their attention to human babies for the next 20 years.

