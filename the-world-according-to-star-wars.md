---
id: 581e6bad7882c20003e52cc9
slug: the-world-according-to-star-wars-en
published_date: 2016-11-10T00:00:00.000+00:00
author: Cass R. Sunstein
title: The World According to Star Wars
subtitle: None
main_color: 78A6B3
text_color: 445F66
---

# The World According to Star Wars

_None_

**Cass R. Sunstein**

_The World According to Star Wars_ (2016) reveals the many life lessons to be learned from George Lucas's _Star Wars_ films. Discover what popular science fiction can tell us about ourselves, what _Star Wars_ has to say about the politics of popularity and how we interpret movies and inject our favorite stories with our own ideas.

---
### 1. What’s in it for me? Understand the world through Star Wars. 

Whether or not you've watched the _Star Wars_ movies, you've almost certainly heard of them, and you're probably even familiar with some of the franchise's characters and artifacts: the tiny Jedi master Yoda and his inverted syntax; lightsabers with their swooshing noise; the evil Darth Vader with his heavy breathing and famous line, "Luke, I am your father." These are just a few of the most popular references. There are many, many more — some serving as set-ups for jokes and others ready to be quoted by committed fans.

But it's all just fiction, right? And what can fiction really tell us about the world we live in?

Well, as it turns out, quite a lot. So, let's see exactly what we can learn by taking a trip in time and space — by traveling back a long, long time ago, in a galaxy far, far away.

In these blinks, you'll find out

  * why people needed a world like _Star Wars_ to escape to in the late 1970s;

  * how _Star Wars_ can be seen as an analogy for world religions; and

  * why freedom of choice is the main takeaway from the _Star Wars_ movies.

### 2. Exploring how Star Wars was created debunks a myth about the creative process. 

_Star Wars_ has become so influential and iconic — despite a science fiction world so full of classic characters and famous lines — that it sometimes seems like it's been around forever.

But there was once a time when things like Death Stars and Wookiees were but vague ideas in the mind of its creator, George Lucas.

Indeed, these ideas were so unclear back in the early 1970s when Lucas first talked about making a _Star Wars_ movie, that he only had a basic premise: the heroes would be aliens and the villains would be humans.

While that's an interesting idea to start with, it bears little resemblance to the movie we know today and the all-human trio of heroic main characters: Luke, Leia and Han Solo.

Like many films, the first _Star Wars_ movie, _A New Hope_, went through multiple drafts before it got to the screen. Lucas ended up writing four different drafts before he arrived at the version that went on to become the movie that's known and loved today.

And that was just one movie. It certainly wasn't a detailed storyline that would extend over two trilogies with a third still on its way to completion. 

Even the iconic plot twists that came in later movies, such as Darth Vader turning out to be Luke's father or Luke and Leia being twins, weren't there from the beginning.

Many years later, when talking to the writers of the TV show _Lost_, Lucas confided that he didn't know where things were headed when the first movie came out.

Indeed, those revelations that seem so integral to the mythology of _Star Wars_ were all the result of brainstorming sessions between Lucas and his writers.

It's a common misconception that famous creators like Lucas give birth to worlds that are fully formed and planned out in advance; we can call this _the myth of creative foresight_.

But that's not how creativity really works. The creative process is about being open and uncertain, and latching on to ideas that emerge along the way.

> Adjusted for inflation, the movie Star Wars: Episode IV — A New Hope has made approximately $1.55 billion; that's $700 million more than the GDP of Samoa.

### 3. The popularity of Star Wars is a matter of timing and the desire to be part of the phenomenon. 

Given how immensely popular _Star Wars_ turned out to be, you might think that the creators knew they had a hit on their hands. But this would be another misconception. In reality, most of the people involved thought it was going to flop.

However, the first _Star Wars_ movie had two huge factors working in its favor: timing and luck.

In 1977, the mood in the United States was pretty gloomy: People were still recovering from the assassinations of Malcolm X and Martin Luther King, Jr., and of two Kennedys. On top of that, the economy was in a recession, terrorism was on the rise and the Cold War continued to cause tension.

What audiences needed was a big, uplifting work of art that would bring people together in a spirit of camaraderie.

And that's just what _Star Wars_ did. The gigantic space ships, comical droids and lightsaber duels let people forget about their problems and have a bit of fun.

What's more, the heroes and villains were clear and well defined, so, unlike the current political climate, everyone could happily agree about who the good guys and bad guys were.

_Star Wars_ appealed to just about everyone, and this was a key to its lasting success.

There is a human impulse to like what other people like, since such consensus allows us to feel connected to others and enjoy a shared experience.

This can lead to what's called a _cascade of popularity_ — when something starts off being extremely popular and grows even more popular because people want to be part of the popularity.

It can also be called the _network effect_ : the more people use something, the more valuable it becomes. Facebook is a prime example; the main reason people use Facebook is because tons of other people use Facebook.

_Star Wars_ continues to have a cascade of popularity today: When the first trailer for _Star Wars: The Force Awakens_ was released, it broke records by being watched 88 million times in 24 hours. Clearly, people still wanted to be part of the phenomenon.

> _"I was going to see_ The Force Awakens _because all my friends were going to see it, and everyone else's friends were going to see it, too."_ \- Ann Friedman, magazine editor

### 4. Star Wars continues to fascinate people by offering numerous ways of being interpreted. 

When a movie gets watched as many times as the _Star War_ s films have, it can lead to a variety of different perspectives. And these different interpretations can even lead _you_ to wanting to watch the films again.

Some people interpret _Star Wars_ as a Christian allegory about love, sacrifice and redemption.

First of all, one of the central characters is very Christ-like: Anakin Skywalker was born to a virginal mother, and, by eventually becoming Darth Vader, he embodies and transcends the sins of others before dying a symbolic death to save his children.

Further, the Dark Side of the Force can be seen as Satan's apple. It tempts an innocent Anakin with the promise of immortality and the ability to save his loved ones.

Or, looked at another way, _Star Wars_ is about Buddhism.

After all, Yoda does look rather Buddha-like, and his teachings about how to use the Force in order to become a Jedi emphasize detachment and mindfulness in fashion similar to Buddhist teachings.

For example, Yoda tells Luke: "Fear leads to anger. Anger leads to hate. Hate leads to suffering."

Now, compare that to the words of Buddha, who said, "There is suffering. There is a cause of suffering. There can be an end to suffering. The eightfold path brings about the end of suffering."

In fact, there's even a book on this topic called _The Dharma of Star Wars_, written by Matthew Bortolin, a student of the renowned Buddhist teacher Thich Nhat Hanh.

There are also more pessimistic takes on _Star Wars_.

One states that it's all about order and chaos. This interpretation flips the traditional view of the good Jedis versus the bad Empire by asking a provocative question: Maybe the galaxy was better off under emperor Palpatine?

If the Emperor is viewed as a relatively benevolent dictator, then it's the Jedis and the Rebel Alliance that are causing all the trouble and violence.

### 5. Star Wars also fascinates people by reflecting the real-world nature of politics and rebellions. 

_Star Wars_ also fascinates because it offers insight into real-world scenarios.

For example, the opening text of _Star Wars: Episode II — Attack of the Clones_ tells us that the Galactic Senate is in turmoil as thousands of solar systems are threatening to leave the Republic; as a result, the Jedi are struggling to keep things peaceful.

Once again, we see the _cascade effect_ — only this time, rather than a cascade of popularity, it's a cascade of rebellion. 

In _Attack of the Clones,_ the solar systems in the galaxy aren't acting independently of one another within the senate. Rather, once a few of them leave the Republic, others follow, which then leads to more and more leaving until a cascade effect has led to a full-on political secession.

This causes the Jedi to become overwhelmed, which creates a general climate of rebellion, which in turn creates even more unrest.

The cascade effect is just one of the ways the _Star Wars_ universe reflects our own world.

Take, for example, the 2008 presidential election. During his campaign, Barack Obama benefitted from a cascade of popularity that attracted more and more donors and voters.

In contrast, you could look at the more recent 2015 campaign of Republican candidate Scott Walker.

Once considered a front-runner for the Republican Party, Walker suffered after being labeled a loser. Remarkably, the idea soon spread and people started to give him less money because other donors were giving him less money. Before long, it became impossible to raise funds and his campaign crumbled.

The cascade effect also shows how important primary elections are to US politics; it's the time when, for better or for worse, a candidate's cascade begins.

### 6. In the end, the biggest lesson from Star Wars is freedom of choice. 

While _Star War_ s can be read many different ways, there's no denying that it was made by a guy from the United States — a country known as "the land of the free and the home of the brave."

It's also clear that George Lucas created _Star Wars_ in the 1970s, at a time when the country was dealing with the Cold War and the aftermath of the Vietnam war and President Nixon's Watergate scandal.

Take all these factors into account, and it becomes clear that _Star Wars_ is about the price we pay for freedom.

Many Americans in the 1970s saw the Soviet Union and its satellite countries in Eastern Europe as a kind of evil empire; the United States, on the other hand, was, for them, a symbol of freedom.

But with the horrors of Vietnam and President Nixon's resignation in 1974 after the Watergate scandal, Americans knew that there was also a darker side to their institutions.

It became clear that there was a delicate balance between the "forces" of democracy and liberty. George Lucas, like other members of his generation, had witnessed firsthand what happens when the Dark Side wins.

And so, though _Star Wars_ celebrates political freedom, it also acknowledges that there's good and bad within us all.

Nor is the main struggle for political freedom alone; the characters in _Star Wars_ also face some tough personal choices.

Some of them have to choose to leave home and, instead of helping their loved ones with the family business, go out into the universe and fight the good fight.

Often the characters must choose one of two paths: the easy way out or the more difficult but rewarding way. And this freedom of choice often comes at a very high price.

For instance, young Anakin Skywalker decides to leave his mother in order to train as a Jedi. But not only does he never see his mother alive again, he has to kill his master and mentor in order to save the galaxy!

> _"Everybody has the choice of being a hero or not being a hero every day of their lives."_ \- George Lucas

### 7. Final summary 

The key message in this book:

**No one has their life or creative projects figured out in advance. Everyone is playing it mostly by ear. Once you create something and put it out into the world, the response it will have and how people interpret that work are no longer in your hands. If something is massively popular, it's often because an initial wave of popularity fueled it to grow into something everyone wants to be part of. While this effect can also apply to politics — in both good and bad ways –** ** _Star Wars_** **ultimately reminds us that the great beauty of life lies in our freedom of choice.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Nudge_** **by Richard H. Thaler & Cass R. Sunstein**

The message of _Nudge_ is to show us how we can be encouraged, with just a slight nudge or two, to make better decisions. The book starts by explaining the reasons for wrong decisions we make in everyday life.
---

### Cass R. Sunstein

Cass R. Sunstein is a professor at Harvard University and founder of the school's Program on Behavioral Economics and Public Policy. He has worked for the White House Office of Information and Regulatory Affairs and as a member of the President's Review Group on Intelligence and Communications Technologies. His other books include _Nudge: Improving Decisions about Health, Wealth, and Happiness_ and _Simpler: The Future of Government_.

