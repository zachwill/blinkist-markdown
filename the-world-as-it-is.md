---
id: 5b2fdc1ab238e10006f8a85c
slug: the-world-as-it-is-en
published_date: 2018-06-26T00:00:00.000+00:00
author: Ben Rhodes
title: The World as It Is
subtitle: A Memoir of the Obama White House
main_color: 5E5880
text_color: 3A3266
---

# The World as It Is

_A Memoir of the Obama White House_

**Ben Rhodes**

_The World as It Is_ (2018) is a deeply personal look at the Obama presidency, written by a man who not only worked closely with the forty-fourth president, but also became his friend. Taking us on a behind-the-scenes tour of Obama's presidency, from his first campaign to Trump's inauguration, these blinks also chronicle the author's personal journey from fresh-faced staffer to hardened national security operator.

---
### 1. What’s in it for me? Get a behind-the-scenes look at Obama’s White House. 

Ben Rhodes was beside Barack Obama through it all, from the 2008 election campaign to the inauguration of Donald Trump, and he filled many roles: speechwriter, national security advisor, all-purpose aide — and friend. These blinks tell the story of Obama's Presidency from Rhodes's perspective.

From building support for the Iran nuclear deal to conducting secret negotiations with the Cuban government, Rhodes explains how the Obama administration tried to use foreign policy to do good in the world, despite stumbling blocks such as the hyper-partisanship of US politics and the sometimes stubborn and conservative administration in Washington.

Rhodes also explores some of the difficulties faced by a US government up against Russian-funded disinformation campaigns, and a political culture less and less rooted in respect for facts and the truth.

In the blinks that follow, we'll look at some of the highlights and lowlights of working in the Obama White House.

Along the way, you'll discover

  * how Obama came close to quoting Adolf Hitler in a speech to a vast audience in Berlin;

  * why Obama's upbringing gave him a nuanced view of American power; and

  * how Obama felt about his meeting with president-elect Donald Trump.

### 2. Obama did things differently from other presidential candidates, offering change and a fresh approach. 

When Barack Obama first ran for the presidency, in 2007, he represented something new. For starters, he had opposed the Iraq war when almost everyone else supported it. To many, he seemed like a beacon of hope. He used words that sounded moral, and authentic, at a time when Washington politics seemed anything but.

Obama fought his campaign with a promise of change. From a foreign-policy perspective, that meant doing things that went against the grain of establishment thinking.

For instance, Obama called for diplomacy with Iran without preconditions. The Washington political class saw this — or anything that deviated from instinctive "toughness" toward Iran — as a blunder, despite the fact that Iran was quietly progressing its nuclear program. But Obama doubled down, responding to criticisms of his foreign policy by saying, in a nod to the disastrous Iraq war, that he wouldn't be lectured by people who had supported the greatest foreign-policy mistake of his lifetime.

In most presidential campaigns, foreign policy is a minor issue, with few votes in it beyond veterans and key ethnic constituencies. But Obama's campaign wanted to do more. Conscious of the higher expectations placed on an African-American candidate, Obama wanted to prove his ability to handle international diplomacy and the demands of being commander-in-chief. To do this, he embarked on a campaign tour of crucial European nations and the Middle East — an unusually long detour from the standard presidential campaign trail.

A key piece of the tour would be a speech in Berlin, delivered on the site of two iconic speeches from American presidential history — Kennedy's famous "Ich bin ein Berliner!" speech and Reagan's speech, "Tear down this wall."

The speech almost ended in disaster, however. Just hours before Obama spoke, the author pondered a crucial line in the speech's ending that referred to the German word for "community of fate,"_Schicksalsgemeinschaft_. Double-checking with a translator, he discovered that the word had been the title of a well-known speech by Adolf Hitler!

The line was changed, just in time. And the speech — given to an enormous, cheering crowd — was a huge success. More than his actual words, which emphasized globalism over nationalism, the image of an African-American candidate addressing vast crowds on a historic stage was a powerful one.

### 3. Obama’s worldview caused tensions at home, including in his own administration. 

Few presidents have had their background and outlook analyzed, and misrepresented, as much as Obama.

Obama's worldview and background differed greatly from those of former presidents, and from those of the people — typically white men — serving in senior national security posts.

Take his upbringing. He was born in a former US colony, Hawaii, which had a diverse population and served as a link between the United States and East Asia. He also lived in Indonesia in the late 1960s, shortly after a US-sponsored coup that led to violence and hundreds of thousands of deaths — events that were barely mentioned in the United States, but were extremely formative in the region. Obama's grandfather served during World War II and his great-uncle served in the forces that liberated the Buchenwald concentration camp in Germany. As a result of these influences, Obama grew up with a view of America — of its power and position in the world — that was more complex than that of most Americans.

Obama's nuanced view of the United States sometimes generated criticism at home. On his first foreign tour as president, he addressed the Turkish parliament. Together with the author, he had discussed in advance how to tackle the controversial issue of Turkish treatment of minority groups. Obama decided to reference it through the prism of America's treatment of minority groups such as Native Americans and African Americans. In his speech, he talked of how, not long ago, his own country made it hard for someone like him to vote, let alone become president.

For Obama, this was an expression of patriotism. In his view, the capacity for improvement is what makes America exceptional. But the speech led to criticism at home — that Obama wouldn't embrace American exceptionalism, that he wasn't patriotic and that he was perhaps a Muslim. Among his critics, the foreign trip became known as the Obama Apology Tour.

Obama's view of the world soon caused tensions within his own administration as well.

When Obama wanted to close Guantanamo Bay, the author was called in to write a speech on the subject. At Obama's request, a draft stated that the Muslim detainees in the camp had been in a "legal black hole" for years. None, after all, had ever been convicted of any crime. But the national security advisers who reviewed the speech took issue with this language and wanted instead to say that the detainees had received more legal representation and protection than any other enemy combatants in history.

### 4. Early in his presidency, Obama made a conscious effort to talk directly to the Muslim world. 

Mutual suspicion had long characterized the relationship between the United States and the Muslim world. In 2009, in a speech at Cairo University, Barack Obama tried to transform that mutual suspicion into mutual respect.

As he prepared for the speech, tensions were high. That was particularly the case for the Israelis, who had lobbied hard over its content. They feared Obama would identify the Israel-Palestine conflict as the root of all ills in the Middle East. In response to this pressure, Obama decided not to visit Israel after Cairo; he wanted to avoid any perception that the conflict was at the heart of the speech. Ironically, he would be criticized for years by supporters of the right-wing Israeli president, Benjamin Netanyahu, for this decision, despite its being made in their interest.

What came to be known as the Cairo speech clearly signaled Obama's desire to reset relations between America and the Muslim world.

Obama used it to argue that the West needed to reeducate itself about Islam's contributions to the world, while the Muslim world should recognize the universal principles and rights of the West. In preparation with the author, he reminisced about his time as a child in Indonesia, when girls swam outside freely, never covering their hair. That, he said, was before the Saudis started funding madrassas, or religious schools, and a less liberal Islam took hold. In the speech, he would make a bold case for women's rights, religious tolerance and government by the rule of law.

By the time the speech was delivered, expectations on all sides were high. Obama greeted his audience with "Assalamu alaikum" — peace to you — and the room burst into cheers and applause. The speech was punctuated by applause throughout. Religious leaders cheered Obama's defense of American Muslims' right to wear the hijab. As he invoked democracy, activists shouted, "We love you!"

The speech was a pure expression of Obama's outlook on the world. Hopeful, optimistic, rooted in universal values, it envisaged the world as it should be. The tale of Obama's foreign policy would be one of constant struggle to live up to this vision.

### 5. For Obama and his young advisors, the Arab Spring was an opportunity to be a force for good in the world. 

On December 17, 2010, a story — one of thousands he received every day — popped up on the author's BlackBerry. A fruit seller in Tunisia, Mohamed Bouazizi, frustrated by the corruption of his government, had set himself on fire. The act had sparked a blaze of protests in the region.

Neither the author nor anyone else knew it at the time, but the Arab Spring had begun.

Weeks later, the protests had spread like wildfire to Egypt. The images coming out of Egypt were dramatic. The author watched on TV as the same security forces that had guarded their visit for the Cairo speech suppressed thousands of young men gathering in Tahrir Square.

Obama's administration was divided over how to respond to the protests. Younger staffers like the author felt that this was an opportunity to support the positive vision outlined in the Cairo speech, against a backdrop of clearly repressive behavior by the Egyptian government. Defending the government felt impossible. As another communications advisor put it, while watching machete-carrying troops clear Tahrir Square, "How the f*ck am I supposed to call that restraint?"

By contrast, others — Hillary Clinton among them — were much more conservative, arguing that Hosni Mubarak's government was stable, that protests would dissipate and that a dialogue between government and people in Egypt could be promoted. They were supported by the likes of King Abdullah of Saudi Arabia, who cautioned Obama against supporting the Cairo protesters.

The author felt this division when he drafted a statement that discussed the rights of the protestors and the need for government to pursue a "path of political change." When it came back after review from other advisors, every word mentioning human rights and the plight of the protestors had been removed. Someone had scribbled "balance" in the margin.

In private, Obama told friends that his sympathies were with the people and, in the end, he delivered the author's original draft.

Eventually, Obama decided that Mubarak's attempts at reconciliation with his people were inadequate. The author listened as Obama called Mubarak and told him that the time had come for a new government.

But Mubarak held on — for a while. Older hands in the Washington establishment criticized Obama's stance, saying his young advisors had led him to betray an old ally of the United States. But day after day, protests continued. Eventually, Mubarak fled Cairo and resigned.

This would not be the last time that Obama's young advisors disagreed with the old guard in Washington.

### 6. Obama delivered on one of his earliest foreign-policy promises. 

In 2011, in a secluded compound in Abbottabad, Pakistan, a tall man paced up and down in a courtyard. Unbeknownst to him, he was being watched by US intelligence analysts. As they were not yet sure of his identity, they called him "the pacer."

His true name? Osama bin Laden.

One of Obama's first foreign-affairs promises had been to hunt down bin Laden — and now, possibly, he could deliver on it. Officials proposed a mission to capture or kill the pacer, or to take out the compound completely. But it was a high-risk situation. No one knew for certain whether the pacer really was bin Laden, with analysts putting it at a 40-60 percent chance that it was. The repercussions of a failed raid based on faulty intelligence could be severe.

Obama didn't take the decision to go ahead lightly. In the last of what seemed like endless meetings, Obama quizzed his advisors and demonstrated a deep knowledge of the intelligence — how tall the residents of the compound were, how many people lived there, that they burned their trash rather than taking it out.

Days later, Obama gave the go-ahead for the raid. He gathered his advisors in the situation room. The president's photographer was there at the author's request, to capture the scene, whatever happened. While the seal team helicopters flew from Afghanistan for the flight to Abbottabad, Obama retreated to the Oval Office to play cards, his way of killing time.

On May 1, watching the raid was a tense experience, especially when one of the helicopters grazed a compound wall and had to crash land. But then the phrase "Geronimo EKIA" rang out. This was the code for "bin Laden, Enemy Killed In Action." "We got him," Obama said.

Obama made one final decision — not to release photographs from the operation. The author and advisors flipped through the collection of photos taken: bin Laden's bloody corpse; preparations for an Islamic burial, taken from a US ship in the Arabian sea; a final shot of the corpse slipping beneath the water. Obama was firm in his decision not to release them, saying that he didn't need to put them out there as trophies.

As Obama later said in his address to the nation, justice had been done.

> _"...for all the pain and polarization of the last decade, we stuck with it, and we got bin Laden."_

### 7. Normalizing relations with Cuba was, for once, an opportunity for the author and Obama to be proactive, not reactive. 

During his time in Obama's administration, the author had to mostly deal reactively with the messy reality of the world, from handling the legacy of George W. Bush's wars in Iraq and Afghanistan to addressing emerging threats, such as Syria. He had little time to help proactively shape the world according to Obama's foreign-policy goals and ideals.

So Obama's efforts to normalize relations with Cuba were a welcome opportunity to engage in an affirmative agenda.

In 2013, the author started a series of secret meetings with Alejandro Castro, son of the leader Raúl Castro. Meeting in a remote, lakeside Canadian home, chosen for the sake of neutrality and discretion, the author and Alejandro Castro developed a constructive dialogue and relationship.

After their second meeting, there was a sign that the relationship was working. Edward Snowden, the leaker of US intelligence, was holed up in Moscow Airport, apparently hoping to fly to Venezuela, via Havana. Rhodes told Castro that any assistance for Snowden would make it impossible for Obama to continue working to normalize relations with Cuba. No more was ever said. But days later, the author woke up to reports that Snowden was still stuck in Moscow, as Havana wouldn't let him fly to Cuba. The Cubans, it turned out, were serious about improving their relationship with the United States.

With the help of Pope Francis, well-liked in Cuba for his South American heritage, an agreement to restore diplomatic relations between the two countries was reached. 

The day before the deal was announced, Obama, joined by the author and other advisors, made the first call to a Cuban leader since the revolution. When Raul Castro spoke, he spent half an hour listing American efforts to sabotage his government over the years. The author passed a note to Obama, saying he could interrupt Castro, but Obama shook his head and said that it had been a long while since Cuba had communicated with an American president and that Castro had a lot to say.

### 8. Race was a constant presence in the Obama White House, but not an easy topic for Obama to deal with. 

For many Americans, Obama's election represented a breakthrough in America's attitude toward race. But while racism would never be far from the surface during his presidency, Obama and his team rarely addressed it directly.

The author regularly received intolerant messages on social media. In private, Obama sometimes displayed dark humor about the subject. Rehearsing for media Q&As, he'd sometimes give frank answers: Q: "Why do you think you have been unable to bring the US together?" A: "Well, maybe because my being president has driven some white people insane." But in public, if asked whether racism drove the levels of opposition to his presidency, he'd say no, attributing it to other factors.

On one occasion, however, Obama's true feelings came through.

On June 17, 2015, a white supremacist called Dylan Roof entered a black church in South Carolina and killed nine people. In one day, Roof murdered more Americans than ISIS had in the last decade. Obama privately lamented that he was lost for words and said that he should maybe go to the memorial service but not say anything.

The night before the memorial, Obama stayed up late, rewriting his speech. He wanted to address racial taboos head-on — the historical racism of the confederate flag, and the present day racism of the criminal-justice system. And he wanted to root his speech in the concept of grace.

As he addressed the congregation, Obama fell into the rhythmic style often used in many black churches. He talked of the dignity of the victims, and the grace they showed in their lives. If we can tap into that grace, he said, everything could change. In an imperfect voice, he started to sing the hymn "Amazing Grace." The congregation, filled with emotion, soon joined him.

Days later, the author found Obama sitting in the Oval Office, reading a letter. "Dear Mr. President," he read aloud. The writer said that for his whole life, he had hated people based on their skin color, but since the nine people were shot, he had been thinking things over, and realized now that he had been wrong.

There was silence in the Oval Office. It's a shame that people needed to die for that to happen, Obama said.

### 9. Obama didn’t take military action in Syria, despite his “red lines” over the use of chemical weapons. 

During Obama's time in office, the Syrian civil war emerged as one of the world's most difficult and bloody conflicts. By 2012, the White House was concerned enough about the possible use of chemical weapons in Syria that Obama made a clear statement — any signs of the use of chemical weapons would be a "red line." Cross it, and the United States could act.

So when reports and images of a deadly gas attack outside Damascus emerged in August 2013, there were expectations that Obama would launch military action. But he didn't.

The international community did little to help Obama. On a call with Angela Merkel, the German chancellor and the president's most admired ally, Obama asked for her support for military action. But Merkel argued in favor of a UN security council resolution, despite the likelihood of the Russians blocking it. The diplomatic route would take weeks, and Obama knew that as the horror of the gas attack faded in public memory, opposition to US action would grow.

Weak support internationally was compounded by weak support in Washington.

A group of Republican Congress members wrote to Obama, setting out a blunt challenge to his authority to take action. Engaging in Syria, in the absence of a direct threat to the United States, and without congressional approval, they wrote, would be a violation of the separation of powers set out in the Constitution.

During his candidacy in 2007, Obama had supported requiring congressional approval before going to war. And he decided that was the right thing to do in Syria's case. But winning support was hard. Senate Republican Leader Mitch McConnell refused to support action, but would later criticize Obama for not launching strikes. Congressional leader John Boehner said he would support Obama, but did nothing to help him win the votes of other Republicans.

Eventually, Obama was given an out. A conversation with Putin led to an agreement that the United States and Russia would work together to destroy chemical weapons in Syria, and the Assad regime announced that they would give up the weapons, though they'd previously denied their existence. No congressional vote ever took place.

The war in Syria continued — and the United States stayed out of it.

> _"Obama was the most powerful man in the world, but that didn't mean he could control the forces at play in the Middle East."_

### 10. Obama pushed through the Iran deal despite strong opposition. 

When Obama took office, Iran had the knowledge and infrastructure required to develop a nuclear weapon. By 2013, it was less than twelve months away from producing the raw materials required. The need to stop Iran's nuclear program was becoming critical.

The administration sought a diplomatic deal to halt Iran's nuclear activity. But this required congressional approval, and there was strong opposition to a deal. The Israeli government was pushing hard, and groups like the American Israel Public Affairs Committee would go on to spend roughly $40 million on ads and lobbying to kill the Iran deal.

At the same time, some media outlets were playing dirty tricks. Right-wing website Breitbart published a story quoting the author as saying that even a bad Iran deal would be worth making — a completely fabricated quotation. He had in fact said that any agreement had to be good enough to last ten or 15 years. But Breitbart's story spread on social media and was read by millions of people in a matter of hours. It was pure, and effective, fake news.

The author formed a unit, entirely focused on securing support for the deal in Congress. He called it the Antiwar Room, because its arguments were simple. The deal would prevent Iran from obtaining a nuclear weapon. Not having a deal would mean military action.

The unit worked hard to use the credentials of adversarial establishment figures against them. When Scooter Libby, former advisor to Vice President Dick Cheney and one of the key figures behind the Iraq war, attacked the proposed deal, the unit argued that the very same people who'd taken America to war with Iraq now wanted war with Iran.

Gradually, things turned in favor of the White House. Leading physicists supported the deal. The former head of Mossad, the Israeli security service, supported it. Iranian dissidents confirmed that they were in favor too.

The day the author's team secured their final, crucial votes, arch-neocon Dick Cheney gave a speech. The Antiwar room watched with pleasure as Cheney reinforced their argument that proponents of the Iraq War now wanted war with Iran until antiwar protesters disrupted the speech. Obama grinned. "That was actually kind of fun," he said.

### 11. Obama’s administration was on the losing side of the information wars. 

Throughout Obama's presidency, the author witnessed the spread of increasingly shameless misinformation. Fake news was becoming a serious issue.

The author saw Russia invest heavily in disinformation, and the United States was not well placed to fight back.

After a Malaysia Airlines Flight from Amsterdam to Kuala Lumpur was shot down over Ukraine, in 2014, a Russian disinformation campaign kicked in. Russia's Foreign Ministry held a press conference, proposing multiple, contradictory theories. A Ukrainian aircraft had done it, they suggested, or maybe it was Ukrainian surface-to-air missiles. The theories were repeated in Russian state-run media and flooded social media. Russian media outlets even invented fake quotations, attributed to a US state department spokesperson. The lying was relentless and deliberate.

The author and his colleagues had few means to fight back. His Russian counterparts had control of TV stations and an army of social media warriors who were encouraged to lie. The author had a five-person press office and a Twitter account, and he was prohibited by law from giving editorial direction to government broadcasting.

In 2014, Rhodes and others examined what — hypothetically — could be done to replicate something like the Russian state-media channel, RT. The project was quickly abandoned when Obama laughingly pointed out that the Republicans would never sign off on a well-funded Obama propaganda channel.

Political attempts to counter Russian disinformation were also flawed.

After it had become clear that the Russians had hacked the Democratic National Convention and were meddling in the presidential election, the White House tried to secure a robust, bipartisan statement condemning the acts. But the Republican leader in the Senate, Mitch McConnell, refused to sign, in an act that Democrats regarded as unpatriotic.

Obama was criticized for not speaking out more strongly. But, as he commented privately, people reading anti-Clinton propaganda were unlikely to take his warnings seriously.

His view? Russians had found a soft spot in American democracy.

### 12. Obama and the author were surprised by Donald Trump’s victory. 

A few days before the 2016 election, Obama and the author were sitting in the presidential helicopter, en route to the White House after a campaign visit, when they received an email from Clinton's campaign. The day before the election, it asked, could Obama go to Michigan?

"Michigan?" Obama said, shocked. He had taken Michigan by ten points in 2012. This was not a good sign.

Later, Obama found it hard to understand the election result.

Sitting in the Beast, the presidential state car, cruising through the streets of Lima on one of his last foreign visits, Obama said he'd been surprised by the result, given the positive economic indicators — low unemployment, cheap gas prices, many more Americans covered by health insurance. Obama asked, "What if we were wrong?" He wondered out loud whether liberals had lost sight of the importance of identity — of the fact that many Americans were skeptical of the kind of metropolitan globalism liberals promoted.

The author himself had the feeling that he should have anticipated the result. On reflection, he thought, Obama's own campaign message against Clinton, eight years earlier, had been similar to Trump's — minus the misogyny. Both had argued that she was inextricably part of an establishment that couldn't be relied upon to bring about real change. He also felt that Trump's win was the result of the increasing irrelevance of facts in right-wing politics — a disturbing state of affairs that he and the Obama administration had had to tackle while in office. A solid majority of Republicans still refused to believe that Obama was actually born in the United States. In these circumstances, perhaps Trump's victory shouldn't have come as a surprise.

Obama was left even more astonished after meeting with Trump.

After a long and friendly meeting with the President-elect, a week after the election, Obama was stupefied. He said that Trump had regularly turned the conversation back to the size of his, and Obama's, campaign rallies, saying that both men could draw a crowd far larger than Hillary could. He voiced openness to Obama's views on health care, on Iran and immigration, but almost prided himself on avoiding taking a clear position on any policy at all.

Obama's final advice on dealing with Trump? "Find some high ground," he said, "and hunker down."

### 13. Final summary 

The key message in these blinks:

**The author explains how during his time in office, President Obama attempted to advance ideals — ideals of universal rights, the benefits of strong, democratic institutions, and of openness and respect. He wasn't always successful in doing so and regularly had to deal with the hard realities of our imperfect world, for example when trying to intervene in the war in Syria or fight Russian disinformation.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _We Were Eight Years in Power_** **by Ta-Nehisi Coates**

_We Were Eight Years in Power_ (2017) reflects on President Barack Obama's two terms in power and the aftermath of the first black presidency. These blinks take a candid look at racism and white supremacy throughout American history.
---

### Ben Rhodes

After working on Barack Obama's first election campaign as a senior speechwriter, Ben Rhodes served as Obama's deputy national security adviser from 2009 to 2017. In that role, he oversaw communications and speechwriting for the administration's national security policy. Previously a congressional staffer and a writer, he coauthored _Without Precedent: The Inside Story of the 9/11 Commission._

