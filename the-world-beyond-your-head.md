---
id: 5681b730704a88000700004b
slug: the-world-beyond-your-head-en
published_date: 2015-12-30T00:00:00.000+00:00
author: Matthew B. Crawford
title: The World Beyond Your Head
subtitle: On Becoming an Individual in an Age of Distraction
main_color: C7B49F
text_color: 7A6349
---

# The World Beyond Your Head

_On Becoming an Individual in an Age of Distraction_

**Matthew B. Crawford**

Our world has changed enormously in recent years. We've developed amazing technology, but this technology has also taken a toll on many important aspects of our lives: our attention spans, our relationships and even our personalities. _The World Beyond Your Head_ (2015) is all about the modern crisis of attention and how you can extract yourself from it.

---
### 1. What’s in it for me? Learn how modernity is ruining your ability to pay attention. 

Decreasing attention spans, shattered mental lives, an increasing sense of distraction — these are issues you're probably aware of, especially if you already know the eerie feeling of walking down a street surrounded by zombie-like people hunched over their phones. These blinks focus on the reasons why we are losing touch with the world beyond our heads, and explain how to become an individual in an age of mass distraction.

These blinks will also reveal

  * what a craftsperson can teach an office worker about attention;

  * why actually being understood would stress out some managers; and

  * why a narcissist might want to have sex with a robot.

### 2. We’re currently living in a crisis of attention. 

In the modern world, we're constantly surrounded by attention-demanding technology — phones, laptops, iPads — which makes it almost impossible to focus. Thanks to this environment, we've developed a need for constant mental stimulation. 

This need has a big effect on what cognitive psychologists call our _orienting response_. The orienting response is what makes us pay special attention to anything that enters our field of vision. We evolved this important survival instinct to help us avoid predators. 

The problem is that these days, new stimuli pop into our field of vision every few seconds! And we've gotten so used to this constant stimulation that we actually _crave_ it, even when it's not important to us.

The fact that we take our ability to pay attention for granted adds to the problem. Attention is crucial to how we interact with the world; it's what enables us to think and create.

But our attention span is inherently limited. In this era of Big Data, ads and bits of news flash at us constantly, an overwhelming barrage of information. And for those who grew up in a different "attentional landscape" — that is, adults who grew up without this constant stream of information — it may be even worse. 

We also generally engage less in activities that demand a lot of attention these days — and it's impoverishing our lives. Paying attention is a skill you build, by doing things like reading a book from cover to cover, something people today do less and less.

Certain skilled practices like repairing bikes or sewing are also on the decline. We just don't have the patience for them anymore, preferring to spend most of our time engaged in activities that occur inside our own heads.

But attention is crucial; it's the secret ingredient that allows us to thrive. In fact, studies have shown that children able to control where they direct their attention are more successful in adulthood.

> _"Our distractibility seems to indicate that we are agnostic on the question of what is worth paying attention to."_

### 3. We’re easily manipulated – so much so that we often manipulate ourselves. 

When faced with a big decision, like which career to pursue or retirement plan to choose, what determines your choice? Economists tend to assume that we simply compare the pros and cons in such situations. But they're sorely mistaken. 

Our behavior and the decisions we make are highly dependent on context. When we have a range of options to choose from, the way those options are presented matters a lot. 

Behavioral economists have studied the way people choose retirement plans, for instance. When looking for a plan, people don't just consider whether a given plan is a good investment. An important detail that has a big impact on their decision is whether they have to actively opt in to a 401(k) plan, as opposed to being signed up for it automatically (with the option to opt out). There's a much higher chance they'll take a plan if they don't have to opt in to it themselves. 

That's why marketers are careful to present their products in certain ways. Companies pay extra for supermarkets to display their goods at eye level, for example, because people are more likely to buy whatever they see first. 

All in all, we're easily manipulated. The good news is that you can even manipulate yourself to enhance your concentration!

Craftsmen like cooks and carpenters do this all the time because they usually can't afford to let themselves get off track. And they don't just concentrate harder — they strategically create environments conducive to focused work. 

An experienced cook might arrange her ingredients in a certain order before she starts cooking, for example, and thus avoid losing time searching for them later. 

You can create a distraction-free environment, too, even if you're not a craftsman. One strategy is to minimize all sensory input that might distract you from your goal.

### 4. Our bodies bring us happiness and allow us to understand the world. 

Our brains aren't the only tools we have for taking in the world around us. Our bodies are of equal importance.

For the brain, the body is like any other tool — a means for performing tasks more effectively. In short, the body allows the brain to better understand its environment.

But this isn't limited to the body itself. According to the concept of _cognitive extension_, your brain can learn to use _any_ physical tool as an extension of the body. That's what happens when athletes seem to "become one" with their equipment. Professional hockey players, for example, can control their hockey stick as dexterously as their own limbs.

Likewise, a blind person can learn to "see" with a special cane. Their brain will even eventually learn to treat information coming from this cane as if it was sensory information coming from their eyes. 

In a sense, our bodies are much like a special cane — a tool that we master as we mature. We struggle with our body as toddlers; throughout childhood, we slowly gain control of it; and, by the time we're adults, we command it with little conscious effort. 

So as we come to understand the world, we also learn how to physically move in it. We actually _think_ through our bodies. If our bodies were unable to process three-dimensional space, for instance, our brains could only understand two-dimensional images. 

And because our bodies are so fundamental to our lives, we derive a great deal of satisfaction from developing manual skills. That's what Friedrich Nietzsche meant when he said that joy was feeling your power increase. 

You also broaden your possibilities as you develop more manual skills. Developing skills and learning to control your body in new ways is also satisfying — for toddlers and adults alike!

### 5. Our technology simultaneously connects us to and estranges us from the world. 

The technology we're surrounded by keeps us in constant connection with the world, right? Well, in some ways it does, but in others, it estranges us from it, too! 

Our devices are designed to be simple and user-friendly, to make our lives easier and more interconnected. The latest car models, for example, are filled with equipment designed to maximize the comfort of the driver and passengers.

But these same technological tools pull us away from reality, too. In a high-tech car, for instance, you barely notice you're in a car at all: the complex design minimizes the sensations of speed and danger. 

This dulled experience of the world has an effect on human behavior, too. As we've seen, our behavior depends heavily on how we perceive our environment, and we can't fully perceive things when sensory input is dulled. 

Most modern consumers know very little about the inner workings of their electronic devices. Most people, for instance, couldn't fix a broken washing machine. That's why we have repairmen. 

Repairmen earn their living by tuning into the details of our machines. A repairman could fix a broken washing machine by assessing its problem and finding the root of it. "Smart" technology encourages us to do just the opposite; it's made to be so simple to use that we don't have to think about how it works. 

These days, few people want to become repairmen, cooks or craftsmen; however, such trades may actually hold the secret to happiness and freedom. A repairman comes to have complete control of his environment: his job depends on his ability to understand and control the materials and machines he works with. 

Having such control of your physical environment can be deeply satisfying — an excellent antidote to the feelings of helplessness and passivity engendered by the world of smart devices.

### 6. Modern society turns us into passive consumers. 

Everyone feels uncertain sometimes. So, how do you deal with that feeling?

As we grow older, most of us become better and better at coping with the unpredictability of the world. We come to accept that the world doesn't always give us what we want. 

The more we're exposed to frustration, for instance, the more we learn to delay our own gratification and come to terms with the chaos of the world. Our coping mechanisms only go so far, however: we all get frustrated sometimes.

This ultimately encourages us to turn away from social interaction, just as people who have autism often do. People with autism often don't socialize well because they have trouble facing uncertainty. Situations they can't control are often very unsettling for them; they usually prefer establishing routines and minimizing social contact. Social situations are frightening because people are unpredictable creatures. 

In the modern world, the control we have over our lives lessens every day. Because of this, we tend increasingly to avoid social situations, which are always hard to control.

Consider how this plays out in the workplace. If you have an office job, you can't fully understand how your work specifically contributes to the company overall. Thus, you won't feel fully in control. 

It's happening in our homes, too, because we keep outsourcing skilled activities like cooking and doing repairs. These activities used to afford people at least some control over their environment. 

It makes us yearn for some form of control _somewhere_. So we turn away from the external world and uncontrollable social situations and start obsessing over minor activities that we _can_ control. 

And the corporate world caters to this! Corporations know we're hungry for control, so they manufacture _experiences_ like video games, which give you the illusion of control but actually turn you into a passive consumer. Gamers just lose themselves in the game, playing along robotically.

> _"The media have become masters at packaging stimuli in ways that our brains find irresistible."_

### 7. Both relationships with other people and the development of manual skills shape your individuality. 

No one lives in a social vacuum; you share your world with droves of other people. And it's a good thing, too, because our social interactions play a major role in our mental development. Indeed, your social experiences aren't _yours_ alone. 

We all seek, in the social world, to be recognized as individuals. We feel validated when other people approve of or praise our actions. That's what Hegel was talking about when he said other people enable us to "check" our own self-understanding.

Economics works in a similar way. The price of a product or service isn't only determined by a few mathematical calculations — it also depends on how much value we _perceive_ it to have. 

Similarly, we need to be perceived positively to feel good about ourselves. That's another reason specialized skills are so important: we get recognition for things we do well. 

A mechanic, for instance, seeks recognition from other mechanics, because the level of someone's expertise can only be accurately assessed if the assessor is an expert herself. And when someone earns such recognition, he will feel more confident about his identity and chosen path.

According to the sociologist Alain Ehrenberg, the uptick in depression cases in Western countries can be linked to the decrease in specialized skills. A few decades ago, it was much easier to construct a stable identity: skilled workers usually worked and lived in the same place for most of their lives. Today, however, we highly value _flexibility_ and _mobility_, which means we're encouraged to keep reinventing ourselves.

This results in people struggling to establish a sense of self, which leads to their feeling lost and depressed.

### 8. It’s harmful to your wellbeing to be self-centered and narcissistic. 

No matter what experiences you have, you're always experiencing yourself. So it's okay if you feel like the center of the universe sometimes — that's just part of being an individual!

Our self-centered nature does have its downsides, however. Highly self-centered people tend to perceive any problem they encounter as a personal affront, which makes it much harder to cope with life. 

A self-centered person might perceive every traffic jam or long checkout line as an undeserved and hostile situation. Such a person might get stressed out or angry, which, of course, is no help at all. 

Shifting your attention _away_ from yourself is a much better way to deal with negative feelings like frustration or rejection. Dwelling on these emotions only makes them worse. 

No one ever purposefully stops being in love, for example. So if your partner loses interest in your relationship, don't put all the blame on yourself. Try directing your love elsewhere by meditating or praying — whatever works for you. 

Being self-centered and narcissistic creates other problems, too. For instance, narcissists tend to need other people desperately while simultaneously finding it difficult to empathize with them. This can push them to seek out unhealthy relationships.

Insecure people crave control, praise and social validation — and a strong desire for control isn't always good in a relationship. 

Our relationships also suffer from the fact that we increasingly communicate online and through text messages instead of in person. That affords us more control of the conversation but may have consequences we still don't fully understand. 

The modern desire for control and self-preservation may even result in people seeking intimacy with robots and other virtual companions, whether just for sex or as amorous outlets. Non-human relationships provide the ultimate form of control, not to mention an alternative to human unpredictability.

### 9. Our personal relationships suffer when we diminish our individual differences. 

As we become more distanced from the physical world and more immersed in the abstract space of the digital world, our personalities and relationships become more abstract, too. Even now, a number of social customs are changing because of changes in the way we communicate. 

For instance, people shy away from expressing strong opinions these days. It's just too risky, because people might disagree with you or get offended. Some people might even accuse you of being arrogant if you value your own opinion over that of someone else!

A group of sociologists studied this in 2008, when they interviewed 200 young Americans about their values and morals. Most of them avoided saying anything too direct. Instead, they made vague statements about how opinions differ from person to person — that no one person can say with assurance what's right and what's wrong.

Even managers hide behind fuzzy language in the workplace by employing vague corporate jargon. This allows them to avoid taking responsibility for their work, because if something is unclear from the get-go, it's easy to blame negative outcomes on a lack of clarity.

Our fear of stating real opinions has created an opinion-vacuum in the public sphere. We live in a state of _colorless cohesion_, forgoing our individuality by viewing ourselves as insubstantial parts of a mass public. 

Kierkegaard, the renowned philosopher, opposed this. He criticized what he called _leveling_, the egalitarian social process that erodes and denies the differences between individuals.

Leveling is at work when parents seek to be friends with their children rather than standing as authority figures. Kierkegaard, on the other hand, wrote that meaningful relationships only arise between two distinct (and thus incomplete) people who need each other, like a teacher and student.

### 10. We value both autonomy and education, though the two conflict with each other. 

The word "education" derives from the Latin word _educere_, which, roughly translated, means "to be led out." At its core, education is about being led out of your own self. 

Education is a fundamental part of modern life. But we also highly value autonomy, and our desire for both creates some inner-conflict. 

We highly praise individuality. However, education begins with acceptance of some kind of hierarchical structure. 

Consider the process of becoming a musician. Musicians start out by learning about keys or frets; they learn their craft by mastering techniques passed on to them by others. So human talent and ingenuity can only arise within the confines of established, external limits.

Obedience and structure are becoming less important in modern education, however. Just think about the numerous online courses that have become available in recent years. They provide us with information, but they lack structure and the dynamic of the student-teacher relationship.

Education is also a vital part of regaining our ability to pay attention. Today's students don't only suffer from the absence of teachers; they're also offered classes that don't interest them, thanks largely to the increase in stimuli competing for their attention.

Students need to be treated like human beings, not hypothetical machines that learn for the sake of learning. They need a structured learning environment and courses that relate directly to the real world. 

More students would appreciate trigonometry, for example, if they knew how to build a racecar engine. The goal of being a racecar mechanic is enough to propel some students through school. Such students might also enjoy mathematics for its own sake; however, their studies should still have concrete applications. Our academic disciplines shouldn't be purely abstract: they need to connect us with the world around us. Education should free us by bringing us _outside_ our heads.

### 11. Final summary 

The key message in this book:

**The modern world is saturated with distracting stimuli. Our technology has done us a lot of good, but it's also eroding our ability to focus, develop skills, form and express our own opinions and connect with other people in meaningful ways. It has ultimately estranged us from the world it aims to connect us to, dulling our perception of our surroundings and even our sense of self. Fortunately, you can reconnect with the world beyond your head by honing your manual skills, creating constructive physical environments for yourself and engaging in social interactions instead of shying away from them. We aren't meant to live in our heads — get back in touch with the real world!**

**Suggested** **further** **reading:** ** _Shop Class as Soulcraft_** **by Matthew B. Crawford**

_Shop Class as Soulcraft_ (2009) is an eye-opening view into how working with your hands can transform your life. These blinks take a look at the changing nature of work, the value of manual labor and how choosing a trade, as opposed to a profession, might be your ticket to happiness.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Matthew B. Crawford

Matthew Crawford is a philosopher and motorcycle mechanic. He's also a contributing editor at _The New Atlantis_ and a senior fellow at the University of Virginia's Institute for Advanced Studies in Culture _._

