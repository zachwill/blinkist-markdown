---
id: 513f574ae4b0569be343764f
slug: the-world-until-yesterday-en
published_date: 2014-05-20T14:35:52.000+00:00
author: Jared Diamond
title: The World Until Yesterday
subtitle: What Can We Learn from Traditional Societies
main_color: B16629
text_color: 965723
---

# The World Until Yesterday

_What Can We Learn from Traditional Societies_

**Jared Diamond**

_The_ _World_ _Until_ _Yesterday_ explores the lessons modern humans can learn from the primitive hunter-gatherer societies that roamed the earth before centralized governments emerged.

---
### 1. What’s in it for me? Discover the lessons our hunter-gatherer ancestors can teach us. 

Around 11,000 years ago, the human species went through a major cultural shift known as the _Neolithic_ _Revolution_, when humans moved away from hunting and foraging to agriculture and farming to obtain nourishment.

This change brought about an increase in the availability of food and facilitated a population explosion, which in turn lead to urbanization, central governance and many other aspects we consider staples of modern society.

These days, whenever societal problems arise, we typically tend to look for new technologies and ideas to solve them with. But perhaps another approach is needed: perhaps we should look back to the traditional arrangements and lifestyles that worked for humanity for millennia before the advent of farming.

After all, much of our physiology and psychology evolved to suit the needs of the hunter-gatherer lifestyle: from an evolutionary perspective, the Neolithic Revolution was recent — practically yesterday.

Therefore, it stands to reason that by examining traditional societies, we may in fact be able to learn valuable lessons to help us solve the problems of modern life. The following blinks are an exploration of these lessons.

Among other things, you'll find out

  * why boys as young as three months old are already given bows and arrows in some societies,

  * why it's important that criminals apologize to their victims and

  * why religion will probably continue to play a role in society well into the future.

### 2. Small, traditional societies like hunter-gatherer bands and tribes still exist all over the world. 

You might think that modern civilization has occupied all corners of the globe, but there are still several traditional societies that live much like all of humanity did some eleven thousand years ago. Though these enclaves are no doubt influenced by the modern states in which they reside, examining them allows us to get a perspective on what life was like for our ancestors.

So what kind of traditional societies can we find across the globe today?

First, there are the _hunter-gatherers_ such as the Siriono Indians of South America and the Andaman Islanders in the Bay of Bengal.

These societies typically split up into _bands_ of less than a hundred individuals and are very egalitarian and democratic in nature. Everyone knows one another, so they can make decisions in face-to-face discussions and no formal leadership is required.

Second, there are _tribes_ consisting of a few hundred individuals like the Iñupiat in Alaska. These tribes can actually practice agriculture or animal herding, but it's less intensive and sophisticated in nature than that in actual states.

Finally, there are _chiefdoms,_ like the Chumash Indians of North America. These chiefdoms are a marked step closer to modern states and can even comprise hundreds of thousands of individuals. They have a clear, centralized leadership which collects economic goods from members and redistributes them further to warriors, priests and craftsmen who serve the chief. There is also clear social stratification: the chief's family is at the top of society, meaning they get the best housing, food and goods.

As you can see, there is an entire continuum of societies spanning from the hunter-gatherer bands to our moderns states. We will mostly focus on the hunter-gatherers because they are the most different from our own, and hence hold the most valuable lessons for us.

### 3. Traditional societies have high mortality rates from both wars and homicide. 

Archeological information and evidence of modern hunter-gatherer bands show that this form of society tended to exist in bands comprising fewer than a hundred individuals.

Why were the groups so small?

Because a larger band would have required them to acquire more land from which they could live and there was only a finite amount of unoccupied land available.

This not only restricted population growth, but also caused conflict between traditional societies.

Indeed, war seems to have been a common feature of life in hunter-gatherer bands and tribes, and war-related mortality rates were far higher for these kinds of societies than modern states.

In fact, wars raged constantly because even if peace was negotiated, inevitably, some individual hothead would ignore it and start a whole new cycle of violence.

Modern states, on the other hand, have strong central governments which, once peace has been negotiated, can restrain the few aggressor individuals and thus guard the fragile peace.

Interestingly, the traditional societies involved in agriculture were even more warmongering than hunter-gatherers: because the larger stores of food produced by agriculture were something to fight over.

Very few societies, including some Australian desert tribes, have been able to abstain completely from war. In large part, this was due to circumstances, such as low population densities and harsh, unproductive environments, that weren't worth fighting over.

But in all societies — even those where war is unknown — there is intra-group violence and lots of it: traditional societies have far higher homicide rates than modern state societies.

This is mainly because these traditional societies lack a judicial system that punishes those who resort to violence. Without it, the victim or their family feel it is their duty to avenge the crime, and this makes revenge killing very common.

Clearly, one of the greatest benefits of having a state lies in its ability to curb violence by enforcing peace and punishing criminals.

> _"Most of the Kung killings (15 out of 22) were parts of feuds in which one killing led to another and then to yet another over the course of up to 24 years."_

### 4. Traditional societies resolve conflicts by re-establishing relations – which modern judiciaries could learn from. 

As we have seen, conflicts and disputes exist in traditional societies, but they are resolved very differently from state societies.

When disputes need to be settled in traditional societies, the emphasis is on re-establishing the previous peaceful relationship between the two parties so they can continue to live with each other in their closely knit society.

To achieve this, it was considered necessary for the offending party to demonstrate sincere regret for causing harm to the victim.

In addition, they also needed to offer some form of _compensation_ to the victim to make up for their loss. But in fact this compensation, known in Papua New Guinea as "sori money," also had another purpose: validating the regret and apology by the offender.

Let's contrast this with modern states, where disputes which cannot be resolved privately often wind up in one of two kinds of court.

_Criminal_ _justice_ aims at punishing individuals who have committed crimes against the laws of the state.

_Civil_ _justice_ aims to obtain compensation for the victim of a non-criminal injury from the offending party.

As you can see, neither of these courts has the goal of re-establishing peaceful relations between the two parties. In fact, in many cases, going to court makes the relations of the two parties even worse.

This is an arena where modern state societies could learn a lesson from traditional ones: our judicial system should make a greater effort to help the two parties reconcile.

In the civil setting, this could be done by training and funding more mediators to help settle disputes in the court room.

In the criminal realm, a program known as _restorative_ _justice_ could help. Here, the victim and the offender are encouraged to talk directly to each other if they are willing. The goal is to help criminals take responsibility for their actions and for victims to achieve closure. In fact, some experiments with these kind of programs have already yielded promising results, such as criminals being less likely to reoffend, and victims feeling less fearful.

### 5. People in traditional societies raise their children very differently from those in modern ones. 

In hunter-gatherer groups, mothers are incredibly responsive and accommodating. Infants are at their mother's breast day and night, and can nurse whenever they want to, even if the mother is sleeping. Studies have also shown that when infants begin to cry, they are mostly comforted and responded to very quickly: in under ten seconds.

In modern societies, some might consider such attentive parenting detrimental, potentially stifling the child's self-reliance. But this is disproven by the fact children in traditional societies grow up to be incredibly confident and autonomous, a fact noted by many observers.

As children in a traditional society grow up, they receive increasing care from caregivers who aren't their parents. These so-called _allo_ _parents_ can be grandparents, aunts, uncles, older siblings or even older playmates. Sometimes, the actual parents may go off for weeks at a time to forage and hunt while allo parents take care of the child.

This rich group of social influences helps make the child more socially capable. What's more, it helps the older playmates learn how to take care of children, thus preparing them for the challenges of parenting themselves.

Another difference compared to modern societies is that, in traditional societies, children are given adult-like roles much sooner than in modern societies.

For example, boys of Bolivian Sirionó Indians receive a miniature bow and arrow when they're only three months old, and begin shooting practice when they're three. At age eight, they're ready to accompany their fathers on hunting trips and thus contribute to society.

Another marked difference to modern societies is that in traditional societies, children's games rarely involve competition. Instead, they encourage cooperation and sharing, which is also an integral part of adult life in traditional societies.

Child-raising practices like these have churned out resourceful, happy individuals for nearly a hundred thousand years, so we should clearly consider how we might benefit from them today.

### 6. Traditional societies tend to respect and appreciate the elderly more than modern ones. 

When it comes to the treatment of the elderly, traditional societies vary greatly.

In rural Fiji, for example, people house their elderly parents and take good care of them, even pre-chewing their food for them if necessary.

On the other end of the extreme, there are groups where the elderly are abandoned or killed if resources are insufficient to care for them.

In general, though, traditional societies do hold tremendous respect for the elderly since they've accumulated a great deal of knowledge about things like myths, good foraging grounds, and the various uses of plants and animals.

What's more, in many traditional societies, the elderly continue to contribute in whatever way they can: as mentioned in the previous blink, they take care of their grandchildren, but also hunt and forage for whatever animals and plants suit their abilities, for example, smaller game.

In modern society, unfortunately, the elderly tend to be viewed as far less useful, and there are many lessons we could learn from traditional societies in this respect.

One big reason for the declining respect for the elderly is that we now have other ways of storing knowledge, which lessens their role as reservoirs of wisdom. But, in fact, young people would benefit greatly from hearing first-hand about historical events like World War II. Indeed, some educational programs aim to do this by bringing together the elderly with high school students, who can then learn valuable lessons from their first-hand accounts.

What's more, grandparents could play a more significant role in today's child-rearing: they're experienced parents themselves, tend to be highly motivated to care for their grandchildren, and the actual parents would typically welcome a break, so this is a win-win for everyone.

At the same time, we should try to find ways to allow the elderly to stay in working life if they want to. They must be able to contribute in areas they're skilled and experienced in at a pace that suits them.

> _"The challenge for older people themselves is to be introspective, to notice the changes in themselves, and to find work utilizing the talents they now possess."_

### 7. Our modern lifestyle has brought us many health concerns traditional societies didn’t have. 

One arena in which modern societies are arguably far better off than traditional ones is _health_. We've managed to eradicate many of the diseases and conditions that plagued humanity in the traditional world: diarrhea, malnutrition, parasites, etc. Indeed, life expectancies in modern societies can be twice as long as they were in traditional ones.

However, we in the modern world are plagued by other major causes of death which weren't present in simpler times: _non-communicable_ _diseases_ (or NCDs). These are things like cardiovascular diseases, diabetes, kidney conditions and lung cancer. In fact, almost 90 percent of all Americans, Europeans and Japanese reading this will die of these NCDs.

So where have these new afflictions come from?

The major causes lie in our modern lifestyle.

First of all, we're sedentary. Whereas in traditional societies people are physically active as they hunt and forage, we mostly just sit in front of computers or televisions all day and night.

What's more, we have unhealthy diets. While the main diet in hunter-gatherer societies consists mostly of protein and fiber, we gorge ourselves on carbohydrates, fat and sugar. Consider that just over the past three hundred years, sugar consumption per person in England and the United States has grown by almost fortyfold.

This modern diet is causing more and more people to get diabetes: by 2030, around 500 million people are projected to have it, which would make it one of the world's biggest health problems.

What's more, compared to hunter-gatherers, our modern diets contain three to four times more salt, which raises blood pressure and increases the risk of getting almost all the modern NCDs. Shockingly, though, around 75 percent of this salt intake does not come from us being overzealous with the salt shaker, but from manufacturers adding it to processed food.

The lessons for modern society are fairly straightforward: we should exercise regularly while limiting our intake of sugar, salt and fat, and avoid alcohol and smoking.

### 8. From traditional societies to modern ones, religion has played an ever-present, yet evolving role. 

One interesting aspect of humanity is that religion always has a part to play, whether we look at traditional hunter-gatherer groups or modern states, and its role evolves as society does.

Probably, religion first came about due to the natural human inclination to look for intentional causes for events that we see. For example, traditional societies tend to believe that things like rivers and the sun are propelled by supernatural forces.

Similar supernatural explanations are then extended to other difficult-to-explain matters like illnesses, death and the origin of the world. Prayer and other rituals then emerged as a hopeful means of influencing these supernatural forces.

As societies became bigger, the role of religion became more diverse.

At this point, inequality was on the rise as chiefs began to demand taxes and tributes from commoners and led a comparatively lavish lifestyle. Typically, it was religion that helped them justify this, as it would proclaim that the chief was a god and could therefore help peasants by bringing rain or a good harvest. Thus paying tribute to the chief started to seem like a good idea.

At the same time, religion began to include moral guidelines. This was because societies were growing too large to rely on interpersonal relations to avoid violence and conflict: a set of rules for treating strangers in a civil way was required.

It is noteworthy, though, that these rules only applied to intra-group members. Societies still had to wage war against each other, so religions also emphasized that members of _other_ societies should be despised.

These days, the role of religion in society is waning: secular laws enforce civil behavior even without religious incentives, while increases in scientific understanding have lessened the need for religion to explain the worlds and calm our existential angst.

But even today, science still can't provide all the answers people seek, and it's thus likely that religion will continue to play a role in society well into the future.

### 9. Final Summary 

The key message in this book:

**Until** **practically** **yesterday,** **all** **of** **humanity** **had** **lived** **in** **small** **bands** **of** **hunter-gatherers.** **Even** **today,** **such** **traditional** **societies** **have** **a** **lot** **to** **teach** **us:** **from** **the** **way** **they** **raise** **their** **children** **to** **how** **they** **settle** **their** **disputes.**

Actionable advice:

**Enlist** **your** **parents** **help** **in** **raising** **your** **children.**

One of the lessons parents in modern society can take away from those in traditional societies is that grandparents can play an important part in raising their grandchildren. If you have a child and live close to your parents, why don't you see if they would be interested in playing a larger role in the child's upbringing by caring for him her or a few evenings a week? They are experienced caregivers themselves and often happy to spend time with their grandchildren, who in turn get a richer social environment to learn to interact in.
---

### Jared Diamond

Jared Diamond is a respected American scientist and a Pulitzer prize-winning author of several popular science books such as _Guns,_ _Germs_ _and_ _Steel._

