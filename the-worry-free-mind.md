---
id: 59e4a3feb238e100062ad1e7
slug: the-worry-free-mind-en
published_date: 2017-10-19T00:00:00.000+00:00
author: Carol Kershaw, Bill Wade
title: The Worry-Free Mind
subtitle: Train Your Brain, Calm the Stress Spin Cycle, and Discover a Happier, More Productive You
main_color: 2F77E5
text_color: 2F77E5
---

# The Worry-Free Mind

_Train Your Brain, Calm the Stress Spin Cycle, and Discover a Happier, More Productive You_

**Carol Kershaw, Bill Wade**

_The Worry-Free Mind_ (2017) takes a close look at why we spend so much time worrying and what can be done to reduce these worries. Having a stressed-out mind may be part of being a human being, but that doesn't mean you can't take steps to better control your thoughts and emotions. Here are some tips and techniques to take control of your mind and live a happier life.

---
### 1. What’s in it for me? Leave your worries in the past. 

Are you constantly battling bouts of anxiety and worry? Do you do things such as wonder incessantly whether you locked the front door? Maybe you break out in a cold sweat worrying about whether you really sent off the right documents to the new client — or whether you sent something totally inappropriate.

Such worrying is normal, but it's also avoidable. The truth is, your brain has different gears, just like a bike — and each gear is well suited to certain activities and pretty unsuited for others. The trouble arises when you use the wrong gear on the wrong activity.

In these blinks, you'll learn

  * about your brain's different frequencies;

  * how to let go of the past for a sunny future; and

  * how peripheral vision can help you to rid your life of worries.

### 2. Modern humans are stuck with a surplus of negative thinking and stress chemicals. 

You lay awake in bed, unable to sleep because your mind is racing with worry after worry after worry. Sound familiar? Then let's look a bit deeper into stress and worrying.

First of all, much of your incessant worrying is due to an overabundance of stress-related chemicals in your body. It used to be that we humans had a need for these chemicals. Back in the days of saber-toothed tigers, we lived in a world full of life-threatening dangers and fight-or-flight situations. And, though such daily threats have all but disappeared in the modern world, these instincts are still with us. As a result, your mind has a _negativity bias_ : it constantly seeks to recognize threats around you.

The biochemicals related to this state of worry and stress can only be alleviated by engaging in vigorous activity. But since we're no longer being chased by wild animals, or chasing them down for meat, these stress chemicals stay built up in our bodies and keep us in a state of constant worry.

Most of us have grown used to it. It feels normal to be chronically stressed. And, disturbingly, the more stressed out your brain is on a regular basis, the more likely it becomes that this state will be the normal setting for your brain in the future.

This is due to your brain's neural pathways, which are created by your everyday thinking habits. These neural pathways also affect how you see the world around you, and determine whether you see opportunities waiting around every corner, or dangers and difficulty.

So the more you base your thoughts on stress and worry, the more likely you'll be to act defensively and distrustfully.

But hope is not lost. It's in your power to reduce your worries and put yourself on a more positive track.

### 3. We have five different brainwave frequencies that correspond to different states of being. 

If you're familiar with cats, you know that they're often volatile; they can be gently resting one second and bouncing off the walls the next. Well, our thoughts are rather catlike. The shift from peaceful to stressful can occur in the blink of an eye.

The brain is made up of billions of nerve cells called _neurons_ that use electric pulses to communicate with each other. Depending on what you're thinking, feeling or doing, these pulses create brainwaves of different frequencies.

So, to better understand your brain, you should know that it can switch between five different frequencies, depending on what you're doing. The five brainwave frequencies can be measured in _hertz_, or cycles per second. These waves are called _delta, theta, alpha, beta_ and _gamma_.

Delta is the slowest of the brainwave frequencies, ranging from 0 to 4 hertz. It occurs in deep sleep and is extremely useful for growth and regeneration.

Theta is the frequency of deep relaxation. It is measured at 4 to 7 hertz, and is often reached in the moments after we wake up from the deep sleep of the delta frequency.

The alpha frequency is measured at 8 to 12 hertz. It is a conscious and calm state that allows you to regain energy. In this state, you might be lying on the couch with a good book and without any fears or worries.

The beta frequency is a bright and attentive state, measured at 12 to 35 hertz. It's good for the kind of focused attention you need for getting some work done.

Last but not least is the gamma frequency, at 35 to 70 hertz, which is where the sought-after state of flow exists, characterized by a blissful sense of peace. It's the state that Buddhist monks are very familiar with through expert meditation.

Ideally, you want to avoid spending too much time in one brainwave frequency. But this is what happens with a worried mind, which is usually stuck in beta mode — constantly alert. If you've ever been on vacation but been unable to relax and enjoy yourself, it's due to your brain being unable to downshift out of beta mode.

In the next blink, we'll look at ways to get unstuck.

### 4. To quickly calm your mind, engage your peripheral vision or take a walk. 

The key to cultivating a worry-free mind is to redirect your focus away from the things that trigger worry and stress.

Once you learn how to deliberately shift your focus, and get into the habit of doing so, you'll be changing your brain wave activity. Change that and you can change your feelings, thoughts and perspective. That's why knowing how to shift your frequency from beta to alpha is the best way to burst your worry bubble once and for all.

Now, one of the best ways to refocus your attention is to use your _peripheral vision_. This is the area of your vision that exists off to the sides of whatever you're directly focusing on. This takes some practice, so start by keeping your eyes locked straight ahead, but rather than noticing what's directly in front of you, take notice of what's to the left or right edges of your vision.

You'll find that, when trying to use your peripheral vision, it's rather difficult to entertain a negative thought. Why? Because you've shifted your attention. So the next time you're stuck, engage your peripheral vision and get yourself unstuck.

Another effective technique is to simply go for a walk. When upset or caught in a cycle of stress, the human mind's capacity for rational thinking becomes faulty. Studies show that, when caught up in such a cycle, a person's blood will flow to the right side of the brain, away from the left hemisphere where rational thinking takes place.

But going for a walk is what's known as a bilateral activity; it engages both hemispheres of the brain and can kick your rational thinking back into gear. This increases the chance that you'll abandon the irrational doom-and-gloom thinking.

So the next time you're worried about a meeting or presentation, get yourself outside and look around. Notice the scenery, architecture and nature that surrounds you. This will reduce your anxieties and can even put you into an alpha frequency.

### 5. Future thinking can improve your outlook on life by changing the way you question the future. 

We often place people in one of two categories. There are the pessimists, who see a glass as being half empty, and the optimists, who see the same glass as being half full.

But a better personality test is to ask someone how they feel about the future. Are they excited and eager to see what happens in the next five years, or are they filled with dread?

It's only natural to have some concern about the future, but when someone's really pessimistic, they're actually increasing the chances of being miserable. When you spend all your time worrying, you're blocking any chance of happiness, not to mention wasting energy that could be used to make a brighter future.

The author had a client named Marina, who was moving to Paris for a job. Prior to this, Marina had spent her entire life in Houston, so this was a big change that put her in a constant state of worry. She was especially stressed about recent terrorist attacks and fearful that her relationship with her boyfriend would fall apart. Any excitement she may have once felt was now drowned in worry.

However familiar this may sound, there is a method to break free of these unwanted thoughts, and it's called _future thinking_.

Future thinking is about looking forward with positivity and seeing opportunity rather than problems. Not only will this help rid you of worry; it can lead to a lot more happiness and success in life.

The key to future thinking is taking your "what if" worries and confronting them with "can do" solutions.

Marina was stuck with questions like "Why can't I enjoy this?" and "What if I'm in the wrong place at the wrong time?" But when she applied future thinking, the questions began to point the way to a solution.

Instead of "What if we break up?" the question became "What steps can I take to make our long-distance relationship work?"

With the right frame of mind, Marina could once again see the opportunities Paris had in store.

### 6. Memories can have a negative impact on your present and future. 

If you think about it, you can probably come up with at least one decision that you regret. But, for some, regretful choices and actions can be so overwhelming that they trigger a deep depression.

Unfortunately, past events often have far more power over us than we'd like. All of our memories have an emotion that goes with them — usually whatever emotion we were feeling at the time. But if we keep thinking about a certain memory, we can get stuck in that emotion, which is particularly bad if it was a traumatic or sad event.

When we get hung up on a past regret, it also prevents the mind from focusing on creating new and better emotions in the present or recognizing opportunities for a better future.

Gabriella is another one of the author's clients. Her problem was that she had trouble saying no and sticking up for herself. As a result, she often took on too many responsibilities, many of which belonged to other people.

For Gabriella, this habit went back to childhood. She was the oldest sibling in a broken home with many financial struggles, and she often had to cook for and take care of her siblings.

Gabriella regularly felt that if she didn't take responsibility, things would fall apart, a feeling that stayed with her as an adult. And so Gabriella didn't protest when her siblings expected her to take care of their elderly mother, and she never kicked up a fuss when she was severely underpaid at work — she simply added another job to her busy schedule.

Ultimately, the only thing Gabriella couldn't take care of was her own well-being. Gabriella was stuck in a cycle of thinking that began in childhood — a cycle that became so routine that her mind began regarding being exploited as a normal, everyday part of life.

But this is no way to live life, so let's see how we can break out.

### 7. By asking the right questions, we can identify the sources of our worries. 

When life gets you stuck in a bad pattern, what you need is _neuro-repatterning_. This technique first makes you aware of what triggers your negative emotional responses, and then empowers you to regain control over your reactions.

In other words, it allows you to change your thoughts, feelings and behaviors when certain situations or memories present themselves.

There are four steps to neuro-repatterning, the first of which is to identify the precise trigger by asking yourself probing questions, such as: "Is it certain people that make me worried or fearful? Or is it a specific type of place?"

Acknowledging your fear and becoming curious about it is the first step toward overcoming your worried state of mind.

The second step is to get closer to your feelings. Do this by asking questions such as, "Is there a physical discomfort associated with my worries? Where in my body do I feel this pain? Is it a familiar feeling?" Narrowing things down like this will help you get closer to a solution.

For Gabriella, she felt her worry in her stomach, and she would feel it whenever she went to visit her mom. Noticing this helped her realize that her problems are related to her family.

The third step is to ask yourself, "What do I need to do to rid myself of this feeling?" This question could help Gabriella, too. It could start with something as simple as Gabriella treating herself to a day at a spa.

Finally, there are four feelings that you can keep in mind to refocus your mind and reduce worry: _curiosity_, _lust_, _care_ and _play_.

So it's good to have a hobby that sparks your curiosity, such as food and exploring new recipes. Having a loved one is of course a fine way of keeping both your lust engaged and someone to care for. And finally, things like sports, a regular card game or spending time with a young one can keep your sense of play engaged.

### 8. With meditation you can synchronize your brain in alpha frequency and thereby sustain a clear, worry-free mind. 

When you're overcome with worry, you want to be ready with the steps you can take to start calming yourself down and feeling better.

The best way to do this is to synchronize at least part of your brain down to the alpha frequency. Ideally, with enough practice, you can reach a _whole brain state_. When this is achieved, a clear mind will be your new normal, and worries will be an infrequent and controllable part of life.

The ability to synchronize the human brain to alpha was first demonstrated in the 1960s by the head of the Princeton Biofeedback Centre, Les Fehmi. He used a _biofeedback_ technique, which electronically monitors parts of the brain to keep track of their frequencies.

Fehmi found that when multiple parts of the brain were in synch and running at the same frequency, they could communicate with each other in harmony. This state is characterized by clear thinking, better understanding of the world around you and less anxiety and worry.

Sounds wonderful, right? So what's the secret? Simple: using meditation to clear the mind completely.

Meditation is a way of getting rid of the clutter that causes a brain to function poorly. It also helps bring the brain down from the frantic beta state and into the calm and cool alpha state, which is where you can chill out and live worry-free.

In the 1970s, Anna Wise of the Evolving Institute, and pioneering biophysicist C. Maxwell Cade, found that meditation led to an increased amount of alpha frequency in the brain. They also found that their subjects, all advanced meditation practitioners, had more alpha in everyday life as well.

Naturally, these people were a rather calm and happy bunch.

So if you want more control over your mind and a stress-free life, start a regular meditation practice and start working toward a whole brain state. With a little patience, you'll be on your way to minimizing worry once and for all!

### 9. Final summary 

The key message in this book:

**It's completely natural to worry from time to time. But it's a problem when your life is controlled by fears and anxieties. Fortunately, there are ways we can calm our mind and get some relief. The brain is very capable of falling into patterns that constantly promote stressful thoughts. But we can add new patterns to our daily routine, and, before long, change the negative into a positive.**

Actionable advice:

**Say Cheeeeeese!**

Next time you feel a bit worried about a job interview or taking an important phone call, try smiling! By holding a smile for a minute, you'll find that your worry will begin to vanish. That's because of the interconnectedness between your physiology and your internal, emotional state. So go ahead. Show those pearly whites!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Positivity_** **by Barbara L. Fredrickson**

_Positivity_ presents the latest research into the positive emotions that are the foundation of our happiness. By presenting different strategies to increase the amount of positive emotions you experience, this book will help you adopt a positive general attitude toward life.
---

### Carol Kershaw, Bill Wade

Carol Kershaw is a clinical psychologist who specializes in clinical hypnosis. She heads the Milton Erickson Institute of Houston along with her husband, Bill Wade. It strives to provide psychotherapy to individuals, families and couples in the community. Kershaw is the author of _The Couple's Hypnotic Dance_ and the co-author of _Brain Change Therapy: Clinical Interventions for Self Transformation._

Bill Wade is a professional counselor and therapist who specializes in couples and families while always looking for new approaches to better serve his patients. Along with his wife, Carol Kershaw, he heads the Milton Erickson Institute of Houston.

