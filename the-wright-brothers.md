---
id: 592c0464b238e1000507d132
slug: the-wright-brothers-en
published_date: 2017-06-01T00:00:00.000+00:00
author: David McCullough
title: The Wright Brothers
subtitle: The Dramatic Story Behind the Legend
main_color: 91353A
text_color: 91353A
---

# The Wright Brothers

_The Dramatic Story Behind the Legend_

**David McCullough**

_The Wright Brothers_ (2015) is the inspiring story of two men from Dayton, Ohio who turned a fascination with birds into a mission to enable and achieve human flight. The story of the Wright brothers is a perfect example of how hard work, persistence and a touch of genius can lead to endless possibilities.

---
### 1. What’s in it for me? Get inspired to fly. 

Whether you're itching to start your own entrepreneurial venture, tackle a new challenge or literally learn how to fly, the story of the Wright brothers is an ideal source of inspiration. These two pioneers of invention and aviation created the world's first successful airplane.

But behind every success story there are untold tragedies, struggles and necessary failures. These blinks will give you a behind-the-scenes look at the trajectory and progress of the Wright brothers' lives and innovations, and tell the story of how they became idols for generations of inventors the world over.

In these blinks, you'll learn

  * why the first accurate account of motorized flight was published in the periodical _Gleanings in Bee Culture;_

  * why skipping school might be worth it for certain things; and

  * why it can be good to keep a safe distance from a life of luxury.

### 2. The Wright brothers’ personalities and upbringing made them a winning team. 

You probably know that the Wright brothers invented the airplane — but do you know how they got to the remarkable moment when their aircraft took its first flight? Let's go back to where it all started.

Wilbur Wright was the older of the two brothers, born April 16, 1867, four years before Orville, born August 19, 1871.

The two were virtually inseparable throughout their lives. Not only did they eat meals together and keep a joint bank account, they even had similar handwriting.

But they did have their own unique personalities. Wilbur was the more scholarly of the two, and with his nerves of steel, he was clearly the unspoken leader. Orville, on the other hand, was a gentle soul who was more sensitive to criticism than Wilbur. But he was also more cheerful and more adept at the business side of things.

The brothers had a modest upbringing in Dayton, Ohio, which was the state's fifth-largest city at the time.

Their father, Bishop Milton Wright, encouraged his children to be open-minded and hard-working, and made sure their modest home was well stocked with books.

Their mother was Susan Koerner Wright, who would later die of tuberculosis when the boys were in their early twenties.

The boys also had one younger sister, Katharine Wright, who lived with them, and two older siblings, Reuchlin and Lorin, who lived elsewhere.

Bishop Wright was eager to encourage his boys' education, but he did let them skip school if they wanted to take advantage of the books at home instead.

This encouragement led Orville to discover an early interest in business, and he started his own print shop while he was still in high school. Later, he and Wilbur would open a bicycle shop, which helped fund their work on airplanes.

Meanwhile, it was Wilbur who became fascinated with flight, thanks to his father's private library.

He was especially taken with birds, as well as the work of Otto Lilienthal, a German glider enthusiast, and Pierre Mouillard, a French poet and farmer who also shared a profound love of flight.

### 3. In the face of considerable obstacles, Wilbur and Orville built an innovative glider. 

The Wright Brothers weren't alone in their dream of flight. By the start of the twentieth century, many had already tried and failed, and the press were always quick to jump on these failures.

One popular source of ridicule was Charles Dyer, who had built an aircraft in the shape of a duck in the 1870s.

But the risk of failure and mockery did not deter Wilbur and Orville, and they went on to develop important insights, especially when it came to equilibrium.

In order to sustain flight, the brothers realized that all the forces of the aircraft needed to be perfectly balanced and able to change with the wind. Therefore, the key to flight was in allowing the pilot to adapt to changing winds by controlling the aircraft precisely and quickly.

It was Wilbur who solved the problem. They needed to enable the wings of their glider to bend or "warp" in order to achieve different angles and get the plane to turn. If the pilot could control the warping, they could control the plane.

With this knowledge, they went to work on their first glider in the summer of 1899.

The famous fields of Kitty Hawk, in the isolated area of Dare County, North Carolina is where the duo staged their first and successful trials with their glider.

The area had ideal conditions: steady winds to lift the glider and sand dunes to provide a cushion for any crash landings.

The first test flights took place in September of 1900, when their assembled glider weighed just under 50 pounds. It had two fixed biplane wings, one on top of another, and it could be maneuvered with the warping controls and a movable rudder at the front.

The pilot for this contraption would lie on his stomach along the middle of the lower wing, facing forward. Thinking of the worst-case scenario, the brothers agreed never to pilot the glider together, so that if one died, the other would still be left to continue the work.

Their first attempts were remarkably successful, achieving glides of up to 300 to 400 feet at 30 miles per hour — clearly, the brothers were on to something.

> _"… for the Wrights the winds were never the enemy."_

### 4. From gliding, the brothers moved on to motorized flying. 

Despite their remarkable initial glides, the brothers were eager to continue improving their design.

To do so, Orville and Wilbur built a laboratory above their bike shop, which housed a custom-made wind tunnel consisting of a six-foot-long wooden box with an opening on one end and a fan on the other. This allowed them to test a series of different wing shapes and curvatures.

Within a couple of years, the results had helped them to produce a new and improved glider, and in August of 1902, they were ready to perform new test runs at Kitty Hawk.

The results were brilliant. Over the course of two months, they performed almost 2,000 glides, one of them spanning 600 feet.

It was obvious to the brothers at this point that they had mastered gliding — now it was time for a motor.

Unfortunately, they couldn't find anyone who knew how to build a motor light enough for their aircraft.

So they called up a friend, the mechanic Charlie Taylor, who built a custom-made, 12-horsepower motor that weighed 152 pounds. For the propellers, the brothers made their own from scratch.

The new aircraft was called the "Flyer," and it had two 8.5-foot propellers, which spun in opposite directions so as not to pull the craft to one side.

Back at Kitty Hawk, the brothers tossed a coin to find out who would get to ride the Flyer first.

Wilbur won, but during the flight, he pulled too hard on the rudder and crashed, resulting in a few days of repairs.

Then, on Thursday, December 17, 1903, it was Orville's turn. At exactly 10:35 AM, with a small crowd of five locals watching, the Flyer took off and flew for a total of 12 seconds over a distance of 120 feet, beginning a new era of motorized flight.

Even so, the Wright brothers weren't about to rest on their laurels, as more improvements were on the way.

### 5. The brothers continued making improvements, despite skepticism from the press and military. 

As they made progress, the brothers started looking for a new location for their test flights.

To save time and the costs of transportation, they found a cow pasture called Huffman Prairie in their home state of Ohio.

Since the winds weren't as ideal as Kitty Hawk's, they built a catapult to help with their takeoffs. It worked by dropping weights from a height of 20 feet, causing a sling to push the plane down a track and up into the air.

It took a few failed attempts, but before long they were making great progress, even managing a half turn to land back where they started.

Oddly enough, now that they were perfecting motorized flight, the local press wasn't interested.

James Cox, the publisher of the Dayton News, later admitted that he and his staff thought the reports of the brothers' flights were bogus, so they never followed up on them.

One reason for their skepticism was the recent failed attempt at mechanized flight by Professor Langley at the Smithsonian Institution in December 1903.

Langley's aircraft had cost $50,000 of government money, and its failure had received a fair share of ridicule from the press.

The first accurate account of the Wright brothers' achievements would come from Amos I. Root, a beekeeper and flight enthusiast, who published the results in the 1905 edition of his _Gleanings in Bee Culture_ periodical.

Undeterred by the lack of press, the brothers began thinking commercially.

With their patriotic leanings, they attempted to sell their invention to the military after filing their patent in 1903. However, despite two different proposals, the army never responded, likely due to the recent failures with Langley.

So, Wilbur and Orville turned to British and French representatives, finally signing a contract with a team of businessmen from France in December 1905.

The contract earned them $200,000 for a Flyer, which was subject to certain conditions, such as the public demonstrations that would eventually take them halfway across the world.

### 6. Commercial ventures took the brothers to New York and then to Europe. 

In 1907, the brothers received a patent for the _Wright Flying Machine_, and their business in the European market soon picked up.

German interests offered $500,000 for 50 Flyers while the deal with the French businessmen was still being negotiated.

To help in these matters, the brothers hired the New York firm Flint & Company as their sales representatives. They had experience in selling military goods to Europe, and would only take a 20-percent commission on European sales, leaving the US market for the brothers to handle.

A few months later, Flint & Co's European representative, Hart O. Berg, suggested that at least one brother should accompany him to Europe and speak to the buyers in person. Naturally, the more serious-minded Wilbur was chosen, and on May 18, 1907, he boarded the _RMS Campania_ for Europe.

On the trip, Wilbur was treated to the luxury of first-class travel. Hart O. Berg was accustomed to opulence, and once they arrived in London, the first thing Berg did was make sure Wilbur got a tailored suit from the Strand. In Paris, Berg got Wilbur a room at the New Hotel Meurice, complete with a rooftop garden and a panoramic view of the city.

Unfazed by the opulent treatment, however, Wilbur was more interested in European art and architecture and wrote home often to give his opinions on the city's buildings and museums. Wilbur wrote of his disappointment with da Vinci's _Mona Lisa_, saying he preferred the more obscure _John the Baptist_.

Meanwhile, their business in Europe was stalling, and Wilbur was eventually joined in Europe by Orville in late July of 1907, followed by their mechanic friend, Charlie Taylor.

The latest model of their plane, the Flyer III, was packed and shipped to Europe for the demonstrations.

Unfortunately, the demonstrations had to be delayed; the trio returned from Europe in November 1907 with the Flyer still stuck at customs in Le Havre, France.

> _The Wright's Patent for their Flying Machine is number 821,393._

### 7. Early public demonstrations were a great success. 

Finally, in early 1908, the brothers got some good news: the US War Department had come around and accepted their $25,000 bid for a Flyer. The only catch was that the machine had to pass their various tests.

Meanwhile, public demonstrations in France were still scheduled for the summer of 1908.

So it was back to France for Wilbur. But before that, the brothers had prepared a new Flyer at Kitty Hawk that allowed the pilot to sit at the cable controls rather than having to lie down. Plus, there would even be space for a passenger.

Wilbur arrived back in France on June 8, 1908, only to find that their Flyer had been severely damaged by the customs agents at Le Havre.

Remarkably, Wilbur did what had to do be done and essentially rebuilt a new Flyer from scratch, almost entirely on his own.

Two months later, on August 8, the new Flyer was ready, and Wilbur took to the skies before a small but influential crowd at the Le Mans racetrack. He soared for two miles, at 30 to 35 feet off the ground, making two successful half turns and landing gently.

It was a tremendous success, causing an immediate shift in public opinion.

Any skeptics in the crowd were stunned, and within 24 hours Wilbur's flight had become international news, with papers in Paris, London and Chicago heralding its success.

The whole world was put on notice as Wilbur continued giving increasingly popular demonstrations, with crowds swelling to thousands.

Back in the US, Orville was about to put on an equally amazing show at Fort Myer, Virginia.

On September 3, 1908, he flew for a small crowd of military officials, starting out tentatively, but getting more and more daring in his subsequent demonstrations. He soon became the new star of aviation, and within a couple weeks, he set seven new world records, including ones in altitude, speed and duration.

But the challenges weren't over yet.

### 8. Orville had a brush with death, but this didn’t stop the brothers. 

Thanks to his daring military demonstrations and world records, Orville spent a couple weeks outshining his brother Wilbur.

But then, on September 17, 1908, things took a disastrous turn.

At Fort Myer, Orville took off with a passenger, a distinguished young officer by the name of Lieutenant Thomas Selfridge.

Orville had already flown with two different passengers, but this time one of the propeller blades cracked in mid-flight, causing it to get tangled with a wire that controlled the rear rudders. The plane thrashed wildly in the air before diving head-first into the ground from a height of 125 feet.

Tragically, Lt. Selfridge died of a fractured skull, while Orville was seriously injured: he suffered a fractured leg and hip, as well as four broken ribs.

Orville would eventually recover, thanks to the help of his sister Katharine, who stayed by his bedside day and night.

For a while thereafter, Orville had to get around with the help of a cane. But despite this setback, the brothers would continue making history.

After postponing some demonstrations while Orville recovered, Wilbur finally resumed flights, and their success resumed along with them, with around 200,000 people showing up to watch his latest flights at Le Mans.

As part of the terms of his agreement with the French businessmen, he began training three French aviators, earning him $35,000 by January of 1909.

The brothers were given many awards in France, including the Legion of Honor. Wilbur also won the Michelin Cup of aviation after setting a new distance record of 77.5 miles.

Orville and Katharine eventually joined Wilbur in France, where they got to meet a variety of royal spectators, including King Alfonso XIII of Spain and King Edward VII of England.

Their time spent in Europe was a tremendous success for the Wright brothers, but their story's ending takes place back home in the United States.

### 9. The brothers became America's heroes, but they never stopped working. 

On May 13, 1909, Orville and Wilbur returned from Europe, $200,000 richer and with a number of prestigious awards. But little did they know that the celebrations were only getting started.

When they arrived in New York, they received a hero's welcome as a swarm of fans and reporters followed them all the way home to Dayton, where the festivities really began.

When they finally got home, around 10,000 people were there to greet them at their front porch. As it turned out, the city had organized a huge, two-day-long celebration in their honor, complete with a parade that proudly covered the entire history of the United States and Dayton.

Amazingly, to commemorate the achievements of these two men, 15 floats and 560 historically dressed actors paraded through Dayton, along with 2,500 schoolchildren dressed in red, white and blue, singing the national anthem.

All this was topped off with a trip to the White House, where President Taft presented the brothers with gold medals.

But this being the Wright brothers, they never stopped working.

Within 48 hours of the parade's end, they were on a train for Fort Myer so that Orville could finish passing the speed and endurance tests and complete the terms to finalize their sales to the US Army.

Less thrilling was a pending legal issue regarding Glenn Curtiss, a 31-year-old celebrity pilot, who was unlawfully using their patented wing-warping system. This resulted in a full-scale patent war that would go on for nearly a decade with no clear winner.

Meanwhile, aviation was reaching new heights around the world.

Wilbur kicked things off by flying along the Hudson River and circling the Statue of Liberty.

Two weeks later in France, a Russian-born aristocrat that Wilbur had trained, named Charles Lambert, flew 1,300 to 1,400 feet high over Paris and the Eiffel Tower.

Finally, at Huffman Prairie, the brothers brought their dad along to complete a personal milestone by flying together for the first time. The two brothers finally left the earth together on May 25, 1910.

Next, Orville took flight with his father, Bishop Wright, who was 82 at the time.

As the plane soared above the Ohio landscape, Bishop leaned over to his son to say one thing: "higher, Orville, higher!"

### 10. Final summary 

The key message in this book:

**The Wright Brothers' story is as remarkable as their success. Against all the odds and in the face of numerous challenges and setbacks, they managed to pioneer and master the art of flying through sheer talent and determination.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Boys in the Boat_** **by Daniel James Brown**

_The Boys in the Boat_ (2013) tells the story of how a group of unassuming college boys from the University of Washington went from struggling through the Great Depression to securing a victory in the 1936 Berlin Olympics.
---

### David McCullough

David McCullough is an award-winning author who has written some of the world's most acclaimed historical accounts and biographies. He won the Pulitzer Prize for his books _Truman_ and _John Adams_ and has received multiple National Book Awards, as well as the Presidential Medal of Freedom in 2006.

