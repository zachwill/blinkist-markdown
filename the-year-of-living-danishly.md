---
id: 5a5cab08b238e10007f154cd
slug: the-year-of-living-danishly-en
published_date: 2018-01-19T00:00:00.000+00:00
author: Helen Russell
title: The Year of Living Danishly
subtitle: Uncovering the Secrets of the World's Happiest Country
main_color: EDCE2F
text_color: 87761B
---

# The Year of Living Danishly

_Uncovering the Secrets of the World's Happiest Country_

**Helen Russell**

_The Year of Living Danishly_ (2015) is all about Denmark and why it is considered such a great place to live. These blinks take an inside look at Danish culture and society to explain why this small Scandinavian country consistently tops the UN World Happiness Report, as well as the well-being and happiness index of the EU Commission.

---
### 1. What’s in it for me? Discover Denmark’s secret to topping happiness rankings year after year. 

Over the last decade, studies ranking the world's happiest countries have consistently had a small, northern European country, mostly known for pastries and LEGO, at the top: Denmark.

With a total population surpassed by many of Europe's capitals, this nation certainly seems to be doing something right. So what is it?

In these blinks, we will look at the happiness miracle that is Denmark, and what exactly allows Danes to live in such a bubble of bliss.

You'll also find out

  * what Danes love to burn;

  * why Denmark's laws makes parents happy; and

  * how one single gene in the Danes might be helping their happiness.

### 2. Danish happiness begins with a comfortable and stylish home life. 

When most people think of Denmark, their mind will jump to LEGO, frigid winters and a confusing language. But more recently, this Nordic nation has come to be associated with a new word that Danes absolutely love: _hygge_.

While many people still struggle to explain what it means, it's actually quite simple: hygge just refers to coziness and a nice home environment.

For instance, just about everyone in Denmark likes to stay indoors with family and friends during the winter; this is their time to get hygge. The word can also be used as an adjective or verb, and essentially means having a cozy time. In other words, it's a lifestyle concept that's associated with home, people, food, drinks and cold weather.

That means a nice dinner with friends can be hygge, but so can cuddling on the sofa with your sweetheart, or a glass of wine and some candles to chase the chill of the dark, cold weather.

To ensure such experiences are ready at hand, Danish people transform their homes into environments of pure comfort. For instance, most Danes keep their houses tidy; people are expected to remove their shoes when entering a house and, to create small cozy corners, they use lots of lamps. These range from stylish ceiling chandeliers to floor and table lamps.

In addition to this electric light, they also use tons of candles for a warmer ambience. In fact, Danes burn more candles per person per year than any other European nation.

The final basic addition are blankets and cushions to help them stay warm. These vary by season and it's quite common to have different sets of cushions for winter and summer.

So, why do the Danes go to such great lengths to set their homes up for hygge?

Well, research shows that a comfortable and stylish home makes you happy. For example, a 2011 study done by the University College London. It found that simply looking at something you consider beautiful will release dopamine in your brain, making you happy. As such, having a comfortable home environment, marked by a tidy and stylish interior, is key to Danish happiness.

> _"Denmark is very much a design society, and this plays quite a big part in happiness."_

### 3. Danish people have a good work-life balance and do jobs they love. 

The European Commission recently conducted a survey that uncovered an interesting fact about Denmark: the country is home to the most satisfied employees in the entire European Union. Danes even have their very own word for their work mentality in Danish. It's _arbejdsglæde,_ which is a combination of the words for work and happiness. So, why do they feel so content with their jobs?

For starters, Danes aren't stressed out by overwork. With a 37-hour workweek, Danish companies have some of the shortest hours in Europe. In fact, Statistic Denmark puts the real average at closer to 34 hours per week.

And beyond that, working overtime isn't expected, and is actually viewed quite negatively in Denmark because it means you couldn't finish your specified work tasks in the allotted time.

But shorter hours aren't the only reason Danes like their jobs. Young people can also choose professions they enjoy, as doing so is socially acceptable and education is free. WHat's more, in Denmark, it's not about earning as much as possible. Danes actually avoid judging each other on the basis of salary and a higher-paying job really just means paying more taxes. As a result, work is about doing something you love.

Finally, Danes who don't choose their dream jobs the first time around can easily shift careers. The barriers to doing so are remarkably low in the country and they rest on a couple of social pillars.

First, Danes have a great social welfare system. So, if you quit your job, you need only wait five weeks before claiming the same benefits as people who were fired. This expanded unemployment insurance amounts to between 80 and 90 percent of your previous salary for up to two years!

Second, among the members of the Organisation for Economic Co-operation and Development (OECD), Denmark spends the most money on lifelong training. This is largely because companies, labor unions and the government all pay for employee trainings that teach new skills and make it easier for workers to take on new roles.

### 4. Danes love leisure activities, especially cycling. 

Whether your weekend passion is practicing the flute or playing racquetball, everyone knows that a hobby makes for a more relaxed and happier life. The Australian Happiness Institute even found that leisure activities increase quality of life and productivity.

So it's no wonder that most Danes have not one, but several hobbies, many of which are professionally organized into clubs and societies. Many of these clubs are actually supported by the government and are central to the country's culture.

In fact, Denmark has 80,000 registered associations, many of which receive free space from the government as well as subsidies for membership costs. There is even specific support for setting up clubs for people under the age of 25. As a result, nine out of ten Danes participate in such a club and, statistically, each Dane belongs to 2.8 of them.

If that's not beautiful enough, in these Danish clubs, everyone is treated equally, regardless of whether he's a CEO or a janitor. After all, the mutual activities that bond these groups foster a sense of community and trust among their members.

So, what kind of activities do they enjoy?

One of the most popular is cycling, a great leisure activity that can be done anywhere. Just about everyone in the country loves to ride bikes and many people choose to commute to work by bike as well, regardless of their professional status.

To support this popular passion, the government has paved over 7,500 miles of bike paths in this tiny country. There are even safety lanes that protect cyclists from cars in the busy capital of Copenhagen.

Not only that, but all taxis are required to have bike racks to take cyclists home in the event that they have a few too many drinks.

The end result of all of this cycling is really just more happiness. This connection is substantiated by scientific research too, such as a Harvard Medical School study that found that cycling increases cognitive well-being.

> _"Danes, in common with all Scandinavians, love a club, an association, or a society of some description where they can pursue a hobby._ "

### 5. Traditions and patriotism are important for most Danes. 

Traditions are a big deal in many countries — but in Denmark, they're fundamental to the local culture. What's more, these traditions contribute to the country's happiness by cultivating community and security.

One of the biggest of these traditions is confirmation, the affirmation of a child's religious faith.

This ceremony is a necessarily important moment for Danes given that 4.4 million out of Denmark's 5.5 million inhabitants belong to the Protestant state church, which celebrates confirmation as a representation of coming of age. Confirmation takes place when children are around 14 years old and is meant to confirm their belief that God is watching over them, as was promised during their baptism.

Following such ceremonies, Danes enjoy huge meals with speeches, presents and a party. It's traditional to give big gifts during such ceremonies and, according to a Nordea Bank survey, your average Danish teenager receives a total of around $3,200 from the event.

But perhaps more important is a study done by the University of Minnesota, which found that rituals allow people to enjoy more. Traditions create a sense of stability and affiliation, since they stay the same over generations, which people find comforting in a constantly transforming world.

Another vital aspect of Danish happiness is patriotism. When the International Social Survey Programme asked Danes if they thought Denmark was better than other countries, 42 percent said yes — and with so much going for it, you can't blame Danes for being proud of their country.

Naturally, one of the symbols of the nation's patriotism is its flag, the _Dannebrog_, which you see all over the country.

This symbol of Denmark is one of the oldest flags in the world, and it's displayed everywhere from office desks to outside houses and even in the background of TV broadcasts. In fact, the flag is held in such high regard that it's forbidden to display a different flag without permission.

Such a prohibition may seem extreme to foreigners, but in this instance, patriotism can be a good thing. Consider a study published by Psychological Science, which found that if you feel good about your homeland, you'll generally be happier.

### 6. Danish parents spend tons of time with their kids thanks to strong state support. 

Imagine a world in which children spend the first months of their lives with parents who are unburdened by work.

In Denmark, that's totally normal. One of the other reasons Danes are so happy is that they get to raise their children in a comfortable, fulfilling way. In fact, Danish law mandates that parents be given 52 weeks off work per child, which they can distribute between themselves however they choose.

What effect does this have?

Well, a study from the American National Bureau of Economic Research showed that a high level of maternity leave positively affects the health of a child and reduces maternal depression rates. In Denmark, many fathers take time off too, making the whole family happier.

OECD studies have even found that men in Scandinavian countries are more involved in child-rearing than at any point in the past. They spend more time at home and, as a result, form a bond with their children much more quickly than prior generations.

One researcher at the University of Missouri found that sharing the parenting and household responsibilities between partners also increases the happiness of both men and women.

So, Danish parents are happier for spending time early on with their kids. But for children, this dream doesn't end when their parents go back to work; the government also supports affordable day care for children.

Denmark guarantees day care for kids from the age of six months to six years, when they begin school. It is also incredibly affordable; a total of 45 hours of day care per week for infants under two years of age costs just $400 to $635 per month. If that's too much, there's always the option to get just 25 hours per week, and the price drops when a child turns three.

Finally, depending on their household income, Danes can get a deduction for day care or even receive it for free. Compared to other countries, the cost of day care in Denmark is incredibly low simply because the state covers 75 percent of the costs.

### 7. Danes have happiness written into their genes. 

When you think of the things that make people happy, what comes to mind?

For most people, it's factors like relationships, money and health. But what about genes?

Well, the _genetic distance_ within a population — that is, how internally similar a group's genes are — plays a role in its overall happiness. Niels Tommerup, a professor in the University of Copenhagen's Department of Cellular and Molecular Medicine, cites a study which found a significant correlation between genetic distance and well-being.

According to the research, the more similar the genes are among a population, the more content it is and the more people trust one another. After all, scientifically speaking, such populations are like one big family.

Interestingly enough, Denmark is the country with the least distance between the genes of its population, since its inhabitants didn't move much in the past. As a result, contentment is now part of the country's biological heritage.

Or take another study from the University of Warwick, which found that countries that are genetically dissimilar to Denmark report low well-being. In other words, the more similar a country's genetic pool is to that of Denmark, the happier its people are.

But while the Danish gene pool supports happiness in general, there's also one gene in particular that makes Danes happier.

The neurotransmitter _serotonin_ is one of the primary reasons people feel happiness. But having an abundance of serotonin isn't sufficient on its own. Scientists have found that our brains need the _5-HTT gene_, also known as the _serotonin-transporter gene_, to utilize this neurotransmitter.

As a result, the more of the long form of 5-HTT a person has in her brain, the better her mood will be in general. At this point, it should come as no surprise that the Danish population has one of the highest levels of this gene in the world, right alongside the Netherlands.

### 8. Final summary 

The key message in this book:

**Denmark, a nation of just 5.5 million, is consistently ranked as one of the happiest countries in the world. The key to their contentment rests on several factors, but boils down strong support for people in every aspect of their lives, a love for time spent at home and for leisure activities — and maybe even their genes.**

Actionable advice:

**In search of a hobby that makes you happy? Try learning a language.**

In addition to cycling, another favorite hobby of the Danes that surely contributes to their happiness is language learning. Numbers from the Office for National Statistics points to the happiness-increasing impact of this hobby, suggesting that lifelong learning in general increases self-confidence and mental health, and adds a powerful meaning to life.

So, do as Danish pensioners do and try out a new language during the cold months. By the time summer rolls around, you'll be ordering wine in Italian or Spanish on your next holiday!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Little Book of Hygge_** **by Meik Wiking**

Different countries, like different people, have different personalities. Some are more upbeat; some are more downcast. Out of all the countries in the world, Denmark is often rated as the happiest. And that's because Denmark has _hygge_. _The Little Book of Hygge_ (2016) explains this concept in full and offers tips on how you can achieve it, too — wherever you live.
---

### Helen Russell

Helen Russell is a British journalist and author whose work has appeared in _The_ _Times_, the _Guardian_, _The_ _Wall Street Journal_, and other publications.

