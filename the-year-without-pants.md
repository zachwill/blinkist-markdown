---
id: 54d89f0f383963000aa40000
slug: the-year-without-pants-en
published_date: 2015-02-13T00:00:00.000+00:00
author: Scott Berkun
title: The Year Without Pants
subtitle: Wordpress.com and the Future of Work
main_color: C8282F
text_color: C8282F
---

# The Year Without Pants

_Wordpress.com and the Future of Work_

**Scott Berkun**

_The Year without Pants_ offers an intimate look at life inside one of the most successful tech companies out there: Automattic, the brains behind sites like Wordpress.com and Polldaddy. By thinking outside the box, abandoning tradition and cutting the fat from your processes, you too can follow Automattic's example and jumpstart innovation.

---
### 1. What’s in it for me? Learn how to develop creative teams from the masters behind WordPress. 

Next time you're at work, ask yourself this question: "Can I be sure that my colleagues are wearing pants?" You're probably thankful that the answer is "yes, of course." But think of it this way: if you can be sure that every one of your colleagues is fully clothed, then it means that they are all with you in the office.

And that means your company is not taking advantage of flexible working.

These blinks are based on advice from Automattic, the highly innovative company behind WordPress.com. They'll show you how most of us are stuck in our traditional office practices, how we can all break free from unproductive work, and how we can really start to change (with or without pants!)

In these blinks you'll discover

  * why Automattic ship a new product every day, even if it's not perfect;

  * why you shouldn't necessarily fix something when it breaks; and

  * how to make people enjoy the office meeting.

### 2. If you want your employees to be independent, hire people you can trust. 

If you've browsed the web, you've seen the name WordPress. It's among the most popular blogging platforms available.

WordPress.com was started by Automattic, one of the most famous web developers in the world.

They're known for their awesome team spirit and flexible working environment, which attract developers from all across the world. Getting a job there, however, isn't easy.

Automattic places heavy emphasis on hiring precisely the right people for the job.

At other job interviews, you might face abstract questions designed to test your problem-solving skills, such as: How many ping pong balls can you fit into a 747?

Not at Automattic. Here they prefer to concentrate on assessing the concrete skills you'll need to do your job. So, interviewees are instead tasked with completing a small project, using the tools and techniques that would be available to them if they joined the team. This way, only the people who can actually do the job get recruited, not the ones who are best at interviews.

After recruitment, Automattic ensures that staff are immediately trained in the most important area of the business: customer support.

With any new job, people can concentrate so much on their own little world that they ignore the wider goals of the company. To counteract this, Automattic starts their employees off in customer-facing roles, where they learn that the client is all important.

The author, for example, spent his first weeks at Automattic as a _Happiness Engineer._ Each day was spent dealing with customer problems in different areas of the company. This ingrained in him the knowledge that, wherever he worked in the company, the needs of the customer would always come first.

> _"Founders often hire to solve immediate needs and simultaneously create long-term problems."_

### 3. If you want to have the most effective team, your leaders need to create the best culture. 

In 1999 a highly innovative and influential design company, IDEO, went on the popular American TV show _Nightline_ to shed some light on how they managed to be so creative.

One of the tips they revealed to the world was a brainstorming practice called "deep dive." They explained how they had used this process to develop new prototypes and products with incredible speed, such as when they redesigned a shopping cart in only five days.

Unsurprisingly, companies all across the United States tried out the deep dive for themselves. If IDEO could do it, then why couldn't they? However, most of the companies who experimented with the deep dive failed to achieve any real results. Why?

Simple: although they followed IDEO's instructions to the letter, they lacked the staff to implement their new ideas. No matter how great your business practice is, without the right _staff culture_ you can't succeed.

But how do you develop the right culture? How do you ensure your staff has the mind-set necessary to adapt to change and be innovative? It all starts with the management.

Think of the development of a staff culture as a plant growing from a seed. Leaders plant the seed by coming up with an effective culture. They then nurture it by hiring and training staff, helping the culture to grow and blossom throughout the team.

At Automattic, founders Mike Little and Matt Mullenweg established the company culture very early. They then faced a dilemma: the blogging software they were using was slow and ineffective, and the alternatives weren't open source, and thus contradicted their philosophy of sharing.

Rather than abandoning their principles, they instead created their own software — WordPress. This act cemented a culture of being true to yourself and your principles, and being innovative in order to overcome difficulties. To this day this defines the working experience at Automattic.

> _"The culture in any organization is shaped everyday by the behavior of the most powerful person in the room."_

### 4. A simple innovative process, free from overmanagement, is the best method for developing creativity. 

Here's a simple question: How do you innovate in your team? Almost every company tries to encourage innovation, but many fail in establishing an effective process.

Some organizations simply make the process of innovation too complex, placing too many barriers between ideas and implementation. Maybe you've even experienced this at your company: ideas are closely scrutinized on paper, then the prototype is evaluated and then there's a beta test. Only after the innovation has run the gauntlet can it finally be sent out into the world.

If this process sounds too slow and cumbersome, that's because it is.

If you want your company to be innovative, you need to make the process of innovation as simple as possible.

At WordPress.com something new is made available every single day. Even if it's only a minor improvement or a bug fix, every day brings something new. To achieve this, they allow their programmers and designers to release their projects as soon as they're finished. There are neither tests nor approval processes. This way ideas don't get stifled before they're even off the ground.

But won't this lead to disaster? Even if their new application or feature is faulty, it can easily be taken down, fixed and put back up again. And if customers don't find it as engaging as they'd hoped, Wordpress can just remove it later. With such minimal risk, there's no reason to hold back innovations.

There's another reason to abandon the checks and balances in the innovation process: they make employees complacent.

If employees know that their work will be thrown away if it isn't perfect, then they'll have little incentive to even try. What's more, if they are inexperienced in fixing errors, then they won't have the opportunity to learn this vital skill.

So Automattic made the process of innovation as simple as possible. It keeps employees brave and keen to try out new things.

> _"The fundamental mistake companies that talk about innovation make is keeping barriers to entry high."_

### 5. Lose the traditions that hinder productivity. 

Many businesses swear by their traditions and time-tested practices. But if our ancestors had stayed loyal to _their_ traditions, we'd still be living in caves and wearing skins!

The simple truth of the matter is that no matter how convenient and comforting traditions may be, they can often hinder productivity.

Perhaps the most glaring instance of holding on to tradition in our modern working environment is our adherence to the standard working day. So many companies require us to work at the office for eight hours a day, from nine to five. Many of us can't even show up to work without donning a suit or other "business attire."

But why do we do this? The eight-hour day is by no means the most productive. How often do you feel exhausted in the afternoons? And why do you have to wear clothes that are uncomfortable? That can't be good for your productivity!

Why are you even at the office anyway? Surely you can do most of your work virtually, from home, without the need to commute? These silly traditions simply hold us back.

Automattic is ruthless about abandoning traditions that stifle productivity.

For one, the company is flexible about working practice. Staff can work when and where they want, and choose what to wear while they do it — pants or no pants!

They've also abandoned the traditional approach to fixing things.

Standard business practice says that if something is wrong, you should fix it immediately. Automattic, in contrast, says that when something breaks, that's the perfect time to evaluate its usefulness.

In 2010, the software that enables people to blog their posts on LinkedIn broke. Instead of fixing it, the author simply left it alone. He wanted to see how many people noticed and complained about it. In the end, this particular service was missed by enough people to warrant being fixed, but if it hadn't been, they would have simply cut it.

> _"There is nothing wrong with tradition until you want progress."_

### 6. Your system should support the people who create the products. 

If you've ever worked at a large company, then you'll probably remember how massive their support teams were. In fact, it's a fair bet that the IT and HR departments were the largest in the company. But this can't be right!

The most important employees in any business are those who actually _create_ products and services. Quite intuitively, if you concentrate too much on the support staff and not enough on your creators, then your products and services will suffer in quality.

Automattic understands this. The founders realized from the outset that their company would only be successful if they concentrated their efforts on developing great teams of designers and programmers.

And of course, with their practice of having all employees learn about customer service, they ensure that the one important support role is covered.

In addition to neglecting important, productive staff in favor of support staff, managers also make the mistake of focusing too much on data.

Some managers believe that "you are what you measure," and so they spend their entire work days poring over reports and research. Any practice or person that doesn't live up to the key performance indicators and the desired numbers is cut out.

But this isn't the best way to ensure productivity. Sure, data can be a great way to assess how you're doing, but you can't let it dictate the direction of your business. You have to let your creatives do that.

For example, at Automattic they collect lots of data. They evaluate things like the number of posts published per hour, the number of comments, new blogs, the number of site visits per week and from which countries.

But they don't use this data to decide where to go next; they use it to evaluate what they're doing already.

### 7. We all hate meetings, but they are an important part of the working environment – when they’re done right. 

How many meetings do you have every week? No matter how many it is, it probably feels like too many. Meetings are one of the most frustrating elements of modern work, and quite a few of us would prefer not to have any meetings at all!

Our frustration isn't the fault of meetings per se. Rather, we get frustrated because meetings aren't conducted effectively.

If you want to have better meetings, consider the following tips.

First, never make them longer than they need to be. When planning out your meeting, make sure they are as short as possible. If no one objects then you know you've got it right.

To make sure that you fill this time effectively, only discuss those things that require a meeting to solve. Off track meanderings are the main reason why people hate meetings. Allowing trivialities and unrelated topics to creep into discussions can prolong meetings that could have otherwise been finished in only a few minutes.

Second, ensure that everyone is fully prepared for the meetings.

You don't want to waste lots of time explaining things, so make sure that everyone is fully briefed beforehand. Sometimes, during the course of planning and preparation, you may even solve problems you were due to discuss in the meeting!

If you are leading the meeting, make sure you take the meeting notes yourself. As long as you take good, honest notes, without any attempt to shift blame or paint a picture rosier than reality, then the rest of your employees will see you as both competent and trustworthy.

> _"Almost no one can convince an entire conference room of coworkers with a speech."_

### 8. Effective teams need good, healthy communication and plenty of laughter. 

In any organization there should be one goal that the entire staff aims to achieve. To achieve this goal, good communication is needed both between teams as well as within them.

For one, healthy teams need to laugh with each other, as laughter fosters connection between people. We're at ease when we laugh, and this creates trust within the team and helps strengthen relationships.

It's so effective that the author uses laughter as a method of building team spirit and trust. As he so eloquently puts it: "Laughter leads to running jokes, and running jokes lead to a shared history."

One of the first running jokes he had with his team was about ouzo. When they were in Athens they asked what the local drink was, to which the waitress replied "ouzo." Berkun said that he hated ouzo, but they bought a round of ouzo shots anyway. As they drank, they all yelled "ouzo!" with a hearty laugh.

From then on, it became a ritual every time they went drinking together.

In addition to a good laugh, you need to ensure that you and your team communicate effectively.

Most companies use email to facilitate communication among employees, but it's far from perfect.

Email creates an asymmetrical communication dynamic, where the sender has more power than the recipient. The sender decides what shows up in your inbox, and you don't get to chose what to read because email is a closed channel. And if you aren't even a recipient, then you're cut out of the communication entirely, which breeds mistrust and hierarchy.

At Automattic they overcame these shortcomings by communicating openly in _P2,_ a blog where the team members discuss and communicate important issues

On the P2 blog, readers choose what they want to read, and don't have the content forced upon them. The P2 is available for everyone to see, and all the posts remain there indefinitely.

> _"If you have people who trust each other and have similar goals, they'll be effective with smoke signals and carrier pigeons."_

### 9. Final summary 

The key message in this book:

**Companies love their traditions, but their traditions are actually holding them back. The amazing success of Automattic offers us lessons on how to abandon unhelpful traditions in favor of fun, productive new directions.**

Actionable advice:

**Take your team out for drinks.**

The fastest way to get your team to trust and value one another is to get them to laugh together. What better way than to imbibe a little?

**Suggested** **further** **reading:** ** _Rework_** **by Jason Fried and David Heinemeier Hansson**

_Rework_ throws out the traditional notions of what it takes to run a business and offers a collection of unorthodox advice, ranging from productivity to communication and product development.

These lessons are based on the authors' own experiences in building, running and growing their company to a point where it generates millions of dollars in profits annually.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Scott Berkun

Scott Berkun has written six popular books on creativity, leadership, philosophy and speaking, and is a regular blog contributor for _Harvard Business_ and _BusinessWeek_. He was formerly a co-host on CNBC's _The Business of Innovation_, and taught creativity at the University of Washington.

© Scott Berkun: The Year Without Pants copyright 2013, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

