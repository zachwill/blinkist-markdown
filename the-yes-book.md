---
id: 596169cab238e10006ad5732
slug: the-yes-book-en
published_date: 2017-07-12T00:00:00.000+00:00
author: Clive Rich
title: The Yes Book
subtitle: The Art of Better Negotiation
main_color: F2D130
text_color: 8C791C
---

# The Yes Book

_The Art of Better Negotiation_

**Clive Rich**

_The Yes Book_ (2013) is your guide to negotiating. These blinks explain how modern negotiating is all about cooperation and offer actionable advice that will keep you advancing your goals during each step of the bargaining process.

---
### 1. What’s in it for me? Learn the art of negotiation. 

Whether you're at a business meeting or rummaging at a flea market, asking a friend for a favor or buying a car — negotiation is an important part of the process. Indeed, it could be argued that an ability to negotiate determines, to a large extent, your success in life.

So it's surely good to know how to turn a negotiation into a success — or, in other words, how to reach an outcome that profits both you and the person you're negotiating with.

These blinks will teach you how to do exactly that. You'll learn when to be steadfast and when to concede, how to behave favorably and which attitude to adopt when trying to make a deal.

In these blinks, you'll learn

  * how to give coinage;

  * why an advertising company left British Rail waiting in a dirty room with stale food; and

  * why you should talk to strangers.

### 2. The changing nature of business has transformed the style of successful negotiations. 

Whether you're working on a promotion for yourself or a deal on behalf of your company, bargaining skills are essential. There are a few central skills that will have you negotiating like a pro and you're about to learn how to develop them.

But first, it's important to understand how negotiating has transformed with time.

A successful negotiation used to be described as a process of wrestling your opponent into submission. For instance, when two warring nations would meet to agree on a peace treaty, the stronger military power of the two would essentially force the other into accepting their desires. The same was true for business negotiations when a large firm wanted to acquire a smaller one.

In the past, this approach has worked just fine for big businesses, since they tended to be less interdependent than they are today. Back then, forcing a rival or smaller partner out of the game might present a small setback — say, a financial loss in one quarter — but wasn't sheer suicide.

But that's all changed. Today, losing a business partner can cause long-term damage and serious financial loss.

That's why current negotiations are all about satisfying every party involved. Just take firms like Apple and Coca-Cola, which produce, market and sell their products all over the world. This global presence means they're more dependent on the cooperation of other firms such as materials suppliers or local marketing agencies.

Because of this, they must approach negotiations in an entirely new manner; they must strive for common ground and solutions that leave everyone feeling good. Instead of simply strong-arming others, they now have to sit down with smaller companies to gain access to their markets.

But that doesn't mean bad negotiations are a thing of the past. It's still important to actively prevent deals from going sour, and that all depends on the attitude you bring to the table — a concept you'll learn about in the next blink.

### 3. Attitude is everything in negotiations and the best approach is to look for win-win situations. 

The basic attitude you bring to a negotiation will play a major role in shaping the discussion. Simply put, some attitudes are great and others aren't. Let's first discuss the ones you should definitely avoid.

The two most harmful attitudes you can bring to a negotiation are _losing_ and _using_.

The first refers to entering a negotiation with the sense that no matter what happens you'll come out the loser. Such a feeling can stem from bad experiences in the past or a sense of total powerlessness when facing your negotiating partner.

This attitude can only do harm — both to you and, potentially, the other party. After all, if you come to the table in this state, you might just say "to hell with it" and try to drag everyone else down with you. It goes without saying that this isn't a winning strategy.

The second unhelpful attitude — using — refers to a desire to reap all the benefits, while refusing to cede a single demand to the other party. In the business world of today, such an approach just won't get you anywhere.

If a company feels like it's being used, it will simply look elsewhere for business partners. Furthermore, behaving this way will earn you a reputation as a "user" and companies with whom you seek to do business will start coming into negotiations with more aggressive attitudes to protect themselves from being used. The end result can only be worse deals for you.

So rather than being a loser or a user, be a _fuser_ — a person committed to finding the best possible outcome for everyone involved.

Instead of focusing on cutting the biggest slice of the pie for themselves, fusers endeavor to create a bigger pie for everyone to share. So if you find yourself using the other side, remember that this approach will only mean missing out on creating greater value for everyone.

But how do you become an expert fuser? We will find out in the next blink.

### 4. A good negotiator is aware of her position and focused on the outcome. 

Anyone who has ever played a game of poker knows that, before betting begins, you need to determine how strong your hand is. Well, the same goes for any negotiation. So how can you determine the strength of your position?

First, you need to "count your aces." You can do this in three steps.

First, know exactly your company's specialty is. In a negotiation, you'll be the expert on your company's market position and you shouldn't let someone with an inaccurate understanding of your product walk all over you. Therefore, do your homework on your company's position.

Second, you need to remember that bigger isn't always better. If you're in negotiations with a firm that's larger than yours, you don't need to be scared of the clout they carry. Being smaller can actually be a benefit; it affords more agility, flexibility and adaptability. As a smaller company, these are your advantages.

Third, consider the knowledge you have that the other side doesn't. This might come in the form of a recent customer survey or product satisfaction study. Having new, up-to-date information could help put you in a strong negotiating position.

From there, it's key to remember what you want to achieve, which, in every single case, should be a positive outcome for everyone. Just take Will, an author. When he cut a deal with a publisher, he proposed a cover picture for the book consisting of a Moses-like figure standing in front of a wall of fire and holding the ten "Rock and Rollercoaster" commandments.

In this case, the publisher didn't exercise their veto power, even though they knew the picture was a bad idea. Instead, they allowed it to become the cover image as it would please the author and create a friendly climate..

That's the gist of fusing. Next, you'll get more in-depth knowledge about the negotiation process itself.

### 5. Good preparation and empathy will make your negotiations a smash success. 

When entering a negotiation, as when preparing for a job interview, it's crucial to have the facts straight about the other party. But that's not all the matters.

You must also prepare the ground for the setting of the negotiation.

This means fostering the appropriate climate. You can do this in a variety of ways, from getting the meeting started on time to providing your guests with a comfortable place to wait and drinks to enjoy. But sometimes you'll need to take a more creative tack, and comfort won't be your goal at all.

Just consider the advertising company Saatchi and Saatchi's approach when negotiating with British Rail. They intentionally made the representatives of British Rail wait in a dirty room with stale food until they were ready to leave, only to reveal that this was all to show how the rail line's customers felt on a daily basis — a reality that Saatchi was ready to help them transform.

This stunt drove the point home and put Saatchi and Saatchi in a much better position to negotiate.

Setting the stage is of the utmost importance, but it's also essential to notice the little clues the other party might send your way. This is a major part of being a successful fuser. But how can you decode the hidden information being communicated?

A good place to start is by turning up your empathy and increasing your observation of body language and vocal tone. Facial expressions — vacant eyes or a little smile tugging at the lips — can often give you a powerful sense of how your negotiating partner feels.

For instance, vacant eyes might mean that you're losing the person's attention, and that it's time to spice up your language. On the other hand, the beginnings of a smile might imply that she is pleased with what you're suggesting and you're on the verge of a great result.

### 6. Careful bidding and bargaining are all important when successfully closing a deal. 

As you near the end of a negotiation, it's time to get serious. The phase during which you bid and bargain can easily make or break the whole deal and it's essential that, before making your own bid, you ask the other party what _they_ want.

Doing so will make them feel both heard and like their position is being taken seriously. Not just that, but putting your needs first could damage the mood or, if the other company is smaller, make them feel steamrolled.

That being said, there are times when bidding first can be to your advantage. For instance, if you want to buy a necklace that you think is worth $500, you might open with a bid of $200. The seller, who wants a price of $800, would normally start his offer around $1,200 to prevent the negotiation from dropping below his desired price. However, your low starting bid might cause the seller to hesitate and open with a lower initial offer of, say $900. This will start him off much closer to his bottom line while you still have plenty of room to negotiate with.

From there, once bids have been put on the table, remember that bargaining might take place before an agreement is reached. It's important that you to come prepared for this phase with a list that clearly details which points you're willing to concede and which are absolutely non-negotiable. Such a list will help ensure that the negotiation ends profitably for you.

But remember, you can't win everything and there will inevitably be points that you lose during bargaining. If these are pieces you absolutely need, don't be afraid to walk away from the deal and find a new partner.

And finally, once everything is looking good, let the deal close. If bargaining is taking too much time and you feel that the deal is ready to go, just accept it flat out and avoid making final concessions as an act of goodwill.

### 7. Body language and practice are essential to skilled negotiating. 

Now that you've got a picture of what a successful negotiation looks like, it's time to have a closer look at the subtleties of making a deal. Two major ones are behavior and body language, both of which are crucial to setting the proper climate.

For instance, if your goal is to achieve a sense of cooperation, you should ask the other side what they want, and then talk about all that you can accomplish by working together. Your body language and tone should communicate your excitement and engagement.

Don't speak in a monotone or let your shoulders slump; you should be excited, speaking faster than usual and smiling broadly. These signs of excitement will show the other party that you're happy to be speaking with them.

But if things are going differently than you expected and a behavioral change is called for, definitely make it. For example, if the climate becomes explicitly hostile, don't hesitate to switch from a welcoming tone to a challenging one. Failing to do so will signal to the other party that you're easy prey.

One sign of such a shift could be that your opponent is squaring up her shoulders and taking an aggressive stance. If she does so, don't be afraid and don't back down. By squaring up yourself and looking her straight in the eyes, you'll show that you're ready for a fight.

And remember, you don't need to be in a negotiation to practice your skills; you can actually hone your technique in a variety of everyday situations. So if you're having a hard time in business negotiations, try opening a conversation or negotiation outside of work.

Just go to the nearest farmer's market and, rather than accepting the listed prices for vegetables, engage the vendors in negotiations to reach a better price for yourself. Such practice will build your self-confidence without incurring any risks for you or your company.

### 8. Final summary 

The key message in this book:

**Successful modern negotiation is all about achieving the best result for everyone involved. By paying close attention to the way you negotiate, you can come to the table with an improved attitude and set a climate that results in a winning deal for all parties.**

Actionable advice:

**Change your inner language.**

Feeling insecure about your skills as a negotiator will set you up for failure. So when preparing for a meeting, try having a conversation with your inner self that's positive and supportive. Reflect on why you think others will take you seriously and figure out why you believe it. Acknowledge the progress you are making and, before you know it, you'll be walking into the negotiating room with a can-do attitude.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Negotiating the Nonnegotiable_** **by Daniel Shapiro**

_Negotiating the Nonnegotiable_ (2016) offers insights into a new framework that can be applied to solve stubborn conflicts in both our personal and professional lives. The blinks emphasize the importance of the "tribal mind," while also illustrating how we actively address emotional pain and examining the role of identity in conflict resolution.
---

### Clive Rich

Clive Rich is a digital-media and entertainment lawyer, mediator, arbitrator and creator of the app Close my Deal, which helps people hone their negotiation skills.

