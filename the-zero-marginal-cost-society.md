---
id: 5534fdaa6638630007960000
slug: the-zero-marginal-cost-society-en
published_date: 2015-04-22T00:00:00.000+00:00
author: Jeremy Rifkin
title: The Zero Marginal Cost Society
subtitle: The Internet of Things, the Collaborative Commons and the Eclipse of Capitalism
main_color: 88A4B8
text_color: 4F5F6B
---

# The Zero Marginal Cost Society

_The Internet of Things, the Collaborative Commons and the Eclipse of Capitalism_

**Jeremy Rifkin**

_The Zero Marginal Cost Society_ (2014) lays out a strong case for the self-destructive nature of capitalism, demonstrating how it is sowing the seeds of its own destruction. But in its wake, a new, collaborative, democratized economy will materialize — one made possible by the internet.

---
### 1. What’s in it for me? Discover how new technology is threatening capitalism and leading the way toward a more democratized economy. 

Business magazines today are full of stories about companies that went from pet project to multimillion-dollar business in just a few short years.

How is this kind of growth possible?

As these blinks will show you, new technology is rapidly lowering the cost of production and distribution. This new technology, however, isn't only changing the way businesses make money; it's upending many traditional assumptions about who produces and who consumes, not to mention democratizing markets, and even threatening to erode the very foundations of capitalism.

In these blinks you'll learn

  * why more effective production methods aren't as good for business as you might think;

  * why capitalism is its own worst enemy; and

  * how the internet will help change energy markets all over the world.

### 2. As new technologies replace human labor, companies may find themselves in trouble. 

Ever since the process of automation began, people have asked themselves: Will there come a point when machines do _all_ our work for us? It's an appealing thought, for sure. And in our capitalist system, companies have indeed embraced technological innovations in order to cut costs and improve productivity.

But as this trend continues, we have to ask: How will the automation of labor affect our society? Is there such a thing as _too much_ automation?

Well, if the current state of affairs is any indication, things could get pretty dire. As sophisticated technologies such as software and robotics replace human workers, unemployment rises. And this has a cascading effect on the rest of the economy.

For instance, in 2011 alone, the sale of robots grew by 43 percent in the US and the EU. Companies like Walmart and Costco are already employing self-checkout terminals, and Walmart alone installed another 10,000 terminals in its stores in 2013.

By 2050, hundreds of millions of people will have found themselves out of a job, having been replaced by robots or software.

But this isn't some far-off future. It's already begun: although global production increased more than 30 percent between 1995 and 2002, twenty-two million manufacturing jobs disappeared around the globe in the same timeframe.

Nor is this limited to unskilled labor. Even highly qualified workers with immensely specialized knowledge, such as radiologists, may end up being replaced by pattern-recognition software over the course of the next few decades.

And this sudden upsurge in unemployment will have serious consequences for the economy, because people who earn less also consume less.

If companies produce more thanks to automation, then they'll also need customers to buy more. But, of course, unemployed people have less money to throw around. So if more and more people become unemployed, then fewer and fewer goods will be consumed.

In essence, these cutting-edge companies may end up shooting themselves in the foot!

### 3. Capitalism sows the seeds of its own obliteration. 

There's an old joke about doctors: if they're too good at their jobs, then there won't be enough sick people to keep them employed. The same thing could be said about capitalism: if it performs too well, its success might mean its collapse.

Capitalism is creating its own doomsday scenario by making certain assumptions about the economy.

The first assumption is that, in a free market, supply and demand will balance each other out. In other words, if the demand for a certain good goes down, then suppliers will lower the price for that good.

For instance, if Russia refuses to import foodstuff from Western countries, overall demand for Western foodstuff decreases. As a consequence, Western suppliers have to decrease prices for groceries even in their own countries in order to stimulate consumption.

Conversely, they'll raise their prices if there's a rise in consumer demand.

Capitalism's second major assumption is that a competitive market leads to increased productivity.

In order to compete under capitalism, companies must utilize technologies that allow for increased production at decreased cost. Eventually, as production technology improves, the _marginal costs_, i.e., the cost to produce one _additional_ unit of a given good, approaches zero.

To illustrate this, imagine you run a printing press. For each book you produce, you have to consider a number of costs — materials, utilities, labor, storage for your books, distribution, and so on. Each book you produce generates additional costs.

Being the savvy businessperson that you are, you decide to switch to e-book distribution. Now, apart from purchasing the rights to the books you distribute, your production costs and marginal costs are negligible.

The capitalist system will collapse if it continues on this path. Think about it: as productivity increases, supply increases. Any increase in supply causes demand, and thus prices, to plummet.

Eventually, prices will fall so steeply that enterprises will be unable to generate enough revenue to survive. In fact, many businesses that relied on print have already been forced out of business by e-book distributors.

> _"What's undermining the capitalist system is the dramatic success of the very operating assumptions that govern it."_

### 4. The universal tendency toward disorder places limits on capitalist concepts of growth. 

Concentrated energy is present in all objects in the universe, from pianos to trees to resources like a piece of coal or a cup of water. But it doesn't want to stay in one place. According to the second law of thermodynamics, energy has the tendency to dissipate — that is, it becomes unavailable. That's why objects disintegrate over time.

For example, we harness the power of rivers as they fall through turbines positioned on dams. But, eventually, the water level will be equal on both sides of the dam, and there will be no energy to harness.

Economic activity also harnesses energy from natural resources to create goods. Once used, however, this energy is unavailable for other uses.

Interestingly, economic activity works against nature's tendency toward disorder. As an example, imagine you have 10,000 component parts to build a piano — strings, wood, keys, etc. Without your intervention, these component parts won't randomly form an ordered object, i.e., the actual piano. They'll just lie there in a chaotic heap.

To build the piano, you must use energy. But if you do that, the energy from the materials — the wood for the body or the metal for the strings — can't be used for other projects.

In creating this beautiful, ordered object, you've simultaneously created disorder, because you've destroyed resources in the process of producing it.

This highlights the inherent conflict between capitalism's tendency toward continuous growth and the limited supply of resources on the planet. Remember: in a healthy capitalist market, productivity is always increasing, meaning that it uses more and more resources.

But these resources are themselves limited: a point will come when there won't be sufficient energy to support continuous growth.

Capitalism won't be there to organize our society indefinitely. So how will we produce, consume and exchange goods in the future?

> _"The capitalist era is passing — not quickly, but inevitably."_

### 5. The emerging economy – enabled by the internet – is one of democratized access. 

The internet has brought us much more than Facebook and Lolcats. It is also the birthplace of an entirely new kind of economy.

In the new technology-powered economy, control is more evenly distributed, and people are no longer limited to being consumers.

Under capitalism, a few big players control the lion's share of a society's assets.

Historically, the costs of producing and distributing anything, from books or records to utilities like electricity, were simply too high for ordinary people. As a consequence, companies had a big say in _what_ books and records would be published, _how_ energy was produced, and so on.

But in the twenty-first century, the internet serves as the foundation for our emerging economy, and unlike physical infrastructure, much of the internet is controlled by the masses.

In the new economy, virtually everyone has the chance to become a producer or a provider. You no longer need capital to distribute content. Today, a third of the world's population is already distributing their own photos, films, music or writing via the internet.

They have become _prosumers_ — people who simultaneously consume and produce — and they trade their goods in peer-to-peer businesses or share them for free.

In the new economy, the focus is on sharing and procuring access to goods, not owning them. Prosumers would rather stream music via the internet than buy a tangible CD.

This new class of prosumers is also very mindful of the negative impact traditional consumption has on the environment. Moreover, they don't want to spend their money on things that can be enjoyed for free.

Finally, the abundance of varied online content is available for consumption without taking up precious space in your apartment.

As we'll see, the internet as we know it today — let's call it the _Communication Internet_ — is part of a larger set of interconnecting internets, all of which play a central role in our emerging economy.

> _"A new economic model is emerging in the twilight of the capitalist era."_

### 6. The green energy market, powered by the internet, will revolutionize energy conservation. 

As new technologies are introduced into society, economic power sometimes falls out of one industry's hands and into those of another. As the green energy sector emerges, a growing portion of the energy market will fall to the prosumers instead of monolithic electric companies.

We're seeing the emergence of the _Energy Internet_.

Ten years ago, a handful of electric companies dominated the European energy market. Today, more and more people are generating their _own_ green energy.

Worldwide, governments have created favorable feed-in tariffs to foster renewable energies. These tariffs encourage people to install things like windmills, geothermal generators and solar panels to produce their own electric energy and feed it into the grid.

In 2011, in Germany, for example, individuals owned as much as 40 percent of the renewable energy capacity. They also owned almost half of the country's wind turbines.

This emerging Energy Internet is closely connected to the existing Communication Internet.

For example, many countries are implementing _smart grids_, intelligent energy networks that monitor the flow of energy throughout the grid and adjust to changes in energy supply and demand.

In the future, every building and every device will be equipped with sensors and then connected to the internet, feeding information on electricity consumption to the millions of players in the Energy Internet.

With this huge wealth of information, the network will learn about our complex usage patterns, and adapt. As a practical example, during a heat wave, when people crank up their air conditioning, washing machines might automatically shorten their rinsing cycles to conserve energy.

But back to the present: In 2012, Facebook, the National Resources Defense Council, Opower and 16 utility companies launched the Social Energy App, which ranks your energy consumption by comparing it to that of similar households and to that of your Facebook friends. 

The platform allows users to exchange energy advice, so that savvy energy savers can share their conservation knowledge with the world.

> _"In the new era, we each become a node in the nervous system of the biosphere."_

### 7. If companies are willing to cooperate, the internet provides tools to streamline logistical operations. 

Every day companies around the world innovate to bring new, highly complex, revolutionary products to the market. But when it comes to getting those products from point A to B, there is still massive room for improvement.

Indeed, the logistics are still very inefficient, and companies make poor use of the available resources for storing and transporting goods.

In the US, for example, the average trailer truck on the road is only 60 percent full. Even trucks without cargo travel for miles on end. It's a huge waste of resources.

Adding to the problem is the fact that goods are stored in warehouses far from their ultimate destinations. It's a system that relies on enormous centralized warehouses and distribution centers, meaning that products have to be shipped in circuitous routes.

One consequence of these logistical inefficiencies is that many time-sensitive goods, such as food, simply go to waste because distributors aren't able to deliver them in a timely manner.

Yet, if companies were willing to share storage spaces and distribution routes, they could use the internet to make these processes much more efficient. In other words, they could help to create a _Logistics Internet_.

A great place to start would be with warehouses and distribution centers. There are 535,000 in the US alone — so why can't they be shared among all suppliers?

Such an open supply web, aided by sophisticated software, could be coordinated via the internet in a way that would drastically reduce inefficiencies.

As a result, each participating company could use this system to store goods and route shipments as efficiently as possible.

The rise in productivity would be dramatic — as would the reduction of fuel consumption and carbon dioxide emissions!

The necessary components — the internet, GPS and sophisticated optimization software — all exist today. Companies simply need to agree on a shared logistics system with standardized procedures and make their resources, i.e., their storage space, available to other participants.

### 8. 3D printing decentralizes production systems, which are more democratic and better for the environment. 

Since the Second Industrial Revolution, mass production has been the prevailing method of creating goods. During the last fifty years, factories have grown into gigantic organizations, and control over the manufacturing sector is wielded by the uber-wealthy few.

Yet, thanks to new technologies, regular people are now better equipped to become producers and entrepreneurs.

3D printing, for example, is redistributing and democratizing the production process. Thanks to 3D printing, more people can afford to become manufacturers, which, in turn, will lead to increased production efficiency and decreased production costs.

Today, you can purchase a high-quality 3D printer for $1,500. Like all other technologies, they will only become more sophisticated and less expensive over time.

Moreover, the pioneers of 3D printing made sure that the necessary software for 3D printing remains open-source, meaning no single company can have total control over the 3D-printing production process. Adding to this, hobbyists have organized networks to exchange their 3D-printing expertise, and tutor each other for free, which further reduces the cost of printing products.

And because 3D-printed products can be sold for less, 3D-printing enterprises have the competitive edge over traditional manufacturing enterprises.

Compared to traditional manufacturing, 3D printing is also much better for the environment. In part, this is due to its resource efficiency.

In traditional manufacturing, raw materials — trees, for example — have to be chopped down and cut to size before they can be assembled into products, and those parts that have been shaved and trimmed from the resource often go to waste.

In contrast, 3D printers are able to use the _exact_ amount of material necessary to produce the end product. To be exact, 3D printing consumes just one tenth of the resources used by traditional manufacturing methods.

The production process is also more energy-efficient, as it requires fewer production steps. Moreover, 3D printing allows people to produce a good wherever it's needed. So less energy is spent on transportation, because all you have to transport are the raw materials.

> _"Earth provides enough for every man's need but not for every man's greed." — Mahatma Gandhi_

### 9. Education will undergo a fundamental transformation as we steer toward a more collaborative community. 

For a long time, sharing knowledge among students was considered cheating. School was competitive, and students were expected to fend for themselves. But in the schools of the coming _Collaborative Age_, sharing information will become the norm.

In modern education, learners are becoming more interconnected, and academic disciplines are also bridging the gaps that once separated them.

This interconnectivity is made possible by the internet. Right now, 117,000 teachers are sharing open-source curricula via the internet.

Online, teachers can also meet up and co-create curricula on learning platforms called _Collaborative Classrooms_.

One such platform is the global classroom project _Skype in the Classroom_, which aims to connect one million classrooms around the world. It's off to a good start, with more than 60,000 registered teachers!

In physical classrooms, pupils are encouraged to work in groups and share their knowledge among themselves. With this increase in shared knowledge also comes an increase in interdisciplinary academic offerings for students.

Furthermore, modern schools are becoming significantly less authoritative than traditional ones.

Traditionally, schools subjugated children and treated them as learning machines. More than anything else, they were forced to follow commands and perform well; learning was less self-directed and more top-down.

But in new schools, teachers act as guides who try to steer and facilitate their students' progress. For instance, instead of attending long lectures, students may be encouraged to learn through a self-directed science project.

In contrast to the past, students are increasingly encouraged to critically question our existing knowledge, rather than treating it as objective and unquestionable.

The new collaborative, decentralized conception of education in many ways mirrors the emergent zero marginal cost society. Both reflect the values of a world where resources are easily accessible, where hierarchies vanish and collaboration is king.

### 10. Final summary 

The key message in this book:

**Capitalism as we know it is waning. Soon, it will be supplanted by an egalitarian economy, where collaboration is king and people have nearly free access to the goods they need. Today, we're witnessing the exciting transformation from the "old way" to a democratized economy.**

**Suggested further reading:** ** _The_** **_Second_** **_Machine_** **_Age_** **by Erik Brynjolfsson and Andrew McAfee**

_The_ _Second_ _Machine_ _Age_ examines how technological progress is drastically changing our society, and why this development is not necessarily positive. It compares the rapid development of computer technology to the advent of the steam engine, which once catapulted the world into an Industrial Revolution.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jeremy Rifkin

Jeremy Rifkin is one of the most popular thought leaders of our time. In addition to advising leading companies, heads of state and even the European Union, he is the bestselling author of no less than 20 books, including _The End of Work_ and _The Empathic Civilization_.

