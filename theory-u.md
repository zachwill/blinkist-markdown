---
id: 531f6a576166310008000000
slug: theory-u-en
published_date: 2014-03-11T10:31:47.000+00:00
author: C. Otto Scharmer
title: Theory U
subtitle: Leading From the Future as it Emerges
main_color: F1BC30
text_color: A68121
---

# Theory U

_Leading From the Future as it Emerges_

**C. Otto Scharmer**

Today, there are major societal shifts underway, each of which creates major challenges for the leaders of today and tomorrow. _Theory U_ (2016) describes how the so-called U-process can be used to tackle those challenges collectively and implement the solutions quickly. This is achieved by tapping into the blind spot, the source of our deepest creative instincts.

---
### 1. What’s in it for me? Learn how you can solve problems by tapping into your own deep, unique source of creativity. 

Society is currently facing tremendous challenges such as overpopulation and political conflicts. To face them, leaders must not only look to the past, but learn from the future as it emerges.

To learn from the future as it emerges, leaders must lead themselves and their organizations through a U-process, first descending to the source of their deepest creativity and then coming back up to turn their ideas and insights into reality.

In _Theory U_, you'll discover how you can connect with your future self and learn from it rather than from the mistakes of the past.

You'll also find ways in which you can tap into your blind spot, the deepest source of your creativity.

Finally, you'll also learn how the U-process could make, for example, healthcare services warmer and more human while simultaneously increasing efficiency and the satisfaction of all parties involved.

### 2. Society is experiencing three revolutionary shifts that present it with great challenges. 

There are several trends in the world today that seem to be leading to imminent disaster. For example, although the economy is thriving, the number of poor people in the world is rising. Similarly, although we're investing heavily into agriculture, we're only using unsustainable mass-production methods to produce our food.

These trends arise from three major shifts occurring in society.

First, there's the technological and economic shift of a globalized economy. Since the collapse of the socialist economies, the whole planet has, for the first time ever, been adhering to similar economic policies like downsizing governments and privatizing state-owned industries.

This shift presents us with the challenge of how to make the economy more equitable to all, including future generations.

Second, there's the shift in international relations as powerful global institutions like the United Nations and the World Bank emerge.

This challenges us, as we need to find a way to deepen democracy and evolve our political institutions so that everyone can directly participate in the decision-making processes without supranational institutions overruling them.

Finally, there is a cultural and spiritual shift in the way we see and interact with the world. Non-governmental organizations and civil groups have emerged as global actors, having achieved nonviolent societal transformations like the civil rights movement of the 1960s and the peace and human rights movements of the 1980s. Also, people are increasingly interested in spiritual topics like personal mastery and flow. One study found that including a spiritual component in employee training significantly raises their productivity.

This presents us with the challenge of how we can start seeing every human as being on a journey — a journey to become their true, authentic self.

Because of these major shifts, many leaders and key decision-makers now find themselves feeling lost and powerless.

### 3. To be effective leaders, we must access our “blind spot” – the source of our creativity. 

Today's challenges place enormous pressure on leaders. To understand how they might overcome them, let's take a look at three perspectives on creativity and leadership. Consider the work of an artist:

  * There's the _thing_ that results from her creative process — this is the _what._

  * There's the _process_ of painting that happens _while_ the work is created — this is the _how._

  * Finally, there's the moment as she stands in front of a _blank canvas_. Here, you look at the work _before_ creation begins and focus on the _source_ of her inspiration.

An analogous division can be seen in leadership:

  * You can focus on _what_ the outcomes of leaders' actions are.

  * You can look at _how_ leaders lead, observing the processes and strategies they use.

  * Or, finally, you can ask what the source of their leadership decisions and creativity is. This source is usually overlooked, and is therefore known as the _blind spot_.

To be an effective leader, you must be able to access your blind spot, because it will allow you to _learn from the future as it emerges_.

To grasp what this means, you must first understand that everyone has two selves: one connected to the past and the other connected to future aspirations.

The author experienced this divide when, as a young boy, he came home from school one day to find his parents' farmhouse burned to the ground. His home and the things he had been attached to had suddenly been taken away from him. In effect, his past self had vanished. But then he became aware of another self, one which he could bring to life through his own actions: his future self.

You can learn from both of these selves: learning from the past is what schools teach, for example: you make choices that avoid the mistakes of the past. But you can also learn from the future as it emerges, meaning you make decisions based on what you aspire to be in the future.

### 4. The path to deep creativity is U-shaped: dive down to the blind spot and come up again with new ideas. 

Imagine, for a moment, that your consciousness is a lake. In the humdrum of daily life, you usually stay on the surface of that lake without indulging in deeper introspection or reflection. But the blind spot, your source of deep creativity, is at the bottom of that lake. And to find the truly innovative ideas needed today, leaders must be able to tap into the blind spot.

So how can you dive down to the bottom of the lake?

First, you need to open your mind and heart to new ideas. In everyday life, most of us develop fixed, predictable ways of thinking, like mental ruts. These patterns show that we always tend to reproduce past behavior.

But to reach the blind spot, it's important to not let the past color your perceptions of others, and to refrain from making judgments. If you go even further and fully imagine yourself in someone else's shoes, you get a fuller understanding of their experiences and can thus experience the world more richly. This is essentially a deep dive toward your blind spot.

Once there, you'll find that your two selves — the past self connected to your experiences and the future self connected to your hopes and dreams — can converse. This phenomenon is known as "presencing" and it results in genuinely new and innovative ideas.

Finally, you must swim back up and bring the innovations you found at the bottom to life. First, crystallize your visions and define clearly what exactly it is you want to achieve.

Next, start to realize your ideas in small steps. Don't start with a grand commitment or a big public speech. Instead, test out your ideas incrementally.

And so, the way to tap into your blind spot for the most creative and innovative ideas is U-shaped: you go down, you "presence" and you come back up to turn your ideas into reality.

Read on to find out how you can use the U-process in practice.

### 5. The first step in going down the U is to listen to others and seek out knowledgeable opinions. 

To start on your journey down the U, the first step is to gather information with an open mind.

Listen to the people around you without judgment. Remember that all great ideas need a trigger.

For example, the author was once approached by a student to teach a class on corporate social responsibility. At first, he refused due to a full schedule, but after some consideration, he realized the class was _exactly_ what he wanted to do and he changed his plans to accommodate the class. If he had not been open to the student's idea, he would have missed this great opportunity.

To ensure you don't miss any great ideas, you should practice putting yourself in other people's shoes. Take four minutes each evening to review your day, but imagine you're looking at yourself from someone else's perspective. Pay attention to how you interacted with others and what people wanted from you or suggested you do. Don't judge what you see, just observe.

But to descend down the U, it's not enough to listen passively: you also need to actively approach people and talk to them. This is valuable information gathering.

Whatever the problem or topic is that requires you tap into your blind spot, find and talk to the people who have a lot of knowledge and experience in that field. But remember to also talk to the less-visible individuals who are often ignored — they can be crucial.

For example, if you wanted to find ideas to reform the education system, you should talk to knowledgeable and important people like principals, but also to the students, who may not normally have a say.

Here too, it's important to stay open to suggestions and listen to what the people really tell you, without judging them.

This type of listening will help you understand the problems people face and see possible solutions to them. Which helps you dive deeper down the U.

### 6. At the bottom of the U, you need to listen to your blind spot and find out what’s really important to you. 

While the advice you get from others is very valuable, listening to your blind spot is even more important.

Life is a journey where you must follow the feeling you get from your blind spot, that deep source of creativity unique to you and the life you've lived. Trust that feeling more than the advice you get from others.

To help you listen to your blind spot, adopt a practice of intentional silence. Many great thinkers and leaders do this in their daily lives.

For example, you could adopt a morning practice where you rise early and spend some time in a quiet space. You can meditate, pray or just practice internal silence, but also try to think about what brought you to your current place in life. Then, make a commitment to what you want to accomplish on this particular day. In this silence, you will find it easier to listen to your blind spot and what it tells you.

The blind spot will help you understand what's really important to you and evaluate whether it's worth the cost of pursuing it. Ask yourself: What situations in your life are the ones when you feel most connected to your source of energy and inspiration?

Now think of those situations and activities as seeds. What would a future look like where those small seeds have grown into an inspiring forest? What would nurturing those seeds into a forest demand, and what would you need to give up for it? And what if, despite all your work, you were to fail in bringing about the forest? Would you think it had been worth the risk?

By asking these questions and listening to your blind spot for the answers, you'll find out which projects and goals are really important to you and worth pursuing.

### 7. Come back up the U: make your vision crystal clear and bring it to life by testing it. 

Once you've found creative ideas for your projects in your blind spot, you must swim back up and turn your ideas into reality.

The first step is to crystallize your ideas and your vision, meaning that you must define them clearly and unambiguously for the implementation phase. To crystallize your vision for the future, you can use this exercise:

First, focus on where in life you wish to be in the future.

Second, think about where you are at this very moment.

Finally, in the present, find the seeds for the future you wish to achieve and develop those seeds. For example, perhaps the future you envision demands you develop some capabilities or acquire some resources?

Next, to get the ball rolling you must set priorities and manage your time. You should start each morning by asking yourself, "What are the one or two most important tasks for today?" This allows you to prioritize those things and ignore unimportant distractions, resulting in important headway each day.

As soon as it's possible, you should test your idea. Make a prototype and present it to others before your idea is even fully developed. This lets you gather feedback from others and refine your assumptions about what direction the project should take. Remember, though: prototypes are not meant to be successes, they're merely meant to teach you things, so embrace all kinds of feedback.

If you have multiple ideas for projects to pursue, you need to decide which one you'll prototype first. To make this choice, you must ask yourself three questions:

  * Does it matter to the people involved?

  * Is it a new idea?

  * Can it be done quickly and on a small scale?

If the answer to all three is "yes," then that project is a strong candidate to be prototyped. Such projects will likely attract lots of supporters, making it more likely to be successful.

Next, you'll find out how exactly the U-process can be used to lead a group to solve a real-world problem.

### 8. Case study: Doctors and patients used the U-process to improve emergency care in Germany. 

****In a rural area near Frankfurt, emergency care services had been in a bad way. Both doctors and patients felt unhappy with how cold and mechanical the approach to care was, where patients were treated as mere biological machines that were broken and needed to be fixed.

To help reform this system, the author moderated a meeting between physicians and patients, structured around the U-process.

First, he led the group down the U by asking them to really talk to each other and listen to what everyone had to say. He managed to draw people into first-person stories told by others so they better understood the big picture. Quickly all groups realized that they actually shared the same problem.

Once at the bottom of the U, the group began coming up with ideas together. They moved away from polite conversation and into a genuine dialog and collaborative problem-solving. This intense dialog allowed them to access their blind spot, and they realized that they all shared a similar vision of the solution to their problems: physicians acting more like lifestyle coaches than healthcare-dispensing machines.

Finally, the author led the group back up the U to make a concrete implementation plan and put it into action. Based on the ideas they had generated, the group prototyped and then institutionalized a new, cheaper, more effective emergency care process. Today, emergency calls are transferred to a central call center instead of individual doctors, increasing efficiency and reducing the workload of some doctors. Rather than treating every emergency call as an emergency, physicians are now able to also provide counseling, comfort or a home visit as needed. This has resulted in more satisfied patients and more satisfied physicians, as the process has a human touch to it.

This illustrates how effective leaders can use the U process on an organizational level to come up with new, innovative solutions to problems and then implement them rapidly.

### 9. Final Summary 

The key message in this book:

**To counter the challenges of tomorrow, future leaders cannot simply learn from the past. They must learn from the future as it emerges, and this means they need to tap into their blind spot: the source of all their creativity, where their past and future selves meet. To reach their blind spot, leaders can use the U-process, which leads them down to discover great ideas, and then brings them back up so they can be implemented.**

Actionable advice:

**Start your days with silence.**

Make time in your mornings to connect with your deep source of creativity by, for example, meditating in silence. During this time, don't allow any outside stimulus like, the radio, the TV or the internet, to disturb you, as it kills the very inner silence you want to cultivate.

**Prioritize your day to make headway in important projects.**

The first thing you do each morning should be to ask yourself what the one or two most important things you want to get done during the course of the day are. Then ruthlessly prioritize those tasks. For example, ignore incoming messages until those tasks are done. This strategy means that every day you can at least be sure you got the tasks done that mattered the most, and thus made headway in your work.
---

### C. Otto Scharmer

Otto Scharmer is a senior lecturer and chairman of the IDEAS program at the Massachusetts Institute of Technology. He has worked with several governments and prestigious companies like Google and Fujitsu to create innovation programs for them.

