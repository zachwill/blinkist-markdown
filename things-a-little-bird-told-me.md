---
id: 5385ec7a6162380007ca0000
slug: things-a-little-bird-told-me-en
published_date: 2014-05-27T14:08:36.000+00:00
author: Biz Stone
title: Things a Little Bird Told Me
subtitle: Confessions of a Creative Mind
main_color: 3AB0DE
text_color: 267391
---

# Things a Little Bird Told Me

_Confessions of a Creative Mind_

**Biz Stone**

_Things_ _a_ _Little_ _Bird_ _Told_ _Me_ is the story of how Biz Stone came to co-found Twitter, and explores the invaluable insights that led him there. Encouraging others to be daringly self-confident and focused, and simultaneously aware of how they can help others, Stone proposes a new capitalism for the web age.

---
### 1. What’s in it for me? Lessons on creativity, success and failure from the founder of Twitter. 

When Biz Stone and Evan Williams conceived the idea that would ultimately become Twitter, no one imagined it would become the success it is today — not even them.

Who could've imagined that one day all members of Congress — not to mention the President of the United States — would have their own Twitter account?

And who could've envisioned that Twitter would be the central tool in accelerating the revolution in Egypt?

In _Things_ _a_ _Little_ _Bird_ _Told_ _Me_, Twitter co-founder Biz Stone addresses the question: Just how did Twitter become the phenomenon it is today?

Stone recounts many entertaining stories from his career, sharing lots of key insights and lessons he learned during Twitter's inception and development.

In these blinks, you'll find out

  * how to unlock your creativity — by limiting yourself,

  * how imagining the absolute worst-case scenario will increase your chances of success,

  * that "hashtags," "retweets," and even the word "tweet" itself actually came from Twitter users,

  * how Twitter helped a college dean to rescue one of his students from an Egyptian jail and

  * how Twitter's founders got Mark Zuckerberg to offer to buy the company for $500 million — and why they turned him down.

> _"Inventing your dream is the first and biggest step toward making it come true."_

### 2. Limitations inspire creativity by forcing you to look for innovative solutions. 

Think back to your school years when being faced with a blank sheet of paper and a pencil presented so many possibilities that you simply had no idea where to begin.

It's only when the teacher would offer a direction, like "draw a rainbow" or "write a short story," that the ideas would start to flow. Though it may seem counterintuitive, such limitations can often be a springboard for creativity.

Why?

Because whenever there are limitations on a task we try to perform, we have to abandon our usual approach. To come up with a solution to a problem, we are forced to get creative.

Everyone has to deal with such constraints — even people who are the most accomplished in their field. Take filmmaker Steven Spielberg, for example, who'd originally envisioned using an animatronic shark for the hit movie _Jaws_. The constraint was a financial one: the shark was too expensive: a limitation that forced him to be creative.

His solution? To avoid directly showing the shark by shooting from the shark's point of view.

The result? _Jaws_ ended up being far scarier than it would've been had Spielberg been able to use an animatronic shark, and the film became a megahit. Furthermore, the effect was so terrifying that Spielberg's POV technique subsequently became very popular in horror films.

This principle — that constraints inspire creativity — can also be applied to products, like Twitter, and their users.

Twitter's users are limited to 140 characters per tweet, which encourages them to find ways to express their ideas concisely.

It's even considered a valuable skill to be able to craft a Tweet that's _exactly_ 140 characters long — known in the Twitter community as a "twoosh" — a challenge that sparks users' creativity.

### 3. Even before you have an idea, believe in yourself and make your own opportunities. 

In 2008, when Facebook's founder Mark Zuckerberg offered to buy Twitter, its founders, Evan Williams and Biz Stone, jokingly told him that they would sell — for a ludicrous $500 million. Although this price was obviously much higher than Twitter's actual valuation at the time, Zuckerberg agreed.

Williams and Stone weren't serious about selling their company, but surely Zuckerberg's incredible offer was tempting?

Actually, no. Twitter's founders were convinced that their idea could be worth far more. And they were right: as a result of Zuckerberg's offer, the company's value skyrocketed and is worth about $15 billion today.

What can we take away from this success story?

If you believe in yourself and your idea, others will notice you and follow suit. The self-confidence that Twitter's founders displayed in valuing their product so highly seduced others into believing in the product too.

But how can you effectively signal this self-belief to others? Often, the best way is through "branding" yourself. For example, before Twitter, Biz Stone had business cards with the title "Genius" printed on them. He'd also tell people about completely fictional business deals he'd played a part in. It was exactly this outlandish confidence and self-belief that helped him to land a job with Google.

Sometimes, however, you'll find it difficult to get people to notice you, no matter how much self-belief you have. In such cases, you have to get creative.

For instance, if you're aiming to get noticed in a competitive field, but the skills you have aren't unique, you have to create a niche for yourself.

Imagine, for example, you're finding it hard to get recognized as a good football player in a school already filled with good players.

What can you do to get yourself noticed? You could encourage the school to offer a new sport, such as lacrosse. By doing so, you'll find it a lot easier to stand out, as you'll have less competition.

### 4. Don’t be afraid to come up with new ideas and take risks in order to succeed. 

At some point, many of us find ourselves bored with our jobs and feeling that our well of creativity is drying up. We become desperate to try something new, but at the same time are terrified of the risks involved.

"What if I'm simply no good at it?," we wonder. "What if it fails?"

How can we overcome this fearful way of thinking?

The first thing to consider is that creativity never really dries up, but is in fact a _renewable_ resource. Therefore, whatever task you put your mind to will stimulate your creativity, so there's little reason to fear moving onto a new venture if you're dissatisfied in your current position.

For example, although Biz Stone had a lucrative position at Google, he wanted to pursue a new project. He decided to take a leap of faith and leave Google to join the podcasting company Odeo — which he eventually dropped to focus on the ultimately more successful Twitter.

However, once you're ready to accept the risks and dive into something completely new, it's crucial to first investigate the potential negative outcomes to make yourself aware of the worst-case scenario.

Why?

Because when we start something new, we're often discouraged by thoughts of failure, causing the steps we take to be tentative and half-hearted.

In order to overcome this, we have to consider the question, "What's the worst that can happen?" as it's often not as terrifying as you might believe.

For example, if you're attracted to someone, you might be scared to ask them out on a date because you fear being rejected. But when you consider the worst-case scenario, you realize it's that they say "no" — that's it!

Once we realize that the risks aren't as potentially devastating as we first believed, we can muster up the confidence to give all we have to make our new project succeed.

> _"When you let people understand that you are people like they are, passionate but imperfect, what you get in return is goodwill."_

### 5. Failure is inevitable, so you should learn how to deal with it. 

We all have to deal with failure at some point in our lives, and being aware of this can keep many of us from trying something new.

But it doesn't need to be the case: there are lots of productive ways to deal with failure.

First, whenever you fail, try to remain positive and find a solution. Take the way Timothy Preston handled a potential failure:

Following the tsunami in Indonesia in 2004, eight incubators for infants were donated to an Indonesian hospital. But when Preston — an MIT professor with a team designing an incubator for the developing world — checked on them four years later, he discovered that they'd all broken down because no one knew how to service them.

Rather than letting pessimism get the best of him and abandoning the project, Preston decided to stay positive and find a solution. One colleague had noticed that all the Toyota trucks in the area were running perfectly — which meant that local mechanics clearly knew how to repair them. Preston thus built incubators using truck parts so local mechanics would know how to fix them.

Second, when you fail, you must admit your failures to others. That way they'll know you're working on a solution and be patient with you.

For example, as Twitter's popularity increased, so did the amount of service outages due to the overwhelming demand on the servers.

Rather than keeping quiet, Twitter informed their users about the problem and assured them that they were working on it — and the error message was accompanied by an image of a whale being lifted by birds.

As a result, any disappointment the users had was eased by their belief that Twitter was being straight with them. In fact, their acceptance of the situation was so great that the "Fail Whale" became an internet meme, probably attracting even more users.

### 6. Listen to the masses because the ideas of many individuals will always beat your own. 

If a great idea springs to your mind, is your first instinct to keep it to yourself? If so, you're probably making a mistake.

Why?

Because ideas rarely come out fully formed and perfect. It's only with the input and assistance of others that these ideas can be improved — which is exactly what Biz Stone discovered when he was designing the Twitter logo.

At first, Stone simply drew a sketch of a bird based on an image he found online. Since this first attempt wasn't quite right, Stone allowed someone else to make a few changes that he hadn't considered. After that, he sought out a professional designer to finalize the logo.

Had Stone not used outside sources, allowed others to give critical input and, finally, called on the services of a professional, the iconic Twitter logo never would've been realized.

But this process shouldn't end once you've released your product. Once it's out there, the opinions of your users can be a crucial source of guidance for how to improve your product.

That's because your users know what they want. It's extremely useful, therefore, to observe their behavior, listen to their opinions and implement ideas in response to them.

Twitter is a great example of the value of this process, as many of its popular features were actually inspired by its users.

For example, users initially would copy and paste those tweets that they wanted to share. Observing this behavior, Twitter created the "retweet."

Another example is the hashtag, which was first suggested by one user at the South by Southwest festival who wanted all tweets about the event to be tagged together. The user's solution was to add "#sxsw" to his tweets. The hashtag later became one of Twitter's most famous features.

If these examples aren't enough to convince you to listen to your users, this should: even the word "tweet" was invented by Twitter users themselves!

### 7. If you aren’t a passionate user of your own product, you can’t expect others to be. 

Do you remember how torturous even the easiest classes in school were if you didn't care about the subject matter? Or perhaps you recall getting an "easy A" in a difficult class because it was a subject you loved?

In that vein, you have to be a passionate, avid user of your product for it to become a success.

For example, as Stone and Williams were developing the podcasting app Odeo, they realized they didn't care about podcasting, nor did they know anything about audio. Unsurprisingly, Odeo didn't pan out.

However, Stone _was_ passionate about connecting people, so they began to develop a product focused on the status update — Twitter. Part of the reason that Twitter was successful and Odeo was not is that the founders were more passionate about Twitter and thus able to experience the service from a user's perspective.

But it's not just founders who benefit from following their passions. Employees also should be encouraged to work on whatever they're truly interested in.

Why? Employees are hired because they're talented and intelligent. Therefore, letting them follow their own passions will lead ultimately to the development of great products.

For example, after selling Odeo, Stone and Williams used their team's passion to help find a new direction for the company by holding a "hackathon" in which employees could focus on their own ideas.

Out of this process, the idea that would lead to the creation of Twitter was born.

Of course, sometimes you'll lose your passion in a project. In such cases, it's crucial that you allow yourself to take on a new project that reignites your interest.

For example, when Stone's interest in Twitter waned, he followed his passion, creating the app Jelly, a search engine with a twist: questions entered into Jelly are answered by real people.

### 8. Instead of just avoiding being evil, you should actively do good by giving others a win-win deal. 

Most of us are aware of Google's motto "Don't be evil."

However, in striving to succeed with your company, it's not enough to merely refrain from doing evil. Rather, if you want your company to succeed, you must actively do good — which, in business, means offering your partners, employees and users a good deal.

And the best deal is a _win-win_ deal as it benefits both parties. By ensuring that the other party benefits equally, you stand a greater chance of establishing a long-term relationship with them.

For example, a major UK mobile company offered to pre-install Stone's app Jelly on their phones before they knew the first thing about it.

Why?

The mobile company had had a very productive relationship with the same staff at Twitter and believed that Jelly would be equally as productive.

But how can you make a win-win deal with your _users_?

The answer is to focus on their interests, rather than solely on monetization.

While most websites use advertising to make money, their webpages often contain more ads than content. Such an approach alienates the user, as they feel they're being exploited because they're getting very little out of the arrangement.

To avoid this, companies must ensure that their users benefit from the ads that they see. Twitter, for example, tracks retweets, favorites and clickthroughs in order to deliver only those ads that users will find interesting.

Finally, making a win-win deal with _employees_ means giving them an incentive to work with you.

Of course, this incentive has to be offered right from the outset. It won't be effective if it's only to persuade the employee to stay with the company — which is what happened when two Twitter employees were about to leave the company to work with Stone on Jelly. Although Twitter offered generous financial incentives to stay, they refused.

### 9. Technology connects people across the globe, generating empathy among people who’ve never even met. 

Imagine you were alive fifty years ago and an earthquake hit a nearby town where your friends lived. Would there have been anything you could've done to help them? Probably not, as you wouldn't have even been aware that the earthquake happened until you saw the paper the next morning.

Today, however, technology has made the world a much smaller place, as information travels faster than ever before. Services, like Twitter, connect people across the globe, enabling them to speak their minds freely, understand each other and even help each other out.

For example, in 2008, Berkeley student James Buck was arrested in Egypt while working on a project about the country's anti-government groups.

With no other way to communicate, he tweeted that he'd been arrested. Having read this tweet, his friends informed the dean of their school, who arranged a lawyer for Buck in Egypt.

The result? He was freed.

And the power of Twitter goes beyond just helping our friends when they're in trouble. By using technology to connect with one another, we become _global_ citizens out to do good for the whole world.

This connection enables us to empathize with each other — and help others simply because it feels good to do so.

For example, people hold "Twestivals" on Twitter to generate awareness of social causes — such as supporting homeless shelters and getting clean water to people in arid regions. Equally importantly, it's used to raise the funds to do something about such problems.

Another example can be found in Twitter's support of projects like Product(RED), which partners with companies to raise money to fight AIDS. On World Aids Day, Twitter colored their interface red, a gesture which put Product(RED) on the map, ensuring that its name was universally known. This increased the impact of the project as other social media platforms reached out to assist them.

> _"Global empathy is the triumph of humanity."_

### 10. Final Summary 

The key message in this book:

**When** **you're** **developing** **an** **idea,** **it's** **important** **to** **set** **yourself** **limitations,** **as** **these** **are** **often** **a** **springboard** **for** **creativity.** **At** **the** **same** **time,** **you** **should** **be** **prepared** **to** **take** **big** **risks,** **and** **learn** **how** **to** **handle** **inevitable** **failures.** **Once** **you've** **realized** **your** **idea** **and** **created** **your** **product,** **continue** **perfecting** **it** **by** **being** **open** **to** **other** **people's** **criticism** **and** **establishing** **win-win** **deals** **with** **partners,** **employees** **and** **users.**
---

### Biz Stone

Biz Stone helped create Xanga, Blogger and Odeo before co-founding Twitter. After leaving Twitter, Stone founded Jelly, a picture-based question and answer app, and became an angel investor for a number of companies, including Square and Beyond Meat. His previous books are _Who_ _Let_ _the_ _Blogs_ _Out?_ and _Blogging:_ _Genius_ _Strategies_ _for_ _Instant_ _Web_ _Content_.

