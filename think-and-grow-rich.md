---
id: 530338b86431310008300000
slug: think-and-grow-rich-en
published_date: 2014-02-19T08:04:26.000+00:00
author: Napoleon Hill
title: Think and Grow Rich
subtitle: None
main_color: C7C7AD
text_color: 616137
---

# Think and Grow Rich

_None_

**Napoleon Hill**

In _Think and Grow Rich_ (1937), Napoleon Hill investigates the methods of the 500 most successful people of his time, including the world's richest men, top politicians, famous inventors, writers and captains of industry. First published amidst the Great Depression, _Think and Grow Rich_ has sold over 100 million copies.

---
### 1. We can only accomplish our aims in life if we are driven by a burning desire. 

A great number of us wish for wealth and financial independence — but wishing alone isn't going to help us achieve our goals. If we want to get rich and realize our dreams, we need to recognize a burning desire within us.

For instance, even after more than 10,000 failed experiments, Thomas Edison could not be shaken from his goal of inventing an electric light source. He was — plain and simple — driven by the desire to make his dream come true. And, after years of hard work, he did just that when he invented the light bulb. 

Writer Fannie Hurst had a similar experience: she had to suffer over 36 rejections before a single one of her short stories was published in a newspaper. From then on, her career as a successful novelist and playwright took off. Her burning desire had proven stronger than the frustration she felt about being rejected — and she ultimately met with success.

So it's essential for those of us who want to be successful to examine our own personal attitude towards setting our goals and dreams. 

How do we feel about our goals and dreams? Are they only castles in the sky? Or is our burning desire strong enough to make them come true one day?

### 2. Goal setting and detailed planning are the basis of every achievement. 

Every success story begins with people who know what they want to achieve.

So before you begin chasing after any old dream, you should start by defining your own personal goal as precisely as you can. For example, if you want to get rich, you should decide precisely how much money you want to make.

Additionally, you must have a clear understanding of when you want to achieve your goal and what you're prepared to invest to accomplish it. Because setting a specific goal is pointless if it's floating in some indeterminate future where you'll only ever be able to pursue it halfheartedly.

It's also important to come up with a plan that outlines every step you'll need to take until the realization of your goal. And once you have a plan, get cracking! Don't waste another minute.

The following method comes in handy if you want to make sure that your desire for success is anchored deeply enough in your own thoughts and actions. First, write down your goal and your detailed plan for achieving it. Then, read it aloud twice every day: once in the morning after you get up and once every night before going to bed.

Follow these recommendations and it'll make it a lot easier for you to get rich — or make any other dream come true.

### 3. Successful people have an unwavering faith in themselves. 

Having an unwavering faith in yourself is a way of making sure that you can — and will — achieve your goals.

You can only achieve success if it's based on self-confidence and an unwavering faith in yourself: no wealth has ever been acquired, no faraway continents ever discovered, nothing ever invented without this faith as its basis. 

One paradigmatic example of the power of faith was Mahatma Gandhi. Even without access to the typical instruments of power — like money or the military — he managed to defy British colonial power and lead his country to freedom. The only backing he had was his unbending belief that he could exercise an influence so great upon his fellow countrymen that they would stand up for their common goals.

Our own faith in ourselves has an enormous influence on our self-image and way of life. It can — in the truest sense of the word — help us move mountains.

### 4. By using auto-suggestion, our subconscious can influence our behavior. 

Unwavering faith in yourself is not necessarily something you're born with or that falls from the sky: anybody can develop it, bit by bit, through auto-suggestion.

Auto-suggestion describes a way of influencing yourself by thinking very specific and purposeful thoughts or ideas.

You can use it to convey certain commands or positive goals to your own subconsciousness and, in doing so, to increase your own belief in yourself.

In general, auto-suggestion consists of persuading yourself that: you can reach your goals; your thoughts can be transformed into reality and; you must go your own way with confidence.

The more you use auto-suggestion, the likelier it is that it will help you achieve success. If you impress your desires and goals in your unconsciousness, it will steer all your thoughts and actions towards making them come true.

### 5. Knowledge is power – but it doesn’t have to be what you learned in school. 

Knowledge can make it a lot easier to achieve your aims in life. All you have to do is take a few basic points into consideration.

First of all, you need to take the "traditional" meaning of knowledge and education — and throw it out the window.

Because being knowledgeable or well-educated isn't limited to having a high school diploma or university degree. "Uneducated" people can also know a heck of a lot, as Henry Ford clearly demonstrated. Although he didn't make it to high school, it didn't stop him from establishing an industrial empire and making millions. 

If you want to be successful, you don't need to stuff your head with too many facts — it is far more important to acquire the right experiences and knowledge, use your own strengths and fully exploit your own potential. 

The most important basis is a willingness to continue learning throughout your life. Getting complacent is your worst enemy. Instead, you must always be ready to actively and pointedly expand your own knowledge. There are many different ways to achieve this — by going to university, attending evening courses or acquiring practical experiences.

Equally useful is the ability to know _where_ you can find the knowledge you need, because you don't need to know everything yourself.

Instead, you need to know who you can ask if you need to know something. It's usually more practical and productive to be surrounded by a network of experts who share their specialized knowledge than to learn it all yourself.

### 6. The workshop of the imagination – where we can turn our dreams into reality. 

Each and every success story begins with an idea. And behind each and every idea is the imagination. It is, essentially, the creative workshop of our minds that transforms our dreams into ideas and our ideas into reality.

This imaginative ability has two different forms: the _creative_ imagination and the _synthetic_ imagination.

By using our creative imagination, we are able to come up with completely new things. Ingenious composers, visual artists and writers use this function to create works unlike anything made before.

The synthetic imagination, by contrast, rearranges old ideas into new combinations. The developers at Sony, for example, were using that faculty when they decided to further develop the playback device or dictaphone used by journalists, turning it into a portable music player everybody could use — and so the Walkman was born. 

Creative and synthetic imagination can play off one another in a productive way. Take the story of Asa Candler, who formed the global brand Coca-Cola out of a headache medication about 140 years ago. Although it wasn't Candler himself who came up with the Coca-Cola recipe — he bought it from a pharmacist for $3,000 — he developed the ingenious plans and marketing strategies that turned the product into a huge success.

If you want to keep your imagination from getting sluggish, you need to challenge and encourage it. And you can achieve that by stimulating it, keeping it active and training it like a muscle: the more frequently you use it, the more productive and powerful it will become.

### 7. Knowing your own strengths and weaknesses increases your chances of professional success. 

One important building block for professional success is an awareness of your own strengths and weaknesses — in other words, being self-aware.

Since things like _having vague goals_, _lacking ambition_, _procrastinating_ and _lacking resolve_ are common reasons for failure, self-awareness can help to constructively take action against these weaknesses — or at least to balance them out with your strengths. 

In order to be able to figure out exactly what your strengths and weaknesses are, it's advisable to perform a thorough and honest self-analysis. Don't worry — this doesn't require going to a psychologist. It's more than enough if you go through a checklist of questions, which should include: Have I achieved my aim for this year?; Was I always friendly, courteous and cooperative?; Did I make all decisions promptly and firmly?

Then you should compare your subjective self-analysis with somebody else's objective evaluation of you. This works best when you sit down with a person who knows you really well to openly and honestly discuss your strengths and weaknesses.

### 8. Positive emotions are the key to a successful life – and need to be strengthened. 

Our subconscious receives and stores sensory stimuli, feelings and thoughts. It saves everything we've ever experienced — regardless of whether it was positive or negative.

But saving information alone isn't all there is to it: our subconscious is also constantly exercising its influence on our actions. It can shape us positively, endowing us with strength and enterprise, but it can also shape us negatively, misleading us into despondency and pessimism.

And so, if we want our subconscious to help us realize our desires and goals, we have to make sure that positive feelings play a leading role in our lives.

We must, therefore, "feed" our subconscious positive things because, if we do, it will act as a helpful and constructive guide. But if we allow ourselves to feel too many negative sensations, such as rage, hate, a desire for revenge or pessimism, the very opposite will occur.

That's why you've taken a huge step when in everyday life you're able to, for example, avoid contact with doom-saying windbags and pay no heed to their discouraging remarks.

Instead, you should make it a point to increase all positive impulses, such as enthusiasm and love. That's the only way you'll end up developing a positive mentality in the long term.

### 9. Successful people are remarkable for their determination and steadfastness. 

The analysis of over 25,000 life stories of people who failed to achieve professional success shows that a lack of determination is the main reason for their failure.

The analysis of the success stories of multimillionaires revealed, by contrast, that they all had two characteristics in common: they were used to making split-second decisions and, once they made them, they stood firmly behind their decisions. 

A certain degree of stubbornness — as long as it does not transform into a deaf obstinacy — can even be advantageous. Henry Ford, in particular, was known to stick to his decisions for a long time. For example, many people advised him to replace his famous — but not terribly beautiful — Model T with a new model. But he held on to the car for a long time and was thus able to continue bringing in sizeable profits with it. 

Opinions are cheap: everybody has one and most people want to dispense them. In order to avoid running into the danger of negative influences and straining your own steadfastness, it makes sense to offer others as few opportunities as possible to express their critical opinions. 

People who are disheartened in particular should therefore keep their plans and intentions to themselves and not confide in anybody else — except for the members of their hand-picked team or trustworthy outsiders.

### 10. Only the persistent will succeed. 

Over the course of any project — regardless of its nature — we are all bound to encounter obstacles and difficulties. In such situations, most of us are too quick to give up our plans and let the project die. But there are a few people who, despite all the obstacles, stick to their original plans and give their dreams space to blossom into reality.

Persistence and endurance are key. That means, above all, that we work constantly on the realization of our goals and don't lose sight of them. However, obstinacy and inveteracy should be avoided at all costs: if, say, there's a dire need for a price correction, you also have to be willing to implement it. 

If you want persistence and endurance to take root in your habits, here are four simple yet crucial rules: 

You must have a concrete goal and develop a burning desire to achieve it.

You need a thorough and precise plan to support the implementation of your goals.

You may not allow yourself to be influenced by negative and disheartening opinions.

You need an intimate, trusting relationship to a person or a group that provides you with support and assistance.

Think of these rules as a special exercise program to help you build up your personal endurance and persistence.

### 11. Achieving great things requires being smart and surrounding yourself with smart people. 

The bigger the goal, the more complex the planning; the more complex the planning, the more difficult the goal's execution; the more difficult the execution, the more dependent the brains behind the project will be on the creative, intellectual and moral support of others.

This mutual backing works best in the form of a _brain trust_ — a select group of intelligent people. 

A brain trust is a union of like-minded people, which, contrary to a network — which is a more casual cooperation between partners — focuses on defining a common goal, developing competencies and having a system of checks and balances.

The most important principle of a brain trust is its synergistic effect: if two or more people who work well with one another combine their skills, talents, specialist knowledge, experiences, relationships and all other resources and use them to accomplish the same shared goal, the results will be more than just a sum of its parts: it's a surplus with which you can achieve things you could have never achieved alone.

### 12. Final summary 

The main message of this book is:

**Wealth — in whatever form — is seldom the result of luck or coincidence. Far from that, it's almost always the result of different traits and skills that anyone can learn and acquire.**

This book in blinks answers the following questions:

**How can we find the right goals in life and how do we translate them into action?**

  * We can only accomplish our aims in life if we are driven by a burning desire.

  * Goal setting and detailed planning are the basis of every achievement.

  * Successful people have an unwavering faith in themselves.

  * By using auto-suggestion, our subconscious can influence our behavior.

**Which skills and knowledge are necessary for achieving success?**

  * Knowledge is power — but it doesn't have to be what you learned in school.

  * The workshop of the imagination — where we can turn our dreams into reality.

  * Knowing your own strengths and weaknesses increases your chances of professional success.

  * Positive emotions are the key to a successful life — and need to be strengthened.

**What sort of approach do successful people take when realizing their goals?**

  * Successful people are remarkable for their determination and steadfastness.

  * Only the persistent will succeed.

  * Achieving great things requires being smart and surrounding yourself with smart people.

**Suggested further reading: _I Will Teach You To Be Rich_ by Ramit Sethi**

_I Will Teach You To Be Rich_ takes a straight-talking and amusingly cocky approach to smart banking, saving, spending and investing. You don't need to be an expert to become rich, you just need to have a plan and know a few tricks. Sethi will teach you the benefits of saving as early as possible and setting up automatic investments so you can sit back and let your money work for you.
---

### Napoleon Hill

Napoleon Hill (1883–1970) was an American journalist and writer. From 1933 to 1936 he was an advisor to President Franklin D. Roosevelt.

