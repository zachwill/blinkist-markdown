---
id: 53a1a37b3364660007090000
slug: think-like-a-freak-en
published_date: 2014-06-17T00:00:00.000+00:00
author: Steven D. Levitt and Stephen J. Dubner
title: Think Like a Freak
subtitle: None
main_color: F89C32
text_color: AB6C22
---

# Think Like a Freak

_None_

**Steven D. Levitt and Stephen J. Dubner**

_Think_ _Like_ _A_ _Freak_ is a blueprint for thinking unconventionally and creatively. It demonstrates the benefits of letting go of conventional wisdoms, and teaches you to dig deeper to find out how things really work. By learning to think like a "freak", you'll gain access to an entirely new way of solving problems and making sense of the world.

---
### 1. What’s in it for me? Improve your problem-solving abilities by freeing yourself from conventions. 

Have you ever made a particular choice just because everyone around you seemed to support it? How often have you found yourself repeating an argument you came across in the media, even though you haven't really thought it through?

We're often restricted by conventional wisdom about how the world works. Moreover, this kind of popular belief is especially limiting when it comes to decision making and problem solving.

_Think_ _Like_ _A_ _Freak_, from the authors of _Freakonomics_, aims to solve this particular problem by showing the world, one reader at a time, how to think unconventionally — "a bit differently, a bit harder, a bit more freely", as the authors put it.

In these blinks, you'll learn to see the world as no one else does, and, in doing so, to find solutions that you would not have otherwise considered. In short, these blinks will show you how to think like a freak.

Along the way you'll also find out:

  * why kids are harder to fool than adults when it comes to magic tricks,

  * why you should never depend on the predictions of an expert,

  * how a skinny college student won the world's biggest hotdog-eating championship, doubling the previous record and

  * how the legalization of abortion in the 1970s led to a massive drop in crime in the 1990s.

### 2. Thinking like a freak means violating conventions; it might win you the World Cup but won’t necessarily make you popular. 

When we try to solve problems, most of us are guided by conventional beliefs. However, there's a problem with conventional beliefs: they're often wrong.

Take, for example, the "local food" movement. Most people believe that consuming local food reduces environmental impact.

However, a recent study found that this movement was actually counterproductive because the small farms it supports consume more energy for production, outweighing any positive effects of reduced transportation.

This is exactly what thinking like a freak is about: basing one's beliefs and decisions on statistical evidence, rather than conventional wisdom.

But how can this be of use in everyday life? Well, for one thing, thinking like a freak can help you to solve problems.

For example, imagine you're a soccer player about to make a penalty kick which could win your team the World Cup.

How can you increase your chances of scoring? If you're right-footed, as most players are, aiming towards the left will result in a stronger, more accurate kick. Goalkeepers are aware of this strategy, so 57 percent of the time they'll jump to the kicker's left side, and 41 percent to the right.

Interestingly, however, keepers remain in the goal's center only two percent of the time, so a kick "straight up the middle" is seven percent more likely to be successful than a kick to either corner.

But while thinking like a freak has many advantages, it might also put your popularity at risk.

For example **,** only 17 percent of all penalties in professional soccer are aimed towards the center.

Why?

Because it's such an obvious violation of the conventions. Also, if the goalkeeper remains in the center and catches the ball with zero effort, the penalty-taker might well lose the faith of his fans.

Or, for example, imagine how popular you'll be when you tell a "locavore" friend that the local-food movement actually _hurts_ the environment!

> _"Sometimes in life, going straight up the middle is the boldest move of all."_

### 3. Admit when you don’t know something, and never blindly trust experts. 

Many of us find it hard to admit if we don't know something. Instead of confessing our lack of knowledge, we are overconfident and pretend we know something when actually we don't.

In terms of overconfidence, consider the fact that when asked to rate their driving skills, roughly 80 percent of people rated themselves as "above average", even though — by definition — just 49.99 percent of them can be above-average drivers.

Also, people fake knowledge of a subject because they don't want to appear stupid. So they nod and smile, even if they don't understand what's being said, or they pass off the opinions they've read in the newspaper as their own.

But the truth is that admitting that you don't know something has its advantages.

For one, it can actually increase your credibility. If you're known as someone who owns up to their lack of knowledge, people are more likely to trust you when you claim that you _do_ know something.

Another advantage is that, by maintaining an awareness of what you don't yet know, you can learn and, eventually, figure out the truth.

One group of people who rarely admit a lack of knowledge are experts, as they're highly likely to bluff it when they're not sure. For that reason, we should be careful not to trust them blindly.

For instance, we often base our decisions on the predictions of experts — e.g., when deciding which stock to invest in — but there are reasons why we shouldn't do this.

Firstly, they have the motivations to fake knowledge: experts who make bold predictions that turn out to be accurate might be celebrated for years, but, if those predictions are wrong, they'll probably be forgotten.

Secondly, experts are actually terrible at making predictions. One study even calculated the rate of accuracy of stock-market experts' predictions to be just 47.4 percent — worse odds than you'd get by simply flipping a coin!

> _"The impulse to investigate can only be set free if you stop pretending to know answers that you don't."_

### 4. When solving a problem, look past the public discussion and redefine the problem. 

Whenever the media focuses on a single aspect of a problem — for instance, on the ethnic background of a criminal — rather than considering a host of other relevant factors, their arguments tend to determine public discourse.

However, since public discussion can cause us to overlook other, more fruitful approaches, we should try to look beyond the media's focus.

Take, for example, the American education crisis, where the discussion is focused on the schooling system and on topics such as teachers' skills and class size.

Obviously, making improvements in these two areas would help to improve children's performance in school, but in fact many studies argue that factors such as teachers' skills have much less influence on schoolchildren's performance than parenting does.

The best approach, therefore, would be to focus attention on questions like: What have children learned from their parents? And, has an appetite for learning been established?

Yet because the media focuses on a single aspect of the problem — "what's wrong with our schools" — we overlook the initial question: why do American kids usually know less than those from many other countries? By focusing on the original problem, we're able to identify other aspects of the situation and to arrive at effective solutions.

In addition to looking beyond the public discourse, it can sometimes help to _redefine_ the problem.

For example, when Kobi — a slim Japanese student — first entered the world's biggest hotdog-eating competition, none of his competitors saw him as a threat.

But Kobi didn't just win the contest. He doubled the previous record, consuming 50 hotdogs!

How? He redefined the problem — from "How can I eat more?" to "How do I make hotdogs easier to eat?"

This led Kobi to invent new techniques, such as dunking the buns in water and eating them separately from the sausages, which turned out to be his key to success.

> _"When you ask the question differently, you're able to look for answers in different places."_

### 5. Think outside the box to identify the root cause of problems – don’t focus on the symptoms. 

Finding the root cause of a problem is always a challenge. So what's the best strategy to get you there?

A particularly useful approach is to think "outside the box."

What does this entail? Consider the following example.

In their bestseller, _Freakonomics_, authors Levitt and Dubner examined possible causes of the dramatic fall in violent crime since the 1990s.

By looking at a statistical analysis, they found that the influence of things like an increased police presence was too small to account for the drop as a whole.

So what _was_ the missing factor?

By chance, Levitt recalled a statistic about the dramatic increase in abortions after its nationwide legalization in the 1970s, and after some statistical analysis, he found that this was related to the drop in crime.

While this may sound quite absurd, there's actually a simple explanation: the increase in abortions in the 1970s resulted in fewer births of unwanted children, which in turn meant that fewer children were growing up in the kinds of difficult circumstances that often lead to criminality.

In addition to employing outside-the-box thinking when searching for a problem's root cause, you should also ensure that you don't mistake a symptom for a cause.

For example, consider this question: What are the causes of poverty and famine?

At first glance, the answer seems obvious: the lack of money and food.

However, if that were true, why has the problem persisted despite the constant efforts of aid groups and governments to allocate money and food to poor regions?

The reason is that poverty and famine are symptoms of _another_ problem: the absence of a workable economy and credible political, social, and legal institutions.

### 6. Think like a child: Have fun, be curious and confront the obvious. 

In many ways, thinking like a freak means thinking like a child.

For example, imagine that you're a professional magician. Which audience would you prefer: kids or adults?

While you might think it's far easier to impress children with magic, kids are actually a far more challenging audience.

Why? Because kids are curious and therefore more difficult to deceive. As professional magician Alex Stone — an expert on misdirection — argues, there are several reasons for this.

One reason is that adults are good at focusing their attention on one thing. Though you might consider this an advantage, focus — while essential to getting things done — can leave you vulnerable to misdirection.

Kids, on the other hand, are more curious, and try to see an illusion from different angles. The result is that they tend to notice essential aspects that adults overlook, which leads them to discover how the trick works.

Certain other behaviors associated with children — having fun and confronting the obvious — are also important for all of us.

Why is this?

Firstly, if you have fun doing your work, you'll want to do more of it. Also, research shows that engagement in what you're doing is the best predictor for future success.

Secondly, confronting the obvious — asking questions that others usually don't — can lead to all kinds of insights.

For example, the discovery (mentioned in the previous blink) of a link between the legalization of abortion and the 1990s drop in violent crime resulted from a chance observation of a huge statistical increase in abortions immediately following their 1970s legalization.

While most people think about abortion in political or moral terms only, and would never suspect it might be related to other social phenomena, those who are in touch with their "inner child" would respond to such a statistical spike with wonder and curiosity: "What a huge increase. That must've had an effect on something!"

> _"When it comes to generating ideas and asking questions, it can be very fruitful to have the mentality of an eight-year-old"._

### 7. In order to solve a problem, pay attention to the way in which incentives drive human behavior. 

The key to understanding problems and arriving at solutions is to understand how incentives direct our behavior.

In an experiment that psychologist Robert Cialdini conducted to determine the strongest incentive for people to save energy, participants were instructed to rate the relative influence of four factors on their decision to conserve energy.

Cialdini found that the most important factor was _protection_ _of_ _the_ _environment_. The second most important factor was that energy conservation _benefits_ _society_. The third factor was that it _saves_ _money_. And the lowest-ranked factor of the four was that _other_ _people_ _are_ _doing_ _it_.

But do these incentives hold water in real life?

To find out, Cialdini's research team went from house to house in a Californian neighborhood and hung placards with messages to save energy by using a fan rather than air-conditioning in summer.

Five versions of the cards were randomly distributed among the residents. One had a neutral headline, and the others had headlines which matched one of the four incentives above.

Because the researchers were able to measure the actual energy use of each home, they could see which of the placards was the most effective.

The winner? Surprisingly, it was the card that appealed to the "herd-mentality" incentive. So, in actuality, the factor that was ranked as least important in the survey — "other people are doing it" — turned out to be the strongest incentive for conserving energy.

What can we learn from this?

If you want to change an individual's behavior, telling them that "too many people are wasting energy — that needs to stop!" is a wrong-headed approach because the underlying message is that "many people like you are doing this, so it can't be that bad."

A more effective public service message would be: "Everyone is interested in saving energy — what about you?"

> _"Understanding the incentives of all players in a given scenario is a fundamental step in solving any problem."_

### 8. If you understand what drives people you can set strategic traps that reveal their bad sides. 

Usually, a person who is lying or cheating will respond differently to a given incentive than an honest person would — a fact that can be used to distinguish the good guys from the bad.

How? By employing certain strategies, such as faking cruelty, you can make people reveal their true intentions.

Consider this example from the Bible: Two women came to King Solomon with a dilemma. Both of them lived in the same house and each had recently given birth to a boy. The first woman claimed that the second had exchanged the babies when she was sleeping, after the second woman's child had died. The second women denied this.

King Solomon told the women that, in order to resolve the matter, he'd divide the child in two with a sword and give one half to each of them.

The first women begged him not to hurt the baby and to instead give it to the second woman, who didn't object.

The king reasoned that a true mother would love her child too much to accept the killing, while a woman able to steal another's baby out of jealousy would be content with such a solution.

Another illustrative example comes from real life.

Van Halen — one of the biggest rock bands ever — were famous for their demanding attitude. In the "Munchies" section of their rider, they requested M&M's, with the proviso "WARNING: ABSOLUTELY NO BROWN ONES."

At first glance, this might seem an example of rock star excess, but it was actually a strategic move.

Van Halen's live show required strict safety measures. The only way for them to be certain that a local promoter had read all of their instructions thoroughly was to check whether the bowl of M&M's backstage included any brown ones. If it did, the rest of the equipment would need to be very carefully checked.

The band understood that promoters driven by "easy money" would overlook the passage about the M&Ms, while those motivated by professionalism would not.

### 9. Before you can persuade someone, you have to make her listen. 

You've probably noticed that when you challenge someone's common sense, you often encounter disagreement.

So what can you do to persuade your opponents?

First, acknowledge that persuasion is difficult because people tend to ignore facts that don't fit their worldview.

For example, despite the fact that the vast majority of climate scientists believe that global warming will probably lead to worrisome changes for our environment, the American public is not very concerned.

Why?

In one study, researchers from the Cultural Cognition Project suspected that people aren't worried simply because they're not scientifically literate enough to understand the arguments of climate scientists.

However, when they tested their hypothesis, they couldn't find any association between scientific literacy and concern about climate change. But they did find another interesting pattern.

Scientifically literate people were actually more likely to hold an extreme opinion — e.g., that climate change is extremely worrisome _or_ totally insignificant.

Their high level of confidence in their position probably stemmed from a good general knowledge and from having had more experience with feeling they're "in the right" than lay people.

Also, their higher level of education enabled them to find evidence that proves their point while simultaneously blocking out any opposing arguments.

So even if you have excellent arguments and statistical evidence, you should expect persuasion to be difficult.

What's certain is that, in order to persuade someone, you'll first have to get her to _listen_. This means respecting her point of view and then telling her a story.

Since people tend to be overconfident in their own views and mistrust opposing ones, it's best not to pretend that your argument is watertight. Instead, acknowledge the strengths of your opponent's argument and explain how you still arrived at the opposite point of view.

Also, catchy stories are often the best tool to prove your point; stories are simply harder to ignore and forget than abstract details.

### 10. Letting go of conventional wisdom can make you happier with your life. 

Not only does thinking like a freak help us to problem solve, it can also make us happier.

How?

Most well-meaning, conventional advice is actually counterproductive when it comes to happiness, so do yourself a favor and don't pay it too much attention.

One popular mantra that is actually bad advice: a winner never quits, and a quitter never wins.

Why is this bad advice? Because there are already enough forces which bias us against quitting.

Even when quitting is clearly the better choice, we often hesitate to do it. There are several reasons for this.

Firstly, _social_ _pressure_ : we have been taught that quitting is a weak thing to do.

Secondly, _sunk_ _costs_ : the more we invest in something, the more reluctant we are to quit.

This is also known as the "Concorde Fallacy," after the supersonic airplane. Everyone involved knew that the project wasn't economically viable, but hesitated to quit because they'd already invested so much money. The result? They ultimately lost a _lot_ more money than they would have had they quit sooner.

Thirdly, we tend to forget the _opportunity_ _costs_ : We often overlook the fact that, by engaging in one thing, we also forgo the opportunity to do something else.

However, if we manage to let go of conventional wisdoms such as the "winner never quits" dogma, we allow ourselves to become much happier.

In order to investigate how certain decisions affect happiness, the authors set up a website on which people faced with a difficult decision could flip a virtual coin. After some months, the participants were asked whether they'd acted upon the coin flip and to gauge their happiness level.

Surprisingly, two major decisions to quit left people happier: breaking up with one's partner and quitting one's job.

Of course, this doesn't mean that people would be better off quitting more jobs or relationships, but there's nothing in the data to suggest that quitting leads to misery either.

### 11. Final Summary 

The key message in this book:

**Conventional** **wisdom** **is** **often** **wrong,** **and** **challenging** **popular** **dogmas** **can** **give** **you** **a** **better** **understanding** **of** **how** **things** **work.** **Learning** **how** **to** **think** **like** **a** **freak** **can** **help** **you** **to** **explore** **the** **hidden** **sides** **of** **problems,** **leading** **to** **surprising** **and** **effective** **solutions.** **Curiosity** **and** **the** **inclination** **to** **think** **outside** **the** **box** **are** **crucial** **ingredients** **for** **achieving** **this** **mindset.**
---

### Steven D. Levitt and Stephen J. Dubner

Steven D. Levitt is a professor at the University of Chicago. He was awarded with the John Bates Clark medal, one of the most prestigious awards in the field of economics, second only to the Nobel Prize.

Stephen J. Dubner is an award-winning journalist who has worked for the _New_ _York_ _Times_.

As a team, Levitt and Dubner have written two bestselling books: _Freakonomics_ and its sequel, _SuperFreakonomics_.

