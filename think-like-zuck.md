---
id: 54c650a03732390009310000
slug: think-like-zuck-en
published_date: 2015-01-27T00:00:00.000+00:00
author: Ekaterina Walter
title: Think Like Zuck
subtitle: The Five Business Secrets of Facebook's Improbably Brilliant CEO
main_color: FBCDBD
text_color: 945843
---

# Think Like Zuck

_The Five Business Secrets of Facebook's Improbably Brilliant CEO_

**Ekaterina Walter**

_Think Like Zuck_ takes an in-depth look at the five principles that made Facebook the successful company it is today. With plenty of insight into founder Mark Zuckerberg's inspiring approach to leadership and examples from other top technology companies, the author creates a roadmap for success for any aspiring entrepreneur.

---
### 1. What’s in it for me? Discover the secrets to becoming a great entrepreneur. 

The modern world is full of entrepreneurs dreaming of becoming the next Mark Zuckerberg or a future Jeff Bezos. Of course, relatively few of them will make it to the top, and most will finish a long way short of the billions they chase.

Why do the businesses of some entrepreneurs make it while others don't? Looking into the success of the likes of Mark Zuckerberg, _Think Like Zuck_ highlights the business practices of the most successful CEOs. By following this advice, you stand the best chance of rising above everyone else and becoming the next Mark Zuckerberg yourself.

In these blinks you'll discover

  * why Mark Zuckerberg took things slowly at first;

  * why it's OK to sometimes "take inspiration" from your rivals; and

  * why one company only hires happy staff.

### 2. Successful entrepreneurs let their passion drive them. 

Say you want to launch a start-up. Should you look for the most up-and-coming industry? Or should you create a concept that caters to the biggest available market? Well actually, you should try a completely different approach, and let your passion drive you. After all, that's exactly what Mark Zuckerberg did to build his phenomenal career.

Even before Facebook, Zuckerberg's passion for connecting people was evident. For instance, in the 1990s, he built a messaging system called "Zucknet" to connect computers at his parents' house.

While following his passion gave Zuckerberg his career direction, there was also another benefit to it: Consider the fact that in the beginning, Facebook wasn't so different from Myspace and Friendster. However, since Zuckerberg's passion was all about socially connecting people online, his product best matched what people actually wanted — allowing it to ultimately trounce rivals.

The lesson here is that when you follow your own passion, there's no harm in "stealing" inspiration from elsewhere, because your idea will naturally stand out on its own.

Another advantage of following your passion is that you're more likely to be persistent — that is, you won't give up, even when you fail.

For instance, before Zuckerberg founded Facebook, he created "Facemash," a social platform that allowed users to compare and rate photos of Harvard students. However, since the site used photos of students without permission, it was shut down by Harvard's disciplinary board. Even though Zuckerberg failed, since he was driven by passion, he was able to persist.

It's worth noting that although Zuckerberg's passion was vital, his ability to act also played a huge role in his success.

Because after all, no matter how much you love something, if you don't act on it, you won't succeed. But luckily, you can overcome inaction by remembering the message on a poster located in Facebook's headquarters. It reads, "Done is better than perfect."

### 3. Successful companies are built around an inspiring vision. 

Beyond a passion, the successful entrepreneur also has a _mission_ — a personal goal — and he won't give up until it's accomplished.

The entrepreneur's mission will define the company's mission, and this has huge consequences. After all, an inspiring mission can convert customers into loyal followers, which is a major advantage for any company that wants to stand out in the marketplace.

For example, even though Apple sets higher prices and has a smaller market share than competitors, it's still massively successful, partly due to its inspiring mission to create products that challenge the conventional and encourage people to "Think Different." Because many customers identify with this bold message, they flock to buy the company's products.

And while a mission is a great way to differentiate your company, there's another benefit: Seeing your company as _more than_ a business can improve your chances of succeeding in the long term.

That's the case for Mark Zuckerberg, whose mission for Facebook was "to make the world more open and connected." That's why whenever he received an offer to sell Facebook, he declined: he felt that his mission was still incomplete! And in fact, this mind-set figures into one of Facebook's most famous slogans: "The journey is only one percent finished."

And in addition to serving as a crucial guidepost for the founder, the company and customers, the mission also plays a key role in motivating employees. Because ultimately, highly skilled professionals who don't care about the project won't do a job as well as less experienced employees who deeply care about the mission.

This principle is evident at Threadless, a successful company that prints designs by unknown artists on T-shirts. The CEO prefers to hire people that lack experience but share his vision — that is, giving artists a platform for their work — over those with impressive experience but a lack of enthusiasm about the mission.

### 4. Hire motivated employees who share the company’s values. 

As discussed in the previous blink, someone who loves their work will be a better employee than someone who doesn't. And entrepreneurs have to remember that if they want to hire the right people.

So how do you hire the right people? Well, start by defining your company's culture. Because if you want to find employees who share your core values, you have to understand what those core values are.

That's what the leadership at Zappos, a top online retailer, did when they created quirky slogans to encapsulate the company's core values: For instance, "deliver _wow_ through service" or "create fun and a little weirdness."

And to ensure all new hires match these values, Zappos tests its candidates by asking them, for example, to evaluate how lucky they are, on a scale from one to ten. Zappos automatically filters out all pessimists — that is, anyone who rates themselves below a seven.

If this process seems overly elaborate, remember that the choices you make during the hiring process will make or break your business. To that end, one of the reasons Zappos cares so much about the recruitment process is because the CEO once calculated that the company lost more than $100 million due to bad hiring decisions.

But even when you've hired the right people, there's still more to do to ensure that your employees love their jobs and perform optimally: You have to treat your team well and prioritize their happiness. After all, if you take care of your workers, they will take care of the customers in the same way.

Companies therefore need to establish an atmosphere of trust, and to make the company feel like a fun place to work. To that end, Facebook's offices are equipped with music and gaming equipment, thus allowing workers to take a break and relax.

### 5. Never compromise on the quality of your product. 

Facebook started out as a 19-year-old college kid's coding project, but it eventually became an indispensable communication tool. How did that happen? Well, the key to Facebook's success is that it has always focused on great product and user experience.

And in fact, great product should be the centerpiece of any business strategy, something you never compromise on. In other words, ensuring that your product matches (or exceeds) customer expectations should underpin every business decision you make.

For example, when Facebook started to grow, Mark Zuckerberg ensured the expansion happened slowly and carefully, so that the company could cope with new demands. So at first, Facebook was only available to Harvard students; Zuckerberg didn't open the service to other universities until there was enough server capacity.

However, although you need to be careful to keep the quality of the product high, that's only one piece of the puzzle: You also need to be innovative — that is, you need to constantly look for new ways of making your product better.

Since innovation is such a crucial factor with regards to product quality, employees should be encouraged to think differently.

To that end, consider Facebook's philosophy to "move fast and break things." Piggybacking on this principle, the company hosts internal hackathons for developers to come up with new ideas — and this very process has helped lead to many great Facebook features, such as Timeline, Chat and even the Like button!

However, the hackathon model doesn't just produce innovation, it also teaches employees a crucial lesson: Not all new ideas work the first time, and failure is OK.

Because ultimately, sometimes you need to fail lots of times in order to succeed spectacularly. For example, James Dyson made 5,127 prototypes before he finally got it right and invented a bagless vacuum cleaner that became a bestselling product in Britain and a market leader in the United States.

### 6. A great leadership team is made up of people with complementary capabilities and experience. 

The best teams are made up of people with different skills. For instance, every great football team has a combination of defenders, midfielders and strikers. Not surprisingly, this model also translates to great leadership teams.

And of course — like any great football team — the most successful leadership teams don't merely have _different_ skills, they have _complementary_ capabilities and experience.

So in that sense, a good business team would combine a _visionary_ (someone with great ideas and innovative zeal) with a _builder_ (someone with great organizational skills and business savvy). This balance ensures that the mission stays in focus while the operational part of the business continues to be developed.

For example, since Sheryl Sandberg ("the builder") became COO at Facebook, Zuckerberg ("the visionary") can focus all his attention on what he's best at: Building products and company vision. Meanwhile, Sandberg has lots of experience in business development (an area Zuckerberg doesn't excel in), which she can bring to the table to optimize Facebook's operations.

It's worth noting that creating this balance of employees has a purpose: It's a way of multiplying the company's chances of success. After all, if your team includes many different areas of knowledge and expertise, you'll be able to examine problems and opportunities from many different angles.

And that's exactly what happened when Steve Wozniak showed his first invention, the personal computer, to his friend Steve Jobs. Wozniak was so proud of his creation, he was ready to give it away for free, for the good of all! However, Jobs — who possessed an inordinate amount of business savvy — saw an opportunity for the two friends to create their own company. And that's how Apple Computers was born: Wozniak created an innovation and Jobs figured out how to turn it into a business.

### 7. Final summary 

The key message in this book:

**The most successful businesses are run by passionate leaders who have a purpose, build powerful alliances and focus on creating innovative, high quality products.**

Actionable advice:

**Don't settle for anything you aren't passionate about.**

It can be easy to fall into a project or a job you don't actually care about, just because you think you ought to. But don't give up on your passion. Instead, keep looking until you find something that truly inspires you. Trust us, you'll know when you've found it!

**Suggested** **further** **reading:** ** _The Facebook Effect_** **by David Kirkpatrick**

_The Facebook Effect_ reveals the inside story of social media site Facebook: its modest origins, its meteoric rise and its continued dominance in social networking. Author David Kirkpatrick shows how Facebook has not only changed how we communicate with each other, but also how we think about politics and the media — not to mention our attitudes toward privacy.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ekaterina Walter

Ekaterina Walter is a marketing innovator for Fortune 500 firms like Intel. After her start-up, Branderati, was acquired by Sprinklr, she became a Global Evangelist. She also regularly contributes to Mashable, Fast Company and Huffington Post.

© Image Mark Zuckerberg: Guillaume Paumier, guillaumepaumier.com

