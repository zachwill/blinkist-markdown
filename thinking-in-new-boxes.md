---
id: 5550ad1f6332630007070000
slug: thinking-in-new-boxes-en
published_date: 2015-05-11T00:00:00.000+00:00
author: Luc de Brabandere and Alan Iny
title: Thinking in New Boxes
subtitle: A New Paradigm for Business Creativity
main_color: ED562F
text_color: ED562F
---

# Thinking in New Boxes

_A New Paradigm for Business Creativity_

**Luc de Brabandere and Alan Iny**

_Thinking in New Boxes_ (2013) takes a deep look into the mind in order to create a clear understanding of the creative process. It gives the reader tools to uncover, manipulate and even create the "boxes" we use to organize information, shape our perception of the world and ultimately enable innovation.

---
### 1. What’s in it for me? Recognize the boxes that trap your thinking, and learn how to overcome them. 

You have no idea how much your mind is trying to betray you. Whether it's making hasty decisions that you regret just a few minutes later, or blocking all the good ideas out, sometimes it feels like our minds are working against us!

Well, thankfully it's not that extreme. Most of our mind's evil tricks can be controlled and turned into advantages — after all, if other people have the same tripwires, knowing how to overcome yours can be the key to understanding those around you.

_Thinking inside the box_ is a problem that almost everyone struggles with. It's a cliché that springs from corporate culture in the 1960s and 1970s, but it refers to real phenomena in our minds.

These blinks explain what it means to think inside the box and gives a simple and straightforward step-by-step approach to busting out of it.

In these blinks, you'll discover

  * how to avoid tunnel vision;

  * why we'll have to re-think our image of the world as round; and

  * how pens almost killed Bic.

### 2. Like it or not, you can’t escape thinking in boxes. 

How many times have you been asked to "think outside the box"? While this sage advice has been around for almost half a century, it's much easier said than done. In part, this is because of the way we think.

Mental models — or "boxes" — enable us to process and make sense of our complex reality.

Our mind is constantly developing simplified models of the world, sorting information into categories and patterns while also developing more sophisticated constructions like rules and paradigms. Without these models, we would lose our ability to think efficiently. In fact, it's impossible to reason without boxes — that's just how our brain works.

If you live in Germany, for example, you've learned to associate light beige cars with taxis, i.e., you've created a "box" for taxis. While you know that not all vehicles of that color will offer you a ride, you can nonetheless rule out numerous non-taxi cars in your search for a ride from point A to point B.

If you travel to New York City, however, you won't linger in a boxless void for long. Almost instantly, you will develop a new box based on the new information from your environment, and thus keep a keen eye out for a _yellow_ car if you need a lift.

See, there isn't just "the box." Rather, there are multiple boxes, all of which are shaped by your identity — and your identity's like a series of boxes, too. You can be comprised of an infinite combination of identities — for example, an IT specialist, an Australian, a woman and so on — and thus there is an infinite number of boxes outside your specific worldview.

Boxes are the buttress for our capacity to think; you can't avoid them. Once you step out of one, you'll simply find yourself in another.

> _"You can't think without boxes, so don't even try."_

### 3. Revolutionary ideas come in new boxes. 

So if leaving one box simply means entering another, how can you ever "think outside the box"? Well, you don't. Rather, innovation requires you to develop entirely new boxes.

If you don't develop new boxes, you run the risk of suffering the damaging side effects of established boxes: _tunnel vision_.

Tunnel vision occurs when you lose the awareness that your individual perception of the world is merely an interpretation based on a subjective sample of information. After all, it's _your_ perception — you can't have anyone else's.

What you perceive is _not_ some objective reality, nor is it even the best guess at what an interpretation of our complex reality should look like, and believing _your_ box is the _right_ one constrains the realm of the thinkable.

For example, just think about how long it took people to accept the idea that, despite their subjective inference that the Earth was flat, it's actually spherical. And in 2011, our understanding of Earth as a round globe was once again dethroned, this time by a more potato-shaped model. And _that_ model will also need to be adapted as rotation and gravity change the landscape of the Earth.

This tunnel vision can be severely damaging for creative thinking. Just look at Bic, a company that up to the 1970s defined itself as a manufacturer of plastic ballpoint pens. But defining themselves in terms of ballpoint pens was too constricting: their only opportunities for innovation were limited to different colors and designs!

If we can overcome tunnel vision, we can create new boxes that offer a range of fresh possibilities.

Replacing the "flat Earth" box with the "globe" box meant that people believed it was impossible to fall off the Earth, thus giving them the confidence to sail all around the world and explore other continents.

And when Bic altered its self-perception from a pen producer to a plastic products producer, it was suddenly able to branch out into a wider array of products from lighters to razors and even cell phones.

So how can we break free from conventional thinking and build new boxes? The following blinks hold the answer.

> _"Even the most seemingly obvious and widely shared boxes should not be confused with reality."_

### 4. Step one: Don’t trust your gut feeling! 

If your "train of thought" were an actual train, would you be a passenger or the conductor? Developing an awareness of boxes and the limitations they place on our thinking is the first step to getting into the engine room and taking control.

Start with the simple understanding that sticking to the familiar will always _feel_ right.

The human mind tends to cling to its existing set of boxes, unconsciously accepting information that confirms our worldviews and discarding contradictory information. This, of course, makes it difficult to adjust our perspective.

For example, when Dick Fosbury set a new Olympic record for the high jump in 1968, he was ridiculed for his unconventional technique. Although jumping backward enabled him to jump higher than anybody else, people simply felt he was doing it _wrong_.

Guess what: Since 1976 no one has won a gold medal in high jump without using the appropriately named Fosbury Flop!

Second, realize that the human mind has an unreliable default setting. We're all routinely fooled by a phenomenon called _cognitive bias_, i.e., the subconscious programming that causes us to favor simple intuition at the expense of objective analysis. As a consequence, we make logical errors and often miscalculate probability, value or risk.

To illustrate this, imagine a frozen pond in winter. Most people would feel more secure skating on the ice if they could see others out there doing the same. Yet their intuition couldn't be more wrong: it's _less_ risky to skate alone, without the additional weight.

Furthermore, as research in behavioral economics has shown, cognitive biases also affect our financial and business decisions. Investors, for example, tend to hold on to stocks even after they've fallen below acquisition price rather than to recognize the loss and cut ties with a losing stock.

> _"It is usually much harder to change existing ideas than to come up with new ones."_

### 5. Step one continued: Doubt your worldview – constantly. 

As we've seen, boxes simultaneously enable and constrain our thinking. But once you are aware that all our subjective and collective models of the world, i.e., our boxes, are necessarily incomplete, you can then examine your _own_ boxes.

Recognizing and challenging your boxes is the key to creating new ones. Ask yourself and your colleagues about key values, objectives, hopes and fears in order to identify which boxes you're stuck in and which beliefs and assumptions might be holding you back.

There are no "right" or "wrong" boxes. Rather, you must ask yourself: Will this box still be useful tomorrow?

For decades, it was understood that male teenagers and young adults were the target market for video games. No one questioned this assumption. After all, why change a winning concept?

Well, because a game company could easily triple their customer base by including other demographics such as women, senior citizens and preschoolers, that's why! But if video game companies aren't _aware_ of this box, then they can't transcend its limitations.

Even when innovators do create new boxes, these new paradigms won't last for eternity.

Say, for example, that a video game company has successfully tapped into new target groups, such as women and toddlers. Good job! However, in the meantime, the company's most loyal customers, the male adolescents, have changed: they've grown up. What do they want to play now? Answering this question will require a new box.

Never forget that even the greatest new idea is destined to become obsolete one day. No matter how thoroughly and deliberately you develop your boxes, you will always have to come back to square one and start all over again. The world won't stop changing, and we must constantly adapt our mental models of it to keep or even set the pace.

> _"All your ideas, even the most successful, are hypotheses within you — and not set in stone."_

### 6. Step two: Arm yourself with fresh input. 

The idea that great innovations come from geniuses who find inspiration from within is romantic, but bogus. As we've seen, we're unlikely to come up with any game-changing ideas if we just let our minds run on autopilot.

For this reason, we need to be diligent in gathering information to remove the blinders imposed by your old boxes.

The authors identify areas where we need to gather information: the global environment (society, economics, science, etc.); your industry, field or area (customers, competitors and so on); and your company, team or context (things like products and organization). The goal of this step is to generate questions, not find answers.

To illustrate this process, let's take another look at the video game industry, this time following the fictional company Ultragames.

Concerning global trends, Ultragames is affected by an aging consumer base along with the evolution of the internet and mobile technology. The company always operated under the assumption that gamers were predominantly male teenagers and young men, yet after investigating new data on their industry, they've discovered that the average gamer's age has risen to 37 and that women now comprise 42 percent of the industry's customer base.

Then, to gather data in the company sphere, Ultragames conducts a survey on its products. It reveals that the emerging adult gamer demographic is less concerned with price, and more interested in Ultragames' "no sex, no violence" policy, as it makes it easy to find and purchase games for their kids.

Now that they've got the data, it's time to develop some questions. One such question Ultragames might ask after pondering this new information is: How can we develop products that appeal to _all_ ages without compromising our family-friendly values?

### 7. Steps three and four: Generate and test hypotheses. 

Imagine you're a gold digger in Yukon in the nineteenth century. You wouldn't expect to find a nugget in your first 30 pans of sand, would you? The reality is that you'll be standing over the water for a _long_ time before you catch gold. The same goes for finding innovative ideas.

At this stage, your objective is to come up with a wide range of possible new boxes. It's important not to limit your creativity by discarding ideas before they have a chance to develop. Go into this exercise with the assumption that there are _no such things_ as bad ideas, and encourage your team to look at your problem from all possible angles, even if they might seem crazy or far-fetched.

For example, when NASA was trying to develop a way to land on alien planets with minimal contamination from terrestrial chemicals, they surely didn't envision their multi-billion dollar spacecraft landing on the planet by bouncing around in a shell made of airbags. Yet that's how _Pathfinder_ ended up landing on Mars in 1997.

Once you have a sizeable collection of ideas, it's time to evaluate them using clearly defined criteria. Your assessments should never be made on intuition, but rather by means of a methodology that prevents the cognitive biases that limit your imagination.

But which criteria should you use? Some, such as budget constraints, technological feasibility or government regulations, are relevant for all businesses. Others are more individual, like your brand image, your areas of expertise, the targeted timeframe, and so on.

Ultragames, for example, might feel that developing individual games for each new market segment is too costly. They need an idea that would satisfy their financial criteria and also compliment their family-friendly corporate identity, such as educational games for all ages.

You may have to repeat these two steps several times before you hit gold. Once you have, remember that you should doubt even your winning ideas!

Now that you've learned how to break free from confining boxes, our final blinks will show you how to create the most innovative ideas for your business.

> _"The best way to have a good idea is to have a lot of ideas."_

### 8. The bigger the box, the bigger the opportunities. 

Imagine your business as a house of cards: if you want to remodel, your mental programming limits you to only working on the upper floor for fear that the whole house will come crumbling down. This comes at the expense of radical transformation, as you are not able to touch the foundations upon which your company was built.

But it is in challenging the most fundamental aspects of your business that you find the most creative potential. Think back to Bic's defining moment. They could have created more superficial boxes, for example, by manufacturing all kinds of pens instead of just ballpoint, or even plastic office supplies.

Instead, they went for an even bigger box: any disposable plastic item.

Other companies have made similarly grand transformations by creating new, big boxes. Nintendo, for example, made playing cards before switching to video consoles, and LG evolved from an industrial chemical producer to a manufacturer of high-tech appliances.

When you dare to fundamentally shift your perception, it unleashes a cascade of new ideas, and you'll discover that each box is a box within a box within a box.

For example, Bic's new concept contains all the boxes we mentioned previously (pens and plastic office supplies), including its original product. Once they created a new big box that defined who they were as a company, they could fill it with medium-sized boxes, and fill those with smaller ones, thus generating a whole range of new products.

Similarly, Nintendo's shift in self-perception opened the door to video game production, merchandising and even a movie.

Thinking big isn't easy when it comes to building new boxes. Nevertheless, if you can create big enough boxes, you will significantly broaden your scope of possibilities.

### 9. Plan for an uncertain future. 

What's the difference between companies that get left behind by revolutionary changes and those that profit from or even spearhead them? In short: Their ability to change boxes in real time.

Long term success in business requires you to keep your mind open to many possible futures. Beware of the ever-lurking trap of tunnel vision: all too often, we plan for _one_ vision of the future — one that is likely based on our present understanding. In other words, we use _predictive thinking_ to prepare for the future.

We should, in contrast, employ _prospective thinking_, which asks: What _could_ happen? What could I make happen?

By imagining multiple possible scenarios, your business can stay flexible and well prepared with not only a plan A, but also plans B and C.

When planning for the future, there are two kinds of reasoning available: _deduction_ and _induction_. With deduction, we take an existing box and sort information into it. For example, you might ask: "What are the most important trends that will affect our industry?"

The problem with this type of reasoning is that the range of possible answers is limited, and focuses too much on what is presently known.

_Induction_, in contrast, works the other way around: you take information and try to find a box for it. Inductive thinking asks questions that have an unlimited set of answers, inspiring creativity by moving away from commonly shared associations.

For example, you might ask: "How could scenario XY become the most important trend affecting our industry in the future?" This approach allows you to discover opportunities other competitors simply don't have on their radars, i.e., truly innovative approaches.

As an example, consider that executives from the high-quality glass industry would likely agree that developing screens for mobile communication devices would be among the most important market innovations in the foreseeable future.

But wouldn't it be better to start a technological revolution while everyone else is competing for a piece of that pie? Like developing materials that make glass obsolete? Or something entirely different?

### 10. Final summary 

The key message in this book:

**It's exceedingly difficult to see beyond our own limited perspective. But we'll have to if we want to think creatively. To stimulate creative thinking, we must constantly challenge our worldview — the "boxes" that encapsulate what we believe to be possible — and apply methods that keep our subconscious from sabotaging us.**

Actionable advice:

**Dare to err!**

A fear of failure will limit you to only obtaining the low-hanging fruit — the safe, obvious and ordinary ideas. Don't be afraid of making mistakes! It's the only way you can break out of your current, confining boxes and develop new ones that inspire innovation!

**Suggested** **further** **reading:** ** _Creativity, Inc._** **by Ed Catmull with Amy Wallace**

_Creativity,_ _Inc._ explores the peaks and troughs in the history of Pixar and Disney Animation Studios along with Ed Catmull's personal journey towards becoming the successful manager he is today. In doing so, he explains the management beliefs he has acquired along the way, and offers actionable advice on how to turn your team members into creative superstars.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Luc de Brabandere and Alan Iny

Luc de Brabandere is a research fellow and senior advisor at The Boston Consulting Group in Paris. He has written and co-authored 12 books, including _The Forgotten Half of Change_.

Senior specialist for creativity and scenario planning at The Boston Consulting Group in New York, Alan Iny has an MBA from Columbia Business School.

