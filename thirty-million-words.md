---
id: 580bb1dbd4d2ce0003dd24e2
slug: thirty-million-words-en
published_date: 2016-10-28T00:00:00.000+00:00
author: Dana Suskind
title: Thirty Million Words
subtitle: Building a Child's Brain
main_color: E8972E
text_color: B57624
---

# Thirty Million Words

_Building a Child's Brain_

**Dana Suskind**

_Thirty Million Words_ (2015) explains the importance of language in a child's early development and long-term success. These blinks outline the optimal early language environment for a child and highlight the ways parents can help their children during these critical years.

---
### 1. What’s in it for me? Give your child a better life through language. 

Many of us remember our parents, grandparents or perhaps older siblings reading bedtime stories to us. These were tales of mystical creatures, talking trees, beautiful faeries and fearless heroes on great adventures.

But what is this fairytale world good for?

Reading and telling stories to our children fosters their inclination toward imagination and, eventually, speech. But you may not have known just how important a positive early language environment is for the development of speech — and for future academic success.

In these blinks, you'll learn how a child's brain is wired and how language plays a central role in the formation of their neuronal network. You'll discover how talking to your children paves the way for future academic achievement and how you, as a parent, can exert a positive influence.

You'll also learn

  * why a child's neurons are similar to freestanding telephones poles;

  * why it becomes more and more difficult to learn a new language as we grow older; and

  * why it's worthwhile to speak at least 2,000 words to your children every day.

### 2. The formation of neural connections during the first three years of life lays a foundation for future cognitive abilities. 

From the moment you emerge from your mother's womb, every single sound, sight and sensation you experience will affect the way you understand the world. This impact is especially palpable during the first three years of your life, during which time your brain's neuronal network forms.

Unlike other bodily organs, the brain is incomplete at birth and functions like the architectural blueprint for a house. Everyone is born with as many as 100 billion neurons, which are initially unconnected, like telephone poles without the lines.

As these neurons become connected, they pave the way for increased brain function; during the critical years, from birth to three years old, between 700 and 1,000 new neuronal connections are made every second.

This complex wiring has an impact on every cerebral function, from memory to emotion to motor skills and language. However, the initial rapid forging of connections is chaotic and the network is gradually pared down to the essentials through a process called _synaptic pruning_.

Synaptic pruning removes extraneous pathways, culling those that are less used while fine-tuning the more specialized connections. The ability to alter these neuronal pathways, known as _neuroplasticity_, is never as great as it is during this period.

And that's a good thing, because the brain development that occurs during these first three years lays the foundation for future intellectual capacity. Just take the acquisition of language: this ability to interpret abstract strings of sounds to form meaning requires profound intelligence.

In fact, a baby's brain recognizes the specific sound patterns of her parents' languages and strengthens these neural pathways, which will lay the foundation for speaking her native tongue. At the same time, the child's brain will purge less frequently used sounds and pathways, reducing its ability to separate the sounds of a new language learned later in life. This is why many people struggle to pick up new languages as they grow older.

### 3. Early language proficiency is central to educational achievement, including abstract and mathematical thinking. 

Language is much more than just speech and words; it's foundational to learning. A poor early grasp of language by kindergarteners will hamper their educational achievements down the line.

Kids that lack a decent understanding of language upon entering kindergarten will quickly fall behind their peers as their reduced ability to translate sounds into meaning takes its toll on their learning. As a result, they miss out on information, further exacerbating their struggles.

This process is similar to trying to converse in a recently learned second language. In such cases, while listening to your conversation partner, you're actively — instead of naturally — processing their words; by the time you understand them, the topic has changed. 

Language is thus key to learning, and that includes areas like mathematical ability as well as abstract and spatial thinking.

People have an innate sense for numbers and can intuitively estimate value. For instance, when faced with two lines at the supermarket checkout, we can quickly choose the shorter one without much thought. But to advance to higher, more abstract orders of mathematics, we rely on our ability to pair this intuition with symbols and words, which makes math dependent on language.

Just take the _cardinal principle_, the ability to recognize that, when counting a set of items, the size of the set is determined by the last number it reaches. This requires the ability to perceive the connection between number, words and their numeric value. An understanding of this principle is imperative to abstract mathematical thinking.

Beyond that, these abstractions lay the groundwork for a child's developing spatial ability and geometric skills. After all, spatial skills, such as the ability to mentally rotate objects, copy 3-D designs and understand spatial analogies, are an essential predictor of success in the STEM subjects of science, technology, engineering and mathematics.

### 4. Educational success depends less on socioeconomic background and more on how much parents talk to their children. 

Language development is the basis of learning in general — but what determines whether a child will have a supportive language-learning environment in their infancy? Does it depend on how rich their family is? Or perhaps on how much their parents speak to them?

Surprisingly enough, a child's socioeconomic status isn't the deciding factor when it comes to their academic success — at least according to a rigorous six-year study published in 1995 by social scientists Betty Hart and Todd Risley.

The experimenters recorded and analyzed footage of families from different rungs on the socioeconomic ladder and found that, in an hour, children from families with a high socioeconomic status heard 2,000 words on average, compared to the mere 600 words heard by their peers lower on the socioeconomic scale. Over time, this amounted to a 30-million-word gap in the number of words that different kids hear by age three.

This difference translated into children with higher socioeconomic status seemingly enjoying a better language-learning environment since they heard more words, which strengthened their neuronal pathways and allowed for optimal brain development.

At first glance, the study seems to uncover an obvious correlation between socioeconomic status and educational achievement — but it actually shows that socioeconomics and race are not necessarily the primary determinants of a child's ability to learn.

Rather, the study found that a child's language learning trajectory was determined by their early language environment, which implied that poorer families had just as much of an opportunity to provide a supportive and nurturing environment for language acquisition than wealthier ones. Kids who grow up in homes with lots of talking, regardless of their parents' economic status or level of education, did better later in life.

But the quantity of words spoken can't be the only factor — doesn't quality matter too?

Absolutely. The greater the variety of words a child is exposed to, the better they'll learn — and the very same study found that quality comes with quantity. In other words, the more parents spoke, the richer their children's use of language would become. After all, as more words are spoken, the likelihood of more varied language increases.

> _"In a country where a great number of children cannot reach their highest potentials, the country cannot reach its highest potential, either."_

### 5. Both children and parents should believe in their abilities. 

Criticism can be upsetting for kids, causing them to retreat into their shells, while excessive praise can make them dependent on the opinions of others for motivation. So how can you bring out the best in your children?

Start by helping them become confident that every goal is approachable and achievable. This means teaching your child that no matter what challenges they face, they'll be able to overcome them through perseverance.

This is called a _growth mindset_. Children who have one are not easily overwhelmed and believe they can reach their full potential through grit and tenacity. The benefits of this mindset were documented in a 1998 study by Professor Carol Dweck, in which 128 fifth graders were given word puzzles to complete.

Once they finished their puzzles, the kids were praised in one of two ways, either for being smart or for working hard. When Dweck asked the children to select a second task she let them choose between one that was similar to the first and one that was harder, but through which they would learn a lot.

Of the kids who were praised for their smarts, 67 percent chose the similar task while 92 percent of those praised for being hardworking chose the more difficult one. In other words, the kids in the hardworking group adopted a growth mindset — and along with it came the confidence that, with determination, they could handle a new task. They became more open to facing new challenges.

Such an outlook will help kids challenge themselves. But parents should also adopt the perspective that they can positively influence their child's intelligence. This might take the form of affirmative feedback, like saying positive and supportive words, or by prompting kids to interact through language, boost their vocabulary and develop their social skills.

This is an essential step because parents with an affirmative view of their role in this process are more likely to provide additional support to their children, which can help them realize their full potential.

### 6. Engage your children’s interests and talk to them to help them learn more effectively. 

So, what else can you do to aid your child's language development and bridge the 30-million-word gap? One good strategy to increase parent-child communication is to follow the _three T's_ : Tune in, Talk more and Take turns _._ Let's take a look at the first two:

First, tuning in to where your child's attention is will enhance their learning development. Say your kid is sitting on the floor playing with building blocks. While you may think it's a good idea to try to shift the activity to storytime, a better idea is to join in with your child's current activity.

To do so, you might help them with the tower they're building and talk to them about its colors, height and what will happen if it gets too tall. Playing and talking with your kids about what they're already focused on will make for a better learning experience, since they're more actively involved.

Another thing to keep in mind is that an infant's brain can't switch between activities as easily as an adult's; it can be a huge waste of time to try to retune their attention to a new activity, especially if that activity seems boring to them.

In this way, tuning in keeps your kid engaged while fostering intimate communication and, ultimately, more seamless learning. But the second T, talk more, is equally important. You should never pass up an opportunity to talk to your children, and a good technique for doing so is baby talk, meaning speaking with a "cooing" pattern of intonation. This will attract your infant's attention, engage their brain and help them learn more easily.

In fact, while baby talk, or child-directed speech, can get a bad rap, it's central to a child's brain development. A recent Stanford University study found that kids who heard more child-directed speech between the ages of 11 and 14 months had double the vocabulary at age two than those with reduced exposure.

But that's not all there is to the "second T." Next, you'll learn more about what exactly "talk more" entails.

> Parental responsiveness has been linked to cognitive development, self-regulation, social-emotional development and physical health of children.

### 7. Talking more entails three vital elements that engage your child while expanding his or her vocabulary and capacity for conversation. 

Talking to your kids is central to their language learning. In much the same way that tuning in implies meeting your child where she is rather than shifting her focus, it's likewise important to talk to kids about what they're focused on. Here's how:

First, talking more is about using narration to turn mundane activities into opportunities for brain development. _Narration_ merely entails describing to your child what's currently happening; it's a great way to fill her ears with language, thereby increasing her vocabulary and strengthening the links between sounds and the objects they correspond to.

The best part is that narration transforms routine events like diaper changing or feeding into educational activities.

Second, talking more also involves the use of decontextualized language, an essential aspect of educational achievement. A child's first language experiences consist of information about the present, things she sees and events she is engaged in.

This is called _contextualized language_ and it stands in contrast to _decontextualized language_, which refers to objects and events that are unrelated to the situation currently at hand. For instance, talking about memories or telling an imaginary story are ways to use decontextualized language. Building the capacity for this form of communication is crucial, since it is used heavily in academia.

And finally, talking more entails expanding on speech every chance you get, in order to build vocabulary and generate flowing conversation. This is key because kids begin to speak using actions, partial words and incomplete sentences. As a parent, you can help fill in the gaps while bulking up the conversation.

For example, if your child says "cat black," you can put her words into a more complete and expanded form like, "Yes, the cat is black. And look how fast the cat moves." By building on your child's speech in this way you can introduce new vocabulary and give her an insight into how conversation develops.

### 8. Encourage your children to speak by taking turns – and remember to read to them. 

The last of the three T's, _Taking turns_, is all about engaging your child in conversation by responding to each other's gestures, sounds and words; this technique is designed to encourage your child to speak.

An essential aspect of successfully implementing the third T is to give your child the extra few seconds he needs to think of a word, instead of saying it for him. While this will initially mean he hears less language, it will lead to conversation, providing more opportunities for language learning in the future.

Another strategy is to ask your child open-ended questions that begin with "how?" or "why?" These questions work well because it's hard to answer them with gestures, thereby prompting your child to speak.

Finally, don't forget to read to your kid and tell him stories. Doing so is an excellent chance to apply all three Ts. In fact, storytime is one of the best chances to put these strategies into practice.

We all know that reading and storytelling give kids opportunities to learn, and with a little extra effort you can take that opportunity much further. First, by paying attention to changes in your child's interest or enthusiasm, you can shift focus to the things that jump out to him; in other words, you can get him to tune in.

Second, in this context, talking more isn't just about reading _more_ books. To get the most out of story time, you should talk to your kid about what's going on in the book and how it affects the characters. Giving such explanations will build meaning in your child's mind.

Then, as a child ages, taking turns becomes more important. At this point, instead of explaining to your child what happened, you should encourage him to share his own ideas about the story and engage in discussion about them.

### 9. It’s up to us to promote language development, and every parent can help. 

Now you know some strategies for helping children with their language learning. But how can you be sure that your kids and others have a solid foundation to lead successful lives?

An important step is to support parenting programs and spread the word about how important the first few years of intellectual development are for a child. In other words, let people know about the difference a good early language-learning environment can make.

Columbia University's National Center for Children in Poverty found that, in 2013, 32 million US children lived in low-income families — the social group most at risk of falling below their potential. Not only that, but James Heckman, a Nobel laureate in economics, estimates that every dollar spent on high-quality early childhood education for disadvantaged kids will produce an annual economic gain of seven to ten percent through improved school achievement, behavior and adult productivity.

So, investing in young children is crucial, but parents also need to nurture their children. By doing so, every parent has the power to shrink the achievement gap and help their kids be all they can be.

This produces a ripple effect as parents are a key component in changing the attitudes of other parents. For example, James, a father participating in the Thirty Million Words program — an educational endeavor to aid children with speech development — grew increasingly confident in his ability to help his son Marcus. He then used his newfound confidence to recruit others and pass his knowledge along.

As James makes clear, enthusiasm is contagious; parents have a lot of power to influence and enhance their children's future. Therefore, programs that focus on parents lay the foundations for broader, federally sponsored efforts that will ensure academic readiness and long-term success.

> _"Why do I tell my friends?"... "I Tell them because I want their kids to have the same advantage my kid has."_ \- James, a father in the Thirty Million Words program

### 10. Final summary 

The key message in this book:

**Every child has the potential to succeed socially and academically. But to do this, they require assistance from their caregivers from a very early age. This means it's important to create positive environments for early language learning and spread the word about this central aspect of parenting.**

Actionable advice:

**Encourage your child's ability to self-regulate.**

Offering your child choices like one between wearing a red shirt or a blue shirt today will provide an opportunity for him to consider options, weigh important factors and make an informed decision. This approach also avoids using directives that discourage brain building and results in a discussion that lets the child speak more, consolidating their vocabulary and allowing them to express their emotions.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Wonder Weeks_** **by Hetty van de Rijt, Ph.D., and Frans Plooij, Ph.D.**

_The Wonder Weeks_ (1992) is all about the major changes babies go through during their first year and a half of life. These blinks explain the huge developmental steps that every infant makes as they discover their own identity, learn how to move, talk and relate to others.
---

### Dana Suskind

Dana Suskind is a surgeon specializing in hearing loss and cochlear implantation. She is the founder of the Thirty Million Words initiative, a program focused on the critical importance of early language exposure on a developing child.

