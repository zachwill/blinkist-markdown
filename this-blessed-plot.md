---
id: 59288fd9b238e10007b6b088
slug: this-blessed-plot-en
published_date: 2017-05-29T00:00:00.000+00:00
author: Hugo Young
title: This Blessed Plot
subtitle: Britain and Europe From Churchill to Blair
main_color: 285168
text_color: 285168
---

# This Blessed Plot

_Britain and Europe From Churchill to Blair_

**Hugo Young**

Nowadays, it's common knowledge that Britain sees itself as distinct from the rest of Europe. For instance, they use the pound instead of the euro and do not take part in the EU free travel zone. But how did Britain's relationship with Europe end up like this? That's what _This Blessed Plot_ (1998) is all about. It explains that, since WWII, Britain has had a conflicted relationship with the European project, filled with negotiations and exemptions. With the "Brexit" now on everyone's mind, find out more about this complicated history and what it might be able to tell us about Britain's future.

---
### 1. What’s in it for me? Learn about the complex relationship between the United Kingdom and the European Union. 

It's not often that one knows for sure that a date will go down in history. However, June 23rd, 2016, is such a date. On that day, British voters chose "Brexit," that is, for the UK to leave the European Union. Though opinion polls showed it would be close at the ballot boxes, most pundits, journalists and politicians were stunned and surprised at the outcome. But should they really have been so surprised?

The United Kingdom's relationship with the rest of Europe in general, and with the European Union and its predecessors in particular, has been complicated, to say the least. In these blinks, we'll go back to the end of World War II and examine the evolution of this complex relationship, gaining on the way a deeper understanding of what led up to the historic times we're living in.

In these blinks, you'll find out

  * how the United Kingdom's colonial past affected their integration into European cooperation;

  * how a dispute in Egypt shaped the United Kingdom's relationship with continental Europe; and

  * how tightening European cooperation led to political tension decades before Brexit.

### 2. Britain’s post-war relationship with continental Europe got off to a shaky start. 

Britain was in a unique position at the end of WWII: They were the only European power that had successfully defended its land from Nazi invasion, and they used this position to help liberate the rest of the continent from fascist rule.

Winston Churchill, eager to prevent future catastrophes, saw the unification of Europe as a way to accomplish this.

The war, Churchill recognized, was caused in large part by the divided state of Europe. He hoped that the old rivalries between these nations could be extinguished if they were all united, both economically and politically.

In fact, he'd been dreaming of a unified Europe since 1930, when he published an article calling for a "United States of Europe."

In 1946, Churchill carried that dream to Zürich, where he gave a major speech that highlighted his ideas to the rest of the continent.

The first step in his plan called for a Council of Europe, which wouldn't interfere with national sovereignty but instead act as a forum to help kickstart the process of deepening ties between European nations.

French and German leaders were particularly receptive to this plan and it inspired them to continue working together.

Churchill, however, having lost the general election to the Labour Party in 1945, was unable to put his European plan to work in the Britain.

The Labour Party opposed the "European supranationalism" of Churchill's plan, preferring a more international — rather than regional — approach to European policy-making. This broader approach included the creation of NATO and providing economic aid to Europe through the American-led Marshall Plan.

Over the next few years, Britain continued to oppose plans for further European integration. So, France and West Germany moved on without them and, in 1950, they formed the European Coal and Steel Community (ECSC).

The ECSC acted as a common market that also included Italy, Belgium, Luxembourg and the Netherlands. By joining the ECSC, these nations gave up some sovereign powers to a supranational European authority, making the ECSC the first version of what would later become the modern European Union.

> _"We must recreate the European family in a regional structure called, as it may be, the United States of Europe."_ \- Winston Churchill

### 3. As Western Europe came together, Britain had other problems to deal with. 

In 1951, Churchill and his Conservative Party were voted back into government, but by now the message of European unity had been replaced by other priorities.

Indeed, it was Britain's partnership with the United States that was taking precedence in British decision-making.

To begin with, having supplied 15,000 troops to assist America in the Korean War, Britain wanted to make sure that the United States would help control Soviet activities in eastern Europe.

This partnership created many conflicting interests, including the desire for a unified Europe.

For Britain to keep the Soviets at bay, they needed to continue funding the Marshall Plan and support keeping US troops in Europe.

But in return, the United States wanted Britain to lead the call for European unity.

This presented a big conflict for Britain because they now believed that a unified Europe would bring about two things they were afraid of: the loss of their uniquely influential power in Washington and the withdrawal of US troops from the Continent.

With these fears in mind, Britain remained on the sidelines regarding European unity. However, a geopolitical crisis was about to make Britain reconsider its stance.

In 1956, Egypt nationalized the Suez Canal, much to the dismay of Britain and France, which had both held claims in the region for decades.

And as Britain was debating how to respond to this crisis, it became clear that there was another big problem: the United States refused to provide Britain with military and economic assistance, effectively leaving Britain to fend for itself.

But the operation against Egypt proved too costly, and without American assistance, Britain was forced to abandon the enterprise, a move that left them looking like a deeply weakened power in the eyes of the rest of the world.

This is how the Suez Crisis marked the beginning of the end of the British colonial empire, and forced Britain to once again realize the benefits of joining the growing European family.

### 4. Britain recognized the benefits of the EEC, but maintaining relations with its Commonwealth was problematic. 

In January of 1957, the political fallout from the Suez Crisis led the Conservative Party to reshuffle its leadership and make Harold Macmillan the prime minister. Like Churchill, Macmillan, who clearly saw the benefits of a unified continent, had a history of pro-Europeanism.

This proved providential, because just two months after Macmillan's appointment, the leaders of the ECSC signed the Treaty of Rome and gave birth to the European Economic Community (EEC).

The EEC was designed to create a common market without trade barriers and it further reduced the customs duties between the nations that took part.

It was clear to Macmillan that Europe would continue to unify with or without Britain and that if Britain continued to resist joining, its position would continue to deteriorate.

Germany was a case in point. For the first time, Germany had a larger economy and was a bigger exporter than Britain, and it was quickly continuing to grow.

Finally, in 1962, Macmillan submitted Britain's application for entry into the EEC.

However, there were still some negotiations to conduct and a number of stumbling blocks to overcome before it was a done deal.

A major roadblock was Britain's relationship with its extended Commonwealth.

Ideally, Britain wanted to be a part of the EEC but also continue to maintain its preferential trading deals with its Commonwealth nations. In short, the Brits wanted to have their cake and eat it too.

But after the Suez Crisis, it was clear that Britain's colonial empire was deteriorating and that any economic advantages it could still provide were going to be meager compared to the growing European economy.

In fact, Britain was becoming less important for the Commonwealth nations: Australia was finding better deals by trading with Asia; Canada, by trading with the United States. And while Commonwealth trade with Europe was on the rise overall, their trade with Britain was decreasing.

In the end, however, it was a different relationship that got between Britain and the EEC.

### 5. Britain’s efforts to join the EEC faced stiff resistance, both at home and abroad. 

On January 14, 1963, French President Charles de Gaulle vetoed Britain's bid to join the EEC. But why did he do such a thing? After all, excluding Luxembourg, the members of the EEC were furious at de Gaulle's action.

Chances are, de Gaulle felt threatened by Britain, fearing that it might supplant France as the political leader of the EEC.

Two years prior, de Gaulle had told Macmillan that he supported Britain's joining the EEC. But it now became clear that he was opposed to the British Commonwealth entering the common market.

De Gaulle even raised the question of what possible role India and Britain's African territories could play in Europe.

This brought them to an impasse. Macmillan was still keen to take part, but France's veto meant Britain's entry into the EEC would have to be put on hold.

Macmillan didn't get a second chance, however. A year after the veto, his Conservative Party was again replaced by the Labour Party, this time led by Prime Minister Harold Wilson.

At this point, the Labour Party was so opposed to joining the EEC that the party's former leader, Hugh Gaitskell, gave a blistering speech in 1962 declaring that joining the EEC would mean the end of Britain.

It was with this platform of Euroscepticism, pro-Commonwealth and pro-sovereignty that the Labour Party was able to capitalize on Macmillan's political failings and win the election.

This message worked again in the 1966 election, allowing Wilson to stay in power, though by now it was beginning to dawn on him that Britain's position couldn't last much longer.

As a matter of fact, the Commonwealth was crumbling all around him: Rhodesia had declared independence from Britain and other Commonwealth nations were getting increasingly upset with Wilson's poor decisions.

This anger reached a boiling point when Wilson decided to begin withdrawing British troops from Southeast Asian territories such as Singapore.

And things weren't much better at home, as Britain continued to face one domestic economic crisis after another.

### 6. With the economy in trouble during the 1960s, the barriers to Britain’s EEC entry slowly fell away. 

In light of Britain's increasing economic struggles, it finally dawned on Wilson that best way to restore prosperity was to shift focus away from the Commonwealth and toward Europe.

So Wilson began a cautious campaign for British entry into the EEC.

In early 1967, he delivered a speech in Strasbourg that hinted at the full reversal he'd made in his European policy. Wilson's speech alluded to Britain and Europe's shared history, even referencing the original Anglo-Saxons who came from the continent to settle in Britain.

Then, on May 2, 1967, he announced that Britain intended to submit its second application for the EEC.

Remarkably, the response was universally positive. The mood had changed drastically in the four years since France's veto, with many wondering how Britain's entering the EEC might alter things for the better.

Even Charles de Gaulle seemed more receptive. This was likely due to the Russian occupation of Czechoslovakia and the awareness that a united Western Europe was needed in order to properly deal with Soviet aggression.

Regardless, de Gaulle soon stepped down from the presidency. In 1969, the door was wide open for Britain's entry.

However, 1970 was another election year, and Wilson planned to submit the application afterward, confident that his Labour Party would win.

Alas, Wilson's predictions were wrong. The Conservative Party took control that year, and the fate of Britain's European future lay in the hands of Prime Minister Edward Heath.

At this point, though, Heath's policy on the EEC didn't differ much from Wilson's cautious pro-Europeanism, and the application and negotiations proceeded on schedule.

But when it came to the negotiations, Britain was no longer the great world power it had been but a decade before and was certainly in no position to make demands.

The tables had turned, and this time around Britain was willing to agree to the established EEC rules, though some exceptions were made.

For example, the EEC allowed Britain to have temporary special trade arrangements with the Commonwealth for a five-year transitional period.

### 7. Britain began its shaky relationship with the EEC during a chaotic time. 

Britain officially joined the EEC on January 1, 1973. A year later, however, the Labour Party was back in power, after having campaigned on a ticket that promised to renegotiate the terms of Britain's entry.

Many of the Labour Party's left-leaning members were strongly against the EEC altogether and viewed it as a threat to British workers.

So, while the renegotiations were partly successful, a majority of the party still wanted to leave the Common Market. This left things at a bit of a deadlock, so the party decided on a referendum; with a new set of terms in place, they asked the British people whether or not they wanted to stay in the EEC.

The vote was set to take place in 1975, a particularly unstable year for Britain.

A global recession had been hitting Britain badly for the past two years, driving inflation rates up by 25 percent.

Further, Irish militants had been launching anti-British attacks that were weighing heavily on the public consciousness.

Making matters worse, following the increasing popularity of the pro-independence Scottish National Party, there were growing fears in England that Britain was on the verge of breaking up altogether.

With all this uncertainty in mind, many voters regarded the EEC as a stable body that could possibly help Britain get back on its feet and stay in one piece.

In fact, pre-vote polling consistently showed that two-thirds of the public were in favor of the EEC, including business leaders and much of the nation's media.

Meanwhile, the anti-EEC campaign was busy spreading its opinion that staying in the Common Market would cause further economic hardships and put their sovereignty at risk.

These campaigners failed to influence the mainstream consensus, however, and on June 5, 1975, 67 percent of the British electorate voted to remain in the EEC.

### 8. For better or for worse, Margaret Thatcher’s leadership further integrated Britain into the EEC. 

In 1979, the reigns of the British government again changed hands, and this time the Conservative Party was being led by Prime Minister Margaret Thatcher.

Another round of renegotiations were soon underway, as Thatcher was eager to lower Britain's net contribution to the EEC.

Much of this haggling occurred during backroom meetings at the 1979 European Council summit in Dublin, where Thatcher reached a temporary deal that lowered Britain's contribution for two years.

Unlike previous negotiators, Thatcher had a direct way of doing things that could come off as abrasive, and yet, it was also effective.

So when the temporary two-year deal ran out, the negotiations started once again, this time at the 1984 EEC summit in Fontainebleau, France. And Thatcher was successful again, managing to secure a permanent rebate for Britain.

But this wasn't the only purpose of the summit: Thatcher got along well with French President François Mitterrand, and with his help, she hoped to launch a new and improved EEC.

The plan was to move the Common Market toward a truly "single" market, or, as Thatcher preferred, an unregulated "free" market.

But this goal of free trade had unintended consequences that would eventually lead to Thatcher's downfall.

For example, in 1986, they passed the Single European Act, aimed at completing the transition to a single market within six years. But this inadvertently led to proposals that Thatcher was completely opposed to, such as a monetary union and a European central bank.

And then the President of the EEC announced that he intended to give the supranational body even more autonomous power. This is when Thatcher realized that things were getting out of hand, but, at this point, there was little she could do to stop the momentum.

### 9. In the 1990s the Labour Party and Tony Blair took advantage of Britain’s divisiveness over Europe. 

After the declaration of a monetary union, Thatcher only intensified her anti-EEC attitude. This inflexibility eventually led to the party replacing her in 1990 with a more pro-EEC leader.

But then, in 1991, the Maastricht Treaty was signed into effect, giving others opposed to the EEC even more reason for concern.

The treaty was actually a huge step toward Churchill's dream of creating a United States of Europe. But, in this case, it was to become known as the European Union.

While the Conservatives had won the 1992 election on a pro-EEC platform, the Maastricht Treaty was a divisive subject within the party.

Even though Britain managed to obtain an exemption from the planned monetary union, many conservatives feared that Britain's economic autonomy would be snatched from Westminster and placed in the hands of unelected bureaucrats in Brussels.

These fears continued after the treaty was ratified in 1993. And a large group of eurosceptics formed in the Parliament and put pressure on the Conservative Party leadership.

The party began tearing itself apart and, unable to reconcile its differences, lost the 1997 election.

As a result, the Labour Party emerged victorious with Prime Minister Tony Blair leading Britain's first entirely pro-European party.

This united front was a stark contrast to the disorganized in-fighting in the Conservative Party, but there was another reason for the Labour Party's pro-Europeanism: the atmosphere of anti-Thatcherism in the 1990s (Thatcher was perceived as being against a united Europe) set the stage for the Labour Party's winning platform.

Blair had even declared support for the monetary union, but when it came down to it, during a 1998 meeting with the eleven other EU heads of state, every member except Blair signed a pledge to begin using the Euro within the next five years **.**

This was another political move on Blair's part, who feared that, by signing the pledge, he'd trigger a negative reaction from the right-wing press that might cost him the next election.

Even though he'd been elected on a pro-Europe platform, Blair knew that it would take more than an election to change Britain's enduring feelings of being separate from the rest of Europe.

### 10. Final summary 

The key message in this book:

**The post-war relationship between Britain and its European neighbors has been filled with political maneuvering and carefully negotiated back-room deals. Caught between commitments to its Commonwealth, its American interests and its need to fix its own troubled economy, Britain eventually joined the European project in 1973 — 20 years after its inception. Decades later, not much has changed: Euroscepticism still plays a strong role in British politics, and its relationship with the continent continues to be anything but stable.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Crisis in the Eurozone_** **by Costas Lapavitsas and others**

These blinks explain the root of the eurozone crisis in a comprehensive, methodical way. They shed light on the deep structural problems the eurozone is facing and outline scenarios that could help restore competitiveness among the southern peripheral states of the region.
---

### Hugo Young

Hugo Young was a British author and acclaimed journalist whose work appeared regularly in _The Sunday Times_ and _The Guardian_. His other books include _One of Us_, an award-winning biography of Margaret Thatcher. He died in 2003.

