---
id: 54574dc53365660008010000
slug: this-changes-everything-en
published_date: 2014-11-03T00:00:00.000+00:00
author: Naomi Klein
title: This Changes Everything
subtitle: Capitalism vs. the Climate
main_color: 30C5F1
text_color: 269CBF
---

# This Changes Everything

_Capitalism vs. the Climate_

**Naomi Klein**

_This Changes Everythin_ g addresses one of the most pressing issues today: climate change. The book outlines exactly how we're harming the planet and why we've thus far failed to stem our destructive behavior. Author and activist Naomi Klein also points out how some early movements are meaningfully fighting climate change and what more needs to be done to prevent global disaster.

---
### 1. What's in it for me? Learn about what's happening to your planet. 

Climate change may completely destroy the world as we know it. If we keep polluting the earth at our current rates, we could face massive social, economic and ecological disaster.

So why are we doing this? Why do industries and corporations continue to pollute, even though the scientific community agrees that such behavior is destructive? And how can certain politicians and people _deny_ that climate change is happening, when the evidence for it is so clear?

In these blinks, you'll hear how the activities of certain groups, particularly the fossil fuel industry, are causing global temperatures to rise, and why such groups aren't interested in changing their methods. You'll also learn about the movements that are working to curb climate change, and what you can do to help.

In the following blinks, you'll also discover:

  * why efforts by the United Nations to fight climate change have been less than helpful;

  * how a volcanic eruption sparked an idea to combat an increase in global temperatures; and

  * why a powerful, global movement for renewable energy started in a small English town.

### 2. Greed and extractivism push people to make profits, even if it means destroying the environment. 

The fight against climate change has been ongoing, with mixed results. If we continue to pollute the earth at our current rate, we could face the real possibility of environmental and social collapse.

Back in 1990, global leaders in an attempt to curb climate change agreed to a goal of limiting global temperatures to an increase of 2 degrees Celsius. The most effective way to make this happen would be to cut _carbon dioxide_ emissions.

Yet to meet the goal, wealthy countries would have had to cut carbon dioxide emissions by eight to ten percent. Not only have countries failed to cut emissions, but the opposite has occurred.

In 2013, global carbon dioxide emissions were 61 percent _higher_ than they were in 1990!

So what exactly is the problem? Our inability to tackle climate change appropriately is mostly due to short-term greed and _extractivism_. Extractivism is the exploitation of natural resources for profit.

The story of Nauru, an island in the Pacific Ocean, illustrates just how disastrous extractivism can be.

Nauru was rich in a valuable mineral called phosphate. In the 1960s, Nauru allowed mining companies to begin extracting the phosphate, creating an incredibly profitable industry. By the 1970s, Nauru had the second-highest gross domestic product (GDP) per capita in the world.

This prosperity, however, came at a price. The people of Nauru became greedy, especially for imported processed food. Diabetes and obesity rates soared, so much so that the small nation soon became known as the "fattest country on earth."

What's more, the phosphate was mined carelessly, and quickly. The mineral was soon mined out, and the industry collapsed along with the island's economy.

Today, Nauru is riddled with debt and vast swaths of land have been destroyed by the former mines.

> _"Surviving is not the same as thriving, not the same as living well."_

### 3. The practices of the fossil fuel industry endanger both people and wildlife. 

Fossil fuels such as oil, gas and coal are the main energy source in the world today. We burn such fuels to generate electricity as well as keep ourselves warm in cold weather.

Yet fossil fuels are quite dangerous, and they've been shown to directly interfere with our health.

Fossil fuels are especially hazardous to those who live near processing plants. The people of the Aamjiwnaang First Nation, an indigenous community in southern Ontario, live near a petrochemical plant where crude oil and natural gas are processed. Health problems have plagued the community.

The birth rate of Aamjiwnaang boys declined dramatically between 1993 and 2003 because of the group's exposure to dangerous chemicals such as p _olychlorinated biphenyls_, used in coolants. During that decade, only one-third of the children born were male.

Animals are also endangered by the fossil fuel industry. In 2010, for example, the _Deepwater Horizon_ oil platform located in the Gulf of Mexico experienced a blowout that resulted in large quantities of oil being leaked into the sea.

After this disaster, researchers found elevated levels of heavy metal in oysters, for example. Some 89 percent of oysters in the area were found to have a form of _metaplasia_, an abnormality that interferes with reproduction.

The effects of such a disaster have lingered, too. Two years after the explosion, fisherman in Florida noticed an unusual lack of young fish.

Scientists learned that the fish had been in their larval stage at the peak of the disaster, and they died because of the pollution. Thus the spill didn't only kill living animals, but also wiped out future generations.

### 4. Right-wing critics of climate change are working to protect their interests, not the earth’s. 

Do you believe climate change exists? If so, you agree with a consensus of the scientific community.

Shockingly, you may also be in the minority in society at large.

In 2007, 71 percent of Americans believed that burning fossil fuels was causing climate change. By 2009, this figure had dropped to 51 percent, and by 2011, it was 44 percent.

This skepticism is shared by certain politicians, especially those in the U.S. Republican Party. In some areas, only 20 percent of Republicans accept the scientific evidence for climate change. Of the remaining 80 percent, some are actively trying to fight against climate change claims.

People who benefit from the largesse of the fossil fuel industry as well as those on right of the U.S. political spectrum, often dismiss climate change as a farcical claim. A study in 2013 found that 72 percent of books denying climate change are linked to right-wing think tanks, such as the _Cato Institute_.

Some "conferences" on climate change are also pushed by right-wing ideologists, such as the annual _Heartland Climate Conference_, hosted by the right-leaning research facility the _Heartland Institute._

These conferences claim to be scientific, but most of the speakers have ties to the fossil fuel industry.

Willie Soon, an astrophysicist and keynote speaker at the Heartland Conference in 2011, is one example. Between 2002 and 2010, he received _100 percent_ of his research grants from companies like _ExxonMobil_.

Why do people on the right oppose climate change? Simple: it challenges their ideology.

James Delingpole, a Heartland speaker, argues that climate change is a left-wing myth. Tackling climate change would require the rich to pay higher taxes, and necessitate more government intervention in the economy — causes the left-wing support and the right-wing hates.

Which may mean if we successfully overcome climate change, we may end up with a more left-leaning society in the United States. Thus those on the right benefit if we _don't_ fight.

> _"The bottom line is that we are all inclined to denial when the truth is too costly — whether emotionally, intellectually or financially."_

### 5. Some environmental groups shockingly cooperate and get money from the fossil fuel industry. 

Do you think it's possible to drill for oil and protect endangered animals on the same piece of land?

Well, the _Nature Conservancy_ thinks so.

The Nature Conservancy is an environmental group that was initially created to protect land and water for people and animals.

In 1965, the group was interested in protecting the Attwater's prairie chicken, an endangered ground bird that lives along the coast in Texas and Louisiana. The population was at risk from the growing oil and gas industry in the area.

In 1995, _ExxonMobil_ donated some of their property to the Nature Conservancy to help protect the endangered animal. Unbelievably, the Nature Conservancy responded by beginning to drill for oil on the donated land. The group argued that their drilling would bring in revenue and not interfere with the bird's habitat.

They were wrong.

Previously, the Attwater's prairie chicken was bred in captivity to then be released into the wild. When the construction of a gas pipeline was delayed, however, the birds' release was also delayed. When the birds were finally set free, they were all eaten quickly by the region's wild animals.

And the Nature Conservancy continued drilling. The last of the Attwater's prairie chicken in the area disappeared by November 2012.

The Nature Conservancy isn't the only environmental group associated with the fossil fuel industry, too. _American Electric Power_, a dirty-coal utility that's had a long relationship with _Royal Dutch Shell_, has donated to the _Conservation Fund_ and _the World Wide Fund for Nature (WWF)_.

We need to question the integrity of environmental groups that receive funding from multinationals involved in oil extraction. How can they protect our earth and cooperate with the fossil fuel industry at the same time?

Anyone looking to donate money to environmental causes should carefully analyze which groups truly deserve their support.

### 6. International trade agreements often carry more influence than do climate agreements. 

In 1945, the United Nations was formed to foster global cooperation. One of its many tasks is drafting international agreements.

One type of agreement is a trade agreement, which stipulates general rules governing international trade. In 1994, The _World Trade Organization_ (WTO), which works to liberalize international markets, was founded by such an agreement.

The United Nations also works on _climate agreements,_ such as the 1997 _Kyoto Protocol_. As part of the Kyoto Protocol, various countries agree to reduce emissions that are harmful to the environment.

Yet when the goals of trade and climate pacts conflict, trade agreements often prevail.

Canada's most populous province, Ontario, in order to reduce emissions made a goal to be coal-free by 2014. To reach this goal, the government in 2009 created legislation to support renewable energy as part of the _Green Energy and Green Economy_ act.

The program from the start was a success. By 2013, Ontario was the largest solar energy producer in Canada, and the province had only _one_ coal-fired power plant still in operation.

Yet to qualify for the program, energy providers had to ensure that a percentage of their workforce and materials were local to Ontario. Japan and the European Union however considered this stipulation to be a violation of WTO rules, as it discriminated against foreign manufacturers and effectively violated the organization's free-market doctrine.

The WTO ruled that Ontario's provision to require local workers and materials was illegal, so Ontario was forced to change it. It is yet unclear how the ruling may affect the overall program.

Yet the fight is an interesting example of how international bodies can push free-market interests over the environment. The positive effects of the Ontario program on limiting fossil fuels, for example, weren't a factor at all in the legal decision.

> _**"** How absurd, then, for the WTO to interfere with that success — to let trade trump the planet itself."_

### 7. Loopholes in well-meaning environmental programs encourage companies to cheat and pollute. 

In addition to climate agreements, the United Nations also develops programs to help curb harmful emissions. These programs however have had mixed success.

The _Clean Development Mechanism_ _(CDM)_ was created in tandem with the Kyoto Protocol in 1997. The goal of both was to reduce global carbon dioxide emissions, also known as "greenhouse gases."

The CDM stipulated that companies in developing countries could receive a certificate, or a "carbon credit," for reducing emissions. A company could then sell the certificate to a firm from a developed country, which when purchased, would give that firm permission to continue to pollute.

This mechanism was seen a win-win: companies in developing countries would get money for emitting less, and companies in wealthier countries would have to pay to emit more.

Yet the CDM doesn't work, thanks to a few loopholes that essentially give companies an incentive to produce more greenhouse gases.

After the CDM was put into practice, companies in developing countries focused their efforts on acquiring certificates for financial gain.

Coolant factories in China and India, for instance, manufacture a certain gas used for air conditioning and refrigeration which emits a harmful gas called _HFC-23_ as a by-product. HFC-23 however can be destroyed with relatively inexpensive equipment.

Such companies earned lots of carbon credits that they could sell to Western companies by destroying HFC-23. In 2012, some _93.4 percent_ of one Indian firm's revenue came from selling carbon credits.

Many critics believe that Chinese and Indian companies now produce HFC-23 _on purpose_ so they can receive more certificates for destroying it. These meaningless certificates when sold then allow companies in developed countries to emit more, so overall emissions still increase.

Thus instead of encouraging the reduction of emissions, the CDM encouraged companies to cheat.

### 8. While some billionaires acknowledge climate change, they're doing little to prevent it. 

While many of the world's billionaires certainly have the means to help fight climate change, the actions of the few who show some interest have still been lacking.

In 2007, Warren Buffett, the owner of investment company _Berkshire Hathaway_, said that climate change is serious and that preparing ourselves for such an event is important.

After meeting with former U.S. Vice President Al Gore about climate change, billionaire Richard Branson of the _Virgin Group_ sought to invent a new way of doing business. He called his plan g _aia capitalism_, a fusion of the goals of making money and preserving the environment.

And at the 2006 _Clinton Global Initiative_ meeting, Branson also announced that he would spend $3 billion to help develop biofuels as an alternative to oil and gas.

Unfortunately, much of these statements haven't resulted in concrete action.

Buffett is the owner of coal-burning utilities, such as energy provider _MidAmerican Energy Company_, and his investment company holds a large stake in _ExxonMobil_.

Moreover, Buffett announced in 2009 that his investment company would invest $26 billion in the _Burlington Northern Santa Fe railroad_, one of the biggest transporters of coal in the United States.

And some eight years after his pledge, Branson as of 2014 is estimated to have donated only some $300 million to biofuel development.

As part of his Virgin Group, Branson is the owner of airline company _Virgin Airlines_ — and it goes without saying how polluting the airline industry is.

Yet Virgin Airlines has grown despite Branson's promise to preserve the environment. Some 15 million passengers flew on Virgin flights to Australia in 2007, and some _19 million_ made the trip in 2012, an increase of 27 percent.

So if the individuals with the money to make change aren't doing much to help the environment, what other options are out there?

### 9. Solar Radiation Management could help fight climate change, but it has its disadvantages. 

So are there other ways to prevent global warming besides reducing carbon emissions?

In 1991, the eruption of a volcano in the Philippines offered scientists an interesting idea: dimming the light of the sun.

When Mount Pinatubo erupted, it sent a large amount of sulfur dioxide and dust directly into the stratosphere — a rare occurrence. The particulate matter remained there, acting effectively like light-scattering mirrors and preventing some of the sun's radiation from reaching the earth's surface.

The scientific journal _Nature_ investigated global temperatures the year following the eruption and found something remarkable: the effects of the stratospheric disturbance had caused a drop in global temperature by as much as 0.5 degrees Celsius.

This event inspired many discussions about _Solar Radiation Management_ (SRM), or the artificial injection of radiation-scattering particles into the stratosphere.

SRM is now considered a "back-up plan" for tackling climate change. If we are not able to cut greenhouse gas emissions sufficiently, SRM might offer another option.

There are a few ways SRM could be put into effect. Helium balloons equipped with a mechanism to spray sulfur dioxide could be launched into the stratosphere, for instance.

The drawbacks of such an approach are many, however. Sulfur dioxide could created a permanent haze in the stratosphere, making pure blue skies a rarity and obscuring our view of the stars.

And if we go through with SRM, it can't be reversed; and carbon dioxide emissions will still remain a problem if we don't act. So while SRM could help, we need to be cautious.

Reducing greenhouse gases globally is still the top option to combatting climate change.

### 10. Wanted: Strong governments to fine fossil fuel producers and to support renewable energy. 

Many countries have laws against littering. If you throw trash on the street and a police officer catches you in the act, you have to pay a fine.

We need to use this same principle to fight climate change. Strong governments should make fossil fuel companies pay for the pollution they cause.

These companies can certainly afford the fines. Fossil fuel companies made $900 billion in profits between 2001 and 2010. _ExxonMobil_ holds the record for the highest corporate profits _ever_ reported in the United States: in 2011, $41 billion, and in 2012, $45 billion.

Part of the reason ExxonMobil's profits are so high is that they're seldom on the hook financially for the pollution they cause.

This needs to change. Yet it won't without a strong government that can enforce restrictions and fines on this polluting industry.

Unfortunately, most governments won't curb fossil fuel companies' behavior as so many other industries depend on these energy sources.

In tandem, governments need to support efforts to expand the development and use of renewable energy. Germany serves as an interesting role model in this field.

The German government guarantees that renewable energy providers have priority access to land. The government also offers providers guaranteed prices, so their risk of losing money is low.

Such policies have created strong incentives for companies to invest in renewables. Since the policies have gone into effect, some 1.4 million photovoltaic arrays for solar power have been installed and 25,000 windmills have been built in Germany.

While Germany's progress toward more renewable energy is good, there's still room for improvement. The government needs to regulate the burning of coal, especially since the country decided to abandon nuclear energy in 2011.

Following this decision, Germany has unfortunately filled its energy gap with brown coal, which has pushed the country's emissions output higher.

So it's clear a strong government is crucial in combating climate change, but it isn't only the government that needs to act.

> _**"** Like Angela Merkel, Obama has a hell of a hard time saying no to the fossil fuel industry."_

### 11. Blockadia is a growing global movement that fights extractivism all over the world. 

Have you ever heard about a movement called _Blockadia_?

Blockadia is international activism geared to curb the power and influence of multinational corporations involved in fossil fuel extraction. It is a loose affiliation of people who not only want to stop extractivism, but also are seeking a better way forward.

Thus Blockadia appears wherever it is needed, from individual protest to group action, all over the world.

When a group of _Greenpeace_ activists was arrested in Russia in 2013, Blockadia was there.

The thirty activists were sailing on the Greenpeace ship _Arctic Sunrise_ to protest oil drilling under the melting ice in the Russian-held regions of the Arctic. The crew was arrested by the Russian Coast Guard and held in custody for two months.

The Blockadia response to the arrest was huge: heads of state complained about the arrest, 11 Nobel Peace Prize winners offered public shows of support and public demonstrations took place in at least 49 countries. Subsequently, the activists were released.

Blockadia efforts also helped fight the _Keystone XL_ project, a plan to expand the Keystone pipeline that delivers oil from tar sands in Canada to the United States. Tar sands contain a dense form of petroleum; a potential spill from the pipeline could cause serious environmental damage.

In February 2013, more than 40,000 people demonstrated against Keystone XL at the White House in Washington, D.C. People from all sorts of backgrounds were united in their opposition: vegan activists and cattle farmers, ranchers and indigenous peoples.

The spirit of Blockadia prevailed and their activism was successful; the project was put on indefinite hold. From this victory, the power of Blockadia around the world is likely to increase.

### 12. Sell your stocks in fossil fuel companies, and act locally to make the switch to renewables. 

Do you hold stock in fossil fuel companies? You might think about divesting yourself of those shares.

Major public institutions too are beginning to sell their interests in fossil fuels companies, in response to activist pressures.

If you compare fossil fuel companies' carbon reserves with the amount of carbon that scientists say we can still emit before we reach a disaster point, two things become immediately clear:

First, that the fossil fuel industry has no interest in fighting climate change.

And second, we need to force the fossil fuel industry to change.

One way to do this is to limit the political influence of these companies. Activists must work to make all publicly run institutions stop investing in the fossil fuel industry.

In 2012, for instance, 13 American colleges and universities announced that they would divest their endowments of fossil fuel stocks and bonds. Two years later, Stanford University also announced that it would divest its endowment of coal stocks. That endowment alone was worth $18.7 billion.

If more people and institutions continue to pull their money out of the fossil fuel industry, the idea is that the industry will lose its political influence as it is more and more stigmatized.

Smaller communities can also make a difference. A small English town called Totnes did just that in 2006. The town started the _Transition Town_ movement, by drafting an "energy descent action plan" to articulate the town's move toward using renewable energy rather than fossil fuels.

The Transition Town movement is powerful, as it doesn't only help the environment but it also brings members of a community closer together, as they join a common fight. Since Totnes, the movement has spread to over 460 locations in at least 43 countries.

So although there is a lot of work ahead, the good news is that meaningful efforts have already begun in the fight to curb climate change.

### 13. To keep climate change at bay, we need a mass social movement and direct action on a global scale. 

The Chinese philosopher Lao Tzu once said, "If you do not change direction, you may end up where you are heading."

We should all heed this warning with regard to the pollution of our planet.

If we want to change direction, we need a mass social movement against climate change. This is the only way that climate change can be stopped.

Brad Werner, a complex systems researcher, gave an important talk about this idea at the 2012 _Meeting of the American Geophysical Union_. He spoke on how global capitalism has made it convenient, fast and easy for humans to deplete the earth's resources.

Our planet is now dangerously unstable, he says, thanks to us.

Werner argues that only a mass social movement against global capitalist culture can change this. _Blockadia_ is an early manifestation of this kind of movement, but it still needs to grow.

Social movements have been transformative in the past, and they can be transformative in the future. The movements for the abolition of slavery and independence from colonial rule, for instance, completely changed how societies were organized from that point forward.

More recently, the uprisings in the Arab world showed us how social movements can topple dictators. The revolutions in Egypt, Libya and Tunisia took nearly everyone by surprise.

Transformative movements can happen even faster now as the world is much more connected. This is especially true when there's an immediate crisis, or a cause for taking to the streets.

There is no doubt that we'll face another crisis — environmental or financial — due to climate change, and we need to be ready to fight it.

A mass social movement is the only thing that can empower us to fight, and win.

### 14. Final summary 

The key message in this book:

**So far, we've been failing to fight climate change. Global carbon dioxide emissions are still on the rise and are reaching dangerous levels. Government intervention is necessary to make the transition from fossil fuels to renewable energy sources. To make governments act, we need a broad social movement that won't just fight climate change, but change everything.**

Actionable advice:

**If you want to fight climate change, act.**

Participate in demonstrations, educate yourself and work with the right organizations. Most importantly, do what you can to pressure public institutions to divest their interests from the fossil fuel industry, and push for more government intervention in the fight against climate change.

**Suggested** **further** **reading:** ** _Prosperity_** **_without_** **_Growth_** **by Tim** **Jackson**

_Prosperity_ _Without_ _Growth_ argues that our present model of economic growth is not sustainable: it strains the resources of our planet to a breaking point, and causes climate change, environmental damage and psychological harm. Jackson presents a vision for a sustainable, ecological economic model that focuses on public welfare rather than growth, and explores the ways in which this transition might be realized.
---

### Naomi Klein

Naomi Klein is a climate activist and writer of international bestsellers _The Shock Doctrine: The Rise of Disaster Capitalism_ and _No Logo: Taking Aim at the Brand Bullies._ She's also on the board of directors of _350.org_, an organization that aims to build a grassroots movement to address climate change.

