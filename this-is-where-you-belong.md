---
id: 580b66aad4d2ce0003dd2443
slug: this-is-where-you-belong-en
published_date: 2016-10-25T00:00:00.000+00:00
author: Melody Warnick
title: This Is Where You Belong
subtitle: The Art and Science of Loving the Place You Live
main_color: EF384E
text_color: EF384E
---

# This Is Where You Belong

_The Art and Science of Loving the Place You Live_

**Melody Warnick**

_This is Where You Belong_ (2016) is a guide to loving the city you live in. These blinks explain how to enjoy your life wherever you are by discovering all the amazing opportunities your city has to offer.

---
### 1. What’s in it for me? Make yourself at home wherever you live. 

Are you happy in the city you live in right now? Or are you exploring other options, hoping to find that one place that fits like a glove — a beautiful place that's as quiet or lively as you want it to be, where your neighbors are fun and open-minded and where you can pursue all your favorite pastimes?

It turns out that you don't need to search far and wide for a place like that. If you give it a proper try, you can turn almost any city or village into a true hometown. Once you're willing to make an effort, you'll find amazing opportunities abound in your city. These blinks will tell you how to get started.

You'll also discover

  * why your life might actually be more strenuous than that of a fighter pilot;

  * why you won't get to know your city from behind the wheel of a car; and

  * why there are people who feel less nauseous thanks to their neighborly relations.

### 2. People move from place to place, hoping to find happiness – but what we really want is a place to call home. 

Do you find yourself constantly moving from city to city, unable to settle down? Well, you're not alone. Every year in the United States, 36 million people move cities for new jobs alone. And while jobs are just one of the many reasons people choose to move, behind every one of these reasons lies one thing: hope.

In other words, people move to new places hoping to find a better life. After all, moving seems to offer the chance to restart your life with a clean slate and, as a result, people tend to move because they've experienced too many failures, like disappointing friendships, unbearable commitments or unsatisfying jobs.

What it all boils down to is that humans expect to find a better life somewhere else, linking our happiness to geography.

This causes people to move in search of that happiness, but what we really want is simply to feel at home somewhere. Humans are instinctively driven to create deep connections with our surroundings, to identify with them.

This form of connecting with the area we live in is called _place attachment_ and you know you've formed one when you get back to your hometown and say with a sigh, "it's good to be home!" The connection to your city builds a feeling of security and happiness.

And it can be good for your health, too. People who are happy about where they live and like the people around them are less likely to suffer heart attacks or strokes.

Ironically, it's the very habit of running away and searching for happiness somewhere else that prevents people from finding what they truly crave: a place that feels like home. In an attempt to overcome this dilemma, the author decided to try a new strategy: instead of moving cities, she tried to love the one she already lived in.

In the following blinks, you'll find out how.

### 3. Walking and biking help you connect with your city and are good for your health. 

The first step to falling in love with your city is getting to know it intimately, and there's no better way to do so than by taking a walk or bike ride. Exploring your city in these ways, especially using back roads and almost getting lost will create a detailed _mental map_ of your city.

As you navigate through the city you'll find yourself associating different places with specific thoughts and rich sensory experiences. For example, every time you pass a café in your neighborhood, the smell of fresh coffee might spark a memory of the first time you went there.

The reason walking and biking are important here is that when you drive, you miss out on this close interaction, hidden behind your windshield and too focused on traffic to appreciate the sights. As a result, your mental map forms with less detail.

Another perk of a strong mental map is that it will help you get to places instinctively. Navigating without thinking too much about it will make you feel more at home.

Finally, getting around by foot or bike will also enhance your physical well-being, which will, in turn, make you feel better about your city. People who walk regularly feel better about their lives, are more creative and have more energy. Obviously, you're more likely to see your surroundings positively when you're always in a good mood.

Conversely, research has shown that commuters who are forced to fight through rush-hour traffic or ride in crowded trains have a more powerful stress reaction, as judged by their heart rates and cortisol levels, than even fighter pilots or riot police facing an angry crowd. Naturally, these people will have a harder time appreciating their environment.

### 4. City life is more pleasant when you shop and eat locally. 

Shopping can be a great way to connect with your city — if you frequent local stores, that is. After all, by supporting local businesses, you'll help your city's local economy, which will lead to improvements in infrastructure and, in turn, your quality of life.

For instance, a study of businesses in Salt Lake City found that 52 percent of the money spent at local businesses continues to circulate locally. That's compared to the meager 14 percent that remains in local circulation after being spent at big corporations.

So, in Salt Lake City, if you spend $25 at a local store, $13 will stay in the city, whereas if you spent the same at a big chain store, just $3.50 would remain. If you buy locally, more of the money you spend will go towards your city's infrastructure through taxes. Local schools, for example, will have more money to hire teachers, while more streets will get repaired, both of which are likely to improve your life and those of your family members.

But that's not the only way buying locally can benefit you. Shopping at local stores and eating local food can also make you feel more attached to your city. These businesses are probably run and frequented by your neighbors, while offering a relaxed environment where you can chat about your offspring or discover how those beautiful apples were grown.

It's through these everyday encounters that you'll become a part of your local community and start to feel at home in your neighborhood. Beyond that, local restaurants offer a taste of the cultures that live in your city, which will also strengthen your connection to the place in which you live.

Just take a study done in 2013 on more than 800 residents of Aragón, Spain. It found that 80 percent of them enjoyed local food because it gave them a sense of belonging; in other words, patronizing local businesses can make you feel more at home.

### 5. Becoming involved in your local community is good for your health and exploring what your city has to offer will help you enjoy it. 

Are you the kind of person that spends a lot of time with their neighbors? Or perhaps you don't live in the best part of town and can't wait to leave your house in the morning. Well, regardless of where you fall on the spectrum, it generally enhances your well-being to become involved in your local community — and it's not so difficult to do.

A recent study found a close link between a person's neighborhood relationships and their health, and here's how:

For eight days, 2,000 participants were asked to report on their emotional state as well as any physical symptoms they experienced, from nausea to shortness of breath. They were then asked a series of questions to determine how connected they felt to their neighbors.

The research found a correlation between the two sets of data: the more a participant felt connected to her neighbors, the less she suffered from physical ailments and the better she felt emotionally. The benefits are clear — so try getting to know your neighbors, whether by going to a block party or seeking out a local community garden.

Forging connections with your neighbors plays an important role, but being happy in your city can also come from exploring and learning about everything it has to offer. After all, a town is only as great as you think it is and you can only explore potential if you see it.

Every community has its upsides and it's up to you to find out what they are. For instance, if your city is full of yoga studios, ask yourself if you might enjoy attending a few classes.

Or, in some cases, you'll discover your city has an asset that makes it well-suited to a particular activity, like mountains that are great for rock climbing, but for which no group exists. Why not just create one? Just seek out an instructor and form a climbing group!

> "What's good for the community is usually good for you."

### 6. Green cities enhance your well-being, compelling you to give back. 

For generations, humanity has been making the big move from the countryside to cities, yet people are usually happier and healthier when in nature. As such, it's no wonder that green spaces in a city are good for your health; of course, the happier and healthier you are, the more likely you are to enjoy the place you live.

That makes _biophilic cities_, those that have lots of green space like parks, community gardens and green roofs, especially great places to live. So, how can you tell whether your own city is biophilic?

Just ask yourself what outdoor activities you can do in your city. If there are lots of parks to walk through and fields to play sports in, you likely live in a green-friendly city.

This is important because studies have found that those who reside in green cities and take part in outdoor activities tend to have stronger immune systems. They also have higher cognitive functioning, better concentration and are often thinner than people who spend less time in green spaces.

Being outdoors makes you happier and healthier, and when people are happy to be in a city, they grow attached to it, making them more willing to get involved in activities that protect the place in which they live. After all, loving a place makes you want to invest in keeping it beautiful or improving it in any number of ways. The best part is, in addition to improving your city, volunteering will make you feel better — even if it's just by picking up litter in the park.

If you don't have the time or energy to invest in physical efforts, you can give money to a cause you believe in. Just think of what you value in your town and write a cheque. Maybe you love the gardens your city is famous for or want to support the local hospital that specializes in cancer treatment.

> _"You volunteer, so your town becomes better, which makes it easier to love, which makes you more attached to your town."_

### 7. It’s both possible and healthy to love your city, no matter what happens. 

Say you're finally starting to love the city you live in — but all of a sudden, a disaster strikes. Should you move away? Not necessarily.

When a city finds itself in crisis, people begin to form closer and more caring connections. For instance, in the aftermath of Hurricane Sandy, 33 percent of residents said that the storm brought neighbors together and 36 percent said they're now connected to neighbors they didn't know prior to the storm.

These connections are crucial because they help people feel more rooted in the place they live in. So, while cohesion and social connections can't stave off natural disasters, these human relationships _can_ help people manage stress and suffering, since feeling attached to and invested in others makes us want to help them.

Beyond that, for most people, a strong attachment to a place is left unscathed by a natural disaster. Just take Hurricane Katrina, which, despite destroying much of Louisiana, couldn't make most residents even dream of moving away. They were so committed to their hometowns, no matter how ravaged, that they preferred to stay and rebuild.

But if you do feel compelled to move cities, whether it's because of a disaster or simply a new job, you'll be better off if you try hard to love that new place. Just remember, wherever you are, there are always amazing things to discover and interesting people to meet. As a result, it's always possible to make fond memories and explore a place, regardless of where it is.

And, most importantly, committing to loving a place is the only way you'll escape the endless cycle of moving from one city to another, desperately searching for happiness.

Play the hand you're dealt and, in the end, you'll be a lot happier if you make a commitment to your city.

> _"If you went all in and invested big time in your city, you'd eventually experience a kind of religious conversion from Mover to Stayer."_

### 8. Final summary 

The key message in this book:

**You can be happy wherever you are as long as you put a little effort into discovering the amazing opportunities that your city has to offer. To do so, just connect with the people around you, enjoy the natural beauty of your surroundings and volunteer to make your community stronger and more enjoyable.**

Actionable advice:

**Connect with your community**

Ever heard of Good Neighbor Day? Most people haven't, but what an excellent excuse to celebrate the people who live around you. So, the next time the 28th of September approaches, invite all your neighbors to a dinner party to celebrate and take the opportunity to connect with your community.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Happier at Home_** **by Gretchen Rubin**

_Happier at Home_ (2012) is a guide to transforming your home into a sanctuary that reflects your family's personality. By helping you identify both your and your family's needs, this book gives you everything you need to start changing your home and family life for the better.
---

### Melody Warnick

Melody Warnick is a freelance journalist and contributor to _Reader's Digest, Woman's Day_ and _The Atlantic's CityLab,_ among other publications. She lives in Blacksburg, Virginia, the city that she fell in love with by participating in the Love Where You Live experiment.

