---
id: 576175050beabd0003161b07
slug: this-is-why-we-cant-have-nice-things-en
published_date: 2016-06-20T00:00:00.000+00:00
author: Whitney Phillips
title: This is Why We Can't Have Nice Things
subtitle: Mapping the Relationship Between Online Trolling and Mainstream Culture
main_color: F0E430
text_color: 8A831C
---

# This is Why We Can't Have Nice Things

_Mapping the Relationship Between Online Trolling and Mainstream Culture_

**Whitney Phillips**

_This is Why We Can't Have Nice Things_ (2015) explores the subculture of trolling: where it came from, who does it, why they do it and what exactly it is they do. The book examines the blurred line between a malicious online attack and revealing social commentary, and shows how trolling and mainstream culture have come to form a close bond.

---
### 1. What’s in it for me? Step behind the scenes of the internet’s own breed of provocateurs. 

Have you ever come across an obviously incendiary comment on a YouTube clip or Facebook post? If so, chances are you've met a troll. Trolls and the act of trolling have become a widespread phenomenon; some consider it a problem, some a testament to the internet's freedom of expression and some just don't care at all. So what is trolling and is there really any reason for alarm?

Well, the answer isn't so clear-cut. Since it first emerged on the internet, trolling has evolved and has assumed many different forms. In these blinks, you'll explore the origins of trolls, why they troll and what impact — both negative and positive — trolling can have on society. Trolling might even be able to tell us something about ourselves and the society we've created.

You'll also find out

  * what lulz are and how they fuel the trolls;

  * why some trolls argue that they're just doing what the media already does; and

  * how trolling can be utilized for positive social change.

### 2. Trolling takes many forms, but usually it means posting intentionally hurtful or provocative opinions online. 

If you've spent a lot of time clicking and scrolling your way through the internet, you've probably heard of _online trolling_ and those who take part in it, the _internet trolls_. But what is this whole subculture about?

Trolling is discussed constantly all over the internet and in the media, which can make it difficult to define.

When it comes to trolling, most people will immediately think of the message boards and comment sections of websites and social media. This is where you'll find many anonymous users, or _trolls_, who post nonsensical, disruptive and sometimes offensive comments.

Many of these comments appear to be posted with the aim of angering others, and the trolls seem to take a perverse pleasure in ruining a complete stranger's day.

While these antagonistic web comments can lead to cyberbullying and general online aggression, these blinks will focus on a specific type of trolling: the kind perpetrated by people who self-identify as internet trolls and use highly stylized tactics to intentionally provoke others and produce _Lulz_.

Lulz is an aggressive form of humor that mocks anything and everything, allowing trolls to derive their pleasure at the expense of others. These negative emotional reactions from the victims become both the reason and justification for Lulz.

But even as we focus on this one area, we can still find a wide spectrum of trolling behavior.

While some of it is incredibly aggressive, disturbing, tasteless and comes close to crossing the legal threshold for harassment, other forms are comparatively harmless.

Take _Rickrolling_ for example: this is a tame form of trolling, much like a schoolboy prank, where internet surfers click on a link only to find themselves redirected to a clip of Rick Astley's 1987 hit "Never Gonna Give You Up."

As we'll see, the word "trolling" is a complex term that brings together a wide variety of people, ideas, intentions and actions.

### 3. Seen as a potential problem in the early days of the internet, trolling became a popular subculture in the 2000s. 

To fully understand trolling, we first need to take a step back and look at how the word has evolved from being an accusation to becoming a point of pride and self-identification.

At the dawn of the internet era in 1992, trolls were seen as possibly the greatest obstacle to forming an online community.

Around this time, trolling was a term used to describe and condemn any disruptive and malicious behavior on the internet. People believed that trolls who acted as imposters, or under the veil of anonymity, posed a serious threat to the internet's status as a legitimate and progressive way to communicate and connect.

But by the mid to late 2000s, trolling blossomed into a subculture that people were eager to be a part of, with some even finding mainstream success.

This is when users began to identify themselves as trolls and develop their own unique language and practices, many of which would be used to create _memes_.

internet memes began as inside jokes that were created within the troll communities of the internet. Since they were made using their own specific language and format, only those familiar with the ways of the subculture could understand the memes.

But nowadays, memes are an ever-present form of internet communication.

There's even a Wikipedia-like site called KnowYourMeme.com, devoted to cataloging any and all online memes. And these days, anyone with a computer can go to a "Meme Generator" website to create their own.

A central reason for this mainstream acceptance is the emergence of the website _4chan_. Its popularity has been instrumental in making these troll-made artifacts recognizable to a wider audience.

For example, LOLcats first appeared on 4chan as a series of images with cute cats accompanied by funny absurdist captions. These went on to spawn a multi-million dollar company and a website called _I Can Has Cheezburger_, which pioneered the trend of humorous, animal-based memes in mainstream culture.

But as we'll see in the next blink, trolling isn't all cute cats and harmless fun.

### 4. RIPtrolls are an example of the fine line between trolling’s cruelty and social commentary. 

Trolling was founded upon the idea of disturbing the peace by disrupting the standards of polite communication and decency. But sometimes these tactics also reveal uncomfortable truths about their targets.

There is one phenomenon that is a perfect example of how trolling works: RIPtrolls.

In the late 2000s, memorial groups dedicated to the recently deceased began to pop up on Facebook. Many of these were for young white teens who had passed away and created by family members to provide a public forum for friends and family to share their grief.

Since these Facebook groups were often public, strangers also began posting comments, writing things like, "I didn't know you, but I'm sorry you died…"

Soon enough, RIPtrolls began to flood these groups with mocking and offensive messages, such as, "Who died??? Some suicidal slut." Or, in the case of a girl who had hung herself, "Alexis Pilkington Hang in There!"

At first glance, such comments can seem unusually cruel and heartless. But others have argued that some RIPtrolls were providing important social commentary by criticizing what they called _grief tourists_ : strangers who are just as guilty as the media in giving excessive attention to dead white kids.

RIPtrolls pointed out that their targets weren't family members or friends of the deceased, but rather those on the sidelines who were exploiting the tragedy by airing their false sympathies.

They were also indirectly trolling media outlets whose continuous reports sensationalized the deaths of dead white teens, which would often have its own negative effects. For example, a report in the _New England Journal of Medicine_ revealed that teen suicide rates go up following media reports on suicide: the greater the coverage, the greater the increase.

Trends like this demonstrate the fine line between shameful behavior and the social context of trolling. And in the next blink, we'll take an even closer look at the relationship between trolling and the media.

### 5. Trolling behavior is not so different from mainstream media practices. 

As we've seen, the media and the trolling subculture can sometimes appear to be at odds with each other — but there are also common traits among them. After all, trolling and mainstream media both take advantage of spectacles to be successful.

In the case of the suicides of white teenagers, we saw how both the media and the RIPtrolls used the spectacle of this attention-grabbing issue for their own gain:

For the media, sensationalizing this kind of issue translates into high ratings and website clicks, which in turn brings in advertising revenue and profit.

For trolls on the other hand, success is not about money — it's all about the Lulz.

More insight into this relationship can be found in the aftermath of a Fox News investigation, called _Report on Anonymous_, that aired in July of 2007.

_Anonymous_ was the name that trolls on 4chan were going by and, in their report, Fox referred to them as a "hacker gang" and an "Internet Hate Machine," while suggesting they were "domestic terrorists."

Naturally, the trolls were delighted by this report.

Fox News had been a target of trolling by 4chan since it first launched, and this fear-mongering story was just another example of its shallow and sensationalistic practices. Unwittingly, Fox News was providing perfect ammunition for the trolls.

Not only that, but the report also gave trolls even more mainstream attention.

The Fox News clip was posted on YouTube the same day it aired, and as of 2014 it had received 2.5 million hits and over 20,000 viewer comments, shining a national spotlight on Anonymous.

While the influence of trolling will never be seen as entirely good or bad, in the next blink we'll look at further examples of what it can reveal about our society.

### 6. Trolling is catching on beyond the internet and has even found its way into politics. 

Through its ongoing relationship with mainstream culture and media outlets, trolling has managed to evolve into a more progressive form of activism, and even branch out beyond the internet.

In 2008, the website _Gawker_ posted a leaked video of Tom Cruise praising the Church of Scientology overzealously. Not long after the video became public and garnered widespread ridicule, embarrassed church officials tried to have it censored and taken down.

Anonymous saw this as a violation of freedom of speech and launched their own campaign, _Project Chanology_, designed to further embarrass and criticize the Church of Scientology. And on February 10, 2008, they coordinated demonstrations featuring hundreds of masked protesters outside Scientology centers across the United States. You could call this a display of real-life trolling.

While mainstream culture has continued to be influenced by trolling, it has also picked up some lessons along the way.

In 2011, a public library in Troy, Michigan was in danger of being shut down due to budget cuts. Despite the fact that a slight tax increase of 0.07 percent could have saved it, the local Tea Party was opposed to this legislation.

To fight back, advertising agency Arc Worldwide used some clever trolling tactics: they posed as a Tea Party group called _Safeguarding American Families_ and launched a multimedia campaign, advocating against the tax increase and announcing plans for a book burning party.

Troy citizens were outraged and saw to it that the tax increase was passed and the library was saved.

Even the government has found that trolling tactics can be useful in fighting threats to society.

In 2012, the US State Department created a program called Viral Peace in an effort to use "logic, humor, satire and religious arguments" to confront, undermine and demoralize violent extremists. It's no coincidence that these are the exact same tactics that internet trolls have been using effectively for years.

So, while there is no doubt that trolling has its darker side, it has also had an undeniable influence on society and retains an ability to reveal a great deal about who we are.

### 7. Final summary 

The key message in this book:

**Online trolling is not merely an oppressive entity of heartless people and belligerent behavior. When we take a closer look at the trolling phenomenon, we can see that trolls' methods and tactics actually reveal a great deal about who we are. From trends in mainstream culture to the practices of corporate media companies, trolling can shine a light on both good and bad aspects of our society.**

Actionable advice:

**Remember the roots of memes**

internet memes are everywhere. Next time you take a screenshot of a funny one to send to your friend, consider how the meme is a communication tool that emerged from the troll subculture! Fascinating, right? It might make you think think twice about how oft-demonized "trolls" are not so one-dimensional.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _So You've Been Publicly Shamed_** **by Jon Ronson**

_So You've Been Publicly Shamed_ (2015) looks into the terrifying nature of online public shaming. Tracing it back to its historical roots, the book details the motivations behind modern public shaming and offers tips on what to do if you find yourself at the center of a public shaming scandal.
---

### Whitney Phillips

Whitney Phillips earned her PhD in communications at the University of Washington; _This Is Why We Can't Have Nice Things_ is a revised version of her dissertation. Her findings on trolling have appeared in numerous publications, including _Scientific American_, _NYMag_, _The Atlantic, VICE_ and _The New York Times._

