---
id: 554781923666320007bd0000
slug: this-is-your-brain-on-music-en
published_date: 2015-05-07T00:00:00.000+00:00
author: Daniel Levitin
title: This Is Your Brain on Music
subtitle: Understanding a Human Obsession
main_color: 497D71
text_color: 497D71
---

# This Is Your Brain on Music

_Understanding a Human Obsession_

**Daniel Levitin**

In _This is Your Brain on Music_ (2006), musician and neuroscientist Dr. Daniel J. Levitin illuminates exactly what happens in the brain when people listen to rhythms, timbres and pitches, helping us understand why we're so profoundly affected by music.

---
### 1. What’s in it for me? Discover how the wonders of music arise in the brain. 

Throughout history, music has been a central part of human culture. Whether it was a Homeric epic sung at a festive procession in ancient Rome, a shepherd's lonely pipe echoing through the hills or a solemn hymn sung at a funeral, music has always deeply engaged and moved us.

The rise of modern neuroscience is shedding new light on that mysterious phenomenon known as music and, although there is still much to discover, it's clear — music is an integral part of our brain.

So what is music and why does it make us feel so good? How does it arise in the brain? What is the connection between music and memory? Why are musicians so irresistibly sexy? These are some of the questions answered in these blinks.

In these blinks, you'll learn

  * what distinguishes music from mere noise;

  * why groovy tunes are so groovy; and

  * how hearing that old song can bring back so many memories.

### 2. When basic elements of sound combine in a meaningful way, it gives rise to music. 

What is music? While some people consider classical music the only "real" music, other people are adamant devotees of Elvis Presley or Michael Jackson. So, is it possible to come up with a general definition of music?

Well, yes! We can define music as a meaningful combination of specific building blocks.

As opposed to random noise, music has fundamental elements that come together to create meaningful relationships with one another.

The most important of these fundamental elements are pitch, rhythm, tempo, contour, timbre, loudness and reverberation.

_Pitch_ answers the question "what note is that?" — it's the only thing that changes during the first seven notes of the childrens' song "Mary Had a Little Lamb."

_Rhythm_ involves the duration of a sequence of notes, and it is the only variance in the first seven notes of The Beach Boys' hit "Barbara Ann." Every note is sung at the same pitch.

_Tempo_ is the general speed of a piece, and _contour_ refers to the overall shape of a melody — that is, whether a note rises or falls.

_Timbre_ concerns the tonal characteristics that distinguish one instrument from another when they are sounding the same note.

_Loudness_ is the amount of energy an instrument creates, and _reverberation_ involves how far away from us we perceive the sound to be, or how large the room is in which the sound occurs.

When we take all these attributes and combine them in a meaningful way, they give rise to higher-order concepts, such as melody, that form we perceive as music. We actually often think of music in terms of melody — the succession of tones we hear.

### 3. Some think music exists because it gives us pleasure. But most theorists believe music has an evolutionary basis. 

Where does music come from? It's a simple enough question, but it has an extensive history of conflicting answers.

For instance, one small school of scientists absolutely disregards its evolutionary origins.

Cognitive scientist Steven Pinker posited that, although there is an obvious evolutionary purpose behind language, music is a mere by-product of language which happens to bring us pleasure.

Other theorists concur, asserting that music serves purely hedonic purposes and arguing that if music was suddenly eradicated, everything else in life would go on unaltered, because music doesn't serve an adaptive purpose.

However, the majority of music theorists, the author included, state that music _does_ have an evolutionary origin. In fact, most cognitive psychologists researching music argue that it evolved because it fostered cognitive development.

Actually, the idea that music was what paved the way for speech for our pre-human ancestors is pretty compelling. Because music and speech share many of the same features, music could have been a way of honing the motor skills that vocal speech requires.

In addition to its possibly being a precursor to speech, music may have been an early means of courtship. Darwin in fact made this argument about 150 years ago: Musicianship, much like the peacock's tail, is a sign of sexual fitness. In order to sing and dance well, you must be mentally and physically fit.

Also, musical proficiency and success signal stability to potential mates. If you have enough leisure to sing and dance, you probably have plenty of food to eat and a place to take shelter.

Because it probably signalled biological and sexual vigor, we have reason to believe that music indeed served evolutionary purposes.

But regardless of whether it serves an evolutionary purpose or not, music is here and it affects us on multiple levels. So let's take a look at how it affects our brain.

### 4. Processing music involves almost every region of the brain that we know of. 

Thanks to modern-day brain imaging techniques and neuropsychological advances, scientists have reached a point where they can identify specific brain regions that are involved in processing music.

What have we discovered, then, when we examine how the brain responds to music?

Music processing takes place in nearly every area of the brain, with different aspects of music being managed by different regions.

Different musical characteristics, such as pitch, tempo, timbre and so on, are analyzed and combined to build a comprehensible representation of what we're hearing.

When we listen to music, our brain's subcortical structures are the first responders. These structures are the oldest parts of the human brain; they handle things like emotions and the planning of movements.

Next come our auditory cortices, which are placed on both sides of the brain.

Then, as we're trying to follow along with the music, our memory center is triggered, which includes other regions of the brain, like the hippocampus.

This high-level processing then incorporates the aforementioned basic elements, such as pitch and tempo, to make up an integrated representation. This occurs in the more sophisticated areas of the brain, connected with planning and self-control.

Although it may seem like a serial processor, all this is executed in parallel. The brain's auditory system needn't wait to figure out what the pitch of a sound is in order to find out where it's coming from; the brain circuits dedicated to these two different procedures are trying to dig up the answers at the same time.

In summary, the brain first draws out low-level features of the music using specialized regions and networks, and then melds these into an integrated sense of form and content.

### 5. Appreciation of music is linked to the brain’s ability to predict what will come next, and a good composer controls these musical expectations. 

Chances are you've been to a couple of weddings in your life. As you've sat through the service, have you ever noticed that many people only start to tear up when the music begins? Why is this?

The emotions inspired by music result from our ability to forecast what will come next in the music. By knowing what our expectations are, composers imbue music with emotion by deliberately controlling whether or not these expectations will be met.

It is the adept manipulation of the expected and the unexpected that makes "Here Comes the Bride" an emotional song, rather than a lifeless, robotic one.

In fact, laying the groundwork for and manipulating expectations is at the core of music, and can be done in myriad ways.

Take the electric blues. A standard move in this genre is to violate rhythm expectations by building up momentum and then ceasing to play completely, while the singer or lead guitarist continues.

Another way our expectations are controlled by composers is through melody.

For instance, the classic _deceptive cadence_ is when the composer repeats a chord sequence over and over until the listeners is lead to expect another repetition — but then, at the last minute, an unexpected chord is played that doesn't completely resolve.

Composers play with the tendency for the melody to "want" to go back to its jumping-off point. You can hear this in "Over the Rainbow," in that surprising leap from the first to the second note of the chorus — "some-WHERE."

This leap disrupts our expectations, but then the composer compensates for this, soothing us by bringing the initial melody back again. The trick is not to overdo it, but to keep building tension; that's the art of composition.

> _"It is when the music begins that I start to cry."_

### 6. Songs can act as keys to memories, because people use the same brain regions for remembering as they do for perceiving music. 

You know the feeling of re-listening to some old music that sends you down memory lane? Suddenly, you're immersed in all the sensations and perceptions that accompanied the first time you listened to the song.

For many of us, songs are tied to specific memories. But how can music unearth memories that were otherwise buried or seemingly lost?

Well, songs can function as keys to our mind, unlocking all the experiences we associate with a song.

Neuropsychologically speaking, recognizing a tune involves a number of complex neural computations that interact with memory. That is, when we listen to a song, it appears that memory extracts an abstract generalization for later use.

Interestingly, this is why we can recognize a song almost immediately and accurately, even if it's been transposed to another key or had it's original tune somehow deformed.

The song leaves an imprint in the brain that is triggered when the song is recalled.

Studies that tracked people's brain waves while they both listened to and imagined music showed that the pattern of brain activity between these two actions is indistinguishable. So, when we perceive a piece of music, a particular set of neurons fire in a particular way, and when we remember it, we recruit that same group of neurons in order to create a mental image of it.

This is known as the "multiple-trace memory model" and it posits that the traces left in our brains store both the abstract and the specific information contained within the songs.

This explains why we can retrieve a faded memory of our childhood when an old song suddenly comes on the radio.

### 7. The brain responds emotionally to the violations of timing in music that has groove. 

Thump-thump-thump-thump: the pulse in music is what lets us know we're moving forward and that we can anticipate certain things at certain points in time in the music.

When the pulse or beat division in a piece of music creates a strong momentum, it's known as _groove_.

Groove is a quality that can be achieved by subdividing the beat in different ways, accenting some notes in a different way than others. But the best grooves involve a subtle aspect of performance.

People generally agree that groove functions best when it's not machine-like, and instead has a drummer who speeds up or slows down the tempo in a subtle way that is informed by the emotional nuances of the music. This is what makes the rhythm track "breathe."

But how does groove affect us? Well, our brains have evolved to react emotionally to it. Recognizing the pulse and expecting it to occur and reoccur is an integral component of musical emotion, and music communicates to us with particular effectiveness when it systematically violates our expectations.

The cerebellum — the part of the brain that monitors timing and the coordination of physical movement — appears to also be involved in tracking the beat of music and in our emotional response.

This has been demonstrated in studies where people were asked to listen to music they liked and disliked. During the experiment, the cerebellum lit up, as it also did when people were asked to tap along to a beat.

But why are emotion and movement connected and processed in the same region of the brain? It could be because, from an evolutionary standpoint, emotions served as a motivator for us to _act_ : The fear triggered by the appearance of a lion hopefully causes us to run away!

Therefore, the emotional system may be directly linked to the motor system, allowing us to react more rapidly.

> _"Effective music — groove — involves subtle violations of timing."_

### 8. Musical expertise comes from a combination of practice and genetic predispositions. 

A huge amount of us take music lessons as children, so why don't more of us become concert pianists or world-renowned rockstar? Let's explore what exactly makes people become musicians.

Research shows that musical expertise is the result of practice, and if you want to become an expert, you should be prepared to practice for at least 10,000 hours in total.

Studies looking at conservatory students have shown that the absolute best are simply those who have practiced the most. In one study, conservatory students were secretly split into two groups according to who the teachers deemed to be the most talented. Years later, those who obtained the highest performance ratings were those who had practiced the most — the "talent" group to which they had been assigned was irrelevant.

Such studies indicate that you don't merely need to practice to become an expert — you've got to hit the magic number of 10,000 practice hours in order to be in the company of the world's best. That means twenty hours of practice per week for over ten years.

It's not all blood, sweat and tears, though. Your genetic makeup also plays a part in musical expertise.

Just as being tall makes you a better basketball player, large hands are a blessing if you want to be able to reach keys far apart on the piano with as few hand movements as possible. So, like every other skill, some of us have a biological predisposition toward singing or a certain instrument.

But how much expertise can be put down to genes? The best guess that scientists have is that genes and the environment each account for around 50 percent of how proficient we become in a skill.

Genes can dictate a tendency toward being persistent or possessing great eye-hand coordination. But certain life events — from conscious experiences, to the food your mother consumed while you were in the womb — influence whether or not your potential becomes a reality.

### 9. Musical preference begins with the music we’re exposed to. We choose music we’re familiar with. 

What's your first musical memory? Perhaps you can think as far back as your toddler days. But it goes back further than that. In fact, without being aware of it, you probably have prenatal memories of music.

Research has shown that we prefer music we were exposed to while we were still in our mother's womb.

In one study, pregnant women were assigned a specific song to listen to on a regular basis. A year after the babies were born, the researchers played the babies both the assigned song and another song, to see if they showed any preference. Sure enough, they wanted to keep listening to the song they had heard in the womb.

This demonstrates that early exposure to music is so significant that it shapes our later musical preferences.

When we get older and can start choosing music for ourselves, familiarity seems to play a role in what we like.

In order for us to warm to a piece of music, it mustn't be too simple, nor too complex. This has to do with predictability. If it's too complex, we won't be able to predict it, which makes it feel alien, and if it's too simple, it becomes predictable and we discard it as trivial.

Familiarity also shapes our musical preferences because we like sounds that we associate with previous positive experiences with music. We're pleased by the comfort and sense of safety that familiar sensory experiences bring.

Interestingly, safety plays a role for many of us when selecting music, since, in a way, we surrender and make ourselves vulnerable to music when we listen to it.

Just think: Through speakers or headphones, either to bring comfort or sadness, we let these strangers into our homes, our minds, our hearts.

> _"We let them into our ears, directly, through earbuds and headphones, when we're not communicating with anybody else in the world."_

### 10. Final summary 

The key message in this book:

**Music triggers almost every region of the brain and is so deep-rooted in us that it may have helped our pre-human ancestors learn to speak. Some of the remarkable effects music has on the brain include the ability to uncover hidden memories, soothe us and move us to tears.**

Actionable advice:

**It still pays to be a dabbler!**

You don't have to be a classically trained pianist or jazz trumpet aficionado to benefit from music. Just tapping along to a song you love or humming your favorite melody has a positive impact on your brain as it will activate areas associated with learning and well-being.

**Suggested** **further** **reading:** ** _Musicophilia_** **by Oliver Sacks**

_Musicophilia_ explores the enriching, healing and disturbing effects of music. It delves into fascinating case studies about disorders that are expressed, provoked and alleviated by music.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Daniel Levitin

Daniel Levitin is a psychologist, neuroscientist, musician, record producer and best-selling author. As well as working as a professor of psychology and behavioral neuroscience at McGill University, in Montreal, Levitin has worked as a producer, sound designer and consultant for the likes of Chris Isaak, Steely Dan, and Stevie Wonder.

