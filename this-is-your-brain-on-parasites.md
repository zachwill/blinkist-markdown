---
id: 5971f942b238e1000570327c
slug: this-is-your-brain-on-parasites-en
published_date: 2017-07-25T00:00:00.000+00:00
author: Kathleen McAuliffe
title: This Is Your Brain on Parasites
subtitle: How Tiny Creatures Manipulate Our Behavior and Shape Society
main_color: D3654B
text_color: D3654B
---

# This Is Your Brain on Parasites

_How Tiny Creatures Manipulate Our Behavior and Shape Society_

**Kathleen McAuliffe**

_This Is Your Brain on Parasites_ (2016) is about the microscopic organisms that live inside us. They sometimes make us sick and, more surprisingly, they drive human evolution in a variety of ways. These blinks explain how parasites can guide personalities, emotions and even culture.

---
### 1. What’s in it for me? Step into the hidden world of parasites. 

Every year, as winter approaches, everyone seems to have it: the latest flu. These tiny viruses annually develop new strategies to get into our bodies, multiply and jump on to their next victim. They change and adapt — and they seem to know exactly what they do.

In fact, parasites, such as viruses, worms, microbes and bacteria, are much more advanced creatures than we would like them to be. They can alter human bodily functions and cognitive capacity, and, when you consider that we've been fighting them since the dawn of time, it could even be argued that they shape human culture!

These blinks offer some of the most astounding examples of how parasites have developed methods to pounce on other species, use them as their host and take advantage of them.

In these blinks, you'll learn

  * why shrimps are pink;

  * why zombies really exist; and

  * why your brain is in your gut.

### 2. Humans have been battling a microscopic enemy for millennia. 

In the history of humankind, conflict abounds; from opposing tribes to warring countries to contradictory religions, people have always found a way to fight. But there's another type of deadly conflict that has gone practically unnoticed for thousands of years — a hidden war of microscopic proportions.

It's the battle between humans and _parasites_ : viruses, worms, microbes and bacteria. In fact, this fight has been so formative that it has affected the evolution of both the human physique and human behavior.

For instance, the human body has formed complex defenses to ward off intruders. The skin prevents microbes from entering; the nose and ears are lined with tiny hairs that filter out parasites and our eyes produce tears to force out invaders.

But even when a microbe does make it into the body, it's immediately met by other defenses. There's stomach acid, which is so strong it could burn a hole through a shoe, and mucous, a slime that traps microbes in the nose to be expelled by the next sneeze.

That being said, parasites have a few things going for them. For starters, they greatly outnumber us. They can also reproduce at an incredible rate and are both intelligent and highly adaptable.

So while the majority of them might die, a few survivors can mutate, infiltrate our system and take full advantage of the habitat our body offers.

It's this incredible capacity for survival that has led to parasite-related deaths in the past. Just take the bubonic plague, a bacterial infection that decimated a third of Europe in the Middle Ages. Or consider Columbus's arrival in the Americas; his landing resulted in the eradication of 95 percent of the indigenous population by way of smallpox, measles and influenza. And there's always the Spanish flu, which claimed more lives in 1918 than World War I.

It all goes to show that, while they may be invisible to us, parasites are a powerful foe. They're also smarter than we might imagine, which is what we'll explore next.

### 3. Parasites alter the behavior of their hosts to their own advantage. 

So parasites are bad news, but they're not just agents of disease that attack the human body. They can actually alter human behavior to their own benefit.

In fact, parasites regularly influence the behavior of their animal hosts to complete their reproductive cycles. Just consider tapeworms, which enter brine shrimp, incite them to mate and thereby spread themselves to new hosts.

The presence of these worms changes the shells of the shrimp from transparent to pink. As a result, when shrimp gather in large quantities, red clouds form beneath the surface of the water. The flamingos that feed on brine shrimp jump at the opportunity for an easy-to-spot meal and the parasites are pleased, too. The flamingos become their final host, the place where they reproduce and release their eggs, which reenter the water through flamingo excrement, ready to infect new shrimp and begin the cycle again.

Pretty creepy, right?

Well, parasites do a similar dance with us. Just consider the Guinea worm. This parasite, which can be found in Central and Northern Africa, is transmitted to humans through drinking water containing fleas carrying the worm's larvae.

Our stomach acid can kill the fleas, but not the larvae. So these parasites go on to mate within our abdominal muscles until all the males die, leaving just one female that can grow to be a meter in length. The surviving female then develops tons of eggs inside her over the next year, and begins to move to the connective tissues in our feet or calves.

Then, once she is ready to give birth, the worm moves right beneath the surface of the skin, releasing an acid that burns and forms a blister. The natural reaction is to rinse the skin with water, which is when the worm bursts through the skin, releasing the eggs back into the water. Once in the water, the eggs enter the fleas, where they hatch into larvae, and the circle is completed.

In this way, the Guinea worm alters human behavior to its advantage. Luckily for us, this nasty disease has practically been eradicated today.

### 4. Parasites can manipulate other organisms’ brains. 

You might not be ready to hear it, but the zombies you see in Hollywood movies aren't just things of fiction. They actually exist in real life. In fact, living creatures can become zombies by way of parasitic mind manipulation.

A great example is the jewel wasp, which lives in the tropical regions of South Asia, Africa and the Pacific Islands. It gains control of its victims' brains by essentially performing neurosurgery. Here's how:

First, the wasp injects poison into a roach's brain, making it a zombie in the service of the wasp. The poison limits the effects of octopamine, a neurotransmitter that controls muscle contraction during sudden movements.

From there, the wasp merely needs to hold one of the roach's antennae to gain total control over the creature. Once this is done, the roach can only move when told to do so by the wasp.

The wasp then leads its zombie prey into a burrow and lays eggs upon it, which will feed on the roach for six days. While being eaten, the roach is utterly helpless. The wasp will even inject it with dopamine, which causes the roach to groom itself, remove any parasites from its shell and protect the eggs from infection.

It's pretty wild stuff and research even suggests that parasites can manipulate the brains of humans. Consider _Toxocara_, a type of roundworm. It enters the human body by way of contact with dog or cat feces and infections often occur in poor regions, with bad hygiene.

Once inside the host, the worm remains at the larval stage, enabling it to move around into different organs like the eyes, liver and brain. A 2012 US study, which tested the cognitive capacities of kids and teenagers between the ages of six and sixteen found that, when infected with the parasite, subjects performed significantly worse at math, on IQ tests and in reading comprehension.

But that's not to say _all_ parasites are bad. In fact, humans are born with parasites and some of them are a natural part of our organism.

### 5. Our guts are home to a panoply of bacteria, which help regulate our emotions. 

People tend to think of the brain as the central control mechanism of the human body. But we actually have another brain that lives in our stomach, which is home to an incredible number of microbes and bacteria.

These organisms are a part of the _enteric nervous system_, which has more neurons — cells that process and transmit information — than the human spine, which is why it's often referred to as the second brain.

These stomach-dwelling parasites are no small part of who we are. In fact, the genetic material of our parasites is 150 times larger than our own!

These organisms join us while we're still in the womb and we get another major dose of them when we're born. They're responsible for releasing chemicals like dopamine, GABA and serotonin, neurotransmitters that influence our emotions. This means that, when we're happy, depressed or energetic, it's in part due to the tiny inhabitants of our bodies, most of which live in our guts.

A massive nerve even connects this system directly to the brain and enables these bacteria to communicate with the brain itself.

And this isn't just hearsay. Experiments have in fact shown that the microorganisms in the human body affect our emotions. For instance, in 2013, UCLA did a test with three groups of women. The first was given yogurt that contained bacteria twice daily for a month. The second was given non-fermented milk twice daily for a month and the third group was given nothing.

The researchers then did MRI scans of the women while they were being shown pictures of faces displaying various emotions, ranging from anger to sadness to fear. In the end, the women who were given yogurt were much less disturbed by the depictions of negative emotions than the other groups. It therefore stands to reason that the bacteria had a positive impact on these women's brains.

Next up we'll consider the bigger picture. That is, what do parasites mean for society at large?

### 6. Humans have adjusted their culture and behavior to avoid parasites. 

In Darwin's theory of evolution, biological change is driven by natural selection, a process through which the best adapted species survive. But Darwin's explanation doesn't say anything about how _culture_ evolved.

This area is still up for debate and modern biologists have found that cultural behavior might be related to the presence of parasites. For example, in 2007, biologists at the University of New Mexico began a study exploring how the evolutionary environment shapes culture.

They found that collectivist cultures, those that put emphasis on the group over the individual, are more heavily clustered along the equator, a zone heavy with parasitic infections. The researchers also discovered that the prevalence of parasitic infections affects the level of spice people prefer in their food, with those living along the equator preferring spicier food, which kills bacteria and prevents infection.

So culture may be biologically connected to parasites. Psychologists have uncovered similar findings. Just consider the Canadian psychologists who worked with Mexican biologists and found that regions with a history of parasitic infection tend to produce more introverted, less outgoing cultures. One explanation for this data is that such cultures are less inclined to mingle with others, for fear of being infected by other people's parasites, and therefore stick to themselves.

Such cultural preferences lead to societies with strict sexual rules that favor stable commitments and ritualistic practices, such as hand washing before prayers, a Buddhist ritual, or bowing instead of shaking hands, as is the custom in Japan.

And it's all to avoid getting infected by parasites. We steer clear of people who look sick, even feeling repelled by them at times. The same goes for bad smells like those of feces or urine and colors that indicate rot. In other words, since we can't rapidly adapt to mutated parasites, we've simply evolved to behave in ways that help us avoid infection.

### 7. Final summary 

The key message in this book:

**There's an entire microscopic kingdom all around us and even within us. The parasitic organisms that compose it have played a central role in human evolution. Though some of these parasites are deadly, the majority are essential to our biological functionality.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _I Contain Multitudes_** **by Ed Yong**

_I Contain Multitudes_ (2016) peers into the microscopic world of microbes, and offers fascinating insight into the countless ways in which our lives are influenced by them. You'll find out how ancient microbes helped make the world livable for mankind and how they continue to help all of Earth's living creatures through remarkable and essential partnerships.
---

### Kathleen McAuliffe

Kathleen McAuliffe is a science journalist who garnered a great deal of attention for her piece _How Your Cat is Making You Crazy_, which was published in the _Atlantic._ In addition to this work, she has been published in the _New York Times_ magazine as well as the _Smithsonian_.

