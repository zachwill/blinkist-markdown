---
id: 5913721ab238e10007a75440
slug: this-is-your-brain-on-sports-en
published_date: 2017-05-12T00:00:00.000+00:00
author: L. Jon Wertheim & Sam Sommers
title: This Is Your Brain on Sports
subtitle: The Science of Underdogs, the Value of Rivalry, and What We Can Learn From the T-Shirt Cannon
main_color: 2578B8
text_color: 2578B8
---

# This Is Your Brain on Sports

_The Science of Underdogs, the Value of Rivalry, and What We Can Learn From the T-Shirt Cannon_

**L. Jon Wertheim & Sam Sommers**

_This Is Your Brain on Sports_ (2016) is a fascinating journey into the human brain and an examination of what exactly happens to it when we play sports. Far more than mere games people play, sports provide a great deal of insight into our psyche and what makes us tick, observations that ring true for players and fans alike.

---
### 1. What’s in it for me? See your mind mirrored in the world of sports. 

When we see a brilliant soccer player score a seemingly impossible goal from midfield, or angry supporters fighting in the streets after a game, it's hard to believe that both professional athletes and riotous fans have brains similar to the rest of us.

Whether it's the World Cup final or a neighborhood showdown at the local park, there's just something about sports that brings out the best — and the worst — in us. By looking at the world of sports, there's a lot we can learn about human behavior in general.

In these blinks, you'll learn

  * why we love underdogs;

  * why we should reward improvements as well as victories; and

  * how Michael Jordan made it through his famous Flu Game.

### 2. Sports can teach us about our love for underdogs and about group mentality. 

No matter which sport you play, there's generally a winner and a loser. But even when one team seems to lose all the time, they'll still have devoted fans following them in every game. In fact, these fans can be even more passionate than those of other teams, since people love rooting for an underdog.

There are good reasons for this phenomenon, as it plays into our sympathy for those who are considered weak and our desire for the unthinkable to happen.

In an experiment conducted at the University of South Florida, students were presented with some details about the Israel-Palestine conflict, including different maps of the region. The two nations were then pitted against each other in a hypothetical sporting event.

As it turned out, when students who received maps that showed Israel surrounded and outnumbered by larger Muslim-majority countries, most of them rooted for Israel.

Likewise, if students were given a different map where the main territory was Israel, with its settlements encroaching on Palestinian territory, most of these students would then cheer for Palestine.

An underdog scenario is appealing because it represents the possibility of an unlikely outcome, where a team or an individual might persevere in the face of a daunting challenge. This makes the game more personal, since we often think of our own situation in life as a similar battle.

For instance, when we try to get someone's attention and sympathy, we'll often portray ourselves as an underdog trying to overcome impossible odds.

This impulse also sets up an _us_ - _versus_ - _them_ mentality that can lead to both cheering and fighting.

When our team does win, we feel the desire to join other fans and celebrate in the streets like it was a festival or parade.

But this tendency has its dark side as well, and it can easily turn to aggression against the fans of the opposing team or even bystanders with no interest in sports at all. In the end, our desire to belong often goes hand-in-hand with our desire to exclude.

### 3. Leaders tend to be attractive, and we value things into which we put a lot of effort. 

Have you ever heard of the _halo Effect_?

No, it's not a new video game — it's a term used to describe how we feel about quarterbacks or other sports stars and team leaders.

Research has shown that when we consider someone skillful or successful in one particular discipline, this feeling crosses over into our perception of other aspects of their personality, such as how attractive we find them. This works the other way around as well, since we often consider attractive people to be good leaders.

According to Nick Rule, a psychologist at the University of Toronto, we take the tiniest aspects of someone's physical appearance and extrapolate on them to determine how good a leader the person is.

Coaches, for example, might pick up on the smallest physical features of a young football player, like say his broad shoulders, and this influences their view of how good a quarterback he will be and how much specialized training he should receive. These kind of physical leadership traits can be an important factor, because his teammates will need to look at him and intuitively believe in his ability to lead the team.

Another psychological effect that can be seen in sports, as well as our daily life, is _effort justification_.

We tend to believe that the more effort we invest in something, the more value it has. This can be observed in the _IKEA Effect_ : when you spend most of the day putting your new IKEA bed together, you'll value it more than a fully assembled piece of furniture that you only had to move in.

This effect is also true in sports: if a manager puts a lot of effort into improving a troublesome player, that person will usually be kept on the team longer in order to justify these efforts. This will happen even when the player hasn't actually made much of an improvement. Likewise, a player that hasn't required a great deal of effort from the team's management is more likely to be let go, even if they perform well.

### 4. The best players often make the worst coaches. 

Let's say your sales department needs a new manager. You could just look at the statistics and promote the best salesperson to the job. But due to a phenomenon known as the _curse of the expert_, this approach has some pitfalls.

A great example can be seen with what happened to Michael Jordan in the later years of his professional career. No one can deny that Jordan was one of the best basketball players of all time — but this didn't make him a great manager.

In 2001, he tried to be both player _and_ president of basketball operations for the Washington Wizards. The result was a disaster; he had no mentoring skills whatsoever and would blame the other players for not doing what he wanted them to.

Jordan had fallen victim to the curse of the expert, which is when someone can't understand why others don't see everything as clearly as he does. For an expert like Jordan, his skills were second nature to him, and he didn't know how to properly explain them or put himself in the shoes of a novice.

We can also see this in baseball. Many Hall of Famers have turned out to be poor managers, yet other players with less-than-stellar playing careers have gone on to become Hall of Fame managers!

When we take a closer look at what causes the curse of the expert, we can see that ego is a crucial factor.

Successful athletes need a tremendous amount of self-confidence, but this tends to get in the way of coaching, when you need to focus on the needs of others.

This extreme self-confidence can culminate in a _totalitarian ego._

The boxer Floyd Mayweather exhibits this kind of ego. Since he has won just about every fight in his career, he needs something besides the experience of losing to stay motivated. As a result, he nurtures an image of himself as someone who is still underestimated and disrespected, and he's motivated by the desire to prove his critics wrong. In this way, the boxing itself becomes all about him and no one else — the totalitarian ego is born.

> _"Jordan was less a mentor than a tormentor."_

### 5. Hot impulses can drive us to do bizarre things. 

If you're a soccer fan, you probably remember the day Zinedine Zidane made international headlines when he lost his cool and head-butted an Italian opponent in the 2006 World Cup Final.

Sport often involves making split-second decisions, which makes impulses crucial to athletes' success — but these same impulses can make them do some pretty dumb things as well.

Positron emission topography (PET) scans allow us to monitor the flow of blood to someone's brain, and they have revealed two distinct states of being: one cool and one hot.

The hot state occurs when we're excited. We become very emotional in this state because the _anterior cingulate cortex_ and _anterior temporal cortex_ are especially active, and both of these are responsible for processing emotions.

These changes also shut down the pathways that facilitate higher-level thoughts, which is why someone can end up doing something regrettable, like Zidane's headbutt or when Mike Tyson infamously bit off a chunk of Evander Holyfield's right ear. This hot state is also what can lead to us make bad decisions at, for example, the negotiation table.

However, hot impulses have been vitally important for human survival in general, particularly in terms of our ability to act quickly and not overthink things when danger approaches. In these situations, not wasting time with higher brain functions can be a matter of life or death.

In sports, impulses determine whether you win or lose, and you can't waste time thinking deeply about how to react to a tennis ball or baseball that is zipping toward you.

Another intense aspect of sports is rivalry, where competitors push each other to the limit. Players often have their best games in such rivalries.

By way of example, Serena Williams and Maria Sharapova were locked in a tennis rivalry for years, and even though Williams has won most of the matches between the two, both players helped each other perform better and pushed themselves to improve.

This is known as a healthy rivalry, and it can happen in the business world as well, when the competition between two companies pushes them to be the best they can be.

### 6. Never take action simply for the sake of doing something. 

Whenever a team loses, it's usually the coach who takes the blame, and a string of losses can often get a coach fired.

The reason for this phenomenon is _action bias_.

Faced with bad results, a team owner often feels compelled to take _some_ action to try to influence the situation. Coaches, just like managers and CEOs, generally have a higher turnover rate than players or employees, and the owner will probably reason that a change in leadership should lead to a change in performance.

But this logic isn't always sound, because it takes time for a new coach or a CEO to learn all there is to know about a team and to identify where the real problems lie. This is especially true if the new coach is hired from outside the organization.

Another, perhaps less risky way of improving performance is _praise_.

But it's important to go about praise the right way: it should always be based on effort, and not just on achievement. So you might bestow an award for a team's most valuable player, but also one for the player who improved the most. This ensures that she knows her efforts are noticed and appreciated.

Of course, sports is also about winning. Part of being a sports fan is the desire to bask in the reflected glory of a winning team. This allows people to feel more successful than they really are.

Another example of how people like to be linked to success can be seen in US colleges.

Ivy League Universities are considered an elite group, but within this group, there are relatively minor institutions like the University of Pennsylvania, but also major icons like Harvard University.

A University of Pennsylvania study found that their own students were more likely to mention the establishment's Ivy League status than their counterparts at Harvard. The reason is that Harvard students probably felt the Harvard name already linked them to success, whereas at the lesser-known establishment, the Ivy League's prestige had to be invoked.

### 7. Sports can teach us about our great potential to overcome adversity. 

Our love of sports is perhaps strongest when watching an epic come-from-behind victory or when a team or player redeems themselves after a crushing defeat.

A central reason for the universal appeal of such victories is how they can reflect our own struggles.

Often, we'll hear about how running a business is more like running a marathon than a sprint, because you need to think about long-term goals, pace yourself and save energy for the right battles.

When it comes to potential, we can again look to Michael Jordan and the famous Flu Game of 1997.

During the fifth game of the NBA Finals against the Utah Jazz, Jordan was severely ill — yet he still scored 38 points and hit the clutch 3-point shot that won the game. And much like a marathon runner after crossing the finish line, when the clock hit zero he collapsed, unable to move a muscle. This reflects the amazing potential of sport to squeeze every last drop of energy from our bodies.

Most of the time, in order to protect us, our brain prevents us from expending all our strength, but when the mind has a goal or finish line to work toward, we can push ourselves to extremes, especially when the finish line is clear in our minds.

That's why, in business too, you need clear, measurable and attainable goals.

And then there's the final lesson sport can teach us, about picking yourself up after a major setback.

The Green Bay Packers' quarterback, Brett Favre, is a great example of this. Just one day after his father's death, he delivered one of the best performances of his career.

This isn't because Favre is an emotionless robot, but because he could channel his grief into honoring his father by playing as well as he could. And we all have this same capability, to use sport as a way of coping with tragedy and loss.

This yet another reason we love sports: it shows us just how remarkable our brains and bodies are, with capabilities that far exceed what we usually give them credit for.

### 8. Final summary 

The key message in this book:

**The world of sports can serve as a window into many aspects of the human psyche. From our love of underdogs to the importance of picking yourself up and dusting yourself off after a defeat, the behavior of both professional athletes and raving fans give us important insights into our day-to-day lives.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sports Gene_** **by David Epstein**

_The Sports Gene_ takes a look at the physiological traits that are beneficial in various sports, and at their hereditary background. It also examines why people in certain parts of the world have evolved in their particular way, and how this is beneficial in the realm of certain sports.
---

### L. Jon Wertheim & Sam Sommers

L. Jon Wertheim is a senior writer and executive editor at _Sports Illustrated_, as well as a contributor to CNN, NPR and the Tennis Channel. He has written several best sellers on a variety of sports, including _Strokes of Genius_ and _Running the Table_.

Sam Sommers is a social psychologist at Tufts University, and his work has focused on the way racial diversity can influence how people think and act. He is also the author of _Situations Matter_.

