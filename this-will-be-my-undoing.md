---
id: 5b3adddab238e100071d7d90
slug: this-will-be-my-undoing-en
published_date: 2018-07-06T00:00:00.000+00:00
author: Morgan Jerkins
title: This Will Be My Undoing
subtitle: Living at the Intersection of Black, Female, and Feminist in (White) America
main_color: F6E631
text_color: 8F861D
---

# This Will Be My Undoing

_Living at the Intersection of Black, Female, and Feminist in (White) America_

**Morgan Jerkins**

_This Will Be My Undoing_ (2018) delves into the author's experiences as a black woman living in modern-day America. By examining race, culture and feminism, the book demonstrates why and how black women have been marginalized and offers suggestions on how this serious situation can be improved.

---
### 1. What’s in it for me? Learn how to foster a society that empowers black women. 

Most of us want to contribute to building a better society for all, but we're often not sure where to start. In an increasingly multifaceted world, it can be difficult to navigate the varied perspectives on what needs to be done in order to support marginalized communities.

One such community is that of black women in the United States — but in order to help them, you first need to understand the context and historical background that gave rise to their marginalization in the first place.

This is where author Morgan Jerkins steps in. Drawing upon her own experiences growing up and living in contemporary America as a black female, Jerkins identifies why black women face hardship, while highlighting the areas that must be addressed to improve their position in society.

Whether you're a white female, a man or even a black woman yourself, everyone has a role to play in creating a society that is fair and just for all — including for black women.

In these blinks, you'll learn

  * about the _crabs-in-a-barrel_ theory;

  * why we should acknowledge skin color; and

  * why you shouldn't touch a black woman's hair.

### 2. Morgan Jerkins is a black woman and a human. 

When Jerkins was a young girl, she wanted to cast aside her blackness and assimilate into white culture.

This is a common desire among young black girls wanting to make it in a white-dominated world. They understand that their blackness is a threat and that in order to be nonthreatening and succeed in the world, it should be toned down.

Therefore, during her teenage years, Jerkins didn't leave her hair in its natural state and wore jewelry and clothes that were status symbols of whiteness — such as items from Gap and Limited Too — and sought to avoid black classmates who shunned white culture.

During this time, Jerkins learned when it was advantageous to use her racial heritage and when to adapt to the rules of whiteness. She soon realized that, in most cases, white culture always won because many people don't see black culture as a viable option. Why is this so?

Remarkably, the answer is that some white people see the labels "human" and "black woman" as mutually exclusive.

Once, a white man asked Jerkins why she didn't just define herself as a human instead of as a black woman — a label that many white people stereotype negatively. Her reply was that she is both; however, the man said that she didn't assume the role of a black woman. Why?

Because Jerkins was well-spoken, went to Princeton and worked in publishing — all qualities that didn't fit with the man's perception of a black woman, the man didn't see her as a "typical" black woman. Moreover, Jerkins sometimes adjusts her mannerisms to fall more in line with white culture; she minimizes gestures, avoids sucking her teeth and doesn't talk as loudly.

Blackness is denied when black women don't conform to the stereotype of "sassy black woman." This stereotype reinforces the perception that black people aren't as educated or worthy as white people, and if a black woman doesn't fit this description, then she isn't black but rather white — and thus human. And so, by this logic, it is impossible to be both black _and_ human.

Unfortunately, acknowledging a black person as though she or he were white is still considered a compliment.

### 3. “Color-blindness” is a myth that doesn’t help people of color. 

Many white people think that not being able to distinguish between skin colors is a progressive stance. It's not.

Being blind to black skin is the same as being blind to black history and everything that being black represents. The daily experiences of people of color aren't the same as those of white people; a lot of liberal white people claim that they don't see skin colors because they think identifying someone as black is somehow equivalent to being racist.

However, racism only exists when a negative judgment is made _due_ _to_ the color of a person's skin.

The problem with ignoring a person's blackness is that the societal challenges they face, including _real_ systemic racism, are also ignored as a result. Furthermore, the rich cultural history of black people — including all the good things that contribute to their uniqueness — is also disregarded.

Those who claim to be color-blind hold a "universal" standard that is actually a white perspective. The "universal" standard is, in fact, the perspective of white North Americans and Europeans, which white people inaccurately present as inclusive of everyone.

It's easy for a white person to claim they don't see color when referring to a black person who has become successful in a white environment. That's because the black person possibly took on white characteristics in order to assimilate into the dominant culture.

So, when black people aren't regarded as black because of the claim that color doesn't exist, it actually means that they have successfully camouflaged themselves as white.

The bottom line is that white people don't have the right to determine whether someone identifies as black or not.

### 4. White people fetishize black women’s bodies. 

Historically, black women's bodies were put on display so that people could openly gawk at them. For example, in the early 1800s in South Africa, Hottentot Venus, a black woman with an extraordinarily big bottom, was turned into a freak show by Dutch colonists.

Though this kind of distasteful display doesn't happen as obviously now, the mentalities underpinning such acts are still present.

Using black women for entertainment purposes is made possible by dehumanizing them, a process that has been happening for centuries. Black women were once slaves, which meant that their bodies were the property of their white owners. Back in those times, they were raped by their owners, and in more modern times, they've become overly sexualized by the media.

The notion of black women as sexual creatures rather than people has been embedded in society for hundreds of years. Black girls are taught that they shouldn't embrace their sexuality because it will be used against them — but this teaching restricts their self-discovery.

Even if it's intended as a compliment, fetishizing a black woman robs her of her agency. For instance, asking a black woman if you can feel her hair treats her as though she were an animal or object that exists only for your enjoyment.

Whether the white person wants to touch the hair because it's beautiful is beside the point; it's stepping beyond the boundary of personal space, an act white people hardly have to defend against. Similar acts of touching or petting are also used with animals as a way to express or exert power.

There's a lot of culture in black hair, and trivializing it as something nice to touch disregards its cultural significance. Natural black hair or black hairstyles, such as twists and braids, are considered bold by white standards. Having your hair natural has become a political sign of nonconformity to mainstream, white culture. Thus, a white person attempting to turn that political statement into something trivial is an offensive and degrading act.

### 5. Mainstream feminism usually belittles, negates or doesn’t take into account the experiences of black women. 

The 2014 film _Girlhood_, is about a young, black French girl and was directed by a white woman, who said that the movie doesn't feature the experiences of a young black girl but instead the experiences of a young girl in general. This, however, is an inaccurate claim, and it serves as an example of color-blindness negating real experiences.

Having black women's stories told by white women can often be oppressing and belittling.

With _Girlhood_, generalizing a young black girl's experience to that of any young girl is inaccurate because it dismisses the very different experiences of black girls compared to white girls. Above all, it ignores the fact that black girls are often sexualized or stigmatized in a way white girls aren't.

For instance, in coming-of-age TV dramas, the white girls usually experiment with drugs in a manner that is portrayed as challenging the constraints of society. Black girls experimenting with drugs, on the other hand, may well become addicts.

Generally, there are fewer representations of a black girl's potential role in society, whether seen in the minority of black Barbie dolls or the lack of shows and movies in which the main character is a black female university student.

What women can do to change this lack of representation is to try to understand it, but at the same time come to terms with the fact that they will never be able to fully empathize. This doesn't mean that white women can't talk about black women; however, they must do so reflexively and realize that there's much of the black female experience that they will never truly grasp.

Ultimately, feminism cannot be successful if it doesn't accept the discrepancies between women of different ethnicities.

> "_Unlike with white girls, our inherent innocence is not assumed."_

### 6. Black womanhood is complex and has been appropriated by white women. 

Black womanhood is not homogenous. However, it is usually perceived that way by people threatened by its complexity and multitude of power.

Black women are limited by stereotypes forced upon them either by white people or their own community.

One stereotype is that they're expected to be able to handle a lot of suffering and not succumb to the pain or pressure, just like their ancestors did. Signs of vulnerability are prohibited because they are already at the bottom of the social hierarchy.

Black women perpetuate this stereotype further as a means of protecting themselves and their daughters in a world where they're already vulnerable. This kind of teaching can prevent black girls from growing up into proud black women who are able to freely express their emotions. Furthermore, it can deny their ability to reject a burden they don't deserve to carry.

While black womanhood has both negative and positive connotations attached to it, the positive ones only emerge once they have been appropriated by white culture.

The stereotype of black women as sexualized beings is limiting and diminishes these women's existence as intelligent humans who have control over their own bodies.

Meanwhile, dance moves that originate from black culture, such as twerking, are appropriated by white women. The irony is that white women performing black moves are celebrated, while black women celebrating their culture tend to be frowned upon.

A case in point is the aforementioned twerk, which Miley Cyrus took and profited from. On the other hand, Nicki Minaj is deemed a bad example for doing the exact same dance move.

Another example is black hairstyles. While Kylie Jenner received praise for her cornrows, many black girls and women can't have their hair this way at school or work because it's seen as distracting — or, in other words, threatening.

### 7. Successful black women need to help other black women. 

As we've established, the modern world isn't so accommodating to black women and their efforts toward success.

But just because it's difficult, doesn't mean women shouldn't try to help; rather than being a threat to a woman's success, elevating fellow black women can be strengthening.

Many black people believe in the _crabs-in-a-barrel_ theory, which likens members of the black community to crabs trying to escape their confines. Once they're out, they're advised not to look back and try to pull another crab up in case they get dragged back down.

It's a mentality of self-preservation that black people — especially black women — learn as a result of the lack of opportunities available for them. The mentality goes like this: if one black woman helps another black woman out of the barrel, it could result in her spot being taken, leaving her to fall back down.

But by working with and helping one another, the constricting barrel can be eradicated altogether.

The success of one black woman means success for all black women.

It's not surprising to find that there's a strong connection between members of the African-American community, even between those who have never met. Often, this bond originates from a shared sadness, but we must try and extend it to encapsulate joy as well.

When a black son is unlawfully killed by the police, the entire community weeps as if he was biologically their own. Underpinning this shared sadness is the possibility that next time it really could be a member of one's own family.

Similarly, a black girl making it in a world run by the white patriarchy should be a success for all black women and girls. Parents should be inspired by this one girl's success, as it lets them know that a bright future is also possible for their own daughters. Furthermore, a single success story can validate the hopes, dreams and struggles of the community at large.

Whether black or white, feminists need to continue fighting for the progress of black women.

> "_I first had to recognize that success is a white domain, and if I did not adhere to its rules, I would never go anywhere."_

### 8. Final summary 

The key message in these blinks:

**Black women in America are dehumanized, fetishized and even ignored by feminists. To overcome these struggles, they need to empower one another, and white feminists must be more inclusive and sensitive to the polarizing effects of cultural appropriation.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _We Should All Be Feminists_** **by Chimamanda Ngozi Adichie**

In _We Should All Be Feminists_ (2014), Chimamanda Ngozi Adichie expands on her much admired TEDx talk to address our deepest misconceptions about feminism. By masterfully interweaving personal anecdotes, philosophy and her talent for prose, she explains how men and women are far from being equal, how women are systematically discriminated against and what we can do about it.
---

### Morgan Jerkins

Morgan Jerkins is an author and associate editor at _Catapult_, a publishing house and literary association. She has also contributed to numerous publications, including the _New York Times_, the _New Yorker_ and _Rolling Stone._

