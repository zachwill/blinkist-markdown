---
id: 59363dbdb238e100051fdaec
slug: thoughts-without-a-thinker-en
published_date: 2017-06-06T00:00:00.000+00:00
author: Mark Epstein
title: Thoughts Without a Thinker
subtitle: Psychotherapy from a Buddhist Perspective
main_color: F07B3D
text_color: D66E36
---

# Thoughts Without a Thinker

_Psychotherapy from a Buddhist Perspective_

**Mark Epstein**

_Thoughts Without a Thinker_ (1995) describes the fundamental principles of the Buddhist tradition through a psychoanalytic lens. These blinks explain how meditation and mindfulness can soothe the mind, alleviate suffering and heal mental illness.

---
### 1. What’s in it for me? Find out what psychotherapists can learn from Buddhist teachings. 

"Self-fulfillment" seems to be the magical concept of our era. Find it, mainstream society says, and you'll be happy and successful. But how's that possible if we misunderstand the fundamental idea of the "self"?

Indeed, nowadays in the Western world, mental illness is endemic and psychoanalysts trace our mental suffering to a false conception of the self. But it wasn't Freud, the father of psychoanalysis, who first uncovered the meaning of the self. In fact, to find the true meaning of the self, we have to go back to ancient India — and to the teachings of the Buddha.

These blinks connect Buddhist teachings with the ideas of Freudian psychoanalysis. They reveal what modern psychotherapy can learn from Buddhist wisdom and how we can loosen our attachment to the self to eventually become "thoughts without a thinker," and finally grasp the meaning of "no self."

In these blinks, you'll learn

  * what it takes to reach enlightenment;

  * why Westerners are hungry ghosts; and

  * why you should focus on the taste of toothpaste in the morning.

### 2. Buddhism and psychoanalysis have a shared emphasis on common feelings. 

In Buddhism, there is a famous image known as the _Wheel of Life_ that represents the universe, or more precisely, existence itself. This wheel places desire, anger and delusion right at its center, represented by a green snake, a red rooster and a black hog, respectively, all of whom are biting each other's tails.

The three animals, and the feelings they represent, are at the center of this wheel because together, desire, anger and delusion all prevent us from understanding our true selves. In other words, they keep us bound to the world. This is why they're known as the "three poisons" and are considered the root of all suffering.

But these ideas aren't unique to Buddhism. In Sigmund Freud's psychoanalysis, _Eros_ and _Thanatos_ represent the same concepts as the snake and rooster in Buddhism. In fact, desire, symbolized by the snake, and anger, symbolized by the rooster, were among the first forces recognized by psychoanalysis.

Freud said that while Eros and Thanatos are innate to all humans, we repress them. He argued that it was this repression that formed the primary source of psychological suffering.

In Greek mythology, Eros is the god of love or, in Freud's interpretation, the "life drive" that pushes us to procreate. Because of this connection, it's sometimes seen as having sexual undertones. That makes it eerily like the Buddhist snake of desire, even though we know for certain that Freud wasn't interested in Buddhism.

For Buddhists, however, desire is what keeps us striving for pleasant experiences, like love, and rejecting unpleasant ones, like suffering.

Meanwhile, Thanatos is defined in Greek mythology as the personification of death and, in Freud's interpretation, it's the "death drive" that explains the human penchant for anger. Freud argued that death is involved in every aspect of our psyches, producing anger deep within us.

He even said that the anger of others conjures thoughts of death, and that simply being yelled at by another person will cause us to think of death, which is why many of us so carefully avoid such confrontations.

Next, you'll learn about the final animal, the black hog, and the delusion that it represents.

### 3. We delude ourselves into believing such a thing as “the self” exists. 

The third core of the Buddhist wheel of life is delusion, represented by the black hog.

In Buddhism, delusion is said to prevent us from perceiving ourselves accurately. In psychoanalysis, it is the underlying cause of a variety of behavioral disorders, such as _dissociative disorder_, in which the subject experiences a split in his personality.

In other words, a misunderstanding of the self is central to both Buddhism and psychoanalysis. But in Buddhism, the true nature of the self is a difficult concept to comprehend. In fact, the Buddhist definition of enlightenment is in part predicated on an ability to grasp the meaning of "no self."

For instance, the self is different from the _ego_, the part of us that makes decisions while mediating between our natural drives and the expectations of society. In distinguishing the ego from the self, Buddhists believe that the ego can be used in meditation to reveal the absence of self and attain enlightenment.

If this sounds confusing, you're definitely not alone. The Buddhist view is that, unless you have actually attained enlightenment, you're likely suffering from the black hog of delusion.

The misconception of the concept of the self is also central to the psychoanalytic perspective, which states that we're likely to comprehend a false self. This process occurs in childhood as the mind develops. When a child gradually comes to realize that his mother is separate from him, he needs something else to hold on to.

After all, as babies, our mothers care for us extremely attentively, and we feel a part of a unified whole. Then, as we slowly come to realize the truth and comprehend our limitations, we are unconsciously driven to yield to the temptation of satisfying our every desire. In the end, this can result in the conception of a false self that denies how terribly helpless and small we truly are.

### 4. Many common, yet painful psychological disorders are caused by an inflated or deflated sense of self. 

As children, we're often expected to behave as our parents desire, which often implies acting against our authentic nature. Children can't simply do as their instincts tell them, because the societal expectation is to fit in and make a good impression.

Because of this tension between one's true nature and social self, a person's sense of self can become entirely warped. For instance, we often have to be quiet when all we want to do is scream, and are at times forced to say what others expect, regardless of what we think.

In this way, we learn that being accepted means appearing to be someone we're not — someone who is better, smarter, stronger or more confident. In the process, we suppress our true thoughts and feelings.

The result can be deep-seated psychological trauma due to years of inflating and deflating our sense of self. In fact, narcissism is closely connected to an inflated sense of self. This disorder expresses itself through pathological self-centeredness and an overpowering need for admiration.

Narcissistic symptoms include grand delusions, arrogance and pride, all of which are examples of overexpressing the self. Just consider the fact that so many people are obsessed with ideas of individualism, "selfie" culture and competition, all of which are quintessential reflections of an inflated sense of self.

At the other end of the spectrum, depressed people often suffer from a _deflated_ sense of self. Those who experience depression often express nihilistic thoughts about how they don't matter and may as well be dead. In such situations, it's as if the person wishes they had no self at all.

Beyond that, depression can cause feelings of emptiness, alienation and guilt, all of which are predicated on a deflated sense of self when compared to others or the world at large.

> _"Emptiness is not a thing in itself, yet it is nonetheless the vehicle for maintaining a proper view of the road in front of us."_

### 5. Buddhism can offer relief from mental illness by freeing you from the self. 

We've seen that the cause of many mental illnesses is a disproportionate sense of self, whether it's inflated or deflated. But Buddhism might have a cure for both of these ailments.

Buddhist practice can enable you to transcend your attachment to the self. Just take the Buddha himself: when asked by a wanderer named Vacchagotta if the self existed, he merely said that to answer the question would only reinforce the deluded concept of the self, and remained silent thereafter.

Buddhist scholar Herbert Guenther explains that emptiness can help you free yourself from attachment, but clinging to emptiness as a goal is damaging. In fact, the Buddha believed that attachment to such an idea of a void was just as unhealthy as being attached to an inflated self.

But regardless of whether reality is a void or you truly exist, Buddhism offers a life path that's free of attachment to such ideas. In this way, it can help you break free from these deluded ideas of the self that foster psychological issues.

This is accomplished through meditation and compassion. The first technique, Buddhist meditation, is designed to quiet your mind and enable clear thinking. When in a meditative state, you can experience your thoughts and the sensations of your body without feeling attached to them. In other words, meditation offers you a bird's eye view of your experience.

By practicing meditation for long enough, you can come to understand how the self is truly empty by nature.

Another essential aspect of Buddhist practice is compassion. The Dalai Lama even believes it to be the central facet of Buddhism and the key to real happiness.

After all, when you behave with compassion, you instinctively put the needs of others before your own. That necessarily means keeping your ego from taking over and avoiding narcissistic behavior.

So, while psychoanalysis works to root out the true self, Buddhism is primarily concerned with unearthing the delusion that such a thing exists. Its end goal is the unmasking of this truth and the understanding that the true nature of the self is simply emptiness.

### 6. Westerners feel an unstoppable hunger, while Easterners struggle with humility. 

Have you ever heard of the _hungry ghosts_?

They're powerful metaphors originating in that Buddhist image of existence, the Wheel of Life. These ghostly creatures are depicted as having huge stomachs and mouths so small they are barely the size of a pinhead. Because of this, they constantly crave, never reaching satisfaction.

Symbolically, their hunger can be seen as a combination of anger, desire and attachment, a type of suffering much more common in the West than the East.

After all, those of us in the West are pushed by capitalism to constantly seek and accumulate possessions, and always push for more. We're never satisfied with what we have and, as such, are hungry ghosts.

But why, exactly?

Well, Westerners commonly suffer from loneliness, which produces an insatiable need to be attached to other people and things. Just take Western children, who tend to suffer from feelings of abandonment.

In fact, many psychiatric patients in the West, rather than suffering from the fallout of a specific traumatic event, simply express feelings of being abandoned or neglected during childhood. This makes perfect sense, since Western mothers often return to work within a year or less of giving birth, leaving behind their small and rather atomized families.

Then, upon reaching adulthood, we find that the world at large is just as atomized as our families were, which makes it difficult to forge powerful connections. In the end, this is a perfect recipe for low self-esteem.

Naturally, those living in Eastern cultures experience emotional issues as well, but many of them actually suffer from the exact opposite problem. Asian families are larger and children are bound by an expectation to respect their parents, as well as the norms of society. This fact is well expressed by an old Japanese saying: "If a nail sticks out, hammer it down."

As a result of this outlook, individuals often end up feeling smothered by society and respond by developing an inflated sense of self-regard. As such, many Easterners see themselves better than others in their society and could stand to work on their humility.

Next up you'll learn what you can do, as a Westerner, to address your pain and quiet your hungry ghost.

### 7. Buddhism can help you live with your emotions. 

The highly acclaimed experimental musician John Cage endeavored to make music with odd sounds, but had an aversion to those that were loud or irritating. So, he began practicing the Buddhist technique of bare attention, cultivating openness by focusing on sounds without judgment.

By practicing this way, he came to accept and even enjoy sounds that he had previously found annoying. He grew as a musician and expanded his creative repertoire.

Just like Cage listened to sounds, Buddhists notice their emotions and feelings without judging them, and psychoanalysis can weigh in on this as well.

According to Freud, humans are all happy as babies, since a parent or caregiver tends to our every need. When babies cry, their mothers are sympathetic — without judgment.

Then, as we grow up, stuffed animals become our "transitional object," taking over the role that our mothers played in our imagination. In this way, Freud believed that teddy bears and mothers help babies cope with their emotions.

But what he didn't realize was that Buddhists had already discovered a way to reconnect with this blissful state of bare attention to our needs: by observing their emotions nonjudgmentally.

This approach is powerful and, if you can learn to accept your emotions, you'll handle them easily. Just consider one of the author's patients, Sid. Sid had grown obsessed with a girl named Rachel and was calling her so often that it had virtually become harassment.

The author asked Sid to work with the Buddhist technique of bare attention, allowing himself to feel the pain and let his emotions wash over him like rain, without attempting to fix them or "scratch the itch," which, in Sid's case, meant calling Rachel.

One night, Sid was lying in bed and decided to feel his pain without dialing Rachel's number. He lay in bed all night with this sensation and, by doing so, took the first step toward recovery. By harnessing the meditative power of bare attention, Sid learned to live with his pain.

> _"Once the reactions to the pain . . . are separated out from the pure sensation, the sensation . . . will stop hurting."_

### 8. Practice mindfulness to feel grounded in the present and overcome suffering. 

Mindfulness has become something of a buzzword over the last several years. So what's all the fuss about?

Well, mindfulness is about training yourself to exist in the present moment by focusing your attention on your body and breath, rather than the thoughts that constantly threaten to pull you into the depths of your own head. Mindfulness is a great tool to stop excessive worrying about the future or to avoid replaying painful experiences in your mind.

To successfully practice mindfulness, the key is to feel grounded by remaining in your body. The Buddha found that humans feel alienated when our minds and bodies are in conflict, which is why it's so important to stay in the moment.

For example, when brushing your teeth in the morning, you could let your mind focus on the sensation of the brush and the flavor of the toothpaste, rather than racing ahead to all the things you have to do at work that day. By doing so, your mind and body will function as a single entity, which will make things feel more real and less alien.

You can also practice mindfulness through your breathing. In fact, most meditative practices, regardless of the tradition they come from, involve the breath in one way or another. A focus on breathing is an excellent way to stay present.

And presence of mind isn't the only benefit. In addition to bringing you into your body, focusing on your breath will enable you to experience time _as it happens_. This is essential, since mindfulness is all about remaining grounded in the present. Focusing on your breath will let you feel the gentle passage of time, without wishing it would move faster or slower.

In this way, mindfulness is the first step to moving beyond the emptiness and suffering of living with a false self in mind. Staying focused on the present will put you in touch with your senses and reveal what is real in the current moment.

### 9. Final summary 

The key message in this book:

**Both Buddhism and psychoanalysis can shed light on the ways human beings suffer. A major focus of both traditions is an attachment to the idea that a "self" exists, which, in many cases, is the root cause of mental illness. By practicing Buddhism, you can can find freedom from suffering and a sense of calm in your mind.**

Actionable advice:

**Allow yourself to feel pain.**

The next time you feel sad, don't try to push the feeling away or deny it. Instead, realize that such emotions are a natural part of being human. During such painful moments, you have access to your innermost truth and are able to see how you are clutching to the illusion of self, causing yourself to suffer in the process.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Wherever You Go, There You Are_** **by Jon Kabat-Zinn**

_Wherever You Go, There You Are_ (1994) explains how to fully enjoy the present moment without worrying about the past or the future. By providing step-by-step meditation practices, both formal and informal, that can easily be incorporated into everyday life, Kabat-Zinn steers us toward the peace and tranquility that we're yearning for.
---

### Mark Epstein

Mark Epstein is a Harvard-educated psychotherapist, Buddhist practitioner and author. Several of his titles have earned the endorsement of His Holiness the Dalai Lama.

