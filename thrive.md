---
id: 54479f7c3033610008790000
slug: thrive-en
published_date: 2014-10-24T00:00:00.000+00:00
author: Arianna Huffington
title: Thrive
subtitle: The Third Metric to Redefining Success and Creating a Life of Well-Being, Wisdom and Wonder
main_color: DFE2F1
text_color: 586BCC
---

# Thrive

_The Third Metric to Redefining Success and Creating a Life of Well-Being, Wisdom and Wonder_

**Arianna Huffington**

_Thrive_ argues that it's time for society to stop thinking of success only in terms of money and power, and redefine it altogether. If we want to truly thrive in our professional and personal lives, we have to create room for well-being, wisdom, wonder and giving as well.

---
### 1. What’s in it for me? Redefine success to improve your health, happiness and productivity. 

Imagine a typical Monday morning: the phone won't stop ringing, the unread messages in your inbox are piling up, you have a stain all over your shirt because you spilled coffee on yourself on the way to work. What's wrong with this picture?

Stress has become a permanent part of our daily lives, and it doesn't just interfere with our peace of mind — it's also driving us to the grave. As scientists continue to connect stress to more and more serious health conditions, it's worth asking, why are we doing this to ourselves?

In _Thrive_, the author takes us on a journey to finding success, but not in the traditional sense of the word. She explains why it's so important for us to redefine the term success and add a "third metric" to our traditional definition, which consists of only money and power.

The third metric comprises well-being, wisdom, wonder and giving. It improves our health, our happiness and even our productivity.

In addition, you'll learn:

  * why getting some sleep will make you a better swimmer;

  * how a nurse's intuition can save a newborn baby's life; and

  * how many times you actually look at your smartphone every single day.

### 2. To live a life of real fulfillment, think beyond the usual metrics of money and power. 

We all want good, happy lives for ourselves and our loved ones. But what does happiness actually look like? And what does it have to do with success?

Traditionally, success is defined in terms of _money and power_, but for most of us, being rich and powerful isn't enough to make us happy. So to live a life of real fulfillment, we need to _redefine_ success.

The author learned this lesson soon after she co-founded _The Huffington Post_ in 2005. At first, she viewed success solely through the prisms of money and power. Consequently, she put everything she had into her new company and worked like a maniac — often 18 hours per day.

She was exhausted, but she persisted. But then one day in 2007, the author fainted in her office and shattered her cheekbone. After meeting with several doctors and undergoing a battery of tests, the results were conclusive: Huffington's unforgiving work schedule had driven her body to a breaking point, leading to her collapse.

The incident was a wakeup call. Huffington realized that focusing solely on money and power had prompted self-destructive behavior. By thinking of success only in terms of those two metrics, she had sacrificed her personal health and happiness.

To outsiders, it looked as though Huffington had achieved success because she had money and power in spades. But to her, it was clear that she wasn't thriving. So she had to rethink her life and create her own definition of success.

To that end, she realized that _thriving_, living a life of real fulfillment, was the third crucial metric for achieving real success.

Thriving means creating space in your life to take care of your own _well-being_, tap into life's _wisdom_, feel _wonder_ and engage in _giving_.

So as you can see, money and power alone don't lead to real fulfillment. But what are we actually missing when we see life only in those terms? Keep reading to find out!

> _"Over time our society's notion of success has been reduced to money and power."_

### 3. Find a daily rhythm to support your well-being; it will help you thrive and also prevent stress-related health risks. 

_Well-being_ is a key aspect of living a truly successful, satisfying life. But what does it actually entail?

Well-being is about taking care of your health on a daily basis to prevent disease. At a minimum, it means getting enough sleep, disconnecting from your devices and connecting with your inner self through meditation.

Taking these steps will support your health by mitigating stress, a mainstay of the modern workplace.

Why is office-related stress so all-pervading? It may be because standard office culture dictates that we should work as much as possible, take few breaks, meet strict deadlines, and so on.

These rigid rules create a lot of pressure, which leads to stress. Technology makes this situation worse, as it becomes difficult to disconnect from its stressors, even at home. For example, an email from your boss can pop up on your phone anytime, even when you're enjoying a peaceful dinner with your family.

But stress is more than an inconvenience, it also has severe health consequences. High stress levels are responsible for conditions like heart disease, diabetes, addiction and eating disorders. And it's worth noting that the negative effects of stress disproportionately impact women.

As you can see, stress poses a real threat to your health and peace of mind. That's why taking care of your well-being is so important: it's a way of decreasing stress and reducing its harmful effects.

One way to take care of yourself is by _opting out_ of the unhealthy work standard. Make it a priority to get enough sleep and find time during the day to connect with your inner self.

And despite what you might think, supporting your well-being doesn't have to mean sacrificing success.

For example, one of the author's friends quit her stressful job soon after having a baby. But instead of being a typical stay-at-home mom, she launched a start-up, opened a preschool and started a synagogue. She's still a high achiever, but now she's living and working on her own terms.

### 4. Find wisdom by learning from your own experiences and listening to your intuition. 

_Wisdom_ might seem like an old-fashioned, mystical concept that has little to do with your daily life. But if you really want to thrive, you have to find a way to tap into wisdom from every possible source.

In today's information age, where can you go to find wisdom? Unfortunately, you can't Google it. In fact, the abundance of knowledge we have at our disposal makes it even harder to find true wisdom.

That's because _wisdom_ isn't connected to intelligence or knowledge. Rather, wisdom is a mindset — a way of looking at life in a more insightful way, without getting distracted and frustrated by day-to-day wins and losses.

The solution is to stop seeking wisdom outside of yourself; instead, _turn inward_. Take advantage of your experience and listen to your intuition.

The wisdom that's inside of you is intangible and mysterious, but it's very real and very powerful. Nurses, for example, often know when a newborn has sepsis (a life-threatening infection), even before conducting any tests. They can't explain how they know, but listening to their inner voice allows them to act quickly to treat the infection, thus saving the newborn's life.

Although wisdom inherently exists inside all of us, it's formed and strengthened by our experience with the external world. So when something unfortunate happens to you, you can choose to tap wisdom from that experience, instead of being crushed by it.

The author, for example, overcame her devastating divorce by channelling all of her energy into taking care of her children. This helped her overcome her negative emotions and even allowed her to develop a friendly relationship with her ex-husband.

Because after all, life is full of all kinds of experiences. Some are pleasant and others are painful. However, we can learn something from every kind of experience if we view life "as a classroom." Adopting that kind of mindset will help you draw strength from experiences that might otherwise demoralize you.

> _"Wisdom is about recognizing what we're really seeking: connection and love."_

### 5. Foster a sense of wonder about the world to make your life more joyful. 

When we're children, everything in the world is full of _wonder_ — flowers, puppies, even our grandmother's old hats. But as we grow older, these things come to seem more ordinary and life slowly loses its magic.

This is especially true in today's hyperconnected age. Although we're immersed in technology that allows us to connect with others, these same gadgets often prevent us from connecting with ourselves.

Consider the fact that we're attached to devices and screens at the expense of spending time enjoying nature or engaged with art — two of humanity's greatest sources for feeling existential wonder.

Another drawback to our always-connected way of life is that technology can prevent us from experiencing surprise and serendipity.

Take the case of social media platforms like Facebook, which personalize our experiences by using complicated algorithms to display content based on what we already like. This actually prevents us from encountering the unexpected.

But the element of serendipity is extremely important in our lives, because it reveals the deeper logic that orders our world. This fills us with a sense of wonder that helps give our lives purpose.

Consider the story of a man named Paul Grachan. One day, Paul spotted a dollar bill in his wallet which had the name Esther written on it. Since it happened to be his new girlfriend's name, he gave her the bill as a gag gift. Then years later, after they were married, she finally confided to him that she'd written her name on that very dollar bill when she was 19, and told herself that she'd marry the man who found it.

It was a crazy coincidence, but it led Paul and Esther to question the role fate and destiny had played in their lives. It filled them with a sense of wonder and made them feel like there was something magical and joyful at work in the world.

### 6. Give generously to others to bring joy and happiness to your life. 

As anyone who's ever given someone a birthday present knows, giving feels good. In fact, giving feels really, really good. When we give to others, it makes us feel better about ourselves, which in turn makes us happier. It's as good for the giver as it is for the receiver.

That's why most of us naturally give to others in times of crises. Giving allows us to feel connected to others, which makes us feel happier and safer in these times.

So for example, when superstorm Sandy hit the East Coast in 2012, people pulled together to rebuild what had been destroyed. It inspired those who hadn't been personally affected to lend their support to those who were, as a way of showing the world that they were all part of something bigger than themselves.

Although people are mostly inspired to give only when there's a catastrophe like a hurricane or a tsunami, there are countless other opportunities in our everyday lives to give to others, and to show kindness and compassion.

For example, distance swimmer Diana Nyad had a neighbor who lost his wife. The neighbor had to take care of the children and earn a living at the same time, all while coping with his grief. But then, one of the other neighbors decided to organize the community to help the man. Diana joined her neighbors: together they took on some of his chores, like cooking dinner and taking care of the kids, thereby easing his burden.

This is just one example, but the truth is, there's always someone in need — you just have to look around and be willing to help.

And if you're not yet convinced by all the positive benefits to redefining success, read on! Thriving doesn't just improve your personal life, it also pays dividends at work too.

> _"When we flex our giving muscles every day, the process begins to transform our own lives."_

### 7. Our rushed, overconnected lives stand in the way of our thriving. 

As you already know, taking care of your well-being, seeking wisdom, fostering wonderment and giving back to others will allow you to thrive. But what prevents most of us from following this path?

Well firstly, most of us are always on the run. We rush from place to place without stopping to reflect on what's happening around us. This happens easily when we only view success in terms of money and power.

And rushing also makes us feel stressed about time, a condition called _time famine_.

When we live in that time famine, we skirt our obligations to ourselves and don't prioritize our own well-being.

We forget that the only reason we ever wanted power and money was to _feel good_. So why don't we feel good yet?

Time famine also prevents us from accessing wisdom, because it makes us think that stopping to reflect on what's happening to us is a big waste of time. So instead, we rush through our lives without considering the big picture.

Carl Honore presents a good example of time famine: he almost bought a book explaining how to tell your child a bedtime story in just one minute. But then suddenly, he realized how crazy it was to cut corners on the time he spends with his kid. The realization prompted Honore to found the "slow movement," which advocates slowing down our lives.

Another factor that prevents us from thriving is feeling like we need to be available all the time for work, because it can distract us from what's important. Technology makes it easy to connect with people all over the world, all the time. It's an amazing gift, but it also makes concentration and self-reflection difficult.

Consider that the average person checks her smartphone 150 times a day. How can she really deeply concentrate on _anything_, much less tap into the wisdom and wonder of the universe, if she's constantly checking her phone?

### 8. Prioritize your well-being: it will actually support your professional success – not jeopardize it. 

If you're still hesitant to embrace these steps because you think you'll have to sacrifice success, don't worry — it doesn't work that way.

In fact, taking care of your well-being will actually improve your productivity. That's because sleep (a major factor in your well-being, as previously noted) increases your level of productivity multiple times over.

This was proven by a Stanford study, which found that athletes on the swim team performed better when they slept well the night before.

Like sleep, _meditation_ can also powerfully boost your well-being, thus improving your performance. In concrete terms, research shows that meditation vastly improves your ability to focus on a task.

Considering that fact, is it surprising that many incredibly successful people, like Oprah Winfrey and the author herself, practice meditation on a daily basis?

There's another important way the principles laid out in these blinks can help you be more successful. As we previously mentioned, giving back does a lot of good — but did you know that helping others would actually also have a positive impact on your performance at work too?

In fact, research shows that people who are more giving in a general sense are also better at their jobs: the most effective salespeople are more likely to help both their customers _and_ their colleagues. Similarly, engineers are more productive when they do favors for their co-workers.

To see how generosity can impact a business, consider _Starbucks_. Throughout the company's history, CEO Howard Schultz has insisted on providing healthcare coverage for all workers, even part-timers. Even when the company was experiencing financial trouble, Schultz refused to give in to investor demands to cut healthcare costs. This policy fostered loyalty among employees and boosted the company's reputation.

In the case of Starbucks, Schultz created a win-win situation that allowed his company to thrive, not just succeed. And you can do the same thing in your life.

> _**"** Our creativity, ingenuity, confidence, leadership, and decision making can all be enhanced simply by getting enough sleep."_

### 9. Final summary 

The key message in this book:

**If we want to thrive, we have to create room for well-being, wisdom, wonder and giving back. To do that, we have to redefine success and take control of our overconnected, busy lives. Doing this will improve our health, happiness — and even our productivity.**

Actionable advice:

**Find ten minutes in your daily schedule for a meditation practice.**

Sit down for ten minutes every day and try to quiet your thoughts, focusing just on your breathing. Adding this one simple habit to your daily life will help reduce your stress and improve your ability to focus on all manner of tasks.

**Suggested** **further** **reading:** ** _Mastery_** **by Robert Greene**

In _Mastery_, author Robert Greene argues and illustrates that everybody can achieve mastery of a skill or field if they just follow the established steps of historical and present-day masters. Based on interviews and studies of some of the best in their respective fields, Greene provides a diverse array of tips and strategies on how to become a master.
---

### Arianna Huffington

Arianna Huffington is the co-founder and editor-in-chief of _The Huffington Post_.

