---
id: 560929988a14e60009000071
slug: through-the-language-glass-en
published_date: 2015-09-30T00:00:00.000+00:00
author: Guy Deutscher
title: Through the Language Glass
subtitle: Why the World Looks Different in Other Languages
main_color: FEE7FB
text_color: CC54BC
---

# Through the Language Glass

_Why the World Looks Different in Other Languages_

**Guy Deutscher**

_Through the Language Glass_ (2010) explores the many ways in which language both reflects and influences our culture. By exploring the different ways that languages deal with space, gender and color, the book demonstrates just how fundamentally the language you speak alters your perception of the world.

---
### 1. What’s in it for me? Discover how language both reflects and shapes human culture in sometimes surprising ways. 

The question of how colors are expressed in language has long been, somewhat surprisingly, a hot potato in linguistics. When philologists in the nineteenth century first noticed that the words for colors in the ancient literatures of the world were completely bananas, a debate began that rages on today.

At the heart of this debate lies the question of what a particular language can tell us about its speakers. If a language has no word for "yellow," does that mean they can't see this color? If it has no word for "tomorrow," does that mean there isn't one? Or are these differences merely cultural?

These blinks take you on a journey through the languages of the world and show you that, far from just reflecting the culture of its speakers, language can have drastic, and sometimes curious, effects on how we think about and perceive the world.

In these blinks, you'll learn

  * why there is no word for "blue" in Ancient Greek;

  * why the word for "red" came before the word for "yellow" in all of the world's languages; and

  * why Spaniards and Germans think of bridges in different ways.

**This is a Blinkist staff pick**

_"These blinks take you through history and around the world in search of answers to the age-old question of how the languages we speak affect the way we think and act. Really fascinating stuff!"_

– Erik, Editorial Production Manager at Blinkist

### 2. Language reflects culture. 

If you've ever read the works of the Ancient Greek poet Homer, you might have noticed that he never employs a word that could be taken to mean "blue." Why? Because color is one area where language reflects culture. The Ancient Greeks, as you'll soon discover, had a very different culture of color than we do today.

Based on the words for color used in _The Iliad_ and _The Odyssey_, the English prime minister and scholar William Ewart Gladstone argued that the ancient Greeks' sense of color must have differed from ours.

In his _Studies on Homer and the Homeric Age_, published in 1858, Gladstone argued that the Greeks perceived the world in something closer to black-and-white than technicolor.

According to Gladstone, Homer wasn't merely exercising poetic license when he chose his words — words that seem strange by today's standards. Rather, like the rest of the Ancient Greeks, he had an undeveloped perception of color, largely confined to light and dark. This is why he described things like honey and freshly-picked twigs as _chlôros_ (green), a color neither black nor white, to give a sense of their paleness and freshness. 

Further adding to Gladstone's case was the fact that Homer made little or no reference to color when we might otherwise expect it, such as when speaking of spring flowers in a field. Moreover, he generally preferred elementary forms of color — black and white — over others. For example, _melas_ (black) can be found 170 times in his works, whereas _xanthos_ (yellow) appears only ten times.

This led Gladstone to claim that, at some point, mankind underwent an education of the eye — that is, we learned to perceive differences in color — that hadn't yet happened in Ancient Greece. But why?

In Ancient Greece, artificial colors, produced through paints and dyes, were still in their infancy. For instance, Ancient Greeks rarely saw blue (apart from the sea and sky), as blue eyes, blue dyes and truly blue flowers were rare. Perhaps this is why Homer never uses a word for "blue."

### 3. Lazarus Geiger got us asking if differences in language are related to arbitrary naming or to perception. 

In 1867, philologist Lazarus Geiger claimed that the evolution of humankind could be traced through language. This is because, like Homer's poems, other ancient texts, such as the Indian Vedas and the Bible, also treat color strangely.

Geiger's postulation is significant because it transformed Gladstone's discovery about a color deficiency in _one_ ancient culture into an explanation of the evolution of color sense in the _entire_ human race.

Interestingly, color words developed in the same order everywhere in the world. After an awareness of dark and bright (black and white) comes a sensitivity to red, then yellow, then green and finally blue and violet. 

Unfortunately, Geiger died during the early stages of his research, at the age of only 42. And subsequent researchers spent decades chasing a red herring — the erroneous yet widespread belief in the inheritance of acquired characteristics, as proposed by Jean-Baptiste Lamarck.

This belief served as the foundation for Hugo Magnus's thesis that the human retina, as Gladstone also thought, developed its sensitivity to color via acquired improvements in color perception inherited by the next generation.

The critics — described by the author as _culturalists_, in contrast to _nativists_ — thought, contra Gladstone and Magnus, that these changes in color perception were unrelated to _anatomical_ changes. 

Instead, they followed Geiger, reasoning that we can't infer which colors ancient humans perceived by examining language alone. For example, we call white wine "white" despite its being yellowish-green. Black cherries are really dark red and even red squirrels are brown.

Then there are anecdotes of ancient cultures preceding Homer, such as the Mycenaeans, who highly valued lapis lazuli, a distinctively blue stone. This suggests that they indeed could perceive the color blue; why else would they prize an otherwise normal rock?

### 4. Our color terms reflect our culture and not our visual capabilities. 

The triumph for culturalists came with expeditions. When W. H. R. Rivers, in 1898, went to Murray Island at the northern tip of the Great Barrier Reef, it was finally proved that a lack of color terms wasn't due to a lack of perception. 

Many of the Islanders' color names were vague. For example, the word _mamamamam_ (derived from _mam_, the Islanders' word for "blood") was used to describe red, pink and brown.

But the fact that the Islanders had a less distinct vocabulary of color than other cultures had nothing to do with their vision.

When subjected to a Holmgren wool test, whereby the subject chooses similarly colored wool pieces from an array of differently colored wool, they could distinguish between colors despite not having words in their language for particular colors.

To make this point clearer, consider this counterfactual (albeit plausible) scenario: 

Imagine Russians came to England for the first time, where they were baffled to discover that the English called both _siniy_ and _goluboy_ (the Russian words for, respectively, dark and light blue) "blue"! Although navy blue and light blue differ by wavelength just as much as sky blue and green, and although the English can indeed discern the difference, their cultural conventions regard these distinct colors as shades of the same color.

Why shouldn't this be the case for "blue" and "black" with respect to the Islanders investigated by Rivers? In all likelihood, they simply chose the closest color label in their palette to represent a given color. For them, "black" or "green" could represent blue, in the same way that the English say "blue" instead of _goluboy_.

### 5. The work of Brent Berlin and Paul Kay reinforced the idea that color concepts are determined by both culture and nature. 

As we saw in the last blink, the culturalists had some convincing arguments. 

Nevertheless, Lazarus Geiger's question as to why color words developed in the same order in many of the world's languages, and why there are so many similarities among languages' use of color words, was forgotten. It resurfaced a century later with the 1969 publication of Brent Berlin's and Paul Kay's _Basic Color Terms: Their Universality and Evolution_.

But both the culturalists and the nativists didn't quite hit the nail on the head. The answer to Geiger's question in fact is that color concepts are determined both by culture _and_ nature.

Let's examine the color red, for example. There are both natural and cultural reasons why red is the first color to be named in nearly all cultures. 

In nature, red signifies danger (think "blood") and sex (for example, a female baboon's red bottom signals that it's time to breed). It's therefore practical to have a word for these things — our survival depended on it! 

Now let's look at culture. If you want to make a dye, red dyes are the most common and least difficult to manufacture. Ease of production makes red dyes more common, and it's more likely that you'll have a word for something you see often than something you rarely see (like blue, for some cultures).

But, then, how do we account for linguistic difference among cultures? These differences only arise when nature is not simply categorized.

For instance, it's clear that a dog is different from a rose. All cultures where dogs and roses exist will have distinct words for dogs and roses.

Colors, however, aren't so clearly distinguished. One culture can thus treat yellow, light green and light blue, for instance, as shades of one color (one with no tint of red), while another might have different words for each of these colors.

Younger generations will inherit these cultural methods of color categorization, thus demonstrating that acquired characteristics can indeed be cultural.

> _"Culture enjoys freedom within constraints."_

### 6. Language reflects culture through the complexity of its grammatical structures. 

Color perception is not the only way that culture is reflected through language. Another way is through the complexity of a language's grammatical structures.

All languages are complex, but that doesn't mean that all languages are _equally_ complex. Unfortunately, comparing degrees of complexity is difficult. For instance, which language is more complex: one with more vowel sounds or one with more verb tenses? There's no real answer.

However, we _can_ compare the complexity of particular aspects of language by, for example, looking at how many tense distinctions are marked on a verb.

When we compare languages in this way, we find that a language's grammatical complexity sometimes reflects social structure.

A good example of this can be seen in the _morphology_ of words — that is, the structure of those individual words. 

As linguist Revere Perkins discovered in 1992, the languages of larger, more complex societies tend to have simpler word structures. This is because people in more complex societies often have to explain things to strangers, and are thus less able to "point" to information without additional words to establish context.

For example, saying something like "the two of them went back there" will be enough for someone who understands the context of what you're saying, but might be meaningless for a stranger. In less complex communities, this kind of pointing information — when words such as "there" point at people, places, events and times — is much more common, since people have a greater amount of shared context and information. Over time, pointing information is likely to fuse together and turn into word endings, which are essentially a form of morphological complexity.

Another factor is that simply encountering many different types of a language (regional dialects, social backgrounds, foreign accents) leads to the simplification of language. Just think of "globlish" (or global English) and how much simpler it is than Shakespearean English.

### 7. Languages can affect our thoughts by controlling what we are required to express. 

In the 1930s, the work of Edward Sapir and Benjamin Lee Whorf thrust forward the idea that our native language determines the way we think about and perceive the world.

À la Einstein, Sapir called this "linguistic relativity." Unfortunately, Whorf's and Sapir's theories were largely pseudoscientific; nonetheless, they had a profound impact on the way that scholars thought about the interaction between language and mind. For one, people largely stopped believing that language could influence our thoughts.

For example, Whorf concluded from Sapir's analyses of the Nootka language spoken on Vancouver Island that differences in grammar pointed to differences in perceptions.

Take falling stones, for example. Instead of saying "the stone falls," the Nootka say "it stones down." The Nootka have no verb for "fall," but they _do_ have a special verb to describe the movement of a stone: "to stone." 

While this might seem odd, consider the English verb "to rain." Whereas English-speakers have a special word for falling rain, there is no such word in Hebrew. Instead, they say "the rain falls."

Yet, the fact that language can express ideas differently doesn't necessarily mean that their speakers' experiences and perceptions are completely different. 

What it does mean, however, is that different languages _require_ us to express different ideas.

Linguist Roman Jakobson, summarizing the words of Franz Boas, put it quite succinctly: "Languages differ essentially in what they _must_ convey and not in what they _may_ convey."

So, the real difference in languages is not what they can express, but what each language requires speakers to express. And this _can_ have an effect on the way we think.

For example, some languages, such as French and German, require speakers to specify the gender of living nouns, like the neighbor you've invited over for dinner. English, on the other hand, does not. 

If you're speaking in French, then the listener will automatically think of the neighbor as male or female. Not so in English.

### 8. Gendered nouns show another way in which language affects thought. 

Let's start with a riddle: Why can Spanish speakers remember an apple named Patricia but not one named Patrick? The answer has everything to do with language.

Gendered nouns affect the thought processes of speakers of gendered languages. For example, the female gender of the German word for "bridge" ( _die Brücke_ ) affects how Germans think of bridges (like other speakers of Europe's gendered languages).

This was verified by the research of psychologist Toshi Konishi in the 1990s. He found that when Spanish and German speakers were asked to describe the properties of certain nouns, they tended to pick masculine properties for masculine nouns and feminine properties for feminine nouns.

The words for bridge in German and Spanish are feminine and masculine respectively ( _die Brücke_ in German and _el puente_ in Spanish). Interestingly, Germans tended to describe bridges as beautiful, fragile, peaceful and slender, while Spaniards would describe the same bridge as big, dangerous, long and sturdy. 

Despite the absence of a discernable logic behind a noun's gender, these associations affect how we think. Of course there's no real reason why a little girl is an "it" in German, or why water in Russian is a "she" and then a "he" when a tea bag is dunked in it.

But these associations — masculine, feminine or neuter — also affect a speaker's ability to recall information. This was demonstrated in an experiment conducted by Lera Boroditsky and Lauren Schmidt, which revealed that Spanish and German speakers could remember names of inanimate objects, like bridges and apples, so long as the names correspond to the genders.

Looking back to our riddle from before, the Spanish word for "apple,"_la manzana_, is feminine. Thus, it's easier for Spanish speakers to remember an apple "named" Patricia rather than Patrick. The same applies to the masculine _el puente_ : Spanish speakers will better recall a bridge "named" Claudio rather than Claudia.

### 9. The way languages describe spatial relations can affect the way we think. 

A striking example of how our native language affects the way we think can be found in the Guugu Yimithirr aboriginal language in Australia, which has a unique way of describing spatial relations.

Their language makes absolutely no use of egocentric coordinates (left and right), thus demonstrating that the egocentric system is not a universal feature of human language, as was once thought.

_Egocentric coordinates_ are based on our body and our vision. We don't need a map to know that we need to take the first _left_, _cross_ the street, take the road _behind_ the supermarket and continue until we get to the red house _in front_ of us.

But the Guugu Yimithirr language doesn't use this system. Instead, they use compass marks, like north and south. For them, frozen fish would be found in the north-east corner of a shop, for instance.

Interestingly, speakers of languages with different systems for spatial relations will consider the "same reality" differently. 

For example, if you're shown three pictures — one of a girl directly left of a house (in this case, north of the house), another of a tree much further left of a house (but this time the picture has been turned 180 degrees, so the tree is south of the house), and another just of a girl — and asked to draw the tree in the third picture, most of us would put the tree to the left of the girl.

Guugu Yimithirr speakers, however, consider the tree to be south of the girl since it is south of the house, and therefore draw the tree on her right. Their language leads them to a totally different assessment of reality.

> _"Speech habits...can create habits of mind that have far-reaching consequences beyond speaking."_

### 10. Speakers of different languages perceive colors differently. 

In the first four blinks we saw how language can reflect a culture's perception of color. However, language actually plays a much larger role in color perception; specifically, it can _influence_ how speakers of different languages perceive colors themselves.

In 1984, Paul Kay and Willett Kempton asked English speakers to choose the odd one out among three colored chips across the blue-green spectrum. 

Compared to speakers of the Mexican language Tarahumara, which treats green and blue as shades of the same color, English speakers tended to exaggerate the distance between colors on the green-blue spectrum.

If two of the chips were green and the third one was greenish blue, the English speakers always picked the greenish blue chip, even if the distance on the color spectrum was actually greater between the two shades of green.

When asked to explain their choices, the English-speakers insisted that the chips they chose _looked_ different. Why? Because their language affected the way they visually processed the colors.

In 2006, Kay conducted another experiment. Knowing that the left hemisphere of the brain manages language and processes information from the right visual field, they wanted to check if language affects visual processing. As they learned, it does!

The participants (all English speakers) looked at an X in the middle of a computer screen encircled by squares of the same color (green-blue). However, one of the squares was a different color. If this square was to the left of the X, participants had to press the left button; if it was to the right, the right button. 

When the shades were similar, participants quickly recognized the odd one out if it appeared on the right-hand side of the screen (part of the visual field monitored by the left hemisphere), thus suggesting that language can influence visual perception.

### 11. Final summary 

The key message in this book:

**So does language affect culture? Or is it the other way around? As it turns out, the truth is somewhere in the middle. By creating associations with words, speakers of different languages ultimately have different perceptions of the same reality.**

**Suggested** **further** **reading:** ** _The Language Instinct_** **by Steven Pinker**

_The Language Instinct_ provides an in-depth look into the origins and intricacies of language, offering both a crash course in linguistics and linguistic anthropology along the way. By examining our knack for language, the book makes the case that the propensity for language learning is actually hardwired into our brains.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Guy Deutscher

Guy Deutscher is a linguist and Honorary Research Fellow at the University of Manchester. In addition to his numerous academic contributions, Deutscher is also the author of _The Unfolding of Language_.

