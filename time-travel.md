---
id: 5922026ab238e100077ef9cf
slug: time-travel-en
published_date: 2017-05-23T00:00:00.000+00:00
author: James Gleick
title: Time Travel
subtitle: A History
main_color: A99153
text_color: 8F6F1B
---

# Time Travel

_A History_

**James Gleick**

_Time Travel_ (2016) details the history of a captivating concept. These blinks explain how the idea of time travel came into the popular consciousness, what problems the theory presents and how you might already be time traveling without even knowing it.

---
### 1. What’s in it for me? Journey through the history of time travel. 

We are obsessed with the passage of time. Whether it's a fascination with the past through retro fashions or historical fiction, or excitement about the future and whether or not we'll be colonizing other planets, we think about the past and future more than we do about today.

And of course, our preoccupation with the past and future has led to our desire for time travel. How fantastic would it be to take a trip back to ancient Rome or fast forward to an age of flying cars and interstellar travel?

But is time travel possible and where did the concept come from? These blinks show you how the dream of journeying back and forth through time captured our imaginations.

In these blinks, you'll discover

  * why cyberspace allows for time travel;

  * why going back in time is theoretically possible; and

  * why you time travel when you open a book.

### 2. The idea of time travel is only about a century old. 

Nowadays, time travel is just about everywhere you look, from films to TV shows and even literature. And that's no accident: society has become obsessed with the idea of traveling through time.

However, that wasn't true just 150 years ago. The portal was opened by H. G. Wells' novel, _The Time Machine_, which was published in 1895.

In _The Time Machine,_ Wells describes a man, called simply the Time Traveler, who builds a machine that enables him to move throughout the course of history and into the future. The story is important because it approaches time as something flowing, that, with the appropriate vessel, could be sailed.

Before this book came out, time was basically viewed as a one-way street. You could only go forward, and only at one speed. In this way, Wells' fictional account transformed the way people viewed time and time travel.

But why was it so successful?

Largely because it came out at the end of the nineteenth century when tremendous change was in the air. As the twentieth century dawned, people were becoming incredibly excited for the future. It was a period of huge technological as well as scientific growth and, for the first time ever, people were living in a way that was dramatically different from the generation before them.

In other words, it was the first time that the past, future and present would be entirely different. This simple historical moment is what made time travel so riveting.

It's important to remember that before this, the future was considered to be similar to the present. Stories regarding future events would often be based on prophecy, as in _Oedipus Rex_ or _Macbeth_. Such tales of prophecy center on a personal journey that will happen to a particular character in the future. But they fail to envision a major change in how people generally live. The twentieth century, and H. G. Wells, changed all of that.

> It took some years for the term "time travel" to settle into language; it occurred in English only in 1914.

### 3. As the idea of time travel progressed, rules for it were agreed upon. 

So, when time travel emerged as a concept, it was all the rage. But as the topic underwent further discussion, ever greater theoretical problems came to light.

For instance, can you travel forward in time to meet your future self? What if you went back in time and altered the course of events that led to your life?

Because of these quandaries, rules had to be established for time travel. These strictures were rapidly worked out by writers and thinkers, with the majority of them published in pulp magazines.

Just take the question of how to travel through time from 1895 to today without altering major events like the two World Wars. It was decided that time travel could occur so rapidly that you wouldn't touch anything on your way. You'd simply glide through the decades unnoticed.

Other theoretical problems focused on how the "self" would be changed as a result of time travel. If you met yourself in the future, would you be you? Or would you be an entirely different person?

To handle this issue, the science fiction community decided that each self you encountered over the course of your time travel would be a different entity. So your self from 2010 would always be "2010 you" and your self from 2020 would always be distinct from him.

Another even more abstract question in this debate formed around the question of free will. If you travel to the future and meet yourself, does that mean your life will necessarily lead to the two of you meeting? In other words, is your fate predetermined?

Other questions arose, too. Time travel allows you to go back and correct past mistakes, like that chemistry test you bombed in high school. Does this make learning and effort irrelevant?

### 4. Scientists like Albert Einstein changed the way we think about time. 

By now you know how fiction helped build the idea of time travel, but some real scientific thinkers also shaped the concept. Albert Einstein was one who transformed the way other scientists approached time itself.

His theory of _special relativity_ proposed that the speed of light is a constant, no matter how quickly or in which direction you travel. This had incredible effects on the idea of time. Now, rather than moving at a constant, universal rate, time could be considered relative.

In other words, the faster you travel, the slower time moves. By this logic, a person in a rocket moving at close to the speed of light would experience time more slowly than someone standing on the ground.

Following Einstein's breakthrough, science came up with a variety of other theories that impact the way we consider the passage of time. Just take the idea of _multiverses_. This concept proposes that there are an infinite number of parallel universes, each different from the next due to a series of small decisions taken. In other words, there's a universe in which you decided to study for your test and one in which you didn't.

Since such scientific developments didn't occur in a bubble; many of them influenced the philosophy of time as well. One such philosophical debate focuses on memory.

The human memory is imperfect, and each time you remember something, you're actually altering the story. In doing so, aren't you, in fact, altering the past and the future?

That's to say, if you forget that you went out partying instead of studying, you're actually transforming the way you consider your life and your future self. Isn't that similar to the idea of multiverses?

But naturally, most people struggle just with the idea of time as relative. For ordinary people, outside of science fiction, time really does move at the same speed and certainly in the same direction. Such a concrete experience makes it difficult to think of time as fluid.

### 5. Time travel is full of paradoxes. 

If you traveled back through time and murdered your own grandfather, would you disappear? Then again, if you never existed, how could you have killed your grandfather in the first place?

Therein lies the most common paradox of time travel. But it certainly isn't the only one.

Most of these paradoxes result from traveling backward in time. The _butterfly effect_ suggests that small events can have major repercussions down the line. If a butterfly flaps its wings on one side of the planet, could the small gust it creates transform into a raging storm thousands of miles away?

This concept is essential in considering time travel, as any time traveler would face countless moments in which she could dramatically alter future events, even through a nearly imperceptible action. If you went back in time and killed Hitler in his infancy, how different would the world be today? There likely would have been no World War II, no United Nations, no Cold War — and who knows what else would have changed?

Any changes made by a time traveler could lead to dramatic results in the future course of history. Questions like these are so complex that they have even perplexed the world's top mathematicians and physicists.

Just take the Austrian logician Kurt Gödel. He thought time travel was mathematically possible and postulated that universes with _closed timelike curves_ could exist. Such curves can be thought of as time loops that move continuously back into the past. By riding such a loop, you could theoretically travel through time.

Science also opened up a whole new set of questions by allowing for changes in causation. In theoretical science, there's no law to prevent causality from reversing directions. So somebody in the future can theoretically alter the past.

But despite such theories, other academics, Stephen Hawking and Einstein among them, didn't believe time travel could be a reality. According to Hawking, if it were possible, we would already be surrounded by time traveling tourists.

So, you've got a good sense of the history by now, but what does time travel mean today?

### 6. We time travel every day thanks to literature, memory and the internet. 

Now you know how time travel has thrown esteemed thinkers and academics throughout history for a loop. But if you stop and consider what exactly time travel could be, you'll quickly see that humankind has actually been doing it since the beginning of history.

That's because time travel is possible through literature and memory. Each and every time you read something, you travel through time by reaching into the past and seeing what someone thought of the world back then.

After all, when a writer puts words down on a page, she's thinking about someone who will read them in the future, whether near or far. Of course, there's also memory, which means that you can travel through time by remembering past events and altering them however you wish — every single day.

Beyond that, due to the development of the internet and its prominence, the very idea of time has transformed, and time travel along with it. In cyberspace, you can halt time, go back into the past and plan for the future.

Go back through your Facebook feed and alter the past by untagging friends, deleting photos and editing posts. You can even immortalize yourself in cyberspace as your social media accounts will likely exist long after you die.

The most amazing thing is that all of this happens at lightning speed as communication on the web is practically instantaneous.

And those aren't the only ways time travel is different today. Since the first age of time travel in the late nineteenth century, the times people want to visit have changed.

People in the past primarily wished to travel to the future, but nowadays we also want to return to the past.

Much contemporary culture is focused on retro styles. Even futuristic movies and TV shows like _Back to the Future_ and _Westworld_ are centered on narratives of returning to the good ol' days; not plunging forward into the unknown.

### 7. Final summary 

The key message in this book:

**Time travel is a popular, mind-boggling concept that has captivated humans for over a century. Today time travel is a commonly accepted idea, but one bound by complex considerations, internal laws and theoretical impossibilities.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Genius_** **by James Gleick**

_Genius_ (2011) charts the life and career of brilliant physicist Richard Feynman, from his formative upbringing to his remarkable and lasting contributions to science. Though he's not as renowned as Albert Einstein, and has no groundbreaking theories to his name, Feynman did change the way scientists look at the world.
---

### James Gleick

James Gleick has written a range of books on the history of science and scientific thinking that cover chaos theory and the lives of prominent figures.

