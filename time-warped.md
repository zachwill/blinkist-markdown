---
id: 5385ef626162380007ec0000
slug: time-warped-en
published_date: 2014-05-27T07:00:00.000+00:00
author: Claudia Hammond
title: Time Warped
subtitle: Unlocking the Mysteries of Time Perception
main_color: B82625
text_color: B82625
---

# Time Warped

_Unlocking the Mysteries of Time Perception_

**Claudia Hammond**

_Time_ _Warped_ (2012) is about that enduring mystery: our perception of time. Using research from the fields of neuroscience, biology and psychology, Claudia Hammond investigates the many reasons why, on one day, time appears to pass rapidly, while on another, it seems to grind to a halt. In addition, _Time_ _Warped_ suggests ways in which we can control our individual experience of time.

---
### 1. What’s in it for me? Discover how to gain control over your perception of time. 

The mystery of how we perceive time is certainly an enduring one. Why is it that time often feels like it's dragging on endlessly, while at other times it appears to zip by? 

Drawing on a wealth of examples and an abundance of contemporary research in neuroscience, biology and psychology, the award-winning author and broadcaster Claudia Hammond confronts the mystery of what she calls "time warps."

Along the way, Hammond explains why time feels as though it speeds up as we get older, why it slows down when we pay close attention to something, and why different people can have vastly different perceptions of the passage of time.

Additionally, _Time_ _Warped_ is a trove of advice about using time more effectively, and — most importantly — using time warps to your own advantage, speeding up or slowing down your life at will. 

In these blinks, you'll discover:

  * Why we tend to believe that important events happened more recently than they actually did.

  * Why we have more vivid memories of events occurring when we were aged 15–25 than at any other period.

  * Why a man who decided to live alone underground for two months believed he still had almost a month left to go when his friends collected him.

Whether you simply want the time you spend waiting in line at the post office to zip by, or you're looking to create a rich, long life to look back on, these blinks will show you the way.

### 2. Although time runs at a constant speed, our perception of it is affected by different factors. 

Have you ever wondered why time seems to drag on endlessly one day, while the next day it appears to fly by?

What's behind this strange phenomenon?

In short, our _perception_ : While the flow of time itself doesn't vary, our perception of it does.

So what factors influence this perception?

Firstly, our emotions. For example, when we're afraid, we process all sense data — what we see, hear, smell, taste and feel — more thoroughly. When we process more information in a given period of time than usual, time seems to slow down.

This phenomenon was tested by neuroscientist David Eagleman, who intentionally terrified his (willing) participants by throwing them off a skyscraper roof (don't worry, they were wearing safety harnesses). The participants' fear caused their brains to process information in far greater detail, and consequently, time seemed to decelerate.

The second factor in "time warping" is memory: the more memories we make, the more slowly time seems to pass. Furthermore, if we're experiencing a lot of stress, our senses are heightened, so we tend to make more memories.

For example, imagine that you despise public speaking yet you're obligated to give a five-minute presentation. When you're finally up on that stage, you'll probably feel highly stressed. That stress will cause you to "record" the event in great detail, making a lot of memories — e.g., of the faces of the people in the front row.

The result? Those five minutes will seem like an eternity.

Finally, even something as mundane as body temperature can affect our time perception.

Take psychologist Hudson Hoagland's wife, who was bedridden with flu. Hoagland nursed his wife attentively, and didn't leave her alone for longer than a few minutes at any one time. Nevertheless, she complained that he was gone for much longer periods.

Curious about her apparently warped sense of time, Hoagland performed an experiment which demonstrated that the higher his wife's fever, the more quickly a minute seemed to pass for her.

> _"Time dictates the pattern of our lives — when we work, when we eat, even when we choose to celebrate."_

### 3. Time is shared between people and based on social rules. 

If you've ever taken a vacation in a warm, southern country, you might've observed that its inhabitants have a completely different idea of punctuality. While most people in the northern hemisphere set an acceptable limit for lateness of, say, five minutes, elsewhere it is not uncommon for people to turn up a whole _two_ _hours_ late.

This is because every society has its own rules and conventions concerning time, which its members share and understand.

For example, if you've booked a train which departs at 8 p.m., it's considered normal to arrive at the station ten minutes early. However, if you're invited to a dinner party starting at 8 p.m., you'll be expected to arrive a little later. Such rules provide us with a way to agree on what's acceptable, and thus give us a sense of security.

That's why you may run into difficulties when visiting a country where the established rules are different. You might, for example, turn up 30 minutes late for a dinner party — customary in your own country — and yet still have to wait a few hours for the other guests to arrive!

However, the social aspects of time don't just influence the way we manage it. They can also affect our perception of time passing.

For instance, an unpleasant social situation can make time appear to drag. In one study set in a laboratory, participants were instructed to choose their lab partners by writing the name of their preferred partner anonymously on a piece of paper.

Several participants were then told that no one had chosen them, and that they'd have to work alone. Other participants were informed that everybody had chosen them, but since this was impossible to arrange, they would have to work alone also.

Afterward, all participants were asked to estimate the time that had passed. Perhaps unsurprisingly, the rejected participants perceived time to have passed much more slowly.

### 4. Our perception of the past is affected by the magnitude of events and the age at which we experienced them. 

In which year did Michael Jackson die? It must've been _ages_ ago, right?

You might be surprised to learn that it was as recent as 2009.

When we reflect on the past, we often find it difficult to judge time accurately. Significant events, such as the premature death of a superstar, stand out in our memories, leading us to believe they occurred more recently than they did.

This effect — known as _telescoping_ — is just one example of the difficulty we have in correctly recalling the passage of time.

On the other side of things, we also tend to forget the relatively insignificant events from our past because we usually don't contemplate them.

This can be seen in an experiment conducted by researcher Marigold Linton. Every night, for ten years, she described two of the day's events on an index card, and dated the back.

Once a month, Linton would choose two cards at random, and estimate their dates. She found this a very difficult task, especially with mundane events like "coffee with friends."

After each test she'd return the used cards to the pile, so she could test her memory of some events more than once.

The events she became most proficient at dating accurately were the ones she selected from the pile repeatedly — which shows that we remember the events that we contemplate the most.

Another fault of our memories is that we tend to have made most of them between the ages of 15 and 25 — the "reminiscence bump." According to psychologists, the memories we have of this period of our lives are vivid and easily recalled.

Why?

The key is _novelty_ : during this period, we experience many things for the first time — relationships, jobs, independent travel, living alone, and so on — which renders the events of this time much easier to remember.

### 5. We each have an inner clock that controls our bodily functions but runs for longer than 24 hours. 

Have you ever wondered why you get hungry at certain points of each day? Or why we tend to get tired around the same time each evening?

This is the work of an inner biological clock that times all of our bodily functions.

While this clock is internal, it nevertheless depends on external cues, like daylight, which synchronize the body's timing with that of the rest of society. Indeed, without such external cues, the cycle of our inner clock would stray from that of the normal 24-hour day.

This has been illustrated by an experiment conducted in the 1960s by Michel Siffre, who decided to test his inner clock by secluding himself in an underground cave for two months.

Because his mealtimes and sleep weren't governed by external stimuli, he slept and ate whenever it felt right. The result was that each "day" would seem endless to Siffre. In reality, however, time was passing much faster than he thought. In fact, he'd be conscious for an entire day, but would believe that he'd been awake for just a few hours.

To Siffre, time lost all meaning — even to the extent that the music he'd brought with him provided no pleasure whatsoever. To his ears, Beethoven sounded like pure noise.

When the two-month experiment was over and Siffre's colleagues came to fetch him, he was under the impression that he still had 25 more days to go!

Experiments like this have led scientists to conclude that our inner clock's cycle of sleep and active periods actually lasts 24 hours and 31 minutes.

In everyday life this cycle is usually corrected by daylight. But when we're deprived of this external stimulus — as Siffre was — we will revert to this internal cycle.

### 6. Your perception of time depends on your mind and body working together. 

As you've learned, your brain influences your bodily functions and your perception of time. But how exactly does the brain do this?

Firstly, the brain's _frontal_ _lobe_ is responsible for our perception of the passage of time, as well as the creation and storage of memories and the development of ideas.

Without a fully functioning frontal lobe, we wouldn't have the ability to judge time accurately — even in the course of just one day. Consider, for example, the case of a 49-year-old man who complained to neurologist Dr. Giacomo Koch that something was clearly wrong with his perception of time. As the patient explained, he would wake up, drive to his job, do what seemed like a full day's work, prepare to leave for the day — and then discover that it wasn't even lunchtime!

Aside from this strange complaint, the man had no other problems and passed many neuropsychological examinations.

It was only when a brain scan was performed that an abnormality was discovered in his frontal lobe — one that diminished his ability to judge the passage of time correctly.

Secondly, our perception of time is regulated by a neurotransmitter in the brain called _dopamine_. This chemical sends information around the brain.

If dopamine levels are increased — which occurs if we take certain recreational drugs, like methamphetamines — our inner clock speeds up. As a result, we perceive time to be passing much more quickly than it is, so the hours seem to fly by.

Finally, our perception of time is dependent on clues from the body, which it gains via a sense called _proprioception_ — the awareness of the position and movement of one's body. One example of this is the ability to feel one's heart beating at any given moment.

Without this sense, we'd find it difficult to perceive time accurately. Indeed, experiments have shown that people who lack this sense perceive time as going more slowly.

### 7. The more attention we pay toward something, the slower time will feel. 

Imagine that you're sitting in a lecture, bored out of your mind. As hard as you try, you find it difficult to keep your eyes off the clock as the seconds tick away.

If you glance up at this clock at just the right moment, the second hand seems to rest longer than you expect — so much longer, in fact, that you wonder whether the clock has stopped. Of course, an instant later, the hand moves.

Why does this happen?

Amelia Hunt explains this phenomenon in terms of our attention: the more intently we focus on something, the more slowly time seems to run.

This is why when we're bored, we feel as though time is dragging. By focusing solely on the passage of time, we feel that it's slowing down, even to the point of _chronostasis_ — the sense that time has stopped.

A similar effect can be observed when we witness an unexpected event. This is the _oddball_ _effect_ : because unanticipated happenings draw our attention, they seem to last longer.

For example, in one experiment, a sequence of images is presented on a screen: apple, apple, apple, apple, apple, dog, apple, apple.

Most participants are convinced that the dog is on screen for a longer duration than the apples, yet the duration of each image is identical. It's only because the participants don't expect to see a dog that time seems to slow down for them.

Whether this particular time warp is prompted by observing an unexpected event or focusing intently, neuroscientist David Eagleman believes that it's fundamentally the result of energy expended by the brain.

For example, in the case of the oddball effect, when all of your attention is suddenly diverted to something new, the neurons fired by your brain consume energy. For Eagleman, the more energy we use, the greater an event's perceived duration.

That's why new experiences seem to last longer: processing new stimuli requires more energy.

### 8. We all represent the passage of time to ourselves in one way or another, and some people do it visually. 

When you think about last week, do you imagine time laid out in front of you, from left to right?

Many people — approximately 20 percent, in fact, tend to "see" time in this way.

This visual perception of the passage of time is an example of synesthesia, which researchers have demonstrated in the following experiment.

A sequence of words is presented on a computer screen, some in a red font, others in blue.

The participants must press the "M" key with their right hand if the font is blue, and the "Y" key with their left hand if it's red.

Participants are told that the meaning of the words is irrelevant, and instructed to respond only to their color.

However, when the names of the months appear, participants who visualize time running from left to right respond in an interesting way. Whenever a later month appears, they react much faster if the font is blue (i.e., they use their right hand), and when an earlier month appears, they respond much more quickly if the font is red.

Why? Because such people associate earlier times with the left and later times with the right.

While we don't all "see" time in this way, everyone represents time in their minds in one way or another.

So, how do _you_ represent time? To find out, consider this problem.

"Next Wednesday's meeting has had to be moved forward by two days. On what day is the meeting happening now?"

To reach their conclusion, some readers will instinctively answer "Friday" because they visualize themselves moving forward two days from Wednesday.

But other readers will answer that the meeting is rescheduled for Monday. If that was your instinctive answer, you don't visualize _yourself_ as moving, but instead see time moving towards you by two days, from Wednesday to Monday.

### 9. Our ability to consider the future is an advantage that distinguishes us from other animals. 

Think ahead to next winter, and imagine yourself sitting on the couch, wrapped snugly in a blanket and reading a gripping novel.

Can you feel the soft blanket around you? Perhaps you can even smell the freshly baked Christmas cookies.

This ability to mentally "time travel" — to think about the future — is a crucial skill.

Being able to imagine and "feel" the future can help you prepare for upcoming events — for example, a job interview. By imagining how you'll feel when you're actually in that interview, faced with the task of having to impress, you give yourself the opportunity to consider in advance the questions you'll be asked, and therefore to mentally rehearse your answers so that you're fully prepared to give your best performance when the time comes.

However, some people simply don't have the ability to imagine the future: for instance, those who have suffered brain injuries, or those with certain mental health problems, lack this crucial skill.

Even if people with certain brain injuries are given vivid details about a future scene — sights, smells, sounds — they're unable to imagine it as if they were there. This is also true of people with Alzheimer's disease and schizophrenia. In fact, the more severe the illness or injury, the more difficult it is for the sufferer to envision the future.

Furthermore, future thinking is a uniquely human ability. If you're a dog owner, you might like to believe that occasionally, when your loyal companion is resting at your feet, he's actually thinking of your most enjoyable walks together. However, as far as we know, dogs cannot recall past events in detail. Therefore, it's unlikely that they can engage in future thinking.

Although animals are able to learn and are capable of intelligent behavior, there's no evidence that they can imagine the future as humans do.

### 10. Memories are made of flexible fragments which we can use to create new ones. 

Have you ever been telling a story that you're convinced is from your own life, only to have a friend interrupt to complain that you're recounting _their_ experience, not yours?

Why does this happen?

Memory is _reconstructive_ : our memories are composed of many fragments that we can instantly combine to create new memories — ones that might never have occurred.

This flexibility is what makes it easy for us to imagine the future.

For example, try to imagine riding a school bus to a tropical beach where your best friend is going to marry Johnny Depp.

You're able to imagine this scene instantly. But if your memories were fixed, not flexible, the process would take much longer. As with a videotape, you'd have to rewind and scan your memories until you found one of you on a school bus. Then, you'd have to dig through your memories for movies you've seen starring Johnny Depp, and try to recall a tropical beach wedding that you've seen on TV.

These memories could be decades apart, so collecting them and stitching them together would take ages. Yet because memory is fragmented and flexible, images of possible futures are easily created.

However, while the flexibility of our memories makes future thinking a lot easier, it also causes us to misremember the past — as researcher Elisabeth Loftus demonstrated.

In an experiment, Loftus aimed to get participants to recall events from their past which had never actually occurred. To do this, she interviewed their relatives to gather background information about real events from the participants' pasts.

Using these fragments, Loftus would discuss the past with a participant, ultimately convincing them that they'd met Bugs Bunny at Disneyland as a child.

While this might not sound like such an implausible memory, it actually is: Bugs Bunny is a character belonging to Warner Bros., not Disney, and thus he'd never appear at Disneyland.

Now that you've learned how we mentally represent time to ourselves, the following blinks will show you to how to change your perception of time at will.

### 11. You can make your life seem longer by making every day special, or you can even make time speed up. 

You've probably noticed how time flies when you're doing something enjoyable — like watching your favorite TV show — and how time drags endlessly when you're bored.

But have you noticed that longer periods of time, such as the passage of a year, are also subject to the time-warp effect?

Whether you want to feel you're living a rich, long life, or you want to speed up at certain times, you can use this knowledge to your advantage.

For instance, you can slow down your experience of time by taking advantage of the _holiday_ _paradox_ : while a vacation seems to pass quickly when you're in the middle of it, the moment you come home you'll feel that you've been away far longer.

Why?

The main reason is that vacations usually involve very few routines. Thus, if you want the years to feel longer in retrospect, you should try to avoid routine and add variety wherever possible.

For example, you could take a different route to work, or organize lunch dates with a different friend for each day of the week.

Of course, you probably won't want _every_ event in your life to feel spun out. Luckily, there are also many ways to speed up your perception of time.

For example, if you're stuck in line at the post office, don't focus on the waiting time as this will only make time drag. Instead, you could distract yourself from the wait by chatting with people in the line, or perhaps by writing some thoughts on your phone.

However, the best way to make time fly by would be to practice mindfulness, allowing yourself to become fully aware of your surroundings.

### 12. If you feel that there aren’t enough hours in the day, you're probably not using them efficiently. 

Imagine a very busy day: you're rushing to meet several deadlines, you have a lunch date you can't cancel, and your phone just won't stop ringing.

On days like this we tend to feel that if there were just a few more hours in the day we'd complete all our tasks with time to spare **.**

However, the problem isn't a lack of hours — it's the way that we use the hours we have.

For example, in a study of over 1,500 people, psychologist William Friedman found that those who felt they were constantly racing to get everything done also believed that time went by very quickly.

To combat this problem, and effectively slow down time, the first step is to get an accurate view of how much time you have. For instance, in one study, participants believed that they had, on average, just 20 hours of free time per week. However, by looking at their diaries, researchers found that the participants actually had 40 free hours each week. Only once you're aware of how much time you have can you take steps to use it more efficiently.

One way to ensure you'll accomplish more in a single day is to use strict deadlines. For example, if you have to write a report for work, and must complete, say, 30 steps before the work will be ready to deliver, that project's end will seem very distant. The result is that you won't feel a pressing need to focus on it.

But if your boss requests that the work is delivered by the beginning of next week, the project's end will seem much closer, generating an urgency that makes it easier for you to focus on the task at hand. In short, deadlines alter our time perception in a way that increases our focus dramatically, so if you set strict deadlines for each step you'll find that your time is used far more effectively.

### 13. Remembering the dates of past events isn’t easy, but certain tricks can help you. 

Often when we look back on our past, the events seem to blur together and their order becomes difficult to pin down.

What year did the recent London bombings take place? When did Princess Diana die?

Recalling exact dates can be difficult. However, by using the following system, you'll be able to estimate the date of any given event far more accurately.

To date a personal event in your life, you should first make a rough guess as to the number of years, months or weeks ago that the event occurred.

Next, try to guess the exact date.

For the final — and most accurate — estimation, either subtract or add a few months or years. For instance, if you've guessed that the event happened over two months ago, you should add a little more time.

For example, say you need to remember the date that you collected your dog from the shelter. If six months ago is your first guess, you should estimate that it was closer to seven.

However, if you believe that you collected your dog less than two months ago, you should subtract some time from your estimate, as the event probably occurred more recently than you think — for example, make your final guess five weeks rather than six.

When it comes to remembering the dates of public events, try using _time_ _tags_ — personal memories of the time in question. You make your most accurate guesses when you can connect the public events to those of your personal life.

For example, to figure out when Princess Diana died, ask yourself what you were doing when you heard about her death. What was your job at the time? Where were you living?

The more time tags you can recall, the more likely you'll be to remember the date correctly (the answer, by the way, is 31 August 1997).

### 14. Final summary 

The key message in this book:

**Our** **perception** **of** **time** **is** **affected** **by** **both** **internal** **and** **external** **factors,** **such** **as** **our** **emotions,** **our** **memories** **and** **our** **bodies.** **For** **this** **reason,** **time** **perception** **actually** **differs** **from** **person** **to** **person.** **By** **learning** **how** **we** **perceive** **time,** **we** **can** **learn** **to** **influence** **our** **personal** **experience** **of** **it** **–** **for** **instance,** **we** **can** **slow** **it** **down** **or** **speed** **it** **up** **whenever** **necessary.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Claudia Hammond

Claudia Hammond is an author, lecturer and broadcaster for the BBC World Service and BBC Radio 4. She has won many awards for her work in representing science in the mainstream media. Hammond's previous book was _Emotional_ _Rollercoaster:_ _A_ _Journey_ _Through_ _The_ _Science_ _Of_ _Feelings_, published in 2005.

