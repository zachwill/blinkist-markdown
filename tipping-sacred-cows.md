---
id: 54eaeafd393366000a130000
slug: tipping-sacred-cows-en
published_date: 2015-02-23T00:00:00.000+00:00
author: Jake Breeden
title: Tipping Sacred Cows
subtitle: Kick the Bad Work Habits that Masquerade as Virtues
main_color: B82625
text_color: B82625
---

# Tipping Sacred Cows

_Kick the Bad Work Habits that Masquerade as Virtues_

**Jake Breeden**

This book will challenge your assumptions about what virtues really matter in the workplace. It explains how the seven "sacred cows" — balance, collaboration, creativity, excellence, fairness, passion and preparation — may actually be _hindering_ your organization's performance. It outlines some alternative strategies that will make you and your employees happier and more productive.

---
### 1. What’s in it for me? Discover common flaws in our thinking and how we can overcome them. 

In a famous old prank, especially in some of America's more rural communities, a drunken adolescent and his friends push over some cows, and all manner of hilarity ensues.

These blinks won't teach how to tip a cow, but they will show you how we must overturn our "sacred cows," virtues such as fairness, passion and perfectionism that we strive for everyday. The truth is, although these virtues may benefit us in many ways, they have to be used in moderation.

For example, being fair to everyone can lead us to reward the weakest, less deserving members of our teams, to the annoyance of the best ones; perfection and fear of failure can keep us from innovating.

These blinks will show you better ways to operate in business.

In these blinks you'll find out

  * how far away Ritz-Carlton staff are told to stand from customers;

  * why being passionate can be bad for your health; and

  * why perfectionism is bad for your health.

### 2. We all want fairness, but often equate it to sameness. 

The desire to live in a fair world is hardwired into our brains.

When we see someone experiencing an emotion, our brains mimic it. In fact, one study of this phenomenon showed that if you see someone getting an electric shock, your brain reacts as if _you've_ been shocked.

This natural tendency toward fairness isn't always a good thing, though.

In our quest for fairness, we sometimes confuse it with _sameness_. We form empathetic connections with others, but sometimes project our own thoughts onto them. Have you ever caught yourself buying someone a gift that was actually something _you_ wanted? When we want something, we often assume others want it too.

In business, this conflation of fairness and sameness often leads people to make the mistake of treating every customer the same. We imagine our customers must all want the same things _we_ want. This can make us accidentally shut out any customers who are different.

So what's the better alternative? Well, you need to constantly remind yourself that everyone is different; strive to treat everyone as an individual.

The _Four Seasons_ hotel implements this doctrine well. It encourages employees to make their guests feel at home, and give them any service they desire. By contrast, the _Ritz Carlton_ have specific rules; e.g., they have to stand at least ten feet away from all guests and greet them with, "How may I help you, [sir or ma'am]?"

Of the two hotels, the Four Seasons has a higher customer satisfaction rate, _and_ a growing market share. It may be because people prefer to be treated as individuals.

### 3. Balancing your work, passions and personal life equally isn't necessarily healthy. 

"Balance" and "passion" are both buzz words in the self-help industry these days. Many people strive to be passionate at work, while balancing the different aspects of their life. This isn't really a healthy attitude, however.

Consider the idea of "work/life balance." People often think this means they should limit their daily working hours to spend more time at home. In many cases however, that only amounts to the person working at home rather than spending time with their family.

Instead, be bolder with your balancing. Don't "balance" things by constantly making trade offs and compromises. Work hard at one important thing at the time.

If you need to work, work. Don't compromise. When you need to spend time with your family, give them your full attention. In the long run, things will still balance out, and you'll be much happier and more productive.

Obsessive passions can also lead to burnout and poor health. In fact, one study of professional dancers found a correlation between passion and chronic injuries. When the "passionate" dancers got injured, they just kept dancing and made their conditions worse, rather than taking the time they needed to recover.

So don't become obsessed with your work. Practice _harmonious passion_ instead. This means showing passion for the things that make you happy, even if they aren't your main focus in life.

Serena Williams, for example, is one of the most successful tennis players in history, but she also has a great passion for fashion and nails. She's actually a qualified nail technician. She still devotes time to nails, even though it isn't the focus of her career. That balance makes her happier and healthier.

### 4. Your results should be excellent – not your process. 

Many people assume that excellence is the key to success. If you do each little thing perfectly — by keeping your desk tidy and delivering everything on time — your output will be perfect too.

Actually, that isn't true. Aiming for excellence throughout the entire work process _won't_ produce excellent results.

If a leader pushes his team to be excellent at all times, the team will get worn out. The team members won't strive to be creative or think differently because they'll be afraid of imperfection. Demanding excellence at all times will ultimately prevent the team from making any serious progress.

The key here is to understand that your _end product_ should be excellent. The path to it, however, almost certainly won't be.

Consider the success of the golfer Bubba Watson. He's won several majors, but his golf swing is far from perfect. He learned to golf from his father, not a professional coach. His imperfect training helped him develop a perfect technique.

To ensure that your staff are freed from the pressures of excellency, _lower the stakes_. Don't overstate the importance of any single role. If a person feels like her role is absolutely vital to the team, she'll be afraid of doing anything wrong, so she won't be creative.

An experiment on a group of women in the American military illustrated how much the pressure of perfection can affect a team's performance. Participants in the study were tested on their ability to shoot. One group was told that the purpose of the experiment was to see why women were worse shooters than men.

These women felt pressured not to let their gender down, and their results were worse than the control group. Pressure doesn't help people — it diminishes them. When you free your employees from pressure, they'll inevitably perform better.

### 5. Great innovation is about quality, not quantity. 

Our world is changing rapidly. People often assume that the more innovations they produce, the more successful they'll be. That's a common fallacy, though.

When you produce new things just for the sake of it, you're innovating for the wrong reasons.

_Narcissistic creativity_ is a case in point. Narcissistic creativity means wasting resources creating something that just shows off the company's skill, rather than filling an actual market need.

_Sony_, for instance, has unleashed thousands of new products — like TVs, video games and music — in an attempt to outdo its rival, _Apple_. Apple's collection of products is tiny in comparison, yet the _iPhone_ alone generates more revenue than Sony's entire annual sales.

So instead of innovating for no reason, innovate sensibly. Here's how:

First, try _rechanneling_ your creative energy. When you find a problem, you may be tempted to attack it with your full force of creativity. If you find a flaw in one of your products, you might want to design a new one. But take a step back — sometimes a small problem can be fixed with a simple tool. Don't try to reinvent the wheel.

If you don't fully unleash your creative energy on each little thing, you'll save it for more important times. So wait until your innovation can produce something people really need. That's how you transform the market.

Another way to be creative in a constructive way is to _repurpose_ ideas. Use tools that already exist instead of struggling to come up with something completely groundbreaking.

This is how _Pinterest_ has become so successful. Pinterest simply repurposed the idea of the pin board, and put it online so people could collect interesting images and share them. This simple concept has allowed the site to experience huge success.

### 6. Final summary 

Key message:

**The "sacred cows" aren't sacred at all, so don't let them hold you back. Re-examine your definitions of "fairness" and "balance"; let your process be imperfect, and only innovate when the time is right. When you stop following the sacred rules of business, your organization will prosper more.**

Actionable advice:

**Work and innovate when the time is right.**

When you need to devote yourself to your work, do it. Don't cut your work short, but also don't work when it's time for the other important things in your life, like family. Similarly, when you need to innovate, innovate. Don't innovate just for the sake of it — you'll only waste your creative energy. Save it for when it has the potential to yield something great.

**Suggested further reading:** ** _Linchpin_** **by Seth Godin**

_Linchpin_ explains why you should stop being a mindless drone at work and instead become a linchpin — someone who pours their energy into work and is indispensable to the company. It is not only better for your career but it also makes work far more enjoyable and rewarding.
---

### Jake Breeden

Jake Breeden is a writer and consultant who specializes in marketing, communications and leadership. He's advised leaders from some of the world's leading companies, such as _Starbucks_, _Microsoft_, _Google_ and _IBM_.

© Jake Breeden: Tipping Sacred Cows copyright 2013, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

