---
id: 5e64f54c6cee070006a1dbc7
slug: to-be-a-machine-en
published_date: 2020-03-10T00:00:00.000+00:00
author: Mark O’Connell
title: To Be A Machine
subtitle: Adventures Among Cyborgs, Utopians, Hackers, and the Futurists Solving the Modest Problem of Death
main_color: None
text_color: None
---

# To Be A Machine

_Adventures Among Cyborgs, Utopians, Hackers, and the Futurists Solving the Modest Problem of Death_

**Mark O’Connell**

_To Be a Machine_ (2017) charts the strange, emerging world of transhumanism, taking an honest look at the men and women working on undreamed-of new technologies. In this book, Mark O'Connell describes the people who are attempting to evade death, create hyper-intelligent machines, and even hack their own bodies.

---
### 1. What’s in it for me? Discover how emerging technologies promise to revolutionize the human experience. 

There are some aspects of life you can't escape. Death, toil, illness, and aging are in store for all of us. Or at least, that's been the human story up until now — but suddenly, something has changed.

Harnessing the latest developments in technology, transhumanists, a group interested in using scientific advances to improve the abilities of humankind, look at the unwelcome aspects of life and see challenges rather than inevitabilities. Approaching human existence like any other engineering problem, this group of misfits and visionaries is beginning to solve the difficulties that have dogged us for eons.

But are transhumanists really on the right track? And are their utopian dreams feasible, or even desirable? These blinks open a warts-and-all window into a movement that, for better or worse, is poised to change the world.

In these blinks, you'll find out

  * why people are paying top-dollar to have their heads frozen after death;

  * what the _intelligence explosion_ is — and why it makes predicting the future so difficult; and

  * how scientists are trying to eradicate aging.

### 2. Transhumanism provides new answers to questions as old as humanity itself. 

Have you ever wanted to live forever? To possess superhuman intelligence or strength? Or even to be raised from the dead?

The popularity of transhumanism lies in the fact that it responds to aspirations like these, and engages with the most persistent and tantalizing desires of humankind.

From time immemorial, the stories that we've told ourselves have conjured up visions of supernatural powers, immortality, and natural abundance. And for equally long, we've used myths and fables to explain away the hardships of life, like death, illness, and pain.

The Epic of Gilgamesh, the oldest written story we possess, tells the story of a king who ventures across the earth in search of everlasting life. But that's just one example. 

The Bible engages with similar themes. With the banishment of Adam and Eve from the Garden of Eden, humanity was doomed to experience mortality, and to undergo the sufferings that characterize human life.

Stories like these have remained compelling for millennia. But with the decline of faith and the growth of science, something important has changed. Thanks to a whole host of emerging technologies, we can now begin to think of our human frailties as solvable.

That's where transhumanism comes in. The vocabulary is new, and the ideas are cutting-edge, but the quest for immortality and an end to suffering is as old as humanity itself.

Transhumanists believe that we can halt aging; that we can use technology to improve our minds and bodies; and, ultimately, that we can literally unite our physical bodies with ever-advancing technology, and approach cyborgism in the process. In this way, they propose to liberate us from the limitations of our biology.

In some cases, the science underlying transhumanist goals is contested. In others, the ideas just seem plain crazy. Either way, these blinks will familiarize you with the ins and outs of transhumanism, allowing you to make that decision for yourself.

### 3. Firms are already capitalizing on transhumanists’ hopes of resurrection, but the science doesn’t quite stack up. 

Many people with the desire to live indefinitely have pinned their hopes on the Alcor Life Extension Foundation. This foundation offers their customers a significant discount if they opt to have their heads cut off. That's because Alcor specializes in _cryonic suspension_, the long-term preservation of bodies after death, and it's far easier to store your disembodied head than your entire cadaver.

The idea is simple. According to Alcor's founder Max More, there's a brief window between clinical death and the actual breakdown of the body, and it's during this gap, before the process of decay sets in, that Alcor intervenes.

Your body's kept cool as it's flown to the firm's Arizona premises, where some preparatory surgery takes place. This operation involves drilling holes into your skull so that your brain can be inspected, as well as the replacement of your blood with a _cryoprotectant_ preservative liquid.

Once that's done, you can be stored indefinitely in liquid nitrogen. This means that if scientists of the future ever learn how to revive corpses, you'll be well-preserved and ready to be brought back to life immediately.

Failing that, More says, technology might one day allow us to scan brains in order to extract their mental data. This information, he hopes, could then be used to duplicate your mind and run it on a computer.

However, the science underlying this procedure is dubious. Michael Hendricks, a neurobiologist at McGill University, has said belief in reanimation and mind simulation is a false hope. The procedures that Alcor imagines scientists will one day be able to perform, enabling the dead to be revived, are simply "beyond the promise of technology." Hendricks even argues that we should be angry with people who profit from cryonic suspension.

Max More, on the other hand, says he never guarantees that his customers will be brought back to life. In fact, he says, the philosophy of his company is simply that cryonic suspension is worth a shot.

In a way, this attitude seems to underpin the transhumanist movement as a whole. For people who pride themselves on the scientific rigor of their ideas, transhumanists' faith in the future often seems to approach religious conviction.

### 4. Ambitious life extension treatments are already in the pipeline. 

One way to avoid the problems associated with reanimation and the duplication of minds is simply to avoid dying. At least, this is the attitude of Aubrey de Grey, an English biomedical scientist and the director of Strategies for Engineered Negligible Senescence (SENS).

According to de Grey, we shouldn't regard aging and death as natural and unavoidable aspects of life; on the contrary, he argues, aging is an illness which can be cured.

De Grey claims that the strategies he's currently developing will allow humans to continue living indefinitely. He divides his plans to make this a reality neatly in two. The first part, "SENS 1.0," will consist of various therapies he believes can be developed over the next 20 or 30 years. These treatments, he says, will extend the lives of those who are currently middle-aged by up to three decades.

These alone are bold claims, but it's the second part of his plan, "SENS 2.0," that is especially controversial. Essentially, the debate centers on what de Grey calls _longevity escape velocity_. This theory suggests that our medical treatments will eventually allow life expectancy to increase by more than a year, every year.

In other words, longevity escape velocity predicts a world in which the growth of life expectancies outpaces our actual aging — prolonging life indefinitely. As de Grey puts it, this would allow us to remain "one step ahead of the problem" of growing old.

SENS isn't the only organization trying to extend the human life span, however. Laura Deming, who enrolled at MIT aged just 14, founded the Longevity Fund in order to finance research into life extension technologies.

She said she became interested in the field as a child, when she noticed how aging affected her grandmother's quality of life. She was shocked, she says, that no one was working on a "cure" for aging. Even worse, it wasn't even seen as an illness.

As a result, Deming decided that, instead of addressing secondary issues like Alzheimer's, diabetes, and cancer, she was going to tackle the root cause of these diseases: aging itself. Indeed, Deming was pleased to discover that some drugs aimed at these illnesses also had potential as life-extending medicines. Diabetes treatments, she said, were attractive investments for the Longevity Fund, because of a strong but murky link between insulin, blood sugar levels, and life spans.

### 5. The Technological Singularity is coming, whether we want it to or not. 

The _Technological Singularity_ might sound like something straight out of science fiction, but it's an event we're quickly approaching.

On the most basic level, the term describes the point at which the intelligence of machines will outstrip our own, drastically altering the course of human history. But what exactly this will look like, and what the consequences of this development will be, is anyone's guess.

The notion of a coming singularity is, after all, quite recent. The first substantial engagement with the idea came from Vernor Vinge, the science fiction writer and mathematician. In 1993, he claimed that within a couple of decades, the intelligence of our machines would surpass our own. This, he announced, would signal the end of "the human era," for better or for worse.

It's here that Ray Kurzweil, director of engineering at Google and famed futurist, enters the conversation. According to Kurzweil, technology is going to grow simultaneously more powerful and more compact, a fact which will ultimately start to influence human evolution. We will no longer simply "use" devices, he contends; instead, we'll incorporate them into our bodies, and become part machine ourselves.

Kurzweil predicts the singularity will be reached by 2045. And while some disagree, Kurzweil thinks this moment should be welcomed. The singularity will allow us to transcend our human limitations — as a result, he says, there will cease to be any real distinction between human and machine, or between external and virtual reality.

This is one of the most intriguing and troubling things about the singularity; it challenges our most deeply-held convictions about what it is to be human. If we can control our own mortality and merge with our devices, in what sense do we remain human beings?

Kurzweil thinks he has the answer. Rather than ending our humanity, he contends, the singularity would in fact mark the culmination of our human ambitions, by overcoming the greatest obstacle of all: our own nature. What has always defined humanity, according to Kurzweil, is our unwavering commitment to triumphing over limitations. What greater limitations are there, he asks, than the frailty of our own bodies and the inadequacy of our minds?

### 6. Machines with superhuman intelligence pose a grave threat to human welfare. 

The notion of an _intelligence explosion_ was first put forward in 1965 by the British statistician I. J. Good. What would occur, Good asked, when machines with superhuman intelligence became capable of designing newer, improved machines themselves?

With each iteration of machines producing its own more powerful successors, artificial intelligence would rapidly exceed anything we humans could have created. This sudden, machine-driven increase in the powers of AI is exactly what the intelligence explosion refers to.

If the term "explosion," with its associations of detonation, worries you, it's probably because it should. According to Nick Bostrom, a philosopher and former transhumanist, the prospect of a sudden acceleration in the powers of AI has some stark implications for humanity.

These dangers don't match the apocalyptic scenarios depicted in movies, featuring cold and calculating androids who wage war against their makers. According to Bostrom, AI wouldn't display any malice. Instead, a machine might simply carry out its task with such single-minded efficiency that humanity's destruction would be an unintended byproduct.

Take a machine instructed to make paper clips in the most productive way possible. Might it not, asks Bostrom, set about using all the matter in the universe to complete the task, leaving nothing standing but paper clips and paper-clip-manufacturing facilities, and end human life in the process? The example is fanciful, he admits, but it illustrates the type of threat we're dealing with.

It's disasters like this that Nate Soares, executive director of the Machine Intelligence Research Institute, is trying to prevent. Unfortunately, he thinks the odds are stacked against us.

Part of the problem, he says, is that it's difficult for humans to predict the behavior of machines with superhuman intelligence. After the intelligence explosion, all bets are off. For this reason, he remarks, the singularity is "the point past which you expect you can't see."

If we somehow manage to avoid death at the hands of AI, Soares predicts great things– a never-ending stream of scientific breakthroughs. But he's pessimistic. "I do think," he says, "that this is the shit that's gonna kill me."

> "_This was what we did as a species, after all: we built ingenious devices, and we destroyed things."_

### 7. Robots still lag behind when it comes to everyday usefulness – and that might be a good thing. 

For upwards of a hundred years, robots have exerted a powerful hold over the imagination of humankind — but up until now, they've been more significant as cultural icons than as actually _useful_ machines.

One reason that the development of robots has been slow compared to, say, the progress of AI, is what's called _Moravec's Paradox_. This refers to the fact that, although our machines are capable of outperforming us on high-level cognitive tasks, it's been tremendously difficult to make robots perform simple physical jobs, like exiting vehicles and opening doors.

At the DARPA Robotics Challenge, the author saw this firsthand. The event is an annual competition attended by teams of robotics engineers, all keen to take home the million-dollar prize. In order to win, a team's robot must outperform its rivals in challenging physical tasks, like driving a vehicle, entering a building, and navigating an obstacle-filled environment.

The thing is, many of the robots were simply _bad_ at these tasks. In fact, many teams opted to forfeit points by lifting their robot out of a vehicle, rather than risk the robot sustaining damage by exiting on its own.

What we increasingly see, however, is that engineers can overcome these limitations — meaning that robots are gradually making incursions into the labor market. For example, Amazon recently held its own robotics competition, challenging engineers to develop robots capable of doing the job of its human stock pickers.

Does this mean that a robotic utopia is around the corner, with humans served by helpful androids? Not exactly. As this example suggests, the immediate impact of high-functioning robots will not be entirely positive. As Uber's plan to replace drivers with automated cars demonstrates, the advance of robotics might threaten the livelihoods of many low-skilled workers.

And the problems go deeper than that. Take DARPA, for instance, which runs the annual robotics competition: its full name is the Defense Advanced Research Projects Agency, and it's a branch of the Pentagon tasked with the development of novel military technologies.

Once we develop robots capable of opening doors and exiting vehicles, we might not have to wait long before we see the first generation of robotic supersoldiers. After all, the Predator and Reaper drones that were deployed to such lethal effect in Pakistan, ultimately killing hundreds of civilians, were both DARPA projects.

### 8. Cyborgism is emerging from its infancy, with the help of a group of dedicated biohackers. 

One thing that seems to define transhumanists is their focus on the future. After all, the advent of the singularity, life-extension technology, and useful robots are all decades away.

But the transhumanists at Grindhouse Wetware are different; they focus on what can be done in the present.

In practice, this boils down to the first amateur steps toward cyborgism. Tim Cannon, leader of the group of biohackers collectively known as Grindhouse Wetware, is uncompromising on the topic of human improvement. Optimization, he says, is simply not enough; what we need to do is change our "hardware."

This isn't just idle talk. For three months Cannon had a device named Circadia implanted in his arm. About the size of a pack of cigarettes, the Circadia took biometric measurements every five seconds and uploaded them to the internet. Cannon even linked the air conditioning in his home to the device, so that it automatically adjusted to suit his body temperature.

Another implantation Grindhouse Wetware is working on is called Northstar. The current version, which is implanted under the skin on someone's hand, detects magnetic north and glows red in response. The planned upgrade will expand the device's functionality by using gesture recognition to allow a user to open the door of her car or turn on the engine simply by moving her hands.

These inventions aren't earth-shattering, Cannon admits. What matters, however, is that they're a step in the right direction. When you recognize that humans are suboptimal machines, his thinking goes, there's no telling what can eventually be done to improve us.

And yet, for all the futuristic glamour of these biohacking devices and for all the loftiness of the group's ideals, their concerns are firmly rooted in the messiness of human life. Talking to Cannon, for example, the author couldn't help but wonder whether his vision of humanity had been inspired in part by the difficulties he'd faced in his own life.

Cannon struggled with alcoholism for years, before eventually overcoming his addiction with the help of Alcoholics Anonymous. Is it any wonder, the author asks, that he's come to see humans as breakable creatures, prone to fail and to make mistakes?

> "_Biology was the fundamental difficulty; the nature of the problem was nature itself."_

### 9. Final summary 

The key message in these blinks:

**For better or worse, humanity is at a juncture. Technologies that will fundamentally alter our way of life are already in the pipeline, and transhumanism, for all its flaws, represents an attempt to use these developments as a force for good. But whether this mission will ultimately succeed, or whether it will lead to humanity's utter destruction, remains an open question.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Immortality_** **, by Stephen Cave**

You've just encountered some of the key ideas fueling the growth of the transhumanist movement, one of which is the notion of triumphing over death. But why is this particular desire so common? Why, in other words, do we fear death — and what can we do to manage our apprehension?

In _Immortality_, __ writer and thinker Stephen Cave sets out to answer these questions. In these blinks, you'll learn about the "immortality narratives" that we use to cope with our impending death, and to which all forms of culture owe their existence. To find out more, check out our blinks to _Immortality_, by Stephen Cave.
---

### Mark O’Connell

Mark O'Connell is an award-winning literary critic, journalist, and essayist from Ireland, with a PhD in English from Trinity College Dublin. He has contributed to the _Observer,_ the _New York Times Book Review_ and the _Dublin Review._ He is also the author of _Notes From an Apocalypse_.

