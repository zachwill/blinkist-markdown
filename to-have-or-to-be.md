---
id: 5e32c94d6cee0700067c3d73
slug: to-have-or-to-be-en
published_date: 2020-01-31T00:00:00.000+00:00
author: Erich Fromm
title: To Have Or To Be?
subtitle: None
main_color: None
text_color: None
---

# To Have Or To Be?

_None_

**Erich Fromm**

_To Have or To Be_ (1976) challenges the consumerist motivations that drive us "to have" and instead invites readers to embrace a new ideal: "to be." This way of life provides the fulfillment that power, status, and keeping up with the Joneses promise don't deliver.

---
### 1. What’s in it for me? Find the freedom to just be. 

What encapsulates you? The money in your bank account? Your professional accomplishments? Your physique? What about your status? Your house and your clothes?

Culture tells us that we are what we possess; that the road to significance, purpose, and joy is paved with purchases and accomplishments. That fulfillment comes through "having."

But what if there was a better way? 

Through what Erich Fromm calls "being," the things that we achieve and acquire cease to define us. By transforming our mindset from "having" to "being," we no longer need to compete for significance, purpose, and joy. Instead, we're free to pursue that which truly fulfills us—physically, mentally, and emotionally—and to shape our society in a similar way.

These blinks explain how we can shift to this new way of living.

In these blinks, you'll learn

  * what's wrong with a "having" mindset;

  * why you can't tie happiness to economic growth; and

  * which mindset might just save the planet.

### 2. Economic progress doesn’t guarantee happiness or wealth. 

Everywhere we look, another product promises to better our lives: a new car to make us feel successful, a phone to help us manage our responsibilities. But if a better life is readily available, why are so many of us overwhelmed, lonely, disillusioned, and discontented? 

Contrary to what we often hear, there's more to life than unbridled consumption. 

In fact, Western civilization's "pursuit of happiness" has produced anxiety, depression, and addictions like never before. The urge to compete and compare leaves many of us isolated, and convinced that we need to be more and have more. Ironically, as long as accumulation is our primary ambition, we'll never accumulate enough. We'll grow more and more unfulfilled. But the consequences of unbridled consumption don't stop at the level of the individual.

There are societal consequences, too. One of these is the consolidation of wealth in the hands of the few.

Because our capitalistic, "every-man-for-himself" system rewards selfishness and greed, things like solidarity, sharing, and contentment are devalued. Over time, this increases the disparity between economic classes, and can even cause wars. Historically speaking, greed has factored into almost every international war—from the French and Indian War to World War I.

When greed drives society, everyone loses. Employers seek to deceive their customers, destroy their competition, and exploit their workers. So, where does that leave us? Is communism the solution? No, because, like capitalism, communism doesn't seek to curb consumption.

Nevertheless, drastic social and economic changes are necessary to reshape a society that no longer benefits the majority. Otherwise, our cycle of dissatisfaction will continue.

Every day, we're inundated with ideas about what we need to buy in order to be happy, and what we need to think in order to belong. But how does this affect the way we relate to ourselves and others? Is there a better way? Let's explore those questions next.

### 3. Two modes, “having” and “being,” determine how we experience life. 

Name some of your possessions: watch, sunglasses. Check. What about your job or your spouse? 

Wait—are those possessions? Hardly, but that doesn't mean we don't see them that way. That explains why, when we lack these things, we're prone to perceive ourselves—and others who also lack them — as incomplete. However, if we exchange the "having" mode for the "being" mode, we recognize that we're complete regardless of how much we accumulate.

Let's start with the having mode, which objectifies _everything._

When the meaning of life is to accumulate as much as possible, we can even view people as things to be acquired. For example, consider our use of language: We speak more about "having" a wife than we do about "being" a husband. But let's take it a step further. 

In the having mode, what we call "love" can actually be a means of manipulating others to get what we want; this can be the case when parents require their children to behave a certain way or achieve certain things.

But the objectification doesn't stop there.

The having mode leads us to objectify our opinions, too. This is evident in the way we cling to our thoughts rather than remaining open to new ideas. To change opinion is like losing a possession. We aren't just relinquishing an opinion — we're relinquishing a piece of our identity. 

In contrast, when we're in being mode, authentic relationships are emphasized. This frees us to engage with others — even those who don't share our viewpoint — honestly and generously, allowing for a deeper, fuller life. 

While in the being mode, our purpose is to _become_ — to express who we are rather than what we have. This empowers us to embrace change, because what truly matters is how we relate to the world and the satisfaction we find in our unique and authentic needs. 

So the choice between having and being is central to our fulfillment. But it's not just the author who recognizes this. Many of history's greatest thinkers did too.

The Buddha, for example, taught that we overcome suffering and achieve Nirvana only when we stop craving possessions. According to the Bible, Jesus asked, "What do you benefit if you gain the whole world but are yourself lost?" Karl Marx argued that luxury is as dangerous as poverty and that, rather than striving to _have much_, people should strive to _be much_. 

"Being" sounds better, doesn't it? And yet the having mode permeates every layer of society. We'll see how, and what it results in, in the next blink.

### 4. The having mode breeds selfishness. 

Getting everything we want seems like it would be fulfilling. But would it? When everyone assumes this objective, those in power only consider themselves instead of acting in the interest of others, as we'd like them to. Citizens, meanwhile, sit idly by when societal conflicts don't directly affect their personal concerns. 

Let's start by looking at those in power. Glorifying selfishness produces selfishly motivated leaders. 

It's common for business executives and politicians to create compensation policies, rules, and legislation benefiting themselves rather than their employees or constituents. An example of this is when a CEO's year-end bonus and corporate luxuries remain unchanged amid company layoffs. Another example is when a politician serves the interests of a corporation or wealthy donor in exchange for financial support, even if those interests don't benefit the general public. 

These instances of self-centeredness no longer shock us. We even anticipate them. But if these leaders are products of a society we collectively create, can we really expect more of them?

That brings us to the general public. Here, the having mode leads people to forgo social responsibility.

Rather than looking for and then doing things to improve society for everyone, we focus solely on ourselves. Consumed with our private interests, we care little about those of others.

Sure, we may bemoan the state of affairs. But in the end, we choose passivity over actively holding our moral and civil leaders accountable, because the latter doesn't directly give us more of what we want.

And so the cycle continues.

While policymakers and corporate leaders breed antagonism between classes, the general public silently participates by doing nothing. Over time, a growing rift separates those with power and money from those without, leaving two groups: the exploiters and the exploited.

For the exploited, the lone route to change is often revolution. A social revolution is ultimately what a society in the having mode needs in order to become a society built on "being."

The positive effects of such a shift would extend far and wide. Foreign relations would improve. Wars for the sake of acquiring resources would no longer occur. Peace would be possible.

So it's apparent how the having mode breeds selfishness and hostility. But this doesn't just occur between people — it also occurs between humans and nature. Let's look at that next.

### 5. Humanity wars with nature, regardless of the cost. 

Until the Industrial Revolution, humanity worked within the confines of nature. We respected it, recognizing that if earth couldn't flourish, neither could we. However, fueled by greed, our environmental exploitation now endangers not only the earth's survival, but also our own. 

Gone are the days when we viewed ourselves as a part of nature and prioritized its wellbeing. Now, we seek to conquer it for our benefit.

One way we do this is by exploiting natural resources like water, air, and forests. But no resource lasts forever, and nature eventually fights back against human irresponsibility.

But even as nature fights back, humanity arrogantly presses on.

With mechanical and nuclear energy capable of mass destruction at our fingertips, we feel omnipotent, and substituting computers for the human mind has convinced us we're omniscient, too. We've constructed a new world in which we are the gods, and nature is nothing more than a building block. But we've reached a critical juncture.

Our survival depends on a dramatic ideological shift.

Industrialism treats the environment with contempt. But poisoning the earth and plundering its resources causes ecological damage and meteorological change. These could ultimately result in global famine, the annihilation of humanity, and even the end of all life on earth. 

Concerning nature, and to avert catastrophe, we need a new ethic. We must undergo radical transformation. Next, let's explore what that might look like.

### 6. We need to understand the root of our dissatisfaction and revolutionize our experience by transforming our character. 

Two thousand years ago, Siddhartha Gautama abandoned a life of power and opulence when he realized that having everything he wanted hadn't eliminated suffering from his life. Recognizing that a religion of poverty and love provided the enlightenment and meaning he sought, he became the Buddha. While we needn't abandon everything as he did, we must experience a seismic shift in thinking to find the life we truly crave.

The first step toward this kind of change is awareness of the problem, which is that the "having" mode leads to suffering. 

How? Here are three ways. 

One: When we anchor our identity in what we have, everything we possess can be lost, so insecurities naturally arise. We're never emotionally safe. Our entire existence is precarious.

Two: Competing for power, profit, and approval pits us against one another. But in this game, no one wins. Because there's always someone else with more talent, muscle, influence, or accomplishments, comparison only serves to rob us of purpose and joy.

Three: We chase pleasure, seeking to satisfy fleeting desires. Our happiness is dependent upon the next thrill, but when the initial excitement dwindles, it inevitably gives way to sadness.

So awareness of the problem is the first step toward change. The next step is recognizing that the way out of suffering is choosing a mindset of "being."

How does that help? Here are three ways.

One: The "being" mindset creates self-confidence and a healthy sense of strength. After all, no one can take away what's inside of us.

Two: When we're free to appreciate things without striving to possess them, we encounter mutual joy between people, one of the most profound of human experiences.

Three: As "being" people, we cultivate fulfillment by expressing our ability to love, reason, create, and share. We live in the radiance of being, regardless of our circumstances. 

Everyone is capable of operating in the empty state of having or the fulfilling state of being. Nevertheless, culture plays a critical role in determining which of these is our dominant motivator. In other words, if we're going to reshape how we live, we need to reshape our society.

### 7. To transform the human experience, we must reshape society and identify a new reason for existence. 

Though we may not like to admit it, most of us are products of our environment. So to free ourselves from the suffering born of consumerist culture, we need to reconstruct our collective foundation. As the author suggests, we can only establish a new society if we first establish new priorities. 

We're all in this together. If we're going to achieve solidarity, we must together identify a shared ideal or a conviction on which to model our "being" society.

So, where do we start?

Philosophically, we must return to the question, "What benefits humanity?" rather than, "What benefits the system?"

Practically, we must redefine what it means to be productive. As a society, productivity no longer means _outward_ business to the detriment of _internal_ wellbeing. Instead, it means doing things in service of and as an expression of who we are. W _hy_ we do things becomes far more important than _what_ we do.

This shift in motivation will enhance our relationships with people, possessions, and time.

As society stops conforming to the having mindset, we will stop objectifying and dehumanizing each other. Honesty and creative self-expression will be not only permissible but cherished.

With regard to possessions, humanity will always need to own certain things. But through this shift in motivation, we will relinquish the unsatisfying pursuit of consumerist having, and instead pursue responsible having. We will aim to acquire only that which is necessary to live.

Lastly, we will learn to be responsible with time rather than submitting to its tyranny. Currently, we try to save time; we try to kill it. Relating to time differently means respecting it enough to avoid the extremes of hyperproductivity and laziness. 

It's easy to perceive dissatisfaction and suffering as inevitable. But if we collectively pursue being rather than having, we can better our society and, in turn, find individual fulfillment. So what are the barriers to these changes? We'll examine that in our next and final blink.

### 8. Humanity is inclined to avoid change, even at the expense of present and future fulfillment. 

In 1937, during the Spanish Civil War, Arthur Koestler, a British correspondent, took refuge in the city of Málaga. Though he was aware that General Franco's Spanish nationalist forces wanted to arrest him, and though most of the city's inhabitants had fled, Koestler was unwilling to relinquish the comfort of his warm villa and venture out into the cold and rainy night. He stayed, and this inability to act led to his capture. Koestler's arrest illustrates how the fear of discomfort and the unknown can enable our downfall.

When considering the changes society needs to make, we get uncomfortable, so much so that we're willing to risk tomorrow's calamities as long as we don't have to sacrifice today.

Let's face it: Taking a risk or venturing into the unknown can be frightening. Every step feels dangerous. We could fail. To avoid confronting these fears, we often stick to what feels familiar and safe. But as a society, we can't afford to keep the status quo.

We must transition from a spectator democracy to a participatory democracy in which private citizens prioritize the public good.

For example, by forming committees to evaluate products and policy, government and citizens alike would receive the information they need to clearly identify those which benefit society. Based on this information, the government would gradually install a thorough educational process to help recognize, address, and even remove harmful commodities.

This transition won't be easy. But we must overhaul our existing system, which prioritizes a healthy economy at the expense of human health. 

This means shifting from unbridled to responsible consumption. Rather than aiming for unlimited growth, we should strive for discerning growth, implementing the necessary changes in a balanced way in order to avoid economic crisis.

This kind of action may sound costly. But the cost of inaction is humanity's eventual destruction. If all of this sounds overwhelming, here's a reason to be hopeful: Cultures shape people to behave as they believe they must. If enough of us choose "to be" instead of "to have," everyone can experience deeper fulfillment.

### 9. Final summary 

The key message in these blinks:

**Finding true significance, purpose, and joy hangs on your ability to abandon the "having" mindset in favor of the "being" mindset. This means prioritizing that which brings lasting fulfillment to both yourself and others, rather than selfishly pursuing power and possessions. So release your fear of change and embrace the life you've always wanted.**

Actionable Advice:

**Assess whether or not the ways you spend your time are really meaningful to you.**

Let's say you often default to binge-watching television. There's nothing wrong with that. But would doing something else make you happier? Something like learning to play the piano, for example, or having coffee with a friend? Disrupting your routine might initially cause discomfort, but it might also brighten your day far more than that next episode on your watch list. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next: The Art of Loving, by Erich Fromm.**

We've just explored how Erich Fromm thought that moving from a "having" to a "being" mindset could transform society for the better. Fromm was one of the most prominent thinkers of his time. His thoughts and theories on a range of subjects from sociology to psychology influence people to this day.

In _The Art of Loving,_ Fromm explores the concept of love and how we can maximize our own ability to love. If you want to know the difference between motherly and fatherly love; how modern society is damaging our ability to love; and why practicing love is like practicing a musical instrument, head over to our blinks to _The Art of Loving._
---

### Erich Fromm

Erich Fromm (1900-1980) was a world-renowned psychologist, sociologist, and philosopher. Born in Frankfurt am Main, Germany, Fromm was the child of Orthodox Jewish parents. When the Nazis took power in 1934, he moved to New York; later, he became a US citizen and professor at several universities, among them Columbia and Yale.

