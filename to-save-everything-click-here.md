---
id: 53fdb9dc3762620008ae0000
slug: to-save-everything-click-here-en
published_date: 2014-08-26T00:00:00.000+00:00
author: Evgeny Morozov
title: To Save Everything, Click Here
subtitle: The Folly of Technological Solutionism
main_color: FFFCD7
text_color: 807A38
---

# To Save Everything, Click Here

_The Folly of Technological Solutionism_

**Evgeny Morozov**

_To_ _Save_ _Everything,_ _Click_ _Here_ confronts us with some of the startling truths about our use of technology, namely: it's not always good for us. While modern gadgets have made our lives better in many ways, it turns out that technology alone may not be the best answer to all of our problems.

---
### 1. What’s in it for me? Learn why technology is both a blessing and a curse. 

In our frenzy to purchase and use new technology, we often forget to ask ourselves some important questions, such as: Is this gadget actually good for me? How will our society change once everyone is using this device?

These questions are easy to gloss over, but they're important. The steam engine, the radio, TV, the internet: all of these new technologies had profound implications for the societies in which they were first introduced, and that's no less true for the technologies of the future.

Because of this, it's important that we look at these new technologies with some skepticism, not just jump for joy at every new iteration of the iPhone. In fact, today's society in particular could use a strong dose of sober awareness.

That's exactly what these blinks set out to provide. They'll show you how our latest and greatest technologies and all the fantastic things they're supposed to bring with them may not be so great after all.

In these blinks, you'll discover

  * how suspicious it is _not_ to have a Facebook account;

  * how Google's autocomplete might be leading you astray; and

  * why offering you money to donate blood makes you less likely to head to the blood bank.

### 2. Technological solutionists believe that technology will always improve society. 

Throughout history, new technologies have made human life easier. From the cotton gin to robotic arms in factories, many technologies have been invented in order to simplify or totally take over our tasks, meaning that people today have a lot less to worry about than they did 100 years ago.

In today's world, modern machines line conveyer belts in factories, food production is almost completely automated, and new intelligent machines that take even more tasks off our hands are being invented every year.

This technological progress shows no signs of slowing, which suggests that the machines, gadgets and smart devices of the near future will only further simplify our lives.

_Technological_ _solutionists_ — whom the author refers to as "solutionists" — take this to mean that, with the right technologies, every single problem becomes solvable, and that there's no problem that doesn't have a "technological fix."

While some of today's problems might _seem_ to be impossible to grapple with, history has shown that even these "impossible" problems can be solved by novel inventions.

For example, we once thought it was impossible for humans to take flight. And yet, the invention of the airplane suddenly allowed us to overcome great distances above the clouds. In the same vein, the internet and smartphones allow us to be constantly connected with people all over the world.

Solutionists take this a step further. They believe that new technologies won't only solve specific problems, but will improve the quality of our lives in general. They see technology as a vehicle to improve every aspect of our modern society.

They believe, for example, that we can improve our democratic systems by using technologies, such as the internet, to make them more transparent, or use smart devices to curb crime.

Looking at the history of technology, it's understandable why they would believe this. But, as the following blinks will show, technology may not be the answer to all our problems.

### 3. The solutionist demand for total political transparency is hard to satisfy. 

Nearly every election cycle, the political discourse becomes rife with demands for greater transparency. Reading the latest corruption scandals, it's no surprise that calls for transparency take center stage in political discourse.

After all, our politicians (should) represent the will of the people. But how exactly can we achieve this transparency?

Solutionists believe that the internet will force politics to be transparent. Virtually everything that has been written on the internet is archived on a server somewhere and, since everyone has access to this information, solutionists believe that an internet that never forgets will force politicians to stay true to their word.

This, in turn, would lead to better political decision-making: politics would refocus itself on the greater good rather than the good of those in office.

However, total transparency is difficult to achieve.

Although the internet has the power to archive the promises of politicians, that doesn't necessarily mean that these promises will become part of our common knowledge or even reach the public.

Instead, politicians — knowing that everything they say in public will be shared and archived — will return to the clandestine world of private conferences and hallway meetings as a means to prevent the press and public from becoming privy to their words.

Or, if they'd rather take a less covert route, they could overwhelm their constituents with a process called _snowing_. Rather than concealing information, they simply spread as much low-quality information as quickly and widely as possible, thus making it impossible to understand what is really going on from an outsider's perspective.

This tactic isn't new, either. In fact, Jean-Jacques Rousseau complained even as far back as 1754 that "books and auditing of accounts, instead of exposing frauds, only conceal them."

### 4. The internet promises more democratic participation, but that’s not necessarily a good thing. 

The internet enables people to participate directly in online activity all across the world. Solutionists have high hopes for what this will mean for politics, dreaming that this will translate into direct democracy.

Governments, however, already know that direct democracy is nothing to strive for. After all, not everyone who has an _opinion_ on politics necessarily has _knowledge_ of the issues, which is why most democratic countries use a form of indirect democracy instead.

And since no one can be an expert in everything, politicians themselves are usually just experts on one particular topic. We, of course, can't fault them for this, as gaining expertise requires spending a lot of time focused on one particular topic. In fact, those who claim to be experts in a wide variety of topics often have no real expertise in any.

In this system, citizens can form opinions on different political issues and then vote for the party which best represents their political leanings. They don't require the same kind of expert knowledge on political topics that professional politicians do in order to be politically engaged.

Recent history has also shown us that attempts at direct democracy have only proven that indirect democracy is more productive.

Take the Pirate Party, a European political party that focuses on reforming copyright law, which granted all members of the party the opportunity to directly participate in the political decision-making process by allowing them to propose topics the party should discuss.

As a result, the party was focused on topics that were unrelated to the "more serious" issues facing Europe at the time, such as the European financial crisis or the civil war in Syria.

In trying to be more democratic, they missed opportunities to deal with events that were perceived as more important by their constituents, leading to a devastating loss of popularity in all the countries where they had been elected.

### 5. The internet is not neutral: its content is controlled by large companies like Google and Facebook. 

Nowadays, we don't often discover new websites by typing interesting-sounding URLs into the browser and hoping to happen upon something worthwhile. Instead, we place our trust in search engines, find interesting new trends on Youtube, or click on personalized ads provided by Facebook.

Consider, for example, Google's autocomplete function in its search engine. If you start typing a query in Google, their algorithms automatically offer you a series of queries which are supposedly influenced by other users' searches, and thereby related to yours.

For instance, if you type "Britney Spears is" into Google, autocomplete will offer suggestions like "a hot mess" or even "a reptilian alien." In this way, Google actually participates in _creating_ trends by directing your attention to certain topics you may not have considered before, rather than merely providing you with results to your search.

What's more, companies like Google will insist that the algorithms they use to determine which links we see are perfectly neutral, claiming that the highest-ranked pages in your search are there because they are visited the most frequently.

However, this is wrong for two reasons.

First, companies and individuals can influence their page rank by simply paying people to search certain queries or for linking to certain websites. Essentially, anyone can just throw around money to improve their ranking and thus become more visible.

Second, the algorithms used by companies like Google or Facebook _can't_ be neutral because they actually censor the content that appears on the page. As of right now, this censorship is restricted to illegal hits, meaning autocomplete won't help you if you search for pirated movies online.

So it seems that technology doesn't have all the answers. Even worse, however, is that modern technology can even alter our humanity.

### 6. Although new technologies can predict crime, predicting every crime wouldn’t benefit our society. 

Can you imagine living in a world where the police were able to nab criminals _before_ they ever committed a crime? Believe it or not, that future is fast approaching.

Indeed, new technologies offer various ways to prevent crimes by identifying them before they even happen. Predictive policing is becoming increasingly popular, especially in the United States.

For example, new smart systems can predict gun violence by cross-referencing the sounds it picks up with sounds that have preceded gunshots in the past. By recognizing these patterns, it has become easier for technology to assess the situations that often lead to gun violence and alarm the police so they can intervene.

In the same way, large internet companies can also predict when it is likely that their users are about to commit a crime. Companies like Facebook have built-in algorithms that can compare the chat behavior of their users with indexed past chats that preceded crimes.

For example, in 2012, Facebook helped law enforcement arrest a middle-aged man chatting about sex with a 13-year-old girl. Thanks to its predictive algorithms, Facebook was able to recognize the inappropriate chatting and prevent a possible crime.

And as these smart systems and surveillance technologies become more prevalent, it will become increasingly difficult to violate the law.

However, sometimes breaking the rules is _necessary_ to improve our society. When law-breaking becomes impossible, so does civil disobedience. Suppose a law is introduced that violates a fundamental human right. In this case, civil disobedience is necessary to challenge the state and reverse the law.

Consider Rosa Parks's refusal to give up her seat to a white man on a public bus — an act which helped galvanize the civil rights movement in the United States. However, this courageous act was only possible because there was still room for disobedience. Rosa Parks may not have even been able to enter a smart bus of the future, which might have predicted her actions.

> _"Remove the opportunity to break the law, and the government loses an important channel of learning from the citizens."_

### 7. As self-tracking becomes the norm, we lose our natural ability to assess what’s good for us. 

Do you post photos on Facebook? Do you check in at your favorite restaurant on Foursquare? Nearly everybody is on some social media platform or another. Participating in and sharing our lives with online communities is near universal.

In fact, _self-tracking_, i.e., the monitoring and tracking of your own behavior, is so popular that soon non-participation will be considered suspicious. Today, it's normal to belong to one or more social networks and be logged into all of them simultaneously.

But what exactly led to the rise in self-tracking? Technology journalist Gary Wolf sees it as relating to the convergence of shrinking electronic sensors, the ubiquity of smartphones, social media sharing, and cloud computing. All of these things have made it possible to see and store our data, and share it with others if we want.

In a world where all of our experiences are laid bare for all to see, privacy has become the exception, not the rule.

So what does all this sharing mean for us?

Self-tracking provides modern machines tons of data on our habits, which the machines can then use in a variety of ways to "optimize" the habits.

For example, smart cameras in the kitchen can tell us when we're using the wrong ingredients for our cake recipe; cutting-edge smart devices can even identify which of our behaviors will result in increased happiness.

This analysis and quantification of life creates two problems.

First of all, when we trust our gadgets to make all our decisions, we lose our natural sense of what's good for us. We begin to follow the advice of machines without questioning it.

Secondly, the data derived from self-tracking isn't easy to interpret. Just because a clever device tells you that you're happier when you're out drinking with friends than you are at work doesn't mean you should quit your job.

Regardless of how sophisticated self-tracking becomes, it's unreasonable to trust it blindly.

### 8. Gamifying our lives could mean that people no longer do anything out of intrinsic motivation. 

Imagine that everything in your life was monitored by a computer that could tell you exactly what you needed to do in order to achieve your next big goal — and even rewarded you for it!

This strange reality might be on the horizon due to the rise of _gamification_, i.e., applying the principles of games to "real life" to make it more exciting. With gamification, users get virtual rewards, like points or badges, for solving problems in the real world — just like they would in their favorite video games.

For example, not everyone likes to exercise. But if you could turn exercise into a game, as Nintendo did with _Wii_ _Sports_, you can entice more people to burn a few calories.

And many other companies are jumping on the bandwagon. In fact, one survey from 2013 found that over 70 percent of _Forbes'_ Global 2000 companies planned to include gamification in their marketing. This will only increase as time passes and motion-tracking and communications technologies become more sophisticated, thus making gamification much easier to implement.

However, gamification isn't all fun and games. It also carries the very real danger that people will only be motivated to act if they receive some kind of reward.

Motivation can either be _extrinsic_, where you try to earn some external reward (such as money or virtual points), or _intrinsic_, where you create the purpose for your actions, i.e., you act because it's the right thing to do.

The difference between them is important: several studies have shown that intrinsic motivation is far stronger than extrinsic motivation.

For example, when people are offered cash for blood donations, fewer people end up donating. And when fines are imposed on parents who are late in picking up their children from school, there are _more_ — not fewer — late pick-ups.

While new technology might get us to exercise more, it can't necessarily motivate us to do the right thing.

### 9. Final summary 

The key message in this book:

**Seeing** **all** **the** **great** **things** **that** **technology** **has** **done** **for** **humanity,** **it's** **no** **wonder** **that** **some** **people** **believe** **that** **technology** **can** **solve** **_all_** **our** **problems.** **But** **this** **belief** **is** **shortsighted:** **technology** **is** **not** **always** **good,** **and** **in** **some** **cases** **can** **even** **be** **seriously** **damaging.**

Actionable advice:

**Don't** **let** **autocomplete** **push** **you** **around.**

The next time you see Google's autocomplete recommendations, take a moment to consider whether they truly reflect what you're looking for. In this small way, you can take back a bit of control over your life from large online corporations.

**Suggested** **further** **reading:** **_The_** **_Second_** **_Machine_** **_Age_** **by** **Erik** **Brynjolfsson**

_The_ _Second_ _Machine_ _Age_ examines how technological progress is drastically changing our society and why this development is not necessarily positive. It compares the rapid development of computer technology to the advent of the steam engine, which once catapulted the world into the Industrial Revolution.
---

### Evgeny Morozov

Evgeny Morozov is a writer and researcher who focuses on the political and social impacts of new technology. In addition to authoring the critically acclaimed book _The_ _Net_ _Delusion_ (also available in blinks), he has also been published in _The_ _New_ _York_ _Times_ and _The_ _Wall_ _Street_ _Journal._

