---
id: 512750a6e4b0714805cbbe9c
slug: to-sell-is-human-en
published_date: 2013-05-13T00:00:00.000+00:00
author: Daniel Pink
title: To Sell Is Human
subtitle: The Surprising Truth About Moving Others
main_color: D52B4E
text_color: D52B4E
---

# To Sell Is Human

_The Surprising Truth About Moving Others_

**Daniel Pink**

_To Sell Is Human_ explains how selling has become an important part of almost every job, and equips the reader with tools and techniques to be more effective at persuading others.

---
### 1. Selling – or at least moving others – is part of almost every job today. 

Even though the door-to-door salesmen who used to peddle everything from mothballs to feather dusters may have disappeared, the activity of selling is still as relevant and vibrant as ever — and is increasingly becoming part of everyone's job.

In large companies, the traditional line between sales and other departments is blurring fast. Engineers, product designers and customer support staff all interact with customers and can therefore help bring in sales. The enterprise software company Atlassian, for example, generated revenues of just over $100 million in 2011 without a single dedicated salesperson.

This same "we're all in sales" ethos also applies to entrepreneurs, soon to be the majority of the workforce in the United States. A start-up usually can't afford a sales department — therefore everyone has to sell.

In addition to sales activities creeping into many jobs, research shows that, on average, people spend some 40 percent of their time at work engaged in so-called _non-sales selling_, meaning persuading, convincing and influencing others. In other words, we spend almost half our time at work trying to _move people_.

Non-sales selling is perhaps most important in the education and medical (Ed-Med) industries: A teacher persuades students to trade time and energy for an education; a doctor persuades patients to give up the pork chops they love for better health. Considering that Ed-Med is now the largest job sector in the US economy, and growing fast, it's clear that moving people is becoming an increasingly important part of many Americans' work.

This transforming landscape in both sales and non-sales selling is making most of us some sort of salesperson.

### 2. Honesty and service are the new tenets of sales. 

When most people think of selling, the first thing that springs to mind is a slick, pushy used-car salesman — a distinctly negative image.

Indeed, salespeople used to be able to get away with almost anything in the past, because it was the customer's responsibility to not make a bad purchase, a policy known as _caveat emptor_ — "buyer beware." With used cars, for example, customers could be duped into overpaying for a poor product because the seller knew much more about it than the customer; there was an _information asymmetry_. 

The internet has radically changed this dynamic. Looking to buy a used Nissan Maxima? You can go online and find information on car values, technical specifications and reviews of dealerships in your area. You can also expose a dishonest dealer online, doing serious damage to their business.

We have gone from caveat emptor to _caveat venditor_ — "seller beware." The internet has made honesty and transparency vital for most sellers. Because the seller can no longer function as a custodian of information, she must become more service-oriented, asking the customer questions and helping him understand the flood of information available online.

This same trend applies to non-sales selling as well. Teachers and doctors are no longer the keepers of knowledge on topics like the Crimean War and irritable bowel syndrome, as students and patients can find information online for themselves. Hence, the value of their work must come from the service they provide, curating and explaining the information.

This radical change also means that the famous ABC of selling, "Always Be Closing," is hopelessly outdated. Instead, a new _ABC of moving people_ is needed: "Attunement, Buoyancy, Clarity."

### 3. Attunement: Understand others’ perspectives to move them effectively. 

_Attunement_, meaning our ability to see things from other people's perspective and act accordingly, is vital for our ability to move others.

The stereotype is that successful salespeople are always fast-talking extroverts, but research has shown that being too extroverted can actually hurt sales. An overly gregarious showboat doesn't listen to customers, much less see things from their perspective. In fact, the most successful salespeople are _ambiverts_, occupying the middle ground between introvert and extrovert. They are able to listen to customers, attune themselves to their perspective and then make the sale.

Attunement means understanding what others are _thinking_, and should not be confused with empathy, which means understanding what others are _feeling_. While empathy too is valuable in its own right, research has shown that when trying to move others, cognitive perspective-taking is even more important. So how can you help yourself be attuned?

You might assume that a position of power is a good thing in any encounter, but in fact, studies show it decreases attunement. If we feel we're in a powerful position, we tend to stick rigidly to our own views. Therefore, always start encounters with an assumption that you have a low power position, for that will help you understand others.

Finally, don't forget the somewhat surprising physical component of attunement: _mimicry_. Humans tend to naturally, and often unconsciously, mimic the accents, postures and behaviors of others. Research has shown subtle mimicry can help salespeople close a deal.

Experiment with mimicry by, for example, repeating back verbatim points your conversation partner has made, or by adjusting your posture to suit hers. Just don't overdo it or be too obvious about it, or you'll look ridiculous.

### 4. Buoyancy: Overcome rejection before, during, and after it happens. 

From a polite "no" to a slammed door in the face, every salesperson in the world has faced rejection. This is where _buoyancy_ comes in, the ability to stay afloat in a sea of rejection and keep selling day after day, without losing faith. So what can you do before, during and after a sales call to stay buoyant?

Self-help authors have long advocated pumping yourself up with motivational self-declarations like, "I am the greatest!" when preparing for a difficult task. But research shows we should rather emulate Bob the Builder, who asks, "Can we fix it?"

Such _interrogative self-talk_ (i.e., asking yourself questions) helps you prepare for and deal with problems. This is because questions like, "Can I sell this vacuum cleaner?" force you to think about the answer, thus uncovering possible sales strategies and your underlying motivations beforehand.

To be buoyant during the actual sales activity, you must stay _positive_. Research has shown this helps broaden your perspectives, allowing you to see the customers' problems better and propose alternative solutions if he rejects the initial proposal.

The last component of buoyancy is how you deal with rejection after it has happened (i.e., how you explain it to yourself). Research has shown that a salesperson who sees a rejection as something _temporary, specific and external_ (e.g., "Today was a bad day to sell because this merchant had a full stock already") will likely sell more than a salesperson who sees things as _permanent, pervasive and personal_ (e.g., "I'll never sell anything to anyone since I'm a horrible salesperson").

If your explanations are closer to the latter, try to consciously analyze and poke holes in them. How do you know the customer wasn't just having a bad day?

### 5. Clarity: Move others by helping them see their problems and solutions more clearly. 

Because many salespeople — as well as non-sales persuaders like teachers and doctors — have lost their value as providers of information, they must find a new way of moving people. One key way they can do this is by providing _clarity_ : helping people see their situations in a fresh light and from new angles.

One effective way to do this is to _find_ problems rather than _solve_ them. Sure, consumers today can buy whatever vacuum cleaner they need online, but what if they've got their problem wrong? What if the problem is actually that their carpet gathers dust? A salesperson who identifies this problem provides a valuable service.

The keys to finding the right problem are to ask questions of the customer and then help them sift through the masses of information available.

Once the problem is defined, the way you frame your offered solution can greatly impact how it is perceived. For instance, it is wise to evoke a comparison; research indicates we often understand things better when they are not seen in isolation but rather contrasted with something else.

To help customers make a comparison, consider giving them _fewer_ choices, not more. Studies show that limiting customers' options actually leads to increased sales, as the choice is made easier. You could also frame your offering as an experience rather than a product. Several studies have demonstrated that people derive greater pleasure from buying experiences than just material possessions.

Finally, when moving people, ensure you give them clear and detailed instructions on how to solve their problem. This will greatly increase the likelihood of them acting on it.

### 6. Modern pitches need to be short and engaging. 

In 1853, in front of an enthralled audience, Elisha Otis took an axe and severed the cable of the open elevator he was standing in, some three stories off the ground. The audience gasped in fright, but the elevator barely budged.

Otis had just proven that his new automatic safety break worked. Both his invention and his method of moving people with a simple, short and effective pitch proliferated rapidly.

Today's hectic information environment means that pitches must be even shorter and more engaging than in Otis's time, or no one will pay attention to them. Sometimes you have to get your pitch across in just the length of a Twitter message or the subject line of an email. The founder of advertising agency Saatchi & Saatchi even said pitches should be boiled down to just one word, like President Obama's 2012 campaign slogan: "Forward."

But with all the distractions around, in addition to being short your pitch needs to be engaging. Research has shown that the most successful pitches are the ones where the target becomes actively involved in developing the pitch. Therefore, in a sales situation, you should encourage the customer to contribute her ideas as well.

For instance, consider formulating your pitch as a question. Before Ronald Reagan defeated Jimmy Carter in the 1980 US presidential election, he asked voters to consider whether they were better off now than they were four years ago. Questions force people to come up with their own reasons for agreeing with you, making it easier to move them.

Another, almost simplistic trick is to use _rhyming_ pitches. Research indicates people subconsciously think rhyming statements are more accurate than non-rhyming ones. Hence, pitches that rhyme are more than fine!

### 7. Borrow techniques from improvisational theatre to use in your sales. 

Just like actors in a theater, salespeople used to rely on pre-prepared scripts: Each sales call was to follow a set course that had been determined to produce results. In some companies, even the body language of salespeople was scripted.

But today's dynamic and complex sales environment demands a more versatile approach than the mere robotic reading of sales scripts. Just as unscripted, improvisational theater — _improv_ — reinvented classical theater by focusing on open-mindedness and cultivating the interaction between actors, so can the same elements help in making a sale today.

One basic tenet of improv is listening for _offers_. Often, we're so focused on what we want to say next that we only half-listen to others. For example, surveys have shown that doctors usually interrupt their patients within the first _18 seconds_ of them speaking, before they've fully described their ailment. Improv, on the other hand, emphasizes that actors should listen to others, ready to work with what the other person offers.

Another key lesson is to always make your partner look good. Improv actors have long understood that if they help their colleagues on stage, the end result is far more impressive than if everyone were to focus only on him- or herself. This idea was popularized in the business sphere as "win-win." Moving people is not a zero-sum game, so try to find solutions that benefit both sides rather than just pushing your own agenda.

Finally, the last improv component is the power of "yes, and…." Answering your customers' ideas with these words and improvising further, rather than saying "no," or "yes, but…," creates an optimistic mood and allows you to incorporate multiple viewpoints, moving the conversation forward in a constructive atmosphere.

### 8. To successfully move people, make your efforts personal and purposeful. 

Today, moving people must be about something greater than a mere exchange of resources. To be successful, you must make your efforts _personal_ and _purposeful_.

Making it personal means focusing on being of service to the person you see rather than seeing them in abstract terms as customer X, student Y or patient Z. Research has shown that radiologists do a far better job of reading people's X-rays if they also see a photograph of the person in question. Seeing a human face reminds them that they are working for this person's benefit.

Similarly, a restaurant owner who puts his picture and cell phone number up on the wall and implores customers to call him with any complaints is making it personal for the customer, encouraging them to come back.

Making it purposeful, on the other hand, means finding the higher purpose in what you're doing and conveying it to those you're trying to move.

Consider a study examining how doctors in hospitals could be motivated to wash their hands to avoid spreading infectious diseases. One form of encouragement was tied to doctors' own health: "Hand Hygiene Prevents You from Catching Diseases," whereas the other was tied to protecting patients' health: "Hand Hygiene Prevents Patients from Catching Diseases." The study showed that the encouragement in line with the de facto _purpose_ of the hospital — curing patients — was much more effective and got doctors to wash their hands more frequently.

The desire to help people personally and the wish to make the world a better place are both noble human aspirations. Because both are also vital for moving people effectively today, one can say that, truly, to sell is human.

### 9. Final summary 

The key message in this book:

**Whether selling products, negotiating with colleagues, teaching children or pitching ideas, the art of moving people is becoming an ever-increasing part of our lives. To do this effectively, you must embrace the new ABCs of selling (Attunement, Buoyancy and Clarity), as well as master the tools of the trade: pitching, improvising and serving.**

The questions this book answered:

**What does the new landscape of sales look like?**

  * Selling — or at least moving others — is part of almost every job today.

  * Honesty and service are the new tenets of sales.

**What are the new ABCs of sales?**

  * Attunement: Understand others' perspectives to move them effectively.

  * Buoyancy: Overcome rejection before, during and after it happens.

  * Clarity: Move others by helping them see their problems and solutions more clearly.

**What techniques are essential for moving people today?**

  * Modern pitches need to be short and engaging.

  * Borrow techniques from improvisational theatre to use in your sales.

  * To successfully move people, make your efforts personal and purposeful.
---

### Daniel Pink

Daniel H. Pink is an American author whose previous bestsellers include _Drive_ and _A Whole New Mind_. He was named as one of the top 50 most influential management gurus by _Harvard Business Review_. His earlier books have sold over one million copies in the United States alone.

