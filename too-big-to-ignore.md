---
id: 54aa4dec6233320009a70000
slug: too-big-to-ignore-en
published_date: 2015-01-09T00:00:00.000+00:00
author: Phil Simon
title: Too Big to Ignore
subtitle: The Business Case for Big Data
main_color: 9DB53A
text_color: 849C22
---

# Too Big to Ignore

_The Business Case for Big Data_

**Phil Simon**

_Too Big to Ignore_ explains why Big Data is tremendously important for your business. It illustrates the ways that Big Data analysis can revolutionize your company, and takes you step by step through the tools you need to harness it. It also looks ahead to our Big Data future.

---
### 1. What’s in it for me? Learn what the big deal with Big Data is, and how you can wield its power for your business. 

What do you do when you hear people talking about Big Data? Sort of nod your head, but hope you don't have to say anything? Wonder if you should find out what it actually means? Or brush it off as "just another buzzword"?

Well, we should all be taking Big Data seriously. As Phil Simon argues in _Too Big To Ignore_, not only is Big Data too big, it's too important to overlook or shrug off.

These blinks show why Big Data is suddenly relevant, and how you can use it to your advantage. That's why this pack is a must-read. If you want to improve your business or organization, you need to make Big Data work for you.

After reading these blinks, you'll know

  * how you can steer your business toward Big Data;

  * how to visualize Big Data; and

  * how companies like Netflix used Big Data to fuel enormous growth.

### 2. Our changing consumption patterns and plummeting technology costs have led to the growth of “Big Data.” 

Everyone's talking about Big Data these days, and many companies hire data scientists. But what exactly is _Big Data_? And why is it spreading _now_?

"Big Data" refers to huge data sets that are too large or complex to be dealt with easily. They can't be processed using basic applications like Microsoft Excel or Access.

The growth of Big Data has been driven by our changing consumption patterns. Smart phones, cloud computing and broadband connections ensure that we're constantly consuming and generating data.

This new, fast and simple access to data ensures that we're always "on." Think about it — what's the first thing you do when your plane lands? Turn on your phone, right?

After that, you probably check your email, Facebook or Twitter. When you do that, you aren't just accessing data — you're creating it. This kind of consumer behavior created Big Data.

Our consumption patterns have changed because of plummeting technology costs. Technology — especially storage and bandwidth — has become extremely cheap.

This change really can't be understated. Modern, inexpensive technology enables and even _encourages_ us to contribute to Big Data. How many TV series would you stream if each episode cost $10 of bandwidth, on top of rental or purchasing fees?

In 1990, data storage cost $10,000 per GB. By 2010, that figure had fallen to ten cents.

Affordable technology and Big Data allow for 48 hours of video to be uploaded to YouTube every minute. It allows over 200 billion videos to be viewed each month. Without Big Data, such things wouldn't be possible. So what are we supposed to do with it?

### 3. Big Data can provide deeper understanding of your customers and business. 

So what makes Big Data any different from, say, data collected in the 1950s?

Well, it's not just that the quantity. The _kind_ of data we collect has changed as well.

Most Big Data is _unstructured_. Previously, data was _relational_, meaning it was much simpler and could easily be read on tables. Just think of a chart with customers in one column, and the products they've ordered in another.

Unstructured data, on the other hand, is very messy. It can't be organized the same way. Each tweet about your product is a piece of data, but you can't just put tweets into a table.

Why? Because tweets don't have just one _relation_ to another thing. Imagine if someone tweeted that they were considering your product. That tweet could contain a lot of data, like the person's age, hobbies, demographic, education, or more.

So you simply can't make a table with "Tweet #1" and all its characteristics in columns — it would be unmanageably huge. Now imagine trying to do that with thousands and thousands of tweets! It would be impossible.

This is what makes the data "unstructured." Unstructured data accounts for more than 80 percent of organizational data today.

If we can learn to manage unstructured data, we stand to gain great insights into consumer behavior. Unstructured data is actually a huge opportunity.

Netflix is a perfect example of this. They keep track of where, when and how many times their movies are watched. They also track which devices their users watch them on, and they monitor people's comments about the company on Facebook and Twitter.

In the summer of 2011, Netflix lost 800,000 customers. When they evaluated how their services were being discussed on social media, they realized a lot of customers had been driven away when their mail service, Qwikster _,_ rebranded and repriced their DVDs. So they got rid of Qwikster, and their business recovered.

> _"Big Data only brightens the flashlight, it does not eliminate darkness."_

### 4. Visualizing your data will allow you to analyze trends. 

So even if you organize your unstructured data, how do you evaluate it? How can you learn something from millions of pieces of information?

There are two main approaches here. The first, _time series analysis_, examines data over time.

It's easy to predict that sales will rise around Black Friday, for example, but a time series analysis can offer much deeper insight.

It can show you how your sales change in relation to the customers' payday dates — usually the 1st and 15th of each month. It also differentiates between long-term and seasonal trends. A time series analysis can also account for irregular sales fluctuations that exist outside trends — say if someone wins the lottery and lets their friends go on shopping binges.

When you can make sense of your data, you'll be able to ensure you don't build strategies around temporary fluctuations. You won't increase your inventory just because a lottery winner went on a one-time shopping spree in your store.

The second approach, _heat maps_, help you visualize a large amount of data very easily.

A heat map represents values with different colors, which can show you a lot more than traditional methods of displaying data. A table with 100 million entries certainly won't illuminate much, and graphs technically can reveal more, but they only show the relationship between two variables.

Heat maps, on the other hand, provide an overview of several variables at once. They can assess the amount, content and location of books sold, for instance. They're also much more intuitive to read.

You can detect trends by the intensity of colors in the heat map. Lots of red in one area might indicate that sales are high in a particular neighborhood in summer, for instance.

### 5. Use new, innovative platforms to manage your Big Data work – or outsource it. 

Here's some news that's both good and bad: with Big Data, you'll never use Excel or Access again. You'll have to use new platforms if you want to get the most out of Big Data, and the platforms have to be versatile.

_Hadoop_, for example, is a large collection of projects that handle data. It doesn't have one standard configuration. Instead, it's made up of several subprojects that are each quite complex.

The precise workings of Hadoop are very technical, but it's essentially a tool for breaking down Big Data tasks into smaller subtasks. Those subtasks are then processed individually, and put into new data sets. Facebook uses Hadoop to analyze huge swaths of user data.

If you don't want to spend the money on Big Data hardware and maintenance, you can also outsource it to a tech company. If you want, you can test the waters by letting another company run your Big Data calculations and see if they prove profitable. Kaggle, an online start-up, was developed for this very purpose.

Kaggle allows you to post Big Data tasks online, and find data scientists who can solve it. Even if you don't know what you want to do with your data, Kaggle users can suggest ways it can be used. Kaggle users were once given flight and weather data, and asked to predict runway and gate times for airplanes. They had to help the flight schedule adapt to changing conditions. The winner's analysis was 40 percent more accurate than industry standards.

Ultimately, you have to find your company's own best way of managing Big Data. Always ask yourself if what you're doing is best for you.

> _"Traditional database approaches don't scale or write data fast enough to keep up with the speed of creation."_

### 6. Make sure your organization is really ready for Big Data. 

Even if you feel ready to jump into working with Big Data, you still need to pause. You've got to make sure your company is ready, too.

Collecting and using Big Data will cost you, although some tools are free.

Hadoop is freely available, but you'll still need a considerable budget for consulting and training, to ensure that your Big Data investment is worthwhile.

Don't think of Big Data as a new program that just yields results when you install it. You have to restructure your approach to technology and data in general.

_Explorys_, a company that uses Big Data to improve health care, learned this when they started work. They had to introduce grids for data storage, and develop a platform that worked across different health care providers. They also had to build a new team of over 100 employees.

And even if you get great Big Data tools, remember that they can only help you if you've got good data. Even the best tools are useless if you aren't gathering useful information.

So start by asking specific questions, and outlining some short and long-term goals. You have to know precisely what information you want, or you won't be able to figure out where to get it.

Ask what consumer patterns make certain products successful, for instance, or what makes customers drop your brand. Then, collect as much data as you can on current and former customers.

Eventually, you'll be able to use this information to predict which products will sell well. You'll also know when customers are about to leave you, so you can try to win them back.

> _"[Big Data] is not a contest, and he who stores the most data doesn't win. [It] is just another means toward solving business problems."_

### 7. Big Data amplifies existing security and ethical issues. 

Big Data is not without its problems. If you think that storing a tremendous amount of information about people sounds like it has some drawbacks, you're not wrong.

Big Data raises privacy issues to new heights. If the wrong people get hold of certain kinds of Big Data, it could be disastrous.

Apple and Amazon, for instance, are both reported to have around 400 million customer credit cards on file. Think about what that could mean for hackers or other data pirates.

Even if we assume these _companies_ can be trusted with our data (which is not necessarily a sound assumption), there have been many cases of data theft. So companies have to protect their customers' data in addition to their own internal data.

In 2012, Google got into some hot water when it was revealed that their Street View software had been gathering data from open Wi-Fi networks. This is the downside of Big Data: it can threaten our privacy.

Major companies like Google, Amazon and Facebook can exploit user data endlessly if they choose. If this bothers you, you can consider using alternative services like DuckDuckGo — a search engine that doesn't save any user data.

> _"Yes, Big Data is cool, but it is also scary, it is concurrently beneficial and potentially malevolent."_

### 8. Big Data will make products “smarter.” 

So what's the future of Big Data? Some people overlook the effect it will have on the consumer market.

For one, we'll see a slow move from _active_ to _passive_ data.

Currently, most of our data is _actively_ created. That means we use the internet through our laptops or smartphones, and create our data ourselves.

But increasingly, data will be _passively_ generated. We'll have cars, TVs and other devices that will connect to the internet and track our behavior. These technologies will create data on their own.

This may seem like an invasion of privacy, of course. It also means, however, that our technology will adapt itself to our specific behaviors.

In the future, technology will be able to use data more effectively. Tony Fadell, the designer of the iPod, developed a program for his new firm, Nest, that's an example of this.

One of Nest's devices is a thermostat that collects user data to "learn" people's personal preferences and control their central heating accordingly.

You don't have to program Nest after it "learns" you — it uses your data to adjust the temperature. It might know, for instance, that you like your living room colder in the day and your bedroom warmer at night. The more you use it, the more it adapts to you.

This data, in turn, is collected and sent online. This means you can control your heating with your smartphone, and see records of your temperature preferences.

Clearly, Big Data is going to have a huge impact on our technology, environment and lives. Will you be a part of it?

### 9. Final summary 

The key message in this book:

**Big Data demands a new way of thinking, new tools and a fresh approach to data analysis in general. However, if you can manage your unstructured data, your company stands to benefit tremendously. Big Data will only play a bigger role in our future, so don't miss out!**

Actionable advice:

**Test the waters.**

Feeling iffy about jumping into Big Data? You don't have to! Ask a Big Data company like Kaggle to run some analysis for you. You'll get some insight, and find out if you're ready for it. Don't rush into things — take it slowly, and make sure you're prepared.

**Suggested further reading:** ** _Big Data_** **by Viktor Mayer-Schönberger and Kenneth Cukier**

_Big Data_ provides an insightful look at why a change to "big data" is a major shift in how we collect, use and think about the data around us. It provides great explanations and examples of how individuals and companies already ahead of the curve are using the tools of big data to create value and profit. Casting an eye forward, the book also outlines the future implications for a big-data society in terms of the risks, opportunities and legal implications.
---

### Phil Simon

Phil Simon is a technology consultant, and the author of _The Age of the Platform_. His work has been featured on NBC, CNBC, ABC _,_ in _BusinessWeek_ and _The Huffington Post_.

© [Phil Simon: Too Big to Ignore] copyright [2013], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

