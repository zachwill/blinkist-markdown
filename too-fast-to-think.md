---
id: 59c2e26eb238e10005c1fc69
slug: too-fast-to-think-en
published_date: 2017-09-21T12:00:00.000+00:00
author: Chris Lewis
title: Too Fast to Think
subtitle: How to Reclaim Your Creativity in a Hyper-connected Work Culture
main_color: FFDB33
text_color: A68E21
---

# Too Fast to Think

_How to Reclaim Your Creativity in a Hyper-connected Work Culture_

**Chris Lewis**

_Too Fast to Think_ (2016) serves as a handy reminder to reevaluate the way you use your mind in this rapid, ever-changing era. These blinks explain why social media has such a negative effect on people and how you can bolster your creativity by giving your brain a break.

---
### 1. What’s in it for me? Time to slow down and get creative. 

In the modern world, we move about at breakneck speed. Our food is fast, our communication is instant, and we work in a constant hurry.

Unfortunately, all of this rushing about is not good for us. It leaves us stressed and unhappy. Not only that, but it also destroys our creativity. So what can we do?

These blinks help explain how we can step off this treadmill. They lay out the benefits of giving ourselves and our brains time to be creative. By following this simple advice, we can start becoming happier and more innovative people.

In these blinks, you'll learn

  * why social media isn't good for us;

  * why universities are harming our creative abilities; and

  * why we need to find time for sleep.

### 2. Constantly checking your emails and social media accounts creates stress, especially for women. 

In the last 20 years, the line between people's working and social lives has blurred. The primary reason for this change is social media.

Frankly, it's transformed human behavior. Being connected has become increasingly important: how often do you interrupt what you're doing to check your news feed, emails and other means of communication?

In 2015, Adobe surveyed more than 400 US-based white collar workers aged 18 and older. More than half of the millennials who participated checked their personal email from work and vice versa.

Social media has changed how people think about the world. Thanks to algorithms and targeted news pieces, your feeds are populated with content catered to you. Individuals tend to know little about issues outside their own interests. This is why young people often know more about Kim Kardashian's life than the candidates in a presidential election.

Interestingly, women are more active on social media. Compared to men, they delegate more time to staying connected with online friends and following brands they support.

In January 2014, the Pew Research Center conducted a survey in conjunction with Women's Marketing, Inc, and SheSpeaks. The results showed that women are more likely to use social media to advertise their lives and buy products from brands they follow. In addition, the survey found that 46 percent of women look at their smartphone first thing in the morning while 31 percent check their computers.

This increased amount of online engagement leads to more stress. The Pew Research Center noticed that social media allows for a higher awareness of others' lives, which can promote stressful feelings. As women are naturally more susceptible to stress, social media has a greater effect on them. It's drastically changed the way that people act and communicate.

### 3. Higher education focuses on theory rather than teaching communication and creativity. 

As modern life is always rapidly changing, people need to adapt to thrive. Unfortunately, the tools needed to do so are not being taught.

When it comes to higher education, universities and colleges tend to teach the theory pertaining to a subject rather than how to approach it in a constructive, creative way. This practice of focusing on a narrow theory base during teaching means that graduates are often left without the ability to use their creativity in a practical way.

This idea is supported by the award-winning interior designer Vanessa Brady. She noticed that most design colleges neglect to teach a personal finance course because they adhere too closely to the rigid constraints of design theory. By refusing to teach the practicalities of business to students, the universities are essentially limiting the creative potential of their alumni.

Another aspect which universities tend to neglect is communication. As previously discussed, social media is changing how young people interact — these days, they tend to express themselves via texts and emojis.

Texting is even the preferred method to calling, as the latter is seen as too intrusive because it doesn't allow the person on the other end of the line to deal with the information in their own time. This lack of spoken or face-to-face communication grossly alters what people say to one another.

In fact, the internet has led to a phenomenon that psychologist John Suler dubs _toxic disinhibition_. This is the notion that because the recipient of the message isn't physically present, an individual is more likely to make statements online that they would be far too embarrassed or ashamed to say in real life.

In essence, the education system needs to rethink its teaching methods to properly equip young people with the skills they need to face the problems posed by contemporary living.

### 4. In order to reach your creative potential, let your brain rest. 

As you've learned, social media can have a detrimental effect on your happiness. But what can be done to combat this?

Nowadays, to achieve contentment, you must understand how your brain works. The brain has two hemispheres which tackle tasks. The _left brain_ deals with processes such as critical thinking, analysis and language, while the _right brain_ manages creative functions like recognizing faces, colors and reading emotions.

They tend to work together. When you meet a new person, the left brain analyzes aspects such as their gestures and how they're dressed. After ten minutes, if you feel an affinity toward them, your left brain will relax, and your right brain will kick in to allow for emotions like empathy and friendliness.

The way you use your brain can change how it functions, because the parts you use most often are strengthened. For example, say your goal is to enhance your creativity. To bolster it, do other right-brain activities like writing and dancing.

However, you have to be careful not to overload your brain, as this can have a negative impact on both your creativity and health. It's important to rest your brain to have a healthy creative mind.

As it's a passive activity, sleep is frequently overlooked when it comes to improving creativity and overall health. It's often seen as a waste of time, so many people go without it and push themselves to the limit until serious health problems arise.

Sleep restores the brain and improves its processes. People who capitalize on sleep tend to have triple the creative and cognitive power of those who don't.

Take former US president Bill Clinton. He worked so exhaustively that he ended up having a heart attack. In hindsight, he even realized that he made mistakes because of tiredness. It's vital to take care of your brain to boost your creativity.

### 5. Use the Eight Creative Traits to prevent the fear of failure from inhibiting your creativity. 

As life is so hectic these days, it's difficult to find the necessary space and time to allow your mind to wander and strike upon a creative idea.

Everyone's looking for the game-changing solution, but the fear of failure serves as a roadblock. The ideal conditions for creativity come when the left brain, which is responsible for processing worry and fear, is distracted by routine or habit. This allows the creative right brain the freedom to ruminate.

That's why your best ideas are created when you're doing menial activities like walking your dog or having a shower. However, with the right tactics, you can train your fear-inducing left brain to switch off.

In order to harness your creativity in the face of fast-pace modern life, use these _Eight Creative Traits_ : _quiet_, _engage_, _dream_, _relax_, _release_, _repeat_, _play_ and _teach_.

Sometimes, doing nothing is the perfect way for new ideas to come to light. Try to have a quiet moment — sit alone without your devices and allow yourself to daydream to boost your imagination. Alternatively, adding some basic activities to your schedule also helps — have a relaxing bath or cook a meal at home.

Remember not to rush the process. It takes a lot longer than you think to truly engage with your creative side. When it comes to creative thinking itself, though, take a less serious approach. Play with your ideas to really engage your right brain. It's also essential to repeat your creative routine to get to grips with your new skills.

And finally, teach others about your passion. This will allow you to think more about your area of interest — who knows what ideas might come to mind as you do so!

> _"Creativity is the residue of time wasted." — Albert Einstein_

### 6. Flexibility and sensibility are two valuable assets for your creativity and success. 

Creativity and success are one and the same — they come from your sensibility and your capacity to tap into your instinct.

As soon as you learn to listen to your instinct, you'll be able to use it to benefit your life.

One person who can vouch for this practice is Sinclair Beecham. He co-founded and built the highly successful chain Prêt à Manger. Along with his partner Julian Metcalfe, Beecham made the humble sandwich a healthier and more accessible food option.

His idea was so successful because he instinctively thought it'd be best to focus on what makes people happy rather than how to make a profit. If you can understand what makes people happy, a good profit will follow. However, if you solely focus on making a profit, you'll inevitably waste time and money.

To consistently make people happy, Beecham chose to strictly follow his instinct. He discovered that after years of using his brain for analytical purposes, his best ideas would come as he was not working.

In fact, going out into the world and simply _being_ can be a better educational journey in terms of exercising your creativity than going to university. You should endeavor to experience as much of the world as you can — that way, you'll generate thoughts and ideas that'll reinforce your creativity later in life.

As the Global Creative Chair at Edelman, a communications market leader, Jackie Cooper's career is proof that success isn't dependent on having a university degree. After opting out of education, she didn't dive straight into a leadership position. Instead, she enjoyed life by having many different adventures. It was these experiences that shaped her into becoming the ideal fit for her current position.

The lesson to be learned here is that success and creativity can emerge from the most unexpected places.

### 7. Final summary 

The key message in this book:

**Social media is upping our stress levels, and higher education is not always the answer when it comes to fostering creativity. The world we live in operates at an exceedingly fast rate, so give your brain the respite it needs to reconnect with your creative side.**

Actionable Advice

**Read a book before you go to sleep.**

Next time you're in bed, turn off your electronic devices and reach for a book. This will allow your brain to relax before you close your eyes. As you sleep, your mind will be in a better state to restore itself, which will have positive effects on your creative process.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:The Productivity Project** ** __****by Chris Bailey**

_The Productivity Project_ (2016) is a practical guide to how to live a life that's both productive and meaningful. With easy-to-understand techniques and reliable advice, you'll find out how to work smarter and accomplish the work that really matters. Stop wasting time and procrastinating, and pick up some new tools to take control of your life!
---

### Chris Lewis

Christopher Lewis is the founder and CEO of LEWIS Advisory Board, one of the world's most influential marketing and communications agencies. He is also a published journalist and media trainer who's worked with senior politicians, celebrities and business people.

