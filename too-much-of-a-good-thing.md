---
id: 573483e9e236900003290232
slug: too-much-of-a-good-thing-en
published_date: 2016-05-20T00:00:00.000+00:00
author: Lee Goldman
title: Too Much of a Good Thing
subtitle: How Four Key Survival Traits Are Now Killing Us
main_color: F8323F
text_color: F8323F
---

# Too Much of a Good Thing

_How Four Key Survival Traits Are Now Killing Us_

**Lee Goldman**

The human body evolved to allow us to survive in a world very different from the one we inhabit today. These blinks explain why we're not suited to the modern world, and the health complications we're suffering as a result.

---
### 1. What’s in it for me? The struggles of the human body in a fast-changing modern world. 

Ever since the Industrial Revolution in the nineteenth century, our world has constantly been changing, and at a pace that gets faster every day. Yet, our bodies seem unwilling to adapt to the speed of modern life. In fact, they haven't changed much since the Stone Age.

Our "outdated" bodies are confused about the eating, drinking and working habits that a luxurious Western lifestyle involves today. They are also confused about the massive and constant information flow to which our TV screens and smartphones expose us.

In these blinks you will find out why our human bodies struggle to adapt to modern life, you will learn how this struggle results in chronic diseases and, finally, what we can do to counter them.

You'll also learn

  * why our ancestors ate nine pounds of meat on one day;

  * why, in earlier times, a small wound could mean death; and

  * why Japanese adults must have their waistline measured every year.

### 2. The human body isn’t suited to the modern world. 

Older people often struggle when first learning how to use a computer. It takes time to adapt to the digital world. In the same way, the human body is confused by the modern world.

The human body is very slow to adapt to changes in the environment. Our ancestors were all dark-skinned and lived in Africa until only about 60,000 years ago. Because we need sunlight to stimulate our vitamin D production, the humans who migrated to less sunny areas suffered from deficiencies.

Vitamin D is important for developing robust bones, so people who could take in more sunlight had a better chance of survival. So, over thousands of generations, random genetic mutations that resulted in lighter skin gave people an advantage, and were passed on to new generations.

After thousands of years, our ancestors' bodies gradually adapted to the less sunny environments and the people in them developed lighter skin.

However, since the industrial revolution, our environment has been changing at a much faster rate than we can keep up with.

The Industrial Revolution has changed a lot about the way we live. Whereas food used to be scarce, we now have an overabundance of it. Many more people have lives based around intellectual pursuits rather than physical work, and our societies are much safer and less violent. As a result, our bodies are pretty confused.

> _"To invert a well-known aphorism, we've won the war of human survival, but we're losing a battle with adaptation."_

### 3. Human bodies have adapted to food scarcity, but we now have food overabundance. 

We can't live without the right amount of food. These days it's easy to go to the supermarket and get a frozen pizza for dinner, but 10,000 years ago food worked pretty differently.

Food wasn't always available back then, so it was advantageous to stock up on fat when it was available. Early pioneers who visited Native American tribes wrote that they would eat up to nine pounds of meat — about 12,000 calories — in just one day if there was a lot of food available.

People had to eat whenever they could because food was scarce. Their bodies had to create energy reserves, so they stored fat in their hips, bellies and thighs.

Fat reserves were a big survival advantage, especially in colder environments because they also helped protect the body from freezing.

These days, we have an overabundance of food and we're piling on too much fat. We don't need fat to keep us warm anymore either, as we've developed effective clothing and housing.

Our metabolism and genetic makeup also make it difficult to lose weight. When we do lose weight, the number of calories we need also declines. So for every percent of body mass that we lose, we need 20 fewer calories per day. We can only lose weight by consistently eating less for the rest of our lives, and not just for a few weeks. 

In 2012, Dr. Priya Sumithran found that when a person loses weight, their body releases hormones that make them want to eat more, no matter what size they are. That was useful when food was scarce, but today it's contributing to the obesity epidemic.

> _"All the remarkable traits that helped our ancestors survive recurrent food shortages are simply out of sync with modern supermarkets, fast-food restaurants, and unlimited snacking options."_

### 4. We were once threatened by having too little water and sodium, but too much is also dangerous. 

People from arid climates are often confused when they visit northern countries and see people jogging in the streets. That's a healthy habit in some environments but it's deadly in places where fresh water is hard to access.

From the hunter-gatherer period to the Middle Ages, extreme exertion could lead to death by dehydration because there wasn't enough clean water and sodium available.

In 490 BC, for example, the messenger Pheidippides ran from Marathon to Athens to report that the Greeks had been victorious against the Persian invaders. He ran through arid territory without enough water and lost up to two liters of sweat every hour.

His body eventually stopped sweating to conserve water and sodium, so his temperature started to rise because he couldn't cool off. According to legend, Pheidippides died of heatstroke right after he delivered the news, due to his excessive dehydration.

Our bodies have developed mechanisms to conserve water and sodium like this, but these mechanisms are dangerous now. Today, we have too much sodium and high blood pressure as a result.

When you lose water through sweat, urine or diarrhea, you lose sodium too. That, in turn, reduces your overall level of blood, causing low blood pressure. The reverse is also true: when you take in too much sodium and water, you end up with high blood pressure.

Our ancestors were always potentially threatened by dehydration, so they evolved genes to produce more hormones that helped maintain high levels of sodium in the blood. These genes pose a threat to us today because of the high amounts of sodium we consume. In the United States, 15 percent of all deaths are caused by high blood pressure.

> _"If we could even get close to the low salt habits of the . . . hunter-gatherer societies, we might be able to avoid high blood pressure altogether."_

### 5. Our tendency to panic once helped us survive but it’s now causing anxiety and depression. 

A lot of people overreact to relatively small, everyday problems as if they are serious threats. That's another holdover from our past.

In prehistoric times, people really were constantly in danger. They were threatened by dangerous predators and violence was prevalent in early human societies, leading to a lot of deaths. This is still true in some communities, such as the Aché hunter-gatherer tribe in Paraguay. There is so much strife between different Aché factions that more people die in battle than from illness.

Because our ancestors faced so many threats, it was advantageous for them to react as quickly as possible. People who were more easily frightened had a better chance of surviving and passing on their genes.

Injuries came at a high price. In 2004, Randolph Nesse, a psychiatrist, calculated that a human typically spent about 200 calories fleeing from a predator. If they got injured, they'd lose over 20,000 calories the following week because they wouldn't be able to hunt. You had a much better chance of surviving if you were frightened and ran away quickly.

Today, this tendency to panic causes anxiety and depression. We're no longer threatened by dangerous predators but we're still affected by smaller problems. We react to them as if they were much more threatening, however, which is part of the reason that depression and anxiety are so prevalent now.

Our stress hormone is triggered by the high speed of our modern lives. We have endless work deadlines and we're bombarded with upsetting news 24/7. It tricks us into thinking that we're always under threat, so we live in a state of constant panic.

### 6. Blood-clotting mechanisms that once protected us from bleeding to death are now causing heart attacks. 

When you were a kid, did you ever scrape your knee and watch in fascination as your blood clotted around the graze? Our bodies evolved to do this so we wouldn't lose too much blood from wounds.

Until the advent of modern medicine, severe wounds typically resulted in death as there was no way to artificially stop blood loss. Childbirth and injuries incurred from hunting or attacks caused many more deaths than they do today. This meant that people whose blood clotted quickly had a better chance of survival.

Thanks to natural selection, rapidly clotting blood soon became a widespread genetic trait. Our bodies now have a high concentration of clotting agents called _platelets,_ which move to wounds and attach to them like magnets, sealing the wound. Platelets also send out chemical signals that alert other platelets to the emergency. There are about 15 million platelets in one drop of blood.

Our eager-to-clot blood now poses a threat when combined with an unhealthy diet. If you eat too much unhealthy fat like saturated fats and artificial trans fat, cholesterol deposits build on the walls of your arteries. Those deposits rip easily, causing the lining of your arteries to tear.

Tears in your arteries attract platelets, which then try to mend the wound. When a clot builds on top of a layer of cholesterol in an artery, it can block blood flow and cause a heart attack.

Our outdated genes are clearly causing some big problems for us. So will our bodies adapt to our new environment? Find out in the following blinks.

### 7. Our genes are unlikely to evolve fast enough to challenge modern chronic diseases on their own. 

Imagine if there was a gene that made you immune to chronic diseases like heart conditions, cancer or diabetes. Genetic engineers may dream of producing such a gene, but natural selection is unlikely to weed those chronic diseases out for us.

In natural selection, genes are gradually weeded out when they hamper someone's ability to survive and reproduce. However, modern chronic diseases like obesity and diabetes usually don't affect the number of children a person has.

Obesity and diabetes _have_ been shown to decrease fertility but we now have new technology like in vitro fertilization, so people can still reproduce if they choose. These genes are disadvantageous to our survival, but that doesn't mean they won't get passed on.

It's also unlikely that a random, advantageous gene mutation will spread throughout the entire global population. Immigrants have the potential to introduce new genes into various gene pools and replace less advantageous genes, but the human population is now so vast and dispersed that it would be virtually impossible for a new gene to spread to everyone.

We also can't adapt to our environment quickly enough to pass any serious advantages on to our children. Our behavior does have an impact on the next generation, however. Children who spend too much time indoors and don't get enough daylight are more likely to suffer from nearsightedness, for example, which they can then pass on.

In theory, the reverse could also happen. It's possible that our kidneys could become better at eliminating sodium from our blood and we could pass that ability on to our children.

However, no studies have thus far been able to confirm that this will happen. For now, it's best to assume that our bodies won't naturally adapt to the big changes in our lifestyle.

> _"If we're going to blunt or reverse the traits that make us eat more than we need, crave salt, get too anxious and depressed, and clot too much, we can't count on our bodies to do it for us naturally."_

### 8. Collective action is more powerful than individual willpower when it comes to changing dietary habits. 

Oprah Winfrey famously lost 67 pounds in 1988. Her weight has fluctuated since then but she was back around the 200-pound mark in 2008. Oprah is one of the most formidable and hard-working people in the entertainment industry, so why wasn't she able to control her weight?

The most likely answer is that Oprah's weight is about more than just her willpower. Willpower alone won't fundamentally change dietary habits. Even very determined people, like Oprah and former US president William Howard Taft, struggle to overcome their weight problems. Taft weighed 354 pounds when he became president in 1909, despite his many prior efforts to lose weight. Determination alone isn't enough when it comes to your health.

Collective action is much more powerful. Governments need to step up to the plate.

The UK parliament, for example, successfully convinced companies to reduce the amount of salt in processed supermarket food by 20 to 30 percent. Finland used health campaigns and compulsory salt content limits to reduce sodium consumption by 25 percent, which resulted in a decrease in high-blood-pressure-related complications.

Cultural norms and traditions can even help fight obesity, as is the case in Japan. Japan's obesity rate is 5 percent — one of the lowest in the world. In 2008, the Japanese government passed a law requiring all adults aged 40 to 70 to have their waistline measured every year. If it exceeds 33.5 inches for men or 35.4 inches for women, they have to undergo dietary counselling.

### 9. Pharmaceutical companies are backing high-tech medical research. 

The modern health situation isn't all bad news, however! Many people are surviving serious diseases despite the odds. Bill Clinton, for example, lived through two heart attacks thanks to surgery and good medication.

Pharmaceutical companies have come up with a number of pioneering solutions to medical problems. In the film _Dallas Buyers Club_, for example, pharma companies are criticized for failing to provide adequate treatment in the AIDS epidemic of the 1980s but since then, they've discovered an HIV-resistant gene mutation in about 1 percent of the human population.

A man named Timothy Ray Brown was dying of both AIDS and leukemia in 2007 when his doctors in Berlin found a bone marrow donor with HIV-resistant genes. With a double bone marrow transplant, they were able not only to cure his leukemia but also to make him immune to HIV. These treatments were possible thanks to research conducted by pharmaceutical companies.

In high-tech research centers and gene-laboratories, scientists are studying methods for targeting specific cells. Dr. Susumu Tonegawa and his team have conducted experiments on mice and found that anxiety and depression-inducing memories can be erased with a biological technique called _optogenetics_, whereby light is used to control cells.

First, they repeatedly shocked male mice in a specific room to make them develop a fear of that location. Then they moved them to a different room with female mice, so that they came to feel happy again. This activated different brain cells.

Next, they returned the mice to the room that frightened them. This time, however, the scientists reactivated their happy brain cells with a laser beam, which made them feel calm again. The shocks erased their anxiety.

### 10. Final summary 

The key message in this book:

**The human body evolved to its current state through thousands of generations of natural selection. But many of the genes that helped us survive in the past are now a detriment to our health. We're programmed to eat whenever food is available, store excess fat, get frightened easily and have rapidly clotting blood. Medical research on genetics may be the best answer to these problems as it could allow us to reprogram our genes for our current environment.**

Actionable advice

**Imagine that you're a hunter-gatherer.**

Eat foods you'd find in the wild: vegetables, fruit, lean meat and nuts. That's what we've evolved to eat. Be sure to get a lot of physical activity in too.

**Suggested further reading:** ** _Salt Sugar Fat_** **by Michael Moss**

_Salt_ _Sugar_ _Fat_ examines the rise of the processed-food industry in America and globally, and why it has been fueled by the liberal use of salt, sugar and fat. These three ingredients are near irresistible to us humans, but their overuse also comes with devastating health effects.
---

### Lee Goldman

Famed cardiologist and public health specialist Lee Goldman is a professor at the Columbia University Medical Center. He is best known for having developed the Goldman Criteria, a tool for determining which cardiac patients should get priority treatment.

