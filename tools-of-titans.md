---
id: 58a999008e98580004e5560d
slug: tools-of-titans-en
published_date: 2017-02-23T00:00:00.000+00:00
author: Tim Ferriss
title: Tools of Titans
subtitle: The Tactics, Routines and Habits of Billionaires, Icons and World-Class Performers
main_color: FFAA33
text_color: CC8829
---

# Tools of Titans

_The Tactics, Routines and Habits of Billionaires, Icons and World-Class Performers_

**Tim Ferriss**

_Tools of Titans_ (2016) details the stories, strategies and successes of some of the most inspirational achievers, thinkers and doers of modern times. These blinks will teach you how to strengthen your body and your mind, all while building your creative business.

_"There's a reason why Tim Ferriss has become such an influential voice when it comes to achieving top performance. He always manages to get the best advice out of such fascinating, impressive people. Even Seth Rogan is in here!"_ — Ben S. Head of Salad at Blinkist

---
### 1. What’s in it for me? Get tips on health, wealth and wisdom from some tip-top people. 

Imagine you could talk, one-on-one, with some of the most successful people in their respective fields. What would you get out of it? Well, if you asked the right questions, you'd probably gain a ton of insight. And that's exactly what Tim Ferriss got from talking to a broad range of experts on his podcast, _The Tim Ferriss Show_.

In these blinks, we'll lay out the advice these people gave about health, wealth and wisdom. Extreme athletes, cartoonists, entrepreneurs, comedians — all these professions are represented, and each has some knowledge that can be applied to almost anything.

In these blinks, you'll find out

  * the benefits of showering in 30 to 60 seconds of ice-cold water;

  * that working a terrible job may be the best way to get a good one; and

  * how the sexless lives of Seth Rogen and Evan Goldberg made them into creative comedians.

### 2. Some of the best health tips come from extreme athletes. 

Have you heard of obstacle course racing? It's a new athletic pursuit in which competitors run a course full of different barriers, including walls, bodies of water, barbed wire and even fire.

Although the sport is relatively new, it already has some major stars — and Amelia Boone is one of the most successful.

She's already placed first in 30 obstacle races and, in 2012, participated in the World's Toughest Mudder competition, a 24-hour long race without any breaks. Boone covered the 90-mile course, successfully hurdling nearly 300 obstacles. As you can imagine, she has some great tips for how to recover from strenuous activities.

Before every race and training session, she blends beetroot powder with hydrolyzed gelatin, a flavorless nutrient derived from animal products. This simple protein drink regenerates her connective tissues.

Another of Amelia's tips is to regularly roll your feet over a golf ball. Doing so stimulates the hamstrings, which are one of the most important muscle groups for overall strength and posture.

Or consider another athlete, Wim Hof. His passion for enduring extreme cold even earned him a nickname: "The Iceman." He's broken over 20 world records in icy climates, including a 7500-meter climb up Mount Everest in 2007, wearing nothing but shorts and shoes.

To prepare himself for such incredible feats of endurance, Hof sits in an ice bath. But exposure to the cold is nothing new. It's long been used as a health-boosting measure. Just take surfing champion Laird Hamilton and the self-improvement guru Tony Robbins, both of whom work with cold exposure to increase immune function, promote weight loss and fight depression.

That being said, Hof takes exposure to cold to new levels. For instance, one time he swam across an ice-covered lake. During this stunt, his retinas literally froze. Luckily for him, the damage wasn't lasting.

Naturally, such extreme activities might not be the best place to start. So, for a gentler introduction, try finishing your daily shower with a 30- to 60-second blast of cold water.

> _Amelia goes to the sauna four times a week as she finds it improves her endurance._

### 3. There’s no formula for maintaining personal health. 

You might have noticed that people tend to gravitate toward simple answers to health questions. For instance, most of us think that, since high cholesterol is a bad thing, low cholesterol must be good.

However, things aren't quite that straightforward. Physician Justin Mager, a personal health coach to Olympic athletes, corporate executives and even health experts like the founder of the CrossFit center in San Francisco, Kelly Starrett, has been working to counter this type of thinking for years.

He has found that, in certain cases, high cholesterol can actually help people build lean body mass and defined muscles over a short period of time. In other words, health solutions aren't formulaic. While an office worker might need to abide by a low-cholesterol diet, an athlete might benefit from one that's cholesterol rich.

That's why determining what's healthy should be accomplished through experimentation, not strict rules. This is essential today since lots of modern health advice is on the ascetic end of the spectrum: no meat, no alcohol, no sugar.

James Fadiman, an expert in psychedelics, encourages a similarly experimental approach. He suggests just trying things out.

A good example comes from the author's friend, referred to by him as "Slim Berriss," who takes microdoses of psychedelic drugs. He has found that these drugs improve his well-being and empathy, and expand his consciousness.

This regimen includes two weekly microdoses of _ibogaine hydrochloride_, a psychoactive substance found in some flowering plants, including a perennial rainforest shrub called _Tabernanthe iboga_.

In addition to these weekly doses, he participates biannually in a two-day retreat during which he imbibes a higher dose of the _ayahuasca plant_, a psychoactive botanical used by the indigenous people of Amazonia. This plant induces a sort of spiritual enlightenment. People who've taken it say it makes plain the interconnections between everything in the universe.

Of course, it's hardly recommendable for anyone to take psychoactive drugs, especially without medical supervision. Rather, it's the non-formulaic mindset when it comes to well-being that may be useful to some. Now that we've looked at health, how about _wealth_?

### 4. Working a really tough job will give you the perspective and the drive you need to succeed. 

Most people believe that hard work will bring success. But there's also nothing like a hard job to make you _want_ to be successful.

Just take Chris Sacca, a successful investor in the San Francisco Bay Area whose parents drove home this important lesson when he was still young. For the Sacca children, summers always had two halves.

During the first part, they went to intern with a high-powered relative. For instance, at the age of twelve, Chris Sacca was teamed up with his godbrother, the father of whom was a lobbyist in Washington, DC. During that summer, Chris sat in on meetings with members of congress and learned how to convince them to endorse a bill.

Then, during the second half of the summer, the Sacca children were sent to work on a less prestigious job. One of Chris's jobs was to service septic tanks. He spent weeks cleaning off excrement, lugging filthy buckets and being bossed around.

This kind of experience is essential, because if you know that you want your life to be more like that of an executive than a septic-tank cleaner, you can start taking steps in the right direction.

Those initial steps are often half the battle — and they're usually not easy. So keep in mind the advice of the successful publisher Seth Godin. He believes that becoming successful requires keeping track of every positive thing that happens.

After all, people spend a huge quantity of time thinking about the things that didn't work, whether it's a promotion that didn't pan out, a relationship that went up in flames or even something trivial, like a forgotten umbrella on a rainy day. The problem here is that thinking so much about your failures will leave you depressed and defeated.

So a better idea is to focus on all the times you were successful, brave or helpful. Being able to see your positive impact will empower and encourage you to do ever greater things.

### 5. Success comes from working with long-term systems, rather than short-term goals. 

The comic strip _Dilbert_ tells the story of an innocuous office worker who is, in many ways, the perfect antihero. But that doesn't mean he's not a successful character. In fact, for Scott Adams, the comic's creator, antiheroes can still be successful — because success isn't about achieving goals; it's about forming systems.

So what does it mean to focus on systems?

Well, one thing it means is working on projects that seem to be doomed to failure — but only to those who can't see the bigger picture. For instance, if you have a passion for photography you might start a photographic project that studies rotting leaves.

There's a chance that your photos won't be successful; they probably won't end up in an exhibition, a gallery opening or on the walls of a wealthy collector's home. From the outside, such a result would appear to be a failure. But, in reality, you're building a new skill that will be incredibly useful down the line when you're, say, asked to take photos for someone's website.

The point is that people tend to focus on short-term goals, like creating and marketing a successful new product. If such goals can't be attained within a given time frame, people tend to give up, decide that they're not cut out to be entrepreneurs and go back to their mundane office jobs.

This is a huge mistake. Instead, focus on a skill or relationship that you want to develop over time and forget any specific goals. Just take Scott, the creator of _Dilbert_. He started blogging even though it meant lots of extra work with little to show for it in added income.

He knew it was worthwhile nonetheless because he was working within a system. The goal of his blogging wasn't clear, but, by practicing, he went from being an aspiring writer to a skilled one with an online presence. The system was bound to pay off — and it did. Scott became a best-selling author, but not until many years later.

> _"Losers have goals. Winners have systems."_ \- Scott Adams

### 6. Ignoring peer pressure and taking big risks are both key to success. 

Have you ever teased a friend just because everyone else was doing it? Well, if you tend to fall into such traps, you might not have what it takes to become wealthy and successful.

In fact, to become a success, you need to develop an immunity to peer pressure. Just take the Olympic snowboarding champion Shaun White. When he was 15, his parents paid for him to travel to Japan to compete in an international snowboarding event. (The other competitors were older and had all their expenses covered by sponsors.)

The day before the event, all the other athletes went out partying. On the day of the competition, they were so hungover that they decided not to take the event seriously; instead, they would do a show run and split the prize money between all the participants.

Or at least that's what would have happened if White hadn't had another plan. Despite the jeering of the older athletes, he refused to accept the deal. In the end, he won the competition, along with the $50,000 prize, and today is one of the most marketable athletes in America.

White's bold move is also a reminder that entrepreneurial success relies on risk-taking. Just ask Peter Diamandis, the founder of the XPRIZE foundation, an award that motivates people to design innovative solutions for space exploration.

He decided that the prize should be at least $10 million and, with the help of his friends, managed to raise half that amount, but couldn't secure the rest. When faced with such a situation, most people would give up. Diamandis, however, took a risk. In May, 1996, he invested the money by organizing a massive fundraising event.

And his gamble paid off. Sponsors were reassured by the huge event and donations began pouring in. Because of this risk, the money was available when the prize was awarded for the first time in 2004.

Now you've got a sense of how to bring in some money. But what good is wealth without wisdom? That's what we'll tackle next.

### 7. Seth Rogen and Evan Goldberg are comic geniuses with recipes for creativity. 

If you think wisdom is all about spending hours meditating and being perfectly respectful, then you aren't familiar with Seth Rogen and Evan Goldberg. These two guys exemplify the human wisdom to be gained through humor and naughty behavior. Despite the puritanical underpinnings of American society, Rogen and Goldberg have managed to create films that address sexuality with irreverence and hilarity.

Their work on the subject began in 2007 with the movie _Superbad_. This comedy follows two awkward teenagers who are so desperate to lose their virginity that they have little brainpower left to devote to anything else.

The latest film from the duo, entitled _Sausage Party_, came out in 2016. It features animated supermarket foods like hot dogs, tacos and tubes of mayonnaise that embark on a comic adventure, spiced with sexual innuendo.

So what do Rogen and Goldberg have to share about their experience?

Well, they have three basic recipes for life and creativity:

The first is to work with what you know. For them, as young artists, this meant dealing with their overwhelming desire to have more sex. The compulsion to fulfill this desire inspired _Superbad_, their first film success.

The second strategy is from their mentor, Judd Apatow. He told them to work against the male stereotype of non-emotionality. In fact, the most important thing in life and creativity is to use your emotions since doing so enables you to better communicate and share with others.

Filmmakers who follow this logic ensure that every character in a movie has at least one emotional wound to work with.

And finally, both Rogen and Goldberg firmly believe in the use of marijuana to get the creative juices flowing. They know so much about weed that they use different strains for different situations. For instance, when it comes to writing and being creative, they prefer a strain called Jack Herer.

### 8. Being creative requires living your life and embracing fear as well as discomfort. 

Have you ever felt a surge in your ability to do work right after a holiday? This experience is actually pretty common and it's not just a result of being well rested. It also happens because you're full of new, inspiring experiences.

So, in other words, to be creative, you need to have a life. Just take the comedian Whitney Cummings, who wrote the hit TV series _2 Broke Girls_. For years, she struggled with self-doubt, low self-esteem and procrastination. She was so plagued by these problems that she thought the only way she could be funny was by making fun of herself.

Eventually, she found that she needed more time for her personal life, which makes sense since, as a workaholic, she basically didn't have one. Before making this change, her comedy was stuck in a self-imitating loop. Now, however, both her and her comedy are enriched, bringing her jokes to a whole new level.

So living your life is key. But life isn't all joy and pleasure. That's why it's also essential to embrace fear and discomfort whenever possible.

To better understand this, consider the sociologist and public speaker Brené Brown. She teaches that the idea that some people are brave heroes and others cowards is absolutely false. In reality, most people are afraid and brave all at once. In fact, for her, what takes true courage is to feel and confront fear.

That means it's essential for people to look fear and discomfort in the eye when discussing topics like race, homophobia or climate change. These issues can't be resolved in a comfortable way; engaging with people with different perspectives and backgrounds is crucial.

For Brown, it was a huge personal achievement to show her vulnerability during a TED talk in which she spoke about her breakdown and subsequent spiritual awakening. This talk reached an audience of 31 million people, and for Brown, presenting it was by no means easy. So each and every day, ask yourself whether you've moved outside your comfort zone. Discomfort is a sure sign of growth.

### 9. Final summary 

The key message in this book:

**There's no single path to success and, while every person's journey looks different, you can still take inspiration and advice from those who came before you. Their success stories will expand your horizons, teach you how to put yourself on the right track and compel you to keep trying no matter what.**

Actionable advice:

**Don't be afraid of bad ideas.**

If you want to make a dream a reality and are looking for ways to do so, don't limit the ideas you generate. By trying to immediately weed out bad ideas, all you'll get is stuck. Instead, write down every idea that pops into your head, even the wildest and most implausible ones. When you read back through them, you'll probably find that you've created a few gems.

**What to read next:** ** _The 4-Hour Workweek_** **, by Tim Ferriss**

After that tour de force of advice from the world of successful entrepreneurs and world-class performers, let's go back to Tim Ferris's roots, and discover his _The 4-Hour Workweek_. In this self-improvement classic, Ferriss shows how, by adopting some basic rules of the New Rich, you can escape the drudgery of office work and find happiness in the here and now, while still earning enough money to thrive.

To learn how to get there — including why you should focus on doing the _right things_ rather than doing things right — get the blinks to _The 4-Hour Workweek_, by Tim Ferriss.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tim Ferriss

Tim Ferriss is a successful investor and advisor to technology start-ups. He was involved in the creation of companies like Uber, Facebook and Alibaba, to name just a few. He is the best-selling author of _The 4-Hour Workweek_ and the creator of a celebrated podcast series, _The Tim Ferriss Show_.

