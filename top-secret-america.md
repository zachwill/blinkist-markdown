---
id: 54198ab16439610008520000
slug: top-secret-america-en
published_date: 2014-09-19T00:00:00.000+00:00
author: Dana Priest and William M. Arkin
title: Top Secret America
subtitle: The Rise of the New American Security State
main_color: D56864
text_color: C73B36
---

# Top Secret America

_The Rise of the New American Security State_

**Dana Priest and William M. Arkin**

This book shows how the American security state expanded its powers after 9/11 by establishing Top Secret America — a group of agencies that operate largely in secrecy. Though Top Secret America was founded to protect Americans, it's created new dangers instead. Top Secret America has been a waste of resources and a threat to liberty, and it must be shut down as soon as possible.

---
### 1. What’s in it for me? Learn about the dangers of the new American security state. 

We've seen a great deal of debate about how to balance security and personal freedoms since 9/11 — and it's only increased in recent years. Following the attacks on the Twin Towers, the American government greatly increased the powers of its security agencies, ostensibly to protect people from future attacks. However, instead of benefiting the public, these security agencies drained money, abused their power, and began to erode people's personal freedoms. Top Secret America is alive and well, and it's harmful not only to American citizens, but to the United States' international image and relations. In these blinks, you'll learn about Top Secret America and the dangers it poses.

You'll also learn

  * why you can be arrested for driving too quickly near the White House;

  * why security agencies are so terribly disorganized;

  * what a "lone wolf" terrorist is, and why the hunt for them has been harmful;

  * why secrecy is no longer relevant in modern society; and

  * why shifting to a policy of openness would lead to greater safety and prosperity.

### 2. Secret sections of the American government are increasing their activity, with very little supervision. 

Do you trust your government? In recent years, an increasing number of Americans have begun questioning the power and actions of their politicians and civil servants.

This is largely because many government activities are now taking place in secret, rather than in the public sphere. The authors of this book call this hidden area of the government "Top Secret America."

Secret agencies, such as the _National Security Agency_ (NSA) and the _Joint Special Operations Command_ (JSOC) have grown immensely in recent years. This is supposedly a response to new threats to the nation, most notably from post-9/11 terrorism.

In an attempt to fight such threats, these secret agencies have lost a great deal of accountability and visibility in government. Citizens are unable to track what they get up to. Why is this?

Well, when the security sector began to grow, it grew without supervision. Many of the documents used by security agencies are classified as _top secret_, meaning that most outsiders can't access them.

Even those who _can_ access these documents find it hard to make sense of them. Top secret documents can only be read in secure reading rooms, where note-taking is forbidden. Moreover, readers aren't allowed to consult experts like lawyers or academics for help in understanding them.

Two _super-users_ — people who _are_ able to access the documents — told the authors that there simply isn't enough time in one human life to thoroughly go through all of them.

Clearly, there is little adequate supervision of what secret agencies are doing. For the most part, they are allowed to do whatever they please, and this has had serious consequences.

### 3. In the fight against terrorism, ordinary crimes were transformed into potential terrorist acts. 

Did you know you can be arrested for driving too quickly near the White House? Authorities view this act as a potential terrorist attack, rather than a mere speeding offense.

This sort of thinking is the result of authorities expanding their power to prevent further strikes by terrorists. After 9/11, the Patriot Act of October 2001 eroded the separation between _criminal cases_ and _intelligence investigations_.

Traditionally, if the government wanted to arrest someone suspected of a crime, they needed a proper degree of evidence, and they had to follow strict procedures. However, they aren't bound to such clear rules in an intelligence investigation. The blurring of the line between criminal cases and intelligence investigations thus weakened the rights of suspects.

For example, before 9/11, certain laws ensured that people could know what personal data the government collected on them. After 9/11, these laws were stripped, or abandoned altogether.

Authorities were terrified of missing potential terrorist threats, partly because many people believed that greater security could've prevented the original attacks. In fact, Mohammed Atta, a leader of the 9/11 plot, was stopped twice before the hijackings. An arrest warrant was even issued for him, but the officers who stopped him the second time weren't aware of it.

To avoid future situations like this, authorities started connecting many ordinary crimes with terrorism. For instance, in preparation for President Obama's inauguration, regular crimes in the capitol region were seen as having potential links to terrorism. During that time, crimes in the area were investigated by the FBI rather than the police, just to be safe.

When regular crimes became more associated with terrorism, ordinary people entered the surveillance lists of the FBI and other agencies. This allowed a culture of surveillance and control to spread.

### 4. Authorities use expensive surveillance technology on the public. 

If you live in a Western society, you've probably seen posters telling you to watch out for suspicious behavior. That's because security agencies need help from the public to keep them informed about potential dangers.

Security agencies rely on citizens remaining vigilant in the face of threats, and they spend a great deal of money on sophisticated technologies to ensure this.

For example, the city of Memphis paid a huge amount for license plate readers and secret cameras. But even with this sort of technology, authorities still need the public to notify them about potential danger. To encourage this, they have to make sure people are constantly reminded of the possibility of threats. The government thus plays up the danger through posters, press conferences and media reports on terrorism.

Authorities also began to introduce technology developed for warfare into the public sphere. There was an increase in biometric identification such as iris scanners or body-heat detectors, for example. These technologies were originally developed to locate and kill terrorists, but now they're being used on regular citizens.

This technology was implemented partly to connect different sets of data. Police officers in Memphis, for instance, feed all their information into a coordinating office called the Real Time Crime Center (RTCC). The RTCC allows the entire police force to view all available data in real time.

Analysts can use this to decide which areas need a greater police presence, and redistribute the police throughout the city. Police officers on the street can also use the RTCC to instantly access entire files about the owner of a car just by scanning their license plate.

This new culture of surveillance is about more than just an increase in control — it's also a big business, and it's grown enormously since 9/11.

### 5. The expansion of Top Secret America has provided immense possibilities for private companies. 

The recent expansion of government surveillance has created many lucrative jobs.

To get a job in an agency of Top Secret America, you first need top secret security clearance. That can be difficult, but if you manage to get one, it's a guaranteed ticket to prosperity. A person with top secret clearance will have a high salary and great job security, even in difficult economic times.

Private agencies also seek out clearances, and benefit greatly when they attain one. For example, after 9/11, nearly all security agencies sought a _Sensitive Compartment Information Facility_ (SCIF), which was their equivalent of a top secret clearance. Security agencies needed a SCIF to perform many secret activities, and thus earn more money.

The privatization of security services also became very profitable. After 9/11, many agencies in Top Secret America were privatized by the government to save costs.

So if the government needs a translator, for instance, it won't create a government post to fulfill the job. Instead, it'll go to one of the 56 private firms that offers translation services.

This has provided many new opportunities for private companies, but it hasn't been beneficial in the way the government hoped it would. A study in 2008 illustrated this. It showed that contractors made up 28 percent of the workforce of intelligence agencies, but cost the equivalent of _49 percent_ of their personnel budgets.

So although the government hoped to save money through this sort of outsourcing, it's actually ended up costing more.

Top Secret America isn't just about secrecy and control — it's also a new economy. Many people have founded their careers in this sector, but unfortunately, this has had some consequences.

### 6. The security of intelligence information still ultimately depends on human beings. 

Many people imagine that modern intelligence gathering involves supercomputers that sift through information, without any need for human involvement. Unfortunately, this isn't the case — humans remain at the heart of the security system.

This poses a problem for the government: every human involved in a security agency is a potential security risk. Therefore their role has to be as limited as possible.

To this end, authorities limit the amount of information that each person in the security system can see. For example, a person in one agency probably has very little access to the work of their colleague in another department.

This disorganization means that very few people have access to an overview of the entire system. Essentially, one hand doesn't know what the other is doing.

The rapid expansion of Top Secret America further complicates this issue. As more people are hired, the percentage of inexperienced staff members increases. Overall, this leaves us with a rather inefficient system.

Even those working for Top Secret America often suffer as a result. They often lose important parts of their private life, for instance.

Employees of Top Secret America are constantly monitored to prevent abuse of the system. Lie detectors are widely used, but they aren't always effective, so other machines that measure eyeball movements or voice stress have also been developed.

The authors of this book spoke to a woman named Jeanie Burns, who is a businesswoman married to an intelligence officer. She has no idea what her husband does specifically, and he's under immense pressure to remain silent about his work. When she told the authors her story in a bar, she was very afraid that counterintelligence services might be spying on the conversation.

Human beings like Jeanie Burns' husband are the core of Top Secret America, but they're also its Achilles' heel.

### 7. Top Secret America is characterized by redundancy and a lack of coordination. 

After the 9/11 attacks, the American government quickly began to pour massive amounts of money into its intelligence and security agencies.

Sometimes different agencies such as the Army or CIA were simultaneously given hefty resources to collect intelligence or provide security in the exact same areas. This obviously led to redundancy and wasted resources.

Unfortunately, any attempts to make things run more efficiently were undermined by the agencies themselves. 

Agencies often didn't allow their members to share information, fearing leaks or other dangers. For example, the CIA was so afraid of compromising its informants in Afghanistan and Iraq that it preferred to risk them being captured or killed, rather than share their names with even the highest military officers.

The disorganization of Top Secret America also created confusion as to who exactly had authority over everything. At planning conferences or meetings, the number of attendees grew as the agencies grew. This resulted in increasingly inefficient, bloated gatherings where nothing truly got decided. 

The lack of shared information between agencies made these meetings even more inefficient. Meetings were very difficult to conduct if each person only knew snippets of information.

In an attempt to address these problems, the post of _Director of National Intelligence_ (DNI) was created. The DNI coordinates all agencies and departments that make up the intelligence community. He or she also acts as the principal adviser to the president.

However, the DNI still can't solve all the intelligence problems created by disorganization. The DNI can track the security agencies, but they lack the authority to make them act in a coordinated manner. Overall, there are still a great deal of problems in Top Secret America.

### 8. Top Secret America has to constantly change its strategies, which has created many new problems. 

The threats facing the United States change all the time. There are long-term threats from groups like al-Qaeda, but also new dangers both at home and abroad.

One strategy alone couldn't possibly manage all of these threats, so the government must constantly weigh which threats should be tackled at which times.

For example, in recent years, the Obama administration has shifted away from focusing on _sleepers_, to _lone wolves_. Sleepers are groups of foreign terrorists who've supposedly infiltrated American society, and are waiting for a sign to attack. Lone wolves are domestic terrorists who operate alone, outside any cells.

Lone wolves are difficult to spot. How can you tell an ordinary citizen from one who might commit terrorism? In an attempt to sort the sheep from the wolves, the government has increased security measures that target the public, such as license plate detectors.

However, it's not easy to decide which strategy to follow. Security agencies must constantly weigh various costs and benefits against each other. Unfortunately, this decision-making process isn't conducted in public. This means that unethical and potentially dangerous intelligence activities can go on uncontested.

Concentrating on lone wolves, for instance, has led to the expansion of domestic security services. As a result, the government has more power over American citizens. Previously, only people who worked directly for the federal government could get security clearance to spy on citizens, but now investigators at state level can do so as well.

There has been very little debate about this increase in domestic security measures, and little chance for people to even learn about it.

The intelligence community's new focus on lone wolves has been wracked with redundancy and inefficiency. However, the fact that lone wolves are so difficult to find is often used to justify this focus.

### 9. Security measures must always be increased, because threats can never be fully eliminated. 

Most people think of the Cold War as a very scary and dangerous time. To some extent, this was true, as there was always the threat of nuclear war. But at least people knew where that threat came from, and what form it was likely to take.

When you know what threat you face, it's much easier to develop effective counter threats. In the Cold War, the counter threat was _Mutually Assured Destruction_ (MAD) — if they blew you up, you'd blow them up.

The situation is quite different now. The threat of terrorism comes from many places, and it can take many forms. This is problematic for security.

The main problem is that security agencies can never be 100 percent sure there's no threat. There's always a chance that they might've missed something somewhere. Thus the government has to maintain the mindset of "you never know." Everything that could possibly pose a threat must be investigated.

This culture of perpetually increasing security measures has led to the misuse of authority and the abuse of liberty. After 9/11, several security agencies took actions that would've previously been considered unacceptable.

For example, in order to force insurgents to turn themselves in, some Joint Special Operations Command (JSOC) troops detained their mothers, wives and daughters. They treated them like hostages rather than suspects. Some interrogators only gave bread and water to them, and they took their clothes and didn't allow them to sleep. Their motto was "no blood, no foul."

The way that crimes are classified has also changed. Some security agencies rebranded certain civil crimes as "acts of war." That meant they could treat suspects more harshly, as they weren't bound to civil codes of conduct.

Top Secret America was developed through faulty logic and poor argumentation. It then spread rapidly, infiltrating more and more areas. There is an urgent need for change.

### 10. It’s impossible to keep information fully secret, so the United States should embrace openness instead. 

Secrecy and lack of accountability in Top Secret America has raised many questions about the loss of liberty. But there's also another question: is it even possible to keep such large amounts of information secret?

Security agencies are struggling to stay ahead of those who want to access their data banks, such as hackers, foreign security officials or terrorists.

Internal investigations have shown that most federal agencies have little understanding of how to protect their sensitive information. Employees are often unaware of where attacks and leaks might come from, or what they can do to stop them. The dramatic growth of Top Secret America has made this even more problematic, as information is now being collected at unprecedented rates. Security agencies are struggling to cope.

Because it's not possible to manage so much secret information, the American government should instead try to embrace openness. Foreign policy, for example, is an important area where this could be implemented.

If foreign policy relationships were based on openness, the authors believe that 99 percent of currently secret information wouldn't need to be kept secret at all.

An open approach would also strengthen people's belief in the security system, and allow it to work more effectively. The current security system is endangered by it's own actions in Top Secret America. Only a fundamental policy change can lead to greater world-wide trust and lasting safety from terrorism.

A change in security policy is definitely possible, and it could be immensely beneficial. If the American government moves toward being more open, it will also improve the image of the United States, both at home and abroad.

### 11. Top Secret America remains strong, and we need to tackle it. 

After 9/11, secrecy became the core of American security. But rather than increasing safety, Top Secret America has proved to be dangerous, and a threat to democracy, personal rights and freedom.

The concept of "Top Secret" doesn't work in our present society. Firstly, it's no longer possible for one secret to cause grave harm. This was true in the past, for example, if another country stole the procedure for making an atomic bomb.

Today's world is much more interconnected — states are now far more willing and able to help each other. Stolen Secrets in one state or area cannot be used anymore to bring down entire societies.

The desire to keep things top secret is also harmful to society. The fear and panic it incited led to things like the depiction of Muslims as cartoonish villains, which harmed foreign relations.

The reliance on secrecy also allowed officials to hide their own wrongdoings, and to chip away at democracy.

Unfortunately, things don't appear be changing at the moment. No serious threats actually appeared after 9/11, and the worst fears went unrealized. Despite this, the security system continued to grow.

The Obama administration did initially vow to change the security system — first by closing Guantanamo. Despite these promises, and the general safety of the country, the base remained open.

Instead, the Obama administration initiated even more leak investigations. They passed new laws, such as Cardin's Law, which made it a felony to disclose classified information to an unauthorized person. This had serious implications for journalists. If Cardin's Law had been passed earlier, the public might never have learned about Guantanamo or the Abu Ghraib prison abuse.

Top Secret America has endangered personal rights, and even democracy itself. It's imperative that we dismantle it, to ensure the liberty and safety of the United States and the world.

### 12. Final summary 

The key message in this book:

**National security is vital, but it can't be based on secrecy. The current security state is redundant, inefficient and a grave waste of resources. Though it's a big task to undertake, we must dismantle the powers of Top Secret America. Doing so will not only lead to greater freedom, but more permanent security as well.**

**Suggested** **further** **reading:** **_No_** **_Place_** **_to_** **_Hide_** **by Glenn Greenwald**

_No_ _Place_ _to_ _Hide_ details the surveillance activities of secret agencies as according to information leaked by American whistleblower Edward Snowden. Rather than serving as a means to avoid terrorist attacks, as the US National Security Agency (NSA) claims, Greenwald explains that these dubious activities instead seem to be a guise for both economic espionage and spying on the general public. _No_ _Place_ _to_ _Hide_ also brings to light the media's lack of freedom in detailing certain government and intelligence agency activities, and addresses the consequences whistleblowers face for revealing secret information.
---

### Dana Priest and William M. Arkin

Dana Priest is an investigative reporter for the _Washington Post,_ and the author of _The Mission: Waging War and Keeping Peace with America's Military._ She's received two Pulitzer Prizes for her work, and she writes extensively on national security. William M. Arkin is a political commentator and a journalist. He's also an expert on both American and international military activities.

