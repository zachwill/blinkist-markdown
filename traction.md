---
id: 54bd29183465370009330000
slug: traction-en
published_date: 2015-01-22T00:00:00.000+00:00
author: Gabriel Weinberg and Justin Mares
title: Traction
subtitle: A Start-Up Guide to Getting Customers
main_color: ACDBF5
text_color: 3A91C2
---

# Traction

_A Start-Up Guide to Getting Customers_

**Gabriel Weinberg and Justin Mares**

_Traction_ (2014) explains why the success of every start-up depends not only on its products, but on the customer base it builds. Weinberg and Mares present proven methods for gaining customers, and help you choose the best for each growth phase of your company. With a bit of _Traction_, you'll win — and develop — the audience your product deserves.

---
### 1. What’s in it for me? Get traction for your start-up. 

Picture a car stuck in deep mud, its wheels spinning and spinning, sinking deeper into the dirt. Now picture a tank arriving beside the car, its tracks easily gripping the ground, propelling the tank through the mire with no problem at all.

What's the difference between the tank and the car? _Traction_. It doesn't matter how good your product is, or how high you rev the engine: if it isn't equipped to tackle the tough conditions where new companies fight for a market share, it'll never lift itself out of the mire.

So is your start-up going to be a tank or a car?

These blinks explain the authors' key insights into the _traction channels_ that will become your start-up's caterpillar tracks, propelling you ahead of the crowd.

After reading these blinks, you'll learn

  * why having a good product won't guarantee your start-up's success;

  * how Google's early partnership strategy won it more users; and

  * how to use social media for traction without annoying people on Twitter.

### 2. Start thinking about traction early on by growing customer demand for your product. 

Countless start-ups launch every day, but only a fraction succeed. Why? The answer lies in _traction_ : the growth of customer demand for a product.

Traction is crucial because the defining metric for any start-up is its growth rate, and this growth can only be driven by a corresponding growth in consumer demand.

However, the amount of traction your company needs depends on the stage it's reached. Early on, even a handful of new customers count as traction, but by the time you have a customer base of 10,000, only increments of thousands will work.

Another key to understanding traction is that it isn't just about a good product; it also depends on solid marketing strategy. Many mistakenly think that a great product is all that's needed to attract customers. However, this is almost never true. Your product needs to be marketed to the right audience in the right way.

Nonetheless, what you're selling is still important. Split your time 50/50 between product development and traction. By devoting time to traction early on, you'll also get valuable feedback that will help you further develop your products.

For example, when Dropbox started out, they tested search engine marketing but quickly realized it was too expensive: gaining a single customer cost twice the amount they made from a subscription! Luckily, they had time to experiment with cheaper and more effective viral marketing strategies.

A final advantage of thinking about traction early on is that it builds useful experience and allows you to grow quickly once the product is ready. For example, marketing software company Marketo started blogging about their product long before it was fully developed. This led to a better product thanks to the feedback they received, but also resulted in a ready-made customer base of 14,000 buyers on the day they launched.

So how does your start-up build traction? Read on to learn about proven techniques.

> _"Traction is the best way to improve your chances of start-up success."_

### 3. Don’t discount traditional media – a great PR campaign can locate new users. 

How do you start building traction? The key is to create a _traction channel_, that is, a way to market or distribute your product.

In order to achieve this, consider both traditional public relations, like stories in newspapers, and unconventional PR, like flash mobs centered on your product.

Let's first examine traditional media. Think of it as a food chain with small blogs at the bottom. In order to get broad coverage, you don't call the big fish like CNN directly. Instead, approach smaller blogs and perhaps one of the bigger news sites will pick up on the story, resulting in lots of attention.

This technique worked for DonorsChoose.org, which allows teachers to raise money for things like gadgets for science classes. The company was first noticed by a few small, local media outlets, and the resulting coverage attracted the attention of _Newsweek_. And then, when someone on Oprah Winfrey's team saw the _Newsweek_ piece, the website appeared on Winfrey's annual Favorite Things list — which brought many new sponsors to DonorsChoose.

Another route to media coverage is unconventional PR. This could mean techniques like flash mobs or creative customer appreciation gestures. This can be a great traction channel because it never gets boring and can attract a lot of attention. However, make sure that your quirky idea isn't misunderstood. What makes one person laugh could offend someone else — or simply not attract very much attention.

On the other hand, if you're hoping to reach a broader demographic, think about traditional offline advertisements such as radio, TV and billboards. This can actually be done quite cheaply by advertising in local media or digging for _remnant advertising_ — unused ad space which can carry discounts of up to 90 percent. This is especially good in targeting an older audience that might not engage with technology as frequently. Unfortunately, however, the success rate of offline advertisements is difficult to track.

> Fact: On average, adults watch 26 hours of TV weekly.

### 4. Use social media to encourage traction and build a stable customer base. 

Social media isn't just for sharing pictures of what you ate for lunch; it can also get your start-up a lot of traction.

The first way to achieve traction through social media is via _viral marketing_ : encouraging your existing users to refer new ones. Once you start the ball rolling, it gathers its own momentum. It's all about encouraging your users to refer other users to you — and ultimately growing your customer base in an exponential way.

Viral marketing is cheap and fast, but it can also be difficult to pull off. There are two factors that make it successful.

First, you need as many people to recommend your product as possible. One way to achieve this is to give your existing users incentives. Dropbox, for example, offered users more storage space if they referred a friend.

Second, you need to maximize the number of referrals who go on to become users. This happens by making it as quick and easy as possible for referrals to buy or use your product. So make your sign-up process simple and fast.

In-house blogs are another avenue for growing traction. Not only do they allow you to communicate with and show your product to potential users, they are also great for soliciting feedback.

While company blogs keep devoted customers engaged, it can take a while to build up a reader base for them. So instead, consider sponsoring a third-party blog that your customers already like, in order to gain attention.

The goal is to build a community of users, which is ultimately how you achieve traction and ensure a stable customer base. Devoted users are more likely to refer others to your product and you don't have to worry about losing them to the competition. They're also a great way to generate product-development ideas.

So, in order to foster a strong community, give users opportunities to interact. Encourage friendly but honest communication via social media, blog comments, forums or even at parties and meet-ups!

### 5. Online advertising allows you to target potential customers and raise brand awareness. 

How can online advertising help you gain traction?

The first factors to consider are _Search Engine Marketing_ (SEM) and _Optimization_ (SEO).

Let's start with SEM: These are the ads displayed next to search engine results. So when someone does a related search, they see your ad and are informed about your product.

For example, if your company produces Danish backpacks, it'd be a good idea to have ads appear anytime someone searches for "Danish backpacks" on Google. You pay for each time a user clicks on your ad. One way to decrease costs is to target long, specific search phrases, as they will help narrow down the people clicking on your ads to only those who are truly interested.

SEO, on the other hand, involves pushing your website to the top of the results people get when they search for related words. This is important because data shows that only ten percent of all clicks happen beyond the tenth search result.

Take, for example, that Danish backpack manufacturer. It currently only appears on page three of the Google search results for "best backpacks." The manufacturer should engage in SEO by adding that crucial keyword "backpacks" to more parts of the site, or adding more precise search terms that highlight what's unique about their product, like "backpacks from Denmark," as this will improve its search engine rankings.

In addition to search engines, _social_ and _display ads_ also raise awareness.

Display ads, also known as banner ads, appear on websites and target all visitors to the site, in a similar fashion to a traditional print advertisement, although with added clickability and a younger audience reach.

Similarly, social ads appear on social networking sites like Facebook or Twitter. Since most people use these sites for pleasure, it's best practice to use social ads to start a conversation that guides attention toward your product rather than making a hard sell, as this would most likely just annoy users.

For example, eyeglasses maker Warby Parker encourages customers to try glasses on, take a photo wearing them and then post it on their social networks. This way the company doesn't pay for advertising but still gains attention while its customers have fun.

> Fact: Marketers spend around $100 million a day on Google's AdWords.

### 6. Partner with others to gain more traction. 

You wouldn't start a business without a support network, so why market without one? Partnering with other companies in a mutually beneficial way is called _business development_, and it's a clever way to build traction.

Google is a great example of a company that leveraged partnerships to become a worldwide success. In 1999, it partnered with Netscape and Yahoo! by becoming the default search engine for the former and improving the search results of the latter. It was a win-win situation, as Google enhanced these sites' search capabilities and itself gained many new users itself in the process.

Other collaborative ways to get traction include attending trade shows, speaking engagements and other offline events.

Trade shows are oriented toward the latest product innovations in an industry, but they also attract people who might help with your next step.

For example, bicycle brake manufacturer SlidePad Technologies began attending trade shows before their product was even ready, and landed a deal with major bike manufacturer Janis. While they didn't have a final product to show, they met Janis executives at a trade show and learned what they needed to do to form a partnership. SlidePad then met those requirements and eventually partnered with the bigger company — enabling them to get traction and reach thousands of customers!

Other offline events like conferences, meet-ups and parties are also a great way to find people and promote your product.

For example, Twitter promoted itself at the SXSW conference in 2007 by putting TVs showing live tweets in the conference hallways. By the end of the week, their traffic had jumped from 20,000 to over 60,000 tweets.

Another way to capitalize on these offline events is to take on speaking engagements. Not only does this improve your speaking and management skills, it also spreads awareness of your product.

### 7. Email marketing and traditional sales remain important ways to gain traction. 

Sometimes the most obvious methods are also the most effective.

Email, for example, is still a powerful traction channel for reaching customers, especially those who've expressed interest in your product. It's personal and gets you the undivided attention of prospective buyers.

However, it's important to differentiate your emails from unsolicited spam. Find legitimate ways of attaining email addresses, such as asking for them on your blog or at the end of a conference. That way, when you reach out with product improvements or other news, your customers will want to share it — further improving their connection with your company, and improving your traction!

Another avenue to consider is traditional _sales_ : approaching customers and proposing the direct exchange of products for money. This personal traction channel works especially well for expensive products. That's because personal contact builds trust, so potential customers are willing to spend more on your product.

The best way to approach sales is to get people you know to be your first customers. But if that's not possible, _cold calling_, or contacting unknown people who might be interested, will have to be done. In either scenario, the key to success is to thoroughly prepare what you want to say thoroughly.

And ultimately, the goal of any good sales strategy is that it is durable and works for future customers too. To that end, ask yourself: How do customers make purchasing decisions? What common questions do they ask? How do you address their concerns? And what are the most effective ways to influence their decisions during a pitch? These questions form the basis for a solid sales strategy.

As you now know, there are multiple traction channels. Read on to learn which one is best for you.

### 8. The most effective sales strategies involve trying multiple traction channels and being open to change. 

Each of these traction channels have proven effective for different companies, but which is the best for you? Well, it's important to keep an open mind — don't rule anything out in the very beginning.

Try the _Bullseye Framework,_ a five-step tool to find the best traction channel for you:

One: start by brainstorming all the different traction channels in your industry, and how you might use them with your particular product. Ask yourself questions like: Where would we get potential customers' email addresses? Which companies would we partner with and why?

Two: put all the channels into one of three groups: really promising ones, possible ones and long shots.

Three: choose the three top options and put them on your A-list.

Four: test these three options. Try to keep the tests cheap because you're just exploring. Consider these questions: How much will you spend in order to gain one customer with this channel? How many customers can you get this way? And are those the customers you want and need?

Five: focus on the channel that seems the most promising, or repeat the process if none of the channels seem to be working.

Remember also that the right channels for you may change as your business develops. Think of your business' lifecycle in three phases: product development, marketing and scaling. In principle the goal for every traction channel remains the same — increasing the customer base — but what this means in practice will change.

In the first phase you might be happy just to generate some interest in your product, but in phase two you'll need to generate thousands of customers, so this narrows your channel options.

So just as you'd review your business plan as your company grows, review your traction channels periodically too.

### 9. An effective traction strategy is pivotal for the success of your start-up. 

Traction will determine the success of your start-up. This is why you should take some time to develop a well-thought-out strategy. To do so, set clear and specific goals and frame your overall strategy around it.

For example, when DuckDuckGo wanted to become a serious player in the search engine market, the company determined its traction goals, including the number of people it wanted to be using their product as their primary search engine, and how many searches per month should be completed (in their case, 100 million).

You may also find traction sub-goals, such as number of Facebook shares, helpful indications of your progress.

An additional benefit of setting traction goals is that they help organize your start-up's limited resources. Start-ups always operate in a hectic environment where there are too many things to do and never enough time or money to do them. But tasks like product development, designing a company logo and building a team all serve the same ultimate purpose: gaining traction. This is why you should determine a _critical path_ of actions that will lead you toward your traction goals, and then focus intently on following that path.

For DuckDuckGo, the critical path to reaching 100 million searches a month included getting more press coverage and improving search speed. Having traction goals allowed them to focus on these steps and put other tasks like optimizing product features on the backburner.

But just as the best traction channel for your company can change over time, so can the critical path. This is why you need to be flexible and open-minded. Constantly review and improve your path, keeping top-notch traction as your main priority.

> _"Your traction goal dictates your company direction."_

### 10. Final summary 

The key message in this book:

**Even great products don't sell themselves. A start-up stands or falls on the customer base it can build. This is why you need to think about traction early and often, and build your company's goals around achieving that traction.**

Actionable advice:

**Attend a trade show: they're great for getting press and viral marketing — and for building partnerships.**

Do some research on which trade shows are popular in your industry and then attend a few. They are a great way to get media attention, build a customer base that can spread your message virally, and meet other businesses who might become future collaborators. Just make sure you do put on a good show that makes you stand out among all the other start-ups.

**Suggested** **further** **reading:** ** _Crossing the Chasm_** **by Geoffrey A. Moore**

_Crossing the Chasm_ examines the market dynamics faced by innovative new products, particularly the daunting chasm that lies between early to mainstream markets.

The book provides tangible advice on how to make this difficult transition and offers real-world examples of companies that have struggled in the chasm.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

### 1. What’s in it for me? Optimize your enterprise through a simple but effective management tool. 

How is your business performing? Are you satisfied with results? Perhaps you feel your success is being hampered by hundreds of issues, like employee bickering or too much bureaucracy. Or maybe you feel like you need to reinvent the wheel just to survive the next few months.

These blinks outline a step-by-step method to solving your business problems. The Entrepreneurial Operating System will help you weed out unnecessary processes in your organization and help you regain footing in your chosen market.

Based on lessons learned through practice and experience, this system shows you how focusing on just six elements can help get your company back on track. So get your leadership team on board and take the first steps toward revitalizing your company by reading these blinks.

In these blinks, you'll learn

  * why trying to please everyone is a bad idea;

  * why one is the magic number when it comes to responsibility; and

  * why a company needs "rocks" to thrive.

### 2. Guide your company toward success by defining clearly what it values and what it seeks to provide. 

When we reflect on Steve Jobs's tenure at Apple, it's clear his work inspired many people. After all, his goal was to build a company that didn't just create technology but improved humanity.

While the goal of the Entrepreneurial Operating System (EOS) isn't as lofty, its powers shouldn't be underestimated.

This entrepreneurial tool is like an abbreviated business plan, seeking answers to a series of questions to get your company on track. It has six steps.

The first is to define a vision for your company.

Your vision will guide every other process and decision in your organization, which means that it needs to be crystal clear. It will define your organization, where it goes and how it gets there.

Beyond that, your vision needs to be understood and shared by everyone in the organization. If it isn't, your employees won't move or act in concert.

Finding a vision begins by defining what you want to be like as an organization. To do so, have your leadership team identify two things: your _core values_ and your _core focus_.

Your core values are three to seven fundamental principles by which your company is guided.

For example, among the core values of American restaurant chain Zoup! Fresh Soup Company are a "can do" attitude and a passion for the company's brand. These values determine who the company hires as well as how it handles staff, clients and business partners.

Once you've defined your core values, you can move to your core focus by defining what your company seeks to provide. Or, to put it another way, which needs your company wants to satisfy.

Why is defining this focus important?

Let's look at laser printer service and supply company, Image One. The company faced problems when it expanded its business into computer networking.

Amid the turmoil, company executives reminded themselves of Image One's core focus: to simplify the printing environment of its clients.

So they decided to ditch the computer division and stick to their core focus. Ever since, the company has thrived, experiencing an annual average growth rate of 30 percent.

### 3. Plan a series of targets based on the months and years ahead to determine your marketing steps. 

Imagine you're planning an around-the-world getaway. You're probably thinking about which cities to visit, how to pack in smaller attractions and, of course, how to get around.

When developing a business plan, you need to consider similar details. Ensure that your leadership team plans carefully what will be accomplished, when it will happen and how.

To do so, brainstorm a _ten-year target_ with your team. This is your company's larger-than-life goal for where you want to be in ten years, crucial to defining your company's overall direction.

Next, suss out your _three-year picture_. This should describe which strategies you'll use over the next three years to approach your ten-year target. Once you've done that, you can make a _one-year plan_ that states what has to happen in the next 365 days.

And finally, you should define your _quarterly rocks_. These are specific, attainable and measurable milestones you can reach within the next 90 days.

After you've planned for the future, both near and far, you can decide on a _marketing strategy_.

The first step is to ensure efficiency in how you win loyal customers.

Remember, you can't make everyone happy. Trying to do so might just end up frustrating loyal clients. This is why you need to define a target market and adapt your strategy to it.

For instance, if you're the owner of a small aquatics center, it's not in your business interest to cater to both active children and stressed-out adults. The needs of the two customer bases are different, and you won't be able to satisfy them both simultaneously.

Having defined your target market, you should identify any _issues_ that might affect your business. In the case of the aquatics center, for example, these might be rising energy costs or a similar business opening nearby. Being aware of these will help you address them ahead of time.

> _"If you try to please everyone, you're going to lose your ass."_

### 4. Make sure you’re choosing employees who adhere to your values and fit their jobs appropriately. 

Every good manager knows you're only as strong as the people who work for you. This is why staff members are the second crucial component to organizational success.

So how can you make sure you're employing people who share your company's values?

Use the _people analyzer_! This chart allocates one column to each of your company's core values and a row to each employee. To populate your chart, record which employees adhere to which core values with a plus mark for "most of the time," a plus/minus for "sometimes" and a minus for "never."

Once you've collected this data, look for employees who exhibit most of your core values most of the time and all of your core values at least some of the time.

While an employee might be growth oriented — a possible core value — that person might not always adhere to another value important to your company's success, such as fairness.

Having the right people on your team is key, but you also need to make sure people are in the right roles.

The GWC (Get, Want, Capacity) chart helps you ensure all employees are doing jobs that are best suited to their skills.

To use this chart, you ask three questions. Does the employee _Get_ it — does she understand her role? Does she _Want_ it — does she like the job? And does she have the _Capacity_, whether intelligence, experience or any other quality, to perform the job effectively?

If your answer to any of these questions is "no," then you know the person isn't right for the job.

Let's say one of your most talented engineers is in line for a promotion to an executive position. He Gets it, knowing what the position entails; he has the Capacity to be a good executive, too. But there's a problem, as he dreads meetings — he doesn't Want it enough, and thus wouldn't make a good executive.

### 5. Use scorecards to monitor staff performance and resolve issues before they get out of hand. 

The third key to optimizing your business for success is good _data management_.

Many people greet this term with contempt, but managing data doesn't have to be a grueling task. In fact, keeping track of information crucial to your business is simple with a couple of easy-to-use tools.

You can monitor and manage employee performance with a _scorecard_.

Decide with your leadership team which performance factors — from five to 15 — to monitor and set a target goal for each, such as number of sales proposals given or new subscribers won.

Define then who is responsible for reaching a given weekly target and, when someone doesn't meet a goal, follow up to find out what happened.

By tracking employee performance, you'll be able to identify any issues early on. And by quantifying performance in this fashion, you can hold people accountable clearly and objectively and also trigger healthy competition.

Yet what do you do if your scorecard indicates that your team is failing to meet its targets or is facing another problem?

Here's where the fourth key to managerial success comes in. It's designed to help you identify important issues before they can harm your business. The name of the game is IDS, or _identify, discuss_ and _solve_.

Begin by listing everything that doesn't work the way it should, making sure your staff is comfortable reporting any concerns. Then, approach the issues using the three steps stated above.

First, _identify_ what's going on, bearing in mind that some issues might be symptoms of bigger problems.

Then _discuss,_ giving everyone on your team the chance to voice opinions about key issues and suggest ways to handle them.

And finally, _solve_ by deciding on solutions. Often you'll solve an issue by assigning a concrete task to a particular person. When they complete this task, the issue should ideally disappear.

> _"Problems are like mushrooms. When it's dark and rainy, they multiply. Under bright light, they diminish."_

### 6. Identify, review and streamline your company’s processes, making sure they align with your vision. 

Just like your vital organs power your body, _core processes_ keep your company running smoothly.

On average, companies have seven such areas, such as marketing, human relations and operations. These processes represent the fifth component of the Entrepreneurial Operating System.

For your company to thrive, you need to identify, document and streamline them all. Here's how.

The first step is to have your leadership team list all the processes that are core to your business. Once your leadership team understands how these processes function within the company, they can begin working on ways to simplify them.

Be sure that everyone on the team agrees with the list and uses the same names to refer to the different processes. Doing so will add clarity and simplify work moving forward.

Once you've agreed on a list, have the people in charge of each process document each important step. For instance, the head of human resources might write down the steps she takes when recruiting, orienting or terminating employees.

Next, you should review the process yourself. By taking a look at the steps team members have articulated, you'll likely identify some that are redundant or discover areas that can be simplified, such as with technology.

You might find that a department prints and files a paper copy of every company document, even though the same documents are saved and filed both in the cloud and on the company's intranet servers. In this case, printing just creates extra work and adds cost.

After you clarify redundancies, you can eliminate other unnecessary tasks, simplifying processes however you can.

From there, the remaining task is to ensure that your processes line up with your company's core values, the values you and your leadership team defined from the start.

If one of your core values is family orientation, for example, you'll want to not sell just family-friendly products but also provide family-friendly working conditions, such as offering paid family leave.

Now that your processes are under control, it's time to learn the final step of the Entrepreneurial Operating System.

### 7. Establish concrete quarterly goals and assign ownership to foster maximum accountability. 

Remember the concept of "quarterly rocks" from an earlier blink? These small milestones help you realize your company's greater goals.

But for these rocks to be effective and lead you toward success, you need constantly to set new goals.

To do so, meet quarterly with your leadership staff to review progress and set goals for the next quarter. Setting quarterly targets is effective, because 90 days is about as long as one person can remain focused on a single project.

At the meeting, have your leadership team list everything that needs to be accomplished during the next quarter. Teams should generate around ten to 20 goals on average.

Since this is far too many goals to realistically reach, the second step is to discuss ideas and whittle them down to seven top priority goals, or _rocks_.

Rocks differ from goals in general in that they're specific, attainable and measurable. For instance, one of your rocks might be to hire a new expert for human relations.

Once you've chosen your top goals, make sure people are accountable for them. Assign each rock to a person on your leadership team who will be in charge of realizing it. It's key that only one person be assigned to each rock; when multiple people are in charge, nobody feels responsible.

After all your rocks have been assigned, each member of your team should set a personal rock. This can happen on a "rock sheet," or diagram that lists company rocks and individual rocks.

You can use this list at weekly meetings to monitor progress. By making clear the responsibilities of each leader, you'll create maximum accountability.

Finally, share the company's rocks with every person in the organization and have each department generate rocks, too. Do this by inviting everyone to a quarterly meeting where you share new rocks and report on those from the previous quarter. 

From here you can have departments and employees decide on one to three individual rocks in line with those of the company at large.

### 8. Final summary 

The key message in this book:

**To gain traction in the market, you need to develop a clear and coherent plan for your business. But that's not all. It's also essential to have the right people in the right places, track data, streamline processes and account for any potential issues in your business.**

Actionable advice:

**Keep meetings focused by shouting, "tangent alert!"**

Meetings and discussions go on too long because participants go off on tangents, or stray from the issue at hand. To prevent this, invite meeting participants to say "tangent alert!" when they notice a conversation is drifting. This will help your team stay focused and accomplish the meeting's goals.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Scaling Up_** **by Verne Harnish**

You had the idea, you drafted the business plan, you raised the cash, you launched your new venture and you became a success. But now you need to grow. _Scaling Up_ (2014) reveals the most useful tools for doing just that. Use the Scaling Up system of checklists, levers and priorities to establish a strong company culture as your business expands through the right strategic and financial decisions.
---

### Gabriel Weinberg and Justin Mares

Gabriel Weinberg is the founder and CEO of privacy-oriented internet search engine DuckDuckGo. Justin Mares is a start-up founder with expertise in mobile health and 3D printing. Both have been directors at start-ups that sold for eight-figure sums.

