---
id: 56c062b14f33cc0007000035
slug: traffic-en
published_date: 2016-02-17T00:00:00.000+00:00
author: Tom Vanderbilt
title: Traffic
subtitle: Why We Drive the Way We Do (and What It Says About Us)
main_color: FFF533
text_color: 99931F
---

# Traffic

_Why We Drive the Way We Do (and What It Says About Us)_

**Tom Vanderbilt**

In _Traffic_ (2008), Tom Vanderbilt explores the chaos and order of our driving experiences. From the psychology of traffic jams to the paradoxes of driving safety, these blinks will reveal one aspect of the eternal battle between the limits of human nature and the technology that sets us free.

---
### 1. What’s in it for me? Find out why we drive the way we do. 

Isn't it funny how different people become when they get behind the wheel of a car? Mild-mannered introverts become violent and angry. Meek old grandmas become raging loudmouths. Why does getting into a car change us so much?

These blinks will help explain this baffling phenomenon. By looking into the psychology of driving, they will help you understand the background behind our driving quirks. They will also give you a few small hints on how you can avoid traffic jams and have a happier driving experience.

In these blinks, you'll discover

  * why changing lanes won't get you there quicker;

  * why zoning out (but not too much) helps us drive; and

  * why more roads will not alleviate traffic.

### 2. Traffic (usually) brings out the worst in us. 

How did you react the last time someone cut you off at an intersection? Did your usual, calm self transform into a psychotic demon, pounding the horn and shouting obscenities out the window?

It happens to the best of us. These emotional outbursts happen because human nature is not made to be trapped inside the small, mobile, metal boxes we call cars. Humans are fundamentally communicative beings, but the enclosed and separated spaces of automobiles prevent us from expressing ourselves properly. So when something bothers us, we become frustrated and aggressive instead of communicating about it.

But the modern technology of cars can't stop human nature, and we'll try anything to get our message across — no matter how absurd.

For example, one study examined how drivers respond to being honked at. Over 75 percent responded _verbally_, even though they were separated by steel and glass!

And drivers often try to send messages that won't improve the situation whatsoever. Like when a driver dangerously overtakes another car, and we try to show them how wrong they are by doing _exactly the same thing_ to them. Or when we give someone the finger for honking at us, which only makes them angrier.

But all this anger has a deeper meaning: we use it to maintain our lost human identity.

When we step into a car, our sense of self is transformed into the anonymous metal box we are driving. In a sense, we become less human, and more machine. In this cyborg state, when someone cuts off our vehicle body, we feel like a part of ourselves has been cut off, too. So in a vain attempt to protect our identity, we rage at the other cyborgs on the road.

### 3. Traffic jams mess with our perception of time and social justice. 

Imagine you're stuck in traffic. The cars continue so far into the distance that you can't see the end. Your phone is dead, your bladder is bursting and you look at the clock and see you're running late for your important meeting. 

Then the cars in the other lane start moving. Even though you know deep down that your lane will move in a moment and theirs will stop, you get even angrier.

Why?

Because of your sense of social justice.

Waiting in line is a notoriously frustrating experience, but waiting in a line next to row after row of other lines is even more so. When someone arrives after us, but is served first, we feel unjustly treated and get angry. Although studies have shown that multiple lines don't favor anyone in particular, people prefer lines with one single row, because they feel more assured that they'll be served in the right order. 

Multiple lanes in traffic can also lead to peculiar behavior.

When we get anxious in a traffic jam, we start looking for any way out of it. So we start switching between lanes, weaving in and out of other cars. But the truth is that switching lanes gives only a miniscule advantage. In fact, a study examining an 80-minute drive showed that drivers who constantly changed lanes only moved four minutes faster than those who didn't.

One reason we think switching works is our faulty perception of time in traffic. When we aren't moving, it always seems the other lane is. And when we move, it only lasts a moment before we stop again. But in reality, both lanes move at a similar pace.

### 4. You’re a worse driver than you think because you receive no feedback. 

Imagine your father says he'll let you borrow his favorite toy: an expensive Audi. You say yes, of course, and are already picturing yourself zooming down the highway — when he suddenly tells you he's going to ride with you in the passenger seat.

Immediately you imagine all the comments he's going to make about going too fast, turning too late, etc. But while that might spoil your fun, his feedback may well help you behave better on the road.

Indeed, feedback is an effective way of getting people to follow the rules.

eBay is a great example. Every day, millions of perfect strangers exchange money and goods over the internet — and it's all regulated by feedback left by buyers and sellers. If someone has misbehaved, other users won't want to do business with that cheater. As a result, the vast majority of people behave.

Unfortunately, the nature of driving makes giving feedback impossible. There's no way to directly communicate with more or less anonymous drivers while driving. And attempts to give feedback online have proven unsuccessful.

For example, Platewire.com is a forum where people can type in a license plate number and give feedback about the driver. But the site has only 60,000 members - a miniscule proportion of all drivers. And, unlike feedback on eBay, being called a bad driver doesn't really affect our ability to drive. If there is no enforcement, it's a judgement without power.

What if we were to just give ourselves feedback?

Well, given that we are notoriously bad judges of our own abilities, this also wouldn't work. Studies show that we suffer from an optimistic bias, which makes us believe we are more capable than we are. In an absurd twist of logic, we all think we are _above-average_ drivers. But if everyone is above average, who is below?

Definitely not us, that's for sure.

### 5. Even though it’s easy to drift off when driving, we should avoid distractions. 

Have you ever driven somewhere and arrived with no memory of the journey? Then you've experienced _highway hypnosis._ Although we don't fully understand this mysterious phenomenon, we do have some ideas about why we "doze off" when we drive.

One main reason is that driving is nearly always _automatic._

In fact, driving is an _over-learned_ activity. This means that we've practiced it so much that we do it without any conscious thought. Just as experienced tennis players learn to move instinctively on the court, running to the exact spot they need to go, experienced drivers can navigate complex routes and obstacles without having to think.

In one sense, this is a good thing: especially when you consider that driving requires around 1,500 subskills, like judging speed or looking out for moving dangers. If we had to do everything consciously, we wouldn't make it to the supermarket without needing a nap.

But there is a downside to automatic driving: if we stop paying attention, we can easily fall prey to dangerous distractions.

When we go into auto-mode on the road, we often get bored, and when we're bored, anything is a good distraction. We listen to the radio, enjoy the view, use our phones. Soon we think we've mastered the art of driving and doing something else at the same time.

Until something goes wrong.

The vast majority of car crashes happen when drivers are distracted. A major year-long study placed cameras in cars, and recorded what happened just before each crash. They found that drivers who were distracted for no more than three seconds caused 80 percent of crashes.

So if you want to avoid accidents, remain mindful when you're driving, and pay attention to the road.

### 6. If you build it they will come: more roads means more traffic. 

You might be like many people who have to suffer a traffic-jammed commute to work, slowly crawling to your destination. So when you read on the news that a new road on your route is opening the next day, you're delighted. But by the time you get there, that road is blocked, too.

How come?

Because of _latent demand_.

Latent demand is traffic speak for the drivers who don't take a certain road because it is too busy. They might take other, smaller routes, public transport or just stay at home. But when a desirable road becomes less busy, the latent demand materializes, and drivers who wouldn't typically take the road replace the missing traffic.

For example, in 2002, a major labor strike at two ports in California stopped the usual flow of 9,000 trucks on the main port road during a week. But the total number of vehicles dropped only by 5,000, which means that 4,000 other cars took that road that week — the latent demand waiting to jump on a free-flowing road.

Another name for this phenomenon is _induced travel._ Drivers are induced to take a specific road when the incentive to travel increases, like when a new lane is added on a highway, and there's less congestion. But when people learn about the new lane, they all think it's the best choice, and soon that lane is congested, too.

So if more roads lead to more traffic, what can be done about congestion?

One tested method is _congestion pricing._

Congestion pricing means people have to pay if they want to drive in highly congested areas. In cities like London and Stockholm, it has effectively made people think twice about taking main roads that become easily congested.

### 7. Dangerous roads are safer than safe roads. 

You're driving to your partner's house on the other side of a mountain. You can either go take the highway or a narrow, snaking path up and down the mountain. Which do you choose?

If you want to be safe, you should choose the mountain path.

The fact is that people drive more carefully on roads that feel dangerous. And more careful driving means fewer accidents. Although we feel comfortable on wide, smooth streets with many clear signs, they lull us into a false sense of security, and we drive more recklessly. But on, say, a narrow mountain path, we have to pay attention constantly to avoid an accident — like the Swedes in 1963.

That was the year they switched to driving on the right side of the road. Many people assumed there would be many more accidents while people were getting used to the new system. In fact, the number of accidents significantly decreased to below average — that is, until the next year, when people got confident again, and the amount of accidents returned to average.

This prudence — and lack thereof — also shows up in how people drive on roundabouts and intersections. Contrary to what most people think, roundabouts are actually safer.

Many people feel roundabouts are more dangerous because they generate more stress as we try to weave into the flow of vehicles. However, their design actually forces people to drive more slowly, preventing drivers from going through a red light, one of the most dangerous things a driver can do. One study showed that replacing 24 intersections with roundabouts reduced crashes by nearly 40 percent, injuries by 76 percent and fatalities by 90 percent.

In short, unfamiliar or unsafe-seeming situations make people pay attention to their driving - which of course, is what you should be doing whenever you drive.

### 8. Final summary 

The key message in this book:

**The strange nature of traffic brings out the irrational side of humans. Our inability to understand how traffic works makes us stressed, angry and dangerous on the road. But if we can learn the hidden dynamics of traffic, and how they affect us all, we can make wiser decisions, and improve the roads for everyone.**

**Suggested further reading:** ** _Fooled by Randomness_** **by Nassim Nicholas Taleb**

_Fooled by Randomness_ is a collection of essays on the impact of randomness on financial markets and life itself. Through a mixture of statistics, psychology and philosophical reflection, the author outlines how randomness dominates the world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tom Vanderbilt

Tom Vanderbilt is a journalist, writer and blogger who contributes regularly to publications like _The_ _Wall Street Journal, Slate_ and the _London Review of Books_. He is also the author of _Survival City: Adventures Among the Ruins of Atomic America_ and _The Sneaker Book_.

