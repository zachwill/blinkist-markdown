---
id: 54c651b837323900093a0000
slug: trendology-en
published_date: 2015-01-28T00:00:00.000+00:00
author: Chris Kerns
title: Trendology
subtitle: Building an Advantage through Data-Driven Real-Time Marketing
main_color: D7DF2D
text_color: 8C911D
---

# Trendology

_Building an Advantage through Data-Driven Real-Time Marketing_

**Chris Kerns**

_Trendology_ examines how brands can use social media such as Twitter to make direct contact with their customer base, and can benefit from tapping into trends in the social media conversation. In addition to social media theory, _Trendology_ offers a step-by-step guide on how to build a successful social media program and succeed in real-time marketing.

---
### 1. What’s in it for me? Learn how to trend your brand through real-time marketing. 

Today, advertising is everywhere, from eye-catching billboards to hidden ads online. To cut through the noise and grab our attention, brands have to be more creative and adaptable than ever.

_Trendology_ argues that since such a big portion of our time is spent online, brands should use social media channels, such as Twitter, to contact customers and take advantage of current social trends. 

After reading these blinks, you'll learn

  * that real-time marketing means more than jumping on the latest meme;

  * how Ben & Jerry's tapped into legalizing pot to promote and brand their ice cream; and

  * that tweeting during the Super Bowl will win you lots of followers.

### 2. Social media has turned advertising from a one-way message into a two-way conversation. 

Companies are always looking for new ways to refine their message and reach out to potential customers. Yet "traditional" advertising for generations has been mostly a one-way conversation.

The advent of social media, however, has turned the advertising world on its head.

Early advertising had its start in the 1920s, as the United States expanded its interstate highway system and Americans were spending more time in their cars. As brands realized that their target audience was on the road, billboards began appearing along highways to deliver a company's message and grab people's attention.

From static ads like billboards to jingles and commercials on radio and television and later, a whole new market for ads online, brands have always sought new ways to attract potential customers.

With the advent of social media, however, advertisers are now jumping on board platforms such as Twitter and Facebook.

However, social media doesn't act like a commercial or a billboard. Social media is based on _conversations_, not one-way communication. This fact has fundamentally changed the nature of advertising, as companies now realize that they need to _react_ as well as act.

But social media also provides brands with new marketing opportunities. For one, conversations on social channels can be more persuasive than traditional advertising, as it makes a brand more human.

It allows companies to speak directly with customers, responding to their complaints and their love letters, and even have fun with them. For example, when the author tweeted "thanks for your lunch today" to Pizza Hut, he received an answer only a few minutes later: "Anytime."

A good marketing plan can build a brand into an authentic voice, the likeability of which garners a company "likes" or followers, giving a brand greater reach with any future messaging.

Social media also gives brands the opportunity to react to events in real time in a relevant way, thus enabling a brand to target specific demographics and further allow the audience to engage.

In essence, social media allows brands to come alive in ways that a billboard simply can't!

> _"Social media is, and has always been, a mix of art and science. It's a conversation and not a billboard."_

### 3. Social media is a great platform for real-time marketing – that is, if you can avoid certain missteps. 

Real-time marketing is based on the idea that, when the world's attention has shifted to a certain subject, you can use the topic of the day, hour or minute to grab a piece of attention for your brand.

More concretely, _real-time marketing_ is the practice of creating content inspired by current trends and events. Social media is a great platform for this kind of strategy.

But what does it look like in action?

Imagine you're watching the Super Bowl, and your favorite team, the Green Bay Packers, intercepts a pass thrown by the New England Patriots' quarterback.

You tweet: "_OMG damn straight right #goPackers #interception_." As other users respond to your tweet, another tweet appears from your local pizza place: "_Nice pick! Packers fans, pick your favorite pizza now for only $10. #interception_."

That's real-time marketing at work. The pizza place saw a momentary opportunity for attention and grabbed it, and now _you_ might consider ordering pizza at halftime.

Social media is a real-time environment, allowing a brand to launch campaigns that make it part of the moment. What's more, social media has generated a new culture of trends and viral sharing that offers great opportunities for real-time marketing.

Moreover, social networks also work to ensure that brands feel welcome on network platforms, with the hope of earning money from advertisements to which brands buy access.

Nevertheless, brands often make social missteps as they jump headfirst into real-time marketing.

For example, when a brand wants to capitalize on a particular trend or topic, it should be sure to wrap the message around existing context. Otherwise, the message will be ignored, or even laughed at.

For the same reason, it's best to post content within a few minutes of an event, or everyone will have moved on and you'll have missed your opportunity.

Ultimately, the success of your real-time marketing program lies in finding the right balance between engagement and standing on the sidelines.

> _"Social media is what brands needed for real-time marketing to really take off."_

### 4. Real-time marketing success in social media is measurable, and gives you data you can use further. 

So what exactly does success look like on social media? To answer this, we'll need the help of certain performance indicators.

First, success can be measured by _audience_, or the individual users who've elected to have your posts show up in their newsfeed.

Different platforms have different ways of identifying audience. For example, on Twitter, audience is measured by _followers_, for whom your tweets appear on their _timeline_.

Success can also be measured by _engagement_, or the amount of audience interaction with a brand's content.

On Twitter, engagement is measured in terms of _favorites_, essentially the equivalent of the "like" button on Facebook, where the audience shows its support for a tweet as well as the number of _replies_ your tweet receives.

Finally, you can look to your _shares_ to gauge your success. Getting the audience to share your social media posts means that your content not only connected with your customers but also was good enough for them to attach it to their own personal profile.

On Twitter, shares can be measured by _retweets_, which cause your tweets to show up on the timelines of those who follow that particular user who retweeted your post.

All together, the true measure of success is an amalgamation of all these performance indicators, or _actions per follower_.

If you just look at audience, engagement or shares, then the results can be misleading.

Success isn't just about getting the greatest number of people to retweet your content. Rather, you should be thinking about what _percentage_ of your followers are taking some kind of action with regards to your content.

For instance, if you have 5,000 followers and 50 of them favorite one of your tweets, then you can be pretty happy with that level of engagement. However, if those 50 favorites are from a pool of 20 million followers, then you should probably rethink your strategy. 

Real-time marketing is valuable for your marketing team. The following blinks will give you actionable tips on how you can get started on social media right away.

### 5. Twitter provides company brands with a number of different ways to reach out to followers. 

With Twitter, company brands have three different ways to reach out to potential customers. A brand can broadcast a message to the masses; it can echo the messages of others; or it can reply directly to a user's tweet.

Let's take a look at these three different types in order.

First, brands can create _original content_. This can include things like posts about product news or other information, questions for followers to answer, or perhaps calls to action.

A good example of this can be found on the Pampers Twitter feed: "Retweet if baby brings nothing but good luck into your life!"

Next are _retweets_. These echo the remarks of other people or companies, rather than your own original content, and are sent to your followers by clicking the "retweet" button.

Finally, there are _replies_. By using the reply function, a brand can talk one-on-one with a customer to address his concerns. Replies are a great way to get customers to feel like the brand is interested in their ideas, and acknowledges their presence.

Moreover, brands can make use of diverse wording, characters and emoticons to generate engagement. Twitter gives users only 140 characters to convey their message, and with such limited space, marketers have to make choices about how to best craft their message.

For example, brands can include exclamation points in their tweets to bring animation and emphasis to content. Similarly, posing a question to followers can spark a lively response from your audience — a practice that brands use a fair amount.

Volkswagen USA, for instance, once posted a photo of a new car design and asked their followers: "What would you do to drive the Design Vision VW GTI for one day?" The result was a lot of great engagement and interest in the product.

Finally, you can give your brand a personal touch by including smiley faces or other emoticons in your posts.

### 6. The conversation on Twitter moves fast; stay on top of the chatter to take advantage of micro-trends. 

New trends appear on Twitter nearly every hour, only to disappear hours later.

Yet, during this brief period, these _micro-trends_ can still garner huge amounts of buzz for brands to exploit.

Twitter makes _trending topics_, or the most-popular Twitter hashtags across each regional user base, easily recognizable, giving your brand an easy opportunity to broadcast your message to an engaged audience.

For example, on January 24, 2014, one trending topic was #WeWillAlwaysSupportYouJustin, right after singer Justin Bieber was arrested for allegedly driving while intoxicated.

Tweets that contain trending topics usually provide a significant performance boost for brands. In fact, tweets with trending topics enjoy a near 50-percent boost on average in terms of both retweets and favorites per follower.

Moreover, almost 70 percent of the 93 companies examined by the author have experienced a bump when using this practice.

Amazon's tweets, for example, see a 139-percent increase in favorites per follower when they use trending topics compared to the company's other tweets.

Brands can make use of breaking news by mentioning it in their tweets. For example, when the state of Colorado passed Amendment 64, a referendum supporting the legalization of marijuana, Ben & Jerry's ice cream company jumped on the subject with a clever and effective tweet: "BREAKING NEWS: We're hearing reports of stores selling out of Ben & Jerry's in Colorado. What's up with that?"

The tweet included an image of a nearly empty freezer shelf upon which only a single tub of Ben & Jerry's remained, a flavor called "Hazed and Confused."

This single tweet's engagement results were better than any other social content the company created in the first half of 2014!

> _"A message without an audience is a message that isn't heard."_

### 7. Real-time marketing during large events can be an opportunity to boost a brand’s social performance. 

We've seen how brands can leverage popular trends, but now let's take a closer look at how to harness the power of large social events, such as the Oscars, the Super Bowl or the Grammys.

Real-time marketing during events like these is valuable for one main reason: attention.

Big pop culture events draw such an enormous amount of social media attention that brands fight for the chance to broadcast their message and start a conversation with potential customers.

Social media teams can and should plan ahead by building content around an event's theme beforehand, to then post in a timely fashion during the event.

For example, many facts about the Oscars are known in advance: nominees, the location, the starting time, and so on. This gives brands the opportunity to create _planned_ content, such as a simple tweet: "Here we go, the #Oscars just started!"

Real-time content can also be _opportunistic_. For example, during the World Cup in 2014, when Uruguay's Luis Suarez bit Italy's Giorgio Chiellini on the shoulder, Snickers posted a picture showing Snickers bar with a bite taken out of it, along with the phrase: "More satisfying than Italian."

Live-tweeting during events isn't just a fringe tactic, either. In fact, when the author analyzed data from the Twitter accounts of the 100 most valuable brands in the world, he found that 58 percent of them saw a positive performance bump in retweets and favorites after posting planned content.

Moreover, a striking 85 percent of brands who post _opportunistic_ content see a higher rate of retweets and favorites per follower than with their normal social content.

If that weren't enough, brands who engage in real-time marketing also enjoy an increase in Twitter followers after a big event. Looking at the numbers, brands earn 359 percent more followers on average than those who forgo real-time marketing during events.

These tactics, however, won't implement themselves. Our final blinks will show you how to put together the team that will successfully carry out your social media mission.

### 8. Real-time marketing success requires assembling a creative and responsive digital team. 

To become a brand that thrives in real-time marketing, you'll need more than just a social media staffer with a quick wit and access to the company Twitter account.

Your social media team needs to know how and when to act, and trust that they are free to be both creative and responsive.

To accomplish this, your team needs _executive support, resources_ and _authority._

First and foremost, garnering support from the C-levels will provide you with the resources, public relations support and a long-term vision that you need to be successful.

Naturally, you'll need to carve out a dedicated budget for all these newfound financial and human resources. You'll also need a competent and diverse team, as diversity within the team will help the group produce unique, creative content.

Finally, a successful real-time marketing team needs the authority to act. Nothing is worse than having a great, timely post ruined by the bureaucracy of a two-day approval cycle: _"Sorry, we can't post that because of the wording. Give me a few hours, because I need to run this through legal."_

Your social media team doesn't want to hear that, and you can actually harm your brand by delaying posts and chiming in on day-old trends.

Of course, before you can give your team resources and authority, you'll have to have a team to begin with. It will help to keep these three things in mind while assembling your team:

First, recruit candidates who love real-time marketing and have witnessed its success. Their learning curve will be less steep, and they're likely to come into the job with great ideas.

Second, recruit for seniority. The more senior employees you can get on the team, the more stable the quality of work will be. What's more, experienced team members will attract other great minds as well.

Finally, find people who work well with data. Analyzing data from social media channels will prove crucial in identifying the trends that help your brand get an edge.

### 9. A good real-time marketing strategy begins with establishing goals and measurement criteria. 

Imagine you've just completed your first big real-time marketing event: you've assembled your team, monitored trends and posted some riveting content.

As you sit down to write a summary of your real-time marketing efforts to the executive team, you realize that you forgot to set _goals_.

Setting goals should actually be the _very first_ step! Without goals, you'll feel lucky for _any_ engagement you get. Goals, by contrast, force you to learn what works and what doesn't.

You have to ask yourself: _What am I trying to achieve with real-time marketing?_ For example: Do you want to increase the reach of your message? Or would you rather have more engagement with your audience?

Perhaps your goals already exist. In all likelihood, your company has probably already set goals for the campaign, the quarter and the year. Your real-time marketing goals and the efforts that accompany them should work to help support these larger goals.

But how do you know whether you've reached your goals? To do this, you'll need to first set up your success metrics.

There are so many things you can measure on social channels, so it's important to boil down your measurement strategy to a few key indicators, such as favorites or retweets per follower.

By limiting yourself to a handful of key indicators, you'll keep your team focused on a specific definition of success, and thus be able to track effectiveness without having to look at 50 different metrics.

What's more, settling on just a few key metrics will help simplify your post-event analysis.

Finally, you need to be honest with yourself and your team about how you performed and critical about any flaws. Only then you can learn from your mistakes.

### 10. Building up a solid, real-time marketing program is a five-step process. 

By this point, you've seen what real-time marketing can achieve. Now the trick is to create a program that allows you to produce solid, repeatable results.

Luckily, developing such a program is as simple as following a five-step process before events where you want your brand involved in real-time marketing campaign.

Step one is to _align_ : make sure that your actions are always aligned with your goals. One way to do this is to constantly remind yourself and your team of your goals, by continually bringing them up in discussions about decisions the team plans to make.

Step two is to _discover_. Ask yourself, "What can I learn about an event beforehand that would help my content be more relevant?" For example, one of the quarterbacks in the next Super Bowl might be a hometown hero, or an independent film producer in your area may have received four Oscar nominations.

These facts might be relevant to your target market, so give your team time to do some research.

Step three is to _prepare_. Once you've considered the different scenarios that could arise during an event, try to build content for each. How will you react on social media, for example, if Spielberg wins the Oscars, or if a performer slips on stage?

Having content ready to go allows you to quickly pull the trigger in the event that one of these scenarios plays out.

Step four is to _execute and engage_. As you now know, real-time marketing isn't _just_ about your posts. It's also about engaging your followers, for instance, by replying to the activity that the post elicits and replying to followers' posts.

The final step is to _analyze_. Look back on your performance and try to discern whether your content resonates with your audience. Ask yourself whether your goals need to be updated and where new opportunities exist.

If you follow this five-step process, your team will be well on its way to real-time marketing success.

### 11. Final summary 

The key message in this book:

**Social media provides your brand with great and unique marketing opportunities. But these opportunities are different than with traditional advertising. To maximize your success, you'll have to find ways to actively engage and interact with your audience to create a brand image they love.**

**Suggested further reading:** ** _Growth Hacker Marketing_** **by Ryan Holiday**

_Growth Hacker Marketing_ charts a major departure from traditional marketing practices, relying heavily on the use of user data and smart product design. This book illustrates how today's top technology companies, such as Dropbox and Instagram, have used this strategy to gain millions of users.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Chris Kerns

Chris Kerns leads the analytics and research group at Spredfast, a social marketing consultancy that empowers organizations to connect with consumers through social media. He has set the digital strategy for some of the world's largest brands, including Ford, Microsoft and Nike, and his research has appeared in publications such as _The New York Times_, _Forbes_, _USA Today_ and _AdWeek._

