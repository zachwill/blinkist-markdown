---
id: 53f4698f34333900084d0000
slug: tribal-leadership-en
published_date: 2014-08-19T00:00:00.000+00:00
author: Dave Logan, John King and Halee Fischer-Wright
title: Tribal Leadership
subtitle: Leveraging Natural Groups to Build a Thriving Organization
main_color: A3D9FD
text_color: 4494C9
---

# Tribal Leadership

_Leveraging Natural Groups to Build a Thriving Organization_

**Dave Logan, John King and Halee Fischer-Wright**

This book explains how members of the same workplace function together as a tribe. Each tribe has a culture that determines its productivity, and there are five distinct stages of tribal culture. _Tribal_ _Leadership_ will show how you, as the "tribal leader," can guide _your_ tribe to higher levels, resulting in a healthier and more productive work environment.

---
### 1. What’s in it for me? Learn how to lead your workplace to greater success. 

Why do some workplaces fail while others enjoy wild success? A great deal of a business's success depends on how the employees work together. Every group of co-workers is a _tribe_. Each tribe has a _tribal_ _culture_ that's expressed through the attitudes of its members. There are five stages of tribal culture, and tribes at the higher stages are more efficient and successful. _Tribal_ _Leadership_ will teach you how to identify those five stages, and become a _tribal_ _leader_, so that you can guide _your_ workplace tribe up the ladder, and enjoy greater success.

You'll also learn:

  * how being too focused on outperforming your competitors can be counterproductive,

  * how bringing your tribe to a higher stage can put you on the _Fortune_ list of best companies to work for, 

  * why the vice chair of CB Richard Ellis only talks to people in groups of three at business parties, and

  * how you can move your whole tribe to a higher culture stage, one member at a time.

### 2. People function as a tribe in the workplace. 

People have formed groups throughout history, and for good reason: we need each other to survive. The workplace is no exception to this. It's impossible to complete a large-scale project, for example, or launch a new product by yourself. To complete these goals we need help from other people, so we form _tribes_.

What exactly is a tribe? It's the social structure in which people operate. Tribes consist of 20 to 150 people, so a company that has over 150 employees is more like a big tribe comprised of smaller tribes. Also, there must be some social interaction between its members. Think of it this way: if you saw a member of your tribe walk by, you'd at least say hi to them.

In all tribes, the members have different roles that serve different purposes. A group of people that all think and work the same way won't succeed — tribe members have to work individually on some tasks but _together_ toward the same goals.

For example, consider the roles you'll find in a small town. Every town has a police officer, a preacher, a librarian, etc. These roles are quite different, but the town needs them all to function properly. Tribes work in the same way.

Forming a tribe is necessary for completing any large-scale project. No one could've built cathedrals or survived the Ice Age alone!

Tribes are equally necessary for modern day goals like releasing a new medicine or editing and publishing a book. You'll find tribes in all workplaces, as they are important and necessary in every field.

Since tribes are so critical for completing any kind of sizable goal, it's important to have a thorough understanding of how they work. They are an essential part of how we, as humans, operate.

### 3. Every tribe has a culture that determines its efficacy. 

Have you noticed that some people are more hardworking than others? The same is true of tribes: some are more productive and function better.

Each tribe has it's own _tribal_ _culture_, characterized by the members' relationships and attitudes toward each other. The tribal culture results from the interactions between members, but it also influences the members in turn.

To understand this, think of a class at school. A good class is one where most of the students perform well. But how do they perform well? By being in a good class. Even a "bad" student will probably start to improve if moved to a better class. The performance of the class as a whole depends on the students, but the students' performance also depends on the class.

In an office setting, the tribal culture works the same way. It influences the actions of individual employees. Over time, the employees unconsciously adopt the culture and mind-set of their fellow tribe members. So if an office has a lazy or apathetic culture as a whole, even an ambitious new member will become lazy eventually. So even if a tribe has _some_ skilled employees, it won't function well if it has an unproductive tribal culture.

A workplace's tribal culture is also characterized by its size. A tribe is bigger than a team, but smaller than the company as a whole. Realistically, a single team can't determine the work process of an entire company, and the top management echelon of a company also can't maintain a personal relationship with each of the employees. That's where a tribe's culture comes in: the tribal culture determines the mood of the employees, and how they handle their individual workload.

Naturally, it's critical to foster a good tribal culture if you want your workplace to succeed. But what is a "good" culture, anyway?

### 4. Tribal cultures have different stages, and stage one and two are ineffective. 

Why are tribal cultures so important? Because they affect the entire tribe, and which _stage_ the tribe reaches. There are five stages, and the lowest stages are the least productive.

A tribe in _stage_ _one_ has a hostile environment. People with a stage one mind-set feel that life is cruel and unjust. They often believe they must be violent or hateful to survive. Only two percent of Americans have a stage one mind-set at their workplace, but many more do in society at large.

For example, children from underprivileged backgrounds often have a stage one mind-set. If their family can't afford a pair of shoes, they might steal them without guilt. They believe that life is unfair and they need to break rules in order to save themselves.

If a tribe has a stage one tribal culture, it's too hostile to function well. The mafia, for instance, typically consists of people with no regard for rules, and members often end up fighting each other.

So if you're an employer, don't hire stage one employees. They won't be loyal, and will take any opportunity to further their own interests at the expense of the company.

Like stage one, _stage_ _two_ tribal cultures are also ineffective. Stage two is characterized by apathy: people with this mind-set don't think that _all_ life is awful, just that _their_ life is awful. They don't believe their situation can improve, so they avoid responsibility at all costs.

This mind-set is more common: around 25 percent of American workplaces have a stage two culture. It occurs frequently in highly bureaucratic environments, where many people do dull, repetitive work in cubicles. They'll likely cite the boss, system or their education as the reasons they can't do something more creative.

In stage two workplaces, employees won't show initiative unless they have to. Thus, like stage one workplaces, they are quite unsuccessful.

### 5. Stage three tribal cultures are the most common, but are still unhealthy. 

In the middle of the five stages of tribal cultures, we find _stage_ _three_. Stage three is the most common mind-set, affecting 48 percent of American employees. Unfortunately, it is still negative and unhealthy.

Stage three is characterized by selfishness and arrogance toward others. People with a stage three mind-set only care about their own interests. They'll form two-sided bonds with other stage three people, but only if both sides directly benefit.

A employee with a stage three mind-set probably feels quite lonely, largely because they often view their colleagues as incompetent or lazy. For example, a doctor once told the authors of this book that nurses are only nurses because they're not smart or hard-working enough to be doctors.

Consider how alone this doctor must feel: he thinks his colleagues lack the intelligence to appreciate his work, and aren't as dedicated as he is. His stage three attitude and statement is not only unkind to his colleagues — it affects him negatively as well.

A stage three tribal culture makes a workplace unproductive, because it prevents real cooperation between members. Individual employees might benefit at times in stage three, but good collaboration is necessary for any meaningful project to succeed, and stage three employers are usually too self-interested to collaborate well.

Imagine that our stage three doctor wants to develop a new suturing method. If he doesn't have enough confidence in his colleagues to delegate some responsibilities to them, he won't have time to work on the suturing between his usual daily tasks.

Completing a meaningful goal like that is quite difficult on your own — it requires help from others. A stage three mind-set can really hinder the cooperation that's necessary in any prosperous workplace.

### 6. A stage four tribal culture is ideal. 

_Stage_ _four_ is the stage you'll ultimately want to aim for.

In stage four, tribe members are united around common values and a noble cause they truly believe in. Members are more devoted to the tribe's cause than to their own success.

For example, a surgeon in stage four won't focus on their own fame or financial gain. They'll remember that they went to medical school to help others, not themselves. That desire to help others will be the driving value of their career.

Values are very important in stage four tribes. The former CEO of Amgen, for instance, spoke to all his employees to make a list of what they valued most in the company. The Amgen executives then used this list as a guideline to make sure that the company always adhered to the employees' values.

Because of their shared values, tribe members collaborate much more easily in stage four. They often form three-sided relationships called _triads._ The vice chair of CB Richard Ellis does this at business parties. She only speaks in groups of three, to make sure that people in different fields can meet and truly engage with each other.

This healthy collaboration and engagement makes stage four work environments the most successful. The power of harnessing all the members' ideas can pave the way for real innovation.

In 2003, for instance, IDEO designed some new buildings for the hospital group Kaiser Permanente. Before they began, they worked together with the Kaiser staff in role-play situations, asking them to pretend to be patients. By doing this, they discovered that the new buildings were unnecessary — it was better to rearrange the space they already had. They held their value of focusing on the patients over their desire to expand, and collaborated to find the best solution.

> _"If core values are the fuel of the tribe, a noble cause is the direction where it's headed."_

### 7. A stage five tribal culture is unstable, but it offers miraculous innovation. 

Though stage four is optimal for businesses, _stage_ _five_ is where the magic truly happens.

Stage five is rare — only two percent of American workplaces have a stage five tribal culture. In stage five, the central cause of the tribe is the only thing that matters to the members. They don't work toward the company's goals for personal gain. Instead, they feel an almost religious sense of wonder about their cause, or the possibilities it offers.

For example, the biotechnology company Amgen was extremely successful in the 1990s. When asked who their main competitor was, they didn't name another company, but diseases themselves. They didn't aim to become the most successful corporation in their field: instead, they truly wanted to defeat adversaries like cancer or obesity.

Stage five mentalities like this pave the way to innovation and success. When people aren't distracted by potential competitors, they can fully invest themselves in their goal. An engineer at Amgen embodied this when he spoke about the billions of dollars Amgen had made from their patents. He didn't say anything about the advantage this had given them over their competitors. Instead, he marveled at the new diseases they might now have the funds to cure.

Stage five tribal cultures usually occur right after a great discovery or achievement, so they're quite unstable. The euphoria that members find in devoting themselves to their goal might not last. Incidentally, Amgen floated in and out of stage five during the 1990s, depending on what advances they were making.

Businesses in stage five always outperform the lower stages, but in the end, they're unreliable, so a stage four business is still more productive in the long run.

### 8. The tribal leader has the role of bringing the tribe to the next stage. 

All tribes, including workplace tribes, need a _tribal_ _leader_. In a business tribe, the leader doesn't necessarily have to be the manager or CEO. However, tribal leaders do eventually have high positions in their companies, as their performance typically results in them getting promotions.

The role of the tribal leader is to guide the tribe forward when they're stuck in one of the stages. This can be challenging, as the members can make it difficult to move forward. We saw how individual employees adopt the nature of the tribe's culture, meaning that if the tribe is in a lower stage, the members might prevent each other from moving forward. A strong tribal leader, however, can pull the whole group higher.

For example, the CEO and vice president of Griffin Hospital successfully brought the Griffin tribe from stage two to stage three, then finally stage four. They engaged the staff by asking for their input on important company issues, such as new building designs and patient service plans. This taught the members to collaborate and work toward higher goals together, eventually resulting in _Fortune_ naming Griffin one of the best companies to work for.

Tribal leaders have the responsibility to bring the tribe to higher levels, as the Griffin leaders did. A sustained, high tribal culture level ensures the company's long-term success. Most managers only focus on raising profits and gaining customers, but those goals can't bring the company prosperity in the long run.

The leader's genuine devotion to their employees is what guides the tribe into higher and more efficient levels. The Griffin tribe leaders' devotion was so successful that representatives from other hospitals still visit Griffin to learn how they can benefit from studying their model. Strong leaders create environments that are not only successful, but inspiring too.

> **Fact:  

** The majority of the people that the authors interviewed and recognized as tribal leaders are considered for CEO positions in their companies.

### 9. Tribes can only progress one stage at a time, and there are many diverse strategies to make that happen. 

A leader must move their tribe to each higher stage, but it's important not to rush it. Before moving on to the next stage, the leader must become a master of the previous stage.

Although the stages are quite different, it's important to progress through all of them. If a leader tries to move the tribe from stage two straight to stage four, for instance, the tribe won't be stable. If the tribe encounters a serious challenge, members will be tempted to back away and think only of themselves, because they didn't mature out of that by passing stage three.

Consider how this applies to Griffin Hospital. The leaders couldn't have fostered collaboration if the staff had hated their jobs in the first place. They wouldn't have cooperated. Firstly, they needed to feel secure about their own interests, then they could unite around the common cause of patient welfare.

To move the tribe in this way, the leader or leaders need to build the right kind of relationships between the members, and use the right kind of language.

The tribe's stage is expressed by the way the members relate to each other. Let's look at a person moving from stage two to stage four.

People in stage two think their lives are awful, and they don't communicate well. To ease them into stage three, where people are motivated by self-interest, encourage them to test out new ideas, and to build two-sided relationships they'll benefit from. They'll start to see that they _can_ improve their situation, and begin to focus on that.

Once they're secure in stage three, you can start to guide them into stage four. Do this by encouraging them to work in triads, and reminding them of the cause they're working toward.

With the right focus, an attentive and strong tribal leader can bring the tribe to greatness.

> _"Tribal leadership can never emerge out of weakness."_

### 10. Final summary 

The key message in this book:

**Tribal** **culture** **is** **the** **key** **to** **success,** **and** **tribes** **in** **the** **higher** **stages** **of** **culture** **are** **the** **most** **efficient.** **The** **tribal** **leader** **has** **the** **responsibility** **of** **moving** **the** **tribe** **to** **the** **next** **stage** **by** **encouraging** **certain** **behaviors** **among** **individual** **employees.** **With** **a** **strong** **tribal** **leader,** **the** **majority** **of** **the** **tribe** **members** **will** **eventually** **arrive** **at** **the** **next** **stage.**

Actionable advice:

**Work** **with** **your** **employees** **individually.**

Even though your goal is to get the employees to a higher stage as a group, you achieve that by helping each one individually. So encourage them to be creative, work with each other, and keep reminding them of the cause they're working toward. The more employees you can get to progress to a higher stage, the more they'll influence the others, and eventually elevate the group as a whole.

**Suggested** **further** **reading:** **_Tribes_**

In _Tribes_, author Seth Godin reveals the most powerful unit of social organization: the "tribe," or a group of people connected to a cause, a leader and each other, who together drive change in society. It shows us how we can harness the power of the internet to form and lead our own tribes. _Tribes_ also advocates the universal need for change and leadership, so we can grow as a company or as a society.
---

### Dave Logan, John King and Halee Fischer-Wright

Dave Logan is a professor of business at the University of Southern California and the author of several successful books. With John King, he co-founded the management consultancy CultureSync. John King is also a successful writer, and a popular keynote speaker. Halee Fischer-Wright is a former pediatrician who is now a leading expert on healthcare and business management. She is also a partner at _CultureSync,_ and serves as assistant clinical professor at the University of Colorado, where she earned her M.D.

