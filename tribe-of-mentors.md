---
id: 5a808ba5b238e100085ef1e8
slug: tribe-of-mentors-en
published_date: 2018-02-12T00:00:00.000+00:00
author: Tim Ferriss
title: Tribe of Mentors
subtitle: Short Life Advice from the Best in the World
main_color: FFCC33
text_color: B28F24
---

# Tribe of Mentors

_Short Life Advice from the Best in the World_

**Tim Ferriss**

_Tribe of Mentors_ (2017) is an insightful collection of interviews with mentors — highly regarded professionals who are widely considered to be leaders in their respective fields. To understand the secrets of their success, Tim Ferriss asks these mentors a variety of questions, from "What are your favorite books?" to "What are your daily habits?" The answers add up to a trove of useful tips for getting to the top.

---
### 1. What’s in it for me? Learn how the most respected professionals in the world got to the top. 

Are you a recent graduate or a budding talent, ready for the next step in your career? Or maybe you've been working for a while now and are frustrated with the results so far. Either way, you've probably encountered a few challenges that you're eager to overcome, so why not get some advice from the world's top performers, the people who've already secured positions as pillars of their respective fields?

Tim Ferriss, the author of _Tribe of Mentors_ and a rather well-respected entrepreneur in his own right, set out to create a handy guidebook for anyone looking for a little inspiration — a book of insightful and meaningful advice from some of the world's brightest minds. Feeling the need for a motivational boost? Dive in to find some of the juiciest bits of advice!

In these blinks, you'll discover

  * the one book that both Terry Crews and Jimmy Fallon hold dear;

  * the one self-investment that most mentors couldn't do without; and

  * how "forest bathing" can come in handy.

### 2. To help you grow and succeed, turn to books that inspire and stimulate you. 

The word "success" means different things to different people, but most of us would agree that being known to the world as a leader or a mentor is indicative of a high level of accomplishment. That's why, when trying to pinpoint the habits of successful people, the author interviewed a series of renowned leaders and mentors. So what did he discover?

If asked to list the habits of success, you might not put reading at the top — and yet the author found that, for many leaders, books are an important source of inspiration.

For instance, the British journalist Matt Ridley, whose books on science and economics have sold millions of copies and been translated into over 30 languages, cherishes _The Selfish Gene_, by Richard Dawkins. Dawkins's book not only provided Ridley with an answer to that most pressing of questions, "What is life?" — it revolutionized how people write about science and think about evolutionary biology.

In general, the books that mentors hold dear tend to offer spiritual or personal guidance.

Both the talk-show host Jimmy Fallon and the actor and former NFL star Terry Crews value Viktor Frankl's _Man's Search for Meaning_. Fallon believes the book made him a better person, and it keeps Crews focused on what's truly important in life.

Social science researcher and TED Talk superstar Dr. Brené Brown says that Harriet Lerner's _Why Won't You Apologize?_ revolutionized her approach to apologizing. And then there's that infamous book, _The Fountainhead_, by Ayn Rand, which taught _Wired_ magazine cofounder Kevin Kelly the importance of self-reliance — so much so that he dropped out of school after reading it!

Five-time Grand Slam tennis champion Maria Sharapova gained priceless insight into happiness from _The Beggar and the Secret of Happiness: A True Story_, by Joel ben Izzy.

Even children's books can contain life-changing insights: TED Talk curator Chris Anderson will never forget the way C. S. Lewis's _The Chronicles of Narnia_ ignited his imagination. And, for Jimmy Fallon, _The Monster at the End of the Book_, by Jon Stone, continues to be a touchstone on the important subject of bravery.

### 3. Failure can help you grow and improve yourself. 

Most of us, at some point in the past, have experienced at least one epic fail. Though such setbacks can feel insurmountable, it's crucial to pick oneself up and carry on. But it's even more important to try to _learn_ from failure.

For instance, Rick Rubin, who's produced countless Grammy Award-winning records, is familiar with the fickle finger of success. Masterful albums sometimes flop completely, and mediocre ones sometimes go platinum. The lesson he learned from failure is this: you can't control how your work will be received. All you can do is stay true to your standards and make it the best you can.

Failure can also motivate you to take action and make improvements.

After 15 years of getting rejection letters from publisher after publisher, Steven Pressfield felt he'd had enough — so he packed up his typewriter and left New York for Hollywood, where he could focus on improving his craft. In this new environment, Pressfield flourished, selling screenplays like 1993's _Joshua Tree_, before moving on to bigger and better things, including the screenplay for Robert Redford's _The Legend of Bagger Vance_.

In 2005, Leo Babauta felt like a complete failure. He was overweight, in debt and addicted to junk food. That same year, however, he put all his energy into developing healthy habits, a process that he chronicled on _Zen Habits_, a blog that's helped more than 25 million people to do the same.

But perhaps the best aspect of failing is that it can give you artistic freedom because it removes the burden of expectations.

When the Beastie Boys' second album, _Paul's Boutique_, did poorly, the group could take a moment to escape from the public eye and focus on their music. Three years later, they released the artistically and commercially successful _Check Your Head_.

When you fail and live to tell the tale, the fear of failure no longer seems like a threat — and this will make it easier for you to take risks and pursue true innovation. That's why Grammy Award-winning comedian Patton Oswalt recommends getting that first failure under your belt. Once you've failed, you can start growing.

> Ray Dalio says he enjoys the unusual habit of "reflecting on painful mistakes."

### 4. Not all worthwhile investments are necessarily traditional ones. 

When you hear the word "investments," you might think about the stock market, but an investment can mean much more than what happens on Wall Street.

For mentors, the best investment you can make is one that takes your career to the next level.

_Wired_ cofounder Kevin Kelly recalls that the first $200 he invested in his first venture taught him far more than he ever learned while pursuing his master's degree in business. It was also a lot cheaper! That $200 got him an ad in _Rolling Stone_ magazine to promote a travel-guide catalog that didn't even exist yet. But by _bootstrapping_, or carefully managing the money that came in because of the ad to spur further growth, he was able to create that catalog and get his foot in the publishing world.

Therapist and relationship guru Esther Perel invested in herself by learning nine languages, and now she can communicate fluently with people around the world — and it's definitely paid off. Sex and relationships are complex subjects, and she's always able to find the right words.

Some of the most rewarding investments can be those made in other people: Jason Fried is CEO for the software company Basecamp, which has made millions facilitating communication and project management services. When he thinks of the most fulfilling moments in his life, it's the times he gave to friends, with no expectation of ever getting a cent back in return. When one friend was starting up a new gym, he paid for the first year's rent, no strings attached.

However, nothing's more important than investing in yourself.

After she collapsed from exhaustion in 2007, the founder of _Huffington Post,_ Arianna Huffington, decided to take a break and invest in her own well-being. She has no doubt that her future work, not to mention her personal life, benefitted from that decision.

Mentors also concur that meditation is one of the best self-investments out there.

A small list of those who regularly practice meditation includes Rick Rubin, Jimmy Fallon, the historian Yuval Noah Harari, the Oscar-nominated director David Lynch, chess master Adam Robinson and the investor Ray Dalio. For each of them, meditation provides personal insight, a sense of fulfillment and sharpened focus, and makes it easier to overcome challenges.

> _"Every smart person and stable person I know both walks and meditates."_ — Jimmy Fallon

### 5. Self-improvement can come from joining a community, prioritizing or seeing things differently. 

No matter who you are, there's always room for improvement, right?

Well, the author was particularly curious about the methods mentors use to improve themselves.

Both Rick Rubin and the best-selling author and journalist Neil Strauss have found it helpful to build a community of friends around their gym and exercise routine. Strauss says that, since he looks forward to seeing the people he works out with, he's always motivated and excited about exercising. And Rubin, with the help of a reliable gym community, has lost over a hundred pounds since his peak weight.

Others have discovered that improvement is often a case of prioritizing.

Jason Fried found a better balance by reducing his workout schedule. He realized that, for him, exercise was turning into yet another habit — one that he was using to justify other bad habits, like eating poorly and not getting enough sleep. So he cut back and is happier for it.

Sleeping is something Dr. Brené Brown thinks everyone should prioritize. In her experience, good sleep habits do more to improve your life than whatever diets and exercise plans you may be following.

Kevin Kelly has also experienced the benefits of prioritizing. For him, it was about learning how to delegate tasks so that he only has to deal with projects that really require his particular expertise.

Another secret to self-improvement is learning how to see things differently.

Janna Levin is a professor of physics and astronomy at Barnard College, and she used to think of obstacles as something that got in the way of a happy life. But Levin has since adjusted her outlook and found more happiness by recognizing that life is really a series of obstacles that, when approached calmly, offer the chance for you to learn and improve.

Bear Grylls, a former member of the British Special Forces and the star of his own Emmy-nominated survival show, realized that life was too short to waste time being fearful about the future or wishing he were somewhere else. Now, whether he's at home with his feet kicked up or waist-deep in a swamp, he embraces living in the moment.

> Rick Rubin is a lifelong pro-wrestling fan.

### 6. Dealing with a work overload means saying yes to yourself and no to others. 

As the Notorious B.I.G. track "Mo Money Mo Problems" affirmed back in the 90's, "The more money we come across, the more problems we see.'' So the author asked the mentors about their techniques for dealing with the difficulties that come with success.

What many agreed on is the need to reset and re-center yourself from time to time.

Some mentors, like Jason Fried and five-time weightlifting world champion Aniela Gregorek, do this by taking regular walks. Fried likes to find new areas he's never been to, since focusing on the new surroundings helps him clear his mind.

Gregorek prefers to walk in the forest, and cites the Japanese restorative practice known as "forest bathing." This involves letting the sounds, smells and cool air of the forest wash over you and refresh your mind and body.

Other restorative methods include taking naps, which is the preferred technique of Uber's chief brand officer, Bozoma Saint John; having sex, a recommendation from People's Choice Award-winning actor Ashton Kutcher; or taking a moment to think of all the things you have to be grateful for, which is how psychologist and best-selling author Dr. Jim Loehr resets himself.

But sometimes, when you know you're already stretched thin, you have to practice the art of saying no.

The celebrated authors Rabbi Lord Jonathan Sacks and Yuval Noah Harari both have people in their lives who help them decline all the incoming requests for speaking engagements.

But, of course, we don't all have the means to employ others to say no for us. When Neil Strauss has to come up with an answer, he now asks himself, "Am I only thinking about saying yes out of fear or guilt?" If this is the case, he'll then give a polite no.

When Kevin Kelly has to make a choice, he imagines that the invitation is for an event taking place tomorrow morning. If he'd still be willing to go, he'll say yes.

However, according to millionaire entrepreneur Gary Vaynerchuk, sometimes saying no, even though you want to, isn't the best choice. To stay open to life's many surprises, he'll say yes 20 percent of the time, even though it might not seem worth it.

### 7. Focus on your passion and don’t fret about the future. 

If there's one common question that mentors get, it's something like, "What piece of advice would you give someone who's just starting out in their career?"

When the author brought up this question, many of the mentors said that it's crucial not to get distracted by fears about the future and instead focus on the now.

The social-media guru and trend-setting entrepreneur Gary Vaynerchuk says it's all too common for people to worry about where they'll be in five years while wasting time drinking and watching TV. The truth is, if you make the most of each day, the future will take care of itself.

Sure, uncertainty can be scary, but rather than letting it paralyze you, you should take the advice of _Zen Habits_ blogger, Leo Babauta, and use it to spark your motivation and determination. Otherwise, you're bound to end up procrastinating and missing out on opportunities.

So should you start your career by following your passion?

According to podcaster and communications advisor Veronica Belmont, employers are likely to admire your ambition if they see you were hard at work on your passion before you even had a job.

In a similar vein, legendary skateboarder Tony Hawk believes that true success only comes when you're living the life you truly love.

However, there are mentors who believe a more strategic approach is called for. Kevin Kelly stresses the importance of devoting the necessary time to mastering a skill related to your passion. Once you've done this, you'll likely have your choice of jobs that will pay you to exercise that skill.

If you don't know what your passion is, you needn't panic. Yuval Noah Harari reminds us that we're living in a particularly fast-changing world, so your current dream job may not even exist ten or twenty years from now. So it's best to develop your emotional intelligence and work on becoming resilient. This will help you stay strong and balanced in an uncertain future.

And once you do settle on a profession, don't forget to follow Maria Sharapova's advice and say "please" and "thank you." No one likes a disrespectful person, regardless of how successful that person might be.

> _"I'm not worried about my years, because I'm squeezing the fuck out of my seconds, let alone my days." -_ Gary Vaynerchuk

### 8. Don't follow advice just because it's trendy or other people are doing it. 

We've heard a lot of good advice, but maybe you're curious about the _worst_ advice the mentors have received?

Ed Coan, who's set 71 different weightlifting-related world records, warns people against adopting techniques just because they're new and trendy. Remember, there's a good reason to trust the tried and true fundamentals that have stood the test of time.

Mark Bell, the founder of the renowned Super Training Gym in Sacramento, reminds people to think twice before trying to take on too much. Like a weightlifter who always tries to lift the most, you can end your career by overdoing it. So focus on working within your zone and what's optimal for you.

Most people would agree that it's wise to avoid advice that could reduce the quality of your work.

When starting out in a creative industry these days, it's common to be told about how important it is to build hype and grow your brand on social media. But this ranks far below the importance of focusing on your craft, especially when it comes to gaining satisfaction from your work.

Hollywood screenwriter Steven Pressfield can attest to how superficial the rewards of social media are. True satisfaction comes from getting deep into your work over a significant period of time. Put in the hours, and the rest should follow.

The pioneering cancer researcher Lewis Cantley thinks there's a trend of bad advice these days. Budding scientists are often warned against sharing their findings until they've published it in a scientific journal, but Cantley believes there should be more teamwork and a willingness to share. Such cooperation wouldn't only make science more fun; it'd likely lead to more progress.

However, if you do have teammates, you should always avoid blindly following their lead.

Ray Dalio warns against investing in a market just because someone else is. Chances are, that market is being flooded and prices are going up meaning you'll likely overpay.

Similarly, the advice of industry experts isn't always sound. Scott Belsky, the cofounder of the social-media platform Behance, believes that it's not experts, but rather outsiders like the people behind Airbnb and Uber who really revolutionize industries by breaking the established rules.

### 9. Remember to value yourself and others. 

At one point, the author asked all the mentors to consider what piece of advice they would give to everyone in the world if given the opportunity.

Many of them focused on asking people to be good to one another.

The CEO of Salesforce, Marc Benioff, is a committed philanthropist, and he would advise everyone to find out how they can help their local public school.

This help needn't take the form of a donation. It could be asking the school principal if there's anything you can do to help, like mentoring students by sharing what you do for a living and showing them how they could follow in your footsteps. Even small gestures that only take a couple of hours out of your day can transform a student's life.

Author, philosopher and neuroscientist Sam Harris would advise everyone to be more reasonable in their dealings with other human beings. Talking and exchanging ideas are the most effective ways to move forward as a species and overcome the challenges that lay ahead. Violence, on the other hand, usually prevents progress.

Other mentors urge everyone to make the most of what we've been given.

Ray Dalio and Scott Belsky remind people to stay open-minded and seek out the less obvious opportunities. While joining a struggling team might not look immediately appealing, it would likely be more rewarding than heading a flourishing one, because there'd be more room for change and surprises.

But even opportunities like this aren't going to magically show up at your doorstep. You have to be ready and open to jump aboard when the time is right.

Which brings us to the last piece of advice: if you miss a good opportunity, don't beat yourself up. Contrary to what you might be feeling, it isn't the end of the world or of your chances.

Remember the words of Leo Babauta: "You are good enough as you are."

As Maria Sharapova puts it, you should strive to be your unique, original self, and no one else. And don't let the words and actions of other people, or a bunch of "what ifs," define who you are. Be your own leader, and maybe you'll become a mentor to the next generation yourself.

### 10. Final summary 

The key message in this book:

**There's more than one way to live a successful and fulfilling life. The advice given by mentors, ranging from reading more and investing in yourself to embracing failure and saying "no" more often, can help you to find inspiration and guidance to help you no matter when you are in your own journey.**

Actionable advice:

**Don't let distractions steal your attention.**

Your attention is even more valuable than your time — which is why everyone wants a piece of it. So remember to protect your attention and use it wisely. Arianna Huffington regularly "scrambles" the apps on her phone, rearranging their location so that, whenever she starts automatically checking them, she's forced to pause and notice what she's doing. This gives her the opportunity take a breath and course correct.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Tools of Titans_** **by Tim Ferris**

Tools of Titans (2016) details the stories, strategies and successes of some of the most inspirational achievers, thinkers and doers of modern times. These blinks will teach you how to strengthen your body and your mind, all while building your creative business.
---

### Tim Ferriss

Tim Ferriss, sometimes referred to as "the human guinea pig," has conducted countless experiments on himself in a quest to find the best and most effective self-help techniques. His diligence has resulted in his best-selling _4-Hour_ series of books, which include _The 4-Hour Workweek_ and _The 4-Hour Body_. He's worked as a consultant for top companies like Facebook and Twitter, and hosts his own popular podcast, _The Tim Ferriss Show_.

