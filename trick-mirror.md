---
id: 5e57d96a6cee07000673b9fa
slug: trick-mirror-en
published_date: 2020-02-28T00:00:00.000+00:00
author: Jia Tolentino
title: Trick Mirror
subtitle: Reflections on Self-Delusion
main_color: None
text_color: None
---

# Trick Mirror

_Reflections on Self-Delusion_

**Jia Tolentino**

_Trick Mirror_ (2019) is the long-awaited first collection of writer and essayist Jia Tolentino. In nine intertwined stories, she tells of the trends and ideas — as well as the personal and collective delusions — that have shaped her life, our country, and the culture. Examining everything from the internet to workout crazes to modern marriage, Tolentino interweaves the personal and political, calling to mind great feminist writers like Susan Sontag and Joan Didion.

---
### 1. What’s in it for me? See our confusing times through the eyes of a radical thinker. 

The thoughts and stories in these blinks were gathered during a time of crisis _._

Jia Tolentino began writing them just after the election of Donald Trump in 2016. By telling them, she attempts to reckon with the cultural, social, and political changes that preceded and followed this historical event. 

In doing so, Jia considers the internet and reality TV, the similarities between drugs and religion, the pitfalls of liberal feminism, and many other phenomena that define our time. Threading the personal into the cultural into the political, she shows how our collective narratives shape our individual ones — and how they have shaped her own.

Like a trick mirror, these blinks may bend and transform your view of yourself and the world you live in.

In these blinks, you'll discover

  * how athleisure became the quintessential fashion statement of our time; 

  * what compels modern grifters; and

  * why female celebrities are flawed ambassadors of feminism.

### 2. The internet has turned us all into narcissists – and our narcissism into business. 

"I'm literally addicted to the web," Jia Tolentino wrote in her online diary at the age of 12. 

That was in the early 2000s, before we _all_ became addicted to the web. Actually, back then, the internet wasn't much of a web at all. It was more of a loose collection of people's private obsessions. Services like _GeoCities_ allowed users to build personal websites dedicated to golfing, or Ricky Martin, or, in Jia's case, the TV series _Dawson's Creek._

But then, the Web 2.0 rolled around. With the advent of blogging, people's online lives started to connect and intermingle. As more and more people started to share their lives online, it became harder to participate in society without having an online presence on centralized platforms such as Myspace– or later, Facebook and Instagram. 

**The key message here is: The internet has turned us all into narcissists** **–** **and our narcissism into business.**

While the Web 1.0 was ruled by the idea that you could be anyone you wanted online, the Web 2.0 closely entwined our online lives and our real lives. Of course, we all wanted to make sure that only the best parts of our real lives ended up online.

In a world governed by hashtags, comments, and likes, we learned to post only the most flattering selfies. We learned to express either our most relatable or our most extreme opinions. And we learned to align ourselves with trendy political causes to demonstrate our virtue to the world.

Of course, people put on a show all the time in real life, too. In 1959, sociologist Erving Goffman described how any human interaction requires us to perform a certain role.

At a job interview, for example, you might play the conscientious worker. But around your friends, you're known as the bubbly entertainer. It's only when you're not being watched that you might feel as if your mask is coming off.

The problem is that the internet doesn't have scene breaks, or a backstage, for this to happen. Our online selves need to perform all the time, for everyone. Your Facebook self, for example, interacts with your boss, your spouse, and your 11-year-old niece alike. 

And companies like Facebook have made a business out of our complicated online performance. They no longer profit by selling us stuff, but by selling _us_ — our identities, our relationships, and of course, our data.

> _"[T]he Internet brings the 'I' into everything."_

### 3. Appearing on a reality TV show can be a way to indulge your self-delusion – and to reckon with it. 

There's a dark secret in Jia's past, one that she's tried to hide, even from herself: when she was 16, she appeared on a reality TV show. 

_Girls v. Boys_ _–_ _Puerto Rico_ was the fourth season of a low-budget production from the early 2000s. In it, Jia and three other girls competed against a team of four adolescent boys in various physical challenges for a prize of $50,000.

Young Jia had been at the mall with her parents when they saw a casting booth for the show. Jokingly, her parents suggested that she try out. So Jia recorded a goofy casting video and didn't think anything more of it. But three weeks later, the producers called to invite her on the show. 

**The key message here is: Appearing on a reality TV show can be a way to indulge your self-delusion** **–** **and to reckon with it.**

The show was filmed in Puerto Rico over the course of three weeks, during which Jia lived in a house with the seven other contestants. 

When the show aired, Jia invited her friends over to watch the first episode. But seeing the exaggerated version of herself that she played on the show made her so uncomfortable that she couldn't finish the rest of the episodes until 13 years later. Even then, older Jia couldn't help but cringe. 

Young Jia is desperate to make herself seem special. On the first day of the show, she volunteers to eat a plate of mayo to impress the others. Throughout the show, she presents as a smart girl with firm principles. When one boy tries to make out with her, for example, she declines and smugly rebuffs him because she has "morals." 

Looking back, Jia thinks that the show fueled her narcissistic fantasy that her life was a movie. 

Indeed, the show's producer, Jessica Morgan Richter, later confirmed that all of them had been cast to represent recognizable teenage archetypes. Like in any high school comedy, the show had a good guy, a jock, a prissy girl, and a goofball. Jia was cast as the tightly wound know-it-all.

Older Jia reflects that her experience with reality TV prepared her well for the internet. She'd learned at a young age that you may not be able to change how people perceive you, but you _can_ amplify your traits so that people will pay more attention to you.

### 4. Beauty ideals for women haven’t become any less demanding – they’ve just shapeshifted. 

Feminism is now mainstream __ — and for the most part, that's a good thing.

Today, even "old school" women's magazines like _Cosmopolitan_ push the idea that women should decide for themselves how they want to look, love, and live.

But mainstream feminism has its limits. In order to become mainstream, it had to make concessions to the two systems that still govern our world: capitalism and patriarchy. This tamer feminism places great value on the success of individual women but de-emphasizes the collective action that threatens these systems.

One area where mainstream feminism fails us is female beauty standards. As Naomi Wolf observed in the modern feminist classic _The Beauty Myth_, expectations for women's appearances have exploded ever since women gained financial independence from men.

**The key message is: Beauty ideals for women haven't become any less demanding** ** __****–** **they've just shapeshifted.**

Mainstream feminism would like us to believe that our beauty standards have greatly broadened since Wolf wrote her book. The same women's magazines that were encouraging us to diet in the 1990s are now pushing the "body positive" message that anyone can be beautiful, no matter their size or shape.

In reality, our idea of beauty hasn't changed all that much. We've just changed how we talk about it. 

Instead of doing "beauty work," as Wolfe called it, the modern ideal woman practices _self-care_. Instead of diets, she goes on _cleanses_.

In fact, in our digital economy, beauty may matter more than ever before. On social media, good looks translate to social capital, and social capital is readily exchanged for economic capital. 

Many women today are taught to see their appearance as an asset to their personal brand, which they have to continuously improve to increase their performance on the market. The pressure to be "naturally" beautiful is now so intense that for many women, traditional beauty tools are no longer enough. They prefer lip fillers over makeup and rigorous workout routines over shapewear. They don't just want to improve surface appearances, they want to optimize themselves from the inside out.

No phenomenon exemplifies this obsession with self-optimization better than the rise of _athleisure_ — expensive athletic clothing made for casual wear, which signals our dedication to improving our appearance through exercise. 

And while we're busy figuring out how we can be beautiful "on our own terms," we forget about the more emancipatory option of devaluing the concept of beauty altogether.

### 5. Women in literature are often characterized as innocent children, sad teenagers, and bitter adults. 

If you're an avid reader, you know the feeling of remembering a favorite character's life better than your own. 

For young Jia, the heroines of her favorite novels were like an extension of herself. But reflecting on their stories later, she found that their lives all followed an eerily similar trajectory.

**The key message here is: Women in literature are often characterized as innocent children, sad teenagers, and bitter adults.**

Heroines of children's classics are usually depicted as curious, inventive, and resilient. Jo March of _Little Women_ writes plays for her sisters to act in; Laura Ingalls of the _Little House_ series becomes a seamstress to help out her family; and Hermione Granger uses magic to take more classes at Hogwarts.

Even when they experience terrible things, the quirky personalities of these young heroines remain unmarked by trauma. Literary girls are allowed to be "human before becoming a woman," as feminist philosopher Simone de Beauvoir wrote in 1949.

When they _do_ become women, however, marriage and motherhood await them. In _Little Men,_ the last book in the _Little Women_ trilogy, rebellious Jo March ends up a wife and full-time foster mother. 

Adolescent heroines are aware of the fate that society envisions for them. They are sadder, angrier, and more resigned than their younger counterparts. In dystopian novels like _The Hunger Games,_ the teen heroine is often an isolated exceptionalist struggling against an unfair system at great personal risk. In romantic ones like _Twilight,_ she accepts what society expects of her and takes her place next to a man whose life comes to determine hers. 

The heroines of adult literature have already fallen into that trap. Leo Tolstoy's _Anna Karenina_ and Gustave Flaubert's _Madame Bovary_, for example, are both intelligent women stuck in loveless marriages out of economic necessity. When Anna Karenina gets pregnant from an affair, she sees no other option than to commit suicide.

The narratives of the classic literary heroines __ — young or old, defiant or resigned __ — provide templates for understanding our own narratives. But their stories are confined by marriage, sex, and motherhood in ways that ours need not be. 

For Jia, as the daughter of Filipino-Canadian immigrants, the limitations of identifying with these exclusively white characters soon became obvious. She now sees these literary heroines not as her sisters, but as mother figures: with a sense of gratitude, but a firm determination to become _more_ than they were allowed to be. 

In charting her own path as a woman in real life, she takes a cue from essayist Rebecca Solnit, who once wrote, "There is no good answer to being a woman; the art may instead lie in how we refuse the question."

### 6. Religious ecstasy is not so different from chemical ecstasy. 

Jia spent much of her early life in a church so big that it's nicknamed the "Repentagon."

It was one of those megachurches typical of Texas: a $97 billion campus complete with a preschool, kindergarten, and high school. As a child, Jia felt at home there. She happily attended the church services and fondly remembers how pure and blessed the love of God could make her feel. 

But in middle school, Jia started having doubts about her conservative Christian education. She thought it was strange that she wasn't allowed to watch Disney movies or use a notebook with a peace sign on the cover. 

She became increasingly critical of organized religion. From high school onwards, she tried to avoid the church altogether. 

But it wasn't until she tried ecstasy for the first time at a concert in college that Jia stopped believing in God altogether. 

**The key message here is: Religious ecstasy is not so different from chemical ecstasy.**

Ecstasy, or MDMA, is an _empathogen_, meaning that it makes us become more compassionate. On ecstasy, we become more sensitive to our own emotions, are more empathetic to those of others, and feel a deep sense of connection between the two.

In the 1970s, many scientists were studying the mood-boosting and prosocial effects of MDMA in clinical trials. But after eight people died from overdoses, the drug was banned by the US government and placed in the same risk category as heroin. Later, ecstasy made its comeback in the 1990s rave scene and is now a widely used party drug.

For Jia, both religion and ecstasy provided a way to transcend herself __ — to feel like part of something bigger. Both experiences contained a bit of sin and salvation; of feeling "pure" and "naughty" at the same time. 

In fact, throughout history, people have described spiritual highs and drug highs in similar ways __ — including the comedown. Fourteenth-century anchorite Julian of Norwich described her religious visions as "waves of luminous color" that filled her with "a strange peace and joy," followed by a feeling that left her "oppressed, weary of [herself], and so disgusted with [...] life that [she] could hardly bear to live." 

For Jia, the analogy holds true, too. As a kid in church, God felt like everything to her. As an adult on ecstasy, everything felt like God to her.

> _"You don't have to believe a revelation to hold on to it."_

### 7. Scamming is the quintessential ethos of our times. 

It was supposed to be the most exclusive music festival ever. Instead, it ended up becoming the funniest flop in recent memory.

We're talking, of course, about Fyre Festival. This luxury music festival was scheduled to take place on an island in the Bahamas in April 2017. But when guests arrived at the scene, there were none of the promised exclusive performances, VIP yurts, or even a stage. There were only wet mattresses, disaster-relief tents, and soggy sandwiches. 

Fyre Festival was spawned by self-proclaimed entrepreneur Billy McFarland, whose previous enterprise Magnises had just gone belly-up. __ This VIP club for well-off millennials promised its members exclusive access to a clubhouse, concerts, and networking events __ — most of which never materialized. 

**The key message here is: Scamming is the quintessential ethos of our times.**

It quickly became clear that McFarland was a professional con man. The word "con man" derives from "confidence man," the title newspapers gave to petty criminal William Thomson in the 1900s. Thomson would eloquently convince people to entrust him with their watches for a day __ — and then never return them. But McFarland also differs from traditional con men like Thomson in a crucial way. 

Sure, Fyre Festival was a much larger scam. But unlike Thomson, McFarland also seems to have believed significant chunks of his own bullshit. Even when the festival disaster passed the point of no return, he instructed employees to keep going, telling them he knew that they could pull it off.

McFarland's ability to delude himself __ — and by extension his team and his customers __ — was the reason he was able to take the Fyre Festival scam as far as he did. Moreover, his belief in "fake it 'til you make it" is shared by many people today. As a result, we live in the era of the scam.

Some scams are so obvious and widespread that we've become numb to their deceit. Facebook, for instance, is essentially a global grift operation for mining people's personal data — and that's hardly a secret. But Facebook is so ingrained in our everyday lives that it's easy to forget about its business model.

Other scams are so deeply embedded at the core of our economic system that it's hard to think of the world without them. Consider the 2008 financial crisis, which was caused by shadowy loans to barely solvent home buyers. Or take the scam of student loans, which plunge people into crushing debt.

Nowadays, there seem to be fewer and fewer opportunities to survive in a morally defensible way. Scammers like Zuckerberg or Donald Trump embody the maxim that you should profit from others in any way you can __ — and society rewards them with extreme wealth and power.

### 8. Despite feminism’s many accomplishments, sexual violence is still deeply ingrained in our society. 

Jia wasn't planning to attend the University of Virginia in Charlottesville when she visited it in her senior year of high school. But when she saw the green, sun-lit campus, she fell in love and knew it would be an idyllic place to kick off her writing career. 

Later, after finishing her MFA and moving to New York, Jia didn't think much of UVA anymore. But then, in 2014, _Rolling Stone_ published a story that placed her old university at the top of the news cycle.

The piece, written by journalist Sabrina Rubin Erderly, was a graphic account of how members of a UVA fraternity had raped a freshman named Jackie at a party. According to Erderly, the report was based on Jackie's own telling of the story.

Jia knew that UVA had a history of sexual violence — and a history of ignoring the issue, as she did while she studied there. In 1984, for instance, Liz Secura, a 17-year-old freshman, had been gang-raped by members of a fraternity. When she reported the assault to the dean, he dismissed her experience as just a "rough night." In fact, before Jackie's story was published, not a single UVA student had been expelled for sexual assault. 

**The key message here is: Despite feminism's many accomplishments, sexual violence is still deeply ingrained in our society.**

The problem was, Jackie's story wasn't true. 

Soon after it was published, people started to notice inconsistencies. Jackie and Erderly refused to clarify the details, like the names of the attackers. Later, it was revealed there had been no party at the fraternity on the night in question.

Regardless of the story's factual errors, the popularity of the story suggested that the public was ready to discuss sexual assault. Soon, stories about rapes on other university campuses surfaced. A terrifying truth began to emerge: sexual assault wasn't an anomaly, but an ordinary experience for countless students.

Many universities were caught in the cleft between honorable appearances and a record of sexual abuse __ — which, in the case of fraternities, they routinely overlooked.

The _Rolling Stone_ article was the first major piece of journalism to tackle the awful reality of campus sexual assault. But it also increased the pressure on journalists to report more accurately, and fact-check more meticulously, than Erderly had in Jackie's case. The excellent, careful reporting on the allegations against Bill Cosby, for example, were a testament to this learning process. Despite its flaws, Jackie's story may have opened the floodgates for future reporting that in time will change the tide.

### 9. Our new obsession with “difficult” female celebrities is diluting the wider feminist project. 

If you believe women's magazines of recent years, celebrities are our new feminist superheroes. Britney Spears is a sympathetic antiheroine broken by her fame. Kim Kardashian is a shrewd businesswoman who uses her sexuality to challenge the patriarchy. Caitlyn Jenner is a brave LGBTQ activist.

We celebrate these women for being "difficult" and "complicated," and we rewrite their narratives in a feminist light.

But the bar for becoming a complicated she-ro is lower than ever. Today, any woman who has succeeded in the face of sexist criticism is hailed as a groundbreaking feminist. The truth is, any woman who has succeeded at all has done so in the face of sexist criticism __ — because no woman has led a life completely free from sexism.

**The key message here is: Our new obsession with "difficult" female celebrities is diluting the wider feminist project.**

The definition of feminism we apply to successful women is dangerously slippery. Under it, any criticism of a woman can be called anti-feminist, and any woman who receives criticism becomes a feminist.

Consequently, one could easily use this definition to describe the women in Trump's cabinet as feminist icons __ — even as they support his sexist administration. Of course, people have tried to do exactly that. In 2017, the _New Yorker_ published an essay titled "Kelley Conway Is a Star," and the _New York Times_ ran a column on Melania Trump's "quiet radicalism."

How did it come to this? 

Well, feminism is an enormous, complicated project. When we oppose a system as vast as patriarchy, we sometimes end up ventriloquizing its sexist arguments and weakening our own. For example, when Hillary Clinton faced blunt sexist attacks during her 2016 presidential run, her supporters often defended her with equally blunt arguments __ — implying that her status as a woman was the one and only reason to vote for her.

We also tend to forget that famous women have one huge advantage over the rest of womanhood: money. Caitlyn Jenner's coming-out as a trans woman, for example, was eased enormously by the fact that she's rich, white, and famous. So much so, in fact, that she now has no problem supporting Trump and his anti-LGBTQ policies. 

We need to keep in mind that when our feminism centers around a small number of individual exceptionalists, the struggles of ordinary women fall out of focus. That's why, instead of analyzing the supposed feminism of famous women, we'd be better off analyzing our own.

### 10. Modern marriage is a massive industry with sexist origins. 

Love it or hate it, at one point you'll reach the age when _all_ your friends are getting married. 

For Jia and her partner Andrew, this point came nine years ago __ — and hasn't ended since. The couple has been invited to no fewer than 49 weddings. 

It's easy for Jia to understand why people get married. But when she tells them that _she_ doesn't want to, the understanding is seldom returned. "But it's such a beautiful tradition!" people exclaim.

Except it's not. 

Weddings as we know them __ — with bridal showers, receptions, and ring exchanges __ — were invented by savvy businesses over the course of the last 100 years. 

**The key message here is: Modern marriage is a massive industry with sexist origins.**

It all started with Queen Victoria's white wedding dress. For centuries, weddings had been simple, private ceremonies. But after Queen Victoria married Prince Albert in 1840, newspapers couldn't stop writing about her beautiful wardrobe and elaborate ceremony. The Victorian elites started copying their queen. And with the emergence of the middle class in the early twentieth century, the consumer market for weddings really took off. 

In 1924, Marshall Fielder invented the wedding registry. And when ad writer France Gerety coined the slogan "A Diamond is Forever" in the 1940s, diamond engagement rings became all but mandatory. 

Today, weddings are an $11 billion industry. The average ceremony costs about $30,000 in total, with $3,000 reserved for the wedding planner alone. You can now book bridal fitness boot camps, an engagement photographer, and even social media consultants for your big day. 

All the glitz and glamour obscures the actual historical purpose of marriage. Before businesses began marketing marriage as a ritual of love, it was a purely economical transaction. For a long time, it was impossible for women to be financially independent outside of marriage. In fact, until 1974, American women still needed their husband's signature when applying for a credit card.

Of course, society has made some strides since then. Marriage today is more diverse and more equal. But when it comes to financial risk, for example, women still carry an uneven load. On average, they lose 20% of their wealth after divorce, while their ex-husband's wealth goes up. 

All of this suggests that the extravagant ceremonies are just a trick to make women conform to tradition __ — women are expected to cram all their self-interest into a single day, so that it won't interfere with their new life as a duty-bound wife.

### 11. Final summary 

The key message in these blinks:

**No matter how trivial they might seem, cultural trends like our social media addictions, celebrity obsessions, and love for lavish weddings can help us better understand our social, political, and economic systems. They are trick mirrors that reflect not only our personal identities and relationships — but also our collective ideas that shape the world we inhabit.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Year of Magical Thinking,_** **by Joan Didion**

Critics and contemporaries sometimes speak of Jia Tolentino as the new Joan Didion: a uniquely lucid feminist thinker and exceptional writer with a keen eye for cultural trends and social dynamics.

American journalist and essayist Joan Didion is most famous for her incredible memoir, which she wrote after the death of her husband in 2003. __ In case you're not familiar with this brilliant work of nonfiction, you can dive right into our blinks to _The Year of Magical Thinking._
---

### Jia Tolentino

Jia Tolentino is a writer and editor. After studying at the University of Virginia, she served with the Peace Corps in Kyrgyzstan and received her MFA in fiction from the University of Michigan. She has previously worked as an editor for feminist media outlets _Jezebel_ and _Hairpin,_ and is now a staff writer at the _New Yorker._

