---
id: 544e04f33630630008880000
slug: true-enough-en
published_date: 2014-10-29T00:00:00.000+00:00
author: Farhad Manjoo
title: True Enough
subtitle: Learning to Live in a Post-Fact Society
main_color: 4F9BCD
text_color: 2279B2
---

# True Enough

_Learning to Live in a Post-Fact Society_

**Farhad Manjoo**

_True Enough_ is an exploration of how facts are dealt with in the news and media. It explains how our preconceptions and opinions shape the way we experience reality, and how media producers manipulate us by using our notions to their advantage.

---
### 1. What's in it for me? Learn how the media is manipulating you. 

Thanks to the Digital Revolution, the media and the news landscape have undergone massive changes in recent years. Some changes have been good, but some have been detrimental, especially changes in media. In _True Enough_, Farhad Manjoo shows us how easily PR firms, false experts, news agencies and conspiracy theorists can use existing media outlets to manipulate us. He also shows how _you,_ as a consumer, are biased against certain pieces of news and how your preexisting beliefs deeply affect the way you process new information.

In these blinks, you'll find out why it's now so much easier for producers to bend "facts," and the ways they manage to do it. You'll also learn:

  * why unfounded conspiracy theories can spread so rapidly;

  * why opposing football fans can watch the same game footage and see different results;

  * how a cigarette company manipulated smokers into fighting anti-smoking laws without them knowing;

  * how a group of conservative veterans might've swung the 2004 presidential election and

  * why Democrats and Republicans both think the media is biased against them.

### 2. The media landscape has become fragmented, which makes it easier to spread misinformation. 

Before the internet, media was distributed by a small handful of news channels on TV, radio and newspaper. Needless to say, things are different now.

Now that nearly everyone uses the internet, our news and information channels have become fragmented, meaning we can get our news from nearly anywhere.

We can record proof of events with digital cameras, for instance. We can spread and receive ideas through blogs. We can now be the producers, distributors and editors of our own news.

This development allows people to spread their opinions, even if they're wrong.

The _Swift Boat Veterans_, a right-wing American group, did exactly that in 2004. They opposed John Kerry in the presidential election, so they started a campaign to discredit his participation in the Vietnam War.

The Swift Boat Veterans claimed that Kerry had been a poor soldier, and didn't truly qualify for the medals of honor he'd received. This was untrue, and there were many veteran testimonies and countless documents to prove it.

Although their information was false, the Swift Boat Veterans succeeded in helping take down Kerry. At first, no respectable news channel would listen to them, and their press conference didn't receive any coverage. But they kept relentlessly spreading their message.

They went on right-wing talk shows and promoted their website. There, they offered an open letter to be signed and forwarded to friends and relatives. This eventually snowballed enough to get picked up by the mainstream media, which changed public opinion about Kerry's involvement in the war.

In the end, George W. Bush won the election by only two percentage points. The veterans might have swung the results with their misinformation.

The internet is now so accessible that we no longer need mass media to change a public opinion. Feats like the one pulled off by the Swift Boat Veterans are even easier now.

### 3. Our preconceived ideas affect our perception, and even video evidence can't always change that. 

There's now an abundance of people taking photos and videos at any significant event. We can access these materials online, but they don't necessarily make us agree on what really happened.

Even an overwhelming amount of coverage doesn't eliminate the uncertainty of what truly happened at important events.

During the terrorist attacks of 9/11, for example, at least 30 television photographers caught the impact of the plane into the South Tower. Despite this coverage, 42 percent of Americans believe the attacks were not properly investigated.

A businessman named Phillip Jayhan has advocated a theory called _MIHOP_, which stands for "Made It Happen On Purpose." He believes the American government itself carried out the attacks, and he asserts that he clearly sees a missile coming from the plane on the video tapes.

Many people believe this theory, even though countless experts have dismissed it as false. The extensive footage of the plane hitting the building still isn't enough to make everyone agree that it occurred.

One reason we can maintain our uncertainty even after seeing clear evidence is that our preexisting beliefs partly determine our perception.

A 1951 football game between Princeton and Dartmouth illustrated this well. Fans of both teams were convinced that the other team had played dirty.

A psychologist and sociologist decided to investigate these differing perceptions. They showed the same game footage to groups of Dartmouth fans, and groups of Princeton fans, and let them rate both team's offenses.

As you might expect, the fans disagreed which team had disregarded the rules more. The researched found that the opposing fans saw two different games, even when they tried to watch the footage objectively. They concluded that their preexisting beliefs shaped the way they processed the information.

Video proof doesn't always lead to definitive agreement on what really happened.

### 4. We tend to seek out information that confirms what we already believe, even if it's dangerous for us. 

Our beliefs don't only shape _how_ we see things, they also determine _which_ things we see in the first place.

We're all at risk for falling to a bias called _selective exposure_, which makes us seek out information that's already aligned with our views.

In 1967, researchers Timothy Brock and Joe Balloun investigated how people would react to information that contradicted their beliefs. They made smokers and nonsmokers listen to two different radio programs, which either stressed the link between smoking and lung cancer, or dismissed the link. There was also a lot of static noise in the programs.

The participants were asked to rate how persuasive the speakers had been. They could also choose to erase the static noise by pressing a button, which the researchers interpreted as a sign of interest in the program.

The smokers pressed the button to eliminate static much more often when listening to the program that dismissed the lung cancer link. Naturally, nonsmokers did the same in reverse.

We prefer listening to information that confirms our beliefs. And that can sometimes hurt us.

Christine Maggiore was a sad example. She accepted the unconventional view that HIV didn't cause AIDS. Even though she was diagnosed with HIV, she didn't take any medication and breastfed her children. Her 3-year-old became sick, but she didn't tell her doctor about the possibility of an HIV infection because she didn't believe in it.

Her daughter eventually died, and the autopsy strongly suggested that her death was related to AIDS. Maggiore prevented her daughter from getting proper treatment because she kept listening exclusively to information she agreed with.

We're all prone to have a selective exposure bias, and sometimes it can have very grave consequences.

### 5. Republicans and Democrats prefer different news, but they both think the media is biased against them. 

Have you ever noticed that people from different political backgrounds seem to be accessing completely different news material? This is especially noticeable with Republicans and Democrats in the US.

Republicans tend to have a stronger preference than Democrats for information that's already in line with their beliefs. In 1964, a psychologist named Aaron Lowin experimented with this phenomenon.

Lowin sent advertisements to Democrats and Republicans for brochures for different political candidates. The brochures contained either weak or strong messages, which were in line with either Republican or Democratic views. The people who received them could then choose which ones to order and read about further.

Democrats ordered all kinds of brochures. Republicans, on the other hand, preferred the brochures that had strong messages they already agreed with.

You can still see this pattern today. In 2005, researchers found that conservative and liberal blogs both favored sources in line with their partisan beliefs. This is truer for conservative blogs, however, as they tend to link only to other conservative blogs.

Democrats and Republicans _do_ have something important in common, however: they both think the media is biased against them.

Both parties believe that the mainstream media presents more messages against them, even though numerous studies have shown that there are many programs targeted at both groups. The researchers Ross and Lepper dubbed this the _Hostile Media Phenomenon_.

When Democrats and Republicans were asked to recall what they'd seen on the news, they both remembered more facts and arguments from the other side. They're both convinced of the bias towards the other side, even though there is no evidence for it.

> _"The bias we see in the news isn't strategic. It's real. It's real to us, at least, and that's as real as it gets."_

### 6. When news channels spread their propaganda, they must be careful how they do it. 

It's easy for false information to spread, but media corporations still have to be cautious about _how_ they spread their propaganda.

Most importantly, news channels should never try to twist a _high feedback_ topic to fit a certain agenda. High feedback topics are those that involve hard facts which can be easily checked, like the weather, sports results or the stock market.

If you hear from a news source that your favorite soccer team won the national championships, you'll check this fact if you know they've been having a bad year. If you find that the news channel lied to you, you probably won't seek it out again.

Because it's easy to verify the facts, news channels rarely lie about high feedback topics. Instead, they use false information to twist _low feedback_ topics.

Low feedback topics are things like health care, tax policy or the necessity of war. These topics are somewhat elusive, because they don't have definite truths that can be easily checked.

Global warming, for example, is a classic low feedback topic. _Fox News_ constantly doubts global warming, by pushing interpretations of information in a way that argues against it.

For instance, _Fox News_ might twist the occurrence of a drought, a flood or a good harvest as evidence that climate change isn't happening. In those cases, the facts they present will mostly be true, but they'll be interpreted in a misleading way. They'll use real facts to present a false argument, and omit all other information or evidence for global warming's existence.

News channels like _Fox_ often present false information, but in a way that's hard to prove wrong, because their facts are correct. New channels need to make sure their propaganda is nuanced enough for people to be fooled by it.

> _"...Fox and its audience can live safely in the comfort of the lie — at least until they personally begin to feel the planet starting to heat up."_

### 7. Subtle PR campaigns are more effective than direct ones. 

Sometimes the media influences us quite directly, but other times it's sneakier. PR firms can develop and spread campaigns that we hardly notice, and they can have a big impact on nationwide discussions.

PR firms affect us indirectly, especially for products with falling popularity. They prefer indirectness because blunt campaigns aren't' as successful. This happened in the 1990s, when cigarette companies tried to counteract new laws that limited them.

_RJR_, the second largest tobacco firm in the US, had their PR department rally against the laws. They started a campaign called _Choice_. The campaign stressed that smokers and nonsmokers could coexist, and it encouraged smokers to get involved in the fight against their "choice" to smoke.

Each ad had the logo of _RJR_ on it. The campaign failed, because even smokers themselves opposed the cigarette companies. In the end, _Choice's_ effect was the opposite of what it intended: it created a huge backlash and was unsuccessful in changing the anti-smoking mood of the time.

_RJR_ realized that their campaign had failed, so they redesigned it in small but significant ways. They launched a new campaign, _GGOOB_ : "Get the Government Off Our Backs."

On the surface, _GGOOB_ wasn't focused on smoking. Instead, they demanded the government stop its regulatory policies in general. Part of their reasoning was, "once the government bans cigarettes, it'll go for liquor and fast food and buttermilk and who knows what else!"

The companies that funded the campaign never took credit — they styled it as a "grassroots" movement instead. This time, their approach was widely accepted. It helped prevent some of the harshest laws, such as a ban on cigarettes in the workplace.

Corporations like _RJR_ know you might be wary of believing them if their messages are too direct. They prefer to launch subtle campaigns, and influence you without you noticing.

### 8. Video production and distribution is much easier now, so conspiracy theories and advertisements can spread more rapidly. 

Until recently, video and audio production tools were very expensive. Production was limited to major news broadcasting channels, as few people could afford it.

Thanks to the digital revolution, this is no longer the case. Anyone with a camera can produce a movie, so people can promote their beliefs much more easily.

This happened in 2005, when Dylan Avery released his movie _Loose Change_. It argues that 9/11 was an inside job — a conspiracy theory that Avery himself believes. It had no distributor and wasn't released in theatres, but it still reached the top of the Google Video Charts.

Avery produced the film with his laptop and over-the-counter video software. It cost him less than $2000. These days, it's very cheap to create a movie and spread it to people.

And it isn't only conspiracy theories that can spread — advertisers can also spread material easier. The video system _Pathfire_ makes this especially easy for them.

Pathfire is a program that gives TV stations access to video material from all over the world. It's used by advertisers, who make short clips called _Video News Releases_ ( _VNRs_ ). About 90 percent of TV stations prefer to use these clips, because it's cheaper than recording original footage.

This means that advertisements are inserted into the news. So, for example, in a "news" piece about National Pancake Week, producers used a clip from a company that produces pancake mix. In this way, advertisers can influence the news without us realizing it.

When so many people are able to produce video content, it becomes very difficult to escape false information and avoid advertisements. The integrity of the news becomes compromised when it can be hijacked by advertising companies.

> _"In practice, what propagandists are doing is simpler to describe: they've mastered a new way to lie."_

### 9. It's much harder to distinguish real and false experts in our current media landscape. 

Imagine the commotion that would result if a doctor was discovered to be practicing without a license. Well, a very similar thing is actually happening in our news.

Because everyone's voice can now be heard online, it's becoming harder and harder to distinguish real experts from false ones.

This happened in 2004, in the aftermath of the presidential election. Kathy Dopp, a mathematician, saw a statistical anomaly in the election results and completed a report about it.

She concluded that the 2004 election had been hacked by Republicans, and she posted her analysis online. Her calculations were mathematically correct, but she didn't interpret the figures correctly because she wasn't a political scientist.

Numerous real experts refuted Dopp's conclusions, but they still circulated on the news, and were even referenced by former president Bill Clinton! Nowadays, even people who aren't true experts can influence the media.

We're especially prone to believing people when they can impress us with jargon in a convincing manner. In 1970, a researcher named John Ware conducted an experiment on this phenomenon.

Ware introduced a television actor named Dr. Fox into a training conference of academics. Dr. Fox delivered a speech on an obscure subject the other academics weren't very familiar with. The speech contained several references to unrelated topics, and was generally nonsensical, but Dr. Fox delivered it charismatically.

After the speech, all the faculty stated that Dr. Fox had stimulated their thinking, and they were impressed by his expertise. Ware dubbed this the _Dr. Fox Effect_. The Dr. Fox Effect illustrates that the _way_ you say something might actually be more important than the _content_ of what you're saying.

### 10. The increase of false information in the media negatively affects our trust, which is harmful for society. 

So what does all this false information mean for our daily lives? Well, one alarming consequence is that makes us less trustful.

Trust has been shown to be important for a society's well-being. A researcher named Edward Banfield studied trust and society in Italy in 1954.

Banfield investigated a small town he called by the pseudonym Montegrano, to protect the residents' anonymity. While the rest of Italy was thriving, Montegrano was steeped in poverty.

Banfield found that it was largely due to their lack of trust for people outside their immediate families. Women there even lied about their symptoms to their doctors because they were afraid to display any weakness. They didn't know who might find out.

One reason that trust is so important is that a lack of it will lead to bribery or extortion. If people don't trust each other, they fear that any transaction with a stranger is a chance that someone might take advantage of them. This makes everyone anxious, and it hurts society as a whole in the long-run.

Since we know this lack of trust is harmful, it's disturbing that our trust for each other is continuously decreasing because of the media.

In 1960, nearly 60 percent of Americans agreed that they trusted most people. In 1990, it was below 40 percent, and by 2006 it was only 32 percent.

Robert Putnam, a political scientist, showed that Americans have interacted with each other less and less since the 1960s. There are fewer churchgoers, sports teams, card game groups, volunteers and blood donors.

Putnam explains that this decreased interaction is caused by our falling trust for each other. Ultimately, the media is hurting society in this way when it shows false information. A good amount of trust is necessary for a healthy society to flourish.

### 11. Final summary 

The key message in this book:

**Our present media is incredibly deceiving. It's riddled with covert advertisements, false experts and misinformation. We further deceive ourselves by seeking out news that's already aligned with our views. These changes in the media have decreased our trust for each other, which is hurting us in the long-run.**

Actionable advice:

**Beware of Dr. Foxes.**

Sometimes people can convince others of their views even if they're incorrect or illogical, just by being charismatic. So the next time you watch a charming speaker, make sure you really focus on _what_ they're saying, not _how_ they're saying it. Don't let a Dr. Fox fool you.

**Watch out for covert PR campaigns.**

The next time you get excited about a new campaign or other movement, check out who the sponsors are. Make sure you aren't unwittingly supporting a PR campaign you don't really agree with.

**Suggested** **further** **reading:** ** _Trust Me, I'm Lying_** **by Ryan Holiday**

_Trust Me, I'm Lying_ is an in-depth exposé of today's news culture, which is primarily channeled through online media sites called blogs. By detailing his experiences with multi-million dollar public relations campaigns, the author takes us behind the scenes at today's most popular and influential blogs to paint an unsettling picture of why we shouldn't believe everything that is labeled as news.
---

### Farhad Manjoo

Farhad Manjoo is an author and journalist. He serves as a technology columnist for the _Wall Street Journal_, has written for the _New York Times_ since 2014 and contributes regularly to _NPR_.

