---
id: 536a0e676631340007d00000
slug: trust-agents-en
published_date: 2014-05-06T10:51:50.000+00:00
author: Chris Brogan and Julien Smith
title: Trust Agents
subtitle: Using the Web to Build Influence, Improve Reputation, and Earn Trust
main_color: D7825A
text_color: BD592A
---

# Trust Agents

_Using the Web to Build Influence, Improve Reputation, and Earn Trust_

**Chris Brogan and Julien Smith**

_Trust_ _Agents_ describes how anyone can use the web to become a trust agent — meaning someone who is well-versed in the tools of the web and who is very influential and trusted because of this.

---
### 1. What’s in it for me? Learn how to become an influencer and rally people to your cause. 

Whether you like it or not, the advent of the internet has forever changed the way we communicate and share information. Companies and marketers across the globe have begun to see that their TV adverts and billboards just aren't getting the job done anymore.

Herein lies a great opportunity for individuals who want to extend their own influence, spread their ideas and benefit the causes they care about. They can become trust agents: the savvy, warm, community-minded operators of the internet age.

In these blinks you'll find answers to the following questions:

  * Why aggressive selling and advertising is useless today, while humanity and compassion work wonders.

  * Why sometimes it may be a good idea to disparage your own products.

  * Why it can be useful to view life as a mere game where you score points.

> _"In social media, being human is the new black. People are the next revolution, and being active on the human-faced web is your company's best chance to grow its business in the coming years."_

### 2. The internet has spawned a new kind of marketer: trust agents. 

We all know how profoundly the emergence of the internet has affected our daily lives, but how should marketing techniques adapt to this major shift?

The most obvious effect is that the increased availability of information has led people to be less trustful of companies and products.

Just a few decades ago we tended to trust advertisements and salespeople — we had no choice. They were pretty much the only sources of information about products. These days, however, we can do our own research via Google, so we don't need to trust advertisers anymore.

However, even in these distrustful times, there are still some individuals who are trusted by their customers, and are thus very influential: _trust_ _agents_.

Though trust agents represent companies or products, they build up trust by being knowledgeable, yet also warm, human and community oriented. This is especially true in the realm of new technologies, where trust agents use their expertise to show customers how they can use new, unknown technologies to better their lives.

Trust agents generally build up their influence over the web.

For example, let's say you want to become a trust agent. To get started, you set up a public blog about your field of expertise, e.g., digital photography. This allows you to share your expertise with potentially millions of photographers, and since you wouldn't be selling anything, they would also see you as trustworthy.

But even though trust agents don't really sell things, at the end of the day they _are_ marketers and able to spot business opportunities that they can take advantage of thanks to the _social_ _capital_ they have built up.

For example, if your photography blog has lots of followers, you have social capital: People know you, respect you and are grateful to you for sharing. One way you could capitalize on this would be to publish an e-book based on your blog, as your loyal followers would probably be happy to buy it.

In the following blinks, let's look at some of the key traits of trust agents and how you can emulate them.

> _"In a way, the web is like your Hollywood agent: it speaks for you whenever you're not around to comment."_

### 3. Think of life as a game. 

Do you ever feel stressed out at work?

How about when you're playing a game, like football or Monopoly?

Probably not.

Somehow striving for success when you're playing a game seems like a simpler endeavor than attempting it in real life. So why not try to see real life as a game too? Certain elements of games can help you succeed.

First of all, games define clear goals, like reaching a certain number of points.

You should set similar clear goals in real life. For example, you could say you want your blog to have 10,000 followers within a year.

Second, games often also include an element of competition. You need to get into a spirit of friendly competition in real life too to push yourself to perform better. It's not that you want your competitors to do badly, you just should always try to be as good or better than them.

Third, games are hackable: experienced players find hidden codes and tricks that allow them to score more points.

You can do this in real life too. For example, your job is a game, probably with rules like, "You must be at the office from 8 a.m. to 4 p.m." But ask yourself, is this rule necessary? Could you reinvent this rule by suggesting to your boss that as long as the job gets done, she shouldn't really care where or when it happens? If so, you could start working from home to save commuting time.

The final lesson you can take from games is that while playing is fun, the biggest rewards lie in making your own game with your own rules.

The real life equivalent of developing your own game is going boldly where no one has gone before and trying something new. Though this is a bit risky, the potential payoff is huge. Consider the success Twitter achieved by bravely finding out what would happen if online communications were reduced to text message length.

### 4. To promote your product online, don’t focus on selling it. 

Everyone hates adverts online, whether spam emails or annoying pop-up windows.

This means that if you have a product you wish to promote, you clearly cannot rely on these old marketing methods.

Instead you need to become a trusted part of the community you're trying to reach. You won't get there by trying to sell them things, but rather by forging personal connections, being reliable and building personal credibility.

So if you're representing a product, don't try to force it upon people or sing its praises to anyone who'll listen. People hate that.

Instead, focus on being active in the community, and you'll find your product gets promoted on the side.

As an example of this, consider the media company Comcast. It actively searched for Twitter users who had tweeted about issues regarding the company, and responded by offering to help them. While this innovative and proactive type of customer service did not actually sell anything, it did generate positive word-of-mouth within the Twitter community and increased Comcast's brand value.

Another way to show how much you care about the community is by eschewing the promotion of your own ego in favor of showing interest in others.

For example, support up-and-coming bloggers in the community and honestly praise great products even if they are not the ones you represent. This shows that you care about the value delivered to the community more than your own personal gain, which makes you very trustworthy.

Sometimes this can even mean putting down your own product. For example, while working as a technology evangelist at Microsoft, Robert Scoble actually criticized Internet Explorer in his blog. This honesty made him more trusted and influential in the long run!

> _"Nobody minds buying, but everybody hates being sold to. Although people sell on the web all the time, they don't do it by selling."_

### 5. Start building online channels to reach more people and maximize the impact of your actions. 

Many of us enjoy watching funny YouTube videos or browsing our Facebook feeds, but these activities do sort of beg the question: are we really using the internet to its full potential?

When it comes to any new technology, this is what every aspiring trust agent must ask herself. This is because trust agents are expected to be technology savvy.

So what is the overall goal of new technologies?

Simply put: to help us do things better, more quickly, more efficiently. In other words, new technologies function as _leverage_, multiplying the impact of your actions.

For example, the invention of the airplane allows us to get to faraway places quickly, in hours instead of weeks.

The web provides similar increases in efficiency: instead of having to go the post office to send letters, you can just send emails.

But in fact, the internet's biggest potential for leverage comes from the fact that it allows you to reach millions of people easily.

This is the leverage trust agents use to become influential: they share their expertise on forums and blogs read by millions.

What's more, the trust agents' posts and comments work for them tirelessly around the clock, being read and shared even when the trust agent herself is asleep or on vacation.

So how can you take advantage of this leverage?

Start building _channels:_ online relationships with large groups of people whom you can communicate with easily.

Do this even if you don't _yet_ have a business or product you want to market, for when you do, you'll need a solid channel network already in place.

For example, if one day you decide you want to raise money for a good cause, you can use your Twitter followers as leverage to maximize the impact of your idea. Think about it: if you tweet a request for donations to 10,000 followers, you'll probably raise much more money than by just asking the handful of people you know at work.

### 6. Become an agent zero: the center of your networks. 

Do you know the kind of person who seems to know everyone at every party?

These people are _agent_ _zeroe_ s: they are at the center of the networks they belong to, meaning everyone knows them.

In fact, this is something you as an aspiring trust agent should also strive for, because agent zeroes are in a great position to spread new ideas throughout their networks.

What's more, you can further expand your reach by looking for other agent zeroes in other networks, as you can multiply your own influence by getting them to spread your ideas as well.

For example, let's say you're the agent zero in an online music forum. You might then find a Facebook group dedicated to local bands, and by reaching out to the founder, gain influence in that network as well.

To establish and build up networks, trust agents focus on selflessly helping the people in their own network. This makes them memorable and trustworthy.

So what does this mean in practice?

For example, let's say you want to become an influential music blogger.

The first thing you need to do is make people _aware_ of your existence, so start increasing your visibility online. Create accounts on social media platforms like Twitter, and consider creating your own home page. Then use tweets, comments, status updates, etc., to interact with the community you hope to influence, which in this case would be artists and music fans.

Next, you need to actually secure the _attention_ of people; it's not enough that they just know you exist.

You can start to do this by deciding on a few people you'd really like to meet — for example, music producers whom you really admire — and then introducing yourself to them at real life events. They may then start directing more people's attention to you by, for example, linking to your page in a tweet.

Finally, this attention will translate into _authority_. The more people follow your site, the trustworthier and more authoritative your voice will seem.

### 7. Online etiquette is simple: behave as you would in real life. 

If you've ever read the comments section of a YouTube clip or a CNN news story, you've no doubt already surmised that many people don't know how to behave on the internet.

But this observation prompts an interesting question: what rules of conduct should one follow online?

Quite simply, the same ones as offline.

For example, if you walk into a party in the real world without knowing many people, you would probably try to behave politely and participate in conversations, right?

Well, you should do the same if you're new to an online community. Read through the material on the forums and ask when you have questions. Try to be helpful and constructive in your comments, and reciprocate when others help you. The "golden rule" applies online as much as it does in real life: treat people as you would like to be treated yourself.

One difference between the internet and real life is its mixture of anonymity and transparency.

Many people use pseudonyms and anonymous identities when communicating on online forums. This is partially what causes bad behavior online.

However, this anonymity also promotes transparency, because it helps people to talk openly about their feelings and life experiences. This makes the internet a great place to learn about others and their emotions, which in turn promotes empathy.

As a trust agent you should also open up about yourself: share a personal story every now and then, because it helps people feel connected to you in a more human way.

Beware though: the internet has a long memory, so always ask yourself whether you can handle your post living online forever. You may feel it would be a fun idea to post drunken pictures of yourself, but do you really want your future employer to find them years later?

### 8. Join and create groups online to magnify your own influence. 

Could you imagine moving house on your own? Probably not, as the job would simply be too much for one person. Instead, you'd ask your friends to help.

This is the same ethos you should follow in your online endeavors: engage groups of people to help you succeed.

For example, let's say you find a hundred people in your town who all share your interest in fighting poverty. Sure, they could all hand out flyers and maybe reach a few thousand people that way, but online these one hundred people could reach potentially millions through, for example, social media. And the internet allows these millions to partake in their own small ways — for example, by signing petitions or donating just a small sum.

The internet also helps empower people, because everyone can not only consume what information they want, but can also distribute it. Earlier, if you wanted to distribute information, you had to get past gatekeepers like publishers, newspaper editors or radio stations, but today you can publish whatever you want yourself, for example, regarding the hardships of the poor.

So by joining or creating groups around the causes that are important to you, you can multiply the influence you have. What's more, you can leverage the expertise in these groups, which, thanks to the global span of the internet, can really be world class and not just the best in your town.

Remember though that if you want to increase your own influence by using groups, you have to give something back: it's quid pro quo.

General Motors (GM) learned this the hard way when they tried to boost their marketing by asking car enthusiasts to upload clips about their newest car. They didn't offer the enthusiasts anything, so the response was mostly negative.

Having learned its lesson, GM later launched a platform for car enthusiasts to talk about the cars they loved, whatever the manufacturer. This actually provided value for the enthusiasts, so they began promoting GM's online presence.

### 9. Use trust agent principles offline too. 

So far you've seen how you can use the internet to become more influential. But do trust agent principles only apply in the online world?

In fact, they can be applied offline as much as online, in both your private and professional life. For example, networks are pretty much universally beneficial, whether you're looking for a new job or need help moving.

Also, the trust agent principles are very useful in today's turbulent technological environment. After all, the technologies you've mastered today to help achieve your aims probably won't be relevant in a few years, so you need to be willing to adopt and master new technologies as they emerge.

To do this you once again need to leverage networks to learn more about emerging technologies as well as boldly pursuing the opportunities these new technologies provide.

For example, if you'd heard via network of technology enthusiasts a decade ago that we'd soon all be constantly online thanks to smartphones, you could have reaped huge benefits by designing completely new kinds of businesses around this trend.

What's more, you can be a trust agent, whatever your role or industry: it's more a mind-set than a job description. So whether you work for a Fortune 500 company or a non-profit charity, the same principles apply.

After all, in one way or another, we're all trying to sell something, whether products, ideas or our persona. But this sale should not be the main focus. Instead, being a trust agent means rehumanizing the web through personal, meaningful relationships with others. The main principle to being a trust agent is to be there before and after the sale, no matter what the industry or context.

### 10. Final Summary 

The key message in this book:

**All of us are trying to sell something, whether it's a product, our ideas, or our personas. But to succeed, we can't focus on selling — instead we need to build meaningful relationships and networks where we do our best to help others. The selling will happen as a by-product and meanwhile these networks greatly expand our influence.**

Actionable advice:

**Build your online presence.**

If you haven't done so already, start making yourself seen on the web. Get a Facebook, Twitter, etc., accounts, start a blog or launch your own homepage. Use the same name throughout all your accounts and instead of a logo, use one single picture of yourself. This are starting steps for building your own online brand.

**Check your trust situation.**

To see what your online reputation is like, do a Google search for your name and the name of your product. Read and learn what others think of you, so you can improve where necessary! If you encounter complaints, acknowledge them, apologize to the customer and try to rectify the situation.
---

### Chris Brogan and Julien Smith

Chris Brogan is a well-known American blogger, journalist and marketing consultant. Julien Smith is a consultant, speaker and CEO of the start-up Breather, a company that rents out rooms for work and relaxation.

© Chris Brogan: Trust Agents copyright 2010, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

