---
id: 52b176073130330012000000
slug: trust-me-im-lying-en
published_date: 2013-12-18T11:23:04.000+00:00
author: Ryan Holiday
title: Trust Me, I'm Lying
subtitle: Confessions of a Media Manipulator
main_color: E8412E
text_color: E8412E
---

# Trust Me, I'm Lying

_Confessions of a Media Manipulator_

**Ryan Holiday**

_Trust Me, I'm Lying_ (2012) is an in-depth exposé of today's news culture, which is primarily channeled through online media sites called blogs. By detailing his experiences with multimillion-dollar public relations campaigns, the author takes us behind the scenes of today's most popular and influential blogs to paint an unsettling picture of why we shouldn't believe everything that is labeled as news.

---
### 1. Blogs get content from other blogs, meaning that even trivial stories can get passed along to respected news sites. 

In today's internet-dominated society, most people get their news online. These online news sources, or _blogs_, are the newspaper of the twenty-first century.

And just like newspapers, blogs are always looking for fresh stories. These days, that means they watch what spreads across social-media sites like Twitter and what is written on smaller blogs.

If a story generates enough buzz across these media, chances are it will be picked up by mid-level blogs, which bring it to an even wider audience. And if the buzz continues from there, the story may make its way to major news outlets like the _New York Times_ or CNN, as they also keep an eye on blogs for promising stories.

A perfect example of this is when American football quarterback Kurt Warner jokingly suggested that a quarterback from a rival team, Brett Favre, join the reality TV show _Dancing With The Stars_.

The humorous, yet erroneous, story debuted on a small entertainment blog with the title "Brett Favre is Kurt Warner's Pick for DWTS: 'Controversy is good for ratings'." But then the story was picked up by a CNN-affiliate which ran it with the headline: "Brett Favre's Next Step?"

When it finally reached national news publication _USA Today_, the joke had transformed into a fully fledged rumor: "Brett Favre joining 'DWTS' Season 12 Cast?"

The stories aren't always this petty, though. Smaller blogs and social media have broken major stories as well.

Surprisingly, the death of Osama Bin Laden was first reported by a user on Twitter. This was before major news outlets, blogs or even President Obama confirmed its factuality.

Regardless, both examples show one thing: all blogs are interdependent, so it's no surprise that even trivial stories can become national headlines.

### 2. Bloggers want to make money: primarily through advertising, but most also dream of selling their blog for a fortune. 

All blogs, big or small, are businesses. And like all businesses, a blog's main goal is to make money. They primarily do this by selling advertising space.

Each blog sells advertising space in different ways, but one common way is "per impression" pricing — a fee the advertiser pays a blog for each time someone opens a blog page with their ad on it.

In short, this means blogs make money every time you visit them.

But while advertising keeps a blog going, the big payoff most bloggers dream of is selling their blog to a large media company.

Large media companies buy blogs because each blog is like a piece of internet property with advertising space they can sell. Usually these companies target high-traffic blogs, or blogs with hundreds of thousands of daily visitors, because with these they can sell ad spaces to advertisers at really high rates.

So the more traffic a blog gets, the more it will sell for.

A few examples: blog group Weblogs, Inc. was sold to AOL for $25 million; news aggregator Huffington Post was sold to AOL for $315 million; technology news site Ars Technica was sold to Condé Nast for $30 million.

Most blog owners have this in mind when they start their blog. They know that if they can make their blog look popular enough, some bigger player from news media may just buy it.

After the transaction, the former blogger — now multi-millionaire — might retire to the Caribbean, while the large media company gets a popular site on which it can display advertisements.

### 3. Blogs rely on iterative journalism: publish first, fact-check and revise later. 

You may have noticed that most blogs produce multiple posts a day. This is because the more content there is, the longer the reader usually browses, which in turn generates more ad impressions and more revenue for the blog.

Additionally, while traditional news media are restricted by time or size limitations — like newspaper word counts or one-hour-long TV news programs — there is no limit to how much content a blog can publish.

These factors encourage blogs to practice _iterative journalism_ : the act of publishing first, then fact-checking and revising later.

In the first stage of iterative journalism, a blog publishes an article based on even the slightest whiff of a story — without doing any fact-checking whatsoever.

For example, in 2008, someone reported via CNN's citizen journalism website iReport that the founder of Apple, Steve Jobs, had died of a heart attack. The news was at first virtually ignored by all blogs. But one blog, Silicon Alley, eager to fill its pages with eye-catching content, published it as if it were a fact.

In the second stage of iterative journalism, if the article _does_ turn out to be incorrect or only founded on rumor — something the well-informed readers tend to point out — the blogger will either change its headline to something more in line with the facts, or add a twist to the article to make it seem like the mistake wasn't their fault.

This is exactly what happened with the Steve Jobs story. When Apple said the story was a flat-out lie, Silicon Alley simply rephrased it, changing its opening line to "Citizen journalism fails its first test," as if citizen journalism's failure had been their point all along.

This just shows how faulty iterative journalism can be. While it allows blogs to generate content rapidly, it also allows lies or misinformation to be reported as "news."

### 4. Blogs need readers, which means they’ll publish any garbage that turns heads. 

Just as any business needs customers, blogs need web traffic: people to browse and read their content. In fact, they need lots of it, since the advertising fees they charge are directly related to the traffic they get.

Clearly, blogs want to get as many visitors as possible. To do that, they rely on a variety of methods to snag people's attention.

The most common method is to use sensational or controversial titles — sometimes at the cost of reputable information.

A good example of this is when women's lifestyle blog Jezebel published an article with the eye-catching title, "The Daily Show's Woman Problem," criticizing the American political satire show for its male-dominated history.

The story's sources were anonymous and outdated, and women working at The Daily Show issued a letter denying the article's allegations, calling it "inadequately researched."

Nevertheless, after it was published, the story circulated through major US news syndicates like ABC News and the _Wall Street Journal_, who, like Jezebel, saw a huge spike in their readership. The original story and its syndicated versions attracted a cumulative total of 500,000 page views.

Some blogs even go so far as to use computer-generated algorithms to find catchy headlines. Demand Media — owner of sites like cycling celebrity Lance Armstrong's Livestrong.com — is one such example. Their algorithms scan the web for lucrative search terms, which its blogs then cram into single headlines. Only then do they decide what the actual content of the article will be. This algorithm-driven content creation increases the chances of the articles showing up on search engine queries.

Yet whether spawned by algorithms or human bloggers, practices like these are solely intended to create content that turns heads, thereby increasing a blog's advertising revenue.

The unfortunate result, however, is that quality content and accurate reporting get sidelined.

> _"Every decision a publisher makes is ruled by one dictum: traffic by any means."_

### 5. Blogs deliberately bait us with content that generates an emotional response. 

Have you ever read a story online that made you angry? This was probably not a coincidence. Blogs often try to pique our emotions to get us more involved.

For example, they know that when we see a story about an injustice, like a child getting kidnapped, we get angry. That anger then spurs us to interact with the story, either by sharing it with others or by posting our thoughts in its comments section. This then creates buzz around the story, generating more ad revenues for the blog.

Anger is the main emotion blogs are interested in stoking, because it generates the biggest reaction. Other powerful ones include fear (for example, terrorist attack warnings), excitement (for example, the release date of the new iPhone) and laughter (for example, a video of cats running into walls).

The power of anger was demonstrated empirically by researchers at the Wharton School. When they looked at 7,000 _New York Times_ articles that made the publication's "Most E-mailed" list, they found that the articles which inspired the most anger got a significant boost in page views, equal to them having spent three extra hours on NYTimes.com's front page, which is where they originally debuted.

Articles which inspired the most positivity, however — like happiness or excitement — only reached as large an audience as if they'd spent 1.2 more hours on the front page.

And sadness? Wholly non-viral. Researchers found that because sadness is a low-arousal emotion, it depresses our impulse for social sharing.

Blogs look at studies like these to learn what will bait and hook us. Content that pulls us in and riles us up means more traffic — and more money — for their business.

### 6. Because blogs constantly correct and update their content, we rarely see news in its most accurate form. 

One time or another, you've probably encountered an article on a blog with the words "UPDATE" or "CORRECTION" in its title. As you can probably guess, this means the article you're reading has been changed from its original version.

This change was most likely made because the blogger who first published the story got something wrong or presented the story while it was still developing.

But even if corrections are made, that doesn't undo the impressions of people who encountered the article in its inaccurate state.

Just look at the history of _Wikipedia_ 's article on America's Iraq War. As of 2010, the article had received over 12,000 edits. Yet since the war started in 2003, thousands of blogs, other _Wikipedia_ pages and internet users have used it as a reference — all before it had reached its most accurate form.

And even if a reader re-visited the page today, psychologists say it might not help.

In a University of Michigan study, participants were split into two groups who'd read the same article. One group, however, got a version of the article with a corrective note at the end, refuting a central claim.

Afterward, researchers found that, somewhat counterintuitively, those who saw the corrective note were _more likely_ to believe the claim it refuted.

Why on earth would we be more likely to believe something after we've been told it's not true?

Because reading something twice is better for memorization than just reading it once, say the researchers. Therefore, a correction at the bottom of the page just reinforces the original claim in the reader's mind, having the exact opposite effect to that intended.

So in sum, blogs' tendency to revise and correct content as they go means that much of what they spread can be considered misinformation.

### 7. From igniting international protests to boosting political campaigns, blogs have proven they can shape society. 

A news story can affect everything from stock market prices to presidential elections. It's no surprise, then, that blogs wield enormous power in shaping society. 

For a political example, look no further than Tim Pawlenty. Pawlenty was the governor of Minnesota when political blog Politico started speculating he would run for president — even before he'd expressed any intention to do so.

He eventually started a campaign, and his name made it onto the presidential ballot — mostly thanks to the coverage by political blogs.

But why did blogs start writing about him in the first place?

Simple: political blogs know they get the most traffic during election cycles. By speculating on Pawlenty's run, they made it seem like they already had an "election season" story, which they knew would attract far more readers.

Other examples of blogs' power are more harrowing, like when news of Florida pastor Terry Jones's desire to host a Koran-burning ceremony began spreading around blogs, before getting reported by major news outlets like CNN.

When the ceremony eventually took place, the media refused to cover it until an American college student broke the silence by reporting the story for a French newswire. Once it spread across blogs internationally, it resulted in global protests and the deaths of 27 people in countries including Afghanistan.

But blogs aren't powerful because of their reader numbers — it's all about the _type_ of readers they have.

Blogs like Politico are mostly read by the "media elite" — people who own other blogs, or work for major news outlets like CNN and the _New York Times_. This means that though the blogs may not have a huge direct following themselves, anything they publish can find its way into mainstream media and thus reach the masses.

### 8. In the twenty-first century, blogs have become a stage for public witch hunts. 

History has shown that we enjoy watching other people get publicly punished, humiliated or torn down. Just look at our ancestors' love for witch trials, guillotine executions and gladiatorial battles.

These "degradation ceremonies," as anthropologists call them, allow the public to single out and denounce one of its members — simply because we need a scapegoat to take our anger out on.

Blogs are the twenty-first century version of these ceremonies. Instead of standing around a scaffold chanting "off with his head!"we express our anger on current affairs by posting irate replies on relevant blog articles or spreading said articles through social media along with outraged comments.

For example, look at how Julian Assange, founder of political watchdog website WikiLeaks, became a victim of these witch hunts.

After Assange became a well-known public figure in 2010, influential blog Gawker would post playful, friendly articles with titles like "What happened to WikiLeaks' Founder Julian Assange's Weird Hair?"

Two weeks later, after Assange was accused (only to later be un-accused) of sexual assault, Gawker began publishing headlines like "Are WikiLeaks Activists Finally Realizing Their Founder is a Megalomaniac?"

As soon as there was the slightest hint that Assange was a sexual predator, even if an unconfirmed one, people who previously supported him turned against him, and blogs like Gawker used this momentum to their advantage by further stoking public fury with their articles, generating thousands of additional page views from an old topic.

Our love for public witch hunts means blogs are only too willing to demonize anyone whom we deem unpleasant.

### 9. Final summary 

The main message in this book is:

**While blogs are our main source of information in the internet age, the unfortunate truth is that they are predominantly unreliable. Each blog is a business, surviving by profiting from revenue from ad sales. Because this revenue increases as a blog's readership increases, blogs are more concerned with getting readers than producing factual, quality content.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts! **  

**
---

### Ryan Holiday

Ryan Holiday is a media strategist best known for his campaigns with _New York Times_ bestselling authors Tucker Max and Robert Greene. He currently works as Director of Marketing for the $550-million clothing company American Apparel, and runs the marketing company Brass Check Marketing.

