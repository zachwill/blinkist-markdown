---
id: 553f5d8e6136310007150000
slug: truth-lies-and-advertising-en
published_date: 2015-04-28T00:00:00.000+00:00
author: Jon Steel
title: Truth, Lies and Advertising
subtitle: The Art of Account Planning
main_color: EF3048
text_color: D62B41
---

# Truth, Lies and Advertising

_The Art of Account Planning_

**Jon Steel**

In _Truth, Lies and Advertising_ (1998), leading account planner Jon Steel shares an insider's insight into the world of advertising. For Steel, the creation of great ads is all about understanding the consumer, and his compelling behind-the-scenes anecdotes illustrate the role account planners play in developing a successful campaign.

---
### 1. What’s in it for me? Discover how an account planner works magic in advertising. 

Why do the oddball geniuses in advertising get all the credit for great projects? Probably because we have a romantic notion of Don Draper characters sitting around in skyscraper penthouses, ruminating on society and coming up with beautiful and concise taglines and copy.

The truth, however, is different. Behind every Don Draper genius, there's an account planner. And without that account planner, Don Draper would be unemployed.

As you'll see in the following blinks, the account planner has arguably the most important job in advertising: he stands between the client and the creatives, does research, collects feedback and does everything to ensure a client gets the great campaign they're paying for.

After reading these blinks, you'll find out

  * how the author was instrumental in the "got milk?" campaign;

  * why you should be skeptical of whatever a consumer tells you; and

  * when you've done enough research.

### 2. Account planners bring together the client’s business interests, the agency’s creative team and consumer needs. 

Ad people have a bad reputation for being untrustworthy and unethical, almost on par with snake oil salesmen. But not all ad agencies are trying to hoodwink you or overwhelm you with ads; some strive to connect with their audiences on a human level. And for this kind of next-level advertising, an _account planner_ is crucial.

This role is all about researching consumer needs in order to solve client problems. An account planner conducts consumer interviews, develops information briefs or looks into various related factors (like sales figures) that might be contributing to the client's problems.

Additionally, the account planner is responsible for keeping the creative team on track by sharing information about the target consumer and communicating the client's needs. In other words, this job isn't about making the decisions, but rather about making ideas happen.

For instance, when the author was working on a project for the auto company, Isuzu, he hosted focus groups at dealerships to understand the customer base for individual models. When he learned that the Rodeo model attracted adventurous types, he relayed that information to the creative team. Building on that insight, the creatives developed a commercial about a young father and his son visiting a toy store, where they find a Rodeo packaged like a toy car. And the campaign tagline read: "The Rodeo. Grow up. Not old."

As you can see, the account planner's job requires in-depth research. And that's why a planner should ideally work with three clients at a time, max. That might not seem like much, but it's important that the account planner doesn't cut corners due to overwhelming workloads or time pressure.

After all, getting the job done right will help the agency create lasting relationships with clients and build an impressive portfolio, which makes it a more profitable strategy in the long-term.

> _"Ultimately I believe that any good planner has to be very strong both strategically and creatively."_

### 3. The account planner guides the creative team by conducting consumer research and analyzing the results. 

What does it take to effectively liaison between creatives and clients? Well, perhaps the most important aspect of being a good planner is listening well — which also means asking the right questions.

When you talk to consumers, you have to figure out how a product fits into their lives. And asking questions in a logical, methodical way is the best way to do that. Also, don't be afraid to ask the obvious!

That's something the author learned when he asked some focus group participants, "How much milk do you drink?" Many people said they drank little milk, or even none at all, completely forgetting the fact that they drink their coffee white and pour milk on their cereal every day.

Along the same lines, stay away from questions that only produce one answer. For example, in a blind survey, testers preferred a new kind of Coca-Cola over the original formula. However, since the public has such a strong emotional attachment to the traditional Coke brand, once this new version was released to the wider public, it completely failed.

As you can see, focus groups can have a major impact on a business. That's why it's important to create a comfortable environment for test consumers, so you can collect the most accurate, useful information. Make participants feel relaxed, so they say what they really think.

To that end, it helps to hold interviews in private homes, rather than in an intimidating research building. That worked for Sega account planners, who watched kids play video games in their own rooms and then asked questions on the spot.

Interviewing style is also important: Don't interrogate focus group subjects. Instead, give them a way to participate, perhaps by lending them the product for a week to test independently before discussing it with you.

Next up, we'll be taking a look at the single most important part of the account planner's job. Namely, creative briefing.

### 4. The account planner uses the creative brief to present a simple, powerful idea to the creative team. 

How does the account planner's consumer research turn into a creative campaign? Well, after conducting research, the planner prepares a creative brief — a report with the overall campaign strategy — and presents it to the creative team.

Since the main goal is to communicate with creatives about your work, it's best to keep it simple and casual, to better facilitate the flow of ideas. But no matter what format you use — oral or written, long or short — every brief should contain the following information:

  1. Any business problems the campaign should address: "Why didn't the client's cycle helmet sell well?"

  2. Specific campaign objectives and desired effects, in order of priority: "Do we want to attract new consumers or encourage regular ones to buy more helmets? If both, which is more important?"

  3. The specific target audience: "Parents who worry about bicycle safety but also helmet-wearing kids who don't want to look like dorks."

  4. Concrete knowledge about the target consumers: "How does the helmet fit into their lives?" You can include some of the personal stories you've gathered in your research.

Beyond those four points, the most important part of the brief is the _proposition_ — the core message written as one simple sentence. This is what creatives will try to convey with the final advertisement.

And since the main goal of any ad is to communicate a product's special features in an accessible way, your brief should include ideas on how to convey this proposition in an entertaining fashion, perhaps using material from your research.

Take the Cuervo focus group: When participants were asked to imagine a guest arriving with a bottle of Cuervo, they started shaking their heads and laughing to themselves. Their body language was clear: Cuervo equals party.

And so the account planner used this anecdote in his creative brief to support the Cuervo proposition: "A party waiting to happen."

> _"Creative briefing is the key tool with which planners...can unlock the talents and imagination of their agency's creative people."_

### 5. The account planner works with the creative team to incorporate client expectations into the campaign concept. 

Once the account planner has presented the creative brief, what's next? Now it's time for the creative team to polish the planner's preliminary ideas and give them a distinctive flair.

Consider the author's experience on the Sega account: In his research, he observed that kids preferred Sega's console to Nintendo's. So in the creative brief he compared the experience of using the console to getting picked to play in baseball's Major Leagues, describing it as getting called up to "The Show."

The creative team liked the idea, but decided to translate it from baseball-ese to gamer-speak. It took the author two weeks to find his idea, but in just 30 seconds the creatives had come up with the winning tagline: "Welcome to the next level."

The process doesn't end here: After the creative team writes the campaign, it's the planner's job to make improvements by testing consumer response and incorporating client feedback.

For example, for the Foster Farms account, the core campaign message was that although the California chicken company's poultry was more expensive, it was natural, fresh and local — not frozen and imported.

So the creative team developed a commercial that showed chicken puppets driving to California, trying to pass themselves off as high-quality, Foster Farms chickens. Their car was cluttered with empty beer cans and cigarettes.

But the president of Foster Farms didn't like the alcohol and nicotine props, so the client asked the agency to scrap the chicken puppets and come up with two new concepts. The creatives did that, but also improved the original idea by replacing the beer and cigarettes with junk food.

But then, when the account planner tested the two brand new concepts and the junk food-eating chickens in focus groups, people were most excited about the puppet chickens. In the end, those results convinced the Foster Farm president. Note that the author didn't do _exactly_ what the client wanted, but instead found a solution that won him over in the end.

Now that we've broken down all the steps of the process, let's see what the account planner's job looks like, from start to finish.

### 6. The author developed the iconic “got milk?” campaign by following the account planning process. 

Let's see how the author used the account planning process described in earlier blinks to develop one of the most famous advertising campaigns of all time.

The client was California Fluid Milk Processors Advisory Board (CFMPAB), a group that wanted to increase milk consumption.

Steel started by researching declining milk consumption. He was also trying to identify target customers and understand how milk already fitted into their lives.

He learned that many people were drinking less milk because they thought it was boring, too childish and fatty. Nonetheless, people did drink some milk, typically in combination with something else.

The author conducted a focus group, paying consumers $25 to forgo milk for one week. In the discussion afterward, participants described how they'd completely forgotten how much they liked drinking milk with a chocolate cookie or peanut butter sandwich.

So in the creative brief, the author outlined the strategy: The campaign should remind consumers to stock up on milk, to avoid that feeling of deprivation. He figured that by creating desire for certain foods, it would also create greater desire for milk.

Then the creative team developed a campaign around this idea, portraying milk as an essential companion for certain foods. So the "got milk?" tagline was born.

And happily, the CFMPAB liked the initial concept, so the author didn't have to keep going back and forth between the client and the creative team.

Since his research showed that milk consumption primarily happened in the home, the campaigns were first tested on TV and then on billboards located near grocery stores.

The campaign also ran in magazines, as a photograph of a chocolate chip cookie with a bite missing, with the tagline "got milk?" running underneath.

Focus groups showed that the campaign was raising awareness of milk, which made people more likely to stock up at the store.

And soon after the campaign launched in 1993, milk consumption in California outpaced every other state. And once the campaign expanded nationally in 1995, it achieved similar success.

### 7. Final summary 

The key message in this book:

**An account planner helps develop great advertising by working to understand the consumer's needs and communicating those to the agency's creative team as well as the client. This important role is all about creating a meaningful message and a human connection.**

Actionable advice:

**Read everything and talk to everyone who crosses your path.**

Inspiration can come from anywhere, so agency staffers should broaden their minds by reading non-business books and magazines and talking about ideas with people outside the industry. An outside perspective is especially crucial for account planners.

**Suggested** **further** **reading:** ** _Hey Whipple, Squeeze This!_** **by Luke Sullivan with Sam Bennett**

_Hey Whipple, Squeeze This!_ has become a seminal guide to the world of advertising for those who have been in the business for decades, for newcomers, and for anybody intrigued by what happens when creativity meets commerce. The title is an irreverent nod to an unconventional 1970s campaign for Charmin toilet paper that featured an annoying shop clerk called Mr Whipple, who couldn't stop squeezing the product. It sets the tone for Sullivan's honest and practical insights into the sometimes crazy creative process of advertising.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jon Steel

Jon Steel is currently planning director at WPP Group and chief strategy officer at George Patterson Y&R. He's spent decades at the top of the advertising industry on three continents — America, Australia and Europe — working with iconic brands like Sony, Budweiser, Hewlett-Packard, Nike and Porsche.

© [Jon Steel: Truth, Lies & Advertising] copyright [1998], John Wiley & Sons [Inc. or Ltd. as applicable] Used by permission of John Wiley & Sons [Inc. or Ltd. as applicable] and shall not be made available to any unauthorized third parties.

