---
id: 5d2ee4776cee070007a717de
slug: truth-en
published_date: 2019-07-18T00:00:00.000+00:00
author: Hector MacDonald
title: Truth
subtitle: How the Many Sides to Every Story Shape Our Reality
main_color: EC5C2F
text_color: EC5C2F
---

# Truth

_How the Many Sides to Every Story Shape Our Reality_

**Hector MacDonald**

_Truth_ (2018) shows us how we live in a world of competing truths, where politicians, activists, corporations and countries tell the stories they'd like us to hear. Identifying the different ways that truth can be used to mislead or inspire, Hector MacDonald draws from history and current affairs to demonstrate how we should wait to see the whole picture before deciding what is "true."

---
### 1. What’s in it for me? Find out how the honest truth can be used like a lie. 

You can't escape the truth. Turn on the TV, browse your social media feed or scan the newspaper headlines, and you'll be confronted with claims of the truth. Whether it's by a politician trying to secure your vote or a tech corporation promoting its latest gadget, the truth can be deployed to change your mind one way or another.

As you'll find out in these blinks, we live in a world of competing truths, where the news that we read, the opinions that we hold and the histories that we're familiar with often reflect only one side of the story. 

Truth can be manipulated for the darkest of purposes, by unscrupulous corporations or nationalist politicians spreading division. Truth can also be used to inspire. In the right hands, a selective truth can raise employees' morale or win victories for noble causes. 

In _Truth_, you'll learn how to better arm yourself against a world of competing claims and discern fact from fiction. In times where fake news, bewildering statistics and manipulative advertising swirl around us, the truth remains our best defense.

In these blinks, you'll learn

  * how George W. Bush left a complicated legacy;

  * the difference between an advocate, misinformer and misleader; and

  * how Coca-Cola rewrote its history to avoid an embarrassing detail.

### 2. There are often competing truths about people, events or things, all of which can be true at once. 

We're often told that we should read a variety of sources to get the full picture. _The_ _Guardian_ claims this, the _New York Times_ claims that, and _El Pais_ claims something else. It's often the case that all of them have something true to say. 

When we look closely, we often find that there are a variety of equally legitimate ways of describing a person, event or thing. For instance, one person might claim that the internet is a force for good, as it makes knowledge readily available, while someone else might claim that it's full of hatred and misinformation. Both statements are true: the internet has brought us access to a wide range of information, but it's also swamped us with "fake news."

Similarly, a local bookstore might describe Amazon as a disaster for its business, while a self-publishing author may think of it as a great platform to distribute her work. Indeed, the British bookstore chain Waterstones described Amazon as "a ruthless, money-making devil," while in a survey of its members, the UK's Society of Authors found many more respondents praising the online giant than attacking it. 

And that's just the perspective from the world of books. Amazon also produces its own television shows and movies, runs Amazon Marketplace for budding entrepreneurs and owns the grocery store chain Whole Foods. Devil or savior, it's many things to many different people. 

If we don't acknowledge these competing truths, we can oversimplify the world. Consider US President Richard Nixon, who was a Republican. He's almost universally maligned by those on the progressive side of politics. But he also created the Environmental Protection Agency and passed a raft of progressive legislation, such as the Endangered Species Act and the Ocean Dumping Act. Likewise, when thinking of George Bush's legacy, we don't often get further than the invasion of Iraq. However, over his two terms, Bush sent more financial aid to Africa than any other American president. 

The truth, then, is more often plural than singular. When you hear something asserted as _the_ truth, pause and reflect on the wider picture before passing judgment. In the next blink, we'll find out how our judgment can be shaped by the selective truths of others.

### 3. The selective truths that others choose to tell us shape our mindsets and actions. 

How do we form an opinion about a public figure, a country we've never visited or an imported "superfood"? It can all begin with a comment heard on the radio or a headline glanced at in passing. 

What we hear first often shapes our subsequent ideas about something. For example, take the following story about a new health-food craze. Sometime in the mid-2000s, the West "discovered" quinoa and began importing it from Peru. It was celebrated by food writers like Yotam Ottolenghi, while NASA declared it to be one of the most balanced foods on Earth. However, there were soon reports about this growing demand damaging the environment in the Andes, where quinoa is grown.

Our minds are susceptible to first impressions, so if the first information you heard about quinoa came through Yotam Ottolenghi, you would be more likely to purchase some. He would have influenced your behavior through his selective truth. He also would have influenced your _subsequent_ attitudes to other "superfoods." Having learned of the health benefits of quinoa, you might be keener to hear about those of kale or acai. In contrast, if the first time you heard about quinoa came from a report about environmental damage, you might judge a colleague harshly for buying other imported "superfoods" at lunch.

These selective truths can leave a lasting impression on us. The ones we believe can inform our entire mindset going forward. Our opinion about quinoa might feed into the way we think about the world more broadly. As the political journalist Walter Lippmann wrote: "Our opinions cover a bigger space, a longer reach of time, a greater number of things than we can directly observe." This means that, cumulatively, these selective truths can determine important choices we make: the way we vote, shop and interact with others.

So when forming an opinion, it's really worth making sure that the facts it's based on add up. Next, let's look at the different ways in which truth can be used to inform or deceive us.

### 4. Selective truths can be used either responsibly or deceitfully. 

The way we wield the truth matters. Well chosen, a selective truth can raise people's spirits, snap them out of bad behavior or get them successfully from point A to B. Equally, selective truth can be used to deceive and exploit the worst in us. 

Let's look first at how selective truths can be used responsibly. A good example is when we visit the doctor and ask her for a diagnosis. Rather than giving us a detailed lecture on all of the biological processes that have led to our illness, the doctor will stick to a few clear facts so that we know how to act next. Instead of worrying us with unnecessary scientific analysis — like, for example, the cellular anatomy of a tumor or a lesson on virology — the doctor will tell us to prepare for treatment, get a prescription or take some rest. 

Similarly, a government official might use selective truth when instructing a population on what to do in the event of an epidemic. He may withhold specific information to prevent widespread panic, which could spread the disease or cause chaos. 

The flipside, of course, is that selective truths can be used deceitfully. For example, in 2016, the Texas Department of State Health Services published a booklet for pregnant women that drew a link between abortion and breast cancer. Without lying as such, the clever wording of the booklet implied that having an abortion could increase the risk of breast cancer, which scientific evidence disputes. The booklet stated that "if you give birth to your baby, you are less likely to develop breast cancer." While it's true that giving birth early in life seems to lower the risk of breast cancer, it is _not true_ that having an abortion increases the risk. 

Since the department didn't outright lie, this is an example of a selective truth being used to manipulate people for ideological purposes. Through selectivity, the department was intentionally attempting to distort Texans' perception of reality; it was trying to create a false impression of the facts. 

Next, we'll look at the three specific types of communicators who use the truth to shape the world around them.

### 5. There are three types of communicators who all use truth differently: advocates, misinformers and misleaders. 

To simplify things, let's separate the different types of communicators into three camps.

The first type are the _advocates_. An advocate uses competing truths that create a fairly accurate impression of reality in order to achieve a constructive goal. So the government official making a public announcement about what to do in the event of an epidemic would be an advocate. By selecting the right information and omitting unsettling details, the official's goal would be to protect the citizens of her country. 

Next, we come to the second type of communicators: the _misinformers_. A misinformer innocently spreads competing truths that unintentionally distort people's perceptions of reality. For instance, in 1991, two psychologists from California State University declared that left-handed people were more likely to die earlier than those who were right-handed. Studying the deaths of 1,000 Californians, they had found that, on average, right-handers died at 75 and left-handers at 66. Their study was reported by international media, including the BBC and _The_ _New York Times_. 

Unfortunately, they had misinterpreted their findings — previous generations had treated left-handed people with a level of discrimination that meant that parents had "corrected" their children until they were right-handed. The truth wasn't that left-handers died earlier, but that among that generational cohort, there were simply fewer of them because they were forced into right-handedness as children. The psychologists had unwittingly become misinformers.

The third type of communicators are the _misleaders_. A misleader intentionally uses competing truths to create an impression of reality that they know to be false. For example, the toothpaste brand Colgate-Palmolive ran advertisements over many years that claimed "more than 80 percent of dentists recommend Colgate." In fact, this was a misrepresentation of survey data — the dentists had been asked what brands (plural) they recommended, and most had named several others besides Colgate-Palmolive. Colgate-Palmolive knew this very well. They were misleaders. 

Until it comes into the hands of one of these three types of communicator, a competing truth is morally neutral. Like a box of matches, everything depends on how it is used. So, the next time you hear a politician or TV commercial state a fact, pause and consider the source. Be sure you know exactly who you're listening to.

### 6. The historical record can be rewritten using selective truth. 

We've all heard the saying "history is written by the victors." Well, as we will see in this blink, it's also re-written by corporations, states and even the losers of historical conflicts.

Through selective storytelling, key events can be removed from our understanding of the past. 

For instance, to celebrate an important anniversary in 2011, Coca-Cola published a 27-page pamphlet entitled _125 Years of Sharing Happiness_. Illustrated with classic Coca-Cola advertisements, it presented an interesting fact for nearly every year since 1886. Its second biggest international product, Fanta, makes one appearance, where its introduction to Italy is listed for the year 1955. However, Coca-Cola didn't mention that the invention of Fanta had taken place in Nazi Germany fifteen years earlier. Back then, in response to the trade embargo of World War II, the German branch of the company manufactured an alternative to Coke using available waste products, like whey and apple fiber.

There are even more dramatic cases of historical omission. For instance, Israeli schoolchildren reading history textbooks today will find no reference to the Palestinian exodus of 1948, also known as the Nakba, which means "the catastrophe" in Arabic. During the Nakba, 700,000 Palestinian Arabs were forced to leave their homes.

As you might imagine, these selective portrayals of the past have serious consequences for the future. As with the omission of the Nakba in Israeli history, a similar situation is evolving in the US. In 2015, the state of Texas decided that there should be no mention of the racist Jim Crow laws or the Ku Klux Klan in their schools' history textbooks. Texan school children would also read an alternative history of the Civil War, in which the fighting was simply over "states' rights," without learning that slavery was the "right" that the Confederate states were most keen to protect.

This downplaying of slavery and racism in the American South has very serious consequences regarding social attitudes. In this climate of ignorance, white supremacists can muster their forces and spread their toxic narratives easily.

The lesson here? Learn your history _scrupulously_. It's the best defense against the misleading stories told by those who would seek to exploit us.

### 7. There are competing moral truths across history, cultures and different parts of society. 

In this blink, we'll look at how different societies in antiquity held radically different beliefs and how the opium addicts of the nineteenth century would be judged by today's standards.

Let's look first at how moral truths differ across societies. In the _Dissoi Logoi_, an anonymous text from Classical Greece, the Greek author compared the dramatically different moral standards of nearby ancient peoples to his own. He noted that the nomadic Scythians believed that whoever killed a man should scalp him and wear that scalp on his horse's bridle. Then, they thought it perfectly acceptable to gild his skull and use it as a drinking vessel when paying homage to the gods. 

Among the Greeks, this would have been viewed as an outrage and no Greek would have entered the house of a man who had done such a thing. 

A similarly dramatic inversion of moral truth can be found today. Gay marriage is legal across much of the West, while in Iran, Sudan and Saudi Arabia, homosexuality is punishable by death. 

Rather than being set in stone, moral truths can shift with time. During the nineteenth century, many people, including Queen Victoria and the Pope, drank Vin Mariani, which contained 6 milligrams of cocaine per fluid ounce of wine. Other notable figures took opium, including Charles Dickens and the mathematics genius Ada Lovelace. While addiction was considered unwise, it wasn't morally problematic to use what we now consider hard drugs.

Then, in the twentieth century, a great change took place in moral attitudes. As governments understood the risks of drug addiction, they embarked on a mass propaganda exercise to stigmatize users and "correct" this moral truth. This was achieved by associating addiction with "out groups," such as black and gay people. 

This culminated in the racially-charged "War on Drugs" by Richard Nixon and Ronald Reagan. The outcome of this exercise was the demonization of anyone caught in possession of drugs — a far cry from the days of Victorian gentlemen partaking in opium. 

Today, however, we see the emergence of a more tolerant and understanding attitude toward drug use, which recognizes addiction to be an illness with societal causes, rather than simply a crime.

In the next blink, we'll look at how numbers can be manipulated by those pushing competing truths.

### 8. Numbers are often used to support competing truths. 

We're inundated with numbers. Governments tell us how much they're going to spend, and the headlines alarm us with statistics. Meanwhile, supermarket shelves are a bewildering array of percentages. How do we begin to decode them all?

By changing the way something is measured, numbers can be used to fit a particular truth. Consider this statement: "Canada and Australia have the highest rates of kidnapping in the world." It's true. Not because they're particularly dangerous countries, but because their governments include parental disputes over child custody in their kidnapping figures. 

Similarly, Sweden has the second highest incidence of rape in the world. There are 60 cases per 100,000 citizens every year. But this is more a reflection of Sweden's better reporting of assault and their broader legal definition of rape.

As most of us are not experts, we can be misled by numbers. Take the shower gel Original Source Mint and Tea Tree. The brand claims that 7,927 real mint leaves are packed into each bottle. The number "7,927" is printed prominently across the label. But unless we are experienced botanists, how can we verify whether this impressive number is as significant as it sounds? Is 7,927 an unusually high number of leaves to compress into a shower gel? Thousands of roses are required to make just a few milliliters of essential oil, so perhaps it isn't so impressive. 

Similarly, the British Prime Minister Theresa May stated in 2017 that she was investing £2 billion in affordable housing. Though it sounds like a large number, it's only enough to build an extra 25,000 new homes — not nearly enough to deal with the 1.2 million households on public housing waiting lists. 

So before you are awestruck or dismayed by a number, you should always query how it's being used. What is the precise context? Is it quoted merely to impress or worry you? Next time an alarming statistic is hurled your way, dig a little deeper.

### 9. In the right hands, a well-chosen truth can make a difference in the corporate world. 

Ever been stuck in a rut at work? Wondered why you were doing it at all? Then you'll know how important a rousing speech from the boss can be. 

Chosen wisely, the right competing truths can be used to motivate and inspire employees. Let's imagine that you're taking on a new recruit at your company. What could you tell her about her new role? That she will probably not get a pay raise for a couple of years? That her co-workers are badly organized? That the work can be a little dull? This all might be true. But it'd be better to tell her about the opportunity to learn a new skill, the chance to travel to company offices abroad and her exciting role in a growing business.

These stories can shape the way we behave at work. At the British multinational Barclays, employees are often reminded of the bank's historical founders, the Quakers, and their principles of honesty, integrity and straight-forward dealing. In a profession often maligned for recklessness and greed, this story is a useful reminder to employees that the banking world doesn't have to be defined by vice and that they can take pride in what they do. 

Businesses looking to define their brand must choose the best competing truths. For instance, during the 1990s the telecommunications company Ericsson was one of the world's largest mobile phone manufacturers. Today, it specializes in connecting automated technologies, including setting up "the world's largest floating network" with the Danish shipping company Maersk, as well as road vehicles with Volvo and Scania.

To help customers and employees understand this new mission, the company reflected on its history and decided to focus on the narrative that Ericsson was a "technology pioneer." It highlighted the fact that its founder, Lars Magnus Ericsson, started designing telephones in 1878 and that the company launched the first mobile telephony system back in 1981. There were many other ways to describe the giant organization, but this particular aspect would be most useful in foregrounding its futuristic, ground-breaking side, which would help employees bond over the new direction and customers place Ericsson in their minds.

So what can we learn here? Smart, agile business leaders can shape the world around them with the right truth, delivered at the right time.

### 10. Final summary 

The key message in these blinks:

**The world is full of competing truths, where many different things can be true at the same time. Truth can be used by those who seek to mislead, those who unintentionally misinform and those looking to inspire. It's up to us to learn how to decipher how truth is used — by politicians, corporations and the media — so that when we form an opinion, it's based on the full picture, not partial or selective information.**

Actionable advice:

**Always listen to the other side.**

The next time you decide that someone with a different opinion to your own is simply beyond redemption, _pause,_ take a deep breath and try to understand his worldview and the different truths that have shaped it. Sometimes you might find that he has a point or two. And if not, you might at least have a better understanding of why he thinks like he does. If we all did this, how often might we avoid conflict and division? 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Art Of Thinking Clearly_** **, by Rolf Dobelli**

As you've just discovered, the truth is subjective. We all think we're wiser than we actually are, and we're prone to confirmation bias; we seek out the facts that back up our pre-existing beliefs. 

But our pre-existing beliefs sometimes do us little good. And this can lead to us getting stuck in bad habits like scrolling endlessly through online news or failing to take the risks that could improve our lives. 

To learn how to avoid these thinking errors, get the blinks to Rolf Dobelli's _The Art Of Thinking Clearly._ Here you'll learn how our subtle, subconscious biases lead us to make stupid decisions, and how, by using just a little more logic, we can be happier, more prosperous individuals.
---

### Hector MacDonald

Hector MacDonald is a strategic communications consultant who has advised some of the world's top corporations in the areas of financial services, telecommunications, technology and healthcare. He has also written four novels, including the best-selling thriller _The Mind Game_.

