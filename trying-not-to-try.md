---
id: 58bea6168eba16000496c7f5
slug: trying-not-to-try-en
published_date: 2017-03-09T00:00:00.000+00:00
author: Edward Slingerland
title: Trying Not to Try
subtitle: Ancient China, Modern Science, and the Power of Spontaneity
main_color: EBB531
text_color: 9E7A21
---

# Trying Not to Try

_Ancient China, Modern Science, and the Power of Spontaneity_

**Edward Slingerland**

_Trying Not to Try_ (2014) is your guide to ancient Chinese philosophy. These blinks explain why you should allow life to manifest itself to you without forcing things to happen. They also introduce ways to live a less stressful life.

---
### 1. What’s in it for me? Discover an ancient path to happiness and inner peace. 

Many of us live stressful, over-complicated lives that make us unhappy. We work long hours in jobs we hate, hardly spend any quality time with loved ones, and seldom feel a true sense of purpose.

So what can we do?

The secret to a life with purpose can be found in ancient China. The Chinese teachers and philosophers who lived millennia ago knew how to live effortlessly and in the present. And, although modernity and ancient China are worlds apart, these teachings are still valuable.

In these blinks, you'll discover

  * why we should listen to our bellies;

  * which daily rituals will help you achieve happiness; and

  * why you should think of people as wounded animals.

### 2. Traditional Chinese philosophy teaches the concept of effortless work through total immersion in process. 

Can you remember the last time you were completely engrossed in an activity like playing with a toddler or solving a challenging puzzle?

Well, Chinese philosophers have a word for this experience — _wu-wei_. Pronounced "oooo-way," it roughly translates as "effortless acting." In other words, it refers to being so wrapped up in an experience that you become one with the experience itself.

This idea poses interesting questions about the relationship between the body and the mind. People tend to think of the mind as connected to rational thought and the body as more instinctive. We separate them along these lines as different entities.

But for _wu-wei_, the goal is to bring these two "selves" together to work as one. The result of this marriage is an intelligent spontaneity that adds a certain fluidity to the act of working.

Just take the author, who experiences _wu-wei_ when he writes. During these moments, he's in a kind of writing zone, entirely absorbed by the process and overcome by a feeling of relaxation and pleasure with his work.

Or, for other examples, we can look to Taoism, the Chinese philosophy that celebrates living in harmony with the natural flow of the universe. In the Zhuangzi, one of the foundational texts of Taoism, there's a story about a woodcarver who makes ornate bell stands.

Before he begins work, he fasts for seven days, bringing himself to a point at which his mind is so still that he forgets his body even exists. From there, he enters the forest to find the single tree in which he can see the bell stand. Once he has selected the specimen, he gets to work.

This story describes an essential aspect of _wu-wei_ — it makes you whole. When experiencing this phenomenon, your mind stops seeing your body as a separate force. You don't think, you just act, guided purely by instinct. Work done while in this state reflects the beauty, sincerity and ease of the process.

### 3. The Chinese concept of effortless work leads to great power and a path to becoming a perfect human. 

So, _wu-wei_ unites your whole being, but people who are in this state also have _de_, a concept that refers to power, virtue and charisma. _De_ transports one into a perfectly spontaneous mind-set.

For a ruler, _de_ is a source of power and influence. When leaders reach this state, they no longer need to threaten anyone because people are compelled to obey them. If you're an average person, having _de_ will make people like you, feel attracted to you and even stop wild animals from threatening or attacking you.

But why are we attracted to people with _de_?

Because they're in touch with the _unconscious mind_. We trust this mind, sensing that it allows people to act from their natural instincts, not from any sneaky forethought.

Those are just some of the benefits of _wu-wei_ and _de_. They are also connected to _The Way_, the ancient Chinese concept of heaven. For the ancient Chinese, heaven wasn't a place, but rather a path or journey that led to becoming a perfect human being.

When someone has _de_, they're automatically on The Way because they act spontaneously, using the virtues of their subconscious as a guide instead of their imperfect, rational mind. To think of this spontaneity through a more modern lens, consider the concept of _flow_, defined by the psychologist Mihaly Csikszentmihalyi.

For him, being in the flow means concentrating to the point of losing a sense of oneself and of time. The only thing standing between such Western thinking and comparable concepts in the Eastern tradition is that, according to Csikszentmihalyi, to get in the flow you must constantly challenge your sense of ease, whereas in the Chinese conception, you're encouraged to move with ease throughout life, avoiding any difficulties that might harm your spirit.

> _"_ De _is like a halo that surrounds someone in_ wu-wei _and signals to everyone around: 'Heaven likes me! You should too! I'm okay.'"_

### 4. Confucianism relies on a more strict practice to attain wu-wei. 

So, Taoism offered one concept of attaining _wu-wei_, but not every Chinese thinker got on board with it. The philosopher Confucius developed another strategy for becoming a perfect human being.

His school of thought, known as _Confucianism_, sought to diminish the natural aspect of our being, transforming it through education and effort in the service of attaining _wu-wei_. Confucius and his successor Xunzi believed that to reach _wu-wei_ you need to work on your conscious mind. In other words, you had to improve your willpower, behavior and knowledge of the cultures that came before yours.

In this respect, rituals that taught people social manners, like how to eat and how to behave in public, were essential for reaching _wu-wei_. For instance, Confucian rituals assign roles to participants with a clear repertoire and a specific set of duties. One such edict states that children must ask their parents about their health a certain number of times every day.

But why does Confucianism stress strict behavior to such a great extent?

Well, it's about the way the conscious mind works. To reach its full potential, the conscious aspect of your mind needs practice.

For example, when you learn to drive, you first focus on verbal instructions from your driving teacher. At this point, the prefrontal region of your brain, which is responsible for conscious awareness, is running full speed, which is why you need to concentrate.

As you practice this action, your focus slowly transfers to the basal ganglia, a group of neuronal clusters that control automatic motor routines. Gradually, the basal ganglia and relevant motor regions can master skills. As they do so, brain activity previously responsible for such skills drops and, as these skills become automatic, the conscious mind can take a break.

So, once you get good at driving, you can drive while talking to a friend or doing another task. Similarly, Confucius' goal was to help people build the power of their subconscious minds through conscious repetition.

> _"Human beings are above all cultural animals, and that means that any type of_ wu-wei _worth wanting is going to require education and effort."_

### 5. Another school of Taoism teaches its followers to attain wu-wei by not trying. 

If Confucius thought the path to heaven was paved with proper social conduct, Laozi, the great thinker and founder of Taoism, originally thought just the opposite in the sixth century BCE. He believed that the best way to reach heaven was to forget what society had taught you and return to human nature.

In other words, he thought the Way of Heaven was to stop trying to be a specific way, to forget all you'd learned and connect with a raw version of yourself. To put it more poetically, Laozi wanted people to think more with their bellies and less with their eyes.

That's because the belly is the center of our basic desires and, if we let it guide us, we will only want simple things, like food, water and shelter. However, if we open our eyes we begin to desire things that we don't have and likely don't need.

For instance, our belly might be comfortable with our current car, but our eyes can see a newer and nicer car in our neighbor's driveway. Immediately our satisfaction with our car diminishes — even though nothing about the vehicle has changed.

That's why for Laozi, the only way to _wu-wei_ was to do absolutely nothing. He described this as "going home," by which he meant thinking less with the conscious mind and more with the unconscious. To promote this change, he would offer conceptual riddles that people had to meditate on to understand.

His goal was for people to reach a particular state that resembles one you may already have experienced. Scientists refer to it as _runner's high_ since it's triggered by the stress that vigorous physical activity puts on the body, causing the brain to downregulate regions like the prefrontal cortex, which usually controls conscious thinking. As a result, people in this state find themselves running on a kind of blissed-out autopilot.

### 6. Wu-wei can be attained through a balanced and flexible mentality. 

If Confucius and Laozi feel too extreme for you, you might be interested in the philosopher Mencius, whose thinking fell somewhere in between. His belief was that you don't need to change anything since you're already _wu-wei_. The only task is to realize this fact and nourish it.

So, instead of spontaneously discovering _wu-wei_, you find it under the guidance of a wise teacher who helps you until your _wu-wei_ is strong enough to take over. Such a teacher would show you how to find a virtue you already have, focus on it, strengthen it, and expand it through a mixture of introspection, practice and imaginative extension.

This last element is key because we think in images, and learning depends a great deal on the power of the imagination. Say you have a deep empathy for animals but are cruel to people. To change your behavior toward humans, you need to project the empathy you feel for animals toward people, imagining what it would be like to be someone who suffers as much as a wounded animal.

Another "in-between" theory can be found hidden in the Zhuangzi. In fact, many of these texts encourage people to move beyond the dichotomy of wrong and right.

How come?

Well, saying that something is wrong means that you value something else as right and vice versa. To attain _wu-wei_, you need to abandon this logic and instead adopt a neutral stance toward all things.

The sages who followed the Zhuangzi lived their entire lives without adhering to strict values. They had vague goals, such as the journey to The Way, but since they were free from judgment, they could transform any part of their lives with absolute ease whenever they needed to.

In this way, the Zhuangzi wants people to learn calmness and flexibility, to abandon attachments to ideology and to be ever-ready for the inevitability of change.

### 7. To attain wu-wei, practice with sincerity and a higher purpose. 

So, there are a few different schools of thought when it comes to attaining _wu-wei_, all of which urge followers to reach a state of complete spontaneity and unselfconsciousness. However, that's easier said than done. The problem is honesty.

You can't achieve _wu-wei_ if you're not sincere, both with yourself and with others. After all, if you don't truly love The Way, there won't be a powerful emotion propelling your practice. As a result, you won't find the flow you're looking for.

For example, if you want to be a generous person, it's not enough to just say you're generous and act charitably around the holidays. Instead, you need to guide your whole life by the principle of generosity and dedicate yourself to it in everything you do.

That means finding a purpose that's higher than simple generosity. If you can form an intention that becomes the foundation for your generosity — like treating other people as _you_ want to be treated — it'll be much easier for you to abide by it. Uncover this underlying purpose and you'll be able to live a life that's wholeheartedly generous.

In fact, there are certain ways in which you can shape yourself to help attain the natural and sincere flow of _wu-wei_. The basic idea is simple: choose one of the strategies espoused by the Chinese thinkers, practice that mode and repeat it until it becomes sincere, pervading your entire character.

In case you were worrying about it, following such a plan doesn't require you to read complex theories all day. The early Chinese philosophers understood that people exist to do, not to think. That's why they developed action-oriented models of perfection that put your brain and body to work through physical practice, visualization and meditation.

Just give it a try. You too can live the life you want and enjoy every moment of it.

### 8. Final summary 

The key message in this book:

**Many ancient Chinese philosophies promote a relationship to life that is effortless and flowing. However, they vary in how they think people can attain this result. Some offer strict rules, while others urge a total lack of effort.**

Actionable advice:

**Enjoy your next date by focusing your attention.**

The next time you're on a date and want to appear cool and relaxed, direct your focus to the present and try to simply notice the moment and avoid analyzing your every thought and action. By doing so, you'll be able to truly connect with the other person and enjoy a spontaneous conversation. You'll experience _wu-wei_ and the power derived from _de._

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Wherever You Go, There You Are_** **by Jon Kabat-Zinn**

_Wherever You Go, There You Are_ (1994) explains how to fully enjoy the present moment without worrying about the past or the future. By providing step-by-step meditation practices, both formal and informal, that can easily be incorporated into everyday life, Kabat-Zinn steers us toward the peace and tranquility that we're yearning for.
---

### Edward Slingerland

Edward Slingerland is an academic and author who specializes in cognitive science and Chinese thought. He has previously authored _What Science Offers the Humanities._

