---
id: 575011527bea3d0003c715ce
slug: tubes-en
published_date: 2016-06-10T00:00:00.000+00:00
author: Andrew Blum
title: Tubes
subtitle: A Journey to the Center of the Internet
main_color: FDF633
text_color: 96931E
---

# Tubes

_A Journey to the Center of the Internet_

**Andrew Blum**

_Tubes_ (2012) traces the origins of the internet, from its humble origins at a few US universities to its current superstructure status. You'll find out about the physical components of the internet, including fiber cables, hubs and massive internet exchange points.

---
### 1. What’s in it for me? Become a connoisseur of the internet. 

Are you one of those people who just can't wrap your head around the internet, made up of those seemingly abstract connections floating around in the sky?

Then listen up because these blinks will take you right to the heart of the internet and show you the physical places where it exists. Turns out, the internet is not just a magical cloud that allows us to share our lives but is actually made up of physical places all around the world that connect different networks to one another. Without these vital places, you'd never get online.

In these blinks, you'll learn

  * where the biggest hubs of internet networks are located;

  * what it takes to load a website; and

  * why Facebook is known as a "peering slut".

### 2. The internet may seem purely virtual, but it owes its existence to a few very physical places. 

The internet is everywhere: in our houses, on our phones, in our cities. You're even reading this blink online right now. Yet when we think about how the internet works — and where we can find it — most of us draw a blank.

The truth is that, despite how much we use it, most of us don't understand the internet. We use smartphones to send emails, Skype our families in different countries and watch movies wherever we are. Yet if we're asked where our internet comes from, we usually can't get past the router blinking away in a corner of our home.

But the router is only the final link in a long chain of connections: your router is connected to a fiber cable, which is connected to more cables that travel to bigger hubs, and these hubs are connected to even more cables and hubs. Information is sent in the form of bits of light throughout the network all over the world, allowing everyone to communicate and access the connected information.

While that is still quite abstract, there are places where you can go and see the internet in the physical world: the hubs.

Internet hubs are in buildings straight out of a sci-fi movie: towering gray walls with no company name on display outside, while thousands of routers blink in the dark as fans roar to keep the machines cool inside.

The biggest of these hubs are located in well-known cities, like Frankfurt, Palo Alto, London and Tokyo. But there are also thousands of miles of cables beneath the sea, connecting the continents of our world.

Now that we know a bit more about the physical nature of the internet, let's take a look at its origins.

> _Today over two billion people use the internet._

### 3. The internet started slow but blew up in the 1980s. 

How much do you know about the origins of the internet? Chances are your knowledge doesn't go much deeper than the jokes about Al Gore inventing it.

In the beginning, only a few people used the internet, and it was mostly for academic purposes. When the internet first went online in 1969, it was made up of a few different networks at four US universities, which allowed faculty and students to share research and academic papers more quickly. Back then, there were only about 5,000 users: so few that there was a physical "phonebook" of users so you could locate people to communicate with.

At first, widespread internet use was hindered by the fact that all the networks spoke different programming languages and couldn't communicate with each other. Trying to find someone in a different network was nearly impossible!

But all that changed in 1983 when TCP/IP became mandatory for every online network. This official lingua franca allowed networks that had never communicated before to exchange information and build new connections.

And so the internet began to bloom.

While there were only 15 autonomous networks online in 1982, there were more than 400 in 1986. Computers with internet access rose even faster, with 2,000 networks in 1985 growing to 159,000 in 1989.

And it didn't take long for the internet to infiltrate virtually every part of our lives.

Everybody uses it today: companies store their data and sell their products online, and people use their smartphones to manage their finances, post selfies and read up on current events from around the globe.

But how does the internet allow us to do all that?

### 4. The internet is a network of networks – and the more networks, the merrier. 

Some things grow slowly. Others grow steadily.

And some grow like the internet.

Indeed, the internet has grown exponentially over time. While there were only a handful of networks at the beginning, there are now over 35,000, including giant networks like Facebook and Google.

Although such big networks might seem like they'd be a mess of data, they're actually a good thing: the more connected a network is, the faster and more efficient it gets.

After all, that's the way the internet works. When you click on a link, light bits containing information travel back and forth through fiber cables. If a cable directly connects two networks instead of having to travel halfway around the world to a third router, the information can travel a lot faster. So the more interconnected networks there are, the quicker information will get through.

For example, when you go to Google, search for NASA's website and click on it, the site loads almost instantaneously because Google and NASA have a cable plugged between their routers.

And that's why networks need hubs.

A hub is a massive space where many companies can store their routers and connect to each other. Internet exchange providers recognized this need and started to build and rent enormous buildings to connect networks.

These internet exchange points, such as ECIX or Equinix _,_ are the places where hundreds of networks connect their routers to the mainline, a tube filled with fiber that connects one internet exchange to another. These hotspots of interconnectivity allow internet users to enjoy increasingly faster internet.

### 5. The internet is run by a small group of people who try to peer with each other. 

Have you ever noticed that some websites load a lot faster than others? While it might take a while to get from a Google search to your local hairdresser's website, Facebook will probably load a whole lot faster. The secret lies in their connections and whether the networks like to work with each other.

And that's because networks try to _peer_ together.

There are regular conventions like NANOG (North American Network Operators' Group), where the people who "run" the internet — meaning the big networks, internet providers and the internet exchange points — try to connect with their fellow businessmen and -women.

They seek to peer with other networks. Peering means agreeing to plug a cable directly from router to router, allowing the bits to travel shorter distances and increasing the connection speed.

Some companies — like Facebook — have such an open peering policy that they're jokingly called _peering sluts_. If you want to peer, and know how to look after your router in case something goes wrong, they'll happily peer with you.

But companies can decide to stop peering with each other and literally pull the plug. Sometimes a company wants to be paid more for granting another company access to their hub, and their partner refuses. And when this happens, it can cause huge problems for internet users.

For example, when the plug was pulled between internet exchange providers Sprint and Cogent in 2008, the US Department of Justice, NASA and New York Courts couldn't send emails to each other for three days. Not to mention the millions of average users who couldn't use the internet.

### 6. Underwater cables and data storage centers lie at the heart of the internet. 

The internet connections around the world are not only found at internet exchange points. In fact, the real connections go far deeper.

Like under-the-sea deep.

In fact, the underwater connections between continents could be called the veins of the internet.

The idea of using underwater cables to communicate dates back 150 years, when the first telegraph lines, composed of thousands of miles of copper cables, were laid in the depths of the oceans. The same happens, today, with fiber cables: every year fiber cables are spread across the ocean floors to connect countries and continents — be it Somalia, Brazil or the United Kingdom.

But when these connections fail, it can cause enormous problems.

Natural disasters like tsunamis can do serious damage to internet connections. And if important cables get damaged, it can take entire countries offline. Just look at what happened in Taiwan in 2006: a huge earthquake destroyed a good part of the fiber cables in the Luzon Strait, knocking most of South Asia, China and Hong Kong offline.

While underwater cables keep the internet flowing, the data they transport is the real heart and soul of the internet. And the data storage centers, which safeguard this data, are always growing.

Today we store most of our data and personal information in the cloud instead of on physical hard drives. Take our internet identities: all the emails, Tweets and images we send on a daily basis that communicate our identity are primarily online.

Add all that together and it makes for one busy sky. But the cloud is actually composed of hard drives and, every day, the amount of storage space needed increases. To name one example: in 2011, Facebook reported that every month over six billion pictures were uploaded using their service.

This means that data centers like the one in The Dalles, Oregon, have to grow to the size of villages to accommodate the huge amount of data they store. But because they store all our data, they're well protected and often shrouded in secrecy.

> "_A data center is the storehouse of the digital soul._ "

### 7. Final summary 

**The key message in this book:**

Despite its largely invisible nature in everyday life, the internet is a very real, physical thing, made of cables, hubs and data centers that create one giant network spanning the whole world.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The Soul of a New Machine_** **by Tracy Kidder**

_The Soul of a New Machine_ is a Pulitzer Prize-winning book about one team at the computer company Data General who was determined to put its company back on the map by producing a new superminicomputer. With very few resources and under immense pressure, the team pulls together to produce a completely new kind of computer.
---

### Andrew Blum

Andrew Blum is a New-York based journalist who has published his work in _Wired, Wall Street Journal, Vanity Fair_ and _Popular Science_.

