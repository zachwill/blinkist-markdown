---
id: 54a986006233320009000000
slug: turn-the-ship-around-en
published_date: 2015-01-05T00:00:00.000+00:00
author: L. David Marquet
title: Turn the Ship Around
subtitle: A True Story of Turning Followers into Leaders
main_color: D3B02A
text_color: A18620
---

# Turn the Ship Around

_A True Story of Turning Followers into Leaders_

**L. David Marquet**

_Turn the Ship Around_ reveals the story of how one United States Navy captain managed to turn a dissatisfied submarine crew into a formidable and respected team. But how did he do it? By changing the way we think about leadership, this story will show you that inside, we all have the power to be leaders.

---
### 1. What’s in it for me? Learn how to turn a team around from an inspiring military commander. 

If you're a manager, you know that constantly improving your leadership skills is important, so you can keep your team focused and moving in the right direction.

But what if your strategies aren't working? What if employees seem increasingly listless, uninspired by the work you assign?

Perhaps it's time to reconsider how you lead. In these blinks, you'll learn from the first-hand experience of L. David Marquet, a U.S. Navy officer who transformed one of the fleet's worst-performing submarines into a well-oiled, effective team.

Marquet learned that encouraging others to lead made him a better leader, and in turn, empowered his whole team to perform better and succeed together.

In the following blinks, you'll also discover

  * why the leadership style that helped build Egypt's pyramids is no longer relevant;

  * why the best way to lead is to create more leaders around you; and

  * why briefing puts people to sleep instead of inspiring them.

### 2. The United States is facing a crisis of leadership, and this is bad news for business. 

Be honest: Do you like your job? If you answered "no," you're not alone.

Job satisfaction in the United States is at an all-time low. From 2004 to 2012, less than half of U.S. workers considered themselves satisfied with their jobs; in 2009, a Conference Board Survey showed worker satisfaction to be at its lowest rate ever.

Dissatisfaction in the workforce isn't the only trend that's emerged in recent years. We've also witnessed a considerable drop in productivity, with up to $300 billion lost due to productivity issues in the United States alone.

Times have also been tough for those outside the workforce, as U.S. unemployment sat at 9 percent for a record 31 months, up until November 2011.

The symptoms, a doctor might say, are bad. Yet before they worsen, it's crucial that we work out where the illness actually is. To do so, we've got to examine the structure of organizations themselves.

For centuries, we fashioned our organizations according to a particular style of leadership. The Egyptians harnessed it to build the pyramids; this strategy also drove the Industrial Revolution.

We're talking about the _leader-follower_ approach, where decisions are made by a leader (a boss) and carried out by followers (the workers).

A leader-follower system is perfectly suited for work requiring physical labor, allowing menial tasks to be delegated among many individuals to increase efficiency. However, many more jobs today are dominated by cognitive tasks and decision-making, not at all suitable for a "follower" workforce.

As a result, the leader-follower approach is ill-suited to the times. Think of it this way: it's a great strategy for building pyramids, but not for operating a nuclear-powered submarine or even starting a technology business!

To truly empower workers, we need to overhaul our idea of leadership. Find out how we start in the next blink.

> _"What do you do on board?"..."Whatever the hell they tell me to.'' — First class petty officer aboard the USS Santa Fe, in response to the author._

### 3. A leader-leader approach, where everyone pitches in, is the key to organizational success. 

It's clear that our leader-follower mentality needs a makeover. So what are the alternatives?

If we want to create a satisfied, empowered workforce, our best bet is to adopt a radically different leadership structure: a _leader-leader_ system.

This approach proves that leadership isn't just a magical quality bestowed on a select few. Instead, it recognises that we are all leaders, in some fashion.

The difference between a leader-leader and leader-follower approach is reflected in the way decisions are made.

In a leader-follower structure, information is sent up the chain of command, and a decision is made only once the information has reached and been digested by those at the very top.

Yet in a leader-leader system, the power to make decisions is distributed throughout the chain of command, allowing individuals to act even on newly arrived information.

For example, a navigator realises his submarine is off course and in very shallow water. Instead of alerting his commander first, he jumps into action, rectifying the problem safely and effectively.

This example isn't hypothetical — the author experienced it firsthand.

A nuclear-powered submarine with the U.S. Navy, the _USS Santa Fe_ was notorious for its poor performance. It also had the worst crew retention rate. That is, until the author became its commander and used the leader-leader structure to take the sub from one of the worst performing in the fleet to one of the best.

Tactical effectiveness went from "below average" to "above average to excellent" in inspections, some 36 crew members chose to re-enlist, and the _USS Santa Fe_ was awarded the Arleigh Burke Trophy for most-improved ship in the fleet — all thanks to the author's leadership strategies.

A leader-leader approach clearly is worthwhile. But how do you implement it? We'll see in the following blinks.

> _"Don't move information to authority, move authority to information."_

### 4. To make your employees more productive, empower them by giving them more responsibility. 

So now you know that a leader-leader approach can help your team perform more effectively. Yet how do you start to implement it in a business?

Establishing a leader-leader system begins with rewriting the basic foundations — the DNA, so to speak — of your organization.

You need to ensure that decision-making involves employees and is sustainable. To do this, however, as a manager you may need to give up some of your power. While uncomfortable, it is the only way to create change in a deeply ingrained hierarchical system.

On the USS Santa Fe, this meant moving power traditionally reserved for captains down the chain of command to _chiefs,_ who were responsible for each division in the submarine. Before anything else, the author talked with each chief to learn how he could empower them.

He found that the first thing the chiefs wanted was greater responsibility for the men in their divisions, starting with the ability to approve leave time. Formerly, this involved chiefs signing off on a leave application, which was then sent through three different officers _,_ a department head _and_ an executive officer, above them all.

Ending the inefficient process, the author gave his chiefs the power to fully approve leave. With greater responsibility for their divisions, chiefs became more passionate about the performance of their men and began to take charge of improving efficiency overall.

However, reassigning responsibility isn't the only way to empower a workforce. In fact, the USS Santa Fe made use of a simple three-word phrase to actively involve the entire crew in the running of the submarine. The phrase was: "I intend to…"

For example, instead of a navigator _requesting_ to change course, he would simply inform the captain of his decision by saying, "I intend to alter our path." The captain could then quickly approve this by saying, "go ahead." Yet the decision essentially was made by the navigator.

We've seen two examples of how passing control down the chain of command is at the core of a leader-leader system. However, other steps are needed to sustain such a dynamic.

Next, we'll discover the pillars of _competence_ and _clarity,_ and why they are so vital.

### 5. When you give employees more responsibility, you also need to ensure they can handle it. 

Giving more responsibility to employees often seems like a risk. You know that such a move is empowering, but are you sure they'll be competent enough to handle it?

Fortunately, there are practical mechanisms that ensure your workforce builds and maintains their competence levels. The first of these concerns "taking deliberate action."

Once, on the USS Santa Fe, a crewmember breached protocol and shut off a circuit breaker earlier than he should have. The captain had to ensure this would not happen again. The crewman accepted that he went against protocol, so extra training wasn't an answer. Nor was more supervision, as that was already in place.

The problem was a lack of attention, which the captain solved by introducing a policy of deliberate action. The crew would now pause, vocalise and gesture toward what they were doing before they did it.

Whether turning a valve or changing course, _taking deliberate action_ reduced errors in any setting regardless of the task, as it allowed others to monitor and correct any mistakes before they could even occur.

This technique saw the USS Santa Fe go on to receive the highest grade ever given on nuclear-reactor operations inspections. Yet there was another vital change on the USS Santa Fe that boosted crew competence.

When assigning tasks, emphasis was shifted from _briefing_ to _certifying_. When briefing, you can only ensure the competence of the person giving the brief. Those _being_ briefed often daydream or stop listening halfway through, as the task is a familiar one, and they've heard it before.

Rather than just giving information, _certifying_ made crewmembers answer questions about the task they'd been assigned. If the answers were acceptable, the crewmembers were then "certified" to take on the task in question. In this way, underprepared or inattentive teams can be identified and addressed, when a crewmember can't respond adequately to questions about a task.

In this way, the leader-leader structure on the USS Santa Fe boosted both employee satisfaction _and_ performance. However, one more element was crucial in bringing this newly empowered team to its full potential, as we'll see in our final blink.

### 6. Corporations need to state their goals clearly, so a leader-leader strategy can best succeed. 

In a leader-leader organization, the power to make decisions lies with everyone. Because of this, it's vital that every employee is on the same page and working toward the same goals.

But how do you ensure this?

To keep your organization united _and_ productive, it's vital that core values are upheld. One way to do this is to inspire employees with the company's legacy or history.

The crew of the USS Santa Fe emphasised the historical importance of their work in several ways, including making an announcement to the whole ship whenever it passed a sunken U.S. submarine. This way, the crew's sense of purpose — that of serving and protecting the United States — was bolstered.

Corporations can also use the company's legacy to inspire. For example, Apple regularly holds employee conferences, in which the philosophy of "thinking differently" is highlighted through Apple's history of innovation. This reinforces original thinking as a corporate and personal goal in the minds of Apple employees.

Core values can also be strengthened by rewarding employees that uphold them in the tasks they perform. However, administrative issues often get in the way of giving immediate praise — rewards often take days to be received, by which time the action is already long forgotten.

On the USS Santa Fe, the author understood that praise had the greatest impact when good performance was recognised immediately. So if a sailor avoided a disaster through quick decision-making, the success was announced to their division shortly afterward, immediately reinforcing the core values of the ship.

The way that rewards are structured can also have an impact on an organization's productivity. Man-versus-man rewards, where employees are encouraged to compete against each other, are unproductive. However, man-versus-nature rewards pit employees against an external enemy, which helps build camaraderie and a stronger work ethic among workers. An example would be rewarding employees when the company's stock price surpasses that of a competitor.

The USS Santa Fe demonstrates the success of the leader-leader approach in both increasing productivity and empowering workers, simply by changing the way we think about leadership itself.

Think back to the statistics cited in the first blink. It's clear that the U.S. economy could also do with an overhaul! If organizations everywhere took steps toward establishing a new leader-leader dynamic, just imagine the success we would achieve!

### 7. Final summary 

The key message in this book:

**A top-down approach to leadership is ineffective and does not tap the natural leadership abilities we all have. A switch to a leader-leader organizational style takes only a few institutional changes but does require a complete reevaluation of the way we look at leadership. Yet the benefits of such a switch will be felt both by workers and the organization itself.**

Actionable advice:

**Communication is key!**

When given a project, don't try and complete it perfectly before showing your work to your boss. Keep her up to date with short, but early and frequent conversations to make sure you are both on the same page in terms of what your goals are.

**Suggested further reading:** ** _Leaders Eat Last_** **by Simon Sinek**

_Leaders_ _Eat_ _Last_ explores the influence that neurochemicals have on the way people feel and consequently act, and examines the discrepancies between how our bodies were designed to function and how they function today. Ultimately, we need true leaders to direct us back on the right path.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### L. David Marquet

David Marquet graduated at the top of his class from the U.S. Naval Academy before commanding from 1999 to 2001 the USS Santa Fe, a nuclear-powered, fast attack submarine. Marquet now works as a leadership consultant and is a lifetime member of the Council on Foreign Relations.

