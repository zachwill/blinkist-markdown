---
id: 5e27061e6cee07000686af34
slug: twas-the-nightshift-before-christmas-en
published_date: 2020-01-24T00:00:00.000+00:00
author: Adam Kay
title: Twas The Nightshift Before Christmas
subtitle: None
main_color: None
text_color: None
---

# Twas The Nightshift Before Christmas

_None_

**Adam Kay**

_Twas The Nightshift Before Christmas_ (2019) details the bizarre and tragic experiences of a doctor working for the British National Health Service, known as the NHS, during the dreaded Christmas shifts. Providing a hilarious and eye-opening behind-the-scenes glimpse into the pandemonium that envelops hospital wards around the United Kingdom during the festive period, it also pays tribute to all the NHS staff who sacrifice their holidays each year to save lives, deliver babies, and remove Christmas paraphernalia from places it doesn't belong.

---
### 1. What’s in it for me? Be overwhelmed with admiration for the unsung heroes in the NHS. 

Most of us take it for granted that for a few days at the end of every year we can hang up our coats, put our phones on silent, and focus all our efforts on reaching the bottom of a chocolate box. But while we're lobotomizing ourselves with endless shots of schnapps and eating like it's a competitive sport, over a million NHS staff are working through the holidays to pick up the pieces when things inevitably go awry.

These blinks are a tribute to all the people, from porters to doctors, who sacrifice their Christmas holidays to provide essential care services to the British public. They're an occasion to offer our respect and gratitude to the people through whose herculean work the NHS continues to function despite the many difficulties that ail it. They're a reminder that the NHS shouldn't be taken for granted, and that the nation owes it to the NHS staff to save the system that has saved all Brits at some point or another.

A short warning before we begin: These blinks contain descriptions of blood, death, miscarriage and abortion.

In these Blinks, you'll learn

  * why the Christmas season places extra strain on the NHS; 

  * how a novelty Christmas tie can be used to deliver tragic news; and

  * why being a medical professional can put a huge strain on your romantic life.

### 2. NHS Hospitals attempt to partake in the festive cheer – with disturbing results. 

Once a year, at the end of December, the British public collectively succumbs to a fever dream of festivity, trading its usual tight-lipped demeanor for uncharacteristic cheer and goodwill. 

For two weeks, all the normal standards of behavior are turned on their head. People go spelunking into their wardrobes to dredge up the most garish clothing they can find; they actively seek out the relatives they spend most of the year avoiding; and the concept of daily routine goes out the window, replaced with family board games and carb-induced coma-naps — this is the only time of year the British practice siesta. 

It's a normal and beloved part of British life. But even though the rest of the country's infrastructure is declining into seasonal dysfunction, health services are too vital for hospitals to close their doors for a week or two. That doesn't stop them from trying to partake in some of the Christmas cheer. Unfortunately, however, festivity combines with the gore and tragedy of a hospital about as well as eggnog combines with amniotic fluid.

Take the festive sweaters that many wear in the ward, for example — in close proximity, they cause the air to crackle with static electricity, transforming people into human van de Graaff generators. It's also somewhat unnerving for patients to be given serious medical diagnoses by people wearing novelty apparel. Would you want your prostate examined by a man wearing a Rudolf sweater? 

Things don't improve much when it comes to ambiance. Many hospitals get festooned with morbid facsimiles of standard Christmas decorations, things like wreaths fashioned from unrolled condoms and Christmas tree angels sporting umbilical cords.

One year, the nurses in the author's ward adorned googly eyes and red noses to several specula — devices used to open orifices — to create the world's creepiest looking reindeer. 

Another misguided attempt at creating a festive feel in the ward led to a whole room having to be evacuated and deep cleaned. The cinnamon and mulled-wine scented diffuser mingled with the hospital smells of blood, placenta, and feces to produce a stench as potent as noxious gas.

Of course, there are some touching moments to be found as well. There was a tradition in the author's ward for the head surgeon to slice a turkey in the break room while asking a nurse to pass him each utensil one at a time as though performing surgery — "Knife please."

Christmas in the ward might not be as cozy as at home, but given the circumstances, it's good enough.

### 3. Festive periods place extra strain on the NHS. 

People winding down for the holidays hardly tempers the number of accidents and emergencies — far from it. 

Amid the usual scrapes and breaks, the Christmas season gifts the NHS its own sleigh full of charmingly festive ailments: diabetic comas caused by cake overconsumption; black eyes from poorly aimed champagne corks; turkey bones that refuse to go down; burns from roasting pans; Christmas light electrocutions; and fingers amputated by absent-minded parsnip-choppers are just a few of the incidents.

Of course, there are also the inevitable crimes of passion that ensue when the family spends too much time together, has too much to drink, and is disappointed that Santa didn't get them what they asked for.

On top of this, there's a noticeable spike in costume-related injuries. Deciding he would go to the office Christmas party dressed as a turkey in the oven, one patient wrapped himself head to foot in foil. He lasted for a few hours before falling over from blood loss, the razor-sharp foil having desiccated his flesh, much like a turkey _after_ it's been sliced up.

Sadly, not all holiday tragedies are accidental. Consider the act of _granny dumping_. This is the heart-wrenching practice of off-loading elderly or infirm relatives on the NHS, alleging some vague medical complaint, so the family can be rid of the responsibility of looking after them over Christmas.

As sad as this is, there's an even more troubling trend in the UK. Surrounded by ads featuring smiling couples, family-oriented Christmas movies, and past-their-prime pop stars singing about how much we're all enjoying ourselves, many people understandably feel lonely this time of year. People check themselves into the hospital over Christmas just so they can have some company.

One Christmas, the author was making the rounds trying to discharge as many patients as possible so they could return home in time for Christmas. But, when he broke the news to one elderly patient, her face collapsed. The patient mumbled an "OK" and turned away. The author quickly observed that, on second thought, her wound was actually looking a little red, and he suggested she spend an extra night at the hospital so they could monitor it. The patient visibly relaxed. Who knows what situation she would have faced at home, but at least the NHS could offer her some company on Christmas day.

### 4. The NHS is beleaguered by ill-conceived “efficiency” initiatives devised by out-of-touch executives. 

Most people in Britain realize that the NHS is underfunded. What they might not realize, however, is just how much of an understatement "underfunded" is in this case!

When executives at the author's hospital cut costs by eliminating "inessentials," they ended up axing things most people would consider basic amenities — the cafeteria, for one, which got replaced with vending machines.

Lack of funding is to blame for the NHS lagging decades behind when it comes to technology, too.

The author remembers his hospital upgrading from a paper system to a fax system — in 2006, by which time there were already exhibits dedicated to fax machines in technological history museums.

The author got accustomed to his hospital's antiquated technology, so much so that when he once witnessed the child of a patient working diligently away on a new laptop, it looked completely out of place to him, like something out of a sci-fi movie. It was the most high-tech computer he'd ever seen inside an NHS hospital. 

The proposed answer to the NHS's funding malaise, a series of ill-fated "efficiency" initiatives, ended up causing more problems than they solved. Supposedly intended to improve working conditions for staff, they were really just hapless schemes to save money.

A case in point was when hospital execs announced one Christmas that the staff's blue and green scrubs were going to be replaced with festive red ones. It soon transpired that this was a cost-cutting ruse. The execs figured they could save money by washing scrubs less frequently since blood doesn't show up as easily on red.

Then there were the scrubs dispensers — vending machines that would issue a fresh pair of scrubs at the wave of a key-card. A great idea in principle, but naturally execrable in execution. The machines issued a daily maximum of three pairs of scrubs, hardly enough for a full shift, so doctors sometimes had to walk around covered in blood. 

The hospital burdened the staff with yet another efficiency "upgrade" when the execs installed voice-activated switchboards to replace manually entering phone numbers. The idea was to make communication between wards easier, but unfortunately, the voice-recognition software couldn't understand regional accents. The accent that it was programmed to understand was so upper-class that people were forced to repeat themselves over and over in an increasingly caricatured British voice: "Theater, thurta, thee-aarh-tahhh."

In the end, it might have been more efficient to simply ax the execs.

### 5. The demanding working conditions that NHS staff endure are detrimental to their personal lives. 

Over the holiday, 1.4 million NHS staff divvy the shifts between themselves and are lucky if they get to spend a few hours with the family on Christmas day.

Unfortunately for the author, during his stint as a practicing doctor, he pulled the short straw six years in a row. His Christmas day consisted of plundering the fridge for leftover sprouts at 11:00 p.m. before crashing from exhaustion.

For those who have to work, there's no easy way to wiggle out of it. The NHS's HR department might as well be replaced with an automated email that responds to every request with "We're sorry, but no." 

HR is so inflexible that one member of staff wasn't even allowed time off to attend her grandfather's funeral. Compassionate leave is restricted to first-degree relatives only, so the late grandfather didn't make the cut.

Nor can medical staff shirk their responsibilities or slack off like people with normal jobs do when they say they're "working from home." After all, they're responsible for people's lives. 

As you can imagine, the demands of the job put a great deal of strain on the author's romantic life. Every year, too busy to even think about his phone during his shift — he once delivered six babies on Christmas day — he'd end the day to find his phone inundated with a series of missed calls and increasingly dejected text messages from his partner, who had hoped to speak with him.

Six missed Christmases proved too many. Eventually, he and his long-term partner made the decision to separate.

But, it isn't all grief and gloom in the hospital. Love finds a way to blossom even under trying circumstances.

The author and his colleagues only discovered that Molly, one of the midwives, had started dating Petr, an A&E nurse, when Petr arrived at the ward with Christmas dinner for two in a tupperware box. Having colluded with the midwife supervisor to reserve the break room for himself and Molly for half an hour, Petr served up his microwaved offerings in candle-lit style. The author's stomach panged — not for the lackluster food, but for the rom-com corniness of the scene, which made him yearn for a bit of company himself.

After seven years, the author did finally have a Christmas off from work to spend with his family. But, as he observed with humorless irony, he missed the drama of the ward terribly in the end.

### 6. Working in a hospital is extremely emotionally taxing. 

NHS staff spend their lives in the emotional equivalent of the brace position, prepared for disaster at any time. The responsibility they carry, after all, is unimaginable. They hold people's lives in their hands, and sometimes those hands haven't rested for twelve, thirteen, fourteen hours, or more. 

Once, when coming to the end of a twelve-hour shift, the author passed his scalpel a little too deep during a cesarean section, causing him to knick the baby's cheek. While the injury was superficial, it could have been much worse; it never would have happened at the beginning of his shift. 

Slip-ups like this are liable to leave medical staff with a heavy emotional burden. What's more, no matter how hard medical staff try to depersonalize their relationship with their patients, they inevitably end up sharing their existential pains and fears. 

The author once balked when a dying woman in her nineties took his hand and asked, "Is my time up?" He told her not to be silly but felt ashamed of his cowardice for the rest of his shift; he had denied her fear instead of recognizing it as legitimate and offering her emotional support.

Hospitals are minefields for emotional bruises like this. Working day-in-day-out on the ward inures doctors and nurses to horrors that would break ordinary people, but even they have their limits.

The author discovered this when tasked to perform a rare procedure on a young woman who, at twenty-one, suffered from a cardiac condition that meant she was unlikely to survive if she continued with her pregnancy. Four months in, she was forced to make the difficult decision to terminate the fetus.

The author was not prepared for how intensely this procedure would affect him. Although it only took a few minutes, they were excruciatingly long ones.

The woman lying on the bed was suffering the bleakest day of her life, and the author felt guilty for making things about himself in any way. 

But he felt nauseous, and for the first time, he found he had no choice but to shirk his other responsibilities and escape the hospital for a while.

### 7. Tragedy doesn’t observe the festive calendar. 

Somehow, the Christmas cheer doesn't make up for all the death on the ward.

So it was little consolation to the author that, when he had to break the news of an elderly lady's imminent passing to her family, he got to wear his novelty Santa tie. 

No matter the time of year, it's horrible having to break bad news. Some people sob, some scream, and others gaze blankly at you as they retract into themselves. In this instance, the patient's son asked desperately whether there wasn't more that could be done. There wasn't.

While attempting to communicate his sincerest commiseration with the family, the author inadvertently leaned on his novelty tie, causing it to go berserk with a computerized rendition of _Jingle Bells_. Flushing Santa-suit red, he fumbled as he attempted to silence it, but only set it off yet again.

Eventually, he ripped the thing off and threw it in a nursing station. About to apologize profusely, he noticed that one of the children had burst into laughter and the rest of the family was smiling. Maybe there's a curative quality to Christmas after all.

The seasonal antics helped that family deal with their grief. Usually, however, grief is only made more harrowing when surrounded by joy, as though the whole world were mocking.

This was the experience one young couple had when they found themselves in the emergency room on New Year's Eve. They were on their fourth attempt at in vitro fertilization; at six weeks, this was the furthest along so far. Anticipating a child, the couple had thrown every penny they owned into the deposit on a house. 

The expectant mother was experiencing bleeding, the author scanned the woman's belly. Finding her uterus empty, he had to tell the couple that the bleeding did indeed indicate a miscarriage. They begged the author to check again, but the conclusion was the same. Their last chance of having a family was effectively lost.

Just as the shock was passing into grief and tears, there was a ruckus from the other side of the curtain. Someone had turned up the television. They heard a countdown — three, two, one — and the hospital suddenly erupted with applause, cheering, party-poppers, and a chorus of Auld Lang Syne.

The author could only say "I'm sorry. I'm so sorry."

### 8. Final summary 

The key message in these blinks:

**During the seven Christmas seasons that he worked as a doctor for the NHS, Adam Kay dealt with cases ranging from the utterly bizarre to the thoroughly traumatic. Yet, he was only one of many men and women working through the holidays to ensure the British public survived into the new year. It's thanks to the heroic efforts of its staff, who sacrifice their time, health, and social life, that the NHS continues to deliver its vital service despite its many travails.**

Actionable advice:

**Don't forget those who keep us safe and healthy.**

Take a moment in the routine of giving thanks over Christmas dinner to acknowledge all the people who've had to miss Christmas this year because they're at work providing essential care services. Material gestures of thanks are also very much appreciated. If you're going to write Christmas cards anyway, why not add an extra one for those nurses who looked after you during your stay in the ward? Remember, they're real people who need support and care just like anybody else. If you want to go the extra mile, consider donating blood or marrow, and sign up to the organ donation register.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _This is Going to Hurt_** **, by Adam Kay**

You've just read the blinks to _Twas the Night before Christmas_, which is the festive sequel to Adam Kay's million-copy best-seller _This is going to Hurt_. Fortunately, we have the blinks to his original, so you can pick up right where you left off.

The blinks to _This is Going to Hurt_ are much in the same vein, presenting glimpses of life behind the privacy curtain. They provide more detail on Kay's career and romantic tribulations, as well as further commentary on the maladies afflicting the NHS. But most importantly, they offer a fresh assortment of bizarre and heart-wrenching stories that will at times make you laugh, cringe, and maybe even cry.
---

### Adam Kay

Adam Kay is a British screenwriter, author, and stand-up comedian. He's also a former NHS doctor who has incorporated his experiences into his writing and stand-up routines. He's the author of the acclaimed runaway hit _This is Going to Hurt,_ which is currently being adapted into a comedy-drama for BBC2.

