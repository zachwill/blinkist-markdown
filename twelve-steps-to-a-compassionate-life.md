---
id: 54eaecb1393366000a210000
slug: twelve-steps-to-a-compassionate-life-en
published_date: 2015-02-24T00:00:00.000+00:00
author: Karen Armstrong
title: Twelve Steps to a Compassionate Life
subtitle: None
main_color: FBE3BF
text_color: AD7A2D
---

# Twelve Steps to a Compassionate Life

_None_

**Karen Armstrong**

_Twelve Steps to a Compassionate Life_ is a step-by-step guide to bringing more compassion into the world. It shows you in concrete terms how you can cultivate compassion in your everyday life, and helps you to do your part in making the world a better place.

---
### 1. What’s in it for me? Learn how to become a more compassionate person and how to transfer this feeling to others. 

Nothing has done more for alcoholics than the 12 step program of Alcoholics Anonymous. Having a series of basic steps to follow has helped many people with substance abuse problems to get a grip on their life.

These blinks take the successful practice of the 12 step program and apply it to different sphere altogether: compassion. In a world of division, violence and war, we desperately need a dose of compassion. But where do we start? How can we give up our addiction to selfishness and mistrust in others? Read on and find out.

In these blinks you'll discover

  * the power behind Greek tragedy;

  * why we should take the time studying people we don't like; and

  * why many religions are based around the concept of suffering.

### 2. Compassion is our natural ability to put ourselves in somebody else’s shoes. 

When you turn on the TV, you'll find no shortage of horror stories. We see refugees turned away at the border, the brutality of war, starvation and poverty, and an increasing imbalance of power and wealth across the globe. This begs for a counterforce of compassion.

But what do we even mean when we say "compassion"?

_Compassion_ means enduring something with someone. It comes from the Latin word _patiri_, which means "to suffer, undergo or experience." When you feel compassion for someone, you essentially feel that person's pain as if it were your own.

In practice, compassion can be summed up by the _Golden Rule_, which nearly all faiths share: treat others as you would have them treat you.

It was this principle that motivated philanthropists such as the founder of modern nursing, Florence Nightingale, to action. And it is what inspired Martin Luther King, Jr. to fight against the oppression of his fellow African Americans and lead the Civil Rights movement.

If we are to cultivate compassion, then surely it must come from somewhere. But where?

Compassion, as well as its nemesis, selfishness, are hard-wired into our brains.

Our selfishness is rooted what you might call our "old brain," i.e., the brain functions we share with our reptilian ancestors and which work to ensure our personal survival.

Over millennia we have evolved a "new brain" that coexists with the old one. We can thank this _neocortex_ for our reasoning, reflection and compassion.

The new brain is what causes us to search for meaning rather than simply creature comforts. It's what inspires our interest in art, religion and fellow humans.

The neocortex, as well as the brain system called the _limbic system_, is linked to positive emotions, such as joy and maternal affection. As a result, our old and new brains are in constant conflict.

With a firm understanding of compassion, the following blinks will outline the 12 steps that you can start using today to bring compassion into your life.

> _"Compassion can be defined...as an attitude of principled, consistent altruism."_

### 3. There are 12 steps to a compassionate life – the first is to learn. 

When we look at all the violence and selfishness in the world today, it seems like we are a million miles from compassion. How can we even begin to change things for the better? To cultivate a more compassionate life?

This is no small task. To simplify things, the author compiled her research on compassion into an easily followable program — one that seeks to make compassion take the place of egotism, prejudices, violence and self-righteousness.

Her program includes 12 steps, each of which embodies a new discipline that builds upon the one before it.

Like the 12 steps in Alcoholics Anonymous, the disciplines you learn at each step must become an integral part of your life in order for the program to work. These steps aren't to be rushed through. Rather, you must practice them until the discipline has fully become part of your daily routine.

You should therefore expect that your transformation will be slow and incremental, not something that happens at the snap of your fingers.

The first step on your path is to simply learn about compassion by reading about the myths found in traditions such as Buddhism, Confucianism and Greek philosophical rationalism. A good way to do this is to form reading and discussion groups where you can help each other expand your knowledge about the different traditions of compassion. Keep a notebook for the passages and excerpts that you find particularly inspiring or meaningful.

You should immerse yourself in these traditional teachings because they demonstrate universality and the practicalities of the compassionate ethos.

Great sages, such as Buddha, Confucius and Socrates, understood that it's possible to refocus the mind and create distance between your reflective, compassionate self and your selfish, potentially destructive instincts.

These insights will be integral to your transformation into a compassionate person, and are therefore at the very top of the list.

### 4. Take a compassionate look at your own world, and practice compassion toward yourself. 

As important it is to look to the past for wisdom about compassion, we must also take a compassionate look at our immediate environment.

The second step to a compassionate life is to view our own community with compassion in mind, assessing its strengths, weaknesses and potential for positive change.

The American mythologist Joseph Campbell has shown that every culture has its own myth of a hero who transforms the life of his people through his own immense sacrifices. To do this, however, he must first take a look at his own society in order to see what's missing or what threatens his community.

In India, for instance, that hero is Gandhi, who chose to leave his city's elites to travel the country and observe the troubles of his people before deciding on how best to help them.

This compassionate outlook on society should also be narrowed down to your own world, including your family, workplace and nation.

Ask yourself: What makes a family's culture compassionate? What are the criteria for a compassionate business? And what compassionate values does — or could — your nation practice? By considering these questions, you can hone in on what your contributions to a more compassionate society could be.

The third step sharpens its focus even further. It asks you to practice compassion toward _yourself_.

This step is essential, because your compassion for others depends upon your compassion for yourself.

In our capitalist Western societies in particular, we are inclined to focus on our own shortcomings, always looking for ways to improve. But treating ourselves harshly increases the risk that we'll treat others by those same unfair standards — hardly an exercise in compassion!

While it's important to take responsibility for your misdeeds, you should nonetheless recognize your positive qualities, talents and achievements.

### 5. Put yourself in others’ shoes and be mindful of the present moment. 

The ancient Greek philosopher Aristotle believed that tragedy puts people's emotions and troubles into perspective, and in so doing teaches people to feel compassion for the unfortunate.

The fourth step follows a similar vein. It asks us to open our hearts to the suffering of others as though it were our own.

All major religious traditions consider suffering to be an integral part of the path to enlightenment. From Buddhism's central tenet that "life is suffering" to Christianity's representations of crucifixion, the world's religions show us that our empathy — meaning our willingness to feel the suffering of others — is a necessary step toward a compassionate life.

The Ancient Greeks brought these symbols into the public with their dramas, which aimed to help the audience understand that they weren't alone in their pain — everyone else suffers too.

The famous tragedy of Oedipus, for example, evokes our empathy despite the fact that he kills his father and marries his mother. The chorus guides the audience through their reactions to his tragedy, first shrinking away in horror and then reaching out and calling him "dear one" once they understand and are able to relate to the depth of his grief.

Just as the Greek audience learned to identify and suffer with the tragic hero, so we should try to recall our own past difficulties when we encounter someone who is suffering. By doing so, we can translate this empathy into a helping hand or a friendly word.

The fifth step likewise focuses on our perspective, and asks us to practice _mindfulness_.

Mindfulness is a meditation technique designed to help us live more fully in the present and foster a greater awareness of the fleeting nature of our emotions.

Whenever you find yourself latching onto an negative or unhelpful thought or feeling, e.g., greed, envy or anger, simply recognize that they are there and let them pass. This way, you direct your attention to more liberating and compassionate thoughts.

> _"Tragic drama reminds us of the role that art can play in expanding our sympathies."_

### 6. Make time for small acts of kindness, and realize your own ignorance about other people. 

Has anyone ever done something nice for you that made you feel cherished even after many years had passed? In all likelihood, that little act of kindness has been completely forgotten by the person who reached out. But while it might be forgotten to them, it's still deeply meaningful to you.

The sixth step to a compassionate life involves making space for these small acts of kindness. They don't need to be dramatic gestures. They can be tiny acts that may even seem insignificant to you.

They can be things like giving an elderly relative a phone call or taking the time to listen to a colleague vent about their frustrations. These small acts, though insignificant to you, can have a lasting positive impact on other people's lives.

Another way of increasing compassion in your interactions with others is through the seventh step: realizing how little you know about other people.

All too often we impose our own beliefs onto others, which tends to result in dismissive and inaccurate snap judgments. These beliefs are informed by our experiences — but our experiences aren't the definitive standard for decision making.

Instead, we should exercise open-mindedness, thus approaching people as if we knew nothing about their thoughts and actions, rather than making assumptions about them from our own experiences.

The Greek philosopher Socrates believed that wisdom isn't about reaching conclusions, but rather about knowing that we, in fact, know nothing at all. It is this realization that can help us be more aware of the mystery of all those we encounter, and allow ourselves to step into another perspective.

### 7. Be kind in your conversations, and be concerned for everyone’s well-being – not just your tribe’s. 

Human beings are social creatures who share their ideas with one another through dialogue. Unfortunately, however, these conversations aren't always about sharing.

You have only to look at today's political advertising, often little more than mud-slinging polemics, or the aggressive and competitive discussions in today's boardrooms to see that we also use language to undermine and demean our competitors. This obviously doesn't lead to compassion.

In contrast, the eighth step to a compassionate life is about developing friendly discourse. But it's not easy.

Our conversations can't offer us any insights unless each participant listens intently and sympathetically to their conversation partners' ideas. We must also ensure we don't make our conversation partners uncomfortable or insecure.

In developing this friendly discourse it's up to us to enter into conversations only if we're ready to _really_ listen and to potentially change our minds if what the other person says differs from what we believe.

This kind of development starts with your in-group, i.e., the people you are close to and who are like you. The ninth step is about transcending this group and being concerned for everybody.

_Tribalism_, i.e., demonstrating strong loyalty to one's own group, once enabled the human race to survive in harsh circumstances, as people found safety and protection within their tribe. But in our globalized and modernized society, there is no more need for tribalism. National, ethnic or tribal chauvinism, where one group feels it is superior to others, is essentially what leads to terrorism and war.

We must look back to the other steps, recognizing our limited view point and the legitimacy of outsiders' perspectives. We must remind ourselves that we are all one people, and that everyone deserves our compassion and understanding.

### 8. Expand your knowledge and find your own personal mission. 

The seventh step showed us the limitations of our perspective, and we must now put this new-found humbleness to good use.

For the tenth step we must make a concerted effort to fill the gaps in our knowledge. In practice, this means seeking out media that you suspect will challenge your point of view in order to develop a healthy skepticism toward what the Buddha called "hearsay."

For example, if you believe that Islam is an inherently violent faith because everyone around you does, then make it your duty to study Islamic history from a variety of sources in order to learn as much about it as possible. In all likelihood, you would realize that Islam, like most other religions, puts compassion at its core.

The idea is that in investigating the complexities of other traditions and ideas you will expand the scope of your compassion to include parts of the world that are foreign to you.

Once we have this knowledge, how can use it to make a difference in the lives of others?

According to the eleventh step on the way to a compassionate life, we must use our knowledge and compassion to discover our personal mission.

Consider Christina Noble, an Irish woman who had grown up in poverty on the streets of Dublin. Her life eventually changed for the better when she met a partner with whom she started a successful catering company.

But it wasn't until she took a trip to Vietnam where a poor local girl tried to hold her hand that she knew what her life's mission was: to advocate for the homeless children of Vietnam.

Like Noble, we all need to seek out that moment of recognition when we see our own pain in that of someone else, and realize how we can make a difference in the world.

> _"There is a need that you — and only you — can fulfill."_

### 9. The twelfth step to a compassionate life is to love your enemies. 

Though tribalism has long been essential to human survival, it no longer makes sense in our highly connected global society. History has shown that if we destroy those we perceive to be threatening our violence will eventually come back to haunt us.

To acclimate to this new way of interacting, the twelfth and final step toward a compassionate life asks that we develop a sense of compassion for our enemies.

To accomplish this, you'll need to use some of the tools from the previous steps: realize how little you actually know about them, and fill in the knowledge gaps with a thorough and unbiased investigation of their histories.

Your enemy could be anything. It might be a person who threatens you, your livelihood or what you stand for; it could be a state with which your home country wages war; or it could even be a religion.

Regardless of what or whom you see as your enemy, you'll need to examine them until you've reached an understanding of their context and condition — enough to allow you to look at their situation with empathy.

This requires you to constantly question the motivations of their actions in order to identify the suffering that underlies your enemy's story. It also means asking whether _you_ share responsibility for your enemy's pain.

The kind of compassion we're looking for is the kind that brings together Israelis and Palestinians, or Indians and Pakistanis. We want to emulate the efforts of those who, for example, have lost children in conflict and now campaign together with their "enemies" for peace between their countries, because their shared suffering has created a bond that transcends political divisions.

The most important principle in this final step is your sincere effort to listen to your enemy's story. Let them tell it without interruption, denunciation or doubt.

Try to really understand and share his suffering until you have transformed your animosity into compassion.

### 10. Final summary 

The key message in this book:

**Compassion is an intrinsic trait that we all share, but it doesn't become part of your life without a little work. But by following this step-by-step system, you will learn how to imbue your life and outlook with a healthy dose of compassion, and help transform the world into a better place.**

Actionable advice:

**Ask why.**

The next time you find yourself reddening with anger because of something someone did or said, take a moment to ask yourself: "Why is this person behaving in this way?" By attempting to understand other people's motivations, you can gain insight into their lives and their personal suffering, and hopefully even feel compassion toward them, instead of simply fighting fire with fire.

**Suggested** **further** **reading:** ** _The Art of Happiness_** **by Dalai Lama**

_The Art of Happiness_ is based on interviews of His Holiness the Dalai Lama conducted by the psychiatrist Howard C. Cutler. The combination of Tibetan Buddhist spiritual tradition with Dr. Cutler's knowledge of Western therapeutic methods and scientific studies makes this a very accessible guide to everyday happiness. The book spent 97 weeks on the _New York Times_ bestseller list.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Karen Armstrong

Karen Armstrong is a British author and former nun who was awarded the $100,000 TED prize in 2008. In addition, she created the Charter for Compassion, which has been signed by over 100,000 people worldwide. Her other books include _The Case for God_ and _The Great Transformation_.

