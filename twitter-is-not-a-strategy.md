---
id: 548627a36331620009f50000
slug: twitter-is-not-a-strategy-en
published_date: 2014-12-12T00:00:00.000+00:00
author: Tom Doctoroff
title: Twitter Is Not A Strategy
subtitle: Rediscovering the Art of Brand Marketing
main_color: 31C8F4
text_color: 228AA8
---

# Twitter Is Not A Strategy

_Rediscovering the Art of Brand Marketing_

**Tom Doctoroff**

_Twitter Is Not A Strategy_ cuts through the mess of hashtags and handles to get to the meat of effective marketing, outlining the core principles that make up the foundation of a successful company brand. Although social media is all the rage, this book shows that traditional marketing still matters, and that the secret to success is — as it always has been — having a good brand idea.

---
### 1. What’s in it for me? Find out how to turn your company brand into solid profits. 

With all the books about successful branding on the market, it's refreshing to read a new and straightforward approach.

That's where _Twitter Is Not A Strategy_ comes in. This book breaks down the art of branding not only as a creative pursuit but also as a structural approach to a successful business.

Branding isn't just about "likes" and logos, but about having a clear idea of what your business offers and knowing how to deliver that idea effectively and engagingly to customers.

After reading these blinks, you'll discover:

  * how ancient Egypt played a role in the origin of the word "brand";

  * why marketing in newspapers and television is far from dead; and

  * how talking to the "me" and the "we" is the best way to get customers to buy.

### 2. A product needs a strong brand to inspire customer loyalty and encourage repeat purchases. 

What do you think of when you see Nike's swoosh? You might associate it with joy, or coolness, or perhaps freedom of motion.

These associations are the foundation of Nike's _brand image_ — the key to Nike's corporate success.

But what is a brand, exactly?

The idea of a "brand" comes from ancient Egypt, where the skin of cattle was burned — or branded — with a particular mark to show to whom the cattle belonged.

Corporate branding, of course, works differently. A company uses a brand to make its products stand out from competing products, with the goal of creating a dedicated base of repeat customers.

For example, Procter & Gamble in the late 1800s used mostly generic names and packaging. But with the invention of a new soap executives thought exceptional, they decided to give it a special brand.

So instead of calling it a "white soap bar" which was usual, the company christened the product "Ivory," to suggest its mild, long-lasting qualities. And thanks to the company's successful branding campaign, annual Ivory sales exceeded $3 million!

But _why_ is branding such an effective tool? In essence, a brand can inspire customer loyalty.

Apple built a strong brand centered around exceptionally well-designed computer hardware. Even though competitor Samsung might offer better value for the money, many people buy Apple's more-expensive iPhones as they believe in the company's brand.

Branding too can be a life raft amid a public relations disaster. In the early 1980s, bottles of the painkiller Tylenol from Johnson & Johnson were tampered with and laced with cyanide, a deadly poison. Seven people died; a country-wide recall pulled product off the shelves and the company's market share plummeted.

Yet within a year, Johnson & Johnson had recovered nearly all of its losses, thanks to its hands-on, transparent crisis management at the time. The company communicated the situation to customers in a way that aligned their brand image with values like "tender" and "caring."

> _"Our market is the world." — Henry J. Heinz_

### 3. Top-down, or bottom-up: There are two effective ways to communicate your brand idea. 

To have an effective brand, you need to find a way to communicate it to your customers.

Traditionally, companies spread their brands through a _top-down communication model_ — in which a brand message is created by the manufacturer and then broadcast to a mass audience.

This model relies heavily on newspapers and television to reach the most people possible. To understand the power of mass media in the twentieth century, consider that as early as 1965, some 90 percent of American households owned a television set!

Even though the internet has changed the media landscape, top-down communication can still be powerful. Analysts predict that the global TV advertising revenues will surpass $200 billion by 2017, partly because television advertising is growing in emerging markets, such as in Indonesia and Kenya.

Although top-down communication is still important, technology _has_ shifted the balance of power between brands and customers. Today, brands also need to use _bottom-up communication models_, which encourage direct engagement with the customer.

Today's consumers don't want to passively receive information; they want advertising that is as engaging as any other form of entertainment. Additionally, consumers want to have more say in the creation of a brand than ever before.

As a result, companies now have to relinquish some control and allow brand campaigns to be co-created by customers, from the bottom up.

When a company advertises on YouTube, for example, a consumer may repost the video clip to Facebook. Then users on both social networks might critically respond to the campaign. On the other hand, users might also respond positively, sharing the campaign within their own network, resulting in valuable brand exposure.

Although there's an element of risk associated with the bottom-up approach, it can also be a highly effective way of building customer loyalty.

That's why today, the success of a brand is measured by how many people it engages through retweets, shares and likes. 

But regardless of whether your brand bubbles up or trickles down, you still need a coherent and compelling _brand idea_. How do you get one? Read on to find out.

### 4. Figure out what consumers want, and then build your brand around those desires. 

Rolling out a new app won't yield your company much success if you haven't first answered the age-old question, "What do consumers want?"

_Consumer insight_, or figuring out what motivates your customers to buy, is extremely important. To do so, you need to ask "why" questions.

For example, why do certain people love rebellious campaigns, such as Diesel's "Be Stupid" concept? And conversely, why do other people loathe it?

Such insights tap into the conflicts consumers face when making purchases. Certain tensions naturally arise between our wants and our needs, and building your brand around these tensions can be extremely effective.

The Diet Coke brand, for example, resolves a conflict for consumers who want to indulge without putting on weight.

Although this sort of consumer insight works broadly, others are more culture-dependent.

For example, the idea of individualism differs significantly across cultures. According to social psychologist Dr. Geert Hofstede, in a test judging how individualistic a person is, Americans score 91 (out of 100) while Chinese score only 20.

This means that a branding campaign focused on individual advancement — such as Nike's _Just Do It_ campaign — might fail in a country where individualism is frowned upon.

Using these kinds of cultural insights will only add to your _unique brand offering (UBO)_ and help establish your _brand idea_.

UBO refers to the key element which sets your brand apart from competitors; we'll talk more about it in the next blink.

Together, UBO and consumer insights unite to form the _brand idea_, or the long-term relationship between a consumer and a brand. Although the brand idea should be consistent, it can evolve.

For example, Nike's brand idea is _Just Do It_. Although the company has consistently maintained this idea, in Asia, Nike shifted its message from being one about personal advancement to one about self-possession, where through sports, one can achieve anything.

> _"The best insights spring from conflicts, or tensions, of the heart."_

### 5. For a strong brand, be sure to clearly identify how your product addresses a customer’s needs. 

A _unique brand offering_ (UBO) is the distinctive way your product meets a specific consumer need.

So how can you go about finding your product's UBO?

Start by looking at your _product truth_, or the physical and functional characteristics of your product. It's important to realize that this doesn't describe any aspect of your product that can be measured, tasted or touched.

For a toothpaste brand, for example, the product truth addresses the toothpaste's chemical makeup (like fluoride), _not_ its minty taste.

However, since product truth plays a major role in determining your UBO, it should articulate something about the item beyond its physical makeup. This is where you might want to leverage your consumer insight.

For example, a toothpaste with a "strong teeth formula" has mass-market appeal, but it won't entice luxury customers. On the other hand, "germ-killing, fresh-breath action" appeals to middle-class consumers who might want something special.

The second component of UBO is the _brand truth._ This is the tangible or intangible assets that become associated with a product over time, through constant marketing.

A brand's _tangible assets_ are elements like packaging, color and symbols. For example, Coca-Cola's iconic contour bottle hasn't changed since 1916, and it is still one of the brand's defining tangible assets.

_Intangible assets_ are slightly harder to pinpoint. These elements are mostly stable and relate to how we perceive brands. Volvo's intangible asset is safety; so much so that one of the company's goals is to try to ensure that by 2020, no serious injuries or deaths will occur in a new Volvo.

It's nice to have both a product truth and a brand truth, but the most important thing is to have a clear conception of just one truth, as that will allow you to develop a strong brand idea.

The next step? How to bring your brand to market.

### 6. To get customers to engage with your brand, speak to the “me,” the “we” and the “world.” 

So once you've put together all the components to create your brand idea, how can you communicate your concept to consumers?

To do this, you need an _engagement idea_, or a way of getting customers to actively interact with your product. You don't just want people passively watching commercials; instead you want them to "like" your Facebook posts and talk about your brand with friends.

It's important to note that although your brand idea may be concrete, your engagement idea should be flexible, to account for different beliefs, cultural norms, styles and so on.

For example, Nike's brand idea ("unbounded freedom without limitation") has long been epitomized by its tagline, _Just Do It_. In Hong Kong, Nike communicated this concept with the engagement idea, _Life is a Race_, placing racing track lines in subway stations all across the region.

So, once you've figured out how to engage customers, go further and motivate them to incorporate your brand into their lives. There are three key ways to do this.

First, relate to the customer individually; that is, speak to the "me." Axe, for example, created an alarm app with personalized videos of their models telling the customer to use Axe deodorant. In Japan, this approach fostered a 27 percent jump in repeat purchases!

Another way to motivate consumers is by relating to common interests, or the "we." To this end, Canon unveiled "EOS photochains," technology that lets users upload photos and merge them with other users' photos. This campaign helped to push Canon's market share to a record 67 percent!

The third way to motivate consumers is to speak to the idea of "the world."

How does a brand do this? Sporting goods company Adidas, for example, used nanotechnology to weave the names of 100,000 fans on a special kind of thread, that was then used to stitch the national symbol of New Zealand on the captain's jersey of the national rugby team.

> _"How many times have we heard, 'let's create an app,' without first asking why?"_

### 7. Leverage your brand idea by engaging with customers at every stage of the buying process. 

So how do you take a brand idea and translate it into sales?

Think of your customers' purchasing decisions as an _engagement system_. Here's how it breaks down:

First, there's the _trigger_. This is when a customer realizes they have an unmet need, to buy a camera. As a marketer, you can't make needs, but you _can_ suggest products to satisfy needs.

Second is the c _onsideration_ phase _._ A customer reviews product categories that could meet his need, such as, "Should I get a camera, or a smartphone instead?"

Third, the period of _comparison._ Here, a customer reviews information available within a certain product category, perhaps asking, "Does an iPhone or a Sony Xperia have a better camera?"

Fourth, the customer expresses a _preference._ He'll start to differentiate between brands, and develop a liking for one or the other. "I'll take the Sony, I like its multimedia compatibility."

And fifth, a _purchase_ is made. Interestingly, the purchase isn't always contingent on the customer's preference, as there are many factors in play. Maybe the Xperias are out of stock when the customer visits the store; or maybe the Apple salesperson was particularly attractive.

Finally, the _experience_ is evaluated. Was the sale personalized? Was there adequate tech support? These details matter. A customer who gets a free case with their smartphone is more likely to buy the same brand again.

You need to find ways to speak to your customer at every stage! Yet make sure to tweak your approach, depending on whether you're dealing with a high-involvement or low-involvement product.

_High-involvement_ _products_ are luxury items or costly goods. In this category, decision making is extremely detailed, as consumers will deliberate through each phase of the cycle, often going back-and-forth between stages.

_Low-involvement products_ are basic, inexpensive commodities, like toilet paper and soap. In this category, customers move quickly through the stages, skipping some altogether.

But whichever product category, engaging with customers at every stage is _critical_.

### 8. Final summary 

The key message:

**Even in today's digital age, so-called traditional advertising is as relevant as ever, as the key to success for any company is still about having a strong brand, one that establishes a lasting relationship with consumers.**

Actionable advice:

**Market your brand locally before you think globally.**

Make sure your brand idea is secure before you make a move to expand to other cultures, that bring a whole different range of consumer insights and potential tensions.

**Suggested further reading:** ** _Social Media is Bullshit_** **by B.J. Mendelson**

_Social Media Is Bullshit_ puts a damper on the hype around social media by unveiling the economy behind it and addressing the commonly held misconceptions about the value of social networks for business. While social media can be a valuable asset for certain companies, it is _not_ the cure-all that marketers and social media gurus would have you believe!
---

### Tom Doctoroff

Tom Doctoroff is the chief executive office of _J. Walter Thompson_ in the Asia Pacific region. With over 20 years of marketing experience, Doctoroff has shaped some of the world's biggest brands, such as _Nestle_ and _Microsoft_.

