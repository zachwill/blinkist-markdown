---
id: 55b7e1993235390007460000
slug: two-awesome-hours-en
published_date: 2015-07-29T00:00:00.000+00:00
author: Josh Davis
title: Two Awesome Hours
subtitle: Science-Based Strategies to Harness Your Best Time and Get Your Most Important Work Done
main_color: 92C3DC
text_color: 228AA8
---

# Two Awesome Hours

_Science-Based Strategies to Harness Your Best Time and Get Your Most Important Work Done_

**Josh Davis**

Between open-plan offices, telecommuting jobs and freelancing in coffee shops, the modern workplace is a minefield of distractions. Time to reclaim your brain with a little lifehacking strategy! _Two Awesome Hours_ (2015) reveals unexpected and insightful steps to a more productive you, from managing your mental energy to letting your mind wander. 120 minutes is all you need to make a difference.

---
### 1. What’s in it for me? Set the framework for a daily dose of peak performance. 

Sometimes you have days when it feels like you have been super busy but haven't accomplished anything significant. On these days, it's as if all of your time is spent checking emails, attending meetings, responding to questions from colleagues, and other seemingly trivial tasks that don't really lead to progress. So what can you do to prevent these tasks from sucking the time and energy out of your days?

These blinks present five strategies that will help you make sure that you don't waste all of your time and mental energy on boring routines and futile decision making. They establish some simple conditions for your day, and will teach you how to be awesomely effective with your work. This way, you'll ensure you have at least two hours of peak productivity every day.

From these blinks you'll learn

  * how to make the best out of a sudden interruption;

  * why letting your mind wander can be good for your work; and

  * that eating fat can make you work better.

### 2. In order to become awesomely effective you have to consciously recognize your decision points. 

Over the course of each day, you engage in all manner of habitual tasks. Think about how many times in your life you've hopped out of bed, gotten dressed, scanned your emails and attended weekly meetings without a second thought.

In these cases, we often switch off and go into automatic to get through our daily routines. Rarely do we stop to consider whether our routines make sense. It may be that your daily tasks are causing you to waste a great deal of time and energy without even realizing.

Take Doug. As a consultant, he was required to write a monthly analysis report. But instead of completing this rather important task, Doug ended up tending his inbox instead, responding to a dozen emails in a trance-like state. Because sorting through his emails is part of his daily routine, Doug gave it preference over what should have been his real priority.

Like Doug, many of us let routines get in the way of real productivity. So how can we change? By recognizing our _decision points._

Decision points are those moments in times when a given task is completed or interrupted. At a decisions point, you have the opportunity to consciously choose what you do next.

Let's look at an example. Say a colleague comes to your desk and asks you to go to lunch with him. As a result, you're interrupted in the middle of drafting a report. You then recognize that you've got a decisions point on your hands. You could either decline his offer and continue your work, or take a break and get a bite to eat.

By consciously considering your options here, you'll be better able to make a decision that benefits you and your productivity the most. As you become more aware of these points in time between tasks and make fewer impulsive decisions, you'll be able to become more effective with your time.

### 3. Managing your mental energy will bring you one step closer to being awesomely effective. 

It's rare to have just one task to do at a time. In order to keep all of our responsibilities fulfilled, we're forced to juggle them. Should you sort through your emails? Or should you prepare for today's meeting? It's important to choose wisely, as you only have so much mental energy to use.

Unfortunately, the juggling act itself can reduce the amount of mental energy available to us by causing _mental fatigue._ This happens when we overwork our _executive functions_, which are the parts of our brain that help us manage, regulate and control.

If you switch off email alerts while you prepare for an upcoming meeting, you're saved the hassle of constantly having to re-focus on the preparation, and you still have the energy to deal with a worthwhile task.

Making several decisions, even those that seem unimportant or routine, can also deplete our mental energy more than we realize. Other activities commonly known to be mentally fatiguing are networking, switching back and forth between different tasks, project planning and scheduling.

Obviously it's not possible to avoid all the activities and tasks that might fatigue your mind, but if you remain aware of those that do, it will help you spend your mental energy on the tasks that matter most.

Start by recognizing the tasks that tire you out the most, and refrain from performing them right before you need to give your best effort. If finishing an important report is the task that matters most that day, then don't begin your day by responding to emails, like you usually do. Instead, start with the report immediately.

### 4. If you want to be awesomely effective, you have to stop fighting distractions. 

We've all experienced the frustration of wanting to get work done, but finding ourselves constantly distracted by incoming emails, calls and notifications. It's not easy to maintain focus, but there's a good reason for that.

Did you know that our brains are designed to get distracted? Consider our prehistoric ancestors: Do you think they would have been able to survive had they failed to shift focus from the berries they were picking to the saber-tooth that was creeping up behind them?

Nope! Our brain has adapted to become distracted and refocus when changes occur in our surroundings. Distractions are perfectly natural. With that in mind, we'll be better able to manage their negative impact on our work.

Workspaces have several _predictable_ sources of distraction — computers and smartphones being the biggest offenders. Even though we love and appreciate all that those devices can do for us, they also hinder our focus and productivity. That's why turning off their distracting features, such as notification sounds and pop-up windows, is a great place to start.

But what about _unpredictable_ distractions, like an ambulance that passes by your window? Well, not all distractions are bad. We sometimes simply have to let our minds wander in order to boost our focus.

Seems like a paradox, right? But there's truth to it. A 2012 study from the University of California revealed that when we deal with tasks such as creative problem solving, our performance level will be enhanced if we let our mind wander once in a while. So how can you let your mind go without letting it drift off altogether?

You can actively enable mind wandering by engaging in an unrelated and cognitively easy task — like tidying your desk or making lunch — after having focused on a difficult problem for a certain amount of time. You can also allow passive mind wandering, by letting yourself drift off, becoming aware that you're distracted and returning to the task that needs your attention. Both approaches offer your brain the break that it needs to solve problems effectively.

### 5. A healthy body is a productive body! 

We all know that exercise is vital for good physical health. But if that's not enough to motivate you, you might be interested in the positive effects it has on your mental performance, too.

These effects were revealed in a study where participants were presented with color words written in a different shade of ink (for instance, "yellow" written in green). The participants then had to say either the color of the word (green) or what the written word was (yellow), before or after doing physical exercise. The participants who solved this task _after_ the physical exercise gave correct answers _faster_, showing that physical exercise enhances the brain's ability to make decisions and solve problems.

Another study found that physical exercise sharpens your focus. Here, participants were required to aim at a target on a screen while ignoring distracting stimuli on either side of it. After doing physical exercise, participants demonstrated an enhanced ability to focus and to ignore distractions.

Therefore, it might be a good idea to reserve some time to work on your important projects just after you're back from the gym. But what if you have no time for exercise?

Luckily, there are also certain foods that will increase your level of effectiveness too. Consider, for instance, carbohydrates and fats: Research shows that immediately after eating carbs, you'll experience improvements in your ability to focus.

But it's short-lived, and other executive functions may decline after only one hour. Surprisingly, fats might be more helpful than carbs. One study showed that certain fats are likely to improve several executive functions — even three hours after eating. People who are dehydrated will experience more fatigue and difficulties with maintaining focus, so drinking water is also crucial if you want to lift your level of effectiveness.

What goes on inside your body isn't the only important factor for your productivity. What happens outside it in your immediate environment is crucial too! Find out more in the final blink.

### 6. Fine-tune the noise, light and objects around you to create a powerfully productive workspace. 

Are you one of those people who needs complete silence while working? Do you worry that music and background noise will sabotage your concentration? In fact, noise such as intermittent speech and music inhibits your ability to perform your best at work.

Intermittent speech refers to words or sentences with pauses in between — basically, office chatter. Several studies have shown that intermittent speech negatively affects your ability to stay focused, read and process text.

So, when you want to be effective with your work, closing the door to your office or reserving a conference room might be a good idea, so that you avoid that distracting chatter.

Also, certain types of light actually boost your productivity. Both blue light and bright white light positively affect your ability to stay focused, and they can help you combat mental fatigue.

A study in the United Kingdom exposed two nearly identical workspaces to different light: white and bluish white. The people in the workspace exposed to the bluish-white light reported improved concentration and work performance.

But if you aren't able to improve the light source in your workspace, there's still one more factor that you can control to make a world of difference: clutter. All those notes, unfiled papers and free samples fight for your attention, and thereby decrease your ability to focus on your most important work.

Being unable to move around is also detrimental to your productivity. If you've got room to get up and walk around every now and then, you'll be surprised at how much you can sharpen your focus, and return to your work in a more effective mind-set than before.

So, once you've finished these blinks, hop out of your chair and go for a stroll! When you return, you'll be ready to start implementing the strategies you've learned to give yourself two awesome hours of productive, effective work.

### 7. Final summary 

The key message in this book:

**You can harness at least two awesome hours of effectiveness every day by following five simple strategies: recognize your decision points; manage your mental energy; stop fighting distractions; leverage your mind–body connection; and make your workspace work for you. This way, you'll provide yourself with the psychological and biological conditions you need for top performance.**

Actionable advice:

**Work it out!**

Next time you feel as if you're having a brain meltdown, and your focus has left the building, step away from your desk. Exercise is what you need. And remember: the gym is not the only place where you can exercise. Be creative! Simply going for a walk in the area in which your office is located, or running up and down the stairs for a bit, will help you feel more mentally agile, focused and — last but not least — ready to take on the most important work of the day.

**Suggested further reading:** ** _The Power of Less_** **by Leo Babauta**

_The Power of Less_ introduces Leo Babauta's ideal of _productive minimalism_. His approach focuses mainly on the development of good habits as the key to long-term changes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Josh Davis

Josh Davis is the lead professor and director of research at the NeuroLeadership Institute. He received his doctorate of psychology from Columbia University, and engages with research areas such as embodied cognition and the regulation of emotions.

