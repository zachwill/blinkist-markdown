---
id: 5a097d0eb238e100065bcca6
slug: two-birds-in-a-tree-en
published_date: 2017-11-15T00:00:00.000+00:00
author: Ram Nidumolu
title: Two Birds in a Tree
subtitle: Timeless Indian Wisdom for Business Leaders
main_color: 813221
text_color: 813221
---

# Two Birds in a Tree

_Timeless Indian Wisdom for Business Leaders_

**Ram Nidumolu**

_Two Birds in a Tree_ (2013) is a guide to holistic business practices, inspired by the ancient Indian text, the Upanishads. These blinks will teach you how to build a business that is good to its employees, customers and the environment while posting record numbers and garnering huge success.

---
### 1. What’s in it for me? A more holistic approach to business. 

With climate change, rising inequality and aggressive takeovers a plenty, there is little or no doubt that modern business is a winner takes all, short term orientated industry. For the business leader, nothing matters apart from quick profits and happy shareholders, not the destruction of the environment, nor happy staff.

Of course this cannot continue. Luckily, there is a better way of doing business. A path first highlighted millennia ago in what is now India. These blinks, show us how business leaders can use ancient thinking to think long term, and leave the world a better place than when they found it.

In these blinks, you'll discover

  * how to identify a worthy sage in business;

  * why should stop doing and having, and start being; and

  * find the REAL path to business enlightenment.

****

### 2. Better business means being connected to the world. 

What do you _do_ and how much do you _earn_? Simple, everyday questions, but ones that illustrate a crucial point: the vast majority of your worklife is based on _doing_ and _earning_. So, what would it be like if you focused more on just _being_ –the essential state that connects all living things?

Well, that question will take you back thousands of years to the _Upanishads._ This cache of Indian wisdom comes from the Vedas, ancient texts of an 800 BC religion that formed the basis of Hinduism.

They tell a tale about two birds in a tree:

One is in the lower branches and the other is at the tree's peak, looking out on the view. The lower bird represents the anxious ego, the one that jumps around doing things and trying to attain others, while being blind to the bigger picture. Meanwhile, the higher bird is perched serenely at the top, enjoying a full perspective of the world and its connections.

Unfortunately, business tends to act like the lower bird and it causes a lot of harm. That's because businesses often see themselves as separate from other areas of responsibility like those to humanity and nature. Instead they focus only on the ones that are explicitly in their domain, like stockholders and investors. Not just that, but it's this logic that has been foundational in economic and environmental crises, things like the 2008 financial meltdown and climate change.

Luckily there's a way to change that and it's called _being-centered business_. This approach is all about attending to the deep connection between a company and the world it operates in. All business leaders should adopt this approach and one that's done so in stride is The Body Shop.

For instance, Anita Roddick, the company's founder, became famous by defending the environment as well as indigenous Amazonian tribes. In fact, it was this deep connection to the planet and its people that helped her build a billion-dollar corporation.

> "We call ourselves living _beings_, we are neither living _doings_ nor living _havings_."

### 3. Being is a higher realm of existence that you can lead yourself to. 

The Upanishads tell another story about a boy who wants to understand _Ātman_, or the essence of the world and all beings. So, his father instructs him to place a piece of salt in a glass. The following day, the boy's father asks if he can see the salt. When the boy says he can't, the father tells him to sip the water and describe its taste. Naturally, the boy says that it's salty.

The point is that just like the salt invisibly permeates the entire glass, so does Ātman permeate all being. That means that since we're all beings, we're all part of Ātman. For instance, think back to the two birds: the one in the lower branches is focused on material objects and experience, but the bird on the top is different. It's our universal self, in other words, Ātman.

So, how can one embrace the higher bird?

Using the author's REAL road-map, a guide to becoming a Being-Centered leader. The map breaks down into four parts, each of which shows how a lower bird can reach for the top of the tree.

The first step is to _recognize_ that there's something higher than yourself, the higher bird.

Second, you must _experience_ connection to that higher bird.

Third is to _anchor_ yourself in a stable vision of the higher bird while letting go of the lower bird's fear.

And finally, you embrace _leadership_ by example for other birds.

In this process it's important to remember that the lower bird should be led by the higher one but both are necessary. That's because being without doing has no outcome but doing without being has no higher purpose.

### 4. Business is a part of, and dependent on, a larger system. 

Did you know that the english word _right_ stems from the Vedic concept of _Rta?_ It's the idea about how worldly things fall into the right order. For instance, night follows day, summer comes after spring and a balance is maintained between gods and humans.

The manifestation of this order is called _Dharma_, or the balance within and between systems.

So, since business is a subsystem of other, greater systems, we all need a world in which we can conduct it. For instance, business is a subsystem of the economy, which is a subsystem of humanity, which is a subsystem of nature, which is a subsystem of Being. Each system follows another and is dependant on it.

Therefore, without nature there could be no humanity, without humanity no economy and no economy would mean no business. And, since these systems are interdependent it's essential that we keep them in mind when doing business. That's because each subsystem can be considered a form of capital.

For instance, there's the _material capital_ of goods and services, infrastructure and the stuff of which it's built. Then there's the _human_ and _social capital_ like human resources and the wealth developed through socializing. There's also the _natural capital_ of ecosystems which provide humans with necessities like food, air and water. And finally there's _being capital_, that is the foundational values of a business.

But the higher systems are prone to danger and it's essential for a Being-Centered leader to employ the _Dharma principle_. That's because if the capital of a system runs out, that system will run the risk of collapsing. So, if balance is not maintained within and between subsystems, the larger systems will be undermined.

For instance, going back to the birds in the tree, the tree can be seen as the larger systems. Therefore, the goal of a Being-Centered leader is to protect the tree's fruits and keep its roots firmly grounded, despite the desires of the lower birds.

> "When we destroy nature, we are destroying an integral part of ourselves."

### 5. Business and work can help you connect to your higher reality. 

So we, as the lower bird, have seen that there's a higher reality, a larger system that we're inside of. But what we've seen so far is just a brief glimpse. Then how can we see further?

By aspiring to a higher sense of self through the work we do. That's because work is the realm in which we develop skills and the aspects of our material selves that are important to us. This fact is abundantly clear in startups, work environments in which people give their work everything they've got, often investing more hours to work than sleep or any other activity, with the drive to change the world in whatever way.

It's this ambition that business leaders can seize on to deepen our connection to the higher reality and here's how:

First of all, our material insecurities deter business leadership, making it difficult to access the higher reality. Most commonly this occurs through excessive planning meant to hide insecurities. For instance, when the author's startup, Gandiva began facing problems, the author invested a lot of time in planning for different scenarios. But all this planning only laid bare the author's crippling fear of failure.

Luckily, he found another way to deal with his insecurities. When he felt insecure, he would remember the reasons he founded the company in the first place. In other words, he would get in touch with the sense of higher self that he'd set out to realize.

As a result he was able to move from his constantly planning, efficiency obsessed self to a higher one, in the process finding a deeper sense of purpose. So, however sentimental his notes to self from that time may seem to him now, they represent the author's path to connecting to his higher bird, his innermost motivation for opening the business.

### 6. Businesses built on the well-being of employees and society are more successful. 

Another story told in the Upanishads is that of Bhrigu, a boy who endeavors to understand reality's mystery. He meditates over what could be essential to humanity and comes to the realization that joy is foundational to our lives. Well, the same is true of business.

That's because joy means happy employees, and happy employees mean long term profitability. For instance, a business has lots of different stakeholders from customers to employees, but in the hectic capitalist world, plenty of companies devote all their energy and attention to the shareholders. The problem is that this strategy is unsustainable because it lacks dimensionality.

So, by focusing more on their customers and employees, business can increase long term success and profits. For example, Southwest Airlines is famous for creating a fun work environment. In fact, the CEO, Herb Kelleher takes a lead role in this endeavor by planning pranks, like hiding in overhead bins to surprise employees.

His actions have inspired employees to enjoy themselves at work with funny-in-flight announcements like "please do not leave children or spouses when you exit the plane". And Southwest isn't just having a good time, they're also the most profitable U.S. airline and have enjoyed 39 consecutive years of profitability in an industry where such streaks are rare.

So, happy employees are good business, but they aren't the only people businesses should look out for. In fact, caring for the well-being of all humanity is also a path to success, whether it's lending a hand to educational, social or environmental work.

For instance, the Brazilian cosmetic company, Natura Cosméticos consistently ranks among the top five sustainable companies in the world. They're deeply connected to the Amazonian tribes who live in the forests where they gather ingredients and are therefore committed to protecting this ecosystem. And that's not all, the company also dominates the Brazilian beauty product market, seeing global sales of over $3 billion in 2011!

### 7. Ethical business practices mean successful companies. 

Have you ever read an article on a major corporation's quarterly report? It's likely you have and that's because the businesses of today are obsessed with pleasing shareholders. In fact, it's common for successful business leaders to hold fast to this ideal.

For instance, a study of over 70,000 quarterly conference calls on the topic of earnings found that leaders were more focused on producing short term profits to attract investors interested in quarterly earnings. However, there's another route and it goes back to Being-centered leadership, which brings employees to the table as key stakeholders.

Take Costco, a company whose ethics place employees before investors, paying them 40 to 70 percent more than competing businesses, like Wal-Mart, pay their employees. The result is that turnover after the first year is significantly lower: 6 percent at Costco compared to Wal-Mart's 44 percent. That means Costco benefits from a more experienced and knowledgeable workforce.

And that's good for business. In fact, Costco's returns are double the Standard & Poor's index and 3.5 times higher than Wal-Mart's.

But don't forget the customer. They can be included as stakeholders as well. A good example is the _Tylenol Crisis_ of 1982. Here's what happened:

Bottles of the pain medication had been tampered with, the pills laced with lead. This resulted in the deaths of seven people in the Chicago area. James Burke, the CEO of Johnson & Johnson, Tylenol's manufacturer, jumped into action, pulling 32 million bottles of U.S. shelves at a cost of $100 million. He even appeared on _60 Minutes_ to explain how the company was improving customer safety, a strategy that included a three month project to enhance anti-tamper packaging.

The result?

People who saw the show were 5 times more likely to choose a Johnson & Johnson product than those who didn't. So, when Tylenol was reintroduced it almost immediately recouped its 35 percent market share.

### 8. Caring for the environment is essential to long-term business success. 

Having been inspired by the Upanishads, Mahatma Gandhi died in possession of only ten things, amongst them were an eating bowl, sandals and spectacles. His commitment was based on the intention to have as small a negative impact as possible; that the world is not here for us to take from, but to care for. Unfortunately, this sentiment is rarely shared by the businesses of today.

In fact, business as it's done today, doesn't account for the true costs of its actions. For instance, we overuse nature, but don't see the costs because the environment doesn't send an invoice. So, at present, the global economy receives $72 trillion in "free goods", like natural resources, per year and does annual damage that costs about $6.6 trillion. Not just that, but by 2050, the costs of that damage will likely have grown to $28 trillion!

But to break free from this approach and embrace stewardship, we'll need to train our sights on long term growth, a concept that we struggle to understand. That's because compound aggregation of wealth, wherein capital grows exponentially each year, is difficult to comprehend and it's even harder when trying to determine the effects something will have 100 years in the future.

So, leading as a steward is all about understanding that nature and society aren't free amenities for business to take, but resources that companies should care for. Take Eileen Fisher, founder of the eponymous fashion company and a superb steward. For instance, her company only employs organic natural fibers and makes durable clothes built to last, thereby reducing the impact on the environment. Additionally, they design many of their items for cold washing, which uses less energy. The company made 90 percent of its 2012 spring line in this way.

But the business is also good to its employees. They all begin every day with yoga and receive $1000 annually as a _wellness allowance_ to be spent on health.

> "Support yourself by renouncing ownership" - The Isha Upanishads

### 9. Leaders can inspire change by setting examples. 

Have you ever heard of a sage? It's essentially a wise person whose physical self, as represented by the lower bird, is perpetually connected to the higher bird. As a result, they can be extremely inspirational.

In fact, even some CEOs are sages. Just take Paul Polman, CEO of a gigantic consumer goods corporation called Unilever. For Polman, the 2008 financial crisis was a "crisis of ethics" that lead him to reevaluate the way Unilever operates. He decided to stop quarterly reporting and focus the company on long-term growth.

As a result, the shares of the company owned by short-term, profit hungry hedge funds dropped from 15 percent to 5 percent between 2009 and 2013. Not just that, but the company's _Sustainable Living Plan_ endeavors to double sales while cutting in half the corporation's environmental impacts and improving the nutritional value of its products.

That means the company includes a variety of stakeholders including the natural world. And it's profits?

Well, during Polman's tenure, Unilever's share price increased 55 percent, revenues grew by 25 percent and operating costs were even reduced. But beyond even that, in 2012, Unilever was also ranked the world's most sustainable company by Deloitte Innovation.

So, CEOs can be sages, but so can investors. Just take _The sage from Omaha_, infamous U.S. investor Warren Buffet. His investment firm grew by over 587,000 percent between 1965 and 2012. That means if you invested $200 with Buffet in 1965, you'd be a millionaire by 2012. Compare that to the $15,000 your investment would have returned if invested in the S&P 500.

You're probably not surprised to hear that Buffet's secret is a focus on long-term growth. But he's also a profound humanist who has committed to donating his entire fortune upon his passing and pushed other billionaires to give away at least half of theirs. And, when Buffet makes the move to give environmental issues a central role in his investment philosophy it will have incorporated all perspectives.

### 10. Final Summary 

The key message in this book:

**There's a different way to do business, a departure from the status quo that supports instead of harming nature and humanity. It's called** ** _Being-Centered_** **leadership and it works by linking the goals of a company to the interests of the planet in which it operates.**

Actionable advice:

**Use rituals to connect to the higher reality.**

For ages, society has been connected by rituals conducted surrounding birth, marriage, death and countless other life events and rituals can likewise help business leaders connect to their higher reality of existence. It's as simple as accessing the greater meaning embodied in rituals and using it to build a more inclusive identity and meaning for your business.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Peak_** **by Chip Conley**

_Peak_ (2007) reveals how hotelier Chip Conley became inspired by a book on psychology and applied it to his own successful business philosophy. These blinks show us how we can find lasting success and happiness by shifting away from fixating on profit and instead focusing on creating positive relationships with customers and investors.
---

### Ram Nidumolu

Dr. Ram Nidumolu, CEO of InnovaStrat has been a strategy and leadership consultant for 20 years. He has published writing on leadership in the Harvard Business Review and the Stanford Social Innovation Review.

