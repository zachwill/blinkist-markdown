---
id: 56f3bd352bf5b90007000071
slug: two-hours-en
published_date: 2016-03-30T00:00:00.000+00:00
author: Ed Caesar
title: Two Hours
subtitle: The Quest to Run the Impossible Marathon
main_color: E24646
text_color: E24646
---

# Two Hours

_The Quest to Run the Impossible Marathon_

**Ed Caesar**

_Two Hours_ (2015) is about the world of international professional running. It offers background information on the sport and explains why so many great marathon runners come from Kenya, with a focus on Geoffrey Mutai, an athlete known for a string of record-breaking wins and his innovative running technique.

---
### 1. What’s in it for me? Become a marathon expert. 

Have you ever run a marathon? If not, have you ever dreamt of doing so? 

Marathon running is more popular than ever. But what do you really know about the sport other than the fact that it'll make you sweat a lot?

Who first thought of running a marathon? How did the race develop over the years? And how did the Kenyan runner Geoffrey Mutai become so good at it? 

In these blinks you'll find the answers to such questions — and more. They'll provide you with both great insights into the world of marathon running and the personal story of Geoffrey Mutai.

In these blinks, you'll learn

  * how it all began in ancient Greece;

  * the secret to Geoffrey Mutai's success; and

  * how science got involved with the marathon.

### 2. Marathon running dates back to ancient Greece. 

If you think it's strenuous to ride your bike to work, be thankful you weren't born in ancient Greece!

Back then, you might have had to run long distances as a profession. According to the foundation myth of the modern day marathon, in 490 BC, an Athenian messenger named Pheidippides had to run to Sparta to ask for support against the Persian invasion at the settlement of Marathon.

It's said that Pheidippides had to cover 150 miles in two days. Then when the Greek army won the battle, he was sent on a 25-mile trip back to Athens to deliver the news. Legend has it that he said, "Joy to you, we have won," then collapsed and died from exhaustion.

It's unknown if Pheidippides's story is historically accurate, but it inspired the organizers of the first Olympic Games, held in Athens in 1896. They included the marathon race in the official program. It was a success but later fell from popularity. In both the 1900 Paris Olympics and the 1904 Saint Louis Olympics, some long-distance runners were found to have cheated by taking shortcuts. The sport's reputation suffered as a result.

Marathon running didn't become widespread until the mid-1970s, with the introduction of city marathons. 

In October 1976, the first city marathon was organized in New York City. It marked the beginning of professional running, as some athletes were paid for participating in the event. Unlike the Olympics, however, city marathons were open to all comers, which turned the sport into a community event. 

In 1976, Bill Rodgers made $3,000 by winning the New York marathon, and the sport has been popular ever since. In 2013, Mo Farah won over a million dollars for his performance in the London race.

### 3. Professional marathon running is based on breaking world records. 

A lot of people approach sports with a carefree attitude: "The most important thing is to have fun!" Professional marathon runners view their sport differently, however.

Modern professional marathon runners aim to set new world records. Geoffrey Mutai, for example, thought he set a new record when he finished the Boston Marathon with a time of 2:03:02, but his time wasn't recognized because the International Association of Athletics Federations don't count Boston as a race where a new record can be set.

Mutai's determination has only increased, however. He's now set on beating his own outstanding performance in another race where the record can be officially recorded.

This isn't easy. In a marathon, every second counts. Any small mistake can throw off an athlete's entire race. 

This happened to Mutai at the 2012 Berlin Marathon. His fellow runners helped him by running ahead of him and setting the pace. 

After the first five kilometers, the runners had a problem with the time display on the monitors they used to measure their speed. It froze on their initial pace of two minutes and 50 seconds per kilometer, a speed they couldn't maintain for the whole race.

The runners thought they were going too fast, so they slowed down. By the time they realized their monitors were broken, it was too late. They had already lost too many precious seconds moving at too slow a speed to enable Mutai to set a new world record.

### 4. Scientists conduct research to help marathon runners go even faster. 

Have you ever wished for some kind of scientific advancement that would make it easier to get out of bed in the morning? Athletes are looking for performance boosts like this too. 

Scientists are just as active in the quest to break long-distance running records. It was Mike Joyner, a scientist and former marathon runner, who first outlined the possibility of running a marathon in under two hours. 

Joyner measured the relationship between a runner's performance and various physiological criteria, such as their lactate threshold (the point at which lactic acid begins to increase exponentially in the bloodstream), and the maximum oxygen intake. 

He calculated that if all of those variables were at an ideal level, a person could physically complete a marathon in one hour, 57 minutes and 58 seconds under optimal conditions. 

Scientists have also found that an athlete's mind plays a big role in his performance. Kevin Thompson illustrated this when he conducted an experiment in which athletes raced on stationary bicycles against a computer-generated avatar.

The athletes were told that the avatar raced at their own best rate. That was a lie, however: the avatars actually raced 2 percent faster than the athletes ever had. Despite this, the athletes nearly always beat the avatar. They won because they believed they could if they just tried a bit harder.

Thompson concluded that the body has an excess energy reserve of 2 percent, but that we usually don't use it as that would mean complete exhaustion. That's why marathon runners aim to beat their own records only by a few seconds: they don't want to use up their last bit of energy.

### 5. Kenya seems to have the ideal conditions for training marathon runners. 

When was the last time you stopped to appreciate how important your feet are? Your feet allow you to move from one place to another! Few people give their feet the appreciation they deserve.

And if you grew up in Kenya, you might have even more reason to appreciate them.

Kenyan marathon runners are known for their high performance levels. According to numerous physiotherapists, it's partly because of the structure of their feet. 

A physiotherapist named Vincenzo Lacini first noticed this when he massaged marathon winner Martin Lel's feet and realized they were bigger and more muscular than normal, almost like a bodybuilder's torso. Lacini believes that some Kenyans develop such strong feet because they spend a lot of time running around barefoot at school when they're children.

Feet aren't the only reasons for the dominance of Kenyan long-distance runners, however. Nearly all Kenyan marathon champions come from the same tribe, the Kalenjin. They were all brought up in rural and relatively poor areas where they walked or ran to school and worked on farms while barefoot. 

What's more, the Kalenjin have a diet that consists mostly of a porridge dish called _ugali_, vegetables and the occasional piece of meat. That might well be the ideal diet for marathon runners. 

The Kalenjin also tend to live at a pretty high altitude. Studies have shown that spending a long period at such an altitude can increase a marathon runner's lung surface area by 50 percent. That allows them to take in more air with each breath, which gives them more stamina for running.

### 6. Geoffrey Mutai’s upbringing is typical of many Kenyan marathon runners’. 

Tennis and golf stars tend to come from privileged, stable backgrounds, but that's not true of professional marathon runners. 

Kenya's top competitors are usually from challenging backgrounds. In fact, in January 2008, violent conflict broke out in the Rift Valley Province, home to most dominant Kenyan runners. Many people felt that the re-election of President Mwai Kibaki had been rigged, triggering strife between the Kalenjin and Kikuyu peoples. 

Mutai was cycling through the town of Timboroa when he saw men from the Kikuyu tribe patrolling the street with machetes, looking for members of the Kalenjin tribe. Luckily he looked very young and could speak their dialect, so they let him go, but he later found out they killed another Kalenjin man that same day. 

Mutai was born into a poor peasant family. He grew up on a farm and ran barefoot to school every day. His family had little money and lived quite simply, but Mutai says his childhood was a happy time. He always had shelter, food and love. 

Things changed when his family moved to the city of Rongai, however. Mutai's father lost his factory job, drank heavily and soon started beating his son. Mutai often fled back to the farm where his grandparents still lived. 

Mutai struggled with alcohol himself during his teens but was eventually able to finish primary schooling at age 18. Then he went back to live with his grandparents, where he could focus on running. 

If it hadn't been for Mutai's grandparents, he might have fallen into alcoholism like his father — an unfortunately common fate for Kenyans from his demographic.

### 7. Performance-enhancing drugs are part of the professional running world. 

Do you sometimes wish you could fly? Certain drugs can give you that feeling. Other drugs can make you run faster or increase your stamina. 

Sadly, illegal performance-enhancing drugs may be another reason that so many Kenyan runners have reached world class. In Kenyan cities, there are a lot of corrupt doctors willing to sell illicit drugs to athletes to boost their own incomes. 

In fact, nearly every successful Kenyan runner has been accused of drug abuse. They're usually suspected of using _EPO_, a drug that encourages red blood cell growth. However, very few of these accusations have been proven to be true. It's difficult to know if runners are reporting each other because they're telling the truth or because they want to take out their competitors.

Some people argue that Kenyan runners can't benefit from using EPO because their red blood cell count is already so high that if it increased even more they could suffer a heart attack or stroke. Researchers at the University of Glasgow have disproved this, however, by comparing Scottish athletes and Kenyan athletes. 

The researchers found that EPO increased the performance of both groups of athletes. Kenyan athletes also benefited from the drug. 

The problem is further compounded by the fact that the Kenyan Athletics Organization doesn't do much to crack down on it. In 2013, Kenyan athlete Wilson Loyanay Erupe revealed the name of his doctor when he was caught with EPO in his blood. The doctor was put out of business, but the Kenyan Athletics Organization didn't publish his client list or investigate the matter further.

### 8. Geoffrey Mutai has set new standards and world records for marathon runners worldwide. 

A plane expends most of its energy during take off. Once it's started moving, the ascent is much easier.

Geoffrey Mutai's career started out the same way. After it took off in 2008, he started to become rich and famous very quickly.

After Mutai won his first race in Monaco in 2008, it was easy for him to enroll in other prominent races. Shortly after that, he soon won an even more important race, the Eindhoven Marathon in the Netherlands. 

These early wins brought Mutai a lot of attention and financial success, as he was soon being offered large sums of money to participate in future races. He earned enough to be able to buy a large piece of land back home. In fact, Mutai's win at the Boston Marathon in 2011 alone earned him $225,000 — enough to buy an estate mansion in Kenya. 

After his early string of victories, Mutai became even more determined and dedicated to his sport and developed a new running strategy. 

Marathon runners typically pace themselves, saving their energy for the last stretch, but that technique wasn't enough for Mutai. So he started running entire races as fast as he could, without trying to conserve energy. 

This strategy brought him unprecedented breakthroughs. In the 2011 Boston Marathon, Mutai set a new record of 2:03:02. A few months later in New York, he crossed the finish line more than a full minute before the second place runner and set a new record of 2:05:06.

### 9. Final summary 

The key message in this book:

**Though we don't know its exact origins, long-distance running has long been a cultural tradition. Modern professional running is focused on breaking world records and has been dominated in recent years by Kenyans, largely because of the country's unique social and environmental conditions. Geoffrey Mutai exemplifies this, as he was propelled into the international spotlight because of the skills he honed in his childhood and his high level of dedication.**

Actionable advice:

**Go barefoot!**

If you want to be a winning marathon runner, walk and run barefoot as often as you can. It builds the muscles in your feet!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sports Gene_** **by David Epstein**

_The Sports Gene_ takes a look at the physiological traits that are beneficial in various sports, and at their hereditary background. It also examines why people in certain parts of the world have evolved in their particular way, and how this is beneficial in the realm of certain sports.
---

### Ed Caesar

Ed Caesar is a nonfiction writer whose work has been featured in the _New York Times_ and the _Smithsonian_. _Two Hours_ is his first book.

