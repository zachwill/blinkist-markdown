---
id: 589c62f30394950004eff58a
slug: two-nations-indivisible-en
published_date: 2017-02-09T00:00:00.000+00:00
author: Shannon K. O’Neil
title: Two Nations Indivisible
subtitle: Mexico, the United States and the Road Ahead
main_color: 84383A
text_color: 84383A
---

# Two Nations Indivisible

_Mexico, the United States and the Road Ahead_

**Shannon K. O’Neil**

_Two Nations Indivisible_ (2013) tells the story of the United States' relationship with its neighbor to the south: Mexico. These blinks explain the profound connections between the two countries as well as the misunderstandings that keep them apart, with an emphasis on political and economic relations.

---
### 1. What’s in it for me? Understand the complex relationship between Mexico and the United States. 

During the 2016 US presidential primaries, a key plank of Republican candidate — and eventual winner — Donald Trump's campaign was the construction of an immense wall along the Mexican border, ostensibly to keep out undocumented migrants from the United States' southern neighbor.

The proposal spurred reactions ranging from vociferous support to fierce criticism, showing just how complex the United States' relationship with Mexico is. And south of the border, the relationship isn't straightforward by any means, either.

In these blinks, we will examine this dynamic relationship and how it has changed and evolved over the last decades. We will explore the possibilities and challenges Mexico is facing and how they can be tackled in concert with the United States.

In these blinks, you'll learn

  * how United States' news portrays a flawed image of the current situation in Mexico;

  * why free trade is a good start, but won't solve all of Mexico's problems; and

  * how immigration from Mexico into the United States has actually stalled since 2011.

### 2. US news coverage of Mexico focuses on drug-related crime while ignoring positive developments in the nation. 

In 1997, Amado Carillo, an infamous drug lord and head of a major drug cartel in Juárez, Mexico, died in hospital while undergoing cosmetic surgery to disguise his identity. You can easily imagine what a juicy news headline this made and, of course, the US media ate it up.

In fact, US news coverage of Mexico is primarily focused on drugs and crime. The city of Juárez is central to this narrative and the news of Carrillo's death is just one example of the American media's obsession with the city.

The focus on Juárez might be a result of the city's location right along the border with the United States, just south of Texas and New Mexico. But it's also a product of the city's ludicrously high rates of drug-related crime.

For instance, in 2009, there were over 2,500 violent, drug-related deaths in the city, the highest drug crime murder rate in all of Mexico. Just one year later, the number had risen to 3,000 deaths, making Juárez the most violent city on earth. Between 2007 and 2011, over 9,000 people were murdered by drug lords and dealers in the city.

The American media is especially skilled at depicting all the tragedy and violence of the Mexican drug wars, but it misses the fact that the country has come a long way toward prosperity. While drug violence is still an issue, it shouldn't overshadow the rapid progress Mexico is making in other areas.

For example, Juárez is growing quickly, with new buildings and factories from well-known multinational companies, like Siemens and Bosch, emerging all the time. The city's economic development has even been mentioned by _Foreign Direct Investment_ magazine, a journal of the Financial Times Group. According to the journal, Juárez is a city with future prospects and a per capita income that's above average for the country.

### 3. The United States and Mexico have tried to develop their relationship, but diplomatic relations remain strained. 

Nowadays, lots of Mexicans live in the United States and many Americans, especially retired ones, move to Mexico to enjoy the warmer climate. This exchange makes it all the more essential for the two countries' governments to find common ground. To their credit, the United States and Mexico have been working to improve their diplomatic ties for years.

For example, in 2009, Carlos Pascual was named the US Ambassador to Mexico. Determined to strengthen cooperation between the two nations, he and President Barack Obama traveled to the Mexican city of Guadalajara for the annual North American Leaders Summit.

From there, Pascual went on to Mexico City and then to Washington, where he worked tirelessly to build consensus between the two governments. One of Pascual's primary ambitions was to foster support for a US-Mexico security strategy that, instead of merely pursuing drug lords, would strengthen Mexico's judicial institutions and address social changes in the regions most affected by drug wars.

His strategy was ambitious and looked very promising, but, unfortunately, the countries just weren't ready to cooperate. As a result, diplomatic relations between Mexico and the United States remained turbulent, as major issues surrounding the strategy's implementation quickly came to light.

The Mexican governors and ministers didn't want to do things the American way and instead stuck with their old methods of military and police raids. Then, in 2010, WikiLeaks released confidential messages sent by Ambassador Pascual and his team, analyzing the Mexican government's security apparatus. The analysis was largely critical, pointing to infighting between various authorities, as well as widespread corruption and human rights violations.

Naturally, Mexican President Felipe Calderon was furious about the reports and criticized the American government as poorly coordinated. In the end, Ambassador Pascual, who had done a tremendous amount to improve diplomatic relations between the two countries, was forced to resign in March of 2011.

### 4. Since the 1980s immigration to the USA from Mexico has increased. 

Mexico's oil industry has long been one of the country's primary sources of wealth, which meant serious trouble in the 1980s when oil prices suddenly tanked. This market crash had far-reaching effects and produced a wave of immigration, with Mexican citizens flooding into the United States.

It all started in 1982 when Mexico's currency, the peso, was devalued by 40 percent over the course of just a few months. As a result, Mexico couldn't afford to pay interest on its foreign debt, which at the time amounted to $80 billion; in turn, the peso lost even more of its value.

The Mexican economy was plummeting, as was the number of jobs available in the country. In a desperate attempt to flee poverty and unemployment, huge numbers of Mexican immigrants entered the United States every year. The number of border crossings doubled once in the 1980s and again in the 90s.

But despite this long history of Mexican immigration onto their soil, the United States still struggles to accept and acknowledge Mexican immigrants. Just take a Pew Research Center Hispanic Study that came out in March of 2005, which found that there were over 6.5 million Mexicans living in the United States in that year without any type of legal recognition.

This is a huge problem, since, for example, more than 100,000 children of illegal Mexican immigrants have earned diplomas from US universities, but are then unable to work since they don't have papers. To tackle this problem, in 2012, the Obama administration made the decision to halt deportations of illegal immigrants and give work visas to children who were brought into the United States before the age of 16.

Subsequently, congressional legislation known as the DREAM Act went even further. It gave provisional, and eventually permanent, citizenship to all immigrants who arrived in the country before the age of 16, as long as they lived in the United States for at least five years and had earned a high-school degree.

### 5. Protests during the 1990s led to Mexico’s first truly democratic elections in 2000. 

Mexico's Institutional Revolutionary Party, or PRI in Spanish, first came to power in 1929. The party was monopolistic, corrupt and had to use gerrymandering to maintain elected office.

So, it's no surprise that the Mexican people eventually began protesting against this authoritarian government. This wave of protest emerged in the 1990s and, by 1994, Mexico's expanding middle class felt utterly abandoned by the PRI as the peso crisis continued.

When protesters amassed in front of the Bank of Mexico, burning their credit cards in an act of defiance, it was welcomed as a sign of positive change for the country; things were finally shifting from apathy to rage and the government would be forced to make changes.

That same year, 3,000 men and women, dressed all in black, donning ski masks and carrying guns, appeared on the Mexican political scene. This was the Zapatista National Liberation Army, led by the enigmatic masked figure Subcomandante Marcos. They organized demonstrations in the city of San Cristobal de las Casas, in the state of Chiapas, demanding a halt to the violation of indigenous and poor people's rights.

When the Mexican army attempted to stop them, the Zapatistas retreated into the jungle, finding ways to disseminate their message from there. Finally, in 2000, thanks in large part to this growing public pressure, Mexico held its first truly democratic elections.

The president at the time, Ernesto Zedillo, was not keen on the violently repressive tactics his party had previously used to maintain power. Zedillo was ready to accept political reform and, in 1996, the electoral system experienced sweeping changes.

Up until that point, the electoral institute had been entirely run by the government's ministry of the interior — but after the reforms, it became an independent body run by citizens. Due to these reforms, the 2000 presidential race saw the election of the opposition candidate, Vicente Fox of the National Action Party, or PAN.

For the first time ever, control of the presidency had democratically changed hands in Mexico.

### 6. Trade agreements have strengthened Mexico’s economy, but liberalization won’t solve everything. 

Immigration remains a hotly contested issue, as people from different countries and cultures often experience misunderstandings and can have trouble finding common ground. But when it comes to international economic exchange, everyone stands to benefit.

Just take the Mexican economy, which has grown more robust in large part thanks to international trade agreements. The first such agreement was the _North American Free Trade Agreement,_ or _NAFTA_, a deal between Canada, the United States and Mexico, which some politicians began lobbying for as early as the 1980s.

A central facet of the deal was to do away with border taxes on imports and exports while protecting the intellectual property rights of entrepreneurs. Sure enough, when NAFTA was signed in 1992, trade between the three countries increased rapidly, while their economies grew faster than in the years prior.

For instance, by 2011, Mexico was exporting five times more products to the United States than it was before NAFTA, while US exports to Mexico had risen fourfold. In Mexico's case, this meant a boost to direct foreign investment, which was a huge development for the manufacturing sector and in turn made Mexico Latin America's largest exporter.

But this liberalization wasn't enough to solve Mexico's economic problems. While it might have given the country a trade advantage for a certain period, when other countries, such as China, strike their own free trade agreements with the United States, that advantage will dwindle considerably. Because of this distinct possibility, Mexico has to deal with its other economic problems sooner rather than later.

The main one at the moment is that the country is dominated by a handful of major companies. Without competition, these firms freely inflate their prices.

For example, according to the Organization for Economic Cooperation and Development (OECD)'s, estimates from 2010, Mexican families pay 40 percent more than they should for basic household goods. To reign in these huge corporations and cut prices, Mexico needs to toughen its economic regulations and deter such monopolistic tendencies.

Just consider Mexico's telecommunications market, which is dominated by two companies: _Telmex_ and _Telcel_, both of which belong to a single multibillionaire by the name of Carlos Slim.

### 7. Drug trafficking has become big business in Mexico, bringing tremendous violence along with it. 

Most people know that recreational drugs are commonplace in Western countries. But the relatively harmless aspects of recreational drug use, primarily involving teenagers eating too many Cheetos, is counterbalanced by a much darker side.

Over the last 30 years, Mexico has become a major hub for drug trafficking. It used to be that the primary path by which drugs like cocaine entered the United States was from Colombia, through the Caribbean and into Miami. But in the 1980s, the United States spent billions of dollars to tighten up security along its southeast coast, effectively cutting off this route.

With massive demand and no supply, the drug trade shifted its business to Mexico. While the country initially served as a mere alternative path for Colombian drugs, Mexicans soon began building their own drug cartels to cash in on the huge profits of this new business.

As a result, today, 90 percent of the cocaine that arrives on US soil comes from one of the big Mexican drug cartels. Not only that, but Mexicans also trade in a huge portion of the marijuana and heroin that crosses the US border, while also playing a key role in drug shipments to other continents.

As you might imagine, the booming Mexican drug trade comes with a concomitant escalation in violence. In fact, in Mexico, the transportation of drugs caused violence right from the start, as drug cartels quickly butted heads.

However, until the 1990s, the situation was relatively controlled, since the drug dealers had made agreements with the corrupt authoritarian government. But when opposition politicians assumed office in the late 90s, the drug cartels could no longer rely on the government for immunity and violence erupted.

For instance, in Chihuahua, murders jumped by 60 percent in the two years following the 1992 election of a National Action Party governor. And when Felipe Calderòn was elected president in 2006, he declared war on drug trafficking, prompting continued and escalating violence between drug lords and the police.

> In 2011, towards the end of Calderòn's administration, five drug-related murders took place every day in Juárez.

### 8. The United States and Mexico must collaborate more on immigration and strengthen their economic ties. 

Have you heard about the American citizens who take it upon themselves to patrol the Mexican border? Their intention is to control the flow of immigration, but this tactic is by no means sustainable.

Instead, the United States and Mexico need to foster greater collaboration on the issue of immigration. The first thing that needs to be done is to speed up the process by which immigrant families are reunited. An easy way to do this would be to allocate family-based immigration visas to the immediate family members of naturalized US citizens.

After that, the legal flow of immigrants should be set to meet the needs of the US labor market. To accomplish this, the United States should be more flexible with the number of work and permanent visas it issues each year.

As it stands, the United States uses a fixed quota system of visas that doesn't account for the fluctuating needs of the market. It's only rational that when more foreign workers are needed, more visas should be granted.

And finally, the United States needs to enforce existing labor laws by cracking down on employers and employees in the black market.

Beyond the need to handle the immigration issue, the United States and Mexico can also strengthen their economic ties, bringing mutual benefits in the process. While Mexico enjoys booming manufacturing and export industries, it's sorely lacking in infrastructure.

For instance, El Paso, a city on the border with the US state of New Mexico, has a railway network that's over 100 years old and can't keep up with the rapid pace of exports to cities in the American South. To help out, the United States can play a crucial role as an investor by supplying the billions of dollars it will take to build new airports, highways, ports and railroads.

In exchange, the United States will gain access to Mexico as a cheap, ideally located place for building factories and manufacturing goods.

### 9. If political and economic reforms continue, Mexico will become a strong new democracy. 

In July of 2012, Enrique Peña Nieto of the PRI, which held Mexico back for so many decades, was elected president of the country. However, it quickly became clear that the party would no longer impose an authoritarian socialist regime.

In fact, the latest indicators point to a Mexico that is both democratic and prosperous. For instance, during his first year in office, Peña Nieto introduced 16 reforms, all on important issues.

The first dealt with education by ending the long-held Mexican custom of families buying or inheriting positions at schools. In its place, a federal census was formed to calculate student and teacher numbers in the country and evaluations were adopted to monitor the quality of education.

From there, the government took on the corporate monopolies. To do so, they passed new laws, empowering regulators to better control large media and telecommunications firms that abuse their power. One such law states that companies owning more than 50 percent of a particular market must share their infrastructure and pay higher fees to boost their competitors' chances for success.

Going forward, there are still decisive changes to come, including political and economic reforms. One of these crucial decisions, to be made by Mexico in the coming years, is whether or not to allow the reelection of politicians.

This is a fundamental issue because, right now, politicians in the country can only be elected for a single six-year term, which means they have no incentive to keep the promises on which they campaigned. If reelection becomes a reality, it should encourage greater accountability and long-term thinking among the country's elected officials.

Another important reform now under consideration would open up the Mexican energy market to foreign investors. Such a change would make a big difference since the country's national energy company _Pemex_ remains under firm governmental control, with all investment approvals going through the national treasury.

If this reform passes, energy companies in the future would be in control of their own budgets and investments, attracting billions of dollars in foreign investment.

### 10. Final summary 

The key message in this book:

**Despite a long history of strained diplomatic relations and disagreements, Mexico and the United States are finding more common ground every day. Both countries stand to benefit as they develop their relationship as economic partners and democratic neighbors.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Brazillionaires_** **by Alex Cuadros**

_Brazillionaires_ (2016) exposes the true story behind Brazil's tumultuous economy. By tracing the rise and fall of billionaires like Eike Batista, these blinks take you through the country's history of inequality and corruption, and explain how the nation's politics and business have become inseparable.
---

### Shannon K. O’Neil

Shannon K. O'Neil is a specialist in Latin America at the Council on Foreign Relations, an American NGO and think tank that endeavors to educate the public on issues of foreign policy. Her work has been published in the _Washington Post,_ the _Los Angeles Times,_ and _USA Today_. _Two Nations Indivisible_ is her first book.

