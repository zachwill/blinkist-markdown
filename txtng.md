---
id: 587a86baa8cb0a000443a1c4
slug: txtng-en
published_date: 2017-01-17T00:00:00.000+00:00
author: David Crystal
title: Txtng
subtitle: The gr8 db8
main_color: E25537
text_color: C93B1C
---

# Txtng

_The gr8 db8_

**David Crystal**

_Txtng_ (2008) is a bold endorsement of texting as an effective and creative — and even poetic — form of communication. These blinks offer a look at how the unique language of text messaging came to life and why critics of texting's inventive shorthand need to calm down, stop worrying and learn to love the SMS.

---
### 1. What’s in it for me? Get 2 c the impact txtng has on how we communic8. 

Messenger, WhatsApp, Viber and of course SMS — the list of ways to send texts is getting ever longer. These days, it seems like calling someone is just something you do in an emergency. For all other communication, the written word is the way to go.

In the early days of texting, you could only use so many characters in a single text. This resulted in a new shorthand, filled with abbreviations and acronyms, a shorthand that is still with us today. But not everyone thinks this is a good thing. Some actually think it will destroy our language. So let's look at how texting affects our language and see if the detractors are right.

In these blinks, you'll learn

  * that abbreviations have been around for a long, long time;

  * who the most active abbreviators actually are; and

  * what impact texting really has on the way we write.

### 2. Some critics see the new SMS language as a threat to proper English. 

If you have a cell phone, there's a good chance that using it to make phone calls is a secondary function — and that when it beeps or buzzes, more often than not, it's an incoming text message.

Since 2000, the use of text messages as a way to communicate has skyrocketed.

In 2001, 12.2 billion text messages were sent in the United Kingdom alone. While that might seem like a lot, that had number exploded by 2007. Forty-five billion texts were sent that year.

To financial analysts, these numbers translate into big money, and, in 2005, the worth of the SMS business was estimated at $70 billion.

Of course, this isn't limited to any one area. By 2007, there were more cell-phone subscriptions in places like Sweden, Hong-Kong and Italy than there were people living in the country, which means some people owned two or more cell phones.

Even areas where cell phones were scarce started catching up. Africa grew the fastest, with cell phone usage climbing from 6 percent to 21 percent between 2003 and 2007.

This cell-phone boom ushered in a new texting language, which relies on creative shorthand and abbreviations. Some people have suggested that this new language is ruining standard written English.

Professor Crispin Thurlow is a linguist who has collected numerous reports from US newspapers that point to a significant amount of adults who feel that the language of texting has morally corrupted society.

One report, from a 2007 Washington newspaper, blames the abbreviations and colloquialisms of text messages for ruining the way people use the English language. And this is just one of many similar articles.

That same year, in the _Daily Mail_, UK broadcaster John Humphrys called texting a "rape" of the English vocabulary. He called abbreviations like "IMHO U R GR8" completely incomprehensible.

For the record, that's short for: "In my humble opinion, you are great."

> _"'Texters are vandals who are doing to our language what Genghis Khan did to his neighbours eight hundred years ago.'"_ \- John Humphrys

### 3. Texting is a creative, versatile and even poetic language that can take on many forms. 

You might have a relative, teacher or friend who harbors negative opinions on texting. 

And, yes, it's understandable if that person finds some of the abbreviations confusing. But what critics fail to see is that texting is also a creative and even poetic form of writing.

There are even awards for it. In 2001, the _Guardian_ put together the first SMS poetry competition, with these simple rules: Write a poem containing less than 160 characters, the limit in a standard text message.

The contest received 7,500 submissions that were judged by a panel that included the poet laureates Peter Sansom and U.A. Fanthorpe.

Ultimately, Hetty Hughes won with the following composition: "txtin iz messin, / mi headn'me englis, / try2rite essays, / they all come out txtis. / gran not plsed w/letters shes getn, / swears i wrote better, / b4 comin2uni. / &she's african" 

The contest also shows that texting isn't all about abbreviations. Three of the five best SMS poems used standard English and no abbreviations at all.

Steve Kilgallon provided this evocative submission: "Sheffield / Sun on maisonette windows / sends speed-camera flashes tinting through tram / cables / startling drivers / dragging rain-waterfalls in their wheels / I drive on"

Recognizing that SMS offers a unique opportunity to be imaginative, the _Guardian_ awarded a special prize for the poem with the most creative use of abbreviations.

It went to Julia Bird, whose poem, "14: a txt msg poM," contained memorable lines such as "my @oms split / wen he :-)s @ me."

So the next time someone starts bad-mouthing texting, you can let them know that it's also a creative form of self-expression.

### 4. There is a historical precedent for the abbreviations and symbols used in texts. 

Another thing that critics of texting seem to forget is that abbreviations are nothing new. The letters RIP, for instance, have been on tombstones for centuries. It's unlikely that a proliferation of shorthand will destroy the English language.

Indeed, the practice of abbreviating words actually goes all the way back to ancient civilizations.

In fact, text messages and Egyptian hieroglyphs both use _rebuses_ — a message made up of pictures that represent certain words. For example, a picture of an eye can stand for the word "I" and the image of a bee can refer to the word "be," and so on.

Egyptians are the one's best known for using symbols to create messages, but the tradition of rebuses has carried on throughout history.

In the village of Kidlington in Oxfordshire, you can visit the church to see a rebus that spells out the name of the town: a goat + a fish + a ton of wool = Kid-ling-ton.

This is the same principle that texting has incorporated, with letters, numbers and pictures being used to create all kinds of abbreviations.

The most common abbreviations simply use one letter or number as a phonetic stand-in for a simple word, such as the letter "B" for the word "be," the number "2" for the words "to" or "too" and an "X" to represent a kiss.

Things get slightly more complicated when we combine these into longer words, such as "b4" for "before," "2day" for "today" or a bunch of "Zs" when we're feeling sleepy.

But texting's more unique contribution to written communication — the one that has stronger ties to rebuses — are emoticons. Using a combination of punctuation symbols, we can create a simple or elaborate smile. For instance, :-) or (^_^). Or we can make a frown, like this one :-( if we feel unhappy.

### 5. The unique language of texting began as a response to the painfully slow process of typing on a cell phone. 

It's easy to say "never" to something new that comes along: "I'll never join Facebook" or "I'll never use an emoticon in a text." But, before long, you might find yourself becoming an expert on all the different smileys you can use in a text.

When texting first started, it was actually quite natural to turn to abbreviations and symbols since traditional cell-phone keypads weren't ideal for writing quick messages.

Large portions of the developing world still have these standard keypads that don't offer the variety of options that come with a QWERTY keyboard.

A standard cell phone only has 12 to 15 keys to accommodate all 26 letters of the alphabet, which can make texting a slow and arduous process. Just to write the letters "DEF," you have to press the number 3 button once, wait for the "D" to register, then press it two times for the letter "E," wait some more, then press it three times for "F" and wait once again.

Cell-phone services quickly came out with _predictive texting_, which uses dictionaries to anticipate what you're trying to type and give your thumbs a break, but these systems are far from perfect.

In 2002, the linguists Crispin Thurlow and Alex Brown found that only half of cell-phone users were using this service. Many found it frustrating that the wrong words were being suggested, such as offering "pervert" instead of "request," or "book" instead of "cool."

And though it's possible to add new words to the cell phone's memory, most people found it simpler to use abbreviations or spell the words out. It's not only faster to come up with a quick way to say what you want to say; it's more fun as well.

### 6. People compose texts differently depending on their age and gender. 

If you take public transportation, then you know that, for the most part, your fellow travelers spend their commute staring into the glow of various mobile devices. Many of them spend this time typing away, composing various messages to friends or family.

There's commonly believed that young people are the primary texters. But it's actually a much bigger demographic than that. 

When Norwegian social linguist Richard Ling took a closer look in 2005, he found that 85 percent of teenagers and young adults would send a text at least once a day. Only 2.7 percent of retired people were this active.

However, two years later, in 2007, the UK Office of Communications found that British adults sent an average of 28 text messages each week, compared to the 20 cell-phone calls they would make.

But Ling found another unexpected statistic: His research showed that older people were, in fact, more likely to use abbreviations and creative spelling in their text messages, presumably because they didn't want to spend all day trying to type on a phone.

Statistics also show that while women are more frequent texters, men are more likely to use abbreviations. 

Ling's research reveals that 40 percent of women send a text every day, compared to only 35 percent of men.

Women also tend to write longer and more complex messages. On average, their texts contained 6.95 words, though it wasn't uncommon for their messages to contain multiple sentences.

On the other hand, men kept their messages to a single sentence 85 percent of the time, especially the adolescents.

Women are more likely to apply the standard rules of writing in their texts as well, with 8.5 percent using correct spelling and capitalization compared to only five percent of men.

What all these numbers show is that it's men who keep their texts the shortest by using the most abbreviations.

### 7. Texting allows parents to communicate with their children, and teenagers use it for dating. 

Even before the texting phenomenon, parents would complain about being ignored by their teenage children. But even though texting is a formidable distraction for teens everywhere, this method of communication is proving to be a handy way for parents to keep in touch with their kids. 

Parents are discovering that sending a text is a perfect way to deliver a small but important message or piece of advice to their children.

In 2006, a study by Mediathink showed that 63 percent of US parents who own cell phones agree that using text messages improves their communication with their children.

This development hasn't been lost on the cell-phone industry. That same year, the Prom Text campaign was launched, allowing parents go to a website, www.prmtxt.org, and enter their child's cell-phone number along with the details of the upcoming prom. This way, the teens will receive a message on prom night that reminds them to be safe and not drink and drive. 

Some people think texting has a big advantage in certain situations. For instance, it provides a safe distance when you need to deliver a message that someone might not want to hear. Teenagers, in particular, are willing to use texts to quickly break up with someone or to confront an angry partner.

In Japan, texting has become a big part of courtship in the country's introverted culture.

There's even a ritualized form of cell-phone dating called keitai, or go-kon. It involves four boys sitting across a table from four girls, and the only flirting that happens takes place strictly in a flurry of text messages. The only talking that takes place is between the fellow girls or boys, who might whisper encouragement or advice to one another.

> _"You can say some things in text that you can't say face-to-face."_ \- Aidan, 15 years old

### 8. Traditional language skills aren’t harmed by texting, though schools hesitate to accept it as a form of language. 

As you may have gathered by now, there's nothing to worry about — texting is not a sign of falling literacy rates or the end of proper language skills.

It's clear that people know when and where to use the language that has developed with text messaging.

When the author was working with a group of UK teenagers, he asked them if they ever thought about using texting language in their schoolwork. As you might imagine, the students were quite confused and responded with their own question: Why would anyone use texting language outside of a text message?

In a 2003 BBC documentary, this was the exact point that several adolescents tried to make when they became tired of hearing their parents complain about the horrors of texting. It's easy for people, both young and old, to distinguish between the language of text messages and standard English. It's not unlike knowing when and where to use slang.

This was further confirmed in a 2005 study by language therapist Veenal Raval. He found that students who send off frequent texts are just as literate as those who never text at all. 

However, it might still take some time before the language of texting is accepted as a legitimate form of creative communication. 

In Victoria, Australia, one progressive school proposed a text-messaging course as part of their English language curriculum for young teenagers. Students would have been tasked with translating messages and keeping a glossary that they would add to and compare against traditional English.

There's actually a precedent for this exact kind of class: For many years it was a standard part of a British school curriculum to compare nonstandard English with the textbook standard. Nevertheless, the proposed course was met with heavy criticism by many, including the federal Minister of Education. The critics didn't think such a class would be sufficiently academic.

Only time will tell if academia will join the mainstream and appreciate the creative, useful and versatile form of communication that has become part of our culture. In the meantime, we can all enjoy this new and creative language in our texts.

### 9. Final summary 

The key message in this book:

**Texting is not endangering the English language; it is actually enriching it by offering a creative and immediate way for people to express themselves and share their feelings. This form of communication actually has its roots in shorthand writing and the pictorial traditions that date back to the ancient Egyptians.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Reclaiming Conversation_** **by Sherry Turkle**

_Reclaiming Conversation_ (2015) reflects on how we interact with one another in our increasingly digitized world. Constant interruptions, leaving messages unanswered and lack of interest have all become the norm in a world rife with mobile devices and screens. But is this what we want? And if not, what can we do about it?
---

### David Crystal

David Crystal is a linguistics professor based at Bangor University. He specializes in Shakespearean studies as well as the history and development of the English language. In addition to numerous articles about language, he is also the author of _Just a Phrase I'm Going Through_ and _The Fight for English_.

