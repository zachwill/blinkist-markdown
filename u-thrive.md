---
id: 59e4ac3ab238e1000617b407
slug: u-thrive-en
published_date: 2017-10-20T00:00:00.000+00:00
author: Daniel Lerner and Alan Schlechter
title: U Thrive
subtitle: How to Succeed in College (and Life)
main_color: E4612E
text_color: E4612E
---

# U Thrive

_How to Succeed in College (and Life)_

**Daniel Lerner and Alan Schlechter**

_U Thrive_ (2017) provides the tools necessary to thrive on a college campus and beyond. Informative and actionable in equal parts, these blinks provide great insights and offer helpful advice on everything from alleviating stress during final exams to surviving nightmarish dorm mates.

---
### 1. What’s in it for me? Thrive in college. 

College is a place of opportunity. But for many young adults, getting the news that they have been accepted brings along a complex mix of emotions, with feelings of excitement, relief and anxiety all swirling around at once. Some may suddenly start wondering if they'll be able to thrive in their new surroundings, among new friends, communities and learning environments. But with proper preparation, there will be nothing to worry about — and these blinks are a good place to start.

In these blinks, you'll find out

  * how students today are much less social than previous generations;

  * why physical exercise is the key to success in college; and

  * how there are two different mind-sets you can have — and only one is good for your college years.

### 2. Good grades and success don’t bring happiness; rather, happiness brings success and good grades. 

There's a popular belief that one of the keys to happiness is to thrive at school and be successful in your endeavors.

As we grow up, we're led to believe that getting into the right college, earning good grades and a spot on the school football team are all steps that bring us closer to happiness.

But this isn't how life works. Earning degrees and acing exams are no guarantees of happiness.

Being accepted to a university certainly isn't a direct precursor to happiness. When a group of college freshmen were asked to rate their emotional well-being on a scale of zero to one hundred, their average score was only 50.7.

We've also been led to believe that some jobs are a marker of success and are therefore more desirable. Being a lawyer is one such highly-regarded profession, but in one poll of over 800 lawyers, the results showed that they were the least happy of all white-collar professionals and, as a result, drank and smoked more than any of their counterparts from other professions.

A better way to think about life is to understand that success doesn't lead to happiness; rather, happiness will help you find success.

Whatever field you hope to thrive in, be it business, finance, the arts, sports or entertainment, happiness and a positive mind-set will increase your chances of success. In 1997, an experiment at Cornell University had three groups of doctors diagnose a case. To lift their spirits, the first group was shown a bag of treats that they could indulge in later, the second group was reminded of how medicine was a humanitarian pursuit and the third group was given no encouragement whatsoever.

Both of the groups that had received positive encouragement performed better, diagnosing the case 20 percent faster and more precisely than the group that received no positive priming. This illustrates how positive emotions can help you perform better.

So, remember: let happiness into your life and move forward with positivity in mind. This will help you find success and make your dreams come true.

> _"Reaching our goals does not automatically flip on some circuit breaker labeled HAPPY LIFE."_

### 3. Avoid isolating yourself in times of stress; friendships and socializing are keys to a healthy mind. 

College can be a scary place. Not only do you have the pressure of trying to get good grades, but you're facing a whole campus full of strangers for the first time, which can lead to a lot of social anxiety.

This kind of stressful situation can cause a person to retreat into their shell and seek the comfort of isolation. And, sure enough, an increasing amount of college students are doing just that.

A 2014 survey of around 150,000 freshmen revealed that today's students spend less time socializing than any other generation of the past three decades. The results showed that, over the course of a week, 39 percent of the freshmen population spent less than five hours with their friends. By contrast, in 1987, only 18 percent spent that much time alone.

One of the primary reasons for this behavior is stress, as 53 percent of the students surveyed reported a tendency to avoid their friends when feeling stressed and miserable.

If you hope to thrive in the college environment, you need to fight against the urge to isolate yourself. One of the best ways to relieve stress and help prevent future outbreaks of anxiety is to spend time with friends and nurture your relationships. As it turns out, moments of increased stress are actually the best times to visit friends and socialize.

Solid friendships are one of the best defenses we have to maintain a healthy emotional disposition, even in the worst of situations. In one study, researchers found that after examining 800 hurricane survivors, those with solid friendships were four times likelier to avoid post-traumatic stress disorder than survivors without a strong support network.

Clearly, you will struggle to thrive if you are sad and stressed out, and isolating yourself from others will only make the problem worse.

> _"Placing yourself in solitary during the tough times is like driving away from the hospital as your appendix is about to burst."_

### 4. Let your talents guide you on your career path and stay determined along the way. 

It's clear that engaging with others is central to thriving in whatever pursuit you choose, and when you identify your talents and put them to use, it will naturally lead to collaboration with others in your community.

If you're not sure what your talents are, think of any activity that you excel at, or a skill that you're motivated by and hope to improve. Often, your talent lies in any skill that causes time to fly by when you're practicing it. This deep engagement with a skill is known as _flow_, and it's a sure sign that it is something you should keep practicing.

Once you know where your talents lie, let them inform your decisions in college and beyond, and you'll likely thrive in life.

However, it's also important to understand that things change, and your high school passions and talents can transform in college.

Consider what happened to a bright student named John, who had been the captain of the baseball team in high school and highly passionate about the sport. But then, in his first year of college, he lost his motivation and soon stopped playing entirely. Meanwhile, he kept trying hard to succeed in his classes, but wasn't enjoying them at all either.

Luckily, John soon realized that he was letting some vague notion of what it means to be successful direct his life, rather than examining his own interests and talents.

After some soul-searching, he was back coaching a children's baseball team and had shifted the focus of his college courses toward teaching and critical pedagogy. He realized that this was where his talent and skills were — once he had figured it out, he was able to thrive once again.

But talent isn't everything. You should also be sure to awaken your _character strengths_.

Character strengths are things like perseverance, determination and other qualities that keep you moving forward in the face of adversity.

For example, when the legendary basketball player Michael Jordan was trying out for his school's varsity team, he didn't make the cut the first time around. Although he was heartbroken, he didn't give up. He simply kept playing and getting better, safe in the knowledge that there's always next time.

### 5. Avoid a fixed mind-set by being open to change and accepting constructive criticism. 

It's important to have the strength to forge ahead in tough times, but to do so this demands the right mind-set.

If you're inflexible and unwilling to change or accept critical feedback, this is a sign you have a _fixed mind-set_ and it's unlikely that you'll thrive with this attitude.

A fixed mind-set is problematic in college because it makes failure seem permanent: if you didn't pass the exam today, you feel like you'll never pass it because you don't believe in your ability to change or improve.

Many students tend to feel judged and unfairly criticized when they receive bad grades or papers filled with red marks — but there's no need to despair.

Instead, a thriving student just needs to adopt a _growth mind-set_ and, rather than focusing on the negative, identify ways to improve.

With a growth mind-set, critical feedback isn't seen as an attack and a bad grade isn't the end of the world. Instead, they are helpful signs that point to skills that need improvement.

Let's say there are two students, Jocelyn and Peter. While Jocelyn sees a bad grade from a teacher as a personal attack, Peter accepts the feedback as valuable information that will help him improve. Now, which student would you expect to thrive in college?

If you're sensitive to criticism, don't despair; you can always change your mind-set by acknowledging the benefit of change and taking control of your reactions.

Jocelyn recognized that she needed a new mind-set and went to the authors for help. They told her that it was important to view teachers as people who want to help, not judge.

Remember, teachers devote their time to providing carefully considered feedback to help students improve. With this in mind, Jocelyn could begin to see feedback as a crucial aspect of her overall learning process.

One tip that Jocelyn really found helpful was to say "thank you" when a teacher offered advice. This strengthened her ability to see the teacher as a friend and ally, rather than a judge who is out to make her life miserable. ****

### 6. A healthy mind thrives in a healthy body, so stay on the move and keep your heart rate up. 

Changing your mind-set will only take you so far if you're neglecting the rest of your body. To really thrive in college, you need to make sure you're not sitting in lecture halls or your dorm room all day.

A thriving person is someone who stays on the move.

Jogging, biking and regular walks are all helpful, so don't think you need to be running marathons to stay active and healthy.

When you're on the move, your brain produces a protein known as BDNF, or _brain-derived neurotrophic factor_. This protein interacts with the brain's frontal lobes, which are responsible for your ability to learn and grow.

One school in particular has shown how movement is an important factor in helping students thrive. When Naperville Central High School in the US state of Illinois found their school ranked eighteenth in the world for mathematics and nineteenth in science, the faculty decided they could do even better — and they did this by strengthening their physical education program.

They didn't focus on running faster or jumping higher; they simply made sure that their students spent less time sitting and more time moving and keeping their heart rates up. Sure enough, when the next rankings came in, Naperville Central High was in sixth place for math and _number one_ in science!

Even if you think there's no time in your busy schedule for exercise, with a little creativity, you can surely fit more movement into your routine.

Even short, five-minute breaks are great for getting out of your chair and moving around. Whether it's a quick walk or a round of push-ups, anything that gets the heart going is better than sitting still.

Psychologists at the University of Sheffield monitored a group of students who danced during their routine breaks, and they were found to be more happy, energized and creative than students who continued to sit around during breaks.

You can also use a standing desk from time to time, so you can do your readings or write essays while on your feet. With some well-placed books and a bit of ingenuity, anyone can build their own.

### 7. To achieve excellence, practice deliberately, set reachable goals and find an apprenticeship. 

Everyone wants to be excellent at what they do. But to get there, you need to be familiar with _deliberate practice_.

According to the psychologist K. Anders Ericsson, greatness and excellence aren't a matter of being born with a genetic advantage; rather, they are all about putting in hard work and practicing your craft. Whether you're a doctor, dancer, musician or business manager, practice makes perfect.

But not all practice is equal, and to reap the greatest benefits, you must know how to make the most of the time you invest. This is where deliberate practice comes in.

One important aspect of deliberate practice is setting concrete goals, which help you chart your progress and stay motivated.

Meredith is someone who dreamed of being the best real estate agent around. She knew she faced stiff competition out there, so she wanted to give herself an advantage by broadening her range of expertise and studying sales, financing, marketing and other useful subjects.

So, each month, she focused intently on one subject and practiced deliberately. During her month of studying sales, she read biographies on the best salespeople, sat in on sales meetings and reached out to colleagues for advice.

As the months went by, Meredith filled a series of notebooks with the knowledge she had accumulated, and she realized she was reaching her goal of growing and learning.

Another way to achieve excellence is to find a mentor and learn from someone who's already been through it all.

Jon dreamed of being an outstanding photographer, and so he became an assistant for the esteemed photographer, Richard Avedon. Jon didn't expect Avedon to spend his days sitting with him and being a teacher; instead, it was an opportunity for Jon to keenly observe and find out things like which lenses Avedon used for certain situations while also witnessing what it's really like to run a world-class photography business.

It takes a lot of effort to be the best that you can be, but when you're doing something you love, it'll be work that you won't mind doing.

### 8. Final summary 

The key message in this book:

**Although college is tough, there are ways you can survive and even thrive in this stressful environment. Remember to focus on your talents and build your character strengths — and don't neglect physical exercise. With the right attitude and mind-set, you'll not only make it through your undergraduate years in one piece; you'll even flourish and put yourself on the path to a fulfilling career.**

Actionable advice:

**Think of a happy memory for 30 seconds.**

Whenever you're about to be challenged, whether it's during your final exams, midterms or a stressful social situation, close your eyes and spend 30 seconds thinking of a cherished, peaceful memory. When you prime your brain in a positive way, it can make any challenge that little bit easier to overcome. It may sound simple, but it really does help.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The End of College_** **by Kevin Carey**

The End of College (2015) is about the American higher education system. These blinks give a historical overview of how the author sees the development of the American university and its evolution from European models. He evaluates its current status and advocates for the _University of Everywhere_ — a remotely accessible university of the future.
---

### Daniel Lerner and Alan Schlechter

Daniel Lerner is a clinical instructor at the University of New York, where he teaches the wildly popular elective class "The Science of Happiness" with his colleague, Alan Schlechter. With his background in positive psychology, he also works as a performance coach for many prominent artists and executives.

Alan Schlechter is an assistant professor at the Langone Medical Center at the University of New York. Schlechter graduated from Wesleyan University and Mount Sinai Medical School, and now heads the Outpatient Child and Adolescent Psychiatry program at Bellevue Hospital in New York City.

