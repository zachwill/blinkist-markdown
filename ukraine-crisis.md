---
id: 5740b9ac2edd1b00032e6bcb
slug: ukraine-crisis-en
published_date: 2016-05-26T00:00:00.000+00:00
author: Andrew Wilson
title: Ukraine Crisis
subtitle: What It Means for the West
main_color: E4612E
text_color: E4612E
---

# Ukraine Crisis

_What It Means for the West_

**Andrew Wilson**

_Ukraine Crisis_ (2014) addresses the peaceful protests and violent conflicts that have rocked Ukraine in recent years. This book take a look at the events surrounding the Maidan uprising, the Russian annexation of Crimea and the ongoing conflict in the Donbas. Importantly, the crisis is put into context not just for the future of Ukraine but also how it affects Russia, the European Union and the world.

---
### 1. What’s in it for me? Take a closer look at the roots and the dynamics of the crisis in Ukraine. 

A European country governed by a rogue leader. Oligarchs and mafia dons making political decisions. Voice an opinion against those in "power" and find yourself in jail.

This is modern Ukraine. For years, politics in this former Soviet country have been for sale, and Russia has been doing the buying, garnering more and more control. And meanwhile, the rest of the world sits back and watches, unable or unwilling to change the fetid status quo.

These blinks will show you how Ukraine got to this point, and importantly, explain what's needed to get it out of this mess — and why the rest of the world simply can't stand by idle anymore.

In these blinks, you'll also learn

  * how a president allegedly fled the country with truck filled with cash;

  * how a computer hack triggered the Orange Revolution; and

  * why it took just a few masked men to take control of Crimea.

### 2. Ukraine, sandwiched between Russia and the EU, is a country torn by old and new loyalties. 

Since long before protests erupted in 2014, Ukraine has struggled with political and economic problems. Much of its trouble has to do with its location: Ukraine is wedged between Russia and Western Europe.

Unsurprisingly, Russia has a long history of intervention in Ukraine. Recently, the country has sought to reassert its control in the region, following the collapse of the Soviet Union.

Ukraine however depends heavily on Russia's gas and oil reserves. Russia is well aware of this imbalance, and could easily drive Ukraine into bankruptcy by raising the price of fossil fuels. This position gives Russia a lot of leverage over its neighbor.

Russia has also funded certain Ukrainian politicians, who advocate political agendas written by Moscow, stressing pro-Russian and anti-Western actions.

On the other "side," European Union countries also work to influence events in Ukraine. In 2008, the country began negotiations over an _association agreement_, a treaty with the European Union that creates a framework for strengthening Ukraine's political and economic ties with the West — much to Russia's chagrin.

The power of both the European Union and the North Atlantic Treaty Organization (NATO) has been increasing, expanding eastward after the fall of the Iron Curtain. These two groups have brought more and more of Russia's former allies, such as Poland and the Baltics, into their fold.

Russia, in turn, has tried to aggressively guard its former sphere of influence.

This has put the European Union in an uncomfortable position. Member states have cut back on military spending while Russia is rearming — so provoking conflict is a dangerous game. And like Ukraine, the European Union also depends on a steady supply of Russian fossil fuels.

For some time, the strategy of the European Union was to simply ignore Ukraine's growing predicament. Unfortunately, this ended up only making things worse.

> _"Why should the rest of the world even listen to Europe if it couldn't prevent disaster in its own neighborhood?"_

### 3. Putin’s authoritarian Russia spreads propaganda to bring countries like Ukraine back under its influence. 

After the collapse of the Soviet Union, Western countries assumed that Russia would embrace democracy and Western political modes of thinking and behavior.

This, however, has been far from the case.

While it's true that Russia has formally implemented a democratic governmental system, undemocratic practices such as bribery, censorship and political thought suppression still thrive.

Russian President Vladimir Putin has sought to gain as much power as possible without international interference since coming to power in 2000, paying only lip service to any notion of Russian democracy.

Putin has manipulated elections by engaging in voter fraud, such as doubling votes from regions known to support him. He has also been known to use any means he can to suppress anyone who opposes him, but funding has been his most powerful domestic tool. He's cut funds to regions that criticize his policies, while spending lavishly in areas or on conservative groups that support him.

The government also works to persecute or silence journalists who try to bring to light the regime's wrongdoings. Journalists at rallies are attacked more often than are protesters, to keep them from doing their jobs and reporting on dissent.

Putin works to manipulate Russians both at home and abroad, including those who live in Ukraine.

In fact, Putin once spent over $8 billion in one year as part of a public relations campaign, geared to convince Russians living in neighboring countries that those lands should be reunified with the "fatherland." Ukraine was targeted heavily in this campaign.

Putin ultimately uses all resources at his disposal to gain power for himself and his government, fully embracing manipulative or coercive means to strengthen both his domestic and international influence.

### 4. The early Orange Revolution failed to root out rampant corruption in Ukraine’s political system. 

In 1991, Ukraine gained its independence from the Soviet Union. At the time, the country seemed ready to embrace a legitimate democratic government.

Yet this young country continued to be poorly governed, thanks to Russia's heavy hand and a lack of support from the European Union.

Corruption was widespread in the newly independent Ukraine. The government was too weak (or perhaps too unwilling) to reign in elites or crack down on organized crime.

Oligarchs, clans and mafia groups heavily influenced Ukraine's political future. As a result, Russian elites eager to keep a hold on Ukraine were happy to approach these elements with cash bribes in hand.

Ukrainian presidential candidate Viktor Yanukovych stole millions of votes by hacking into the computer system that managed the official vote count. He did this with the help of computer experts, who were either brought in from Russia or paid with Russian funds.

More than a decade into independence, Ukrainians fed up with endemic corruption began a series of protests that grew into what became known as the _Orange Revolution_.

People gathered on Kiev's central square, the _Maidan_, to protest Yanukovych's fabricated win of the 2004 presidential election. Citizens pressured the Ukrainian Supreme Court into examining and eventually annulling the election.

Unfortunately, the events of the Orange Revolution did little to improve the country's political situation. In fact, things actually became worse.

While Viktor Yushchenko, the people's choice, was finally elected president, corruption was still widespread. And when Yanukovych finally came to power in 2010, corruption was not just a problem but a way of life in a "democratic" system that had completely broken down.

Such a situation should have prompted EU intervention, but no help was forthcoming.

### 5. Citizens returned to the Maidan to protest corruption, and eventually drove the president from office. 

In early 2008, the European Union and Ukraine began negotiations over a free trade agreement, but there were a few strings attached.

The European Union wanted Ukraine to sign an association agreement, a treaty that would ideally work to dismantle corruption in the government and in the judicial system.

President Yanukovych however refused to cooperate, knowing that such an agreement would destroy his ability to siphon funds from the government for both himself and his friends.

After Yanukovych's Russian-backed government rejected the agreement in November 2013, Ukrainians again took to the Maidan in protest, furious as many believed the EU agreement was their only hope for real economic and political reform.

Though the protests started small, tensions and crowds grew quickly when Yanukovych refused to engage with EU leaders in Vilnius at the Eastern Partnership Summit, a gathering tasked with improving trade and political ties between the European Union and post-Soviet states.

Yet when Yanukovych ordered government troops to use force in the Maidan, citizens came out in droves, with some 10,000 people flooding the center of Kiev. Events turned violent on both sides, as protesters started throwing rocks and Molotov cocktails at government troops.

Finally, the European Union determined it was time to act. EU foreign ministers called for emergency negotiations, offering Yanukovych a second deal that was mostly similar to the original association agreement.

Fearing the outbreak of civil war in Ukraine, Yanukovych had no choice but to accept the EU's offer.

The two sides agreed to temporarily reinstate the more democratic Ukranian constitution that was in place before Yanukovych came to power, at least until an improved document could be written. With this, militias surrendered in the Maidan, signaling that the protesters had won.

Yanukovych quickly fled to Russia, taking with him an estimated $32 billion. Ukraine's acting prosecutor General Oleh Makhnitsky believes that the former president smuggled most of the cash in trucks.

### 6. While the new government tried to manage a revolution, Putin jumped at his chance to grab Crimea. 

The Ukrainian people finally drove out their corrupt leader, and many felt euphoric as a better future finally seemed a real possibility.

These feelings didn't last, however. Bad news came just a week after the freedom party had started.

While the new government and its citizens adjusted to the changes brought on by the Maidan uprising, Russia's Putin moved to annex Crimea, a peninsula on the Black Sea that belonged to Ukraine.

In doing so, Putin made the first territorial annexation in Europe since World War II.

He justified his actions by claiming that Crimea was once a part of Russia, before Soviet leader Nikita Khrushchev transferred it to the control of Ukraine in 1954. Tellingly, over half of Crimean residents identify as Russian and sympathize with Putin's regime. Crimea is also strategic, as it provides Russian access to the Black Sea.

So when masked soldiers from Russia swiftly and unexpectedly entered Crimea, Ukraine didn't have the power to respond.

Why was this the case? Ukraine's military had been underfunded for decades. Yanukovych spent more resources on territorial troops than on national forces, so the country's national defense was weak — particularly in the face of a threat as large as Russia.

Yet it wasn't hard for Russian forces to assert control. In fact, it only took 60 soldiers, masked and armed with Kalashnikov rifles, to ensure a referendum to confirm Russian rule over the territory.

Encouraged by his quick and easy success in Crimea, Putin set his eyes on his next target: the Donbas.

> _"The West was diverted into constantly speculating about what Putin might do next, rather than helping Ukraine deal with the situation..."_

### 7. Russian-backed troops provoked conflict in the Donbas after Russia annexed Crimea. 

After successfully claiming Crimea for the Russian Federation, Putin then sought to increase control over other former Soviet territories.

The eastern Ukrainian region of the Donbas was his next target.

On the surface, the situation in the Donbas may seem similar to that in Crimea. This eastern region also claims a large population of ethnic Russians and Russian speakers, many of whom identify strongly with the regime in Moscow. The Donbas is also a politically strategic area, as it has large coal reserves and is heavily industrialized.

Such elements aren't sufficient to justify an invasion during peacetime, however. Thus Putin declared that the Russian-speaking people of the Donbas were under threat by pro-Ukrainian activists, who were allegedly discriminating against them.

In parallel, Putin launched a new wave of propaganda to convince ethnic Russians in the Donbas that they would be better off under the protection of Moscow. A pro-Russian rebellion soon broke out and quickly received armed support from Russian forces.

Pro-Russian militias began taking over key government buildings, and Russia provided these local militias with additional combat forces and supplies.

The takeover of the Donbas wasn't as smooth as Putin had expected, however. This time, the Ukrainian government was prepared and fought back. Soon enough, war broke out in the region between Ukrainian forces and pro-Russian rebels.

One tragic event during this time made the situation even worse. A Malaysian Airlines commercial airliner was shot down as it flew over the Donbas on its way to Kuala Lumpur, killing 298 civilians.

Investigations showed that pro-Russian rebel groups appeared to have shot the plane down, based on the ammunition that was used. There has been no official conclusion, however, and the ongoing investigation has only resulted in accusations of guilt on both sides.

The conflict between Ukrainian forces and pro-Russian rebels continues to rage in the Donbas, as well as in Crimea. Outside diplomacy attempting to quell the fighting has so far been unsuccessful.

### 8. The crisis in Ukraine has changed the post-Cold War global political landscape for the worse. 

Many people in the West think that the crisis in Ukraine is far removed from their daily lives. This violent conflict affects more than just the citizens of the region, however.

Relations between Russia and Western nations are the worst they've ever been since the fall of the Iron Curtain. When Russia ignored Ukrainian national sovereignty through its annexation of Crimea, the European Union and the United States responded by imposing sanctions on Russia, with the intent to further weaken its already ailing economy.

Russia fought back by imposing an embargo on food imports from the European Union and the United States. What's more, Putin further placed blame on Western nations for Russia's economic problems — even those that weren't directly related to the conflict.

In such a tense atmosphere, it's questionable that the usual diplomatic relations might help to bring peace to eastern Ukraine any time soon.

The crisis has also confirmed that Putin wants to expand Russia's influence by reestablishing ties with its former Soviet states. The sovereign nations that border Russia have good reason to fear this.

Putin has said that the collapse of the Soviet Union was "the greatest geopolitical catastrophe of the twentieth century." He wants to reassert an alliance between Russia and nations that broke away from Soviet influence. He has even compared his aim as similar to German reunification in 1990.

Considering the weakness of the EU's military forces and the bloc's inability to defend Ukrainian sovereignty, it's easy to imagine that similar crises could play out in other states in Russia's perceived sphere of influence.

Political circumstances are different in every country, but countries such as Moldova, Georgia, the Baltic states, Belarus, Armenia or Azerbaijan could become hotbeds of conflict, too.

All in all, how much the Ukrainian crisis will influence global politics remains to be seen.

### 9. Final summary 

The key message in this book:

**The Ukrainian crisis has shown the world that a new Cold War may be upon us. Russian President Vladimir Putin has proven that he's willing to take drastic measures to reassert Russian influence in the region, and in return, the European Union has struggled with its response.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Man Without A Face_** **by Masha Gessen**

A biography of Russian President Vladimir Putin, _The Man Without A Face_ shines a clear light on one of contemporary history's more shadowy political figures. The book charts Putin's almost accidental rise to Russia's highest office, starting from his benign beginnings in the state secret police. His vindictive personality, overwhelming greed and disdain for democratic norms continue to transform Russia today.
---

### Andrew Wilson

Andrew Wilson is an expert on post-Soviet Ukraine and Eastern European history. He is the professor of Ukrainian studies at the University College of London and a senior policy fellow at the European Council.

