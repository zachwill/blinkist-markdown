---
id: 5e95af9e6cee0700060a0da4
slug: un-trumping-america-en
published_date: 2020-04-17T00:00:00.000+00:00
author: Dan Pfeiffer
title: Un-Trumping America
subtitle: A Plan to Make America a Democracy Again
main_color: 239CB1
text_color: 0F5C69
---

# Un-Trumping America

_A Plan to Make America a Democracy Again_

**Dan Pfeiffer**

_Un-Trumping American_ (2020) is former Obama advisor Dan Pfeiffer's playbook for beating Donald Trump and fixing the broken system of American democracy. Filled with practical campaigning and policy suggestions, it's an ambitious guide to a brighter future.

---
### 1. What’s in it for me? Discover one Democrat’s plan for how to fix a broken political system. 

Donald Trump has been a unique president in many ways, and that's putting it kindly. Democrats have watched aghast as he has torn up the political rulebook and shamelessly acted as if he were above the law, while facing no repercussions. Sometimes it's felt like a waiting game, where people hang on to the belief that eventually some terrible scandal will emerge that even Trump couldn't get away with. 

But that hasn't happened. And this is largely due to the Republicans in Congress.

What this means is that it's time for the Democrats to wake up. It's not just the keys to the White House that are at stake — it's the future of the country. With Trump in power and Trumpism firmly enshrined in the Republican party, this is no longer just a party political issue. It's only the Democrats that can un-Trump America, undo the damage that's been done, and make sure this can never happen again.

Dan Pfeiffer, a prominent advisor to Barack Obama during his presidency, has a plan for how to make that happen.

In these blinks, you'll find out

  * the different games that the Republicans and Democrats are playing;

  * why Democrats should ignore Trump's weaknesses and focus on his strengths; and

  * what should be at the top of the next Democratic president's agenda.

### 2. Trump may be unique, but Trumpism is a bigger, more dangerous threat. 

Back in 2016 — before the critical US election, before Trump's inauguration, before the white supremacist rally in Charlottesville, before the Muslim travel ban, ...oh, you get the idea — the author Dan Pfeiffer found himself at the Republican National Convention in Cleveland.

He wasn't there for fun, you understand. A mainstay of the Democratic party for some 20 years who worked closely with Barack Obama, Pfeiffer was there as a CNN analyst. As a Trump supporter took to the stage, Pfeiffer leaned over to a friendly Republican in the Never Trump camp. He must be looking forward to after November, Pfeiffer said sympathetically, when Trump had lost and all his crazy followers had left the Republican party.

The Never Trumper responded, "Are you kidding me?" These people, the Republican believed, were here to stay.

As Pfeiffer soon realized, he was right.

**The key message here is: Trump may be unique, but Trumpism is a bigger, more dangerous threat.**

Now, you may well be wondering if "Trumpism" is really a coherent set of values. True, "the Donald" is hardly a philosopher — rather, he seems to have a remarkable and terrifying absence of any particular beliefs at all.

But there are some key strands that add up to something like a list of political principles… if you can call them that. For instance: lie. Promote racial divides. Hate on the press. Use propaganda. Win at all costs.

Trump, as it happens, is particularly good at these things. And in doing them, he's simply tapped into the very worst instincts of a lot of other people in the Republican party.

You could even say that he's settled a long-running Republican party debate. The last decade of Republican politics has been defined by a civil war. On the one side were wealthy establishment figures keen on Reagan and the Bushes, and on the other were those who hated Obama, listened to commentators like Rush Limbaugh, and generally wanted to disrupt business as usual — despite having very little practical experience in politics. These people used to be called the Tea Party, and are now the Freedom Caucus.

Though it's strange to use these words, Trump has healed this divide. He has kept the focus on race and the anti-establishment energy of the Tea Party, while making policies — like his tax agenda — that establishment figures love. The result is something with such broad appeal among Republicans that it's bound to continue long after Trump has gone.

Which means that Democrats need to up their game.

> "In this union lies the core of Trumpism — billionaire-funded racial grievance politics."

### 3. Democrats are more principled than Republicans, which means they’re also less ruthless. 

It's kind of like the Republicans and the Democrats are playing different games. For Democrats, the author suggests, political power is something to be used to make the country better. But for Republicans, the aim is power itself. It's as simple as that.

**The key message here is: Democrats are more principled than Republicans, which means they're also less ruthless.**

Remember after the 2016 election, when the intelligence services proved that Russia had interfered? Dealing with a huge crisis like that required Congress to put politics to one side for a moment, and produce a bipartisan response. But Republican Senate Majority Leader Mitch McConnell refused to sign a joint letter. Why? Because it would hurt Republicans.

Way back at the start of Obama's presidency, during the 2008 financial crisis, McConnell also declined to work with Obama on an economic recovery package. This was a disaster for the US economy — but McConnell's objective was achieved: he successfully prevented a Democratic boost in the polls.

The Democratic approach couldn't really contrast more, the author claims. Even during his presidential campaign in 2008, Obama placed good policy at the top of the agenda. Some of the ideas he promoted were politically dangerous — like the Affordable Care Act — but these were things actually worth risking the election for.

In that particular case, the gamble paid off. But Democrats' general tendency to play by the rules and prioritize principles gives unscrupulous Republicans like Mitch McConnell a huge advantage.

And what do they do with this advantage? They consolidate power, such as by suppressing the vote. Making it compulsory for voters to show ID sounds innocuous, but it just so happens that African Americans, who are often Democratic voters, are far less likely to _have_ ID than white people. The Republicans gerrymander, too — they redraw electoral districts, fragmenting Democratic-voting areas so that they count for less.

Republicans also work hard to shape the courts — both by getting a conservative majority onto the Supreme Court, and by appointing members of the sinister Federalist Society to the bench. The Federalist Society, once funded by the notorious Koch brothers, has long been a training ground for right-wing lawyers. Under Trump, it's essentially this society's job to choose new judges.

Bleak and terrifying? Yes, and that's why Democrats need to step up. In the next blink, we'll look at some of the best tactics they could employ in the 2020 presidential election. And after that, we'll consider how a Democratic president can try to change the system so that Republicans no longer get to place their thumb on the scales.

### 4. Despite the complexity of the task, Democrats need to come together and form a radical 2020 election strategy. 

It's tempting to think of the "Democratic Party" as a single entity, with a central office that issues out orders for everyone to follow. But it's not like that at all.

The Democratic National Committee is really just an umbrella organization for the state-level parties and elected officials. There are countless other Democratic organizations: campaign committees for the Senate, the House, governors, and so on; there are labor unions; think tanks; grassroots organizations; super PACs… the list goes on.

Sounds like a mess? It is. But somehow, all of these parts need to work together, and focus on the common goal of defeating Trump. It'll be tough, but it couldn't be more important.

**The key message here is: Despite the complexity of the task, Democrats need to come together and form a radical 2020 election strategy.**

Step one might sound strange, but Democrats actually need to stop chasing Trump over his weaknesses.

The thing is, people already know about them. Time and again, unbelievably damning evidence has emerged about Trump — from the "grab them by the pussy" tape to the Mueller report — and he never loses much support.

It's far smarter, then, to go after Trump's supposed strengths — areas like immigration and trade. People believe he's doing a good job in these areas, and that he's therefore worthy of their support, regardless of his other failings.

But his records here aren't exactly untarnished. For instance, despite Trump claiming to be tough on immigration, his businesses have used undocumented laborers. And, while he ramps up the trade war with China, Trump-brand goods continue to be manufactured there. Then there's the economy. Democrats need to be crystal clear that Trump's tax cut was specifically designed for the rich, and has hurt everyone else.

All of that said, one thing Democrats _shouldn't_ do is turn the election into a referendum on Trump. Rather than just asking voters to evaluate his record, Democrats must offer a genuine, positive alternative. The Democratic candidate needs to be a _good_ candidate, not just the lesser of two evils.

All of this needs to be done through a media strategy that is properly attuned to the new world we live in — with a focus on social media and far less emphasis on traditional communication methods like newspapers.

Winning the election, complex and hard though it will be, is only the first step. In the next few blinks, we'll explore what a Democratic president needs to do to make sure that the Republicans don't do even more damage to the country than they have already.

### 5. Once in power, the Democrats need to make the whole political system much fairer. 

Dan Pfeiffer has met a lot of politicians over the years, but even he was blown away when he met Stacey Abrams. The Democratic candidate for Georgia governor in 2018, she is an incredibly charismatic civil rights activist, former business owner, and occasional romance novelist. An ideal candidate.

So why did she lose the election? Well, it didn't help that her opponent, Brian Kemp, was Georgia's Secretary of State, in charge of state elections. And he, as it happened, had recently removed tens of thousands of names from the voter rolls — mostly African Americans. That made it harder for them to vote — which gave the Republicans a big advantage.

That sort of underhanded tactic — not such a rarity in Republican politics — cannot be allowed to continue.

**The key message here is: Once in power, the Democrats need to make the whole political system much fairer.**

Republicans are trying to suppress the vote everywhere. Democrats need to do the opposite — aggressively. Not just because unregistered voters are more likely to become Democrats — also simply because getting more people to vote is morally right and better for democracy.

There are so many ways this can be done, from automatic voter registration to expanding early voting. But Democrats have been reluctant to push this issue, afraid to look too partisan. That's exactly the kind of thinking they need to shake off. In fact, they should push things even further, and consider lowering the voting age to 16, or even making voting mandatory, like in Australia.

The next Democrat-led administration also needs to look seriously at other problematic systems. First up: the Senate. Not just because of its ridiculous loopholes like filibustering — something else that the author wants to can — but also because the two-senators-per-state system is essentially undemocratic, especially as young people move toward big cities in blue states. The University of Virginia calculated that 70 percent of the US population will live in just 16 states by 2040. That hands just 30 percent of the population control of 68 of the Senate's 100 seats.

Here's another idea: make more states. Sound crazy? There's one place that absolutely merits it: Washington, DC. There's also Puerto Rico, whose currently disadvantaged residents should be able to choose between full independence or full statehood.

Last but not least, Democrats in government should scrap the electoral college. This antiquated system has seen the candidate with fewer overall votes win the presidency 40 percent of the time since 2000. Even Trump agrees it's crazy… according to several tweets he sent in 2012.

### 6. Democrats need to reform fundraising, and also help the unions. 

The 2012 presidential campaign was an unusual one. Barack Obama became the first incumbent in recent history to spend less on campaigning than his opponent. The reason? A 2010 Supreme Court decision, _Citizens United_, took the cap off campaign contributions. Republicans could then fully leverage their much wealthier supporter base.

It's another example of how the Republicans have an unfair advantage. Democrats shouldn't be shy about eliminating that advantage once they're back in power.

**The key message here is: Democrats need to reform fundraising, and also help the unions.**

_Citizens United_ was a boost for Mitt Romney's unsuccessful campaign in 2012, but it's especially turbo-charged Trump's campaign. Essentially, you can now buy political power — there's nothing to stop super PACs from spending huge sums on whatever campaign they want. That's a problem for democracy in general, because it tips the balance in favor of the wealthy. But it's an absolute catastrophe for Democrats, whose supporters include fewer billionaires.

What can they do about it? One step would be to create a new nationwide system for funding Democratic campaigns — something you could even call a liberal version of what the infamous Republican Koch brothers have established. Pfeiffer has raised a lot of eyebrows by suggesting this, since liberals don't like the idea of emulating that sinister organization. But perhaps it's the only effective way to compete.

Also, a Democratic administration needs to give the Federal Election Commission some real power. Created by a wary Congress after Watergate, the FEC doesn't have anywhere near enough authority to truly stand up to bad electoral behavior. With more resources and authority, that could change.

Another step to take is passing an act that was filibustered in Congress during Obama's tenure. The DISCLOSE Act was designed after _Citizens United_ to force organizations to disclose what money they were raising and spending on campaigns. A filibuster-free Congress would pass it in a moment.

Pfeiffer thinks that step one for a new Democratic administration, though, should be something else. The Employee Free Choice Act, a.k.a. Card Check, should be top of the list — an Obama policy he didn't get around to implementing. It would make things a lot easier for unions, by forcing employers to recognize them and making it easier to get to arbitration. Additionally, Democrats should help workers by taking on the gig economy.

Protecting workers' rights is both fair in general, and advantageous to the Democrats. Republicans do whatever it takes to look after their own supporter base. Democrats should do that too.

### 7. Court reform is a particularly important move for the Democrats. 

Of everything that Trump has done during his time in the Oval Office, the thing that may well prove hardest to undo is his success in getting two Supreme Court nominees, Neil Gorsuch and Brett Kavanaugh, onto the bench.

The effects of that will be felt for decades. The age difference between Kavanaugh and the eldest justice, the liberal Ruth Bader Ginsberg, is 32 years. And appointments to the Supreme Court last for life.

**The key message here is: Court reform is a particularly important move for the Democrats.**

Getting involved with the Supreme Court might sound controversial — it's meant to be completely independent from party politics, after all. But the political nature of this court is an open secret. According to one poll in 2019, only 38 percent of people consider the Supreme Court to be primarily motivated by the law. So what can be done to fix it?

Simply put, the Democrats could add two more seats to the Supreme Court. There's no particular reason why the number has to stay fixed at nine: there have been other numbers in the past. The author considers this a frankly necessary move given that one of the seats was effectively stolen from Obama. After Justice Antonin Scalia died in 2016, Mitch McConnell declined to consider Obama's nomination for a replacement — paving the way for Gorsuch's appointment after Trump took office. That's a move, the author says, that deserves retaliation.

It's also worth considering limits on these justices' terms. The current system of lifetime appointments is messy and unpopular — with only 17 percent popular support, according to a 2015 poll. A majority of Americans, though, support having a term limit of ten years. Even the arch-Republican Ted Cruz has said he'd consider imposing limits.

This really isn't just about getting the Democrats a majority. There are hugely important principles at stake that a solidly right-wing Supreme Court could damage. _Roe v. Wade_, the decision protecting a woman's right to choose an abortion, is of course in danger. And there are other Supreme Court decisions that simply need to be overturned, like _Citizens United_, as well as their oddly permissive views on gerrymandering.

The author advises the Democrats to put together a plan for court reform, then, and campaign for it — strongly.

### 8. The Democrats need to limit the power of the presidency. 

It's not quite true to say that the US political system can't cope with a corrupt president like Trump. In fact, that's exactly what impeachment is for. What the system really can't cope with is a whole _party_ that's corrupt enough to leave someone like Trump in office.

Nevertheless, the last few years have made it painfully clear that the power of the presidential office is too great. So when the Democrats regain control, they need to do something about it.

**The key message here is: The Democrats need to limit the power of the presidency.**

That's right — the next Democratic president needs to limit their own power. Working together with Congress, there are various steps that can be taken fairly easily. 

For starters, Authorization for Use of Military Force resolution, created after 9/11, needs to be repealed. It's been used for drone strikes in Libya and Somalia, and for the continuing presence of troops in Afghanistan. Getting rid of it would mean that future presidents would face greater scrutiny before using military force.

The National Emergencies Act also needs to go. This 1970s act gives the president extra powers in times of crisis, but lately it has not been well used. Believe it or not, American citizens are currently living in 30 simultaneous states of emergency. Maybe the most egregious example was Trump's use of it to fund his border wall.

One more: the Hatch Act should be strengthened. According to this law, federal government officials being paid public money must not engage in party politics. Trump's advisor Kellyanne Conway has spent years violating this law, the author says — but all the Hatch Act could offer was a letter to Trump, recommending that he fire her. Obviously, he didn't. This law needs more teeth.

Several longstanding conventions should also be turned into laws, so that another Trump can't come along and ignore them. It should be made obligatory for presidential candidates to release their tax returns. Plus, presidents should be required to divest from business interests. Before Trump, they all did it voluntarily. But then, well, he didn't.

Finally, sitting presidents cannot currently be indicted. But given Trump's record, that's a rule that should change too.

Limiting the president's power is the morally right thing to do, says the author, given what we've seen recently. But it would also be a popular move with the public. Perhaps it would even restore a little bit of faith in politics.

### 9. If you think Trump is bad, imagine a competent version. That’s what we should be scared of. 

Sure, Donald Trump is an authoritarian, says Pfeiffer. But he's also… well, not that good at his job. His inclinations may be dictatorial, but he's easily distracted and unaware of how things actually work.

Here's a scary thought. Imagine a president as authoritarian as Trump… who knows how to get things done.

**The key message here is: If you think Trump is bad, imagine a competent version.** ** _That's_** **what we should be scared of.**

There's a very real chance that Trump could just be the warm-up act.

That's why, even though this advice might sound party-political in nature, it's actually a lot more important than that. With the Republicans appearing hell-bent on securing and consolidating power at all costs, it really is down to the Democrats to make sure they don't do irreparable damage to the country's already dysfunctional systems.

It's laudable that Democrats are generally so concerned with getting policy right and acting respectfully. But the recent actions of the Republican party — not just Trump, but McConnell as well — make it clear that this is not the time to play it safe. The Republicans have used dirty tactics to wrest control of elections, the Senate, the courts, and so much else. They need to be stopped, and the system needs to change so they can never do this again.

Pfeiffer remembers a moment in 2011 when things were looking bleak for Obama and his team. A year before his reelection, the economy was hurting and the president's popularity was way down. They really seemed to be heading for defeat next year.

So Obama called a team meeting over a weekend, and gave an inspiring speech. Things weren't going to get better quickly, he said — they faced a tough battle. But the stakes were even higher now than they had been in 2008.

What should they do? Obama's advice was to stop being cautious. He wanted to see big, substantive differences between their vision for the party, and the Republicans'. And he wanted to stay true to that vision.

"If I go down," he said, "I'm not going down like a punk."

Well, guess what, the stakes are high once again. There is everything to play for.

And, once again, that's not a reason to play it safe. Quite the opposite: it's a reason to be visionary. It's time to take some risks, and make it clear just how much better the USA can be.

### 10. Final summary 

The key message in these blinks:

**Despite everything that Donald Trump has done during his time in the White House, he's not the biggest threat to American democracy. Dan Pfeiffer believes that the Republican party as a whole — now thoroughly shot through with "Trumpism" — is a much greater and longer-term threat. The Democratic party needs to respond to this threat with fresh energy and ambitious, visionary plans.**

Actionable advice: 

**Get involved in politics.**

It's tempting to feel disillusioned by politics — or that you're too insignificant to make a difference. Well, now is frankly not the time to feel like that. There are all sorts of steps you can do to support change in the political process, from proudly expressing your political affiliations online to donating money. Don't forget — the presidency isn't the only show in town. So much politics happens on a smaller scale than that: there's always something to get involved with. Could you even run for office yourself?

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Yes We (Still) Can_** **, by Dan Pfeiffer**

_Un-Trumping America_ is Dan Pfeiffer's plan of action for the future, as the USA wrestles with Trump and his fellow Republicans' legacy. The reason Pfeiffer knows what he's talking about is that he himself spent years working in the White House, as one of Barack Obama's advisors.

In _Yes We (Still) Can_ (2018), Pfeiffer looks back instead of forward. He shares some of what he learned while working as communications director for the president, offering a unique insider's window into what this high-pressure job really entails.
---

### Dan Pfeiffer

Dan Pfeiffer was White House director of communications in 2009–13, and senior advisor to Barack Obama in 2013–15. He has since written the _New York Times_ bestseller _Yes We (Still) Can_, and also co-hosts the podcast _Pod Save America_.

