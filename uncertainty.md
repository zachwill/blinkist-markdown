---
id: 545ca7b16331390008190000
slug: uncertainty-en
published_date: 2014-11-11T19:00:00.000+00:00
author: Jonathan Fields
title: Uncertainty
subtitle: Turning Fear and Doubt into Fuel for Brilliance
main_color: 4CB2E3
text_color: 2E97C9
---

# Uncertainty

_Turning Fear and Doubt into Fuel for Brilliance_

**Jonathan Fields**

_Uncertainty_ (2011) offers us the chance to tackle our fear of failure and the unknown, and discover the steps we can take to be focused, productive, confident and successful.

---
### 1. What’s in it for me? Find out how to deal with fear of failure and uncertainty. 

In today's world, the risk of failure is all around us and uncertainty abounds. Even when we have enticing career opportunities, we are often too afraid to take them because we don't want to risk losing a stable income.

However, some people refuse to accept this and learn how to deal with uncertainty, and there are many techniques to tackle it. When we master uncertainty, we can expand our mental capabilities and have the energy to fully commit to new ideas and inventions. In these blinks you will learn what uncertainty does to us, why we are so scared of it, and find out about the tools you need to deal with it.

You'll also find out

  * that there are no fearless people;

  * why people stick to the same routine every single day;

  * why sport is not just about physical fitness; and

  * how you can fool your own brain.

### 2. Risk is always connected to anxiety, fear and criticism. 

Think about the last time you had to make a decision which involved taking a gamble. How did you feel? Nervous and anxious, right?

Risk is always associated with the possibility of _loss_, which is what makes us so scared. When we create something, we need to give it some kind of input, such as effort or money. But if we fail, we run the risk of losing some or even all of this input.

If the possibility of loss — of money or our reputation, for example — is too high, we shy away from investing in new ideas and shut off our options in order to maintain the security and stability that already exists in our lives.

It follows, then, that those of us who have the most to lose, maybe because we have a mortgage to pay or a family to look after, are often the ones who are most wary of taking risks.

But why else do we curtail risk? Well, taking risks and thinking outside the box sometimes invites judgment, and we hate being criticized for our actions.

When we try something new, others are likely to observe us and judge us if we go against norms and traditions. This is often because others believe that following an unknown path with an unknown consequence could leave us unnecessarily exposed. This judgment may come from the people who support us, our investors or even from ourselves.

But judgment and fear of criticism can be toxic to our confidence and inhibit our creativity.

Take one experiment from Harvard, which found that painters are less creative when working on commission. This is because they are forced to deliver something that matches the purchaser's expectations rather than experiment with new things.

The reality is that uncertainty makes us uncomfortable because we don't want to fail in front of others. But you can take action to fight this fear.

### 3. Only when we tolerate risk and uncertainty can we create new and innovative things. 

We have just seen that a fear of risk and failure often prevents us from following through with what we want to do. But does it even matter? If you have a nice stable life, why should you take risks anyway?

Because playing it safe can't offer you the same advantages that taking a gamble does! Anything that has a predictable outcome has probably already been done by someone else. So where's the fun in that?

Our own experience and the experiences of others lend us certainty about different subjects. The trouble is, those experiences are just approaches that have already been tested and proven. They're not innovative. Innovation is found through _un_ certainty.

Say you want to write a song but you only listen to Mozart. You could copy him and compose beautiful music, but the thought of creating a totally new musical style like Bill Haley's rock 'n' roll would not occur to you.

It's the eagerness to take on the unknown that aids innovation and creativity.

In one study, psychologist Franck Zenasni discovered that people who were willing to accept uncertainty were significantly and positively more creative. They didn't mind a degree of ambiguity, which meant that they were prepared to press on if the solution already in place wasn't perfect.

They were more likely to work well on complex problems, were open to more stimuli and were unsatisfied with anything other than the best possible solution.

Google is acutely conscious of this relationship, so they allow employees to invest 20 percent of their work time in new ideas and projects. Staff have nothing to lose because they don't have to report their results. They therefore take risks, acknowledge uncertainty and improve their ability to solve problems, which nurtures their creativity in their own projects and their day-to-day work.

> _"Anything certain has already been done."_

### 4. The right mindset + a willingness to take risks = the creation of things that matter. 

We've all heard about innovators who continue to invest in new ideas and don't seem at all phased at the prospect of losing everything. How is it that they can still motivate people to support them, even when there is a slim chance of the gamble paying off?

Uncertainty leads to novelty and innovation. When we push the envelope, it shows that we're not satisfied with the status quo and that we want things to improve. So if someone is ready to take a risk, it means that they are committed to trying something out.

Moreover, staking money and reputation is only worth it if you create something of value for others and not just yourself. If others can observe your commitment and see the possibility of reward, you are more likely to persuade them to support you and your project.

Crowdfunding is a good example of how great ideas can be realized when they demonstrate innovation and meaning to others.

Teenager Boyan Slat was successful in enlisting this technique. He developed a way to clean up plastic from the ocean, and the enthusiasm he had for his idea managed to generate $2.1 million from supporters.

So how can we enter a mind-set where we can commit to uncertainty? The trick is to come face-to-face with our fears. Only when we conquer these fears can we focus on our targets and increase our motivation.

Author of _War_ and ___The_ _Perfect Storm_, Sebastian Junger, consistently overcomes his fear of failure by using it to his benefit. When the thought of failure creeps into his mind, he harnesses the energy created and uses it as a motivating force to drive his creativity.

You can learn how to do this in the following blinks.

> _"When you run from uncertainty, you end up running from life."_

### 5. Routines and rituals create certainty even in unstable times. 

What do you do when a deadline is approaching? You probably work day and night and push friends, sports and hobbies to the side in order to finish the task at hand. You probably think this helps you get the job done, but giving up these things actually hurts you. Why?

Rituals and routines build consistency and act as _certainty anchors_. A certainty anchor, such as going to the gym or having coffee with friends, is a stable part of your life which gives you something to rely on. These routines calm us and release us from unimportant decisions, allowing us to focus on our main task.

For example, choreographer Twyla Tharp has a precise, unchanging daily routine. She gets up at 5:30 a.m. each day, goes to the gym, makes calls for an hour and then heads to her studio to work for a couple more hours. Completing these rituals every day better enables her to use her mental skills to solve problems that require more creativity.

Rituals also help us accomplish and improve at tasks which don't relate to our _creative orientation_.

We tend to either be oriented toward the "big idea" (like inventors) or toward the process (like those who produce or build the invention).

But what if you're a solo entrepreneur, like a writer? Then you have to wear both hats; you're the idea person _and_ the process person.

Since it's challenging to be good at both, you can use rituals to help you focus on the area that you're lacking in. For example, if you're a big ideas person who hates dealing with details like administration, set aside a couple of hours per day to work on these tasks. The daily routine will ensure that you'll get through it.

### 6. Even though criticism and feedback can create discomfort, they are essential when turning uncertainty into certainty. 

You might not always welcome what other people have to say, but you can benefit from their opinions.

The judgment we receive from others is a form of feedback, and this is essential to the creative process, as it teaches us how we can improve ourselves or our product.

Say you're developing a new soda. The easiest way to see if people like it is to get them to taste it and then ask them what they think.

There are two useful ways we can get this feedback. The first is by utilizing a _creative hive_ — a community of like-minded professionals who provide criticism and support while presenting their work to one another at regular intervals. The TechStars community, which enables entrepreneurs in technology to discuss and refine their ideas, is one example.

The second way to obtain feedback is through a relationship. This can be with a role model who provides inspiration, a romantic partner who offers confidence and trust, or a colleague or friend who gives you insight and experience.

But why is feedback so important? Well, without it, we experience uncertainty.

Take, for instance, a product that is released without customer input: it's likely to fail miserably when it enters the market.

However, regular and consistent feedback from potential customers during the development process will enable better results and diminish risk.

So, if you are the manufacturer of the soda and you decide to go through the entire process without requesting any feedback, you run the risk of it tasting awful after you've poured millions of dollars and a great deal of time into creating and marketing it.

One final thing: when requesting feedback, remember that you are in control of your own project. Other people's opinions can guide you, but ultimately you know what's best. Even if feedback means admitting failure, it's better to recognize this at the beginning of product development rather than at the end!

> _"Judgment is often experienced as pain, but it's really just feedback data."_

### 7. Mental and physical exercises improve our brain functions and help us stay focused. 

After learning about the world of uncertainty, let's look at how we can thrive there. How can we shape our thoughts in order to enhance our focus and creativity?

One technique you can use is _Attentional Training_ (AT), which builds creativity and psychological power. AT involves different methods, but all are linked to strengthening focus and attention, which can help you in both your personal and professional life.

AT can entail physical exercise like running or biking, or mental exercises such as Zen meditation, hypnosis or visualizing your working process. For example, students who picture themselves studying in the library will begin working earlier than students who don't.

These practices are effective because, much like certainty anchors, they put us at ease and cultivate our mental capacities. This change in our physiology and psychology in turn improves the way we handle challenges.

Regular practice of these techniques works wonders for your mental capacity and enables you to get a better handle on uncertainty and fear. In addition, during the exercises you will find your thoughts wandering, which is often when the best big ideas rise to the surface.

Of course, it's not just mental techniques that can help our brains. The benefits of physical exercise extend beyond the body, as it also improves our mood, our ability to think and solve problems, and helps us become more comfortable with uncertainty.

One study at Leeds Metropolitan University showed that on the day employees exercise, their mood was enhanced and their mental, interpersonal and work performance improved.

One example of adding some physical exercise to your day is to take a short run in the morning. This will help you start the day with more energy and an emotional lift. You'll probably find you can then start your workday feeling more focused, and achieve better results.

It's worth taking the time to try these things out, as you'll likely experience the results faster than you think.

> _"If you feel organically unable to survive the angst of creation long enough to bring genius to life, we're about to hack the system for you."_

### 8. Staying realistic and assessing the circumstances will help you maintain a good overview. 

Still not sure if what you're doing is the right thing? Still scared of failing? Ask yourself if you're working productively and how your motivation is affecting your work.

You can do this by _zooming out_, which means looking at your circumstances objectively to help you determine if it's worth continuing your work.

Imagine you just started a new job and you're stressed out and thinking of quitting. Ask yourself the following questions: Why do I feel insecure? Am I afraid of failing because I have to go beyond my comfort zone? Is the job just not for me? Would going beyond your comfort zone mean developing in the wrong direction?

The answers to these questions should give you the clarity to understand what you really want.

It's also important to notice whether your work is starting to consume your life. Sometimes the creative process can use a lot of energy and put a strain on our bodies, minds and relationships.

To stop this from happening, find a work rhythm that suits your needs. Ask yourself what the most productive time for you is, and recognize when you need a time-out. Also be sure to leave time for your family, friends and health.

If you find that you're chronically unhappy with your life, _reframe_ your situation. This means looking at the facts and changing the story you tell yourself about your fear.

This is known as _cognitive reappraisal_, where we alter the context of what we fear and train our emotions, thereby controlling our anxieties and worries.

For example, creative hives can reframe participants' uncertainty by stating something like, "We're in this together and can help one another." By reminding the group of this, everybody understands that fear is normal and it will start to loosen its grip.

Assessing our situation objectively can help combat insecurity. Now let's take a look at how we can gain more control over the often paralyzing fear of failure.

### 9. Handling your fear of failure is nothing more than a mental exercise. 

We all know how crushing the fear of failure feels, so here's how to do something about it. To break down our uncertainties, we have to develop a _growth mind-set._

Most of us have a _fixed mind-set_, where we believe talent is settled by genetics, and so have a hard time taking on feedback. We give up easily because we think it's no use, nothing will change.

A _growth mind-set_ is the opposite: this is the state of mind where we believe in our work and view feedback and uncertainty as ways to improve. Those who have this mind-set are more flexible in finding solutions, accept risks and don't quit.

For instance, we know that students with a fixed mind-set will give up on a test with unfamiliar exercises and students with a growth mind-set will try everything until they solve the problem.

A growth mind-set helps us cope with uncertainties. But what about the fear of failure? Well, this can be managed with a simple exercise.

The thought of losing everything can be paralyzing, because we often imagine that recovery will be impossible. As a result, we become overprotective and block off risky but potentially advantageous opportunities. The three following questions can help you banish your fear:

What if I fail, then recover? Sure, failing is painful, but concentrate on how you would recover if you did fail.

What if I do nothing? If you're unhappy now, you will stay that way and also run the risk of losing your last bit of fire, or even becoming depressed.

What if I succeed? Think of your ideal outcome and imagine how it will feel. Dwell on it, add details and come up with action steps you can take toward achieving it.

Practicing these scenarios will reduce anxiety and allow you to be realistic. Failure is still a possibility, but staying positive and realistic will let you work on your goals without feeling so distressed.

### 10. Final summary 

The key message in this book:

**Uncertainty is something that we all fear, and it's especially important for creatives and professionals to learn how to deal with it. By practising some specific techniques and receiving regular feedback, we can learn how to face our anxieties and cope with uncertainty.**

Actionable advice: 

**When you have a deadline in sight, stick to your routines.**

Dropping everything in a panic and pulling an all-nighter (or three) will make you anxious and cloud your focus. Instead, when a deadline is approaching, maintain your social and exercise routines to ensure you stay as calm and focused as possible.

**Suggested** **further** **reading: _The Art Of Non-Conformity_ by Chris Guillebeau**

Based on the author's blog and online manifesto, "A Brief Guide to World Domination,"_The Art of Non-Conformity_ deals with ways of pursuing unconventional living and offers tools for setting your own rules, succeeding with your passions and leaving a legacy.
---

### Jonathan Fields

Jonathan Fields is an entrepreneur, author and business strategist. He is the author of _Career Renegade_ and has been featured in the _New York Times_, _Businessweek_ and the _Wall Street Journal_. Field blogs on his site JonathanFields.com and for GoodLifeProject.com, covering entrepreneurship and lifestyle.

