---
id: 55ed285511ad1a000900002c
slug: unconscious-branding-en
published_date: 2015-09-08T00:00:00.000+00:00
author: Douglas van Praet
title: Unconscious Branding
subtitle: How Neuroscience Can Empower (and Inspire) Marketing
main_color: 937362
text_color: 935736
---

# Unconscious Branding

_How Neuroscience Can Empower (and Inspire) Marketing_

**Douglas van Praet**

_Unconscious Branding_ (2012) reveals how marketers can tap into our subconscious, encouraging our participation in and support of company brands. In just seven steps, you'll discover new strategies to guide your own company toward developing a brand with which customers can build a genuine relationship.

---
### 1. What’s in it for me? Learn how to create a brand, not just a product. 

Maybe you're already a great entrepreneur. You're always first with a new idea, and you've created a truly great product. But then you have to market that product. What to do? 

Marketing just isn't as simple as you might think. The marketplace is packed with products, and newer and greater ideas are introduced every single day. If you don't get seriously creative, your product will lose its way and fail to grab the attention of the audience you want. 

These blinks provide you with a new way of looking at product marketing. Based on a deep understanding of how the brain works, this seven-step process will supercharge your marketing skills and help you to build a successful brand in today's highly competitive marketplace.

In these blinks, you'll discover

  * how your choice of mustard reflects your desire for social status; 

  * why a "cute" Darth Vader can attract more attention than an "evil" one; and 

  * how gum can be effectively marketed as a healthy product.

### 2. Remember that your prospect is a human, too. How can group mentality help you as a marketer? 

Have you ever been baffled by the array of choices on a supermarket shelf? How did you come to a decision? You might think you just grabbed the product with the most attractive label. But what you were actually doing was making a decision using _heuristics._

We use heuristics, simplified methodologies for finding a sufficient solution, when finding the perfect solution is either impossible or impractical. 

One of these methodologies is _social proof_. This means when you're in doubt of what to do, you check what other people are doing and then do the same. 

Buying the brand of a market leader is one way to use heuristics. Another example is the idea that "expensive equals good," which reflects our readiness to believe that the more expensive a product is, the better the quality. This is based on the adage, "you get what you pay for." 

But heuristics isn't the only thing that influences how people make decisions. Thus a marketer shouldn't only employ such methodologies when constructing a campaign. 

There are also certain evolutionary tics that a marketer can tap into to influence a potential customer. One is our innate need for the safety that a group provides.

Think about the lives of our Stone Age ancestors. If an individual was banished from a group, it was essentially a death sentence. The threat of predators, not to mention the challenge of finding sufficient food, meant a single person wouldn't survive long.

In today's modern world, we're not terribly worried about a sabertooth tiger attack when walking home alone. Yet people still exhibit a _group mentality_, seeking like-minded individuals attracted by a certain brand. 

Imagine a group of Harley Davidson motorcycle owners, out for a Sunday ride en masse, all wearing the same leather outfits — a perfect example of group mentality.

To create a good campaign, however, you need to see prospects not just as consumers but as humans. And don't forget that humans have brains that control their thoughts and actions. So let's learn more about how that brain actually works!

> _"When in doubt, do what everybody else is doing."_

### 3. Harnessing the brain’s physical, emotional and rational parts is key to an effective campaign. 

We know that the brain is divided into three "parts," a physical, an emotional and a rational part. And knowing this can help a marketer better conceive of campaigns to effectively motivate a potential customer.

The brain's physical part deals with our powerful, yet unconscious, human needs. Often called the "reptilian brain," this part deals with breathing, eating and mating. 

Marketers can harness this part of the brain by channeling any of the six "S" words in a marketing campaign: survival, safety, security, sustenance, sex and status. 

Created by the Lowe and Partner agency in the 1980s, an ad for Grey Poupon mustard had two British aristocrats specifically requesting the mustard in a posh setting. This spot not only focused on food as a _survival_ item, but also connected the particular food with increased _status_.

The brain's emotional part controls memories and feelings, allowing a person to make a connection with a certain brand. A marketer can tap into this emotional well to encourage people to buy a product. 

This particular part of the brain is comprised of the _amygdala_, the _hippocampus_ and the _hypothalamus._ When examining a can of Coke, for example, this trio works in this fashion: the hippocampus recognizes the Coke logo; the amygdala tells you that you like to drink Coke; and the hypothalamus tells you that you're thirsty. Combined, you're likely to buy the can of Coke. 

Finally, the brain's rational part is responsible for higher cognitive functions, such as problem solving. We use this part when deciding to buy a certain product, perhaps by comparing prices. 

To stimulate this rational region, marketers can use figures, facts or other comparative information in brand campaigns. Touting price savings, for example, is a great way to harness the rational brain. 

So really, the human brain is predictable, and cycles through these three parts all day without us even knowing it. That is, unless something _interrupts_ this cycle.

### 4. Break a pattern and grab attention. An “Oh yeah!” moment inspires; an “Oh shit!” moment clarifies. 

So you're walking down a street you walk down every day, and suddenly, you see a car crash into a storefront just a few hundred yards ahead of you. Do you keep walking, or do you stop and stare? 

Of course, you stop. But how is such a reaction relevant to a marketer? 

A car crash is an unusual, out-of-the-ordinary event. In the same way, providing information that doesn't fit expectations allows a marketer to get the attention of a target consumer. 

The human brain follows patterns. Known patterns help us recognize objects and actions in our daily experience. Interrupt these patterns, however, and you grab attention; your brand stands out. 

The author's spot for Volkswagen is a great example of pattern interruption. As part of the television ad, a small child is dressed up like Darth Vader, a towering character in black usually associated with evil. As the child tries to use "the force" on his dog and some dishes, we laugh and think his actions are cute, rather than frightening. 

Another way to get people's attention and interrupt brain patterns is to create an "Oh yeah!" or "Oh shit!" moment. 

Such a moment is created in another Volkswagen ad, called "Safe happens." The viewer's point of view is from the backseat, riding along watching the driver and passenger having a normal conversation. Suddenly, the car is crashed into — the "Oh shit!" moment. 

Whether creating a positive "Oh yeah!" moment or a worrisome "Oh shit!" one, interrupting patterns is a good strategy to get the attention of a consumer. 

But while jolting a customer out of her comfort zone is one way to get attention, it's important as well to make a customer feel comfortable. So how do you balance the two?

### 5. Make your customers comfortable. Comfort builds trust, which in turn builds brand loyalty. 

Who do you trust most in your life? Your spouse, or perhaps your best friend? But what about the company that makes your flat-screen TV? Do you trust it?

Trust isn't just something you share with friends or family members. Consumers and brands also share trust relationships — or a lack thereof. 

In 2008, amid an industry meltdown, US automobile manufacturer General Motors was seeking a loan from the government to the tune of tens of billions of dollars.

During a government hearing, the company's CEO promised that GM would cut costs. Yet it was revealed that he flew to the hearing in a private jet, the trip costing some $20,000. A first-class ticket on a commercial airline, in contrast, would have cost $837. 

The public (whose tax dollars were to bail GM out) was outraged. GM executives had flouted the public's trust, and in doing so, not only lost money but also consumer confidence. 

In short, if your brand isn't trusted, people won't buy it — and your company won't last long. Customers will trust you when you give them the comfort they seek. Once you do so, people will not only choose your product, but also won't mind paying a bit more for it. 

A Columbia University study revealed that people who are relaxed are willing to pay about 15 percent more for a product than people who aren't relaxed. 

To simplify, people relax when they feel safe. If you can imbue your brand with a sense of safety, you'll encourage customers to relax and trust your product. And once they trust the product, they'll be willing to pay more, just for the confidence they feel when they choose your brand. 

So now that you've got your customer's attention and trust, what's next? You'll need to kindle her imagination. Read on to find out how a marketer does this.

### 6. Capture a customer’s imagination to encourage a brand connection. Leave key messages open. 

Great brands capture the consumer's imagination. So how can a marketer achieve this in a campaign?

In theory, it's quite simple. A great marketer leaves her key message _open_. When a customer has to use his imagination, he'll interpret a general marketing message into one that speaks directly to him. 

One of the most successful "open" campaigns is Nike's "Just do it." This simple yet powerful phrase helped Nike increase its market share over ten years from 18 to 43 percent. 

Instead of using a slogan such as, "Just get off your ass and go jogging," which, while being more direct, is considerably less inspirational, the company left "it" open to the audience's imagination. 

Nike even received letters from people who said the campaign had encouraged them to leave an abusive husband or rescue a person from a burning building.

The brain can't always distinguish between reality and imagination, as these two areas use the same neural circuitry. By encouraging a person to imagine using a product, a marketer helps him connect directly with the brand. The jump from imaginary to real is short — if someone imagines trying on a new pair of running shoes, he's already thinking of what those shoes would feel like for real, and thus will likely buy based on that connection. 

Apple's iPod campaign used images of people in silhouette, wearing an iPod. By showing only silhouettes and not real faces, a potential customer looking at the ad was able to imagine herself wearing the iPod. Making a personal connection helped inspire customers to realize their imagined scenario by purchasing the technology. 

Importantly, when you channel a customer's imagination, you trigger emotion. And this is exactly what you want a customer to be: emotional about your brand.

### 7. A great marketer doesn’t sell a product or a service, but an emotion. We feel, therefore we buy. 

Our behavior is nearly always driven by our emotions. Thus it is incredibly important that you consider your customer's emotions when developing brands and campaigns.

You could even say that a marketer doesn't sell a product or a service, but an emotion. 

Emotions guide what we do, including what we buy. If you purchase a product that doesn't make you feel good, then that bad feeling will linger, telling you to alter your behavior and choose something next time that will reverse that bad feeling — in short, something that will make you feel good. 

Marketers can take advantage of this by pushing a message that says a customer will feel good when he buys a certain product. 

Nike, for example, is more than the product it sells. When you buy a pair of Nike shoes, just the act of purchasing makes you feel more fit, given the strong connection between your sporting dreams and the company's brand message of successful, self-confident and athletic individuals. 

What many successful brands have in common is that they address people's emotions. Bulk retailer Costco turns shopping into a treasure hunt by placing popular items in curious locations and forcing shoppers to discover them on their own. Low prices aren't the only incentive to shop; the positive emotion of discovery keeps customers engaged. 

Apple has a very powerful brand. When you buy a Mac, for example, you're not just buying a computer but joining a club of like-minded people and a larger mission. Purchasing and owning a Mac affects you as a person and influences how others see you, too. 

This connection isn't simply about the product, but about what you, and the people around you, feel.

Positive emotions and connections are one way into a consumer's heart, yet a good marketer shouldn't ignore a customer's critical faculties. Read on to learn how to harness your customer's inner skeptic.

### 8. Critical minds take a lot of convincing. Use statistics and trusted voices to make your message stick. 

You'd have to be a serious fool to believe every marketing message out there. The human mind is by nature critical, and there are evolutionary reasons for this. 

As a species, we've evolved to avoid harm and loss, thus it stands to reason that a consumer would question anyone or anything that might constitute a threat. 

Today, our primeval, suspicious mind guards us when it comes to advertising. A wary consumer doesn't want to suffer a loss, whether in cash or efficiency, when buying a new or untested product. 

Choosing the right brand or product is in essence a new form of evolutionary "survival" in modern society. As a marketer, you need to know how to convince critical minds to buy a certain product. 

So how do you do this? It's best to gather statistics and credentials from trusted voices or authorities, placing your product or service in front to speak directly to the critical mind in a positive way.

In the 1990s, chewing gum was frowned upon as an ugly habit, believed to contribute to unhealthy teeth and gums. Yet Trident Gum brought in statistics to turn consumers' attitudes around. In its ads, the company claimed that "four out of five dentists surveyed recommend sugarless gum to their patients who chew gum." 

Not only was the company's message easy to understand, but also the message was backed by a trusted authority — dentists.

Yet even if you can satisfy a consumer's critical mind, you'll still need to ensure that she associates your brand with desirable attributes. So how do you build positivity in your brand?

### 9. Positive associations strengthen a brand. Infuse your brand with traits your customers desire. 

If you think of a popular brand, chances are the association brings up a host of positive attributes as well. A great brand makes us feel good, and feeling good is a positive emotion.

Remember that a brand lives in the minds of your customers, not on a supermarket shelf! 

Brands and their associations, however, depend on the subjective viewpoint of the observer. Once, while vacationing in South America, the author noticed a man wearing a "Tommy Halfmaker" shirt.

The shirt's logo and design were identical to that of popular clothing brand Tommy Hilfiger, with a crucial change to the company name. The article was clearly a knock-off, but to locals, a shirt from Tommy Halfmaker in Santiago was just as desirable as a shirt from Tommy Hilfiger in New York. 

The power of brand association is especially important to consider if you're attempting to attract a new demographic. More often than not, you'll need to change your brand image accordingly. 

For example, when tobacco company Philip Morris introduced the Marlboro cigarette in 1924, it was marketed mainly to women. 

In the 1950s, however, the company wanted to expand Marlboro's reach to men. Marketing creatives brainstormed masculine symbols, and decided on the cowboy — and so in 1954, the "Marlboro Man" was born. 

This image change is considered one of the more brilliant marketing coups in history. A consumer didn't buy Marlboro because he wanted to be a cowboy, but because he desired a cowboy's traits: independence, defiance, adventure and romance. 

Additionally, such traits also spoke to women, the brand's original audience. By 1972, Marlboro became the leading brand of cigarettes in the United States.

### 10. Lead your customers to an action, and they’ll convince themselves they’re in control. But you are! 

"Tell me and I'll forget; show me and I may remember; involve me and I'll understand." This Chinese proverb is incredibly relevant when it comes to consumers. 

Companies can use marketing to help consumers take an active role in their relationship with a brand. 

But how exactly is this relationship built? 

Energy drink company Red Bull used an innovative approach to relationship building. The company offered college students cases of the drink and allowed them to organize their own events where it would be consumed. 

In doing so, the students associated Red Bull with amazing parties and wild fun! 

But marketing doesn't need to be this direct, either. Some companies can encourage customers to make an active decision without the customer even realizing it. It all comes back to human behavior. 

Whenever we act in a new fashion, our minds try to justify the different behavior. We generally like to think we make all decisions voluntarily, and that our choices align with our personality and identity. 

Let's say you're at your usual coffee shop, yet instead of ordering a latte, you're offered a free green tea instead. By agreeing to the tea, you interrupt your usual pattern.

While you're enjoying the tea, you might think about ordering a tea in the future, as drinking green tea fits your health goals. You might even convince yourself that you chose the green tea because you've been thinking lately about being healthier. 

What really happened, however, is that the presentation of the free tea was appealing, and you couldn't say no. The marketing pitch made you believe that you had made the choice yourself, but actually, the environment and the marketer chose for you!

Yet they let you take _action_, changing your behavior to their advantage — the ultimate example of unconscious marketing!

### 11. Final summary 

The key message in this book:

**Centering a marketing strategy around human behavior is the most effective way to create a relevant brand. By understanding the different ways people think and branding your product in a way that responds to those patterns, you'll be on your way to creating an intuitive, sophisticated marketing campaign.**

Actionable advice:

**Solve problems with creativity.**

As a marketer, you should do the same as many in business are doing these days — get creative. Differentiate yourself from competitors and coworkers alike by doing something new. With creative solutions you can address the conscious as well as the unconscious part of a consumer's brain, increasing the positive outcome of your work. 

**Suggested** **further** **reading:** ** _What Great Brands Do_** **by Denise Lee Yohn**

In _What_ _Great_ _Brands_ _Do,_ author Denise Lee Yohn draws on her over twenty-five years of experience in brand-building to demystify the branding process. You'll discover the main principles that have made brands such as Nike, Apple and Starbucks such iconic household names.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Douglas van Praet

Douglas van Praet is the founder of Unconscious Branding, a brand strategy consultancy with a focus on behavioral science. He was executive vice president of Deutsch LA, where he was responsible for the Volkswagen account. He is lauded for creating Volkswagen's "The Force" commercial, with its memorable "mini" Darth Vader, the most-shared ad in Super Bowl history.

