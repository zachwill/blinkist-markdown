---
id: 5852cacd58b1350004ed16e3
slug: under-new-management-en
published_date: 2017-01-03T00:00:00.000+00:00
author: David Burkus
title: Under New Management
subtitle: How Leading Organizations Are Upending Business as Usual
main_color: AE2336
text_color: AE2336
---

# Under New Management

_How Leading Organizations Are Upending Business as Usual_

**David Burkus**

_Under New Management_ (2016) is about the changing role of managers in a world where flexibility and knowledge work have become hallmarks. These blinks explain how certain standard management wisdom no longer applies and what to do instead.

---
### 1. What’s in it for me? Forget everything you think you know about management. 

Imagine what would happen if you had to repair your notebook with a Stone Age tool. Obviously, it wouldn't work. That tool was perfectly fit for the olden times, but nowadays we simply need something else.

And it's just the same with management strategies. Of course, those "ancient" strategies really aren't all that old, but they're obsolete, nonetheless.

Over the last few decades, the character of work has changed fundamentally. We've delegated more and more plannable work to machines while human work has become increasingly creative and unpredictable. And often, this labor isn't manual anymore; it's mental, like writing computer programs. We're manipulating ideas and knowledge instead of things. Work like this comes with rules of its own.

In these blinks, you'll discover new management strategies that will help you adapt to this transformed world of work. Additionally, you'll learn about some popular management tools that never really worked from the get-go.

You'll find out

  * what's so bad about emails;

  * why Amazon pays its employees to quit their jobs; and

  * why, sometimes, companies are better off sharing their ideas with their competitors.

### 2. Happy employees mean happy customers. 

We've all been taught that the customer is king, but the reality is that this saying no longer holds much water. That's because your employees should be your main concern. And don't worry about your clients, they won't mind if you focus on the people who work for you.

In fact, happy employees lead to happy and loyal customers. For instance, if you, as a manager, make certain that your employees love their jobs, they'll be more motivated and therefore more likely to be friendly and engaging with customers.

Don't believe this?

Well, it's actually supported by research.

Stephen Brown and Son Lam of the University of Houston published a 2008 study that found that employee job satisfaction is closely tied to happy customers. Their research showed that the clients of happy employees perceived the quality of their service to be much higher and, interestingly enough, the correlation holds up even for customers who have little employee contact.

So it makes good financial sense to put your employees first. Because if you take care of them, they'll do the same for your customers.

How?

Well, the key to happy, committed and creative workers is trust. That means you need to cut loose your rigid rules and avoid constantly checking up on your employees. After all, nobody likes being micromanaged.

Instead, set a few simple rules that allow your employees to use their common sense, and trust them to excel with this freedom. For example, Netflix has no standard working times and no vacation policy. This means their employees set their own schedules according to their workloads and are free to take time off at their own discretion.

And the same goes for travel expenses. The only rule there is to act in Netflix's best interest. Amazingly, the result of this policy was actually a reduction in costs!

Not just that, but it produced more engaged employees who wanted to invest in "their" company and who were less likely to move on.

> _"Great leaders don't innovate the product, they innovate the factory."_

### 3. Invite your team to participate in hiring, and pay disengaged employees to move on. 

While some stories of lone wolf heroes stand out, behind every star there's a hardworking team. So, if you're hiring a new employee, don't just look for stellar qualifications, make sure they also fit your company.

After all, just because someone was successful at one company, doesn't mean they will be at yours. In fact, research has found that even top performers who transfer companies don't necessarily excel in their new workplace. That's because they need the right team to enable them.

Not just that, but your entire team's performance can suffer if they don't click with a new hire. For instance, the same research found that teams with a new star employee often find themselves facing new conflicts.

So, while hiring is conventionally done by one or two people, if you want to hire an employee who will fit in, it's important to have your whole team participate in the hiring process. For instance, when Whole Foods wants to hire a new worker, they let the prospective employee work with the team they're applying to join. After a few weeks, the team votes on whether to bring the person on board.

But regardless of your hiring process, there will always be some disengaged employees who are pulling your company down. If you want to cut these bad seeds loose, the best strategy is to pay them to quit.

That's because disengaged employees are unproductive, prone to skipping work and letting their bad attitudes rub off on others. The only reason these employees stay on with the company is that they've already invested so much in it.

After all, it takes a lot of effort to get a job and receive the training you need to do it. As a result, employees have a hard time admitting to themselves that their efforts were poorly invested.

But it's easy to help these disengaged employees move on by offering them a decent bonus for quitting. Because if someone is truly disengaged, receiving a cash payment will help alleviate some of the pain they feel upon parting from their job.

So, now that you know how to let disengaged employees go, it's time to look at the changing character of work and what it means for management.

### 4. Manage your company flexibly and let your employees organize themselves. 

In today's economy, lots of us think for a living. In other words, rather than using our hands to make a physical product, we write essays or software, analyze systems and teach others. The term for such employees is _knowledge workers_, and their rise to prominence has a huge impact on the world of management.

That's because strict hierarchies, schedules and job descriptions are way too rigid for knowledge work, which means managers now need to make all organization temporal. After all, knowledge work, like developing an operating system, is far more unpredictable than manual labor in, say, a textile mill. It's just too difficult to come up with a strict timetable for a creative pursuit.

As a result, knowledge work tends to revolve around projects in which everyone's tasks change as the project does. For instance, someone might be the leading product designer for one team, but join another to iron out issues with the packaging of an established product's special edition.

Basically, employees are organized around projects, not fixed job descriptions. That means that for modern companies that depend on flexibility, rigid organizational structures and work descriptions just don't make sense.

In fact, your company will be much more flexible and productive if employees have the ability to make their own decisions. That's because they'll be able to react immediately to circumstances that change their tasks instead of waiting for a manager to give the go-ahead.

For example, if there's a new project, employees can organize themselves into project groups. But for this to work, you need to ensure that everyone in the company knows what skillsets their co-workers possess, even if they are in different departments. Having this knowledge is crucial to the development of flexible, well-running teams.

In fact, research has found that workers who feel that they're in control are the most productive. That's because when people act autonomously, they develop _intrinsic motivation_. That is, their motivation comes from within, not from an external source like pay, praise or prestige.

It might seem as though a manager is obsolete in this context, with workers organizing a lot on their own. But the function of a modern manager has simply changed to enabling employees to achieve this self-organization. For instance, by providing the appropriate environment. You'll learn how to accomplish that next.

> _"The fuel running most organizations today isn't brute labor — it's mental energy."_

### 5. Let your employees choose their workspace, and limit email use in your company. 

Facebook was proud to announce that they'd built the biggest open office ever to exist. Is this the future of work?

Well, when designing workspaces, it's best to pair open offices with closed spaces, giving your employees a choice in their environment. After all, while open offices are great, facilitating communication and teamwork, a 2005 study by the researchers So Young Lee and Jay Brand, indicates that people in open offices complain about noise and lack of privacy — and they take more sick days.

So, since both open and closed offices have their advantages, your best bet is to offer both. Interestingly enough, that's exactly the strategy of companies like Facebook, who are in love with the idea of open offices. The system works because employees can switch office types depending on the task they're currently working on and their personal preferences.

And according to the same study, this type of personal control over work environment really satisfies employees.

So, the hype around open offices tends to only talk up their pros. In just the same way, people tend to overrate the benefits of email. The truth is, limiting the use of email helps your employees focus.

That's because the inbox is one of the biggest distractions of modern society. For instance, according to a study, the average employee checks their inbox about 36 times _per hour_. Obviously checking your email this often is going to disrupt your work. Just imagine trying to write an article or do accounting while being drawn to your email every two minutes!

That's why the French technology company Atos SE limited the in-house use of email, replacing it with a custom-designed internal social network. The network leaves people to actively seek out the information they need, rather than constantly bombarding them with unsolicited content.

So, working environment and style are key, but moving forward you'll need a broader view.

> _"Your email inbox is a list of things that you're behind on, sorted in the wrong order."_ \- Phil Libin, founder of Evernote.

### 6. Foster contact with other companies. 

Everybody knows about Silicon Valley, but have you ever heard of its rival region, Boston's Route 128?

This tech center was a leader in the field long before Silicon Valley until its love for _non-compete clauses_ hindered its success.

Why?

Non-compete clauses are bad for business because they prevent companies from learning from one another. Here's how.

Basically, this legal tool prevents employees of a company from working for a competitor if they quit, thus preventing former employees from spreading company secrets. Sounds good, right?

Well, not really. Companies rely on the flow of knowledge and ideas. And we all know that ideas flow through people as they network, talk and cooperate.

So, if employees are allowed to move around from company to company, they'll build knowledge and ideas from all over the spectrum and connect other employees with the experience they've gained. That means amazing networks that can span an entire region.

In fact, that's exactly what happened in Silicon Valley. Companies in the region allowed for the free flow of information and the huge amount of input it generated meant incredible innovation for everyone as they used other people's ideas as stepping stones to their own inventions. Simply put, collaboration helped the region thrive.

However, with a non-compete clause, you cut yourself off from the knowledge of others. That was the case with Route 128.

Non-competes became widespread in Boston and, as companies locked their doors, innovation shut down too. Eventually, Route 128 was struggling economically and couldn't attract the top talent it needed.

So, obviously companies compete with one another, but they should collaborate as well. To foster connections you should maintain contact with employees who are no longer with you or who are soon to move on.

That's because research has shown that companies with close ties to other businesses are more likely to prosper. Building an alumni network will help your company gain a better footing in an industry network.

But maybe your company is already well connected. Don't sweat it; there's plenty more you can do to make it thrive.

### 7. Give personal, timely feedback and be transparent about wages. 

OK, so you've already learned a ton about the most innovative management strategies, but two more policies are definitely worth your while. The first is about those dreaded performance rankings.

After all, performance rankings just don't work the way they should, and you're better off going with different feedback mechanisms. For instance, most companies rank their employees once a year. It's a ton of work for little payoff.

In fact, Microsoft discovered that performance ranking actually killed ideas, creativity and innovation. That's because employees were so concerned with their rank that they were scared to make or admit to mistakes and avoided any challenging tasks. Not just that, but employees were competing with one another rather than collaborating.

So, Microsoft decided to give its staff personal, timely feedback instead.

A few times a year, a Microsoft manager meets individually with his employees to go over how well they have collaborated with others, what they want to learn and achieve before their next meeting and what they did to accomplish the goals they set at the last one.

With feedback like this, everyone feels more fairly assessed, comparison between peers is less evident and employees are encouraged to build their skills. Remember, timely feedback is important, and your employees want to know how well they're performing.

But lots of them also wonder how much their co-workers make, and it actually makes good sense to have transparency on this issue. That's because being open about wages will show your employees that you're fair and prevent them from incorrectly envying their colleagues.

After all, if you keep quiet about your pay scale, your employees might get the misguided idea that they're being shortchanged compared to their co-workers. This will breed envy and prevent bonding as well as collaboration.

For instance, at Whole Foods, any employee can look up the performance data and salary of their peers. As a result, the company enjoys amazing teamwork.

### 8. Final summary 

The key message in this book:

**Modern knowledge work is about creativity, collaboration and engagement — the exact opposite of classical management strategies. So, instead of strict schedules and hierarchies, employees now need autonomy and flexibility. Care for your employees and you'll be amazed by the payoff.**

Actionable Advice:

**Keep your organizational chart flexible.**

Do you keep an organizational chart? Good, keep it up but use it as a rearrangeable board. That's because people's roles in the modern economy aren't determined by job descriptions, but by the flexibility of project-oriented work. That makes trying to squeeze your company's organizational structure into a permanent graph impossible. So, keep your chart, but use a pencil or other erasable utensil to write it.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Originals_** **by Adam Grant**

In _Originals_ (2016), Adam Grant taps into the field of original thinking and explores where great ideas come from. By following unconventional rules, Grant gives us helpful guidelines for how we can foster originality in every facet of our lives. He also shows that anyone can enhance his or her creativity, and gives foolproof methods for identifying our truly original ideas — and following through with them.
---

### David Burkus

David Burkus is a professor of management and an author specializing in leadership, creativity and innovation. He is a contributor to Forbes, and famous for his podcasts, speeches and workshops. _Under New Management_ is his third bestselling book.

