---
id: 5e83305d6cee070006945607
slug: understanding-and-responding-to-self-harm-en
published_date: 2020-04-02T00:00:00.000+00:00
author: Allan House
title: Understanding and Responding to Self-Harm
subtitle: The One-Stop Guide: Practical Advice for Anybody Affected by Self-Harm
main_color: 20A19B
text_color: 06756F
---

# Understanding and Responding to Self-Harm

_The One-Stop Guide: Practical Advice for Anybody Affected by Self-Harm_

**Allan House**

_Understanding and Responding to Self-Harm_ (2019) explores how and why people deliberately harm themselves. Drawing on real-life examples, these blinks reveal the reality of self-harm and offer some tried and tested strategies that can help sufferers recover.

---
### 1. What’s in it for me? Learn how to recognize and prevent self-harm. 

Self-harm is increasingly common, but many of us struggle to understand why it happens and how to help ourselves or others who are affected. Although self-harm is often addressed in the media, it can be difficult to separate fact from fiction — or to recognize our own situation in what we read online. 

All this confusion can lead to feelings of frustration and isolation. How can you tackle self-harm when you're not even sure what you're dealing with? 

These blinks are here to provide some clarity. You'll learn the basics about what self-harm is, whom it affects, and what strategies can help you stop, or reduce, our self-harming. If you're a friend or family member of someone who's self-harming, you'll come away with more confidence about how to offer support. 

Using real-life examples and scientific insights, this is your essential introduction to the topic of self-harm. 

In these blinks, you'll learn

  * what self-harm is;

  * what to do if someone tells you they are self-harming; and

  * some of the reasons people hurt themselves.

### 2. Self-harm is an action carried out by an individual to deliberately inflict pain or damage to the self. 

Learning about self-harm can be a confusing experience. There's a lot of material available online, but some of it can seem melodramatic or designed to shock. Let's try to reduce this confusion by exploring what we actually mean when we talk about self-harm. 

**The key message here is: Self-harm is an** ** _action_** **carried out by an individual to deliberately inflict pain or damage to the self.**

This is a useful definition because it keeps things broad and simple. 

The term "self-harm" relates to physical damage of the body. It's important to note that self-harm is not a mental disorder, and it's not a label that describes a person. This is important because people who self-harm are often described as "self-harmers." Labeling someone like this is damaging — it implies that his self-harm defines him and that this aspect of his behavior is fixed and unchangeable. 

It's also important to note that self-harm is intentional, in that the person deliberately damages himself as the end goal of the action. This differentiates it from activities like binge drinking or starving oneself in order to be thin. Although these activities may also cause harm, the person's primary intention is not usually to hurt herself. 

We can also see that this definition of self-harm doesn't specify a _reason_ why people harm themselves. 

This ambiguity is deliberate — self-harm can include suicide attempts, as well as acts that are definitely not suicide attempts. When someone harms herself, we can't make any assumptions about what her intentions were. Not every act of self-harm is a suicide attempt. With this being said, research has found that people who self-harm have a much higher suicide rate than the general population. 

Now that we understand the broad definition, we can begin to explore how __ people harm themselves. The two methods of self-harm are _self-injury_, such as with a blade, and _self-poisoning_, such as an overdose. 

Many people assume that self-harm only means injuring oneself. If you google self-harm, you will see lots of images of people cutting themselves. In contrast, we usually assume that people who overdose intend to commit suicide. But this is a false distinction. In reality, self-injury and self-poisoning are often not that different from each other. Both are acts of self-harm, and both might be associated with either wishing or not wishing to die. Tellingly, people who repeatedly harm themselves will often alternate between these two methods. 

In the next blink, we'll take a closer look at what self-poisoning and self-injury commonly involve.

### 3. Self-harm can take many forms, and they should all be taken seriously. 

Every single one of us is unique. We have different experiences and different ways of doing things. So it's no surprise that there are many different ways in which people self-harm. 

**The key message here is: Self-harm can take many forms, and they should all be taken seriously.**

Self-poisoning — also known as an overdose — typically involves swallowing an excess amount of medication, such as painkillers or antidepressants. However, poisoning might also be done with non-medical substances like bleach or weedkillers. 

Laura is a 19-year-old woman who took an overdose. She describes feeling terrible and just wanting her distress to stop for a while. With this in mind, she took some painkillers that she found in the family bathroom — they had been prescribed to her mother for a bad back. Laura took all the pills that were there and then lay down on her bed. 

Self-injury often involves cutting oneself with any implement that is sharp enough to do damage, like razors or broken glass. Usually the most accessible parts of the body are cut, such as the thighs and lower arms. 

Some people injure themselves by carving words into their skin. Sarah was in her bedroom after a bad day at school, thinking about how unpopular she was. As she was sitting there, she decided to scratch the word "loser" into her lower arm.

Sometimes the act of self-harm is done on impulse, in a totally unplanned way. 

Mike was out with his friends at a bar. It seemed like a typical night; they talked about soccer and their weekend plans. But on his way home, Mike began feeling lonely and bad about himself. Then he saw a bottle on the street. He picked it up, broke it, and used it to injure his arm — right then and there. 

But for some people, self-harm is carefully planned and anticipated. 

Anna is a 26-year-old woman who regularly self-harms. She says that she always knows when she is going to do it — and that she thinks about it for the whole day beforehand. Anna self-harms in the evening, alone in her room, when she knows no one will interrupt her. 

Self-harm can cause different levels of physical damage. Some incidents result in life-threatening injuries, whereas other episodes only cause scratches on the skin. But remember that any self-harm is a sign of distress and should never be ignored.

### 4. Self-harm is a symptom of underlying emotional and psychological distress. 

Why do people self-harm? This is a difficult question with many possible answers. Everyone who harms themselves has their own reasons and their own unique experiences. However, research has found that there are some common features in the lives of people who self-harm. 

**The key message here is: Self-harm is a symptom of underlying emotional and psychological distress.**

Those who self-harm often report relationship difficulties — either with their family, friends, or a partner. These difficulties might result in more arguments or a heightened sense of tension in the relationship. 

Loss of relationships is also a common feature for those who self-harm. People going through a break-up with a partner may feel anxious about the future. Those who suffer bereavement may feel a double sense of loss — not only have they lost their loved one, but they might also have lost a source of emotional support. 

Self-harm can also be linked to practical problems, such as financial difficulties. 

This link is often forgotten because self-harm is so closely associated with psychological and emotional distress. But self-harm may also be a response to anxiety over problems like personal debt, housing issues, or unemployment. These issues are a very real and intense source of distress, and they are becoming more common in societies in which the welfare state has been reduced. In situations like this, the stress that people feel is twofold — not only are they coping with practical problems, but also with the emotional pain that society does not care enough about them to provide help. 

People who self-harm may also be struggling with a physical disability. These can be especially difficult to live with when they cause painful symptoms, and also when they bring practical problems such as financial insecurity. 

Many people who self-harm also report distress around sex and sexuality. 

Although many societies have become more tolerant in recent years, it is still the case that people often struggle with their sex lives. This is particularly true for people who are dealing with feelings of uncertainty about their sexual orientation — or for those who believe their sexuality will not be accepted by others. Unfortunately, abuse and bullying behavior is still experienced by many people who identify as anything other than exclusively heterosexual.

> _"If you ask someone who has self-harmed about it, they will almost certainly tell you a story about their personal circumstances."_

### 5. Self-harm serves as a coping mechanism for distressed individuals. 

There's no doubt that self-harm is a sign of distress. But this doesn't explain the full story about why people harm themselves. To get a clearer picture, we need to understand that self-harm is also a way of _coping_ with distress.

**The key message here is: Self-harm serves as a coping mechanism for distressed individuals.**

Sometimes the physical pain of self-harm is strong enough to alter the person's emotional state in a way that makes them feel better, albeit temporarily. For these people, self-harm gives them more control over their negative emotions. 

Asma is someone who tries to control her emotions through self-harm. 

She abuses strong painkillers in order to numb her emotional pain for a while. She cuts herself sometimes, too, because she finds that focusing on this physical pain is easier than concentrating on her emotional anguish. 

Self-harm can also provide a temporary release from a condition known as _depersonalization._

This is a distressing emotional state that makes sufferers feel completely disconnected from reality — or even from themselves. 

Marie is a 43-year-old woman who uses self-harm to combat depersonalization. Sometimes, when Marie looks at herself in the mirror, she feels like a stranger is staring back at her; she can't recognize herself. When this terrible feeling won't go away, she burns herself with a match. Marie has found that the resulting pain somehow brings her back to her body. 

Other people self-harm in order to punish themselves. 

Sadly, many people who have led troubled or deprived lives often believe that they are to blame for the bad things that have happened. They may feel a need to inflict pain on their bodies, perhaps as a way of seeking forgiveness. For example, Andy believes that he deserves the pain he feels when he self-harms because he is a bad, worthless person. He uses self-harm as a punishment for being who he is. 

Lastly, some people self-harm as a way of telling other people how bad they are feeling — or as a cry for help. After Patience's father physically abused her, she took an overdose of tablets and then told her mom what she'd done. Patience says she wanted her mom to understand how awful she was feeling about what had happened, and to talk to her father about it. 

In the next blink, we'll look at what you can do if you or someone you know is struggling with self-harm.

### 6. Self-harm can be prevented via several concrete actions. 

If you have been self-harming, there are certain strategies that can help prevent you from doing it again. Some of these strategies might not work for you, but this doesn't mean that you have failed — you might just have to try again, or give something else a go. 

**The key message here is: Self-harm can be prevented via several concrete actions.**

The first thing you should do is seek professional help. It's really important to make an appointment with a mental health professional, so that he or she can assess the nature of any mental health problems you might be facing. It's also crucial that you have a discussion with them about whether therapy might help you to tackle your self-harming. 

You will need help from a medical professional, but there are complementary prevention strategies that you can try on your own, too. Some of these strategies are quite simple, like trying to stick to a healthy diet. 

It's well known that there is a connection between your diet and your mood. Eating whole foods, plenty of fruits and vegetables, and trying not to eat too much processed food can help improve your mental health. It's also a good idea to cut down on alcohol — or to stop drinking altogether. It has been scientifically proven that reducing alcohol intake also reduces the risk of self-harm. 

If you're self-harming, it's also really important to get support from someone in your life whom you can trust. This could be a friend or family member. 

Those who self-harm often say there is no one they can talk to. But what they usually mean is that there is no one whom they feel _comfortable_ talking to. Perhaps you've had a bad response from someone you've told before. But don't give up on people too soon. Instead, make a list of individuals you know to be sympathetic, or whom you think you can trust to keep sensitive information to themselves. 

When you've decided on someone to confide in, try to plan the conversation in advance. 

Set a time and place to talk to them, rather than blurting everything out spontaneously. Remember that you don't have to tell them everything about your self-harming all at once if you don't want to. The first time you confide in them, it might be enough to say that there are times when you feel low — and that you would really appreciate some company during these times so that you're not alone with your feelings. 

If you don't get the response you were hoping for, try not to be too disappointed. Don't give up after the first conversation. Consider how you might do things differently to get a better response, and try again. If that fails, have the conversation with someone else.

### 7. You should proceed carefully when someone tells you they are self-harming. 

When someone tells you that they have been hurting themselves, it can be difficult to know what to say or how to react. But even in these challenging situations, it is possible to handle the conversation in a sensitive way and get a positive outcome for the person involved. 

**The key message here is: You should proceed carefully when someone tells you they are self-harming.**

When you're confronted with the news that someone has been self-harming, it's perfectly OK to express your own feelings of shock or sadness. 

This being said, try not to express your own feelings too strongly, as that might close down the discussion. You could say something like: "I'm very upset that you've been dealing with this on your own, but I also appreciate that it must have been tough for you to tell me."

When someone discloses his self-harm to you, try to get an accurate picture of what's been going on. Ask how he feels about the situation, how he has been hurting himself, and what he wants to do going forward. It's also a good idea to ask what you can do to support him. Often, the person will have picked you for a reason — perhaps he thinks you're a good listener, or that you can help him get medical assistance. 

Try to have an open conversation with the person, but make sure you don't interrogate them. When Aisha confided in a friend that she'd been cutting herself, she was disappointed when her friend repeatedly asked her "why" she was doing it — and that she demanded to look at her scars. Eventually, Aisha cut off the conversation. She didn't feel like her friend really wanted to listen to her. 

Remember that you are not a mental health professional. Your job should be limited to supporting the person to seek help from someone who is. Don't try to force them into getting this help if they are resistant, but gently encourage them. 

Lastly, you should also ensure that you keep any information that someone shares with you confidential. The exception to this rule is if you think that the person you're talking to is at immediate risk of suicide. In this case, you shouldn't feel guilty about telling a medical professional, even without their consent.

> _"Learning more about self-harm can help demystify it, make it seem less frightening, and teach you that you are not alone."_

### 8. Final summary 

The key message in these blinks:

**Recovering from self-harm can be a long road, and the strategies outlined in these blinks will not work for everyone. What's important is to confide in someone you trust, to access support from a mental health professional, and to keep trying different things until you find something that works for you. Dealing with self-harm can be a very isolating experience, but there is support and help out there for you.**

Actionable advice: 

**Know where to find support during times of crisis.**

If you're struggling with the issues raised in these blinks and need to talk to someone urgently, then help is at hand. In the UK, Samaritans can be contacted at 116 123. In the US, the National Suicide Prevention Lifeline is 1-800-273-8255. In Australia, the crisis support service Lifeline is 13 11 14. Other international suicide helplines can be found at www.befrienders.org.

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Lost Connections_** **, by Johann Hari**

Many people who self-harm have also struggled with depression or trauma. If you're ready to learn more about depression, and how to break free from its grip, then head over to the blinks to _Lost Connections._

These blinks will take you on a journey through the history and science of this debilitating condition. Along the way, you'll discover the cutting-edge treatments that are being used to help sufferers — and learn about why depression touches so many of us.
---

### Allan House

Allan House is a Professor of Liaison Psychiatry at the University of Leeds. Professor House was previously the Director of the Leeds Institute of Health Sciences, where he focused on education and applied health research. 

A short note before we begin: These blinks contain descriptions of self-harm, bodily injury, mental illness, bullying, and suicidal ideation.

