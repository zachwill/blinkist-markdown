---
id: 560946f08a14e600090000b4
slug: unfair-en
published_date: 2015-10-01T00:00:00.000+00:00
author: Adam Benforado
title: Unfair
subtitle: The New Science of Criminal Justice
main_color: E9D7AE
text_color: B5913F
---

# Unfair

_The New Science of Criminal Justice_

**Adam Benforado**

_Unfair_ (2015) outlines the major flaws inherent to the United States' justice system. In addition to the unreliability of eyewitness testimony or the arbitrary nature of many judges' decisions, every actor in the entire justice system — cops, lawyers, jurors and judges alike — is fundamentally, yet unconsciously, biased. Ultimately, the author argues that addressing these blind biases is the key to reforming our justice system.

---
### 1. What’s in it for me? Discover what’s wrong with our legal system and what we can do about it. 

In 2000, a 40-year-old, happily married Virginia schoolteacher began experiencing strange sexual urges, which eventually pushed him to begin collecting child pornography and even pushed him to attempt to molest his stepdaughter. When the man later sought help for severe headaches, a brain scan revealed a large tumor in his brain.

When the tumor was removed, all of his abnormal sexual urges disappeared. This unsettling episode raises important questions about criminality and guilt; in a case like this, who is really committing the crime, the man or the tumor?

And it is not only criminals whose behavior can be easily and unconsciously swayed by neurological shifts in the brain; judges, lawyers, cops and juries are all subject to cognitive biases that, taken together, have helped create a deeply unfair justice system in the United States.

These blinks will show you how the United States' justice system is corrupted from all sides by our faulty brains, as well as the steps we can take to make the system fair.

In these blinks, you'll learn

  * how David Rosenbaum of _The_ _New York Times_ died from a vomit stain on his shoulder;

  * why judges judge more fairly after lunch; and

  * why current ways of punishing criminals are just not working.

### 2. Hastily assigning the wrong labels to people can lead to unfair treatment. 

Can a vomit stain make the difference between life and death? It did for _New York Times_ reporter David Rosenbaum. After he was attacked on the street, passersby who found him lying unconscious on the curb spotted a vomit stain on his jacket. They thus assumed he was just another drunk and not in critical condition. 

Tragically, David ultimately died from head injuries, and things might well have turned out differently if he hadn't been mislabeled. 

Unfortunately, these kinds of mistakes aren't rare. Humans are quick to jump to conclusions based on scant evidence, largely due to the interplay between two main processes that order our brains. 

First, the _automatic process_ takes in a scene and forms rapid conclusions based on the evidence presented, disregarding missing pieces. Second, the _deliberative mental process_ works through information more diligently and can override these initial impressions.

In David's case, firefighters, cops and hospital staff all attributed the vomit stain to drunkenness via the automatic process. Without deferring to the deliberative process, they all discounted the possibility that he was actually in need of urgent medical attention.

This story demonstrates the fact that how we label victims affects how their cases are handled. 

Consider this neurological study: when people looked at photos of Olympic athletes, middle-class Americans or the disabled, the region of their brain involved with human interaction was activated. But, when asked to look at photos of homeless people and addicts, participants registered no activity in that area; instead, their brain activity corresponded with feelings of disgust.

This study confirms a sad fact: most of us don't regard those who are desperately down and out as human beings with feelings and needs. Instead, we tend to view alcoholism and similar disorders as choices, or voluntary behaviors. This tendency is called _moral distancing,_ and it leads us to treat substance abusers differently. 

Clearly, we need to avoid assigning damaging labels to others. In the next blink, you'll find out how.

> _"The labels we give victims can make a big difference in how their cases are handled."_

### 3. Suspects are subject to grueling interrogations, which can lead to false confessions. 

Consider the story of Juan Rivera, an Illinois man who falsely confessed to the rape and murder of an eleven-year-old girl. His semen didn't match the sample in the police evidence and he had an alibi, but he was still sent to prison. Although he was eventually freed after wrongly spending over 15 years in prison, the question remains: why do people confess to crimes they didn't commit?

One key factor is that we automatically assume people believe what they say, even when we have evidence to the contrary. For instance, in one famous study, researchers asked people to evaluate various essays about Fidel Castro. Even though participants were told that the authors had been randomly assigned pro- or anti-Castro positions, readers still believed the pro-Castro writers were genuinely committed to their stances. 

In other words, we can't separate convincing lies — like false confessions — from the truth. This tendency is compounded by an erroneous faith in the criminal justice system. 

Indeed, police-coerced false confessions aren't an outdated practice — they still happen! In retrospect, those who falsely confess say they did so to escape police abuse, accepting long-term punishment in exchange for short-term relief. This occurs when suspects (who are often vulnerable and sometimes even mentally ill) are subjected to grueling interrogations. 

Take the Reid Technique of interviewing and interrogation, which is widely used by police in the United States. First, the suspect is brought in for a nonconfrontational interview. If detectives think he or she is lying and guilty, they become more aggressive. Eventually, the suspect starts to believe there's no point in resisting, which is when he breaks down.

During the interrogation process, the vast majority of suspects are urged to confess in exchange for leniency. They're told that if they continue to profess innocence, they'll suffer harsher consequences if the jury decides against them. As a result, 90 to 95 percent of suspects admit guilt! This dysfunctional plea bargain practice needs to be at the focus of any reform efforts.

> _"The reason so many people elect to plead out is that they dare not take their chances with the rest of the system."_

### 4. Brain trauma and situational factors lead people to criminality. 

Few people are born evil. Rather, there are certain behavioral or situational circumstances — like poverty, social pressure or even brain trauma — that can lead people to criminality. 

The brain is naturally what controls our thoughts, emotions and behavior, but it is also hugely complex, and different parts of the brain each have a massive impact on how we act. 

For instance, abnormalities in the prefrontal cortex can lead to impulsive, emotional crimes — like responding to a minor slight by smashing a bottle over someone's head. On the other hand, abnormal activity in the amygdala can lead to calculated and emotionless crimes — like stalking someone for weeks as part of a plot to murder them and steal their jewelry.

Just these two examples show that the brain has a powerful hold on our behavior. Add to this the fact that 60 percent of the prison population has been shown to have had at least one traumatic brain injury. A number like that suggests that the cause of much criminal behavior lies with abnormalities in the brain.

Even so, brain abnormalities aren't the only factor in criminality — situational factors also play an important role. 

Take the famous Milgram experiment: participants thought they were joining a memory study and were assigned to play the role of "teacher." They were told to give electric shocks to a "learner" (played by an actor) every time he or she made a mistake. 

For every mistake, the shock level increased — all the way up to 450 volts, a life-threatening amount. Though they could see the actor screaming in mock pain, most teachers continued to administer shocks. And if they hesitated, a third person in the room, pretending to be a lead researcher, encouraged the participants to continue. 

The shocking outcome? A whopping 63 percent of subjects continued administering shocks all the way up to 450 volts! The experiment goes to show that criminals aren't so drastically different from the rest of us.

> _"Deficits in the brain can produce profound changes in behavior."_

### 5. Like the rest of us, lawyers manage to maintain a virtuous self-image – even when they break rules. 

Dishonest behavior is far more common than we'd like to believe; almost everyone breaks rules from time to time. Some people illegally download movies, others fudge the numbers to pad out their expense accounts and students sometimes cheat on tests or assignments. 

But here's the interesting part: we lie to ourselves to justify this kind of unethical behavior, all in order to preserve a virtuous self-image. 

For example, a study of high school cheaters showed that 93 percent would call themselves ethical, even though 61 percent said they'd lied to a teacher and another 20 percent admitted to theft. 

In other words, even when we flagrantly violate basic ethical codes, we can still convince ourselves that we're virtuous individuals. We do this by downplaying the link between dishonest actions and the harm they cause. After all, it's much easier to justify our behavior and maintain a positive self-image if we believe that we're not actually doing any harm. 

Lawyers are no different in this respect. Even when they engage in prosecutorial misconduct, they don't think they're cheating defendants.

Consider the story of lawyer Gerry Deegan, who withheld evidence regarding blood types that would have cleared a suspect of charges for armed robbery. The suspect, John Thompson, had also been accused of murder in a separate case, and Deegan believed he was guilty. Deegan suspected that if Thompson were acquitted in the robbery trial, he might be able to dodge the murder charge, too. 

The fact that Deegan was working on Thompson's robbery trial and not the murder case made it easier for him to distance his dubious actions from the possibility that Thompson would receive a death sentence, which allowed him maintain a positive self-image. Nevertheless, his actions in the case haunted him, and he would eventually confess nine years later.

> _"Rationalizing dishonesty is part of human nature."_

### 6. Jurors’ life experiences, as well as camera perspective bias, can powerfully influence a verdict. 

Any time a politician denies the existence of climate change, people rush to call him an idiot — but is that actually fair? It can be hard to accept, but other people might have a reasonable justification for holding different views from one's own.

Ultimately, our disagreements reflect differences in our backgrounds, not character flaws. 

Here's an example: the United States Supreme Court once blocked a case from a jury trial, reasoning that the primary piece of evidence — a video — could only support a single conclusion. The video showed a dangerous car chase between a speeding 19-year-old man and the police. The crash left the man paralyzed from the neck down.

The court was convinced that any reasonable juror would attribute fault to the 19-year-old, who was evading the police, and _not_ the cops themselves, who ultimately pulled a deadly move to terminate the chase. 

But when researchers asked a representative sample of Americans to evaluate the video, responses were far more varied, typically breaking down along ideological and cultural lines. 

For instance, a less affluent, left-leaning, highly educated African American woman holding egalitarian views was more likely to see the police as the primary culprits. 

On the other hand, a conservative white man supporting existing social hierarchies would likely attribute the blame to the 19-year-old. 

This goes to show that our backgrounds powerfully shape our perspectives, which is exactly why it's so important to have a diverse jury — something the United States justice system still struggles with. 

It's worth noting the jury bias we outlined above can be compounded by camera perspective bias. Studies have shown that it's easier to attribute a confession to police coercion when a video only shows the suspect's responses to interrogation. 

So, although cameras can supply valuable evidence, they can also have a major impact on how we judge a defendant's guilt and determine punishment. As such, we should be wary of how we use video evidence in the courtroom, to avoid unfairly influencing the jury.

> _"There is a difference, however, between being sure and being right."_

### 7. Human memory is extremely unreliable and erroneous eyewitness accounts lead to wrongful convictions. 

We like to think otherwise, but eyewitness accounts are extremely unreliable, and this can have tragic consequences.

This was the case for John Jerome White — an elderly woman wrongly identified him as the man who broke into her home and raped her. 

Sadly, 30 years passed before a DNA sample finally revealed this serious mistake. White had spent three decades in jail while the real offender went free.

White's story shows that erroneous identification can take place even after a witness spends a significant amount of time face-to-face with an attacker. Now, consider the implications for witnesses who only manage to catch a brief glimpse of a criminal! 

And yet, eyewitnesses are one of the primary sources of evidence in the United States' criminal justice system. Every year, 77,000 people are charged with crimes simply because an eyewitness picked them out of a lineup — that seems grossly unfair.

Not surprisingly, erroneous eyewitness identifications are one of the primary causes of wrongful convictions. In fact, out of the 250 first DNA exonerations, 190 involved mistaken identification!

Why do these mistakes occur with such frequency? Well, it's clear that the justice system doesn't take the limitations of human memory into account. 

This is a serious mistake, because human memory is frail. We often don't register the locations of fire extinguishers, for example, because they're not relevant to our daily lives. Also, memories erode over time: one study showed that eyewitness identification accuracy fell by 50 percent between one week and one month after an incident. 

Plus, our own motivations, expectations and experiences play a crucial role in shaping our memories. This principle was demonstrated at the "Horror Labyrinth" tour, which recreated scenes from London's gory past (think Jack the Ripper and Sweeney Todd). Participants who weren't scared during the tour were _four times_ better at picking the actors out of a lineup than those who self-reported fear!

### 8. Like the rest of us, experts fail to accurately distinguish lies from truth. 

We tend to trust experts, believing that their training and experience allows them to detect lies and unravel the truth. But unfortunately, that's just not the case. 

For starters, regular people and experts alike usually fail at detecting lies, as numerous studies have confirmed. Generally, most of the conventional wisdom we hear about spotting lies isn't borne out of fact; an averted gaze doesn't necessarily indicate a fib, and liars tend to fidget less than truth-tellers, not more.

In addition, people tend to rely on bizarre and arbitrary cues to determine falsehood. For instance, we associate round faces with happiness and happiness with trust. And since brown-eyed people tend to have rounder faces, we're more likely to believe a brown-eyed man than a blue-eyed man.

In fact, a recent meta-analysis of over 200 studies found that participants only managed to correctly distinguish lies from the truth 54 percent of the time — that's only marginally better than statistical chance!

Unfortunately, professional expertise doesn't eliminate subjectivity or improve accuracy, as much as we'd like it to. Incidentally, there's even a term for jurors' blind deference to experts: _white-coat syndrome_. 

For instance, one experiment showed that many participants decided a defendant was guilty after reading that he'd failed an fMRI lie detection test. However, these "expert" techniques and technologies, like polygraphs and thermal imaging, are not very reliable or scientific. This just goes to show how much blind faith we place in science.

The consequences of this can be grim. In another unsettling case, Kevin Fox was charged with raping and murdering his daughter after falsely confessing to a crime he didn't commit. It all happened because the police coerced him into confessing on the basis of an inaccurate polygraph test.

So, it's worth repeating: the so-called experts are just as fallible as the rest of us!

### 9. Justice isn’t blind; everything from social background to momentary bouts of hunger can determine judicial bias. 

Blind justice is one of the foundational principles of democracy. Accordingly, we expect judges to remain objective and unbiased by keeping their personal views out of the courtroom. 

But, of course, that doesn't always happen. How else would you explain the fact that judges who have a daughter, rather than a son, are 16 percent more likely to decide gender-related civil rights cases in favor of women's rights? 

How about the fact that judges appointed by the Democratic party are more likely to act favorably toward minorities, workers, convicts and undocumented immigrants? Meanwhile, judges appointed by the Republicans tend to side with big business.

This demonstrable bias makes the lack of diversity among judges all the more worrying. At the moment, white men are overrepresented in American courtrooms — by a factor of almost two to one on state appellate benches — while nearly all other groups are underrepresented. 

But social background isn't the only factor determining bias. Even something as simple as the time of day can have an impact on judicial decision making. 

One study examined the work of eight experienced judges who processed around 40 percent of all requests for parole in Israel. Incredibly, researchers determined that favorable rulings were far more common at the beginning of the workday or just after a food break. Meanwhile, unfavorable parole decisions tended to come at the end of the day.

The reason for this imbalance is simple: judges get tired as the day progresses. Once they're mentally depleted, they'd naturally rather take the cognitive easy route by sticking with the status quo and denying parole. As a result, they make more intuitive and less deliberative decisions as the day goes on. 

So, what can the United States do to reduce judicial bias? First, the country needs to help judges confront their subtle biases. Then, judges can be made to feel fearless about looking back at past cases, which is exactly what Judge Frank Barbaro did; he went back to a decade-old case and overturned his verdict, publically acknowledging that he was wrong. 

In the next blink, we'll show you how you can follow Barbaro's lead to overcome your own biases.

### 10. The public’s desire for retribution skews our justice system. 

Nobody wants to admit it, but a central goal of the US justice system is not only to deter potential offenders and incapacitate dangerous people — it also punishes out of vengeance. 

To illustrate this point, psychologist Geoff Goodwin looked at public responses to shark attacks in order to determine the extent to which people were motivated by retribution. 

Goodwin figured that if the motivation was simply to ensure future safety at the beach, the response to the attack shouldn't be influenced by whether the shark had killed an innocent young girl or a middle-aged pedophile. 

But it did make a difference! Before putting down a shark that had killed a pedophile, locals injected it with an anaesthetic to ensure its death would be painless. 

Along the same lines, researchers found that we punish people with higher IQs more severely; the same goes for adults, as opposed to children, and healthy people, as opposed to the mentally ill. 

But there are still other factors that affect the severity of punishment. Black people, for instance, are judged more harshly. In one study, participants were more likely to support a longer sentence if they knew that the perpetrator was black. On another point conventionally attractive people tend to get lighter sentences. 

Finally, and not surprisingly, simply apologizing for a misdeed tends to result in lower fines or no punishment at all. This just goes to show that it's retribution — and not reason — that often drives the United States' justice system. When the desire for vengeance decreases, the punishment gets lighter.

### 11. Solitary confinement is not only ineffective, it’s torture. 

Compared to other countries, the United States' justice system is unique — and not in a good way. Minor crimes are penalized far more harshly than in other places, and the United States is the only Western country where capital punishment is still legal. 

Plus, the prison system continues to embrace solitary confinement. Though proponents argue that keeping inmates in isolation leads to self-reflection and remorse, that's simply not the case. As we've seen in earlier blinks, criminals aren't necessarily rational people who made bad decisions based on clear reasoning or deliberation.

Another argument in favor of solitary confinement posits that the punishment will deter future offenders and keep uncontrollable prisoners off the streets, ultimately leading to a safer society. 

But the bottom line is, solitary confinement is unacceptable — it's torture. On a basic level, humans need contact with others, and not having any at all can have profound health consequences. 

For instance, researchers have shown that adults with strong social relationships are 50 percent more likely to be in good health than those with weak ties to others. This striking finding shows that social contact is critical for adults; without it, we jeopardize our physical and mental health.

Also, it is imperative to reject the "criminals deserve solitary confinement" narrative. This story neglects both situational and psychological factors, and completely disregards the fact that convicts often receive very long sentences for minor crimes. 

If solitary confinement effectively deterred crime, we'd see lower crime rates — but we don't! 

For the past 20 years, recidivism rates in the United States (the proportion of released prisoners imprisoned again after three years) have stayed stubbornly at 40 percent. In certain states, it's closer 60 percent! Stats like these prove that the United States' prisons aren't working. 

On the other hand, countries like Germany, Norway and the Netherlands organize their penal system around resocialization and rehabilitation. This model appears to be working, given that Norway has one of the lowest recidivism rates in the world, at 20 percent after two years.

### 12. There are three challenges that must be overcome to have a fairer justice system. 

We've learned all about the fundamental injustice of the United States' justice system. If citizens take action, they can reform the justice system to make it fairer. But in doing so, they will face three challenges:

First, they will need to eliminate the establishment-supported notion that simple self-reflection can eliminate bias. Overstating our own objectivity entrenches our most deep-seated biases, which leads to unfair sentences. 

Consider the fact that jury selection requires potential jurors to answer "no" to questions like, "Does it matter to you that the defendant was born in Guatemala?" Answering in the negative makes jurors feel that there's no real threat of bias in a case with a Hispanic defendant. But as we've seen, bias can creep in in any number of ways. 

This is just one example of how the legal establishment bolsters the flawed idea of total objectivity. In reality, self-doubt is crucial if we want to truly determine whether our decisions are fair or biased, and respond accordingly. Just think of Judge Frank Barbaro from an earlier blink!

Second, the United States needs to reform the way cops, prosecutors and judges bend the rules. The country's current laws assume that people make rational decisions — but as we've learned, that's rarely the case. 

Finally, the country needs to democratize access to legal information. Currently, only the rich and well connected can exploit weaknesses in the legal system and avoid prison; meanwhile, the poor and uneducated are locked up with far more frequency.

> _"Doubt is the friend of fairness."_

### 13. The United States must restructure its justice system on the basis of scientific evidence and reduce dependence on human faculties. 

As we've established, the United States' justice system has a lot of problems. But the biggest problem of all is the fact that Americans have gotten used to it! This needs to be the starting point for all reform efforts. 

In this way, instead of accepting the status quo, Americans can restructure the system on the basis of scientific evidence rather than conventional wisdom. 

Here's one example: studies show that an implicit racial bias puts unarmed blacks at greater risk of being shot than armed whites. In response, scientists are developing techniques to combat this bias, like showing images of famous blacks with positive associations (Martin Luther King, Jr.) together with photos of notorious white people (Charles Manson). Alternately, participants are encouraged to imagine being hurt by a white assailant and rescued by a black man. 

In addition to these kinds of scientific responses, there must be a broader reconceptualization of the entire justice system based on such findings. 

To start, the US justice system should stop depending so heavily on human faculties. Other sectors are already doing so: baseball players used to be picked based on scouts' intuition, but today, teams increasingly rely on statistical analysis — the justice system could follow suit. 

Additionally, the hugely flawed practice of in-court witness identification (a process that is basically just pure show, irreparably corrupted by earlier identification procedures) should be eliminated. Then, live trials could be replaced with virtual ones. That way, jurors and judges wouldn't be swayed by attractiveness, skin color or mannerisms. 

It's also worth noting that there are many existing technologies that could reduce dependence on human faculties. The author experienced this firsthand when a murder occurred a few blocks from his house. The case was solved within a few days without any eyewitnesses, simply because cameras had recorded the murderer entering the victim's house and then driving away in his truck.

### 14. Final summary 

The key message:

**The United States' justice system is flawed and unfair. To reform it, Americans need to address the hidden biases that all humans have. Developments in psychology and neuroscience will help the general public become more aware of these blind biases and lead the way to restructuring the current system.**

Actionable advice:

**Be aware of how you phrase things. If you want to encourage leniency, use percentages!**

The way we phrase things can impact the way others receive your message. 

For instance, two sets of forensic clinicians, asked to determine whether Mr. Jones should be released from a mental hospital, arrived at two very different conclusion. 

What made the difference? One pair received a memo stating, "Of every 100 patients similar to Mr. Jones, 20 are estimated to commit an act of violence." 

The other group received the same message, but phrased in a different way: "Patients similar to Mr. Jones are estimated to have a 20% probability of committing an act of violence."

The first pair were twice as likely to keep Mr. Jones confined! The takeaway? If you want someone to be more lenient, use percentages.

**Suggested** **further** **reading:** ** _The New Jim Crow_** **by Michelle Alexander**

_The New Jim Crow_ (2010) unveils an appalling system of discrimination in the United States that has led to the unprecedented mass incarceration of African-Americans. The so-called War on Drugs, under the jurisdiction of an ostensibly colorblind justice system, has only perpetuated the problem through unconscious racial bias in judgments and sentencing.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Adam Benforado

Adam Benforado is an associate professor of law at Drexel University. He served as a clerk on the United States Court of Appeals and worked as an attorney at Jenner & Block. His scholarly articles, op-eds and essays have appeared in various publications including _The_ _New York Times, The_ _Washington Post_, _The_ _Philadelphia Inquirer_ and _The_ _Emory Law Journal._

