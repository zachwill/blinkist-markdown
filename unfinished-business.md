---
id: 5681b8a5704a880007000050
slug: unfinished-business-en
published_date: 2015-12-30T00:00:00.000+00:00
author: Anne-Marie Slaughter
title: Unfinished Business
subtitle: Women, Men, Work, Family
main_color: FEEE5C
text_color: 807514
---

# Unfinished Business

_Women, Men, Work, Family_

**Anne-Marie Slaughter**

_Unfinished Business_ (2015) offers a frank analysis of a pressing question. Is it really possible for men and women to "have it all" in our modern society? Can we hope to have a great family life _and_ great career? As these blinks reveal, striking a balance between the personal and the professional isn't only possible; it's easier than you might think.

---
### 1. What’s in it for me? Learn how to have it both ways: a career and a family. 

Back in days of old, the chances of a woman enjoying a career of her own were very slim. Regardless of her true passions and talents, she was supposed to stay at home and care for the children. Happily, things have improved, and these days women can have it all — a loving partner, children _and_ a career.

So why are so many successful women single? And why do the careers of many married women take a nosedive once children come into the picture?

In these blinks, you'll learn about the many injustices that women still face today — and how your own behavior may be contributing to the situation. After grasping what's still holding women back, you'll be better equipped to work toward change — both generally and in your personal life.

In these blinks, you'll also discover

  * how striving to be Superwoman can actually make you weaker;

  * about the impact your way of speaking has on all the women in your country; and

  * why spending more time with family means getting fired in Washington.

### 2. Even today, it’s usually the woman who ends up sacrificing her career plans for the family. 

Men can have it all — a thriving career _and_ a happy family. But if you're a woman, these two goals might seem completely incompatible. 

To thrive in any highly competitive field, you have to commit the lion's share of your time and energy to your work. And if a couple has children, it's much easier for the man to do this.

That's because most people still adhere to traditional gender roles: the man is supposed to provide for the family financially and the woman is expected to raise the children and be warm and nurturing.

Consequently, most women are willing to support their husband's decision to substitute time with the family for time at work.

But even if they make a concerted effort to commit to their job and meticulously plan every move of their life, women still can't control the fate of their career and family.

Sheryl Sandberg, Chief Operating Officer of Facebook, thinks that anything is possible if you just _lean in_ to your career. But it doesn't matter how much you're _leaning in_ when you have two kids back home and an inflexible work schedule; the unpredictability of life will eventually force you to shift your focus away from work.

Some women hope that marrying a progressive partner will result in a fairer distribution of domestic responsibility — thus enabling them to pursue their career. However, this rarely works out.

In reality, most men are reluctant to set aside their career goals. For instance, a woman whom the author met at a conference shared that just a fraction of her intelligent and driven female friends, all graduates of Harvard Business School, had met their career goals, despite marrying peers who had pledged they'd be equal partners once married. Once they started a family, the men were unwilling to scale back on their own career, so the women had to sacrifice their goals and assume primary responsibility for childcare.

### 3. A father’s presence is priceless, but dedicating more time to their family comes at a high cost for men. 

In a way, women aren't the only ones who struggle to have it all. Indeed, many men would love to spend more time with their family, but there are huge obstacles between that dream and their lived reality.

Men also struggle to maintain a work-life balance. And many men regret not making more of an effort to be with their family.

Working in palliative care, writer Bronnie Ware discovered that as people (men especially) approach death, they regret that they weren't around more when their children were growing up, and that they didn't take more time to enjoy their partner's companionship.

Despite this widespread regret, men are considered to be somehow unmanly if they ask for a flexible job or decide to give up their career to spend more time with their children.

That was the case for the young lawyer Ryan Park. He recalls that, after leaving his job to stay home with his daughter, most of the moms he encountered at the park didn't believe that he could possibly be doing this by choice. Instead, they assumed that he must have been a failure in his professional life. 

This is unfortunate, as, despite the popular myth of the primacy of a mother's love, children need their father around just as much as they need their mother. 

Children need a mother, but not _more_ than they need a father, grandparents, siblings and so on. The only time a child literally can't do without his mother is during pregnancy and while he is being breastfed. Afterward, it doesn't matter whether mom or dad is in charge.

This has been verified by numerous studies, which have shown that children raised by gay parents turn out as well-adjusted as children raised by straight parents. Children need love, stability, care, nurture and consistency; they don't _necessarily_ need a parental arrangement that includes a mother.

### 4. Instead of accommodating working mothers, society burdens them with irrational expectations. 

If a woman's career stalls once she becomes a mother, maybe it's her own fault: she simply lacked the necessary ambition! Or maybe she was just incompetent, incapable of juggling a career and a family.

Obviously, this is nonsense. Nevertheless, it's a commonly held belief — one even peddled by women's leadership groups! 

The problem isn't a lack of capability or ambition, however. Rather, employers simply fail to accommodate mothers. Even worse: they actually shortchange them.

Most workplaces fail to provide sufficiently flexible schedules. As a result, mothers may revert to jobs far beneath their level of qualification, where they can compensate for time-consuming contingencies — like taking their sick kid to the doctor — by simply working faster. 

Furthermore, working mothers are not only paid less than men but less than single women, too. In 2013, for instance, an average single woman with no children earned 96 cents to the male dollar; married mothers earned just 76 percent of their male peers' earnings.

Additionally, society overwhelms working mothers with a crazy expectation: they should be perfect mothers _and_ perfect employees. This social pressure is reflected in what women expect of themselves.

For example, a physician with two children once wrote to the author professing a feeling of guilt for not being a good enough mom due to her 80-hour workweek — but also for being less than 100 percent devoted to her work.

As a working mother, you may feel guilty if your colleagues are able to work all the time and you aren't. But there's no reason to feel guilty! After all, overwork actually results in inefficiency. 

Studies show that long working hours also have a negatively impact on productivity and focus, while periods of free time stimulate creativity.

The author experienced this while working at a law firm. Sleeping merely five hours per night, she was constantly nervous and forgetful. It eventually got so bad that she'd literally forget what she and her research assistant were talking about mid-conversation.

> _"...babies are a 'focus killer.'"_

### 5. Anyone who has to take care of others will face some major disadvantages. 

In today's world, Mother Theresa would probably have earned more respect working a prestigious, competitive job than caring for the sick. 

Not all parents aspire to be like Mother Theresa; however, they do have to provide care, and anyone who has to do that, whether for days or decades, is immensely disadvantaged in the job market. 

No matter how successful a woman is in her career, once she quits her job for a period of time to stay home with her kids, all current and prospective employers cease to see her achievements as having any relevance.

For instance, if a young female lawyer on a promising career track decides to work part-time or even take time off in order to care for her children, her career in law is essentially finished.

It's even worse for single mothers, who typically can't afford to even think about quitting their job to stay home with their children. That was the case for Maria, a single mother from Rhode Island, who worked at a factory for $7.40 an hour. When she missed her shift because her son was ill, she was suspended from work for two weeks, and upon returning was greeted with a pay reduction.

African American women have it even worse. For generations, staying home to take care of their own children was an unimaginable luxury for African American women. The only way to provide for their kids was to leave them alone at home and go off to look after _other_ people's children.

And while caring for others may be rewarding in many ways, it's also the most underappreciated job on the market. The easiest way to gauge how much we actually value caregiving is to look at how much we pay for it. And as it turns out, caregivers are the lowest paid workers in America. 

Next, we'll look at how we might improve things — for women and men alike.

> _"That's the trifecta of low value: woman, color, and care."_

### 6. The first step toward gender equality is to let go of your own stereotypes. 

When we look at magazines or TV shows from half a century ago, it's easy to laugh at all the absurd and blatant gender stereotypes. But even today, though in subtler ways, these same stereotypes greatly affect our lives.

Gender stereotypes and role expectations limit women's (and men's) choices, so it's a good idea to question them in everyday life. Assumptions and expectations can influence our behavior, sometimes resulting in self-fulfilling prophecies. 

For example, if a woman assumes that she is much better at caregiving than her partner because that's what she's been told by society, she's more likely to shoulder most of the caregiving responsibilities, thus confirming her initial assumptions about her role and capabilities — no matter how erroneous those assumptions might have been. 

Moreover, as long as women believe that they are fundamentally better at raising children or doing housework, they're likely to control and criticize their partner's parenting style and ability to do chores. But no partner likes to be policed, and being critical takes a lot of energy, too. 

And if a man is told time and time again that he's somehow unfit to care for his children, he might start believing it. As a result, he'll avoid spending time with them and consequently fail to acquire the necessary child-raising skills.

In order to let go of these stereotypes, you'll need to drop the superwoman image. Many women think that perfection is just around the corner, and that getting there is just a matter of working harder, sleeping less and putting on a smile.

But as long as you hide your exhaustion and carry out your responsibilities without complaint, your partner will never know that you need help. 

Superwoman may be in control of everything, but you're not superwoman — nor do you need to be. If you truly want your partner to be equal at home, then let him do the chores as he sees fit.

### 7. Change the way you act and react in order to improve the situation of working mothers. 

We've all heard the rhyme _sticks and stones may break my bones, but words will never hurt me_! In actuality, words are very powerful. 

Indeed, what we talk about and how we talk about it greatly influences gender-role expectations. The words we use and the topics we discuss all reflect our attitudes, and influence other people's judgments.

Working mothers, for instance, are invariably asked how they manage to juggle work and family. But this seemingly neutral question implies that it is a _woman's_ responsibility to take care of the kids. Tellingly, men are rarely asked the same question.

You have the power to flip this trope on its head. For example, if you're interviewing a man for a job, ask him how he's going to manage his responsibilities at home and work at the same time!

Then there's the Washingtonian expression "leaving to spend time with your family," which is really a euphemism for getting fired! This phrase delegitimizes people who _choose_ to spend more time with their family.

Besides combatting this kind of subtle sexism, there are some practical things you can do to strike the right balance between work and family. 

First, when you start a family, it's vital to decide whether you want to keep your job.

After quitting their jobs to be full time mothers, many women find that it's extremely hard to reenter the job market. So, if you do decide to keep your career, you need to strategically prepare for your comeback by doing things like, for example, keeping your career network up to date.

If your work environment is tough and inflexible, then you'll have to speak up to improve the situation. One way could be to take inventory of how your working environment enables or hinders qualified women's careers, map it all out and present it to your boss.

> _"Language is one of the principal ways that we make the invisible visible and the silent heard."_

### 8. Final summary 

The key message in this book:

**It's an unfortunate reality that working parents, both mothers and fathers, face undue discrimination and injustice at the workplace. In order to have both a career and a family, we have to work to change our culture and create life partnerships that are truly equal.**

Actionable advice:

**Don't let work be a competition.**

The next time someone starts telling you loudly and proudly how many hours she worked last week, encourage her to talk more about other things, like how she likes to spend her spare time, or what good movies she's seen recently. This subtle technique is a great way to remind people that a good life involves more than slaving away at the office. 

**Suggested** **further** **reading:** ** _Moms Mean Business_** **by Erin Baebler and Lara Galloway**

_Moms Mean Business_ is a guide to time management for mom entrepreneurs. These blinks help you discover where your true priorities lie, and provide you with planning techniques that will make it possible for you to dedicate more time to your ambitions and yourself.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Anne-Marie Slaughter

Anne Marie Slaughter is the president and CEO of the nonpartisan think tank New America. Her stellar academic career in international law has landed her articles in numerous publications, and she was the first woman ever to be appointed Director of Policy Planning for the US State Department.

