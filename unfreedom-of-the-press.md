---
id: 5d1c647f6cee070006fed8f0
slug: unfreedom-of-the-press-en
published_date: 2019-07-04T00:00:00.000+00:00
author: Mark R. Levin
title: Unfreedom of the Press
subtitle: None
main_color: E42E34
text_color: E42E34
---

# Unfreedom of the Press

_None_

**Mark R. Levin**

_Unfreedom of the Press_ (2019) takes a radical look at the state of American journalism in the age of Trump. Right-wing commentator Mark R. Levin calls out what he considers the fabrications, lies and propaganda being disseminated by the modern mainstream media. In addition to examining the sad state of affairs in journalism today, Levin also travels back in time to uncover the roots of the sickness that afflicts the majority of the American press.

---
### 1. What’s in it for me? Discover Mark R. Levin’s radical take on the state of the modern American media. 

We live in times of huge societal change. Traditional news sources are being overtaken by the rise of social media, and the line separating fact and fiction has become hazier than ever before. Critics on both the left and right regularly decry the sorry state of affairs in the American media, citing a rise in "fake news". 

On the left, critics of President Trump accuse him of lying every time he opens his mouth. And on the right, commentators lament the mainstream media's seemingly endless coverage of manufactured scandals based on anonymous sources, speculation and rumors.

But Mark R. Levin goes even further. He labels the mainstream media the "Democratic Party-Press", and asserts that their sole goal is to make Trump's job of governing as difficult as possible. Levin, like Trump, believes that the media is the enemy of the people, and he has some radical theories as to why this might be the case.

In these blinks, you'll learn

  * what _pseudo-events_ are;

  * why the American media landscape used to be much more ideologically balanced; and

  * how the _New York Times_ ignored the plight of European Jewry during the Holocaust.

### 2. Having a free press is vital for a functioning democracy, but the modern United States press is far from free. 

In the liberal democracies of the West, it is common knowledge that press freedom is one of the tenets of a healthy society. But America's media has been sick for many decades.

Our story begins in 1947, when the citizen-led Commission on Freedom of the Press released a report on the state of the American press — a report that sheds light on our current media landscape, too. 

The Commission concluded that, while the availability of mass media had greatly expanded in the preceding decades, the quality of its content had decreased. Not only had the press become much more inflammatory and sensationalist, it had become outright irresponsible in much of its reporting. 

But ending on a more positive note, the Commission emphasized the importance of the media equipping people with knowledge, in order to promote the values of a free society.

Fast-forward to the present, where more modern efforts have been undertaken to define the responsibilities of a free press. In 2007, for example, former journalists Bill Kovach and Tom Rosenstiel published a book called _The Elements of Journalism_, wherein they concluded that the most important duty of the press is to equip citizens with information that allows them to be "free and self-governing." 

One of the key indicators of whether this is the case is diversity amongst journalists. A tapestry of different ideologies is a characteristic of a genuinely free press, where a balance exists between opinions representing different intellectual strains. 

Sadly, the American media today falls short of this ideal. A 2014 Indiana University study found that while 28.1 percent of journalists identified as Democrats, only 7.1 considered themselves Republicans. 

This is hardly a balanced playing field in terms of intellectual diversity.

And these imbalances are reflected clearly in public opinion. Gallup polling now shows that while nearly 80 percent of Democrats trust the media, only 20 percent of Republicans can say the same.

These statistics bring to light a very worrying phenomenon — that the vast majority of the modern media landscape shares the ideological mindset of the Democratic Party. 

And this has dire consequences.

One is the _extremism-redefined principle._ This entails that if enough coverage is given to what would normally be considered extreme left-wing positions, these views will eventually become considered mainstream. This is just one of the implications of the stranglehold that the "Democratic Party-Press" has over American media today.

### 3. The American media of the past was blatantly political but balanced, and only later attained a fake aura of objectivity. 

In order to understand the sorry state of affairs the American media has arrived at today, we must look at how we got into this mess in the first place.

In the years leading up to the founding of the United States, freedom of opinion in the press reigned supreme. But unlike the supposedly objective news of today, the pamphleteers of the American Revolution were blatantly biased in their reporting. Thousands of pamphlets condemning the monarchy were distributed up to and during the Revolutionary War, arguably helping to turn public opinion against oppressive British rule.

Blatantly biased journalism continued unabated in post-revolutionary America. Partisan newspapers were the rule, not the exception. Throughout much of the 19th century, the American media landscape represented a healthy balance of opposing political ideologies. Newspapers would declare their support for various politicians and parties, whilst simultaneously deriding their opponents.

However, this was not to last. Around the turn of the 20th century, the United States went through the Progressive Era, during which a number of left-wing presidents began changing the nature of American governance. 

Gone were the days of limited state intervention — in its place, increased regulation in both the economy and people's personal lives became the norm. Big corporations were split up, and people were even forbidden from drinking alcohol.

One of the ideological concepts responsible for this shift was an increasingly "scientific" approach to all things. Instead of the state letting people live as they pleased, governments applied logic and reason to "help" their citizens live better lives.

This shift in thinking also changed the nature of the American press, planting the seeds for the modern Democratic Party-Press. Instead of journalists transparently expressing biased opinions, they now attempted to be neutral observers, reporting facts and not opinions. 

Thus, the myth of an objective press was born.

It was also during this time that activist journalism came into being. During the First World War, left-wing president Woodrow Wilson launched the Committee on Public Information. Their job was to provide mainstream media outlets with government propaganda masked as news briefings. In other words, Wilson created an official government news outlet. And dissenting journalists faced either arrest or intimidation if they didn't toe the line.

Left-wing presidents like FDR continued with tactics like this throughout the Great Depression and the Second World War, entrenching activist journalism — masked as neutral, objective reporting — into the American media ecosystem.

### 4. The media has been unprecedentedly hostile towards President Trump, and yet he has not placed restrictions on the press. 

Following in the footsteps of his left-wing predecessors, President Obama was a master at getting the media to bow to his will. 

The Associated Press reported in 2018 that the Obama administration prosecuted more journalists and whistleblowers than all other presidents combined, and even subpoenaed and spied on _New York Times_ reporters, along with a host of other news organizations. 

But that didn't matter to the Democratic Party-Press. It toed the line, and proclaimed Obama's progressive agenda a transformative phenomenon that was helping to bring America out of the dark ages of the Bush years.

The same positive reporting isn't applied to President Trump. For example, a 2017 study conducted by Harvard University's Shorenstein Center found that six of the seven media outlets that Trump labeled "enemies of the people" were extremely unbalanced in their reporting of his first 100 days in office. In fact, there wasn't a single topic that these six mainstream news outlets gave Trump net favorable coverage over. The most prominent of these were the _New York Times_, CNN and the _Washington Post._

But the media's apparent aversion to Trump hasn't stopped them from obsessively reporting on him, to an extent unprecedented in modern American political history. Just consider the fact that, during Obama's tenure, stories regarding his presidency occupied between 3 and 5 percent of airtime on cable news channels. For Trump, those numbers have more than tripled, to between 13 and 17 percent. 

With this sort of obsessive, unbalanced press coverage, it's no surprise that Trump calls the media the enemy of the people. In response to him doing so, 300 progressive publications decided to simultaneously publish a piece called "Journalists Are Not the Enemy," in which they accused Trump of attempting to replace a free press with a state-run media. 

This is of course nonsense — just because Trump is extremely critical of the Democratic Party-Press, this doesn't mean he wants to curtail freedom of the press in general. 

Luckily, most Americans were unconvinced by this coordinated, progressive nonsense — a 2018 survey found that 52 percent of Americans didn't see press freedoms as under threat. In fact, it seems that more and more Americans are starting to agree with Trump — according to polling organization Ipsos, 51 percent of Republicans think that the media is an enemy of the American people. 

And this number will probably continue to rise.

### 5. The Democratic Party-Press no longer reports the news, and instead opts to create fake news. 

Unless you've been living under a rock for the last few years, it's likely you've heard the term "fake news" being thrown around a lot. But fake news is not a recent phenomenon. In fact, its existence was initially examined more than a century ago by esteemed University of Chicago historian Daniel Boorstin.

In his 1962 book _The Image_, Boorstin explains that in the time before omnipresent mass media, only a limited number of events were reported on. What's more, literacy rates were lower, meaning that fewer people could read this news. 

But starting in the 20th century, this all changed. 

Instead of simply reporting on actual events, such as wars, assassinations or earthquakes, journalists were tasked with seeking out stories to report. After all, readers expected their morning papers to be filled with news, analysis, opinion pieces and so on. 

And when all of these options were exhausted, journalists would fill the remaining empty pages with pieces on what Boorstin called _pseudo-events._ Instead of collecting news, it became the job of journalists to fabricate news based on speculation, anonymous tips or rumors. Journalists were no longer reporting facts — they were creating fake news.

And perhaps the best example of a pseudo-event in recent times has been the Russian collusion story. In fact, the author holds this up as the biggest news scam that the Democratic Party-Press has ever gotten away with. 

During her 2016 presidential campaign, Hillary Clinton started to accuse President Trump of being in Putin's pocket. From then on, the Democratic Party-Press began a relentless torrent of coverage based on speculation, not facts.

This pseudo-event spanned a disgraceful two and a half years. It included round-the-clock reporting, from before Trump was even inaugurated right up until Robert Mueller sent his report to Attorney General William Barr. During this time, ABC, CBS and NBC's evening news dedicated a whopping 2,284 minutes of coverage to the "Russiagate" pseudo-event. That's an average of three minutes every night for two and a half years.

This extreme obsession with Russiagate came crumbling down in April 2019, when, as the author and many others see it, President Trump was cleared of all wrongdoing. Almost immediately, many CNN and MSNBC shows experienced major ratings drops.

### 6. Instead of being the paper of record, the New York Times has acted more like the enemy of the people for decades. 

One of the worst examples of the Democratic Party-Press is the _New York Times._ It's known as "the paper of record" for its supposedly exhaustive and unbiased reporting, but the reality couldn't be further from the truth. Even Jill Abramson, the former executive editor of the paper, admits that they have become the "anti-Trump paper of record," and that the paper has become a key propaganda outlet of the Democratic Party.

But is the _Times'_ disgraceful record on reporting events — and pseudo-events — only a recent phenomenon? Or could it be that the paper has actually been shamefully misinforming its readers for a very long time? According to the author, this is definitely the case. 

Just consider the tragedy of the Holocaust. By the end of 1942, evidence of the Nazis' systematic campaign against European Jews was unambiguous. You might assume, then, that there was a huge media outcry over the ongoing genocide, which would become the largest in world history. Surely the _Times_ plastered its front page with stories about the suffering of Europe's Jewish community?

Well, it turns out that this wasn't the case. 

In fact, while around half of the _Times'_ front-page pieces were about the ongoing war, the paper only featured the plight of European Jews six times before 1945. 

Instead, stories on the topic were usually buried in the _Times'_ inner pages. Indeed, their first ever piece on Nazi extermination camps, which described the situation as "the greatest mass slaughter in human history," only appeared on page five. Meanwhile, the deaths of non-Jewish civilians made regular appearances on the front page.

Not much has changed today. According to the author, the _Times_ continues its anti-Jewish editorial policy by regularly publishing hit pieces about the Israeli government, and blatantly advocating for Hamas and the Palestinian cause. For example, during the momentous relocation of Israel's American embassy to Jerusalem, Judaism's holiest city, the _Times_ instead focused their coverage on protests happening in Gaza. 

Instead of congratulating America's diplomats on this victory in supporting their Israeli allies, the _Times_ and the rest of the Democratic Party-Press instead glorified Hamas terrorists. Some even went so far as to accuse President Trump of having blood on his hands when riots broke out in the Hamas-controlled Gaza Strip.

The _Times'_ apparent anti-Jewish bias leads the author to think that if the millions of Holocaust victims could speak today, they would probably label the _Times_ and their Democratic Party-Press cohorts enemies of the people for turning a blind eye during some of the world's worst moments of inhumanity.

### 7. Final summary 

The key message in these blinks:

**While America used to have an ideologically balanced press, that is not the case today. The mainstream media works at the behest of the Democratic Party to spread their progressive agenda to the masses. This so-called "Democratic Party-Press" has worked tirelessly to spread fake news about Donald Trump, particularly in the case of the non-existent scandal over his possible collusion with Russia during the 2016 presidential election. A key progressive media outlet is the** ** _New York Times_** **, and their record of being on the wrong side of history stretches back decades.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _True Enough_** **, by Farhad Manjoo.**

Now that you've experienced Mark Levin's radical conservative take on the modern American media landscape, it might be time to check out a slightly different approach to analyzing the news. After all, it's important to hear both sides of the story.

Luckily, our blinks to Farhad Manjoo's _True Enough_ convey that other side. In them, discover how both liberals and conservatives think the media is biased against them, and how facts are dealt with differently by left and right-wing outlets.
---

### Mark R. Levin

Mark R. Levin is a right-wing commentator and author. He has his own show, _Life, Liberty & Levin, _on Fox News, and he hosts _The Mark Levin Show_ on syndicated radio. Before entering the world of media, Levin served in the Reagan administration as chief of staff for Attorney General Edwin Meese. Five of his previous books have topped the _New York Times_ best seller list, including _Ameritopia: The Unmaking of America_ and _Liberty and Tyranny: A Conservative Manifesto._

