---
id: 56d596ac94333d0007000043
slug: ungifted-intelligence-redefined-en
published_date: 2016-03-02T00:00:00.000+00:00
author: Scott Barry Kaufman
title: Ungifted: Intelligence Redefined
subtitle: The Truth About Talent, Practice, Creativity and the Many Paths to Greatness
main_color: 6F8B63
text_color: 4B733B
---

# Ungifted: Intelligence Redefined

_The Truth About Talent, Practice, Creativity and the Many Paths to Greatness_

**Scott Barry Kaufman**

_Ungifted: Intelligence Redefined_ (2013) is about finding a new, more holistic approach to assessing human intelligence, rather than focusing solely on IQ. Scott Kaufman outlines the flaws in IQ tests and offers a more effective theory of what makes us smart.

---
### 1. What’s in it for me? Challenge your beliefs on intelligence and giftedness. 

We all like to believe that we are good thinkers. Now imagine that experts told you that you were not intelligent _at all_. What a nightmare! Would you believe them?

Would you bury your dream of writing that novel once and for all, convinced that someone with a below-average IQ could never write anything worthwhile? 

While this might just be a hypothetical (though stressful) thought experiment for you, it happens to thousands of children every day.

But is it really true that those children will never succeed? That's one of the many questions these blinks answer. You'll also learn about the beginnings of intelligence testing, find out what distinguishes an average from a masterful violinist and encounter a radically new way of thinking about intelligence.

Additionally you'll learn

  * about two Nobel Prize winners who weren't considered gifted;

  * why telling your child she's bright can actually make her shy away from challenging tasks; and

  * what a _Dream Director_ does _._

### 2. A child’s development is determined by her genetics and environment. 

People used to think genes were the sole determinants of a person's intelligence and character. This view has changed quite a bit, but genetics still have an impact on human psychology.

Genes do affect certain personality traits but they only account for a small percentage of what makes us "us." IQ is a good example of this. One recent study of IQ assessed 500,000 genetic markers in over 3,500 participants. The researchers found that the genetic markers accounted for between 40 and 50 percent of the differences in the participants' IQ scores, but any single gene could only account for a tiny percentage of IQ variance.

And this isn't only true of IQ. Many human traits are _polygenic_, meaning that several genes interact with each other to produce them. 

So genes are important, but a child's developmental environment plays a big role as well. Imagine two children entering the same year at school. One of them can already read well whereas the other struggles with reading. What will happen at school? Will the difference between them even out?

No, it won't. The child who has a head start will probably keep reading and learning, getting even further ahead of the other as time goes on. She'll acquire a bigger vocabulary and be more likely to pick friends who are also advanced readers. 

The other child will face the opposite problem. She'll probably feel discouraged and may even turn away from reading: if books are more of a struggle, they'll be less rewarding. The child's vocabulary will grow more slowly as well. By the end of the year, the two children will probably be even further apart in reading skill than they were when they started.

### 3. IQ tests are widely used to measure intelligence, but they’re not ideal for the task. 

The first modern IQ test was developed by Alfred Binet around 1900 and was quickly adopted as a standard measure of intelligence. It's changed over the years but still rests on the same logic today. 

Shortly after the IQ test was developed, school authorities in France started using it to root out intellectually disabled students so they wouldn't slow down other students. Binet himself opposed this. He intended his test to be a qualitative, not a quantitative measure, and was against using summary IQ scores. 

Binet's original test consisted of 30 questions that had to be completed in 20 minutes. Today, seven IQ tests are commonly used, often as tools for labeling children and sometimes even determining their paths in education.

In many countries, career counselors and psychologists rely solely on IQ to decide if a child is "learning disabled" or "gifted." If they're labeled as "slow learners," children will likely be discouraged from taking certain courses and may even be steered away from certain careers. 

IQ tests are often unreliable. Children commonly score differently on different IQ tests. The author encountered a young girl named Brianna who took three tests and got three different results, ranging from 105 (average intelligence) to 125 (gifted).

A person can also achieve different results at different ages. An 11-year-old's IQ score can be used to predict his IQ at 21, but within a 20 point range and with a probability of only 68 percent.

All in all, IQ testing should only be used as just _one_ tool for assessing intelligence. There are other important factors that influence a child's success, like taking time to practice and having a particular mind-set.

> _"I wouldn't cross a busy intersection if I had only a 65 to 70 percent probability of making it to the other side."_ — Alan Kaufman

### 4. Focused practice and having a growth mind-set help you to succeed. 

It takes practice to learn a skill. To excel, you need _deliberate practice_ : structured practice in which you work hard to improve on your weaker areas. Mentors and teachers are particularly helpful here. 

A study conducted in the Music Academy of West Berlin is often cited in discussions about practice. Researchers there found that most violinists practiced for 50 to 60 hours per week, regardless of how established they were. 

One thing set the best violinists apart, however: deliberate practice, not talent. Top performers did twice as many sessions in which they focused on specific aspects of their playing, following instructions from their music teachers. 

The best performers had clocked in around 7,410 hours of solitary practice before their eighteenth birthdays. That's slightly higher than the 7,336 hours of solitary practice done by middle-aged violinists playing in international-level orchestras

A _growth mind-set_ also pushes people to achieve more. Carol Dweck, a Stanford psychologist, was the first to describe students as having _fixed mind-sets_ or _growth mind-sets_. 

A person with a fixed mind-set believes that everyone is born with abilities at fixed levels. If such a person fails a math test, she'll take it as a sign that she's ungifted and will be more likely to give up on math. A child will develop a fixed mind-set if she's regularly told she performs well because she's gifted.

Students with a growth mind-set, on the other hand, believe they can improve their skills and abilities. They think success depends on effort and interpret setbacks as signs they didn't work hard enough. They're more likely to persist when faced with challenges, to try new things and be less hurt by failure. 

Over all, a growth mind-set leads to better performance.

### 5. IQ can’t predict one very important aspect of giftedness: creativity. 

Creativity is elusive. We can't really explain what it is or how it arises but we know it's very powerful.

Creativity is about developing new and useful ideas. It's an essential part of problem-solving. 

IQ tests measure our ability to think logically about straightforward problems, which is certainly an important part of intelligence. However, IQ tests don't measure creativity, because creativity isn't straightforward. It's about considering multiple approaches to a complex problem, without dismissing any of them too quickly.

Many researchers have tried to develop methods for predicting a child's creativity. In 1921, Lewis Terman began an extensive long-term study in an attempt to prove that having a high IQ score correlates with creative genius. He failed.

Terman tested 168,000 children with an IQ test and only used those who scored above 135 for his study. This group of children, whom he called "termites," had an average IQ score of 151, which is very high.

Terman found that the termites weren't only smarter (in having larger vocabularies and more powerful memories, for instance), they were also healthier, taller and more socially adept than their peers.

Thirty-five years later, Terman summarized the group's accomplishments: 2,000 scientific papers, 60 books, over 230 patents, 33 novels and 375 short stories, among other things. 

The termites were exceptionally bright but none of them turned out to be true creative geniuses, meaning none of them created or contributed to something that revolutionized their field, and none of them won a major prize. However, two children who had been excluded from Terman's study because of their lower IQ scores grew up to win the Nobel Prize in physics.

### 6. The Theory of Personal Intelligence is a more holistic approach to assessing human intelligence. 

Our tools for measuring intelligence didn't change much over the years — until recently.

Kaufman has developed a new theory of intelligence that's more holistic than traditional approaches. Traditionally, theories of intelligence haven't considered traits like passion or willpower to be connected to intelligence. They've just compared the ways in which people solve the same abstract problems — without considering how those people might face real-life obstacles or pursue personally meaningful goals. 

Kaufman defines intelligence as something more dynamic. Intelligence is really about the way a person's abilities interact with her engagement and determination. Truly intelligent behaviors help a person move closer to achieving her goals, so Kaufman's _Theory of Personal Intelligence_ isn't about comparing people's test scores.

The Theory of Personal Intelligence is based on four tenets.

The first is the _self_ on which human intelligence is centered. It's comprised of all the characteristics that make up a person's identity. This must be taken into account when you assess the intelligent behavior a person shows in working toward her goals. 

The second tenet is a person's _engagement_ with the tasks she performs, like playing the violin. Engagement enables us to build skills and focus on the task at hand. Practice, engagement and ability are virtually inseparable.

And a person doesn't always have to use straightforward thinking to achieve her goals, which is the fourth tenet of Kaufman's theory. Spontaneous creative thinking is just as important in tasks that take you into the unknown, like scientific research.

The final tenet is that there aren't any fixed, universal measures of intelligence that determine a person's success. Researchers have suggested that people need ten years of intense practice to excel in a given field, but even that's just an average. No single rule can universally predict success.

> _"The comparison isn't with others; it's with your former and future selves."_

### 7. Education should be reformed to match the new understanding of intelligence. 

Most children around the world would agree that standard schooling methods need to change. 

Education is supposed to be beneficial and meaningful for the students. We saw earlier that people strive to develop their skills when they're invested in a goal they find meaningful. So if education is to help the students make the most of their potential, it needs to cater to them and their interests.

Students should be coached as they pursue their own goals, but also taught skills that have a broader application.

_The Future Project_, founded by two Yale graduates, is a new approach to education that's shown promise thus far. It pairs students who have an idea for improving their communities with mentors who can help turn it into a project.

The Future Project recruits people from local communities and pays them to work full-time as _Dream Directors_ in one of the country's high schools. Dream Directors set up a mentoring system in the school to help the students realize their own _Future Projects_.

Technology can also improve education by making it more personal and engaging. If we want to really transform education, we have to stop measuring success with grades and IQ scores, and start measuring it by student engagement.

Shimon Schocken, a computer science professor, developed two game apps for helping children learn math in a more fun and engaging way. In one game, children have to distribute a bowl of apples evenly among a group of aliens. They earn points for distributing them evenly and the app tracks their progress.

Technology like this allows children to learn faster and in a way that is more entertaining and suited to their own progress. The traditional education system isn't good enough for our highly intelligent kids!

### 8. Final summary 

The key message in this book:

**We tend to impose awkward labels on children when assessing their intelligence. These labels can be very harmful to the development of both "challenged" and "gifted" children. So instead of conceiving of intelligence as a score on a test developed over a hundred years ago, we need to start accepting it for the fluid and dynamic concept it is, and tailor our education system to reflect that.**

Actionable advice:

**Ask a child what they want to learn.**

Children deserve to have a say in the direction their education takes. The simplest way to start figuring out what they're passionate about is to ask them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an e-mail to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Scott Barry Kaufman

Scott Barry Kaufman was diagnosed with a learning disability as a child but went on to study at Carnegie Mellon, Yale and Cambridge. He cofounded _The Creativity Post_, contributes to _Scientific American_ and serves as director of the _Imagination Institute_ at the Positive Psychology Center at the University of Pennsylvania.

