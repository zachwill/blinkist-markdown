---
id: 5e8c5fb66cee070006984f26
slug: united-en
published_date: 2020-04-09T00:00:00.000+00:00
author: Cory Booker
title: United
subtitle: Thoughts on finding common ground and advancing the common good
main_color: 30384C
text_color: 30384C
---

# United

_Thoughts on finding common ground and advancing the common good_

**Cory Booker**

_United_ (2016) is the inspirational story of state senator Cory Booker's lifetime spent fighting for the common good. From Ivy League classrooms to the roughest neighborhoods in Newark, New Jersey, Booker examines the big issues America faces up close. These blinks explore the depth and impact of these problems, and call on each one of us to work together in finding common ground.

---
### 1. What’s in it for me? Get to know the hard-earned wisdom of one of America’s most impressive and ambitious politicians. 

Some of you may have heard about Cory Booker as just a one-time Democratic candidate in the 2020 race. But Cory Booker's life and career in politics are far from typical. Whether Booker was turning down high-paying job offers to work for a non-profit in a rough area of New Jersey, camping in the courtyard of an apartment complex until tenants' complaints were taken seriously by the mayor, or driving around and personally helping drug dealers find legal, safe work, Booker has always put himself on the front line of fighting for justice. 

From his time as councilman, mayor, and state senator of New Jersey, Booker has gained valuable insights into how government functions (or doesn't) at all levels. He's also developed a simple and powerful life philosophy, rooted in values he was taught as a child and solidified through his experiences.

Booker believes that in the United States, a nation that is continually dividing itself into an increasing number of hateful camps, the only way forward is to see across our differences and become united as a people. It's only once we have established common ground that we can work toward the common good. 

In these blinks, you'll find out

  * why housing should be treated as a human right;

  * how America stopped having the best infrastructure; and

  * what the "conspiracy of love" is.

### 2. Understanding that we’re all connected is the first step toward finding common ground. 

New Jersey state senator Cory Booker had spent his whole life identifying as a black American man. That's why he was so surprised to learn about his mixed heritage.

As is the case when most of us delve into our backgrounds, research into Booker's family tree revealed a complex web of genetics. He's the descendant of slaves and slave owners, of Native Americans and of a white man who fought in the Creek War of 1836, pushing Native Americans from their land. In other words, his ancestors quite literally fought each other.

Using DNA results, Booker found and met a white man to whom he never would have guessed he was related, but with whom he actually shared a great-grandfather. The day Booker shook this man's hand, something his parents had told him in childhood really clicked: Making the world a better place begins with setting aside whatever differences we may think divide us. To find common ground, we have to understand that we're all connected. 

Booker's parents, Carolyn and Cary, made sure to teach their sons this lesson — because without the help of a Jewish lawyer named Arthur Lesemann, the whole family's lives would have turned out very differently. 

What happened was that when Booker's parents first moved to New Jersey, they were continually barred from purchasing a home in the all-white neighborhood they wanted to live in, even though this was technically illegal. The Bookers would view a house, make an offer, and then be told it had been sold or taken off the market. 

Luckily, Arthur Lesemann, inspired by the historic march on Selma, had begun volunteering at the Fair Housing Council. Lesemann believed that we all share a common destiny. With his legal help, the Bookers successfully conducted a sting operation. They sent a white couple to a house the Bookers had tried to purchase. When the white couple put in an offer and it was accepted, Arthur and Cary confronted the real estate agent. They had evidence that he was in violation of New Jersey state law, which put his real estate license at risk. The next day, the house was theirs. 

As a child, Cory Booker was frequently reminded that many of the opportunities he benefited from were the result of people working across their differences to make the world a better place. It's a lesson he never forgot.

### 3. We are responsible for more than just ourselves. 

When Booker graduated with a law degree from Yale, he had the perfect resumé, and could have had his pick of jobs at high-paying law firms. But getting rich wasn't a worthwhile pursuit to Booker, so he traded in his Ivy League lifestyle to become a public servant, living in one of the roughest neighborhoods of Newark, New Jersey. 

Since childhood, Booker had been taught to strive for personal excellence and to value personal responsibility. He lived out these values, relentlessly pursuing and achieving his ambitious goals, motivated by something his father called "the conspiracy of love."

According to his father, the conspiracy of love is an accumulation of billions of positive actions taken by countless people across time, all so that we get to enjoy the opportunities we have now. That meant that while Cory had a responsibility to do his best, he also had a responsibility to understand that his achievements were in large part due to all of the sacrifices, hard work, and small acts of good other people had done to pave the way.

For example, Cory's success in school couldn't be separated from the fact that he was given the privilege of growing up in a safe neighborhood with good schools, and connections with people like Arthur Lesemann and the other volunteers at the Fair Housing Council. 

In other words, Cory wasn't just responsible for himself. He was also a part of the conspiracy of love, which meant that he had a responsibility to pass along a better world to future generations. The problem was that he didn't know how best to do that. That's when his mother asked him a question that would define the rest of his career. "Cory," she said, "if you couldn't fail, what would you do?"

The answer hit him. By packing his bags and heading to Newark, Booker set the stage for spending his life fighting for the causes he most believed in. In the following blinks, we'll explore the issues closest to Booker's heart and the lessons he has learned through a passionate career spent grappling with them.

### 4. Crime has a negative ripple effect. 

One of the first things Booker learned in public service was that when a crime is committed, its consequences are felt by far more people than just the perpetrator and victim. Crime has an impact on the whole community, future generations, and society at large. 

Take Natasha Laurel, a hardworking single mom whom Booker met when she served his table at an IHOP pancake house in Newark. Natasha was just barely able to make ends meet by working an afternoon shift as well as a late-night shift at the IHOP, which was open twenty-four hours a day. This schedule allowed her to work while her children were in school, go home to feed them and put them to bed, and go back out for another shift. 

Natasha told Booker that while it was a difficult schedule, she was grateful for the work. That all changed late one night, however, when a fight ended with fatal gunfire just outside the restaurant. After that incident, customers stopped showing up after dark. Natasha relied on tips, so when the customers stopped coming, the income she depended on went away, too. 

The shooting didn't just take away an innocent man's life. It also threatened Natasha's ability to provide for her family. In addition, multiple bystanders were shot and injured in the incident and rushed to the hospital — and such events can literally cost everyone. Booker found out that the price tag for Newark taxpayers in many gunshot cases was between $100,000–$200,000. 

Another example of crime's effects rippling outward is the story of Charlton Holliday, who was a bright kid with no good role models. In middle school, Holliday started shoplifting, which helps strip community businesses of their profits. 

When the local drug dealers noticed how good Holliday was at selling stolen goods, they recruited him. Drug dealing led to violence, and Holliday ended up shooting a man and going to prison. He was twenty-one at the time, and had already fathered children with three women. In reaction to his incarceration, one of these women started taking narcotics. This led her into a downward spiral, and their son, Hassan, began bouncing around among the homes of various relatives until he ended up shot, another victim of an endless cycle of violence. 

Booker had known Hassan for years and was devastated when he died. Attending Hassan's funeral, Booker was more motivated than ever to reform the criminal justice system.

### 5. The criminal justice system is one of America’s most dire problems. 

In his last year of law school, Booker made a trip to Green Haven Correctional Facility in upstate New York. It changed his life forever. Sure, he'd read about problems with the criminal justice system, but witnessing just how bleak the conditions were and talking with men who desperately wanted to get back on track but were stuck there made a big impression. 

Unfortunately, the numbers paint a grim picture. Since 1980, the federal incarceration rate has increased eight hundred percent, and the state incarceration rate has increased five hundred percent. Even though the United States has only five percent of the world's population, twenty-five percent of all the imprisoned people on the planet are imprisoned in the US. 

Looking at these statistics, you might think that America must be a particularly violent society. How else could the system justify locking so many people up? But the truth is actually the opposite. More than three-quarters of people incarcerated in the US have been found guilty of non-violent crimes, like drug possession or property damage. Instead of helping to reintegrate these people into society, the prison system keeps them locked up, costing taxpayers billions every year. 

What Booker finds most problematic about the criminal justice system is that it's stacked against those who most need protection. If we look at the numbers, we see that the vast majority of Americans charged with felonies are poor and suffer from mental illness or addiction. These are the very people society needs to support, not to lock away. 

Another reason Booker is passionate about criminal justice reform is that when society pours resources into prisons, those resources are taken away from other places. 

For instance, between 1990 and 2005, a new prison opened in the US every ten days, funneling away public money that had once been used for infrastructure and education. The results are unsurprising — and catastrophic. Once home to the top-ranked infrastructure in the world, the US has slipped to twelfth place, and is also in danger of not making the top ten in global rankings for education. 

Reforming the criminal justice system is such a huge problem that prominent figures on both sides of the aisle are calling for reform, from Jeff Flake, Rand Paul, and the Koch brothers to the author Cory Booker, Chuck Schumer, and Al Franken. It's easy to see why.

> _"Actions, small and large, radiate out into eternity. What we do or fail to do — to one another, for one another, or with one another — leaves a lasting imprint beyond what we can imagine."_

### 6. Early in his career, Booker’s understanding of human rights expanded to include more categories, like housing. 

Newark's nickname of "brick city" has a dark history. Low-income housing blocks, like Brick Towers, where Booker lived after law school, were the result of a government effort to push all low-income housing into one area. The goal? Concentrate all of the poverty and crime in central Newark, so that more affluent, white communities could feel safe in suburbia. 

How did this happen? Well, after World War Two, a whole raft of racially focused housing policies was passed. Not only was low-income housing concentrated, but the transfer of property to blacks was banned, and real-estate agents were encouraged to steer minorities away from white zones.

Booker knows that his family was extremely fortunate to enjoy the privileges that came with living in an otherwise all-white neighborhood. But most people of color in New Jersey weren't so lucky. 

The issue is that housing rights are about far more than living in a nice house. Inequitable housing policy has intergenerational impact. Living in a poor neighborhood lowers your chances of getting a well-paying job and your children getting a quality education. This is because good schools are concentrated in wealthy neighborhoods. Studies show that segregated minority schools are far more likely to be in poor neighborhoods, that these schools have lower graduation rates, and that their students have far fewer opportunities to prepare for college. 

Booker had always thought housing was important, but hadn't necessarily thought of it as a human right. That is, until he met Frank Hutchins, a longtime advocate for tenants' rights. Frank argued that human rights are much more expansive than is commonly imagined, and include freedom from fear, human dignity, and security, all of which require fair access to housing. 

Inspired by Frank, Booker made housing a cornerstone of his policies. During his time as mayor, Newark doubled the production of affordable housing, ensured there were affordable units built in a new, affluent area, and reformed the troubled Newark Housing Authority. In fact, Booker and his team were so successful that housing authority officials from all over the United States began to study Newark's turnaround. 

As Booker's career progressed, he realized that there was another space to which we all have a right and responsibility: nature. We'll explore that in the next blink.

### 7. The greatest common ground we share is the literal ground we stand upon: our planet. 

When it comes to a common destiny, nowhere is our dependence on and responsibility to one another more obvious than with our planet. 

The idea that all humans are communally responsible for our land goes back a long way. In AD 530, the Roman emperor Justinian made stewardship of the planet a written law. He codified something called the _Corpus Juris Civilis_, which claimed that nature is part of the public trust, and that we're all both entitled to it, and responsible for it. This idea has also been a part of American ideals, through patriotic songs like "This Land is Your Land," and "God Bless America."

During Booker's time as mayor of Newark, he met a man named Leroy Edwards, who truly embodied this spirit of stewardship of the earth. When Mr. Edwards received a stimulus check, he went out and spent it immediately — not on a widescreen TV or new clothes, but on a lawnmower. With his new purchase, Edwards slowly transformed the overgrown lot across from his apartment block into a community green, clearing away trash and bringing life back to the land. As a result, the drug dealers who hung out there left, and it became a prosperous and safe public space. 

Moved by people like Mr. Edwards and other environmental activists, Booker instituted policies to help protect and nourish Newark's land. He did this because he believes the earth is our ultimate common ground, and that when we care for it, it cares for us. 

For example, research shows that planting trees makes sense because trees cool cities, provide oxygen, decrease respiratory problems, help prevent storm runoff, and increase property values. There is even data showing a causal link between more trees and decreased crime rates. 

Booker also founded a program called Clean & Green, where recently released inmates got steady work on greening projects around Newark. The program was incredibly successful, not just at planting trees and greening the city, but in keeping the men out of jail and positively reintegrated into society. In fact, many of them went on to be enthusiastic supervisors in the program. 

Now that we've explored the core issues Booker has worked on throughout his career, we'll dive into some of the key lessons he's learned about how best to bring about change.

### 8. Collective action is often more creative and powerful than individual action. 

For most of his life, Booker was a go-it-alone kind of guy. After all, everything he set his mind to, he was able to accomplish. But, as Booker quickly learned in his first years in Newark, sometimes working in a group actually brings about more profound change. 

Soon after being elected a Newark councilman, Booker received a call from an old friend who couldn't get any help from the mayor or police with continued violence in her apartment complex. Determined to help her, Booker rallied together his staff and proposed a radical strategy: they would pitch a tent in the courtyard of the apartment complex and stay in it, as a way to raise awareness and, they hoped, force the mayor's hand. 

His team did a great job mobilizing the community. As soon as the tent was up, people flooded in, from residents to activists to priests. A handful of business owners donated food and other supplies. Even a man with a street-sweeping truck joined the effort, clearing the area of broken glass! 

Because Booker and his staff were able to get such a large group of people together, the media soon caught on. After ten days, the mayor showed up, and Booker presented him with the tenants' complaints, which they'd discussed at length during their protest. Booker and the mayor were then able to agree on a strategy to address them.

Coming up with creative, group-based solutions was also a key part of Booker's approach to lowering crime rates once he became mayor. For instance, to make the most of the police budget, he shifted the gang enforcement unit schedule to reflect peak times for criminal activity, which were weekends and evenings, not weekdays. It was an unusual schedule, but it was much more effective at stopping crime. 

Booker also saw an opportunity to get employees from different branches of government to prioritize projects that had the bonus of preventing crime. He gathered the department leaders from these different branches, and they came up with collaborative and creative solutions. For instance, they got maintenance men to prioritize boarding up abandoned buildings in high-crime areas, and they directed funds toward fixing signs, providing street lights with brighter bulbs, and cleaning up parks that were known for criminal activity. Through their efforts, they turned these parks into safe community centers. 

The result of all these innovative approaches? In 2008, Newark experienced its longest period of time without a murder since 1961, and overall crime rates plummeted.

### 9. We all pay the price for allowing injustice to exist. 

America may be the land of the free, but it certainly isn't the land of the just. Whether Booker was falling asleep listening to gunshots or just trying to order pancakes at IHOP, he was constantly witnessing ways in which the government was letting its citizens down. 

Remember Natasha, the single mother and IHOP waitress? Natasha is just one of a huge number of service workers who are food insecure. That means that the same people who are serving food to customers all day often don't have stable access to food for themselves and their families. How is this possible?

The answer is that the system is stacked against them. First off, restaurant workers are often entirely dependent on tips, because it's still legal for restaurants to pay their workers unbelievably low baseline wages. There's simply no way that Nathasha could pay her family's living expenses on $2.13 an hour. 

But there's another problem that makes the tipping system even more unjust. The same government that allows restaurants to pay their workers so little also heavily subsidizes many of the ingredients that are used by restaurants like IHOP. These include corn, sugar, and dairy. The cheaper the food, the lower the bill, and the lower the tips. As a result, workers like Natasha end up relying on public service programs, like food stamps, just to feed their families. 

Natasha's situation isn't unusual. In fact, Restaurant Opportunity Centers United reports that twenty percent of restaurant workers rely on food stamps. Between 2009 and 2013, those food stamps cost Americans 9.5 billion in taxes. The price of allowing a system to undervalue and underpay Natasha is paid — literally — by everyone. 

Society also picks up the bill for making it so difficult for people with criminal records to integrate back into civilian life. For instance, it's almost impossible to get a job if you have a criminal record. Most people with such records have to wait at least five years to regain a clean slate — and that only happens if their crime was non-violent. That means that people who may have been arrested for drug possession or a traffic violation can be out of work for half a decade. It's no wonder they end up returning to crime just to survive.

### 10. Big changes come about through small actions. 

Booker's no idealist with his head stuck in the clouds. He knows there's an almost impossible amount that needs to be done if we want to change our society for the better. But every day he wakes up and keeps going, because if there's one lesson he's learned, it's that the most important thing to do is just to do _something._

Remember the tent Booker and his staff pitched in his old friend's apartment complex? Before Booker settled on that strategy, he had spent the morning disheartened and frustrated, feeling as though there were no way to help her. In the few months he'd been a councilman, everything he'd tried to do had been blocked. He felt as though nothing he could say or do would bring about change. 

That afternoon, he ran into a mentor of his and complained, saying that he didn't know what to do. She hugged him and said, "Cory, I know what you should do. Do _something."_ From that moment on, Booker realized that the most important step is the first one, no matter how small it is. 

Another mentor of Booker's who understood the importance of small, well-intentioned actions was Frank Hutchins, the advocate for tenants' rights. In their first collaboration, Booker and Frank held a public gathering in order to learn about tenant grievances in their neighborhood. 

Lots of people showed up, and they had a lot to say. While Booker started enthusiastically, taking down notes and imagining how he could come up with solutions, he soon got worn out and impatient with the citizens' long-winded stories. Looking over at Frank, he was stunned to see Frank smiling gently and nodding, encouraging each person to go on for as long as they needed. 

Afterward, Frank sat Booker down and taught him an important lesson: one of the most meaningful things you can do is truly to see and listen to a person. Often, that's far more important than your actual ability to fix a problem. 

Over Booker's impressive and far-reaching career, he credits all of the major changes he's been a part of to a series of small actions taken by a large number of people. Whether that's planting trees, hiring an ex-felon, mentoring adolescents who need good role models, or cutting the grass of a vacant lot, lasting change is the accumulation of tiny acts of goodness. 

You don't need to do anything huge to make the world a better place. Just start small, and start now.

### 11. Final summary 

The key message in these blinks:

**In a world with so much that needs to be done, now's not the time to focus on our differences. We all share a common destiny, and it's our responsibility to be united in our efforts to make society and our planet fair and habitable spaces. Luckily, if every individual just does something small, huge changes can occur.**

Actionable advice: 

**Take local issues seriously.**

Don't just worry about presidential elections. Learn about what is going on in your city — or even better, your neighborhood. Be an active part of your local community, whether that means picking up trash on your way home, helping a neighbor out with childcare, or showing up to city council meetings. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _A People's History of the United States_** **, by Howard Zinn**

Because of Booker's mixed background and unusual, hands-on approach to politics, you just learned about a unique perspective on the biggest issues in the United States today. Want to dive deeper into the history of these problems in a fresh and engaging way?

In _A People's History of the United States,_ historian Howard Zinn lays out a completely new approach to the history of the United States. Instead of hearing it from the traditional, removed perspective of the historian, you'll learn about how factory workers, immigrants, and women — who are among the groups that have been most often silenced throughout history — share _their_ stories. For an up-close, personal history of the country that won't be forgotten, head on over to our blinks on _A People's History of the United States._
---

### Cory Booker

Cory Booker is a New Jersey state senator and was a Democratic candidate for the 2020 presidential race. Booker is a graduate of Stanford and a Rhodes Scholar, and holds a law degree from Yale. Before being elected to the state senate, he served as a councilman for and then mayor of Newark. According to the _Wall Street Journal_, Booker is just the twenty-first person to move directly from being a mayor to being a senator.

