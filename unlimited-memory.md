---
id: 59982502b238e10005eeaecb
slug: unlimited-memory-en
published_date: 2017-08-23T00:00:00.000+00:00
author: Kevin Horsley
title: Unlimited Memory
subtitle: How to Use Advanced Learning Strategies to Learn Faster, Remember More and be More Productive
main_color: D97496
text_color: D9326A
---

# Unlimited Memory

_How to Use Advanced Learning Strategies to Learn Faster, Remember More and be More Productive_

**Kevin Horsley**

_Unlimited Memory_ (2014) explains memory techniques that will help you remember and retain any information you want to. These blinks will show you how to organize and store information in your mind so that you'll never forget it.

---
### 1. What’s in it for me? A better memory. 

We've all been there: we see someone at a party or meeting, someone we've met at least two or three times before and even talked to for a few minutes. But the thing is, our memory betrays us, and we simply cannot remember her name.

Perhaps you devise some ingenious way to get it out of her without asking her directly. Or maybe you'll run into a friend of yours and introduce him to this woman, hoping that she'll then introduce herself by name!

Wouldn't it be nice to finally put an end to forgetfulness? That's where these blinks come in.

In these blinks, you'll learn

  * why a pink tuxedo might be a good addition to your memory skills;

  * how you can turn your boring morning commute into a memory bank; and

  * what the sweetest sounding word is.

### 2. If you want to improve your concentration, you’ve got to clear your mind and be in the now. 

Everyone wants to have excellent memory, right? But considering how many people find it hard to concentrate on details, remembering them just seems out of the question.

There are ways to do it, though. Here are some top tips to improve your concentration.

First, rid yourself of conflict — that is, anything that leads your mind in different directions and stops you from concentrating. Do you spend time switching between checking your e-mail and sending several messages on your phone? Well, that would make you like just about everyone else.

But how do you get rid of internal conflict without doing something drastic like joining a monastery? A good starting point is to stop multitasking. According to neuroscience experts, multitasking actually slows you down by 50 percent and increases your chances of making a mistake by 50 percent.

For example, chatting on the phone while driving will lead you to hit the brakes 0.5 seconds slower than normal. If you do the math, at 112 kilometers per hour, your car would need an additional 15.5 meters to come to a complete stop — and a lot can happen in that space, at that speed.

Another smart move is to prevent your mind from wandering by giving yourself a purpose. That way, you can easily remind yourself why you wanted to concentrate on something in the first place.

In this case, the _PIC_ rule might come in handy: (P)urpose gives you a reason for learning. Take learning a programming language, for example. Your purpose there could be building a website for your family.

Once you've found a purpose, ask yourself questions so you become (i)nterested and (c)urious about what you've chosen to learn. "Is this currently relevant to my life?" or "Can I use this at work?" are great ones to start with.

So, now that you know how to improve your concentration, it's time to discover how to retain information, too.

> _"Cultivate curiosity, and life becomes an unending study of joy."_ — Tony Robbins

### 3. Use your creativity to bring information to life in a fun way that will help you remember it. 

Have you ever read a page of a book and then immediately forgot what you just read? Well, that never has to happen again.

An easy way to remember a bit of information is to bring it to life, and this is all about using your creativity to make a memorable movie or picture.

When it comes to words, there's a lot to play around with. They can often be broken down into smaller words which sound similar to others — that way, you can make them more memorable by making them sound funny or absurd. This is great when it comes to memorizing foreign words or capital cities.

Say you're trying to learn "pollo," the Spanish word for chicken. Picture yourself playing polo while riding on a massive chicken and you'll never forget it again! Or, let's take the capital of Australia, Canberra. If you're trying to remember it, visualize a KANgaroo eating some BERRies to help jog your memory. It takes some practice to think in this way, but it makes the process of remembering things much easier.

To make the pictures you've imagined super exciting, and therefore more memorable, just use the _SEE_ method.

Information always enters our brains via our (s)enses. So, for example, when you're trying to remember the word "horse," it's important to imagine its skin, smell, touch and even its taste. That way, you've created a multisensory image that's far easier to recall than five letters.

Up next is (e)xaggeration. Compared to an ordinary horse, you're more likely to remember a giant bright pink horse wearing a tux, right? Forget logic, it's all about making funny, fantastical images.

And lastly, (e)nergize. A horse galloping at full speed into the sunset is far more entertaining than one standing still in a stable. Adding a bit of action will make the information you're trying to retain a lot more vivid.

The key to remembering information, then, is to use your imagination to bring it to life.

> _"Your mind is the greatest home entertainment center ever created."_ — Mark Victor Hansen

### 4. Sort information into categories that already exist in your long-term memory. 

Now that you've discovered how to create memorable images, it's important to learn how to organize them in your mind. The best way to do this is to use the _loci method_, also knows as the _route method_, which is the practice of combining new information with something already familiar.

Humans are experts at remembering specific routes or places (just think about your commute to work), so most memory systems already make use of loci. It works by putting the items to be remembered at specific locations along a route that is already vivid and familiar in our minds.

Here's how you can put the loci method to work: Say you're trying to memorize bits of a speech you have to give. You would imagine walking around your house along a particular route. The idea here is to create a string of locations to visit as you go.

So, in each room you imaginarily venture to, pick three specific locations in the room, and put them in a specific order. In this way, as you go around your house, you combine each part of the speech with the locations you've picked.

Let's say you start in the kitchen and the first loci is your favorite pink breakfast bowl. You'd think of the horse in a pink tuxedo from the SEE method in the previous blink and remember that the word "horse" is in the first paragraph of the speech. To make it even more memorable, you could imagine spinning the pink breakfast bowl on its head!

Next, let's say the following paragraph in your speech includes the words "summer camp," which you remember by imagining your family pictures on the refrigerator.

This method is effective because it'll work with any structured location that you know well. Whether it's your car, your body or a museum, you can use your route to recall your list. If your route is intentionally structured to hold, say, five objects per room, then you'll remember both the list and its exact order.

And if you're still not convinced, consider this: the author used this very method to remember the first 10,000 digits of the number π (pi)!

> The loci method has been around for more than 2,500 years.

### 5. Use sounds to remember numbers and dates. 

It's always remarkable when a person can easily recall complex mathematical facts or historical dates at the drop of a hat. If you want to be able to do the same, here's a simple way to remember numbers that only requires three steps.

The idea is to transform numbers into images that'll stick in your mind.

First, you'll need to learn a system that changes numbers into letters or alters how those letters sound. Take the number zero, which can be represented by the letters s, z and c. Tough to remember? Think about a hissing wheel. After all, zero looks like a wheel anyway!

Each number from zero to nine can be assigned a group of similar sounding consonants. For instance, the number six can be represented by the sounds j, sh/ch or a soft g. You can also use the shape of the numbers when you assign the sounds to make them easier to remember. For instance, if you flip the numbers two and three on the side they look like the letters N and M respectively and if you flip the number 9 horizontally or vertically, it looks like a p or a b. Vowels are left out for now, but come into use later.

The next step in remembering a date or number is to write down the letters that represent them and make up a word.

Say you want to remember the year 1969, when humans first walked on the moon. Most memorable dates happened in the last millennia, so you only need to remember the 969 bit. Therefore, the sounds b, sh and p should come to mind. Together, they can be used to make the word BiSHoP.

The last stage is to make a picture for you to remember. Using the SEE principle, take the word you've created and your chosen date or number. For the above, a bishop dancing on the moon with Neil Armstrong should do the trick!

Now that you know how this system can transform numbers into words, it should be simple to create memorable images that you can use to recall particular numbers or dates.

### 6. Use the four C system to remember names. 

Is there anything more embarrassing than forgetting a colleague's name? Well, when it comes to memorizing this kind of information, just remember the _four Cs_ : concentrate, create, connect and continuous use. If you use this handy system, you'll never have to deal with this awkward situation again.

First and foremost, you've got to concentrate on the person's name if you want to remember and use it. Say the name out loud and then repeat it. If it's a tricky one, just ask for the spelling.

To help the name stick, pick it apart and use any words that come to mind to create a memorable image. Take, for example, the author's surname: Horsley. To remember his name, you might picture a fight between a HORSe and Bruce LEE.

Once you've sorted your image, connect it to the person's face whose name you're trying to remember. That way, seeing their face will immediately bring up their name.

A great way to match a face to a name is to use the _connection method_. If the person has a striking feature, connect that to the name you're trying to remember. If your colleague Janice has icy blue eyes, imagine icicles flying out of them!

But what happens if you already know someone with that name? This is where the _comparison method_ comes in. The key to this technique is to compare the new face to the one you know. Take two people with the name John. You could imagine both of their heads on one body, chatting to each other about how they look that day.

Whichever method you use, make sure you revisit the names you've learned so that you use them continuously. It would also be handy to list the names in your diary or add the people whose names you're trying to remember on your favorite social media channel.

You're now set to remember any information you want. In the final blink, you'll learn how to make sure it all sticks in your mind.

### 7. Stop yourself from forgetting information by reviewing it. 

Do you remember much of what you learned at school? According to the author's research, within two years of finishing school, people can only remember the equivalent of about three weeks' worth of lessons from 12 years of daily classes.

It's no surprise, then, that without training your memory you'll forget most of what you've learned.

To put it simply, all this brain training is pointless without a review process to make it stick. But what's the most effective way to do this?

Each time you revisit what you've memorized, the information you've absorbed has more of an impact in your mind than it did before. Therefore, to really make sure your brain never forgets what you've learned, you should gradually leave more time between one review and the next.

After learning something new, you should go over it after one day, then three days, then seven days later and so on. You should return to it two final times after you've had a two-month break and again following a subsequent three-month break.

Another point to remember is that when you're going through the process of reviewing, take advantage of the memory techniques you've learned. The SEE principle should have provided you with clear, vivid images to bring your information to life. Has the bishop still got moves on the moon? Make sure to use the tools you've got, otherwise you'll lose what you're trying to retain.

It's always a good idea to stay focused on your main goal, too. If you're learning a foreign language, booking a trip to that country could be the motivation you need to study extra hard.

When it comes to memorizing what you've already learned, the key is reviewing what you've done to prevent yourself from losing your newfound knowledge.

### 8. Final summary 

The key message in this book:

**To make information memorable, you need to bring it to life. The best way to do this is to connect what you learn with what you already know, use your imagination to form memorable images and engage your senses. Finally, by reviewing what you've stored in your mind, you'll make sure it's never forgotten.**

Actionable advice:

**Get off Facebook!**

Stay in the present by spending less time on social media apps — you'll find it much easier to concentrate and learn new information with a clear mind. In no time, you'll find that by using the memory techniques you've learned, you won't even need to write down your weekly shopping list!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The Memory Palace_** **by Lewis Smile**

_The Memory Palace_ (2012) is a step-by-step guide to using your spatial memory to help you remember absolutely anything. It teaches you how to build a palace of memories that will give you the power to recall everything you read, and even to memorize the names of every Shakespeare play in just 15 minutes.
---

### Kevin Horsley

Kevin Horsley is an expert when it comes to how the mind works. As one of the very few people to receive the title of International Grand Master of Memory, he currently works as a consultant for organizations around the world on how to approach learning, motivation and creativity.

