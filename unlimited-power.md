---
id: 584e94db3a41200004701638
slug: unlimited-power-en
published_date: 2016-12-16T00:00:00.000+00:00
author: Tony Robbins
title: Unlimited Power
subtitle: The New Science of Personal Achievement
main_color: E7C12E
text_color: 9C821F
---

# Unlimited Power

_The New Science of Personal Achievement_

**Tony Robbins**

_Unlimited Power_ (1989) is a powerful, useful guide to overcoming fear, uncertainty and the feelings of unworthiness that can plague your life. With a few mental and physical exercises to help generate positive thoughts and improve body language, you can achieve the goals in life that truly matter to you.

---
### 1. What’s in it for me? Unlock the full power of your mind and body. 

Do you have desires that you haven't yet achieved? The answer is undoubtedly yes! Many people walk through life feeling unfulfilled yet also believing there is nothing they can do to attain fulfillment.

In the worst case, such feelings can lead to depression and the abandonment of a person's deepest desires. But what if there was a way to be fulfilled, to achieve the things of which you always dreamed?

These blinks will show you that you already have the answers. The human mind is an extraordinary thing and finding fulfillment is simply a matter of unlocking your latent potential. These blinks will help you tap into that unlimited power.

In these blinks, you'll learn

  * why modeling yourself on another person can help you achieve anything;

  * why casting your eyes to the upper right can help you memorize things; and

  * how changing the position of your body can alleviate depression.

### 2. There is no such thing as failure, as each step you take offers you an opportunity to learn and grow. 

If you've ever suffered from depression, you probably know that one of the most powerful triggers is feeling like a failure. Perhaps you flunked a test or made a poor business investment and suddenly the cycle of darkness begins.

In reality, there is no such thing as failing.

Indeed, if your definition of failure entails not getting the results you'd hoped for, then you could call the world's most successful people "failures."

The difference between successful people and everyone else is that successful people see "failure" as an opportunity to grow and learn.

Consider this scenario: You leave home to start your own business, but it goes bust by the time you're 21 years old. Soon afterward, you lose a partner to disease; then in your 30s and 40s, you try to start a career in politics but end up losing one election after another.

You might call this life trajectory a string of failures and, in despair, give up entirely. Or you could do what Abraham Lincoln did. This narrative was his life, and instead of throwing in the towel, he learned from his experiences. He then went on to become one of the most celebrated presidents in US history.

Thomas Edison also didn't believe in failure. He never gave up in his quest to create the light bulb, even after 9,999 unsuccessful attempts.

Edison didn't consider his previous attempts to be failures. Instead, each one was a discovery that revealed a new way of how _not_ to create a light bulb.

Throughout history, successful people have learned from mistakes and have been unafraid to try again.

This is good advice for personal misunderstandings as well. Sometimes, even when we're trying to be positive and loving, our actions can lead to hurt feelings.

In these cases, instead of becoming defensive or blaming the other person for misinterpreting your words or actions, try to understand why your message was misunderstood.

Maybe it wasn't what you said but rather how you said it. Consider your tone of voice and how you expressed yourself. And once you figure out how the problem arose, be sure to try again.

### 3. You can control your brain and change the way you process your emotions to overcome depression. 

Before they've been cut and polished, gemstones look like regular rocks. Tough situations in life are similar to an uncut gem; it's up to you and your brain to polish them and give them brilliant facets.

In other words, you can't prevent difficult situations from happening, but you can choose how your mind processes and retains such events. You can consciously control your perception of the world!

For example, maybe you want to be an actor, but you balk as your inner voice says you're not talented enough to pass an audition. What your brain isn't telling you is that your skills can be improved and even perfected through classes and diligent practice.

Changing that inner voice can make all the difference. First, listen to what that voice is telling you. Next, instead of letting it tell you what you can't do, try making it tell you what you _can_ do.

If this doesn't work, try changing the tone of your inner voice. Make that voice appealing, even sexy. This trick can divest your inner voice of the power to cause fear and anxiety, making it easier to do the things you're scared of.

Such control can also be used to defeat depression.

Depression doesn't have to be inevitable. One way to prevent it from taking over is to ask yourself: What thoughts go through my mind and what happens in my physiology to create the feeling of depression? Being aware of how depressive feelings are created within you can help you regain control over them.

Negative thoughts often trigger or accompany depression. You may think of yourself as no-good or ugly or unsuccessful, or perhaps you say discouraging things to yourself in sad tones with your head hanging low, for example.

By paying attention to these thoughts and sensations, you can figure out which ones trigger your depression and catch them before they take over. Remind yourself that you are good, beautiful and not a failure, and take control of your inner voice. With these tools, you'll find that you have the power to ease your depression.

### 4. You can achieve anything through the five-step process of modeling. 

Have you ever admired someone and thought, "There's no way I'll ever be as good at my job as they are"? Well, don't sell yourself short — there are concrete methods to reach any goal.

By emulating the strategies, attitudes and skills of successful people, you can achieve similar results.

This is called _modeling_, and it allows you to achieve whatever skills you desire — even the ones that might not be in your field of expertise.

For instance, the author knows nothing about shooting guns, but he accepted an offer from the US Army to improve the shooting accuracy of its soldiers.

When he arrived, only 70 percent of the recruits were passing the final shooting test. So the author looked closely at the top performers and started modeling a new training program after them. He studied everything: how they thought, how they held their bodies, how they decided when to fire a shot.

By following the new program and modeling their behavior after the top performers, 100 percent of the new recruits passed the test with flying colors.

So how exactly did the author do this? Well, he follows a five-step process, and it works for just about anything.

Say you want to be a better downhill skier. First, find a gold medal skier, and study the exact movements that person uses to glide along the slopes.

Second, practice by imitating and reproducing these movements yourself, something many athletes do automatically.

Third, visualize the gold-medal-winning skier in your mind's eye. Picture every movement in detail until you have the full image of what perfect skiing looks like.

Fourth, picture _yourself_ in place of the expert skier in your visualization and imagine your own perfect movements. You might see yourself fall or struggle at first, but continue visualizing until you're just as good as the expert.

Fifth and finally, take it to the slopes. You'll find that the visualization pays off in the real world — and your performance will improve.

### 5. Use eye movement to activate your brain for improved memory and creativity. 

Here's another exercise for your mind's eye: picture the face of someone who is thinking. There's a good chance that you pictured someone with their eyes pointed up and toward the right.

You'll probably notice, too, that this is a typical expression for a cartoon or comic book character who's thinking.

Try asking yourself a tricky question, such as how many windows your house has, and see what happens. You probably looked up and to the right as you tried to picture each room and counted. This is a standard reflex, triggered by any attempt to remember something. (For some left-handed people, the eyes will go up and to the left.)

Here's another exercise: imagine what you would look like with three eyes; or picture a creature that has the head of a lion, the body of a lizard and a scorpion's tail.

This time, you'll notice that your eyes only move sideways, not upward. That's because these are new images, not ones stored in your memory bank.

Believe it or not, knowing about these eye movements can actually improve your memory and creativity.

The next time you're trying to remember something, like the ingredients for a complex recipe, look up and to the right. This will help you tap into the part of your brain that stores memories — and hopefully result in a tasty meal!

On the other hand, if you are asked to develop an advertising campaign for your company's newest product, you can help yourself out by looking to the right. Instead of reaching into your memory bank, this will tap into the creative part of your brain that visualizes new solutions.

### 6. By finding the right method of communication, you can make sure your partner receives your love. 

You probably heard about different learning styles in school — some people retain information better by reading, others by listening or doing.

Well, the same applies to romantic partners. When your style of communication differs from your partner's, it can lead to unfortunate misunderstandings.

People who prefer auditory forms of communication will consider what they hear and say more important than what they see. Therefore, when they communicate their love to their partner, they'll focus on talking and using terms of endearment.

By contrast, a visual person will express love for a partner by writing love notes or buying jewelry and flowers. Such people rely on visual cues to get their message across. But these visual and physical gestures might be lost on a partner focused on auditory communication — a person who won't feel fulfilled until love is expressed verbally.

In order to figure out which type of person your partner is — be it auditory, visual or kinesthetic, which is someone who prefers touch and physical communication — you can ask some simple questions.

First, ask your partner about a memorable time when he or she felt completely loved.

Then, ask about what led to that feeling: Was it something someone said, or perhaps a hug or a surprise gift that made them feel so loved?

Your partner might be both visual and kinesthetic, in which case you will want to use both touch and visual communication. Or they might be auditory and visual, which will require speaking and touching. So be sure to try combining multiple ways of expressing your love.

### 7. You can change your body language to fight depression and boost energy. 

Ever notice that after a bout of crying, we stop the tears by raising our heads and breathing deeply through our noses and into our chests?

This is just one way we can use our body to control our emotions.

Another adjustment to your body can provide you with new energy when you're feeling tired.

We often give in to tiredness, and our productivity can suffer as a result. But there are steps we can take to help get a second wind.

The key to fighting off tiredness is recognizing the signs that accompany this feeling: our muscles will start to relax, our shoulders will slump and our jaws will begin to slacken.

When you get used to paying more attention to your body, you can notice these signs as they emerge and you'll be in a good position to reverse them.

You can do this by adding more tension to your muscles — pull back your shoulders and open up your chest, and smile. This posture will induce a boost of energy, enlivening your body and mind. Soon enough, you'll be ready for action.

Your posture can also help fight feelings of depression.

We've explored the power of positive thinking in previous blinks, but many people have also found it useful to fight depression by using their body.

When depressed, we often hold our body in a particular way. In the _Peanuts_ comic strip, one of the characters refers to a certain posture — shoulders slumped, head tilted toward the floor — as his "depressed position." He says that this position is ideal for gloominess, because if he straightened up and looked out at the world, he'd feel better and would no longer be able to enjoy his depression.

There is a lot of truth to this. By simply raising your head and breathing deeply, your chest will open up and you'll move with more energy. And once you start doing that, it's only a matter of time before the depression fades away.

### 8. The power of conviction comes when you align your body and mind with a confident plan of action. 

Imagine being on the front lines of a fierce military battle and having your commanding officer pull the troops together to lay out a new plan of attack. Now imagine that the general laid out this plan while looking at the ground, mumbling in a barely audible voice, shuffling his feet and gesturing limply. Doesn't inspire much confidence, does it?

The same holds true for your own life. Confidence and personal power come to those who are convinced that their plan is a good one.

Instead of spending your life worrying about what the right choice is, make _a_ choice and follow through on it with complete confidence.

If your plan is full of "maybes" and "mights," your body will mirror that insecurity. Your posture will be slumped and your gaze unfocused. And you'll be confused because your brain will be constantly toggling between wanting to do something and not wanting to do it.

So develop your power of conviction by aligning your thoughts and your body with confidence.

When you give yourself a plan and declare, "I am going to do this," your body will mirror that focused determination. Next thing you know, you'll be holding your head high, your eyes will be focused and sharp and everyone will believe and trust in you, including yourself.

You can help develop your powers of conviction by modeling yourself after charismatic leaders.

Charisma is apparent in those who are assured and confident, and you can see it in the way they speak and carry their bodies.

You can practice the technique of modeling with a friend. Get a friend to recall an intense past event, and pay close attention to how their mannerisms change as they remember. Then begin mirroring everything — their posture, their expression, the movements of their eyes and tempo of their breathing.

Once you've got the process mastered, you can start observing charismatic actors and business or political leaders and begin making their body language your own. Once you can do that, you'll be well on your way to increasing your own personal confidence and power.

### 9. Final summary 

The key message in this book:

**You are not a victim. What happens in your life is the result of your own attitude toward it and how you treat yourself and your body. If you want to change your life, start changing your attitude. By making simple alterations to how you respond to situations and your emotions, you'll find you can achieve anything you want.**

Actionable advice

**Breathe deeply to gain energy**

If you ever feel that you don't have the energy to achieve a given goal, deep breathing will help. Effective breathing has also been shown to activate your immune system and can even prevent the growth of cancerous cells. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Awaken The Giant Within_** **by Tony Robbins**

_Awaken The Giant Within_ (1991) argues that, ultimately, we're all in control of our own lives, and that by changing our habits, controlling our emotions and believing in those things we want to believe, we can make our ideal life a reality.
---

### Tony Robbins

Anthony Robbins is a self-help expert and philanthropist who has counseled innumerable people, including employees of major companies such as IBM, AT&T and American Express, top athletes and celebrities. His live events have become world-renowned.

