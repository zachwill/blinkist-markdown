---
id: 5dfcbfb76cee07000788a274
slug: unlocking-creativity-en
published_date: 2020-01-01T00:00:00.000+00:00
author: Michael Roberto
title: Unlocking Creativity
subtitle: How to Solve Any Problem and Make the Best Decisions by Shifting Creative Mindsets
main_color: None
text_color: None
---

# Unlocking Creativity

_How to Solve Any Problem and Make the Best Decisions by Shifting Creative Mindsets_

**Michael Roberto**

_Unlocking Creativity_ (2019) is a guide to unleashing creativity in the workplace. Starting with the premise that all people are innately creative, it argues that the best way to unlock employee creativity isn't to hire all-star creatives or restructure your businesses. Instead, the key is to dismantle the workplace mind-sets that are stifling the creativity of the people already in your midst.

---
### 1. What’s in it for me? Discover the six mind-sets that are preventing creative greatness in business. 

Here's a little test. Ask some of your colleagues which quality they think is most valuable at work. In all likelihood, one quality will get mentioned more than any other: creativity.

Back in 2010, IBM did the major-corporation equivalent of this test. It asked 1,541 executives and other leaders this same question. You guessed it, 60 percent of leaders, a considerable majority, said creativity contributed more to success than any other attribute.

This opinion makes sense. The current market landscape is defined by innovation and disruption. If a company can't come up with creative solutions to the market's complex, quicksilver problems, then that company is doomed to failure.

But here's the thing: though most leaders talk the creative talk, they don't walk the creative walk. They say they want creativity, but in the meantime, they perpetuate workplace structures and practices that _discourage_ creativity.

These blinks explain the six main mind-sets that hamper creativity at work, and offer a handful of techniques for resisting them.

In these blinks, you'll also find out

  * why businesses should emulate Dave Grohl;

  * what sort of employee Leonardo da Vinci would have been; and

  * how Mark Twain approached the problem of focus.

### 2. Counteract the linear mind-set by incorporating an iterative approach to product creation. 

Imagine you're a team leader. Recently, you've been having difficulty with a member of your team, a talented but eccentric guy. He's got a history of failing to meet deadlines, sometimes simply abandoning assignments altogether. A perfectionist, he also iterates obsessively, tinkering with prototypes rather than delivering.

You like the guy, but it's just not working out. So you call Leonardo da Vinci into your office and fire him.

If you're thinking, "Hold on, I'd never _fire_ one of the most brilliant people of all time," then pause a moment. Yes, history recognizes Leonardo da Vinci as a genius, but the man who painted the _Mona Lisa_ probably wouldn't fair too well in the current market landscape. He was a chronic procrastinator, extending deadlines for years, and sometimes never meeting them. 

Most of today's leaders would fire Leonardo for a simple reason: he had an _iterative mind-set_, and they have a _linear mind-set_, which is the first creativity-blocking mind-set we'll explore.

The linear mind-set approaches projects like this: First, you do your research. Then you analyze the data you've collected. After that, you make an execution plan based on your analysis. Next you create a budget for the plan. Then, you execute it.

No one can deny the importance of research and analysis, but there's no way to make a perfect plan prior to embarking on a project. Unforeseen obstacles will rear their ugly heads. Unimagined problems will arise.

That's why it's better to adopt a less linear approach to production. First, build a working prototype. Then launch it. After that, evaluate its reception. Then adapt based on that evaluation and build again. And then repeat the process.

Skype's Director of Design, Peter Skillman, once created a design challenge that made the effectiveness of the iterative approach particularly apparent. Here's the challenge: using 20 pieces of uncooked spaghetti, a bit of string, some tape, and a marshmallow, make the tallest structure you can. The twist: the marshmallow must sit atop the completed tower.

Business school graduates ended up being among the worst at this challenge. They treated the task linearly, devising a concrete plan and then sticking to it. More often than not, their towers toppled when the marshmallow was placed on top.

Six-year-old children proved to be much better tower builders than the MBA holders. Da Vinci-style, the kids iterated, using the time not to plan, but to experiment.

### 3. Avoid the benchmarking mind-set by striving to be less like the competition. 

Creativity almost always begins as imitation. This often isn't a bad thing. As an example of fruitful imitation, consider former Nirvana drummer and current Foo Fighters lead singer and guitarist Dave Grohl.

As a child, he taught himself guitar by playing along to famous Beatles songs. Later, when learning to play the drums, he imitated techniques used by Ringo Starr, the Beatles' drummer.

Imitating the Beatles taught Grohl about the basics of creating quality music. But he realized that imitation isn't the same as creativity. According to Grohl, when making music, you've got to "be yourself." In other words, you must be authentic.

By building on what he'd learned from the Beatles, Grohl wrote multiple Emmy Award-winning songs. Since 2007, he has collaborated numerous times with Beatles legend Paul McCartney!

Imitation is good when it leads to authentic creation. Unfortunately, many businesses never push beyond the copycat phase. They get stuck, believing that what works for someone else will work for them.

Call it the _benchmarking mind-set_. A company realizes its performance is weak in a certain area. So it analyzes the competition, notes its strengths, and seeks to improve performance through emulation.

Reality television provides a great example. In 2000, the reality show _Survivor_ aired for the first time. It was a surprise hit, with a finale that attracted some 51 million viewers. In the wake of its success, tons of new reality shows began hitting the airwaves. Some, such as _American Idol_ and _The Bachelor_, achieved success season after season, but hundreds of others were one- or two-season flops.

The problem is that benchmarking leads to homogenization. Most of those reality shows, for instance, simply resembled every other reality show, giving viewers little reason to tune in. The same thing often happens to businesses.

One way to avoid becoming indistinguishable from every other business in the market's bland herd is to favor _lopsidedness_. That is, rather than matching the competition move for move, focus on bolstering your own unique strengths. 

Take chainsaw manufacturer Stihl as an example. Most companies in the chainsaw business outsource their labor to reduce costs and sell their products at huge retailers like Home Depot. Stihl does the opposite. The German company manufactures all parts in-house, rather than outsourcing, and sells its saws through a network of select dealers.

These choices have kept the quality of their saws high and also helped them chisel out a niche in the market — which is probably why Stihl is the best-selling chainsaw brand out there.

### 4. Steer clear of the prediction mind-set by prioritizing product quality in the here and now. 

There's an odd catch-22 within most businesses. For a team to get funding for a particular project they have to provide a profit forecast to company executives. If that profit forecast isn't high enough, they're unlikely to get any funding. However, if they increase the profit prediction to secure project funding, then they run the risk of failing to hit those numbers and, as a result, getting terminated.

Why does this happen? The main culprit is the _prediction mind-set_.

Many executives try to predict company growth. What's worse, their predictions tend to be based on unreasonably high growth objectives. A 2001 article published in _Fortune_ notes that many large companies strive to increase earnings by 15 percent every year. This number just isn't realistic. Between 1980 and 1999, only five of the 150 biggest businesses in the United States grew at that rate. Between 1960 and 1999, the post-tax earning average for most major corporations was more like 8 percent. 

Aiming for 15 percent growth isn't only unrealistic, it also nips smaller projects in the bud. Many executives will only approve projects that promise to generate at least $50 million in sales. Meanwhile, smaller projects that might take more time to become profitable are left to rot on the vine.

However, sometimes ideas need time to ripen. There's simply no way to predict how long that ripening will take.

Trader Joe's is an interesting case study. If you weren't familiar with this wildly popular American grocery store, its approach to business might not sound very promising.

It doesn't advertise on television or social media. It offers about one-tenth as many items as most supermarkets — and the items it does offer are private-label. There's not even a guarantee you'll find your favorite item, because the product inventory changes regularly. What's more, its stores are small and cramped, with limited parking.

Sounds like a recipe for disaster, right? And yet, when more than 11,000 Americans were asked to name their favorite grocery store in 2018, Trader Joe's came out on top. 

But Trader Joe's didn't hop to the top in a single bound. Twenty-two years after the first store opened in 1967, there were a mere 30 stores, all in California, and annual revenue totaled $150 million.

But founder Joe Coulombe wasn't troubled by this slow growth. Rather than worrying about the future, he put all his energy into creating a fantastic shopping experience for his customers.

Today, annual revenue is $11 billion, and there are 470 stores nationwide.

### 5. Psychological safety within a company is more important than company structure. 

In 2015, the CEO of Zappos, Tony Hsieh, issued a memo to all employees. It was time, he wrote, "to eliminate the legacy management hierarchy." What followed was a major change in the company's structure.

The transition went far from well. Confused and frustrated by the new structure, more than a fifth of Zappos's employees left the company. In 2016, Zappos wasn't included on _Fortune_ 's "Best Companies to Work For" list, though it once ranked sixth.

What's the moral of this cautionary tale? Well, many leaders, Hsieh included, treat structure like a silver bullet. Get the structure right, the thinking goes, and optimal work will follow. This is the _structural mind-set_ in a nutshell.

The truth isn't so black and white. As a 2012 study published in _Psychological Science_ points out, structural effectiveness is task dependent. In the study, participants were divided into three groups. Two of those groups were egalitarian; one contained all "high-power" members, the other all "low-power" members. The third group was hierarchical, with low-, medium-, and high-power members.

Then the groups were given two tasks. The first task required almost no interdependent coordination, and the three groups performed about the same. In the second task, which required considerably more interdependent coordination, the group with a hierarchy performed much better.

The fact is, when people need to coordinate actions to achieve a task, a hierarchical structure can be more effective than its egalitarian counterpart. In contrast, when less coordination is needed, a flattened management system works just fine. 

So, if there's no structure that will boost performance and creativity in all situations, how can you encourage creativity within diverse structures? You might be thinking that it's all about employing the right people and building teams of creative superstars. But this isn't the case.

A few years ago, Google's Julia Rozovsky set up the People's Analytics team, which set out to pinpoint the qualities that differentiated Google's highest-performing teams from the less successful ones. Of the five qualities they identified, an environment of _psychological safety_ ranked far above the rest.

Team members will feel psychologically safe when they're encouraged to speak up about issues, when they know it's OK to fail, and when they feel comfortable taking risks around their peers.

Every company is filled with creative people. You don't need to hire rock star creatives. You don't need to restructure. What you need to do is create an environment where the people in your midst feel safe to spread their creative wings.

### 6. The focus mind-set can be avoided by taking breaks to focus on other projects. 

In 1983, the Irish rock band U2 released its fourth studio album — _The Unforgettable Fire_. Hailed by critics as a fantastic creative breakthrough for the band, the album reached the top of the charts in the United Kingdom.

How did U2 record this landmark album? Focus. While recording it, the band lived together in Slane Castle, Ireland, creating an environment of hyperfocused creativity. The result speaks for itself.

Major businesses have taken a similar approach to creativity. In 2009, Jake Knapp organized a "creativity sprint," where Google employees got together for five days and focused on a single problem. Since then, other companies, including Facebook, Airbnb, and Uber, have adopted similar sprints, carving out five-day periods of unadulterated focus.

These sprints are intended to do one thing: eliminate that most insidious of creativity assassins, distraction.

While working, most people are barraged with distracting interruptions. Coworkers pop by for a chat, or your focus gets derailed by an email or social media. When these distractions relate to the task you're working on, they can be harmless. On the other hand, according to a 2005 study by researchers at the University of California, Irvine, when they're unrelated to the work at hand, it can take more than 23 minutes to get back on track. 

Focus is a definite good. However, there is such a thing as too much focus. Fixating on focus — regarding it as a supreme ally while treating distraction as the ultimate enemy — is the hallmark of the _focus mind-set_.

In reality, focus is not an unimpeachable virtue. Focusing on a single task without a break can lead to a depletion of energy. At some point, the creative tank runs dry.

The best way to beat the focus mind-set is to interleave periods of focus on one project with periods of focus on something else.

Mark Twain offers us a prime example. Over the course of the summer of 1876, he wrote 400 pages of the book that would become _Adventures of Huckleberry Finn_, widely considered one of the best novels in the American canon.

But then he stopped. According to Twain scholar Henry Nash Smith, the story had become unexpectedly deep and complex. Instead of redoubling his efforts, Twain set the project aside, allowing himself to work on other projects. Seven years later, he returned to it, finishing off the masterpiece that we know today.

The lesson? More focus is not the answer to all roadblocks. It may well be better to set a project aside and allow solutions to slowly germinate than to continue to pound a problem with ceaseless creativity sprints.

### 7. Naysayers are most effective in pairs and when they ask encouraging questions rather than promoting a particular agenda. 

Contemporary culture encourages critique. People are prompted to leave reviews on Amazon, YouTube, Yelp, Uber, and countless other platforms.

This tendency goes deep. In high school and university, teachers emphasize the importance of critical thinking. As a result, students spend the lion's share of their time identifying weak points in the work of others rather than developing their own ideas. Critical ability comes to be equated with intelligence, resulting in the faultfinders, rather than the solution creators, reaping all the praise and promotions.

Call this mentality the _naysayer mind-set_. It's not that naysayers aren't important. Indeed, the author's own research indicates that having a devil's advocate on a team lessens _shared information bias_ — the tendency of teams to discuss and focus on shared information rather than concentrating on, say, information possessed by a single team member. The whole idea of a team is to bring many perspectives to a problem, a goal hindered by shared information bias.

But a naysayer shouldn't be a creativity killer. Team members become less likely to speak up when they know that someone is waiting to pounce on their proposal's every weakness. On the other hand, devil's advocacy can become ineffective when it always comes from the same person.

Just take the example of Henry Wallich, who served on the Federal Reserve Board of Governors in the 1970s and 1980s. In an attempt to reduce inflation, he cast 27 dissenting votes during his tenure — a record in the Federal Reserve's history. What happened? His fellow board members ignored him. Henry Wallich's persistent message, because it was predictable, fell mostly on deaf ears.

There are a few ways to reap the benefits of devil's advocacy while steering clear of its pitfalls. One is to have different team members serve as naysayer from meeting to meeting. This way, other members won't disregard criticism because it comes from one source. Another is to designate two naysayers, as it's harder to shrug off critique when it comes from more than one person.

Lastly, make sure that devil's advocates deliver criticism in a constructive manner. Naysayers are there to help, not hinder, the creative process. So encourage naysayers to ask probing questions that spark people's imaginations and sharpen their thinking, rather than simply throwing cold water on their ideas. 

Every workplace is filled with creative individuals. It's up to leaders to create an environment where they feel safe to come forth and flourish.

### 8. Final summary 

The key message in these blinks:

**Creativity in the workplace is more important than ever. Unfortunately, many work environments discourage people from taking creative risks. There are six mind-sets that hamper workplace creativity. You can empower workers to come up with their own creative solutions by challenging these mind-sets and creating an environment where everyone feels safe to make creative suggestions.**

Actionable advice:

**Take a tip from improv!**

All too often, new ideas are met with a disheartening response. People tend to say, "Yes, but…" — as in, "Yes, but we tried that last year, and it didn't work so well." You can counteract this tendency by following the example of improvisational comedy. In improv, people are taught to always say, "Yes, and…" This keeps the scene rolling, and often leads to some pretty entertaining scenarios. So why not try the same approach at work? Next time someone comes up with an idea, don't shoot it down with a but; build on it with a yes!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Farsighted,_** **by** **Steven Johnson**

No one can deny that creativity is an important attribute in the workplace. But it's still just one ingredient in the business-success recipe. There are many other ingredients, and one of the most crucial is how leaders approach decision-making.

If you'd like to learn more about cooking up an excellent approach to business, then we recommend the blinks to _Farsighted_, which lays out the most common decision-making pitfalls as well as the cleverest decision-making strategies.
---

### Michael Roberto

Michael Roberto is the Trustee Professor of Management at Bryant University in Rhode Island. He teaches business strategy, managerial decision-making, and leadership, and is the author of multiple books on those subjects, including _Why Great Leaders Don't Take Yes for an Answer_. 

© Michael Roberto: Unlocking Creativity copyright 2019, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

