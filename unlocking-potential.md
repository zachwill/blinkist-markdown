---
id: 56143eae3632330007410000
slug: unlocking-potential-en
published_date: 2015-10-09T00:00:00.000+00:00
author: Michael K. Simpson
title: Unlocking Potential
subtitle: Seven Coaching Skills That Transform Individuals, Teams & Organizations
main_color: E32D35
text_color: E32D35
---

# Unlocking Potential

_Seven Coaching Skills That Transform Individuals, Teams & Organizations_

**Michael K. Simpson**

_Unlocking Potential_ (2014) outlines practical coaching tools to help leaders, managers or supervisors better engage their teams and transform their organizations. It's simply the most comprehensive guide to becoming a great coach!

---
### 1. What’s in it for me? Learn how to coach your team toward success today and every day. 

In today's always changing business environment, it is crucial that employees continue to learn and develop a wide variety of skills. 

And yet, this is easier said than done. As a business leader, you may have the desire to learn and inspire your team to do the same, but actually making this happen isn't straightforward. 

Help is here for you! A great coach can create a transformative experience for you and your team in a surprisingly short period of time. In essence, a coach can turn average into exceptional!

How do you harness the power of a great coach to take your team to the next level? These blinks will teach you the tips and tricks of top coaches so you can realize your business goals today. 

In these blinks, you'll discover

  * why the best feedback a coach can give doesn't come from a coach at all;

  * why professional and personal missions need to be in sync; and

  * how "going with the flow" will make you more productive.

### 2. The first two coaching principles are trust and unlocking potential. You need both to succeed. 

If you're a business leader, it's your job to maximize the potential of your team. But how do you go about it? 

The answer lies in _coaching_. 

Coaching is a unique style of leadership. It's not just concerned with consulting and advising, but instead is about unlocking the potential of another human being. 

Yep, quite a hefty task! Before you consider coaching for your team, it's important that you keep the four foundational principles of coaching in mind. 

Let's start with the first two. 

A coach can't succeed unless his team _trusts_ him. As the first principle of coaching, trust is vital because it facilitates good communication. Two people that share trust are more likely to be open and honest with each other. Secrets or lies don't pose a threat to their bond. 

For coaches this is important, as a coach needs to know exactly what makes his team tick. A coach requires _total_ honesty from his clients. But he won't get it unless he presents himself honestly — trust is of course a two-way street! 

The second key principle of coaching is about _potential._ What does this mean? Coaching is based on the assumption that each person has the potential to grow and become something better. 

Unlocking someone's potential begins with close observation. Listen carefully to what the person is saying, and the way she is saying it. Is she speaking too softly? Or avoiding eye contact? Maybe she's just keeping her head down, or is she perhaps quick to anger? 

Each of these aspects allows a coach to discover the _paradigms,_ or views of the client, what she thinks about the world and herself, which can affect (often negatively) the way she acts and behaves. 

For example, the paradigm of a person who exhibits low self-esteem might be that she lacks crucial life skills and so can't ever achieve anything worthwhile. The job of a coach is mine for this information which can then be challenged, to show the client her true potential.

### 3. Ask powerful questions to create commitment. Encourage a flow state to execute effectively. 

With a good grasp of trust and potential, we can move to the next two principles of coaching. These are _commitment_ and _execution._

Let's start by unpacking commitment. Why is it important? 

A person's motivation to achieve long-term goals stays strong only when she has a firm internal _commitment_ to those goals. One way to create lasting internal commitment is to ask powerful coaching questions, and listen actively. 

Consider these questions for your clients. What are the most important strategies and goals you need to accomplish, personally and professionally? Do you want to make a difference in the world? Are you looking to improve your professional skills, every day?

When people become aware of their own goals and importantly, why they want to achieve these goals, their purpose becomes tangible and their will to commit becomes stronger. 

Once commitment has been established, it's time for the fourth and final principle: _execution._ How do coaches approach this crucial step? 

Remember: the job of a coach is not to push or pull a client toward fulfillment. Instead, a coach's job is to ensure that the client can get where she wants to go herself. The best way to ensure this is for the coach to help the client learn to work in the _flow_ state. 

A flow state is when you feel totally absorbed with a task and completely focused, with your mind, body and soul. In the flow state, you get the most done and you move naturally toward your end goal. 

Athletes often achieve a flow state before and during an event, which allows them to free themselves of distractions and finish as strongly as they can.

Now that you know the four principles of great coaching, let's take a closer look at the skills involved in coaching. How do coaches put these four elements — trust, potential, commitment and execution — into action? Read on to find out.

### 4. Coaching requires credibility, and the ability to challenge a client’s attitude. 

It's important for a coach to approach building trust correctly. This is not easy! Coaches need to keep a few things in mind when developing trust with a client.

A trustworthy coach is also a credible person. As a coach, you need to develop both _character_ and _competence._ Character refers to personal integrity, in terms of qualities such as honesty and modesty. Competence, on the other hand, refers to your skills and capabilities. 

Good coaches demonstrate strong character and competence side by side. Let's say you're an honest person, yet a terrible manager. If you're always late and perpetually forgetful, do you think your team will trust you? Of course not. True credibility stems from a harmonious balance of character and competence. 

Like developing trust, unlocking a client's potential also requires a considered approach. It begins with challenging _paradigms_ that are inaccurate and that limit a client's performance. But how much do personal thoughts really influence performance?

Think about it. If someone tells you you're under-performing compared to your coworkers, how you think about this will become a source of pressure. You may become stressed, and this will affect how you perform in the future. 

Coaches must constantly challenge a client's negative thoughts. Granted, this doesn't mean that you should be especially tough on a client with low self-esteem, as that's defeating the purpose!

It all comes down to asking the right questions. Great coaches know how to ask questions that increase a client's self-awareness, so the client can discover the opportunities to create change.

One paradigm-challenging question might be: "You seem to be assuming that such-and-such is the case. Why exactly is this the case?" Coaches help clients reflect on their own assumptions, where these assumptions come from and importantly, whether they are reasonable. 

Once this is achieved, a follow-up question would be: "What are the alternatives to doing this?" This may start a discussion to help the client to develop new, sustainable approaches.

### 5. Manage the day-to-day without losing sight of the long-term. Coaches help keep this balance. 

In general, it can be hard to know in which direction you want to head professionally, as long-term goals sometimes can seem vague or unrealistic at best. 

As a coach, the task of guiding others to achieve their own tentative aspirations is certainly challenging. Yet great coaches get the job done. How do they do this?

Coaches help you clarify your personal and professional missions, and outline strategies for achieving them. With no clear mission to guide you, you can end up clutching at notions that seem attractive yet elusive, such as fame or money. A coach is there to remind you that this time should be spent discovering and defining your own personal and professional missions. 

Moreover, it's vital to consider personal and professional goals simultaneously. Focusing on just one creates issues later on. For example, focusing on professional goals by working tons of hours or taking on extra responsibilities may clash with personal goals of spending more time with family.

To help a client find a better work-life balance, a coach needs to help the client outline both professional and personal missions in harmony. Of course, it's one thing to help a client define goals; achieving them is another feat altogether. 

Research conducted by consultants Michael Mankins and Richard Steele revealed that, on average, company strategies only provide 63 percent of the financial performance promised. 

If missions and strategic goals are not followed through, they fade into mere hopes and dreams. So how do great coaches keep goals a reality?

They start by helping a client identify urgent, day-to-day activities and long-term strategic goals. People spend too much time in the "whirlwind" of everyday demands. A coach should help a client learn the discipline to keep long-term strategies in mind, to achieve those goals sooner.

> _"It is better to have Grade B strategy with Grade A execution, than the other way around."_

### 6. The best feedback you can give is feedback that comes directly from your client’s own mouth. 

If you want to improve your career path and your life's path, you'll need some sort of regular feedback from others. 

Coaches are, of course, no different. If a coach wants to really help a client, he'll have to offer lots of feedback along the way.

Yet negative or poorly formulated feedback is discouraging and is something every coach should avoid. 

So how can a coach formulate feedback to make it relevant, worthwhile and actionable? 

A great coach asks people to give _themselves_ feedback first!

Ask, for example: What do you like about the action you have just taken? Only after the client evaluates her own actions, should a coach offer his factual observations and suggestions. 

Why do it this way? It is more likely that people will take ownership of feedback if they speak it themselves. The adage is true: "You can lead a horse to water, but you can't make it drink."

Honest and helpful feedback, combined with coaching, can lead to effective change, but it is the coach's task to prepare the client to receive it. All too often people have negative associations with receiving feedback. 

Therefore, focus on the positive and play to people's strengths. Consider the following questions: What areas do you see as your greatest strengths? How do you think you can focus on them?

This creates a safe zone to explore the client's potential. Being able to approach the feedback process openly and positively energizes both coaches and clients.

### 7. A great coach taps the stores of innate talent and helps move the “middle” toward greatness. 

Do you know where your talents lie? Most of us have a general idea about what we're good at. But did you know there are many areas in which our skills are unknown and untapped? 

Great coaches seek to unleash and leverage people's innate talents. Through conversations, coaches are able to inspire behaviors and attitudes needed to bring these talents and strengths to light. 

These conversations center on performance progress, as a coach can map out clear measures to define what counts as success and what doesn't. During an improvement conversation, a coach must be honest and direct about goals, while keeping feedback encouraging and constructive. 

At the end, however, the coach steps aside, clearing the path for a client to do the job she was hired to do and thereby avoiding the negative effects of micromanagement. 

In his book _My Way or The Highway_, author Harry Chambers found that 62 percent of employees have considered changing jobs because of micromanagement; while 32 percent actually quit.

Great coaches also focus on well-performing individuals stuck in the "middle." These people are capable of achieving greatness but seldom get the attention they need to blossom. 

In their book _Winning: The Answers_, Jack and Suzy Welch say that every company has a low-performing group (10 to 20 percent of employees), a middle-performing group (60 to 70 percent) and a high-performing group (10 to 20 percent). 

Organizations benefit from fostering this middle-performing group, as it makes up a majority of employees. Think about it! By helping this group work more effectively, you'll be making huge strides toward company productivity. 

This is just one of the many ways in which coaching can have a far-reaching impact!

### 8. Final summary 

The key message in this book:

**Coaching is about tapping into a person's innate potential and achieving goals. From trust to potential to commitment and finally execution, coaching succeeds when these principles are well-defined. Great coaches use their skills to challenge the status quo, helping a client approach goals with clarity and setting up a framework for a client to succeed on her own steam.**

**Suggested** **further** **reading:** ** _What Got You Here, Won't Get You There_** **by Marshall Goldsmith**

Your people skills become increasingly important the further you climb up the ladder of success. _What Got You Here, Won't Get You There_ (2007) describes some of the bad habits that commonly hold back successful people and explains how to change them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael K. Simpson

For more than 25 years, Michael K. Simpson has been a lauded executive coach, working with some of the world's top businesses. His co-authored and published works include _Ready, Aim, Excel_ and _Your Seeds of Greatness_.

