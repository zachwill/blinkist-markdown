---
id: 59ef20c6b238e100064554d5
slug: unmarketing-en
published_date: 2017-10-25T00:00:00.000+00:00
author: Scott Stratten and Alison Stratten
title: UnMarketing
subtitle: Everything Has Changed and Nothing is Different
main_color: D8B889
text_color: 8C662E
---

# UnMarketing

_Everything Has Changed and Nothing is Different_

**Scott Stratten and Alison Stratten**

_UnMarketing_ (2009) lays out a new approach to marketing that goes beyond typical methods like cold calling and ads. These blinks explain how, with the help of new, more sophisticated tools, businesses can build relationships with their customers to engage them in a more natural and effective way.

---
### 1. What’s in it for me? Learn how to truly engage with your customers. 

Do you enjoy getting cold calls from telemarketers? Do you relish finding flyers in your mailbox? Do you simply love receiving marketing email after marketing email? Well, if you answered yes to even one of these questions, you are truly a rare specimen. Practically no one likes marketing in any of these forms.

If you want to keep your business relevant, your approach to marketing must adapt to this reality.

In these blinks, we look at marketing from a new perspective and challenge many of the shopworn marketing assumptions. It's time to start thinking about business in a new way — and that's precisely what these blinks will help you do.

You'll also learn

  * why you should focus more on existing customers than on acquiring new ones;

  * what the hierarchy of buying is; and

  * that you should showcase your expertise via content.

### 2. The best marketing is based on relationships and communication with your customers. 

Imagine getting home after a stressful day on the job; all you want to do is relax on the couch with a delivery pizza and your current Netflix favorite. You order dinner and prepare to decompress, but 30 minutes go by, and your pizza still hasn't arrived. Then it's been an hour. And then, when the delivery guy finally _does_ show up, he's brought the wrong order.

Clearly, the restaurant is not doing well and could use a serious dose of _unmarketing_, a new take on how companies should interact with their customers.

In most companies, marketing is discussed in secret meetings run by specialized departments. But, actually, every single engagement your company has with current or prospective customers, including private conversations about your business, is a moment for marketing.

Just consider the pizza scenario. This actually happened to a customer in Chicago and, unsurprisingly, she complained about the frustrating experience on Twitter. When Domino's saw her post, they sent a fresh pie and even made a personalized video of the manager apologizing for the mistake — and that video ended up getting over 100,000 views.

This is a great example of how good marketing focuses on interaction. But interaction in the service of what?

Well, your primary focus should be building relationships, specifically those that can last a lifetime. So rather than focusing on attention-grabbing ads, you should spend your limited resources on connecting with customers.

Then there's the story of the guy who ordered nine pairs of shoes from Zappos for his sick mother because he didn't know her size. When she eventually passed away, he kept forgetting to return them and, after several weeks, pleaded with the site to take them back.

Zappos arranged a UPS truck to pick them up, refunded the full purchase amount and offered their sincere condolences for his loss. Because of this special treatment, this customer will be loyal for life, spreading the word about Zappos to everyone he knows.

### 3. People buy things for different reasons, some of which are more powerful than others. 

Any good marketer will ask herself the question, "How do people make purchasing decisions?"

This seemingly complicated question becomes pretty simple if you use the _hierarchy of buying_ — a chart that details the purchaser's decision-making process. The authors formulated this chart based on interviews with over 1,000 business owners centered around a crucial question: "Why do you buy?"

The bottom level of the hierarchy is cold calls. Though cheap, cold calls are a nuisance. Most people don't want to be bothered at home, and calling someone out of the blue isn't a great way to make a first impression. Furthermore, since these people likely haven't heard about you before, they'll make a decision based purely on price, which is only to your benefit if you're the cheapest option on the market.

One step up from cold calls are customers who buy because of search results. They don't have anyone to ask about the products they're interested in, which is why they use internet searches or click on ads. The problem here is that, to actually grab the attention of these potential customers, you need to hold a position near the top of the search results or spend a lot of money on ads. Even if you _can_ manage one of these, online customers are going to compare prices as well.

The next level is smack dab in the middle of the hierarchy. These are people you have contact with, but they haven't made a purchase yet. By the time you reach this level you're making real headway because you're already in a comfortable place with these potential customers.

The fourth level of the hierarchy accounts for those people who've been told, by a friend or coworker, that your company is a good one. This is a great place to be, since getting a face-to-face recommendation is the second most common reason that people buy.

The number one reason to buy that a business can wish for, however, is having a satisfying relationship with the company. This ultimate level contains people who have bought from you before and were so happy that they're ready for more. Customers like these are the most valuable because they guarantee a constant flow of income and will happily tell others about your products.

But, of course, this prize doesn't come easy. You'll have to invest a lot to build such powerful relationships. In the following blinks, you'll learn how to go about it.

### 4. Focus on your existing customers and never let them down. 

For most businesses, bringing in new customers is goal number one. However, once a business establishes a stable customer base, it often thinks it can sit back and watch the money roll in. This is a big mistake based on an erroneous assumption: that current customers are always _satisfied_ customers.

Marketing approaches tend to center on acquiring new customers and ignoring existing ones. As a result, potential customers often receive much better treatment than current ones. For instance, companies spend huge amounts of money on ads and deals for new purchasers, yet existing customers have to wait in long lines to reach a customer-service representative.

It's probably already clear that treating your customers this way is a huge mistake. After all, just because somebody bought from you in the past doesn't mean they won't go to your competitor the next time.

So, while customers know that mistakes happen, when you stop paying attention to them, and these mistakes start adding up, they're likely to switch to one of your competitors.

For example, the author was a loyal customer of the Canadian coffee and donut chain Tim Horton's for 20 years. However, as time went on, he became more and more dissatisfied. The servers would forget to stir his coffee and sometimes used artificial sweetener instead of sugar, which he preferred. What's more, the company's inability to accept credit card payments became tiresome for him, eventually leading him to switch to a competitor.

To put it differently, his _experience gap_ became too wide.

This term refers to the discrepancy between the best and worst experiences a customer has ever had with a given company. When the space between the two becomes too large, customers start looking for a change to regain some consistency.

As you might imagine, allowing such a gap to open up can be a disaster for companies. Just take the case of Tim Horton's. Many customers spend two dollars there every day, so losing just one single loyal customer will cost the company $700 a year.

### 5. Attract potential customers by becoming an expert in your field. 

In an era of ad overload, every consumer likes to have a little help when making a purchasing decision. And when faced with such a quandary, who better to advise you than an expert?

After all, experts are reliable, can offer an overview of the total market and aren't just trying to sell stuff. If a customer is trying to decide between two similar products, an expert opinion may be what tilts the scales. And this is why you should become an expert.

The very act of offering helpful information will attract potential customers. But before you start blogging, take a moment to consider your approach. Displaying expertise differs greatly from marketing directly. Ads only attract people who need your product at this very moment. Positioning yourself as an expert, on the other hand, will help you reach people who don't necessarily have an urgent need but are generally interested in your products.

Beyond that, when you, say, place an ad in the newspaper, you have no way of telling how many readers actually need what you're offering. But when you write an informative article that's published on a specialized website, you'll know for a fact that the people you reach are those who specifically searched for your product or service.

You also derive the added credibility of being a helpful source of advice, rather than someone whose only goal is to sell stuff.

So how do you get started?

Well, the first step is to overcome shyness, which can deter you from calling yourself an expert. It might help to know that, according to Merriam-Webster's dictionary, the definition of an expert is "having, involving or displaying special skill or knowledge derived from training or experience." In that sense, anyone can be an expert. It's not an official title or something that requires certification.

Just by starting a business or filling a particular role, you'll learn something about the field, and you don't need to be shy about sharing that knowledge. You don't need to be _the_ expert — just one of many.

### 6. Prove your expertise by creating content your customers will value. 

So it's important to build a reputation for yourself as an expert in your field. But you can't just go around calling yourself an authority and expect people to listen.

Rather, people need to _see_ you as an expert. Here's how to make that happen:

First, think about the types of problems your customers face. A good starting place is to consider the biggest pain your product solves for people in your market. Try to be as broad as possible so that you can write several specialized articles about it, all of which fit into that one big category.

For instance, if you're a chiropractor, your mission is to alleviate physical pain. Or, if you're a financial planner, your goal is to help people get a better handle on their money.

From there, your goal is to find something specific to write about. For example, if you're a financial planner, you might have clients who are worried about running out of money after they retire. A useful piece of information for them could thus be how to plan their finances for old age.

Then, once you've got your topic, you can use the _three Ps_ to write out your article. These are _point, prove_ and _perform._ The first, _point_, refers to the best practice of starting your article with your main point. In other words, in an article about saving for retirement, you could lead with the recommended percentage of income to save each month.

The next step is to _prove_ what you've said by offering examples. For instance, you could talk about clients of yours who saved the amount you suggested and succeeded in having a happy, financially-secure retirement.

And finally, you need to perform your message. In this section, you should discuss tricks that make it easy for anyone to follow the example you gave. You might describe money-saving tools like automatic monthly bank deductions that go right into a savings account.

> "No matter how many new ways exist today to deliver content, it's the content itself that matters most."

### 7. Shape shareable viral content to boost your publicity. 

Do you remember that commercial where Jean-Claude Van Damme landed the "epic splits" between two Volvo trucks? In 2013, the internet exploded over the video and, to this day, more than 80 million people have viewed it on YouTube. So how can you reproduce Volvo's success?

Well, the first step is to find the right kind of message for your video. Videos only go viral if they communicate in a particular way.

The first kind of successful message is one that's hilariously funny. But for this to work, the message can't just be somewhat comical. It has to be so funny that people laugh till they're rolling on the floor. If you need an example, just search for "Chewbacca mask mom" and see if you can keep from laughing.

The second type of message that really lands is one with a certain "WOW factor." In other words, there's something incredible about what happens in the video, compelling people to share it. A great example here is the famous clip of a news reporter who is talking about car accidents as a car actually crashes behind her.

The third kind of message is one that evokes emotion. It doesn't even matter what the emotion is. For instance, videos of landscapes with voice overs of people discussing the fleeting, precious nature of life are always a hit. The author actually made such a video in 2004. Called _Time Movie,_ it has racked up over 4 million views!

Other types of messages are all well and good too, but if your goal is to get shares without paying for them, you should stick to the three above.

And remember, whichever type of message you go with, successful viral content should always be relatable for the audience. It's not about what you find funny or touching; it's about what makes your audience laugh or gasp or sigh. The viewers are the ones who will share the content, but only if it appeals to them. So put yourself in the shoes of your customers, show your ideas to others and always seek feedback.

### 8. Use social media to engage with your customers, one conversation at a time. 

Imagine that your favorite Italian restaurant has a Facebook page. You like the page so that you can stay abreast of any exciting updates, but, after a few weeks, you're disappointed with the results; the restaurant only posts a single photo every week and doesn't reply with so much as an emoji to your thoughtful dessert suggestion.

Rather than making you a bigger fan of the restaurant, your engagement with its social media presence is making you think that it doesn't care about its customers. Clearly a failure on their part.

So how can your company avoid a similar fate?

It's all about interaction. Lots of companies want to control their message and only use social media as a vehicle for PR-department-approved content. This may have sufficed at one point, but nowadays people want to be heard, and social media wouldn't be _social_ without the human interaction.

So rather than posting as much as possible wherever you can, consider how you can actually engage your customers on a regular basis while making them feel valued.

To get started, simply choose a platform and jump in. When choosing the right one for you, it helps to know that there are three basic types of social media. First, _conversational social_ platforms are where people go for updates about their interests, whether they be politics, sports or natural disasters. This is the appropriate forum for sharing links, talking to customers and starting discussions, and some examples include Facebook, Twitter and LinkedIn.

Then there are _visual social_ platforms, which are all about images and video. The main players in this game are Instagram and Pinterest.

And finally, there are _dark social_ platforms — social media where interactions happen solely between users. Just think of applications like Whatsapp and Snapchat, both of which have a ton of users but pose difficulties for companies that want to use them for marketing.

Simply put, if images are your forte, go with Instagram. But if you want your social media to serve a customer service function, Facebook is a better bet.

### 9. Establish your social media presence through a structured, but authentic approach. 

From showing off your awesome camping trip to making new friends with shared interests, social media can be an incredible, exciting space. And putting together a profile for your business can be even more exciting. To do this successfully, make sure you follow three general stages.

At first, your primary goal should be getting attention for your profile. Do this by posting things that potential customers will find engaging while being both present and consistent. To accomplish this, it's a good idea to spend a few hours a day on your platform.

From there, begin following accounts and starting conversations with them. By interacting with others, people will become more aware of you.

Once you've gotten that far, you're ready for the momentum stage. This is where you begin reaching your goal of consistently engaging your fan base.

This process can be tricky, which is why it's time to get organized. To keep yourself on track, try apps like Hootsuite that help you manage your posts and comments from a single dashboard while scheduling new posts and keeping up the quality of your interactions.

And finally, you need to expand. Once you've nailed one platform, you're ready for others. Just remember to take it one step at a time, just like you did the first time.

By following this structure, you'll maximize your chances of success. That being said, it's also important to remain authentic and be yourself. People want to get to know the people behind the business, and you shouldn't be too shy about sharing your opinion from time to time or getting personal with your customers.

For instance, the author found that having a friendly profile picture of himself on Twitter, instead of his company's logo, helped with engagement. It made people feel like they were talking to a real person, not just a company trying to make a sale.

### 10. Respond carefully to customer complaints on social media to maintain your reputation. 

Back in the day, it was hard to voice your complaints about a poor customer service experience. If you as a consumer wanted to get your point across you had to write a letter or call up a hotline, which most people weren't willing to do.

But these days just about anyone can complain, a development that's transformed the terrain for businesses. In today's world, you better have a good strategy for responding to disgruntled customers.

After all, when a customer complains via your hotline, only you and he can hear what's going on, making it easy to ignore. But when people post about you on your Facebook page, other customers can read it, too. Blog posts and tweets can even be pulled up in Google search results.

Such accessibility is great when people share positive experiences or answer questions so you don't have to, but when a complaint is lodged through social media, it can be a disaster. In other words, it's no longer true that any publicity is good publicity. Mismanagement of social media complaints can wreck your reputation.

So, if you make a mistake, it's best to apologize. Just take an example from the cafeteria at Tufts University. When a customer complained about the quality of the fruit in the dining hall, saying, "I've just eaten the most DISGUSTING apple in the world," the cafeteria staff didn't just stand silently by.

Instead, they apologized, asking the customer which location she had gotten the apple from and adding that "it's fresh fruit and sometimes goes bad, but we should be able to catch it before it gets to you." The staff could have easily ignored the complaint as a minor mistake, but by listening and apologizing, they simultaneously calmed down the customer and made a good impression on others.

> "You can't avoid mistakes, nor can you control them after the fact, but you can own a mistake and make it better."

### 11. Final summary 

The key message in this book:

**In the modern world of business, marketing is all about relationships. To truly grow your business and pull ahead of the pack, you need to take care of each customer personally, which means active outreach, lots of listening and thoughtful responses. Attention-grabbing ads and spam calls are no longer enough.**

Actionable advice:

**Trust those around you instead of copying other companies.**

While successful companies are probably successful for a reason, you shouldn't just mimic what they do. After all, you probably can't see most of the things that got them where they are now, and many of the tools they used could well be ineffective in the current climate or in your specific context. So instead, ask trusted friends and associates to give you some honest feedback on your business. These are the opinions that truly matter, and actively soliciting people's ideas is a surefire way to improve your business.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Everybody Writes_** **by Ann Handley**

_Everybody Writes_ (2014) gives you invaluable advice on how to create great content, from using correct grammar to crafting engaging posts, tweets and emails. With just a handful of simple rules, these blinks will help you gain a better understanding of how to use the right words to keep customers coming back for more.
---

### Scott Stratten and Alison Stratten

Scott Stratten and his wife, Alison Stratten, are well-known guest speakers at corporate events all around the world. Scott previously worked as a professor at Sheridan College School of Business. In 2012, he was listed as number five on _Forbes_ 's list of the world's top 50 social-media power influencers. Together with Alison, who previously ran a maternity lingerie business, he has written four best-selling books, including _UnSelling_ and _The Book of Business (UnAwesome)_.

© Scott Stratten and Alison Stratten: UnMarketing copyright 2009, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

