---
id: 58889e74f40da4000409d492
slug: unmasking-the-face-en
published_date: 2017-01-26T00:00:00.000+00:00
author: Paul Ekman and Wallace V. Friesen
title: Unmasking the Face
subtitle: A Guide to Recognizing Emotions From Facial Expressions
main_color: 84285B
text_color: 84285B
---

# Unmasking the Face

_A Guide to Recognizing Emotions From Facial Expressions_

**Paul Ekman and Wallace V. Friesen**

_Unmasking the Face_ (2003) is an illuminating read about the subtleties of facial expressions. From the dynamics of surprise, fear, anger and happiness, to the eight styles of facial expression, these blinks shed light onto the complexity of an everyday skill that deserves more attention: reading other people's emotions.

---
### 1. What’s in it for me? Learn to read the human face. 

The expression "poker face" exists for good reason: if you're playing a high-stakes poker game, knowing how to control your expressions is essential. The slightest unintentional tick might betray your bluff and let your opponents take the pot from right under your nose.

But knowing how to read facial expressions isn't just important at the poker table. From the earliest stages of childhood, we rely on our ability to interpret facial expressions to help us understand what people around us are really feeling — because sometimes, words don't tell the whole story.

These blinks take you on a journey across the human face and show you how to call that next bluff.

In these blinks, you'll learn

  * why reading faces can sometimes be hard;

  * what separates surprise from other facial expressions; and

  * how your particular facial expressions are perceived by others.

### 2. Reading human facial expressions is a survival skill – but it can be a challenge too! 

The ability to read facial expressions is an essential tool in our personal, professional and social lives.

In our close relationships, we observe the facial expressions of our partners to understand how they're really feeling, while in the workplace, we use facial expressions to read between the lines during job interviews or important conversations where we've got to be sure whether someone's telling the truth. In fact, in some professions, reading facial expressions is an indispensable part of the job.

A trial lawyer, for instance, can't always trust a client on the basis of his words alone; physicians need to understand their patients' emotional reactions to news about their health; and counselors, therapists and teachers all rely on their capacity to discern the emotional state of people of all ages.

So how do we read facial expressions? For us, it comes naturally — though some are more adept than others. The key to understanding facial expressions starts with knowing what signs to look for.

One of the most telling signs of someone's emotional state is a _rapid signal,_ a fleeting change to someone's face through the quick movement of facial muscles in three areas: the eyebrows, the eyes and the mouth. These changes can be _macro expressions_, that is, visible for a few seconds, or _micro expressions_, visible for just a fraction of a second. It's all too easy to confuse macro and micro expressions, or even miss them altogether, if you aren't paying close attention to the face of your conversational partner.

A brief tightening at the mouth is one micro expression that often goes unnoticed. If you do pick it up, it's a good indicator that your conversational partner is tense or disagrees with what you're saying (even though they might not admit it). Another macro expression that's difficult to miss is raised eyebrows; however, just because we notice this signal doesn't mean we always interpret it correctly. Depending on the person, raised eyebrows with no expression in the rest of the face can signal mild disbelief, or even genuine alarm.

Raised eyebrows are often linked to surprise, although this is one emotion that requires a careful, simultaneous reading of all three areas of the face. In the next blink, we'll examine the key signals for surprise and its nuances.

### 3. Surprise reveals itself in our faces, but only fleetingly. 

Our faces reveal surprise in a variety of different ways. If you want to accurately gauge someone's level of surprise, be sure to catch the clues in their facial expression before they disappear!

Surprise is linked with three key facial signals: raised eyebrows, widened eyes and a dropping jaw. Let's start with the eyebrows, which will appear high and curved. This signal alone typically suggests that a person is doubtful or skeptical. When combined with wide eyes and a gaping mouth, it's more likely that they're surprised.

When it comes to the eyes, surprise typically reveals itself through raised upper eyelids and relaxed lower eyelids. Finally, the jaw drop results in an open mouth, which is slack rather than tense, as if the person's mouth "fell" open.

Of course, there's a big difference between mild surprise and shock, and this reveals itself in our facial expressions, too.

Mild surprise, which we might experience after smelling a certain scent that reminds us of a memory, is often expressed through a slight raise of the eyebrows. A startling surprise, reacting to a loud noise or sudden movement, for example, makes us blink rapidly and retract our lips. If someone's mouth is gaping, they're almost certainly experiencing surprise.

Surprise is one of the briefest emotions, and is quickly replaced by other feelings and facial expressions. To understand how a person perceives a surprise, we need to examine these subsequent facial expressions. While we often mistake surprise for a mix of emotions, it's simply one reaction followed by another.

Let's return to our mild surprise example. In this case, brows might return to normal as a smile appears on a person's face, indicating that they're happy, or pleasantly surprised. In an unexpected event or shock, blinking eyes and retracted lips might be replaced by a facial expression indicating anger, where the brows are furrowed and in a lower position, for instance.

No matter which type of surprise it is, these facial expressions are fleeting — so catch them while you can! In the case of other emotions like fear or disgust, we've got a much longer time to read the resulting facial expressions. We'll learn which of these signals are most important in the blinks that follow.

### 4. Fearful facial expressions are similar to those of surprise, with three key differences. 

Fear and surprise are displayed quite differently in facial expressions, and these two experiences differ in three ways.

First of all, while surprise can sometimes be pleasant, fear is always _unpleasant_ — so much so that fearful emotions trigger further physiological responses, including perspiration, paleness and rapid breathing. Second, surprise is always experienced after an event.

Fear, on the other hand, can also arise in anticipation of something that you know is going to happen. For instance, you might fear getting vaccinated because you've had shots before, and you know you don't handle the pain well.

Finally, experiences of fear have a much longer duration than those of surprise. Fear that happens in the moment that we experience physical harm may be brief, but nowhere near as fleeting as surprise. In other cases, fear may last as long as it takes for us to evaluate an uncertain situation. For example, a sudden pain in the chest might make us fear an imminent heart attack. We'll feel afraid for as long as it takes for us to work out what's happening and what to do.

Now that we've established the differences between surprise and fear as experiences, let's take a look at how fear plays out in human facial expressions.

Fear is revealed in the same distinct locations on the face as surprise: the eyebrows, the eyes and the mouth. Like surprise, fear causes us to raise our eyebrows, widen our eyes and retract our lips. However, the resulting expression is strikingly different; fearfully raised eyebrows are drawn closer together.

Eyes widened with fear feature a tense lower lid. And unlike the slack, gaping mouth that accompanies surprise, a fearful opened mouth is characterized by tense lips.

Fear can often follow surprise, but in these cases, the dominant expression will always be fear.

### 5. Disgust reveals itself in the lower part of the face, with wrinkled noses and raised lips. 

While we might not find all the same things disgusting, we definitely react to disgust in the same way. Disgust is a feeling of aversion that can be triggered by all of our senses, from foul smells, an offensive sight, the taste of rotten food, the touch of something slimy or even the sound of something gross that causes disgust, like retching. Disgusted facial expressions play out on the lower part of the face, centered around the mouth and nose.

When we're disgusted, we tend to raise our upper lip, and the lower lip may or may not follow. This is typically accompanied by a wrinkled nose, which in turn causes our lower eyelids and cheeks to rise around our eyes.

The more disgusted we are, the more we wrinkle our noses and raise our lower lips, which may sometimes jut out as well. Eyebrows may be lowered, but this isn't a defining feature of disgusted facial expressions — in fact, if disgust is the result of a surprise, our eyebrows might remain raised while the lower part of the face registers our aversion.

As an emotion and facial expression, disgust is similar to, but not to be confused with, contempt. While disgust is largely a response to certain things, contempt is something we feel toward particular people. Both are feelings of aversion, but contempt is marked by an additional element of superiority — that is, we feel that we're above certain people when we're contemptuous of them.

While disgust and contempt make us want to avoid unpleasant things, anger makes us want to confront them directly.

### 6. Anger makes our faces tense in an entirely different way from fear. 

It's always good to be able to recognize anger quickly. If you can tell when someone's mad enough to lash out, you'll be able to get yourself out of harm's way in time. Thankfully, anger comes with clear facial signals around the eyebrows, eyelids and lips.

For starters, eyebrows are typically lowered and drawn together, and they allow us to discern between anger and fear. While eyebrows are also drawn together when a person experiences fear, they're also raised, forming wrinkles on the forehead. When feeling anger, our brows are drawn together and angled downwards or flat, which doesn't create wrinkles on the forehead. Instead, a vertical line may appear between the brows.

Angry faces also feature tense eyelids. Both upper and lower lids retract around the eyes, making a furious, penetrating gaze seem extra threatening. Lips are also tense, drawn together in a firm line, or parted in a rigid square shape. Open mouths usually accompany verbal expressions of anger, while lips are usually drawn together during bodily expressions of anger, including acts of violence.

Like other emotions, we also experience anger at various intensities. Being able to discern someone's level of anger via their facial expression is crucial to helping us decide what to do next. Mild to moderate anger tends to be expressed in only two of the three areas of the face, while more intense anger activates the eyebrows, eyes and mouth all at once. And watch out for tensing around the eyelids and bulging eyes — this indicates real fury!

Anger may also appear as part of a blend with other emotions, such as the anger-disgust combination, a particularly common one. Facial expressions, in this case, would feature tightened as well as raised lips, often with a wrinkled nose too.

### 7. Different kinds of smiles reveal how truly happy a person is. 

Happiness comes with a facial cue that we all recognize: the smile! However, the mouth isn't the only part of the face that reveals joy.

Happiness is largely shown through changes in the lower face and lower eyelids. With the corners of the lips drawn back and raised slightly, our mouths form a happy smile. In addition, wrinkle lines may also form, running from the nose to the corner of the mouth.

As a smile raises the cheeks, the lower eyelids are often raised too, but not in a tense manner that would cause wrinkles to form below the eye. Instead, happiness gives rise to what we refer to as crow's feet. These are wrinkles at the outer corners of the eyes that make our smiles appear genuinely happy. Eyebrow positions can vary, and this part of the face is less crucial to expressions of happiness.

To determine how happy someone is, if at all, we can observe more subtle signals in smiling faces. One of these is the nasolabial fold, or the wrinkles that reach from the sides of the nose to the corners of the mouth, also known as laugh lines. More pronounced laugh lines indicate that a person is experiencing a more intense happiness, such as joy or elation.

While smiling faces can reveal powerful happiness, they're most often used as a mask to disguise other emotions. By adding a smile to our faces when experiencing something other than happiness, we send signals to others about how they should respond to us. This process is called _qualifying._

Let's take a look at how it works:

Say you're sitting with a friend on a rollercoaster that's about to get started. You're not fond of heights and are feeling pretty scared — but you're determined to prove to yourself that you can do it, so when your friend asks you how you're doing, you give her a quick smile. By adding a smile to a fearful facial expression, we can mask the level of fear we're experiencing and indicate to the other person that the fear is tolerable.

Happy facial expressions can also be blended with those of sadness. In fact, sadness is another emotional state that results in unique facial signals, and we'll investigate these in the next blink.

### 8. Sadness lingers longer than distress, and can blend with happiness and anger too. 

In our culture, we're often told to keep our tears to ourselves, and sadness, as a result, is usually accompanied by a muted facial expression. Nevertheless, these signals still make our feelings unmistakable.

First, it's worth noting the relationship between sadness and distress. Distress is physical, usually accompanied by tears and wailing. Sadness, meanwhile, is a less physical and more emotional variation of distress.

When we're sad, all three of our facial areas reflect this in their own distinct ways. The inner corners of our eyebrows become raised and often drawn together, which looks quite different from surprise, where the whole brow lifts at once. The inner corners of our upper eyelids follow our eyebrows to raise slightly. Deeper sadness usually makes itself known through the raising of lower eyelids too.

While milder sadness may see one or more of the three facial areas showing less emotion, all three are still necessary. Differing levels of expressiveness in each area of the face are also the result of sadness blending with the other emotions we've looked at in these blinks.

Someone who is sad and angry — for instance, a person confronting a driver who just hit her dog — would display tight downturned lips, with her upper and lower eyelids both raised.

When sadness blends with happiness, such as when a person recalls a bittersweet memory, lower eyelids are less tense, and the corners of the mouth may even raise in a faint smile.

Now that we're well versed in the composition of our facial expressions, let's put this knowledge to use! In the following blink, we'll explore how to discern genuine expressions of emotion from false ones.

### 9. Deceptive facial expressions can usually be unmasked with a few helpful clues. 

Most of the time, we make facial expressions without conscious effort. But in certain situations, we might choose to manipulate our expressions. Sometimes we can get away with this — but certain clues can reveal the feelings we're attempting to mask.

Morphology is the act of controlling one's displayed emotion for a certain purpose. By deliberately altering certain muscles in the face, we can strengthen or weaken the appearance of an emotion. For instance, a person could mask his sadness by controlling the muscles around his lips, holding them firm instead of letting them tremble or turn down.

Still, the sadness present around the eyes and brows reveals that the person's feelings are more intense than they'd like you to believe. In this way, inconsistencies between the three key facial areas allow us to detect a forced facial expression.

Timing is another telling indicator when it comes to deceptive displays of emotion. When people take a little too long before showing a certain emotion, or their emotional display goes on for unusually long, then they're probably struggling to communicate something that they don't actually feel. When someone takes longer to smile or laughs for an uncomfortably long time at our jokes, that's usually a signal that he didn't really find our punchline funny.

Of course, a forced facial expressions can be the punchline itself — long, serious expressions often accompany a deadpan joke. These facial expressions are typically exaggerated and playful, not to be taken seriously. For instance, if a friend asks if he can borrow something of yours, you might jokingly respond with a long, disapproving glare, when you really don't mind at all.

On a deeper level, mock expressions also allow us to show emotions we really are feeling, without having to take responsibility for expressing them. Say you're in a mildly dangerous situation with friends, but want to play it cool. You might choose to give them a mocking, exaggerated expression of fear and gauge how they react. If they respond in kind and admit that they're also fearful, then you're all on the same page — great! But if they seem relaxed, you'll be able to play things off as if you were only joking about being afraid, and thus hide your fear.

### 10. The eight expressor styles help us pinpoint the reasons for miscommunication in our facial expressions. 

We've now explored how emotions reveal themselves on human faces — but it's a little more complicated than that. After all, we all show our feelings differently. This, in turn, has a powerful impact on our facial expressions. Even so, it's still possible to get to know how people's expressions reflect what they feel, even if their expressions mystify you at first.

Enter the _Eight Styles of Facial Expression._ This is a system that allows us to consider people in light of their general behavior toward showing emotions.

The first two styles are _withholders_ and _revealers_. Withholders have naturally inexpressive faces. If anyone has ever accused you of trying to deceive them or lying when you weren't, then this may be your facial expression style.

Revealers, with their overly expressive faces, struggle to hide their emotions, which may get them into hot water sometimes.

The next two styles are _unwitting expressors_ and _blanked expressors_. Like revealers, unwitting expressors struggle to mask their feelings in social situations where it would be more appropriate to do so. However, the unwitting expressor usually believes she's doing a good job of hiding her emotions, even though they're obvious to everyone else.

Blanked expressors have the opposite problem: they're convinced that they're showing the appropriate emotions, but in reality, their faces are blank. They have a tough time in close relationships, where partners may feel they're cold or uncaring.

_Substitute expressors_ and _frozen-affect expressors_ are the next two facial expression styles. Substitute expressors face a fairly confusing problem, in that while they might be under the impression that their faces are showing one emotion, such as sadness, to others, it might signal anger. If you've ever been confused about people's reactions to you, you might be a substitute expressor.

Frozen-affect expressors may also confuse conversational partners in that one or another emotion always seems to be on her face, even if she's isn't feeling it. This is often due to facial shape and features — a natural downturn at the corner of the lips or heavy-lidded eyes might make people think you're always feeling down.

The final two styles are the _ever-ready expressor_ and the _flooded-affect expressor_. The ever-ready expressor tends to show the same initial reaction, no matter the situation. Shortly after, though, his face will show his real emotional response, but by then it's often too late — most of us react to initial expressions, not subsequent ones.

The flooded-affect expressor sends out the most mixed signals of all; with one to two emotions on his face at all times, his expression is never neutral. Some people tend to avoid flooded-affect expressors, simply because interacting with them is overwhelming.

How can you work out which expressor type you are? The final blink will offer some tips you'll need to find out.

### 11. Discover your own expressor style to gain a better understanding of how you’re perceived by others. 

If the eyes are the window to the soul, the face might as well be a doorway to your feelings. Let's take a closer look at our facial expression types with two simple methods.

Start by having a partner take 14 photos of your face. Two of these should be neutral, while the others should be two each of surprise, fear, anger, disgust, sadness and happiness. Then, bring together ten people who don't know you well, and have them judge the emotions they see in the photos.

This should give a good indication of where confusion might lie between the expressions you think you're making, and the emotions that people perceive in your face. For instance, if most of the ten label your fearful face as a surprised one, you might be a substitute expressor type.

You can also analyze your facial expressions alone with a mirror and a quick Google images search. Find a photo online of a typical angry face, and compare it to an angry face that you make in the mirror. Try to copy the expression in the image. If it feels easy, then you might be a revealer style; if it feels unnatural for your facial muscles, you could be a substitute style.

By working out which expressor type fits you, you'll be better able to understand how others perceive you. This, in turn, could offer new solutions to recurring conflicts in your life.

For instance, as a revealer expressor type, you might find it hard to get away with lying. With a greater awareness that your inner emotions are playing out on your face for everyone to see, you can be more careful when certain situations call for you to tell a white lie to spare someone's feelings.

If you discover that you're a blanked expressor type, you could be one step closer to understanding why important relationships in your life have fallen apart. Humans need their loved ones to display emotions in their relationships. It could be that you have strong feelings for your loved ones too, you just express them differently. Blanked expressors should spend time learning about the unique and hidden ways they express emotions, so that they can help their loved ones learn about them too.

As for changing your expression style altogether, research is only beginning on the matter. By learning more about yours, you'll find it easier to create healthy, happy relationships.

### 12. Final summary 

The key message in this book:

**The way we move our eyebrows, eyes and mouths reveal our emotions and mask our intentions. In addition to facial expression patterns common to all humans, we also have our own expressor styles. By learning about the unique ways in which we convey our feelings, we can be more self-aware and tactful in our relationships.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Emotions Revealed_** **by Paul Ekman**

_Emotions Revealed_ (2003) puts emotions under the microscope, revealing where they come from and how to recognize them, whether they're yours or someone else's. If you've ever wanted to know if someone was being dishonest or trying to deceive you with a friendly smile, these are the blinks for you!
---

### Paul Ekman and Wallace V. Friesen

Paul Ekman is a professor emeritus at the University of California, San Francisco and is renowned for his pioneering work in the study of human emotions, including his contribution to the discovery of microexpressions. In 2009, Ekman was named one of the 100 Most Influential People by _Time_ magazine.

Wallace V. Friesen is a lecturer at the University of Kentucky, where he also conducts research into emotion in old age, having coauthored many articles on emotions and longevity.

