---
id: 59ad8338b238e10005ef001c
slug: unplug-en
published_date: 2017-09-07T00:00:00.000+00:00
author: Suze Yalof Schwartz
title: Unplug
subtitle: A Simple Guide to Meditation
main_color: 97C643
text_color: 669415
---

# Unplug

_A Simple Guide to Meditation_

**Suze Yalof Schwartz**

_Unplug_ (2017), a useful beginner's guide to meditation, makes clear that meditation isn't just for hippies and monks; rather, it's a practical exercise that anyone can use to become calmer, happier and healthier. Get all the basic info you need to start a daily meditation routine, as well as a number of supplemental tools that will aid relaxation and improve your practice.

---
### 1. What’s in it for me? Get meditation into your life. 

_Sit in a comfortable position, clear your mind and take a big, deep breath_. _Hold for a count of two and then breathe out slowly._

Sound familiar? Many people associate meditation with a standard set of guiding words, usually intoned by some overly empathic pseudo guru. Others think that meditation enthusiasts have an irresistible urge to chant in public spaces and spend all their money on meditation retreats.

Obviously, these are oversimplifications. Really, anyone can incorporate a meditative practice into their life, and that practice needn't absorb an exorbitant amount of time or funds. These blinks show you what meditation is when you remove all the fluff, and why it can help recharge your batteries and reshape your brain for the better.

From the six basic steps of a daily meditation practice to dealing with the intrusive thoughts that appear when you try to relax your mind, these blinks cover all the ground you need to unplug — today.

You'll also learn

  * how an electric kettle by your bed might make you a better meditator;

  * why imagining your breath as a train is beneficial; and

  * how picking people off a tree is a type of meditation.

### 2. Meditation fits any lifestyle, and a mere ten minutes per day is already beneficial. 

For many, the word "meditation" conjures up images of long-haired hippies listening to new age music, sitting in rooms full of incense smoke and preaching about their vegan lifestyle.

While you're certainly welcome to go this route and buy all the crystals and incense you can get your hands on; meditation can also match any lifestyle.

If you prefer a minimalistic approach to living, then you can approach meditation in a similar manner.

In fact, there's hardly anything more minimalistic than meditation, since all that's required is stillness and focus.

If you're worried about your friends or roommates mistaking your practice for some newfound spirituality, you can always explain it as a way of improving your concentration.

Chances are, even people who roll their eyes at the M-word will immediately recognize that feeling calm and thinking clearly is desirable for both work and personal life.

Another misconception that often keeps people from meditating is that it takes up too much time.

It's true that you do have to set aside _some_ time — but it doesn't take much. The benefits will soon become apparent.

According to Harvard neurologist Sara Lazar, your brain function will improve in just eight weeks if you spend 27 minutes each day in meditation.

But even ten minutes a day will provide you with tremendous benefits, such as feeling calmer and more grounded.

So there really isn't any reason not to meditate. Even the busiest person can find at least five to ten free minutes a day. As you'll see, it's something you can do almost anywhere!

Next, we'll take a closer look at some of the amazing benefits.

> _"Almost everything will work again if you unplug it for a few minutes, including you."_

### 3. Meditation alters the brain, reducing the levels of stress and increasing happiness. 

Imagine that your brain is a piece of clay, something you can mold and manipulate as you wish, strengthening the areas that are most important.

In a way, meditation makes your brain clay-like; it allows you to reconfigure your gray matter and change the way your brain is structured.

This has been proven by two studies conducted by the Harvard neurologist Sara Lazar. In each, she compared the brain structures of those who meditate to those who don't.

The results show that regular meditators have more gray matter in their frontal cortex, which is the part of the brain that controls memory and decision-making.

The frontal cortex normally shrinks as we get older, but Lazar's studies showed that this doesn't happen with meditators. It revealed that 50-year-old meditators had as much gray matter as 25-year-olds who didn't meditate.

This suggests that meditation could be a great way to fight Alzheimer's, Parkinson's and other old-age diseases, though further research needs to be done.

Lazar's second study shows that meditation can be quite beneficial to newcomers and that it's never too late to start.

Lazar got a group of people who had never meditated before, and for eight weeks straight they practiced meditation for 30 to 40 minutes every day. By the end, each participant showed changes in the areas of the brain related to learning, memory, concentration and emotion. And they all performed better than before in tasks related to those areas.

But that's not all. The results of Lazar's research showed that meditation made her subjects happier.

While the frontal cortex got bigger, the amygdala — the part of the brain responsible for feelings of stress, fear and anxiety — had gotten smaller.

A separate study by the University of California, Davis has shown that people who meditate also have less cortisol, a hormone that accompanies stress. And naturally, people feel happier when there's less stress, fear and anxiety in their lives.

So there are plenty of reasons to start a meditation regimen. In the next blink, we'll explore how exactly to do that.

### 4. There are benefits to meditating first thing in the morning, but comfort is the most important consideration. 

If you're a morning person, it might be easiest for you to meditate first thing in the morning.

The primary advantage to meditating right after you wake up is that the brain is still in its _theta_ rhythm. In this state, we're still tuned into our inner world, and our brain tissue is especially malleable. So it's an ideal time to meditate and positively influence and shape the brain.

The other great advantage to an early morning session is that you'll have it accomplished and out of the way.

If you try to schedule it later in the day, you're sure to find yourself caught up in emails, commuting, work and errands, and before you know it, you'll be telling yourself you're too tired or busy to meditate.

So it's best to wake up, get it done and stay on schedule. But of course this isn't a strict rule. If your lunch break or the time just before bed works better for you, go for it.

As a beginner, a primary concern should be finding your comfort zone.

Don't worry about twisting your body into some sort of fancy position at this stage. This is the kind of thing that can end up scaring people away from meditation.

Just try to sit comfortably with your spine in straight alignment, and if you need a cushion to get comfortable, don't hesitate to use one. Many shops sell meditation cushions that will help you sit straight, but you can also use any comfortable chair or a folded blanket.

Keep in mind that any comfortable position will do just fine, but it is best to keep your legs below your hips if you're sitting; otherwise, they might fall asleep.

And if you'd like to try meditating early in the morning but have trouble getting yourself out of bed, try putting an electric kettle next to your bed. Having a warm cup of tea within arm's reach is a great way to coax yourself out from under the sheets.

### 5. Make it easier to keep up your practice by ritualizing your sessions and clearing your mind beforehand. 

You'll often hear meditators talk about their "ritual." But don't worry; ritual is just another word for routine, and no one is going to try to get you to dance under a full moon or anything.

A good ritual often makes it easier for people to keep up their meditation practice.

Humans are creatures of habit. And when you program your body to do something at a regular time, it becomes second nature, and you don't have to muster up your motivation each time.

Therefore, the first step to building a good meditation habit is finding a regular time for it.

If you start meditating first thing in the morning, you can also have your meditation area prepared the night before. You can set up your cushion near your bed, along with any other inspirational objects that might facilitate your sessions, such as a candle or some music.

These preparations are helpful in more ways than one; they communicate to your body and mind that this is your ritual, and you intend to meditate first thing in the morning. As you'll discover, this preparation will make it much easier to get up and actually follow through.

It's also easier to meditate with a worry-free mind.

When starting out, you'll probably find your mind wandering off to what you have planned for the day or what groceries you need to pick up. To avoid this, follow the brain-drain practice from Julia Cameron's book _The Artist's Way._

Before you meditate, use pen and paper to jot down everything that is on your mind and anything that you need to remember for the day ahead. Now you can put the paper aside and rest easy knowing that your daily concerns won't be neglected. Doing this will help clear your mind and prime it for a peaceful session.

By now you're probably thinking that this is all well and good, but you still don't know what to do while meditating. So let's get to it in the next blink.

### 6. Meditation can be broken down into six simple steps, and mantras can be a helpful focus point. 

The world is full of programs built around a number of steps, like the 7-step weight loss program or the 12-step addiction recovery program.

Similarly, to get your own meditation program going, we can break things down into six simple steps that you can repeat over and over again.

The first step, once you've gotten yourself comfortable, is to _find a point of focus_. People often focus on their breath, as it flows in and out, in and out. But it could also be an object, like a flower or your favorite seashell. Or a particular phrase, often referred to as a _mantra_.

The second step is to reach a point where you're not focusing on anything at all.

The third step is a period of time that can last anywhere from a second to a couple of hours where you are free of any thoughts and merely float in a sensation of peace.

The fourth step begins when a thought enters and interrupts this peace, which can happen within seconds or after a prolonged period of absence.

The fifth step is acknowledging the thought, letting it pass and bringing your attention back to that first point of focus.

The sixth and final step is to continue repeating steps two through six for as long as your session lasts.

Many people find a mantra to be of great help in finding and returning to that peaceful state of mind. Breathing isn't always strong enough on its own, so a mantra — a repeated word or phrase that follows the rhythm of your inhalations and exhalations — can help. Generally, a mantra is not spoken aloud.

Here are some classic mantras to get you started, broken down syllabically. Think of the first syllable as being on the in-breath, and the second syllable on the out-breath: Ah-hum / So-hum / I-am / Oh-mmmmm.

You can also make your own mantra by coming up with an affirmation. So ask yourself, "How do I want to feel?" You might find it helpful to say: I-am / Peace-ful.

### 7. Meditation can help you let go of negative thoughts, and visualization may aid you in doing this. 

It feels good when a tense muscle relaxes. The same goes for when we let go of a troubling and intrusive thought.

But letting go of unwanted thoughts isn't always easy, which is why meditation can be so helpful.

One of the best ways to free oneself from bad thoughts is to redirect one's attention.

A common misconception is that you have to stop thinking in order to meditate. But as you'll quickly find out, trying to stop any and all thoughts is easier said than done.

The real secret is to let the thoughts pass by without engaging them. If you do find yourself engaging, just redirect your attention back to your focus point. This is meditation.

Don't get discouraged by thoughts appearing. It doesn't matter how often you get distracted, just as long as you keep bringing yourself right back to your breathing or mantra.

In this sense, meditation isn't about having _no_ thoughts; it's about not being bothered by the thoughts you have.

To help yourself let go of recurring and disturbing thoughts, you can try visualization.

A helpful image is to picture your thoughts as leaves that have fallen into a river and are being carried away by the water. Or as clouds moving across the sky and disappearing out of sight.

There's no harm in observing or acknowledging your thoughts. In fact, this can be a helpful way of gaining insight into your thinking patterns and emotional issues. Just try not to engage with them. Let them pass by untouched, so to speak.

Another helpful visualization is to see your thoughts as passengers on a train. Your breathing is the train itself, and even if the passengers are noisy, the constant rumble of the train is always there in the background, waiting for your attention to return.

### 8. There are other ways to meditate, such as guided awareness meditation and sound meditation. 

If you're experiencing trouble or are unhappy with these traditional meditation methods, don't give up. You have options.

For instance, many people enjoy _guided awareness meditation (GAM)_ as a nice change of pace from the usual routine.

GAM is a way of adding more imagery to your standard meditation session, with the help of a guide.

For example, after settling in, your guide might tell you to picture yourself lying on a tropical beach while your ideal life is being projected before you like a movie. Or the guide might suggest you imagine your life as a tree with many leaves, each leaf representing an aspect or a person in your life that you feel is a negative influence. In your imagination, you can pluck these leaves away without consequence.

GAM follows the same six steps as traditional meditation. The only difference is that there's a guide who, via visualization prompts, helps keep your focus off those unwanted thoughts.

But this means that effective GAM requires a talented guide who can conjure the kind of vivid imagery that sparks your imagination and commands your attention. If you have trouble finding a good guide in your neighborhood, there a plenty of resources available online.

Another variation that some find helpful is _sound meditation_.

One of the more popular examples is the use of a Tibetan sound bowl, which you can place on your midsection while you lie on your back. Now you just follow the usual steps, but whenever you feel those unwanted thoughts approaching, you gently strike the bowl with a wooden hammer.

This not only provides a strong focus point to keep you present; it also provides a beautiful sound and a vibration that moves through your body.

At this point, you have enough knowledge to start a daily meditation session. In the final blink, we'll take a look at some additional tools that might make it a bit more appealing.

### 9. Aromatherapy and crystals can add new levels of relaxation and healing to your meditation. 

There is no shortage of thingamajigs, knickknacks and tchotchkes out there to assist in your meditative journey. So if you're also a lover of retail therapy, welcome to a whole new world of opportunity.

To go along with that Tibetan sound bowl, you could also consider aromatherapy.

Aromatherapy is a way of supplementing your meditation with relaxing aromatics and scented oils that help calm the body and mind. Certain scents can strongly affect the nervous system and instantly trigger emotions related to relaxation and happiness.

Lavender, for example, promotes relaxation, while orange extract is good for reducing stress and promoting happiness. Peppermint scent can be used to sharpen your senses, and rose oil is a good way to add some love to the air.

Simply rub the essential oils between your palms, lift your hands toward your face and inhale. The scent of essential oils is strong, so a little goes a long way, but you may want to repeat this process a few times during your meditation.

Crystals are another common addition to meditation. According to the author, they can bring a healing element to your session, and while they don't have any magical powers, you can take advantage of the powerful frequencies they emit. This is the quality that makes them useful for laser and watch technologies.

The author recommends that when searching for a crystal, you shouldn't overthink it. Simply trust your instincts and pick whichever one appeals to you. Once you've brought it home, you can go online and read about the qualities your type of crystal possesses, and chances are you'll understand why you were drawn to it.

Some examples of potential benefits that the author mentions are that rose quartz helps in the area of love; amethysts can help soothe the body and mind; carnelian crystals promote happiness; and pyrite is good for career success.

During meditation, you can hold your crystal in your hand, place it on your body while lying down or use it as the focus point of your meditation, noticing its unique shape, feel, texture and temperature.

There are a great many tools to help you get the most out of meditation, but remember, the main thing is just to sit down and do it.

### 10. Final summary 

The key message in this book:

**Meditation isn't some weird spiritual practice; it's a very practical tool to help promote a calmer, more focused and more productive life. And it doesn't require any severe lifestyle changes. All it takes is ten minutes every day, and before long you'll be noticing the rewards.**

Actionable advice:

**Try this quick meditation when you're feeling stressed.**

Take seven slow breaths, inhaling through the nose and exhaling through the nose. Then another seven slower breaths, inhaling through the nose and exhaling through the mouth. Then a final, very slow seven breaths, inhaling through the mouth and exhaling through the mouth.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Zen Mind, Beginner's Mind_** **by Shunryu Suzuki**

_Zen Mind, Beginner's Mind_ (1970) is a starter's guide to Zen Buddhism. These blinks explain how Zen is not only a system of meditation, but also a philosophy of life. They describe how to sit, breathe and observe while maintaining a vital connection to the present moment.
---

### Suze Yalof Schwartz

Suze Yalof Schwartz was born in New York, where she worked as a fashion editor for many years. Since then she has created the Unplug Meditation Studios in Los Angeles, a secular meditation center that highlights the freedom that meditation has to offer.

