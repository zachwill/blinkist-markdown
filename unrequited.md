---
id: 57fbf00d70ec6300032cf6c9
slug: unrequited-en
published_date: 2016-10-14T00:00:00.000+00:00
author: Lisa A. Phillips
title: Unrequited
subtitle: The Thinking Woman's Guide to Romantic Obsession
main_color: EB2F30
text_color: EB2F30
---

# Unrequited

_The Thinking Woman's Guide to Romantic Obsession_

**Lisa A. Phillips**

_Unrequited_ (2015) is a guide to the human obsession with people who will never love us. These blinks explore how romantic obsession affects both the obsessed and the object of their desire, why some people are more prone to stalker behavior than others and how to deal with the fallout of unrequited love.

---
### 1. What’s in it for me?  Discover the science of romantic obsession. 

Imagine you meet your soulmate, the man or woman of your dreams — and they are stubborn enough to ignore the simple fact that you were made for each other. Infuriating, isn't it? It's unbearably painful and embarrassing to be rejected by someone with whom you're so deeply in love.

And chances are you've already been there. According to one survey, 93 percent of us have, at some point, been rejected by someone we were in love with. Yet not all of us lose our sense of self-worth because of rejection, and fewer still turn to stalking as a response. So what's special about the people who react to rejection in such extreme ways? That's just one of the many questions answered in these blinks.

You'll also find out

  * why lovesickness can make you a better artist;

  * how being in love is a bit like snorting cocaine; and

  * whether men and women really differ when it comes to romantic obsession.

### 2. In society’s eyes, unrequited love is an acceptable pursuit for men, but not for women – unless it’s part of fiction. 

When it comes to gender equality, we've come a long way; women today face far fewer obstacles than in decades past, and can today become presidents, astronauts or CEOs.

However, they apparently still don't have the freedom to pursue unrequited love. Society is more comfortable with women being the object of desire and, therefore, pursuit, while leaving the pursuing to men.

In fact, a man's traditional role in society is very much attached to his passionate, and even aggressive, pursuit of unrequited love. For instance, there's no end to stories of romantic knights who put their lives on the line to win the hearts of princesses.

On the other hand, when a woman acts this way, society dismisses her as neurotic and pathetic. In the end, it's considered shameful for a woman to be undesired by the man she loves, and a woman in such a situation would face even more hate if she steadfastly pursued this love, trying to _make_ him desire her.

But why does society subscribe to this double standard?

Well, some people have hypothesized that it's the result of the supposedly biologically wasteful nature of a woman investing her limited years of childbearing in pursuit of someone with whom she might not have children.

So, unrequited love, at least when it comes to women, doesn't work out so well in real life — but it does make for some great stories. After all, unrequited love is far more romantic than its reciprocated counterpart.

For example, the ancient Egyptian hieroglyph for love translates to _a long desire_ and maybe, deep down, all humans still believe that true love is something that requires the investment of profound energy and effort.

Not only that, but while women who pine after indifferent lovers are scorned in the real world, we actually like stories about the very same thing. Just take Jane Austen's novels, which often revolve around a heroine who desires a man who does not reciprocate her love. The difference is, in books, the story ends once the love is requited.

> _"We don't understand (...) what light female obsession can shed on our understanding of relationships and gender."_

### 3. Romantic obsession can be viewed as a mental illness, but it might also guide us toward our ultimate goals. 

You've probably heard the expression "madly in love," but did you know that physicians have actually diagnosed and treated romantic obsessions as a mental illness?

It's true. Throughout history, there are instances of noblemen being diagnosed as suffering from _lovesickness_, also known as _amor heroes,_ or heroic love. These men suffered from things like obsessive thoughts and loss of appetite, and such a diagnosis was considered honorable.

By the thirteenth century, however, women began to show the same symptoms and, practically overnight, the condition became a shameful sign of weakness or moral depravity. This sudden shift might be due to Western religion and culture's obsession with controlling the desires of women.

But love obsession didn't stop in the thirteenth century. In fact, a modern diagnosis was coined by the psychologist Dorothy Tennov in 1979. Called _limerence_, the condition describes a state in which, regardless of whether the patient's love is requited, they remain completely dependent on the other person, perpetually craving their affection. It's also referred to as _relationship obsessive-compulsive disorder_.

So, why do people obsess over others who don't give them the time of day?

Well, the biological anthropologist Helen Fisher found that love activates the same neural reward centers as cocaine, making you feel euphoric and energized. And, just like the results of cocaine use, this feeling is highly addictive, making people crave their loved object more and more.

However, if you dig a little deeper into romantic obsession, you might find that it's really a means to a different end. The professors William Cupach and Brian Spitzberg refer to this phenomenon as _goal linking_ and here's how it works:

Their theory assumes that when we pursue a lower-order goal, like winning the affection of another, we do so because it will help us attain a higher-order goal, such as being cared for, easing our loneliness or finding self-worth. So, if a woman gives up on her unrequited love, she might also feel that she's letting go of her greater life goals.

### 4. Our pursuit of love is determined by our need for bonding and attachment, not our gender. 

To most people, it seems like the roles in romantic relationships are clearly divided between men and women — but that's absolutely incorrect. Even though society deems it appropriate for men to pursue women and not vice-versa, our bodies and behavior contradict this notion.

For instance, when men and women fall in love, they experience identical neurochemical and hormonal changes — that is, with one exception: testosterone. This hormone, associated with sex drive and aggression, goes up in women while it drops in men. In other words, when women are in love, their masculine side is heightened, filling them with fighting spirit and making them just as determined as men to pursue their love interest.

So, it should come as no surprise that men and women act similarly when initiating a relationship or attempting to win back a former partner. Just take a 2005 study of undergraduates at the University of Pittsburgh, which found that both men and women actively court one another. In fact, they both do so using repeated _approach behaviors_ like sending messages, doing favors, starting conversations and asking for dates.

And the similarities don't end there. For both genders, obsessive love is focused on two needs: bonding and attachment. Romantic obsession and the pursuit it provokes are the results of our natural desire to bond with a mate to produce offspring.

But in addition to this biological imperative, traumatic childhood experiences can also set the stage for adult romantic obsessions. For example, several studies have shown that a person who is neglected or physically abused during childhood will tend to develop an _anxious attachment style_, meaning they'll crave intimacy and affection and obsess over their relationships. As a result, they'll be more likely to stalk someone who doesn't reciprocate their desire.

### 5. Unrequited love can make people act like lunatics. 

Have you ever been turned down by someone you were in love with, only to find yourself transforming into a person you could barely recognize?

Well, it might not have happened to you, but for some people, facing rejection can precipitate a descent into a fog of madness that obscures the line between courtship and stalking. Romantic obsessions can make people so self-centered that they fail to see how damaging and intrusive their behavior becomes. This is a major reason why stalkers view their scary and unsettling behavior as a genuine expression of love.

But any sane person knows that nobody would stalk someone who they truly love. After all, no matter how much you love the sound of your partner's voice, if you actually care about them, you won't anonymously call them at 03:00 a.m. because you know it would disturb or even frighten them.

However, unrequited love can turn people into narcissists and masochists as they embody just this kind of self-centered behavior. For instance, a woman who has been rejected by her beloved might cease to even see the other person as she becomes completely consumed by her own romantic delusions and desires.

She'll experience the other person as an extension of herself and not as a separate individual. And, just like a textbook narcissist, she'll be incapable of distinguishing her own desires from those of the other.

As a result, she won't be able to understand that her nightly 03:00 a.m. wake up calls serve only herself; instead, she'll believe that all her obsessive behaviors are sacrifices for her unrequited love.

Naturally, this obsession eventually becomes painful as the woman feels that, no matter how hard she tries, she never attains the satisfaction she would from a real partnership. This, in turn, causes her to feel worthless — but she still won't be able to let go of her obsession.

In the end, as unrewarding as it might be, her obsession is her only connection to the other person. As a result, she'll cope with it masochistically by embracing her suffering and deriving satisfaction from it.

### 6. Unrequited love can be an artist’s best friend. 

Have you ever wondered how muses can so powerfully inspire artists? Could it simply be their beauty and the joy they bring?

Well, when someone who's madly in love focuses her energy on a productive task, we often assume she's just staying busy to distract herself from her beloved. What we don't consider is just how inspiring unrequited love can be.

In fact, when our desire for love is frustrated, we respond through creativity. This makes sense, since acting creatively can actually feel a whole lot like being in love.

For instance, doing creative work and being in love affect our brains in an incredibly similar way that makes us feel great. In both cases, tons of dopamine is released into our brain's center, causing feelings of euphoria and exhilaration. Simultaneously, flow is reduced to the areas responsible for judgment and negative emotions.

So the artist and the lover are alike — and in more ways than one. For example, they're also both focused on achieving a goal. In the case of the artist, she's striving to realize her grand conception; for the lover, she's fixated on the task of wooing the object of her desire. If either one of them succeeds, they experience a tremendous sense of accomplishment and satisfaction.

But, of course, fulfilled love can also distract someone from their creative work. Just consider all the time and energy a person in love spends with their lover that could be spent creating. In this sense, only an undesired woman is really free to follow her inspiration.

Just take a recent study that showed how social rejection can be incredibly inspirational for creative people. Or think of the great Isadora Duncan, the renowned "Mother of Modern Dance," who channeled the suffering from her many unhappy relationships into her art. She even used to say that love and art are inextricable.

You too can use your lovelorn state as a source of inspiration. However, if you feel your romantic obsession is controlling your life, it's important to do something — and fast. How to handle such situations is exactly what you'll learn in the next blink.

### 7. Therapeutic techniques can help adults overcome self-destructive infatuation, while crushes help teens practice love. 

If you find yourself enthralled by a lover's spell and decide you want to have your life back, it's essential to separate your values from your love object. A great technique for doing so is _cognitive-behavioral therapy_, or _CBT_, which helps patients overcome infatuation by transforming their habits of thought and behavior while recognizing their true needs.

For example, psychologist Jennifer Taitz asks her love-obsessed clients to avoid giving in immediately to the urge to contact their love interest. She also advises them to consider what they really need. By doing so, they discover that their needs run much deeper than the affection of that particular person. They might really be looking for, say, a sense of self-worth.

After this realization comes another one: that they'll never get what they need if they waste their energy obsessing over this one person. In this way, CBT guides patients to the understanding that their negative thought patterns and unfulfilled needs are the primary issue that caused their love obsession in the first place.

This is a key point because, for an adult woman, it can be self-destructive to obsess over someone; but for teenagers, a little crush here or there is actually a helpful phase of development. So, if you're a parent, it's important to recall that, for a teen, a crush is more helpful and healthy than a true romantic relationship.

According to the child and family psychologist Richard Weissbourd, few middle schoolers have developed the self-awareness, discipline and courage necessary for a romantic relationship. He also warns that very early love relationships increase a teen's chances of suffering from depression.

On the other hand, crushes are a kind of emotional cocoon within which teens can experience intense emotions without the risk of getting hurt. That is to say, they can practice being in love.

> "_One of the legs that unrequited love stands on is the importance of the beloved - all the goals and dreams you've imbued him with."_

### 8. Final summary 

The key message in this book:

**For most people, the roots of a love obsession run much deeper than the loved one. Often, pining after another actually relates to deeper emotional issues; by exploring the power of unrequited love, you can connect more deeply with yourself and your creativity.**

Actionable advice:

**Don't call, write a letter.**

If you find yourself overcome by the urge to call your unrequited love over and over again, try writing a letter instead. Simply write down everything you want to tell the person — but don't send it. This exercise will help you get things off your chest and quiet down without doing something you might regret later.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _A General Theory of Love_** **by Thomas Lewis, Fari Amini and Richard Lannon**

In _A General Theory of Love_, three psychiatrists take a scientific look at the phenomenon of love. Arguing that our emotional experience in adulthood is profoundly influenced by our childhood relationships, the authors suggest ways to undo this emotional "programming" and establish healthier relationships with friends and romantic partners.
---

### Lisa A. Phillips

Lisa A. Phillips wrote the book _Public Radio: Behind the Voices_ and contributes to the _New York Times, Psychology Today_ and _Cosmopolitan_. She is the recipient of multiple reporting awards and is a journalism professor at the State University of New York at New Paltz.

