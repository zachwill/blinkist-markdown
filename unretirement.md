---
id: 548a1cbc3363370009860000
slug: unretirement-en
published_date: 2014-12-26T00:00:00.000+00:00
author: Chris Farrell
title: Unretirement
subtitle: How Baby Boomers Are Changing the Way We Think About Work, Community and the Good Life
main_color: C1DA73
text_color: 728C20
---

# Unretirement

_How Baby Boomers Are Changing the Way We Think About Work, Community and the Good Life_

**Chris Farrell**

_Unretirement_ exposes the strain an early retirement puts not just on the economy, but on the individual. A more positive alternative is offered: "Unretirement," where older workers reorient themselves to more pleasant careers, using this new phase in their lives to make a difference to the world at large.

---
### 1. What’s in it for me? Learn what unretirement is, and how it can fix the problems caused by America’s aging society. 

We used to refer to retirement as the twilight of one's life, and later it became more appropriate to call it a sunset. Now, it's more like late afternoon. The luxury of giving up work for leisure is a recent development for humanity. We haven't always had the chance to choose a year when we could stop working and spend our time doing whatever we feel like doing. Although that dream was possible for a few decades after World War Two, the huge population of Baby Boomers now approaching retirement threatens to exhaust America's Social Security funds and lead to a demographic crisis.

_Unretirement_ is the answer. Not only is it a viable alternative to retirement, it can also solve America's pending welfare crisis, inspire retirees-to-be and help the economy as a whole.

After reading these blinks you'll have learned

  * what unretirement is, and how you can start preparing for it;

  * why it's so hard to save enough for retirement; and

  * why BMW is preparing its factories for older workers.

### 2. Retirement is a new phenomenon in human history. 

What comes to mind when you think about ageing? These days, we often immediately think of our retirement. However, it wasn't too long ago that the elderly didn't have this option at all.

Until the Industrial Revolution, a retirement that involved receiving money after ceasing work was only possible for a small handful of groups. War veterans and widows received pensions, while all others without a family network to support them were expected to continue working or face poverty.

It wasn't till the early 1900s that the first retirement options emerged, but even these were largely private and inaccessible, used as incentives by railroads and factories to encourage workers to remain with their company for as long as possible. If you left your company or went on strike, your retirement pension would be revoked, and could even be declined on other grounds, such as excessive smoking or drinking.

Retirement as we know it in America only developed during the Great Depression, a time when many people, let alone old people, were driven into penury and struggled to get by. In response, the Social Security Act was created in 1935, ensuring the elderly would receive at least some support from the US government.

But this helpful support network soon spawned idealistic visions of retirement, in which the elderly idled away their days in leisure. Communities such as Sun City in Phoenix emerged, essentially as amusement parks for the resident pensioners.

Yet even after the Social Security Act, retiring wasn't so pretty for everyone, as many people were either uninsured or didn't work for a company long enough to get all the benefits. And today, while we hold the same dreams of a retirement in paradise, we too may not be able to afford all that we hoped for.

### 3. With an aging society, retiring early is no longer an option. 

How do you feel about your job now? Perhaps you love it and can imagine working there for your whole life. But who knows, you might get to 60 and realize you want to spend the rest of your days doing what you really love instead. More often than not, you won't have saved enough money to support yourself.

This is a common mistake many make, by underestimating when they will retire and how much they'll need to save. And when we don't prepare ourselves adequately, we rely on the social system to support us. But our changing demographics may no longer allow this.

Today, we have an ageing society. This demographic trend began after the huge Baby Boom which followed World War Two, and when subsequent generations produced fewer children. Now, the elderly proportion of the population continues to grow. In 2012, 13 percent of the American population was over 65, but by 2030, this figure will rise to 25 percent!

These alarming demographic trends mean that our social systems will soon have to support far more retirees than they can afford. In fact, it is estimated that Medicare and Social Security will cost as much as _half_ the total federal expenditure in 2030.

Moreover, there are fewer young people working and contributing to Social Security. For example, 50 years ago there were five workers for every retiree, but today this has shrunk to three. In 2035, it will be reduced to two. In short, we can no longer retire and expect society to take care of us.

If this all sounds pretty bleak to you, take a look at the following blinks. An alternative approach might stop ageing being such a terrifying prospect after all.

### 4. We should forget negative prejudices against older workers – they’ve got a lot of potential! 

All these predictions might make you feel gloomy about the future. But first, consider the possibilities offered by a new approach: _unretirement_.

Unretirement means well-educated and healthy people continuing to work past the "traditional" retirement age. They needn't stick to the same career, but they also needn't squander perfectly productive years on the golf course. Instead, they can embrace their passions and skills by applying them to a new career, and live the third chapter of their lives to the fullest.

In order to do this, we must first overcome some prejudices against older workers. Perhaps you already have an image of some of your older colleagues, whom you see as narrow-minded, unable to adapt, uncreative or even crotchety. This is simply not the case.

Consider that the Baby Boomer generation is one of the healthiest and best-educated in history. They have just as much potential to start new businesses or re-orient to different careers as their younger counterparts. Plus, they have the benefit of experience, credibility and a well-developed professional network.

So instead of throwing away their potential, let's recognize older workers as the key to our demographic crisis. Studies can already predict the benefits of keeping people in the workforce for longer. In fact, if those who are still fit to work continued to do so for as little as five years longer, the taxes generated would keep the social system solvent _till 2045_. And this doesn't even take into account the productivity boost that unretirement brings — read on to find out more!

> _"We're witnessing the birth of a new business, the unretirement industry."_

### 5. Unretirement is the key to improving our economy, and your office life too. 

How exactly will unretirement benefit our economy? Some of you might be thinking that keeping older people in the workforce will decrease the job opportunities for young people. Well, here's the short answer: Nope!

In the 1960s, men were worried that if women left the kitchen and joined the workforce, they'd take all the jobs traditionally reserved for guys. Now we know that women in the workforce have been incredibly beneficial. With a larger workforce came more income, more spending, more jobs and ultimately, more wealth for everyone. We'll see the same effect if we incorporate the elderly into our workforce.

Keeping older people at work will also create another change that you can benefit from directly. How? Through a shift in work atmosphere.

Older workers value variety and flexibility. They'd like to start something new and more fulfilling, with work hours that allow them to enjoy time outside the office, too. Sounds a lot like something you'd also value in your workplace, right? Well, if employers adjust work environments to older workers then you'll benefit from this new professional climate, too.

But why should employers make changes to suit older workers? Because it pays off. This was demonstrated by BMW when they experimented with assembly line workers who were an average of 47 years old. They didn't perform as well as their younger colleagues at first, but things changed when BMW invested in adjustments like bigger screens with larger type and bigger gripping tools.

With these small changes, the older workers' productivity increased by seven percent, matching that of the younger workers. BMW expects that most of their workforce will be an average of 47 by 2017 — now that they've seen how to make changes to accommodate older workers in their company environment, they won't be having any problems.

> _"Unretirement is a critical part of the much bigger story of repairing the job market."_

### 6. Unretirement is a great way for the elderly to improve their overall quality of life. 

Up until now, you might have considered keeping elderly people in the workforce a cruel necessity driven by a potential economic crisis. In fact, it's anything but.

The financial difference made by working a few more years is astonishing. Remember how most people fail to save for their retirement and rely on Social Security? Well, for starters, the Social Security benefits you receive are a whopping 75 percent higher if you retire at 70 instead of 62.

And as you work, you can continue to save money, instead of draining your savings account. You'll also need to put by less. Think of the calculations: If a couple earns $100,000 annually, they'll need $66,000 after taxes to live at the same standard. If they retire at 62, they'll only get $25,000 in Social Security payouts per year, so they'd need savings of $891,000 to live comfortably until their projected death at around 84 years old.

But if they waited just four more years and retired at 66, their Social Security payout would increase while their savings remained intact. Therefore, they'd only need to save $552,000 to live the rest of their lives out in comfort. Wait till 70 to retire and the sum falls to $263,000!

While these numbers are promising, work is also more than just income. It's a virtue, and elderly people thrive on it like anyone else. In the modern world and in the United States in particular, a strong work ethic is highly valued. People live to work and even the wealthy encourage their children to be industrious. Work isn't just a chance to pick up a paycheck. It also allows people to build bonds with colleagues, channel their creative energy, and make a difference. What could be better for elderly people who often feel alienated from society?

So if you're now thinking that you don't want to head straight to the golf course at 62, read on for some tips about your future unretirement.

### 7. Retirement shouldn’t be an end – it’s a reorientation. 

Too many of us think of retirement as some final and irreversible event. It really needn't be that way.

An abrupt end is far more painful than a slow transition. People with a strong work ethic who live for their job find it especially hard to part from their colleagues and daily routine. Even if you're not a workaholic, a sudden end to a decades-long career is no easy thing to cope with.

Instead of calling it quits as soon as you hit retirement age, aim for a slow transition. Today, around 50 percent of older workers switch to part-time or contractual remote working, to test the waters of retirement and see if they're ready for the shift in lifestyle.

Yet many are still attracted to the idea of a retirement as soon as they can get it. To them, it's not the simple fact of working that repels them, but the job they do. Working past the typical retirement age sounds great to young people with lots of energy, but for older workers with an understimulating or overly stressful job, ten more years of clocking in are the last thing they want to contemplate.

However, unretirement doesn't mean simply staying in your rut. It's all about using your full potential to contribute to society, and a great way to achieve this is by switching careers to something that inspires you.

Say you're a lawyer and sick of the long hours and stress — this doesn't necessarily mean you have to retire. Instead, why not start a coaching program for young people? Or begin learning some new skills. Pat Snyder, a former reporter, gives us a great example of reorienting in life — she received her master's degree in applied positive psychology at the age of 66.

Remember: You're not alone with unretirement. There's a whole generation of boomers out there trying to figure it all out. Read on to see what changes we as a society can make in order to make this transition easier.

> _"The average boomer's identity is wrapped up in work."_

### 8. As a society, we should start viewing the third part of our lives as something to plan well and enjoy. 

Since society can gain so much from unretirement, shouldn't society in return be helping the elderly transition into it? Of course! Let's start with political change.

We've seen how people struggle to save enough for their retirement years and still retire too early. As this phenomenon directly impacts the Social Security system, it would surely make sense to counter it. How? By changing the way we perceive retirement, turning it into something to plan as thoroughly as your education.

Statistics show that a mere 42 percent of workers have pension coverage via their job. Even they aren't safe, because many switch jobs and thus can't be sure they'll accumulate enough savings for their retirement.

And although the increased financial benefits are an incentive to retire later, there's not much else to motivate people to keep working. So what about bonuses that specifically reward later retirements? A targeted approach by the government could prove highly effective in making well-planned retirements the norm.

Another vital change must also be made in the way we conceptualize a "retirement plan." Instead of thinking about how we're going to use our money once we leave the office for good, it's time to start thinking in terms of an "unretirement plan." This simply means considering how you can fill the third chapter of your life with meaning, as well as living comfortably.

So instead of meeting with accountants to discuss your 401(k), you should also think about what skills you've always wanted to learn, or what kinds of new people you'd like to meet.

Of course, unretirement won't change the world overnight, and it's not some kind of a miracle cure for the economy. But it comes pretty close.

### 9. Final summary 

The key message in this book:

**At retirement age people still have far more to offer society than has been previously thought. By becoming** ** _unretirees_** **, they can stay active and boost not only the economy, but their overall quality of life too.**

Actionable advice:

**Conceptualize an unretirement plan.**

As you come closer to retirement age, ask yourself whether you really want to withdraw from the workforce, or whether you simply want to quit the job that you hate. If the latter is the case, then take time to think about what you've always wanted to do, and consider how you can use your skills to make a difference. Your personal network can help you reorient yourself to a new and fulfilling career.

**Suggested further reading:** ** _The End of the Suburbs_** **by Leigh Gallagher**

_The_ _End_ _of_ _the_ _Suburbs_ tells the story of how what used to be the textbook example of achieving the American Dream is in deep trouble today. The rising cost of living and an increase in poverty and crime have made suburbs less desirable places to live. The silver lining in the death of the suburb, however, can be found in the renaissance of once-neglected urban areas.
---

### Chris Farrell

Chris Farrell is a journalist and economics commentator. He began his working life as a merchant seaman, before attending the London School of Economics and later contributing to radio, TV and print media.

