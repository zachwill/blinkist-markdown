---
id: 5a899f84b238e1000705419d
slug: unscrewed-en
published_date: 2018-02-22T00:00:00.000+00:00
author: Jaclyn Friedman
title: Unscrewed
subtitle: Women, Sex, Power, and How to Stop Letting the System Screw Us All
main_color: 99A99F
text_color: 30754A
---

# Unscrewed

_Women, Sex, Power, and How to Stop Letting the System Screw Us All_

**Jaclyn Friedman**

_Unscrewed_ (2017) looks at the numerous obstacles that women face every day on their path to equality and respect. Author Jaclyn Friedman shows the way forward. She shines a light on those who are already hard at work dismantling these barriers and explains why even though the current system may be imbalanced, but we can build a new one.

---
### 1. What’s in it for me? Join the fight to replace the toxic male-dominated society with gender equality. 

The #metoo movement has received a lot of media attention, and many people hope that it will lead to real change. But this isn't the only movement that's currently fighting for an end to the patriarchal system that has been sexualizing and dehumanizing women for centuries.

As you'll come to find in these blinks, the fight for reproductive rights and gender equality has been a long, uphill battle and it's one that can't be won without changing the system from deep within. It's a matter of teaching young men right from wrong at an early age; of giving women unfettered access to healthcare; of changing the way we represent women in our media; and of keeping religion out of our public policies.

It might seem like a massive undertaking, but it's one that has been delayed for far too long. So it's time to change the way society views female sexuality, and these blinks aren't a bad way to start.

In these blinks, you'll learn

  * how _Fifty Shades of Grey_ and the Spice Girls fail to empower;

  * how pick-up artists contribute to male toxicity; and

  * where the religious right got their start.

### 2. Our patriarchal society leads to discrimination and violence, especially toward minority women. 

Let's not fool ourselves into thinking otherwise: We still live in a society that routinely demeans women and robs them of their right to control their bodies. And when women are sexually assaulted or attacked, it's all too common for them to be blamed by authorities and the public.

While these things can happen to any women in America, there is an especially troubling history of mistreatment and dehumanization of women from minority groups. They continue to be the most vulnerable to violence.

Native American women, in particular, have a long history of being characterized as inhuman savages, even by certain church leaders. It's likely a significant reason why this group has experienced the highest rate of rape in the United States, with 49 percent having suffered instances of sexual violence.

Likewise, the number of transgender women being attacked and murdered is outrageous. Many cities and states force them to use men's public restrooms, which leaves them unnecessarily vulnerable to potentially deadly violence.

At the heart of this violence lies the way society views these people. When we see them as different from other human beings, our empathy for them decreases and we are able to treat them differently than we would expect to be treated ourselves.

Black women are another group with a long history of dehumanization. For centuries following America's foundation on chattel slavery, black women were treated more as animals than humans, as if they needed taming by the white man. This disturbing perception has been used to justify countless crimes of rape and abuse, even until our present day, where black women suffer disproportionate injustice.

This injustice can also be seen in current statistics from the United States showing how young black women with a history of sexual abuse are far more likely to be in prison as a result of the abuse they've endured than white women with a similar background. Even though only 14 percent of all girls in the United States are black, in juvenile prisons, 33% of the girls are.

As we'll see in the blinks ahead, women of all races and backgrounds are being subjected to inequality, injustice and limitations to their freedom and liberty.

### 3. Fake empowerment is keeping women from truly achieving power. 

It's easy to think we've made great strides in women's liberation. After all, there have been times when women couldn't vote in the United States, were treated as outcasts if they had pre-marital sex, or faced charges if they tried to obtain birth control pills without being married. But there are still numerous ways in which women are being oppressed.

One of the biggest trends these days is what the author calls _fauxpowerment_, which is the popular yet phony idea that modern women are completely free and liberated.

Sure, some women can be open and public about their sexuality. But this is only a distraction from the underlying misogyny and old-fashioned attitudes that continue to reign supreme among the powers that be.

Take _Fifty Shades of Grey_. These books are often framed as an example of how free a modern woman is to explore her sexuality in a way that has been traditionally frowned upon. But this is fauxpowerment, plain and simple, because at the heart of this story there's still a woman who is being manipulated by a wealthy white man.

What makes this kind of fake empowerment even worse is that men are still often the ones making huge profits from its success.

Hugh Hefner, founder of _Playboy_ magazine, often called himself a feminist and liked to point out how much money he gave to pro-choice causes. But his magazine was strict in only featuring women who fitted Hefner's idea of "the girl next door," which excluded any women who might have unwholesome or assertive ideas about their sexuality, or who might challenge the traditional male-dominated relationship.

The Spice Girls were another prime example of a male-manufactured concept of female empowerment. The group was created by manager Simon Fuller, who developed a cheeky, sexy message that would appeal to young girls while being completely inoffensive to male sensibilities. While naive teenagers might have viewed the Spice Girls as cool and feisty women, they were just distracting their generation from the reality that gender equality meant more than singing catchy tunes and dancing on tables.

### 4. We need to stop sexualizing women in the media and shaming those who express their sexuality. 

In a perfect world, every woman would be comfortable with her sexuality while feeling free to enjoy it and explore it. But unfortunately, that is far from the case, as female sexuality is continually co-opted by men and used to make women less human.

The media, in particular, uses female sexuality to present women purely as objects of male sexual desire, rather than human beings with their own emotional lives.

All too often, women appear in films and television programs for the sole purpose of being a sexual conquest for the male protagonist. And all this does is further obscure the fact that women are intelligent, emotional and complex human beings.

To see how damaging this constant misrepresentation is, an experiment was conducted in which female participants were asked to solve math problems while first wearing normal clothes and then wearing a swimsuit. As you might suspect, the participants scored worse when wearing the swimsuits, and the researchers concluded that women find it difficult to be their intelligent selves when they are made to feel like sex objects.

On the other hand, another danger comes from people frowning upon women who are actively embracing their sexuality.

Some feminists believe it's appropriate to cast shame upon women who work in industries where they are sexualized, such as the porn business. In some instances, the feminists will assume that these women are too dumb to understand what they're doing and what it means. But what these feminists fail to consider is that a woman might have personal reasons for choosing such a profession and that they might find it empowering to explore their sexuality.

The difference between a woman deciding to be openly sexual and a woman who is being sexualized depends a lot upon the context and intention of the situation. It is important not to jump to conclusions or behave self-righteously and confuse the two.

### 5. Women should be free to pursue their sexuality instead of adhering to the male ideal. 

Traditionally, sex was perceived as something that only men actively sought out, with women going along with it but not seeking it for their own enjoyment. Only relatively recently have women understood that they've got just as much right to play for their own benefit.

Society has generally regarded sex as a commodity, something that men hope to buy from women at as low a price as possible. This is the angle that many popular male pick-up artists have promoted, showing men how to get laid with as little effort and money as possible. For them, a successful date would be one that didn't even involve paying for a drink. In this macho mentality, no thought is ever given to whether a woman should receive any pleasure from the date or not.

But if we look at things from a biological perspective, women, not men, have clitorises — the only part of the human anatomy with the sole purpose of providing pleasure. And contrary to what you may have heard or read elsewhere, women don't need commitment or a certain amount of romance to enjoy sex. Studies show that women are just as attracted to the idea of casual sex as men are, but they have to reckon with the inherent danger of being alone with a stranger and uncertainty over whether that man would give any thought at all to their pleasure.

Because of this, many women are sadly insecure about their sexual preferences. There are very few reference materials available for young women to read, and society doesn't encourage them to feel comfortable about exploring their sexuality. In fact, women who are open about exploring such matters are often made to feel shame.

Yet, there are sexualized media images of women everywhere they look. And even the products and advertising content targeted at women are usually made, and primarily enjoyed, by men. This is often the case with products related to female fauxpowerment, because while women are supposed to enjoy this representation, it conflicts with their natural instincts about what they want. So is it any wonder that women today feel insecure and uncomfortable?

> _"The most dominant model of sexuality in the United States treats sex as a commodity in which women 'give it up' and men 'get some.'"_

### 6. Right-wing Christians in the United States have attained too much control over reproductive rights. 

A common question on the minds of many American women today is, "How did we end up in this mess?" At the top of their concerns is how the _Religious Right_, a group of ultra-right-wing Republican Christians, gained so much political power that they've become the ones deciding what a woman can and can't do with her body?

The modern movement can be traced back to 1976 when the Christian conservative Bob Jones University lost its federal tax-exempt status because it wouldn't allow interracial dating on campus. The evangelical community was immediately outraged by this and began to rally support against government interference. But as they built up some political momentum, they eventually shifted their focus from defending racist policies to condemning abortion.

This new right-wing religious faction understood that promoting blatant racism wouldn't get them very far in mainstream politics, but the subject of abortion was another matter. There were enough Republican politicians willing to take up this cause on behalf of evangelical voters that a strong, mutually beneficial political force soon took shape.

Even though the United States is supposed to be a shining example of democracy, it is essentially a theocracy, promoting extreme religious values and bullying women who dare to make decisions that are in their best interests.

One of the worst pieces of legislation to come out of this theocracy was the Religious Freedom Restoration Act. While it was promoted as a law that would protect Americans' right to practice any religion, it actually protects discrimination against those who don't subscribe to the Religious Right's values. For example, under this act, a Christian pharmacist can legally deny a woman access to emergency contraception.

What's perhaps worse is that the Religious Right have begun setting up "healthcare" facilities similar to Crisis Pregnancy Centers. They pretend to be the kind of clinics that can perform abortions but instead staff present vulnerable women with hours of traumatizing and gruesome medical footage to manipulate them out of terminating their pregnancies.

> A group called the Satanic Temple are trying to fight some of the worst laws on abortion by saying they infringe on their religious beliefs. They are currently fighting a 2016 Texas law saying that women must hold a burial for fetal remains after an abortion.

### 7. The fight for reproductive justice is far-reaching. 

One of the most important steps in achieving gender equality is for women to gain control over their bodies and win back their lives and liberty from male oppression.

The name of the _reproductive justice movement_ was co-coined by Loretta Ross, a feminist activist with a long track record of fighting for women's issues. As the name suggests, Ross's cause is primarily about winning the right to decide what happens to her body, but it also takes into account the many considerations that influence decisions about starting a family and other factors that affect women's bodies. This includes access to healthcare and the economic circumstances in which a woman might live.

Unfortunately, black women like Ross are quite familiar with being blamed and persecuted for their sexuality. Anyone who comes from a low-income neighborhood knows the injustice of being at the whims of white, male politicians who get to decide what kind of healthcare access they'll have and how much funding the social programs in their city or state will get.

While it would be helpful for more people to get involved in the reproductive justice movement, we can't lose sight of what it really means. Many people eager to join the movement want to focus solely on abortion rights, but what sets reproductive justice apart is its mission to change the whole system — and that means reaching beyond healthcare issues.

Reproductive justice is about fighting for women to be taken seriously by the police when they report sexual assault, and for the legal system to take swift action against rapists and sexual predators. It also seeks to end rape culture by eliminating the rampant sexualization and objectification of women.

Reproductive justice is about the big picture, and you can join the fight by helping people young and old change the way they think about sex and women's rights. Then they will recognize inequality when they see it.

### 8. Improvements to sex education can lead to healthier sex lives and less toxic masculinity. 

Students in US public schools are supposed to be taught a wide range of subjects to prepare them for adulthood. Yet somehow, sex, one of the fundamentals of life, goes unmentioned, or worse, gets misrepresented.

In many states, sex education isn't a mandatory course. But in states where it is, the class is often focused on teaching abstinence, which means students learn nothing about how to give or receive pleasure and the importance of this in a healthy sex life. So, right from the start, girls, in particular, are being set up to think that bad sex is something they have to endure. And by teaching abstinence over the use of contraceptives, schools are increasing the chances of underage pregnancy.

Other nations point to a better way. Some of the world's most comprehensive sex education is being taught in the Netherlands. Unsurprisingly, the country has one of the lowest global rates of teen pregnancy.

Like it or not, students also need to learn about rape and consent.

This means putting an end to the idea that rape is what happens when a woman is attacked while walking alone at night. The far more likely scenario is being at a college party and getting forced into non-consensual sex.

Abstinence teaching also avoids dealing with the major problem women face, the idea that they must limit their freedom and behave a certain way if they hope to be safe. Instead, male students need to be taught that sex isn't something they're owed and can take whenever they feel like it.

In this way, sex education could be a powerful tool for ridding society of toxic masculinity.

We should be teaching boys that women have an equal right to enjoy sex, and instilling a healthy attitude toward female pleasure. Otherwise, young men are bound to learn a different attitude from America's most popular sex educators, the biggest instillers of toxic masculinity — the porn industry.

By learning a healthy attitude toward consent, they can understand early on that being a man doesn't mean being defined by sexual conquests. They can also understand that the issue of consent doesn't disappear because of the way a woman is dressed or how conscious she is at a party.

A woman's agency is just as important as a man's, and this is worthy of teaching in school.

> _"When all we tell girls is that sex is scary and bad, they expect it to feel that way. We're teaching them to expect abuse."_

### 9. Final summary 

The key message in this book:

**One of the most important issues we face is our need for sexual equality. Society needs to understand that women have a right to control what happens to their body, to explore their sexuality without fear, guilt or shame, and to hold men accountable for sexual assault. A male-dominated power structure currently ensures that old and discriminatory attitudes are kept in place. So we must topple this system and replace it with one that promotes equality and true empowerment.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _She Comes First_** **by Ian Kerner**

_She Comes First_ (2004) is a guide to improving a woman's sexual experience through the act of cunnilingus. It emphasizes the value of the female orgasm and argues for a reduced focus on penetration.
---

### Jaclyn Friedman

Jaclyn Friedman is a writer, speaker, feminist and women's pleasure activist. Her bestselling book, _Yes Means Yes: Visions of Female Sexual Power and a World Without Rape_, was a major contributor to the "yes means yes" standard of sexual consent, which is currently used on many US college campuses.

