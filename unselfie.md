---
id: 58eca986511add00045ae292
slug: unselfie-en
published_date: 2017-04-13T00:00:00.000+00:00
author: Michele Borba
title: Unselfie
subtitle: Why Empathetic Kids Succeed in Our All-About-Me World
main_color: FEF433
text_color: 807B19
---

# Unselfie

_Why Empathetic Kids Succeed in Our All-About-Me World_

**Michele Borba**

_Unselfie_ (2016) takes a close look at the human experience of empathy — why it's declining, what it means for our children and how we can get them back on the right track. These blinks explain the many benefits of empathy and outline several practical ways to help turn your child into a caring and altruistic future leader.

---
### 1. What’s in it for me? Help your child build the Empathy Advantage. 

In our hyperindividualistic society, children are constantly being encouraged to compete with one another, whether it's for the best grades in their class or athletic accolades. With so many aspects of a child's life steeped in competition, it's little wonder that many children have grown up with their main concern being me, myself and I.

It might seem logical to assume that helpful, empathetic children can only lose in such a self-interested environment — but, in fact, the opposite is the case: recent studies reveal that empathetic children are healthier, happier and more successful than their more self-centered peers. This is the Empathy Advantage.

So how do you foster empathy in your children? Let's find out.

In these blinks, you'll learn

  * how an approach known as "jigsaw learning" can turn strangers into friends;

  * why hero stories can discourage a child from helping others; and

  * how beads and sequins can help your child calm down.

### 2. Evidence shows that empathy is decreasing among young people, while narcissism is on the rise. 

Did you know that "selfie" was voted word of the year in 2014 by Oxford Dictionaries? The decision was made following a 17,000 percent increase in the word's usage over the previous year.

This obsession with photos of ourselves is symptomatic of an _all-about-me society_ that's ruled by ego, in which everybody wants to be the center of attention. Psychologists are even in agreement that empathy is on the decline, while narcissism among young adults is steadily rising.

Just take psychologist Sarah Konrath, whose University of Michigan, Ann Arbor team considered 72 behavioral studies among college students over the last three decades. Their results, which were published in _Personality and Sociology Review_, paint a disturbing picture.

They found that students today are 40 percent less empathetic than their predecessors were 30 years ago. In addition, rates of narcissistic behavior, including selfishness, an inflated sense of self-importance and a tremendous need for admiration, have soared by a whopping 58 percent!

Or consider a Gallup poll that found that while only 12 percent of teenagers in the 1950s agreed with the statement "I am very important," that figure has hovered around 80 percent since the late 1980s.

The drop in empathy is also made abundantly clear by the rise in bullying among school children. After all, children who bully others do so by dehumanizing their victims and failing to see life from their perspective, which is why soaring rates of bullying are a strong indicator of decreasing empathy.

And although children have always been mean to one another, recent studies have found that bullying has reached an all-time high in recent years. One study showed a 52-percent increase over a mere four years. Another study determined that children as young as three years old were engaging in bullying behavior.

But what's perhaps most disturbing is that one out of every five middle schoolers reports considering suicide because of peer cruelty.

We can thus see that children today are much more self-absorbed than previous generations were at the same age — but that doesn't mean that they have to stay this way. In the next blinks, you'll learn how to transform this mindset.

### 3. Adults can help kids develop emotional literacy. 

Just as they're not born being able to change their own diapers, kids don't come out of the womb knowing how to understand and act with empathy. Even especially bright kids need years of experience before they can read body language and facial cues with fluency.

That being said, you can coach your children through this process.

First, you can use face-to-face contact to teach kids to read emotional signals. This is crucial, since children and teens are especially prone to misreading such gestures, which causes them — and, potentially, those around them — lots of unnecessary suffering.

To lend them a hand, pay special attention to your own body language and be ready to explain things like, "don't worry, I'm not angry. I'm just tired. If I rub my eyes you'll know I'm tired."

You can also do some casual people watching with your child. During a trip to the mall, you might ask, "who looks angry, tired or bored?"

Second, you can use books and films to teach kids about emotions. To do so, you might watch a few minutes of a TV soap opera together with the sound on mute and make a game of guessing how the actors feel. This kind of exercise is a useful way to teach children about body language.

Books are also great for this. If the main character in a story expresses an emotion, ask your children, "how can we tell he's scared?" or "have you ever felt like that?" Doing so will give your kids an opportunity to understand an emotion from the inside out.

And finally, give your kids an emotional vocabulary. After all, you can't talk about something without the appropriate language to express it, and that's especially the case when it comes to emotions. So, expose your children to words like "eager," "confident" or "dismayed" that go beyond the simple emotions of "happy" and "sad."

To make sure you're using emotional words when speaking with your children, you can make a point of talking about your own feelings. Be especially sure to use lots of emotional words when playing with boys, as they tend to hear less of this language in their daily lives.

### 4. Teach kids empathy by asking them to walk in another person’s shoes. 

What do you need to do to make sure your child thrives? Well, it's essential for her to be able to advocate for her own interests — but that's not enough.

To be happy and successful, kids also need empathy. In fact, children who understand the perspectives of others have more friends and stronger, closer relationships than self-absorbed children. Not only that, but empathetic children are happier, better adjusted and more likely to resolve conflicts or stand up for victims.

Such positive traits are known as the _Empathy Advantage_ and they're linked to more favorable life outcomes, including better job prospects, higher salaries and even greater educational attainment.

Luckily, any child can develop the Empathy Advantage through a few careful exercises, and the first of these is to reverse sides in an argument.

Say your two children come running to you, each begging for you to take their side in a disagreement. Instead of doing so, ask each child what he or she thinks the other child will say about the situation. By grappling with this question, both children will learn to see the situation through the other child's eyes.

Another way you can develop your children's empathy is through the use of props and role play. This kind of strategy will help your child step outside of her own world and into that of another. For instance, you can put on a tiara, an army boot or a sari, then ask your child who they think the wearer of these objects is and what they think about life. What are that person's fears, hopes and dreams?

This is also a good approach if your child bullies someone. While young children might not understand questions like "how would you like it if Bobby did that to you?", props can help them empathize. So, if you instead say "here's Bobby's hat. You be Bobby and I'll be you," and then act out a scene in which you're mean to Bobby, most children will come away from the experience understanding how painful it is to be bullied.

Up until now, most of what you've learned is about teaching kids empathy. Next, you'll find out how to help your child perceive and regulate her own emotions.

### 5. Meditation fosters a child’s self-regulation and social behavior – and it’s easy to teach. 

Mindfulness and meditation have both experienced huge surges in popularity in recent years. But what are the actual benefits of this increased focus on awareness and living in the present?

Well, astonishingly, the brains of Tibetan monks have been shown to be 30 times more powerful in terms of concentration and high mental activities as the average person's brain. Luckily, you don't need to be a monk to meditate; in fact, many schools in the United States have started encouraging their students to meditate on a regular basis — and for good reason.

Meditation is an excellent way to enhance a child's self-regulation and social behavior, since, by meditating, children practice calming themselves down and shifting their focus. A typical mindfulness practice has the practitioner focus on the present moment, for example by observing his breath or the sensations in his hands, all without judgment. Focusing their attention in this way helps children observe annoying situations, such as when someone is teasing or pestering them, rather than acting out against them.

A 2013 study found that a group of elementary school students demonstrated better self-control and greater respect for others after participating in a mindfulness practice. Furthermore, a 2015 study of fourth and fifth graders found that practicing mindfulness made them kinder, more helpful, better at regulating stress, more optimistic and better at math.

Mindfulness has many demonstrable benefits for children, and there are easy ways to adapt such techniques to your child's needs. You can begin with an exercise called _candles and flowers,_ in which you have your child imagine smelling a flower and then blowing out a candle.

This practice combines the two meditation techniques, namely visualization and controlled breathing, into a fun, kid-friendly exercise.

Or, try filling a jar with water, beads, glitter and sequins and have your child shake it like a snow globe. Just watching the swirling glitter settle will relax him, but you can also help him imagine that the jar is like his mind, which can at times feel chaotic before eventually returning to a state of peaceful equilibrium. Alternatively, you can use it to explain that people sometimes get agitated and need to find their own ways of calming down.

### 6. To reconnect with children who have self-segregated, make them listen to each other’s experiences and strive for shared goals. 

At many schools, kids divide themselves into subgroups that rarely interact. At any given school, you might find cliques of jocks, nerds and artsy kids, or discover that children group themselves according to social class or racial background.

However, when children of varying beliefs and perspectives work together toward a common goal, they can become the strongest of friends.

Just consider a dramatic example that occurred shortly after segregation was abolished in Austin, Texas. In 1971, a school in Austin experienced race riots as latino, black and white children were abruptly integrated into the same classes.

It was then that Elliot Aronson of the University of Texas came up with what he called _jigsaw learning_, a practice in which children are split up into multiethnic groups to prepare presentations on specific topics — say, the battle of Gettysburg. Each child takes responsibility for researching a separate but crucial aspect of the project, and the team's success thus depends on each person's contribution. In other words, each child holds a piece of the puzzle.

After several jigsaw learning lessons, the riots subsided and kids from different backgrounds even began playing together at recess and perceiving one another as members of the same group.

Another strategy is to coach kids from diverse groups to consider the perspectives of those in the other clique. Since the early 1990s, the organization Seeds of Peace has been inviting teens from conflict regions to a summer camp in Maine.

While there, they eat, sleep and learn together, but also attend twice-daily _dialogue sessions,_ in which kids from both sides of a conflict, such as Israelis and Palestinians, listen to each other's personal experiences of the ordeal. Through this process, they learn to adopt and understand one another's perspective.

### 7. Children can prevent bullying if you give them the tools. 

Every morning, millions of students are scared to go to school because they know what's waiting for them. These kids are the victims of bullying, and it's up to adults, especially parents and teachers, to protect them.

But that doesn't mean children shouldn't be part of the solution. In fact, kids can and should play a central role in the fight to stop bullying. After all, 85 percent of bullying happens with no adult present and studies have found that, in 57 percent of cases in which peers intervene, the bullying ceases within ten seconds.

Children are thus essential to preventing bullying and need to be taught to stand up for their peers. But how?

First, by breaking the _snitch code_. Kids are often afraid to look like tattletales among their peers and, especially as children age, this fear can prevent them from telling an adult that someone is being abused or is in danger. It's essential to educate kids about the difference between telling on their friends and reporting serious injury and abuse.

From there, you should teach children how to help a victim of bullying. It can be dangerous for kids to intervene in a physical fight, but it's always possible for the child to console the victim after the fact. The child can also be told to get help in the moment, or tell a trusted adult what happened later on.

Finally, positive affirmations and _courage chains_ can be used to encourage kids to stand up for their peers. Just take St. Dominic's Elementary School in Alberta, Canada, which has its students make courage affirmations like, "I dare to do what I believe is right." This practice is intended to build students' confidence to stand up for those around them.

The school's staff also encourages students to do something courageous every day, whether it's standing up for a fellow student or inviting a new classmate to play. Through this effort, they feel brave enough to act upon their beliefs, both at school and in the outside world.

### 8. You can turn kids into changemakers by helping them trust their ability to make a difference and sensitizing them to social issues. 

Everybody loves a good hero story, whether it's about the fireman who single-handedly saved 20 lives or the daring police officer who prevented a mugging. But such stories can also discourage us from taking action by cementing the belief that only extraordinary people can do extraordinary things.

So, how can you help your children become do-gooders and changemakers?

The first step is to instill a _growth mindset_. Instead of letting them believe in superpowers or natural ability, you should teach your children that how they participate in the world depends on their own efforts to become intelligent, fit and empathetic.

Given this mindset, the more opportunities your children have to help other people, the more they'll believe that they _can_ help and the harder they'll work to be helpful — especially if you point out what a difference they're making.

For example, you and your kids could volunteer at a soup kitchen. Doing so will show them that their effort can bring about change, and this lesson will be redoubled if you say things like, "look at how grateful that man looked!"

Once your kids have a growth mindset, you can sensitize them to social issues and encourage them to seek out solutions. This can be done simply enough by keeping abreast of current events; when a suitable issue presents itself, such as poor conditions in refugee camps, you can discuss it with your child, ask her opinion and learn how she thinks she can help at the local level.

A good example is when eight-year-old Vivienne Harr saw a photograph of two Nepalese child slaves in the newspaper and took it upon herself to learn about the issue. From there, she set up a lemonade stand and, instead of charging a fixed price, asked patrons to "give what's in their hearts." Her incredible effort got a lot of attention and, in the end, she raised over $100,000 for antislavery organizations.

### 9. Final summary 

The key message in this book:

**Children today live in a self-absorbed culture that makes them ill-equipped to understand the emotions of others. Nonetheless, parents and teachers can help children learn to feel greater empathy by teaching them about emotions, showing them how their actions affect others and outlining what they can do to change the world.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Age of Empathy_** **by Frans de Waal**

_The Age of Empathy_ (2009) debunks popular theories which suggest that human nature is inherently selfish, cut-throat and prone to violence. Evidence provided by biology, history and science makes clear that cooperation, peace and empathy are qualities that are as natural and innate to us as our less desirable traits.
---

### Michele Borba

Michele Borba is an internationally renowned author who specializes in moral education and nurturing child resilience. She has been featured in the _New York Times_, has appeared on NBC's _Dateline_ and BBC Radio's _Today_ and is the author of 22 books.

