---
id: 594ef421b238e10005bb48ae
slug: unshakeable-en
published_date: 2017-06-26T00:00:00.000+00:00
author: Tony Robbins
title: Unshakeable
subtitle: Your Financial Freedom Playbook
main_color: 897248
text_color: 705625
---

# Unshakeable

_Your Financial Freedom Playbook_

**Tony Robbins**

_Unshakeable_ (2017) is a helpful beginner's guide to navigating the murky waters of today's financial markets. You'll discover practical rules and a series of core principles that will put you on the right path to making smart investments and improving your financial well-being. Don't be afraid of the stock market; rather, put it to work for you and your future.

---
### 1. What’s in it for me? Don’t be intimidated by your finances – own them! 

When in your twenties, it's easy to feel like you're doing a good job with money and finances if you're able to keep a roof over your head, feed yourself and pay your bills. But what if you want to do more than just survive?

You probably won't be able to scrape together a living from juggling part-time gigs forever — nor will you want to. So what can you do to ensure that you can afford to buy a house, send your children to college and enjoy a comfortable retirement? Even if these concerns seem far away, it'd be wise to start saving and investing a little bit right now. Your future you will thank you.

In these blinks, you'll learn

  * why focusing on losing rather than winning will serve you well;

  * why financial-market disasters might be a good time for investing; and

  * that you might have to fight your biology for the sake of your wallet.

### 2. Compound interest means it’s never too early to invest. 

Financial markets can seem so complex and intimidating that one forever balks at the thought of making an investment. So let's first look at some basic aspects that will make investing more approachable.

To navigate the markets, it's important to learn to recognize patterns.

Humans have an innate ability to pick up on patterns and time their actions accordingly. Long ago we recognized the regular change in seasons, which allowed us to successfully plant the right crops at the right times.

A financial market is similar: through observations, you can recognize patterns and find the right time to make an investment.

One basic pattern has to do with _compound interest_, the money that gets added to the initial sum of your savings and which, over time, continues to add value to an investment. Because of compound interest, you should start taking a portion of your paycheck and investing it as soon as you can; the more years that go by, the bigger the value will be.

For example, let's say that every year since your nineteenth birthday you've taken a total of $3,600 from your earnings and invested it. According to the average return of the US stock market for the past century, that means your investment will grow by 10 percent each year. So, by your thirty-fifth birthday, you'll be looking at an investment that's worth $106,782.

Now, let's say you decide to wait until you're 27 to make this investment; by the time you're 35, that amount will only add up to $53,775.

That's almost exactly half of what you would have had by starting eight years earlier. Clearly, it's worth getting started as soon as possible.

### 3. Focus on avoiding losses rather than earning big gains, and know that the market is unpredictable. 

So it's good to start early, but how do you recognize a good investment from a bad one? To help them in this task, expert investors have what they call the _Core Four_ principles.

The first is to focus on how not to _lose_ money, rather than on how to make it.

Obviously, no one wants to lose money. But if you're going to invest, you're going to have to risk a loss — and that means you have to understand how difficult it can be to earn back money that's been lost on an investment.

Let's say you invest $1,000 and lose 50 percent, leaving you with $500. You might think that turning around and making a 50-percent gain will get you back to where you started. But that 50 percent will actually only get you halfway there, since a 50-percent gain on your remaining $500 will earn you $250, giving you a total of $750.

To get back to where you started, you'll have to earn 100 percent of your remaining investment.

This is why you should prefer safer investment options that have a limited downside.

Another guideline for avoiding losses is remembering that no one can predict the financial markets. Even the most experienced and celebrated investors make mistakes.

Ray Dalio, the creator of the renowned investment firm Bridgewater Associates, is considered a superstar of the stock market. But, back in 1971, he learned that no one can know for certain what the future holds.

That year, President Nixon had taken the United States off the gold standard, a move that resulted in the value of the dollar plummeting. Everyone, including Dalio, expected the stock market to take a tumble as well — but it didn't. It soared. And from that moment on, Dalio knew that it was impossible to ever predict anything with absolute certainty.

This is why all the experts know that to avoid losses, they also need to make investments that are robust in the face of unexpected market events.

> _"Rule number one: never lose money. Rule number two: never forget rule number one." - Warren Buffett_

### 4. When investing, find a balance between low risk and high reward and use falling prices to your advantage. 

In any movie about Wall Street, there's usually a character who says something like, "If you want to make big money, you gotta take big risks." Well, this is a big lie.

In order to focus on not losing money, it's best to follow the second core principle and look for investments that are relatively low risk and offer relatively high rewards.

This relationship is known as an _asymmetric risk/reward_ and it's what a lot of good investors look for.

One such investor is the highly respected trader Paul Tudor Jones. To help make decisions, he uses a _five-to-one rule_. That is, he only invests if he can expect to earn at least five times his initial investment. So if he puts in $100, he'll expect to earn $500.

By following this rule, Jones can fail 80 percent of the time and still come out even.

If he makes five investments of $1,000, he only needs one investment to meet his expectations and earn him that $5,000 to be back where he started. However, if he is right about three of his five-to-one investments, he'll earn $15,000, and only lose $2,000.

As we can see, Jones finds success by following the first and second core principles. He's taking into consideration the unpredictable market and earning rewards without taking major risks.

Another way of safely increasing your chances of rewards is to invest in undervalued assets, especially after the market has taken a downward turn and is reflecting people's pessimistic attitude about the future.

Following the 2008 financial crisis, the market was in bad shape, which is why it was the perfect time to invest in the many reliable assets that had dropped in value.

Take the Citigroup banking corporation. In March of 2009, their stock had fallen from $57 to 97 cents per share. Yet, within just five months, the price was back up to $5 — that's a 500-percent increase!

### 5. A smart investor uses knowledge of taxes to make better decisions. 

If you're interested in making investments, it pays to be smart about your taxes, since they can eat up a huge portion of your earnings. That's why the third principle of the Core Four is to _know your taxes_!

For example, the tax on short-term and long-term capital gains, which is, respectively, income earned from investments less than or more than a year old, varies from state to state. So it's worth knowing what your state charges and whether you might save money by cashing out an investment before one year has elapsed.

It's also good to know about _mutual funds_, a type of investment vehicle made up of stocks, bonds and other assets, combining investments from multiple investors.

If you're thinking of investing in a mutual fund, you should know that, for tax purposes, all such investors are considered owners and are liable to be taxed on short-term gains. And because mutual funds trade their shares frequently, these gains will almost always be taxed at the higher short-term gains tax rate.

Being smart about taxes also means knowing the difference between net and gross sums.

If someone tells you that a certain investment has provided a high return, you can confidently ask whether that amount is the _net sum_, which is the total after taxes and fees, or the gross sum — the amount before those deductions have been made. Obviously, the former is a better indicator of the true quality of the investment.

It's also smart to search for investment options that will result in the least amount of investment fees and taxes. This may lead you to look at _index funds_ over mutual funds.

Index funds don't have managers who charge expensive investment fees. Instead, they're composed of stocks from an index such as the S&P 500, which includes successful companies like Apple and Microsoft. These stocks are only traded to reflect changes in the composition of the S&P 500, meaning that the shares are held for longer, thereby enabling you to avoid the higher short-term tax rate on gains.

Clearly, having a good sense of what taxes and fees you face will help you make better investment decisions.

### 6. A diverse portfolio will help protect you against changing trends and market crashes. 

Now we come to the fourth principle of the Core Four, which is perhaps the most familiar — diversify!

This is an important and well-known rule since diversifying your portfolio is a traditional and reliable way of protecting your investment. It's the financial equivalent of not putting all your eggs in one basket.

Essentially, by having a diverse portfolio, you can ensure that you'll still have healthy investments, even when trends change and those inevitable yet unpredictable shifts in the market occur.

So don't put all your money in one asset class, such as real estate. If you do, you may find out just how quickly it can all vanish when the market comes crashing down, as it did in 2008.

There are four ways you can diversify: _across different asset classes; within asset classes; across different markets, countries and currencies; and across time_.

Different asset classes include stocks, which are shares of a company; real estate; or bonds, which allow you to invest in an interest-bearing loan between two parties.

Adding more than one asset class to your portfolio is a great way to diversify, as is making multiple investments in the same asset class. So rather than investing only in Microsoft stock, you could also buy some shares of Apple and Hewlett-Packard as well, just in case Microsoft runs into a crisis.

Also, remember that the financial markets are global and you should feel free to look beyond your country's borders.

Finally, diversifying your portfolio across time means that you should always be adding capital to your portfolio, whether you do this monthly or yearly. The market is always fluctuating, and you can improve your chances of latching on to a new trend by adding new assets on a regular basis.

### 7. Following your instincts can lead to bad decisions, so stay disciplined and use a checklist. 

One of the reasons we need rules and principles to help guide our financial decisions is because of the way our brains are wired.

The human brain hinders our ability to make rational investment decisions because we instinctively respond to losing money the same way we respond to life-threatening situations.

The way your ancient ancestors reacted to the sight of an approaching saber-toothed tiger is the same way your brain reacts to the stock market crashing and taking all of your money with it. Both of these events are processed as being dangerous threats to your livelihood, and the natural response is to immediately withdraw and escape.

When it comes to your finances, this is an unfortunate reaction since often the best response to plummeting stock prices is to _invest more_. Remember, when prices are low, it's the perfect time to invest in an undervalued stock that is sure to rebound in no time at all.

This is why rules and checklists come in so handy. Before you make a mistake by being reactionary, you should refer to a list of criteria and thus identify the most rational decision.

It's sort of like flying a plane. During takeoffs, landings and emergencies, pilots have long and detailed checklists to go through before making any important decision. And this is precisely what any great investor will do.

A good checklist is a way of protecting you from yourself; it turns your decision-making process into a strict and reliable discipline.

Remember the esteemed Paul Tudor Jones and his five-to-one rule? He would never make an investment or trade without first referring to his checklist, which includes questions such as, "Does the investment have a favorable risk-to-reward balance?" And, "Can I expect this investment to have a five-to-one return ratio, or only a four-to-one ratio?"

Okay, now that you know some of the basic questions to ask, and some of the basic potholes to look out for, it's time to stop procrastinating, and start investing!

### 8. Final summary 

The key message in this book:

**Financial success and security are within your reach — no matter what job you have or how small your paycheck is. With the right knowledge and an action plan in place, anyone can make good, lucrative investments. And with a little experience and smarts, you can prosper through even the most volatile and uncertain of times.**

Actionable advice:

**Look out for investment fees.**

Investment fees can be treacherous. In fact, if you're not careful, they can end up eating up two-thirds of your earnings. Take an average investment fund, for instance, which charges its clients a 2-percent yearly fee. Now, say that on average the fund delivers a 7-percent return on your investment over 50 years. This means that, after 50 years, every dollar will be worth nearly 30 dollars. With a yearly fee of 2 percent, however, your return is not 7 percent, but 5, and fifty years down the line, each dollar will only be worth just over 10 dollars, instead of 30. This illustrates how disproportionate the impact of investment fees can be! 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _MONEY: Master the Game_** **by Tony Robbins**

Do you want to master money, and make it work for you? In this book you'll discover the steps you need to take to achieve real _financial freedom._ Whether you're just starting your career or moving toward retirement, _MONEY_ offers sound advice from seasoned professionals on saving and investing so you can live the life you want.
---

### Tony Robbins

Tony Robbins is an entrepreneur, popular public speaker and writer, whose focus is on business strategy and personal development. He's coached a variety of successful companies, athletes, entertainers and even presidents. Robbins is also a philanthropist, donating profits from his books to Feeding America, a charity that provides food to the needy. His other books include the best-selling _Money: Master the Game_.

