---
id: 5a535c22b238e100063370ef
slug: unstoppable-en
published_date: 2018-01-12T00:00:00.000+00:00
author: Maria Sharapova
title: Unstoppable
subtitle: My Life So Far
main_color: E7F737
text_color: 7A8503
---

# Unstoppable

_My Life So Far_

**Maria Sharapova**

_Unstoppable_ (2017) tells the personal story of Maria Sharapova, the internationally renowned and respected tennis star. It chronicles her life, from when her family fled Belarus after the Chernobyl disaster, through to her relocation to the United States and her incredible career achievements. Sharapova's autobiography offers a testimony to the power of strong will and unwavering determination.

---
### 1. What’s in it for me? Learn where success really comes from. 

What does it take to be a great tennis player? Well, it takes many things — with great talent, strength of will and the courage to endure injury and criticism among them. Sharapova fits this mold better than most. Indeed, her position as one of the world's greatest tennis players is predicated on her innate ability, her determination and her confidence.

But Sharapova's autobiography isn't just a logbook of wins and losses. What sets it apart from other autobiographies is Sharapova's way of storytelling and her particular perspective on the world.

Reading these blinks, you'll learn how to face difficulties with good humor and calm. Most importantly, you'll learn that no success can be achieved alone, but only with the support of friends, family and kind souls who believe in you and your talents.

In these blinks, you'll also find out

  * how much young players can make in sponsorship money;

  * what kind of tennis serve can really damage your shoulder; and

  * why Wimbledon is the best of all the Grand Slams.

### 2. Maria Sharapova grew up in a loving home in Sochi, Russia, and she played a lot of tennis. 

It was after the 1986 Chernobyl nuclear disaster that two Belarusians, Yelena and Yuri, left their home and headed to Siberia. After they had settled in Nyagan, these two had a daughter, Maria Sharapova, who was born on April 19, 1987.

But Yuri Sharapov detested life in Siberia, and so he soon moved his family to a small apartment in Sochi, a Russian sea resort on the eastern shore of the Black Sea. Maria Sharapova was two years old.

A few years later, on one particularly auspicious day, Sharapova's father took her down to a tennis court in Sochi. He'd been given a racket as a joke by his brother. While her dad was practicing, the bored Sharapova snatched up an idle racket and started whacking balls herself.

It felt right — almost immediately so. Her ability to concentrate on the game was incredible. Taking notice of this innate ability, her father soon started taking the four-year-old Sharapova to lessons with a proper coach.

Sharapova's early years weren't entirely devoted to tennis, however. Her home was a lively place, often filled with friends, and family members visited regularly, too.

She read and wrote stories with her mother, who taught Sharapova the Russian alphabet and insisted she read all sorts of literature and other stories — everything from _Pippi Longstocking_ to Russian poems.

Nonetheless, tennis soon became the major preoccupation of Sharapova's childhood, and her father ferried her to practice and tournaments.

Her parents were, in many ways, very protective of her. Sharapova still remembers how she used to watch the local children playing from her apartment window. Other than to play tennis, she really wasn't allowed out much.

### 3. Sharapova’s coach was immediately convinced of her talent, and 1993 marked a turning point. 

It wasn't long before it became clear that Sharapova wasn't just a talented tennis player — she was extraordinary.

Sharapova's first real coach was Yuri Yudkin, a local legend, even if he was a bit of a boozer. Yudkin saw her potential immediately. Yes, she was talented — but more importantly than that, she could remain focused for hours at time.

She never tired of tennis. Once the racket was in her hand, she could hit balls round the clock. It made no difference whether Yudkin had her repeat the same strokes over and over again. She never got restless or bored. It was her calling, and she was dedicated enough to mold and improve her innate talent.

To Yudkin, Sharapova was the Mozart of tennis.

Then, one day in 1993, Yudkin voiced an opinion that changed Sharapova's life forever. He went up to her father and said that, if they really wanted to nurture Sharapova's talent, the family would have to relocate to somewhere where it could truly grow. Russia, still recovering from the dissolution of the Soviet Union, simply wasn't the place. America was calling.

Sharapova's father knew at once what was necessary. He decided to dedicate his life to his daughter. He quit his job, he altered his life plans. Everything was now about Sharapova and her tennis career. He began teaching himself about tennis and coached Sharapova each day. He was determined to get her to America.

In the end, the world-renowned Czech-American tennis champion Martina Navratilova gave the family the shove they needed to leave for the United States.

At a tennis event in Moscow where parents and coaches could show off their young talents, Sharapova got the chance, like all the other kids, to rally a little with Navratilova.

Navratilova was stunned. She went up to Sharapova's father and stressed the point that Yudkin had already made: Sharapova was gifted and she had to leave Russia at once.

The plan was set. But trouble lay ahead.

> _"That was my gift. Not strength or speed. Stamina. I never got bored."_

### 4. Getting to the United States was hard for Sharapova, and further tribulations awaited her there. 

Sharapova's father, Yuri, knew what had to be done. They had to make it to Florida in the United States, where tennis legends like Anna Kournikova and Serena and Venus Williams had trained.

In particular, Yuri wanted to get Sharapova onto the national junior team of the Russian Tennis Federation. At the time, the team was practicing at the Rick Macci Tennis Academy, in Boca Raton, Florida, preparing for a tour.

Yuri wrote a letter to the team's coach, though he knew his daughter probably wouldn't be accepted. Normally, the children had to be at least twelve years old. Sharapova was six.

But Yuri was effusive. He detailed Sharapova's talent, and mentioned Yudkin and Navratilova's view on the matter. And, amazingly enough, the coach invited Sharapova to fly over and practice with the team.

The next issue was the visas. In those days, it was almost impossible to travel to the United States from Russia if you weren't a government official. Nonetheless, Yuri made the trip to the American embassy in Moscow and, somehow, he managed to get two visas, each for three years. This meant, however, that Sharapova's mother would be left behind.

Later that year, Sharapova and her father landed in Miami. But when they got to Boca Raton, the coach who'd invited them wasn't there — and no one believed their story. After all, from the outside, it seemed a little bizarre: a poor Russian, with just $700 in his pocket, had traveled to America with his daughter to prove her tennis abilities.

Yuri's disappointment was palpable. Even though Sharapova's talent was acknowledged after she hit a few balls with a passing trainer, the school made a laughable offer. They suggested the pair could stay a couple of days until the owner, Rick Bacci, returned.

But Yuri had his pride. He declined, and he and Sharapova headed for Bradenton, Florida, where the world-renowned coach Nick Bollettieri ran his prestigious Bollettieri Tennis Academy _._

### 5. Life in Florida was difficult for Yuri, but Sharapova began to flourish. 

Sharapova and Yuri had more luck in Bradenton. Initially, Sharapova faced two problems. She was physically smaller than the other players and, because of this, she had to use a cut-down hand-me-down racket. But she adapted quickly, and Bollettieri soon fixed her up with funding that allowed her to practice for free and eat meals with the other players.

But life wasn't easy, especially for Yuri.

On top of mastering English, Yuri was learning everything about the world of tennis so he could ably manage Sharapova's career. At the same time, he still had to be a parent and earn money for rent and food, which he did by doing construction and yard work.

But even though he often found himself mowing someone's lawn, he kept his eyes on the goal: his daughter becoming the world's best. It was this idea that motivated him every day to get up at 5 a.m. and take Sharapova to the academy before heading to work himself.

In those first few months of training at the academy, Sharapova worked hard on developing her own tennis persona. She even refused to make friends, because she mostly saw her peers as competitors. She remains just as fierce and strong today.

Years later, a coach of hers, Thomas Högsted, would — when coaching other players — advise them not to make eye contact with Sharapova at any time during or before a match. Högsted knew that her tennis persona often intimidated other players.

At the academy, it wasn't just her skills as a player that developed. She of course got stronger and better, but what improved most was her ability to concentrate.

Fierce and focused, she maintained an attitude that would prove too much for her opponents.

### 6. Sharapova started playing with Sekou Bangoura, but it didn’t go well. 

The other children at the academy came from an utterly different background — and their rich and snobby parents couldn't stomach the idea that their offspring were being beaten by this strange little Russian girl. Consequently, Sharapova was booted out of Bollettieri's academy.

So Sharapova began playing tennis at a rival academy down the road, El Conquistador. However, problems cropped up there, too.

It was owned and run by an African-born man named Sekou Bangoura. Once a pro player, he'd worked for Nick Bollettieri for many years, after which he founded his own academy.

Unlike at the Bollettieri's academy, Sharapova wasn't funded here, and Bangoura demanded payment. Unfortunately, Yuri's landscaping jobs couldn't pay for the fancy tennis academy and so he agreed to work as a trainer there. It didn't last long. Bangoura soon fired him.

And the reason was obvious enough: Bangoura wanted control of Sharapova and he needed her father out of the picture and away from the tennis court.

Yuri was forced back into landscaping to pay the academy fees, but his back couldn't stand the toil. He even ended up in the hospital, immobilized, after throwing out his back. Weeks passed before he could return to work.

But it was too late. Yuri couldn't pay the rent because of his injury and soon the landlady kicked them out.

Bangoura also used the opportunity to turn the screw and demand payment. He knew full well that Yuri couldn't give him the money. And so Bangoura gave them an ultimatum: leave, or sign a contract that would give him control over Sharapova.

Luckily, Yuri had enough sense not to sign. But this good sense also put them out on the streets.

### 7. The Sharapovas moved in with a friend, Bob Kane, but they eventually returned to Bollettieri. 

It was a crisis. Father and daughter were homeless and didn't know where to go. Then Yuri remembered his friend Bob Kane, whose son, Steven, was training at Bangoura's academy.

Kane had a generous streak and the means to back it up. He let Yuri and Sharapova stay at his house. They ate with the family and even used their private tennis court.

Sharapova loved that time. The house was big and fancy, worlds away from their dingy old flat.

Yuri and Sharapova ended up staying with Kane's family for a year.

Sharapova was elated to be away from the controlling Bangoura. While under his instruction she'd improved noticeably, but Bangoura had also started using her to advertise his academy. Consequently, she'd had to play all week and all weekend, too. Her life had been nothing but training and tournaments.

Sharapova had never been physically imposing or particularly fast. At the age of nine, however, she started to hit the ball harder. And with her ability to concentrate undiminished, her tournament ranking soon skyrocketed. She found herself one of the top players in America in the under 12 age group.

Sharapova's time with the Kane family ended when Bollettieri asked her to return on a scholarship. Bollettieri knew what he was doing — he had watched her win many tournaments — and she was now old enough to play and live at the academy.

Needless to say, Sharapova accepted the offer and relocated to the academy. She had her place in a dorm, and her father stayed with the Kane family.

### 8. Sharapova signed with IMG and Nike, and began training with two amazing coaches. 

At the age of 11, after her early successes, Sharapova caught the eye of the famous sports agency International Management Group (IMG), and they represented her. They started paying her $100,000 a year. The idea was that they would recoup their investment and profit by taking a cut once she became a successful pro, which was an unusual deal for someone so young. Nike did the same, sponsoring her with $50,000 a year.

It was at this time that two people entered her life who would change it forever.

Max Eisenbud was a young sports agent and trainer who worked at IMG. He began coming to Bollettieri's academy to coach Sharapova, and he went on to become a true and trustworthy friend, standing by Sharapova through the good and the bad.

Robert Lansdorp became equally important. He'd coached some of the greats, including the American Tracy Austin, who'd been ranked world number one in the early 1980s.

Lansdorp lived out in LA, so Sharapova and her father flew there once a month for training. They even stayed with Lansdorp's family. One of the two children, Estelle, became a good friend as well.

Lansdorp was a tough coach. He had her hit the same strokes over and over. The shots had to be hard, without spin, and move flatly over the net. He was direct with his advice, too, at times even a bit coarse. But that was exactly what Sharapova liked about him — she knew it was his way of pushing her forward. Lansdorp coached Sharapova until she was around 15 years old and, under his instruction, she improved by leaps and bounds.

The reward for Sharapova's efforts was her mother's arrival in Florida. Finally, after nearly nine years of going through the complicated and arduous visa process, she'd been granted a visa.

### 9. Sharapova struggled at first with her new teenage body, but she learned to harness its power. 

When she hit 14, Sharapova experienced a growth spurt. Suddenly, she occupied a new body, and, at first, she didn't know what to do with it; she crashed to a few defeats because it felt so gangly. But soon she acclimatized to the situation and put this losing streak to an end.

It was a challenge, but this chain of losses forced her to learn new ways of keeping up her spirit when things became difficult. She learned that quitting gets you nowhere.

While she was still a junior in high school, she entered her first pro tournament, near Sarasota, Florida. Prize money was at stake. In the end, she lost the match in three sets. But it was a respectable performance, nonetheless. She'd hit the ball well — but she'd been a bit too slow, that's all. She was even confident enough to have a strong low-strokes game. Though it's not at all easy, she'd managed to make her balls barely skim the net.

The strength of her will was a revelation to her, and it pushed her to take control of her game even more.

Consequently, she began putting serious effort into improving her serve. Now she had a stronger and taller body, and, paired with broader shoulders, she knew that she could make use of it.

It was flexibility, particularly in her shoulders, that turned out to be the decisive factor. She could bring her arm back so far that her serve was brimming with kinetic energy by the time she struck the ball.

Her Australian coach at Bollettieri's academy, Peter McGraw, helped her hone this ferocious serve. The work paid off. Sharapova again started to make winning look easy.

Now was the perfect time to leave the junior tournaments behind her and become a proper tennis pro.

### 10. After a bumpy start, Sharapova claimed victory at Wimbledon in 2004. 

In 2003, Sharapova turned 16. Finally, she could compete with the seniors and wasn't limited to the junior circuit any longer.

That being said, her first Grand Slam tournament — the Australian Open in Melbourne — wasn't an auspicious start. She didn't even win a single match. However, Sharapova, as always, didn't let loss bring her down. She knew she had to keep fighting her way up the rankings.

So she entered many smaller tournaments, all as preparation for the Grand Slams. The more matches she won, the higher her international ranking rose. And she kept winning.

By the time Wimbledon began, in June, 2003, Sharapova's ranking stood at 47. Wimbledon went much better than the Australian Open. She made it all the way to the fourth round. This was fantastic progress and she felt that, one day, she might even win the whole tournament. What really boosted her confidence though was beating Jelena Dokic, the world number four, in the third round.

The success gave her momentum, which she used to win her first pro tournament, the Japan Open, later that year. By Christmas she was number 32 in the world.

The following year, she won her first Grand Slam. It would have been enough to have beaten the incredible Serena Williams; but the icing on the cake was that the tournament was Wimbledon. She had always loved it there. It felt grander than the other big tournaments. Maybe it was the tradition and ritual, or maybe those royal guards in their red coats. Whatever it was, Wimbledon had always seemed something special to her.

Earlier on in 2004 she'd faced Williams at the Miami Open, but she'd lost. Understandably so. Williams was years older than her as well as a perennial favorite at Grand Slams. At Wimbledon she was the reigning champion.

To be able to say you've beaten Williams in a Grand Slam final is something that few tennis pros can claim.

### 11. Sharapova rose to number one in the world ranking, but she wasn’t content to tread water. 

For Sharapova, that Wimbledon victory marked the start of some great years. In fact, by the summer of 2005, she was at number one in the world rankings.

She hadn't added some fancy new skill to her repertoire or gotten stronger or faster or better at serving — nothing like that.

On reflection, Sharapova thinks it was because she'd grown to understand her game. She knew her strengths and weaknesses. And this knowledge allowed her to rise in the rankings.

But her being the best in the world didn't mean she felt she could rest, quite the contrary.

Sharapova needed to keep working hard if she wanted to prove that her title wasn't simply a fluke. After all, she'd seen many supposed stars rise to the top and then vanish without a trace. They'd win big, and then let the winning get to their heads.

Sharapova stayed focused, always concentrating on the next match. It was tough. She had a title. She had her ranking. And now the other pros were desperately snapping at her feet, wanting to beat her and take her throne.

At the 2006 US Open, Sharapova faced Amélie Mauresmo, from France, in the semifinals and Justine Henin in the final. Mauresmo was ranked first in the world, while Henin stood at number two. Sharapova herself had dropped to third. On top of that, Henin had beaten her on each of the four previous occasions that they'd played. In short, it wasn't going to be easy.

But, after some initial difficulties, Sharapova managed to beat Mauresmo in the semis. The final itself was an intense struggle of wills. But ultimately Sharapova prevailed over Henin and won her second Grand Slam tournament.

### 12. Despite winning another Grand Slam, 2008 was a tough year. 

2008 should have been a fantastic year. After all, Sharapova began it by winning the Australian Open. But it turned out to be a year of serious change.

For starters, she switched coaches and stopped training with her old friend Robert Lansdorp. She was sad to let him go, but sometimes you have to change things up a bit to grow as a player.

Likewise, she decided she no longer wanted her father, Yuri, as a coach. She had to prove to herself that she could do it alone. It wasn't that he was a bad coach, or that they were no longer close. But, at this advanced stage in her career, she wanted to be in control. Detaching herself from her father was extremely difficult, but, after she enumerated her reasons, he understood.

As her new coach she chose Michael Joyce.

Later that year, Sharapova's shoulder started to twinge. A tendon had torn and she needed surgery.

Sharapova was scared. Going under anesthesia meant losing control, but she had no choice. After the operation in October, she needed some time in rehab in Arizona, but was back on court by Christmas.

Things weren't the same, though. She was weaker, and her famously strong serve simply wasn't possible anymore. In fact, it had been the serve — which involved pulling her shoulder all the way back and thrusting it powerfully forward — that had torn the tendon.

She'd have to reinvent her game. So she started entering a few practice tournaments. She lost a lot. Her ranking dropped down into the double digits.

At one tournament in Poland she lost to the Ukranian Alona Bondarenko, someone whom Sharapova could've easily beaten before her shoulder started acting up.

But this loss was the impetus she needed to turn things around. She had fire in her belly.

### 13. Sharapova recovered from her surgery and won the 2012 French Open. 

After her grueling year, Sharapova needed a comeback more than ever.

She didn't want to be remembered as a promising star who'd never recovered after an injury. She was determined to get another Grand Slam title. But she hadn't managed to win one since her surgery.

She was training hard and entering tournaments. Even though her game had changed after the surgery, she found she was still winning a fair amount. Her ranking rose, and before long she was in the top five again. Sharapova even came close to winning a Grand Slam: she made it to the final of Wimbledon in 2011 and also to the final of the 2012 Australian Open.

Then, she did it. The French Open was hers. It was an especially sweet victory as it was the only major Grand Slam tournament she hadn't yet won.

Winning Wimbledon, the US Open and the Australian Open prior to her shoulder injury was one thing, but to win the French Open, post-surgery — it was huge!

Winning those four Grand Slams is so difficult that there's even a name for it: a Career Grand Slam _._ A mere nine women had managed it before, among them court legends like Billie Jean King, Steffi Graf and Serena Williams.

In the final of the French Open itself Sharapova faced the Italian Sara Errani. Although Errani was good, Sharapova proved better on that day. Her shots were precise and she could see the ball perfectly.

And, what's more, she'd done it all without her father by her side. She was strong and independent, and it felt even better after her complicated shoulder injury.

But it wasn't all roses from then on. When 2015 rolled around, she would face a new, and particularly grim, challenge.

### 14. Sharapova failed a drug test, but the experience impelled her to keep on playing. 

Now in her late twenties, Sharapova's thoughts turned to retirement and a comfortable end to her career. Then, out of the blue after the 2016 Australian Open, in Melbourne, she failed a drug test.

In February, Sharapova received a letter from the ITF, the International Tennis Federation, saying they'd found a substance called _meldonium_ in her urine. At first, it didn't seem like a big thing — she'd never taken any drugs to improve her game, after all. And what on earth even was it?

The culprit turned out to be Mildronate, a pill she'd taken for years. A family doctor had initially prescribed it to her when she was a teenager, at which time she'd been getting the flu pretty regularly. She'd gotten a few electrocardiogram tests and they'd shown some abnormalities.

Meldonium had only been banned on January 1st of that same year and the ITF hadn't done a great job of informing players about the ban.

Nonetheless, the press descended on her. They called her a liar and a cheat. It was a crushing experience. She ended up having two trials — one with the ITF and another with an arbitration panel.

Needless to say, the panel stacked with ITF members weren't at all sympathetic, and banned Sharapova from professional tennis for two years. But the arbitration court ruled that she hadn't intentionally doped herself. They said she could have been more cautious, but they were also critical of how quietly the ITF had announced the ban.

All this meant she could return to playing tennis sooner than expected. The original ban was reduced to 15 months, much of which had already passed.

The result was that Sharapova's optimism came flooding back. She knew that she wanted to keep playing tennis more than she wanted anything else. Retirement could wait. She wanted to win, to show the world her talents and to remain in the upper echelons of her beloved sport.

### 15. Final summary 

The key message in this book:

**Ever since she started playing tennis at the age of four, Maria Sharapova has fought to be among the top players in the world. Although she faced many challenges throughout her career — and continues to face them — she never gave up. Utterly focused, she battled her way to the top, and, with unwavering willpower and inexhaustible patience, carved for herself one of the greatest tennis careers of all time.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Open_** **by Andre Agassi**

_Open_ (2009) is a revealing account of the turbulent life of one of America's all-time best tennis players. More than anything else, it's a story of Agassi's battle for balance and self-understanding, all while dealing with the constant stream of complications that arose from fame and public scrutiny.
---

### Maria Sharapova

Maria Sharapova, a true tennis superstar, has won five Grand Slam titles. Born in Belarus, Sharapova moved to America with the help of her father, and it's there that her career as a professional tennis player began. She's been ranked number one in the world multiple times.

