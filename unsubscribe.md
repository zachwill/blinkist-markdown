---
id: 58e20e4cff6c5c0004705f5b
slug: unsubscribe-en
published_date: 2017-04-07T00:00:00.000+00:00
author: Jocelyn K. Glei
title: Unsubscribe
subtitle: How to Kill Email Anxiety, Avoid Distractions, and Get Real Work Done
main_color: 172A71
text_color: 172A71
---

# Unsubscribe

_How to Kill Email Anxiety, Avoid Distractions, and Get Real Work Done_

**Jocelyn K. Glei**

_Unsubscribe_ (2016) puts our unhealthy relationship with email under the magnifying glass. Our lives are busy enough as it is, so it's high time to stop wasting precious hours catching up on emails and responding to unimportant messages. With the help of this practical guide, you can organize your inbox and your life.

---
### 1. What’s in it for me? Control your mailbox, rather than letting it control you. 

Imagine if you had a neighbor who, in the grips of letter-mania, went outside and checked his mailbox every ten minutes. You'd probably be very concerned about this person's mental health. But consider that, at work, most of us do the digital equivalent of this every day: we check our email inboxes again and again and again _._

These blinks reveal that email and similar services are ruining our productivity and flow. Luckily, as you'll soon learn, we can stop being slaves to our inboxes.

You'll also discover

  * why it's so easy to misread the tone of someone's email;

  * what's bad about checking your inbox first thing in the morning; and

  * how to respond to an email if you have no time to respond.

### 2. Addiction to email checking is keeping us from attending to what’s important in life. 

How many times per day do you stop to check your email? Do you try to avoid it altogether or are you one of those people who hits refresh again and again, waiting for the next message?

Even if you know you're a little obsessive, you may be more addicted than you think. Just consider this unsettling statistic: the average office worker checks their inbox 74 times a day.

This isn't because people are given a ten-minute deadline to reply; it's because the very act of checking email is addictive.

Perhaps you think that an inbox overflowing with unread emails is a daunting task waiting to be tackled. But that's not exactly the case. Each of these unread messages holds out the promise of a pleasant surprise — of some interesting or fantastic news. So even if the thought of getting back on top of your post-weekend messages gives you the jim-jams, you'll still be drawn to the task. We get addicted to the jolt of joy that some messages give us.

Our addiction comes from a primal impulse that seeks out positive rewards. It's this impulse that drives us to check our inbox over and over, even though we're unsure when the next reward will arrive. It keeps us sifting through the endless junk mail and boring work and family messages to find those rare gems that make us feel excited and alive — whether it's an email from a long-lost lover or a near-forgotten friend.

However, email affects our psychology in more ways than one. It also provokes a _progress paradox_.

When we put off doing important tasks by opening each email message at the moment it arrives, we're tricking our brain. On the one hand, we feel productive because we're reducing the number messages in our inbox, but, on the other hand, we're accomplishing next to nothing.

Nonetheless, getting that number of unread emails to zero is an almost irresistible reward since it triggers the same response we get from accomplishing an important task. But any email that can be swiftly discarded is generally insignificant and has no impact on our long-term goals.

So don't let this feeling of false progress fool you. There are more important things you could be doing with your time.

### 3. Emails lack the nuance of real conversations, and our inability to respond to them all can lead to negative emotions. 

Here's an all-too-common scenario: You send off an email that was written with the best intentions, only to receive a reply that seems flat or perhaps even angry.

The written word is, obviously, very different from verbal communication. Facial cues, tone of voice, body language — an email message can't contain these things. And yet we often write messages in the same language we'd use in a face-to-face conversation.

According to psychologist Daniel Goleman, this leads to a _negativity bias_ : the reader of an email will always assume that the content is more negative than was intended. So if the sender felt good about the content, the reader will feel neutral. And if the sender felt neutral, the receiver will take it as being negative, and so on.

Another problem with our reliance on email is the _rule of reciprocity_, which states that people feel obliged to respond to a positive act with a similarly positive act. But given the enormous amount of email messages people receive every day, this rule has become impossible to follow.

It was a different story in the 1970s, when sociologist Phillip Kunz sent out 600 lovingly made Christmas cards to complete strangers. And sure enough, he received a great many responses, including handwritten letters that were several pages long. Many of these strangers continued to send him similar holiday greetings for the next 15 years.

Kunz's experiment highlights our desire to reciprocate, even when we didn't ask for the original gesture.

But nowadays, no one needs postage or even legible handwriting to contact you, not to mention the 50 other people to whom they can send blind carbon copies at the same time. This makes it impossible to keep up with the rule of reciprocity, a fact that can leave people feeling frustrated, guilty and ashamed.

> _"Like it or not, email breeds a curiously strong sense of obligation."_

### 4. Be the boss of your inbox by identifying what’s truly meaningful to you. 

At this point you're probably saying, "I know the problems, but what are the solutions?!" Well, you'll be glad to know that there are a number of ways to regain control of your email.

One of the good things about email is that you're in charge of everything: how it looks, what it says and when it gets sent. But you also have a say in how many emails you receive.

Remember, no one is pointing a gun at you demanding that you immediately respond to an email or that you respond in a certain way. It's good to keep in mind that the more emails you send, the more emails you are likely to receive.

This brings us to our first exercise, which is to take a moment to think about how you determine what work is important to you.

Meaningful work should be seen as contributing to your life and legacy. So ask yourself, "Am I working on something that is going to make me better at what I do? Is it improving my career, or the lives of others?"

Important work might be writing a book, improving your public-speaking skills or teaching creative writing to others. Whatever it is, you need to identify it, because until you do, you won't be able to use email in a meaningful way.

To stop wasting your time, you need to have something meaningful toward which to direct your attention. Only then will you be able to stop yourself from checking your inbox a hundred times a day and wasting your time writing meaningless responses.

It also helps to write down the different jobs and tasks that contribute to the meaningful work. This will help you prioritize and make sure that whenever you're using email, it will be in relation to something that's important.

### 5. Set strict times for dealing with email, and devote your morning to meaningful work. 

Chances are, it would be unreasonable to remove email from your life altogether, so the question becomes: How can you figure out the optimal amount of time to spend on email?

For this, you should create a daily routine that suits your needs.

No matter who you are or what you do, it is probably best to avoid starting your day by checking your email messages.

If you start going through your inbox before you've even brewed a cup of coffee or had a bite to eat, you're essentially letting other people set your priorities. Before you know it, you'll be wasting time and energy by responding to meaningless messages.

Instead, devote the first 60 to 90 minutes of your day to meaningful work.

Sure, your meaningful work might include writing an important email — but sticking to this rule of thumb will keep you from scrolling through all your unread messages and wasting your day's first precious hour. The morning hours are a valuable time. It's a time when your mind is fresh and sharp. It would be a shame to waste that clarity on random emails.

But mornings aren't the only time it's bad to waste your energy on emails. It's never a good time to _waste_ your energy. The best way to avoid this is to be a "batcher" and only check your inbox two or three times a day.

Batchers have an effective and less stressful approach to email since they tackle their messages in batches, rather than monitoring their inbox 24/7.

This strategy leads to higher productivity. Since you're not being constantly distracted, you'll be able to focus and power through your emails at the appointed time, rather than aimlessly scrolling and replying.

It's also a healthier approach. A 2015 study showed that people who are constantly monitoring their email are more stressed than those who set specific times during the day to deal with their messages.

> _50 percent of people check their email before eating breakfast._

### 6. Organize your emails into prioritized folders and use quick replies that set expectations. 

If you've ever looked at your hundreds of unread email messages and thought, "There has to be a better way," well, you were on to something. There are some easy tips and tricks to help you make the most of your daily batches of time.

The first is to customize your inbox and create useful folders. With the right settings, your incoming emails will even organize themselves.

To start with, you can set up different folders with varying degrees of prioritization. This way, emails from your boss and other important coworkers will be directed to a devoted folder marked "urgent"; those from other contacts will go into other folders that remind you it's okay not to deal with them immediately.

Separating important messages from less important messages will make it that much easier to maintain your productivity.

You can also make a new habit of using quick replies that set clear expectations.

It's only a matter of time before you receive an important message that requires a lengthier and more thoughtful response than you have time for. So what's the best thing to do? Push aside the more urgent and meaningful work? Type up a rushed response that you'll regret the moment you send it? Neither of these solutions is ideal.

In such a situation, the smart response _is_ to send a quick response — one that lets the person know that you've received their email and that a full response will be given as soon as possible.

It might go along these lines: "Dear Karen, thank you for your email. I agree, we need to discuss these new developments. Unfortunately, I am currently facing a deadline on an important project. I will send a more detailed message by the end of next week, when my schedule clears up."

Sending a considerate email like this takes just a minute or two, and it not only allows the recipient to set their expectations accordingly. It also lets them know that you're thinking about the issue and not ignoring them.

In our final blink, we'll give you a few pointers on how to write a scintillating email message.

> _"The easiest way to fail at email is to accept your inbox 'as is.'"_

### 7. Effective emails are concise, to the point and considerate. 

These days, everyone seems to be extremely busy. This, in large part, is due to the many different distractions that we all have to deal with — the endless stream of incoming email and text messages, phone calls, not to mention any number of social-media and news updates.

So how can you get the attention of customers and coworkers, family and friends?

You have to write effectively, and the best way to do this is to be concise. Otherwise, you run the risk of losing the receiver's attention before they've really started reading.

A great way to prevent this from happening is to make a clear point right at the start.

Say you want to invite a professor to speak at a conference you're putting together. Don't spend the first few paragraphs gushing about how much you admire her work and explaining what the entire conference is about. Provide a quick introduction and get right to the point with your proposal.

Also consider the expectations of your recipient and put yourself in their shoes.

Ask yourself two questions: "Are they angry, sad, happy, excited, overworked...?" And then, "What are they expecting from me?" With this in mind, you should have a good foundation for how to compose your message and decrease the risk of misunderstandings. This will ultimately make your efforts more productive since you won't need to waste time clarifying or explaining yourself later on.

Let's say you've been tasked with making workplace improvements and are prepared to tell your boss about your new ideas. If you forget to put yourself in your boss's shoes, you might forget that she's busy this week and spend half a day writing a three-page email that she won't have time to read, when it would have been better to send a brief and efficient message that lets her know that you're prepared and excited to present your ideas whenever time permits.

Whomever you're writing to, be sure to consider how your message will most likely be read.

### 8. Final summary 

The key message in this book:

**The days of wasting time on meaningless email messages are over. It's time to reclaim your inbox and take control of your daily work life. With a few simple changes and some new habits, you can prioritize your life so that you're spending your time on what matters most.**

Actionable advice:

**Use VIP notifications**

Generally, you're better off if you don't check your mailbox more than twice a day. But there are always some people you don't want to make wait — your boss, for instance, or your most important client. In such cases, you can compromise and set up your smartphone to notify you whenever a message from one of these VIPs comes in. This way, you can still refrain, by and large, from checking your inbox except at the designated times.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Tyranny of Email_** **by John Freeman**

_The Tyranny of Email_ (2011) is about the rise and unprecedented power of email as a form of communication, and the profound effect it has on our lives. We have more access to communication now than at any other time in history; but not everything about email is positive.
---

### Jocelyn K. Glei

Jocelyn K. Glei, an American writer, specializes in helping others make the most out of their workday. Her other best-selling books include _Manage Your Day-to-Day_ and _Maximize Your Potential_.

