---
id: 5947d660b238e10005eecf01
slug: untangled-en
published_date: 2017-06-22T00:00:00.000+00:00
author: Lisa Damour
title: Untangled
subtitle: Guiding Teenage Girls Through the Seven Transitions into Adulthood
main_color: BD9C8C
text_color: A36648
---

# Untangled

_Guiding Teenage Girls Through the Seven Transitions into Adulthood_

**Lisa Damour**

_Untangled_ (2016) is a guide for mothers and fathers, for teachers and mentors — anyone who might be trying to better understand the life and struggles of teenage girls. It offers invaluable advice on how to recognize what they're going through so you can avoid some common pitfalls and not make the situation worse.

---
### 1. What’s in it for me? Ease your teenage daughter’s transition into adulthood. 

Every woman was once a teenage girl and every young girl will, at some point in the future, enter her teens. If you remember your teenage years or currently have a teenage daughter, you'll know about the mood swings, the attitude and the inexplicable sadness that accompany this period.

But don't worry — it's all a natural process. The teenage years are the crucible in which girls become women and, though usually intense, this process doesn't have to be unpleasant.

These blinks will make you recognize the patterns of teen behavior and transitions, allowing you to navigate both what they want and what's best for them.

In these blinks, you'll learn

  * how confronting authority can be good for teenage girls;

  * why some teenage girls want to join tribes; and

  * how to navigate the tricky world of teen romance.

### 2. It’s natural for teens to become more private and temperamental. 

It happens to the parents of all daughters. After years of happily spending time with them, their daughter suddenly seems to disappear into herself and her room, avoiding conversation and contact.

This change in behavior can set off parental alarm bells, but there's really no need to panic; it's merely the first of the seven natural stages that every girl goes through when transitioning from childhood to womanhood.

This first stage usually occurs around age eleven, and it's characterized by the desire for privacy and isolation.

But keep in mind that this isolation is both a conscious and unconscious act. While your teen is consciously seeking time alone, she's also unconsciously preparing herself for the independence of adulthood.

Transitioning into adulthood is sort of like learning to ride a bike: isolation at home is the training wheels, the learning period that precedes adult independence. So, rather than worrying, be glad that she is going through all of this in a safe environment.

Now, it's not uncommon for this behavior to be accompanied by temperamental outbursts. Daughters often say mean or cruel things to their parents — especially their mother. And these remarks will often be aimed at vulnerable areas, such as physical appearance or sartorial shortcomings.

The best way to deal with this is to tell your teen that her comments hurt your feelings, and to let her know that she should be more mindful about what she says.

But your daughter's increased standoffishness doesn't mean you can't plan ways to stay in touch.

If you're worried about the weakening of the family bond, try establishing regular family times, whether it's eating dinner at the table or even just sitting together to watch a movie.

Studies show that regular family meals improve not only the psychological health of teenagers but also their grades at school. This is also true when only one parent is present — and even in situations where the teen expresses an overall dislike for her parents.

### 3. Teens value being part of a tribe, and you need to help them avoid the pitfalls of popularity. 

The second stage in the transition to adulthood is when a girl starts spending less time with her parents and more time with her other friends — her "tribe."

Again, the desire to be part of a teenage tribe is a very natural part of the maturing process, but it still comes with certain drawbacks.

On the plus side, being part of a social circle is a great way for maturing girls to gain independence from their families, learn important social skills and understand how to deal with conflict.

More than adults, teens generally regard being part of a social circle as a highly rewarding experience. During puberty, the emotional and social parts of our brain are far more active than the cognitive parts.

This often leads to teenage girls placing too much importance on achieving popularity within their tribe, which, in turn, can create anxieties and fears of becoming tribeless.

As a parent or teacher, you can help a teen through this stage by guiding her to be both friendly _and_ strong.

Research reveals there are three categories of teenage girls:

The first kind are friendly and well-liked, but not very popular.

The second kind are the popular ones. They have social power, yet they're generally disliked because they're mean, snobbish, domineering and often resort to cruelty.

Then there's the elusive third kind: the teens who are both popular _and_ friendly, while at the same time being assertive and willing to stand their ground. Even adults can have a hard time finding this harmonious balance, so it helps to discuss the concept of popularity early on and to encourage teens to admire those who do strike this balance.

This way, they'll see how often popularity goes hand in hand with being disliked, and, hopefully, they'll never try to bully their way to power.

### 4. Joining a new tribe can come with social conflicts and bad decisions. Be a strong parent. 

As teens seek out a tribe on their quest for acceptance and social status, there are some things parents and teachers should keep an eye out for, because girls can gain a "frenemy" just as easily as a BFF.

It's not uncommon for members of a tribe to have personality clashes and instances of peer pressure that can lead to teens developing a frenemy relationship — one that blurs the line between friend and enemy.

Parents can pick up on this kind of relationship when their daughter starts complaining a lot about someone who's supposed to be a friend. Initially, there might be a few normal arguments, but things can easily escalate into emotionally charged and questionable situations.

If such conflicts arise, you have to be a solid, strong parent. Teenage girls often have a hard time safely working through conflict because, in general, teens don't deal with anger very well. They'll often pick one of two extremes: being a pushover or acting out in inappropriate ways.

The first thing a parent or teacher can do is to acknowledge and validate any anger by saying something like, "I understand why you're upset with this friend of yours. But responding to them in a hurtful way is not the right answer. Your feelings should _inform_ the way you react, not dictate it."

This will help the teen develop her mature and rational decision-making skills while at the same time supporting her assumption that friends should be kind to one another.

However, it's also important for parents to dare be the "bad guy" when needed. Resist the temptation to always be the "cool" parent who acts like a friend of the teen.

Cool parents not only lose the respect of their daughter; they also take away her chance to use the threat of punishment as a way to get out of a dangerous situation.

For example, let's say your daughter doesn't feel safe going to a certain party. If her friends know you let her get away with occasionally drinking or smoking pot, she won't be able to use your possible punishment as a valid excuse to avoid something she doesn't feel comfortable with.

### 5. You can also help teens use their strong emotions to make more informed decisions. 

If you've ever had a peaceful breakfast with your daughter only to have her screaming at you 15 minutes later because her favorite jeans didn't get washed, then you've experienced a typical stage-three scenario. This is when girls begin to go through dramatic emotional highs and lows.

These mood swings are due to a very natural restructuring that happens in a teenage brain.

It starts with the changes in the lower part of the _limbic system_, which deals with our primal and more emotional behavior.

Later on, changes will continue in the upper and outer layer, known as the _frontal cortex_, which helps us reflect and calm down. But this second change is only complete when we're adults, so, until then, we're prone to emotional outbursts — especially in our teenage years.

Studies also show that teens are more highly attuned and responsive to the emotions of others.

Researchers monitored the brain activity of kids, teenagers and adults who were shown a series of different faces that expressed happiness, sadness or tranquility. The adults and kids experienced a slight discomfort when they were exposed to sad faces; teenagers, on the other hand, were especially distressed by the sad ones and far more ecstatic when shown the happy faces.

So to help prevent teens from making extremely reactive decisions, it's important to explain how emotions can be a guiding force for making good choices.

First of all, let your teen know that emotions such as sadness, irritation and anger are all normal, valid and useful; they tell us what's happening and how to respond.

If she's feeling disappointment or anger, ask her to take a closer look at these emotions by saying, "I understand you're angry, but what do these emotions really tell you?"

This way, she'll learn to pause before reacting, which will attune her to her emotions and help her make more informed decisions.

By helping her learn how to positively use her emotions, you'll be easing her transition into mature young womanhood.

### 6. Testing authority demonstrates a developing sense of abstract thinking and independence. 

If you've ever been in a classroom with a teenage girl who's sporting a mohawk or sat at a dinner table with your daughter wearing black lipstick, then you're familiar with the fourth stage of adolescence: testing authority.

If you're the proud parent of an authority-testing daughter, don't worry; it's another natural development that, in this case, is associated with her growing capacity for abstract thought.

According to Swiss psychologist Jean Piaget, the development of abstract thinking marks the end of childhood.

Before this, children can only reflect on direct experiences. So if you were to ask a nine-year-old girl why someone would throw a backpack from a moving train, you'd most likely get a blank stare; it's difficult for children to explain something they've never done.

A thirteen-year-old girl, on the other hand, would be able to think about it abstractly and could probably formulate a few plausible answers.

Along with abstract thought also comes the discovery that some grown-up rules are questionable, contradictory or even hypocritical. Parents might find their own daughter asking difficult questions, such as, "You say smoking is bad, so why do _you_ smoke?"

When teenagers begin to notice these contradictions in society, they'll start to rebel, and this is also part of their natural, growing independence.

Often this behavior doesn't actually involve any disobedience. They'll roll their eyes, use sarcastic language and dress rebelliously — but, usually, they'll follow the rules.

However, if your daughter behaves rudely, don't hesitate to question her behavior and then ask her to explain herself. You might tell her, "I don't like your tone, but I am willing to discuss what you are unhappy with. So please, tell me precisely what is going on."

It is every girl's right to disagree. But you can help her think independently _and_ communicate in a clear, civil manner by posing questions that make her think about her reactions.

The wrong way to handle this situation would be to shame your daughter for expressing her independence. Instead, stay calm and apply _the three Fs_ : be fair, firm and friendly.

> "_There are few situations in life which are more difficult to cope with than an adolescent son or daughter during the attempt to liberate themselves."_ — Anna Freud, 1958

### 7. Remind teens that their actions today will affect their future lives as adults. 

As a teenager, you probably did something you knew was "wrong." Perhaps you were pressured into it or maybe it merely gave you a thrill. This leads us to the fifth transition: acting out and learning about consequences.

Remember, acting out is one way that teenagers establish their independence. That said, parents are responsible for preventing their teen from acting in ways that could harm her future.

Nowadays, this means teaching her how the internet and social media can easily lead to harmful over-sharing. Drunken photos and nude selfies are just some of the things parents need to worry about.

It will be natural for a teenage girl to want to use the internet as another way to act out, and it's unfortunately all too easy to do this in a matter of seconds.

To steer them in the right direction, don't try to ban online activity altogether. Instead, remind her that some things are impossible to permanently erase and that college applications and future jobs can be hurt by a single impulsive mistake.

If your daughter challenges your expertise on modern technology, try to enlist a cousin or someone closer to her age whose warnings might carry more weight.

Another area for potential parental conflict is school.

All parents want their children to get good grades. But pushing too hard can backfire, as even the smartest teens can rebel. Don't let them fall into the trap of self-sabotage.

But don't be too easygoing, either.

If bad grades persist, it's likely due to your teen placing more importance on her social life and independence than her schoolwork.

In such cases, rules can help. Few teens would endanger their social life, so tell her that she'll have to cut back on hanging out with friends until she gets her grades back up.

It also helps to focus her attention on _why_ grades are important and how they can impact her college plans and her well-being as an adult.

But whatever you do, don't do her homework for her! Not only does this prevent her learning; it can also lead to her being less independent as an adult.

### 8. Talk to your daughter about relationship expectations before it’s too late. 

For years, girls will often think of boys as those gross creatures they put up with at school. And then one day, they're suddenly talking about going on a date.

Romance is the sixth stage on the path to adulthood, yet getting romantic ideas about boys can actually happen before a girl turns ten.

This is always an anxious time for parents, but it is important for them to help their daughter figure out what she does and doesn't want out of a relationship.

Around the age of three, a girl will usually start to understand that her parents have a special relationship, so by the time she's ten, she may feel as though she's been longing for a romantic relationship for years.

The best way to help her find a healthy one is to talk to her about it.

Try to get a sense of what her concept of a romantic relationship is by asking questions like, "How does a girl let a boy know that she'd like to go out with him?" Or, "What would you do if someone you didn't like asked you out on a date?"

If you don't have these conversations, girls can lose sight of their own desires and instead end up seeking what society, as portrayed by the media, expects them to seek.

Sadly, magazines, television and online sources feature a lot of content that objectifies women and makes them think they should all look and behave in unnatural ways. This extends to how their relationships should function as well.

But if you talk to your daughter, you can help her understand where these images come from and that they're not always an accurate reflection of reality.

This is also a good time to tell her that it's okay to tell a boy no when he makes an inappropriate request — such as asking for a naked selfie.

> _"Girls who accept the media's sexist messages are … more likely to suffer from eating disorders, low self-esteem, and depression."_

### 9. Taking responsibility in caring for themselves is the last step toward adulthood. 

The seventh and final part of a girl's transition into womanhood is all about her ability to take care of herself. This means having a basic understanding of how to stay healthy and safe — and the skills to do so.

It starts with two fundamentals that often go undiscussed: a healthy diet and sleep.

A good parent doesn't want an obese daughter or one that suffers from malnutrition, and since food is generally a sensitive subject for teenage girls, it often goes unmentioned.

But teens are at a high risk of eating disorders, which have the highest fatality rate of all mental disorders.

So make sure she understands the importance of both physical activity and eating healthy foods. Don't draw strict lines about forbidden foods, tell her to listen to her body and act accordingly when it tells her that it's hungry or full.

Another crucial aspect of maintaining a healthy body is the human sleep cycle, and teenagers need about nine hours of sleep every night.

There are two great tips that will help her get a good night's sleep. The first is to avoid using the bed as a couch or desk, only for sleeping. The second is to stay away from phones and computers for at least 30 minutes before going to bed since the light emitted by screens can ruin a sleep cycle.

And even if she doesn't want to hear it, a discussion about the dangers of drugs and alcohol is important to have before your daughter leaves the house.

Sure, most teens are going to drink, and while abusing alcohol and drugs is a bad idea at any age, it can really harm the developing brain of a teenager. Substance abuse at such a young age can lead to lifelong addiction as well as impair learning and memory skills.

Last but not least, as a parent you should let your daughter know that everyone makes bad choices once in a while and you'll always be there for her in times of need. This way, she won't hesitate to call for help, even when she's made a mistake.

### 10. Final summary 

The key message in this book:

**Even though each teen grows at their own pace, there are seven main transitions that every girl needs to go through when leaving childhood behind and entering womanhood. Going through these transitions is always a trying time. It's your job as an adult to guide your teen through this journey by understanding what she's going through and teaching her how to make better decisions.**

Actionable advice:

**Ask specific questions and show that you're genuinely interested.**

Teenage girls often hate prying, indirect questions. So be honest and ask direct, specific questions that show genuine interest. Instead of asking a generic question like, "How was your day?" show a real interest in her life by asking, for example, "How's that art project coming along?"

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Girls & Sex _****by Peggy Orenstein**

_Girls & Sex _(2016) illuminates the shadowy landscape of sex and sexuality that girls and young women struggle to navigate today. Many developing women are trying to find the right balance between who society wants them to be and who they want to be themselves. This is no easy task, but we can make it easier by changing the way we talk about sex and by empowering girls to shape their own future.
---

### Lisa Damour

Lisa Damour is a clinical psychologist and director of the Laurel School's Center for Research on Girls, as well as an associate at the Schubert Center for Child Studies. Her work focuses on helping both teenagers and their parents. She is the proud mother of two daughters.

