---
id: 56a6a905fb7e870007000008
slug: use-your-memory-en
published_date: 2016-01-26T00:00:00.000+00:00
author: Tony Buzan
title: Use Your Memory
subtitle: None
main_color: E64274
text_color: CC2759
---

# Use Your Memory

_None_

**Tony Buzan**

_Use Your Memory_ (1986) reveals how to develop, train and employ a stellar memory. In a series of guided step-by-step mental exercises, this book will give you all the tools you need to memorize everything from short grocery lists to complex subject matter.

---
### 1. What’s in it for me? Become a master of memory. 

From serious infractions, like a missed tax declaration, to the just plain annoying, like the forgotten tin of tomatoes from your trip to the grocery store, how many times has your day been ruined because you forgot to do something important? 

Well, with some useful tricks and strategies, this never needs to happen again.

These blinks show you a few simple techniques that can revolutionize your memory capacity, helping you retain much more information for much longer. 

In these blinks, you'll discover

  * just how many memories you make every day;

  * how everyone can build their own special memory room; and

  * how to create pegs to hang your memories on.

### 2. Your memory is inherently powerful. 

If you had to guess, how many memories do you think you recall each day? Most people answer somewhere between 100 and 10,000. But in reality, we access several billion memories every single day.

Our brains hum along recalling memories all the time, and they operate so efficiently that we don't even take notice. Even something as simple as a conversation activates a whole range of memories, and places them in the appropriate context. 

For example, when Grandma asks little Judy "How's Jake?", Judy's brain has to do a lot more than just interpret Grandma's language; it also has to run through all the "Jake" memories in her brain. Judy has both a dog and a toy named "Jake," but her memory recalls that grandma always asks about the dog. Judy answers "Slobbery, but good." 

Astoundingly, research suggests that our memory may be close to perfect.

Consider, for example, that many people have vivid dreams of friends and lovers they haven't seen for decades. Somewhere in their mind, the brain has stored a perfect, unchanging image of this person, which can be recalled with the right trigger.

We've all had the experience of suddenly recalling people or events from the past. A single smell, sight or sound can bring about memories we thought we had forgotten. For example, the smell of freshly baked cookies might instantly conjure up memories of grandma's house from when you were a kid.

In fact, the famed psychologist and neurophysiologist Professor Mark Rosenzweig has stated that even if we exposed a human brain to ten new pieces of information every second of our lifetime, it still wouldn't even be half full.

If there is enough "space" in our brain to store all the memories we collect through the years, then the key to perfect memory is the self-management of this remarkable storage. The following blinks will examine memory techniques, called mnemonics, which can help you do this.

### 3. Mnemonics are simple methods that ensure excellent memory. 

One way you can significantly improve your memory is by using memory techniques often referred to as _mnemonics_ (the first _m_ is silent). So what's the idea behind this weird word?

Mnemonics refer to any technique designed to help you remember things. Mnemonics can be very useful and effective tools to recall all kinds of information, including lists and sequences of events.

When the average person takes a memory test, they usually score somewhere between 20 and 60 percent. Trained memorizers, in contrast, achieve between 95 to 100 percent accuracy. Impressive!

These results scale as well. In fact, experiments have shown that once you learn mnemonics and are able to remember something, say, nine out of ten items, there is a high likelihood that you'll also be able to remember 90 out of 100, 900 out of 1000, and so on.

So what makes mnemonics so powerful? The secret lies in the fact that this technique activates both the left and the right cortex of the brain, thus unlocking your memory's full potential.

Whereas your left brain helps you remember things through logic, words, sequencing, numbering and lists, your right brain operates through creative associations. These associations can be created using rhythm, color, dimensions, daydreaming, spatial awareness and _Gestalt_ (the whole picture).

By stimulating both sides of the brain when trying to store information in your memory banks, you'll not only improve your ability to recall information, you'll boost your creative potential as well.

For example, if you learn how to play an instrument while also spending lots of time studying math, both your musical and math skills will improve more than if you were only learning one thing at a time. Similarly, you can become a better dancer if you learn a foreign language at the same time. 

By this point, you're probably ready to start your mnemonics training to improve your memory. The remaining blinks will take a closer look at individual mnemonic techniques.

> The word mnemonics is derived from the name of Greek memory goddess Mnemosyne.

### 4. The most important mnemonics use all your senses and emotions to recall information. 

Learning mnemonics is really easy. In fact, you're probably familiar with some of these techniques already! 

Mnemonics comprise a wide range of exercises, but we're only going to focus on the four essential attributes for any mnemonic exercise. These attributes are key, as they engage both our literal (left) brain and visual (right) brain.

The first is _synaesthesia_, or the blending of the senses. In developing better memory, it's important to sensitize and regularly train all your senses, including vision, hearing, smell, taste, touch and kinaesthesia (your awareness of position and movement in space).

So, for example, if you want to be sure you pick up some toothpaste after work, you could engage a variety of senses by vividly imagining yourself squeezing the toothpaste onto the brush, feeling your fingers touch the brush and imagining the tastes, smells and sounds of brushing your teeth.

_Movement_ adds another means for your brain to "link" information. Staying with our toothpaste example, when you imagine the toothpaste, make the images three dimensional. Really try to imagine the rhythmic sensation of the toothbrush against your gums.

Next up are _positive images_. The brain naturally wants to recall pleasant memories, so do your best to attach positive or inspiring associations to the things you want to remember. In the toothpaste example, you might vividly imagine yourself in front of a mirror, admiring your beautiful, pearly white teeth.

The final attribute is _exaggeration_. Interestingly, our brains store exaggerated stories, images and so on with a high success rate. If you imagine that your train home is a massive tube of toothpaste, then you won't need to work hard to remember!

If you use these mnemonics, you can remember anything you'll ever need to — and you won't need any more reminders from your dentist, either.

### 5. The link system will help you remember short lists of items. 

Imagine how nice it would be to never worry about forgetting your shopping list again. With the _link system_, this could become your reality.

The link system creates associations in the mind that link individual items in a list in such a way that makes them easier to recall. 

As you learned in the previous blink, the best way to remember things is to fill your memory with vivid details. Thus, it's important to make these links as exciting as possible. The transitions between the items on the list should also activate all your senses, and the more unexpected and abstract the links, the better you'll remember them! 

As an example, say you need to remember to get scissors, roses, washing powder, apples, olive shampoo, bread, a coffee maker, eggs, napkins and dental floss from the store.

Imagine it's a cold day — so cold that the steel scissors in your hand have frozen stuck against your skin. When you rip them away, your palm is a vibrant red — the color of the roses you want to buy from Mrs. Tulip.

When you enter her flower shop, the fresh smell reminds you of getting the washing powder (imagine the vivid contrast between red and white of the last two items).

So you head to the pharmacy, and on your way you see an organic shop with a beautiful display of apples. You pop in and grab two kilos. At the store, they also have very ripe kalamata olives (imagine them as clips in your hair, completing the association with shampoo).

On your way home, you pass a coffee shop that smells of freshly roasted beans, so you stop in to get a new coffee maker since your old one is broken. You think of how wonderful it would be to eat freshly baked bread with poached eggs tomorrow morning, along with a freshly brewed cup of joe! You can see your breakfast served on a black plate with a bright red napkin next to it. It was delicious. Now it's time to floss your teeth.

### 6. Peg memory systems are useful for short sequences. 

Remembering items on a list is useful, but what do you do if you have to remember something in a particular sequence? In this case, _peg memory systems_ are the key.

Peg memory systems use _key memory words_ as a basis for remembering. A peg is a fixed key memory word, which is used to link items.

Imagine the peg system as a wardrobe with a fixed number of hangers; clothes come in and out, but the hangers stay the same.

Two good examples of peg memory systems are the _number-shape system_ and the _number-rhyme system_.

In the number-shape system, you use the shapes of numbers as pegs. Think of the numbers one through ten — what does each number look like to you? For example, number one might be a pencil. Two kind of looks like a swan, and three might resemble a heart.

Be playful and authentic in developing your own set of number shapes.

Once you have ten shapes, you can use them as references for a sequence. For example, imagine that you need to remember a recipe sequence, the first three steps of which are "mix a spoonful of milk with four eggs, and add a cup of vanilla." 

Think about the images you invented for numbers one through three, and then link them with these items. For example, you might imagine the milk pouring down the handle of a spoon, shaped like a pencil (number one). Then, recall a swan (number two) sitting on four very yellow eggs. The swan's wing boasts a tattoo of a red heart (number three) sprinkled with vanilla.

The number-rhyme system is similar to the number-shape system, except you develop your own rhymes based on the numbers to act as pegs. One, for example, could be "sun." Two sounds like "loo," and so on.

By combining both systems, you can store lists of 20 items in your short-term memory in any order. But what if you want to remember an indefinite number of objects?

### 7. The Roman room system allows you to remember an indefinite number of items. 

Imagine creating a room where you could store countless memories. Well, that's exactly what the _Roman room system_ allows you to do.

By filling your imaginary room with objects, you create pegs to which you can attach new information. The number of fixed objects you create is up to you. Just remember that the more objects you have, the longer the sequences you'll be able to remember.

And while the sequence of objects doesn't matter, it needs to be deliberate and precise. It is this precision that allows new objects to be linked to fixed ones. 

To practice, let's sketch a few items in your imaginary room; these will be your fixed items. 

There is a blue doormat in front of the door with a drawing of a white cat on it. In the middle of the door there is a red doorknob shaped like a dragon. Once you enter the room, you find an oak coat hanger with eight pegs on your right-hand side.

So, if you need to remember to get a vaccine from the pharmacy, pick up your sister from the airport and get your shirts from the cleaners you could imagine something like this: 

The white cat on the doormat is being injected with the vaccine. You see the red dragon spread its wings and welcome your sister into the house. Inside the house, you see your freshly-pressed shirts hanging from the coat hanger by the door.

It's easy, right?

Once you've furnished your room, take multiple "mind walks" in order to cement the pegs in your memory. Activate all your senses during these walks. Be mindful of the colors, smells and noises you hear. Listen for subtleties, like a ticking clock, and really feel the sensations of the furniture.

If you want to make sure you remember the pegs in your room, make a drawing of it. Some people enjoy creating these rooms so much that they've made drawings with hundreds of items!

### 8. Mind maps are a useful device for not forgetting your notes. 

Everyone has had the experience of scrambling to go over their notes before a big meeting or presentation. Long lists of facts can seem overwhelming, even if we were the ones who produced them!

Traditional note-taking activates only employ the left hemisphere of the brain, and that's no good for memorizing.

Think about your own notes. What do they look like? They are probably based in language, full of sentences and phrases, likely organized in the form of lists with the help of bullet points or numbers. 

This is all very important, but language and sequencing only engage your left brain. You've completely left out imagination and association, the domains of the right cortex. 

_Mind maps_ increase your memory by simultaneously engaging both brain hemispheres.

To make a mind map, pull out a blank sheet of paper and draw a key memory image in the middle; this will function as your central theme (right cortex activation). From this center point, you can spread out to subthemes using connecting lines (left brain).

You can either write out the subthemes (left brain) above the lines, or you can draw them (right brain). It's a good idea to alternate so you activate both brain hemispheres. 

For example, if you're studying the history of memory, your central image could be an elephant, as they are famous for their supposedly stellar memory.

You have six subthemes: "The Greeks" are represented with an olive, and Roman sandals represent "The Romans." Then, write out "The Influence of the Christian Church," "Eighteenth Century," "Nineteenth Century" and "Modern Times."

Keep applying a mix of writing and drawing, all while taking notes of details within each subtheme. The process of creating the mind map will force you to find the novel connections between the things you are trying to remember. This alone is enough to increase your memory recall.

### 9. Final summary 

The key message in this book:

**Though it may not feel like it at times, your memory is actually extremely powerful. Learning a few simple principles and techniques is all it takes to turn the frustrations and stress of forgetfulness around, and harness the full potential of your memory.**

Actionable advice:

**Remember SMASHIN' SCOPE.**

Every letter in SMASHIN' SCOPE refers to the first letter of the word describing one of the mnemonics. SMASHIN' stands for Synaesthesia, Movement, Association, Sexuality, Humor, Imagination and Number. SCOPE covers Symbolism, Color, Order, Positive Images and Exaggeration.

**Suggested** **further** **reading:** ** _The Memory Palace_** **by Lewis Smile**

_The Memory Palace_ (2012) is a step-by-step guide to using your spatial memory to help you remember absolutely anything. It teaches you how to build a palace of memories that will give you the power to recall everything you read, and even to memorize the names of every Shakespeare play in just 15 minutes.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Tony Buzan

Tony Buzan is an author whose works focus on memory, creativity and speed reading. In addition, Buzan is the president of The Brain Foundation, the founder of The Brain Trust and the Use Your Head Clubs and the creator of the concept of mental literacy.

