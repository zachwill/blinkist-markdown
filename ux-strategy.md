---
id: 59db809fb238e1000613aede
slug: ux-strategy-en
published_date: 2017-10-13T00:00:00.000+00:00
author: Jaime Levy
title: UX Strategy
subtitle: How to Devise Innovative Digital Products That People Want
main_color: 7676AC
text_color: 393991
---

# UX Strategy

_How to Devise Innovative Digital Products That People Want_

**Jaime Levy**

_UX Strategy_ (2015) is your guide to integrating business strategy and user experience design. These blinks lay out a concrete path to develop a product and business strategy, as well as product experience that will captivate users, capture market share and catapult your start-up to success.

---
### 1. What’s in it for me? Learn how to make user experience a key part of your business strategy. 

If you run a business, one thing is certain: you want your customers to be happy. And the best way to ensure customer happiness is to give people a user experience, or UX. In the digital age, UX design is crucial. It determines how easy an app or site is to use and how well it aligns with customers' wishes and demands. Despite its obvious invaluableness, however, UX design still isn't at the core of most companies' business strategies.

These blinks will show you how to weave together strategy and UX design, creating a UX strategy.

You'll also find out

  * what the four key elements in UX strategy are;

  * why provisional personas are a great tool for trying your ideas out; and

  * how to use storyboards to visualize the user experience.

### 2. Successful start-ups are built at the convergence of good business strategy and UX design. 

Imagine a tiny start-up with an awesome aspiration: they want to build a platform to connect reputable drug rehabilitation centers with addicts in need of treatment.

To realize their dream, they make a database of facilities with empty beds, prioritizing those of higher quality, and make a website as well as an app for future users. Despite following this solid plan, however, nobody signs up.

The company has a sneaking suspicion that their failure might have to do with their UX design and begins redesigning the look and feel of their app. But their work avails them nothing; people still aren't interested.

So what's going on? Well, the UX design is a part of the problem, but the real issue is that it isn't in sync with their business strategy.

This is where _UX strategy_ comes in. UX strategy requires that you take a big-picture view and consider whether your user experience fits your overall business plan. The first question here is whether the product you're offering is actually desirable to your users.

That's why, rather than assuming that your future users will want what you're selling, it's important to gather evidence of a demand way before you even get to your UX.

After all, it might make more sense to revamp your business strategy and reconsider the products and services you'll be selling to users _before_ considering their experience of your app. In other words, you should begin with your business strategy and then design a UX that fits it.

But how?

By following the straightforward plan laid out in the next blinks, you'll learn how to break down UX strategy into its four central elements: _business strategy, value innovation, validated user research_ and _state of the art design_.

### 3. A well-thought-out business strategy will put you ahead of the pack. 

So a solid approach to UX begins with your business strategy. But why, exactly?

Your business strategy is your company's DNA; it guides every decision your company makes. For instance, the famous business theorist and practitioner Michael Porter says that, for a business to be viable, it simply needs to have a competitive advantage over the competition. Without this, it's impossible to guarantee a business will continue to exist. There are basically two ways to achieve this advantage:

The first is called _differentiation_ — offering a product, or aspect of a product, that's different from the competition and for which customers are willing to pay more. This concept explains why Starbucks can charge more for a latte than can your run-of-the-mill cafe. The difference is, quite simply, that the experience of Starbucks is different.

Twitter is another good example of differentiation. Before the company started out in 2006, there was no way for people to broadcast information in such a short form. Now, instead of checking news outlets, users can get all the information they need in 140 characters.

The second way to achieve a competitive advantage is through _cost-leadership_. In other words, by making your product cheaper. Walmart is the prime example of this tactic. This mega retailer has stayed in the game simply by selling products at a price point well below what the competition can manage.

But of course, the best-case scenario is a product that combines both differentiation and cost-leadership. The result of this synergy is called _value innovation_, and it's the key to jumping way ahead of your competitors.

To get there, you need to create a product with no pre-existing competition that's also super cheap. By so doing, you'll enter what's called a _blue ocean market_ — a market devoid of competition, with a vast horizon toward which you can expand.

Just consider Facebook. The product is free, and nothing like it existed before it came on the scene. Its innovations were groundbreaking. Before Facebook, there was no easy way to find people you know, message them and share media.

### 4. User research and strong UX design round out your UX strategy. 

The old world of business was one of trial and error; sometimes ideas worked out and sometimes they went belly up. The only way people learned in this context was through painful failure.

But today there are other options. Now business people can remove the risk from their ideas, while reducing the costs incurred in the event of failure.

They do this by verifying products through user testing. In other words, rather than assuming you have a great idea that will solve your customer's problems, why not just put it into action and see what happens?

People often have great ideas that don't correspond to the desires of the market, and by testing these ideas on a selection of users and gathering feedback in the process, one can fine tune them so they fit market demand. From there, a revised product can be rolled out, its success can be measured and further feedback can be incorporated.

Just take Facebook. When Mark Zuckerberg launched his product in 2004, he didn't make it available to the whole world. Instead, he tested it within the confines of Harvard University. After this soft launch, he and his team improved the product's features based on the information they gathered. For instance, they included new emojis and added a call feature.

It's just one example of how ironing out your product through testing can produce great results. However, even an incredible product will fail without awesome UX design. After all, UX is what a customer feels when she uses your app or website. The best user experiences fully engage customers, making them love your product.

Airbnb is a great example. The interface of this vacation-rental platform is incredibly straightforward. It gives users the option to search using a number of self-explanatory filters, including price and apartment type, while also incorporating a map function that situates users in the city they're traveling to.

By focusing on UX design, Airbnb achieved the overall goal of all good UX: to acquire and bind customers through a captivating experience.

### 5. Testing your ideas, on both hypothetical and real customers, is essential. 

Imagine you've got an idea for an app that helps engaged couples plan their wedding. With your product, they can select everything, from a location to entertainment to hors d'oeuvres. The idea is great, but who are these soon-to-be-married couples?

A great way to answer this essential question is to form _provisional personas_ — tools to help you define your value proposition. More specifically, a provisional persona is a sketch, created by you or your team, of a potential user.

First, choose a name for your persona and assign him a picture. Then list his level of education, age, salary, job and any other relevant pieces of information. From there you can detail his behavior: What devices does he use? Does he have much free time? What does he value?

And finally, lay out his needs and goals by asking yourself two questions: What is he trying to achieve and what motivates him? For instance, he might need information about wedding planning in an efficient form because he doesn't have time for web searches.

But of course this is all hypothetical and you shouldn't assume anything about your future users. Rather, you should also collect real data about them by interviewing users in the real world.

This process is all about testing your assumptions against the facts by conducting a variety of mini experiments. Your value proposition and the provisional personas you've created are your hypotheses. Now test those hypotheses by asking real people the same questions you asked your provisional personas.

For instance, you could go to a mall and interview men with kids. There's a decent chance that these men are recently married and have definite ideas about the wedding-planning process.

Here are some questions you might ask: Was it difficult to find a good venue? What tools did you use? Why?

After conducting such interviews, the next step is to evaluate the results. For example, if you interviewed ten people and eight of them said that a centralized platform would have been helpful when planning their wedding, you'll have facts to support your idea.

### 6. Research and analyze the competition to improve your advantage. 

The best athletes review hours and hours of footage of their rivals to better understand the competition's moves and prepare for a match. Similarly, it's essential for you to observe your competitors to learn how they solve their problems. After all, understanding how other companies have succeeded or failed will help you build your competitive advantage.

This process is a little bit like peeling an onion; the more layers you peel away, the closer you get to the core of what you want. And as you peel, you may realize that your ideas aren't completely original.

While such realizations can be unpleasant, they're absolutely essential to finding out what truly _does_ set you apart. After all, discovering this special something is your ticket to the blue ocean of untapped market potential.

In the case of the wedding planning app, you might search for existing online services that organize weddings. For starters, you could research how much they cost and what they offer.

From there, you could take a closer look at when they were created and how they are funded. This information, as well as much more, can be found on sites like techcrunch.com.

Next, ask yourself how these companies make money. Is it through advertising, like on Facebook? Or through a subscriber plan?

And finally, look at how much traffic they get by using free services like compete.com, Alexa or Quantcast. For mobile apps, you can use App Annie or Mopapp.

Once you've gathered all this information, you can throw it in a spreadsheet and begin analyzing your data. For instance, you can sort the traffic column from highest to lowest and see which sites perform the best. You can also break your competition down into two categories, direct competitors and indirect competitors.

Direct competitors target exactly the same customer segment as you and deliver the same or a similar product. Indirect competitors, on the other hand, have either a similar product but a different target audience or the same audience and a different product.

### 7. Identify key experiences, take advantage of UX influencers and storyboard your value innovation. 

Everybody has heard the old adage "less is more." Well, it should come as no surprise that it applies to the development of stellar digital products as well. After all, to truly succeed, a product should focus on the experiences that are absolutely essential for users.

To define these, you can think about which aspects of your product differentiate it from the competition. In other words, where does your competitive advantage lie? For instance, in the case of Twitter, the key experience of the app is the ability to send a 140-character message. But also keep in mind that your product may have multiple advantages.

From there, ask yourself what experiences make your product different. In the example of the wedding-planner app, the defining characteristics are the ability to display wedding venues, options for food vendors and various design themes.

Once you've identified these essentials, you're ready to take a look at competitors that are neither direct nor indirect. Your task is to identify features that you can reassemble for your own product. Basically, it's a chance to be inspired by what already exists and combine it in different ways to disrupt the market. Airbnb did exactly that when they integrated a Google Maps-like element into their app.

The final step is to combine all these key experiences into a storyboard that visualizes a user's flow from point A to point B in your product. For the wedding app, it might go something like this:

A future bride browses venues and finds a list of five that she likes. She chooses the one she likes the most, selects a menu and entertainment package, receives a confirmation of her booking and, six months later, is getting married on the beach.

To draw out this process, you can simply do a few quick hand sketches or use design software to make something more complete. However you do it, you'll end up with a map toward value innovation that clearly describes the key features of your new product!

### 8. Final summary 

The key message in this book:

**By melding business strategy and user-experience design, you can create products that soar above the competition. This process begins with an approach to business that clearly defines your competitive advantage and results in a product that's both uniquely desirable and flawlessly designed.**

Actionable advice:

**Imagine a future innovation by asking yourself a few simple questions.**

Coming up with a groundbreaking business idea is no cakewalk, but you can jump-start the process by considering a few questions:

What features from existing products can I adopt, splice together and transform into something better?

How can I integrate separate user experiences into a single product that would become the one-stop shop for a certain task? Instagram is a good example as the standard for sharing photos and videos.

What product could bring together two different user segments to negotiate deals that were not possible before? Airbnb is the obvious example here, since it succeeded in uniting hosts and visitors in unprecedented ways.

Consider these questions for yourself and put your best ideas to the test!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Lean UX_** **by Jeff Gothelf**

_Lean UX_ (2013) is a guide to applying lean principles to interactive design workspaces. These blinks explain the techniques of Lean UX and outline how you can best integrate them into your company's design process. You'll learn the importance of close collaboration and customer feedback, as well as how to constantly improve your designs.
---

### Jaime Levy

Jaime Levy is a UX strategist and consultant with almost 30 years of experience. Additionally, she created the influential e-zine _WORD_, which was online from 1995 to 2000.

