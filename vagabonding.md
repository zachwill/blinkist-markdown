---
id: 56effcb3a3f3020007000062
slug: vagabonding-en
published_date: 2016-03-23T00:00:00.000+00:00
author: Rolf Potts
title: Vagabonding
subtitle: An Uncommon Guide to the Art of Long-Term World Travel
main_color: A2C3DC
text_color: 4D728F
---

# Vagabonding

_An Uncommon Guide to the Art of Long-Term World Travel_

**Rolf Potts**

A vagabond himself, Potts details his travel adventures in _Vagabonding_ (2002). Informed by firsthand experience, he outlines what to do and not to do in order to get the most out of hitting the road for the long haul.

---
### 1. What’s in it for me? Learn what vagabonding is all about – and get really good at it. 

There's nothing like taking time off from everyday life and flying overseas. Typically, that's what most people call a _holiday_. And it usually doesn't happen more than a few days every year. But what about _vagabonding_, that is, spending weeks, months or even years traveling around the globe? 

Sound tempting?

Indeed, vagabonding has nothing to do with holidays. Nor does it have anything at all to do with the prejudice of many, who — whenever they encounter the word — envision a gypsy-like man, either bumming around the highway or roaming city parks in search of a place to sleep.

So, what's it all about, then? The answer lies in these blinks. They'll show you how anyone can experience adventures on the road for more than just ten days per year.

In these blinks, you'll discover

  * when the best time for vagabonding is;

  * how much money you need in order to hit the road; and

  * why Russian taxis can be a real thrill.

### 2. Before leaving home to start vagabonding, you need to be independent. 

Are you the vagabonding type? Well, before you answer that, first ask yourself "Am I independent?"

Being independent means being free of the notion that long-term travel is a dream accessible to only a lucky few.

Many of us believe we need to be rolling in cash to enjoy travel for an extended period of time. Take the cult movie _Wall Street_. In one scene, Charlie Sheen's character, a young and ambitious trader, explains to his girlfriend how he plans to make a pile of money before he reaches his thirties so he can fulfill his lifelong dream of riding his motorcycle through China. The thing is, most of us fail to realize the same thing as Charlie Sheen's character does: we'd probably make enough money to do it by working as a janitor for six months!

We Westerners often think of travel as being outrageously costly. Most of us consider it a mere accessory to our lifestyle, rather than an experience that helps us grow. In fact, some of us buy vacation packages with the same mindset as we might a new car or outfit. This kind of travel is limited to short vacations with tight schedules that leave no room for the unexpected. The problem is: typical ten-day trips can't really set us free from our lives back home and often end up failing to give us the rich experiences travel is capable of providing.

Ultimately, this attitude has much to do with our relationship with money.

To so many of us, our daily activities come with a price tag. In other words, money controls such a huge part of our lives that we convince ourselves we're too poor to be free. For this reason, you likely consider long-term travel to be a privilege restricted to hippies, college students or rich people.

But, as we'll see in the following blinks, you couldn't be more wrong.

### 3. The right time to start vagabonding is now. 

Planning the trip of your life doesn't begin with setting a departure date or when you arrive at the airport — it starts with your attitude.

It begins as soon as you stop finding excuses for delaying your journey and start saving money, researching your destination and looking over maps. Vagabonding is about looking, learning, embracing your fears and changing your habits. This type of attitude isn't something you can just pick up at the check-in counter along with your ticket; it's something that starts at home and needs to be cultivated over time.

Your first concrete step, though, before packing your suitcase, is to work. Generating some income is crucial as it doesn't mean just earning your money but your freedom. The voluntary decision to travel for the sake of traveling is important for vagabonding, so having money in your pocket reduces the chances that you're running away from something. 

Then there are people such as _trustafarians_, who travel using their family's money (rather than money they've earned themselves). They're widely known in the backpacking world for being decidedly underwhelmed when it comes to their travel experience. Why? For the simple reason that they never earned their freedom to do it! Their trip has minimal value to them because it's no more than a short break from their lives. They lack an original, personal reason for traveling and will therefore often tell you that they're seeking meaning in their destination, failing to realize that the journey itself is what gives their lives meaning.

Before you set off, you should also be clear on the difference between holidaying and vagabonding. Some people don't work to travel, they work to make a living and then enjoy the occasional holiday as their reward. Vagabonds, on the contrary, work only to travel, and therefore hold a very different perspective on the matter.

### 4. Simplicity is an essential characteristic of the long-term traveler. 

One of the keys of developing a vagabonding mindset is to understand that it's not so much the amount of money you have, but the way you use it. The best approach here is to keep it simple.

Vagabonding involves applying simplicity to your life. Paring down on material possessions in a bid to live more simply is something we're all familiar with; every major religion teaches the absurdity of chasing happiness through material desires. But we seldom follow this principle!

To find out where you are on the simple living scale, try stuffing all your belongings into a backpack and see how well you do! Honing the skill of scaling down your needs is an absolute necessity when you're on the road, so start practicing this as soon as possible.

If you need a little help, there are three methods you can try:

First, stop expanding. This means not buying new things — including travel accessories. There's no point in getting water filters, sleeping bags or the like ahead of time, as you'll come across plenty of travel shops on your journey. Instead, limit yourself to a decent pair of shoes and a reliable backpack.

Second, live more frugally and put your savings into a travel fund. You can save a surprising amount of cash by eating home-cooked meals and taking a packed lunch to the office, for example.

Third, reduce what you already have. To get rid of your superfluous belongings, sell your stuff at garage sales. The extra cash you get from selling a few items is guaranteed to come in handy later. 

Although it doesn't sound alluring at first, you'll probably find that vagabonding on a tight budget turns out to be your preferred way of traveling! It encourages you to take risks, pushes you out of your comfort zone and challenges your habits. Plus, it'll put things in perspective when you realize that for the price of a take-out sushi box, you can eat seven day's worth of delicious meals in India.

### 5. Preparation will affect the amount of surprises you’ll encounter on the road. 

It's your choice: you can either prepare thoroughly in advance or stride boldly into the unknown. Either option will have a huge impact on your travel experience.

Keep in mind, though, that many experienced backpackers find that overly preparing for a trip puts a damper on it. A significant part of the wonder of traveling lies in the unexpected and the happy accidents you encounter along the way. On top of that, preparing too much beforehand can give you the wrong idea of a place. Some travelers pore over so many films and books that it gives them an idealized impression of a city. Then, when they finally get there, they're crushed to find it has dirty, noisy streets or rude people in the hotel.

Of course, if you're a virgin vagabonder, you should prepare to some extent, but be conscious of which material and sources you use to research your destination.

Daily news, for example, should be approached with caution. This is especially true for television, as some programs sensationalize conflicts or disasters and, in doing so, broadcast an unbalanced picture of a city or country.

Guidebooks — which should be read with skepticism — shouldn't be your only source of information. The author recounts a trip to Vietnam where he realized that certain establishments the guidebooks referenced had poor customer service. Why? They were confident that the exposure they received in such books would guarantee a constant tourist traffic regardless of their behavior. An ideal alternative is to browse travel blogs. The internet offers a limitless repository of non-professional, authentic sources, among which you're very likely to find exactly what you need.

### 6. To make the most of your experience, slow down! 

Have you ever gone on a trip and wound up being more stressed than you were at home because of all the activities you wanted to do and places you wanted to see? Don't get stuck in that trap! To really relish your time abroad, throw away your schedules and to-do lists.

Avoid hurtling through the sights as soon as you step out of the plane. Instead, make the most of your stay and take the time to have a good look at everything around you.

By shifting to a lower gear, you'll gain a different perspective on even the most ordinary things. Once you start vagabonding, you leave your routines at home. From then on, your everyday experiences have the potential to become extraordinary adventures.

For instance, an everyday task like taking a taxi in Moscow or ordering food in Osaka can give you a thrill that you simply wouldn't experience back at home. The same is true for the minutiae of daily life that you didn't notice before, like a fragrance in the air or the clothing style of a passerby. All the things you normally take for granted become exciting and exotic.

In this sense, vagabonding is similar to childhood, where everything is new and interesting. Like a baby, you'll find entertainment and wonder in almost anything, from the way people move and talk to the landscapes and wildlife around you. Of course, like any child, you can't understand everything in your environment, but the challenge of taking it all in, of making sense of the language, the customs and even crossing the street, becomes a thrilling adventure.

### 7. Traveling is only as good as the interactions you have while you do it. 

Traveling isn't only about the places you visit, it's also about the people you meet. In fact, these people will play a significant part in the months or years you'll spend on the road.

These people might be travelers, like you, or locals. They may be drinking buddies for an evening, or become lifelong friends, or even lovers. The things you can learn about their cultures will be fascinating, from learning drinking songs and discussing politics to adopting the appropriate dinner table etiquette. Socializing with others during your travels can also teach you a lot about your own country and give you a fresh perspective on things you might consider normal. For instance, Americans who were raised with individualism as a core value may be taken aback or intrigued by Asian cultures, which favor blending in over standing out as an individual.

While on the road, also be aware that money will hold a different value to what you're used to back home, so pay close attention to the way you manage it.

Traveling on a shoestring is necessary while vagabonding, but there's nothing more eyeroll-inducing than a Western tourist haggling to save 10 cents on a taxi ride when they have no qualms about spending 10 dollars on beers the same evening. So don't obsess over your wallet!

Approach your adventure with generosity and an open mind, but don't be _too_ open. For instance, you may be exposed to certain values or traditions by the locals and be tempted to completely renounce your own culture in favor of an idealized version of theirs. This phenomenon, _romantic primitivism_, was exemplified by the mass exodus of Western hippies to India in the 1960s, and it pays to be aware of it.

### 8. The notion of adventure doesn’t always match the image of an Indiana Jones escapade. 

For around two centuries we've known that there's no continent on Earth left undiscovered. So what's next? Is true adventure still possible? In the absence of uncharted territories, we need to rethink the meaning of the word "adventure."

What does adventure really mean in a world where risk-taking is fairly minimal, even if you fly to the other side of the world, hike to villages in the Himalayas or trek deep into the forests of Borneo?

Does it mean leaving Earth? In 2001, millionaire Dennis Tito paid $20 million so he could be the first civilian to travel into space. Tito's idea illustrates a feeling today that perhaps we're living in a world where the only adventure left to experience exists way up over our heads. However, such ideas rely purely on a physical definition of adventure. In fact, genuine adventures are better viewed as a spiritual challenge rather than a physical one.

The problem with modern adventures is that we decide on them in advance and approach them with precise expectations and a consumer attitude.

Conversely, real adventures can happen at any time, if you're willing to welcome the unfamiliar. So, when you're vagabonding, you may, for example, use squat toilets in Turkey, eat fried insects at a Chinese market or take a third-class train ride in India. These pursuits certainly qualify as adventurous experiences for the average Westerner!

Know that adventure can be found in the simple things that you wouldn't even think of doing back home. This way of traveling is the perfect opportunity to push yourself to the edge of your comfort zone and take a leap of faith.

### 9. To sustain interest in your adventure, you need to exercise your creativity. 

Even the most idyllic, lazure lagoon can turn into a flat, boring pool of water after enough time. So be prepared to use a bit of creativity if you want to stay captivated on your travels!

After a long stint on the road, you'll eventually fall into some sort of routine. Even when sunbathing on the beautiful beaches of Guatemala, there will come a point where your enthusiasm for your surroundings will start to dwindle.

Why is this? When you define your dream holiday, you do so at home, which might be a cold, gray, stressful city. And, once you're reclining on white sand, you might be surprised to find that you don't get as much of a kick out of doing nothing day after day as you'd thought. And so, your original dream starts to fade.

But there are ways you can stop this from happening. One way is to vary your means of transportation.

For example, one time when he was in Laos, the author purchased a small fishing boat to sail the Mekong river for three weeks. Another time he bought a Chinese bicycle in Burma and rode it south for ten days. Even getting off the beaten path and walking can be a fantastic way of meeting people, enjoying new scenery, and keeping your curiosity alive. 

Another way to keep things exciting is to find work, as this is excellent at providing some more depth to the places you visit.

When you work during your vagabonding, you'll rarely find a job that pays well. But that's not the idea anyway. The idea is to make enough money to cover your daily expenses while experiencing something different, as opposed to passively visiting your destination. You might choose to teach English or do farm work, for example. Hostels are also a good option, as they often look for help and may offer free accommodation, too.

### 10. Final Summary 

The key message in this book:

**While tourists go on holiday to escape the daily grind and chase after must-see attractions, vagabonders know to keep an open mind to anything and anyone around them. With the right mindset concerning money, planning and interacting with their surroundings, true vagabonders reap the greatest rewards of long-term travel.**

**Actionable advice:** **Keep a journal.**

While on the road you'll experience all kinds of unusual, funny, amazing or awful situations, so keep a journal to write down these moments every day, no matter how brief the entry might be. Once you're back home, you'll be grateful to have a record of all the experiences you had on your journey.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _The 4-Hour Workweek_** **by Tim Ferriss**

_The 4-Hour Workweek_ advocates the idea of the New Rich. These are the people who abandon their jobs as modern desk slaves and instead live a life that is all about enjoying the moment while still achieving big goals.
---

### Rolf Potts

Rolf Potts is a traveler and the author of _Marco Polo Didn't Go There_. His travel stories and articles have been published by _National Geographic Traveler_, Slate.com and _The Guardian_.

