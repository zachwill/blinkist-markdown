---
id: 58a068c9cb5c28000403161e
slug: value-proposition-design-en
published_date: 2017-02-15T00:00:00.000+00:00
author: Alexander Osterwalder, Yves Pigneur, Greg Bernarda, Alan Smith, Trish Papadakos
title: Value Proposition Design
subtitle: How to Create Products and Services Customers Want
main_color: F35D39
text_color: F35D39
---

# Value Proposition Design

_How to Create Products and Services Customers Want_

**Alexander Osterwalder, Yves Pigneur, Greg Bernarda, Alan Smith, Trish Papadakos**

_Value Proposition Design_ (2014) is a comprehensive guide to designing compelling products and services. Real value comes from empathizing with customers to find out what everyday jobs and tasks they need help with. However, coming up a product that helps customers complete these jobs and tasks is only the beginning.

---
### 1. What’s in it for me? A step-by-step guide to creating products and services with real value. 

Where do you start when you want to launch your own business? Many people would answer, "With the product or service you want to sell." That might sound easy enough, but how do you create something that actually provides value to customers?

This is the central question of these blinks. They lay out a comprehensive method for identifying what your potential customers value, and explore how best to deliver it in a way that meets their expectations. It's a step-by-step guide that starts with the perspective of your customers and ends with bringing your products and services to market.

In these blinks, you'll learn

  * why it's essential to understand the tasks you're helping your customers perform;

  * why immersing yourself in their world is a perfect way to do this; and

  * that presales should be seen as information-gathering rather than actual sales.

### 2. To identify value, put yourself in your customers’ shoes and recognize the pains and gains of their jobs. 

If you want to sell something, be it cupcakes, computers or cars, you have to ask yourself one important question: What makes my product valuable to a customer?

To figure this out, you first need to understand the jobs your potential customers need help with _._

A "job" is any task a customer performs on a regular basis. There are two kinds: _functional jobs_ and _social jobs_.

Functional jobs are specific things customers _need_ to get done, such as tending to the garden or buying groceries.

Social jobs are the things customers _want_ to do in order to impress others, such as buying a trendy outfit or wowing their boss with an amazing presentation.

The next step in identifying value is understanding _customer pains_ : the things in life that stand in the way of a customer performing a functional or social job.

The first customer pain is _unwanted outcomes,_ which is when a customer doesn't get the outcome he was after.

Maybe the customer bought a garden sprinkler that turned out to be useless or worked on a presentation that put everyone to sleep — both examples are unwanted outcomes.

The second customer pain is _obstacles_, which prevent the customer from even starting a job.

Maybe that coveted sprinkler system is just too expensive; or maybe the customer simply doesn't have the time to prepare a satisfactory presentation.

The last customer pain is _risks_ — the unpleasant things that happen when a job goes unperformed.

Without a sprinkler system, for instance, your customer's garden might die; and if that customer doesn't nail the presentation, she won't get a promotion.

The final step in determining value is recognizing the three types of _customer gains_ : the things customers are hoping to get.

The first is _required gains_, which is the basic function of a product. The required gain of a sprinkler is that the plants get watered.

The second is _expected gains_. Customers _expect_ a sprinkler to be well made and reliable.

The third is _desired gains_, which is when a product goes beyond expectations — such as a sprinkler system that can be operated with a smartphone app.

### 3. A value proposition defines how your product or service relieves pain and creates gain for customers. 

Once you understand the needs and wants of your customers, you're on your way to delivering a valuable product. The next step is to identify and understand all the components of your product. This will allow you to determine and design the _value proposition_.

You already have your starting point — you've identified the value of a sprinkler system that can be operated with a smartphone. Okay, now let's break this down into its individual components.

Imagine your product being advertised in a shop window, with the display listing all its different features.

There's your product's _physical/tangible component_, which is the sprinkler itself.

Then there's the _intangible component_, such as a five-year warranty.

And there's a _digital component:_ the smartphone app that controls the system.

Breaking things down like this further identifies the ways your product or service alleviates customer pains. And the more pains you relieve, the more valuable you become.

Focus on the biggest customer pains. For functional jobs, ask questions like, "How can I save customers time and money?" (One option might be to offer discounted installation services.)

For social jobs, ask, "Can I help customers be seen favorably by others?" Perhaps you can offer an online social platform where people share photos of their flourishing gardens.

Finally, to ensure your product's value and its appeal to customers, identify exactly how your product creates _expected_ and _desired_ _gains_ for your customers.

Since you can't cover everything, focus on the most important areas. Then ask questions like, "How can I exceed the expectations of my customers?" And, "How can I make my customer's life easier?"

For example, you could offer 24-hour customer service or design a sprinkler hose that never gets tangled.

By relieving pain, providing gains and helping customers perform their jobs, your product already has a fantastic value proposition.

### 4. Keep adjusting to ensure a perfect and profitable fit with the market. 

Now that you've designed the initial value proposition for your product or service, you can take the next step: figuring out how well it will fit in the marketplace. 

There are three different stages for determining whether your product fits the market.

The first indicator of a good fit is presentation. Does your product's value proposition look good on paper? Can you point to how it tackles all of a customer's jobs, pains and gains?

Keep in mind that this is just a hypothetical fit, so it's a good idea to design a few different models of your product before you go out into the marketplace and get real customer feedback.

That's where the second fit happens — in the marketplace, where you roll out a product or service and get the customer feedback that tells you how well you're actually doing.

This is when you get real facts about your product and can make adjustments to attain the best possible fit. The first model you put out there might receive some negative feedback. For instance, maybe some customers will tell you that your sprinkler-control app becomes unresponsive after a few minutes.

If customer feedback indicates that your product isn't meeting expectations, then it's time to redesign the product, reintroduce it to the market and continue fine-tuning it until it's a perfect fit.

The last stage is the financial fit. Here you get the cold, hard facts that reveal whether your value proposition is profitable and scalable.

The goal is for your value proposition to lead you to a profitable business. Generally speaking, this happens when the revenue you generate exceeds the costs of your business.

Whether you're trying to sell a sprinkler system or a fashionable pair of shoes, you'll only succeed if your value proposition generates more revenue than costs.

Next, we'll look at some strategies to help you come up with truly valuable ideas for products and services.

### 5. You can understand your customers by studying data, observing their habits and meeting their employees. 

It might not be obvious, but an ornithologist and an entrepreneur have a lot in common. Only, instead of studying birds to understand avian habits, entrepreneurs study customers to understand consumer habits. Those habits reveal crucial information: customers' wants and desires.

One inexpensive and easy way to start gaining insight is to analyze customer data. 

Once you have a website, you can use the tracking and analysis services provided by Google Analytics. With this data, you can look at how customers end up at your site and see whether it's through browser searches or links provided by other websites. You can also make a list of the top ten most visited pages on your site and the top ten least visited pages.

From there, you can go a step further and immerse yourself in your customers' world to find out more about their jobs, gains and pains.

For example, people in business-to-consumer (or B2C) endeavors — businesses that sell consumer goods like toothbrushes or lamps — might spend a day observing a potential customer's family.

This allows you insight into those day-to-day jobs, such as making breakfast for hungry kids, and what pains and potential gains are connected with them. This is the kind of first-hand observation that can lead to ideas about how to improve, for instance, a slow toaster.

Another, less direct approach is to observe your potential customers by visiting the stores they go to. Notice which products they're looking for, which ones they select and which ones they ignore. Over the course of a full day, you should be able to see patterns in their shopping habits.

For business-to-business (or B2B) endeavors — where one business sells products to another business — you can use a similar strategy. Spend a day or more at the headquarters of a potential customer and set up some workshops with their employees to find out about the pains and gains of their jobs.

You might gain some helpful information — that they're unhappy with their copy machines due to the confusing and hard-to-use interface, for example.

### 6. To come up with valuable ideas, try making napkin sketches and filling out ad-libs. 

Maybe you've heard the old cliché about a billion-dollar business being based on an idea scribbled on a bar napkin. Indeed, it's a good idea to always jot down your ideas somewhere.

The goal is to put the central idea of a value proposition onto a piece of paper no bigger than a napkin — and then share it. Don't bother with any details. This way you can quickly visualize and share an idea with coworkers, or even customers.

You don't need to worry about negative feedback here, either. If an idea doesn't pan out, just move on to the next one. It'll likely take more than a few napkins before you strike gold.

Ikea has a clear (and good) central idea. If written on a napkin, it would probably say something like, "Provide customers with packaged components of fashionable furniture they can put together at home for low cost."

As you can see, a napkin-sketch can be a very rudimentary idea for a prototype.

Ad-libs are another helpful tool to spark a valuable idea.

Ad-libs are short sentences that require you to fill in the blanks, such as: "Our (blank) helps (blank) who want to (blank) by providing (blank) and (blank)." 

In the first two blanks, you put in your product and your target customer, and in the third blank you put in the job that you're helping the customers perform. In the last two blanks, put in the action related to the pain the product helps alleviate, and then the gain you're providing.

You should end up with something like this: "Our _programmable sprinkler system_ helps _gardeners_ who want to _grow an amazing and healthy vegetable or flower garden at home_ by providing _a modern and time-saving system that can help them grow the garden of their dreams_ and _is easy and stress-free to use_.

To see how you stack up against the competition, you can add a couple blanks to the beginning of the sentence, so that it reads, "Unlike (blank product) from (blank competitor), our product…" and so on.

### 7. Prototypes and pre-sales are two great ways to gather early data on customer interest and reactions. 

After you've come up with a couple of ideas for a product or service that is hopefully useful and therefore valuable, the next step is to see how it actually tests with real customers.

At this point, you've already asked people for their opinions about your proposed service. However, while these responses are valuable, they're not always accurate. This is why it's crucial to get customers to interact with prototypes as soon as possible.

There are always high levels of uncertainty when designing a value proposition. So gathering as much evidence as you can to validate your idea is key to moving forward.

A great way to get this validation is through full-size prototypes and accurate models that allow you to test how customers really use your product or service.

This is the idea behind car shows. They offer customers a chance to interact with prototypes that showcase new technologies; meanwhile, the car companies observe the customers and collect important data.

Since these demonstrations don't always turn out as hoped, you should try to make an inexpensive prototype that won't take too long to build. Remember, you might have to make adjustments and produce a whole series of prototypes before you get it just right.

Another way to gain early insight into customer interest is pre-sales.

This is both a way to present customers with your value proposition and a chance to pay for a product that is still in development. Furthermore, it tells you how eager customers are to see your idea come to life.

Kickstarter is a popular way to take advantage of pre-sales data. There are many creators and inventors who've used it to assess the viability of their value proposition.

If your value proposition is geared toward business-to-business, you can still gauge pre-sale interest by presenting your idea to different companies and, if they're interested, having them sign letters of intent.

A letter of intent works kind of like Kickstarter; it's a commitment on their part to buy your product when it's finished.

Okay, you're now ready to start designing your own value proposition using the tools described in these blinks. Good luck!

### 8. Final summary 

**The value of a product or service derives from its ability to help people perform a functional or social job — that is, it makes customers' lives easier. To find out how your product or service can do this, you need to empathize with your customers and learn which jobs they need help with. Once you have your idea, you'll need to test and adjust your prototypes until you have the perfect product.**

Actionable advice:

**Include some "call to action" in your prototype phase already.**

A call to action is a good way to figure out how much potential customers are ready to pay for your product or service. Say you sell cars. You could show people a prototype and then offer them the opportunity to pre-reserve one by paying a sum of their choosing between $250 and $10,000. The more they pay, the higher they go in the queue, so you can easily gauge their enthusiasm for getting one of your cars.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Business Model Generation_** **by Alexander Osterwalder & Yves Pigneur**

_Business Model Generation_ (2010) is a comprehensive guide to building innovative business models. From empathizing and connecting with customers to finding inspiration for products and learning from some of today's most game-changing platforms, these blinks will help you kick-start your business thinking.
---

### Alexander Osterwalder, Yves Pigneur, Greg Bernarda, Alan Smith, Trish Papadakos

Alexander Osterwalder, an entrepreneur and respected keynote speaker, is a co-founder of the company Strategyzer, which offers a platform and services to create superior products and businesses.

Yves Pigneur is a Belgian computer scientist who specializes in management information systems.

Greg Bernarda is a popular speaker on innovation and strategy; he is currently an advisor for the consulting firm Utopies.

Alan Smith is a designer and entrepreneur. He is also a co-founder of Strategyzer, where he creates tools to help people design better businesses.

Trish Papadakos is a photographer and designer who has been at the forefront of many successful businesses. She teaches design at Central St. Martins in London.

© Alexander Osterwalder, Yves Pigneur, Greg Bernarda, Alan Smith, Trish Papadakos: Value Proposition Design copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

