---
id: 596167c7b238e100054fb115
slug: vaporized-en
published_date: 2017-07-11T00:00:00.000+00:00
author: Robert Tercek
title: Vaporized
subtitle: Solid Strategies For Success in a Dematerialized World
main_color: C39333
text_color: A87F2C
---

# Vaporized

_Solid Strategies For Success in a Dematerialized World_

**Robert Tercek**

_Vaporized_ (2015) takes a hard look at the future of technology. As we move further into the digital age, it's wise to know what to expect, including what might happen to banks, jobs and the way we do business. There's no denying that the internet has changed our lives — and, whether we like it or not, there's much more to come.

---
### 1. What’s in it for me? Learn what it means when information and entire industries turn into vapor. 

A few hundred years ago, monks in medieval monasteries were the main guardians of wisdom. They carefully collected the knowledge of the world and transcribed and conserved it for the future.

But with the invention of the printing press, their efforts became obsolete. And now, in the age of the internet and digital technology, printed books are in the boat the monks were once in. In a word, information — once an inert mass, contained in the pages of books — has turned into vapor.

Yet it's not only information that has been _vaporized_. A number of physical things — and with them whole industries — have begun to vanish into thin air.

These blinks explain what an economy in a gaseous state will look like. They tell you what goes hand in hand with the vaporization of information and the economy, which opportunities and risks a vaporized economy entails and how to best prepare for it.

In these blinks, you'll learn

  * about the three chemical states of information;

  * how internet giants like Google and Facebook create value in a vaporized world; and

  * what the vaporization of our economy means for human labor.

### 2. Information can exist in three different states: solid, liquid and vapor. 

With all of our handy internet-connected devices, it's easy to understand why today is called "the information age." Less easy to understand is how this information behaves.

It's helpful to think of information like water, or other chemical elements since it can exist in a variety of different states depending on how we want to use it.

To start with, information can be in a _solid_ state, like in a book or on a CD. The downside of having information in a solid state is that it's hard to share with more than one person.

But selling information in physical form is a convenient way to monetize it, which is another reason why keeping information in a solid state has, historically, been preferred.

Remarkably, the way we exchanged information didn't change much at all between 1455 (when Johannes Gutenberg invented the printing press) and the recent emergence of the internet. Throughout this span of time, information was always stored in a solid state, whether as books, CDs or DVDs.

Though these physical states are durable and long-lasting ways to store information, they also demand a lot of money and energy to produce and distribute. What's more, they eat up storage space and can only be shared one person at a time. Plus, things like laser discs quickly become obsolete.

So when the internet emerged, people didn't hesitate to free information from its physical constraints and transform it into a more _liquid_ state.

Imagine an ice cube beginning to melt; information, water-like, was now much easier to spread over a large area. With the click of a mouse, it was simple to post, edit and instantaneously share a file with the entire world.

But there was still another change of state in store. As information moved from desktop computers to the smartphones, it became like a _vapor_ : fast-moving, free and constantly changing.

This is where we are now. With high-speed internet in our pockets, information is no longer bound to your office, or even to your laptop. It moves all around you, like atmospheric gasses.

No matter where or who you are, be it a scientist in a lab or a monk in a mountaintop monastery, today learning anything, at any time, is but a tap or two away.

### 3. Software can vaporize businesses, physical objects and entire industries with digital equivalents. 

One of the greatest strengths of today's software-based technologies is that they allow us to do more with fewer resources.

They help us monitor and analyze functions while providing new information and insights that allow us to make better designs. For instance, today's cars can collect data about their own performance, and this data helps car companies produce more efficient models that pollute less.

Software can even be used to replace major utilities, such as telecommunications networks that handle the traffic of thousands of people talking and messaging.

With today's advanced software, we could get rid of all the telephone poles and wires, and replace them with an efficient internet-based network that would route calls better and more reliably than before.

Many of the physical goods in our day-to-day life could also be replaced by software, along with all of those old CDs and DVDs.

It's only a matter of time. Vaporization will encompass everything, from single jobs and stores to companies and entire industries. Anything that _can_ be vaporized and replaced with software _will_ be vaporized.

Indeed, this process has already begun, and the music industry is a prime example. Big record shops have been replaced by online services like the iTunes store and Amazon. Instead of dealing with limited physical selections and waiting in line at a store, now anyone with an internet connection has access to a wealth of digital music.

And when these record stores are replaced, the whole consumer experience and supply chain are vaporized too. The industry that produces CD packaging will disappear too, as will the warehouses that stored inventories of CDs. And the same fate awaits the industries that produce secondary products like CD players. All of them will have been made redundant by software.

> Today, about 84 percent of a typical company's value is in intangibles such as processes, data and connections.

### 4. Online companies create ecosystems where their own vague rules are enforced. 

Internet giants like eBay, Amazon, Google and Snapchat didn't stumble upon popularity.

Each company knew how to connect the right people with one another, kind of like the operators at an old-fashioned switchboard.

For Amazon and eBay, the "right people" were obvious — these platforms wanted to connect buyers with sellers. But the same strategy holds true for other internet giants.

Google has made a fortune connecting information seekers with those who have the information being sought. YouTube connects video producers with viewers. And social apps like Snapchat are just facilitators for everyday people who want to stay connected with one another.

But as each of these companies grew, their ecosystem of connections became more complex as different and unexpected users joined in.

Take Apple's App Store, for instance. What started as a place for developers to sell helpful applications soon morphed into a highly competitive marketplace, with developers hiring firms to raise their sales.

These platforms aren't the kinds of public spaces where people can exercise freedom of speech. The platform owner can make up arbitrary rules — whether well explained or not — that they can then enforce, which gives users two choices: either to comply completely or not participate at all

Apple says this ensures the apps being sold in the App Store meet their standards for what's acceptable.

Yet they often give vague reasons, like an app being "creepy," "objectionable" or "over the line" to explain a rejection. And even though fart apps routinely get kicked out, you can find, for example, a "Sexy Poker" app in their store.

It should also be noted that Apple receives 30 percent of every purchase in the App Store, but that the system for determining an app's ranking, which plays a huge role in an app's chances for success, is also extremely vague.

This has led to rumors that secret payoffs, bribes, gifts and other dubious methods can be used to boost an app's ranking in the store.

### 5. Successful online platforms strive to keep and monetize their users. 

Making up rules isn't just a way for internet companies to control user behavior; it can also help ensure that users remain loyal and return to the platform.

This can be difficult in a vaporized economy, as users tend to gravitate toward whichever platform is the biggest. So let's look at some different methods companies use to bind people to a platform and make them more active — all while generating more profits in the process.

One of the most common ways to hold on to users is to create unique tools that allow them to generate their own content.

Whether they're posting photos or videos, or just creating chat threads, these tools promote engagement and keep users from leaving, because, if they leave, they lose access to all their content. This is why Apple and Google offer free content creation tools in the form of iMovie and YouTube.

Another effective way to keep users loyal is to make it easy to find content and then share and chat about it. Facebook and other social media companies thrive on this dynamic.

It's also common for successful platforms to offer storage and playback features to their users.

Without these features, platform owners run the risk of users finding a way to take their content elsewhere. This is why Google has a whole line of hardware such as the Chromebook and Nexus, as well as features like the Chromecast. It's also why Apple provides content that is only compatible with their own products.

Providing content can also lead to monetization when a platform establishes its own marketplace for content, where owners can take a cut of each transaction, as in the iTunes Store.

But another popular method used by many platforms is to get users to fill out elaborate profiles, after which they can sell this detailed user data to marketing firms.

> _"Surveillance is the business model of the internet."_

### 6. The internet of things will continue to increase the amount and value of data. 

The vaporized economy has offered a variety of ways for businesses to make money, most of which involve turning data into big bucks.

Having people constantly connected to the internet produces massive amounts of data, and this amount is set to grow further.

By 2020, it's estimated that every person will be producing a mind-boggling 1.7 megabits of data per second.

This will be due to the _internet of things_, where everyday objects, like your refrigerator and washing machine, will all be connected to the internet and collecting data.

Today, most of us still like to think that there is some time during the day when we're offline. But, before long, we'll all be permanently connected and continuously producing data.

When this happens, big-data companies will mine the output coming from these appliances and monitor and improve on the measurements.

Take a keg of beer, for instance. The US company SteadyServ has a sensor that goes on the bottom of a keg and streams data to a server to allow restaurants and bars to manage their inventories in real time. So, when a keg is nearly empty, an alert can be triggered for a replacement. But more importantly, companies will also be able to closely monitor sales at different bars in real time to get a better understanding of local, state-wide or national trends.

Traditional companies are also placing more and more value on data, but they struggle with the sheer volume of it all. The challenge lies in how to archive, sort, search and effectively analyze data for useful information, all while keeping it out of the hands of malicious hackers.

But surmounting this challenge will unlock plenty of future business opportunities.

> _"Our appliances are about to wake up."_

### 7. New business models are bypassing the traditional concepts of ownership and middlemen. 

Back in the day, a business could be accurately valued by taking stock of its physical assets. But this doesn't apply to companies like Airbnb and Uber, which have no actual rooms or cars of their own.

So how does Airbnb get valued at $30 billion and Uber at $66 billion?

They create a peer-to-peer marketplace by operating software that acts as the middleman between their clients, who own the apartments and cars, and their users, who want to temporarily use this property.

This model is also known as "access-instead-of-ownership" and part of the platform's success is due to its collaborative nature, matching, for example, users seeking lodging with those who already own apartments that they can rent out. In addition, both parties can choose whether or not they want to do business with the other based on ratings from previous transactions.

And innovative _blockchain technology_ promises to take this model to the next level by vaporizing even more middlemen.

Blockchain technology relies on the principles of cryptography to allow for business transactions between two parties without any company or banking oversight, and thus without any associated fees.

It's already being used for Bitcoin, the electronic currency.

Platforms operating on blockchain payments can be completely independent of banks and credit card companies that take big percentages on transactions and sell user data for dubious purposes. A "blockchain" version of Airbnb, for instance, could allow users and owners to save money.

Another example of a streamlined service is crowdfunding, which is making it easier for entrepreneurs to find funding.

Crowdfunding bypasses the slow, traditional process of raising investor funds by getting rid of the paperwork and endless interviews. It puts the power in the hands of individuals to reach out on a software platform, attract investors through incentives and even integrate feedback during the development process.

### 8. To help displaced workers, we need to reform the US education system. 

The previous blinks have described the vaporization of services, physical products and even entire industries. So you may well be wondering, "How many jobs is this going to take away?"

It's true that, along with the vaporization of human labor, automated robots are already capable of replacing a number of human tasks.

At the Henn-na Hotel in Nagasaki, Japan, ten robots are capable of maintaining the hotel by performing a variety of functions, including carrying luggage, changing bed sheets and cleaning rooms.

These aren't exactly desirable jobs, but other fields such as journalism, medicine, law and accounting are also already under threat of becoming automated.

In fact, a 2011 Oxford report estimated that 47 percent of all US jobs could soon be automated.

This might not be entirely bad news. There are optimists who see automation as a chance to raise the living standards of everyone and expand personal freedoms. But then there are also those who believe that automation will render a large portion of society permanently unemployed.

The important question is whether or not automation will destroy more jobs than new forms of businesses can create. Unfortunately, it's too soon to tell.

But what we do know for certain is that our education system is unprepared to deal with the challenge of re-educating and training millions of displaced workers. It was never designed to educate a workforce and teach professional skills, but rather to teach students about abstract ideas.

In a vaporized economy, the education system must adapt so that humans perform the tasks that computers can't handle, like holding student discussions, debating and applying knowledge in novel ways to solve ideas.

While we might not be able to prevent the loss of jobs, we can make sure we're ready and prepared for what lies ahead.

### 9. Final summary 

The key message in this book:

**Thanks to the advent of the internet, more and more information is vaporizing — that is, becoming divorced from tangible objects and entering the cloud. By leveraging information from our environment and feeding it into software, we will be able to do ever more things with ever fewer resources. Today, the data a business generates and collects is its primary asset. And the success of future generations may depend on their ability to perform jobs that can't be automated.**

Actionable advice:

**Be aware of the role you play online.**

Everything you do in a virtual store is tracked. Every offer you notice and respond to is processed and used to build a profile of you. So think twice whether you really need a profile on that shopping site and whether you agree with the access rights of that app you just downloaded.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Industries of the Future_** **by Alec Ross**

_The Industries of the Future_ (2016) gives a sneak peak at the effects information technology and the next wave of innovation will have on globalization. These blinks explain how people, governments and companies will need to adapt to a changing world driven by big data.
---

### Robert Tercek

Robert Tercek is a media and digital expert sometimes referred to as the "TV Anarchist." He's worked for MTV, Sony Pictures and was president of the Oprah Winfrey Network.

