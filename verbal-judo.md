---
id: 5a2d7814b238e1000a9f2ca5
slug: verbal-judo-en
published_date: 2017-12-15T00:00:00.000+00:00
author: George J. Thompson and Jerry B. Jenkins
title: Verbal Judo
subtitle: The Gentle Art of Persuasion
main_color: 2885C9
text_color: 2885C9
---

# Verbal Judo

_The Gentle Art of Persuasion_

**George J. Thompson and Jerry B. Jenkins**

_Verbal Judo_ (1993) is your guide to effective communication, from a police officer's perspective. These blinks explain why some common communication techniques could be holding you back and why others may be better suited to achieving your communication goals.

---
### 1. What’s in it for me? Get a black belt in communication. 

Have you ever tried to communicate something, but, despite your best efforts, been unable to clearly state exactly what you mean? And meanwhile, you know that your interlocutor is getting more and more impatient, or even offended and angry? Well, you're not alone. Pretty much everyone has, at some point, found themselves in such a situation, whether at work, with a loved one or with a complete stranger.

So what are you supposed to do when communication goes awry? How can you defuse the tension and express yourself plainly?

Well, get ready to learn some Verbal Judo — a linguistic art that will help you do away with miscommunication, as well as improve your ability to engage in meaningful conversation. These blinks will teach you the moves.

You'll also learn

  * that direct orders are a counterproductive form of communication;

  * why empathy is the magic wand for tension-free communication; and

  * that paraphrasing is an often unused, but extremely powerful, tool for communication.

### 2. Communicating effectively in difficult situations is an art form that can be studied and learned. 

Imagine you're a rookie cop. It's two in the morning, and you've just been sent to break up a domestic altercation in a rough neighborhood of Emporia, Kansas. Sounds a bit scary, right? Well, the author once found himself in exactly this situation. Luckily for him, his partner, Bruce Fair, was an experienced officer. He was also the man who gave the author his first lesson in _Verbal Judo_.

This term refers to the art of communication, a process that actually has no fixed rules at all.

For instance, consider how the author's partner dealt with the screaming couple: he walked straight into their apartment, plopped himself down on their couch and began reading the newspaper. The couple looked at him a few times, but they kept right on arguing. Finally, officer Fair interrupted their dispute and asked if he could use their phone. The couple, thrown off by his request, complied, and their argument broke off.

From there, officer Fair mumbled something into the phone and put it down, feigning displeasure at the fact that someone would refuse his call at 2 a.m. He then asked the couple what was wrong and reminded them that it was best to be quiet at this time of night. In the end, the conflict resolved amicably.

This was the author's first experience of the true power of communication.

Observing police officers as they defuse tense situations is the perfect way to learn about communication. That being said, it requires close study. Even the author was initially puzzled. He wasn't exactly sure what had happened that night in Kansas and, when he asked his partner why he'd done what he'd done, officer Fair simply replied that he'd followed his instincts.

The author was hungry for more, and so he set out to uncover the structure behind those instincts. During the following years, he observed his fellow officers and took meticulous notes on their approach to communication. All this work resulted in the author's guide to the art of Verbal Judo.

> _"I quickly became convinced that good police officers are the greatest communicators in the world."_

### 3. Avoid direct orders and condescension, and always explain the rules you’re enforcing. 

Have you ever been verbally reprimanded by a random stranger for committing some minor misdeed — say, jaywalking on a deserted street? If this has ever happened to you, you can rest assured that your verbal assailant didn't know the basic rules of Verbal Judo. That's because effective communication is predicated on avoiding direct orders and condescension.

In fact, there are some common phrases that both police officers and civilians would do well to avoid at all times. For instance, ordering someone to "come here!" will never serve your goals. Not only does it sound threatening; it also gives no information about _why_ the person should comply. A much better approach is to politely get the person's attention and then ask if you can have a word.

Another phrase to avoid is "you wouldn't understand." It's extremely condescending, and implies that you regard the other person as either stupid or inferior or both. It's much better to inform your interlocutor that what you have to say might be difficult to understand or even to explain. Make clear that it's the complexity of the information, not a deficiency in your interlocutor's intelligence, that might be getting in the way of immediate comprehension.

Verbal Judo also requires that you actively explain the rules you're enforcing, rather than falling back on de facto authority. A great example of what to avoid here is the classic truism often resorted to by parents, teachers and police officers alike: "the rules are the rules."

As any kid will tell you, there's no justification more annoying than this one. After all, if the rules have a sound reason for existing, you should be able to explain them. For instance, if you want your kids to go to bed at 7 p.m., you _could_ just say that that's the rule, or you could explain to them that if they don't get enough sleep, they won't enjoy school in the morning as they will be tired and ratty.

### 4. Good communication means understanding where the other person is coming from. 

Do relationships with other people ever make you feel tense? Wouldn't it be wonderful if you had a magic wand and could just make all that discomfort disappear? Well, luckily for you, such a magic wand does exist. It's called _empathy_, and without it there can be no good communication.

According to the author, empathy combines the Latin prefix _em-_ ("to see through") and the Greek suffix _pathy_ ("the other's eye"), and so it literally means to see through someone else's eyes. In this sense, empathy is about adopting the perspective of another person, whether he's your spouse, your employer or your friend. No matter what the relationship, empathy is crucial.

After all, without it, you're just talking _at_ each other, rather than truly communicating. But keep in mind that being empathetic isn't the same as being _sympathetic_. You don't need to love or even be that fond of the other person to feel empathy; you just need to see his side of things.

Becoming more empathetic will not only equip you to defuse tense situations. It may even help you save lives. On one freezing, gusty night, the author was called out to deal with a suicide attempt. When he arrived on the scene, he found a man sitting in a bathtub and threatening to electrocute himself with an electric heater.

In such situations, most people would try to convince the person that his life is precious, that he shouldn't do it. But this would be arguing from the perspective of someone to whom suicide seems unreasonable. It's an argument that lacks empathy. And so the author told him how horrific it is to die by electrocution, even exaggerating the terribleness. He then went on to list a handful of easier ways to commit suicide. This unorthodox approach succeeded in defusing the tension and, soon enough, the man agreed to get out of the bath.

### 5. Paraphrasing is another powerful tool, but it requires an interruption. 

Have you ever found yourself stuck in a conversation with someone who keeps going on and on, refusing to let you get a word in edgewise? How can you politely make these people stop talking without hurting their feelings?

Well, the best way to slip yourself into the conversation is to _paraphrase_, which simply means restating, in your own words, what the other person has said.

However, to be able to do that, you first need to interrupt the person. This can feel like a daunting task, especially when faced with someone who either just _loves_ to talk or is in an emotionally sensitive state. There's no easy way around it, though; you've got to summon up the courage and get in there.

Be sure, however, to keep it neutral. Avoid any judgmental interruptions like an irritated request for the person to shut up or calm down. Instead, use a one-word exclamation like "whoa!" or "listen!" Whichever word you choose, say it in a neutral voice and make your conversation partner pause. When he does, you've got your opening to paraphrase.

Such an opportunity is huge since paraphrasing is one of the single most powerful weapons in your Verbal Judo arsenal. After all, it's incredibly empathetic; it communicates both that you've heard what the other person said and that you're trying to understand.

To begin this process, simply say that you want to make sure that you've understood correctly. Doing so immediately signals that you care and are listening. Once you do this, the other person will listen to you. You'll be in control and can then use your new position to clarify what the other person is trying to communicate.

For instance, say your partner accuses you of always coming home late. You might ask her if she thinks you're _always_ too late. Chances are she'll amend her previous statement and admit that, no, you're not late every night of the week — but you have been for the past three nights! From there, you can talk about why you were late on those nights and smooth the situation out.

### 6. To bring your communication to the next level you need to identify your flaws. 

When used appropriately, communication will enable you to make even the most difficult person happy and compliant. But to get there, you have to seriously develop your skills, reaching the level of a _contact professional_. What separates contact professionals from your standard good communicator is that the former can deliver in extremely high-pressure situations.

The famous basketball star Michael Jordan is a great example. When the pressure was on, his game only got better, and your goal should be to strive for the same level of professionalism in your communication. There's only one way to do that.

You've got to name and define your communication flaws. Let's call these deficiencies your inner enemies.

Just take the author. He discovered early on that he absolutely hated it when people challenged his authority; this fact was his inner enemy.

It wasn't easy for him to overcome. From time to time, the author would arrest someone who, instead of going docilely to the station, would begin listing all his influential contacts, taunting the author and saying how, sooner or later, he'd be released from jail or prison. Such interactions brought out a rash, aggressive side in the author that wanted nothing more than to shout, "you wanna bet!?"

As a result, he often became angry with people in his custody, and this led to errors. So he named his inner enemy the "wanna bet" voice. By simply naming it, he had an easier time identifying it when it reared its head. This made it easier for him to step back, remain cool and avoid lashing out.

### 7. What you say matters as much as how you say it. 

Sometimes people speak at such cross-purposes that they might as well be speaking two different languages. So it's no wonder that, in the field of communication, the word "translation" denotes the ability to choose the right words to communicate exactly what you want.

This ability is essential since, in the course of day-to-day conversation, misunderstandings often occur due to an imprecise use of language. Translation endeavors to transmit your precise thoughts, via language, into the mind of the other person. But in order to translate your thoughts into words, you first have to know exactly what you wish to communicate. For instance, you might want to tell your partner that you're sorry for missing your anniversary.

From there, you need to decide on the exact words you want to use. After all, there are a million ways to say sorry, some of them effective, others not.

Then, once you've got your content and your wording, you can make your statement and interpret its reception. If your partner simply shrugs and turns away, your message likely didn't fall on glad ears.

Truly effective translation requires adapting your words to a specific audience. If effective communication is your goal, you can't just be on autopilot, you must tailor your speech to every single person.

For example, if you're a police officer in a city, you need to have a specific tone for every type of individual you encounter. You don't want to speak to an elderly woman in the same tone that you'd use with a young man from the suburbs. A good rule of thumb is to disrespect no one, and to use a different tone with everyone.

Parents usually have a different way of speaking to each of their children. That's because parents know that each child has a unique character and will respond to different communicative approaches. Unsurprisingly, the same goes for everyone else, too.

### 8. Mediation can defuse stressful and dangerous situations. 

To teach a child how to safely cross a road, you'll have to bring her to an intersection so she can observe what's going on. It'll be clear to her that the cars move back and forth, some going in one direction and others in the other, and that they occasionally stop. However, she might not understand _why_ they stop until you point out the traffic lights, explaining that green means go and red means stop.

In pointing out this detail, you're acting as a mediator, allowing the child to recognize something new. Interestingly enough, it's a skill that's also key to police work.

In fact, mediation can save cops from getting into dire situations. After all, police officers often interact with people who have lost all sense of perspective. For instance, once he'd completed his police training and spent a few months on the job, the author was called in to deal with a man causing a commotion in a bar.

When the author arrived on the scene, he found the massive man in an alley, brandishing a broken whiskey bottle and aggressively advancing upon him. The author began by stating the complaints that had been lodged against the man and asking him to come to the station.

Unsurprisingly, the guy didn't respond; he was much bigger than the author and saw no reason to comply.

So the author tried out some mediation. He asked the man what he planned to do. Was he planning on cutting the author with the bottle?

The author then explained that, at the moment, he was only being charged with a misdemeanor — just one night in jail. However, if he attempted to assault an officer, he would be facing a felony — a much more serious situation.

Laying out the facts is at the core of mediation. By making the situation clearer to the man in the alley, the author defused the situation, and the man threw away the bottle and allowed himself to be taken into custody.

### 9. Domestic disputes are inevitable but they can be directed toward productive ends. 

A life of blissful harmony and intimacy is the stuff of every couple's dreams. But real life tends to be quite different. Relationships are hard and domestic fights inevitably pop up, often out of the blue.

For instance, one of the author's friends married a woman who had four kids from her prior husband. Two months into the marriage, he was still enjoying that honeymoon glow. Everything seemed to be perfect.

But then, after he'd returned from a hard day at work, his wife attacked him out of nowhere, accusing him of intentionally undermining her authority with the children when he arrived home from work every night. The friend reacted as you'd expect any blindsided husband to react; he first felt shock — and then, following close on its heels, anger and irritation. And so began a contentious night.

But it could have gone differently.

Indeed, domestic disputes can even be used to productive ends, when handled appropriately. Let's see how what you learned in the previous blinks can be applied to this situation.

First, interrupt with a brief, empathetic sentence like, "Whoa! Hang on a second, I just need to be sure I am understanding you right." Then use that opening to paraphrase your partner's words. In this case, you might ask if your partner really thinks you undermine her every night and if she really thinks you do it intentionally.

This should dull your partner's accusation and give you a chance to move onto discussing what can be changed. If you engage in conflict in this manner, you're actually opening up an opportunity for your partner to see your love and commitment. And if your partner sees that, you'll be repaid a hundred times over.

Now that you understand how to deal with the most intimate of settings, your Verbal Judo arsenal is complete. You're now ready to gently assert your authority and manage relationship difficulties in a constructive way.

### 10. Final summary 

The key message in this book:

**Asserting authority and making yourself heard don't require an aggressive approach. Indeed, your communication will be much more effective if you use Verbal Judo. That means avoiding condescension, empathizing with your interlocutor and paraphrasing and mediating when necessary. It also means being willing to identify and work on your communicative weaknesses. Once you're able to do these things, you'll be ready to communicate well in any situation.**

Actionable advice:

**Avoid abstract terms when communicating.**

Communication often begins with getting another person to engage with you. Achieving this usually requires that you make that person feel that you care for and respect her. There are a few ways to communicate this. In the case of elderly people, if you have their permission to do so, address them by their full name, for instance, Mrs. Smith. In the case of young people, go in the opposite direction, using just the person's first name.

Similarly, never use labels when speaking about people of different races or sexuality. Doing so will make you forget about their individuality and diminish the connection you need to succeed.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Thank You for Arguing_** **by Jay Heinrichs**

_Thank You for Arguing_ (2013) is a guide to the art of rhetoric. These blinks explain what rhetoric really is, how persuasion works and how to win a debate by drawing on in-depth research, anecdotes and theories from the great orators of history.
---

### George J. Thompson and Jerry B. Jenkins

George J. Thompson created the Verbal Judo method and its eponymous institute. He taught English and mastered karate before becoming a police officer, a career move that helped him develop his communication method. He died in 2011.

Jerry B. Jenkins is a best-selling author of fiction and nonfiction alike. Most notably, he co-authored the beloved _Left Behind_ books, a series of religious novels.

