---
id: 54a989016233320009260000
slug: virtual-freedom-en
published_date: 2015-01-06T00:00:00.000+00:00
author: Chris C. Ducker
title: Virtual Freedom
subtitle: How to Work with Virtual Staff to Buy More Time, Become More Productive, and Build Your Dream Business
main_color: 20A099
text_color: 1B8781
---

# Virtual Freedom

_How to Work with Virtual Staff to Buy More Time, Become More Productive, and Build Your Dream Business_

**Chris C. Ducker**

_Virtual Freedom_ is a fully fledged manual on the art of virtual outsourcing. It covers in detail all the things you'll need to consider while assembling your virtual team, while also providing solid advice on how to circumvent the common pitfalls.

---
### 1. What’s in it for me? Discover how to hire Virtual Assistants and free up your own time for real work. 

Being a business owner is a pretty demanding job: there is a team of staff to run, costs to keep an eye on, business strategy to decide and investors to meet. Finding time for it all is not easy, and even if this time is found, summoning up the energy to complete each task is just as difficult.

Luckily, the modern, technologically advanced world has provided a solution: Virtual Assistants. These are members of staff who work for you but are located elsewhere — in another city, state or even country.

If you hire a team of effective Virtual Assistants you will find yourself with the time to plan and lead your business. But if you get the hiring wrong, you could find yourself with even more work to do. These blinks show you how best to recruit, train and manage a team of Virtual Assistants.

In these blinks you'll discover

  * the best way of training someone in another time zone;

  * why you should hire Virtual Assistants for the role but not the task; and

  * the one job you simply cannot outsource.

### 2. Outsourcing your work to staff in other locations allows you concentrate on what matters. 

Life as a business owner in our modern world is not easy. Often, we're expected to be jacks of all trades, doing a myriad of jobs, each of which is time-consuming.

Before you can even think about spending some time creating innovative new products and services, or even selling those products and services, you have to first grind through a schedule packed full of other stuff: there's staff to be managed, checks to write, budgets to review, contracts with suppliers to go through, and the list goes on and on.

It's no surprise that so many struggle! In fact, the author's own attempts to manage his business led him to a complete burnout; he simply didn't have any time or energy left to succeed.

That's when he decided to do things a bit differently: he outsourced some of his work to _Virtual Assistants_ (VAs).

VAs are staff who do work for you, but who are not in the same location as you. They can take on a huge range of tasks, from accounting to office management, thus allowing business owners and leaders to concentrate on the work that they're best at and that matters most. By hiring VAs, they can enjoy what is known as _virtual freedom._

There are two main reasons why VAs present such a great opportunity:

First, the internet allows constant communication across thousands of miles between you and your VA. No matter how far apart you are, you can work as if you're in the same room.

Second, there's a huge pool of available talent. More than 42 million Americans currently work as self-employed consultants and freelancers — all of them are potentially available for part-time, full-time or even project-based work.

So, if you're struggling to keep afloat in a sea of tasks, virtual freedom may be your life raft.

### 3. No single VA can do all your outsourced jobs, and there’s one job you should never give them. 

Just as business leaders can't do everything themselves — no matter how hard they try — neither can VAs.

Simply put, you can't ask a single VA to do all of your outsourced work. While it's tempting to find one superhero VA to dump all of your work onto, the unfortunate truth is that we're all human, even your VAs.

While one VA might be able to do one or two tasks better than you, they won't be able to tackle _all_ of the work that you need to pass on.

Bearing this in mind, you should instead hire virtual staff for specific roles. For example, you might hire a _General Virtual Assistant_ (GVA) to manage the admin for your business. Your GVA is the one who excels at doing the little repetitive tasks, such as buying stationary for the office, organizing meetings or sending out memos.

Keep in mind, however, that there is one thing that you should never outsource — your _content_, i.e., what you sell to your customers.

Good content can be anything. If you run a business advice website, your content might be a free e-book detailing how your customers can build a blog to grow their business. Or you run a wine review blog, where your content might be videos in which you provide insights and advice for wine buyers.

Your content is what makes your business successful. Indeed, a great business is not the one that spends the most on marketing, or the one that makes it to the top of Google's search rankings. Rather, it's the one with the most engaging and relevant content for its customers.

So make sure that _you_ provide the content yourself. This way, you can ensure that it is of the caliber that your customers expect.

Luckily, since you've given your less important tasks to your VAs, you can devote loads of time for content creation!

### 4. Working with virtual staff is a learning process, but there are tricks to make it easier. 

Working with VAs may seem easy — obviously all you have to do is search for and hire the right people, right? Well, there's more to it than that. In fact, you're likely to make many mistakes on the path to the perfect VAs.

The author himself had to learn a few tough lessons through trial and error. For example, in the beginning he found it difficult to judge how much authority and independence to give his VAs. Some of them he gave too much, and they ended up working against his wishes. Others he gave too little to be able to do their jobs.

All this trial and error resulted in some powerful heuristics that you can use when finding your VAs.

First, never sacrifice quality for cost. Good VAs aren't cheap, especially for a start-up. An app developer, for example, costs up to $2,500 per month, and a GVA $900. Although it may seem prudent to hire the least expensive VAs, it's actually quite reckless.

Remember: these people are like your in-house employees, and you want the best work from them. In the real world, you wouldn't hire a cheap office manager with no administration experience, so why do it in the virtual one?

The same consideration applies to location. Overseas VAs will most likely be cheaper, and they can perform just as well as VAs from your home country. However, working in different time zones and dealing with cultural differences can become a burden.

Imagine how frustrating it would be to have to wait for six hours every day to get a reply from the person in charge of the videos for your website if you needed an _instant_ decision on when to upload a hot viral video. You may end up losing out because of the wait.

It's your call, but think about whether the reduction in price for foreign work is worth the added hassle of working across time zones.

> _"Rome wasn't built in a day — it also took more than one person to build it!"_

### 5. Hiring a VA requires just as much attention as hiring a normal employee. 

Even though you may never see your VAs face-to-face, they're still people. Thus, a lot of the things that hold true for hiring "normal" employees also hold true for recruiting VAs.

The main thing to keep in mind when you're hiring VAs, as with normal employees, is that you have to hire for the _role_, not the _task_.

Imagine that you need a VA to edit videos for your website. Before you go and find the very best video editor, think first about what _else_ you may want from your digital video VA in the future. For example, you may need them to upload the videos, or even shoot them.

In other words, don't just focus on their immediate task (digital editing). Think "big picture."

And although you don't have to understand everything your video editor does, you _do_ need at least a basic understanding to be able to make a good hiring decision.

But hiring is a task, like any other, right? Couldn't you just outsource this to another VA?

No. _You_ are in charge of developing your company, so _you_ have to be in charge of recruitment. So be sure to research the roles you're hiring and learn for yourself.

Furthermore, all good hires start with a good job description. You need to be very clear — both for yourself and for your job candidates — about what you want from your VA. The better the job description, the easier it'll be to find the right person to fulfill it.

A good job description is clear, precise and easily understandable. Use bullet points and be very clear about what you want from your VA, and what experience is required for the job.

Make sure that job interviews are personal. Always use something like Skype or Google Hangouts, and never rely on email. Only with the help of planned and high quality interviews will a good job description turn into a good hire.

But hiring is just the first step...

> _"After all, we're business owners — not web developers."_

### 6. It is not enough to simply hire VAs, you have to train them as well. 

No business leader can expect their new in-house hires to know exactly what to do without some sort of guidance. They need training, and the same holds true for your VAs.

There are three things that you should remember when hiring VAs.

First, as mentioned previously, you have to carefully define the role you are hiring for. If, for example, you're hiring a GVA to manage your office admin, then you'll need to take the time to tell them _exactly_ what is required of them. Don't make them intuit anything.

Secondly, your VAs can and _should_ ask for help whenever they have questions, especially at the beginning. Do _not_ allow your GVA to learn through trial and error! Make it clear that you expect them to ask when they need guidance, so that they get it right first time.

Finally, be aware of time zones. Training for someone living 12 hours away from you has to be well planned in order to be well executed. While you may find the best qualified GVA overseas, you don't want to be answering questions at 2 a.m.

These three tenets are good places to start, but it will take more than that to train your VAs well.

Be sure to mix up the types of media you use when training your VA, including online, audio, visual and traditional written training materials. Training can take a lot of time and attention, so developing solid training materials will save you work in the end. Once you've produced the best training resources, you can use them every time you hire a new VA.

Be aware of cultural differences when hiring overseas. For example, if you write your emails to your Filipino VAs in ALL CAPS, then they might interpret this as shouting, and will thus feel personally offended. This in turn leads to decreased productivity and resentment.

### 7. Once you have trained your VAs, your main task is to set up a management system to keep their work efficient. 

After the training is complete, you're that much closer to finally achieving virtual freedom. But this freedom will only come if your VAs are able to work in an organized way. In other words, you need a management system.

The best management systems revolve around FAQ sheets, or _IFTTT_ cheat sheets — the acronym stands for "_if that_ happens, _then this_ has to happen."

You can think of IFTTT cheat sheets as emergency evacuation procedures: in case of an earthquake or fire, you simply follow the procedure to get to safety.

IFTTT cheat sheets help your VAs understand what to do if, for example, your system crashes or they miss a deadline. With an IFTTT, they have clear orders about what to do next.

Additionally, they prevent you from having to micromanage your VAs. Virtual assistants are supposed to free up your time, so you don't want to spend all day checking up on their work. What's more, this smothering, hands-on approach will demotivate your VAs.

However, once your team of VAs starts to grow, then FAQ sheets simply won't be enough. In this case, you'll need a complete management system to keep track of what everyone is doing.

While management systems sound quite scary and complicated, they really don't have to be. Simply having a Dropbox folder or a Google Drive document where all the VAs fill in their progress on their tasks ought to be enough to keep track of everything.

Management systems are especially important for new hires. You'll want to track their progress in the beginning, and management systems allow you to do just that.

For instance, you can ask your new VAs to send you an email after every workday detailing what they've done. Not only does this keep up the communication flow, it also helps you to decide if this particular VA is right for you and your company. Once this has been established, then you can cut down the reports.

### 8. To motivate your VAs to do their best, make sure that they feel like part of your team. 

As you've learned, your VAs are regular people. VAs, just like your regular employees, like being part of a team. Consequently, it's important that you make sure that your VAs truly feel like they belong.

A good place to start is with fair pay. Pay them what they are worth according to their experience and above market value. Be sure to also take market conditions and location into account, as the going rate in certain countries is considerably less than in others where average incomes are higher.

And always pay on time, especially with your overseas VAs, many of whom are the sole providers for large families, whose well-being depends on their timely income.

Be creative with bonuses and benefits. You don't have to entice with high salaries; sometimes things like health insurance will also do the trick. And _always_ pay extra for work on holidays.

But good pay and benefits aren't enough. If you want your VAs to become a true team, then you'll have to involve them in your business.

Be sure to meet with all of your VAs regularly, at least via Skype, and give them the feeling that they're part of your larger vision. Give them regular evaluations, and ensure that they're getting feedback on their work. They want to know when they've done well and what they can do to become better, just like you.

Once your team is large enough, you may find that you're spending all your time managing your VAs and none of your time doing the things that you wanted to make time for in the first place. When this happens, you may need to outsource the actual management of your VA team.

At this point, it's time to consider hiring a Virtual Project Manager to oversee your VAs so that you can spend your time focusing on delivering high quality content and innovations to your valued customers.

### 9. Final summary 

The key message in this book:

**Virtual assistants give you the opportunity to hand over the routine work and focus on what you really love about being a business leader: creativity and innovation. But be careful! Simply hiring VAs won't cure all your problems; you'll need to be smart about it.**

Actionable advice:

**Figure out what's hogging your time.**

The next time you get a chance, make a list of all the specific tasks that you _have_ to do and which distract you from the "real" work. For instance, you probably didn't get into business so that you could revise budgets; you're probably in it to spread your great ideas and further innovate them. With this list, you'll have a much better idea of how many VAs you'll need to hire to free up your time for real work.

**Suggested further reading:** ** _Remote_** **by Jason Fried and David Heinemeier Hansson**

In _Remote,_ you are given an inside look at a new kind of work relationship made possible by modern technology, called "remote work." The book details companies' common fears about allowing employees to work remotely and in contrast, outlines the many benefits of remote work. Importantly, it offers practical advice to managers who employ remote workers or are considering introducing remote work options for their company.
---

### Chris C. Ducker

Chris C. Ducker is founder and CEO of three businesses, and is widely regarded as a leading authority on outsourcing. He is a popular international speaker, blogger and podcaster, and has written a number of e-books.

