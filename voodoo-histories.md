---
id: 59a27940b238e10005eeb721
slug: voodoo-histories-en
published_date: 2017-08-28T00:00:00.000+00:00
author: David Aaronovitch
title: Voodoo Histories
subtitle: The Role of the Conspiracy Theory in Shaping Modern History
main_color: FDF333
text_color: 7D7819
---

# Voodoo Histories

_The Role of the Conspiracy Theory in Shaping Modern History_

**David Aaronovitch**

_Voodoo Histories_ (2009) is a fascinating look at why we love to create conspiracy theories. Why do we feel the need to create stories to explain tragic events, such as the Apollo 11 moon landing and the deaths of Princess Diana and Marilyn Monroe? Read on and find out.

---
### 1. What’s in it for me? Step into the shady world of conspiracy theories. 

Have you ever heard of chemtrails? It's a conspiracy theory that believes the long, white trails airplanes leave after them as they fly through the sky are actually chemicals released by governments to control the public. Sounds pretty crazy, right? You'd be surprised, then, how popular the theory is, and it's far from the only crazy conspiracy theory out there.

So why do people believe in these theories?

As you'll see in these blinks, one of the reasons is that they satisfy our innate need to create order in a confusing and scary world. Plus, some conspiracies have turned out to be true! So how do you know if your crazy neighbor who won't stop talking about JFK's assassination is insane or holding onto the next big scoop?

In these blinks, you'll learn

  * what razors have to do with conspiracy theories;

  * why you can't put down _The Da Vinci Code_ ; and

  * why conspiracy theories can be comforting.

### 2. Conspiracy theories differ from conspiracies, but they do share some common traits. 

Maybe you've watched a movie in which the hero gets to the bottom of some mystery. After much field work, he reveals that some important individual didn't die in a car accident after all. No, it was murder — a conspiracy! Okay, so how does a conspiracy like this differ from a conspiracy theory?

An actual act of conspiracy is _not_ a conspiracy theory.

A _conspiracy_ happens when two or more people are involved in secretly plotting an illegal or deceptive act. Whereas a _conspiracy theory_ is just that — a _theory_ that the official explanation is not true, that there's a conspiracy afoot.

For example, one popular conspiracy theory is that NASA and the US government faked the Apollo 11 moon landing in 1969. But which is more likely: Thousands of people conspiring to create and cover up an elaborate hoax or NASA actually landing on the moon? 

This method of cutting through competing explanations in search of the simplest one is known as _Occam's razor_. It is one of the primary tools for analyzing the probability of a conspiracy theory.

One way conspiracy theories gain believers is by citing "evidence." Sometimes this comes in the form of celebrity endorsement, or from so-called experts with exaggerated qualifications. And these believers will work hard to present the evidence in a convincingly academic fashion.

Conspiracy theories also range in size, from small plots (like a few members of Buckingham Palace being involved in the death of Princess Diana) to massively elaborate ones, like the Vatican hiding the truth about Jesus's bloodline.

Furthermore, real conspiracies sometimes lead to conspiracy theories.

In the Middle East, there have been a number of legitimate conspiracies; this makes the area fertile ground for conspiracy theories.

For instance, the fact that Britain and France conspired to divide up the Ottoman Empire after World War I has led to new conspiracy theories, such as ISIS being an Israeli plot intended to disrupt rival powers.

In the next blink, we'll find out how conspiracy theories create the illusion of truth.

> _"Conspiracy theorists fail to apply the principle of Occam's razor to their arguments."_

### 3. Conspiracy theories create an illusion of truth through circular evidence and doubt. 

So how is it possible for conspiracy theories to create a false sense of believability?

For one, they often make use of _circular evidence_.

Circular evidence is created when two different people claiming to provide evidence cite each other in order to back up their statements.

We can see this in the case of Princess Diana's death: Jeffrey Steinberg, editor of _Executive Intelligence Review_, claimed the event was full of "troubling anomalies" and "disturbing questions." To support these claims he pointed to Mohamed al-Fayed, father of Diana's lover, Dodi al-Fayed, who was also in the accident. But Mohamed al-Fayed could only point to Steinberg, claiming, "the _Executive Intelligence Review_ is supporting my campaign to shed light on the truth surrounding the crash."

So, Steinberg relies on al-Fayed and vice-versa, creating the typical structure of circular evidence that lends no actual support to a conspiracy theory.

This lack of solid evidence is typical for conspiracy theories, which often begin with a shred of doubt and add speculative motives.

Let's look again at Princess Diana's death. The car that struck hers, a white Fiat Uno, mysteriously disappeared after the accident. This led to doubts about whether it really _was_ an accident. Did the car's disappearance point to a conspiracy?

Then comes the addition of speculative motives: Was Diana actually pregnant with the child of Dodi al-Fayed and was she killed to prevent a baby with a Muslim father from being born into the British Royal Family?

Some of these speculative motives can be harder to disprove than others. In this case, both the medical examiners and Diana's closest friend confirmed that Diana was not pregnant at the time of the accident.

So, why do intelligent and level-headed people continue to believe in such conspiracy theories? We'll take a closer look at that in the next blink.

### 4. Conspiracy theories stem from our desire for order and a world free from chaos. 

Since conspiracy theories tend to fall apart under closer inspection, why do we continue to be fascinated by them?

Well, conspiracy theories fulfill the very human desire for a world free of chaos by neatly explaining the random troubles we often face.

This is why conspiracy theories are often started by people who feel like social, economic or political outcasts. A good conspiracy theory offers an explanation for their situation without any of the blame being put on them. Instead, they blame their enemies for conspiring against them.

For example, in 1984, British socialists and pacifists had been losing elections for many years. That year, a campaign activist named Hilda Murrell was murdered, and a conspiracy theory quickly arose: the Thatcher government was behind it. To many, Murrell's murder was another instance of the government's oppressive political power.

We gravitate toward conspiracy theories like these because they give the world a sense of order.

Chaos is disturbing. By definition, it means we don't always have control over our lives. If something tragic happens, we look for a cause; it's less unsettling to think that some unseen power is behind the tragedy than to think that it's just a hard fact of life.

By removing uncontrollable factors like misfortune or chance and replacing them with forces plotting against us, such stories do give some comfort, however cold.

Conspiracy theories are also attractive because they tend to explain the world in a way that reflects our true feelings. In her book _Hystories: Hysterical Epidemics and Modern Culture_, Elaine Showalter suggests that conspiracy theories are "the external manifestation of repressed feelings."

We can see evidence of this in the trial of O.J. Simpson: Conspiracy theories suggesting Simpson was framed by the LAPD were embraced by many in the black community who felt oppressed by the authorities. These theories offered a way for people to vocalize their own feelings of mistreatment.

### 5. We like conspiracy theories that uncover a hidden truth, especially about famous people. 

So we've seen why conspiracy theories sometimes seem so persuasive. But what makes us like them so much? Interestingly, certain conspiracy theories seem to give us a sort of pleasure.

Many of us like conspiracy theories because they offer hope of uncovering the "truth."

Endless lies and constant cover-ups are anathema to humans. So we enjoy conspiracy theories that satisfy our desire to reveal these things, even if there weren't any lies or cover-ups.

Author Dan Burstein believes people enjoy reading books like _The Da Vinci Code_ because they tap into people's feelings of doubt, thereby resonating with readers. He suggests that people can sense that even if the plots of such books aren't true, they still might be onto something.

For example, the fictitious conspiracy theory about the Vatican in _The Da Vinci Code_ hits a nerve with readers, since the real Catholic Church _did_ conspire to cover-up decades of sexual abuse around the world.

Conspiracy theories also fascinate us when they involve famous people like Marilyn Monroe, John F. Kennedy or Princess Diana.

Sadly, the death of iconic people often becomes the subject of a conspiracy theory, as the public tends to be morbidly fascinated by the deaths of those who were admired and envied when alive. Sometimes our obsessions with celebrities can even be tied to someone's death; the sense of guilt we sometimes carry can cause us to focus on a conspiracy theory that shifts the blame.

For example, if our thirst for gossip played a role in the death of Princess Diana or Marilyn Monroe we may create a conspiracy theory to ease this guilt. Therefore, it becomes preferable to think that the CIA killed Marilyn Monroe.

> _"What is assassination, after all, if not the ultimate reminder of the citizen's helplessness — or even repressed murderousness?"_ \- Todd Gitlin, sociologist

### 6. Conspiracy theories can be reassuring in the face of tragedy, but they can also lead to tragedy. 

So, we've seen why we're attracted to the stories that conspiracy theories tell and why we're motivated to create them, but why do some stories prove to be more satisfying and successful than others?

For a conspiracy theory to be successful, it must, in some way, be reassuring.

Satisfying conspiracy theories offer an explanation that helps us make sense of things. They reassure us that things don't merely happen by chance, suggesting the government or some human agency is the cause of the problem. So a good conspiracy theory will make a case that some unseen power actually benefits, giving someone motivation for causing the event.

For example, in the death of Princess Diana, it is too tragic to think that she probably would have survived the car crash if she'd been wearing a seatbelt. So we develop conspiracy theories that suggest that the British government or Royal Family would benefit from the event. Even though the level of plotting required to make such a theory true is highly unlikely, it is much more reassuring to think that there was a conspiracy than to accept the tragic event as a series of random accidents.

Despite the fact that reassuring conspiracy theories can provide comfort, they can also be extremely dangerous.

Take the Holocaust, for instance.

After World War I, the Nazis found comfort in the conspiracy theory that was written into a fabricated document called _The Protocols of the Elders of Zion_. This theory claimed that a secret Jewish organization was conspiring to control everything and destroy all other world powers. The solution: eliminate the Jews.

This proves that sometimes conspiracy theories can be far from reassuring; in the wrong hands, they transform into propaganda and tools of hate.

### 7. Final summary 

The key message in this book:

**Conspiracy theories aren't very plausible. Most require an unreasonable amount of effort, coordination and staging in order to make them believable. Believing in them, however, can be comforting — or absolutely disastrous.**

Actionable advice:

**Think simple.**

The next time someone's pulling your leg or trying to convince you of the latest conspiracy theory, remember to use Occam's razor. Ask yourself: what is the simplest explanation? Chances are, the simpler it is, the more likely it is to be true.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _How Mumbo-Jumbo Conquered the World_** **by Francis Wheen**

_How Mumbo-Jumbo Conquered the World_ (2004) takes a detailed look at irrational tendencies and how they have come to pervade and pervert the modern world. These blinks walk you through bogus philosophies, from neoliberal political and economic dogma that predominated in the 1980s to New Age gurus peddling hollow advice and false hope.
---

### David Aaronovitch

Since the 1980s, David Aaronovitch has been an award-winning journalist for radio, TV and print. His first book, _Paddling to Jerusalem_, won the Madoc prize for travel literature in 2001, and his second, _Voodoo Histories_, was a _Sunday Times_ top-ten best seller.

