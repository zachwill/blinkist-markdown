---
id: 59a962b6b238e10005af7d01
slug: walden-en
published_date: 2017-09-06T00:00:00.000+00:00
author: Henry David Thoreau
title: Walden
subtitle: Life in the Woods
main_color: 8B8831
text_color: 59571F
---

# Walden

_Life in the Woods_

**Henry David Thoreau**

_Walden_ (1854) is the result of the two years Henry David Thoreau spent in the woods on the north shore of Walden Pond, a lake in Massachusetts. It is both a practical and philosophical account of how he sustained himself through farming and by building his own house, and what he learned about human nature by living a simpler life. Although it was a deeply personal experience, Thoreau's approach to society teaches us how we, too, can approach the modern world.

---
### 1. What’s in it for me? Venture into the woods to learn about simple living. 

Do you often feel sapped of energy from the hustle and bustle of life in the city? Are you sometimes frustrated by all the noise, traffic and rushed people that make up your surroundings? Henry David Thoreau had certainly reached this point — and this was back in 1845!

But unlike most people, Thoreau did something radical about it: he moved to the woods and settled into a home on the edge of a lake called Walden Pond.

These blinks explore how he managed to sustain himself at Walden Pond and what he learned about human nature through living simply in natural surroundings. You'll get an insight into both the practical and philosophical sides of Thoreau's two-year experience of a simple life and learn the finer things that a forest sojourn can open you up to.

You'll also discover

  * why devoting yourself to your work can make you a fool;

  * how so-called "savages" are actually very advanced when it comes to housing; and

  * which one of Thoreau's visitors had a habit of crawling up his leg.

### 2. Thoreau was concerned that modern life was offering little opportunity for gaining real wisdom and knowledge. 

It was in the spring of 1845 that Henry David Thoreau made his way to the wooded shore of Walden Pond, a lake in Concord, Massachusetts. The worries of the world weighed heavily upon him; he found modern life to be profoundly disturbing. To Thoreau, the approaching new era bled wisdom and freedom from his society. People were crushed under the servitude of work and had little chance of enjoying what life had to offer.

As he famously put it, "the mass of men lead lives of quiet desperation." They toiled and labored to make money to buy houses that were surplus to requirements and to fill them with useless knick-knacks.

Thoreau reacted forcefully. To him, such an existence was "a fool's life." It was a life devoid of meaning and wisdom, replaced with exertion and drudgery.

Part of the problem, as he saw it, was that people who worked so hard simply didn't have the time or energy to read. More people seemed to know the intricacies of accounting and bookkeeping than classic literature.

He was convinced that those who stopped reading in childhood were intellectually stunted, since there is so much to be learned from literature, especially for people who can read a work in its original language.

A favorite work of Thoreau's was Homer's epic poem _The_ _Iliad_, which became a source of comfort in his new environment. Reading, to Thoreau, could act as a guide and comfort. Perhaps you, too, can find volumes that breathe life and knowledge into your daily life or offer responses to the questions that loom over you.

Thoreau moved to Walden to show himself that there was more to modern life. And there is much for us to learn from his personal experience that we can still apply today.

### 3. To Thoreau, the move to Walden represented a return to the simple life. 

Thoreau was faced with one of two responses when people learned he was setting out to live in the woods at the edge of Walden Pond: they were either intrigued by the unusual decision, or felt he had succumbed to antisocial tendencies.

In fact, Thoreau had no intention of living as a hermit.

The relocation was a response to witnessing the fatigue of working people. It represented a chance for him to really live, and to absorb the wisdom of the simple life.

Thoreau's Walden routine meant boiling things down to the bare essentials so he could stay focused on more enlightened pursuits — and in doing so, devote himself to philosophical, spiritual, creative and artistic endeavors.

The simple life came down to four essentials: food, shelter, clothing and fuel for the fireplace or woodstove.

Thoreau was sure that living in Walden would allow him to easily meet these four demands. Consequently, he would have plenty of time for thinking and writing. He would finally be able to enjoy life.

For provisions, there was space at Walden for Thoreau to grow his own plants to both eat and sell.

For shelter, he would build his own house, as well as a small shed to store firewood.

Thoreau was well aware that farming, as well as building and maintaining the house, would be no small task. But unlike a mind-sapping day job, the work would be satisfying in its own right. It would also allow Thoreau to sustain himself and think freely and clearly, unburdened by the pressures of modern life — quite a reward in and of itself!

Thoreau was also keen to ditch modern concepts of clothing. There was no need to spend money on office clothes. Instead, for the kind of work Thoreau was now looking to do, all he needed was a set of good, long-lasting and functional clothing.

### 4. Building a house and tending a garden are both practical and informative. 

In Thoreau's view, the houses of his era were no longer constructed with function in mind, and instead acted as status symbols. And what a symbol it was when the average home could cost ten to 15 years' worth of wages!

Instead, Thoreau saw the wisdom of supposedly "savage" peoples, like the Native Americans, who kept their tent-like wigwams simple, functional and practical. They were easy to build and designed to withstand all types of inclement weather.

Thoreau saw value in the endeavors of these peoples. In contrast to "civilized" Americans who built boxy homes and left behind homeless multitudes, Native Americans had practical housing and no homelessness to speak of.

There are also advantages to building your own house. Yes, it's practical, but it's also an excellent way to learn about life in general.

Look at it this way: who learns more, the student listening to college lectures on metallurgy, or someone who mines metal ore, and then heats, shapes and hammers it to form a blade before fitting it to a handle? The latter, of course!

The same is true when it comes to growing food. Not only do you experience the satisfaction of eating the crops that you had once sown, but you also get to try your hand at gardening, all while learning about the virtue and benefits of patience.

It was hard work for Thoreau, but it was satisfying. To him, farming was a noble art and a sacred tradition; it had deep roots and tremendous complexity just beneath the surface.

> _"I never found the companion that was so companionable as solitude."_

### 5. At Walden, nature offered an immersive experience and an antidote to solitude. 

And so it came to pass that Thoreau, with the help of a few acquaintances, built a small house not far from the shore of Walden Pond. It stood 10 by 15 feet and had enough room for a bed, a desk, a small table and three chairs.

The house cost a total of $28 to build, an amount that, in 1845, would only have been enough to cover the cost of accommodation at the nearby Cambridge College for one year — and that would have been the cost of only the room!

Thoreau's new home provided him with just about the perfect spot to observe and listen to the nature around him.

Thoreau felt he could have easily spent the better part of the day joyfully listening to the calls of birds around his home; like Thoreau, they too were building their homes amongst the trees. To him, the birds' song swelled with joy. Nothing could have been a better reflection of his own feelings.

Within his home, he could hear squirrels scampering across the roof, or other creatures delving, probing and running about beneath the floorboards. Rather charmingly, a hare persistently bumped the floorboards as it hopped around below.

It was amongst this sonorous wash of animal life that Thoreau began to see his solitude in the woods as an immersive experience in nature.

And this was no wonder — his nearest human neighbor was about a mile away and, when looking out from his modest home, there were no other houses to be seen.

When he cast his eye out over Walden Pond at night, the reflection of the stars glimmered on the surface of the still water. In those gloaming hours, he was content, as if on a planet all his own.

Alone perhaps, but for him this experience was neither lonely nor an insufferable exile. The sounds of nature acted as a constant companion.

### 6. Thoreau's small house actually had a surprising number of visitors. 

Thoreau was by no means a loner. He made frequent trips into Concord, where he would purchase flour for baking bread or sell bushels of beans.

Callers came by the house too. But Thoreau often felt troubled when it came to accommodating these visitors. His quarters were tight and the environment was hardly ideal for serious discussion. He felt that big ideas needed a large space to unfurl and reveal themselves — and that was hardly possible when everyone was crammed inside his cabin.

Therefore, he found it far more fitting to, weather permitting, take the table and chairs outside. There, he and his visitors could debate and eat at ease and in comfort beneath the canopy of the trees.

A particularly favorite guest of his was a young Canadian lumberjack who had made his home nearby.

The logger often walked his dog past Thoreau's house in the mornings as he made his way to work.

Thoreau admired his simple and direct manner. It was refreshingly honest to encounter such a friendly presence who was disposed to independent thinking. Thoreau was delighted to learn that he, too, appreciated Homer's poetry. The young man was so noble and dignified that some of the Concord locals began to suspect that he was actually a prince in disguise!

Thoreau's other visitors included writer, poet and philosopher friends, as well as others whose curiosity had been stirred by Thoreau's experiment.

His hospitality was of a simple, rustic kind: he liked to share a humble meal, like a loaf of homemade bread, as they talked.

Thoreau didn't go out of his way to name anyone specific in his accounts, but it's quite clear that he wasn't living the life of an antisocial hermit. It might be easy to assume otherwise, but his rustic home appears to have been quite an active and pleasant spot!

### 7. Thoreau was never lonely amidst all the wildlife around him. 

Despite the comfort that human interaction brought, Thoreau's real neighbors were the animals that lived all around his home.

Thoreau had a particular predilection for the company of woodland mice.

Unlike the city mice Thoreau was used to, these mice weren't accustomed to human contact, though they soon got over that.

One mouse in particular was quite amiable to Thoreau. He used to scurry up the leg of his pants and onto the dinner table, where he would dine alongside Thoreau and cheekily share a bite of his supper.

A family of partridges had also made their home nearby. Thoreau was intrigued by these ground-nesting birds, which are normally quite shy creatures. But once they were acclimatized to Thoreau's presence, they began to take their constitutionals right in front of him!

Though they looked a little like chickens, Thoreau was sure he could see true intelligence in their eyes. He had to be careful while walking though, as he sometimes found himself mistaking young partridges for fallen leaves!

Thoreau was especially fond of simply sitting outside in a comfortable chair. Before long, all kinds of woodland creature neighbors would show up. It could be an otter, a raccoon, a family of wild cats or one of the many birds around. Thoreau's favorite was the loon, a highly intelligent bird that would plunge into Walden Pond looking for fish.

The red squirrel was another beloved neighbor. When winter came, Thoreau left some unripened corn in the snow, sure that an animal would make good use of it.

Who should pop up but a red squirrel, which put on quite the show inspecting the corn. Apprehensive at first, he looked on from the safety of his tree. But before long, he bounded down over the snow to examine the ear of corn and drag it off — and was he ever delighted when he got back onto his branch!

### 8. Winter provided some of the biggest challenges at Walden. 

On September 22, 1845, a month after the snow had settled, Walden Pond finally froze over completely.

In the freezing temperatures, when the snow was thick and the pond was solid, living conditions in the woods became much harder for Thoreau.

During that first November at Walden, Thoreau had to work quickly to ensure his chimney was built and ready before the harshest winter months arrived. He also plastered his walls to help keep the heat in and the icy winds out. By his second winter, he was able to procure a wood-burning stove to keep him warm.

The frozen lake also posed another problem: Walden Pond was Thoreau's source of drinking water. He had to break the thick ice just to have water to survive, which was no easy task. He had to regularly trudge through the snow to the lake, and before he could even begin cracking the foot-thick ice, he would have to clear all the snow off its surface first.

At least he wasn't the only one — he encountered ice fishermen out on the lake some early mornings.

When it came to wood to heat his home, though, Thoreau was lucky that there was so much quality timber around.

In fact, during the winter, his main task was to search for wood. To his surprise, he found that the dead frozen logs that had become waterlogged in the lake burned surprisingly well. Because they were so full of water, these pine logs burned for far longer than usual, and all the hot steam that emerged as they burned meant they became far hotter than he might have otherwise expected.

Thoreau was happy, not just because these logs were excellent fuel to heat his home; he also found that his home-cooked bread and meat seemed to taste sweeter when prepared over the wood he had recovered himself from the frozen wilderness.

### 9. Spring offered itself to Thoreau as a treasure of delights. 

After the long cold winter, the reward of spring was especially sweet.

In fact, it was because he knew that he would have a front row seat to watch spring emerge at Walden Pond that Thoreau had embarked on his experiment in the woods in the first place.

He could hear the thick ice on the lake crack and slowly melt back into the water. Soon after, a wondrous flow of muddy water cascaded down from the 20- to 40-foot banks that surrounded the lake. As the snow melted at the top of the slopes, the water gushed down in a channel of muddy sludge beneath the ice. It was like watching brown lava descending along the side of a volcano.

The first signs of spring had arrived at last, as the birds announced the end of winter.

By mid-March, bluebirds and red-winged blackbirds flocked about, truly beautiful sights to behold.

Then came the first sparrow of spring, and with him, everything seemed to bloom and flourish in a single moment. The verdant grass turned a darker shade of green; the oaks, hickories and maple trees burst into life, and the frogs sounded their chorus.

In the weeks that followed, the distinctive call of the whip-poor-will could be plainly heard, along with that of the brown thrasher. All around, the grass grew thicker.

Then, one particularly foggy morning, before the rising sun had burned the mist off the lake, the lonesome and haunting cry of a goose, far out on the pond, echoed out for a companion.

To Thoreau the spring was a powerful reminder of the endless cycle of life. Nature had returned to full vibrancy; he was part of it and felt as energized as the forest. To someone accustomed to city life, the metamorphosis had been truly revitalizing.

> _"Let us spend one day as deliberately as Nature, and not be thrown off the track by every nutshell and mosquito's wing that falls on the rails."_

### 10. Thoreau’s Walden sojourn is filled with lessons. 

Finally, after two years at Walden Pond, Thoreau's last day had arrived. It was September 6, 1847, and he began to wonder why there was any need to leave at all. But then it came to him: Walden was just one stop among many. He had only one life to live, and there would be other rich experiences waiting for him, that much was certain.

It was also clear that he'd learned a great deal during his two years there.

First of all, his time in the woods proved that if you simplify your life, you'll find that things become less complicated. You can start to live as what Thoreau called "a higher order of being."

Also, if you want to express yourself and expand your thoughts, it can be a great help to escape modern society. When stuck in a routine, it becomes easy to let your brain stagnate and be lulled into safe ways of thinking.

He also learned that there's no good reason to go rushing about and stressing over completing work when the only reward is money. It may seem odd, but life feels poorest when you are richest; in fact, seeking truth is a far more rewarding task than hunting riches or fame.

The biggest lesson? What the soul needs doesn't cost a dime.

Instead, a good rule of thumb to live by is to cultivate simplicity. Sell what you don't need, as this is sure to help you nurture your intellect.

Furthermore, materialism gets you nowhere. The novelty and the distracting qualities of modern life keep us from living truly fulfilling lives. If you're leading the kind of life where you feel you have to flash your cash at every opportunity, your day-to-day routine is bound to feel somewhat trifling or distracting.

As humans, we have the capacity for deep thought. This is, after all, what makes us human. Our lives should let us act thoughtfully and think ambitiously — and if we can manage it, maybe we can help others do the same.

A simple and self-sustained life surrounded by lush nature is a beautiful thing in its own right. And as we've seen from these blinks, there are clear and universal lessons that can be taken and applied from a minimalist existence.

### 11. Final summary 

The key message in this book:

**In 1845, Henry David Thoreau moved into the woods at Walden Pond, determined to live a life that allowed him to focus on the simple things, as well as his intellectual pursuits. His time in the wilderness was an experiment in sustainable and minimalist living that had fascinating results, and offers lessons that continue to be relevant today.**

Actionable advice:

**Be ambitious and move with confidence toward your dreams.** Don't assume your dreams are out of reach just because they might seem too ambitious or uncommon to someone else. Progress is made by those with ambitious ideas, so shoot for the stars and get to work on building the foundation that will allow you to turn that dream into a reality. Once you reach that goal, you'll find the success is even better than you imagined.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Vagabonding_** **by Rolf Potts**

A vagabond himself, Potts details his travel adventures in _Vagabonding_ (2002). Informed by firsthand experience, he outlines what to do and not to do in order to get the most out of hitting the road for the long haul.
---

### Henry David Thoreau

Henry David Thoreau (1817-1862) was an avid essayist, poet and philosopher. Thoreau spent much of his life exploring the relationship people have with nature, work and government. He is considered one of the leaders of the transcendentalist movement in nineteenth-century America.

