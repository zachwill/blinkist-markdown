---
id: 5519de0366616200073e0000
slug: walmart-en
published_date: 2015-04-03T00:00:00.000+00:00
author: Natalie Berg and Bryan Roberts
title: Walmart
subtitle: Key Insights and Practical Lessons from the World's Largest Retailer
main_color: 2260A9
text_color: 2260A9
---

# Walmart

_Key Insights and Practical Lessons from the World's Largest Retailer_

**Natalie Berg and Bryan Roberts**

In _Walmart_ (2012), authors Natalie Berg and Bryan Roberts share key insights and business principles from the company that reveal how it became the biggest retailer in the world. Examining the retailer's unprecedented success, the authors also discuss Walmart's future challenges.

---
### 1. What’s in it for me? Discover how Walmart became a market leader and what could threaten its position. 

Walmart is not just a retailer; it is a phenomenon. If Walmart were a country, its GDP would be greater than Norway's. The company is the world's largest commercial employer; and it manages just slightly fewer people than does the Chinese military.

Yet how did Walmart, a company with hundreds of billions of dollars in profits each year, secure its market position and its success?

These blinks put you ringside to watch the company's stratospheric rise, explaining how an efficient and masterful combination of technology, management and strategy made Walmart what it is today. Yet you'll see too that what goes up can come down — and that Walmart's toughest challenges are ahead of it. 

In these blinks, you'll discover

  * why during negotiations you should go canoeing with your enemy;

  * how strawberry Pop-Tarts helped Florida cope with a hurricane; and

  * why Walmart may in the near future either win big or fail miserably.

### 2. Walmart delivers low prices by increasing scale and ruthlessly focusing on efficiency. 

How did Walmart become the largest retailer in the world? There are two core principles responsible for the company's success: low prices and broad selection.

Let's start with price. Walmart's pricing policy is famous in its own right. The company's policy is called EDLP — Every Day Low Prices — and Walmart works hard to deliver on that promise.

But how do they manage to do so?

In a word, efficiency. Walmart is ruthless in taking every possible measure to cut its costs and lower its prices.

Here's just one example: In the early 1990s, deodorant was packaged in cardboard boxes. Walmart saw this as an unnecessary cost, as the extra packaging took up precious shelf space and wasted fuel in transport, making shipping more costly. So Walmart asked deodorant suppliers to sell their products without boxes. And as a result, the retailer saved 5 cents on every deodorant sold!

Scale also plays an important role in Walmart's commitment to low prices. Buying and selling higher volumes of goods allow the company to reduce prices.

This may seem counterintuitive, in that we typically assume that selling a product at a lower price leads to lower profits. But that's not necessarily true, as Walmart knows well.

Let's say you buy an item for 60 cents and sell it for $1, rather than $1.20. By pricing the item lower, you'll sell three times as many items. So although you're earning less per item, you earn more overall.

It's also worth noting that there's another advantage to scale. In Walmart's case, the larger it becomes, the more bargaining power it has with suppliers. Walmart has such market clout that suppliers simply can't afford not to place their products on the retailer's shelves.

Thus, suppliers sell to Walmart at lower prices, which allows the retailer to pass on the savings to customers.

### 3. Walmart’s coup was to recognize that rural communities were being underserved by retail outlets. 

Have you ever visited a Walmart store? You can't help but be astounded by the enormity of the store itself, the endless aisles of goods. A typical Walmart Supercenter carries around 100,000 products in a 54,000 square-foot space. In other words, Walmart's assortment of retail goods is without peer.

This is especially astonishing if you consider that Walmart first got its start in mostly rural areas of the United States. The company opened its first store in the small town of Rogers, Arkansas.

What sort of business strategy is that, opening in a rural outpost?

Walmart management realized that rural, often low-income areas were neglected by other retail chains. Conventional corporate wisdom assumes that unless there are 50,000 potential consumers in a general area, a big store won't turn a profit.

Thus rural areas were left dependant on smaller, local retail stores that carried a limited range of products and closed on Sundays, to boot.

But Walmart proved that a giant corporate-owned retailer could not only survive but thrive in rural areas. Walmart realized that customers would be willing to drive 70 miles or more to buy a significantly discounted lawn mower, for instance. By carrying a broad assortment of goods catering to every possible need, Walmart could reach customers who lived both near and far.

Walmart also introduced its own private labels, such as Sam's Choice, to sell alongside popular national brands. Private label products are essentially equivalent to well-known brands but are less expensive, as the costs of advertising and marketing are considerably reduced.

It's worth noting that although having a broad assortment of products benefits Walmart, there's a drawback to having too much choice. That is, customers might get overwhelmed and consequently buy nothing at all.

Sometimes less is more! For instance, when Walmart Canada removed two peanut butter brands from its shelves, sales of the remaining three brands rose.

### 4. Walmart eliminated middlemen and developed close, collaborative relationships with suppliers. 

Another factor that makes Walmart successful is its approach in working with suppliers.

Unlike many other retailers, Walmart doesn't work with middlemen. This policy came at the urging of Walmart founder Sam Walton, who disdained distributors.

Middlemen weren't creating anything of value, he reasoned, but were simply getting paid to sell products produced by someone else. Walton rightly surmised that by bargaining directly with manufacturers, the retailer could save money.

Thus Walmart developed its own supply chain capabilities and cultivated direct relationships with vendors.

In the early days, however, this wasn't easy. Walmart's negotiations with suppliers were ruthless, with each party fighting tooth and nail to get the better deal.

But this changed one day, as Walton was on a canoeing trip with friends. As it happened, the vice president of Procter & Gamble, a consumer goods manufacturer, had also been invited. Over the course of a friendly conversation, the two realized they had the same objective — that is, creating the best possible experience for the consumer — yet had different ways of achieving it.

They also discussed the fact that Walmart and Procter & Gamble were operating with basically zero mutual trust and barebones communication.

To change the situation for the better, the two managers created a Walmart/P&G team to improve communication and collaboration between their companies.

These kinds of close supplier relationships eventually became the rule and not the exception for Walmart. Even though the retailer never fully abandoned its ruthless negotiation style, Walmart has expanded its relationships with suppliers, participating in product development, test marketing, package design and data sharing.

Ultimately these collaborative relationships have led to increased efficiency and lower prices. For example, Walmart shares data on item-level sales with General Electric, so that the supplier knows exactly how many products to deliver and ship to Walmart every day.

> _"Our biggest single advantage globally is our supply chain. We still kick ass in logistics!"_

### 5. State-of-the-art technology has enabled Walmart to manage a logistics empire as well. 

Walmart's eventual logistics mastery started with a simple problem. Since its first stores were located in rural, isolated areas in the southern United States, many distributors found it unprofitable to deliver goods to Walmart stores.

But Walmart made a virtue out of necessity: If no one wanted to deliver to its stores, the company would do it instead, setting up its own distribution centers, complete with a fleet of trucks.

Today, the scale of Walmart's logistics empire is unprecedented. It has more than 40 regional distribution centers, each over one million square feet. And each distribution center works around the clock to deliver goods to between 75 and 100 stores within a radius of 250 miles.

What's more, it takes some 85,000 employees to keep the entire logistics train moving along.

You might wonder: how on earth can Walmart manage such an operation? Well, it all comes down to technology. Walmart from the start embraced state-of-the-art technology and supply chain innovations.

In 2006, Walmart introduced the _Remix_ project to improve merchandise flow and availability rates. In the past, Walmart had two different distribution networks: one for dry groceries, like cereal, and another for household merchandise, like paper towels. Both of these networks however included fast-selling items and slow-moving ones.

Yet Walmart realized that the slow-moving products were gumming up the works, preventing other faster-selling products from getting to shelves quickly, possibly leading to losses in sales.

So Walmart set up High Velocity Distribution Centers for fast-moving goods in any product category. When a store is aware that a product is running low, it sends data to the closest distribution center. Trucks are packed with the required goods and brought immediately to the Walmart shelf, with no time lost.

> _"Technology has been an enabler and facilitator of change throughout Walmart." — Don Soderquist, former senior vice chairman_

### 6. Real-time data mining keeps Walmart’s shelves stocked and even anticipates future needs. 

Walmart's focus on technological innovation has put it ahead of the competition, as it has made the retailer much more efficient, therefore enabling better service and lowering prices.

The company's technology tracks every single item in its stores. And when shelves run empty, a computer automatically alerts a warehouse that an order needs to be placed and shipped.

Importantly, this system generates unparalleled amounts of data, enabling astounding real-time inventory visibility, data which has an immediate impact on merchandise planning.

Here's a fantastic example. When Hurricane Ivan was about to hit Florida, Walmart's algorithms predicted there would be high demand for Kellogg's Strawberry Pop-Tart toaster pastries. The retailer's computers notified distribution centers and the products arrived in time to allow Florida families to stock up on Pop-Tarts before the storm.

Yet the retailer doesn't simply keep its data to itself to benefit, but openly shares some information with its suppliers.

This way, suppliers not only know which products sell well but also can pull accurate information about which items sell well on which days and in which stores. This enables suppliers to plan their production more efficiently and better coordinate inventory levels with Walmart.

With this sort of streamlining, Walmart can reduce not only waste but also the labor costs associated with storing inventory, which all translates to cheaper prices for the end customer.

Tight relationships with loyal suppliers. An end-to-end logistics empire. State-of-the art technology. These are the reasons why Walmart is at the top of its game.

But what's ahead for the retailer? Read on to find out.

### 7. To reach its full potential, Walmart needs to reach more city dwellers and international shoppers. 

Walmart may be the biggest retailer in the world, but the company is still far from reaching its full potential.

There are still two large categories of potential customers to which Walmart hasn't successfully reached out: residents in U.S. cities and international shoppers.

As we mentioned earlier, Walmart got its start in rural, lower-income areas. To grow, Walmart needs to expand to urban centers, especially as there are many underserved urban areas that lack access to fresh, affordable groceries, for example.

Since some 23 million consumers live in such areas, the strategy holds huge potential!

And while Walmart plans to open 300 stores in urban areas by 2016, success is not guaranteed. As of today there is not a single Walmart in New York City. And it's easy to see why: Walmart Supercenters are gigantic, and the real estate costs alone would be astronomical!

There is a simple solution, however, which is to make the stores smaller. And that's precisely why the company introduced Walmart Express in 2011. At just 15,000 square feet, these stores will still be able to offer three times the product selection of an ordinary grocer.

The company is also taking seriously its global expansion plans. Walmart International is already active in 26 markets, making it the third-largest retailer in the world, behind Walmart U.S. and Carrefour.

But there's still more that the company could do to reach out to some 4 billion untapped global customers. The company's long-term potential, especially in countries like China, India and Brazil, is huge.

For now, Walmart International has entered such markets but doesn't yet dominate them. In the future, however, Walmart International might surpass its U.S. counterpart as the biggest retailer in the world.

### 8. Amazon, with even lower prices and more choice, may beat Walmart at its own game. 

How to reach out to new customers isn't Walmart's only challenge. A more serious challenge already looms: analysts expect Amazon to overtake Walmart as the world's biggest retailer by 2024.

There are two reasons why this is a possibility. First, Walmart has criminally neglected e-commerce. And second, Amazon is beating Walmart at its own game when it comes to price and assortment.

Walmart's online sales today hover near $6 billion, which is small when compared to Amazon's $34 billion in annual sales. The question is, how could Walmart miss such an obvious source of revenue?

In part, Walmart has focused its attention on expanding its Supercenters, which in general perform quite well.

But there's also another reason for Walmart ignoring the internet. A decade ago, when the retailer tried experimenting with online technologies, its core customers weren't interested.

In 2004, Walmart launched an online music store to compete with iTunes. However, since the typical, low-income Walmart shopper didn't yet have internet access, the project was a flop.

So Walmart has its reasons for coming late to the online game. But that's not the only factor at play. The second reason Amazon may surpass Walmart is because it's already surpassing the retailer at its core strengths, offering lower prices and a broader assortment of products.

Consider this example: when Wells Fargo compared two baskets of goods in 2011 — one purchased at Walmart, the other on Amazon — the financial services company found that the basket from Amazon was nine percent cheaper, even when factoring in shipping costs.

Another comparison found that when it comes to assortment, Walmart "only" offered 96 different types of camcorders, whereas Amazon offered 2,016. And this trend holds true more generally, in that the online retailer offers approximately 14 times more products than Walmart!

In other words, Amazon is not only a fierce competitor but also a worthy foe.

### 9. Final summary 

The key message in this book:

**If there's a single secret to Walmart's success, it's that the company is ruthlessly efficient in all matters. Whether maintaining a broad mix of products, managing logistics, holding negotiations with suppliers or mastering state-of-the-art technology, efficiency is Walmart's key organizing principle, all for the sake of offering customers the lowest prices possible.**

Actionable advice:

**Be cheap!**

Walmart executives are frugal and have to follow certain rules to keep costs down. For instance, when a Walmart executive travels to meet a supplier to arrange a new purchase, the trip's costs have to be one percent less than the money earned on the deal.

**Suggested further reading:** ** _Built to Last_** **by Jim Collins**

_Built to Last_ examines 18 extraordinary and venerable companies to discover what has made them prosper for decades, in some cases for nearly two centuries. This groundbreaking study reveals the simple but inspiring differences that set these visionary companies apart from their less successful competitors.

_Built to Last_ is meant for every level of every organization, from CEOs to regular employees, and from Fortune 500 companies to start-ups and charitable foundations. The timeless advice uncovered in this book will help readers discover the importance of adhering to a core ideology while relentlessly stimulating progress.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Natalie Berg and Bryan Roberts

Natalie Berg is global research director at Planet Retail and a consultant for companies that want to either work or compete with Walmart.

Bryan Roberts is the director of retail insights at Kantar Retail, and spent ten years working as a Walmart analyst.

Original: ©Bryan Roberts and Natalie Berg 2012. This summary of _Walmart 2012_ is published by arrangement with Kogan Page.

