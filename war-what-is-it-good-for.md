---
id: 5508683f336261000a740000
slug: war-what-is-it-good-for-en
published_date: 2015-03-18T00:00:00.000+00:00
author: Ian Morris
title: War! What Is It Good For?
subtitle: Conflict and the Progress of Civilization from Primates to Robots
main_color: ED4B3E
text_color: ED4B3E
---

# War! What Is It Good For?

_Conflict and the Progress of Civilization from Primates to Robots_

**Ian Morris**

_War! What Is It Good For?_ takes a look at the history of conflict and comes to a startling conclusion: while wars are horrible for those who endure them, postbellum societies enjoy the positive consequences of war, namely peace, prosperity and organization.

---
### 1. What’s in it for me? Discover the ways in which war has helped society in the long term. 

Ernest Hemingway once wrote, "Never think that war, no matter how necessary, nor how justified, is not a crime."

These blinks argue against Hemingway. While Morris agrees that war is awful for anyone involved, he claims that in the long run, war has led us to our current unprecedented period of peace. They argue that, in fact, it was only through war that we reached this moment, with the lowest amount of violent deaths in the history of the human race!

Disagree? See if these blinks can change your mind. After reading them and looking at the broad arc of history, you'll see war's influence on what we both love and hate about the world.

In these blinks you'll find out

  * why there may never be another major war;

  * what would've happened if Hitler and the Nazis had triumphed; and

  * why bigger states want peace.

### 2. War brings new technological innovations. 

Death. Destruction. Desperation. These are among the things that war brings us. How then could anyone consider war to be anything other than pure hell?

It's easy to look at the bad at the expense of the good. For example, while war has brought incalculable misery to millions of people, it is also deeply connected with innovation.

Indeed, military needs are directly responsible for some of humanity's most important innovations.

One way to win the upper hand in a war is by gaining a technological advantage. Throughout history, military technological advancements have equated to the end of war, to victory, and to continued innovation, based on the technology first created for wartime purposes.

Practical things, like bronze and iron, are the results of military research. Iron, for example, was invented very early in human history, and in 1200 B.C. technology made it possible to produce iron in larger quantities.

Its mass production and wide distribution resulted not from civilian application, but because it was a better material for weapons and armor than bronze.

What's more, many basic inventions and ideas underwent further development as a result of war. These ideas, originally developed for other purposes, were propagated and expanded upon as a result of their wartime application.

Gunpowder, for example, was invented by Chinese alchemists and first used only for small fireworks. It was when the military picked it up that new and better recipes were developed.

Competition among warring nations spins off constantly evolving technologies.

For example, when large ships were employed to transport valuable goods, rival nations and pirates developed smaller, quicker ships to rob the traders. This in turn led to the creation of even larger and better armed trading ships, which led to even faster rival ships, and so on.

While technologies made with noble intentions find ignoble applications in war, the opposite holds true as well.

### 3. Productive wars create large empires. 

Wars don't just produce novelties, such as gunpowder. Sometimes their developments are much larger. In _productive wars_, entire nations and states can be erected, which eventually lead to more stable societies with less violence and more trade.

Human history is full of conflicts that favored larger groups. When we abandoned our hunter-gatherer traditions in favor of settled agriculture, we began inhabiting well-defined territory. Soon good farmland became a rarity, and our growing population was forced to fight over resources.

The larger groups, having won these wars over resources, assimilated their former adversaries into their own ranks, thus making them part of their culture and empire. Faced with the choice between assimilation and death, the defeated groups adapted to their new adoptive culture.

Imagine being part of a wandering tribe of Germans during the time of the Roman Empire. Your tribe wants to settle inside the territory of another hostile tribe. So what do you do?

There are several approaches and outcomes: your tribe could fight to conquer your rival and assimilate their culture. You could be defeated and assimilated yourself. Or perhaps your tribes would join forces to defeat Rome's imperial ambitions, or be defeated.

All these outcomes have one thing in common: in the end, one group becomes larger than it was before.

Consider how many great empires were formed through war: the Roman Empire, the Han Dynasty and the Mauryan-Empire in India each stretched over 1.5- to 2-million square-miles. And let's not forget the British Empire, whose conquest through brutal colonial wars created an empire "on which the sun never sets."

As we will soon see, these huge empires had positive consequences for the rest of the world.

### 4. War is the only way to join societies together. 

With all the death and destruction brought by war, we have to wonder if there could have been another way to achieve progress, without all the bloodshed. Sadly, no.

In fact, violence and war are rooted deep in our nature. Our close biological relatives act with extreme violence. Groups of chimpanzees in Central Africa, for example, regularly use violence against rival groups to secure resources and territory, thus suggesting that violence is deeply embedded in our DNA.

Anthropologists also note higher levels of violence within indigenous cultures. For example, when researcher Napoleon Chagnon went to South America to study the Yanomami people in 1964, he expected to find a group of peaceful people living in harmony with nature. Instead, he found a society that was as violent and war-driven as any other.

Human history provides few examples of peaceful unification of states, and even those examples often relied on the threat of violence.

In those rare cases when large nations have worked together or tolerated opposition, their cooperation largely resulted from fear of a greater common enemy. The British Empire, for instance, only tolerated the competition of an independent United States out of fear that an open conflict could end badly for them.

Even our free markets rely on the threat of violence. Many people believe in the power of the invisible hand, i.e., that humans motivated by self-interest in the economic marketplace ultimately lead to a common societal good. However, a market can only function when a greater authority ensures that everybody abides by the rules — that no one, for example, steals or cheats.

It's an _invisible fist_ — the authority and threat of penalty — that ensures the invisible hand can do its job.

So, while states indeed do work peacefully together and trade with each other, these friendly relations are only possible due to the looming threat of violence that compels those involved to be fair and abide by the law.

### 5. War forces states to get organized. 

War has yet another, more subtle societal benefit: it forces states and leaders to improve their organization and administration.

Professional armies require large amounts of funds, which can only be raised by well-organized states. For a state to raise money via taxation, for example, it must first have a functioning bureaucracy, that creates and runs complex governing organizations.

Unable to organize both its troops and subjects, a fighting nation will fail early on.

Warring nations are constantly driven by what's called the _Red Queen Effect_, named after the Red Queen in Lewis Carroll's _Through the Looking-Glass_ : no matter how great their inventions, organization and fundraising, warring nations will always be in competition with others whose innovations match or trump their own. This competitive environment spurs tremendous development.

Looking to the history of Europe, for example, we find that European states were engaged in one war or another for nearly the entirety of the Middle Ages. In that time, every military innovation from one state was countered by new methods of defending against it, meaning constant innovation.

Better cannons, for instance, were countered by better castle walls. The incessant wartime advancements in Europe meant war lasted for centuries, only to culminate with unclear victors. But when European states sought to colonize other parts of the world, they had a tremendous military advantage.

### 6. A big and organized state causes violence to fall. 

We've seen the effects that war has on the organization of nations and states. But how does war affect the people who actually _live_ within the nations' borders? How can war be good for the people?

Nations and states use violence to ensure safe roads and prosperous trading. Consider, for example, that one of the biggest sources of wealth and tax revenue is trade. Thus, it's in states' best interests to maintain peace and order for undisturbed commerce.

The Roman road system is a prime example. The roads connected the entire Empire and allowed quick travel; and they were safe — well-maintained and guarded — because the state had an interest in maintaining safe and expeditious trade.

A self-propelled pattern emerges. Violence suppresses trade; when a state organizes itself to successfully defeat smaller factional violence, trade increases. Increased trade ushers in new prosperity. With more wealth, opportunities to create wealth, and the resulting higher standards of living, people are less prone to violence.

Larger empires then have the ability for a wide variety of goods and services to enter the marketplace, in addition to standardizing currency and trade laws — all of which make commerce easier, more appealing, and potentially more lucrative, reducing the motivations behind violence.

Thus is the paradox of war: violent wars expand nations, and force them to organize if they want to have a chance of surviving. In turn, these big, organized states have the means and motivation to quell violence and foster safety.

Our final blinks will look at common objections to these claims.

> _"Incorporation into a bigger society, gradually...makes conquered and conquerors alike safer and richer."_

### 7. There are forms of counter-productive war, but in the long run, war is productive. 

The most obvious objection to the previous blinks is that not all wars are productive. Just look at World War II, which resulted in the deaths of 50 to 80 million people.

Looking further back in history, specifically from 200 to 1400 A.D., there was a huge cycle of productive and _counter-productive_ wars, i.e., wars that inhibited growth or stability.

The source of counter-productive wars are often military campaigns that favor small groups over big armies, such as the early equestrian troops which gave the small and mobile forces of the Huns an advantage over the slow, mostly stationary armies of the Han Empire.

Counter-productive wars resulted in the destruction of the great Roman, Han and Mauryan empires. Along with their defeat came pure chaos.

Winners of counter-productive wars weren't interested in building a new empire. Rather, they plundered everything they could, destroyed entire villages and killed all opposition. Then they left without establishing structures or institutions of their own.

But even the worst of the modern wars, such as waged by Hitler and the Nazis, led to productive outcomes.

Consider, for example, how nations come together to defeat their common enemy, as the Allied powers did in World War II, and thus create opportunities for productivity.

Even if the Third Reich _had_ won the war, Hitler's successors would have faced a choice: open the borders, commence trading and adjust policy to maintain global peace, or struggle against the competition of bigger states with better policies, and eventually collapse. Either way, their empire could not remain isolated from the rest of the world forever.

While there may be wars that are purely destructive, in the long run the positive effects will always prevail.

### 8. On the whole, violence is decreasing. 

We've shown why war is good _in theory_, but do these claims measure up to hard facts?

All in all, violence has been declining. In 1250 an average of one out of every 100 people died a violent death. By 1590 it was down to one in 300, and in 1950 in was one in 3,000.

So, the reasoning is that wars, though gruesome in and of themselves, cause a decrease in violence and violent deaths because they lead to more highly organized, lawful, and prosperous societies.

During the Stone Age, when violence and murder had no formal repercussions, the likelihood that you would die a violent death was ten times higher as in the twentieth century, perhaps even as high as 20 percent!

Even in cases in which empires fall and the rates of violent death rise, these fallen empires are always replaced by a larger, more stable one which further reduces violence.

What's more, the organized states created by war increase the incentive for cooperation and make violence a more costly alternative.

A stable state punishes violence and conflict while rewarding cooperation through wealth from trade. Because cooperation brings the more desirable results, more and more people turn away from violence.

Again, this is simply the result of the state's own self-interest: it's much easier to govern and tax subjects who are less inclined to violence.

As our societies become more organized and less interested in violence as a way to increase our lot in life, we have to ask: will there still be war if we are less inclined to act violently?

> _"War remains the lesser evil, because history shows that it has not been as bad as the alternative."_

### 9. It’s possible that the future will be one free of war. 

All this talk of war leaves us wondering what the future of war will look like. However, it's possible that war will eventually be nothing more than a distant memory.

At least in part, the abolition of war could be brought about by new technological inventions. Some computer experts believe that in the future it's possible — and perhaps inevitable — that we will upload our minds to computers and virtually link them together, thus creating one huge superorganism.

If we accomplish this, there will be no need or desire for violence, as we would all be part of one great community and network.

More pragmatically, our modern weapons have made the cost of war too high. Calculating whether a war is feasible or desirable essentially amounts to a numbers game: opponents calculate the costs and benefits of war and peace, and make a decision based on those perceived outcomes.

New weapons, such as our vast global nuclear arsenal, drive the potential cost of a confrontation up to infinity, making peaceful solutions suddenly seem much more attractive.

We've seen what the atomic bomb can do, and nobody since the World War II has dared to pay that price.

Finally, the violence inherent in us exists alongside many peaceful aspects as well. Even some of our close ape relatives, like the bonobos, favor a less aggressive society, that is — not coincidentally — matriarchal.

The next forty years, likely marked by severe changes in geopolitics, will prove to be the most important in human history, as they will show whether we've managed to overcome the need for war or whether we return to a long and violent circle of productive and counter-productive wars. In what seems to be its greatest paradox, the greatest thing about war might be that it makes itself obsolete.

> _"If chimpanzee wars suggest that humans might be natural-born killers, bonobo orgies suggest we could be natural-born lovers."_

### 10. Final summary 

The key message in this book:

**War is a terrible thing. It brings death, destruction and unparalleled sorrow. Yet, we owe a great deal of today's relative peace, prosperity and comfort to the wars of years past. Although destructive, war is also a force that creates the organization and cohesion necessary for peace.**

**Suggested further reading:** ** _Brave New War_** **by John Robb**

Modern technology and globalization have made it possible for one man to wage war against an entire country and win. Although it might seem unbelievable, it's not.

Technological advances like the Internet have made it possible for groups of terrorists and criminals to continuously share, develop and improve their tactics. This results in ever-changing threats made all the more dangerous by the interconnected nature of the modern world, where we rely on vital systems, like electricity and communication networks, that can be easily knocked out. _Brave New War_ explores these topics and gives recommendations for dealing with future threats.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ian Morris

Ian Morris is a British historian, archaeologist and Stanford professor who has written a number of critically acclaimed books, such as _Why the West Rules — For Now_ and _The Measure of Civilization_.

