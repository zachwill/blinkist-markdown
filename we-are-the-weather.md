---
id: 5db094556cee070008d0b9cd
slug: we-are-the-weather-en
published_date: 2019-10-25T00:00:00.000+00:00
author: Jonathan Safran Foer
title: We Are the Weather
subtitle: Saving the Planet Begins at Breakfast
main_color: D9CCC0
text_color: 8C5F35
---

# We Are the Weather

_Saving the Planet Begins at Breakfast_

**Jonathan Safran Foer**

_We Are the Weather_ (2019) is a rigorous investigation of climate change, what it means and why humans seem so powerless to tackle it. Jonathan Safran Foer argues that while climate change is terrifying and hard to understand, there is a very simple action that we can take: By leaving out meat and animal products for breakfast and lunch, we can make a huge contribution to the health of the planet.

---
### 1. What’s in it for me? Get a new perspective on what’s causing climate change and learn the most effective ways to fight it. 

The vast majority of us accept that climate change is real. We've all read the news reports and seen the alarming headlines. We know that there are frantic conversations about time running out. But how urgent is it really, and what can we do about it? 

In a sense, we know both too much and too little. We're overwhelmed with facts but we don't have a clear idea of what they mean for our lives. So it becomes difficult to take any action at all. 

In these blinks you'll learn why the story of climate change is so hard to grasp, and why we're given such contradictory information about how to deal with it. You'll learn about a major source of pollution — industrial animal farming — which has not received the attention it deserves in environmental debates. And lastly, you will learn effective and achievable strategies for fighting climate change, one veggie burger at a time. 

In these blinks, you'll learn

  * why vegetarianism isn't the most environmentally sustainable diet; 

  * how small protest actions can lead to huge social transformations; and

  * why industrial farming is one of the biggest polluters in the world.

### 2. Climate change is not an interesting or believable story, so people aren’t motivated to fight it. 

If you think about major political movements like the civil rights movement or the fight to end apartheid in South Africa, your mind will latch onto a memorable story. Like Rosa Parks refusing to go to the back of the bus. Or Nelson Mandela walking out of jail after 27 years and instantly forgiving his oppressors. These stories have heroes, and villains. They also have a clear timeline: we know when the fight started and when the war was won. 

The story of the fight against climate change has no such clarity. That's because the effects are felt all over the world in so many different ways. We're told that a hurricane in New York or the submergence of an island in the Pacific are all related to this thing called _climate change_, but exactly how and why they're connected remains unclear. 

The fact that the story of climate change is so vague and complex makes it difficult for people to engage with. Researchers from the Cowles Foundation for Research in Economics at Yale University found that when people were able to clearly visualize the victim of a tragedy, they were more likely to sympathize and donate money to them. Millions are affected by climate change and hundreds of millions more people will be affected in the future. This means there is no specific victim that people can relate to. Subsequently, we find it harder to feel emotionally invested in the story. 

The idea of hundreds of millions of people being affected by climate change is not only abstract, it's also terrifying. These kinds of statistics and predictions make climate change feel almost too enormous and terrible to be true. 

Our inability to comprehend climate change is similar to people's reactions to the Holocaust during World War II. Few had ever encountered a situation of this particular scale and terror before. That meant that even when they heard eye-witness accounts testifying that Jewish people were being murdered in extermination camps, they struggled to believe it. 

Even though the story of climate change is vague and alienating, we have to find ways to engage with it — rationally if not emotionally. This is a story we can't afford to ignore.

### 3. We’re not wired to respond to abstract threats like climate change. 

Imagine your child is playing on a climbing frame. They lean out too far and look like they're about to fall off a two metre ledge. What do you do? Chances are you dash towards the climbing frame with a speed you didn't know you possessed! Now imagine that someone tells you that something called climate change could affect your child's life to an unimaginable degree at some stage in the future. What do you do? Chances are, you will do nothing at all. 

Humans have evolved over millions of years to be able to deal with threats to their survival. We're wired to deal with threats which are right in front of our faces, though, like children falling off climbing frames. We're not designed to respond to abstract things which will affect us in the future, like climate change.

Experiments have, in fact, shown that humans are bad at even visualizing what the future might look like. In a study conducted by UCLA psychologist Hal Hershfield, participants were asked to imagine their lives ten years into the future. fMRI scans showed that when they were talking about their future selves, their brain activity was the same as if they were talking about strangers. 

In order to respond to climate change, we need to have the ability to project our minds into the future and grapple with what will happen on Earth if the destruction of the planet continues at the current rate. That is something that we're just not equipped to do. 

Even more problematically, we aren't that good at remembering the past. Part of the human design is to be very adaptive to change, so much so that we often don't realize that things _are_ changing. So we just accept that Europe has extreme "hottest ever" heat waves every summer, and we get used to the fact that hurricanes are bashing coastal cities with startling frequency. 

Being so adaptive and responding to immediate threats has helped us to live successfully on Earth for thousands of years. However, to deal with climate change we need to develop a different set of skills.

### 4. We’re given confusing, misleading information about how to prevent climate change. 

Even if you're extremely motivated to fight toward preventing climate change, it's hard to know where to begin. It doesn't help that a lot of the information we're given is incorrect or incomplete. 

Let's start with incorrect information. There is evidence that major oil and gas companies, such as Exxon, already knew about the dangers of global warming in the 1950s, an incredible 70 years ago. However, the industry responded to this knowledge by creating a disinformation campaign and publishing false accounts that minimized the risks. 

As a response, contemporary environmental campaigns have focused almost entirely on the dangers of fossil fuels and how drilling for oil is destroying our environment and contributing to climate change. It's true that fossil fuels are a major environmental hazard, but focusing on this almost exclusively is also misleading, as it provides an incomplete picture of what's really going on. 

Agriculture accounts for 24 percent of harmful greenhouse gas emissions, almost as high as fossil fuels, which accounts for 25 percent. Most of these agricultural emissions are the result of industrial animal farming, which supplies the modern world's tremendous demand for meat. However, environmentalists have paid very little attention to this problem in their discussions of the climate crisis. For example, the famous documentary _An Inconvenient Truth_ by former US Vice Presidential candidate Al Gore, completely ignores it.

Why is that? Suggesting that people eat less meat is so controversial that activists like Al Gore fear that bringing it up will completely alienate people from the environmental movement. So they choose not to mention it. 

However, talking about climate change without mentioning meat means that people don't know about one of the easiest and most important actions they can take to combat the issue. 

Furthermore, we're encouraged to engage in activities that are time consuming but have negligible positive effects on lessening climate change. For example, we're told that using hybrid cars, recycling, planting trees and eating organic food will help the environment. These actions actually have a lower impact on the environment than cutting down on our consumption of animal products. 

So, how do we figure out what to do with all of this contradictory information? We need to look critically at the advice we receive, and make sure that the actions we take will have an effect instead of just making us feel like we're contributing.

### 5. Fighting climate change will require both “top-down” and “bottom-up” activism. 

What can we as individuals possibly do to fight climate change? Faced with such mammoth opponents as Big Oil, it can feel like anything we do will be useless. What can we achieve by waving a sign at a protest march? 

It's understandable why people feel these doubts. After all, it's now popular to say that individuals cannot fight against climate change, because it is the corporations who are responsible for the vast majority of the pollution that is causing it. However, that is a simplistic way of looking at it. Corporations, too, are made up of individuals. Furthermore, we as individuals support corporations and the way that they pollute when we buy their products.

Fortunately, individual actions can also help to change the way that corporations behave. For example, in order to get Google to confront sexual abuse allegations in the company, approximately 20,000 employees participated in strike actions all around the world. A week later, Google gave up and agreed to the protesters' demands. Other companies, such as Facebook, Airbnb and eBay, soon followed Google and changed their policies too. In these instances, "grassroots" protest action was enough to sway the actions of mighty corporations. 

Of course, grassroots protest is not enough on its own to tackle so serious a problem as climate change. It needs to be coupled with "top-down" policy changes like the implementation of a carbon tax and the provision of funds for research on global warming from the government. 

These acts are not mutually exclusive. For a case in point, we can look at the example of another societal problem that seemed completely unstoppable just a century ago: polio. In 1938, US President Franklin Roosevelt decided to tackle the problem by providing a lot of funding for research. With that funding, the brilliant scientist Jonas Salk was able to develop a vaccine. 

Before the vaccine could be implemented on a nationwide scale, however, it first needed to be tested. In one of the largest ever medical trials, two million people volunteered to test the vaccine. Thanks to this sample, they discovered that it was safe and effective. Through this combination of structural support and the collective action of millions of volunteers, polio has been almost entirely eradicated. 

Fighting climate change will require exactly this kind of "bottom-up" as well as "top-down" activism, working in tandem.

> "_Social change, much like climate change, is caused by multiple chain reactions that occur simultaneously."_

### 6. Industrial animal farming is one of the leading causes of climate change. 

When we imagine farms we usually think of cows frolicking in green pastures. We don't typically think of them as being a source of pollution. So it may come as a surprise to learn that the animal farming industry is one of the main sources of harmful greenhouse gases released into the atmosphere. 

Since the advent of what is known as "factory farming" in the 1960s, huge numbers of animals have been concentrated on enormous plots of land, with terrible consequences for the environment. 

For example, farming livestock in such large quantities leads to deforestation: In order to clear land to grow animal feed and for animals to graze, large swathes of forest are burnt down. 

Trees are composed of 50 percent carbon, which means that when they burn they release large amounts of carbon dioxide. Shockingly, this accounts for 15 percent of all the carbon dioxide released into the atmosphere, the same as is created by all the cars and trucks in the world! What makes it worse is that the forests are then no longer able to absorb the carbon dioxide which is produced, because so many trees have been cut down.

Animal farming is also responsible for extremely high emissions of some of the gases which are the worst for the environment. When they are digesting food, livestock produce methane which is passed into the environment through belches, flatulence and waste. Nitrous oxide is released in the animals' urine and manure.

Methane has 34 times the "global warming potential" (GWP) of carbon dioxide, because it has a much greater ability to trap heat. Nitrous oxide traps 310 times more heat in the atmosphere. From the 1960s, when industrial farming became prominent, to the late 1990s, the amount of nitrous oxide and methane in the atmosphere increased more than it had in the previous 2,000 years. 

As the numbers make clear, there's no way we can combat the climate crisis without changing our reliance on animals as food. But how can we change something as fundamental as the way we eat? We'll find the answer to that question in the next blink.

### 7. The best way to save the planet is to stop eating animal products for breakfast and lunch. 

The statistics about the environmental impact of animal farming are very alarming. However, on a positive note, they point to an achievable way that we really can slow the progress of climate change: eating less meat and fewer animal products like milk or eggs.

This is one of the fastest ways to fight climate change. Ending our reliance on fossil fuels is also very important, but it's not something we can do quickly. Even if all the countries in the world held a summit tomorrow and miraculously agreed to abandon fossil fuels, it would still take more than 20 years to transition to using alternative energy sources. 

However, we can immediately reduce how much meat and dairy we eat. There are already healthy and delicious alternatives to meat products on every supermarket shelf. The decision can be as easy as substituting a beef burger patty for one made of tofu.

Critics have argued that it's elitist to assume that everyone can afford to eat a partly vegan diet because vegan foods can be more expensive than meat. However, if you look at the bigger picture, it becomes clear that eating animals is in fact a luxury that very few can afford.

Animal agriculture requires seven hundred million tons of grain every year, which is more than enough to feed every person on the planet. Furthermore, a third of all the drinkable water in the world goes to farming. Considering that millions of people are literally starving and go without access to clean water, this a staggering misuse of limited resources. It would be much _less_ elitist to prioritize the farming methods that feed the most people. 

So if eating meat is so terrible, why stop at breakfast and lunch? Why don't we cut it out from our diets entirely and become vegetarians?

Well, it's undeniable that most people value meat in their diets. It's an ingrained habit with a lot of cultural significance. Allowing room for people to continue eating meat for dinner might make it easier for them to maintain the habit of eating less meat in general, than trying to cut it out entirely would. 

Even more importantly, researchers from the Johns Hopkins Center for a Livable Future have shown that people who stop eating meat and animal products at breakfast and lunch actually have a smaller carbon footprint than people leading the average vegetarian diet, which includes environmentally destructive animal products such as milk, cheese, and eggs.

### 8. It’s already too late to prevent the disasters caused by climate change – but that doesn’t mean we should give up. 

Planet Earth seems endlessly bountiful, but it in fact has finite resources which are being used up quickly. Essentially, we are running up a large debt to the Earth that we can't possibly repay. 

Since the 1980s, we have been consuming natural resources at a rate that is much higher than the Earth can supply. Stripping the Earth of its resources, such as its forests, has knock-on effects for global warming as there are fewer left to absorb the massive amounts of greenhouse gases we are generating. This means that we are entering a phase of what is called "runaway climate change," where positive feedback loops amplify the damage we've already caused in ways that we can't control.

For example, because the polar sea ice is melting, larger areas of dark sea are exposed. The ice is reflective, keeping the Earth cooler by reflecting sun rays back into space, while the dark sea absorbs more heat. This means that the more the ice melts, the hotter the Earth becomes. The hotter it gets, the more ice melts, and so on in a vicious circle.

We are already seeing the effects of climate change in the form of devastating hurricanes, heat waves and flooding. Our lives will change even more as the effects become more drastic.

Faced with that reality, it's tempting to just throw our arms up and give in, or fantasize about starting a new life on Mars. However, the only ethical option is to do everything in our power to fight climate change right here on Earth.

We have an ethical obligation to future generations who will be affected by the actions we take now which are irreversible. Furthermore, we have an obligation to the poorest populations on Earth who create the least pollution, but who are and will be disproportionately affected by the results of climate change. 

What distinguishes humans from animals is that we can make rational decisions based on ethical considerations, rather than just our feelings. In the future, we will be judged on the actions we take.

> _"We cannot keep the kinds of meals we have known, and also keep the planet we have known."_

### 9. Final summary 

The key message in these blinks:

**Climate change is difficult to understand. The causes are complex and the results are so horrifying that at an emotional level, they seem unbelievable. We need to use our powers of reason, though, to accept the reality and do everything we can to prevent it. Industrial animal farming is one of the leading causes of global warming. It follows then that one of the most effective things we can do as individuals is to stop eating meat and dairy for breakfast and lunch.**

Actionable advice: 

**Become a pragmatic climate change activist.**

We often feel that we'll have to change our lives in extreme ways in order to make a real difference. The thought of having no children, pedalling 30 kms to work every day on a bicycle and never eating a steak again is enough to paralyze any would-be activist because it feels impossible. But, smaller actions make a difference too. Like taking two flights less per year. Or car-pooling with a neighbor. Or leaving out the bacon and eggs on your breakfast bun. These are small, doable actions that will cumulatively have a big effect. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Eating Animals,_** **by Jonathan Safran Foer**

You've just had your eyes opened to the impact of industrial animal farming on the environment. But what about how it affects animal welfare? How is the meat that we eat produced and under what conditions? And what exactly are we feeding our children when we buy sausages from the supermarket? 

If you want to find out, then check out the blinks to _Eating Animals_ by Jonathan Safran Foer. You will learn about why, strangely, meat is so cheap, as well as discover the real costs of factory farming for the wellbeing of animals and workers. You will also learn about how unhygienic conditions on farms can harm our health by spreading bacteria like salmonella, and create a breeding ground for dangerous new flu viruses.
---

### Jonathan Safran Foer

Jonathan Safran Foer is an American novelist and nonfiction writer. His previous nonfiction book _Eating Animals_ became a New York Times bestseller, and was turned into a documentary which won the Environmental Media Association Award in 2019. Safran Foer is a board member for Farm Forward — a non-profit organization promoting sustainable eating.

