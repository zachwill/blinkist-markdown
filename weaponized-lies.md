---
id: 596164b9b238e10006ad571a
slug: weaponized-lies-en
published_date: 2017-07-10T00:00:00.000+00:00
author: Daniel J. Levitin
title: Weaponized Lies
subtitle: Critical Thinking in the Information Age
main_color: D22D2A
text_color: D22D2A
---

# Weaponized Lies

_Critical Thinking in the Information Age_

**Daniel J. Levitin**

_Weaponized Lies_ (2016) is a user's manual for today's news media. It teaches you various skills that will help you to analyze the vast amount of information you encounter when skimming the internet or watching the news. Take time to learn what's real and what's fake, so you won't get duped.

---
### 1. What’s in it for me? Learn to filter fact from fiction. 

We all consume news and media in our content-saturated world. But amid all the noise, how many of us take the time to verify the truth of the information we encounter? How often do we accept surprising statistics that are presented in persuasive pie charts or graphs — and cite them later at a dinner party without knowing their accuracy?

Even if you think you're choosing verified sources to get your news from, no source of information is immune to mistakes. So we should all be more rigorous in questioning the veracity of the news we consume, and these blinks will tell you why.

In these blinks, you'll learn

  * why you should double-check all cited links;

  * how averages are not always what they seem; and

  * why you might trust "experts" too much.

### 2. Separating fact from fiction is no easy task. 

We have access to more information than ever before. But it's becoming harder and harder to distinguish between fact and fiction. This is the plight of those living in the information age.

Firstly, the source of much of this information — the internet — isn't regulated. People can write anything they want and in this environment, it's easy to mask falsities as facts.

However, few of us have the time to fact-check everything we read online, and though we're happy citation links exist, we rarely click them. Authors know they won't be challenged, so the links often don't really back up what's said in the text.

The worst culprits tend to have a common tell: if an article proclaims its honesty, something's up.

Just take martinlutherking.org. Initially, it appears to be a site devoted to the life of the civil rights leader. But the veil is thin. It's actually a fount of neo-Nazi propaganda that manipulates facts and uses out-of-context quotations to further its agenda.

One way to avoid deliberately dug pitfalls like this is to only look at respected publications, but it's still good practice to stay alert.

Institutions like the _New York Times_ or the _Wall Street Journal_ have long been respected sources of information as they verify their reports with trustworthy sources. However, these publications aren't immune to making mistakes.

Consider how Pulitzer Prize-winning _Washington Pos_ t reporter Jonathan Capehart was bamboozled in 2011. He wrote an article about a nonexistent congressman and his district based on a lead from a fake twitter account.

This happens because journalists are not infallible. They may cover subjects about which they know little or may need to analyze statistics and graphs beyond their capabilities. This means that if the source material is biased, the journalist may not realize it, thereby allowing the bias to slip into their own reporting too.

The lesson? We shouldn't just take what we read on trust, no matter what the source.

### 3. Don’t take graphs and statistics at face value. 

It's easy to skim over a graph or statistic without thinking about it too closely. But this is exactly what we should avoid.

As an example, consider what's meant by "average." It's a frequently used term, but there are actually three distinct meanings.

The _mean average_ is the number that results if all values are added together and divided by the number of values.

The _mode average_ is the value that occurs most frequently.

The _median average_ is the "middle" value when all values are listed from small to large.

It's not always clear which average is meant. In fact, reports often feature the one that best bolsters the overall message.

Be especially cautious when presented with the mean average, as it's the one most affected by extreme anomalies.

A good example of this can be found in politics. Democratic candidate John Kerry supposedly won nine out of 11 of the wealthiest states in the 2004 presidential race. This is interesting because common knowledge holds that wealthy voters vote Republican.

But in fact, the wealth of the states was calculated using a mean average. Outliers skewed the statistic because a few of the _wealthiest_ people lived in those states. This didn't mean that _more wealthy_ voters lived in those states.

Equally, graphs can be massaged. Values on axes can be changed to produce steeper or smoother curves.

Consider this theoretical example. For two-thirds of a graph, time could be measured in five-year steps but changed to two-year increments for the final third. If the values measured were particularly high during the last two years, many readers would only see that the curve is going up, without realizing that the last value doesn't hold the same weight as the previous ones.

Even visually simple graphics like pie charts can be manipulated.

In 2012, Fox News ran a pie chart showing the standing of the candidates for the Republican nomination. It took a second glance to notice the slices added up to more than 100 percent!

Vigilance with facts and figures is one thing. It's harder still to consider the context of what you're reading. We'll look at that next.

### 4. Know the context; understand the conclusion. 

It might seem counterintuitive to consider what's been left unsaid in an article, but sometimes the details that authors tend to omit are vitally important.

For instance, understanding what methods were used to obtain the facts and statistics presented in an article is critical to determining an author's intent.

Let's say an article is based on data obtained from a survey. In this case, you should remember that the more interested someone is in a given topic, the more likely they are to respond to a survey, thereby skewing the sample.

Another factor is the way the data is gathered. Using landline phones is a common way to find respondents, but these days it's mostly older generations that have house phones, thereby again skewing the sample.

This is why using a _stratified sample_ is critical for conducting a representative survey. This means that the surveyor should identify different subgroups of a population and get a number of participants from each subgroup in proportion to the group's share of a population as a whole.

It's also important to consider how information is framed. The media tends to focus on certain risks more than others. This overemphasis can mean that we, in turn, forget the broader implications and context of the situation.

For example, after the Paris terrorist attacks on 13 November 2015, some experts argued for stricter EU border controls on refugees from Arab countries because one of the assailants had supposedly entered Europe as a refugee.

But such arguments ignore the big picture of saving human lives: Europe's asylum policy has saved the lives of thousands of refugees by allowing them to escape the terror of war-torn countries.

Clearly, it's crucial to always consider the big picture.

### 5. Counterknowledge comes in many forms, and we must be vigilant enough to spot it. 

Misinformation is a dangerous thing, especially when it's made to look like the truth, and we don't pause to properly evaluate it.

There's a term for this. _Counterknowledge_ is false information that many people still perceive as true.

The most common form of counterknowledge is the conspiracy theory. Imaginative conspiracy theories appeal to us because we all love a great story.

The problem with counterknowledge is that it anchors itself in our brains. Even when we're presented with evidence to the contrary, it can be difficult to jettison the falsehood. The reason is that we're predisposed to justify our beliefs and hate admitting we've been wrong — or conned.

Counterknowledge can spread far and wide through false expertise.

It's very easy to cite an "expert" who supposedly validates the claim being made.

That's why it's important to remember that not all experts are equal. Just because someone is an expert in one field, it doesn't mean they know much about anything else. After all, it takes a lot of time and effort to become an expert in the first place. You might think a PhD from MIT gives someone the authority to speak on health or genetics, but what if he's just a tech enthusiast?

This effect was seen in the case of Andrew Wakefield. He was a surgeon who published an article on a link between autism and vaccination that was later debunked. As a doctor, he had the appearance of an expert, but in fact, he was not particularly familiar with autism. The fallout was huge, and his medical license was revoked, but not before many people were taken in by his claims.

So how do you combat counterknowledge? With _diligence_. Look closely at _who_ is telling you _what_, but don't forget to also examine your own predilections and predispositions.

### 6. Use the Bayesian method to become a critical consumer of news. 

How would you respond to the claim that humans no longer need water to survive? You'd certainly require a huge body of evidence to believe it.

Evaluating the probability of the truth of a claim based on its relationship to prior knowledge is known as the _Bayesian method_. Invented by the English philosopher and statistician Thomas Bayes (1702–1761), the method provides a way to decide how readily we should accept an assertion. Scientists still employ it in medical trials and to prove new discoveries.

It works like this. If an argument has lots of well-established knowledge to back it up, we don't need much additional evidence to believe it. For instance, a claim that humans do in fact need water to survive would not need much additional evidence.

But, if you're presented with a "fact" that's counterintuitive — that is, if it flies in the face of established evidence — sirens should start blaring. That's why the claim that humans don't need water to survive would require a lot of evidence to be convincing.

You can also use the same method to appraise what we see in the media: the more ludicrous the claim, the more diligent you need to be.

Luckily, there are sources, such as the fact-checking website Politifact, that can help you.

Take this recent example. In 2015, while speaking at a rally in Alabama, Donald Trump claimed he'd seen "thousands and thousands" of Muslims on TV cheering the collapse of the World Trade Center.

Politifact and the _Washington Post_ got to work. They checked news reports and TV broadcasts of 9/11 and the three months afterward. Nothing was found to corroborate his statement. It was a lie.

Its falsity doesn't diminish one fact though: the statement profoundly affected many of Trump's supporters.

There's a lesson here. Now, more than ever, it's essential that we all fact-check and practice our critical abilities. Society will be the ultimate beneficiary.

### 7. Final summary 

The key message in this book:

**It is not enough to blindly trust even reputable news sources or public figures. We must take it upon ourselves to be sure that the information we absorb is true.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _All the Truth Is Out_** **by Matt Bai**

_All The Truth is Out_ details the sudden transformation of political journalism in the late 1980s, as political reporters shifted their focus from policy to the personal lives of politicians. Using the rise and fall of former presidential hopeful Gary Hart as a starting point, it shows how political journalism and politics in general have changed both in form and content.
---

### Daniel J. Levitin

Daniel J. Levitin is a cognitive psychologist with degrees from Stanford University and the University of Oregon. He is currently a professor of behavioral neuroscience, music and psychology at McGill University. His other books include the number one best seller _This Is Your Brain On Music_ and _The Organized Mind: Thinking Straight in the Age of Information Overload_.

