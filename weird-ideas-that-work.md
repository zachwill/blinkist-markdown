---
id: 52b883083737660008060000
slug: weird-ideas-that-work-en
published_date: 2013-12-24T09:25:33.000+00:00
author: Robert I. Sutton
title: Weird Ideas That Work
subtitle: How to Build a Creative Company
main_color: FEA338
text_color: CC7918
---

# Weird Ideas That Work

_How to Build a Creative Company_

**Robert I. Sutton**

_Weird Ideas That Work_ examines how innovations come about in companies and how they can be fostered. The book also shows how creative tasks require a very different work ethic to performing routine tasks. The author cites various examples from the business world and the work of famous geniuses to explain his ideas.

---
### 1. Companies must recognize the differences between routine and innovative work. 

Routine work and innovative work are both important to a company's success. Recognizing the differences between the two, and understanding where each is useful, is invaluable.

Nobody can predict when a successful innovation will occur, but the likelihood of it happening certainly doesn't need to be left to chance. Certain circumstances can be established that increase the probability of generating useful innovations, whether we're talking about inventing a more efficient engine or creating a great work of art.

Innovations are most likely to emerge when people are always busy experimenting, whether they are generating new ideas or building upon existing ones. The philosophy here is to create as much diversity as possible, and to stay open to experimenting with even the most far-fetched of combinations. Even though this may result in a lot of "garbage" (unusable material and failed experiments), every now and then a true innovation is born that justifies the means. A creative team trying to design a faster race-car engine might produce a lot of garbage, but this isn't collateral damage: it's a natural part of the process.

Routine tasks, in contrast to innovative tasks, have an entirely different function: their purpose is to put existing ideas to good use. The aim is to create successful products that are easily reproducible by following the exact same procedure to make them (e.g., a Volkswagen or Big Mac). Precision, not trial-and-error, is the name of the game: experimentation is unwelcome, and the less garbage produced the better.

The point of routine work is to ensure a company's short-term turnover: it's what fills the coffers today. However, when a company has its sights set on long-term success, routine work isn't enough to make that happen. Ideas that are marketable today may already be old news by tomorrow. The market changes; the competition never sleeps. When someone else comes up with a great new engine, your fine-tuned processes for building the current status-quo engine will be rendered obsolete in no time.

Hence, in order to find the future sources of income, it is important to make space for innovations.

**Companies must recognize the differences between routine and innovative work.**

### 2. Innovative work can’t be judged by the same standards as routine work. 

The productivity of routine tasks can pretty much always be evaluated using a simple formula: draw on past experience to establish both how many errors and how much output you can expect for a certain amount of work. Then use this as a quality standard to measure your current process against. Generally speaking: the less garbage you produce, the better your process.

The makers of a car or a Big Mac already know exactly how the products should look, and so they put all their energy into ensuring that the manufacturing process is as accurate and efficient as possible.

However, it's pointless to apply such standards to innovative work (i.e., the development of new things) because no clearly defined target exists: you don't know exactly what it is you're searching for. The only objective can be to carry out as many experiments as possible and produce a lot of unusable material. Sooner or later, you'll find a gem in the garbage heap.

Alas, it is rare indeed for anyone to produce a great idea when they feel forced or under pressure. Great ideas are also impossible to predict: sometimes they emerge quickly, sometimes they incubate for a long time, and other times, despite all efforts, they never appear at all.

In general, it's fairly common for highly innovative creative teams to work slowly. They may even work so slowly that outsiders get the impression they're spending all their time doing nothing at all, but that's because the outsider is judging the teams' work by the wrong standards; namely, the standards for completing routine tasks.

But think about it this way: how productive would Archimedes have looked while he was taking his bath just before his famous "eureka" moment?

**Innovative work can't be judged by the same standards as routine work.**

### 3. Innovation is often born out of a long series of failures. 

In simple terms, the evolution of all living things goes something like this: random mutations cause the creation of new variations, and of these most fail and disappear; only a very certain few survive. Every living being descends from an almost infinite number of experiments where only one proved adaptive to a certain environment. For example, one branch of hominids might have had only a limited ability to vocalize, but, if so, they couldn't compete with our ancestors, who, through the power of speech, could exchange ideas, warnings and advice among themselves.

And that's exactly how innovation works: successful results arise through constant experimentation. Although most of these experiments fail, every now and then a new idea crystallizes. By producing many new things and maximizing diversity, these experiments will inevitably yield a valuable innovation somewhere down the road.

Anyone who doesn't believe in the power of volume should take a closer look at the work of famous artistic geniuses like Mozart and Picasso. They may be best known for their brilliant works and outstanding ideas, but many people fail to realize that these geniuses produced just as many — if not more — bad works and banal ideas as they did good ones.

Quite simply, they produced an incredibly broad and varied body of work; i.e., they experimented with an extremely high number of new combinations. They were not afraid to fail, and hence eventually succeeded.

This approach holds true for all kinds of innovative work: In order to find good ideas, you need an enormous amount of stock, as it were. You have to be ready to try out many things and accept that most of them will end up in the trash can.

**Innovation is often born out of a long series of failures.**

### 4. Innovating does not necessarily mean creating something from the ground up. 

Innovation refers, first of all, to the creation of something new and useful. However, this _something_ does not necessarily have to be created from the ground up. In other words, innovating can also mean rearranging existing ideas and components to make something new, introducing a familiar idea into a foreign context or simply using an existing idea in a novel way.

Take the programming language Java, for instance: it was considered a major breakthrough in programming, though actually it emerged from the combining of existing components. Its virtue — and novelty — lay in the clever arrangement of these components.

It's completely legitimate to be inspired by existing ideas and to experiment by trying them out in new fields. Taking something out of one context and putting it into another is, often, enough to create an innovation.

For example, the screw bases of light bulbs are modeled after screw-top caps of bottles: a simple and useful innovation that still hasn't been out-innovated today.

Sometimes new things are even invented by accident. The sex-enhancing drug Viagra, for example, was originally conceived as a medication to lower high blood pressure. At first, its current use was registered merely as an unwanted side effect: it took time before this became the main prescribed use for the medication.

**Innovating does not necessarily mean creating something from the ground up.**

### 5. Companies that want to drive innovation should hire non-conformists and misfits. 

Most companies seek to employ so-called fast learners, i.e., people who are communicative, have a social edge and can adapt quickly to a company's standards and conventions.

Such employees are important for the day-to-day operations and routine tasks that earn a company money in the _short term_. But, if a company has its eye on innovation in the long term, it needs to hire slow learners as well, i.e., people who are either slow in accepting a company's dogma and conventions or who don't accept them at all.

Such people are in no way to be confused with idiots: they are simply people who think independently and bring along new ideas, and who resist their herd instinct and stand up for their own views — however unpopular their views may be.

Such people usually tend to exhibit the following characteristics:

  * they are very self-aware;

  * they don't care what others think about them;

  * they are generally not sociable, preferring to work for themselves. 

Throughout history, many geniuses who've changed the world with their revolutionary ideas have possessed these traits. Richard Feynman, brilliant physicist and recipient of the Nobel Prize, cared so little about what other people thought of him that he never even followed the convention of publishing his discoveries.

If a company decides to hire slow learners, one useful criterion is to look for intelligent people who got bad grades in school. Geniuses like Edison and Darwin were very average students: this is because they were independent thinkers who didn't want to just swallow the view of the world that their teachers fed to them.

**Companies that want to drive innovation should hire non-conformists and misfits.**

### 6. If you want to be innovative, hire people you don’t like. 

Groups are particularly innovative and creative when they include an amalgamation of varied ideas, skills and personalities. Companies should therefore not lose sight of the need for diversity when carrying out an applicant selection process.

But more often than not, something stands in the way of achieving this desired diversity — namely, that commonality engenders sympathy. The expression "birds of a feather flock together" is an apt way to describe this psychological effect. We like people who are like us, and so we tend to surround ourselves with people with the same skills and interests.

By only hiring people who are similar to us, we limit the breadth of ideas that could exist in a company, which ultimately makes it more difficult to create innovations. The more perspectives in a group, the likelier it becomes that something new will emerge.

And so, to encourage innovation, we must resist our urge to hire people simply because we like them. The co-workers we could benefit from most (i.e., those with other opinions and characteristics), often come across as unlikeable at first glance.

Being aware of this phenomenon is the first and most important part of getting rid of the unhealthy bias towards hiring clones of ourselves; executives should rather make a habit of seeking out employees with views that oppose their own.

**If you want to be innovative, hire people you don't like.**

### 7. Companies can learn a lot from their young employees. 

Many big companies invest huge amounts of money each year on consultants whose job it is to represent an outsider's perspective and provide the company with innovative and fresh ideas.

However, hiring young people with an impartial view of the company and then giving them the freedom to introduce their own ideas would be far more intelligent. Their ideas might be just as fresh as those of the consultants but come at a fraction of the cost.

Also, it can be extremely useful to encourage newcomers to be rebellious and assert themselves in the face of a company's old hands and conventions. New employees have an unbiased view of the company and can play an essential role in bringing about innovations.

Rather than upholding the convention of re-educating new employees at the first possible chance so that they fall in with a company's established ideas, companies should integrate new employees into the team just as they are, with their unique ideas and opinions.

Several companies go even further and use reverse socialization, i.e., having the younger employees teach the older employees what they know. Apart from the lower costs, this method's big advantage over hiring external consultants is that ideas and knowledge stay within the company rather than disappearing into thin air after a temporary project has been concluded.

Another useful method for gaining fresh perspectives is to tap into the knowledge of young applicants during the hiring process. Even if you don't end up taking on certain candidates, they may voice ideas in their job interviews that the company could put to good use later.

**Companies can learn a lot from their young employees.**

### 8. Companies should penalize employees for their inaction, not for their failures. 

Successful creatives are not afraid of mistakes or failure: they know that good ideas often arise from tireless experimentationand a chain of failed attempts. People who avoid mistakes and failures suffocate all potential innovation. Unmindful of this, many companies punish their employees for making mistakes, and thus inadvertently create an environment where people just try to conform rather than experiment.

Hence, companies that are driven to be innovative need the right incentive system. Employees should be encouraged to take creative risks and put themselves to the test. In other words, they should feel confident enough to fail — and not be punished for making mistakes.

A company should reward its employees for their successes and failures alike. At any rate, failures should at least be tolerated because they make up a key stage of the creative process. People who are always working on new things are bound to fail often.

But one thing that shouldn't be tolerated is inaction: people who don't take risks or try new things will not bring about innovation within the company. Given that the likelihood of new developments increases in relation to the amount of _output_, and that it's hard to say in advance which ideas will be successful, quantity is your surest bet to guarantee innovation.

That's why companies should try to avoid falling into the "smart-talk trap." Sometimes people are perceived as being competent because they sound smart but they never actually take risks themselves and instead criticize other people's ideas. They usually don't contribute anything but hot air, and this type of inaction should be penalized and strongly discouraged.

**Companies should penalize employees for their inaction, not for their failures.**

### 9. Creative teams should be left to work in peace. 

Creative work is easily hindered. When supervisors or executives put constant pressure on the members of a creative team, the team's chances of coming up with viable findings sink rapidly. How many creative risks would you take with someone constantly looking over your shoulder?

The reason a company's higher-ups act this way is simple: in general, people feel comfortable with the familiar and repelled by the unfamiliar. Outsiders tend to be very critical of the work of creative teams and dismiss new ideas as nonsense. Plus, it's not uncommon for executives to believe they know better than their employees.

Not surprisingly, many of history's most successful innovation teams were deliberately isolated from their superiors. For example, the team that designed the first Macintosh computer for Apple worked in a separate building from the rest of the employees.

Apple was smart enough to recognize that the members of the team would not benefit from the presence of authority figures looming over them, demanding progress. When creative teams are safe from constant criticism, the chances are far higher that they will be able to come up with innovations.

Company heads should therefore designate spaces for creative teams to work on their own and, above all, try not to harm their creative processes.

**Creative teams should be left to work in peace.**

### 10. Final Summary 

The main message in this book:

**Innovations are the fruits of constant experimentation and are often associated with frequent failures. When companies want to promote innovations, they should hire non-conformists, reward failures as necessary steps in the search for great ideas and give their teams full creative freedom.**

This book answered the following questions:

**Why is it important to understand the different roles of innovation and routine work in a company?**

  * Companies must recognize the differences between routine and innovative work.

  * Innovative work can't be judged by the same standards as routine work.

**Where do innovations come from and how can they be explained?**

  * Innovation is often born out of a long series of failures.

  * Innovating does not necessarily mean creating something from the ground up.

**What can companies do to boost innovation?**

  * Companies that want to drive innovation should hire non-conformists and misfits.

  * If you want to be innovative, hire people you don't like.

  * Companies can learn a lot from their young employees.

  * Companies should penalize employees for their inaction, not for their failures.

  * Creative teams should be left to work in peace.
---

### Robert I. Sutton

Robert I. Sutton (b. 1954 in Chicago) is professor at the Stanford Business School. He advises many international companies, and publishes popular scientific books regularly.

