---
id: 5d3575216cee070008096c36
slug: weird-parenting-wins-en
published_date: 2019-07-25T00:00:00.000+00:00
author: Hillary Frank
title: Weird Parenting Wins
subtitle: Bathtub Dining, Family Screams, and Other Hacks from the Parenting Trenches
main_color: 8EBC3F
text_color: 5E8A13
---

# Weird Parenting Wins

_Bathtub Dining, Family Screams, and Other Hacks from the Parenting Trenches_

**Hillary Frank**

_Weird Parenting Wins_ (2019) shows you how _not_ to lose it while bringing up your kids — all you need is a little craziness. That means being creative and playful. Children approach life ready to deploy their imagination at every turn, and this book in blinks shows how you can be just as imaginative. You'll find ways to make life easier for the whole family and even have fun in the process.

---
### 1. What’s in it for me? Embrace the crazy fun that being a parent or caregiver can bring. 

Everybody knows that having kids is no walk in the park. Those first months can whizz by in a sleep-deprived blur, while the years after are filled with their own unique challenges. No child is an angel, after all. There's whining and crying and drama — and a hell of a lot more besides. And if you're looking after kids that aren't your own, the challenges are even more manifest.

It may seem daunting, but there are ways to make both your child's life and your own easier. Done right, your relationship with your child will grow in a healthy way and develop as you learn from each other.

The child-rearing hacks laid out in this book-in-blinks don't need to be followed to the letter. These blinks instead advocate an approach that involves engaging your own inner childlike creativity and playfulness. It's a method the author knows well from her own experiences bringing up her daughter. You'll see that a little bit of fun and a little less worry will get you a long way with children.

In these blinks, you'll learn

  * about some postnatal side effects that aren't spoken about enough;

  * how to entertain kids with take-out; and

  * how to get kids to act out their social difficulties.

### 2. It takes imagination to stop kids whining; stopping babies crying takes effort. 

There are some experiences that every parent knows. One of the most common is dealing with a child that won't stop whining. The author knew she would be faced with a bad case of the grumbles when she arrived at a ski resort with her six-year-old daughter Sasha. The outdoor queue for rental skis stretched back for an eternity. It would take them hours to get to the front.

In the freezing cold, it didn't take Sasha long to start whining. Nor was there a chance that she was going to stop soon.

Now, when you're a parent, it takes imagination to stop kids moaning. You can't just tell them to cheer up. That method certainly wasn't working with Sasha.

Consequently, the author tried to come up with some ideas that would at least make Sasha's crying a little more entertaining for them both.

She suggested that if Sasha was so set on whining, then she would have to do so as if she were singing the blues. The author took the lead with the tune and sang about being bored in line and wanting to get to the skis before anyone else.

At first, Sasha was dubious. The whimpering continued. But then the author struck lucky. Quite by chance, she improvised a line about freezing her butt off. Now "butt" was a trigger for Sasha. It was one of her favorite words. After the giggling subsided, she tried singing too, incorporating the word _butt_ as often as she could.

Before long, both were in fits of laughter, and the author still remembers it as a great parenting story. It was all about being imaginative in the moment. There's no rule book for parenting. Sometimes you just stumble upon the solution by being weird and creative and testing things out as you go.

But if creativity is needed to settle young children, then it requires even more effort where babies are involved.

Sarah, a mom from River Forest, Illinois, and her husband, told the author a classical parenting tale about what it took to hush their baby. The initial thrill of having a newborn soon gave way to absolute exhaustion. Their child just would not stop screaming.

Then, in his sleep-deprived delirium, the father tried one last desperate measure. He picked up an electric toothbrush, switched it on, and began waggling it above the baby's head. Amazingly, the baby hushed and was lulled to sleep.

From that night on, the parents placed the electric toothbrush, still on, in the cot. And every night it did the job.

There's a moral to this. Don't give up! You never know what's going to end up working.

### 3. Get your kids to eat by changing their concept of eating. 

If you think back to your childhood, you might remember how frustrating mealtimes were. What a waste of time! Who wants to be stuck at the table when you could be dashing around the house in your underwear?

For parents, this scenario can be a nightmare. Kids need to eat for all the obvious health reasons. But more importantly, no parent wants an irritable sugar-depleted child on their hands.

Well, the author's mom had her own solution to the problem whenever the author herself got grouchy at mealtimes. Her solution was a "snack meal."

In essence, this was just the same chicken that was being prepared for dinner anyway, but it was served in "snack" form. Cold, it was cut up into small chunks, speared with wooden toothpicks and served up on the kitchen counter.

This meant that the author didn't have to eat at the boring family dinner table but could perch up on a kitchen stool by the sink. Once she'd had her snack, she was free to run off and play. Of course, the other advantage was that her parents could enjoy their own supper in peace. It was an excellent strategy. 

Just as we saw in the last blink, creativity is needed when caring for younger children. You can't just ask them, after all. That's particularly true of babies.

Many new moms have problems with breastfeeding. And if you've ever attended breastfeeding class, most likely you'll have had a number of different breastfeeding positions shown to you, the classic "cradle hold" among them.

However, when the author was attempting to breastfeed her own daughter Sasha, simply nothing worked. No matter which way she held Sasha, the little one simply refused to latch on and drink.

Once more it took a left-field solution. The author's friend suggested a new tactic.

They placed Sasha upright in a seated position on the sofa. But of course, as the baby wasn't yet strong enough to stay there on her own, they trussed her up with a pillow. When all was set, the author presented the breast to her. Amazingly they found Sasha could suddenly breastfeed without even a hint of difficulty.

This was definitely a weird position compared to what's taught in class. But it worked. Sometimes adaptability is what it takes with newborns and young babies!

> _"What I really needed was the prop-your-infant-up-as-if-sitting-in-a-chair hold."_

### 4. Parenting can calm children’s wild fantasies and make them feel braver. 

Much like any other child, the author had a vivid imagination. She believed a lion hid beneath her bed ready to pounce the moment she drifted off to sleep.

Many children have similar fears, but though at times it might seem impossible to calm them, there are many ways to hush a child's overactive imagination.

For instance, the author's mother came up with an interesting strategy to help her daughter's fear of scary animals. She drew up a sign that stated in bold letters that lions, tigers and bears were barred from entering the author's childhood room.

Almost instantaneously, this sign proved effective. The author's child self was soothed — what wild creature would dare to cross? Needless to say, numerous other creatures were gradually added to the sign: mice, foxes and wolves also received bans. By the time alligators, crocodiles and sharks were added, the lettering was tiny as there was almost no space left on the sign!

The author's mother was also savvy enough to extend this aegis of protection outside the home. The guestroom at the author's grandmother's home received the same treatment. Up went a new sign.

Of course, such soothing strategies are all good and well as a first response measure. It's one useful way to calm a child's fears. But it would be even better if a child didn't need to be calmed with tricks. Another approach, therefore, is to help develop children's individual sense of bravery. To do this, you can play _bravery games_ with them.

For instance, you could set your child the challenge of going into a dark room and counting to ten before coming back out. Of course, you needn't throw your child in at the deep end; you may want to begin by holding hands and accompanying them to provide a bit of security. If it's clear that your child is enjoying being brave, you can try counting even higher, perhaps to 15 or 20 seconds.

The author has heard from numerous parents that this type of game works wonders.

### 5. Siblings aren’t always kind to each other, and sibling rivalry is a sign of needing to feel loved. 

The author has a brother, Josh. He's six years younger than her. When Josh was a baby, the author was fond of holding him, feeding him, and of course, playing with him. However, Josh's presence also triggered a darker side to the author's character.

There's nothing unusual about that of course. It's quite normal for sibling rivalry to spiral into unkindness.

The author knows this all too well. When Josh was still a baby, she liked to pull open his eyelids as he slept. That way she could see his dreaming eyes rolling around. Nor was that the last of the humiliations he underwent. She photographed him with underwear wrapped around his head or hid him in the cupboard below the kitchen sink just to see if he'd fit.

As they got older, the author went even further. Once, she hit upon a particularly cunning scheme involving the Tooth Fairy.

Josh's gums tended to bleed profusely whenever he lost any of his milk teeth. So, one night, after Josh had lost another tooth, the author crept into Josh's room. She was after the present their parents had left Josh from the Tooth Fairy. She stole it, along with the note. In their place, she wrote a fresh note on blood-red paper, in which the Tooth Fairy made the outrageous claim that children who bled a lot when losing teeth didn't get any gifts!

No doubt stories such as these will sound familiar to parents who've seen similar rivalry between their own children. This begs the question: Should the tricks played by siblings be taken at face value or are they a sign of something else?

In truth, sibling rivalry is often an expression of a need to feel loved.

Just consider Danielle, a mother from Bremington in Washington. She wrote to the author concerning her son who wouldn't stop pestering his new baby brother. Interestingly, the older brother had also begun to say he was stupid, bad and not liked by anyone.

Danielle knew she had to stop this. So she told him he was her favorite child, even though of course she didn't have a "favorite." The boy was extremely uncertain about this claim. But eventually, he relented and stopped troubling his baby brother. He even let up on the self-criticism, and his mood improved too.

It goes to show that parents ought to be aware that sometimes acting up may reveal some deep character traits that need to be recognized and addressed.

> _"I truly believe that my guy just needed to know he was still loved. That his brother was not a replacement for him." –_ Danielle, from Bremington.

### 6. Sometimes parents need to have a good cry, but there are strategies to stop you hitting crisis point. 

Even if you think of yourself as generally even-tempered, kids will probably have brought you to your breaking point nonetheless. Sometimes you simply can't do anything other than explode.

But such outbursts are nothing to be embarrassed about. Sometimes a valve just needs to be released.

As a parent, it's totally normal to be overwhelmed. There are numerous coping strategies. You can plunge your face into a pillow and scream so that your children can't hear. Sometimes eating a packet of cookies can also prove surprisingly effective.

And sometimes you can have a good old cry. It's a classic way to reduce tension.

The author discovered this herself one night at home. She was exhausted to the point of immobility, only able to crash in front of the 2013 Jill Soloway movie, _Afternoon Delight_. The film is all about parents and their relationships, friendships, sex troubles and fatigue. The comedy drama hit home. Before long, tears were pouring out, and she felt all the better for it.

As good as crying can be, it has to be avoided sometimes. There are some strategies for precisely this purpose.

Let's consider Kristina from New York. She wrote to the author concerning her experience taking care of eight autistic children at a YMCA swimming pool.

When it was time to get out of the water and leave, one of the kids refused. As he was a big 13-year-old, Kristina couldn't just yank him out. She was at a loss, overwhelmed and had no idea what to do. It could all so easily have ended in tears.

But then Kristina remembered the boy had an animal obsession. So she asked him to show her how a turtle lays its eggs.

The boy didn't take the bait straight away, so Kristina began mimicking a turtle hatching eggs on the poolside until her inability to capture the fully nuanced role of the mother turtle induced him to get out of the pool and show her how to do it!

Problem solved.

There's a general lesson to glean from the last few blinks. It doesn't matter whether it's you or the child that's struggling or on the verge of tears, the best strategy is clear: You have to shift children's attention onto something that delights them. Just insisting that they do as they're told will get you nowhere.

### 7. Kids can entertain themselves, but it’s a skill they have to be taught. 

Perhaps one of the most tiresome elements to being a parent is feeling like you're constantly on duty, always ready to entertain. But no one can keep that up. We all need an occasional break from childminding.

The author's own salvation came in the form of plastic condiment sachets of spicy and soy sauce from Asian takeaways. She — like many others– doesn't actually use them. So she ended up with a glut.

And boy, did they come in handy.

Her daughter Sasha had become obsessed with the kitchen spice drawer. She would rummage through it and toss out whatever she could lay her hands on.

What the author needed — as we've learned — was a distraction. Thankfully, a pile of sauce sachets was on hand. Sasha loved them and started to arrange them by type, such as duck sauce or mustard.

As it happens, Sasha had also been given a pretend cash register as a birthday present. Sasha loved opening and closing the drawer, but she had no pretend money to put in it. This is where the packets came in. The author suggested a new game: the condiments could be stored in the register by type and then be taken in and out again.

She found that Sasha was quite content to busy herself for hours like this. But you can't always rely on such random finds to give yourself a break.

If you want some regular calm, you're going to have to teach your kids to entertain themselves. If you don't, you won't even be able to go to the bathroom in peace!

Certainly, that's how it was for the author. Sasha would punch her, natter away to her or start demanding snacks whenever the author was perched on her ceramic throne.

So the author decided she had to keep Sasha occupied. She gave her a three-string guitar and an instruction. She was to have composed a song by the time the author was done in the bathroom.

It was a resounding success. Not only did the author get to poop in peace, but Sasha was also thrilled at her new song about a man who loved to "jiddi-i-o." We aren't all lucky enough to emerge from the bathroom to be treated to a sure-fire hit like that.

### 8. Role-playing games help young children express emotions, but listening is often best when dealing with teenagers. 

Most young kids will prattle away at a thousand miles an hour. They'll talk at you while you're driving or during evening meals. The most tenacious, like Sasha, won't even let a closed bathroom door get in the way. But for all that nattering, children aren't good at talking about their feelings.

One way you can get them to open up about their emotions is to role-play with them. 

Let's return to Sasha. As a preschooler, Sasha just used to scream and scream whenever she got upset.

In one particularly heightened moment of emotion, she even started raining down blows on her mother, yelling as she did so. Luckily Sasha was able to show the author what the problem was; in an inspired moment, she started role-playing.

Sasha got her mother to pretend to be Sasha, while Sasha played at being her preschool friend Lily.

Thanks to the role-play it became clear what was going on. Lily was stopping Sasha from playing with any other child at preschool. Lily was terrorizing Sasha by saying they'd no longer be friends if Sasha did so. She even suggested that Sasha would miss out on a big surprise treat if she betrayed Lily.

This role-play helped Sasha express the nuances of the friendship drama through action. It was simply beyond her abstract verbalizing abilities at that point. It certainly made Sasha feel better and also warned the author about problems she would have to help Sasha with later.

Now, while role-playing might work well with little kids, an entirely different approach is needed when parenting teenagers. Silent listening is a great method here.

Take Kirsten, a therapist and friend of the author. She has a teenage son, Jack. Each spring, Jack went through a bit of a grumpy period. He grew morose and refused to talk.

In response, Kirsten introduced a new ritual. They would go on a walk together. She allowed him to walk ahead and rave while she just listened in silence. She had learned that whenever she tried to contribute something, Jack would just withdraw into himself. However, if she just listened quietly until he had finished, he would calm down, and they could walk home in peace.

### 9. Sex can be problematic after childbirth, and some patience might be required until you find a solution. 

We all know giving birth can be anything from wonderous to torturous, and that each experience is unique in its own way.

As for the author, she had to have an _episiotomy_. That's a surgical procedure required when more space is needed for the baby's head during labor. During this procedure, an incision is made in the perineal area.

But her problems didn't end there. Even after the birth was over and her stitches were healed, the pain refused to subside. For a year after the birth, the notion of sex was almost unimaginable — it felt as though her vagina was being stabbed.

The doctors weren't troubled by this at all. They'd advised her that her scar tissue would, in due course, stretch, making sex possible once again. But the author's revulsion over sex got stronger and stronger. So much so, that even seeing couples kissing annoyed her and reminded her of the pain in her vagina.

As a result, the author consulted several doctors. At worst, they were nonplussed, at best, unhelpful.

One of them wanted to make a new incision with the aim of breaking up the scar tissue. But surely, the author thought, that would just result in more scar tissue. The next doctor's recommendation was impractical: he was convinced that the solution was to have another baby, apparently because giving birth again would stretch the scar tissue more efficiently!

This suggestion annoyed the author no end. She was in no place to start thinking about having a second baby while she was still confronting the complications from the birth of her first.

It took time for the author to find the solution. That's something you should bear in mind when facing sex trouble after childbirth. Sometimes it just takes time.

For the author, it was three years. But finally, she consulted a competent gynecologist whose diagnosis was worth something. It was revealed that the author had a _neuroma_, a clustered knot of nerve endings beneath the skin. It's a fairly common side effect of surgeries that produce scar tissue, so it should have been diagnosed much sooner.

Although the extraction of the neuroma proved somewhat painful, gradually the author's sex life recovered to normal.

Remember: it's important to find a good gynecologist and get several opinions until you're happy with the care you're getting.

### 10. Final summary 

The key message in these blinks:

**Parenting may seem like an impossible task, but if you're prepared to get weird and creative, it doesn't have to be — it can even be a lot of fun. Children don't think about the world as adults do, so rational debate is unlikely to get you anywhere. No amount of logical finesse will get your child to eat their vegetables — but novelty and humor will. Whether you're using banishment lists to stop ghosts from entering your child's room or using the hum of an electric toothbrush to get your baby to sleep, sometimes it's the wildest and most desperate moves that solve the problem.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Happy Kid Handbook_** **, by Katie Hurley, LCSW**

You've been won over by Hillary Frank's argument that zany approaches to parenting are not only fun but can also yield results. They will foster a more joyful and fun relationship between you and your children.

This is where the book in blinks to _The Happy Kid Handbook_ comes in. This will also give you some practical advice for raising happy children. But its goal is focused on one specific challenge: How do you parent well in a stressful world? We all want kids to thrive, so no matter whether your child is introverted or extroverted, these blinks will guide you through helping them understand stress, negative emotions, social relationships and the importance of finding calm in their lives.
---

### Hillary Frank

Hillary Frank is a media professional whose career took off when she was working for the podcast _This American Life_. She followed up by creating her own award-winning parenting and family podcast, _The Longest Shortest Time_. Hillary Frank also writes novels for teenagers and young adults, including _Better Than Running at Night_ (2002) and _I Can't Tell You_ (2004).

