---
id: 5831a3e88478e30004aeff27
slug: were-you-born-on-the-wrong-continent-en
published_date: 2016-11-21T00:00:00.000+00:00
author: Thomas Geoghegan
title: Were You Born on the Wrong Continent?
subtitle: How the European Model Can Help You Get a Life
main_color: D18646
text_color: 9E5415
---

# Were You Born on the Wrong Continent?

_How the European Model Can Help You Get a Life_

**Thomas Geoghegan**

_Were You Born on the Wrong Continent?_ (2010) examines the true nature of the American economy as revealed by an eye-opening investigation that compared living standards in the United States with countries in Europe. By examining differences in health benefits and employee rights, among other issues, these blinks reveal how European nations such as Germany have quietly taken the lead when it comes to economic health and prosperity.

---
### 1. What’s in it for me? Discover why policy in the United States could benefit from a little social democracy. 

If you follow certain news outlets, you might think the countries of Europe are on the verge of economic collapse, ruined by the irresponsible policies of socialist-leaning governments. Has France become the new North Korea, you wonder, only with better wine and chocolate?

So you travel to Europe to witness this economic destruction for yourself, but when you land, what you see doesn't add up. Instead of crushing poverty, there are thriving boutiques, schools that are clean and successful, packed cafés abuzz with ordinary people enjoying ample free time.

So what's going on? Where is this so-called socialist collapse, and why do Europeans look so contented?

These blinks will help you to make sense of what's happening in Europe, and how European policy norms differ from what's standard in the United States. You'll learn how social democracy can not only improve a citizen's quality of life but also even stabilize struggling economies.

And if you're European, you'll understand why you were born on the "right" continent, after all.

In these blinks, you'll discover

  * why higher wages can harm your health if you live in America;

  * how to make your country (appear) wealthier by using air conditioning; and

  * how living in a social democracy is related to reading newspapers.

### 2. While doomsday predictions about Europe dominate headlines, the real story is in the travel section. 

While newspapers love to spin the sensational, there's nothing that shocks and surprises better than first-hand experience.

A case in point is Europe: there's a big difference in the way the continent is portrayed in the media, and how things are on the ground.

From _The Wall Street Journal_ to the _New York Times_, US newspapers have warned readers of Europe's "imminent" financial collapse for decades. Business columnists cite high unemployment rates, low wages and high labor costs as the telltale signs of the continent's coming collapse.

It seems that failing European nations are no match for a strong America and its impressive GDP. Even the country's poor are thought to be living better lives than the poor in Europe.

As a result, many Americans imagine that Europe is a gloomy place, filled with crumbling buildings and downtrodden citizens. But this couldn't be further from the truth.

The author discovered this personally while traveling through Zurich. Much to his surprise, this Swiss city's buildings were beautiful, its streets pristine. Citizens were good-natured and content.

It was an experience of Western Europe that you probably wouldn't hear about in the pages of an American newspaper — except in the paper's _travel_ section.

The _New York Times_ travel section is filled with praise for European cities and the European lifestyle. From the warm beauty of Barcelona to Copenhagen's vibrantly cool character, American travel columns paint a vivid picture of the true condition of the European economy — describing an aesthetic that American cities just can't match.

If you want to learn about the real state of Europe, don't read the financial pages but turn to the travel section!

But what about the American state of affairs? As you'll see in the next blink, statistics on taxation and public benefits reveal much about the lifestyle gap between Europe and America.

### 3. We all pay taxes, but Europeans receive far more benefits than Americans for tax paid. 

In general, European citizens pay higher taxes. Americans, on the other hand, can rest easy knowing that the government doesn't want that large a share of their hard-earned income.

But are higher taxes, in general, such a bad thing?

For most Europeans, taxation has its benefits, as this money is essentially returned in the form of generous holiday time, well-paid maternity and paternity leave, reliable healthcare and free education.

Europeans on average also work less than Americans. According to a 2007 OECD report, the average American works 1,804 hours per year while the average German works just 1,436 hours.

Essentially, Europeans have more leisure time to enjoy all the benefits that taxes make possible.

While Americans only pay four-fifths of the tax paid out by Europeans, the benefits they receive in return are few. A much smaller amount of tax returns to citizens as social benefits. Most of the money goes either to the private sector or the military.

Of course, the United States sells a significant amount of weaponry to foreign countries. This _should_ mean that the government earns enough to correct the imbalance between what American citizens pay out in taxes and what they receive in return.

But things don't quite add up that way.

For example, Americans have it tough when it comes to issues such as retirement benefits. In 2004, the OECD found that the average public pension across the top 20 developed countries was 67 percent of an individual's working income. In the United States, however, this figure was only 39 percent

Meanwhile, American's taxes go to private industries such as insurers, pharmaceutical companies and private hospitals that profit from what should be a public good.

And alarmingly though perhaps unsurprisingly, the lower middle class in America is steadily sliding into poverty. The number of children and elderly living below the poverty line is double that of Western European countries.

According to the 2000 Luxembourg Income Study, 24.7 percent of elderly individuals in the United States were below the poverty line, compared to just 10.1 percent in Germany. For children, the numbers were 21.9 percent and 9 percent, respectively.

This goes to show that taxation alone is no indicator of a nation's health. How society benefits from the tax system is what counts.

### 4. European professionals enjoy higher living standards than their counterparts in the United States. 

What's life like for an average, middle-management European professional? Turns out it's pretty good, starting with the worker's commute to the office.

European countries support far more effective and affordable public transportation systems than do US states. Because public transport works so well, European commuters are ten times more likely to use local transit to get to work than US commuters are.

And rather than sitting in traffic every morning and suffering road rage as so many Americans do, Europeans get exercise during their commute. Many people live close enough to the office to ride a bicycle to work, which makes staying healthy a lot easier.

In contrast, few Americans have the opportunity to ride a bike to work, as so many people live in the suburbs, far away from city-based offices.

But why do Americans choose to live in the suburbs? Often the more money a professional earns, the bigger the house he or she wants to have. And the bigger the house, the further away from the city center it likely is. This results in not only longer commutes but also huge mortgages and high utility and gas bills.

To maintain such a high-cost lifestyle, Americans need to work longer hours. They feel insecure in their jobs, worrying that any sign of laziness might lead them to miss a promotion or worse, be fired. Europeans, on the other hand, have clearly defined working hours and greater job security, which allows them to comfortably work a set amount of hours each week.

On top of this, European professionals enjoy greater long-term benefits. While European pension rates have decreased slightly in recent years, Americans witnessed a sharp fall in the value of their 401(k) plans (an employer-sponsored retirement savings plan) amid the economic downturn. Many workers today fear that federally funded Social Security benefits too may not be available when they finally retire.

What's more, education in Europe is far more egalitarian. Young European undergraduates enjoy practically free tuition, while higher education in the United States comes at a notoriously prohibitive cost.

While the United States has long been touted as the land of opportunity, this may not hold true for everyone.

### 5. Statistics like GDP per capita can give a false impression about the health and wealth of a nation. 

Taking a look at nothing but the numbers and it seems straightforward: The United States is statistically better off than other developed nations, especially those in Europe.

But when you take a closer look at American GDP figures, for example, you'll find that some factors bloat oft-cited prosperity statistics.

Gross domestic product (GDP) per capita is a measurement that helps to demonstrate a level of economic health between countries. In 2006, GDP per capita in the United States was $44,155, while in Germany, it was $35,270.

However, there are certain events specific to the United States that boost GDP in general, which can explain the perceived purchasing strength of the average American.

Extreme weather is one such event. As more Americans move to southern states and temperatures in general continue to rise, an increase in energy consumption — whether oil, coal or otherwise — boosts America's gross domestic product, and in turn, American GDP per capita.

So of course Americans aren't wealthier because they use tons of energy to run air conditioning units; but that's the story such statistics tell, if you don't know the details behind the numbers.

Another statistic that is often cited is a country's unemployment rate. This too can be misleading, particularly when comparing rates in the United States to European ones.

According to a 2006 report by the International Labor Organization, unemployment rates for college graduates were 2.5 percent in the United States and 5.5 percent in Germany.

These numbers, however, don't tell the whole story: in the US, one out of every five workers with a bachelor's degree works at a blue-collar job — a position below the worker's qualification level — because of a lack of suitable employment opportunities. Taking this into account, the true unemployment rate of Americans with a bachelor's degree at the time of the report was closer to 20 percent.

In general, US college graduates also suffer from a greater debt load than do European students because of high tuition rates. Such statistics just don't capture the dearth of jobs and crushing debt with which so many US graduates must grapple.

So life in the United States seems harsh. But what are the alternatives? Let's take a closer look at one particularly successful European nation.

### 6. The “German miracle” has powered on, through global recession and competitive threats. 

"Rhineland capitalism," or social capitalism, has long been regarded as opposing the tenets of American capitalism. To understand what makes Germany's social democratic system and economy different from others, let's first compare Germany with another European country, France.

When French workers are dissatisfied, powerful workers' unions encourage members to take to the streets, often resulting in violence. Some actions have even led to employees imprisoning their bosses in the office when demands aren't met.

Germans, on the other hand, strike less frequently, and when they do, workers and owners handle disputes with a greater degree of professionalism and far less upheaval. Why is this the case?

Germany's government runs a "weak" state by design, and as part of government policy, there are few regulations put on private corporations. Thus disputes are more often than not settled within companies, in negotiations with union representatives and workers.

This is just one aspect of Germany's social democracy that is admired throughout Europe. In fact, within the European Union, Germany is a political leader in a much broader sense.

Formerly divided by the Iron Curtain, Germany after 1989 reunified as a single nation — and in doing so, became the largest country in the emerging European Union, based on population.

As the European Union struggles to manage cultural and economic tensions with former Eastern Bloc member states, EU bureaucrats are looking to Germany to help find that elusive balance between free-market capitalism and state-sponsored socialism.

Many experts globally think of China as a leading industrial power. But in many ways, this power is in Germany's hands. Since 2003, Germany has claimed the highest global export sales, occasionally tying with China, while America is in third place. And German workers have achieved this with six weeks of vacation time, don't forget!

It should be noted that as of 2009, China was positioned to surpass Germany in export sales, but this was mostly because of the global recession and rise in the value of the euro.

German companies in general don't stress about how to beat unions or to compete with others on cost. Instead, they focus on offering high-end, high-value goods and engineering services. Demand for German products and services is and remains high because few other countries offer products and services at such a consistently high level of quality.

German products now dominate the markets in which US companies once ruled. Thus the "German miracle" presents itself as a significant rival to American labor.

Social democracy in Germany seems to be the winning policy for growth and prosperity. So where did it come from? The answer might surprise you.

> _"People blow up over Germany because, for all its left-wing bent, it may be a more plausible form of capitalism than ours."_

### 7. The seeds of Germany’s thriving social democracy took root in the years after World War II. 

Many politicians in the United States sniff at Germany's economic system, disdainful of its "socialist" roots. Yet the seeds of social democracy in Germany were planted by the Allies after World War II.

At the end of the war, the people responsible for planning a sustainable economic future for the new West Germany were decidedly pro-labor. Many were either socialists from the British Labour Party or _New Dealers,_ Americans who advocated liberal social ideas. Meanwhile, US Generals also welcomed the idea of workers having a say in how the country was rebuilt. This principle has remained at the core of German business practices, even today.

German workers still have a seat at the negotiating table when important decisions are made by a company. It is this approach that makes German social democracy successful.

Let's take a closer look at how such social democratic organizations are structured.

There are three major components: the _works council_, the _co-determined board_ and _regional wage-setting institutions_.

The works council is made up of on-location employees. Employers must consult the works council on issues such as shift assignments and layoffs before any decisions are made.

The co-determined board, as the name suggests, is the company's board of directors which is made up of members selected by both employees and shareholders. Employees elect half the board and these members have the same voting rights as directors. If there's an impasse, the chairman, elected by shareholders, can cast the deciding vote.

Regional wage-setting institutions are unions that negotiate for uniform wages and pensions in a given region. This means that workers who perform the same job within different companies can expect the same wages and retirement benefits.

In this system, employees, shareholders and business leaders alike have a role to play and an equal voice to express concerns. This fair system has inspired trust between workers and management.

So why isn't a system like this implemented in companies and countries over the world?

If workers want an equal voice in company decisions, they need to be informed about current affairs. Unfortunately, as you'll discover in the next blink, this may not be the case everywhere.

### 8. The average German worker needs to stay engaged and informed about politics and the economy. 

As media outlets go out of business in the United States, printed media continues to thrive in Europe, as any German train full of reading commuters will demonstrate. German citizens still consume a large percentage of daily news and current affairs from newspapers or magazines.

Frankfurt, one of Europe's major cities for business, is located just a few hours by train from other financial centers such as Berlin, Paris or Amsterdam. Many professionals travel between these cities daily, the perfect opportunity to read a newspaper cover to cover.

And while many US tabloids are filled with sensational, bold headlines and colorful pictures, newspapers in Germany tend to be printed with lots of dense text and few images.

Although statistics point to a drop in newspapers' paid circulation numbers in Germany, new free newspapers are being introduced, showing that readership is still on the rise.

In general, paid circulation numbers are still far higher in Germany than in the United States. While US newspapers claim a paid circulation of 34 million (considering a total population of over 300 million), Germany, with its much smaller population of 80 million, boasts a paid circulation of some 23 million.

What explains the German love for printed media? Simple — keeping up with current events is just part of the culture. Compared to Americans, Germans constantly read to stay abreast of political and economic developments.

While the manufacturing industry in the United States and Britain, for instance, has been declining over the years, the sector is still going strong in Germany, which is why political and social developments, such as union news or a sector's gains or losses, can have an enormous impact on the workforce.

To make the most of their deciding power, workers ensure they stay in the loop. A transparent, productive society depends on an engaged, informed population. Citizens who read and discuss articles, rather than gossip over tabloid revelations, support a thriving system.

Of course, reading the news isn't always pleasant. No doubt this was the case for German workers in the late 1990s, when the economy tanked. Find out more in the next blink.

### 9. Even as the economy struggled, the German system protected its citizens and built for the future. 

The German economy today is robust. But in the late 1990s, the picture was very different.

At the time, the economy seemed close to collapse, with the reunification of East Germany and West Germany adding to the enormity of the crisis.

The root of the problem went back to 1989, when some 4.5 million Germans were unemployed, the highest level of joblessness seen since the Weimar Republic. Because of a lack of jobs, many young Germans instead decided to live at home and pursue a higher education degree. But the students were deemed "too old" to hire once they eventually graduated, and companies brought on younger, educated British workers instead! This meant unemployment rates were still high even in the late 1990s.

Supporting a large number of educated but jobless citizens, the German state felt the strain as it paid out millions in unemployment benefits.

To add to the troubles, West Germans feared that reunification was making things worse. They felt that their taxes were only supporting East Germany's failed economy, with no end in sight.

If Germany faced the same challenges today, the state would have two options: to run a deficit or cut benefits. Yet at the time, the European common currency was just starting to be introduced. Running a deficit was prohibited within the currency bloc, which left cutting benefits as the only option.

Despite the painful cuts, there were clear signs that German industry would survive and remain strong.

Labor laws prevented companies from abandoning manufacturing for more lucrative returns in the banking or finance sectors. These limitations allowed Germany to not only recover but also thrive, thanks to its prosperous manufacturing industry.

The introduction of the euro further benefited the German economy. German manufacturers no longer suffered from the German mark fluctuating against rival currencies such as the French franc or Italian lira.

And all the while, thanks to Germany's social democracy, crucial public goods, from healthcare to education, have remained affordable despite times of economic pressure.

How did Germany secure these sectors? Because things such as healthcare and education are protected as public goods, that is, the state prevents private vendors from demanding exorbitant prices for them.

### 10. When it comes to dealing fairly with citizens amid a financial crisis, Germany has got it covered. 

The dark cloud of the 2007-2009 financial crisis still looms over North America, as the economy continues to struggle with enormous levels of debt.

Like the United States, Germany holds a large external debt; yet the country is technically debt-free. How? Germany is a net creditor, meaning its external assets are large enough to balance out its external liabilities.

How did Germany manage this? Basically, the country's high-quality manufacturing exports were in demand even during the economic crisis.

Today, creditor nations such as Germany, South Korea and China consistently outperform the United States in global markets.

It's also worth noting that Germany is not only in better shape than America to handle a future crisis — Germans are also more optimistic.

The author spoke with young Germans living in Berlin, who have plenty of free time to relax and socialize in bars or cafes. The consensus was that American capitalism was "over." The German system, on the other hand, had served them well and could be trusted to do so in the future.

In an economic crisis, many American citizens face the harsh reality of losing their jobs and falling into poverty. German citizens enjoy a far more robust social safety net, from free movement of labor within the European Union to a well-funded pension system.

Germany may even lead the next industrial revolution, according to the CEO of Siemens, a German multinational company. Green technology is just one arena in which Germany is leading the charge, as German companies manufacture much of the machinery used in renewable energy systems.

In contrast, the United States can't afford to invest in industry development until it manages to find a way out from under its crippling debt load.

No country can claim to have a guaranteed secure and safe future. Yet Germany's blueprint for success has served it well. Although European countries have taken a cue from Germany's successes, across the Atlantic Ocean, America still seems reluctant.

### 11. Final summary 

The key message in this book:

**Americans are constantly told that their economic and social system is the world's best. But contrast the US economy with that of Germany, and that claim is thrown into question. Germany's brand of social democracy has allowed it to lead the world in manufacturing, outperforming America in the global market while** **at the same time offering citizens a high quality of life.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Empire of Illusion_** **by Chris Hedges**

_Empire of Illusion_ (2010) offers a close examination of declining literacy levels in the United States, and the disastrous effects that this educational catastrophe is having on the country. These blinks will explain how TV is pacifying the US citizenry, how corporate power has taken over the country and what this means for the future.
---

### Thomas Geoghegan

Thomas Geoghegan is an American labor lawyer. Based in Chicago, he is the author of several books and articles on labor law and politics.

