---
id: 58a04e5ecb5c2800040315fc
slug: what-do-women-want-en
published_date: 2017-02-15T00:00:00.000+00:00
author: Daniel Bergner
title: What Do Women Want?
subtitle: Adventures in the Science of Female Desire
main_color: 921D20
text_color: 921D20
---

# What Do Women Want?

_Adventures in the Science of Female Desire_

**Daniel Bergner**

In _What Do Women Want?_ (2011) Daniel Bergner casts doubt on common preconceptions about women's desires. Drawing on history and recent scientific research, Bergner exposes the myths about, and the ingrained societal norms that often dictate, women's sexuality. We may only just be beginning to uncover what women really want.

---
### 1. What’s in it for me? Come closer to understanding what women really want! 

Who can honestly claim knowledge of what women truly want? Obviously, no one. Women themselves can't even say for sure. The question is simply too broad.

So what if we narrowed it a bit? What stands out when we compare the desires and sexuality of women to those of their male counterparts?

If, right about now, you feel like you're lost in a wilderness of question marks, don't worry! Most people find this topic baffling at best. But what if we could make more sense of all this mystery?

With _What Do Women Want_, the author seeks to unveil the secrets of women's sexuality, claiming that women aren't actually all that different from men when it comes to those heartbeat-quickening, lust-inducing, brain-befogging… well, things.

In these blinks, you'll discover

  * what Freud thought of the stimulation of a woman's clitoris;

  * how women feel about copulating bonobos; and

  * why the sexuality of women has been repressed for so long.

### 2. Social norms often dictate women’s sexuality – and inevitably simplify it. 

Throughout history, women's sexuality has never received the attention it deserves. This, in large part, is because the story has always been told from a male perspective. What we claim to know about women and desire, therefore, should be taken with a liberal pinch of salt.

Ancient texts and religions reveal that the repression of women's sexuality is pretty much coeval with its arrival. Just take Greek mythology, according to which Pandora, the first woman, is responsible for unleashing all of the world's evils.

Later, in the sixteenth century, male scientists discovered the role of the ovum for reproduction; after this discovery, the female libido was regarded as being of minor importance. Because women could conceive regardless of whether they felt desire, men concluded that there was little reason to pay attention to female pleasure.

As well as being repressed, women's sexuality was, in more recent history, mislabeled.

Assuming that genes regulated women's behavior, evolutionary psychologists claimed that genes made women desire security in relationships. Thus, evolutionary psychologists developed the _parental investment theory_, which states the following: because men have unlimited sperm and invest little effort in reproduction, and because women have a finite number of eggs and do more work in the reproductive process, men are programmed to spread their seed, while women must carefully select long-term providers.

Nowadays, such claims go more or less unquestioned. Women are supposedly the more restrained sex, a stereotype that just encourages women to behave in the way we think they should.

The problem is: whether it's through religion, social convention or science, women are told how to feel and act. It's a combination of these influences that perpetuate the status quo.

### 3. When it comes to sex, men may be animals – but women are, too! 

Men and women may not be as different as we thought. Men are sometimes compared to animals tamed by society, creatures that express their true nature in socially acceptable ways: watching pornography, casting predatory glances at women on the street.

They are even said to be easily lured by the lowest regions of their brains, much like animals are. In this regard, women enjoy a far more respectable status, as they're seldom compared to animals when it comes to sexuality.

On the contrary, women are said to long for emotional safety and monogamy — the polar opposite of the wild and restive desires of men.

In reality, however, the range and power of women's desire is vastly underestimated — if not completely misunderstood. And science is only starting to scratch the surface of it.

Take Dr. Chivers, a scientist specializing in sexology. In one study, Chivers intended to debunk the myths surrounding the female libido. She conducted an experiment with the help of a plethysmograph — a light sensor that, placed inside the vagina, measures women's reactions to provocative footage. The women watched a variety of scenes: heterosexual and homosexual intercourse, masturbation and even bonobos mating.

The conclusion was eye-opening: women, regardless of sexual orientation, were aroused by all kinds of footage, even images of copulating apes. Men, however, reacted along more predictable lines: they were only aroused by images compatible with their sexual orientation, and had no interest in the mating bonobos!

Even more interestingly, the study showed that women often misunderstand their own desire.

During the experiment, a keypad was given to the participants to rate their arousal. It turned out that the ratings given to each scene actually went against the sensor's results. Simply put, women reported complete indifference to the bonobos, while their bodies demonstrated obvious signs of arousal. This wasn't true for the men, whose bodies and ratings were in sync.

### 4. Women’s sexuality is complicated by anatomy and social environment. 

Many arguments have been put forward to explain the differences between men's and women's awareness of their own sexuality.

Anatomy is one factor that could lead us toward an answer. From their teens onward, men can clearly tell when they're aroused because, typically, penises grow and shrink in accordance with sexual excitement. An erect penis presses against clothing in a way that is impossible to ignore — both for the man and the object of his interest.

A man's body almost always informs him of whether he's sexually interested in someone. And this feedback loop between the body and mind helps men understand their own desires.

For women, however, it's completely different. Indeed, their anatomy often sends messages that are quite difficult to interpret.

In addition to anatomy, social environments also influence women, making it even harder for them to decipher their desires.

One Ohio State University study asked two hundred undergraduates about their pornography consumption and masturbation habits. The participants were split into two groups: the first completed a questionnaire with the assurance that it would remain private, while the second were instructed to hand in their answers to another student who could potentially read its contents. While men's answers were approximately the same in both groups, women in the second group, unlike women in the first, reported that they neither masturbated nor watched pornography.

We're also taught that, for women, sex is centered around an emotional bond. But one study showed otherwise. Dr. Chivers, in another experiment, this one involving pornographic audio tapes, discovered that, out of several scenarios, one triggered particular arousal among women: X-rated stories involving strangers.

The results showed that women's desire doesn't necessarily fit into societal standards and seems to debunk the assumption that the female libido thrives on emotional connection alone.

### 5. Observing animals provides a window into women’s desire. 

How much do you think sex among monkeys resembles sex among people? Perhaps it's similar, with the male initiating sex and the female displaying hesitancy? Well, actually, these roles are sometimes inverted in the animal kingdom.

Often, it's the females who go after sex and display bullying behavior, coercing males into sex. Take the rhesus monkey: in the 1950s and '60s, rhesus monkeys were sent into space to see if humans could survive the journey in orbit. These monkeys were under scrutiny for years, and what researchers found was that female rhesus monkeys select males and invite them to copulate until their interest dwindles and they dismiss them. In addition, they don't just mate when they're ovulating — they have plenty of sex, regardless of their hormonal state.

Animals also provide evidence that desire and pleasure, not just reproduction, may play a role in mating.

In several cases, animals attach to their partner, begin mating, detach and then join together again until the male ejaculates. Experiments have shown that this type of behavior is initiated by females, who seem to want to draw out the mating process. We can see this in rats, where the females frequently run away from their partners while mating — not to avoid copulation, but to prolong it.

Unlike the monkeys and rats, many women don't give free rein to their desire; however, a few signs among women in recent studies have indicated more male-like behavior.

For example, Nielsen, a company that tracks consumer behavior, recently issued a report stating that one in every three porn users was female. This report, in addition to the fact that you can now find vibrators at mass retailers like Walmart, may signify a new trend of sexual awareness among women.

### 6. Social norms regarding women’s sexuality may explain the development of certain types of fantasies. 

The complexity of women's desire can be explored through their fantasies. The author claims that, as shocking as it may sound, women sometimes fantasize about assault and coercion — a trick that triggers arousal.

Over the last four decades, nine different surveys asked women if they ever fantasized about being overpowered or coerced into sex. The latest results were published by sexologists J. Bivona and J. Critelli, from the University of North Texas, in the _Journal of Sex Research_ (2009). Gathering testimonies from 355 college women, the study revealed that about 62 percent of these women had had such fantasies at least once.

One possible explanation for this, provided by M. Meana, professor at the University of Nevada, is that being desired is central to women's libidos. According to Meana's research, women's sexual drive depends heavily on the extent to which they feel desired by their partner. So, these women's fantasies involve an extreme instance of this: an offender literally willing to break laws and social mores to have sex with them.

Another theory posits that fear may play a role in the arousal of certain women. In an amusement park experiment carried out by C. Meston, a University of Texas psychology professor, hundreds of participants rated the _dating desirability_ of pictures of the opposite sex, before and after a ride. These scores rocketed after the ride, thanks to _excitation transfer_ : a rise in sexual arousal that, due to overlapping brain circuits, is stimulated by the experience of fear.

Finally, Bivona and Critelli theorize that these fantasies help women to eradicate guilt from their desire. Particularly in cultures where sex is a taboo topic, some women may find a way to embrace their own sexual drive through such fantasies, while avoiding the shame imposed on them. For instance, they might fantasize about being raped, because this way they have no reason to be ashamed of wanting sex.

### 7. Women’s desires have deep psychological components, and monogamy might not be their ideal form of relationship. 

Men have Viagra, but what do women have? With pharmaceutical companies making billions from assisting men with erectile dysfunction, the industry has since been scrabbling to find an equivalent drug for women.

Exploring the psychological and physical issues surrounding desire, researchers have uncovered some fascinating results. They found that women's sexual issues are usually more psychological than genital or hormonal. One Australian study even ruled out the assumption that women's loss of desire is hormonal. It showed that the thrill of a new relationship could easily cancel out hormonal effects, and concluded that the reason for a woman's lack of desire may simply be that she's been with the same partner for a long time. In addition, a German study of committed relationships found that women's desire tended to fade more rapidly than men's.

Again, the theory of women being more aroused when they are the object of desire seems to provide part of the explanation. Within the confines of fidelity, a woman feels that her partner is trapped and that she is no longer the object of desire: in other words, her partner no longer has any choice, and so the feeling of being desired dissipates.

Beyond this psychological explanation, evolutionary reasons may also shed light on why a woman's desire might wane. In one study, anthropologists and primatologists observed monkeys, and found that, in India, male langur monkeys commonly committed infanticide against other monkeys' offspring. To prevent this from happening, female monkeys evolved to behave promiscuously, as a means of hiding the paternity of their young.

Overall, monogamy may be an unnatural choice for female animals, and it may therefore evolve into engaging in non-monogamous relations. Whether this is also the case for women remains to be seen.

### 8. Both psychological and physical phenomena surrounding women's sexuality are heavily debated. 

Women's bodies and sexuality have been at the center of many discussions and studies throughout the twentieth century. And until Freud's theories on the topic were challenged in the 1970s, women's sexuality had been largely connected with psychoanalysis.

Freud compared female clitoral stimulation to burning _pine shavings_ and vaginal stimulation to a _hardwood fire_, claiming that women focused only on the clitoris have an immature approach to their sexuality.

In the 1970s, feminists argued that if a woman's pleasure was solely dependent on the vagina, then it was connected to men's satisfaction, which repressed women socially, economically and politically. Thus, the clitoris turned into a weapon by which women could assert their independence from men.

In the 1980s, focus shifted from the clitoris to the G-spot. Though it's been studied since the 1940s, it still remains a mystery. During the 1980s, a book on the topic stated that it was to be found along the interior of the vaginal front wall. According to researchers, it induces powerful orgasms, even though its existence hasn't yet been scientifically proven.

The scientific debate over whether the G-spot is a myth or a reality still continues. Researchers who doubted its existence studied identical twins — who ought to share the same physical experience — and found that these twins weren't any more likely to both report having a G-spot than fraternal twins or regular sisters. Proponents of the G-spot's existence, on the other hand, studied paraplegic women and claimed to be able to isolate nerves distinct from the damaged spine that connected the G-spot to the brain.

So who's right? Difficult to say — but one thing is certain: some women are able to orgasm without the assistance of physical touch, literally thinking themselves to climax. Whether or not this ability involves the G-spot is anyone's guess, but it does seem to prove that the brain is a sexual organ as powerful as any other!

### 9. Speed dating offers interesting insights into the mechanisms of seduction. 

Dating events are fascinating opportunities for researchers to study seduction patterns. One such event is speed dating.

Speed dating appeared in the 1990s in Los Angeles, when a rabbi had the idea to make it easier to match potential partners. The setup is simple: you need a large room, tables and chairs and a group of men and women. Each woman sits at a table. Each man then sits at a table for a four-minute conversation. When his time is up, he stands up and moves to the next table.

At the end of the night, both the men and women submit their verdict for each partner: "yes" if they want to meet again, "no" if they don't. Then, partners that match are put in contact with each other.

In this process, women are far more selective than men, which is no surprise to those in the field of evolutionary psychology. In fact, research on speed dating shows that, as far as second dates go, men are much more likely than women to say yes. Women seem to be far pickier. This seems to corroborate the theory forwarded by evolutionary psychologists: that men are wired to inseminate as many women as possible, while women have evolved to choose their mates very carefully.

However, a new approach to speed dating recently cast doubt on this conclusion. One study had the women move from table to table, while the men remained seated. Oddly enough, when women were the ones to move around, they said "yes" to a second meeting just as frequently as men did. That is, women became just as desirous as men!

So, what to make of all this?

The tangle of assumptions and unproven theories can only mean one thing: the sexuality and sexual desires of women still remain, for the most part, a mystery.

### 10. Final summary 

The key message in this book:

**These blinks shatter the myths and stereotypes about women's sexuality and desire. Contrary to popular belief, women aren't the less desirous sex; they may be as or even more sexually driven than men. Having said that, women's desire remains a largely uncharted territory. There's still a great deal of exploring to be done.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Sex at Dawn_** **by Christopher Ryan and Cacilda Jethá**

_Sex_ _At_ _Dawn_ argues that the idealization of monogamy in Western societies is essentially incompatible with human nature. The book makes a compelling case for our innately promiscuous nature by exploring the history and evolution of human sexuality, with a strong focus on our primate ancestors and the invention of agriculture. Arguing that our distorted view of sexuality ruins our health and keeps us from being happy, _Sex_ _At_ _Dawn_ explains how returning to a more casual approach to sex could benefit interpersonal relationships and societies in general.
---

### Daniel Bergner

Daniel Bergner is a writer for _The New York Times Magazine_, and has authored several nonfiction books, including _The Other Side of Desire_ and _In the Land of Magic Soldiers: A Story of White and Black in West Africa_. His work has also appeared in _The New York Times Book Review_, _Harper's Magazine_ and _The_ _Atlantic_.

