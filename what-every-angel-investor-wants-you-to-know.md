---
id: 552bdff06466340007930000
slug: what-every-angel-investor-wants-you-to-know-en
published_date: 2015-04-15T00:00:00.000+00:00
author: Brian S. Cohen and John Kador
title: What Every Angel Investor Wants You to Know
subtitle: An Insider Reveals How to Get Smart Funding for Your Billion-Dollar Idea
main_color: C4A24C
text_color: 91701C
---

# What Every Angel Investor Wants You to Know

_An Insider Reveals How to Get Smart Funding for Your Billion-Dollar Idea_

**Brian S. Cohen and John Kador**

_What Every Angel Investor Wants You to Know_ (2013) offers start-up entrepreneurs tips on what to look for in an investor — and how to actually get an "angel" to invest. With solid preparation and a good understanding of what motivates an investor, any entrepreneur can secure financing for her next big idea.

---
### 1. What’s in it for me? Discover how to attract an angel investor and fund your start-up. 

If you want to sail around the world, you better make sure you've got the right people with you in your boat. If you want to start a company, the strategy is much the same.

Every start-up needs to find an investor, but finding a good one can be tricky. Many investors are just looking for a quick return on an investment, and have little to bring to the table beyond cash.

What a smart entrepreneur should be looking for is an _angel investor_. Although many angel investors are not as flush as billionaire Bill Gates, they are instead motivated by making dreams come true for hard-working start-ups all over the world.

Do you have a dream that needs a financial boost? Then these blinks will help you find your own angel investor!

In these blinks, you'll learn

  * why an investor is like a spouse;

  * why you should learn to take "no" for an answer; and

  * how a start-up founder got an angel investor to wear his underwear.

### 2. Smart entrepreneurs look for smart money. 

Most people consider starting their own business at some point in their life, be it mowing lawns or starting a multimillion-dollar business.

But no matter what the idea, they all have one thing in common: Start-ups need capital to get started.

For most entrepreneurs, this means finding an investor. And if they're lucky, they'll find an _angel investor_.

An angel investor funds start-ups with her own capital in return for partial ownership of the project. Such a person typically possesses assets valued at over $1 million, has an annual income above $200,000 or a joint income exceeding $300,000.

Considering these requirements, only three percent of Americans qualify as angel investors!

Angels differ from regular investors in that they offer more than just money to a project. While it's certainly true that angel investors write checks, they also contribute to your business by investing their time and sharing their experience and valuable connections.

Typically, angel investors have invested in many enterprises, and in doing so, have acquired tons of experience. They can share with you their insights and potentially save you from early mistakes.

Angel investors also tend to gather together. Finding one who is truly committed to you and your business idea means he could introduce you to other potential investors as well.

Unfortunately, the primary goal of many entrepreneurs is securing funds as quickly as possible. In their haste, they often strike a deal with the first investor that comes their way.

Remember that your relationship with your investor is a long-term commitment. Think of your investor like your spouse: you'll be together for a long time and you'll also share assets.

Because of this, you need to find someone who won't jump ship when things turn sour. You need someone who will stand by you and your business, through thick or thin.

In other words: you need an angel investor!

But how exactly do you find an angel? The following blinks will reveal how.

> Fact: Every year, 250,000 angel investors fund 50,000 start-ups in the United States.

### 3. Enticing angel investors means developing the perfect pitch. 

Successful entrepreneurs have no patience for people who are shy. You have to get out there and talk to people, inspiring them to believe in your business as passionately as you already do.

And you have to try even harder to make an angel investor believe, as they've heard it all before!

Angel investors don't fall from the sky, despite their whimsical name. You have to approach them, and do so in a way that speaks to them on a personal level.

To do this, you'll need to be prepared. Do some research about your potential investor and learn at the very least some basic facts. What have they been investing in? What are they looking for in potential investment opportunities?

Don't ever come across as unprepared, or even worse, uninterested in your own project or simply lazy.

In approaching angel investors, you'll want to get as personal as possible. Find things that you have in common, such as friends or hobbies, and use these shared experiences as leverage.

Your goal in approaching an investor should be to hook them quickly. You should assume that every pitch is an all-in gambit. You either excite them right away or they never want to hear from you again.

One great way to do this is to develop a concise _elevator pitch_ for your first meeting. In essence, this is a short version of your pitch, designed to immediately grab their attention.

Your pitch should be shorter than 150 words and last less than 30 seconds, but it should also mention the specific problem that your product solves.

> _"Angel investing is a personal activity that is nourished by personal connections."_

### 4. Be prepared to answer tough questions when courting angel investors. 

Before an angel investor is willing to jump out of a plane with you at 30,000 feet, she'll want to know first if you've brought along a parachute. She'll also want to know exactly how it works.

The same goes for your business proposition. An angel investor won't pull out her checkbook unless you can clearly demonstrate how your business is going to work.

As "on the spot" as you might feel, you'll need to provide investors with a lot of information about your business, otherwise they won't be convinced that your start-up is worth their time or money.

Put yourself in their shoes. Would you hand over your own hard-earned cash to someone whose business idea is thin or underdeveloped? If an entrepreneur's idea was to "make nice tables," would you invest? Probably not. That is, unless he can elaborate on _how_ he intends to get ahead of the competition and _how_ he'll convince customers that his tables are the best in the world.

Indeed, investing is a risky business. Very few start-ups actually turn a profit, and angel investors understand that a large percentage of their investments will be lost. But although they can't eliminate all risks, they can try to minimize it by getting as much information as possible.

One way investors gather information is by asking you _due diligence questions_, which aim to uncover any potential liabilities within your company. Be prepared to offer information ranging from pending patents to recent power struggles between team members.

Most serious investors will ask these questions, but it would be even better if you started the conversation yourself!

The simple fact that your start-up has issues won't ruin your investment possibilities. In fact, _every_ start-up has issues. Being upfront about these issues will prove that you are honest, and will convince angel investors of your integrity.

### 5. You have to be able to take “no” for an answer with grace. Yet don’t be afraid to ask for feedback. 

You did everything by the book: you gave your pitch and was honest in presenting your strengths and weaknesses. Yet in the end, when you asked whether your investor was in, you received a flat "no."

What do you do now?

First, learn to appreciate a quick "no." Doing so will save you both time and hassle.

Also accept that not everyone will want to invest in your business. It's inevitable that you'll hear the word "no" at some point, but that's not necessarily a bad thing.

Many investors don't give a clear "no" right away, as they are either unsure what to do, want to keep their options open or simply don't want to cause a scene. In any case, ambivalence won't help you as it forces you to spend time pitching to someone who simply isn't interested.

You won't ever want to find yourself in this state of limbo. Thus, you should press for a clear "yes" or "no," and accept both possibilities with equal grace. While you will likely find another investor, you'll never make back lost time, so try to push for expedience.

If you're lucky, sometimes a "no" can be a positive thing, too. Some investors will take the time to explain _why_ they've rejected your proposal. Listen carefully to the feedback and think about how you can improve your pitch in the future.

For example, an investor might tell you that your product price is too high, and that she won't invest in your company because doing so could cause problems in her capital structure.

Knowing this, you could make adjustments to your business plan to reduce your price and make your proposal more viable for the next potential investor.

Don't be afraid to ask for feedback! This will wake your potential investor's instinct to help and might lead to some meaningful insights.

So far we've described how to approach an angel investor. The following blinks will reveal how to get one playing for your team.

### 6. Show an investor that your team has integrity, entrepreneurial skills and can work well together. 

When a start-up team pitches to an angel investor, you often have little to work with but your business plan and your own personalities. Thus you'll need to make the best out of both, and that means proving that you and your team has the _right kind_ of personalities!

In essence, angels invest in your promises, and making your promise believable means convincing them of your integrity.

As you already know, investing is a risky business. No matter how much market research you've conducted, in the end it all comes down to how much trust the investor is willing to put in you. You need to make it your goal to convince the angel investor of your integrity, beyond any doubt.

An important part of demonstrating integrity is simply being honest about who you are. Be authentic — don't try to talk like you're from Silicon Valley if you're from New Jersey. They don't care about where you're from or how you speak. They want to know that they can trust you!

But trust alone isn't enough. An investor can believe in your idea and your integrity without actually believing you have the skills to implement your idea. In other words, investors need to see that you are an entrepreneur to the bone.

It's important to demonstrate that your team has had entrepreneurial experience — even if it ended badly — as it shows that you've already acquired some management skills.

Finally, you'll have to demonstrate that your team can actually _work as a team_.

Teams are the backbone of start-up ventures. Their ability to share responsibilities and work with each other can mean the success or failure of the start-up.

When deciding whether to invest, the author himself looks for _teammanship_, a broad term that describes when a founding team is united by the same vision, whereby each member gives their all to achieve shared goals and resolve conflicts.

> _"A group of founders working together does not necessarily mean a team exists."_

### 7. Founders need big ideas and an execution plan. Don’t forget the figures with your head in the clouds! 

Impressing an angel investor requires you to find the right balance between dreaming big and having your feet planted firmly on the ground.

Behind every start-up is a set of beliefs that motivates the creation of a product or service. Wanting to make money is an important part of entrepreneurship, of course, but there should be something deeper guiding you through — your own little piece of truth, the way you intend to change the world.

By demonstrating your belief in your enterprise, you show a potential investor that your idea has substance and that you'll work tirelessly to bring it to life.

However, a plan too big might make you sound like just a dreamer if you don't back up your vision with a strong execution plan. Indeed, angel investors will want you to show them _exactly_ how you intend to make your vision come true.

Don't ignore details like profit margins or managing bank loans. While such things aren't as exciting as contemplating your vision, they are necessary in bringing your vision to life.

Tom Patterson, the founder of Tommy John, thought that men's underwear as a clothing segment was extremely underdeveloped in comparison to women's underwear. He was determined to bring innovation to this field by developing an undershirt that didn't bunch up or come untucked.

Patterson had a vision to be sure, but he also didn't ignore the facts and figures of the competitive clothing market. When he approached investor group The New York Angels in September 2011, the author — now an avid Tommy John wearer — was so impressed by Patterson's understanding of the financial side of the business that he decided to invest.

A year later, Tommy John revenue had risen from $1.5 million to $5 million.

> _"A start-up needs a belief, but the belief needs to be open to quick implementation."_

### 8. Start-ups need to start with an exit plan in mind. 

One of the best ways to excite an investor is to show that you're thinking about an exit plan.

There are two exit strategies for start-ups: being bought by a bigger company or going public (although going public is exceedingly rare for young companies).

Having an exit strategy means that you've already found a target company that would potentially be interested in purchasing your start-up, and that you've created the conditions necessary for the acquisition to take place, such as having transparent and organized financial records.

For an investor, an exit strategy is one of the primary motivators for why she would want to invest in your start-up in the first place!

An angel investor often owns equity in return for her investment. Consequently, virtually the only way for an investor to actually make money is if your company is purchased at some point.

Investors, just like entrepreneurs, are in the game to make money. Thus it's natural for them to want a clear exit strategy from the start as proof that you're taking their interests into account as well.

It's true that most start-ups end up folding and that consequently, most investments don't turn a huge profit. However, the pursuit of a profit is nonetheless an investor's only logical incentive to invest — no matter how much they believe in your vision — and you shouldn't dismiss it.

For a founder, the start-up is like a baby that they've brought into the world and through which they hope to change the world for the better. But for an investor, even if she takes an active role in shaping the project through advice and consulting, the start-up simply isn't _hers_.

So in the end, every investor is investing with the hope of cashing out. You should show the investor that you understand this and that you're working to make this possible!

> _"Every start-up should start with an exit strategy."_

### 9. Final summary 

The key message in this book:

**Angel investors don't just write checks — they also can provide start-ups with powerful insights, experience and networking opportunities. Grabbing their attention means approaching them on a personal level and building a company that takes investors' interests into account.**

Actionable advice:

**Be specific about what the money is for.**

When you're pitching to an angel investor, get specific about how the money she'll give you will be used. Is it for employee salaries? Research and development? Or something else entirely? Not only will this demonstrate that you've got business sense, but it will also help the investor feel the difference she's making. After all, no one wants to give you money to pay your bank loan!

**Suggested** **further** **reading:** ** _Venture Deals_** **by Brad Feld and Jason Mendelson**

_Venture Deals_ offers insider insights into the mechanisms that govern venture capital deals as well as tricks that will help you get the most out of negotiations with investors. It lays out the nuts and bolts of venture capital deals in a way that is both easily understood and will give you an edge at the negotiations table.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Brian S. Cohen and John Kador

Brian S. Cohen is an author, social entrepreneur, technology media strategist and chairman of The New York Angels, an investor group that has invested over $50 million in early-stage tech start-ups.

John Kador is a management-book author, consultant and speaker. His book, _Mending Fences_, was translated into a number of languages.

