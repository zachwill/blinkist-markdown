---
id: 53d8b8fe39303400073a0000
slug: what-great-brands-do-en
published_date: 2014-07-29T00:00:00.000+00:00
author: Denise Lee Yohn
title: What Great Brands Do
subtitle: The Seven Brand-Building Principles That Separate the Best from the Rest
main_color: E86161
text_color: CF2D2D
---

# What Great Brands Do

_The Seven Brand-Building Principles That Separate the Best from the Rest_

**Denise Lee Yohn**

In _What_ _Great_ _Brands_ _Do,_ author Denise Lee Yohn draws on her over twenty-five years of experience in brand-building to demystify the branding process. You'll discover the main principles that have made brands such as Nike, Apple and Starbucks such iconic household names.

---
### 1. What’s in it for me? Learn how to build a great brand like Nike. 

What do you think of when you hear the word _brand_? Cattle? Marketing? Advertising? An iPad, a Big Mac or a bottle of Coke, maybe?

These blinks give you the inside scoop about iconic brands: how a company's brand secures its market position and why it remains influential, year after year. You'll understand why branding is not merely a marketing tool but also a organizational and strategic tool. You'll also learn why even a strong brand can suffer if its integrity is compromised.

In these blinks, you'll discover:

  * why Nike's "Just Do It" is one of the most successful taglines of all times,

  * why Red Bull's founder actively encouraged rumors that his company's drink was dangerous, and

  * why Starbucks is about more than just coffee.

### 2. A brand is the personality of a business, and it should guide a company’s every action. 

Everyone recognizes contemporary brand superstars, like Apple, Nike and Zappos. So how did these brands become universally recognized household names?

Many people might believe that brand-building is just a matter of experimentation, good timing and sheer luck. In fact, nothing could be further from the truth.

Great brands achieve their iconic status when a company understands its brand and how it should influence the everyday running of the business.

A brand is much more than just the impression people have of your company and your product. A brand is the _personality_ of your business, and therefore should shine through everything your company does.

Successful brands understand that people don't just buy products based on a rational analysis of price and quality, but instead consider the feelings and values they associate with a brand. Having a coffee at Starbucks, for example, is not just about getting your morning caffeine fix. It's also about the store's sensory experience — the music, the furniture or the employees' smiles — that you notice every time you walk through the front door.

But great brands also enjoy other benefits. One is that a company can enjoy higher profit margins. A study by the strategy consultancy Vivaldi Partners showed that customers are willing to pay higher prices for brand-name products than for other products.

Another benefit is that companies with a strong brand tend to have an easier time aligning their organization to a set of values. This is because a brand can be used like a strategic compass, guiding the company at large as to how it should build its corporate culture, design processes and craft marketing campaigns.

This is why Starbucks would never compromise the customer's in-store experience, for example, by buying cheaper furniture. Even though the company could save costs in the short-term, such a decision would not be in line with the company's brand.

> _"Simply put, your brand is what your company does and how you do it."_

### 3. Your corporate culture is your brand’s foundation. 

Many people think that the only function of a brand is advertising and marketing. In fact, a brand is just as important for the people inside a company: if you align your corporate culture with your brand's values, this can help employees better work together toward a common goal.

This alignment also amplifies the power of a brand toward the customer. Imagine your brand is a source of light that shines on your prospective customers. Now imagine your business and its stakeholders like suppliers, and sales representatives are those who connect the brand and the customer. If these people aren't properly aligned with the brand's values, they will block or distort the light coming from the brand, decreasing its efficacy.

To prevent this from happening, you need to fix any gaps or inconsistencies between your company's brand values and your organization.

This means you have to educate every stakeholder about what your brand is and why it's important. Each person must also understand how their actions impact the brand, and how they can accurately represent the brand and enhance its value through their everyday work.

One way to do this is through _brand_ _toolboxes_ and _brand_ _engagement_ _sessions_.

A brand toolbox is a simple item that helps employees understand and apply brand values everyday. It could, for example, take the form of a deck of cards or a little book, with references and anecdotes that relate your company's core values.

Brand engagement sessions are meant to help employees understand and identify how brand values influence every part of the business. Starbucks, for example, held a brand engagement session where managers participated in a walk-through presentation that followed the journey of a coffee bean from farm to cup, explaining how the company's brand affected every step.

> _"…if you don't develop greatness among your employees, your employees are unlikely to deliver greatness to your customers."_

### 4. Great brands build an emotional connection with the customer. 

At first glance, a slogan like "Just Do It" seems ineffective — it doesn't even mention the company's name, Nike, nor the products the brand represents. So how on earth did this phrase become one of the best-known brand slogans of all time?

Successful branding is about emotions, not products. It's about creating an emotional connection between the brand and the customer. As Scott Bedbury, Nike's one-time marketing chief, wrote, "'Just Do It' was not about sneakers, it was about values. It was not about products, it was about brand ethos."

The "Just Do It" campaign featured both professional and amateur athletes talking about their emotions and accomplishments without mentioning Nike, and afterward, the tagline was displayed.

The campaign evoked such a powerful emotional response that people started writing to Nike about how they too "just did it," such as quitting a miserable job, started working out or ended an unhealthy relationship.

So how can you best connect with your customers on an emotional level?

One great way is through _empathic_ _research_, in which you ask customers questions like, "How do you feel when buying the product?" and "What need is satisfied when you buy and/or use the product?" This helps you get as close as possible to the private lives of your customers.

One example of the power of empathetic research can be seen in the plight of Pampers, Europe's best-selling disposable diaper brand in 1998. Although Pampers' product was a market leader in keeping babies' bottoms dry, sales were still in steady decline. After exploring empathic research, the company discovered that actually, young mothers cared more about things like how soundly their babies slept than about whether their diaper was dry. In response, Pampers developed diapers that were specifically designed to help infants sleep more comfortably, and sales soon picked up.

> _"When your business is primarily based on knowledge, [then] people — rather than products — become your brand."_ — Former IBM CEO Sam Palmisano

### 5. Great brands don’t follow trends, they create them. 

Some people think that a key part of building a great brand is being trendy, meaning following current trends.

But in fact, following trends can be a risky endeavor, because trends change quickly, and trying to adapt your brand to each change can compromise its integrity.

When you follow a trend set by a competitor, you're actually putting yourself in a _comparative_ _position_, where consumers see you as a follower, not a leader. Your products will be perceived as similar to your competitors' products, but with a modifying "-er": that is, bigger, cheaper, faster and so on.

This is not how you want your company, your product or your brand to be perceived.

Consider the car company Hyundai _._ Its positioning in customers' minds is something along the lines of "just as good as Lexus, but cheaper." Therefore Hyundai is seen as a mere _copycat_, not a brand that constantly innovates new products. In this comparison, it's Lexus that has the emotional advantage, and that alone can make a big difference.

So instead of following trends, you should _create_ _your_ _own_.

One way to do this is by challenging existing trends: questioning the status quo and commonly accepted dogmas that dominate your industry.

As an example, let's look at fast food chain Chipotle _._ The company challenged the notion that to be competitive in the fast food industry, you have to constantly cut prices, expand your menu selections and hire cheap labor. Instead, Chipotle did exactly the opposite: it uses expensive, high-quality ingredients to entice customers, and pays its staff higher salaries. The result? Although its food offerings are pricier than those of competitor Taco Bell, Chipotle has been a huge success.

Another way to create your own trend is to anticipate and take advantage of a cultural movement. Starbucks anticipated that American society as a whole was headed in a direction where people would start to feel increasingly isolated. Therefore, the company designed its stores to be a "third place" between home and work, where people would feel comfortable spending time.

### 6. Great brands don’t try to please everyone. 

Just as blindly following the latest trends can be dangerous for your brand, so can chasing customers indiscriminately. Sure, it might increase profits in the short term, but trying to appeal to everyone risks compromising your brand's integrity.

Great brands don't chase after every customer but rather focus their energy on _core_ _customers_.

How can you do the same? Start by understanding who your customers are, what they need and what kind of brand you want to be for them. This can be achieved through a _needs-based_ _segmentation_ of potential customers.

First, you target your core, meaning your ideal potential customers. Then you segment their day and analyze during what times the needs relevant to your product are at their strongest. Out of these needs, you pick the ones that best correspond to your brand identity and focus your marketing on those needs.

As an example, consider a snack manufacturer that used a needs-based segmentation to understand what kind of snacks people wanted for what reason at what time of the day. It discovered that one need in the morning was to "pump up for the day," so this was when people wanted an energetic and healthy snack. It just so happened this fit perfectly with the brand's main values, "being healthy and fun," so the next product campaign revolved around the slogan, "pump up."

Another reason why you shouldn't try to please everyone is that it results in a boring brand, and you'll still have detractors no matter what you do. Brands with personality and integrity, on the other hand, will always attract loyal customers and stakeholders.

An example of a divisive brand is energy drink company Red Bull. Though the company is today very successful, at the outset there were rumors that Red Bull was a dangerous drug. Interestingly, the founder of the company actually set up a website to encourage the spread of these rumors.

The reason? It was equally important to the founder that teachers hated his brand as it was that students loved it.

### 7. Great brands adhere to their core ideology, even if it means saying no to enticing opportunities. 

Besides being a slave to the latest trends and chasing after customers indiscriminately, there's a third mistake that brands can make: focusing too intently on growth.

To meet shareholder demands, most CEOs are under constant pressure to introduce new products, diversify services and expand the business. But pushing for growth at the expense of brand integrity can often result in the demise of even a great brand.

One example of this is the experience of Krispy Kreme, a doughnut company that in the 1990s appeared to be the next big thing. The company served delicious, fresh doughnuts at its stores that were meticulously designed to tempt all five senses.

To meet shareholders' growth expectations and to make a quick profit, Krispy Kreme started selling its doughnuts at places like gas stations and grocery stores as well. This not only compromised the quality of the product, but more importantly, ruined consumers' experience of the brand. Sales and profits plummeted, and the perception of the brand was almost destroyed.

So how can you avoid this same fate?

Whenever you face difficult strategic or moral decisions, look to your _core_ _ideology_ : the original intent of your company's founders. The ideology comprises the values, goals and traits that you've promised to your customers will never be compromised. Brand-focused companies should only pursue growth and change that is in line with their core ideology.

In his bestseller _Good_ _to_ _Great_, author Jim Collins writes that for truly great brands, the challenge isn't to find growth opportunities but to decide which ones to pursue. Sometimes a company will be tempted by "once-in-a-lifetime" opportunities, but even then, the company must have the discipline and character to say no, if the opportunity isn't in line with its core ideology.

Remember Krispy Kreme: the company didn't understand how vital the in-store experience was for its brand and started chasing short-term profits, thereby almost killing its brand.

> _"Your brand can't just be a promise; it must be a promise delivered."_

### 8. Great companies ensure every detail of the customer experience is in line with its brand. 

One of the challenges of building a great brand lies in getting your entire company to present a united front to the customer. Big companies may have several departments that deal with customers, and yet the brand must be fully and uniformly integrated into all of them.

To achieve this, you need to do two things:

First, you need to know all the _customer_ _touchpoints_ where your company interacts with customers. These could be things like packaging, marketing, sales channels, customer support and so forth. The customer experience is based on interactions at those touchpoints, and nothing destroys an otherwise pleasant brand experience like a grumpy customer-support person or a broken website. Of course, the most important touchpoint with the biggest impact is your product.

Second, you need to align every touchpoint to your brand values, and then monitor how employees at each touchpoint are living up to these values and how the customer experiences them.

The importance of keeping an eye on your touchpoints can be seen in the experiences of REI, an American sports gear retailer. The company discovered that its prices and product information varied greatly between their online and offline stores. Luckily, it reacted quickly and patched up inconsistencies, preventing further damage to its brand.

So how can you best design the experience that you want your customers to have at these touchpoints?

Think about the details — they are crucial to forming a connection between your brand and your customers.

As an example, consider Apple. Thanks to guidance from Mike Markkula, an early investor, Steve Jobs understood that design factors such as product packaging, the feel of a product and even its smell are very important. Taken together, the experience is a kind of theater, a moment that is to be carefully orchestrated to create the perfect customer experience.

### 9. Great brands are generous in supporting projects relevant to them. 

So far we've talked a lot about how building a strong brand is good for a company. But actually it can also be good for the world: "doing good" is a key factor in branding.

In 2012, an influential article by branding experts John Gerzema and David Roth proclaimed that as a society, we're now entering a new era where _corporate_ _reputation_ and brand are becoming the same thing.

This means that unlike before, companies can no longer rely solely on _hard_ _factors_ such as innovation, quality and price to form their reputation.

Instead, _soft_ _factors_ such as trustworthiness and a sense of responsibility toward people and the environment have become the deciding factors for customers when choosing brands. One study on the matter showed that 73 percent of buyers would switch their current brand to a competing one if the latter supported a good cause.

But make sure your company's charity is generous and doesn't seem calculated. Otherwise, your actions won't be perceived as truly "doing good," and consumers might think you're merely engaging in _greenwashing_ : reaping profits at any cost with one hand while being superficially charitable with the other.

This is especially true if you contribute to random environmental or societal projects in the hope that your actions will distract consumers from the damage your business is responsible for.

As an example, consider if oil company BP announced that it was launching a charity project to save 1,000 acres of rainforest. Would you consider BP an environmentally conscious brand, thanks to their efforts? Probably not.

Instead, you should make your main goal to create _shared_ _value_ for both your company's brand and the outside world. To achieve this, only support projects that are relevant to your industry, your community and/or your target group of customers. These projects should also mirror your brand's ideology.

An example would be the "Create Jobs for USA" program undertaken by Starbucks, where the company provided financing for struggling local businesses. This project aligned perfectly with the brand's socially oriented culture.

### 10. Everyone in your organization must know and live your brand values. 

In an earlier blink, we mentioned that a strong brand can function as a guiding compass for your entire organization, influencing everyday decisions. For this to happen, you need to operationalize your brand strategy, and this requires implementing two brand-building core competences across your entire company.

First, you should distribute information regarding your brand and brand strategy to everyone in your organization, and make sure every employee keeps themselves updated on brand information. Make it clear that your brand _is_ your business, not just some little facet of it.

This is such a high-priority issue that you should require compliance from employees, for example by holding pop quizzes on brand values. Or you could also reward those employees who demonstrate brand values particularly well. You should also regularly plan activities in which employees learn about and experience the company's brand values. The brand toolboxes and brand engagement sessions described in an earlier blink are examples of such activities.

Second, you need to implement projects that aim at aligning the whole organization with your long-term brand strategy. An example of such a strategy would be the importance of design at Apple, which was internalized by the entire company.

These projects must be managed by leaders and _evangelists_ who represent the brand values in your company. They are also in charge of breaking down any barriers to absorbing brand values, such as rigid organizational silos. At Apple, this function was performed by Steve Jobs himself, who was obsessed with design.

Now you know that a brand is not only a marketing method but also an invaluable tool for streamlining your organization and making tough strategic decisions.

> _"Make your brand your business."_

### 11. Final Summary 

The key message in this book:

**A** **brand** **is** **more** **than** **a** **mere** **marketing** **tool** **–** **it's** **a** **guiding** **compass** **for** **a** **company's** **organization** **and** **strategy.** **Strong** **brands** **can** **attract** **lots** **of** **customers,** **but** **companies** **have** **to** **be** **careful** **to** **uphold** **their** **brand's** **integrity** **and** **not** **just** **chase** **after** **trends,** **growth** **or** **every** **customer** **segment.**

Actionable advice:

**Let** **your** **brand** **shine** **through** **your** **work.**

You may feel that your company's brand has little or no impact on your work, but have you ever really tried to let it show? Next time you're at work, try to think about the brand you're working for and what impact your work has on it. Ask yourself: Is my work strengthening the brand, is it aligned with the brand's goals? Also, remember to think about how the customer sees your work: will they know which brand you're working for? If you put branding front and center in your job, you may well find that your work will become more purposeful and enjoyable.
---

### Denise Lee Yohn

Denise Lee Yohn is a recognized brand expert, speaker and writer who specializes in teaching companies how to implement their brands on an operational level.

© Denise Lee Yohn: What Great Brands Do copyright 2014, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

