---
id: 59c8e068b238e10005c2019b
slug: what-happened-en
published_date: 2017-09-25T14:01:00.000+00:00
author: Hillary Clinton
title: What Happened
subtitle: None
main_color: 8F9AB5
text_color: 334A82
---

# What Happened

_None_

**Hillary Clinton**

_What Happened_ (2017) is the story of the woman at the center of one of the craziest elections the United States has ever seen: the 2016 presidential election. This is Hillary Rodham Clinton's account of her experience facing, and eventually losing to, Donald Trump — an opponent like no other — and the road that led her to this historic election.

---
### 1. What’s in it for me? Learn what went wrong with Hillary Clinton’s 2016 presidential campaign. 

When Hillary Clinton announced her 2016 presidential campaign in April 2015, Democrats across the United States were ecstatic. Now she could finish what she had begun in 2008 when she had lost the Democratic Party nomination to Barack Obama.

From the get-go, Clinton's campaign was a well-oiled machine. But as the election day approached, and Donald Trump unexpectedly won the Republican Party nomination, the cracks started to show. In comparison to Trump and Bernie Sanders, Clinton was perceived as aloof and impersonal by many. And what exactly was her message?

Despite these issues, most polls had her as a clear winner until the very end. So what happened? These blinks offer a detailed analysis of Clinton's campaign and show the forces and circumstances that eventually brought it down.

In these blinks, you'll learn

  * how Russian hackers helped Trump win the election;

  * what George W. Bush thought of Trump's inauguration speech; and

  * Clinton's advice to anyone who has lost hope in politics.

### 2. Trump’s inauguration was far from uniting and uplifting, unlike the Women’s March. 

If you lost an election to Donald Trump, you'd probably want to skip standing in the cold to listen to him being sworn in and delivering a speech. But if you're Hillary Rodham Clinton, it's your obligation to show up, even if there are a million other places you'd rather be.

There are many proud and noble traditions in American politics, and having a peaceful and dignified transition of power is one of them.

Many of Clinton's friends told her to stay away from Donald Trump's inauguration, but as both the former Secretary of State and the First Lady to former president Bill Clinton, it would have been bad form if she'd been absent. Prior to the event, Clinton had spoken to former presidents Jimmy Carter and George W. Bush, two people who also disliked the new president elect. The fact that they would also be there by her side gave Clinton some much needed encouragement.

After Trump gave a bizarre and bleak inauguration speech that included the words "American carnage," George W. Bush had the perfect reaction: "That was some weird shit."

Of course, Clinton's speech would have been nothing like Trump's. She sees the tradition of a presidential inauguration as a chance to uplift and unite the people. When she envisions the United States, the disturbing words "American carnage" don't come to mind.

Instead, she sees the kind of people who went out the following day to take part in the Women's March that eclipsed Trump's inauguration. With millions of marchers in cities around the country, from New York to Los Angeles and Wyoming to Alaska, the Women's March was the single biggest united protest the country had ever seen.

Clinton enjoyed the protest from her home in Chappaqua, New York, and for her, it was a perfect remedy for the inauguration. Clinton would have loved to have been there, but she believes it was the right choice not to steal the spotlight from what the country needed on that day: fresh voices.

### 3. Election night led to a period of recuperation and trying to resist becoming cynical. 

November 9, 2016 is a day that Hillary Rodham Clinton won't soon forget.

For the most part everyone went into the day feeling pretty upbeat, but by 1:00 a.m., events had taken a bad turn. It all came down to three states: Michigan, Wisconsin and Pennsylvania. Then the final nail in the coffin came at 1:35 a.m. when the Associated Press called Pennsylvania for Trump.

Having to call Trump to congratulate him on his win was tough, but perhaps even tougher was calling President Barack Obama to apologize for letting him down. Clinton's throat tightened as she passed on those words, but Obama was gracious as always. He told her that she had nothing to feel bad about, and that life after the election would go on.

The key to coping with defeat, Clinton learned, is not to turn cynical. It's also important not to isolate yourself and let friends provide some comfort and reassurance.

This helped a great deal as she heard from friends who told her how motivated they were to take action after the disappointing results. This helped prevent her from going dark.

She did spend a day lying low, but after that it was back to business, and the first job was signing her thank-you letters to her 4,400 campaign staffers and to make sure they got paid and were getting their health insurance until the end of the year.

Another great way to keep the cynicism at bay is to have something like an Irish wake, where sadness is faced with celebration, as Clinton did by hosting a post-election staff party. As a friend advised her: watching the first season of Friday Night Lights with a glass of good chardonnay isn't a bad remedy, either.

Clinton did her fair share of fuming, but she made sure her anger fuelled her, not overtook her. She also took a lot of long walks with Bill to talk it over and figure out what happened.

To get to the bottom of this, let's start at the beginning . . .

> _"I knew I would be fine eventually, but for those early weeks and months, I wasn't fine at all."_

### 4. Clinton had a good, unselfish reason for launching her campaign. 

A lot of people have opinions about what went wrong with Clinton's campaign, and one common suggestion is that her campaign didn't do a good job of telling people why she was running for president. Some people have said that it felt like Clinton wanted to be president because she felt entitled.

To set the record straight: Clinton didn't have any self-serving or power-hungry ambitions for the presidency, and she certainly never felt it was inevitable she would attain it.

No, the real reason Clinton wanted to be president was because she honestly felt she would be good at the job.

It's true that she didn't do the best job of being upfront about her qualifications, but Clinton has never been great at boasting about herself. She prefers to talk about her policies and strategies for improving the government.

When Clinton launched her campaign, polling data showed that even though people knew her name, they were still unfamiliar with her achievements and who she was as a person. This is something she intended to fix by reintroducing herself to voters.

She wanted every voter to know that she'd long ago devoted her life to public service, that as one of only 27 women in Yale Law School, she began to work as a defender and advocate for human rights. And since her days working for the Children's Defense Fund, her primary cause in life has been standing up for those less fortunate.

While Clinton wanted to familiarize voters with her long history of public service, the conversation was always being redirected elsewhere by people who wanted to cast her as a criminal evil-doer. It was hard to get facts and truths to people during the 2016 election, but the real reason for her campaign was that she knew she had what it took to be an effective president who would be on the side of the working people.

> _"In just one day at the White House, you can get more done for more people than in months anywhere else._

### 5. It’s not easy being a woman in politics, but there are some rewards. 

Clinton never wanted to make her gender the focus of her campaign. She's well aware that her story of being a woman who has managed to be successful in a male-dominated profession doesn't sit well with everyone.

Whether people like it or not, politics is rife with sexism and misogyny.

You need only look at the fact that millions of people voted for someone who bragged about sexually assaulting women as evidence of how ingrained these characteristics are in American society.

Or, you could glance at the comments on Twitter and YouTube underneath any post written by a woman voicing a political opinion. It's an understatement to say that it's difficult to be a woman in politics; men will attempt to diminish every word and idea you have.

As former First Lady Eleanor Roosevelt once said, a woman needs to have the skin of a rhinoceros to get by. Clinton has developed some pretty thick skin, but the cruel things people say can still hurt. When people at Trump rallies chanted "Lock her up!" and "Guilty!" it felt like a witch hunt from the Middle Ages.

But the only way change is going to come is if more women enter the political fray. They'll still encounter double standards, like how shedding a tear is a sign of compassion in men, and a sign of weakness in women, but there are also rewards.

Being in the room where decisions are made and being a voice for the underrepresented in your community is a thrill that never gets old. Whether it's the Oval Office, senate chambers or your local town hall, more women need to be heard and experience this for themselves.

In countries like China and Romania, men are still in complete control of a woman's reproductive rights, and the only way to make sure that doesn't happen in the United States is for more women to run for office.

### 6. One of Clinton’s biggest motivations was to take on the gun lobby. 

There are some political forces against which most politicians are afraid to stand, no matter how high their office. The National Rifle Association (NRA) is one such force.

After spending time with the Mothers of the Movement, a group of African American women whose children had been killed by police or gun violence, and watching as seemingly endless mass shootings took place in America, Hillary Rodham Clinton was determined not to bow down to the NRA.

Mothers of the Movement want the United States to adopt stricter gun laws. They've taken their loss and turned it into a powerful force. Clinton took their message to heart; it played a big role in her campaign platform.

There's no doubt that people on both sides of the gun issue feel passionate about their cause. Gun ownership for the purposes of hunting and self-defense are customs many Americans consider deeply woven into the fabric of their nation, and indeed the Second Amendment of the Constitution guarantees the right to bear arms.

But Clinton has no interest in banning guns; she wants it more difficult for people with bad intentions to get their hands on them.

Clinton is familiar with the power of the NRA: they helped defeat Al Gore in the 2000 election, and more recently, they managed to take down a bill aimed at making people on the federal no-fly list ineligible to buy guns. Yes, that's right, people who aren't allowed to board a plane should still be eligible for a handgun as far as the NRA is concerned. And they got their way because politicians are too scared to oppose this powerful lobby.

That's why the United States needs a leader like Clinton who isn't afraid to stand up to big money. Donald Trump made it clear he was courting the gun lobby from the start. After the mass shooting at an Orlando nightclub, he thought it was a shame everyone in the club wasn't armed.

### 7. Through her experiences in 2016, Clinton has learned the importance of lofty goals. 

One of the differences between Clinton and Bernie Sanders was how they dealt with the details of their policies. Bernie Sanders wanted to make college education free for every US citizen, even though the details of such a plan were unclear, including how much it was going to cost the middle class in raised taxes.

This is the difference between idealism and realism. Clinton always felt it was of great importance to have realistic plans that could be enacted tomorrow, even if they aren't as exciting as free college.

Clinton is known as a policy wonk. At her campaign headquarters in Brooklyn there hung a sign that read, "Wonks for the Win!" Her team was filled with like-minded hard workers who enjoyed crafting realistic policies that could be funded and put into action.

But since the election, Clinton has come to see the benefit of having big, exciting goals that may need more work before they can be fully realized.

There's no doubt that Bernie Sanders ran a successful campaign that got people very excited, while Clinton's campaign was framed as representing the status quo — even though her platform was far more progressive than Barack Obama's ever was!

Be that as it may, she now recognizes that her campaign could have benefitted from some exciting ideas that looked to the future, like the "Alaska for America" idea she almost rolled out. Alaska for America was the name of a profit-sharing plan that could someday be used across the country to share the wealth from natural resources with taxpayers. It's already operational in Alaska through a fund that distributes royalties from the state's oil reserves to residents.

Clinton's team was unable to come up with a concrete plan for how this system could be used on a federal, nationwide level, but perhaps that shouldn't have stopped her from making it part of her platform. It might have enthused people, and she could have figured out the details later.

### 8. Clinton was determined to help those who didn’t vote for her, especially in Michigan and West Virginia. 

Another regret that Clinton has from her 2016 campaign was saying the words, "We're going to put a lot of coal miners and coal companies out of business." Sounds pretty harsh, right? And the press, especially Fox News, repeated those words on an endless loop.

While Clinton regrets putting those words in that exact order, the quote has been taken out of context and twisted to imply the exact opposite of the message she was giving. As a result, when Clinton went to visit the people of West Virginia and those in Appalachia, she was met by angry protesters. But these were people she was intent on helping, even if they weren't going to vote for her.

In West Virginia, she was greeted by people carrying signs saying "Go Home Hillary" but she managed to talk to some people and tell them she had every intention of helping them improve their lives. The reality is that coal mining has been on the decline since the 1950s.

Trump promised to reopen coal mines, but in May 2017, his economic council director, Gary Cohn, admitted that "coal doesn't make sense anymore." That's why Clinton said those words, but the full quote was a proposal to create jobs and opportunities for people in West Virginia by fixing roads, tunnels and bridges and getting the area wired with broadband so that small businesses had a fighting chance at succeeding.

She was also passionate about helping the people of Flint, Michigan — something she regrets not being able to do as president.

The people of Flint have been coping for years with poisonous drinking water and the cruel indifference of politicians. Since 2014, people have been getting sick, developing seizures and dying due to the water supply. They're still drinking bottled water and waiting for the problem to be fixed.

That's the kind of problem the federal government should make a priority, which is exactly what Clinton was planning to do.

### 9. Clinton had great difficulty getting her message heard over the obsession with her emails. 

So what happened? Why did she lose? It's a question Clinton returns to often, and one of the answers that keeps coming back is "_those damn emails_ ".

Choosing to use her private email account during her time as Secretary of State is at the top of Clinton's list of regrets.

But here's the thing: since email use began, every Secretary of State has used a private email account. General Colin Powell, the secretary under George W. Bush, used an AOL account while he was on the job!

Nevertheless, the conversation remained stuck on the subject of Clinton's emails for the duration of the entire campaign.

According to Harvard's Shorenstein Center, nightly television news spent three times as many minutes talking about her emails than about the policies in her campaign platform. Overall, in 2016, when you combine the major nightly news shows, they spent only 32 minutes a night discussing the policies of the presidential candidates. In 2008, they spent 220 minutes per night on policy!

With this fixation on her email, it was next to impossible for Clinton's message to reach as many voters as she'd hoped. She sweated over her platform, which was developed after listening to people and talking one-on-one with families who needed help. But television and newspapers were far more interested in questioning her use of a private email account.

Part of the scandal was that her email account was directed to a server in her Chappaqua, New York home, which was set up by Bill Clinton and protected by members of the secret service. The irony here is that while Pentagon, White House and State Department servers have all been hacked, the server at her home has been inspected and found to be uncompromised.

So what has been called a danger to national security actually made her correspondence safer.

### 10. Having to compete with Trump was difficult enough, but dealing with Russian influence was worse. 

Clinton is happy that many of the government's top officials spoke up on her behalf during the email scandal. They helped to clarify that using a private account isn't a crime nor a threat to national security.

These are facts — something for which Trump has shown little concern. And since there's never been a presidential nominee with so little regard for facts, it made campaigning against him an exceedingly frustrating and difficult experience.

According to the independent fact-checkers PolitiFact, Trump was the most dishonest candidate the site had ever measured. All the while, Clinton was rated the most honest in 2016 — even more so than Bernie Sanders.

Before Trump, candidates didn't show up to debates unprepared and tell blatant lies, so there was no playbook for how to deal with someone like him. But Clinton's team did an amazing job in preparing her for those debates, especially Philippe Reines, an advisor who did such a pitch-perfect impersonation of Trump that she was never thrown off by the real deal's bizarre behavior.

Ask yourself, what would you do if a man who bragged about groping women proceeded to creep around behind you and breathe down your neck as you were answering questions on a televised debate?

This was indeed unprecedented territory, and that feeling became more extreme when news broke that Russian hackers were attempting to influence the election.

Never before has a foreign power so directly interfered with a US election. The first signs came when they hacked the email servers of the Democratic National Committee and then the email account of Clinton's campaign chairman, John Podesta.

These emails contained nothing bad about Clinton, but they were released at strategic times to distract voters and the press in the days leading up to the election, including the day Trump's comments on groping were made public.

But that's just the tip of the iceberg. Twitter and Facebook were flooded with fake accounts posting anti-Clinton messages, and at least 21 states had their election systems targeted by hackers.

### 11. The most damaging blow to the campaign was the unprecedented influence of the FBI director. 

Here's an interesting fact: The CIA and FBI were well aware of a potential Russian cyberattack by at least August 2016, three months before the election.

However, while the CIA sounded the alarms, the FBI dragged their feet. In fact, it wasn't until after the election that the FBI admitted that Russia had helped Trump.

When asked about his reluctance to take action, FBI director James Comey said he didn't want to unduly influence the election, which is astonishing since that's exactly what he ended up doing.

On October 28, 2016, with just days to go before the election, Comey released a letter to Congress saying his agency had discovered new emails that might pertain to Clinton's closed case. Then, a week later, Comey admitted that the emails weren't new at all and that the FBI was already aware of them. The case is still closed with no evidence of wrongdoing.

Deputy Attorney General of the Justice Department, Rod Rosenstein, issued a letter condemning Comey's behavior, calling it "profoundly wrong and unfair." And there's no avoiding the fact that it had a critically damaging effect.

Before October 28, the data of Nate Silver, one of the most respected and accurate statisticians in the United States, showed that Clinton had a commanding lead and an 81 percent chance of victory. But after Comey's letter, it dropped to 65 percent. Trump's former campaign manager, Corey Lewandowski, cites Comey's letter as "reversing fortunes" of his campaign.

With the case closed for months and three clear debate victories behind her, momentum was definitely in Clinton's favor. With Comey's letter regarding the email leaks in combination with the bombardment of fake news on social media calling Clinton a criminal and even a murderer, it all began to crumble.

> "'The FBI is Trumpland,' is how another agent put it."

### 12. The 2016 election left much to consider and investigate, but US citizens need to move forward and stay involved 

So what's next? Well, like many, Clinton is eager to discover the full extent of what happened during the election. Astonishing things are still being uncovered, and Clinton's interest in these details is far from self-centered. If anyone is to avoid another attack on the democratic process, it's important to understand exactly what occurred.

While the investigation into what Russia and Trump's campaign team did is ongoing, there are many confirmed facts to consider.

According to the unclassified documents released by the intelligence community, people from the Trump team met with Russians and contacted them through text, email and phone calls. Many meetings were made through the Russian ambassador Sergey Kislyak.

Roger Stone, Trump's campaign advisor, has admitted to being in contact with the Russian hacker who goes by the name of Guccifer 2.0. And we know that Trump's son-in-law Jared Kushner met with Sergey Gorkov, the head of the Kremlin bank associated with Russian intelligence agencies. We also have correspondence from Kushner recommending the use of Russian diplomatic channels as a way of avoiding detection.

Plus, the Trump administration is currently sending encoded messages, which violates federal law on the open nature of White House correspondence.

Hopefully, everything will be made clear in time, and we'll be able to make sure this kind of attack never happens again.

In the meantime, it's more important than ever to go forth and use whatever frustrations or anger you have to positive ends. This means getting involved in politics and making a difference.

Clinton has been keeping four words in mind: Resist! Insist! Persist! Enlist!

The lessons here are: get to know your local politicians and what they stand for. If you don't like their policies, tell them, resist and insist on change. And don't take no for an answer — persist and enlist your friends to support your cause.

This is how change is made, and glass ceilings are broken, starting at the local level and moving up from there.

### 13. Final summary 

The key message in this book:

**Hillary Clinton's loss in the 2016 election came as a shock to many. Some of the answers as to what happened are still being determined, but from Clinton's perspective, many unprecedented events contributed to her loss. These include an out-of-control FBI director, coordinated Russian hackers and a few mistakes of her own that Clinton deeply regrets.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading: _Hard Choices_** ** __****by Hillary Clinton**

Hard Choices offers a first-hand account of the trials and impressive diplomatic successes of the early years of the Obama administration. In this telling memoir, former Secretary of State Hillary Clinton places you at the administration's negotiating table where key policy decisions were made.
---

### Hillary Clinton

Hillary Rodham Clinton has had a remarkable career in public service. She became the first First Lady still in the White House to be elected to a public office when she was voted New York's first female senator in 2001. She went on to become the first woman to win the Democratic Party's presidential nomination. Clinton's other books include _It Takes a Village_ and her two previous memoirs, _Living History_ and _Hard Choices_.

