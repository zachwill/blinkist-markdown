---
id: 5550b6c06332630007700000
slug: what-if-en
published_date: 2015-05-13T00:00:00.000+00:00
author: Randall Munroe
title: What If?
subtitle: Serious Scientific Answers to Absurd Hypothetical Questions
main_color: C6434C
text_color: D64D50
---

# What If?

_Serious Scientific Answers to Absurd Hypothetical Questions_

**Randall Munroe**

In _What If?_ (2014), Randall Munroe presents earnest, thoroughly researched answers to absurd, hypothetical questions in a highly entertaining and digestible format. Munroe serves up the most popular answers from queries he received through his _What If?_ blog, along with a host of new, delightful, mind-bending questions and answers.

---
### 1. What’s in it for me? Get well-researched answers to some of the most far out “What if” scenarios. 

What-iffing is something we usually link to curious children or anxious worriers, but suppose it could also reveal a world of deeply researched insights?

These blinks present a series of absurd and hypothetical questions, and delve into the relevant theories that answer them in accurate and highly entertaining ways. Popular answers from author Randall Munroe's famous _What If?_ blog are presented, but most of the questions are new and answered for the first time. 

In these blinks, you'll discover

  * that you'd be able to run off your little, massive planet, if you were _The Little Prince_ ;

  * that live-updating the English Wikipedia in a paper version would cost four to five thousand dollars on ink cartridges per day; and

  * that even if the entire world's population started doing calculations right now, a mid-range mobile phone would still be 70 times faster.

### 2. If the Sun went out, we would see a variety of benefits in many areas of life. 

What would happen to Earth if the Sun suddenly went dark? It's a haunting thought but, surprisingly, many good things would come from it. Let's look at a few examples.

One positive consequence would be a reduced risk of solar flares — those abrupt, massive releases of energy on the Sun's surface. Solar flares pose a high risk for us as they're often succeeded by geomagnetic storms, which are temporary disruptions of Earth's magnetic field.

We experienced this in 1859, when a massive solar flare and geomagnetic storm hit Earth. The storm produced powerful currents in telegraph wires, setting fire to them and wiping out communications.

Since 1859, though, we have built countless more wires. If a storm were to strike us today, researchers estimate that the economic damage to the US alone would amount to trillions of dollars.

But, of course, we needn't fret about this if the sun went out!

A second advantage would be that infrastructure costs would be reduced. Think of bridges — we build them in order to cross water. But if the Sun went out, we could simply drive across the ice. Add to this the estimated $20 billion we spend on repairing US bridges annually and we'd also save on infrastructure!

Third, trade would be cheaper.

If the Sun were no more, darkness would reign, and a land of eternal night has no use for time zones. Time zones make it more complicated to do business when international office hours don't match up. But without the Sun, we could all exist in one time zone, which would speed up communication, allowing a healthy boost to the global economy. 

But what about the downside? Well, with no Sun, we would all freeze and die. Perhaps we should keep our Sun after all.

### 3. The little planet from The Little Prince would have some remarkable gravitational properties. 

_The Little Prince_, by Antoine de Saint-Exupéry, is an engaging children's story about a prince from a far away asteroid. But could anyone actually live on a very small but very heavy asteroid? Absolutely not! However, it's still fun to imagine what kind of gravitational oddities we would experience if we could.

For a start, on our tiny planet, our feet would feel heavier than our head.

Let's say our mini-planet had a diameter of 3.5 meters and a mass of about 500 million tons. With this mass, we would experience Earth-like gravity on the surface of the planet, but the force of gravity would decrease much faster than on Earth. In fact, at the height of our hips, it would be reduced to 50 percent and at our head, at around 25 percent.

Our feet, therefore, would feel four times heavier than our head! This would be like the feeling you get if you lie on a spinning merry-go-round with your head near the center — a sort of stretching sensation.

Aside from peculiar physical sensations, we could also escape from our little planet by running.

The _escape velocity_ — the minimum velocity to break free from the gravitational pull of our small planet — would be about only 5 meters per second, which is slower than a sprint.

The amazing thing about escape velocity is that the direction you are running isn't that important. Unless we actually ran toward the planet, we'd run off it as soon as our speed surpassed this velocity. We could run off our planet horizontally or jump off it at the end of a ramp.

If we didn't run fast enough to escape the gravitational attraction, however, we could go into orbit around it, since the gravitational pull is too weak to return us to the planet's surface, but it would be sufficient to hold us in its gravitational field.

> _"As a rule of thumb, if you can't dunk a basketball, you wouldn't be able to escape this asteroid by jumping."_

### 4. Six printers is enough to live-update a paper version of the whole English Wikipedia – but it would be costly. 

Want a printed version of the entire English Wikipedia? Surely the amount of printers you would need in order to keep up with the constant edits would be huge, right? Well, actually, it's far fewer than you'd think.

Wikipedia receives about 125,000 to 150,000 edits per day, and yet you'd need only six printers to stay up to date.

Let's assume each edit would require us to reprint a page. Many edits would require us to print multiple pages, but a number of other edits are actually reverts which don't require reprinting — we would just have to reinsert pages we've already printed. So assuming we need one page per edit seems fairly reasonable.

Now, 125,000 to 150,000 edits each day is equal to 90 to 100 edits per minute. A good inkjet printer could churn out around 15 pages per minute. This means we'd probably only need six printers to match the speed of the edits.

Sounds great, but there is a downside: Our printing project would become pretty pricey.

The electricity needed to run six printers around the clock would cost only a few dollars a day, so that's not bad. But there's the paper. Assuming we pay one cent per page, we would blow about a thousand dollars per day on paper alone.

What would really put us into a financial sinkhole, though, is the cost of the ink, which would be somewhere around 5 cents per page for black-and-white and around 30 cents per page for photos. This would mean we'd be spending $4,000 - $5,000 per day on ink cartridges.

What's our total expenditure, then? After just one or two months we would have squandered around half a million dollars on this project.

Maybe sticking to our digital Wikipedia is a smarter idea.

> Fact: Wikipedia user Tompw has calculated the size of the whole English Wikipedia in printed volumes, currently running at eleven full bookshelves.

### 5. The common cold could be wiped out if everyone stayed away from each other for a few weeks – but it’s probably not worth it. 

If all of us on the planet avoided each other for a couple of weeks, wouldn't the common cold be eradicated? Indeed, it probably would. If rhinoviruses don't have enough people to travel between, they simply die out.

The common cold is caused by different viruses, but rhinoviruses are the most common villains. Usually within about ten days, rhinoviruses are completely eliminated from the body by the immune system. We also don't seem to transmit these viruses to or from animals, which means there are no other species that could store these viruses for us. 

Therefore, if everybody was quarantined, the viruses wouldn't have any new victims to pounce on, and they would perish.

Yet, the consequences of keeping all humans away from one another for some time probably wouldn't be worth it.

First, not all of our immune systems are in tip-top condition. Some people with heavily weakened immune systems can have common cold infections for years, so they'd need far longer in quarantine.

Furthermore, colds aren't exactly fun, but never getting them might be worse, as mild infections actually strengthen our immune system.

There are also a few practical consequences we should consider if we were to quarantine the whole population:

The sum of the world's annual economic output is around $80 trillion. Pausing economic activity all over the world for a few weeks would therefore amount to a loss of many trillions of dollars.

And what about the practicalities of keeping people away from one another? If we divide up the world's land area evenly, the closest person would be 77 meters away from us. This is plenty of space to stop the transmission of rhinoviruses, but many of us would wind up standing around in the Sahara desert or Antarctica.

From a biological standpoint, this is a fine way to eradicate the virus. The slight downside, however, is that it would probably cause the collapse of civilization.

> _"All in all, I wouldn't stand in the middle of a desert for five weeks to rid myself of colds forever."_

### 6. If everyone had only one random soulmate, our world would be a lonely one. 

What if we all in fact had a single soulmate? Would we find the one and only? Well, the chances of bumping into our soulmate would be rather slim.

First of all, why should our soulmate live at the same time in history as we do? Perhaps they died long ago or maybe they haven't yet been born.

To get more of a handle on this, let's assume our soulmate lives at the same time as we do. Still, the odds of meeting our soulmate are miniscule:

Suppose we lock eyes with about 24 new people per day. If 10 percent of these are around our age, and we estimate that a lifetime is approximately 60 years, we'll make eye contact with about 50,000 potential soulmates in one lifetime, since 2.4 x 365 x 60 x 10 = 52,560.

There are 7 billion people living right now. Say we whittle this down to our preferred age and sex in a partner, and we can assume we have roughly 500,000,000 potential soulmates. This means we would need about 10,000 hypothetical lifetimes to make eye contact with every potential soulmate, since 500,000,000 / 50,000 = 10,000.

We'd need as much time as possible to find our soulmate, then, and only a select few could actually afford to spend time on this. Because of the time we'd need and the fear of dying alone, society could reorganize to allow as much eye contact as possible. We could do this with webcams, maybe, like a modified version of ChatRoulette.

Theoretically, if everyone chatted eight hours a day, seven days a week, and we only needed a few seconds to know if someone was our soulmate, we could pair everyone with their soulmate within a few decades.

In practice, though, most of us would struggle to spend so much time chatting. Probably only wealthy people would have enough time to spend their days on SoulMateRoulette and get a shot at finding their one and only.

> _"A world of random soulmates would be a lonely one. Let's hope that's not what we live in."_

### 7. A normal cell phone surpasses the processing power of humanity – but humans are superior in other ways. 

If the entire world's population started working out calculations right this minute, how powerful would our computing power be? Would it surpass or would it dwarf a single modern-day computer or smartphone?

Actually, a mid-range mobile phone would eclipse the combined processing power of all humanity.

If somebody manually went through the necessary computer chip calculations, she could execute the equivalent of a full instruction every minute and a half.

Going by this method, a mid-range mobile phone processor would be 70 times faster at performing calculations than the whole human population. What is more, a new high-end PC processor would be around 1,500 times quicker.

But humans and computers think very differently. And there are a whole host of things that a single human can do more speedily than any computer could.

Forcing humans to work things out on pencil-and-paper doesn't reveal much about human processing power, since they only express a tiny fraction of their brain's capabilities.

For example, we are far superior to computers when looking at a picture of a scene and correctly inferring what has happened. When measured in complexity, human brains are more sophisticated than the world's number one supercomputer.

Okay, so we're obviously pretty different, but what if we insisted on comparing humans and computers?

Then a decent processing-power rating could be made by supposing that computer programs are as ineffective at simulating human brain activity as human brains are at simulating computer activity.

Then, we can take both the pencil-and-paper measure and the complexity measure of human brains into account and form an average of these two values. From this, we would conclude that our overall performance is about on par with computers.

### 8. Correctly guessing every multiple-choice question in the SAT is virtually impossible. 

The SAT is a standardized test given to American high school students. A large part of this test consists of multiple-choice questions, and sometimes guessing an answer you're not sure about can be a good strategy. But what if all SAT test takers guessed every multiple-choice question? How many perfect scores would there be?

Well, we shouldn't expect a single perfect score, if everyone guessed. Let's see why.

The SAT has multiple-choice questions in three sections: math, critical reading, and writing. In 2014, there were 158 multiple choice questions in total, each of them offering five options and therefore allowing a 20 percent chance of one being guessed correctly.

Written as a formula, the probability of getting all 158 questions right is: 1/(5¹⁵⁸)=1/(2.7x 10¹¹⁰).

That is to say, the chances of getting all questions right is one in 27 quinqua trigintillion — a probability so remote and impossible to comprehend that it's a strain to even say it.

So even if all four million 17-year-olds guessed haphazardly, no one would get a perfect score. Not even in one of the three sections.

Alright, so it's "certain"? _How_ certain? One could say it's certain enough that there won't be any perfectly guessed scores till the end of humanity on Earth.

Say each high school student wrote the SAT a million times every day for five billion years. By this time the Sun will have swollen to a red giant and the Earth will be burnt to a crisp. Still, the likelihood of any of the students ever scoring 100 percent on just the math section, for example, would be about 0.0001 percent.

It's fair to say, then, that we won't be witnessing any perfect guessing scores till the end of humanity on Earth.

Therefore, it's a good idea to study for the SAT.

### 9. Either in the 2060s or in the 2130s Facebook will contain more profiles of dead people than of living people. 

Facebook is largely a young person's hangout. Though the average age has risen over the last few years, young people tend to use Facebook more frequently than old people. But will there ever come a time when Facebook contains more profiles of dead people than of living ones? There are a couple of scenarios to consider:

Facebook could have more profiles of dead than of living people as early as the 2060s. 

It's not a thought that usually comes to mind, but by now there are probably around 10 to 20 million people who created Facebook profiles and have since died. It's likely that approximately 290,000 Facebook users in the US alone died in 2013.

Therefore, what will determine whether dead profiles outnumber living profiles is how quickly Facebook adds new (young and living) users.

But all of this depends on Facebook's future. If the company starts losing market share later this decade and never recuperates, the number of dead profiles will exceed living profiles somewhere between the year 2065 and the mid-2100s.

But perhaps Facebook won't lose market share and will remain a staple social media platform for generations. If Facebook stands the test of time, the crossover date, when the dead outnumber the living, may not come until sometime around 2130.

This does seem rather unlikely, though. The internet is a fickle place and few things last for long. We're certainly used to rapid change being the norm for anything built with computer technology. One only needs to look at MySpace as an example.

The reality, then, probably lies somewhere between the dates mentioned above. We'll just have to wait and see.

> _"The ground is littered with the bones of websites and technologies that seemed like permanent institutions ten years ago."_

### 10. Final summary 

The key message in this book:

**Even the most ludicrous, hypothetical question can illuminate our minds and entertain us. In fact, mulling over an absurd question can lead you down some rather intriguing paths. For instance, did you ever expect that only six printers would be enough to live-update a hard copy of the entire English Wikipedia? Or that you could escape from a tiny planet just by running fast enough?**

Actionable advice:

**Try the math!**

Next time someone asks you a silly question, try to actually pay some mind to it and figure it out. You may well gain some valuable knowledge and delight the questioner with a well-considered answer. Attempting to thoroughly answer an absurd question can actually produce some pretty humorous insights you may never have stumbled upon otherwise!

**Suggested** **further** **reading:** ** _A Short History of Nearly Everything_** **by Bill Bryson**

_A_ _Short_ _History_ _of_ _Nearly_ _Everything_ offers an enlightening summary of contemporary scientific thinking relating to all aspects of life, from the creation of the universe to our relationship with the tiniest of bacteria.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Randall Munroe

Randall Munroe is a former NASA roboticist and the creator of the hugely popular _xkcd_ webcomic. In 2013, at the request of a number of _xkcd_ enthusiasts, the asteroid 4942 Munroe was named after him by the International Astronomical Union.

