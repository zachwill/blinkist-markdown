---
id: 5a65b082b238e100076c4ebb
slug: what-is-the-bible-en
published_date: 2018-01-25T00:00:00.000+00:00
author: Rob Bell
title: What Is the Bible?
subtitle: How an Ancient Library of Poems, Letters, and Stories Can Transform the Way You Think and Feel About Everything
main_color: D5CA2B
text_color: 8A831C
---

# What Is the Bible?

_How an Ancient Library of Poems, Letters, and Stories Can Transform the Way You Think and Feel About Everything_

**Rob Bell**

_What Is the Bible?_ (2017) takes a fresh look at the best-selling book of all time: the Holy Bible. These blinks take the Bible for what it is — a conglomeration of books written by real people in real historical contexts that are at once subversive, timeless and transformative.

---
### 1. What’s in it for me? Get a new, illuminating perspective on history’s most famous text. 

The Bible is the best-selling book in the history of literature, and one of the most read to boot. But regardless of how many people may have read it, not all have read it correctly.

In fact, the unending controversy surrounding the Bible is in large part a result of critics picking and choosing passages that feel problematic or wrong, while ignoring the greater context in which the book was written, as well as the stories that it tells. Such an approach is a travesty when scrutinizing a book for which context and nuance are vital.

In short, to truly appreciate, understand and learn from the Bible, you need a sense of how it originated and existed in its own time. Delving into this history will bring you a deeper understanding of a book that can shine a light onto even the most obscure corners of life.

In these blinks, you'll learn

  * how the sometimes gratuitous violence of the Bible is in the service of peace;

  * why a literal reading of the Bible misses the point; and

  * how the internal contradictions between stories within the Bible serve a purpose of their own.

### 2. The Bible is the most popular book of all time, yet it’s often misunderstood. 

What's your favorite book? Some like fiction, others history — but there's one book that's a best seller across the board. In fact, it's the best-selling book _ever_.

We're talking about the Bible, which can be found in homes and hotels around the world. But despite how common this book is, many people misunderstand its origins.

For instance, many are unaware that a number of biblical stories are rooted in the historical context in which its authors were writing, and the relative minority status of its original readers. Readers also fail to realize that lots of biblical tales began as oral traditions, passed on until they were written down.

Interestingly, while history is generally written in favor of those with power, the Bible is an exception to this rule. Rather than being penned by the rulers of the time, the Bible was written by Jewish people, an oppressed minority for hundreds of years. This is why the motif of liberating the exploited is so central to the book as a whole.

So, why were these stories published in the first place?

Originally, the ideas in the Bible were presented as a new way of viewing the world, one that gave hope to the oppressed Jews who came to see themselves as imbued with a divine purpose. For example, in the story of Exodus, Moses leads the Jews out of Egypt. In this narrative, the implication is that the divine force is on the side of the oppressed people, and that slavery can be ended. For readers at the time, this idea was absolutely revolutionary.

Later in the book, the reader discovers that, when Moses died, "he still had his vigor." The implication here is that Moses retained his sexual potency until his death.

But why does Moses' libido matter?

Well, metaphorically speaking, the idea that's communicated is that in order to break a cycle of oppression, you have to pass your freedom onto your offspring.

### 3. The stories of the Bible are ones of progress and change. 

Anyone who has ever seen the book knows that the Bible is a rather dense tome — and its writing is no less epic. This book is full of stories, poems, letters, accounts and Gospels, all of which were written down, collected and edited by some 40 people over a period of 1,500 years.

However, while the Bible, which is essentially a library of other books, may be complex, it also has a clear common thread: it describes a historical shift in the way people considered both the world and the divine.

For example, a contemporary reader might be surprised by certain biblical passages, such as when God tells Abraham to sacrifice his son. But for the book's original readers, such an action was considered a common offering to other gods.

So, for people who lived at the time of the story's writing, when God finally intervenes, preventing Abraham from taking his son's life and agreeing to the sacrifice of a ram instead, the effect is actually a subversion of expectations. For these early readers, the picture that was painted was of a new God, one that wouldn't request such a brutal offering.

It's one example of how the ideas introduced in the Bible were, and still are, extremely progressive. Just consider Genesis, in which God names Abraham father of the Jewish tribe, which would bless all others. At the time, tribes were only concerned with their own, and the idea of a tribe that cared for others was groundbreaking.

So the following episode was especially important: In the opening pages of the bible Abraham is blessed by King Melchizedek of Salem in the name of God. Abraham gives Melchizedek one-tenth of everything and they part ways.

Why is this important? What's being said here? Well, the bible explains that Melchizedek isn't part of Abraham's tribe, and he's still connected to the divine (since he blesses Abraham as such).

In other words, the implication is that God is larger than any one tribe, and people outside of religious institutions can show righteousness.

### 4. The Bible reflects humanity while pointing to the divine. 

Since real people wrote the Bible in response to real situations, the book is a compelling reflection of humanity. But within this humanity, readers can also find a much more believable picture of the divine.

In fact, the Bible is so human that it can be of service even to non-Christians. Just take one famous biblical parable in which the older of two brothers works hard to earn his father's praise, while the other squanders the money he inherits from his father's estate.

The younger brother eventually returns home destitute and begging for his father's forgiveness. In response, despite the older brother's demands to ignore his sibling, the father accepts the prodigal son openly, who then recognizes that his home is with his family. The lesson is a simple and universal one about the importance of forgiveness and the concept of home as a state of being, not a physical location.

This is just one example of how the Bible's humanity makes it accessible.

But it's also through this humanity, especially human error, that evidence of the divine can be found. The inconsistencies in the Bible point to a human tendency to mix up accounts of real events. Yet the similarities that connect these stories highlight the evidence behind them.

Just consider the different accounts in the Gospels of Jesus's resurrection. In the account given by Mark, Mary Magdalene, Salome and Mary the Mother of James all traveled to Jesus's tomb and discovered he had risen from the dead. In John's story, only Mary Magdalene went. Matthew says it was Mary Magdalene as well as the other Mary. And Luke simply describes Jesus as walking with two of his disciples!

You could take this as evidence that the book doesn't have its story straight, or that it confirms the Bible's proneness to factual error.

This seems reasonable until you consider that, when the Bible was written, the words of women had no legal bearing. In such a climate, why would anyone say that women first witnessed the resurrection of Christ? In this way, the simple detail about women discovering Jesus underlines the truth of the account.

### 5. Biblical violence illustrates progress toward peace. 

Lots of people consider the Bible a violent and barbaric tale. They may be right, but such an assessment has to be put into context. For instance, the Old Testament was first compiled in Babylon around the year 500 BC, at a time when the Babylonians were forcibly removing the Israelites from Jerusalem and sending them into exile.

This implies that the authors of the Bible suffered atrocious acts of violence, which played a central role in shaping their view of the world, both present and future.

Nonetheless, the Bible tracks a gradual shift away from violence and toward a more peaceful vision for the world. Just take the book of first Kings, in which Solomon utilizes slave labor and a military to expand his empire. This story establishes a narrative of greed and the quest for power.

Later in the book, the authors condemn violence over and over again. For instance, in the book of Judges, God sends Ehud to rescue the Israelites from Moabite oppression. In doing so, Ehud uses violence and nothing is solved; in the beginning of the next chapter, the Israelites are oppressed once again.

Through such narratives, the Bible ultimately shows that senseless violence will never solve anything. Rather, kindness and forgiveness are the tools necessary to experience the love of God.

That being said, many still misunderstand God as being full of wrath rather than love. For example, some people read Paul's letter to the Romans and assume that God will rain down fury upon those who reject him. This is reasonable since the passage asks, what if God chose to demonstrate his destructive power, but was careful not to destroy the people he created without justification?

But if you read carefully, you'll see that a vengeful God is not what's being communicated here. More explanation can be found in chapter 11, which asks the question, "did God reject [God's] people?" The answer is an affirmative, "by no means!"

In other words, the Bible invites the reader to set out on a path toward God, but the reader may choose to take it or not.

### 6. The Bible is rife with contradiction, representing the differing views of its authors and the contexts in which they wrote. 

People commonly complain that the Bible contradicts itself — and they're right. In fact, that's the point.

To take in such complexity, the Bible must be approached as a perpetually developing narrative. For instance, in 2 Samuel 24, God tells David to take a "census." But, in Chronicles, which was written hundreds of years later, Satan is said to have told David to take the census.

At the time, a census was used to assess the size of an army in preparation for war. So, taking a census would stand in stark contrast to the peaceful approach of this new tribe of God; it represented the old approach of building empires and sowing terror.

So why was the account of who ordered the census changed?

Well, people who lived at the time of the writing of Samuel saw gods as necessarily violent. However, as their thinking became more sophisticated, they came to see that a benevolent God would never ask David to do an evil thing. So, in the end, it had to be an opposing force, one representing evil, that made this request.

Interestingly, this passage in the Chronicles is the first Biblical mention of Satan whatsoever. This force of evil was itself a concept developed over time.

It's one of the many ways the Bible changed and contradicted itself as it developed. And this fluidity is only to be expected, since the authors didn't necessarily agree on everything.

For example, in Genesis, the first book of the Bible, God tells Abraham that every man in his tribe must be circumcised to show their commitment to the Lord. But close to the end of the Bible, in a letter from Paul to the Galatians, the author likens this very process to enslavement. To add further complexity, in this exposition, Paul's issue isn't with circumcision itself, but with using it to alienate people from the population.

And what makes these incongruities even more confusing is that, because of the vast expanse of time between the writing of each statement, the two can't be directly compared; they're the products of different people writing in different contexts.

### 7. A literal reading of the Bible leaves much to be desired. 

Many people view the Bible with cynicism; they wonder how people can believe such myths and fairy-tales. It's also quite common for people to assume that one discredited biblical story invalidates all others in the book, mythical or otherwise.

The problem with such stances is that they ignore the way these stories have been told. As a result, they cause people to read the Bible in an intensely literal manner, missing the entire point of the book.

A better approach is to read the use of magic and myth in the Bible as poetic, rather than literal. For instance, in Jonah chapter two, God tells Jonah, an Israelite, to bring a message to the city of Nineveh in Assyria. Nineveh had invaded Jerusalem and forced the exile of Jonah's people. Jonah refuses and instead takes a boat in the opposite direction.

When a storm brews, the crew turns on Jonah, accusing him of causing the bad weather and throwing him overboard. Jonah is in turn swallowed by a fish, in whose stomach he prays for three days. In the end, the Lord commands the fish to vomit Jonah up on land and he finally goes to Nineveh where he finds the Assyrian people extremely receptive to God's message.

Naturally, this whole account is entirely unbelievable — but the fact of whether Jonah was literally swallowed whole isn't what matters. The story has a meaning much larger than this literal content; it begs the question, can Israel forgive the Assyrians? In answering this matter, it confirms that God's followers are loved by all, including their enemies.

By viewing such a story through a literal lens, a reader could easily miss all of the profound learning buried within it.

But challenging yourself to view things more poetically isn't the only way you can get more out of the Bible. In the next blinks, you'll learn a few more approaches, which begin with an early star of this holy book.

### 8. We can learn much more from the Bible by studying it with curiosity. 

When people approached Jesus, asking him about the Old Testament, he responded by asking them a question of his own: he wanted to know how they interpreted it.

Similarly, we can ask ourselves questions about the Bible that lead us to more substantial understandings. For instance, rather than asking, "why would God. . . ," we can ask why the authors thought this story was important enough to write down.

A good example here are creation stories, which were and continue to be important for making sense of humanity's place in the world. Just take the Babylonian creation story, in which the god Murnak tears in half the carcass of the goddess Tiamat, and uses the two halves to compose the world.

By contrast, in the Bible's creation story, God produces the world from divine joy. So, why did the authors think this was important enough to write down?

Well, compared to the previous creation myth, it offered an alternative, nonviolent conception of divinity and what it means to be human.

Another helpful question when considering the Bible is why a particular passage has endured the test of time. Just take the story of Babel, in which the people build a tower high enough to reach heaven, turning the ruler of Babel into a God and allowing him to abuse his power.

Reading the Bible more closely in its different translations shows that the people of Babel accomplished this feat using brick and stone, new technologies for the time.

This latter point is important as, in the end, God decides that the people of Babel should not be allowed to succeed in their overly ambitious, power-hungry endeavor. So, he confuses their languages such that they cannot understand one another.

In ending this way, the story reminds the reader that humans must keep power and technology under control. We have much to take from this lesson today; it's the same principle at play when contemporary people say that Wall Street has too much power.

> _"The Bible is a reflection of growing and expanding human consciousness."_

### 9. The Bible can have transformative power if we embrace it for what it is. 

By now, you know a great deal about the Bible. But how else can its teachings be utilized today?

Well, this holy book clearly has the power to change human perceptions of the world, and its teachings can just as easily be applied to contemporary life.

For instance, a constant theme in the Bible is the apocalypse, which emerges in different forms. A good example is the story of God launching a world-engulfing flood to cleanse the mess that humans have made of the planet. In the end, only Noah and his family escape.

But why were such stories written?

In general, they came out of a basic human fear that life could end in the blink of an eye. However, while such a possibility can't be controlled, people _can_ control how they behave on Earth and uphold the divine responsibility to care for the planet.

At present, this responsibility is being skirted, such as through humans' abuse of fossil fuels, the pollution of natural resources and the use of toxic chemicals. But what if all the human anxiety about the end of the world could be redirected toward preventing climate change?

By embracing biblical teachings and questioning them with curiosity in our hearts, we can begin to see such ancient stories as perfectly relevant to the present day. In fact, the Bible even has the potential to change our hearts; that is, if we're willing to let it.

Just take Matthew's account of the resurrection story. In it, he says that when Jesus met with his followers in Galilee, "they worshipped him; but some doubted."

Why would the authors include this? Doesn't it undermine the narrative?

When reading the Bible, asking such questions forces us to confront ourselves and consider profound issues of our existence: How open are our minds? What would happen if we lived as if Jesus _had_ in fact been resurrected?

By putting our skepticism aside, we can embrace the stories of the Bible as great works of poetry — as tales of humanity that can transform our lives.

### 10. Final summary 

The key message in this book:

**The Bible, the most popular book of all time, is in fact a collection of books that, on the one hand, depict violence and greed, but, on the other hand, also advance an overarching story of progress. By considering the historic situations in which real people wrote these stories, we can see the Bible through fresh eyes and apply it to our lives today.**

Actionable advice:

**Use a biblical mantra to expand your mind.**

Throughout your day, repeat the mantra from First Corinthians 3: _All things are yours._ Write this saying down on your hand or somewhere that makes it readily available, and repeat it when you encounter something inspirational. As you welcome this mantra into your life and your heart, see if it can open your mind to the world around you. Have you missed anything beautiful in your daily life that you now notice?

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Reason For God_** **by Timothy Keller**

In _The Reason For God_, famous New York pastor Timothy Keller defends Christianity and its core beliefs against the most common objections. His fresh approach provides several arguments for continued Christian faith.
---

### Rob Bell

Rob Bell is the _New York Times_ best-selling author of _Love Wins_, among other titles and founder of the nondenominational Christian megachurch Mars Hill Bible Church. In 2011, he was named one of the "100 Most Influential People in the World" by _TIME_ magazine.

