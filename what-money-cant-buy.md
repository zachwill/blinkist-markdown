---
id: 533ae9673430340007380000
slug: what-money-cant-buy-en
published_date: 2014-04-01T09:56:14.000+00:00
author: Michael J. Sandel
title: What Money Can´t Buy
subtitle: The Moral Limits of Markets
main_color: A0A192
text_color: 696E2D
---

# What Money Can´t Buy

_The Moral Limits of Markets_

**Michael J. Sandel**

The blinks to _What Money Can´t Buy_ (2013) explain how market-driven thinking — like the introduction of incentives and making everything available for a price — has snuck into almost every sphere of our lives. This means we are often suddenly confronted by serious moral concerns when market morality manifests itself in an area where it doesn't belong.

---
### 1. What’s in it for me: Find out what shouldn’t have a price tag. 

Have you ever had to hang on to a job you hated just because you needed the money?

If not, count yourself lucky, but if you have, did you ever feel this was unfair? After all, if you had been rich, you wouldn't have had to suffer.

The idea that your labor in this job can be bought, even if you find it repulsive, is an example of market thinking.

This thinking has fuelled economic growth over the past three decades, but it has also started to encroach into other areas of life, thereby provoking very disconcerting moral questions.

For example, what if your unpleasant job hadn't just been unpleasant, but downright dangerous?

In these blinks you'll discover why market thinking can lead to morally questionable practices, like paying female drug addicts to be sterilized.

You'll also read find out why you should think twice before rewarding your children for good grades.

Finally, you'll find out how much it will cost you to get your doctor's private cell phone number so you can get "extra special" health care.

### 2. Market thinking has spread unnoticed into almost every sphere of social life – this is a challenge for society. 

For over 30 years mainstream economists have had unwavering faith in the free market. However, since the financial crisis of 2008, debates questioning this faith have raged.

These debates have concentrated too intently on how this _market_ _thinking_ — characterized by traits like deregulating markets and treating everything as mere purchasable commodities — has influenced the economy.

However, over the past 30 years, market thinking has also encroached into other areas of society. In fact, market thinking has influenced society much more profoundly than most people think. It has had a massive impact on virtually every sphere of life, including our schools, health care and security.

For example, in the United States you can pay to have a closer relationship with your doctor. For a sum starting at $1,500 a year you can have their personal cell phone number. 

Another example is that in the United States and the United Kingdom, publicly employed police officers are a minority in the provision of security, because private firms run most security details, for example, private firms manage many prisons.

Yet another example is found in Dallas, Texas, where school children have monetary incentives to read at $2 per book.

This expansion of market thinking is due to the fact that since the 1980s and in particular since the end of the Cold War, free markets have proven to be the most efficient mechanisms for generating wealth. For example, when stock markets were deregulated, stock prices skyrocketed, particularly in the 1980s.

Similarly, as more and more countries have liberalized their economies, global economic output has flourished and the total gross world product has more than tripled.

Because of their proven usefulness in this respect, people have tended to accept free markets without realizing that market thinking has expanded into ever more spheres of our lives. 

These developments force us to confront the fundamental question of what kind of society we want to live in.

### 3. The triumph of market thinking has major implications for social justice. 

As we have seen, market thinking has become very widespread in society. But why should this worry us?

Basically, for three broad reasons: _social_ _justice,_ _the_ _corruption_ _of_ _our_ _values_ and _the_ _promotion_ _of_ _wrong_ _attitude_ s.

Let's discuss the first of these — social justice — in this blink.

The first implication of widespread market thinking on social justice is that it leads to more inequality in society: when more and more things are up for sale, those with money can buy the things they want, and hence are clearly in a better position than the less affluent. This increases inequality.

For example, today it's already possible to jump the queue at airport security or amusement parks by paying extra. And as you'll recall, the ability to call a doctor day and night is also available for the right sum.

Furthermore, in a market where everything is for sale, poor people can be forced to agree to even degrading exchanges to get money to meet their expenses. For example, they might agree to "rent out" their forehead as advertising space to companies — an exchange most people would not agree to but which is actually fairly common today. And although the person may technically agree to the exchange freely, their circumstances — poverty — may in fact be forcing him or her to do so.

If market thinking increases its influence further, we might even see people auctioning off their foreheads for permanently tattooed advertisements or, worse still, selling their kidneys to rich people who need them.

In general, the root cause to be overcome behind these trends is the current uneven distribution of income.

### 4. While market thinking is effective at directing people’s behavior, not all things should be bought and sold. 

Let's examine the second of the three reasons why we should be concerned about the expanding influence of market thinking: things that shouldn't be mere commodities become them anyway.

We value certain things in our social lives for reasons beyond their monetary worth, so if these are turned into buyable commodities, we feel these things become corrupted.

For example, consider the importance of free Fourth of July fireworks to Americans. Now imagine that a city started charging people to watch them. Obviously this would create inequality as poor people might not be able to afford the ticket, but such a practice would also degrade _the_ _character_ and _values_ intended for the event. After all, the celebration is meant to honor values such as independence and freedom, and it is meant to be a gift which is available to all people. Business considerations would taint these ideals.

Also, while in many cases market thinking may be effective in directing people's behavior, the moral considerations can be overwhelmingly disconcerting.

Consider a popular American charity called _Project_ _Prevention_, which pays female drug addicts to be sterilized. The goal is to reduce the number of drug-addicted babies born, and the project has been very successful. However, despite the efficacy of this market-based approach, the very concept is clearly highly questionable from a moral perspective. 

First of all, here too, the question of inequality and unfair trades arises, as drug addicts tend to be poor, so they are effectively forced to agree to the procedure to earn money.

Second, this practice corrupts commonly held values because it commoditizes the human body and a woman's ability to bear children. Most of us would probably recoil at the thought of living in a society where such things are on sale.

It seems therefore that while market mechanisms can be very effective at directing people's behavior, certain "goods" cannot be bought and sold without grave moral considerations.

### 5. The introduction of market norms in new places can have unintended, unfortunate and irrevocable consequences. 

We've now looked at two concerns regarding the spread of market thinking, so let us examine the third: its negative consequences on our attitudes and morals.

You see, when market thinking is introduced somewhere, it does not merely increase efficiency, as economists believe. It effects a change in the norms and values of that sphere of life.

This is because as soon as you define a price for something, business interaction-related values and norms begin to apply.

For example, say you want your children to write thank-you notes for your wedding so that they learn the importance of gratitude. To incentivize them, you offer them compensation: one dollar per card. However, this will not help teach them the value of gratitude, but instead teaches them that writing notes is something they should get paid for. This will change their values and norms, and you might soon find them requesting money for other tasks as well.

What's more, market norms can often undermine and even irrevocably replace _non-market_ _norms_ that existed beforehand.

For example, in the thank-you note case, the non-market norm that your children had before might have been that it's expected for children to do chores for their parents. But now the new norm of getting paid for the task has overruled this.

One study of an Israeli child-care center illustrates this vividly. To incentivize parents to arrive on time to pick up their children, the center introduced fines for parents who turned up late.

Surprisingly, the late pickups actually increased.

It turned out that the monetary incentive of the fine had actually replaced the existing non-market incentive that it was rude to force the center's employees to wait. The parents were now paying for their lateness, so they no longer felt bad about letting the employees wait, and this increased late pickups.

Unfortunately, this was not a reversible process: late pickups remained higher than before even after the center tried to correct its mistake by removing the fine again.

In the next blinks you'll learn about the blind spots of market thinking.

### 6. Economists are insufficiently aware of the moral questions raised by market thinking. 

As we have seen, market thinking gives rise to many difficult moral questions. This means it is important to understand the economic theories that have fuelled the 30 year-long expansion of market thinking.

In general, the theories rely on some simple assumptions regarding human nature and the world — for example, that people are primarily driven by self-interest and that free markets are the most efficient way of matching supply and demand.

The theories effectively sidestep most moral concerns like fairness, equality and the degradation of values, however. The only morality present in the theories is _utilitarian_ _moral_ _theory_, which is only concerned with satisfying the wishes of individuals.

This ignorance of moral questions is problematic because as we have seen in previous blinks, those questions clearly arise as market thinking spreads. As a reminder, consider that market thinking has led to a situation where a poor person can earn over $7,500 for volunteering as a human guinea pig for drug safety trials. The moral issues here are of course huge: How can we with good conscience allow people to put themselves in danger just so they'll have the means to survive? Is it fair to force them into this dilemma of survival versus personal danger?

Such serious moral questions are not touched upon by economists, however, and in this sense their theories have provided little moral guidance for building a society. Clearly this is food for serious thought.

### 7. Contrary to economic theories, altruism should be encouraged so that people grow their capacity for it. 

Have you ever wondered why the baker on the corner is so eager to provide you with breads and pastries? As Adam Smith, the father of modern economics, made clear, it is not due to benevolence, but rather his interest in taking our money. This belief that self-interest drives humans and thereby keeps the economy going forms the basis for much of market thinking today.

However, this belief is not without its problems.

To understand this, let us consider the phenomenon of _altruism_. Economists who believe people are primarily motivated by self-interest see altruism as a scarce and finite resource: something to be used sparingly and not squandered on unworthy targets.

For example, to encourage more people to donate blood, economists would prefer to offer a monetary incentive rather than rely on people's altruism so that this scarce resource can be saved for something more important.

But there's also a school of thought in which altruism should be encouraged, as our capacity for it can grow.

After all, it has never been empirically proven that altruistic behavior is a limited resource, so this conviction should be challenged by other views.

Several important philosophers have advocated views that do just that. Both Aristotle and Rousseau, for example, believed that capacities like social solidarity and being virtuous can be practiced and will even strengthen as we act to benefit others, deepening our connection to them.

This means that virtuous behavior should not be used sparingly, but rather encouraged in general. This may well make people care more deeply about the needs of others, and thus cultivate a broader capacity for altruism.

In the case of blood donations, it may be better to promote the gesture as a virtuous deed, so more people will donate blood out of conviction and thus be inclined to even more altruistic behavior in the future.

In the last two blinks you'll discover what we should do about the expansion of market thinking.

### 8. We must decide case by case whether the benefits of a market thinking-inspired policy outweigh the moral concerns. 

Ask yourself, is it OK to pay children to read more books?

For most people this is a difficult question to answer because it raises numerous other complex questions. This ambiguity arises from the diverse nature of the problems caused by market thinking.

For example, as we read earlier, market thinking has led to some female drug addicts being paid to get sterilized as well as some people "renting out" parts of their bodies for companies to display advertisements on.

First and foremost this raises the question of whether this is a fair exchange given that people may in fact be forced to accept these deals due to poverty. But both examples also raise the question of the value of the human body and whether or not it should be bought and sold.

Of course, in some cases the use of market thinking may be justifiable despite moral counter-arguments.

For example, many people might feel that it is not right to pay children to read more books, as it teaches them the wrong values by making education a mere instrument for gaining wealth.

However, if these incentives encourage children in poor families to read more and thereby get a better education and better work later on, it could be the most effective way of raising them and their families out of poverty. Thus the benefits could outweigh the moral concerns to make it justifiable.

It seems then that there is no single clear answer to the question of when market thinking is acceptable and when it is not. Rather, we must ponder each case individually, weighing the benefits against the moral problems, because sometimes market thinking can be justified.

### 9. We need a serious public debate about where market thinking is welcome in our society. 

Now that we have discussed the problems and difficulties that arise from the expansion of market thinking, you might be asking yourself what should be done about it.

Quite simply, we need a public debate in the media, civil society and in government regarding the role of market thinking so that its expansion is no longer ignored as it has been in recent decades.

This is the only way we can find out what things we as a society want to be up for sale and what we don't. Naturally, different opinions and disagreements will arise, but a democratic public debate is the only way we can stop market thinking invading our entire social lives.

Debating the merits of market thinking in various cases will also lead us to a more fundamental question: what is _the_ _good_ _life_? This old philosophical question forces us to think about what values characterize a good life, and the answer gives rise to the pros and cons in each case of market thinking-based policy.

Furthermore, we must ask ourselves what we want our society to look like. How important are social justice and solidarity with others? Which values are worth protecting?

In this way we must all put our moral and spiritual convictions on the table so we can have a truly democratic debate about what the good life is. This is the only way in which we can keep market thinking from intruding into areas of our life where it doesn't belong.

### 10. Final Summary 

The key message in this book:

**Market thinking has fuelled economic growth over the past 30 years, but has simultaneously encroached into many areas of our life where it does not necessarily belong. It can, for example, foster inequality and corrupt the values we hold dear. Therefore we need a clear, democratic debate on where market thinking is welcome and where it is not.**

Actionable advice:

**Start thinking about what kind of society you want to live in.** ****

The only way to define clear boundaries for market thinking is via a public debate. This means that you too should start thinking about what kind of society you want to live in. What are the things that you would not want to be for sale, no matter what the benefits?

**Think about moral consequences.**

The next time you come across a solution to a problem that is clearly based in market thinking — for example, someone being incentivized to change their behavior — think about what the moral implications of this solution are.
---

### Michael J. Sandel

Michael J. Sandel is a professor of government at Harvard University as well as a political philosopher. He has been an influential author in the field of justice and ethics for three decades and is famous for his free online course, _Justice: What's the Right Thing to Do_?

