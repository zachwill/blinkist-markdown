---
id: 56faa6c6df814200070000a0
slug: what-the-fork-are-you-eating-en
published_date: 2016-04-01T00:00:00.000+00:00
author: Stefanie Sacks
title: What the Fork Are You Eating?
subtitle: An Action Plan for Your Pantry and Plate
main_color: FAE45D
text_color: 948737
---

# What the Fork Are You Eating?

_An Action Plan for Your Pantry and Plate_

**Stefanie Sacks**

_What the Fork Are You Eating?_ (2014) reveals the hidden ingredients in the food you eat every day and the problematic effects they can have on your body and mind. These blinks give you a plan on how to change your relationship with food and become healthier by eating better and more consciously.

---
### 1. What’s in it for me? Learn not to poison yourself with the food you eat every day. 

It's not difficult to eat healthily. You know you're not supposed to eat tons of hamburgers or greasy fries. Instead, your plate should be piled with plenty of green vegetables and low-fat foods. 

Simple, right? Well, of course not. For one, choosing to consume lots of so-called healthy foods may mean you're gobbling tons of nasty additives or unhealthy ingredients. Do you know where and how that fresh carrot you just nibbled was grown? Did the farmer use pesticides, for example?

To truly eat healthily, you need to get to the heart of the food industry's attempts to hide what they put on or in the food you eat. These blinks explain what to look for on packaging and in ingredient lists, to ensure you eat only what nature intended.

In these blinks, you'll also discover

  * which animal waste product tastes like vanilla;

  * why some of the worst additives are government approved; and

  * what a food diary can tell you about yourself.

### 2. Government regulations against dangerous food additives have been historically ineffective. 

When you shop at your local grocery store, are you worried you might buy something that could be dangerously unhealthy? After all, the government maintains regulations to protect consumers. 

But just how effective are those regulations?

Not very. Food safety in the United States has a bad track record, with regulations that have been historically careless when it comes to keeping dangerous food off our collective plates.

In 1914, for example, the government amended the Pure Food and Drug Act, making it illegal to mislabel food that contained dangerous additives or colorings. Yet this act went largely unenforced. Thus, a product like "Bred Spread," a strawberry jam substitute that contained coal tar and no natural strawberries, remained on store shelves.

The negligence didn't stop there. In 1958, the Food Additive Amendment was supposed to make mandatory that companies prove additives used in food products were safe. Although the law seemed straightforward, companies nonetheless found a loophole: the law only applied to _new_ additives. 

Additives already used in the market, such as monosodium glutamate (MSG), were classified as _GRAS_, or "generally recognized as safe," even though companies didn't have to provide any evidence to support this claim. 

Some people suffer from an ailment called _monosodium glutamate symptom complex_, a reaction to foods with MSG, which can include chest pain, headache, nausea and difficulty breathing. While there is plenty of scientific data to question the safety of MSG in food, it isn't enough to change the additive's GRAS classification.

So it pays to think twice about whether government food regulations keep you safe. A better strategy is to be aware of the ingredients that could be harmful to you. Let's learn how you can do this.

> _"I trust science. But when it comes to things in your food or being done to your food, the science can be highly subjective (depending on who is funding the research)."_

### 3. Many additives come in the form of flavors and colors. 

The first step to eating healthier is knowing which foods are safe and which additives you should avoid. Let's start by looking at two of the most dangerous: artificial flavorings and colors.

There are two types of flavorings: _natural flavors_ and _artificial flavors_. You might be surprised to learn that both come from a laboratory. The difference is that natural flavors are at least generated from the essential ingredient. "Natural strawberry flavor" is thus derived from a real strawberry.

Artificial flavoring, however, can come from anything. The chemical used to create artificial vanilla flavoring can be extracted from cow dung!

Avoiding natural and artificial flavorings altogether can be challenging as such additives are found in most processed products. Yet there are still good options in the market. One brand is Annie's, a food company that makes products flavored with real ingredients rather than laboratory-based flavorings.

The other additive to avoid is artificial coloring. Artificial coloring was first invented in the nineteenth century, with scientists extracting the additive from coal tar. Today, most artificial coloring is made from petroleum. 

The reason many companies prefer to use artificial coloring over natural coloring is because artificial colors are more vibrant and long-lasting. There are hundreds of varieties of artificial colorings, but only three colors make up 90 percent of what is used in the market today: Yellow #4, Yellow #5 and Red #40.

How artificial coloring affects our health is a debate that still rages in many scientific and health communities. There is even evidence to suggest that artificial coloring can cause cancer, neurological damage or gene mutations. 

For this reason, despite continued use throughout the United States, artificial coloring has been banned in the European Union. A McDonald's strawberry sundae in the European Union is colored with real strawberry coloring, for example, while in the United States, the company uses Red #40.

Overall, the best advice is to seek out food made with natural coloring alone. But artificial flavors and colors are just a small sample of what the food industry uses in its products. What else is out there?

### 4. Additives used to both preserve or sweeten processed foods can also be potentially harmful. 

Humans have been preserving foods for thousands of years. By using natural methods such as freezing, salting and smoking, we were able to keep food fresh during harsh winters or long voyages.

But over the years, methods have changed — and companies now use chemicals to preserve foods.

For instance, to prevent food from going moldy, companies use chemicals called _antimicrobials_. To prevent spoilage, they use _antioxidants_. But while such chemicals can make a McDonald's french fry stay "fresh" for a month, many of these additives are harmful.

Antimicrobial _benzoates_ are used to prevent acidic food from spoiling, for example. But when sodium benzoate is added to a beverage containing vitamin C, it can create _benzene,_ a chemical compound that has been linked to leukemia.

This is why it's important to read a product's ingredient list and try to avoid artificially preserved products.

You should also be wary of foods that are artificially sweetened, as most artificial sweeteners are made primarily from oil. Studies that have looked at artificial sweeteners like _saccharin_ have suggested that such products can potentially lead to reproductive problems, genetic damage and even cancer.

But in addition to artificial sweeteners, you should, in general, pay attention to the amount of sugar you eat.

Sugar comes in three different categories: liquid, brown and white, which is the most processed. Each category can be derived from sugarcane, sugar beets or corn.

The bottom line is that sugar is highly addictive, and its overconsumption has resulted in a worldwide epidemic of type 2 diabetes and obesity. What's more, the potentially dangerous effects of eating sugar made from genetically modified organism (GMO) sources are not fully known. 

So when you cook at home, you might consider substituting less-processed sweeteners, like maple syrup, for processed sugar.

What we've learned in past blinks has addressed _direct additives_, or ingredients added directly to your food. In the next blink, we'll look at concerns over _indirect additives_.

> Sugar is often hidden behind other names, such as caramel, corn syrup, dextrin, molasses and panela.

### 5. Many farms use dangerous additives like pesticides and hormones to boost production and profits. 

Chances are you probably don't want to eat something that can also kill an insect. Or how about consuming some antibiotics with your lunch? If you're not careful, this is exactly what you're doing.

Most vegetables while growing are treated with pesticides. Such chemicals kill unwanted pests, but they also tend to remain on and in the vegetables you buy then eat.

In the United States alone, some 5.1 billion pounds of pesticides are used on crops each year. While these chemicals have been tested as safe, it's the people who produce the pesticides who often do the testing. What do you think: would an interest in profits outweigh a company's interest in health?

In 2011, for example, green beans used for baby food tested positive for a toxic pesticide considered too dangerous even for use in home gardening.

Of course, eating vegetables is important for a healthy, balanced diet. So when you can, choose organic vegetables instead, and use apps like _Dirty Dozen Plus_, which can point you in the direction of healthier, safer foods.

When eating meat, it's important to consider the antibiotics and hormones some farmers give the animals they raise. Many farms put profits before the welfare of animals and feed livestock hormones and antibiotics so they grow bigger and stronger more quickly.

While the use of hormones has become a fact in the meat industry, it also has been linked to causing premature puberty in girls, an increased risk of breast cancer in women and a lower sperm count in men. 

To avoid eating antibiotics and hormones along with your steak, you have two options: forgo the steak altogether to become vegetarian, or ensure that the meat you eat is certified organic, which allows you more insight into how the animal was raised and what it was fed.

Sadly, most people do better research when buying a car than buying food. Let's explore how you can become more knowledgeable overall about what you put on your plate.

### 6. Be wary about claims on product packaging; most stretch the truth or intentionally mislead. 

When you shop for food, how do you choose which products to buy? Many people simply look at a product's nutritional value, selecting those with packaging which claims "30 percent reduced fat" or similar. 

But there are better ways to shop, and importantly, many packaging claims are simply misleading.

The nutritional "daily value" listed on a product's packaging is often based on an "average" daily intake of 2,000 calories, a detail you might miss. And considering today's unhealthy, overweight population, this number may no longer be an accurate average.

The nutritional information given on packaging is also based on a recommended serving, which is often different than the total amount contained in the package. A consumer who eats an entire package of chips may be eating two or three servings, thereby doubling or tripling the number of calories listed from a "recommended" serving.

Other nutritional claims, such as "50 percent less sodium than the leading brand," are simple marketing ploys. While there are regulations in place to verify such claims, fact checks are done only after a product is released, meaning false information can end up on the supermarket shelf. So take these claims with a grain of salt!

You should also be aware of misleading claims about animal welfare and additives.

Many companies say their products are "additive-free," "artisan," or contain "no antibiotics." These statements are unverifiable, and should be considered meaningless. Eggs from "free-roaming" chickens can mean any number of things.

The best option for obtaining the true nutritional value of a product is to look for clearly regulated labels such as USDA Organic, Whole Grain or Rainforest Alliance Certified. To use these labels, a product has to meet clear standards and regulations. 

So now you have the knowledge to eat healthily! It's time to put what you know to use.

> _"If you can't pronounce it, you probably shouldn't consume it."_

### 7. Start a new, healthier you by systematically swapping bad food for healthier alternatives. 

Starting a new relationship with food is not an easy task, as it requires you to reevaluate what you decide to put in your shopping cart and eventually on your plate.

Here are a few simple steps to get you started. Start by identifying which of the food products you buy are safe and which contain too many additives. Then you can determine whether you can swap out the problematic product with a healthier alternative. 

For example, maybe you can find a brand of crackers that lists ten simple ingredients instead of 26 mysterious ingredients and additives. Chances are the healthier ones will taste just as good!

Don't worry too much if there are products that you love and for which you can't find a healthier alternative. This isn't about getting rid of everything you enjoy! It's okay to compromise and keep that bag of Skittles for now if you can't live without them.

It's important to note that you shouldn't throw away food. There are always people and organizations that need food; anything you decide to get rid of should be donated to those in need.

With this in mind, go through your pantry then your freezer, followed by your refrigerator. After doing so, make sure you clean up and put things back to prevent food from spoiling or being forgotten.

Don't be fooled by diet products, either. You might be tempted to keep them, thinking they are a healthy alternative. But the truth is that many diet products are no better than the regular product. Ice cream is ice cream no matter what, so it's better to treat yourself to a tasty Ben & Jerry's than it is to eat a bland "diet" alternative.

So now that you've started your healthier journey, how do you stay on the right path?

### 8. Consider keeping a food journal to get a better sense of bad habits and how to break them for good. 

Have you ever made a New Year's resolution, only to have it fall apart in weeks? Here are some tips to stay on track and continue making healthy eating choices.

First, acknowledge bad eating habits and think about how you can change them for good.

You can start a food journal to make note of everything you eat and drink. This way you can truly account for what you're eating, and make a note of how you feel afterward. Many people are surprised to find out how many alcoholic beverages and sweets they consume.

Based on your food journal, you can pick three realistically achievable changes you'd like to make to your diet. You can think of these as roadblocks on your way to eating healthily for the long term.

With these roadblocks in mind, you can start planning how to overcome them. Often this will involve making just small, subtle changes to your everyday routine.

For example, if your food journal reveals that you routinely skip breakfast, a meal that is vital for a good diet, you can try setting your morning alarm ten minutes earlier. This should allow you time to make a healthy breakfast, and keep you from being tempted by that sugary donut at work.

After all, planning ahead is the best way to make a lasting change!

Before heading to the store, take 15 minutes to create a shopping list, planning when and what you'll cook. Be smart about your purchases, and only make one or two big shopping trips per week. And by purchasing mostly perishable items, you'll be more likely to use them and not let them go to waste.

Chances are you won't be able to cook every day, but try to make as many of your meals as you can. For other days, try to plan ahead and have quick staple dishes ready for balanced meals, like turkey meatballs and a simple green salad.

And it's fine if there are nights when a pepperoni pizza hits the spot. Don't worry; enjoy it!

> _"I won't know what to fix if I can't find what's broken."_

### 9. Final summary 

The key message in this book:

**It's not difficult or expensive to nourish yourself in a healthy manner. But you need to know what's in the food you're eating to understand what you have to change and how to change it. With this knowledge, you can outline a plan to improve your eating habits with minor yet important adjustments.**

Actionable advice:

**Eat a healthy breakfast.**

Before you skip another breakfast, stop to realize the consequences. Without breakfast, you will continue to make more and more unhealthy food choices throughout the day. 

**Rethink your pantry.**

Take the chance to restock your pantry with better food choices. Don't just look at a package and ask, "What are these ingredients?" Choose instead a healthier product with fewer additives and artificial flavorings.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _In Defense of Food_** **by Michael Pollan**

_In Defense of Food_ is a close examination of the rise of nutritionism in our culture, and a historical account of the industrialization of food. An expert in food ecology, author Michael Pollan takes a look at the way in which the food industry shifted our dietary focus from "food" to "nutrients," and thus narrowed the objective of eating to one of maintaining physical health — a goal it did not accomplish.
---

### Stefanie Sacks

Stefanie Sacks is a bestselling author, educator, speaker and consultant. She is also a culinary nutritionist with over 25 years of experience, including a master's degree in nutrition from Columbia University.

