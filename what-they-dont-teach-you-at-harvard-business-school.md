---
id: 59c8fcbab238e10005c201d7
slug: what-they-dont-teach-you-at-harvard-business-school-en
published_date: 2017-09-28T00:00:00.000+00:00
author: Mark H. McCormack
title: What They Don't Teach You at Harvard Business School
subtitle: Notes From a Street-Smart Executive
main_color: 2F8BE9
text_color: 2F8BE9
---

# What They Don't Teach You at Harvard Business School

_Notes From a Street-Smart Executive_

**Mark H. McCormack**

_What They Don't Teach You at Harvard Business School_ (1984) is an introduction to everything your professors don't and can't teach you at business school. Learn tips and tricks that only people with real job-market experience have in their arsenal, like how to make a good impression and how to leverage the concept of fear when making sales.

---
### 1. What’s in it for me? Walk and talk like a street-smart business savant. 

Perhaps you've aced all of your exams at business school, and maybe you've already read countless books about how to succeed in your entrepreneurial efforts. But regardless of how much theoretical knowledge you've accumulated, it's quite a different matter to apply these lessons out in the real world.

And that's what these blinks have for you: tips and tricks for building and running businesses, handed to

you from those who already have years under their belt on the job market, and who are already doing business — really, really well.

In these blinks, you'll also learn

  * how Pepsi won over Burger King;

  * why holding back is sometimes the best move; and

  * the advantages of starting your business on a slow burn.

### 2. Get insights from your business colleagues by listening to them closely. 

When it comes to business, it's easy to buy into the misconception that it's all about numbers and growth. But it's not! It's about people.

It doesn't matter if you're selling a product or hiring someone; to get ahead in business, you have to know who exactly you're doing business with. If you know their personality and what makes them tick, you'll be able to predict their behavior.

You're probably used to the clichéd slick businessman facade. But remember, a businessman won't fulfil that stereotype with everyone. He'll talk to his boss, clients or employees in different ways.

If you're aware of these different fronts, you'll know that there's more to the "suit" than meets the eye. There will always be a lot going on beneath the surface.

But how do you discover the richness of someone's personality? You have to really listen.

Listening isn't a passive process — you have to actively take in what you're hearing and make an effort to understand what people are telling you.

Let's take Pepsi as an example.

For years, Pepsi tried unsuccessfully to court Burger King, urging them to sell their product alongside Coca-Cola and arguing that customers wanted options in their drink choices. But Burger King kept declining their proposal, claiming that they _did_ offer a choice, as they had plenty of other types of soda on offer.

It was only then Pepsi started listening to Burger King. They saw what was going on below the surface, so they changed tactics. In their next pitch, they took a novel approach.

"Hey," they said, "we're both number twos." After all, Burger King had long since played second fiddle to their rivals McDonald's, just as Pepsi had to Coca-Cola. This convinced Burger King, and they switched to Pepsi in solidarity.

Listening is one thing, but making a good impression is also important. Let's look at that next.

### 3. To make a good impression, you should challenge preconceptions, and be both personal and personable. 

Say you're about to meet with a potential client. What's the best way to make a good impression?

One way is to play with preconceptions. If you keep things fresh with a bit of unexpected behavior, it can act in your favor.

Imagine, as a manager, you want to approach an aspiring golfer. The golfer will probably expect you to pursue them aggressively to get you on board. But if you do the opposite of what they expect, they'll be surprised and will be even more interested in what you have to offer.

The author himself once wanted his company to represent a famous television personality. However, he knew that she expected him to be aggressive in pushing for a deal.

So, he turned the tables and played with that preconception. In fact, he didn't even mention representing her at all; he just introduced himself and his business, and then said a few words about her career and her prospective opportunities. Perplexed and wrong-footed, she started pushing for a deal herself instead!

Another way to make a good impression is through personalized communication, and in many cases, any personal comment will do. It doesn't matter if it's a few words or a few written paragraphs — showing a bit of genuine interest is bound to grease the wheels. Use whatever personal information you have at your disposal to individualize your communication.

For instance, you could start an email by telling the recipient that you hoped her workload had gotten a bit lighter or that her latest business deal had been successfully signed off.

Business is about people and personalities. Make it work for you!

### 4. Use feelings of discomfort or rejection to fuel and motivate your efforts. 

Imagine you're trying to sell something. You really want a positive response and you're negotiating and persuading your way toward a deal.

It's sometimes at this very moment that you can feel something's off, that something's not right. Do you stop? Or do you press on?

You have to learn to trust these feelings of doubt. It's OK to feel uncomfortable and put the brakes on, especially when you know you're selling something valuable; it's a perfectly understandable response.

If you don't feel right, or if you sense an unpleasant tone in someone's voice, that's fine. Come back another day when the timing is better.

There are other feelings that you can trust, too. Just think of those uncomfortable sensations of rejection and failure. Some people let those feelings interfere when they're selling a product; instead, you can use them to boost your endeavors.

Everyone hates rejection and failure — there's no denying it. But when it comes to rejection in business, it's rarely personal. Far more often, it's about the product you're selling.

Even so, if you _do_ take it personally, it will make you redouble your efforts and dedication to making that sale. Rejection is a powerful emotion and if you harness it, you'll do an amazing job next time around.

It's just as true for failure. It hurts and is a feeling that loves to linger. But it, too, is a powerful motivator that will give you the incentive you need to keep pushing on.

> "_Selling is what they don't teach you at Harvard Business School."_

### 5. When it comes to selling something, seek out the right timing and remember what not to say. 

The best ideas need refinement and time. It's a generally accepted principle that nothing comes out perfectly the first time around.

There's a chance that an idea is not succeeding because it's just not very good, but timing can also be surprisingly important.

For instance, the author once tried to develop a pro golf tour in South America, but the project quickly fell through. The problems were all down to timing; unforeseen events, chief among them being skyrocketing inflation and severe currency devaluation, had derailed what had seemed a solid plan. The project had simply become far too expensive to get off the ground.

The author was certain it was a great idea and knew there was demand for it. Nonetheless, external circumstances had intervened. The South American pro golf tour could be a success — just not this time.

In business, you shouldn't trash an idea immediately. A bit of a headwind to begin with is to be expected, and a "no" from a buyer, bank or collaborator doesn't necessarily mean your idea is a bad one. Economic factors beyond your control might mean that a "no" at that precise time is necessary. But perhaps in a few weeks, months or years, circumstances might be a little more receptive.

Another highly underappreciated facet of selling something successfully is silence.

No, really.

Sometimes it's best to keep quiet, especially about the negative aspects of a sale.

There's no need to mention such downsides because doing so serves no purpose whatsoever, even if you think it might be relevant in the moment.

Imagine you're selling a portable radio. Even though the batteries might have to be replaced in 20 months, you're under no obligation to say so. The same is true if you know the product will soon be replaced by an updated model. So what? This will only make the potential buyer look at the negatives rather than, say, the radio's sleek design or great sound.

Accentuate the positives and you won't go wrong.

> _"Silence is what keeps you from saying more than you need to — and makes the other person want to say more than he wants to."_

### 6. Focus on quality and don’t grow your business faster than necessary. 

At business school, you're taught the dry stuff: how to balance a budget, what "supply and demand" means and how inflation impacts businesses.

But how do you put this into practice? How do you actually run a company?

What matters, first of all, is quality. Whether it's the quality of clients, employees or products, this has to be the focus from the start.

Consider the author. He founded IMG, an international management organization for sports figures and celebrities, in 1960. Arnold Palmer, now considered one of the greatest golfers ever, was his first client. At the time, however, Palmer had only won one championship.

The next two clients, Gary Player and Jack Nicklaus, were also golfers, and were also relative unknowns. Still, the author could see their positive qualities: they had class, character and were courageous and determined. These were traits he wanted to represent with his company.

After all, it's quality that will get you the best footing in the market.

Another tip for newcomers is not to rush things. It may seem counterintuitive, but there's no point in growing for the sake of growing. If you take things slowly, you'll give yourself the time to understand what makes a successful business. You'll be able to build a strong foundation, such as a good management team. Strength in these key foundational positions makes sustained success far more likely.

Let's look at IMG again. In the organization's first six years, it only represented golfers. They could easily have represented other sports figures as well, or even decided to expand in other directions, such as running sports events or managing fashion model agencies and celebrities (in fact, that's what they do now).

But at the time, it was a worthwhile step to get to know the business thoroughly before diversifying. After six years, the well-organized and respected IMG brand was ready to push further out.

So, having plenty of patience is a good way to start a business. But how do you keep things ticking along nicely once your business takes off?

### 7. Manage work and play with an organized yet realistic schedule, and don’t deviate from it. 

There's no denying it: business executives are busy people. They run around trying to do everything at once and never seem to have time for their hobbies, let alone time to relax.

But it doesn't have to be this way. If you manage your time wisely, you can still do non-work-related activities.

The trick here is to include these activities in your schedule. Whether it's meeting friends at your favorite restaurant or getting in a few rounds of golf, it can still be done.

Take the author. When he was busy working as head of IMG, he'd still find time to read, exercise or just decompress. If he had a commitment at 7:00 a.m., he'd get up at 5:00 a.m. and find time for a bit of quiet reading. Most others would likely have taken an extra hour in bed.

By the same token, while scheduling, be sure to allocate sufficient time for your activities; in other words, be realistic, not optimistic. Meetings, writing emails or visiting a client always take longer than you think, so don't be stingy. Allocate more time than you think you need, so that you have a buffer if anything goes astray.

And, of course, once you have your schedule, don't start changing it on the fly. Keep to it. The point of having a schedule is to follow it — otherwise, why have one in the first place?

If something comes up unexpectedly, don't just abandon whatever you're doing in that moment. Instead, check the schedule. Can you fit in this unforeseen task later on that day or later in the week?

And so, with this piece of time management advice, you can check off the last tip for surviving in the business world and find yourself better equipped to run _and_ maintain a business.

### 8. Final summary 

The key message in this book:

**Business schools do not and** ** _cannot_** **teach you everything about making it in the real world of business. In fact what matters more than numbers and figure sheets are personal interactions and self discipline. To be successful in business you have to recognise not only the foibles of others, but also your own. If you focus on quality and timeliness you are sure to make your path to success that much easier**

Actionable advice:

**To gain insights about someone, observe them!**

You don't have to go around giving people piercing gazes, you just have to notice others a bit more. What do people do that gives things away about their personality? Look at what they're wearing and how they're walking. Are they wearing a conservative suit? Are they walking confidently or with their eyes fixed on the ground? All these things can tell you a lot about a person and give you an idea of how best to approach them.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Start with Why_** **by Simon Sinek**

_Start with Why_ gets to the bottom of why certain people and businesses are far more innovative and successful than others — even in situations where everyone has access to the same technology, people and resources. The book shows you how to create a business that inspires customers and has satisfied employees.
---

### Mark H. McCormack

Mark H. McCormack (1930-2003) founded and for several years chaired the International Management Group (IMG), an international organization offering consulting, marketing and management services to prominent figures in sports, as well as other celebrities. McCormack was once an aspiring golfer himself, and later turned to the world of business. He was also a lawyer and writer, and penned several books, including _The Terrible Truth About Lawyers_.

