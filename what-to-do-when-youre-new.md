---
id: 56fa9aa0df81420007000023
slug: what-to-do-when-youre-new-en
published_date: 2016-03-30T00:00:00.000+00:00
author: Keith Rollag
title: What to Do When You're New
subtitle: How to be Comfortable, Confident, and Successful in New Situations
main_color: E7BC2E
text_color: 826A1A
---

# What to Do When You're New

_How to be Comfortable, Confident, and Successful in New Situations_

**Keith Rollag**

_What to Do When You're New_ (2015) is the result of Rollag's 20 years of research on why people become anxious and stressed in new situations. It provides strategies for changing your outlook on new situations and offers techniques for handling such situations with comfort and confidence.

---
### 1. What’s in it for me? Learn how to thrive when you’re new. 

It's happened to you before: you go to some event — a community meetup, a party, a conference: you name it — and find yourself stranded. You don't know any of the other attendees; your heart begins to pound, your hands go clammy and you retreat to some inconspicuous corner, pretending to be occupied with your phone.

How to break the ice? How to start a conversation? Or maybe you're baffled by yourself: Why do I act so awkwardly, you wonder — I'm not even shy!

Well, there are perfectly reasonable answers to such questions, and these blinks lay them out. You'll learn why people often feel uncomfortable in new situations and, perhaps more importantly, what you can do to be more at ease.

You'll also discover

  * that anxiety is part of evolution;

  * why having a script for new situations isn't the best approach; and

  * how to properly introduce yourself.

### 2. Our biological and cultural evolution predisposes us to be alert and anxious in new situations. 

We all have to be the newbie at some point: we change jobs, change residence, sign up for new classes and so on. But no matter how often we're the new person in the room, most of us feel anxious about it.

And for good reason.

Throughout our biological and cultural evolution, we came to naturally fear the unknown. In prehistoric times, we didn't meet many new people. According to evolutionary biologist Robin Dunbar, it was normal, back then, to interact with only around 150 other people, and to encounter no more than 300 or 400 others in a lifetime.

If we happened to meet strangers or enter unknown territory, it made more sense to be anxious and aggressive, as it was unclear whether one would be stolen from or even killed. There was no evolutionary incentive whatsoever to remain calm in new situations.

According to experts on child development, we've retained this fear of strangers; infants, for instance, begin to show anxiety around unfamiliar people at six months of age. This feeling is then reinforced by parents and teachers, who constantly warn their children against talking to strangers.

At the same time, however, we have a fear of being excluded from groups. For more social species, like humans, there are powerful incentives to be part of a group, as exclusion can be dangerous. For example, when male monkeys are kicked out of their birth group and into the world, half of them will either be killed or die of starvation before they are able to find a new group.

These days, belonging to a group isn't crucial to survival, but we still tend to long for inclusion. Modern culture fuels this by stating that only losers are incapable of finding a community.

So, we feel anxious in new situations and yet still want to fit in. How to solve this paradox?

### 3. Becoming comfortable in new situations requires a change in mindset. 

Think of the new situations you've been in recently. Did you feel comfortable? If, instead, you felt stressed and anxious — as so many of us do — you might be wondering why. Why haven't we learned to relax?

Blame a lack of mindful reflection. Without it, we don't improve over time. Once people reach a certain level of performance, they start paying less attention to how they perform. Take driving, for instance. Once we've learned the basic skills, we cease paying conscious attention.

Similarly, most people have developed a certain "script" for dealing with new situations, and they blindly follow it. For example, when meeting somebody new in the office, you might simply say your name, your position, how long you've been at the company and then ask about your conversation partner's position.

Although these scripts can prevent us from becoming overwhelmed, they can also make us feel anxious, as they prevent us from really thinking about how to handle new situations.

Therefore, if you want to get better at being new, you must adopt a different mindset. You can try, for example, various ways of seeing new situations as opportunities to learn. Here are three approaches you might try:

The first is the _coach mindset_, where you think of a new situation as training so you can get better with practice and feedback.

You could also adopt a g _amer mindset_ and think of new situations like a social video game, where you try to win by improving each time you're in an unfamiliar situation. Our approach to life is often too serious, so why not turn it into a fun challenge now and again?

Lastly, you may want to try the _beginner's mindset_ and walk into each new situation anticipating the excitement of learning something new. This also lets you accept any mistakes as those of a beginner, rather than as signs of personal failure.

### 4. As a newcomer, the first skill to master is to properly and successfully introduce yourself. 

Successful and effective communication begins with a proper introduction. In new situations, people are often reluctant to introduce themselves, preferring to wait for others to make the first move. Perhaps you're afraid of bothering others. But you should let that go. Think about it: have you ever gotten angry at somebody for disturbing you in order to introduce themselves? No!

Still, people worry that a weak introduction will create a poor first impression. This is an understandable fear; however, making a bad first impression is far better than not introducing yourself at all.

One way of overcoming such worries is to create an introduction strategy. Here's how it works:

At an appropriate time, offer a greeting. Give your name, say who you are and why you're introducing yourself. It's often wise to ask if now is a fine time to introduce yourself, thus ensuring that you're not disturbing something important. Then you could say, for example, that you're the new marketing assistant and you sit in the opposite room, so you'll probably be running into each other often. 

Remember to be brief, respectful and acknowledge the prospective relationship by saying why you should know each other.

Later, you can follow up your introduction with a chat, which will establish the basis of your relationship.

The goal here is to learn about each other, find out what you have in common and how you can connect in the future. If you get stuck and are unsure how to navigate the conversation, ask questions about the other person. This is effective as it shows respect and interest.

It's also a good idea to write down what you have learned about the other person. You may think you'll remember all the details, but it's far too easy to mix up people's names, roles or backgrounds. By writing down everything as soon as you can after the conversation, you'll be in a far better position to avoid awkwardness and anxiety in the future.

> _"The nice thing about being new is you have the implicit right, permission and justification to introduce yourself to just about anyone."_

### 5. Practice remembering names to show respect, to flatter others and to make a good impression. 

Remembering names is crucial to social interaction. So why is it such a struggle for us to recall them?

Blame it on evolution! Our prehistoric brain wasn't designed to remember names. In prehistoric times, we interacted with a very limited number of people. Only relatively recently did remembering many names become a necessity. Additionally, a person's name is stored in a different brain region than the other information we associate with that person. That's why it's easy for some people to remember faces, occupations or hobbies, but not names.

Furthermore, a psychological phenomenon called _context-dependent recall_ might also hinder name recollection. Context-dependent recall is when your memory for something or someone is tied only to the associated context or environment. For example, you may recall someone's name easily at the office, but if you meet that person during the weekend in a shopping mall, their name may suddenly escape you.

But don't panic. There are some useful name-recall strategies you can use to prevent such embarrassing situations. Here are a few of them:

Imagine that the person whose name you're trying to remember is standing next to a friend of yours with the same name. Say you're introduced to a guy named Peter; if Peter is also the name of one of your friends, then visualize your friend standing next to the new Peter. 

Another trick is to imagine the new person's name written across their face. 

Repeating the new name during the conversation is also a good method. You can ask how it's spelled or correctly pronounced, or say something like, "Hello, Brian, nice to meet you." And then repeat the name at the end of the conversation to help your brain process it.

### 6. Get comfortable faster by overcoming your fear of asking questions. 

To become a swiftly integrated newbie, you'll need to get information, help and advice from others. The problem is, this is precisely the time when you're most reluctant to ask questions.

Why is this? Well, when asking a question, we often experience an irrational fear. We tend to overestimate the possibility of being refused when asking for help, worrying that others are going to judge us or think us incompetent.

Fortunately, there are some simple rules you can follow when asking questions that can help you assuage these irrational fears.

People often appreciate questions if you approach them with an _I want to learn_ mindset — as opposed to a _solve my problem_ one. The first attitude gives people the impression that you want to do something yourself, but just don't yet know how; the second one basically says that you want someone else to do your job for you.

So, instead of asking someone to, for example, enter data into the system _for_ you, ask them to show you how to do it.

It also pays to be concise and avoid multi-part questions. Don't ask, in one breath, how a system works, who is responsible for it and what happens when you click here instead of there. Bundles of questions create confusion and only one part tends to get answered in the end.

Lastly, the best weapon you have when asking questions is saying that you're new. If you say this, people are more willing to help you out and less likely to consider your question (or you) stupid. They're also more likely to forgive you when you make mistakes.

### 7. Don’t let your fear get in the way; stay positive and focus on giving energy. 

Do you ever catch yourself thinking, "I really hope this person likes me," but doubting that they actually do? Well, you're not the only one!

Most of us are preoccupied with how others feel about us. Unfortunately, this also causes us to misinterpret social signals.

Fearing rejection, we become so sensitive to anything that might signal it that we tend to interpret all ambiguous or even neutral behavior as evidence of dislike.

Let's say it's your first week at work and you're worried that nobody will like you. On your second morning, one of your new coworkers hurries past you in the hallway without saying hello. It's easy to interpret this as their not liking you or not wanting to talk to you. In reality, however, they were probably just late to a meeting.

So, when trying to start a new relationship, always put yourself in the other person's shoes. If you were a long-time employee and a newbie approached you, would you be happy to talk to them? Most people would, so there is no reason to be worried.

Aside from worrying less about ambiguous social signals, it's important to be positive and focus on giving others energy when starting a new relationship.

Make sure that people feel energized by your interactions. People gain energy when they feel understood, when their ideas and opinions are validated and respected and when others show genuine interest in them. So listen to people! This encourages people to talk about themselves, and communicates that you appreciate and understand them.

Conversely, attempting to impress others with your own accomplishments tends to sap energy. This doesn't mean you shouldn't talk about yourself, but just keep it balanced by also talking about your conversation partner.

Finally, it's vital to stay positive. When you talk positively about others, people will automatically attribute those sentiments to your personality, too.

### 8. Learn how to rid yourself of the anxiety of performing in front of strangers. 

Feelings of unfamiliarity can breed a whole host of problems. You may be reluctant to contribute or share creative ideas at work; you might quit your new hobby after just two classes. This is what happens when we're afraid to perform in front of people we don't know.

But what is the reason behind this reluctance?

In one study, educational researcher Carol Dweck found that, at an early age, we develop a certain mindset called _talent is fixed_. We subconsciously believe that we're born with certain talents and when something doesn't immediately come easily to us, we conclude that we're bad at it. To make matters worse, parents and teachers strengthen this fixed mindset by labeling us as smart or good at particular things.

A healthier approach is the _getting-better_ mindset. This focuses on talent being developed through learning, effort and practice. To open up to this mindset you must understand that your first performance says very little about your talents, so there's no point in worrying about imperfections. All you need to do is focus on getting better by learning and asking for help.

Another benefit to this mindset is that people perceive it positively. It shows respect when you ask for help or feedback, and you show humility by acknowledging that you still have a lot to learn.

A getting-better mindset means you must maintain a realistic view of your mistakes. We tend to grossly overestimate how much others notice our screw-ups. The fact is, however, that other people are generally far more preoccupied with their own performances or thoughts than with what you're doing!

If they do notice your mistake, they're more likely to attribute it to inexperience than incompetency or a lack of talent, so don't beat yourself up about it!

Keep in mind that you're learning. Your first performance isn't an accurate representation of your talent, worthiness or abilities.

### 9. Final summary 

The key message in this book:

**Being anxious in new situations is natural. In order to overcome fear of the unfamiliar, you must change your mindset, put yourself into other people's shoes and practice some simple techniques for introducing yourself. Ask questions and remember names. And don't forget: we all make mistakes at first.**

Actionable advice:

**Experiment with energy exchange.**

To build healthy relationships, employ what scientist call _reciprocal altruism_. Reciprocal altruism is when other people help us and it sparks an instinctive urge to help them in return.

You can use this to your advantage by giving energy to others through listening to and appreciating them. When you do this, they'll be inclined to pay you back by being ready to listen you, appreciate you or help you out.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Charisma Myth_** **by Olivia Fox Cabane**

_The Charisma Myth_ defies the popular notion that charisma is inherited, arguing instead that everyone can cultivate their own charisma, and in doing so can have a more positive attitude, find more success, and handle obstacles more successfully.

Using wide-ranging examples of charismatic people, from state leaders to CEOs to employees, the book also outlines the different styles of charisma and how to practice demonstrating each, and offers the reader some useful tools and exercises with which to improve their psychological well-being. To find these blinks, press "I'm done" at the bottom of the screen.
---

### Keith Rollag

Keith Rollag is an Associate Professor at Babson College. He specializes in organizational behavior and newcomer socialization and training. His work has appeared in a number of publications, including _Harvard Management Update_, _Stanford Social Innovation Review_ and _Wired News_.

