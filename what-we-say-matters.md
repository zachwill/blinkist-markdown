---
id: 58b405fbafd1210004c15f64
slug: what-we-say-matters-en
published_date: 2017-02-28T00:00:00.000+00:00
author: Judith Hanson Lasater, Ike K. Lasater
title: What We Say Matters
subtitle: Practicing Nonviolent Communication
main_color: FEC5C8
text_color: CC4E54
---

# What We Say Matters

_Practicing Nonviolent Communication_

**Judith Hanson Lasater, Ike K. Lasater**

_What We Say Matters_ (2009) is a guide to communicating with compassion, openness and honesty. These blinks explain how to connect with your needs, as well as those of others, while speaking in a way that communicates your feelings clearly without causing suffering.

---
### 1. What’s in it for me? Learn how to engage in nonviolent communication. 

The words we speak establish our relationships with others and ourselves. Words can build trust and can express sincerity, appreciation and courtesy, but words can also sow the seeds of discord.

Sometimes, when we talk with our partners, family, colleagues or friends, we choose words that hurt other people's feelings. We might not intend to be mean, but it's the manner in which we express ourselves that causes resentment. So how can we communicate honestly without hurting other people's feelings? The answer can be found in _nonviolent communication_, or _NVC_.

In these blinks, you'll find out what nonviolent communication entails, how you can practice it and how it will positively affect your relationships.

You'll also learn,

  * how to tell your housemate to clean the dishes;

  * why making requests can be like offering gifts; and

  * how you can best talk to yourself.

### 2. Speaking truthfully and without harming others increases the well-being of everyone involved. 

Humans love to talk, and speaking is one of the fundamental aspects of our everyday lives. But while we do it just about all the time, this simple action still has profound effects on our well-being and the world around us.

The way we speak reflects how we view ourselves, as well as our opinions of others. For instance, if you say to yourself that you're a good person, you'll begin to act the way a "good" person acts and others will treat you as such. In turn, this will reinforce your sense of self-worth.

The way we speak also affects the way our discussions unfold. Just imagine coming home from work and finding that your housemate left the kitchen full of dirty dishes. In such a situation, you might feel frustrated and want to communicate it to your roommate.

But instead of calmly stating your feelings, you yell, "You're so messy, you don't give a damn about the house!" This reaction will likely just upset your housemate as well; instead of communicating clearly about the issue, you both end up angry and argumentative.

So, how can you approach such situations to maximize your well-being as well as that of the other person?

By using _right speech_, that is, language that's truthful but not harmful. This type of speech is recommended in Buddhist texts, namely the _Yoga Sutra_. It's described as respecting the practice of "satya" or "truth" and the practice of "ahimsa" or "non-harming."

Right speech is a way of speaking that strives to attend to all people's needs. To do so, it suggests that our words reflect the true state of ourselves and the world, while avoiding doing harm to others.

Unfortunately, that's easier said than done. To get started using right speech, you'll need another tool: _nonviolent communication_ or _NVC_. You'll learn all about it, including a better way to communicate your frustration to your housemate, in the following blinks.

### 3. Nonviolent communication allows you to speak in an honest, nonjudgmental way. 

You now know that right speech is a way to communicate truthfully without hurting others and that nonviolent communication is a tool to help you use it. The next step is to put this into practice.

NVC was first developed by the American psychologist Marshall Rosenberg in the 1960s. Rosenberg's intention was to help people resolve conflicts and differences in a peaceful manner.

The first two elements of nonviolent communication are _observations_ and _feelings_. An observation is simply a statement of facts, like "there are dishes on the kitchen counter." The crucial aspect of observations is that they're nonjudgmental. For instance, saying "the kitchen is messy" is a judgment rather than an observation, since the definition of "messy" is subjective.

Feelings, on the other hand, are signals sent by your subconscious that tell you whether your needs are being met. For example, when you feel sad and lonely, you're really perceiving ways that your needs are unfulfilled. You might also feel happy, hurt, irritated, content or all manner of other feelings.

By combining observations with feelings, you can make a nonviolent statement of truth like, "when I see dishes on the kitchen counter, I feel frustrated."

Once you've done that, the next step to nonviolent communication is to express _needs_ and _requests_. Needs are things like food, security or creative expression — in other words, aspects of life that we require to be happy and content. In this case, your need is cleanliness.

In fact, everything we do is to serve our needs, which is why honest communication relies heavily on describing these needs.

Finally, it's important to make a request. Keep in mind that, to be nonviolent, a request needs to be present and doable. For instance, you might ask, "would you be willing to listen to me now for five minutes?"

When you combine all of the above components, you can say something like, "when I see dishes on the kitchen counter, I feel frustrated because my need for cleanliness isn't met. Would you be willing to wash the dishes within the next 30 minutes?"

That's the basic template for nonviolent communication. Even so, there are still a few ways to refine this strategy, and you'll learn about these methods in the next blink.

### 4. Practice expressing yourself honestly, empathetically and openly. 

A major aspect of nonviolent communication is choosing the correct language. But speaking the right words isn't enough; to truly engage in nonviolent communication, you also need to connect to yourself, to others and to what you're saying.

To do so, it's essential to uncover your feelings, as well as your needs, and express both honestly. So, before speaking, give yourself some silent self-empathy. Take the time to determine exactly how you feel and which of your needs aren't being met.

For instance, when someone is insulting you, you might feel hurt and say to yourself, "I feel hurt because my need for respect isn't being met."

After that, you can express your feelings and needs honestly and explicitly. This is a crucial step because, by speaking honestly, you'll connect with yourself and your conversation partner; meanwhile, making your words explicit will allow the other person to hear your feelings and needs.

So, showing yourself empathy is important, but it's also essential to be empathetic toward the other person, which implies working to understand what they're feeling and what they need in the present moment. For example, when you find yourself falling prey to someone else's road rage, you might be able to empathize with them and envision that they're afraid because their need for safety isn't being met.

This is a fundamental step toward open communication; empathizing will help you connect with your conversation partner and make people more likely to engage with you.

The final key aspect of nonviolent communication is that it relies on making genuine requests, never demands. This means being open to the possibility that your request will not be accepted, but remembering that you can always try again with a slightly different one.

> "_The techniques of NVC are first and most importantly about inner awareness."_

### 5. Focusing on your needs and empathizing with others can help you overcome and avoid unpleasant feelings. 

Judgment and anger can often prevent people from communicating truthfully without harming others. But despite such fears of judgment, you can help yourself meet your needs, and the first step is to understand how this fear affects you.

Sometimes, a fear of judgment causes people to agree to things that don't meet their needs. For instance, you might eat a cookie that a friend offers you even though you're stuffed, just because you don't want to seem ungrateful.

The problem is that acting without meeting your needs will leave a negative residue. In fact, in the situation above, you might even end up feeling resentful of your cookie-giving friend.

So, how can you train yourself to make decisions that meet your needs?

One way is to write an index in which you score options based on how well they meet your needs. For example, if eating the cookie scores just a three out of ten, it's probably not worth eating it. By using such an index, you can begin acting more in step with your needs and less out of the fear of judgment.

And what about anger?

Well, to deal with this powerful emotion, empathy is the key. When people are angry, they tend to do things they later regret, like hurling insults at someone they love.

To avoid such unpleasant situations, approach anger by taking a second to figure out what you're really feeling. Chances are that you're actually feeling hurt, afraid, frustrated or some combination of all these emotions. By considering the full depth of your feelings, you'll be more likely to find a solution to whatever made you angry in the first place.

Another benefit of this approach is that empathy for others can help you avoid angry feelings altogether. For instance, by empathizing with another person who is interrupting you, you might realize that they just feel eager to be heard. When you consider it this way, the interruption doesn't need to frustrate you.

### 6. NVC can transform our relationships with our loved ones. 

Certain people, like our partners and children, occupy a particularly special place in our hearts — but communicating with the ones we love can sometimes be the most difficult dynamic of all. So, how can we use nonviolent communication to connect with those closest to us?

For starters, we can consider sharing our feelings and needs as a form of gift-giving. This change in perspective can be powerful in a society encumbered by misconceptions that deter us from honestly communicating with our partners and other loved ones.

For instance, people still commonly believe that women should sacrifice their needs to serve their spouses and that it's inappropriate for men to have feelings. Such misconceptions are obviously no basis for right speech, and thinking about sharing our feelings as gifts is a great way to move past them.

To put this into practice, we need only think of making requests of our partners as giving them the opportunity to meet our needs. We can think of this opportunity as a present we bestow upon them, rather than a burden they have to bear.

When it comes to kids, we can encourage them to work with us to meet their needs and our own. It helps to remember that children, like all people, crave autonomy. By constantly making demands of them, like telling them to do the dishes, we can make them feel that their need for autonomy isn't being met, which often leads to resentment.

On the other hand, if we express our feelings and need for cleanliness to our children and ask them to help us meet this need, they'll do the dishes and for a good reason: to meet our needs, and not just because we said so.

And finally, with our parents, we can use empathy to improve communication. Since speaking with our parents can sometimes feel irritating, maintaining empathy is vital. These kinds of conversations can force people to defend their need for respect and autonomy, causing them to suffer in the process. But whatever the topic of discussion, if we approach it with empathy, everyone will feel more connected and open.

> _"All criticism is the tragic expression of unmet needs."_

### 7. You can use nonviolent communication in the workplace, with yourself and to improve the world. 

The great thing about nonviolent communication is that you can apply it to just about anything. You can even use it to improve your work environment, although it can be difficult to do so.

After all, you might worry that being honest about your feelings in the office could lead to a loss of status or respect from your coworkers. While this fear is common, nonviolent communication at work is not only possible, it can be hugely beneficial.

For instance, imagine you're at a meeting and everyone is trying to add their two cents. Instead of forcing yourself into the already chaotic conversation, you could respond to another employee with empathy. To do so, you might say, "I'd like to explain how I understand what I just heard. Would you be willing to listen?"

By phrasing your request in this way you'll connect with the other person and make him more likely to hear you.

So, you can use nonviolent communication with your co-workers, but you can also use it to speak to yourself. In fact, nonviolent communication is a great tool to move forward from mistakes by applying self-empathy.

For instance, after making an error, you might be compelled to tell yourself what an idiot you are. But there's a more productive way to deal with mistakes: greet them through empathy for yourself.

First, silently practice self-empathy to identify your feelings and needs. Then, if there's someone else involved, share them with that person. By showing yourself empathy, you'll be able to acknowledge that you're human and that it's normal for you to make mistakes. This knowledge will, in turn, make it easier to forgive yourself.

Finally, you can even employ nonviolent communication to change the world. There are loads of ways to positively contribute to the world, and it's impossible to say that one way or another is the best. But regardless of the path you choose, your actions and communication will be much more effective if you come at them from a place of self-awareness and self-empathy.

### 8. Final summary 

The key message in this book:

**Communicating with others can be a trying and sometimes hostile experience. But by honestly expressing our feelings and needs while remaining open and avoiding doing harm to others, we can speak the truth without hurting ourselves or others.**

Actionable advice:

**Practice self-empathy in group situations.**

The next time you're in a big, chaotic group environment, such as a conference, try practicing silent self-empathy. Simply write down how you feel and which of your needs are not being met. Even if you don't speak up about these unfulfilled needs, this practice of silent self-empathy will create a shift in you — and in the group as a whole.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Nonviolent Communication_** **by Marshall B. Rosenberg, PhD**

These blinks introduce the principles of Nonviolent Communication (NVC) as a compassionate way of being with ourselves and others. Through simple techniques, you can learn how to consciously change your language and thinking to forge better quality relationships with others.
---

### Judith Hanson Lasater, Ike K. Lasater

Judith Hanson Lasater has been a yoga teacher since 1971 and is also the president of the California Yoga Teachers Association. She has a doctorate in East-West psychology.

Ike K. Lasater is one of the cofounders of Mediate Your Life, a company that teaches people how to handle conflict. He was previously a trial attorney and has served on the mediation panel for the United States District Court for the Northern District of California.

