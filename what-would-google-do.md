---
id: 52e8d6e2363532000c170000
slug: what-would-google-do-en
published_date: 2014-01-29T09:00:00.000+00:00
author: Jeff Jarvis
title: What Would Google Do?
subtitle: None
main_color: 92B2C9
text_color: 3A7FB0
---

# What Would Google Do?

_None_

**Jeff Jarvis**

The age of the Internet has dawned, but very few companies seem to understand how profoundly it has changed the business landscape and what they must do to thrive. The most obvious exception? Google. _What Would Google Do?_ endeavors to explain what strategic choices fuel the success of Google and other web 2.0 companies like Amazon.

---
### 1. The rise of the Internet has made good, fast customer service a necessity. 

Most business owners know that the worst mistake they can make is to ignore their customers. The consequences of doing so can be dire, as was demonstrated by the author in 2005, when he purchased a Dell laptop. Disappointed with the customer service he got in response to a support request, the author posted a rant on his blog about his experience. Soon, many other dissatisfied customers started chiming in, and the blog post became highly prominent in Google searches for Dell. Steadily, Dell's situation worsened. This kind of scenario is exactly where angry customers can do a lot of damage to a brand.

Dell believed its customers should provide feedback only via Dell's site and on Dell's terms, but it turned out that customers were already talking among themselves elsewhere on their own terms.

The author later sent an open letter of advice to Dell, telling them that losing a single customer today can have large knock-on effects, as that customer will likely go on to tell all of his or her friends about the poor experience as well. And in today's connected world, a single customer can have a lot of friends.

This means companies no longer have any place to hide; every mistake they make is made public instantly via the Internet. Leave a customer on hold for too long? Expect to read public, angry rants about it online very soon.

So if you are running a company, what can you do?

The main thing is to ensure you provide excellent and fast customer service, as this can prevent and repair any damage. Talk to unhappy customers and try to understand and fix their problems. Remember, the fact that a customer bought your product indicates he _wants_ to like it. If you solve the problem, he may well spread the word of this good service.

Also, read blogs where your customers talk about your products. This way you'll stay tuned in to what your customers think of your product without having to spend a fortune on consumer research. Next, you'll find out about even more ways to involve your customers in your business.

### 2. Involve your customers in making and marketing your products. 

Ask yourself, who cares most about a company's products? Why, the customers of course. This is why companies are increasingly trying involve their customers in the creation of their products.

Companies used to believe that to have a competitive advantage, they should keep their plans totally secret. However, today it is openness and interaction with customers that creates a competitive advantage. For example, by blogging openly about their plans, a company can get valuable feedback and new ideas from their customers.

The founder of Dell, Michael Dell, saw this opportunity when he said, "I'm sure there's a lot of things that I can't even imagine but our customers can imagine." Indeed, in 2007 Dell launched IdeaStorm, a website where customers can contribute their ideas to Dell. An example of an idea that came to fruition through this site is that Dell now sells Linux computers.

Similarly, almost every new service that Google offers is initially in beta form, meaning it is an experimental product that customers can test and provide feedback on, thereby helping Google to improve it.

Another area where companies can benefit from interacting with customers is in marketing: happy customers willingly advertise and recommend products they like to friends. For proof, look no further than Google; it became the fastest growing company in history primarily through word of mouth, not through marketing.

As you'll find out next, word of mouth also means you'll have to stay totally honest with your customers.

### 3. To succeed today, companies must communicate honestly and admit mistakes openly. 

Not long ago, companies used to be obsessed with secrecy regarding their products and especially regarding their mistakes.

Today, neither companies nor anyone else in the public eye can afford to lie.

Consider an example from the media world. In 2004, the journalist Dan Rather presented military documents critical of President George W. Bush's military service. Bloggers suspected these documents were fake, and eventually one blogger proved it. Instead of immediately admitting the mistake, Rather chose to ignore the comment storm that arose for eleven days, and then dismissed his critics as being politically motivated. This fuelled even more criticism, as today's post-media generation has been raised on honesty and directness, and therefore expects others to tell the truth as well.

So how should companies behave in this new open and transparent world? They should be honest.

If you run a company, admit your mistakes openly, as this will increase your credibility. For example, bloggers informed Reuters in 2006 that one of its photographers had doctored a photo of Beirut during Israeli bombardments. In response, Reuters removed the photo, fired the photographer and thanked the bloggers. This is the correct approach to dealing with mistakes.

Of course, the other side of the coin is to ensure you do only those things that you would be willing to admit publicly. As Google's motto says, "Don't be evil." For Google, this means they deliver unbiased search results and clearly indicate when they are advertising something.

In today's transparent world, whatever benefits you may get from doing something "evil" will be outweighed by the the costs when word spreads. Remember, everything you say and do can be easily found on Google. In fact, you could consider Google to be the angel on your shoulder, reminding you of how important it is to stay honest.

Next, find out how you can become more prominent on Google.

### 4. Make your site informative and Google-friendly to help many more people find it. 

The lifeblood of any website is the traffic it gets, and the most important channel to get visitors from is Google. People ask questions on Google, and from there find their way to the websites that answer them.

So if you run a website, how can you generate more traffic?

First of all, your site should be very informative; this way it will attract people with questions. In fact, you should try to have answers for every relevant question you can imagine.

For example, people often look for things like

  * product details and support information on manufacturers' sites

  * opinions and voting records on politician's sites

  * nutritional info on food companies' sites

To learn more about which answers you should provide on your site, look at the searches that lead people to your website. These will tell you what people are looking for.

Consider the example of About.com, a content service featuring over a million helpful, focused and usually timeless articles about niche topics from car repair to thyroid disease. The service comprises 700 sites maintained by independent writers who are taught how to craft headlines, page titles and keywords so Google will find them more easily. Writers also monitor which queries visitors use so they can answer new questions preemptively.

Another way to get more traffic is to make your site Google-friendly by improving your PageRank — otherwise known as drinking "Googlejuice." It's like a magic, virtuous-cycle-inducing elixir that sends lots of traffic to you, which increases the number of clicks, links and mentions you get, thereby pushing your PageRank even higher.

So how can you make your site more Google-friendly? Make the presentation of information on your site simple and clear for both your customers and for Google's computers, so the message cannot be obscured or misunderstood. And forget about fancy gimmicks like animations and sounds; they annoy most people, and Google won't recognize most of it anyway.

Next, you'll find out how the Internet has completely changed the face of how media functions.

### 5. Links between web pages have changed the structure of media: they drive specialization, quality and cooperation. 

The Internet has brought with it some profound changes to the media landscape. The fact that the pages of different websites can be linked to each other has changed the way media functions; it is now more collaborative, and information flows in two directions.

For example, consider the author's experiences on 9/11: He was close to the World Trade Center when the first plane hit, and could talk to survivors on site. He later used his notes to start a blog about the experience. Bloggers in Los Angeles read his work, wrote about it on their own blogs and linked to him. He in turn responded and linked back to them.

This is the new structure of the media: everyone can speak their mind, and everyone can listen and chime in. These kinds of distributed conversations that happen in different places at different times have been made possible by the linking of pages and information online.

The ability to link content online is also driving specialization. For example, in the age of print media journalists had to include background paragraphs in their stories to fill the reader in on what had happened so far, but today they can simply provide links to earlier articles. Also, they no longer need to have specialist reporters on niche topics like golf, as they can simply link to other sites for better and cheaper coverage. These factors allow newspapers to specialize and focus on what they're good at.

This specialization drives both more collaboration and higher quality, as people can focus on what they do well and let others "fill in the blanks" with what they do well.

Companies should take note of this: _focus on what you do best, and link to the rest_. For example, for customers seeking product info, retailers should link to the manufacturer, and manufacturers should link to forums and blogs where other customers are already discussing their products.

Find out what types of businesses are most successful online in the next blink.

### 6. The way to build a business in the Internet age is to join a network or provide a platform for others to build networks on. 

The rise of the Internet has allowed people and companies to collaborate and form _networks_ in an unprecedented way. Savvy companies and individuals can take advantage of this.

Founding such a network can be very profitable, because each member will contribute to the network and add value to it. Similarly, joining such a network is beneficial, since every member can benefit from this kind of collaboration. The bigger the network, the more valuable it tends to be.

An example of such a network is Glam, comprising 600 independent sites covering women's fashion, health, celebrities and so forth. It has become an extremely popular network of curated content and advertising, with over 81 million users worldwide.

Sites that join Glam get prestige because the network is so selective. This translates into added traffic, because sites within the Glam network send users to other sites within the network, thus multiplying the effect. Glam then profits by selling advertisements on these sites, for which they can charge a premium because advertisers know that their ads will appear in high-quality environments.

Another successful way to build an online business today is to provide a _platform_ for others to build on. A successful platform allows users to create products, businesses, communities and networks of their own on top of that platform.

A good example is Google Maps. After launching, the service was so handy to users that an old-school mindset would have dictated that Google keep it on its own site and maintain control of it. Instead, Google allowed other sites to embed Google Maps on their pages and even to create apps based on it. For example, one coder developed a new app that combined Craigslist ads and Google Maps. Google didn't sue him for using and altering their product in an unauthorized manner; they hired him.

Because businesses can use the platform so freely, they have become very invested in it, making it difficult for Microsoft or Yahoo to lure them away. Google Maps has become the standard in map services and local information.

Next, find out why niche markets are on the rise.

### 7. The age of big companies selling to the mass market is over; small specialized companies are on the rise. 

It used to be thought that the pinnacle of success for any company was to sell their product to the mass market: everyone, everywhere.

But today, selling to the mass market is no longer an option.The abundance of different and customizable products on the Internet allows everyone to find the products they want instead of settling for generic ones. There is no mass market anymore; there is just an infinite number of small market niches. Therefore, you should focus your resources on serving your niche well rather than serving the masses poorly.

One consequence of this is that small companies are usurping the formerly dominant position of big companies.

Certainly, big companies are still alive and well: Wal-Mart is the largest company on Earth, media companies are forming huge conglomerates, and even Google itself is a behemoth among other companies. But small companies are on the rise.

Consider, for example, that in 2007, small stores sold $60 billion worth of goods through eBay. While this is still peanuts compared to Wal-Mart's $345 billion turnover, it does beat Federated, America's largest department store chain, which sold only $26 billion worth of goods.

Another example is the rise of blogs. In 2006, blogs as a whole already had a total 57 million readers, whereas only 50 million people were buying daily newspapers two years later.

The reason behind the rise of small businesses is that today it is easier for a small business to succeed than it was for a large one before. You no longer need a store, an inventory, staff and advertising to be in retail; you can simply find customers on eBay, Amazon and Google. Without these costs, profits accrue quickly.

Next, did you know that many companies are in the _knowledge business_, even if they don't seem to be?

### 8. Successful companies like Amazon and Google focus on the knowledge business: how what they know can benefit their customers. 

As companies learn more and more about their customers, some savvy companies like Amazon and Google are putting that knowledge to good use. At the same time, they are minimizing the costly and complicated handling of physical products and materials.

For example, did you know that Amazon is not in the business of selling goods; it's actually in the knowledge business?

In fact, Amazon's strategy is to deliberately deal with as few physical goods as possible. They don't own any stores or employ any sales clerks, and keep their inventory to a minimum by obtaining more merchandise as it is ordered by customers. They have no shipping infrastructure of their own, preferring to use external services where they get great rates thanks to their enormous volumes. All this creates savings, which they pass on to customers to drive even more volume.

So how is Amazon in the knowledge business?

They have an excellent database of what their customers buy, when they buy it and what other items they buy with it. This allows Amazon to predict what a customer might need next. Also, they are able to use millions of reviews written by customers to further fine-tune their offerings.

Though Google provides a broad array of services, it too is essentially in the knowledge business. It provides not only a search engine but also email, document management and map services, along with others; and yet this is not where its profits come from. They come from targeted advertising, which is made profitable because Google knows more about us than any other organization. It has achieved this by offering many services for free and then collecting data on the users of those services.

Every company should think carefully about what the true value is that they provide to their customers. It is probably not the physical products you sell, but rather what you know, how you serve or how you anticipate the needs of customers.

As you'll find out next, this could mean you can even provide your main product for free.

### 9. Today, many successful businesses and networks operate by providing their main services for free. 

Just a few years ago, the idea of a business being profitable while providing its services for free seemed impossible. But today, this is a common phenomenon.

This is because it is entirely possible to provide the actual product or service for free while making money through advertising.

A good example of this is the _New York Times_. In 2005, the newspaper erected a paywall on its website, asking readers to pay $50 a year to read columns and access archives.

The result?

Profits were never openly reported, but most likely they were dismal. In 2007, the change was reversed, and immediately more visitors flocked to the site, thereby bringing in more links and clicks and improving the site's Google ranking. The paper could then make more money from advertising to this expanded audience.

Free and low-cost services are also proliferating because they are vital for networks to thrive. This is why the fastest-growing network-based companies like Skype, eBay, Facebook, Amazon and Google charge as little as possible. They know that no-one can compete against a great free service.

Craigslist is also a great example of how offering a free service can grow a network. They only charge for posting job listings, while all other listings are free. This has made it the go-to marketplace for most listings, and no competitor can undercut it since the service is already free.

### 10. Final summary 

The key message in this book:

**The rise of the Internet has profoundly changed the way business is conducted. Companies must embrace the new transparency this brings and start taking advantage of it by collaborating with their customers as well with each other. Small, nimble companies are on the rise to cater to the new, highly-fragmented market of niches.**

Actionable advice from the book:

**Consider making your company's main product or service free**

Most companies still labor under the assumption that they must sell something to survive. Challenge this assumption. What if you had to give away your main product for free? What would that mean for your customers? Where could you then make money?

**Capture the wisdom of the crowd**

Today, people online are openly discussing their needs, desires and experiences about products and services. This is a valuable source of information that every company should tap into. Start reading what the crowd relevant to your business is saying. Ask them about your products, or get them to test the products. This is far better than using focus groups, because the people who are already discussing your products clearly have something to say.
---

### Jeff Jarvis

Jeff Jarvis is an American journalist, academic and advocate for the Open Web. He has written for the _San Francisco Examiner_ and the _Guardian_, and currently heads the new media program of City University of New York's Graduate School of Journalism.

