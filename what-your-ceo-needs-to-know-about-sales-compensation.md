---
id: 55478b0b3666320007410100
slug: what-your-ceo-needs-to-know-about-sales-compensation-en
published_date: 2015-05-04T00:00:00.000+00:00
author: Mark Donnolo
title: What Your CEO Needs to Know About Sales Compensation
subtitle: Connecting the Corner Office to the Front Line
main_color: 81895B
text_color: 647028
---

# What Your CEO Needs to Know About Sales Compensation

_Connecting the Corner Office to the Front Line_

**Mark Donnolo**

These blinks explain how compensation can be optimized to motivate sales representatives and ensure the growth of the company. It offers advice on finding the right people, crafting a strong sales compensation plan that drives your team to hit their targets, and implementing that plan effectively.

---
### 1. What’s in it for me? Discover how refining sales compensation can help companies grow. 

Cary Grant once said you should do your job and demand compensation — but always in that order. Effort, reward. Effort, reward. And the greater the effort, the greater the compensation. Simple.

For companies that wish to give their salesforce a boost, however, it's not quite so straightforward.

These blinks will show you there's more to sales compensation than simply rewarding results with money. Your sales compensation plan should be finely calibrated to suit different types of selling and sellers and always align with the company's overall strategy.

In these blinks you'll learn

  * what you should think about when you're selling a copying machine;

  * why you should always start by defining your success metrics; and

  * why new sales compensation plans must be introduced slowly.

### 2. The sales compensation plan connects the sales representatives to the company’s larger strategy. 

Sales compensation seems simple, right? If a sales representative makes a sale, they get compensated. But it's actually a lot more complex, and if a company implements a successful sales compensation strategy, it can be extremely profitable.

It's important to recognize that sales compensation is _one part_ of the company's strategy. It needs to align with the rest of it.

Every company needs a solid plan for becoming profitable. These plans are always very complicated, however. They require CEOs and management teams to make difficult decisions in several areas, like sales strategy, customer coverage and enablement.

The sales compensation strategy comes at the enablement level, so the CEO has to make a number of strategic decisions before they can focus on it.

It's very important that these decisions all fit perfectly with each other. If they don't, the plan will contradict itself and be ineffective.

Imagine a company that produces copying machines, for instance. The upper management knows their customers are primarily large corporations, so they develop a sales strategy based around sophisticated technology that's suited to them.

However, the company will suffer if the team doesn't allow for the fact that they'll need a lot of time to correspond with customer companies. If their products are expensive, their customers need more time to decide if they're a worthy investment.

Sales reps don't aim to implement any specific company strategy; they're motivated by their compensation rates. So your _sales compensation plan_ has to skillfully connect their work to the company's strategy at large.

Imagine a sales rep working for a copying company that just released a new product. Even if the new product's digital technology requires a longer, more sophisticated sales process, they'll probably use the same tactics they used to sell the analogue one. The key to solve this problem is to craft a sales compensation plan that motivates them to do what's best for the company, not for them.

> _"Where the business evolves, sales evolves as well, and sales compensation must evolve to support that change."_

### 3. There are three different ways to sell, and they all require different skills. 

Fostering an effective sales compensation plan isn't just about throwing money at your sales reps. It's certainly important to reward good sellers, but first you have to determine what kind of selling you're doing and make sure you have the right sellers for the job.

You can use three kinds of selling:

First, there's _retention selling_ — retaining the revenue you get from your current customers. In retention selling, you aim to keep a steady flow of revenue by selling the same products to the same customers again and again.

Next, there's _penetration selling_. Penetration selling means selling new products to customers you already have, or old products to new customers.

Finally, we have _new customer selling_. New customer selling means either stealing your competitors' customers, or selling newly developed products to new customers.

Naturally, these different sales roles require different skills. You need to find people suited to retention, penetration and new customer selling.

Reps who work in retention selling, for instance, need to intimately know their customers' needs. It's a good job for sellers who like hard work and dedication to long-term goals.

Penetration selling also requires a strong relationship with the customer, but these sellers also need to develop that relationship so they can get more out of it. This is a job for people who have a strong drive to hit their numbers.

Independent and more aggressive sellers are best suited to new customer selling. They have to be ready to take risks and push for their goals, so it's a good job for people who are confident, outgoing and strong willed.

### 4. Compensation plans must match each sales role. 

As you might've guessed, the three kinds of selling are different in other ways too. They also necessitate different kinds of compensation rates to incentivize the sellers into behaving the right way.

A sales rep's income isn't fixed. Instead, it's determined by their _target total compensation_ : the sum of their base salary and their target incentive. The ratio between these two figures is called a _pay mix_.

So if the pay mix is 50/50 and the target total compensation is $100,000, the sales rep gets $50,000 at the end of the year and another $50,000 if they meet their quota.

For sales roles that require more aggressive behavior, you'll want a bigger portion of the target incentive in the pay mix. That'll help sustain the aggressive sales approach, as the sellers know they'll have a higher income if they make more sales.

Sales reps that need to build a good relationship with their customers should have less target incentive, however. They shouldn't be pushed into taking unnecessary risks or they might scare off their customers.

You also need to reward your most successful sellers, of course. What if a rep exceeds their company's expectations and goes beyond their quota? A smart company motivates their sellers by offering them upside potential. If you don't, your overachievers might lose their drive or look for a new employer who values their skills more than you do.

Naturally, you should avoid overpaying sales reps that lag behind. After all, it's only fair that the sellers who contribute the most to the company earn a larger share of the pie.

> _"When it's working correctly, the compensation plan is a clear reflection of the sales role."_

### 5. Measure your sales reps’ performance in ways that are meaningful to them. 

So, reps need to be incentivized to perform well, but what does that actually mean? It can be quite difficult to define what "good performance" really is.

First, you have to define your success metrics. Don't forget that your sales strategy needs to align with the company's overall strategy.

For example, imagine you're a brewer and your strategy is to put as many beer cans as possible directly into the hands of the end consumers. If that's the goal, it wouldn't be good for your sales rep to just sell a large number of cans to a retailer. It'd be much better for them to ensure those cans end up in the right spot in the store, facing the customers and in a better position than your competitors' goods.

Performance measures like this have three dimensions. The actual measure, its level and frequency. For example, if the measure is to sell 1,000 beer cans, that can be measured every week, month or year. Performance can also be measured on a division or team level.

These extra dimensions of measurement can have a big impact on the sales reps' behavior. The reps might feel frustrated or helpless if they're not assessed on an individual level because they might perform better than the rest of their team but not be rewarded for it. On the other hand, measuring the team as a whole could strengthen them and bring them together.

The measure also needs to be taken frequently so it stays closely connected to the behavior it rewards. It would be unwise to measure the annual volumes sold if your reps operate on weekly or monthly sales, for instance.

### 6. Setting good quotas is challenging, but vital. 

So "good performance" has a number of definitions, depending on the situation. But even if you figure out the perfect measurement system, a big question remains: when is _good_ good enough?

This is what you have to ask when you determine your reps' _quota –_ the amount of sales they're required to make as part of their compensation plan. Quota setting is very important, but it can also be quite demotivating.

We've seen how sellers who have higher quotas should have larger target incentives. As companies strive to grow, the sales reps' quotas should grow as well. Raising the quotas as the company expands usually works well, as it ensures that reps are continuously striving for their best rather than resting on their previous successes.

You have to be careful, however. A quota that's too high or too low can be devastating. It can cause talented reps to go underpaid or reward lazier reps by overpaying them. A badly set quota could destroy the team.

So you need a quota that'll help you get the best out of every rep. You don't want to crush strong reps by asking too much of them, but you also don't want to make it too easy. You need to take your quota-setting process very seriously.

If your business has serious growth potential and reps that are similarly talented, a flat quota where every rep gets the number would be beneficial. For example, if you're just starting out and none of your reps have an existing customer base, they could all potentially grab 100 percent of the market, so you can assign them all the same quota.

If your reps have different market and customer opportunities, it might be strategically smart to give them separate quotas.

  * _Market opportunity-driven_ and _account opportunity-driven_ quotas are based respectively on the market and customer opportunities that are available.

  * What's great about them is that they make you consider all the growth possibilities you have and not just project your growth onto your reps' previous performance.

> _"Quota problems are consistently at or near the top of the list of sales compensation challenges for most companies."_

### 7. Have faith in your sales compensation team. 

Sales compensation plans are clearly complicated. So who exactly should design them? The CEO? The sales compensation team?

A good CEO knows how to delegate her power. Too much CEO-level involvement in the design and implementation of the sales compensation plan can be unhealthy. Usually the sales compensation team calls most of the shots themselves.

Some CEOs will have a hard time with this. It might be difficult for them to let their sales compensation team develop the strategy on their own. They might want to control the process and know every little detail of it.

This is counterproductive, however. It takes the CEO's attention away from areas that _do_ require more of their involvement. It can also undermine and demotivate the compensation team, leaving them feeling that the strategy is ineffective and they're powerless to change it.

Instead, it's best if the CEO focuses on providing overall direction and meaningful evaluations as the plan progresses.

The sales compensation plan still needs some level of CEO involvement, of course. This is true in all aspects of the company.

The CEO might not know the specifics or mathematical details of the plan, but in the very least they need to provide input when it's first being designed. They need to make sure it's aligned with the company's larger goals, and that the team understands those goals.

As always, the CEO also needs to evaluate the plan as it evolves. They have to provide effective feedback so the compensation team knows they're going in the right direction.

So after you've got a great sales compensation plan in place, you'll need to start working with your managers and sellers. How?

### 8. Sales managers are essential, so recruit good ones by offering them new opportunities. 

How do you go about implementing your sales compensation plan? You'll need to have some good sales managers in place. They're an absolutely essential part of a strong sales team.

Good sales managers need to be highly skilled. The team can't thrive and enjoy success without them. They guide the reps and make sure everyone hits their quota. More importantly, they serve as a link between the executive level and the sales reps working on the front line. Sales managers also have to be on top of any changes that occur, so they can translate effectively between the departments.

Good sales managers are hard to find, however. It's very common for talented sales reps to avoid becoming managers even if they get the chance, because they know they'll earn more by making sales.

Despite this, most managers are still former top-performing sellers. They often find it hard to pass on their knowledge, however. They may be tempted to step in and take over anytime things get hard.

You can recruit strong managers by offering them new kinds of opportunities and benefits. A company almost certainly can't afford to pay their managers what they'd earn in the field, so they need to find other ways to make the job attractive to them.

Think about the profile of a good sales manager. She's a hard-working overachiever who's looking for recognition and new challenges. So rewards instead of cash could be frequent flyer miles to reward her for her success, or long-term incentives like stock in the company. Non-financial rewards could include attractive career paths and new opportunities, maybe by offering her a reward for reaching a strategic goal for the company, or starting a new activity that benefits the team.

### 9. Take care to implement the new compensation plan effectively. 

So now you have a strong compensation plan and a good manager to back it up. What's next?

Well, even the best plan is useless if your sales reps don't understand it. Naturally, the reps may feel uneasy with it at first. People are scared of change, especially when it affects the way they're compensated for their work.

If they're nervous about the plan, it might just be that they're confused by it. Remember: just because the CEO and sales compensation team understand the plan, doesn't mean the sales reps do. Their nervousness is normal. After all, how can you expect people to work well when they're not sure if they'll be paid as usual?

Anytime you make changes to the compensation plan, you need to implement them effectively. Make sure you always explain to the reps why the changes are necessary, and what exactly is changing.

Also, be very clear about the specifics. Who will be affected by the changes? How? When exactly are the changes taking place? Reps need to know the answers to these questions.

They also need to know if changes promise them a better future, and what dangers lie ahead if no change is made. Let them see that your plan can be beneficial to them, and the company isn't just trying to make more money at their expense.

Before you talk to your sales reps, try to predict how they're going to react. Strive to avoid negativity. There will always be some people who resist change. Some people might not engage fully with the plan, while others might behave in more aggressive ways. Identify the people you think will resist the most, and take special care to convince them that the change is actually for the best.

> _"For compensation to be a good communications tool, reps need clear direction on what the company wants them to do."_

### 10. Final summary 

The key message in this book:

**Sales representatives don't consciously implement the company's strategy — they're motivated by their compensation rates. So design a compensation plan that incentivizes them to help the company by putting sellers in the right roles, setting appropriate quotas and rewarding them accordingly. They'll ensure the company's success — and help themselves in the process.**

Actionable advice:

**Know when to stay out of it.**

The sales compensation strategy will almost certainly be more effective if the CEO minimizes her involvement and gives her compensation team the freedom to implement the plan themselves. So even if it's tempting to take over — don't. You'll be glad you didn't.

**Suggested further reading:** ** _The Challenger Sale_** **by Matthew Dixon and Brent Adamson**

Sales strategies have changed. Instead of shilling "one-size-fits-all" products, today's top sales reps excel by providing a customized solution to a unique problem. To do this, these sales reps follow the "challenger" selling model. In this book you'll learn what this model is and how it can revolutionize your sales organization.
---

### Mark Donnolo

Mark Donnolo is a consultant and a managing partner at SalesGlobe, where he consults on executive professional services. He's previously worked for Siegel & Gale/Saatchi & Saatchi and CoalTek, among others. He has an MBA from the University of North Carolina at Chapel Hill.

