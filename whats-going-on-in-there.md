---
id: 58863b545806db0004d7ff4b
slug: whats-going-on-in-there-en
published_date: 2017-01-25T00:00:00.000+00:00
author: Lise Eliot
title: What's Going on in There?
subtitle: How the Brain and Mind Develop in the First Five Years of Life
main_color: E3C1B8
text_color: B05E48
---

# What's Going on in There?

_How the Brain and Mind Develop in the First Five Years of Life_

**Lise Eliot**

_What's Going on in There?_ (1999) delves into the cognitive and physiological development of young children. These blinks explain the most important milestones of a child's development, exploring the shared influence of genes and parenting on children.

---
### 1. What’s in it for me? Delve into the minds of children. 

When a baby is born, it's a sweet little being, sleeping and dreaming through most of the day. Alas, none of us remember what the world looked like to us in our first years. Wouldn't it be amazing to find out what was going on in our minds when we were little babies?

Modern research into child psychology and neuroscience can help us in this endeavor. As you'll see in these blinks, children's minds and their perception of the world are formed through an intricate interplay between nature and nurture, beginning even in the womb. So what does the world look like through the eyes of a toddler?

In these blinks, you'll learn

  * why babies like garlic;

  * how babies end up practicing boxing; and

  * which stories babies enjoy listening to.

### 2. Genetic predisposition and childhood experiences influence an infant’s development. 

Picture a ball rolling down a hill. Gravity is pulling the ball downward, but its path might be diverted or changed by a rock or two in its path. Human development works a little like this. The main direction of our development is determined by our genes, but other factors can change the direction of our development, too.

Just 18 weeks after fertilization, the building blocks of a human embryo's brain are already formed, following a sequence determined by our genes. Our brains begin by first developing the components and capabilities essential to keep us alive, such as controlling our heart rate. More complex components, like those required for imagination and memory, form later on.

Genes also shape the way these different parts of the brain communicate with each other. _Neurons_ connect when our genes cause chemical attractants to be released at the right place and the right time inside our brains.

But after birth, genes aren't the only factors in human development. Between the ages of one and eight, children produce twice the number of neuronal connections required for healthy brain functionality. Because of this, efficient pathways for cognitive processes must be established, and for their brains to do this, children need nurture.

This need for nurture is critical — if children lack stimulation, the neuronal connections they need for healthy adult life will deteriorate rapidly. This has been known to psychiatrists since the 1940s, when René Spitz conducted an experiment comparing two groups of children. 

Children in the first group had been raised by their mothers in prison, while those in the second group had been taken from their mothers and raised in a nearby nursery. Children who'd grown up in the prison developed normally, thanks to their mothers' care.

But the children in the nursery, who had been growing up without human contact and intellectually stimulating play, suffered the consequences. Their neuronal connections degenerated, and many developed retardations that impeded their ability to walk and talk by the time they reached the age of three.

> _"From the first cell development, brain development is a delicate dance between genes and environment."_

### 3. Pregnancy and birth are two highly influential stages in a child’s development. 

Today, there's no shortage of guidebooks filled with advice on what to do during pregnancy. Of course, all children and mothers are different, but these discussions are important. After all, pregnancy and birth are crucial stages in human development.

Mothers' behavior during pregnancy can have lifelong consequences for their children. Regular consumption of alcohol can cause mental retardation in newborns, while over-the-counter drugs such as Aspirin can have devastating effects on unborn children, with the potential to cause internal bleeding, for example.

Even frequent caffeine consumption has been linked to miscarriages. On top of this, the mere influence of stress can cause a premature birth, but calming saunas and hot baths should also be avoided lest they cause neural defects in the unborn child.

Pregnancy isn't the only stage filled with countless dos and don'ts. The moment of birth also has a powerful influence on a child's development. While it represents a potentially painful experience for any mother, a natural birth does wonders for babies. Natural birth causes the newborn's brain to release stress hormones that allow them to cope with the new world they arrive in and develop better reflexes from the outset.

Of course, labor is a complicated process. Lasting brain damage can also result from damage to the placenta or insufficient oxygen. _Cerebral palsy_, a set of symptoms marked by movement and posture disorders, arises from such complications during childbirth, with the cognitive damage often discovered in later stages of infancy as the child exhibits delayed motor development.

### 4. A child’s sense of touch is crucial for the development of the brain and learned behaviors. 

Do babies enjoy the tickling and cheek-pinching as much as we do? Well, it turns out it's actually good for them.

Stimulating an infant's sense of touch is a necessary part of healthy development. Several studies show that preterm infants even benefit from daily massages, showing faster weight gain and better performance in visual recognition tests. Without this stimulation, the corresponding parts of a child's brain will degenerate.

This trend has also been demonstrated in animal studies. In one case, mice's whiskers were removed to prevent stimulation to the brain areas responsible for the sense of touch through the whiskers. As a result, neighboring brain cells took over these areas, which meant that even if the whiskers were reimplanted later, the mice would no longer be able to feel with them. In the same way, it's vital that infants have a consistent experience of touch and stimulation before it's too late.

By touching things and watching how parents respond, children also pick up important behavioral cues, some of which are crucial for a child's safety. Take the case of the abandoned French child Victor. He grew up alone, and never learned how to handle something hot. He burnt his hands attempting to pull potatoes out of a fire.

So, we've established that stimulation is crucial for the development of a child's sense of touch. But how exactly does this development occur? Well, it's a step-by-step process. The area of the brain responsible for sense of touch, the _somatosensory cortex_, grows very slowly.

First, connections to sensory receptors in the mouth are established, followed later on by greater connections to receptors in the hands. This explains why babies' mouths are so sensitive; newborns can even distinguish between their mothers' nipples and other women's. But it's not until they're 18 months old that children can perceive more subtle differences between objects with their hands.

### 5. The senses of smell and taste develop early in infants, leading them toward safety and nutrition. 

Have you ever had a cold and not been able to taste what you knew was a delicious meal? It can be extremely frustrating! Taste and smell are vital sensory functions for humans in general, and this begins in babies from their time in the womb.

Even as fetuses, babies can process information about their mother's smell. This becomes crucial for their sense of orientation — babies use it to work out where their mothers' arms are, as their body scent is most present under the armpits. An infant's sense of smell also allows for self-soothing, which unwashed newborns practice by raising their hands to their mouths, comforting themselves with their own smell.

Sense of taste also develops very early, allowing infants to detect which foods are beneficial to them. Eight weeks after conception, a fetus's first taste buds have already formed and can be put to use. This was demonstrated in a study where doctors injected saccharine (a sweetener) into the fluid surrounding fetuses. The fetuses were able to taste the sugar, and swallowed more of the fluid than usual.

Children generally prefer sweet and fatty tastes because they know instinctively that these foods will give them energy; bitter and sour tastes are less favorable.

However, some infants aren't able to detect salt in food until they reach four months of age. This had tragic consequences in a New York hospital in 1962, when the formula for 14 babies was accidentally prepared with salt instead of sugar. Six infants were unable to detect the difference, drank too much of the formula and died of salt poisoning.

At the breastfeeding stage, babies like variation in taste. In one experiment, four-month-olds consumed more milk after their mothers took garlic pills. Eating vanilla, mint and cheese can also making breast milk more interesting for infants.

### 6. While children develop their sense of hearing in the womb, their vision develops more slowly. 

Imagine watching a film where the images and sound aren't synced — it's very confusing or irritating, right? This is because our hearing and vision are deeply connected. But as infants, we rely far more on sound than we do on sight.

Babies enter the world with great hearing skills. While in the womb, fetuses listen closely and even form memories of the things they hear from the outside world. In one study, pregnant mothers were asked to read _The Cat in the Hat_ out loud several times a day. They read the story to their babies shortly after birth, and the newborns responded by sucking more excitedly on their pacifiers as they listened.

In fact, custom-built pacifiers connected to a set of headphones even allowed researchers to give babies a way to choose stories. Sucking more slowly would cause a recording of a different Dr. Seuss story to play. But by sucking faster, newborns would trigger the playback of the story they'd heard in the womb.

While hearing develops quickly for newborns, their vision remains quite poor for a while. Babies are only able to see what's close to them, which prevents overstimulation by ensuring that infants can only see as far as they can handle.

However, these limited visual systems are also quite impressionable. One study showed how infants' sense of sight adapts to the shapes in their environment. Indigenous babies in Canada were better able to recognize oblique angles, having been raised in teepee-shaped dwellings. White Canadian babies, on the other hand, were better at identifying right angles, which they'd become accustomed to in their homes.

It's only when infants are one year old that they begin to perceive spatial depth and color, perceiving the visual world more fully. But their sense of sight is still developing at this point. If their eyes are damaged before the age of two, children may struggle to estimate distance or even suffer from blindness.

> _"Whereas vision emerges late and matures quickly, hearing begins early but matures gradually."_

### 7. Young babies love movement and rely on reflexes before building more complex motor skills. 

If you've got a crying baby in your hands, the best way to calm it is rocking it gently in your arms. Babies also calm down enough to fall asleep when we push them around in carriages because they love the feeling of being in motion.

Experiences of motion satisfy a baby's need for stimulation. They can handle motion well too, because they're born with a well-developed sense of balance. This, in turn, allows babies to observe their environment from a very young age, as their brains are able to make sense of visual changes when they move their heads.

A baby's experience of motion is also shaped by a set of reflexes that emerges early on. For instance, babies up to around four months demonstrate the _Moro reflex_, which refers to the way infants react to a sudden loss of bodily support by flinging their arms out and bringing them in again in a hug-like motion, usually accompanied by crying. This reflects an evolutionary adaptation that helped infants hold onto their mothers while they made sudden movements.

With continual training, children develop new responses and learn to direct their movements with intention. One of the most important milestones for this development is the ability to reach out for something. At three months, infants already begin intentionally grasping at objects. But to do this, babies have to train — when infants appear to be practicing their boxing moves, they're actually working on their reaching abilities.

When it comes to walking, it takes about one year before babies find their feet — but the first training activities start much earlier. If you hold a six- to eight-week-old baby with its feet touching a flat surface, it'll instinctively start to raise its knees one-by-one.

While genes define the age at which a child is ready to learn new movements, her environment determines when these learning experiences actually happen. In this way, nature and nurture are connected when it comes to motor development. As we'll see in the next blink, this also applies to the development of character traits.

### 8. Temperament in children is often genetically determined, but is also shaped by childhood experiences. 

Much of what we love in other people has to do with their charming personalities and little quirks. What parent wouldn't want their child to grow up into a loveable person? Of course, while some things are determined genetically, other aspects of a human's character can be shaped by childhood experiences.

All babies begin smiling at the sound of other people's voices at around six months of age, and not only their parents'. This is called _social smiling_, and even blind babies develop this innate tendency around the same time. However, babies also exhibit _stranger anxiety_ from six months of age, largely because they start moving around independently at that time, too.

Other tendencies shaping how infants interact with others are determined by different genetic patterns. Researchers have found that around 15 percent of all children are genetically determined to be shy, while 15 percent are more outgoing. Shy infants are genetically predisposed to staying on the alert at all times, leaving them anxious and avoidant, with a higher heart rate and greater dilation of the pupils when under stress.

Nevertheless, personality is also shaped by a child's upbringing. While our temperament — whether we are shy or bubbly — is largely determined by our biological survival instincts, it is affected by our childhood experiences.

Around 40 percent of pathologically shy children improve their ability to deal with stress when their parents gently challenge them. This encourages them to connect with strangers in a safe way and build their social skills.

However, parents can also have a negative influence on their child's personality, and this is unfortunately the case for children whose mothers suffer from depression. As their mothers, through no fault of their own, tend to smile and play less, children can grow up with a depressive and irritable personality.

### 9. Children have a knack for learning languages, but parents must help them learn new words and enjoy the learning process itself. 

How is it that we learned our mother tongue so easily as kids, while learning new languages today is so hard? Well, babies have the upper hand here — they're predisposed to learning languages.

Babies all around the world begin to understand the meaning of their first words at around nine months of age. In their second year, they begin to comprehend words more rapidly, and learn about eight words each day between two and six years of age.

Just as the way children learn languages is universal, the mistakes they make when speaking are also very similar across the board. After learning grammatical rules, children tend to ignore exceptions that come up. English children, for instance, might use words such as _comed, beed_ and _gots_ instead of _came, was_ and _got._

Once we hit adulthood, our ability to learn new languages isn't what it was when we were younger, to say the least. Why? Because as adults, our brains aim for mastery of the languages we already know. When we hear an unfamiliar language, the left hemisphere of our brains — the area responsible for all that language learning we did as infants — isn't even activated.

Despite the powerful influence of these physiological factors, parents can also shape their child's language acquisition. Individuals can differ widely when it comes to their vocabulary, which can be attributed in large part to the level of education received not only by children, but by their parents as well. In other words, socioeconomic levels can impact the amount of words a child learns.

Studies have shown that while children in families on welfare hear around 600 words per hour, children from wealthier socioeconomic backgrounds can hear up to 2,100 words per hour. This makes it a lot easier to learn a language more quickly.

Parents can ensure their children acquire their language at a steady rate by talking to their kids as much as possible, being precise and encouraging their children. Scientists have also found that correcting children too often hampers their innate drive to learn more new words. Parents should simply trust that their kid will correct their mistakes with time, utilizing an ability that they're born with.

### 10. A fun and stimulating home environment helps children develop their intellect at a healthy rate. 

From psychology to preschool pedagogy, experts from all fields claim to have unlocked the secrets of intelligence in children. However, defining intelligence itself is a very challenging task. This is evident in the difficulty we have when it comes to measuring how "smart" children are.

Most scientists believe that a single factor called _general intelligence_ influences most skills. IQ tests are designed to assess general intelligence but, of course, infants can't take IQ tests. So how can we determine whether a child's intellect is developing at a healthy rate?

There are a few key milestones that show children are on the right track. At four years old, children should be able to understand the difference between appearance and identity. For instance, they'll know that a white object is still white even if it appears to be red when seen under a red light. Then, at the age of six, children can grasp the idea that the amount of water in a tall but narrow container can also fit into a short but wide container.

Parents can help kids meet these milestones by creating a great learning environment at home. Researchers demonstrated this back in 1972 with the North Carolina Abecedarian project, where infant children began receiving 40 hours of weekly care. At the age of three, these children outperformed a group of non-participants, who received no special care, by 15 IQ points.

So how can parents make sure kids keep learning? Intellectually stimulating play, like simple word and number games, are a great place to start. Strict preschooling, on the other hand, is less helpful; it can make kids fear school, causing academic problems later on in life.

Another great activity for kids is learning an instrument, which trains motor skills, spatial awareness, timing and listening all at once. But above all, fun is essential! Parents that find playful ways to engage and enjoy time with their children set them up for a better adult life.

### 11. Final summary 

The key message in this book:

**Human biology and genetic predisposition lay the foundations for an infant's cognitive development. However, children are adaptive beings and benefit greatly from a positive, stimulating and fun environment as they learn to walk, talk and think for themselves.**

Actionable advice:

**When it comes to toys, it's all about balance.**

Toys are one way to keep infants stimulated, but it's important to find a balance. Buying kids new playthings too often encourages them to give up on toys they break or don't understand, while too few toys can frustrate kids and bore them. Try keeping a few good toys that you can exchange with fellow parents when it's time for a change.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Whole-Brain Child_** **by Daniel J. Siegel & Tina Payne Bryson**

_The Whole-Brain Child_ (2011) is a parent's guide to understanding children's minds. These blinks explain how to help your child integrate various aspects of his or her brain and develop into a mentally well-rounded human.
---

### Lise Eliot

Lise Eliot is a neuroscientist and professor at the Chicago Medical School. As a writer, she is known for her contributions to the magazine _Slate,_ as well as her book _Pink Brain, Blue Brain: How Small Differences Grow into Troublesome Gaps and What We Can Do About It_.

