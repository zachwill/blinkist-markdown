---
id: 54a98d006233320009510000
slug: whats-mine-is-yours-en
published_date: 2015-01-07T00:00:00.000+00:00
author: Rachel Botsman and Roo Rogers
title: What's Mine Is Yours
subtitle: How Collaborative Consumption Is Changing the Way We Live
main_color: FAEE32
text_color: 7A7518
---

# What's Mine Is Yours

_How Collaborative Consumption Is Changing the Way We Live_

**Rachel Botsman and Roo Rogers**

_What's Mine Is Yours_ paints a startling picture of our future if we stay on our current trajectory. Widespread over-consumption and traditional production models spell almost certain doom — that is, unless we can nurture the new economic landscape evolving in front of our eyes: "collaborative consumption."

---
### 1. What’s in it for me? Find out how collaborative consumption will change the world. 

There's a huge mass of plastic floating in the ocean. Forests the size of Greece are chopped down every year. There are more shopping malls than high schools in America.

What do all these incredibly depressing facts have in common? They all have to do with our almost insane rates of consumption. In fact, consumption has reached unprecedented levels. We're buying, using and throwing out more stuff than any other time in the history of humanity, leading to the kinds of consequences listed above.

Yet it can't go on like this, and so a whole new paradigm is slowly taking shape. That's what _What's Mine Is Yours_ is about: this new kind of economy by which we don't just buy things, use them, and throw them out. We share them, save them, or pass them along.

These blinks explain what _collaborative consumption_ will look like, how it'll change our current economy, and how it might just be the best way to save the world.

In these blinks, you'll learn

  * why farmer's markets are becoming so popular;

  * how you can save $600 a month by car sharing; and

  * why you ought to share that rusty old bush trimmer in your garage.

### 2. We consume more today than ever before. 

Americans love to shop. So much so that, in the United States, there are now more shopping centers than there are high schools!

Indeed, never before have we consumed so much, and the contrast with the past is astounding.

In the past fifty years, Americans have consumed more goods and services than _all_ previous generations combined. American families in the early 1990s had twice as many possessions as they did only 20 years prior.

And since we have more possessions, we need more places to keep them. In the US alone, personal storage spaces comprise a combined area of 2.35 _billion_ square feet. Just think: before 1964, personal storage facilities didn't even exist.

But why do we consume so much more today than we did in the past? The answer lies in several different forces that work together.

For one, today's marketing strategies are more sophisticated and pervasive. Advertisements are everywhere: the average person sees more than 1,000 advertisements every day.

Moreover, today's products aren't built to last. Companies deliberately design products to fail after a certain time as a way to increase sales. General Electric was designing for "planned obsolescence" as early as 1932, when they deliberately shortened the lifespan of their light bulbs.

Supermarkets are filled with incredible amounts of disposable products that can be used only once, like styrofoam cups and disposable razors.

Finally, politicians and economists believe that consumption is the engine of economic growth. As a result, there's a strong political incentive to motivate us to consume even more.

For instance, to boost car sales during the recession in 2009, twelve EU countries decided to offer cash incentives to consumers who would trade in their old cars for new ones. In Germany, for example, customers that traded their old cars for more fuel-efficient models would get €2,500.

But, as you will see, our record-breaking consumption has consequences.

### 3. Overconsumption and “throwaway living” are wrecking our environment. 

In 1955 the cover of _Life_ magazine featured a photo of a family tossing disposables in the air entitled "Throwaway Living." Indeed, disposable products are a huge part of our culture. As you'll soon see, this has major implications for our society.

Our enormous consumption is burning through the earth's resources at break-neck speeds. Since 1980, we've used up one third of our planet's remaining resources — ranging from fish to forests, metals and minerals.

To visualize this, consider that in the tropics, deforestation destroys an area as large as all of Greece _each year_. In fact, we consume so much that, if everyone on earth lived like an average American child, we would need five earth-like planets to sustain our consumption during only one lifetime.

All this consumption means that we produce vast amounts of plastic garbage — 100 million metric tons per year, in fact — that litters the oceans. And plastic waste doesn't go away; it can only degrade into smaller pieces.

Our penchant for disposable goods only exacerbates this problem, since we dispose of both the product _and_ the packaging.

As a result, the oceans are full of plastic garbage. In fact, each square meter of our ocean surfaces are littered with 46,000 pieces of plastic. Birds and fish mistake these plastic bits for food, leading some animals to die with bellies full of plastic.

Sometimes this garbage accumulates, forming vast regions of devastation. For example, in the middle of the Pacific Ocean floats an enormous field of garbage, where marine life then indiscriminately eat the garbage, unaware that it isn't food. An estimated 3.5 million tons of garbage span hundreds of thousands of square miles. This _Great Pacific Garbage Patch_ isn't an isolated incident. Some environmentalists say that up to 40 percent of the sea may already be littered with tiny pieces of plastic.

Clearly, over-consumption leads to serious environmental consequences. But at least it's making us happy, right?

### 4. All this consumption also comes at high personal costs. 

In 2008 a WalMart security guard was trampled to death by shoppers. Was this the result of a panic, in which shoppers were desperately trying to escape a fire or a bomb threat? No. They were trying to get _in_, not out. Why? Shoppers were anxious to take advantage of a new promotion — a plasma HDTV for just $789!

Clearly, we place great weight on acquiring new possessions. But it comes at a cost: our own happiness.

As a culture, we place a heavy emphasis on collecting material possessions, and many people rank shopping as being among our favorite activities. Our rituals, such as birthdays, holidays, weddings, etc., all feature wish-lists (where we wish for "a new TV," not "world peace").

With our sights set on new material possessions, we sometimes become oblivious that we're spending more than we can afford, which can result in serious debt.

The average American household, for example, accumulates $8,000 in debt, and pays no less than $1,000 per year in interest and fees. In 2007, American consumers carried an average of $937 dollars per capita in credit card debt.

The massive increase in consumption since the 1950s doesn't just hurt our wallets. It also hurts our relationships, and coincides with a massive decrease in our happiness.

As we've already mentioned, our consumption has been steadily increasing since the 1960s. But we're no happier today than we were then. In fact, societal indicators signal that we're faring much _worse_ than before.

Compared to 1960, three times as many teenagers commit suicide every day, and the prison population has quintupled. Since the 1980s psychological complaints, like depression and anxiety, have increased astonishingly, as has the rate of conditions like insomnia, obesity and heart disease.

Clearly, our massive consumption harms both our environment and our personal well-being. But what is the alternative? The following blinks will investigate this question.

> _"The things you own end up owning you." - from the David Fincher film Fight Club_

### 5. A new economic landscape is emerging. 

After seeing the horrible side effects of our current system, we have to wonder whether there's a better way to distribute goods. It turns out that there is! But before we get to that, let's take a look at the changes happening in our economy.

People are becoming increasingly aware that the premises of our consumption-oriented economy are false.

Our economy struggles because we're burning the candle at both ends: on one hand, its growth depends on continually increasing consumption, yet, on the other hand, our natural resources may dry up if we continue our current rate of consumption.

Consequently, we're beginning to search for ways to consume less and get more out of each purchase, doing things like repairing clothes and sharing cars and tools.

What's more, we're starting to understand that consumerism impoverishes our relationships. As we increase our focus on material possessions as a source of happiness, we invest less time and energy into our personal relationships.

Surely you know at least one person who works long hours to afford a nice home for his or her family only to discover that they don't actually have the time to enjoy their company.

These failing relationships can lead to isolation and even more consumption. Think, for instance, of those guilty dads who buy their kids extravagant birthday presents to make up for not being around.

But we _are_ learning, and are thus investing more care, time and effort into our consumption. For example, many people today find that they prefer the personal interactions found at places like flea markets or local farmers' markets to the anonymous experience of shopping at a chain-store.

In fact, farmer's markets have become the fastest-growing part of the American food economy.

Indeed, there are signs that we're rethinking consumption. But there is yet another factor that is changing our consumption habits.

### 6. The internet also contributes to the new economic landscape. 

The internet allows collaborative and democratic transactions which are inherently dynamic.

The internet connects people from all corners of the globe, making it easy to communicate and do business with each other other.

You'll find more interest for your goods and services via online marketplaces than you could possibly find using traditional advertising, like classified ads or your local supermarket's notice board.

For example, if you live in a cottage in the forest and want to lease one of the rooms to tourists, you'll discover that there aren't many places to advertise in the forest. Online marketplaces like Airbnb, however, offer a platform to bring together hosts and vacationers all over the world.

Because the internet connects so many people, it becomes easier to reach the critical mass necessary for new systems to function, such as _collaborative consumption_ systems. These systems are comprised of a set of economic arrangements in which people share — rather than own — access to products or services.

Collaborative consumption systems need a critical mass of items and participants to gain momentum. They simply can't compete with conventional shopping systems unless they can supply the customer with enough choice.

Imagine, for example, that you're looking for a specific book. Likely, your chances for finding it are better in a larger marketplace than at a tiny flea market. Today, however, there are huge online platforms where you can swap, share or even sell books (as well as other items) without ever stepping foot in a store: i.e., peer-to-peer.

In these kinds of online marketplaces, there's no producer-seller-consumer divide. Instead, there is often a more personalized interaction, and the seller might also be a buyer, and vice versa.

So what does the future look like for these collaborate systems. Our final blinks will look at different kinds of collaborative consumption systems.

> "...Online networks bring people together again, making them more willing to leverage the old rule of thumb: there's power in numbers."

### 7. Product service systems are good if you don’t want to buy or own the things you want to use. 

Lots of people love movies, but don't want their homes to resemble a video store. They want to _watch_ movies, but have no interest in buying and collecting them. Luckily, we have libraries and online video rental services who serve those customers in a way that gives them the film-watching experience without forcing them to ever own a film.

Indeed, libraries were among the first _product service systems_ (PSS) which shift the focus from owning goods to just using them.

Product service systems are what enable participants to share or rent goods, like DVD rentals. As consumers share or rent objects instead of owning them, then our economy begins to shift from a consumption economy towards a service economy.

In this system, it might be cool for people to _share_ a luxury car rather than own one, for example.

Using a PSS comes with a number of advantages:

For starters, they're better for the environment. With less need for ownership, fewer products need to be produced to meet demand.

Just think: the average car sits idle for 23 hours a day. Imagine how much we could save in terms of resources if five people would share one car! Companies such as Zipcar see the wisdom there, and tapped into the car-sharing potential.

What's more, product service systems save you money, and give you access to goods you couldn't otherwise afford.

Looking back to cars, for example, it's cheaper to share than to buy. Average British car users save around $600 _per month_ when they make the switch to car sharing.

Finally, if you have less stuff, then you need less space to store it. Without a car, there's no need for such a large garage. By the same token, if you share lawn-care supplies, then you won't have to clutter up your shed!

> _"When our relationship with things moves from ownership to use, options to satisfy our needs. . . change and increase."_

### 8. Redistribution markets make the best out of every product. 

Be honest: What do you do with unwanted presents? Remember the bread-maker your aunt gave you last Christmas? The only one who's using it is the cat, and all he does is sleep on the packaging.

Indeed, our homes are cluttered with valuable items that just sit around collecting dust. In Australia, for example, people spend $10.8 billion AUD per year for products that they never even use — not even once.

The products themselves are fine, but they aren't doing anything other than taking up space, collecting dust and possibly ending up in the trash.

All the while, someone _else_ yearns to own the things that you couldn't care less about. Just imagine that something that you really want, like a treadmill or a blender, is rusting away in someone else's basement even as you read this page!

What if someone actually had good use for that bread-maker? Shouldn't there be an easy way for them to take it off your hands?

In _redistribution markets_, goods can be moved between owners — from those who don't want a particular object to those who are eager to have it, either by simply giving it away, swapping or selling it.

Just think of eBay or Freecycle, the "eco eBay." Freecycle functions like an online charity store, only it's open for everyone to participate. Anyone can offer and find almost anything on it, including things like half-used cans of paint. Since its launch in 2003, the site now has seven-million users!

Redistribution markets often create win-win-win situations: for the happy new owner of a good, for the former owner who's just glad to clear his garage of some clutter, and for the environment, since fewer goods means less garbage.

> _"There's no such thing as waste, just useful stuff in the wrong place." - Alex Steffen (futurist)_

### 9. Collaborative consumption isn’t limited to tangible objects. 

If you live in a big city, then you probably don't own a garden, despite wanting to grow your own vegetables, breed rare roses and be part of a community of passionate gardeners.

While you might not be able to tend a garden on your own, you would be able to do so if you adopted a _collaborative lifestyle_. Such a lifestyle is the third kind of collaborative consumption system — one in which networks of people share intangible assets, such as office space, translations or other services, and even lend money.

Collaborative lifestyle arrangements already exist. For instance, the website gardenswap.org facilitates collaboration by pairing those who have gardening space with others who'd like to garden. Eventually, once the crop is tended, it is shared among the participants.

Like Garden Swap, most collaborative lifestyle systems operate on a local level, as it's simply impractical to share a garden, workshop or workspace with people who live on another continent.

Still, collaborative lifestyles are emerging on a global scale.

For example, peer-to-peer traveling communities such as CouchSurfing and the aforementioned Airbnb work across borders, as do peer-to-peer social lending sites.

One such lending service is Zopa, launched in 2005 in the UK. Zopa's service is simple: it matches customers who offer to lend money to those who want to borrow it.

Usually, interest rates are low and monthly payments are flexible. In addition, each loan is spread across several lenders in order to minimize risk, with the minimum loan amount set to £10.

Collaborative lifestyles aren't just about the goods; they're also about community. While consumers may initially use collaborative lifestyle systems in order to find goods or services, e.g., a couch to crash on during a trip, they also find a new form of social bonds.

Often, they meet like-minded people and even forge new friendships. In fact, an estimated 111,186 close friendships have been created through CouchSurfing alone!

These kinds of systems can help us fight against the hyper-individualistic, resource-depleting side-effects of classic consumerism, and live better lives while we're at it.

### 10. Final summary 

The key message in this book:

**Our frantic consumption and "throwaway lifestyles" are wrecking our environment and impoverishing our relationships. Luckily, new economic relationships are emerging that promise to reduce our environmental footprint, curb our consumption and make us happier overall.**

**Suggested further reading:** ** _Wikinomics_** **by Don Tapscott & Anthony D. Williams**

Wikinomics shows how mass collaboration influences global as well as local economic and social structures, and examines how the new possibilities of the internet age have already changed the way businesses operate. In addition, the authors explain how companies and people in general can benefit from global collaboration, rather than feel threatened by its revolutionary character.

**Got feedback?**

We'd love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Rachel Botsman and Roo Rogers

Rachel Botsman is among the most influential thinkers on collaboration. She is the founder of the innovation incubator _Collaborative Lab_ as well as a former director at the President Clinton Foundation.

Roo Rogers began his career working for UNICEF in the developing world. Today he is the co-founder of OZOlab and a former CEO of OZOcar, an alternative town-car service that uses hybrid cars.

