---
id: 594f008ab238e10005bb48e4
slug: when-breath-becomes-air-en
published_date: 2017-06-28T00:00:00.000+00:00
author: Paul Kalanithi
title: When Breath Becomes Air
subtitle: None
main_color: 79C6DF
text_color: 298BAB
---

# When Breath Becomes Air

_None_

**Paul Kalanithi**

_When Breath Becomes Air_ (2016) tells the incredible story of Paul Kalanithi, a neurosurgeon and neuroscientist who was diagnosed with and died from cancer in his mid-thirties. These blinks detail his extraordinary journey in search of the meaning of life in the face of death.

---
### 1. What’s in it for me? Learn about the relationships between chance, life and death. 

Has everything turned out exactly as you planned? Maybe you intended to study one thing but were pulled in another direction; maybe one choice completely changed the course of your life; or maybe you're still in high school and think your career will follow a set path.

Of course, we all know that life is messy and unfolds in unpredictable ways — unexpected twists and turns are inevitable.

Paul Kalanithi's life was no exception. What made his experience even more potent was his career in neurosurgery, where small decisions and unforeseen events can result in a matter of life and death, and his diagnosis with terminal cancer.

In these blinks, you'll learn 

  * how neuroscience might answer some of your most pressing existential questions;

  * why it's difficult to say whether a surgery was a complete success or failure; and

  * how great power comes with great responsibility.

### 2. The author had two passions that persisted throughout his life: literature and neuroscience. 

While he was growing up, the author, Paul Kalanithi, became obsessed with literary geniuses like Orwell, Camus, Sartre, Poe and Thoreau. It was this passion that informed his decision to study literature in college, but things turned out quite differently than he had planned. During the summer before college, he discovered another field that captivated him: human biology.

Kalanithi was getting ready to head off to Stanford when his then-girlfriend gave him a book by Jeremy Leven called _Satan: His Psychotherapy and Cure by the Unfortunate Dr. Kassler, J.S.P.S._ He was fascinated by Leven's idea that the brain is just an organic machine that enables the human mind to exist, so he signed up for courses in biology and neuroscience.

Throughout his college career, Kalanithi continued to ponder big questions like, "what infuses life with meaning?", looking to both literature and neuroscience for answers. Works of fiction remained powerful to him as he considered literature a manifestation of the mind's life, and therefore of human meaning.

So, while meaning itself is certainly no simple concept, he drew inspiration from writers like T. S. Eliot and his work _The Waste Land_, which connects meaninglessness to isolation, and came to believe that true meaning lies in human relationships.

However, literature only offered one piece of the puzzle, and Kalanithi knew he had to keep studying neuroscience. He felt that, since the brain is the organ that enables the mind to exist, our ability to form relationships comes directly from our brains.

This perspective was informed by his experiences. While visiting a home for people with brain injuries, Kalanithi found that the patients weren't entirely capable of forming relationships.

From there, neuroscience gave him a way to explain the laws of the brain, which were central to his understanding of meaning at large. Because of this fascination, he applied to medical school, but only through hands-on experience would he come to understand the true meanings of life and death.

### 3. Medical school gave Kalanithi a direct understanding of life, death and meaning. 

After months of grueling medical school applications, Kalanithi finally landed a spot at the Yale School of Medicine. It was in the anatomy lab that he found himself confronted with the realities of life and death.

As a part of his course work, he spent innumerable hours dissecting cadavers, slicing through skin and tissue and sawing through bone. While the medical students would keep the faces of these dead bodies covered and remained unaware of their names, Kalanithi was still very much moved by their humanity. One day, while opening up the stomach of a cadaver, he found a couple of undigested pills — a sign to him of life even within the dead.

But Kalanithi's experience of life and death would extend beyond the walls of the anatomy lab. After all, as a medical student, he routinely had experiences that illuminated these powerful concepts.

Take, for example, his time in the labor and delivery ward. The first birth he was present for also turned out to be about death.

It happened on his first day on the ward, when he was briefed about a young woman who was expected to have twins but had been hospitalized for premature labor. She had only been pregnant for 23.5 weeks, and the doctors were fighting to keep the pregnancy viable.

Sadly, an emergency cesarean was necessary. Kalanithi was there for the operation and saw the tiny, near transparent infants as they were removed from the womb. Because of the babies' premature state, their organs were incapable of sustaining them, and the twins eventually passed away, leaving Kalanithi with an image of life one day, and death the next.

### 4. In residency, Kalanithi had more responsibility and even more direct experiences with death. 

During his fourth year of medical school, Kalanithi made the decision to specialize in neurosurgery. He knew this choice was far from easy, but the field was beckoning him. So, following his graduation, he began a residency at Stanford, where he would train for the next seven years.

It wasn't long before he found himself working under the pressure of real responsibility, a situation he had only gotten glimpses of during medical school. One day a young boy named Matthew came in with a headache. An examination showed he had a substantial brain tumor, and it was Kalanithi's responsibility to decide on a course of action.

Removing the tumor completely would give Matthew his childhood back, but because of its location in the hypothalamus — the region of the brain responsible for basic drives like hunger and sleep — even the a microscopic error would have drastic consequences. It was a tough decision, but in the end, Kalanithi made the call to operate and removed the tumor.

However, during his first year in residency, Kalanithi saw many deaths, both around the hospital and among his own patients. He saw deaths caused by head trauma, gunshot wounds, brawls and traffic accidents; he saw an alcoholic pass away when his blood no longer had the capacity to clot, causing him to bleed to death.

He even saw a medical practitioner who specialized in the nature of diseases die from pneumonia, eventually being sent for autopsy in the same lab where she had herself worked for so many years.

So, his first year was challenging, and the next five wouldn't prove much easier.

### 5. Amid increased responsibility and exhaustion, Kalanithi found himself neglecting the human side of his work. 

During the second year of his residency Kalanithi was put on call, which meant that in an emergency, he would be the first to arrive. Luckily, the responsibility he was beginning to shoulder increased in proportion to his skill level. But now, more than ever, it was up to him to call the shots and to decide whether someone would or should be saved.

On one occasion a patient was rushed into the operating room with severe brain trauma. The team saved him, but the patient permanently lost the ability to speak or eat by himself. Nothing could be done to save his brain, and he would be forced to spend the rest of his life institutionalized. So, while the patient still had a pulse, Kalanithi wasn't sure whether saving his life was the right thing to do.

Not just that, but because of the long hours he was working and the exhaustion he experienced, he began to question if he was honoring the basic humanity of his patients. After all, like his peers, he routinely worked a hundred hours a week and, under the pressure to excel, he was constantly tired. His head began to ache; he would guzzle energy drinks at night and, before making the drive home, would take a nap in his car.

He began to rush his patients, even in upsetting situations. Once, when needing to question a woman who had just found out she had brain cancer, he raced through the interaction, totally incapable of focusing.

With little regard for her fear and uncertainty, he told her that surgery was definitely the best option. Later, feeling guilty, he reminded himself that he'd gone into medicine because the connections forged between people were an important part of the meaning of life. He needed to honor those relationships with his patients.

### 6. Kalanithi worked in a neuroscience lab before returning to even more responsibility at the hospital. 

While in his fourth year of residency, Kalanithi received training in a field outside his specialization. He began work in a neuroscience lab at Stanford where he began studying to be a neuroscientist — a specialist who studies the nervous system, including the anatomy and biochemistry of neurons and their networks.

As you might imagine, being both a neurosurgeon and neuroscientist was simultaneously highly prestigious and difficult. Kalanithi also took a different route to his peers; while many neuroscientists in the lab were studying technologies that could help, for instance, a paralyzed person operate a computer or robotic leg with her mind, Kalanithi wanted to study the reverse of this.

That is, he was fascinated by _neuromodulation_, which involves finding ways to send signals from robotic limbs to the brain. If research into neuromodulation was successful, technicians could create artificial limbs that help the brain respond to the patient's surroundings. For example, an amputee walking on uneven ground would be able to move more easily with feedback from his artificial foot.

Despite this fascinating work, Kalanithi didn't remain in the lab. After almost two years, he returned to the hospital as chief resident.

As a neurosurgeon at this stage in his training, he was expected to be quick and neat, while holding more responsibility than ever.

It was during this time that he realized that technical excellence was morally required. With so much at stake for patients and their families, good intentions could never stand alone. Skill was critical.

For example, Kalanithi learned that Matthew, the young boy he had successfully treated years earlier, was now in a dramatically worsening condition. He was becoming violent and uncontrollable, eventually requiring permanent institutionalization. Kalanithi had unwittingly damaged a piece of his brain while removing the tumor.

### 7. Just before his residency ended, Kalanithi made a grim discovery. 

Kalanithi made mistakes, but he also excelled. Despite the incredible pressure he was under, his hard work paid off.

His residency was drawing to a close. He'd hurdled all the obstacles in his way and performed all the operations required of him. He'd even received prestigious awards and the respect of the senior doctors with whom he worked.

Furthermore, Stanford had come up with a position perfectly suited to his credentials; they wanted him to work in the hospital as a neurosurgeon-neuroscientist with a focus on neuromodulation.

But with just 15 months left in his residency, Kalanithi's fate took another turn.

For six months he had been shedding considerable weight and was suffering severe back pain, something he'd never experienced before. He went to the doctor, and at his first appointment had a few X-rays. His doctor concluded he'd simply overdone things at work.

Kalanithi was skeptical but returned to the ward nonetheless. He wanted to finish his residency, which he'd fought so hard to complete.

However, the pain came back, this time in his chest. His weight loss continued, he was down to just 145 pounds, and suffered with a persistent cough. Kalanithi was aware that his symptoms were clear indications of cancer.

Finally, his doctor took another look at the chest scan and saw that it was oddly blurred. Kalanithi instantly understood what that meant: his lungs were full of tumors. His spine and liver were deformed; cancer had already spread across his body, and his case was terminal.

### 8. Kalanithi contemplated life as a patient and debated whether he and his wife should have children. 

If you knew your time on earth was going to be cut dramatically short, how would you spend your remaining days?

It's one of the toughest questions imaginable, and during his treatment, Kalanithi had no clue how to proceed with his life and career. In discussions with his doctor, Emma, to whom he felt very close, he talked about how disturbing it was not to know how much time he had left.

After all, if he had decades to live, he would continue doing neurosurgery, but if he had just one or two years, he would probably spend his time writing. Literature and writing were still hugely significant for him, and Emma advised him to focus on what truly mattered.

But his experience was also confusing: How could he be a doctor who is also a patient? As a doctor, he would pore over the medical books for technical answers, but as a patient, he would seek out philosophical answers in the literature that he loved.

Meanwhile, another major question lingered. He and his wife Lucy, who had always been by his side, were wondering if they should have children before it was too late.

So, right after his diagnosis, he and Lucy went to a sperm bank to learn about the healthiest, lowest-risk options. While the couple had always intended to have children, with such uncertainty over how long Kalanithi would live, they were no longer sure.

In the end, they opted for life. To avoid any complications with the medicine Kalanithi was going to take, they froze his sperm before treatment and Lucy was later inseminated and became pregnant.

### 9. Although in a poor state, Kalanithi witnessed his daughter’s birth, and passed away shortly afterward. 

On 4 July 2014, Kalanithi's daughter, Elizabeth Acadia, also known as Cady, was born. While Kalanithi survived long enough to see her birth, he was in a severely weakened state.

When Lucy went into labor, Kalanithi was debilitated, having been in and out of the hospital. Despite a brief and temporary recovery, he weighed next to nothing and had to lie on a cot in the delivery room, unable to stand as Lucy gave birth.

Nonetheless, he went home with his family, still shaky and thin and unable to do simple things like sitting, reading and drinking.

Then, around Christmas time, five months after Cady´s birth, Kalanithi's cancer severely worsened.

The cancer started resisting treatment. Neither the drugs nor the chemotherapy worked anymore. As a result, in the months that followed, Kalanithi became weaker and weaker. Yet despite the deepening sorrow felt by his family, they were still able to appreciate some parts of life, like inviting friends over for dinner and playing with baby Cady.

However, by February, Kalanithi became severely impaired, growing more and more tired and nauseous. In the end he didn't feel like eating at all.

The results from his hospital scans revealed that not only had the cancer spread in his lungs, but tumors had also grown in his brain. With time, Kalanithi knew this would afflict his mind; the neurological damage that the tumors would cause would strip him of his ability to think logically and find meaning in life.

### 10. After Kalanithi’s death, his wife Lucy reflected on their time together with both sadness and gratitude. 

Eight months after Lucy gave birth to Cady, Kalanithi was rushed to the emergency room with breathing difficulties. He would never recover.

Once in the hospital, Kalanithi was put on breathing support, but knew this was a temporary solution only. After discussing treatment options, he asked to be taken off life support, aware that if he'd opted to go on a ventilator, he might never come off it. Kalanithi concluded that as the time he had left was losing its meaning, there was little point in keeping his heart beating.

Within hours, and with his family at his side, he was taken off breathing support and given morphine to alleviate his pain.

One by one, people came to pay their respects and offer words of love to Kalanithi. Eventually, he closed his eyes, breathing only in fits and starts. At around 9 p.m. on 9 March 2015, at the age of 37, Kalanithi passed away.

Although crushed by the tragedy and sorrow of her husband's death, Kalanithi's wife Lucy also remembers her last months and years with Kalanithi as filled with love and meaning.

In the lead up to his death, she and Kalanithi had become as inseparable as they had been when they first met years ago while still studying.

His family, too, drew strength from one another. Kalanithi's parents and brothers would keep him, Lucy, and Cady company at the hospital. They'd chat about football, read books aloud, and eat his mother's homemade Indian dosa.

Finally, Lucy took solace in the fact that she could ensure her husband's "unfinished" manuscript was published.

Although Kalanithi didn't finish his book in the way he'd first hoped, despite toiling away at it for as long as his declining health would allow him, his book is complete in the sense that it captures his reality as he faced death.

Lucy believes that through his book, Kalanithi fulfilled his wish; to help others not only to understand and accept death as a part of life, but to know it's possible to live life with integrity and meaning while facing death.

### 11. Final summary 

The key message in this book:

**As a literary enthusiast and neurosurgeon, Paul Kalanithi lived and worked in close proximity to life, death and the meaning of both. His story is a gripping tale of humanity, mortality and the pursuit of meaning.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Being Mortal_** **by Atul Gawande**

_Being Mortal_ (2014) helps the reader navigate and understand one of life's most sobering inevitabilities: death. In this book, you will learn about the successes and failures of modern society's approach to death and dying. You'll also learn how to confront death and, by doing so, how to make the most out of life.
---

### Paul Kalanithi

Paul Kalanithi was an Indian-American writer and award-winning surgeon. He attended the Yale School of Medicine and completed his residency at Stanford University.

