---
id: 5518968a3961320007190000
slug: when-genius-failed-en
published_date: 2015-03-30T00:00:00.000+00:00
author: Roger Lowenstein
title: When Genius Failed
subtitle: The Rise and Fall of Long-Term Capital Management
main_color: 876A9A
text_color: 613280
---

# When Genius Failed

_The Rise and Fall of Long-Term Capital Management_

**Roger Lowenstein**

_When Genius Failed_ (2001) follows the rise and fall of Long-Term Capital Management, the world's largest ever investment fund. The book reveals uncomfortable truths about the nature of investment and the fragility of the models we use to assess risk.

---
### 1. What’s in it for me? Learn how one hedge fund tried to out-think the market, and failed. 

Do you know the story of Icarus? He was given a pair of wings that allowed him to fly, and he took full advantage. Unfortunately for him, he got a little carried away. Despite warnings not to, he proceeded to fly as high as he could, so high that the sun started to melt the wax that held his wings together. Within seconds, his wings fell apart and he plunged to his death.

This famous fable on the dangers of hubris can be easily applied to the story of Long-Term Capital Management, a huge hedge fund which dominated the financial markets in the 1990s. Like Icarus, they found themselves riding high, with profits through the roof, and yet they also went too far.

These blinks will provide you with an important reminder that no company, no matter how important, can hope to cheat the market.

In these blinks you'll discover

  * why we are far from rational; and

  * why academics might not be the best financial advisors.

### 2. Long-Term Capital Management was an enormous hedge fund that made its money through arbitrage. 

It's a fair bet that you've probably never heard of Long-Term Capital Management (LTCM), a long defunct fund management company. The 1997 Asian financial crisis or the 1998 Russian default, however, are two events that are probably much more familiar to you, as they brought the financial world to the brink of collapse. LTCM had an important role to play in both.

LTCM was a _hedge fund_ founded in 1994 by trader John Meriwether. Hedge funds manage the pooled investments of small groups of mostly wealthy investors. Unlike their cousins, _mutual funds_, which manage the investments of a larger, more economically diverse group of investors, hedge funds are subject to very little regulation, meaning that there are virtually no limits to the size of the fund or where it can be invested.

This lack of regulation makes hedge funds a ripe environment for investment in riskier financial products, such as derivatives _._

Like all other hedge funds, LTCM managed their investments with a strategy called _arbitrage_, whereby hedge fund managers purchase or sell financial products in the hope or knowledge that their price will change in their favor in the near future.

To demonstrate this, imagine that one company sells different stock in two markets. As both stocks represent the same company, you'd expect them to be the same price. However, sometimes the price of the stock in one market may dip below the other. When this occurs, you have an opportunity to quickly buy this stock before the prices reach equilibrium again, after which you can sell that stock at a profit.

In reality, the dynamics of the market don't create these clear-cut scenarios. In fact, most arbitrage strategies rely on tiny, rapidly disappearing discrepancies in the price of financial products.

This gave LTCM an advantage. They used academic calculations and predictions as well as the latest computer software to recognize the opportunities and exploit them quickly. Using this strategy, LTCM became the largest hedge fund _ever_. So what happened?

### 3. LTCM leveraged heavily in order to maximize their profits. 

Hedge funds bet on tiny discrepancies between the present and future price of financial products, which means that they need large investments to make any significant profits.

Despite the huge funds they had received from their wealthy investors, it wasn't enough. To earn the greatest profits, hedge funds need to _leverage_, i.e., they need to borrow in order to maximize their potential returns.

So, LTCM borrowed heavily, and encouraged investors to make large investments. Many banks were more than happy to lend huge amounts to the company, because — at least in theory — LTCM's strategy contained very little inherent risk.

They were betting on financial anomalies, such as changing interest rates, that were thought to be little influenced by market swings. Even if the market _did_ take an unforeseen downward turn, profits wouldn't be greatly affected. At least that was the idea.

Institutions such as Dresdener Bank in Germany and Banco Garantia in Brazil gladly lent to LTCM, as they thought profits were guaranteed.

To give you just a small example of how much LTCM could raise through leveraging, consider that with only $1.25 billion in investment capital, they could borrow enough to invest about $20 billion.

As history will tell, banks made these investments because they simply didn't understand the risks.

LTCM had enormous control over investments and borrowed money, and could invest wherever they wanted.

The banks, on the other hand, had _no_ control over where their money was spent. At the time, this was fine. After all, LTCM were the "experts." Blinded by profit potential, they failed to examine and evaluate the true economic strength — or weakness — of the hedge fund.

As more and more banks were rushing in to lend money, the funds' leverage rate skyrocketed. It eventually reached 30 times the amount owned by the fund, leaving the company at enormous risk if things started to go wrong.

Now that you understand LTCM's essential strategy, the following blinks will demonstrate how they managed to become so successful.

### 4. LTCM brought academic knowledge to investment banking. 

Many people believe that there is a gap between the knowledge and opinions of high-minded academics and the conditions of the "real world."

This is no less true in finance. There is assumed to be a massive chasm between the professors and their theories and the real, dog-eat-dog world of trading.

LTCM believed that for them, things would be different. Their plan was to apply the expert knowledge and theories of academics to the real world.

So they recruited some of the biggest names in economics and trading, hiring people like 1997 winners of the Nobel Prize for Economics Myron S. Scholes and Robert C. Merton to sit on their board of directors.

The policy worked, and many investors were enticed into investing in the company due to its management by such luminaries.

Even universities and similar institutions were persuaded to invest in the fund. St. John's University alone put in _$10 million_.

One of the reasons people were so interested in investing was because the academics believed they had eliminated risk altogether.

Often, sly salespeople will downplay the risks involved in your investment. But not at LTCM. In fact, they were very up-front about the potential downsides.

They did this partly out of cockiness: they felt that with their team of academics they could calculate risks with such precision that the hazards would be easily manageable, or even be eliminated entirely.

Their arrogance was fuelled by incredibly complex mathematical formulas based on careful historical analysis of the market. By scrutinizing how the market had reacted to events in the past, they hoped to predict how it would react in the future. This, they thought, gave them the ability to recognize and circumnavigate risks and crises _before_ they occurred.

This academic approach was a major lure for investors, and contributed to LTCM's enormous success.

### 5. All hedge funds were growing in the 1990s, but LTCM outdid them all with their success. 

In the 1990s it was hip to invest in hedge funds. People saw them as new, exciting, and above all incredibly profitable financial products. Many wealthy individuals and companies wanted desperately to get a piece of the pie.

Yet even in this love affair with hedge funds in general, LTCM stood out, both in terms of popularity and borrowing.

During the mid-1990s LTCM was two-and-a-half times as large as the second largest mutual fund, and an incredible _four times_ the size of its closest hedge fund rival. They also controlled more assets even than huge investment banks, like Lehman Brothers and Morgan Stanley.

Banks were practically fighting to lend to them for a number of reasons.

For one, banks had a relatively large amount of lendable capital due to many years of advantageous market conditions.

What's more, the banking world desperately wanted to become a part of this money-making behemoth. They even offered LTCM loans for insignificant fees — the ones they used when lending to each other. For example, for every $100 borrowed, the fund was charged mere pennies. And they borrowed far more than $100.

Just imagine how much they must have been borrowing when you consider that despite the special terms, they were still paying $100 — $200 million a year in bank credits!

For all the love LTCM was getting, it looked much healthier than it actually was. Part of this was simple deception: although they reported their assets and liabilities quarterly to the banks and monthly to investors, these reports weren't always transparent, and often provided only generalized summaries of accounts.

Their success could not last forever, and soon the signs of danger became unmistakable.

### 6. LTCM’s models told them to take a risky strategy during the 1997 Asian crisis. 

Despite the popularity of the academic models used by LTCM, they had one fatal flaw: human error.

The models assumed that the financial system was a rational, predictable entity directed by rational, predictable people. But it's not. By our nature, humans are irrational and panic easily, a fact which caused enormous problems for LTCM.

These problems began in the 1997 Asian crisis, as the booming _Tiger economies,_ such as Indonesia and South Korea, took a downward turn. This posed a problem for LTCM in that it placed limitations on the places and products in which they could safely invest.

In times of uncertainty and insecurity, the normal thing to do would be to invest in bonds, which, although not very profitable, are very secure.

LTCM decided to walk a different path. Their models were telling them to _increase_ their share of a riskier financial product: equities. According to the models, times of crisis were great opportunities to make money.

So they gave the strategy a shot, and in doing so added more risk. They began investing in _paired shares_ — essentially different stocks in the same company. For example, they invested in Royal Dutch Petroleum and Shell Transport England, both of which comprised the international conglomerate Royal Dutch Shell.

Despite knowing very little about these products, they invested very heavily, as the models told them that everything would work out.

They had already seen a slight dip in profits once the crisis hit in the summer of 1997, but they didn't let that dissuade them. They continued to follow their models — which eventually led them off a cliff.

As you've seen, LTCM enjoyed a period of unprecedented success. But it wasn't built to last. Our final blinks will investigate the mistakes that ultimately led to LTCM's downfall.

> _"Now that they had the scale to operate worldwide, they had no interest in managing money for others and largely froze them out."_

### 7. The real downturn began when the models failed. 

The models that the academics and experts at LTCM developed were centered around the axiom that, if disrupted, markets will always revert to their natural position.

Think about it like a swing: you can push a swing and it will rise and fall until it eventually returns to its natural resting position. The same thing was _supposed_ to happen with markets, according to the models.

This was the reason that the fund chose risky strategies _during_ the Asian financial crisis. They saw the downturn as a small blip in the market which would eventually stabilize and earn them a whole lot of money in the process.

However, this didn't happen. The market didn't return to normal, and instead most people continued to act "irrationally," doing things like pulling out of shares and investing entirely in bonds, which are far more secure.

But LTCM's models told them not to follow this trend and to continue taking risks, so they did. And it cost them: after the Asian financial crisis, LTCM suffered several months' worth of losses for the first time in its history.

LTCM's sheer size actually made it more difficult for them to make money from their risky investments. As problems and losses started to mount up, the ridiculously high leverage rate of LTCM became a millstone: they had to follow the models and take risks in hope of earning enough to pay back their mounting fees and debts.

As time progressed, they became even more reliant on the possibility of the big return promised by the models. Reversing course was no longer an option. They had to keep pressing on.

Finally, the models failed completely and reality set in.

### 8. LTCM’s last weeks of self-sovereignty were marked by events that – according to LTCM – were almost impossible. 

According to LTCM's models, the probability of losing _everything_ in a single year was only one in a septillion (or ten to the power of 24). In other words, it was virtually impossible.

And yet, on 17 August 1998 the impossible happened: the Russian government defaulted on its debts and devalued its currency.

This sent shock waves through the market. It wasn't just that the Russian economy was tanking, but no one — not even the IMF — had stepped in to help them. In essence, they had been left to the wolves.

Investors realized then and there that they wouldn't always be saved. If the worst happened, they were on their own.

This realization initiated a mass exodus from the world's markets. People sought only the most secure bonds possible, and everything else was sold. This was terrible news for LTCM.

According to their calculations, the most they could lose in a single day was $35. Yet in reality, their losses actually stretched to $533 million.

Due to their huge debts and lack of capital, LTCM needed to sell quickly to stay solvent. But no one wanted what they had to offer. The few organizations and banks that _were_ interested in hedge funds were thus in a very strong bargaining position.

The fewer buyers there are in the market, the more severe the losses for the seller. By the end of August, LTCM had lost 45 percent of its capital, had reached a leverage rate of 55 times that capital, and were stuck with $125 billion in assets which they couldn't sell.

As the fund started to lose money, banks demanded that they open up their books to demonstrate that they could pay back their loans. When the banks discovered just how many risks the company had been running, they started to attack (or _short_ ) the fund in order to recuperate their losses.

Their last hope was to join forces with a bank in hopes of preventing total collapse.

> _"Long-term thinking is a luxury not always available to the highly leveraged; they may not survive that long."_

### 9. Finally, the fear of a major financial crisis following the collapse of LTCM led to the rescue of the fund. 

As LTCM started to topple, the banks, many of whom had recently acted against it, had a realization: "If this fund goes bankrupt, we lose our investments." And because so many of them had invested in the fund, a collapse would wreck the market.

Many banks and investors began looking into ways of taking control of LTCM and rescuing it. LTCM wasn't eager to let banks into the fund. They feared that if LTCM lost control, all its earnings were in danger. But as time passed, their position only worsened, and they were left with no other choice.

But who could even buy such a large fund? The sheer size of LTCM meant that no single bank could hope to rescue them without help.

The banks considered simply letting LTCM go under, so each bank absorbed their own losses. With this option, only one or two banks would face any serious risk, and the rest would have been able to survive.

The problem, however, was confidence: if they let yet another financial institution crash — and so soon after the Russian crisis — any confidence that was left in the market would be annihilated.

So the Federal Reserve, understanding the dangers of this situation, stepped in to help create a consortium of banks whose combined resources could deal with LTCM.

This consortium did succeed, but in their desperation to save the fund, they gave the bosses of the failing LTCM enormous negotiating power.

For example, despite the fact that the fund had failed, one of LTCM's founders, John Meriwether, managed to keep his assets (including his home, car, etc.) and avoid any personal debt. Within only a few years he was back again, this time with a new hedge fund.

Despite LTCM's early success, if you look at its overall performance you'll find that it lost 77 percent of its capital in a time when ordinary investors more than doubled their money.

> _"The government's emphasis should always be on prevention, not on active intervention."_

### 10. Final summary 

The key message in this book:

**As you'll see with the rise and fall of one of the United States' largest hedge funds in history, even the best models can't protect investors against the irrational behavior of their fellow human beings.**

**Suggested** **further** **reading:** ** _Liar's_** **_Poker_** **by Michael Lewis**

_Liar's_ _Poker_ tells the story of Salomon Brothers, a leader in the bond market in the 1980s. This tell-all account of the author's experiences at Salomon Brothers explains how the firm became one of the most profitable investment banks on Wall Street through its role in establishing the mortgage bond market, and what it did once it reached the top.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Roger Lowenstein

Roger Lowenstein is an American financial journalist and contributor to the _Wall Street Journal_. In addition to his many articles and book reviews, he has also written five best-selling books, including _The End of Wall Street_ and _While America Aged_.

