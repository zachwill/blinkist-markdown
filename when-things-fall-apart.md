---
id: 58b410eb8566cb0004c1f405
slug: when-things-fall-apart-en
published_date: 2017-03-01T00:00:00.000+00:00
author: Pema Chödrön
title: When Things Fall Apart
subtitle: Heart Advice For Difficult Times
main_color: C69B42
text_color: A17315
---

# When Things Fall Apart

_Heart Advice For Difficult Times_

**Pema Chödrön**

_When Things Fall Apart_ (1997) is a guide to dealing with the biggest challenges life throws at you. These blinks explore a range of concepts and strategies, from meditation to self-compassion to breathing techniques, that'll help you develop resilience in the face of adversity and a deeper appreciation for living in the moment.

---
### 1. What’s in it for me? Learn how to make lemonade when life gives you lemons. 

When life knocks you down, it can be hard to get back up. Anyone who has ever been through a breakup, faced constant rejection when looking for a job or watched a loved one struggle will understand how difficult it can be to get back to feeling positive again. Some of us never really escape our struggles.

But we can also learn to approach life in a way that allows us to accept any obstacles that confront us. This strategy allows us to stay strong in times of adversity and keep calm in times of danger. These blinks explain how anyone can train their mind and emotions to remain level and composed even when things fall apart.

In these blinks, you'll also learn

  * why loneliness can be cool;

  * why fear is nothing to be scared of; and

  * how we experience death every day.

### 2. Embracing your fears is your ticket to greater self-knowledge. 

Most of us don't enjoy being scared, which is only natural. But at certain stages in our lives, fear is something we should welcome.

By getting to know our fears rather than avoiding everything that frightens us, we can gain entirely new perspectives on our personality, relationships and past. Often, fear is our first reaction as we get closer to the truth behind a problem we're facing.

But, if you want answers, you need to understand your fear on a deeper level by taking time to reflect on it. Normally, when things fall apart in our lives, be it our health or our marriage, we tend to focus all our energy on resolving the situation, without spending enough time learning about the situation itself.

The first thing we need to do to change this is to realize that our lives are in constant flux; things fall into place, fall apart and come together again in unexpected ways. To appreciate this process and learn from it, you have to create space in your life to let things happen — you might find that positive solutions emerge from the most surprising places.

Consider the story of a family in tough times, held together by one son who financially supported his parents and siblings through his work. When he fell off a horse and was severely injured, the family was desperately afraid for their future — it felt like the end of the world for them.

But just two weeks later, the army came into their village to collect all able-bodied men to fight in a war. Due to his injuries, the son was allowed to stay home. He was subsequently able to find work again, and continued to care for his family. In the end, all the family's worries were needless, as the shifting nature of life had brought them an entirely new solution.

### 3. Loneliness is the perfect opportunity for self-observation and self-love. 

Loneliness, like fear, is also something we take great pains to avoid. And yet, solitude provides us with some of the best opportunities to relax, recuperate and recenter.

Every day, we juggle our work lives and social engagements, doing our best to improve ourselves and constantly aiming to achieve bigger and better things. But not many of us recognize that there's an alternative to this high-speed, high-stress lifestyle.

This alternative is called the _middle way_, an open state of mind that allows us to simply observe our problems for what they are. To find this middle way you need solitude, and you need to accept that solitude isn't negative.

So, when you wake up in the morning with pangs of loneliness in your chest, don't panic. Instead of worrying about what's wrong with you, relax into the feeling without judging yourself for experiencing it.

Once you start using the middle way to face your moments of loneliness, you'll begin to embrace them as opportunities for self-observation; in fact, you could even turn time alone into meditation sessions.

It's important to remember that meditation isn't always about improving yourself or training your brain. Instead, it's a time to let go of ideals, beliefs and norms to observe yourself as you really are.

Take a peek into your own mind and allow yourself to be surprised, amused or frightened by your thoughts when you meditate. By making this a daily habit, you'll be able to develop _maitri_ — a loving kindness and unconditional friendship with yourself.

### 4. Hope has a surprisingly detrimental impact on our lives. 

What is it that keeps us going when times get tough? Many of us think hope is what helps us cope with adversity, but the reality is that hope can make us fearful and anxious about the future, or lead us to disappointment.

The Tibetan language captures the relationship between hope and fear especially accurately. In Tibetan, the word for hope is _rewa_, while fear translates as _dopka._ The word _re-dok_ refers to the combination of hope and fear, a feeling whose duality captures our perpetual dissatisfaction with ourselves.

In _re-dok_, we're caught between hoping that we'll achieve greater things, and the fear of how our failure to do so might reflect on us.

Just think of the last time you were disappointed with yourself — perhaps a project you were proud of got rejected, or a relationship crumbled even though you cherished it. Did you spend time exploring where your feelings of embarrassment, dissatisfaction or shame stemmed from? Or did you simply wish you could be more like someone else, someone better?

By questioning our hopes and our fears, we can set ourselves free from constant dissatisfaction and disappointment. For example, when someone tells you that you're looking old and you feel offended, try to question why it's so important to you that you hope you look young. Questioning your hopes and fears in this way shows how unimportant they largely are.

Everyone has a unique set of hopes and corresponding fears that arise as a result of their environment and experiences in childhood and adulthood. But there's one universal fear that plays out in the background of all our lives: the fear of our own mortality.

Our fear of death keeps us from embracing death as a natural part of life itself. After all, we experience various forms or representations of death in our everyday lives: when the day ends, when we break up with someone, when we quit a job or even when we exhale — life is constantly presenting us with all kinds of endings.

By accepting these endings as part of the constantly changing flow of life, we can accept that nothing is permanent — not even our existence. In this way, death becomes nothing to be feared.

### 5. Celebrating impermanence, suffering and egolessness brings us closer to the meaning of life. 

Why do we exist? That's a question humans have been grappling with for centuries. While these blinks can't offer a definitive answer, we will explore three truths of existence to deepen our understanding of our time on this planet. These truths are _impermanence_, _suffering_ and _egolessness_, and getting to know these three can also make challenges in life seem a lot more bearable.

First, impermanence is the essence of life. Frightening as this may seem, it's in our best interests to celebrate impermanence, and we can do so by recognizing it during times of new beginnings. When someone is born, when you fall in love with someone new or even when you start the day full of energy, by embracing the idea that these beginnings come with their own unique endings, you'll become more mindful of impermanence and the way it shapes our lives.

Suffering is another inevitable part of our lives. As the saying goes, there's no pleasure without pain; likewise, there's no inspiration without wretchedness. Just like impermanence, suffering is something we should also celebrate, as it reminds us that we can't always get what we want, and helps us feel happier about our current state of affairs.

To embrace suffering as a necessary truth of existence, spend time observing how you react to painful situations, but without condemning your emotional response.

Say you're in the waiting room before surgery that'll leave you in pain for the coming weeks and all you can think about is running away — that's okay! Or perhaps you're feeling full of anger and shame because a friend embarrassed you. That's fine too. Observe your feelings and see what you can learn. Over time, your ability to face pain will improve.

Finally, by embracing egolessness, we can learn to feel at ease with our past and future, and thus learn to live in the moment. Though we often interpret a lack of ego as a lack of confidence, it's actually a sign of deeper happiness; by approaching every moment with curiosity, you'll break free from self-absorbed thinking. Rather than remaining fixated on our life story, egolessness helps us appreciate what's going on around us in the present.

### 6. Being compassionate toward others allows us to love ourselves more deeply. 

Spending time working on yourself can feel a little strange at first — perhaps even a bit egocentric! But, ultimately, it can help you become a more compassionate person.

Compassion doesn't just involve connecting with those less fortunate than us, but also with everyone around us _and_ ourselves. Practicing compassion toward others can make you more accepting of yourself at the same time.

This is what Zen teacher Roshi Bernard Glassman found when running his project for the homeless in Yonkers, New York City. As he explained to the author, Glassman found that building relationships with those that society has rejected is much the same as getting in touch with the parts of himself that he'd rejected for so long.

You can unlock the power of compassion by concentrating on the self as explained in the previous blink. This will help us connect with _bodhicitta_, the suffering of all beings. Practicing bodhichitta involves training our own minds for the sake of other living things.

Say, for instance, you witness a man beating his frightened dog. Like it or not, many of us might turn away because we can't bear to feel the pain that our compassion for the dog's suffering would cause. But our compassion isn't something to run away from — in fact, it can change lives.

But how?

By practicing _tonglen_, where we turn pain into joy through meditations on our own breathing. Start by thinking of someone who suffers. Hold them in your thoughts as you breathe in his or her pain. Then, as you exhale, breathe out the joy you'd like them to feel.

Many AIDS sufferers practice tonglen, breathing in for all those who have the disease too, and exhaling wellness, kindness and compassion. This gives them a sense of community, connection and purpose.

> _Bodhichitta_ is Sanskrit word for _noble_ or _awakened heart_

### 7. Meditation, breathing and a new perspective on the world can carry us through difficult times. 

The lessons we've learned in these blinks can easily be applied to everyday life. And yet, when a lover leaves us or a boss begins to shout us down, it's harder to put these techniques into practice. To stay on track, there are three ancient strategies you can use.

The first is called _no more struggle_. This is the practice of using meditation to recenter yourself at times when you feel powerless. Rather than struggling with your thoughts, embrace them and investigate them to find out more. What scares you most? What do you find repulsive? Observe yourself to find the answers to these tough questions.

The second strategy is _using poison as medicine_ — in other words, using times of suffering as a wake-up call. The three poisons are passion (or addiction), ignorance and aggression. If you feel one of these three rising up inside you, don't suppress or deny it; instead, use the tonglen technique. Breathe in the urge you're experiencing, even if it makes you feel embarrassed or ashamed. Then, breathe it out again with a feeling of creating space and freedom for yourself.

The final strategy is the _manifestation of awakened energy_, or the practice of recognizing that everything is alive and perfect the way it is. By witnessing our world this way, we cease trying to make ourselves look better or hide from problems that we can't ignore. Instead of looking for something purer, we learn to work with what we've got. Take the present moment for what it is, and it will become your teacher.

### 8. Final summary 

The key message in this book:

**By incorporating self-acceptance, calm reflection and a deeper appreciation for the present moment into your day-to-day life, you'll be better equipped to confront challenging times. Taking the time to learn about your fears, flaws and difficulties, while embracing even the most unpleasant parts of life, can also bring you closer to friends and family — and strangers too.**

Actionable advice:

**Accept the place where you are right now.**

If you've all but given up on meditation because of how challenging it is to stop your mind from returning to stressful thoughts, the _shamatha-vipashyana_ meditation technique can help you out. Focus your attention on your breath, and when you notice your mind wandering, simply label those distracted or worrisome thoughts as _thinking_, before gently returning to your breathing. By internally applying the label of _thinking_, you accept your wandering thoughts without judgment and embed self-compassion into your meditation practice.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Buddhism — Plain and Simple_** **by Steve Hagen**

_Buddhism Plain and Simple_ (2013) is your no-nonsense guide to essential Buddhist practices. From building awareness to living in the present moment, Buddhism's most important teachings are explained in a clear and accessible way, and are linked to aspects of everyday life where we need them the most.
---

### Pema Chödrön

Pema Chödrön is a renowned spiritual teacher in the Western world. She is also the author of several best-selling books including _The Wisdom of No Escape, Start Where You Are_ and _The Places That Scare You._

