---
id: 55a39ec03738610007250100
slug: when-to-rob-a-bank-en
published_date: 2015-07-17T00:00:00.000+00:00
author: Steven D. Levitt and Stephen J. Dubner
title: When to Rob a Bank
subtitle: ...and 131 more warped suggestions and well-intended rants.
main_color: FEF433
text_color: 807B19
---

# When to Rob a Bank

_...and 131 more warped suggestions and well-intended rants._

**Steven D. Levitt and Stephen J. Dubner**

_When To Rob a Bank_ (2015) presents a collection of articles published on the Freakonomics blog at freakonomics.com, which has now been going strong for ten years. Honing in on the unpredictable and downright strange, Levitt and Dubner cover everything from why you should avoid anyone whose middle name is Wayne to why some of us should be having more sex than others.

---
### 1. What’s in it for me? Check out the weird world of economics all around you. 

Why should we impose a tax on sex? How come the third chicken wing is sometimes more expensive than the first two? Economics comes into play in so many areas of life that it often goes totally unnoticed.

Since authors Steven D. Levitt and Stephen J. Dubner published the bestselling _Freakonomics_, they have continued to collect peculiar and strange economic and statistical facts. Some of these were published on their blog, where readers can send in economics-related oddities they've encountered themselves.

These blinks, which present highlights from the authors' anecdotes and insights, are a testament to the astonishing amount of nonsensical, strange and downright weird things in our world.

In these blinks, you'll find out

  * why some names spread like wildfire;

  * how driving a car is sometimes better for the environment than walking; and

  * why we should fear the people we know more than we fear strangers.

### 2. There’s more to a name than you think. 

Every year millions of new parents fret over what to name their bundle of joy. But, personal preference aside, names aren't all that important, right? Well, in the world of names, there are some strange statistics that indicate quite the opposite.

For one, you should steer clear of people whose middle name is Wayne. How do we know this? One freakonomics reader, M. R. Stewart, has an unusual hobby. She collects newspaper clippings about crime. This is not too wacky in itself, but all the clippings feature perpetrators who have one thing in common: their middle name is Wayne.

Author Stephen Dubner was shocked by the sheer number of clippings and strongly doubted whether anyone could assemble a list that long for any other middle name. As a result, he has now forbidden his daughters, even though they are currently only six years old, to date a boy with the middle name Wayne.

Another strange thing about names is how they catch on. Even the unlikeliest of names can spread like wildfire.

In 1999, there were eight children in the United States named Nevaeh. In 2005, this number rocketed to 4,457. What happened? The sudden name-craze originated in a single event: A 2000 MTV appearance by Christian rockstar Sonny Sandoval, of P.O.D, and his baby girl, Nevaeh, which is "Heaven" spelled backward. For baby girls, the name Nevaeh is now more popular than Sara.

But the peculiarities don't stop there; some names are uncannily fitting. Since the beginning of freakonomics.com, readers have shared some oddly apt names, like one Idaho man who was arrested for public masturbation after being spotted by a police officer in a public restroom. His surname? Limberhand.

Another reader, who had recently moved away from San Francisco, related how sad he was to leave behind his dentist — Dr. Les Plack.

### 3. The world of prices doesn’t make any sense. 

It doesn't take a rocket scientist to figure out the difference between something costing one dollar and something else costing 99 cents. And yet, paying one cent less somehow feels like a much better deal! When it comes to prices, all our common sense seems to fly right out the window.

We really need to watch out for this, though, especially when it comes to our health, as some retailers charge wildly different prices for the same medications.

Cyril Wolf, a Houston physician, told author Stephen Dubner that pharmacy chains like CVS, Eckerd and Walgreens sometimes put unreasonably high prices on generic drugs such as Ibuprofen. When Wolf investigated, he found something very odd indeed. Two chain stores, Costco and Sam's Club, sold the generic versions at a significantly lower price than the pharmacies mentioned above.

For instance, a bottle of Prozac pills — $117 at Walgreens — cost $12 at Costco. Why on Earth, then, would anyone fill their prescription at Walgreens? Wolf says that retirees with prescriptions don't compare prices at different stores, but simply assume that the price of generic drugs are consistent across all stores. So be sure to shop around!

Hunting for deals is one thing. But there are also "deals" out there that are complete nonsense.

Dropping by Harold's Chicken Shack one day, author Steven Levitt picked up on something strange. The two-wing meal, which includes fries and cole slaw, was listed at $3.03, and the three-wing meal cost $4.50. This means that one additional wing cost $1.47. That's more than the first two wings! We can sometimes be suckers for these "deals," but Levitt reckons this bizarre pricing was simply due to the fact that many businesses don't understand pricing.

If that wasn't disappointing enough, it's not just consumers and businesses that are sometimes clueless when it comes to pricing; the U.S. government keeps the one cent piece in circulation, even though the cost of producing one cent by far exceeds its value.

### 4. We fear all the wrong things. 

When the authors boldly stated in _Superfreakonomics_ that driving drunk for a mile is far less dangerous than walking drunk for the same distance, people balked at the notion. But we're worse at gauging risk than we think.

For instance, we now know that horseback riding is more dangerous than riding a motorcycle.

We all know motorcycling is pretty dangerous, so we wear helmets and other protective gear accordingly. However, one 1990 report from the Center for Disease Control and Prevention showed that the number of severe injuries for every hour of horseback riding is more than the number of injuries for every hour of riding a motorcycle. A lot of the horse-related injuries involved alcohol, but this didn't explain the statistic, because many of the motorcycle injuries did, too. Horseback riding is simply more dangerous than driving a motorcycle — a striking fact rarely mentioned by the media.

As well as taking extra precautions around horses, we should be warier of the people we know — more so than of the strangers our parents taught us to fear.

In 2008, church-goer Bruce Pardo put on a Santa costume and killed nine people, including his ex-wife and former in-laws during a Christmas gathering. Then there was the case of Atif Irfan, who was removed from an AirTran flight because his fellow passengers wrongly suspected him, a stranger, of being a terrorist. Their suspicions were unfounded. Together these cases support the findings that it's the people we know whom we should be wariest of, as opposed to those we don't know, like the people we sit next to on a plane.

And there are more facts to support this. First, the number of murder victims in the US who knew their killers is about three times higher than those murdered by strangers; 64 percent of all rape victims know their aggressor; 61 percent of female victims of aggravated assault know their attacker; and one 2007 _Slate_ article reported that of the 203,900 child abductions in one year, only 58,200 were perpetrated by strangers.

> _"When it comes to evaluating risks, people stink for all sorts of reasons — from cognitive bias to the media's emphasis on rare events."_

### 5. Lying – even when it’s totally puzzling – is also sometimes based in economics. 

What was the last lie you told? Perhaps it was an innocent white lie, just to save you or the other person some trouble. But sometimes we lie for strange reasons, and it often hurts us more than we think it will.

People lie even when telling the truth would be beneficial.

Data collected by César Martinelli and Susan W. Parker from the Mexican welfare program Oportunidades revealed some curious facts in this vein:

A number of applicants, in order to appear less wealthy and therefore eligible for the program, didn't report some of their assets. 83 percent stated they had no car when in fact they did, and 74 percent reported the same about satellite TV. This makes perfect sense, as possessing these things could hinder their eligibility for the program.

But here's where it takes an unexpected turn. A lot of the applicants actually _overreported_ basic facilities: with 39 percent claiming to have a toilet, 32 percent saying they had tap water and 29 percent saying that they owned a gas stove, when they had none of these things. Martinelli and Parker put this down to embarrassment; the applicants, who are often poor, would rather risk not receiving welfare than admit they don't own a toilet.

Conversely, some lies do make sense when we can profit from them.

Every year, a bestselling memoir like Margaret Seltzer's _Love and Consequences_, is revealed to have been partially fabricated. So wouldn't it make more sense to market such books as novels? Not really. If you're an editor with a memoir in the works that you expect to be around 90 percent fact, it makes sense to publish it as a memoir. This is because alleged real-life stories generate more hype and media coverage and generally catch our attention more than novels do.

It's not surprising, then, that some memoirs are more fictional than fiction.

### 6. We’re doing all the wrong things to save the environment. 

Climate change is one of the most pressing issues we face today. But how do we address it, especially when we're unwittingly worsening the situation by doing things we think will help?

Driving, for example, is sometimes far less harmful to the environment than we think. Renowned environmentalist Chris Goodall calculated that if you walk 1.5 miles, and then replenish the calories lost from your walk with a glass of milk, the impact you have on the environment would be the same as if you drove 1.5 miles in your car and opted out of that cup of milk.

But why?

Well, the amount of greenhouse gas emissions it takes to produce that glass of milk roughly equals the emissions of a car driven 1.5 miles. There's the carbon dioxide emitted from the milk-delivery trucks, and the gases emitted from the cows themselves, and in the end it's more environmentally friendly if a few people carpool and go without their respective glasses of milk.

Aside from misunderstandings about our driving habits, we also tend to believe that growing our own food helps the environment more than, say, eating less meat.

Having your own veggie patch has become quite a trend in the US; it gets you in touch with nature and teaches you how to care for plants. We think, "Great! No need for a delivery truck for my dinner!" But is it really the best way to preserve the environment?

According to Christoph L. Weber and H. Scott Matthews of Carnegie Mellon, 83 percent of greenhouse gas emissions come from food production; a mere 11 percent of emissions come from transporting food.

What actually _would_ help would be choosing vegetables over meat products just one day per week, as meat requires a far greater amount of energy to produce. This would lessen the impact of greenhouse gas emissions more than your veggie patch!

### 7. The world of criminals and law enforcement is strangely nonsensical. 

What's the best day to rob a bank? According to one very successful New Jersey bank robber, the answer is Thursday. But that's not the only weird (and perhaps unsettling) piece of advice in the world of crime.

Something a little more helpful to know is that criminals should never be reminded that they're criminals.

Priming is a psychological phenomenon that psychologists use to describe the unconscious influence of stimuli on our behavior, and its effects are demonstrated by the following study:

A group of researchers conducted an experiment where prisoners had to flip coins and then report how many "heads" they had gotten. But before flipping the coins, half the group were asked the question, "What were you convicted for?" They were thus primed with the idea of criminality. The other half were asked the more neutral question, "How many hours per week do you watch television?"

By being reminded of their convictions, the first group lied and said they got heads more often than the group who were asked about television. In fact, they reported they got heads six percent more frequently than the other group. But we can use this knowledge for good; correctional facilities could actually employ such priming methods in a positive way with criminals.

Here's another surprising fact about crime: The best way to catch a criminal immigrating to the US is to ask them if they're a criminal. And we already do this by using a form.

Anyone wanting to become a US citizen must fill out the Immigration and Naturalization Service's Form N-400. This form includes the question "Have you ever committed a crime for which you were not arrested?"

It might sound crazy to include this question — after all, who would admit to it? — but there's a good reason behind it. If US law enforcement can prove that an answer is false, it can be used to prosecute or deport the person in question.

### 8. The economics of sex is a strange affair. 

Do you think you should be having more sex? Well, we needn't get that personal, but you might be surprised to know that how often you have sex may contribute to the common good, and furthermore, it could be connected to economics.

It's actually true that some of us should have more sex — but not for the reasons you might think!

In _More Sex is Safer Sex_, University of Rochester economics professor Steven Landsburg posits that careful, STD-free people should be urged to have more casual sex. It makes sense for the common good for healthy people to do so, as opposed to reckless people, who are more likely to contract and spread STDs. Careless people should be discouraged from having sex, because every time they do they increase the chances of infecting all those who become sexually involved with them. Landsburg compares this to pollution. Most of us think it's a good idea for factories that pollute to sacrifice something, like their profits, for the common good. So why should sex be different?

A tax on sex would also make sense.

Considering how much we spend annually on healthcare associated with sex — think unwanted pregnancies and STDs; considering the lost productivity that sex represents; and considering how strapped for cash the government is, it's clear that we should set up a sex tax. Those who engage in sexual activity that spreads disease and costs society money would be highly taxed; practitioners of safe sex might get a tax break.

For example, committed couples having sanctioned sex could perhaps receive a tax credit.

Some of the authors' suggestions on what to call such a tax include "Extracurricular Intercourse and Lesser Sex Act Tax," or the "Family Creation Tax!"

But this idea is not totally new. In 1971, Democratic legislator Bernard Gladstone proposed such a tax, claiming it would be "the one tax that would probably be overpaid." Unfortunately it was deemed to be in poor taste and was consequently rejected.

### 9. There's economics in everything, but we think about it wrong. 

If you're a freakonomist, you see the economy all around you. You also might know that money and economic trends affect us in ways contrary to what we've traditionally been taught.

For example, we're told to avoid bribery. But bribery is sometimes surprisingly useful — especially with our children and their schoolwork.

Motivating people with money is rarely questioned. Without the promise of a paycheck, who would show up at McDonald's every day to flip burgers? But when it comes to school children, we're usually averse to monetary incentives.

So, with a few colleagues, author Stephen Dubner conducted some experiments to determine whether financial incentives could improve students' efforts in school.

Right before they took a test, some students were offered twenty dollars to improve their score and another group of students weren't given anything. Giving the money to the first group prior to the test made the incentive stronger because it was so close to the encouraged action.

The students who were given the cash bribe were told that they had to give it back if they didn't improve their results. As a result, it was the bribed students who performed better than students who entered the test empty-handed.

Finally, patterns in consumption are often not what we think they are.

For example, why did shrimp consumption _triple_ in the US between 1980 and 2005? It depends on who you ask. Psychologists will often focus on fluctuations in customer demand caused by people becoming health-conscious or the effect of new advertising for shrimp. Economists, though, would probably look at changes in supply. For example, how better nets improve product availability for suppliers and reduce the final price.

But microeconomics students — or anyone new to economics — would probably view the situation more like the psychologists than the economists. This is because the majority of us have more experience as consumers than producers, so we concentrate on customer demand. It's only when you're a full-fledged economist that you start to see economy in everything around you.

### 10. Final summary 

The key message in this book:

**Economics can be found in the most remarkable places and examining the economy sheds some light on the motivations behind our thoughts, emotions and actions.**

**Suggested further reading:** ** _Freakonomics_** **by Steven D. Levitt and Stephen J. Dubner**

_Freakonomics_ applies rational economic analysis to everyday situations, from online dating to buying a house. The book reveals why the way we make decisions is often irrational, why conventional wisdom is frequently wrong, and how and why we are incentivized to do what we do.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Steven D. Levitt and Stephen J. Dubner

Steven D. Levitt is Professor of Economics at the University of Chicago. In 2004, he received the John Bates Clark medal for the most influential American economist under forty and, in 2006, was featured as one of _Time_ magazine's "100 People Who Shape Our World."

During the 1990s, journalist Stephen J. Dubner worked for _The New York Times Magazine_. He is now an award-winning author, best known for the co-writing he has done with Steven D. Levitt.

