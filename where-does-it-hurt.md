---
id: 5489dbbf33633700094c0000
slug: where-does-it-hurt-en
published_date: 2014-12-18T00:00:00.000+00:00
author: Jonathan Bush and Stephen Baker
title: Where Does it Hurt?
subtitle: An Entrepreneur's Guide to Fixing Health Care
main_color: 788CA5
text_color: 3C608C
---

# Where Does it Hurt?

_An Entrepreneur's Guide to Fixing Health Care_

**Jonathan Bush and Stephen Baker**

America's health care system is failing, and a revolution is needed. Hospital visits are a messy experience, and people don't have fast and easy access to the medical services they need. In _Where Does It Hurt?_, Jonathan Bush argues that we can fix our broken health care industry by making it more open. Entrepreneurs need the space to provide alternative services, so hospitals will be forced to better themselves to catch up.

---
### 1. What’s in it for me? Find out how entrepreneurship can save the health care system. 

America's health care system has regressed since the Second World War. It's changed from something that made us proud to something embarrassing. We loathe hospitals and doctor visits. It's hard to get appointments, they make us wait too long, and then we end up with exorbitant bills and a referral to see someone else.

Why does it seem like nobody ever thinks of the actual patients/customers? Why can't the whole system be simpler and more straightforward?

In _Where Does It Hurt?_, Jonathan Bush explains how entrepreneurship can save the day. Customers and patients need people with great ideas to give them more choices than massive, monopolistic hospitals, whose day in the sun is over.

After reading these blinks, you'll have learned

  * why LASIK surgery should be the model for the whole health care system;

  * why investors don't invest as much in digital health care start-ups; and

  * how simple it is to find a niche for your own health care business.

### 2. The health care industry is broken because there’s no competition. 

Big research hospitals function almost like department stores: you go there for all your medical needs. So why aren't there any "boutique" hospitals to compete with this model?

Everyone knows that hospital visits are frustrating. Parking is difficult, service is usually bad, medical records go missing, waiting takes hours and bills are unbelievably high. Each of those problems means a market opportunity for a competitor to offer something better, at a lower cost.

This happened when LASIK eye correction surgery was introduced in 1991. It was expensive and considered very risky, so it wasn't covered by most insurance plans. People had to pay if they wanted it.

So LASIK operators had to provide the best possible service at affordable prices in order to woo customers. This enabled the industry to advance — LASIK's process now has a 95 percent satisfaction rate. This should be a model for the whole health care system: when there's competition, customers win.

The health care industry would be improved by more openness. We need more choices. People should be able to decide for themselves how much their insurance covers, instead of being forced to accept an all-inclusive plan that includes services they won't use.

What if you had the option of being covered only for big accidents, or curable serious illnesses? And for other services like dentists, you could choose the best providers?

Entrepreneurs also need the freedom to develop ways of providing higher quality health care. They need more access to medical information, for instance, which is often guarded by hospitals.

If people had more chances to choose smaller, entrepreneurial medical care like LASIK, they could avoid the problems associated with bigger hospitals.

This, in turn, would push hospitals to improve their services and hold onto their patients (or "customers").

> _"It's by returning to a real marketplace that we can create the health industry we want."_

### 3. The health care industry would be better if hospitals were run like businesses. 

So how can this sort of openness improve our current system?

We need a whole network of hospitals, regional clinics, medical practitioners and other health service providers. Other businesses can benefit from the health care industry's shortcomings by offering alternatives.

Traditional businesses can compete with hospitals. The author's company, Athenahealth, is an example of this. Athenahealth takes on giant hospitals by offering affordable information services that hospitals won't provide. This makes their services more accessible for customers.

Imagine if we had a whole network of businesses like this. It would be a nightmare for big hospitals.

In a market economy, prices would be more transparent and shoppers would have more choices. Hospitals would find it much harder to draw business from people who aren't gravely ill. Some hospitals would disappear; others would specialize.

If you needed a CT scan and it cost $500 at a world-class hospital, but $150 at a new imaging center in the mall, which would you choose? If someone can shop around, they'll find better alternatives.

This is already happening in some places. There's a rapidly growing new generation of community hospitals, health-oriented start-ups and retailers.

In 2010, Cerberus Capital, a private-equity firm in Massachusetts, bought a chain of six Catholic community hospitals and turned them into a for-profit business called Steward Health. Steward Health has since grown to 11 hospitals, illustrating how successful the model can be.

Retailers like Walgreens are also beginning to provide medical services. They already know what consumers want, and they have access to their loyalty data. CVS has around 800 "Minute Clinics," where shoppers can drop in to get basic medical services without an appointment.

We need more diversity in health care options like this, so patients can choose what's most appropriate for their needs and budget. When there are better services available, hospitals will have to catch up.

### 4. We need more primary health care in our lives. 

One reason hospitals are so unpleasant and crowded is that they're full of people who don't need to be there. How can we prevent this?

We need more _primary care_ in our lives. "Primary care" is the basic care you get from a doctor you see regularly. Your primary doctor tends to every aspect of your health, and refers you to a specialist when necessary.

The better your primary doctor knows you, the less specialists have to establish using their expensive equipment. Many people, however, go to specialists too early — they don't visit their primary doctors enough.

Rushika Fernandopulle, a leader in health care reform, met a woman who was taking _27_ kinds of medicine prescribed by _11_ different specialists. Four of the prescriptions were the same, and each specialist focused on only one part of her health.

The woman insisted it wasn't possible to take all 27, so she'd dip her hand into her medicine jar and ask God to guide her to choose the correct five. This could've been very dangerous if she'd selected four of the same kind.

More primary care would prevent situations like this. We need to be healthier across the board, instead of focusing on problem areas. In other words, we need a kind of _wraparound_ care.

Fernandopulle's work with Boeing also illustrates the necessity of wraparound care. Fernandopulle found that many Boeing employees were chronically ill, and wasting the equivalent of what the company would spend at the Ritz on hospitals.

He coached them to treat their illnesses themselves, by doing things like improving their diet and monitoring their blood sugar. This made them healthier _and_ reduced the cost for Boeing.

If primary care becomes a bigger part of our lives, we won't just respond to any illness we get — we'll be healthier in general.

> _"Each of us, on average, spends a little more than half a day per year in a hospital. Yet we pour nearly a third of our health care dollars into inpatient care — five times as much as primary care._

### 5. New, private health care services can bring on the market disruption we need. 

So what's the role of technology in all this? Can it help us improve the health care system?

Big hospitals have massive budgets, so they can afford pretty much all the highly priced equipment they need. The market of opportunity for entrepreneurs is the "middle class" of the health care market: the companies and small health practitioners with budgets in the tens or hundreds of thousands.

If you can provide that middle class with something they need and can afford, you'll get a foothold in the industry. This kind of disruption in the market could lead to the health care revolution we need.

In general, the health care industry moves slowly, and it's resistant to changes — including technological change. In fact, only $1.4 billion was invested in health-oriented digital start-ups in 2012, compared to _$2.7 trillion_ in other digital start-ups. How can we overcome this?

Firstly, people need more access to affordable, simple apps or services that assist in health care or hospital visits — services like Beyond Lucid.

Beyond Lucid allows medics and ambulance crews to send data and electronic records straight to doctors at the ER, so they can be ready for the patient with the appropriate team and technology. It's a rather basic electronic communication link, but hospitals don't have it yet!

RegisterPatient.com is a similar service. It lets people sign up for appointments online, and fill out their forms for registration or prescription. It's very simple, but it makes a big difference to the customers, who won't have to spend so much time waiting at the hospital.

Beyond Lucid and RegisterPatient.com illustrate how health care technology can improve our current situation. Right now, however, it needs a way into the market.

### 6. We need easy access to our medical records. 

How many people know their blood pressure level? Or the results of a medical test they had months ago? Medical information is vitally important, but it's agonizingly complicated to access it from hospitals.

We need our data ready at our fingertips. Patients need easy access to their medical records.

Thanks to technology, health care data could be produced in much greater amounts. People will browse their test results on their phones, and link it to data from their jogging routines, diets, vitamin intake or sleep patterns.

Medical data will simply be another part of our lives, and having easier access to it will have a huge effect on the industry.

First, medical practices will run their businesses more effectively. They'll be more able to optimize their resources, run smoothly and get paid when they can find information quickly.

More data will also help doctors, and allow medical researchers to examine society as a whole. They'll be able to study how people are eating and exercising, or which medicines are working for which groups of people.

All in all, it will be much easier to get the right medicine, therapy and health care advice to each individual person.

An interesting feature of this new medical data is that it might be produced primarily _outside_ medical establishments. This is why companies like Google, Apple and Intel are getting involved.

If we had more data to bring to our annual checkups, we wouldn't have to rely on our memory for the details of our health. Doctors would be better equipped to serve us.

Intel, for instance, now offers a health surveillance service for the elderly. It can record their pathways through the house, the strength of their voices, their sleeping and bathroom patterns. This data is stored on computers, but soon it'll be able to alert people to early signs of disease.

> _"Health data is going to be part of our lives."_

### 7. Final summary 

The key message in this book:

**Medical patients, as customers, need more choices. Big hospitals charge exorbitant bills and provide sub-par service, but we're forced to use them. Entrepreneurs need the space to provide us with better alternatives. More private health care services could bring on the health care revolution we need.**

Actionable advice:

**Visit your primary doctor often.**

Don't wait until you have a serious problem to seek out medical care, and don't go straight to a specialist before seeing your regular doctor. The better your primary doctor knows your health, the more time and money you'll save by avoiding unnecessary hospitals trips.

**Suggested further reading: _Cracked_** **by James Davies**

_Cracked_ gives a clear and detailed overview of the current crisis in psychiatric science: malfunctioning scientific standards and the powerful influence of pharmaceutical companies have caused the overdiagnosis and overmedication of people all over the world.
---

### Jonathan Bush and Stephen Baker

Jonathan Bush is the CEO and cofounder of Athenahealth, one of the most successful medical communications software programs in the United States. Stephen Baker is an author, and a former senior writer for _Businessweek_.

