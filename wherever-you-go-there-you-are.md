---
id: 5678651d0bac9b000700000b
slug: wherever-you-go-there-you-are-en
published_date: 2015-12-23T00:00:00.000+00:00
author: Jon Kabat-Zinn
title: Wherever You Go, There You Are
subtitle: Mindfulness Meditation In Everyday Life
main_color: 104E2F
text_color: 104E2F
---

# Wherever You Go, There You Are

_Mindfulness Meditation In Everyday Life_

**Jon Kabat-Zinn**

_Wherever You Go, There You Are_ (1994) explains how to fully enjoy the present moment without worrying about the past or the future. By providing step-by-step meditation practices, both formal and informal, that can easily be incorporated into everyday life, Kabat-Zinn steers us toward the peace and tranquility that we're yearning for.

---
### 1. What’s in it for me? Experience life to the fullest. 

How often have you found yourself in a perfect situation — on holiday, say, in a wonderful city, enjoying the sights; or sunbathing on a beautiful beach; or walking through a picturesque forest. Wherever it is, it's exactly where you want to be.

But then it hits you: you're not all there. Instead, you're thinking about the laundry that needs to be done or that incomplete work assignment. 

Our minds do this: they wander and take us out of the moment. But what if the moment is exactly where we want to be? How can we stay there?

The answer is mindfulness. This has become a very voguish word over the course of the last decade, but how exactly does it work and how does one actually practice it? 

Well, it isn't really a goal to be achieved, but something that needs to be practiced, requiring constant work and focus. In these blinks, you'll learn the fundamentals of, as well as some more advanced techniques for, practicing mindfulness. 

In these blinks, you'll discover

  * how the Dalai Lama has used mindfulness in dealing with China;

  * why trees are great partners in meditation; and

  * how generosity is one way to practice mindfulness.

### 2. Mindfulness means conscious living and appreciation of the present moment. 

Mindfulness has become a bit of a buzzword in recent times. But what, exactly, is it? You have perhaps heard that mindfulness is an ancient Buddhist practice. In fact, mindfulness is for everyone, and you needn't forswear your earthly possessions and become a Buddhist to enjoy its rewards.

Mindfulness is about overriding our automatic approach to life. From a Buddhist perspective, our ordinary state of consciousness is quite limited: we often do things unconsciously, without being fully present in the moment.

The simple fact that we're always _doing something_, while thoughts race through our heads without cease, leaves very little room for us to just _be_. So, to truly embrace the present, we need to systematically observe who we are and examine our view of the world. This, essentially, is mindfulness.

So why be mindful? Well, mindfulness is a tool that helps us realize the richness and possibility of our own growth and transformation. Cultivating it reunites us with aspects of ourselves that we often overlook, opening us to new ways of existing both in our own skin and in the world.

For example, it can lead us to richer experiences of joy, peacefulness and happiness, and also to better understandings of difficult emotions, like grief, sadness and fear — that is, those emotions of which we're often unaware or only express unconsciously.

Because mindfulness is so illuminating in this way, it's also empowering. As we become more mindful, we become more aware of who we are, which unleashes our creativity and intelligence and gives us clarity.

The cultivation of mindfulness and meditation are often confused with things like relaxation, stress relief or self-development. However, mindfulness isn't about aiming for a particular feeling, nor is it about getting somewhere or becoming a particular kind of person. Rather, it's about emptying the mind, becoming still and allowing ourselves to realize who and where we _already_ are.

Your practice starts with the next blink.

### 3. Mindfulness is cultivated by being in the moment and concentrating on one thing at a time. 

Developing mindfulness is as easy as incorporating some practices into your daily routine. Let's take a look at some of them.

First, rather than _doing_ all the time, begin shifting yourself into _being mode_. Here's how: either sit or lie down and, once in that position, think of yourself as timeless. Observe the present moment without trying to change anything. Engage your senses and focus on what you see, hear, feel or on whatever is happening for you. Try to completely embrace and accept this moment.

If you catch your mind straying during the exercise, gently coax your attention back to your breath. Focus on the sensation of your breath moving in and out of your body without getting your mind too involved or trying to change anything. This exercise will enrich the moments of your normal _active mode_, influencing the decisions you make and providing guidance. 

Along with accepting the moment, concentration and _voluntary simplicity_ are other practices that will be valuable allies on your path to mindfulness.

Concentration is the cornerstone of mindfulness. When you're fully concentrated, your energy is directed toward deeply experiencing one thing or one moment. Everything else falls away, including other thoughts, feelings and the outer world. People often deeply appreciate this feeling, as it allows them to experience inner stillness and an undisturbed peace.

You can also try voluntary simplicity — that is, engaging in one activity, or with one thought, at a time. If you're playing with your daughter, for instance, and you get a text, you could deliberately ignore your phone, instead bringing your full attention to enjoying the game with your child.

It's important not to confuse non-doing with doing nothing. Consciously stopping, with the intention of cultivating stillness and appreciation, is non-doing — but it is not doing nothing!

### 4. Patience and generosity can help you become more mindful. 

You know those times where you're waiting for a friend who's running late and your annoyance starts to turn into anger? Have you ever paused and thought about how pointless that feeling is? Moving beyond feelings like this is a cornerstone of mindfulness. Choosing to be patient would indeed be far better for your own sanity.

Patience and mindfulness are deeply connected. Patience means that you accept things as they are, that you realize events always unfold in their own good time.

That may be well and good, but how do you manage your impatience and the anger that accompanies it? To build patience, compassion and wisdom, start by redirecting and working with anger and impatience.

Remarkably, the Dalai Lama seems not to be angry with the Chinese, whose government committed acts of genocide against the Tibetan people. He understands that even if they have taken everything from the Tibetan people, he won't allow the Chinese to take his mind, too. Instead of putting his energy toward feelings of anger, he uses it to promote understanding and patience.

The next time you start getting impatient or angry, remember that wisdom is found in patience and that what comes next greatly depends on what you do in this moment.

Another quality that sets a strong foundation for mindfulness is generosity. This needn't be manifested in the giving of material goods; you can also be generous with your enthusiasm and trust, with your presence and compassion. As you begin to share and give, you'll discover the abundance of strength and energy you have at your disposal.

When you give, don't get caught up worrying about giving too much, or fretting about your generosity going unappreciated, or thinking that you won't get anything out of it yourself.

Practice this by granting yourself the gifts of self-acceptance and a little time every day.

> _"What lies behind us and what lies before us are tiny matters compared to what lies within us." — Oliver Wendell Holmes_

### 5. Formal meditation involves taking time to be still and focusing on your body and breath. 

Mindfulness can be practiced both formally and informally. Let's take a look at some formal methods first. Formal meditation entails reserving specific periods of time for the development of mindfulness and concentration. All other activities are put on hold.

When sitting down to meditate, your posture is extremely important. There is no one correct sitting position, but you should sit in a way that displays poise. For most people this means sitting up straight, without being too stiff, and with your shoulders and face relaxed; your head, neck and back should be aligned.

You should also position your hands and feet appropriately to direct the flow of your energy. Try paying attention to your varying energy levels and the hand positions that accompany those levels. There is no universal definition of any particular position; it's more about what the positions represent to you. For example, sitting with your palms on your knees may mean that you're simply accepting what is, whereas having your palms facing up might mean being open to a higher energy.

The length of time you commit to formal meditation is up to you. Just be sure to come out of your meditation mindfully. As a general guideline, the author found 45 minutes per day to be a good standard practice; that was enough time to achieve stillness, sustain attention and also experience deep relaxation.

During your meditation, you will have an impulse to end it at some point. When this arises, recognize first where it's coming from. Is it fatigue, impatience, discomfort, or is it just time to stop? When you realize where it comes from, stay with the feeling for a bit, breathe with it and then slowly come out of meditation.

Remember: there is no right or wrong meditation practice. So use the above steps as a guideline. The most effective approach is to focus on what feels right to you.

### 6. Informal meditation can be practiced when sitting, standing, walking or lying down. 

Formal meditation is often what comes to mind when we think of meditation. But it's not the only way to meditate.

If you find the idea of sitting still for an extended period of time too challenging, or if you want to complement your formal meditation practice, you can try a walking or standing meditation instead.

The purpose of a walking meditation isn't to try to get anywhere. So you might choose to walk in a circle or back and forth.

The key is to bring your attention to every movement your body makes: each footstep and how you raise, move and place your feet. If you do a standing meditation, try doing it around trees, as trees are great teachers of stillness and calm. You may also want to close your eyes, stand still and imagine your feet are roots in the ground. Sway your body as trees sway in the breeze. Listen to your surroundings and feel the presence of nature.

In addition to walking or standing meditations, lying down meditations are also good practices in mindfulness. 

As you lie on the floor, consciously release your muscles. This will help your mind to open and release any pressing thoughts. Next, imagine that you're sinking into the floor and bring your focus to your body. You might choose to focus on your body as a whole, as if each skin cell were breathing and radiating energy as one, or on each part of your body separately. The latter is known as a _body scan._

During your scan, focus on different parts of your body and then release them. Move systematically through your entire body, in any direction you like — from head to toe or from side to side. Use your breathing, steering it inward, into each part of your body, as if you were breathing into your toes or knees, for example, when you inhale. As you exhale, release the muscles, allowing that part to rest in stillness.

### 7. Practice mindfulness by questioning yourself during automatic routines. 

Being mindful isn't something you accomplish and then (check!) you're set for life. It requires constant self-inquiry.

But asking yourself questions isn't only about problem-solving. It's about staying connected with life itself, with yourself and with your presence. It means carrying questions with you, contemplating them, being constantly aware of them. 

Even though mulling questions over in your mind will result in many answer-like thoughts, the main goal is to listen to the thoughts that your questions evoke. Whether your question is "What is upsetting me right now?" or "What is my purpose in life?", it will help to imagine yourself as an audience, watching and listening to your own thoughts and feelings.

Thankfully, no special settings or occasions are needed to practice mindfulness. You can do it any time. That said, many people find that early morning is ideal. The peacefulness and solitude that is often more easily available at this time gives you the space to contemplate and focus on being. An added bonus is that you'll start your day in a peaceful, mindful manner and therefore be more likely to carry this peace with you through the day.

But of course, carving out time before you start your day isn't essential; you can even practice mindfulness in the middle of an everyday activity like climbing the stairs. Usually, we do this in a hurry, without a single thought about it. But if you focus on all the intricate movements your body makes as you go up the stairs, you'll increase your awareness of the present and, when you get to the top, you'll be calmer and more connected to the next activity you engage in.

The key is to slow down, taking one step at the time, with the goal of increasing presence and awareness. There is no place you need to be and no moment that you need to sacrifice for the sake of being fully present in this one.

### 8. Your thinking mind and your selfing are some difficulties you’ll encounter when practicing mindfulness. 

By now you have a good picture of what practicing mindfulness involves. Let's now look at some of the most common stumbling blocks — and how to get past them.

It probably comes as no surprise that, as mindfulness is all about the mind, your thoughts and ego are the biggest obstacles you'll face. For example, when you experience a significant moment during meditation, you might start congratulating yourself on doing so well. Be wary of such feelings, however, as they may be your ego wanting to take credit for being special, or giving you a false sense of having "made it" to a higher level.

If this happens and you feel like the _I_ or _me_ is taking over, try asking yourself where exactly you're supposed to get to, or if you're using meditation as a tool to get to a particular place. Remember that meditation is not about arriving at a particular destination. It's about understanding and appreciating the present moment to the fullest.

Another hurdle you may encounter is _the self_ or _selfing_. Selfing is the tendency to keep the mind and the attention on _the self_, therefore making every moment _my_ moment and every experience _my_ experience. This can branch out into all aspects of our lives, such as _my_ child, _my_ opinion, _my_ knowledge and so on.

However, when we realize that everything is interdependent, we discover that there is no isolated, independent _me_. The only _me_ that exists is me in relation to all other forces and events in the world. For example: me in terms of my relationship to a child of whom I happen to be the parent.

That being said, mindfulness isn't about ridding ourselves of our selfing tendency. It's about balancing it, mitigating its impact, seeing things as they are, understanding that everything is connected and constantly changing and, eventually, learning to take things less personally.

> _"The true value of a human being is determined primarily by the measure and sense in which he has attained liberation from the self." — Albert Einstein_

### 9. Final summary 

The key message in this book:

**By being constantly on the move and cramming a crazy amount of activities into our daily schedules, we're losing our ability to fully enjoy the present moment. Our minds are constantly focused on the past or the future, and we're forgetting that the only time we really have is now. Fully appreciating the present is a matter of pausing and bringing our attention to it.**

Actionable advice:

**Practice loving-kindness meditation.**

Help others and yourself by using and extending your heart-centered presence. First, center yourself and, from your heart or your belly, invite feelings of kindness, love and acceptance into your body. Let these feelings grow until they fill your whole being. Once you feel them radiating within you, you can stay in the moment and relish it, or direct these feelings outward to wherever or whoever you please — be it your family or even people you don't know. Picture them, honor them and wish them love and acceptance.

**Suggested further reading:** ** _The Untethered Soul_** **by Michael A. Singer**

_The Untethered Soul_ is all about you: your feelings, thoughts and consciousness. By drawing on different spiritual practices, this book explains how you can navigate your own mind, get in touch with yourself and become your own master, to ultimately achieve enlightenment.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Jon Kabat-Zinn

Jon Kabat-Zinn is the founding director of both the Stress Reduction Clinic and, at the University of Massachusetts, the Center for Mindfulness in Medicine, Health Care and Society. He's studied under renowned Buddhist teachers (Thich Nhat Hanh among them) and leads workshops on stress reduction and mindfulness. He's the author of multiple books, including _Full Catastrophe Living_ and _Everyday Blessings_.

