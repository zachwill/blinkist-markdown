---
id: 5aa46652b238e100071c82e0
slug: whiplash-en
published_date: 2018-03-14T00:00:00.000+00:00
author: Joi Ito and Jeff Howe
title: Whiplash
subtitle: How to Survive Our Faster Future
main_color: 524525
text_color: 524525
---

# Whiplash

_How to Survive Our Faster Future_

**Joi Ito and Jeff Howe**

_Whiplash_ (2016) explains the new rules of our fast-changing world. The current moment is defined by emergent technologies and innovative ideas, and the only way to stay afloat is to adapt. Forget the principles of yesterday and start developing strategies that work today.

---
### 1. What’s in it for me? Learn how to stay innovative in today’s dynamic work environment. 

A generation ago, staying afloat in the world of work wasn't too complicated. If you managed to land a job you liked and did your work satisfactorily, chances were that you could count on keeping it until retirement. This is no longer the case.

Today, more and more things are becoming decentralized — information, corporations, even currencies. And with decentralization comes unpredictability; holding down a job for life is nearly impossible.

So what are you supposed to do in this new environment where the old rules no longer apply? That's what these blinks are here to help you with. Part roadmap to today's changeable workplace terrain and part handbook for how to navigate it, these blinks will equip you with the tools you need to stay flexible and innovative.

You'll also find out

  * how nylon is a product of disobedience;

  * why it's better to be a reed than an oak; and

  * why science is far from infallible.

### 2. Paradigms can prevent the recognition of great innovations. 

If you throw a baseball up in the air, it's going to come down, right? Or what if you fly north in an airplane with limitless fuel? Well, you'll eventually come back to where you took off.

So why do these answers seem so commonsensical?

Well, it's because they chime with our current _paradigm_ — that is, the worldview that underlies our theories about how things should function. Part of that paradigm is gravity, which causes airborne balls to return to earth and prevents airplanes from floating off into space. But, as the philosopher of science Thomas Kuhn points out, paradigms can shift.

Paradigm shifts entirely revolutionize commonly held beliefs. And since people will defend their beliefs at almost any cost, these shifts are usually strongly resisted. Even most scientists will defend current paradigms.

For instance, before the Copernican Revolution in the sixteenth century, it was believed that the sun orbited the earth, and many scientists continued to believe this for decades, despite clear evidence to the contrary.

But nonscientific paradigms exist as well, and are often just as ingrained. Each age comes with a different set of social assumptions and theories. According to the French philosopher Michel Foucault, people have certain systems of norms and beliefs that dictate their actions. These systems change over time, and every generation has its own. As in science, they usually don't change without vigorous resistance.

The unquestioning acceptance of current paradigms, plus a resistance to paradigm shifts, has prevented the acknowledgment of history's greatest innovations.

Take Louis and Auguste Lumière, the brothers who developed the first "living photographs" — later known as motion pictures — in 1895.

Audiences were initially captivated by the moving images. But it only took a few years for the novelty to wear off, prompting one of the brothers to declare that "the cinema is an invention without any future." Consequently, the brothers opted to focus on color photography.

So why could neither the brothers nor the audience see the true potential of this new medium? Well, they were committed to an old paradigm, according to which photos were only capable of telling the story of a single moment, not spinning longer, complex tales. It didn't even occur to them to transform pictures into the modern "film."

### 3. The world we live in is constantly changing and increasingly unpredictable. 

Do you ever feel like time is moving faster and faster, like the last year zipped by much more quickly than the year before it? Well, you might be onto something; a quick glance at earth's history shows that the world is, in fact, changing at an unprecedented rate.

To make this clearer, let's imagine the history of earth as a single calendar year, with the formation of our planet occurring on January 1. It wasn't until December 1 that the first land animals appeared. And it took until 11:50 p.m. on December 31 for our hominid ancestors to start walking upright. On New Year's Eve, less than a second before midnight, recorded history begins.

Now let's reset the scale, and have January 1 be when hominids started walking upright. On this new calendar, mid-December marks the development of the first languages. On the morning of December 31, industrialization began, and later that same day, at around 07:00 p.m., the world population had grown to 3 billion.

In the last few hours of human history, there have been countless life-changing innovations. We can now 3D-print prosthetic limbs and communicate instantaneously from anywhere on earth. It is a time of constant change — but this change goes hand in hand with unpredictability.

Take climate change. For centuries, a society's success depended on its ability to make accurate predictions about the weather. In the Middle Ages, for example, merchants who knew that harvests would be poor after an impending drought could, in dry times, stockpile wheat and sell it later at an inflated price.

Now, our predictive abilities are being derailed by the climate change caused by rapid development and industrialization.

Today, it's getting harder for us to predict where destructive weather events will occur, or which regions will experience increases in temperature. In the future, unpredictable weather will only be more common, and reliable forecasts will become a thing of the past.

> _"Today, the biggest threats to the status quo come from the smallest of places, from start-ups and rogues, breakaways and indie labs."_

### 4. With information now freely available, authorities have become less important. 

It used to be that authorities ruled the roost. From royal families to spiritual leaders and politicians to major corporations, the people with the power called the shots. But this is about to change, and all because of the unprecedented access to information that we have today.

Prior to the advent of the internet, the public had limited access to information — and this access was granted or denied by the people in power. By and large, the public accepted the information given to them; they obeyed orders and rarely challenged the official narrative. Today, however, the internet has made information available to virtually everyone, and the veracity of official claims can easily be scrutinized and challenged.

Wikipedia perfectly embodies this new availability. It's made it possible to look up anything — from the Napoleonic Wars to the history of Nigeria — in a matter of seconds and at no cost.

Other platforms provide further educational resources. On edX.org, one can access lectures from a variety of international universities, and there are countless tutorial videos on YouTube, allowing you to brush up on almost any subject, from statistics to art history.

The internet, and the free flow of information it encourages, are also fueling a phenomenon known as _emergence_. Emergence occurs when a system or organism is created whose whole is superior to the sum of its parts.

Wikipedia is a perfect example. No single Wiki editor could have created such a comprehensive online resource. Even a select team of academics wouldn't be able to do it in a lifetime. What's more, in Wikipedia's case, emergence trumped authority. No one buys expensive encyclopedias anymore; rather, they turn to this autonomous community of information-loving writers who share what they know for free.

Or consider the Arab Spring, which brought down autocratic governments by establishing social movements whose power far surpassed that of any small activist group. These movements took shape as people discussed and shared ideas on various online platforms, creating a massive uprising that would have been hard for one person to instigate.

Emergence is even affecting the world of business. Crowdfunding has made it remarkably easy for people without personal wealth to transform fledgling ideas into successful businesses.

### 5. You can react flexibly to unexpected events by using a pull strategy. 

Do you think you could predict which of the world's ten most active volcanoes will erupt next? Sounds impossible, right? Well, it is. Nonetheless, many organizations still saddle a handful of their top people with a comparable responsibility: predicting which events (be it a natural disaster or a shift in the market) are most likely to occur, and then allocating resources accordingly.

This method — putting resources where top people think they are most likely to be needed — is called the _push_ approach. But this method is inflexible and sluggish. All feedback from lower-level employees must first move up through the tiers of management before it can be considered and acted upon. And only then can someone at the top address the issue.

This drawn-out rigmarole can spell doom for a company that needs to react nimbly to an unexpected event.

Just look at how Tokyo Electric Power Company and the Japanese government reacted to the Fukushima earthquake in 2016, which caused a leak at a nuclear power plant. No one at the top knew exactly how much radioactive material had leaked, or where exactly the leak was, and no equipment or technicians could be sent to deal with the problem until this information traveled up the chain of command.

A _pull_ strategy allows for considerably more flexible solutions.

With this approach, resources and expertise are requested right when they're needed, and requests are made by decision makers who are on-site and can react to whatever is happening on the ground. For this to work, there must also be a free flow of information between departments.

After the Fukushima earthquake, Japanese officials, hobbled by their push approach to management, did a poor job of reorganizing resources, and the information they released on radiation levels was ambiguous and imprecise. So, a group of volunteers rallied online and, armed with self-made Geiger counters, set out to collect concrete radiation data. This information was later made public online, free of cost.

### 6. Disobedience drives innovation. 

Did you have a favorite activity as a child? Whatever it was, let's imagine you liked to draw. What would have happened if, while at school, you refused to set aside your crayons when the bell rang for class? You likely would have been disciplined in some way. Now let's imagine you'd been allowed to draw and had gone on to become the next Picasso . . .

The point is that rules can suffocate possibility. If everyone went about their normative days in predictably normative ways, innovation would cease entirely. That's why breaking the rules is sometimes better than following them.

Today, companies need to be innovative. The alternative is to founder in the constant change and competition of the business world. All innovative companies are fueled by creative employees — and there's nothing that kills creativity quite like a set of inflexible rules.

Remember Thomas Kuhn, the philosopher of science who coined the term "paradigm shift"? Well, according to him, the people who question and dismantle old paradigms always have one thing in common: they pay no heed to what others tell them to do.

Indeed, we have disobedience to thank for many great inventions.

Have you ever heard of Wallace Hume Carothers? Back in 1926, he started studying polymers — complex molecules used to make fabrics — when working in DuPont's first research department. In 1930, however, a new director took over the department and Carothers was told to focus solely on commercial uses for existing fabrics.

But he disobeyed. He continued his work with synthetic polymers and, in 1935, he invented nylon, a groundbreaking fabric that, by 1941, had seized 30 percent of the US pantyhose market.

And disobedience isn't only a source of innovation; it's inspiration as well.

The term _positive deviants_, coined in the 1970s, describes people who disobey the rules and, in so doing, improve not only their own lives but the lives of those who follow their example.

Some corporations, upon discovering that one of their employees is a positive deviant, have been able to harness this innovative rule-breaking and develop it into a change program that helps other employees foster their own creativity.

> _"Nobody has ever won a Nobel Prize by doing what they're told, or even by following someone else's blueprints."_

### 7. When faced with flexible enemies, you have to be more resilient than strong. 

Which plant do you think would stand a better chance of weathering every storm: an oak or a reed? Most people would choose that symbol of strength, the oak. But even an oak can be broken by a strong enough wind, whereas the reed, supple and slim, is certain to bounce back after the storm.

Modern companies should do away with oak-like tendencies and try to be more flexible, like the reed.

The organizations of yore did their utmost to build corporate muscle and then rely on that strength. And it worked. If processes and stocks and plans were strong enough, no turning of the tide could bring them down.

But this has all changed. Today's work environment is not what it was, in part because the people who want to bring businesses down are much wilier and more flexible.

For instance, in 2010, malware was detected in Iranian nuclear facilities. The code tampered with the facilities' centrifuges, causing them to require early replacement, and then covered up the damage by sending false information to the control system.

Here's where an old approach failed to forestall a modern problem: the system was thought to be safe because it wasn't connected to the internet. Since data could only enter and exit the facility via USB sticks, there was little cybersecurity guarding the system. The facility was thought to be strong and safe — but it wasn't.

It's much better for a company to be resilient than strong. In the face of failure, flexibility is crucial.

For instance, when starting out, many of today's companies have little money and few permanent employees. By keeping investments to a minimum, they can learn from mistakes and, if necessary, start over if they fold.

Just take YouTube, which began as a video-dating platform called "Tune In Hook Up." It wasn't a success. But that didn't matter because the founders identified a market demand: people needed an easy and fast way to share videos. And so, in 2005, they launched YouTube. One year later, they sold it to Google for $1.7 billion.

> _"There is no Fort Knox in a digital age. Everything that can be hacked will, at some point, be hacked."_

### 8. Final summary 

The key message in this book:

**Everyone can learn to adapt to today's dynamic and unpredictable work environment. Just remember that old paradigms can blind us to modern innovations, that the rules should sometimes be disobeyed and that flexibility and resilience trump strength and authority. It's time to forget the old rules and start forging a set of your own.**

Actionable advice:

**Favor practice over theory.**

Here's another business survival trick: embrace practice and let theory take a back seat. In fields like software development, the costs of innovation are so low that just making something is often cheaper than developing an elaborate plan. If it doesn't pan out, you can simply start over and try again. It's been reported that Google lets employees spend 20 percent of their work time experimenting on personal projects, a freedom that's led to, among other innovations, the creation of Gmail.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Algorithms to Live By_** **by Brian Christian & Tom Griffiths**

_Algorithms to Live By_ (2016) is a practical and useful guide that shows how algorithms have much more to do with day-to-day life than you might think. And not just that; they can also lead to a better life by helping you solve problems, make decisions and get more things done.
---

### Joi Ito and Jeff Howe

Joichi Ito, a technology expert and entrepreneur, is the director of the Massachusetts Institute of Technology's Media Laboratory. A visiting professor at Harvard Law School, he also serves on the boards of PureTech Health and the New York Times Company.

Jeff Howe is an assistant professor at Northeastern University. In 2006, he coined the term "crowdfunding" in an article for _Wired_ magazine. His writing has also appeared in _TIME_ magazine, the _New York Times_ and the _Washington Post_.

