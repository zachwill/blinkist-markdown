---
id: 58dcbe7f3ab4a600041c22b2
slug: white-trash-en
published_date: 2017-03-31T00:00:00.000+00:00
author: Nancy Isenberg
title: White Trash
subtitle: The 400-Year Untold History of Class in America
main_color: AB8648
text_color: 524525
---

# White Trash

_The 400-Year Untold History of Class in America_

**Nancy Isenberg**

_White Trash_ (2016) retells American history from the perspective of the poor whites who were by turns despised and admired by the upper classes. These blinks trace the biopolitical, cultural and social ideas that have shaped the lives of white trash Americans from early colonial days to the Civil War, through the Great Depression and up to the present day.

---
### 1. What’s in it for me? Delve into the story of white trash America. 

In the aftermath of the 2016 US presidential election, the voting behavior of white people in America garnered a lot of attention. Some commentators argued that they had voted against their own interests; some that they had voted against minorities, and some disgruntled Democrats even talked about a "whitelash" or a "white trash vote." It would be easy to dismiss these people as simply sore losers, but maybe there was something more to it than that?

This is the history of the so-called white trash of America, a social phenomenon that didn't arrive with the first stereotypical trailer park home, but has been engrained in the fabric of American society from the very beginning. It's the story of how poor white America has been a thread running through the history of the United States, and how that story gives us some insight into where we are today.

In these blinks, you'll find out

  * why Benjamin Franklin thought that Western migration could resolve class conflicts;

  * how two legendary Americans identified with their era's white trash; and

  * why the Great Depression changed the country's historic obsession with genetics.

### 2. American society has been shaped by social stratification since the beginning. 

Rome wasn't built in a day, and neither was the United States. The nation's early history laid the foundations for its future class-based society.

In the 1600s, America presented England's ruling class with a rare opportunity to get rid of the poor and profit from it. The vast, undeveloped and unoccupied landmass of North America seemed free for the taking — to the English, the "uncivilized" natives didn't count as the country's custodians.

So America became a dumping ground for England's criminals, orphans and poor, who were then tasked with building up the first English colonies in America. Before long, the class inequalities embedded in English society were reproduced and intensified in the colonies.

Take Virginia, the first Southern colony, for instance. Its economy was driven by tobacco, a crop that demands large tracts of land that only the rich could afford, and vast numbers of laborers. Upon arrival, poor immigrants were forced to work in the tobacco fields for a period of time to pay off their passage to America.

The class system was also entrenched in the Northern colonies. For example, although New England society contained a thriving religious community, its principles were far from egalitarian. Puritan doctrine established a stiff social hierarchy with poor outsiders — both black slaves and white servants — at the bottom.

Gender inequality was also rife in the American colonies. In the minds of early Americans, the ideas of fertility and "good breeding" were key to a prosperous and healthy nation. The importance of fertility led to women being seen in the same terms as land: both required able-bodied men to take responsibility for them if the economy and the population were to grow.

And, in order to protect good breeding, inter-class marriage was frowned upon in early American society. Those at the top feared that allowing the upper and lower classes to reproduce together would lead to a decline in their superior pedigree.

If this was the face of American society in its nascent decades, what did it look like during the time of independence? We'll explore this in the next blink, along with the vision that the founding fathers had for America's future.

### 3. The egalitarian visions of the founding fathers were rooted in classist thought. 

Even visionary thinkers like the founding fathers of the United States are children of their times. Despite their progressive ideas for the future of American society, they couldn't shake the classism of the early colonies.

Benjamin Franklin was a strong believer in an equal society. Nevertheless, he saw advantages in social stratification. Franklin argued that a wider dispersal of people across the land would successfully reduce class conflict by ensuring that each person had some property sufficiently far away from others to prevent fights over resources.

On the surface, this model seems egalitarian. However, Franklin didn't consider the unfree laborers in his vision for a property-owning society. In fact, his idea was designed to protect the elites from those at the bottom. He thought that creating a large middle class would help provide social stability, as those in the middle would be happy with the fact that they were above the laborers of society's lower echelons.

President Thomas Jefferson also proclaimed to be egalitarian. Unfortunately, his background as a member of the Virginian elite blinded him to the real struggles of the lower classes. Rather than understanding the class barriers that prevented them from succeeding and rising through society, he chalked up their problems to differences in land quality and geography.

His attempts to improve the lot of the lower classes also fell short. For example, one of Jefferson's early attempts at reform focused on helping the poor climb the ladder by granting 50 acres to each man without land of his own. However, this reform never came to fruition; it was dropped from the final version of the State Constitution.

Jefferson also considered success to be the result of genetic rather than social conditions. For example, he imagined that, over time, breeding and natural selection would eventually generate a class of talented people who could rise to the top. In other words, Jefferson believed that sexual reproduction would lead to an "accidental aristocracy" of talent.

> _"The circumstance of superior beauty is thought worthy of attention in the propagation of our horses, dogs, and other domestic animals; why not that of man?" — Thomas Jefferson_

### 4. The poor whites who led westward expansion represented both the best and worst of American culture. 

As nineteenth-century pioneers began to settle beyond the Western frontier, the theories of the founding fathers were put to the test. Large swathes of land lay waiting to be exploited, and poor white men were eager to create a better life for themselves.

Unfortunately, the millions of poor Americans who migrated to what we today call Kentucky, Tennessee, Arkansas and Missouri, remained poor. They didn't acquire property rights for land, as Jefferson had envisioned. Instead, they were forced to squat disputed properties illegally.

Why did these pioneers fail to prosper?

In the West, a complete lack of essential infrastructure made trade difficult to organize. Settlers were better off living self-sufficiently, growing their own produce. They typically lived in small nomadic dwellings, often in proximity to Native Americans, and always beyond the reach of civil society.

The middle and upper classes regarded these squatters as vulgar, dangerous and out of control. At best, they were known for their crude habitations and sprawling families. At worst, rumors were spread that they spat indoors and fornicated in the streets.

But gradually, squatters came to be admired figures in American society. In fact, their positive traits would form the foundations of what became known as "the American spirit."

Some thought of squatters as the Adams of the great American wilderness, as natural philosophers and free spirits who shunned wealth and fame. Squatter backwoodsmen were thought of as honest, hardworking and the embodiment of the American love of adventure. Andrew Jackson, the first Westerner to be elected US president, and Congressman "Davy" Crockett, wore their "squatter identities" on their sleeve, which made them both highly controversial and wildly popular.

### 5. The Civil War saw a clash between opposing narratives about poor whites. 

Over time, social structures in the North and South began to diverge. In the South, slavery was the foundation of social order, while the North prided itself on a society of free men and women.

Many Northerners objected to slavery because it kept white laborers poor and marginalized as slavery supplied unpaid labor, which in turn kept white laborers' salaries down. Yet, the elites in the South defended the presence of poor whites as evidence not of slavery's unfairness, but of their own natural superiority.

They argued that the upper classes in the South were the descendants of the English aristocratic _Cavaliers,_ while the poor whites at the bottom were the descendants of the servants and convicts who came to the American colonies.

When the American Civil War erupted, the Southern Confederacy and the Northern Union attacked each other's stance on class and race. The Confederacy defended the practice of slavery as a natural division of labor and stabilizing force in society. They accused the North of undermining this order by assigning low-skilled jobs to both blacks and whites.

The idea of the Cavaliers was expanded to encompass all Southerners, rich and poor, in an attempt to unite people against the Northerners, who were now the "mudsills" of society's lowest tiers.

But the Confederacy continued to struggle with class tensions. Poor white Southerners worked in appalling conditions to support the upper class. As slaves performed most manual labor jobs, marginalized white people struggled to find work. The poor white class revolted, raging against food shortages. Desertion rates in the army were high.

Leaders of the Union, on the other hand, wore their "mudsill" badge with pride and encouraged soldiers to do the same. Their fighters were convinced of their egalitarian mission, which kept morale high.

### 6. After the Civil War, discrimination against the poor reemerged under the guise of eugenics. 

The close of the Civil War did not mark the end of classism and racism in America. Instead, they were disguised under a new ideology: _eugenics._

Eugenicist ideas revolved around the notion of "improving" the genetic quality of the human population. Such ideas found fertile ground in American society, with its historical fixation on lineage and "good breeding."

Several influential individuals picked up the same thread and began to argue that the American "breed" could be improved by calculated sexual reproduction. This was considered a legitimate form of social engineering.

President Theodore Roosevelt, for instance, was a vocal eugenicist. During his presidency in the early twentieth century, Roosevelt promoted the idea that it was the duty of women to raise generations of healthy and disciplined children. He recommended that Anglo-American women from good backgrounds should have up to six children for the sake of expanding the American race.

By viewing "good breeding" through the scientific lens of evolutionary biology, Americans found a convenient way to normalize structural classism and racism. From IQ tests that merely reflected pre-existing literacy gaps, to medical research on diseases that were actually caused by malnutrition, such as hookworm and pellagra, questionable evidence was used to support discriminatory policies. Marriage between different races and classes often led to the ostracization of couples, and young, lower-class white women were sometimes sterilized.

The eugenics movement had a powerful impact on several facets of civil law. Up until 1931, 27 states upheld sterilization laws, which detailed the many categories of people that were to be sterilized. Academics, scientists, doctors, journalists and legislators hopped on the eugenics bandwagon to draw analogies between human and animal breeding. Die-hard eugenicists even believed that the individual did not have the right to marry or reproduce according to their own free will.

> Charles Darwin's works on natural selection and Francis Galton's work on the distribution of intelligence helped spur the eugenics movement.

### 7. Social reforms and new research following the Great Depression challenged the narrative that the white poor were genetically inferior. 

America marched into the twentieth century with an economic boom that created scores of new jobs. But by the 1930s, the nation's economy had collapsed. The Great Depression that followed left millions of workers unemployed, creating a spiral of downward mobility that brought middle-class Americans down to white trash status.

In 1932, 20 percent of the American workforce was jobless. Many who still had their jobs lived in fear of the same fate. Caravan shanty towns inhabited by migrating farmers who had lost their land began to crop up along California highways. The situation was bad and eventually led to large-scale government intervention.

President Roosevelt kicked off a series of new policies to bring the Depression to a halt. In doing so, he made it clear that the social ills suffered by the lower classes could be improved with reform.

Public agencies such as the _Resettlement Administration_ were established to help the poor and unemployed pick themselves up again. For the first time, the psychological burden of poverty was taken into account. By learning to overcome feelings of inadequacy, poor people had a better chance of starting their lives afresh.

Local development agencies such as the Tennessee Valley Authority helped to recover eroded land, build dams and new towns, allowing entire regions to get back on their feet. The success of these agencies demonstrated that the struggles of the lower classes could be lessened if the state invested in solutions.

Soon enough, scientific evidence came to light that proved poor people couldn't be blamed for their poverty. The sociologist H. Odum led the way with his research, which ultimately revealed that the prejudice of the wealthy against "white trash" was driven by their desire to blame their own problems on the poor. Odum's studies revealed that underdevelopment in the South could be reversed if the region's poor were supplied with the resources, training and opportunities they needed to live productive, stable lives.

### 8. White trash culture became a central part of mainstream American pop culture. 

In the latter half of the twentieth century, America fell in love with a hillbilly from Tupelo, Mississippi, named Elvis Presley. He was born into the world in a simple "shotgun shack," a tiny narrow house synonymous with the poor Americans of the South.

His nationwide popularity reflected a shift in attitudes toward "white trash."

For the first time, this pejorative label could be worn with pride, as the nation came to recognize the positive cultural tropes of white trash. This change was tied to the rise of identity politics in the 1950s and 1960s, which seemed to replace class distinctions as people of certain ethnic and social backgrounds began to band together.

At the same time, frustration with white middle-class suburban blandness drove many Americans to search for authenticity. They began reaching for redneck culture, which was in turn commodified and consumed with astonishing success. The NASCAR racing competition is a prime example of this. Born out of the redneck bootlegger's need for fast cars to escape police, the race became a national sensation by wearing its outlaw past with pride.

Unfortunately, not all commercial portrayals of white trash culture were positive. Countless TV shows from _The Beverly Hillbillies_ to _The Andy Griffith Show_ made the white poor laughable. The rise in identity politics went hand-in-hand with conservative bashing of the welfare system, which in Republican eyes bred weak people.

An unsympathetic middle class blamed white trash for myriad social ills, including inner-city violence, economic stagnation and loose sexual morals. The "hillbilly ghettos" of poor white Southerners who'd moved north to cities such as Baltimore, Chicago and Cincinnati were used as examples of the negative impact of the white poor.

From the 1990s onward, Southern white trash culture found itself at the center of attention in America, capturing historical class tensions and new social anxieties. For this generation, Bill Clinton was the epitome of the poor white American. With his rags-to-riches story, trailer-trash roots, sexual misdeeds and Elvis allure, he infuriated his conservative opponents.

### 9. Final summary 

**America has been a classist society since its early colonial days. The white poor endured the discriminatory biological determinism and deep-seated prejudice of the upper classes, but they also gained an important cultural role. From the Western squatter to the Northern "mudsill" of the Civil War, to the trailer trash of the late twentieth century, the white lower classes captured the hearts of the nation as an embodiment of the American spirit.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Death Of The Liberal Class_** **by Chris Hedges**

_Death of the Liberal Class_ is a serious indictment of modern liberalism and today's liberal leaders. It offers a scathing critique of the failures of contemporary liberal institutions while still providing a glimmer of hope for the future of American democracy.
---

### Nancy Isenberg

Nancy Isenberg is a professor of history at Louisiana State University. She is the author of several award-winning books on American history and the founding fathers. She's also a regular contributor to Salon.com, where she reflects on contemporary political and cultural affairs from a historical perspective.

