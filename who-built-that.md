---
id: 562e37353864310007610000
slug: who-built-that-en
published_date: 2015-10-30T00:00:00.000+00:00
author: Michelle Malkin
title: Who Built That
subtitle: Awe-Inspiring Stories of American Tinkerpreneurs
main_color: DE2C42
text_color: DE2C42
---

# Who Built That

_Awe-Inspiring Stories of American Tinkerpreneurs_

**Michelle Malkin**

_Who Built That_ (2015) tells the stories of America's most fascinating inventors from the last 150 years. These blinks reveal how these tinkerpreneurs, against all the odds, achieved commercial success with their revolutionary inventions.

---
### 1. What’s in it for me? Meet the inventors and entrepreneurs who built the United States. 

The foundation of the US is an entrepreneurial spirit. From the many clever inventions of Benjamin Franklin and the industrial entrepreneurs of the nineteenth century to modern day IT giants like Apple and Microsoft, the United States has always been at the forefront of entrepreneurship and innovation. 

What made this possible? One thing: free market capitalism. In the United States, anyone with a vision and a willingness to work hard can capitalize on his idea and create a better society, without government and bureaucracy getting in the way. 

This is changing apace, however, and the United States is beginning to lose its position as the world leader of innovation. It is not the country it once was, and unless we take action, the United States will be outdistanced by its competitors. Luckily, as these blinks will show you, there are ways to stop that from happening. 

In these blinks, you'll learn

  * why Obama is dead wrong about inventors;

  * why the United States needs to change back to its old patent system; and

  * why glass blowers used to be thrown in prison.

### 2. Tinkerpreneurs are the driving force of innovation. 

Have you ever heard of tinkerpreneurs? The term may be unfamiliar, but it's more than likely that their ideas and work are present in several aspects of your life. Inventors that are part-tinkerers, part-entrepreneurs, tinkerpreneurs have chosen to take their inventions out of the laboratory and bring them into the commercial world to create companies, jobs and profit. 

According to President Barack Obama, inventors are nothing special. They aren't smarter than the rest of us, nor do they work harder. Public efforts such as schools, roads and bridges are all that underlie their success.

But nothing could be further from the truth.

In contrast to what Obama thinks, tinkerpreneurs are in fact engaged, clever and determined. They are the ones who create jobs and build companies, not merely the people who work those jobs or for those companies. 

Tinkerpreneurs take full advantage of the opportunities provided by the United States to invent _and_ invest. As makers and creators, tinkerpreneurs constitute the top one percent. They enable both progress and growth. And yet, theirs is a profession fraught with challenges.

> _"I'll show you how just a small handful of tinkerpreneurs profoundly revolutionized and improved every aspect of our lives."_

### 3. For tinkerpreneurs, inventions are opportunities for commercial success. 

When people become inventors, what are they aspiring to? A place in history? Money? Progress for society? For tinkerpreneurs, all three of these dreams are vital, because, for them, inventing is about doing business. And business means being _opportunistic._

One tinkerpreneur that exemplifies this is Tony Maglica. Born in Croatia, this aspiring inventor came to the United States with hardly anything to his name. He took up and excelled in a position as a contractor, working harder, faster and longer than the others. This earned him a reputation for skill and integrity. 

During a job for a flashlight manufacturer, Maglica saw his opportunity; in response to the poor quality of existing flashlights, he created the first Maglite — a revolutionary flashlight with a bright and focusable light. 

And Maglica didn't stop there. He strove constantly to improve and expand the product. His hard work and opportunistic attitude led Maglite to become a multi-billion dollar company.

But innovation also comes from looking at ideas from a different perspective. Many products wouldn't exist today without a change in perspective that turned an invention into a commercial product. Consider air conditioning, for example. 

Inventor Willis Carrier was responsible for inventing a new approach to cooling systems. Rather than cooling with ice, which caused problems due to the resulting humidity, Carrier used _air_ in his cooling systems. He improved his products steadily, and was doing fairly well. But without Irvine Lyle, Carrier's invention wouldn't have had the astonishing impact that it did. 

Lyle was the creative salesman who imagined growing commercial applications for Carrier's work. These allowed factories as well as cinemas to operate in the summer heat, which resulted in a new phenomenon: summer blockbusters. Air conditioning even changed where and when people worked, allowing people to move to hotter states, such as Texas. 

Air conditioning is one example of innovation succeeding brilliantly. It doesn't always unfold that way, however. Innovation involves several risks, especially on the part of the tinkerpreneur.

> _"Government doesn't innovate. People like me do. Government doesn't create jobs. We do." - Tony Maglica_

### 4. Tinkerpreneurs often face critics and naysayers along the road to success. 

The world is full of party poopers, and if you're a tinkerpreneur, you have to be prepared to fight them. The story of the Roebling family is a good example of how true tinkerpreneurs stick to their guns.

John Roebling, who would go on to patent a revolutionary wire rope, came from Prussia. There, he had first-hand experience with how time-consuming and wasteful the rope-making process was. He also noticed that no procedures could be improved without government approval. The Prussian government favored conformity over creativity, so Roebling moved to the United States, where he patented and promoted his invention with great success.

But soon, he met another obstacle. Instead of a rigid government, it was the hemp rope industry that felt threatened by his success. Fortunately, the detractors were no match for Roebling's compelling invention. 

His ropes were used in the Niagara Falls Suspension Bridge, the world's first railway bridge of its kind. But then, Roebling's ropes were put to the biggest test yet: the support of the Brooklyn Bridge. Unfortunately, John Roebling was killed in an accident during construction, and the responsibility of fighting off naysayers, big businesses, technophobes and political power brokers now lay with Roebling's son, Washington.

Washington worked hard and risked a lot to defend his wire rope against its critics. In fact, he overworked himself, getting decompression sickness in the process. However, his wife Emily saw the project through to completion, ultimately showing all naysayers just how wrong they were. The Roeblings fought for their ideas, and it's good they did too! Otherwise, we'd never have iconic landmarks such as the Brooklyn Bridge.

### 5. Small ideas can change the world, too. 

Sometimes the innovations with the biggest impacts are the result of the simplest solutions. Take the humble crown cap, a little invention that made it possible to seal bottles safely, hygienically and cheaply. 

William Painter, inventor of the crown cap, also faced critics and naysayers. To prove that his crown cap really did keep soda fresh, he shipped bottles closed with the cap to South America and back — a trip of 40 days — and celebrated their return with a tasting. Everyone agreed that the soda tasted as if it had been freshly bottled. Disposable and incredibly cost-effective, his invention became the industry standard, and the beverage industry was revolutionized. 

But William Painter's influence did not stop there. He had some of the most creative minds working for him — among them a man named King Gillette. Gillette shared Painter's passion for invention, which sparked a deep and lasting friendship. Painter's innovations even inspired Gillette to develop another radically simple invention: a steel razor that, just like the crown cap, was disposable.

Gillette's razors were patented in 1904. A mere five years later, they has become a household item worldwide. And he didn't stop there. In 1910 he brought the first razor designed for women to market. Painter and Gillette's relationship is just one example of the innovation that is brought about when tinkerpreneurs surround themselves with creative minds and gifted salespeople.

### 6. Collaboration is the best catalyst for innovation. 

As we've seen, invention is seldom a solitary job. Two heads are almost always better than one. Just consider Edward Libbey and Michael Owens, the team responsible for changing the way we produce glass. 

Owens was an engineer; Libbey promoted and defended property rights. In an industry as cut-throat and secretive as the glass industry, Owens and Libbey wouldn't have had success if they hadn't had each other. 

Guilds and labor unions fiercely guarded the secrets of glass production. In Venice, in the Middle Ages, glass blowers were often imprisoned on the island of Murano to ensure that their secrets didn't leak. Owens and Libbey were determined to break through these protective forces and develop a more efficient and practical way of producing glass. And their persistence paid off. At the beginning of the twentieth century, Owens and Libbey's machine-made bottles revolutionized the soda business. 

We find another example of innovative collaboration in the story of scientist Nikola Tesla and investor George Westinghouse. Tesla worked on systems using and producing _variable current electricity_, or AC. Today, AC is the world standard. But without Westinghouse, this wouldn't be the case. 

Together, Tesla and Westinghouse took on their main opponent: Thomas Edison, who promoted DC systems. DC systems were ineffective, especially over long distances. But Edison had friends in high places. By combining Westinghouse's determination and financial support with Tesla's discoveries, the two paved their path to success. 

Thanks to their resilience, the AC system prevailed, benefiting not only Westinghouse and Tesla but the whole of society. Industries such as the automotive industry would be very different today without the cheap production of electricity that Tesla and Westinghouse made possible. From their tale, it's plain to see that innovation can be revolutionary when it succeeds. So why are certain authorities putting innovation in danger today? That's what we'll look at next.

### 7. Innovation is seriously endangered in the United States. 

The United States used to be a paradise for innovation. Why? Because of the US patent system — a unique structure that boosted small-scale inventors and allowed their innovations to be used throughout the world. 

Established way back in 1790, the US patent system is market-based. The underlying idea is that financial reward fuels innovation, and that therefore patent holders should be entitled to sell, license or assign patents to others for monetary gain. In return, inventors agree to public disclosure and an expiration of the patent after a limited time period. 

This allowed for broad dissemination and ever-increasing numbers of innovative inventions. In the end the personal gain was supported to advance public welfare. But today, this vital system is under threat of reform. 

In recent years, the US patent system made the shift from a "first to invent" to "first to sign" approach. This means that what now matters most is not who first came up with the idea but who first signed the patent papers. This small policy change has huge implications. 

First off, large corporations now enjoy a huge advantage. They can allow themselves to sign more patents, even if they aren't sure they'll bring profit. Small inventors need money to file a patent and have to improve their innovation to see its true potential. And if they are not first to sign, they lose it all. 

Unfortunately for our tinkerpreneurs, innovation and creativity are steadily weakening as a result of government regulation. A free market where every individual is allowed to earn money, work together and invent boldly is the key to a progressive society — and those are the rights that we're being stripped of.

### 8. Final summary 

The key message in this book:

**For over a century, tinkerpreneurs have combined the genius of inventors with bold opportunism to give their inventions commercial success, often revolutionizing industries while they're at it. Collaboration and a supportive patent system has allowed tinkerpreneurs to flourish in the United States; new reforms, however, may end the tinkerpreneurial era.**

Actionable advice:

**Who built this!?**

Take a moment to research the history of a product you use in your everyday life. From your toilet paper to your smartphone to your glass coffee table, somebody's brilliant idea or clever marketing brought these inventions from the lab to you. This will give you an appreciation of the hard-working individuals behind the products you rely on. They need your support to keep tinkering!

**Suggested** **further** **reading:** ** _The Innovators_** **by Walter Isaacson**

The Innovators explores the social and cultural forces that inspired technological innovation through the history of computers and the internet. By weaving together the personal stories of technology's greatest minds, The Innovators gives you an inside look at how the best and the brightest innovate and collaborate.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michelle Malkin

Michelle Malkin is a columnist working for Fox News. She founded Hot Air and Twitchy.com. _Who Built That_ is her fifth bestselling book.

