---
id: 56094ad78a14e600090000ea
slug: who-gets-what-and-why-en
published_date: 2015-10-02T00:00:00.000+00:00
author: Alvin Roth
title: Who Gets What – and Why
subtitle: The New Economics of Matchmaking and Market Design
main_color: CC525D
text_color: A6434C
---

# Who Gets What – and Why

_The New Economics of Matchmaking and Market Design_

**Alvin Roth**

In _Who Gets What — and Why_ (2015), Nobel Prize winner Alvin Roth brings his groundbreaking research on market design to a broader, nonspecialist audience, explaining how markets work, why they sometimes fail and what we can do to improve them. Using contemporary examples, Roth outlines the nonfinancial factors that shape markets and shows how we can make more informed marketplace decisions.

---
### 1. What’s in it for me? Discover how markets work when they don’t revolve around money. 

Every year, hundreds of thousands of young Americans apply for admission to colleges and cross their fingers. Some get into their top choice of schools, while many others don't. It might seem to work like one big lottery, but a more accurate way of looking at it is to see it as one big market — a market with serious flaws.

When talking about markets, we often think about items — whether a TV, a shampoo bottle or the latest smartphone — sold at a certain price to any willing buyer who has the money. But these aren't the only kinds of markets. 

As these blinks will show you, public schools, hospitals and universities all operate according to market forces. And in these markets, money isn't king. So how do these markets work and how do you make sure everyone in a society gets what they need? These blinks will show you.

In these blinks, you'll learn

  * why markets are not just about money;

  * how Uber revolutionized the transportation market; and

  * why unlimited access to information is a mixed blessing.

### 2. Unlike classic commodity markets, matching markets are motivated by more than just prices. 

Markets control everything, from the coffee you buy to the schools your children attend. 

In a classic commodity market, price is the only factor determining who gets what. The mechanism here is simple: you decide what you want and, if you can afford it, you buy it. 

Taking a step back, the term "commodities" refers to goods that can be bought and sold in batches, like bushels of wheat. Since each bushel is essentially identical to every other bushel, price is the key factor in deciding whether a customer buys one.

But some products can't simply be classified as commodities. For instance, iPhones aren't identical to every other smartphone in the market; therefore, someone who chooses to buy an iPhone over a Samsung Galaxy doesn't base their decision solely on price, especially since the costs are comparable.

These so-called _matching markets_ are complex. In the most sophisticated kinds of matching markets, both the buyer and the seller have to "choose" each other. 

After all, you can't buy a job at Google, nor can you buy a romantic relationship; in both cases, we're dealing with matching markets.

To be clear, a market involves matching whenever price isn't the sole determinant of who gets what. And in some cases — especially when dealing with scarce resources — money doesn't even enter into the equation (think of kidney transplants or public school spaces).

The thing is, scarcity of resources can create problems for both buyers and sellers in matching markets. And in some cases, inadequate information can lead to bad matches.

These blinks will unpack these failures and advance some proposals on how to improve matching markets. So read on!

### 3. Technology can help ease market congestion. 

Markets are like motorways. Sometimes they become overcrowded, which can lead to major accidents or gridlock. 

Of course, the key isn't to clear the motorway (or the market) completely. To be successful, markets _need_ to be "thick," that is, to involve as many participants as possible. 

Too much thickness, however, can create _congestion_ — an excessive range of options that overwhelms consumers. Markets slow down when participants have to spend too much time evaluating potential deals.

Commodity markets manage to avoid this problem. If you want to buy one hundred shares of AT&T on the New York Stock Exchange, you don't have to submit an application or court the seller. At the same time, the seller doesn't have to pitch the item — the stock market simply brings buyers and sellers together at a price level determined by supply and demand.

Meanwhile, each matching market transaction must be considered individually. Think of the job market, where each candidate is evaluated separately; you can see how market congestion might occur when there are too many candidates for too few slots. 

Luckily, it's possible to overcome this problem by using new technology to make the market quick, even when it's thick. Smartphones and apps have sped up markets that require fluid, two-way communication to produce offers and responses. 

Consider Uber, the transportation network application. In the past, only taxis could pick up passengers on the street, while limo services would rely on advance reservations. This meant taxis had a monopoly over the quick-fix transportation market. 

The proliferation of smartphones changed all that, freeing up lots of limos and private cars that used to sit idle. Uber didn't invent hardware or introduce fleets of new cars, the company simply developed software that helped make the transportation market thicker and quicker at the same time.

### 4. Matching markets can lose efficiency and “unravel” when participants find ways to game the system. 

Most of us like to believe that markets are open and fair. Even so, some people try to get ahead by gaming the system, which, in turn, can lead to markets _unraveling_. 

In unraveled markets, both the buyers and sellers are deprived of the information they need to make an optimal decision. 

For example, in the United States, most new lawyers at major firms are likely to have been first hired as summer associates, about two years before actually completing their law degrees. Law students typically receive "exploding offers" from firms — take-it-or-leave-it offers only available for a limited time. 

Making these kinds of high-pressure early offers severely undermines competition among law firms for the best candidates, as it can dilute the pool of high-quality prospective lawyers on the market. In addition, participants will find it difficult to resist making an early decision; they realize that if they play by the rules and wait to make a choice, they'll probably lose out to those who don't.

This is a faulty system for two reasons: First, firms make offers without having information about the candidate's performance during their last two years of law school. Second, students are forced to accept or reject offers without knowing what other offers might come in. 

In the 1980s, the National Association for Law Placement (NALP) tried to address this issue by instituting a rule barring first-year law students from accepting offers until they had completed their first semester. 

But, of course, since lawyers specialize in knowing the rules inside out and finding loopholes, firms quickly found ways to skirt this regulation; as a result, entry-level lawyers still face an unraveled job market today.

### 5. Encouraging participants to communicate essential information can drastically improve matching markets. 

New York public schools offer a case study in how market design and innovation can drastically improve faulty markets. 

In the past, the school system was badly congested and inefficient. For starters, participants only exchanged information via snail mail. And because some schools only admitted candidates that had ranked that school as their first choice, students were reluctant to reveal their true preferences. 

To make matters worse, some schools withheld information, denying spots to certain students in order to keep them open for preferred candidates later on. Similarly, some parents learned that appealing directly to school principals could secure a spot for their children, thus circumventing the biased, corrupt system. 

These factors produced a grey market ruled by chaos and congestion. Many students struggled to get into their top-choice schools and school curriculums were often determined only at the very last moment, right before the school year was set to commence.

But then, in 2003, the Department of Education appealed to the author for help. He developed a computerized "clearinghouse," basically an electronic, centralized marketplace, that used an algorithm to place students in schools based on the varying interests of all participants — students, parents and schools.

This algorithm ensured that both students and schools were most likely, or at least equally likely, to get their desired outcome if they were forthright about their true rankings and preferences. 

So, if a student wasn't accepted to her first-choice school, her chances of being accepted to her second choice wouldn't be contingent on her ranking preferences. Compare that to the original system, in which certain schools only wanted to accept students that had listed them as a first choice.

### 6. Allowing participants to signal their preferences can improve market congestion. 

We've seen how increasing the flow of information can improve markets, but here's the catch: too much information creates new problems. 

This is the paradox of market design: as communication gets easier and cheaper, it can also become less informative. 

Just look at the proliferation of electronic communication. On one hand, it has spawned useful apps like Uber; on the other hand, it has dramatically boosted the volume of message we receive, making it increasingly difficult to distinguish between what's essential and what's irrelevant. This level of communication overload can create congestion within markets.

For instance, in the past, students had to fill out individual applications for each college they applied to. But today, a centralized web platform enables students to use a single system to apply to over five hundred colleges. 

Although it's easier for students to apply to more schools, it's much harder for colleges to gauge an applicant's actual level of interest. 

However, it's possible to limit congestion by implementing market signaling. 

In South Korea, for example, admissions exams for competing schools are often scheduled on the same day, thus limiting the number of colleges each student can apply to. As such, taking a specific school's exam is itself one way a student can send a strong signal of interest to their college of choice. 

The American college system offers a different set of signaling options. Some schools ask students to sign the guestbook when they participate in college visits. Candidates also have the option of applying through the binding early decision system — students apply to their top-choice school under the condition that, if accepted, they will attend the school. 

Since the early decision system requires applicants to send a very strong signal of interest to the university, the admission rate for this pool of students is higher than it is for the normal pool.

### 7. Final summary 

The key message:

**Matching markets are highly complex and are influenced by far more than just money. In many cases, human fallibility interferes with these markets, which can make it difficult to establish good matches. Luckily, smart market design can resolve many of the resulting problems, while benefiting everyone involved.**

**Suggested** **further** **reading:** ** _Treasure Islands_** **by Nicholas Shaxson**

_Treasure Islands_ offers insight into one of the darkest parts of the financial world: tax havens. It explains how wealthy people and corporations are able to avoid paying taxes by relocating their assets offshore. Tax havens are highly damaging to all but the tiny percentage of people who can afford to use them, and they contribute to the growing gap between rich and poor.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Alvin Roth

In 2012, Alvin E. Roth was awarded the Nobel Prize in Economics. Roth is one of the world's leading experts in game theory and market design, and is a professor of economics at both Stanford University and Harvard University.

