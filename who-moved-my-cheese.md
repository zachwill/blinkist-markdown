---
id: 560000f8615cd100090000cc
slug: who-moved-my-cheese-en
published_date: 2015-09-24T00:00:00.000+00:00
author: Dr. Spencer Johnson
title: Who Moved My Cheese?
subtitle: An Amazing Way to Deal With Change in Your Work and in Your Life
main_color: 90A1B7
text_color: 3F526B
---

# Who Moved My Cheese?

_An Amazing Way to Deal With Change in Your Work and in Your Life_

**Dr. Spencer Johnson**

Revealing business wisdom through a modern parable, _Who Moved My Cheese_ (1998) offers valuable lessons on how to best manage change in your life. Whether you're struggling amid a business downturn or trying to find a graceful way to handle a struggling relationship, this book gives you the tools to better understand human nature and see change as a positive force.

---
### 1. What’s in it for me? Don’t be afraid of change; enjoy life’s adventures! 

"It is easier for a camel to go through the eye of a needle than for a rich man to enter the kingdom of God."

Parables, like the one above from the Bible, are timeless: they offer poignant life lessons, showing us how to better cope with challenges both big and small.

While _Who Moved My Cheese?_ isn't an ancient parable, its message is no less pertinent. Learning to overcome fear, handling major life changes gracefully and finding a path to realize your dream — these are issues with which every person struggles in life.

In this story, four characters live in a maze: two mice, named Sniff and Scurry, and two little people, named Hem and Haw. As they travel the maze looking for cheese, it becomes clear that their journey isn't just to find food — it's a quest to better understand human nature. Read on to see what they find out.

In these blinks, you'll learn

  * how to better prepare for a major life change; 

  * why thinking like a "simple" mouse may better help you reach your goals; and 

  * how to visualize your goals to improve your chances of attaining them.

### 2. Your “cheese” or success in life may be paralyzing you. 

The two mice, Sniff and Scurry, don't think about things too much. They instead spend their time running up and down the corridors of their maze, in search of cheese.

This seemingly "brainless" way in which these two mice set about achieving their goal is instructive, and is often the most effective method in reaching your own goals. In fact, acting without thinking too much can save you time and energy. 

If there's no cheese at the end of a path, for example, Sniff and Scurry simply turn around and scramble down another path — without wasting time being angry or frustrated. 

Hem and Haw were also searching for cheese in the maze, but not because they were hungry. Rather, they thought that finding cheese would make them feel happy and successful.

With their more "complex" brains, Hem and Haw worked out strategies to find cheese, memorizing the maze's dark corners and blind alleys. Yet with all this planning, they still often got confused, and sometimes lost their way. And whenever the pair came up empty-handed, they became depressed, wondering if happiness would ever be attainable.

In "real" life, we too tend to overcomplicate things. Not only do we overthink issues or events, but we also become overly attached to the status quo. 

Finally, Hem and Haw found a huge stash of fancy, imported cheese down one corridor, at Cheese Station C. Every day they made sure to wake up early and visit the station for a snack.

Yet the pair's life gradually began to revolve around the feast at Cheese Station C. They felt at home there, and were very proud of it — yet they also began to take it for granted.

Like Hem and Haw, when we find success or "our cheese," we can quickly become dependent on it, so much so that our life revolves only around our "cheese."

> _"The more important your cheese is to you, the more you want to hold on to it."_

### 3. Good situations never last forever, so be prepared! 

So Hem and Haw woke up one morning and were shocked to realize that their cheese was gone.

Indeed, change always happens, sooner or later. Being aware of this can help you keep a closer eye on your current situation, to better anticipate the change ahead. 

Sniff and Scurry, in contrast, never took the stash for granted, and made sure to keep an eye out for any changes to the supply. Consequently, the two mice noticed that the amount of cheese was slowly but steadily declining.

If you expect things to remain the same, you may miss the signs that things are indeed changing. Hem and Haw were so thrilled with the cheese that they didn't notice the stash dwindling nor even when the cheese started to mold!

Sometimes the things we believe about ourselves make it harder to accept change. If you feel you deserve success, good health or an endless supply of cheese, anything that takes these things away from you will feel unfair — so much so that you might even deny the fact that change is happening at all.

Hem and Haw felt deep down that Cheese Station C was a reward for their hard work. After all, they had spent tons of time looking for it! So when the stash finally disappeared, they just couldn't accept reality. 

You never want to find yourself in this position. Instead, keep your eyes peeled for signs of change in your life and adapt as soon as you can. The sooner you do, the sooner you'll find your way again. 

Once Sniff and Scurry realized that Cheese Station C was almost out of cheese, they moved on without stressing too much about it. And luckily, they found a huge stash at another station, Cheese Station N. 

Hem and Haw were not so lucky. Unable to cope, the pair kept returning to the empty station, growing increasingly hungry, depressed and weak.

Take a lesson from these two "little people": The less you hem and haw and the sooner you adapt to change, the better off you'll be.

> _"If you do not change, you can become extinct."_

### 4. Visualizing your goals helps you push through the fear that keeps you from dealing with change. 

Why do so many of us — like Hem and Haw — get blindsided by big changes? It's fear. Fear is what makes confronting change so difficult.

After all, change requires you to cope with a new situation and grapple with a new set of rules. That can be both disorienting and frustrating, so it's natural to be scared of change.

For example, once they came to grips with the empty Cheese Station C, Hem and Haw had to venture again into the maze to find food. They worried that they might get lost, or find themselves in a blind alley with no way out. 

But remember this: as long as you're afraid of leaving your comfort zone, things will never get better. 

Imagine that your partner has left you, or that an injury is keeping you from playing your favorite sport. The loss you feel — of love, of movement — can be devastating.

You won't be able to recover, however, until you find a new way to fill your life with things that bring you joy again. This means that you _necessarily_ have to look for something new. 

Even though Hem and Haw were hungry, they were still too fearful to explore new paths in the maze. Instead, they smashed down the walls of Cheese Station C to see if there was something to eat behind them! But all in vain. 

Haw eventually discovered a good way to deal with his fear. He visualized himself sitting atop a huge wedge of his favorite cheese, soft-rind French Brie.

Indeed, visualizing your goal in vivid detail will actually increase your desire to attain it. So if you find yourself stuck, or you can't tame your fear, visualize your goal. That will stoke your desire and give you the energy to move forward.

> _"What would you do if you weren't afraid?"_

### 5. Dare to move in a new direction and things will get better. 

After visualizing his goal, Haw moved on to find new cheese. But Hem refused to budge. He stayed at the empty cheese station, angry and emaciated, and crucially, far too afraid to go out and search for cheese. 

At the same time, Haw's experience improved dramatically.

When you move in a new direction, you learn how to better embrace change. If you manage to conquer your fears just once, the next time you're confronted with change, you'll have the confidence to move on again. Never again will fear paralyze you as it did previously.

Haw found the courage to move on, even though he was all alone and still scared when he started his journey. But he slowly gained confidence. Although he came across just a few bits of cheese early on, he was still proud of himself for having come so far. Life just felt better now that he was no longer dominated by his fears. 

Haw learned an important lesson, that fear you let accumulate in your mind before you move in a new direction is usually much more intense than a new situation actually deserves!

And once you start moving, it's likely that things will improve. Sure, sometimes a new situation might end up being "cheeseless." But don't fret! There's _always_ new cheese to be found. 

Your new "cheese" could be a new friend, a new job, or even a new way to handle conflicts or do business. All you need to do is step outside your comfort zone and start the search. 

Gradually Haw began to regain his full strength and confidence. Full of anticipation, he searched the maze until he found Cheese Station N and tons of cheese — the place where Sniff and Scurry had been all along.

> _"When you move beyond fear, you feel free."_

### 6. Use the lessons you’ve learned from this “cheesy” parable to reach your own goals! 

So how can you apply the lessons of this parable to the benefit of your business or even your life?

Consider telling your team the story of Hem, Haw, Sniff and Scurry. They may begin to think more favorably about change and the potential benefits it can bring to an organization.

You can also learn valuable information by asking your employees with which character they identify. For example, people who feel an affinity for the mice Sniff and Scurry could become the agents of change that your organization needs.

People who are "Sniffs" are great observers. They notice the smallest of changes, which enables them to predict subsequent larger changes in a marketplace. Sniffs can also discover new products and competitive angles, or update a company's corporate vision.

People who are "Scurrys," on the other hand, like to get things done. These employees should be encouraged to take action based on a new corporate vision. 

The "Hems" of your organization, however, often feel unsafe during times of change. This may pose a problem, especially if they aren't able to find the courage to turn into "Haws." You'll have to work hard to demonstrate the ways that change can work to their advantage.

The experience of the "little people" and mice can teach your team to be on the lookout for new opportunities. Just consider how our parable ends. Even though there was plenty of cheese at Cheese Station N, Haw — having learned his lesson — kept exploring the maze, looking for other stashes.

Indeed, what works today won't necessarily work tomorrow. As the environment changes, your business needs to change, too — or you'll be left behind. 

So remember, even when things look good, keep an eye out for new, exciting opportunities.

### 7. Final summary 

The key message in this book:

**If there's one thing we know about life, it's that change is inevitable. So how do you handle change? To not only cope but thrive in a changing environment, you'll need to develop the kind of attitude and mind-set that makes adapting to change less frightening and more rewarding.**

**Suggested further reading:** ** _The One Minute Manager_** **by Ken Blanchard and Spencer Johnson**

With global sales of over 13 million, _The One Minute Manager_ is a classic that's still changing the workplace. It explains how managers can get outstanding results from their employees while spending as little time actively managing them as possible. A _one minute manage_ r needs just three simple tools to boost productivity — and transform their company.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dr. Spencer Johnson

Dr. Spencer Johnson is an American physician and psychologist. His seminal work, _Who Moved My Cheese_, was on the _New York Times_ business bestseller list for almost five years.

