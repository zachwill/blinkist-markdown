---
id: 5372226b6264390007010000
slug: who-owns-the-future-en
published_date: 2014-05-13T10:12:08.000+00:00
author: Jaron Lanier
title: Who Owns the Future?
subtitle: None
main_color: F36C78
text_color: E82032
---

# Who Owns the Future?

_None_

**Jaron Lanier**

_Who Owns the Future?_ explains what's wrong with the current way the information economy works, and why it's destroying more jobs than it's creating.

---
### 1. What’s in it for me? Find out why all the free services online are actually shrinking the economy. 

The music business has been through a lot in recent decades: online innovations like Napster, YouTube and Spotify completely transformed their working model, and the result has been that the industry's revenues have plummeted to a quarter of their former size.

Though many middle-class consumers are pleased that music has become available so easily, there is also a flipside. A similar digitization revolution could happen in other industries too: medicine, teaching and transportation to name a few. And this would put lots of people out of work.

These blinks explain why the internet is unlike previous technological advances, which created new jobs in the long run, because it is controlled by monopolies that use our free contributions to rake in huge profits.

In the following blinks, you'll find out

  * why in a few decades robots might be taking care of your grandparents;

  * why pretty soon you might be able to manufacture any products you want in the comfort of your own home; and

  * why you should actually be paid by companies like Facebook and Google for browsing the internet.

### 2. New technologies typically grow the economy, but this is not the case with the internet. 

In early nineteenth-century England, change was in the air: the invention of an advanced type of mechanical loom had edged many textile workers out of factories, leaving them without work.

In response, some workers got so angry that they stormed factory buildings and smashed the machines that had taken their jobs.

These people were known as _Luddites_, so named after their leader, Ned Ludd.

Eventually, the government cracked down on their attacks, and the workers found new kinds of jobs and their fury faded.

This story illustrates the commonly held view of how technological advances affect the economy.

First, a new kind of tool or method is invented that helps accomplish a given task — for example, cloth weaving — more efficiently than before. This means fewer people are needed to perform the task, so they are laid off. They become angry and worried about how they will feed their families, until new kinds of jobs emerge where they can once again provide value for the economy. Thus the whole cycle eventually results in overall economic growth.

This pattern has been repeated countless times in history, and many claim that it is happening once more due to the emergence of the internet and the new information economy. For example, online hotel and flight search engines have greatly reduced the demand for human travel agents.

Just as in the pattern described above, the internet has also created new ways for people to provide value to the economy, by creating content and information to be read by others.

Unfortunately, these contributions are not being rewarded with money, which means that the economy is not growing as it did in the past after technological leaps.

> _"We're setting up a situation where better technology in the long term just means more unemployment, or an eventual socialist backlash."_

### 3. Big internet companies provide free services and make money by collecting data on us and targeting us with ads. 

Let's start our examination of the information economy by looking at how some of the most successful internet companies like Facebook and Google operate.

The one thing that almost all major internet players tend to have in common is that they provide some of their services for free: Facebook costs nothing for consumers, and likewise most of Google's products, including its search engine, involve no fees.

What's more, these successful players don't invest in their own content, but rather let the public generate it for them. On Facebook, users create their pages, posts and comments themselves, whereas Google searches just provide links to websites built by others.

So where do they make their money if the product is free?

Mostly through advertising: both Google and Facebook display advertisements to you when you browse search results or your friends' profile pages.

What makes these advertisements especially powerful is that the companies also keep close track of your behavior and activities on their sites: Facebook knows your hobbies, likes and demographic, and can therefore display ads based on your specific interests, whereas Google does the same by drawing on the keywords in your searches.

Some companies will even sell the information they've collected to third parties and generate more revenue.

If you're upset by this, you should know that you've agreed to it. When you sign up for a service like Facebook or Google, they present you with an _End-User License Agreement_, which you have to sign to proceed. Of course, no one reads them, but buried deep within them are paragraphs that give companies the right to track you and potentially re-sell your information.

### 4. Siren Servers track our online activities and compile dossiers on us for their own profit. 

It's not only the obvious players like Google that are interested in tracking your online activities. In fact, there are over a thousand schemes to do this, because many companies are scampering to become the dominant source of information on consumers' — i.e., your — activities. For example, even a visit to the homepage of a reputable newspaper like the _New York Times_ will spur a dozen tracking services to monitor you. 

And these players are not just interested in targeting you with ads. They also sift through the resulting enormous amounts of information, known as _big data,_ for other ends. For example, insurance companies might use big data to better gauge what kind of insurance premium you should pay.

Another example can be found in finance, where internet trends are used to make savvy investments. For example, if an automatic algorithm found that a lot of people were commenting about mosquito bites, it can instantly invest in a company that produces lotions to soothe them.

Meanwhile, political campaigners use big data to identify voters who can be potentially persuaded to switch sides, and thus target them with appeals.

Finally, an increasingly important use for big data is in _artificial intelligence_, which is relatively powerless without lots of human input to help it. Take Google Translate, for example: though it doesn't provide perfect translations, it does do an amazing job, and this is only possible because it scours the web for all the human translations of the phrase you've entered. Without them, its translations would be far shoddier.

The companies that operate in this manner are called _Siren Servers_ : they lure us in with free services, and then use their privileged overview of our activities to compile data on us and manipulate us to gain money and power.

### 5. Siren Servers tend to evolve into monopolies. 

So why are Siren Servers problematic for the economy?

Well, first off, they tend to evolve into monopolies or near-monopolies.

The nature of the internet allows them to serve the global market, and since they also capture information globally, this usually minimizes any advantages that local, small-time players may have had.

Once the local players are out of the picture, the Siren Servers battle each other for dominance. Typically, the more information a company can accrue, the better the free services it can offer; this attracts more users, giving it even more information. This self-enforcing loop results in just one company prevailing.

This is especially true for consumer businesses like Facebook, where the service actually becomes more valuable the more people are on it. Think about it: if you want to join a social network to stay connected to your friends, would you rather join the one that includes five of your friends, or the one that features 500? In scientific terms, this idea is expressed in _Metcalfe's law,_ which states that a network's value is proportional to the square of the number of its nodes.

What's more, these consumer-facing services often deliberately make it difficult for you to leave. For example, Facebook might encourage you to handle personal messaging via your Facebook page, which will make you reluctant to leave the service for fear of losing your correspondence.

All these factors lead consumer-facing internet companies to typically develop into pseudo-monopolies, and monopolies have a number of drawbacks for the economy, such as reducing choice for consumers. But there are more serious consequences, too, which we'll delve into in the next blink.

### 6. The information economy is not spurring new jobs, but is actually hastening the decline of many industries. 

We might regard the monopolies of Siren Servers as a necessary evil if they compensated for this by employing lots of people, but unfortunately this is not the case.

In fact, major internet companies like Facebook, despite extreme growth, have hired fairly few people. At the same time, they've kept their other costs low by, as you've learned, having users generate the content on their sites.

Far from contributing to economic growth like previous technological innovations, the dynamic of the information economy has actually played a part in the decline of many industries, most notably in the creative realm: music, photography, journalism and so on.

Consider the music industry. Thanks to innovations like Napster and YouTube, the traditional music industry that was built around distributing music is shrinking fast, currently standing at about a quarter of its former size.

But why are musicians and other similarly creative professionals facing this decline in the commercial value of their work? Simply because it is so easy to digitize, copy and share songs, photographs and videos. This means that free copies flood the market, and no one is willing to pay for the original product anymore.

Meanwhile, Facebook is happy for people to share these items on its site, thus generating free content for its users to browse.

> _"Great fortunes are being made on shrinking the economy instead of growing it."_

### 7. Every industry is rapidly entering the digital realm, which will mean lots of lost jobs. 

Now you may be wondering why we should be worried about the economy, when only niche industries like the music industry have been affected?

Simply because the same dynamic will befall every industry that enters the digital realm, and in fact, _every_ industry will eventually enter the digital realm.

If you think this is an exaggeration, think again.

For example, self-driving cars are rapidly becoming increasingly sophisticated, and have even been legalized in some states. Once they become the global norm, the vast masses who work as taxi or truck drivers will find themselves out of work.

Perhaps an even more extreme example is that of healthcare: Robotic nurses are expected to be taking care of the elderly in Japan by 2020, and even doctors could someday be replaced by nano robots or robotic surgeons.

Or what about the age-old profession of teaching? Today, masses of lectures, lessons and other educational materials are already freely distributed online. While this is fantastic in the sense that it provides educational opportunities to those who did not have them before, it does make one wonder if teachers will still be required in the future.

Of course, the manufacturing industry has already been through a number of technological shifts, but could it be that one day the factories themselves will become totally obsolete? This could happen thanks to the latest craze of 3D printing, which can enable you to download designs for products online and then produce them in the comfort of your own home.

Soon it may well be possible to 3D print even complicated electronics like smartphones, which would end the jobs of the masses of people laboring in electronics factories. Obviously, an industry would still be needed to produce the printers and the materials for them, but it's unlikely that this alone would be enough to power an entire economy.

It seems we're heading toward a world where everything will be digitized and can be easily shared online with very little human labor involved.

> _"Higher education could be Napsterized and vaporized in a matter of a few short years."_

### 8. Users must monetize the information they provide to companies. 

So what's the heart of the problem with the new information economy that was outlined in the previous blink?

Basically, it's that there's no middle class: people who today contribute information and content aren't being rewarded monetarily for their efforts; the Siren Servers reap all the profits. Left on this course, this dynamic will eventually turn against the Siren Servers themselves, since even they need a market of economically successful customers to advertise to.

So what's the solution?

Simple: _monetizing_ the information we provide to companies.

This means every valuable piece of information you provide, like a sentence that helps a translation algorithm work, should be rewarded by the company that makes money with that algorithm. Similarly, social networking sites should reward you for the privilege of announcing to others what videos you've watched and what songs you've listened to, for they are profiting from that information.

These would only be tiny nanopayments, proportional to how much value you have created, but nevertheless, these little streams would add up into real income. These payments could either be _instant_, like a set price for every morsel of information you provide, or _legacy_ payments, whereby you're paid a commission later, following a calculation of the profit someone has generated from your information.

This way the companies that profit from user-generated data would have to pay users for it, and, though it wouldn't be free, it would still be affordable for them. This system is a kind of _humanistic information economy_.

### 9. A centralized two-way system is needed to distribute payments from companies to users who contribute information. 

To enable the nanopayments described in the previous blink, we would need to instate a kind of two-way linking system that would track both who contributes information and also who profits from it. This two-way system is called a _Nelsonian network_, after its inventor, Ted Nelson.

Basically, in a Nelsonian network, you would see all the companies that link to your information, and thus would know who needs to pay you for it. What's more, companies could no longer spy on you discreetly, as you would know what information they are extracting.

For this system to function properly and remain operational as it tries to balance incoming payments from companies and outgoing payments to individuals, it would need a centralized governing body that would probably be financed by some kind of taxation.

So where might this central governing body for the humanistic information economy come from?

Probably either from governments, or from a consortium of Siren Servers who have decided to change the system. Though both options seem far-fetched at the moment, the impending economic slowdown caused by a vanishing middle class could drive both of them to want to make this change.

Though this kind of system would probably be full of glitches initially, such a transition is necessary. For it may well turn out that eventually the information sector is the only industry left, and unless a way is found to compensate the people contributing information to it, the capitalist ideology underlying the system is bound to fail.

### 10. Final summary 

The key message in this book:

**You provide two things of value to major internet companies: information on yourself, like your preferences, and the content that you post on, for example, Facebook. The companies use this dynamic to gather masses of free content for people to browse, while profiting by displaying precisely targeted advertisements at them. But the problem is that you're not being compensated for these services rendered, and for the sake of the economy, you need to be.**

Actionable advice:

**Block companies spying on you.**

Whenever you browse the web, you can be sure that there are dozens of companies following your activities precisely to better build up a dossier on you. One way you can stop them, or at least many of them, is by using a browser extension or add-on like _Ghostery_ that is dedicated to blocking these spies.
---

### Jaron Lanier

Jaron Lanier is a computer scientist, musician and writer. He was an early innovator in the field of virtual reality and has taught at Columbia University and New York University. His other books include _You Are Not a Gadget_ — also available in blinks.

