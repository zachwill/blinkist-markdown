---
id: 5a1408abb238e1000652970b
slug: who-en
published_date: 2017-11-22T00:00:00.000+00:00
author: Geoff Smart and Randy Street
title: Who
subtitle: The A Method for Hiring
main_color: 1E6C95
text_color: 1E6C95
---

# Who

_The A Method for Hiring_

**Geoff Smart and Randy Street**

_Who_ (2008) is your guide to the greatest problem that faces every business leader: how to hire competent staff. These blinks are chock-full of actionable tips on how to ensure your firm has a steady supply of adept candidates to choose from and that all your hires are in step with your goals.

---
### 1. What’s in it for me? Learn the secrets to great recruiting. 

Pretty much every business will have to figure it out at some point. How does one recruit the right person? Who is the perfect fit? It might seem like a simple undertaking, but consider this: half the recruiting companies end up hiring individuals who aren't the right fit or don't have the right skill sets. And the cost of those missteps is staggering. So what can be done?

In these blinks, we'll take a look at how you should think about hiring. We'll look at the thinking that needs to be done before beginning the hiring process itself, and equip you with a battery of questions that can be used in interviews.

In the end, you'll be more than ready to find the right "who" for your business.

You'll also find out

  * what the disadvantages of hiring a generalist are;

  * why social skills are as important as professional skills; and

  * the four interviews you should conduct with all potential recruits.

### 2. Hiring mistakes can be catastrophic and many people lack the skills to make the right decisions. 

Hiring can be a nightmare. But why all the fuss about getting the right person on the first try? If a new hire doesn't work out, you can always just replace her, right?

Well, here's the truth: if you think hiring is a matter of trial and error, you're probably pretty new to the grueling initiation rite that is onboarding a new employee. Furthermore, hiring errors can cost companies a fortune and they happen far more often than they should.

Studies done by the authors have found that a typical hiring mistake will cost a company around fifteen times the employee's monthly salary. Some of these costs stem from poor decisions made by the employee; the rest, from the work necessary to fire and replace him.

To put this in perspective, imagine you hire a manager who earns $100,000 a month. If your decision turns out to be a bad one, you'll have cost the company up to $1.5 million!

Clearly, hiring the wrong person is an expensive mistake. Yet it happens all the time. According to management guru Peter Drucker, most managers make at least 50 percent of their mistakes during hiring decisions.

This makes sense, though. Business people often lack the skills they need to make sound hiring choices. So they end up relying not on personal expertise, but gut instinct.

This approach can be likened to that of an amateur art critic whose repertoire of responses to a work of art is limited to "I like it" and "I don't like it." The problem is that art can easily be forged and first impressions are often weak guides. Like the rash and uninformed critic, people who hire based on instinct can be fooled by charismatic candidates with little true ability.

Or, in other instances, managers act like prosecutors, trying to trick candidates into slipping up. As you'll learn, doing this will only put a prospective hire on the defensive, preventing you from finding out whether he has the right skills for the job. Luckily, there's a better approach, and in the next blinks you'll learn all about it.

### 3. To have your hiring needs met, you first need to define them. 

Would you consider hiring an unlicensed contractor to remodel your kitchen? Probably not. After all, there's no way to know whether she has the skills necessary to complete the job. By the same token, you shouldn't hire people without knowing precisely what qualifications they need to succeed. To do that, you must define your hiring needs.

Recruiters often forget to define exactly what it is they're looking for — a lapse that can derail the entire hiring process. Indeed, it's crucial to know exactly what you want right from the get-go.

One of the authors recently worked for a major financial services firm that needed to hire a VP of strategy and planning. The author started the process by asking the management team what the new VP's role would be.

His question sparked a heated discussion. One manager argued they needed someone who could craft a master plan for their budget, while another said they needed someone with a vision for new strategies and products. It was immediately clear that the team didn't agree about what they were looking for, which is precisely what you want to avoid.

After all, if you're not sure who you want to hire, you'll end up with a generalist, rather than someone with specialized skills. In fact, hiring an all-round candidate is one of the most common mistakes in hiring. People tend to be impressed by the generalized abilities of such people, forgetting, meanwhile, to ask about essential and specific skills.

Just imagine you have a backlog in your business operations. You need someone who can tighten up your operations and prune the old work. In interviews, you speak with a capable, innovative and charismatic business developer. Even though she'd be a stellar candidate in a different context, she's not right for the job at hand. Why?

Well, while she might generate more business for the company, this is exactly what you don't want; more business will increase your backlog, thereby creating greater operational delays and increased customer complaints.

### 4. A candidate’s ability to fit into your company’s culture is often more important than her skills. 

Intellect and smarts alone won't make an employee great. She needs to have social skills, too, something recruiters would do well to keep in mind.

But to truly ensure that new hires fit in, you also need to know what the culture of your company is. In fact, one-third of the CEOs that the authors interviewed while preparing this book admitted that they'd made massive hiring mistakes by writing off the importance of a cultural fit.

To avoid making this mistake yourself, define your company culture from day one. Bring your leadership team together and ask them how they would describe the environment of the company. No need to get too fancy — quick, off-the-cuff definitions will do the trick.

For example, one of the author's clients came up with catchwords like "analytical, dynamic, fast and informal." Now they know that new employees should be comfortable in an environment that matches those words.

That being said, ensuring a cultural fit might mean saying no to people with serious talent. Just remember: a prospective hire might be incredible at her job, but not share your company's values and, in the end, your company culture is more important.

For example, the NPO Institute for Sustainable Communities recruited a new employee for an essential role in an African country, where the institute was battling AIDS. The new hire did a great job, managing to transform the opinion of both the country's president and parliament, who had previously been dismissive of the AIDS crisis, calling it a matter of immorality.

However, the new employee wasn't a team player. He was brilliant, but also arrogant and often made those he worked with feel inferior. His work-style had a negative impact on the overall performance of the team and, in the end, the CEO had to let him go.

### 5. Traditional hiring methods are outdated but yours can be easily revamped. 

Plenty of billion-dollar firms consider hiring to be one of their most important business operations. But despite this fact, for most companies, it's not a continuous process.

That's because traditional hiring methods only look for new employees when they're needed, which is a major error. After all, with the clock ticking, the hiring team is under intense pressure to find someone — and fast. This makes it more likely that they'll hire the wrong person.

Not just that, but generally when a manager knows there will be a vacancy, she contacts the HR team and asks them to find some potential candidates. As a result, the company ends up having to choose among the available job hunters instead of selecting the absolute best candidates.

So what's a better method?

It's best to get referrals from your networks, both business and private. In fact, herein lies the golden rule of hiring. Seventy-seven percent of the CEOs interviewed by the authors said that using their own network was hands down the best way to make successful hires for their company. But the key is to always be searching for these people, not just seeking them out when you know you need them.

For instance, the CEO of Aon Corporation, a leading insurance firm, says his success isn't a result of being smarter than the competition. Rather, it's a product of devoting a tremendous amount of his time to finding new talent for his company. He has a goal of finding 30 new potential hires every year and his main managers have the same target. As a result, the company always has a substantial pool of stellar candidates whenever a specific position opens up.

So building a roster of potential candidates is key. But even with such a pool, you need to be able to choose the right person for the job. In the next blink, you'll learn how to do just that.

### 6. To make top-tier hiring decisions, you need a top-tier interview system. 

When sussing out candidates, interviews are crucial — and knowing how to structure them is even more important. That's why, to ensure that your hiring decisions hit the mark, you should use a four-interview system.

Each of these interviews will build on the others, increasing in precision as the process advances. Implementing this system will require effort and dedication, but it will be worth it in the end, as you'll be much more likely to find the perfect match for the specific role you're filling.

Such a comprehensive approach is important. After all, this isn't an audition for a part in a movie; you can't make a decision based on seeming ability and appearance. You'll need a long list of facts about a person's professional life — a life that may span decades.

So make a point of conducting four basic interviews: the _screening interview_, the _who interview_, the _focused interview_ and the _reference interview._ The first of these, the screening interview, is an initial test to filter out candidates who are clearly not right for the job.

This interview should be conducted by phone and last no longer than thirty minutes. There are four essential questions to answer here: What are the candidate's professional goals? What are his professional strengths? His weaknesses? And, finally, how does he think his former bosses would rate his performance? The answers to these questions will give you a rapid-fire overview of the candidate's profile and past performance.

But don't go too deep. Remember: the goal is to screen and eliminate. In other words, as soon as it becomes clear that someone isn't a fit, you should begin winding the interview down.

Next up, you'll learn about the other three interviews that complete this process.

### 7. By diligently investigating your candidates, you can find the perfect person for the job. 

The screening interview will narrow the field, but you'll have a way to go before you select your ideal candidate. The next step is to unearth relevant facts about these promising candidates and the _who interview_ is your tool to do so.

This second interview is more in-depth, the goal being to find out who the candidate really is. It's based on a method pioneered by Brad Smart, the father of one of the authors. Here's how he came up with it:

As a young management psychologist, Brad was asked to sit in on management recruiting interviews involving senior partners. When the partner finished asking questions, Brad was allowed to ask a few more.

In these interviews, he went deep, asking about every single detail of the candidate's career — all her successes and all her failures. The senior partner was impressed by this approach and, since then, such in-depth interviews have proven to be a successful practice.

Typically, these who interviews involve questions related to the tasks required by prior roles, what went well during them and what could have gone better, as well as relationships with bosses and colleagues.

If done correctly, at the end of the who interview, you'll have a full picture that lets you select promising candidates from the broader pool.

Once you've made these selections, it's time to ensure that the candidates actually fit precisely what you're looking for, which is where the last two interviews come in. First, in the _focused interview_, you should go through the specific tasks the job entails. For instance, if the role of the position is to increase sales, you'll want to reevaluate the candidate's skill set around bringing in new customers. This is also an opportunity to ask any follow-up questions after you look at your notes from the who interview.

And, finally, the last step is the _references interview_, where you check the candidate's references by asking precise questions. Doing so will help you uncover the whole truth and set you up to make an intelligent decision.

At the end of this process you should be ready to make an offer. But what if the candidate doesn't want the job? You'll learn how to tackle that in the next blink.

### 8. Selling a candidate on a job means convincing him that the position fits his skills, and lifestyle. 

Imagine how frustrating it would be to conduct four interviews with a perfect candidate, only for him to walk out the door. It would be a huge letdown, but you can make sure it doesn't happen by selling the candidate on the job.

That means convincing him that the job fits both his skills and personality. After all, employees want to be set up for success and the better a job fits their skills and style, the more likely they are to flourish.

Just take Mark Stone of the private equity firm Gores Group. He says that if you're as concerned about a job fitting a candidate's needs as you are about him fitting the job, convincing him will be a piece of cake. Shockingly, taking this step is exceedingly rare. Ninety-nine percent of recruiters only endeavor to learn whether the candidate is equipped for the job, utterly failing to consider the potential hire's perspective.

In short, making sure the role is right for the candidate is as important as figuring out whether the candidate is right for the role. But it's also important for a new hire to feel included in the company culture, which means you'll also need to sell him on the environment. To do so, tell him about the company, its vision and its goals. Discuss the way people behave in the company. Is it inclusive and familial or competitive and fast-paced?

Make sure the candidate understands that you chose him because he fits in. Make him feel welcome!

And, finally, selling a job also means making sure it fits with the candidate's family life. That doesn't just mean having flexible timetables, but also making families a part of the company culture.

For example, Gabriel Echavarría owns a ceramics company in Colombia, and he puts a tremendous emphasis on meeting the families of every new employee. The families are taken on a guided tour of the company site, introduced to employees and invited to parties. The whole goal is to make them feel at home in the company.

### 9. Make hiring a part of your company culture. 

Management experts and CEOs agree that bringing on talented employees is the single most important facet of building a successful company. But to make hiring all-star candidates a part of your company culture, you'll have to lay out a clear process.

First, you need to make clear to your management team that people are your top priority. This is essential. Indeed, successful managers interviewed by the authors said that they spend up to 60 percent of their time thinking about their current and future employees.

From there, your job is to improve hiring methods. To get your team on board with this, you need to do more than just mention hiring at meetings. Rather, you need to have discussions about the idea, convincing your managers why hiring is important. During these discussions, you can bring in materials on the subject and circulate them throughout your firm. You might even hold workshops to train your team to network and keep an eye out for talent.

That being said, be sure your hiring techniques remain within the law. Getting creative during the hiring process is great but there are a few legal restrictions you need to respect. For instance, you need to stick with relevant criteria when evaluating candidates, whether positively or negatively. If you reject somebody, do it because of the facts, not your personal feelings.

Beyond that, you must have some type of standardized hiring procedure so that all candidates receive equal treatment. Be sure that there are no disparities in regards to demographics.

Finally, you should be aware that it's illegal to ask a candidate certain questions. For example, you can't ask a prospective hire if she's married, whether she intends to have kids, what her sexual orientation is or when and where she was born.

With these few precautionary facts in mind, you're ready to build a talent pool for your company to source from. Get started today and you can put your firm on the path to the incredible success that only comes from having the best people on board.

### 10. Final summary 

The key message in this book:

**Hiring is often treated as a last-minute necessity — an approach that brings poor results. To make sure that your company has the talent it needs to succeed, you should instead make hiring a top priority. Treat this process as a constant effort and never stop building your pool of potential hires.**

Actionable advice:

**Be persistent and offer concessions.**

Hiring is hard work and you don't want it all to be for nothing. So, once you find the right person for a job, you shouldn't take his first no as a definitive answer. Instead, find out what he needs in order to say yes and compromise if you have to. Don't risk losing out on top talent because you didn't do everything you could.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _60 Seconds & You're Hired! _****by Robin Ryan**

_60 Seconds & You're Hired!_ (1994) is your guide to nailing your next job interview. These blinks are packed with actionable advice on how to grab a potential employer's attention, sell yourself and land the job of your dreams.
---

### Geoff Smart and Randy Street

Geoff Smart runs the consulting firm ghSMART, which helps business leaders increase their impact and achieve their goals. His other book, _Leadocracy_, was also a _New York Times_ bestseller.

