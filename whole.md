---
id: 58bc915fc7c53e00041d65c9
slug: whole-en
published_date: 2017-03-06T00:00:00.000+00:00
author: T. Colin Campbell
title: Whole
subtitle: Rethinking the Science of Nutrition
main_color: 787D45
text_color: 5E6323
---

# Whole

_Rethinking the Science of Nutrition_

**T. Colin Campbell**

_Whole_ (2013) poses some fascinating questions: Can a change in our diet change the world? Would cutting back on our meat consumption make us and the planet a whole lot healthier? The evidence certainly suggests that a diet based on whole foods, plants and other low-protein foods might be the key to healthier living for everyone. So find out why the powers that be would rather you kept eating fast food.

---
### 1. What’s in it for me? Discover the benefits of a plant-based diet. 

Today, health care has become synonymous with medical care and eating healthily has become a science of eating supplements and buying packaged foods that contain the right nutrients. But this approach to health has had dire consequences, not only for us, but also for our environment and even for the political landscape.

So let's unpack the flaws embedded in our modern conception of health. We'll explore alternative ways of thinking about healthy lifestyles, and propose a whole-food, plant-based diet that could improve everyone's life and longevity.

In these blinks, you'll learn

  * why medical health care isn't the solution to good health;

  * how protein is actually bad for you; and

  * that an apple a day may truly keep the doctor away.

### 2. A change of diet is a better path to healthy living than relying on the health-care system. 

In the United States, the term "health-care system" is a bit misleading. Rather than caring for healthy people and looking for ways to prevent disease, the system spends most of its time caring for people who are already sick. So, really, a more appropriate name might be "disease-care system."

A big part of the problem is the care itself. After heart disease and cancer, medical care ranks as the nation's third biggest killer.

Every year, over 100,000 people are killed by prescription drugs designed to cure diseases — and that number doesn't even include accidental overdoses. Other common medical-related deaths include unsuccessful high-risk surgeries, catching pneumonia at hospitals and patient-care errors.

If all of this is news to you, you shouldn't be surprised. The government does its best to keep these facts under wraps because the medical industry is so profitable. The Centers for Disease Control and Prevention don't even list "medical care" as a cause of death.

Yes, this is all a bit troubling — so the best way to avoid needing care is to maintain a healthy diet. What you eat can not only prevent but also help cure disease.

The food we eat has the biggest impact on our overall health — more than our genes or our environment.

With the right diet, we can avoid diabetes, strokes, erectile dysfunction and arthritis. It can even prevent and cure those top two killers: heart disease and cancer.

This conclusion was reached after decades of research, the results of which you can find in the author's book, _The China Study_. The data revealed that a change in diet could reverse advanced heart disease and have more immediate and profound effects on illnesses than any surgery or prescribed medication.

What kind of diet can do all this? It's one that is based on plants and whole foods, such as fruits and vegetables, legumes, nuts, whole grains and beans, which are eaten in their natural state without any added salt, oils or sugars.

This diet prohibits any animal products and processed foods and consists of 80 percent carbohydrates, 10 percent fat and 10 percent protein. It's as simple as that.

### 3. A plant-based diet can help fight harmful oxidation caused by animal protein. 

You've probably noticed how quickly a sliced apple can turn brown if it's left sitting on the table. This discoloration is the result of a chemical process called oxidation. But did you know that the same reaction can happen in your body?

Oxidation is a common and important process, but, in excess, it can be deadly.

It happens whenever molecules and atoms collide and electrons are given away. Even though oxidation helps transfer energy and eliminate toxins, too much of it can lead to the production of _free radicals_, which can promote cancer, heart attacks and other diseases.

And what leads to excessive oxidation? Too much protein in your diet.

Despite common misconceptions, you don't need a lot of protein in your diet — especially not animal protein.

Researchers in India used rats to study the effects of animal-protein consumption. After placing the rats in an environment that exposed them to high levels of a known carcinogen, they fed one group a diet that contained 20 percent animal protein, and the other group a diet that contained less than 5 percent animal protein.

Much to their surprise, none of the rats on the low-protein diet developed cancer. Yet every single rat on the high-protein diet did.

Decisive results such as this are quite rare in clinical trials, but similar results were later found in a trial with human subjects.

To protect against oxidation, there are antioxidants, which plants produce to protect themselves and which we can take advantage of.

During photosynthesis, plants turn the sun's rays into energy, a process that also produces free radicals, which are just as harmful to plants as they are to humans. However, plants defend themselves by producing antioxidants, so when you eat these plants, you can absorb these antioxidants and keep your body healthy and well protected against cancer.

### 4. Modern science is blinded by reductionism that misses the big picture. 

Our universe is a big place with many mysteries, but when we leave the working out of these mysteries to specialists within various scientific and medical fields, we run the risk of missing the big picture. It's like different people wearing blindfolds trying to identify an elephant — one touches the tail and thinks it's a piece of rope; another touches the trunk and thinks it's a branch.

This kind of reductionism is popular in modern science right now, but it's not the only way to look at things.

Of course, reductionism is an invaluable tool. We focus in on a given subject to make it more understandable, filtering out distracting information. The human brain works in a similar way, using separate filters for sight and sound so that we can properly process the world around us.

Reductionism is also the principle behind a microscope. By isolating and magnifying a subject, we can better comprehend its behavior.

But we run into trouble when we forget that these tools and methods are filters, and not the full picture.

Yet this is exactly what happens when our scientific and medical professionals spend all their time isolated from one another: the reductionists forget the filter is there and believe that by understanding one part, they understand the whole.

This leads to errors and misjudgment — and that's why we need to adopt a philosophy of _Wholism_, which places a value on the overall system, a system greater than the sum of its parts.

Reductionists believe that if you understand how each part of a clock works, then you'll understand how the entire machine works. This may hold true for simple systems, but once things become more complex, as with human bodies, this philosophy falls apart.

Knowing how the brain's neurons and enzymes function won't help you understand how someone will emotionally react to hearing their favorite song or watching a sunset.

The same holds true for how nutrition and diet affects our bodies — you need to see the whole picture to fully understand what is going on.

> _"Wholism isn't the alternative to reductionism, it encompasses it."_

### 5. The way our body processes nutrition is more complex than nutritional labels. 

If you don't bother to check how many micrograms of niacin, also known as vitamin B3, your food contains before you buy it, here's some good news: that level of detailed information isn't of much use to begin with.

To start with, if you want people to eat a healthy diet, you have to do more than just list ingredients and nutritional information.

Informing people about what they're eating is great, but providing too much minute detail can backfire.

People tend to ignore information when it gets as microscopic as modern-day labels often do. These labels also imply that the listed nutrients are more important to your health than the numerous nutrients that aren't listed.

Plus, all the numbers and percentages make it seem like a healthy diet is nothing but a mathematical equation, when, of course, it's much more complex than that.

It's useless to try to get precisely 100 percent of your recommended daily allowance of nutrients since different foods and nutrients are broken down and absorbed in different ways by our digestive system.

This is partially due to _bioavailability_ — the proportion of a substance that will actually be absorbed by the body. Say you ingest 200 grams of vitamin C. You can't be sure exactly how much of it is actually going to be used by your body, or in what way. But don't worry, this is a good thing. Your body will use what it needs when it needs to.

Additionally, the amount of nutrients in our food varies greatly.

Even if you have two peaches that look similar, one might have as much as 40 times more beta-carotene than the other one. And there are any number of reasons for this: the quality of the soil in which they were grown, how much sunlight they received, even the time of year they grew.

So studying labels is a fool's errand. The best way to get all the nutrients you need is to eat a varied whole-food diet, including some of those more obscure fruits and vegetables that you may have neglected in the past.

### 6. Taking supplements isn’t as helpful as eating whole foods. 

The vitamin supplement business is quite strong in the United States. Over half the population takes vitamin supplements on a regular basis, which earned the supplement industry $30 billion in 2007 alone.

However, what these consumers don't know is that most of these supplements are a waste of money.

There have been numerous studies on the benefits of supplements, but after comparing the results, there is no definitive evidence that they provide any long-term health benefits.

For some people, such as those with an iron or iodine deficiency, taking a supplement may truly be helpful. But people with a real need make up a fraction of the overall market, and even in these instances, the supplement won't be as effective as eating something closer to the nutrient's original chemical state, like the natural iodine you can find in dried kelp, for instance.

In the end, the only one really benefiting from supplements is the multi-billion-dollar vitamin industry.

The main reason supplements are generally useless is because they don't contain the hundreds and thousands of other nutrients that whole foods like fruits and vegetables contain.

We know very little about these other chemical nutrients, but we do know that they play an important role in how the body processes the vitamins and nutrients we need. This is why eating an apple provides far more nutritional value than taking a supplement that supposedly contains the equivalent nutrients.

In a study that compared vitamin C supplements to an apple, just two apple slices were found to have the antioxidant strength of three vitamin C supplements, or 1,500 milligrams of the vitamin. However, upon analysis, the apple was shown to contain less than six milligrams of vitamin C.

Therefore, it had to be the hundreds of other chemical nutrients in the apple that either have a similar antioxidant effect or make the small amount of vitamin C extremely effective. Either way, there's no question that the apple is far more nutritious.

### 7. Switching to a plant-based diet could solve many of the world’s problems. 

A whole-food, plant-based diet is not only the healthier option for humans; it would also make the earth a whole lot healthier as well.

To combat global warming, we need to lower harmful emissions such as CO2 and methane, and an effective step toward this goal would be to reduce the number of livestock.

Conservative estimates hold the livestock industry responsible for 20 percent of what's causing global warming, while the World Bank places that number around 51 percent.

Part of the problem are the methane gases released by cows and cattle, which are 25 percent more effective at capturing heat in our atmosphere than CO2. Methane is an even more pressing concern than CO2 because it breaks down in the atmosphere at a far faster rate. Reducing this threat now would have a tangible and immediate impact.

If our environment weren't reason enough, there's also the cruel practices of modern factory farming.

The suffering of animals has increased in tandem with the efficiency of factory farms.

This is especially the case for US farms that qualify as concentrated animal feeding operations (CAFOs). On such farms, overcrowded conditions and barbaric mutilation is common practice.

To expedite growth, animals are pumped full of hormones, and to avoid the spread of infection in their densely packed enclosures, they're given large doses of antibiotics. But if an infection does arise, it's standard procedure for the animal to receive an amputation before it gets any worse.

The best way to take a stand against these kinds of practices is to stop buying factory-farmed meat and dairy.

The methods of this industry are not only unsustainable; they contribute to poverty, starvation and death around the world.

The livestock on these farms consume far more food each year than all the people on the planet combined. Yet, regardless of this, millions of people continue to die every year from starvation.

Making matters worse, land around the world is being purchased by factory-farm corporations that then chop down trees, erode the soil and poison the land with fertilizers.

### 8. Food and medical industries are more influenced by corporate profit than health. 

At this point, you might be wondering why the progress of nutritional science hasn't led to a nation of healthier people.

To figure this out, we have to take another look at the medical and food industries, both of which have vested interests in the government's food and health policies.

The medical and pharmaceutical industries are primarily businesses, so they're more interested in profits than in a healthier nation. So it shouldn't be surprising that these corporations, along with the insurance industry, are among the top ten donors to US politicians.

Every year, these industries spend tens of millions of dollars promoting pro-industry candidates. These candidates push forward policies that will benefit the industry and help unseat the politicians that are against such policies. In this way, these corporations have a huge influence on the nation's health policies.

Money is also central to US health care. For years, the discussion has hinged on who is going to pay for the health-care system, and how much, whereas the discussion should really be about nutrition.

Corporate influence also extends to charities and foundations, which are often dependent on industry donations and therefore allow the corporations to influence the direction their research takes.

This is when reductionism comes back in. The foundations are often set up to fight the symptoms of a single disease, like cancer or MS, rather than fighting the causes behind it.

The corporations running the mainstream media also need to take more responsibility. There should be more reports on food and science, since popular opinion on these important issues is mostly guided by the media.

As it stands now, the media is only reinforcing the reductionist view by keeping the focus on the latest miracle cure, whether it's a fruit that supposedly cures cancer or a plant that fights depression.

One major step forward would be for the media to engage in scientifically accurate and unbiased journalism.

> _"Big Pharma, Big Insurance, and Big Medicine are among the biggest contributors to U.S. political candidates."_

### 9. Final summary 

The key message in this book:

**Our ideas about health are all wrong. We should focus on preventing diseases instead of curing their symptoms. And the way to do this is to switch to a whole-food, plant-based diet. Health and nutrition are complex subjects, and our current governments and health-care systems are not doing them justice.**

Actionable advice:

**Change your diet to help change national and international policies.**

Change from above is very difficult to bring about. Few people have access to the institutions that wield that kind of power. So the single best thing individuals can do to help catalyze change is to make some small but definite steps. Switching to a whole-food, plant-based diet may not transform the system overnight. But it's certainly a step in the right direction.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The China Study_** **by T. Colin Campbell and Thomas M. Campbell**

_The China Study_ (2005) is a controversial book that explores the connection between diet and disease, showing you how a diet high in animal-based proteins can lead to a host of health problems. Based on scientific data, these blinks explain why if you want to stay healthy, you should go vegan.
---

### T. Colin Campbell

T. Colin Campbell is a biochemist and expert on the subject of health and nutrition. He is also a Professor Emeritus of Cornell University and the celebrated, best-selling author of the highly influential book, _The China Study_.

