---
id: 56af455e377c160007000000
slug: whos-in-charge-en
published_date: 2016-02-01T00:00:00.000+00:00
author: Michael S. Gazzaniga
title: Who's in Charge
subtitle: Free Will and the Science of the Brain
main_color: 38BFF3
text_color: 1D6480
---

# Who's in Charge

_Free Will and the Science of the Brain_

**Michael S. Gazzaniga**

_Who's in Charge_ (2011) explains science's latest discoveries in how the human brain works and what such insight means for civil society at large. These blinks examine the concept of "free will" and how advances in neuroscience are also changing how we approach law and order.

---
### 1. What’s in it for me? Get an inside glimpse at how your conscience really works. 

Somewhere along our evolutionary path, humans developed a conscience, or the ability to separate right actions from wrong actions. This ability allowed early humans to live in larger groups, and cooperate in new ways. 

What we now call civilization would have been impossible without a conscience to help guide our behavior. Yet what exactly is this conscience, and how precisely does it work?

In the past 100 years, advances in psychology and neuroscience have shed much light on such questions, but have also raised new ones about how much control people actually have over personal decisions and behavior. In sum, who's _really_ in charge?

In these blinks, you'll learn

  * why we once thought a small creature controlled the human brain; 

  * how brain dysfunction may be at the core of many baffling diseases; and

  * how the modern legal system is essentially society's birth control.

### 2. While scientists have learned much about the brain, we’re still on the cusp of true understanding. 

It's commonly understood that consciousness and decision-making are in some way influenced by the brain. But how does the brain really work?

While the brain as the locus of consciousness has been a topic of study for thousands of years, it wasn't until recently that scientists understood the mechanisms behind it. For instance, in the sixteenth century, people widely believed in the idea of a _homunculus_, a miniscule creature that lived inside our heads, regulating the workings of the brain. 

And up until recently, scientists thought that if any part of the brain were damaged, another part would simply pick up the jobs performed by the damaged portion, essentially replacing it. However, as has been observed with people who suffer quadriplegia — the paralysis of all four limbs following brain damage — we know that this simply isn't the case, because otherwise they'd soon be moving around again.

While there have been plenty of misconceptions about how the brain works, the last few decades have witnessed tremendous discoveries. In the past half-century, scientists have found that contrary to earlier beliefs, the brain is composed of many circuits and areas that work in concert. 

What's more, we now know that particular parts of the brain actually specialize in certain tasks. 

The human brain is made up of a left and right hemisphere and a brain stem. Each hemisphere is responsible for certain parts of the body, and performs particular functions that the other half can't do. Meanwhile, local circuits, divided by defined areas such as the speech center, are always sharing information with one another, which allows the body to function and the mind to make decisions and develop. 

So even though scientists are only just beginning to assemble all of the brain's many puzzling pieces, we are closer today to understanding the human brain than we ever were before.

### 3. A particular part of your brain tells stories to make “sense” of information and inspire action. 

So the human brain is divided into different areas, each vying for attention and trying to be help your body function. But with so much going on, how is it that we individually feel like a single, coherent consciousness, and not a cacophony of competing voices?

This curious situation has everything to do with the _interpreter module_, a part of the brain that allows it to make sense of all the competing pieces of information to which we're constantly exposed. 

Here's how it works. The interpreter module sorts through information and, if anything appears to be missing or just doesn't make sense, the module generates a random story that makes everything nice and neat. 

For instance, _Capgras Syndrome_ is a curious condition in which a person misidentifies a familiar person as an identical-looking "impostor." A person with this condition may see his father, for example, but because he does not feel the normal emotional response elicited when recognizing his father, he believes that the person is instead an impostor. 

In this special case, the brain's interpreter module, deprived or lacking the necessary emotional information, simply generates a story that makes "sense" given the available stimulus. 

We can observe the brain's interpreter module in action when we consider things we do subconsciously. Imagine you're walking through high grass, when all of a sudden the grass in front of you moves oddly. You jump away, avoiding what you think might be a snake — only to realize it was the wind. 

This "jump-and-flee" reaction occurs instantaneously, and requires zero conscious thought. In fact, unconscious behavior is always rapid, as it's what was needed for early humans to survive in the wild. 

Nevertheless, our brain's interpreter module will try to come up with a plausible conscious explanation for even unconscious actions. For example, while walking through the grass, it may come up with a story about a poisonous snake, and that this triggered our need to seek a safe place. But the reality is that your brain made the decision for you, before you even had a single conscious thought.

### 4. Neither neuroscience nor philosophy have yet to decisively determine whether free will exists. 

It can be troubling to think that there is no such thing as free will, but given how the brain's interpreter module actually functions, this seems to be the case. Or is it? 

The brain's circuitry produces an action; and then, the brain's interpreter generates an explanation for the behavior. But this begs the question, what's really in charge up there in our heads?

The theories of determinism and free will have long been in disagreement. This is because the brain is a physical object, and therefore bound by the fundamental laws of physics. This logic causes determinists to say that we have zero responsibility for our actions and, given the right tools, future human behavior could be predicted as well as past behavior explained. 

On the other hand, the theory of free will says that humans have a conscience, thus we are mindful and can influence the actions we take and decisions we make. 

To decide which is correct, it's important to view the brain and conscience as interactive components. So instead of attempting to rationalize behavior through a bottom-up logic that validates deterministic claims of the physical brain being bound by physics, a better strategy is a top-down explanation of how conscience works. 

While the subconscious functions of the brain have an effect on free will, conscious thought can also influence the subconscious. This means there's constant feedback between the brain's conscious and subconscious levels — in other words, free will is possible. 

So accepting that the conscience is essentially greater than the sum of interactions between parts of the brain opens for us a path to understanding that indeed, humans have a say in what we think and do — and are therefore responsible for our actions. 

But transcending the physical realm makes matters even more complicated. That's because we now must decipher the social side of free will.

### 5. The concept and basis of morality is an innate human characteristic; all people share a similar moral code. 

People like company. You might feel lonely or sad when your friends are not around, or be overjoyed when you discover a certain person likes you. 

But why exactly is this the case?

Humans are inherently social beings. Over the course of human evolution, people began to form bonds with one another and live in ever larger social groups. 

But with larger groups came bigger social challenges. For example, people needed to learn to be responsible and fair. We needed to avoid committing crimes against each other, and work to protect our clan members from rivals. 

The result was gradual acquisition of responsibilities that led to the development of social skills, a greater capacity for language and the ability to understand emotions on a broader scale. 

Humans living in groups also began to shun violence committed against members of the same clan or tribe, instead directing aggression toward hunting or waging war against rivals who perhaps threatened necessary resources. 

Most if not all humans abhor violence and would much rather live in peace. And those people who exhibit violent, irrational or egoistic behavior usually do so because they lack or have lacked healthy relationships and a stable social environment. 

But being social wasn't enough to guarantee the survival of our species, as a key piece was missing: the development of _morals_.

Morals are in fact innate to each person; while people often differ in for example their religious beliefs, most people share a similar understanding on basic questions of morality. 

For instance, if asked whether an airplane pilot should make an emergency landing that could result in the death of five passengers or instead let the plane crash on its own, killing 200 people, most people would say the pilot should sacrifice a few lives to save the majority. 

While people all over the world maintain different religions or belief systems, at the core of every human brain is the inclination to resolve moral conflicts in a similar fashion.

### 6. A functioning legal system has worked to “domesticate” society and encourage peaceful interactions. 

Most people have the ability to distinguish between right and wrong. We might say killing a close friend is wrong, but killing an armed robber in self-defense is in contrast justified. 

But what are the deeper implications of moral codes and abiding by man-made laws?

The modern legal system may in fact be another aspect of human evolution, geared toward the creation of a peaceful society. By punishing violent or irresponsible behavior based on a set of codes and regulations, society sends a message about what citizens want or don't want in a community. 

As a result, once a person is branded a criminal, that person is stigmatized, may struggle to build relationships and find it difficult to hold a steady job. 

This means that over time, a legal system can actually result in people who are better able to adapt to society's social and legal rules reproducing at higher rates than those who can't or don't. 

In essence, the human race has "domesticated" itself over many generations by filtering out people who break the rules, thereby evolving toward a more peaceful society. 

As a result, only about 5 percent of all people today actively engage in violent behavior and pose continued resistance to a society based on an established set of laws. 

Neuroscience is also helping the legal system do its job better. In 2004, a convicted criminal on death row in Pennsylvania had his sentence overturned, following the results of a brain scan. 

It turned out that Simon Pirela had aberrations in his brain's frontal lobes, the sections responsible for judgment and impulse control. The scientific evidence was sufficient to convince a jury to throw out Pirela's death sentence. 

Yet the matter is far from simple. Not every criminal can now say that "my brain made me do it" and escape penalties. As a society, we need to ensure that scientific evidence in the courtroom is used judiciously and responsibly. Because even though we've made great strides, we are still discovering new things about how exactly the brain works in tandem with our conscience.

### 7. Final summary 

The key message in this book:

**The human brain has taken millennia to develop, driven by the body's instinct to survive and reproduce. Thus the questions of free will and consciousness are inextricable from human evolution. By understanding how people make the decisions they do, we can gain a deeper understanding and clearer perspective on what exactly drives human behavior.**

**Suggested** **further** **reading:** ** _Free Will_** **by Sam Harris**

In _Free Will_, author Sam Harris explains that the concept of "free will" is essentially an illusion. While it might be hard to believe, what we think and what we do lies mostly out of our direct control. This book explains why exactly this is, and what implications it has for society at large.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael S. Gazzaniga

Michael S. Gazzaniga is the director of the SAGE Center for the Study of the Mind at the University of California at Santa Barbara, the president of the Cognitive Neuroscience Institute and the founding director of the MacArthur Foundation's Law and Neuroscience Project.

