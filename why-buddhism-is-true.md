---
id: 5a439a31b238e10007128c24
slug: why-buddhism-is-true-en
published_date: 2018-01-05T00:00:00.000+00:00
author: Robert Wright
title: Why Buddhism Is True
subtitle: The Science and Philosophy of Meditation and Enlightenment
main_color: F7C531
text_color: 856A1B
---

# Why Buddhism Is True

_The Science and Philosophy of Meditation and Enlightenment_

**Robert Wright**

_Why Buddhism Is True_ (2017) takes a scientific look at the teachings and meditative practices of Buddhism. Robert Wright presents an impressive and surprising amount of data and research, all of which suggests that even Buddhism's more esoteric teachings may have a solid basis in science.

---
### 1. What’s in it for me? Enlighten yourself with science-backed Buddhist insights. 

The modern human mind is an incredible thing. It enables us to communicate, create, analyze and plan, and it has evolved these abilities so that we can successfully navigate the world; so that we can find a mate and propagate our genes, for instance, and so that we can predict and avoid danger.

But if survival is the benefit of having a mind that is constantly on the lookout for trouble, what do you suppose is the cost? You guessed it: We spend a lot of time being hyperaware, overthinking things and worrying.

So what do we do with a mind that's programmed to assess, judge and take fright? These blinks offer an answer: try Buddhism. The many teachings of Buddhism serve as not only inspiration for the worn down and stressed out; they're supported by modern scientific evidence. So discover what Buddhism truly is and what it can do for you.

This blinks also explain

  * why the anticipation of pleasure is more alluring than pleasure itself;

  * why popular ideas of the self are an illusion; and

  * how to make the noise of construction work sound like music.

### 2. Life is full of delusions that evolution has hardwired into the human brain. 

In the modern sci-fi classic, _The Matrix_, our protagonist, Neo, discovers that the life he's been living has all been an illusion — nothing more than a bunch of ones and zeroes.

One of the reasons the movie became a hit is that it touches on some highly relevant themes, including how modern society has "programmed" us to pursue delusional goals.

Ask yourself: Are you in complete rational control over your life and your desires?

How about with sweets and processed foods? A lot of people are aware that sugary foods and drinks are bad for them but they succumb to cravings or fool themselves into thinking that junk food is a well-deserved reward. Most of the time, however, the initial satisfaction of indulging in junk food is quickly replaced by feelings of guilt or shame about having eaten something you know is bad.

So why do we keep falling into these traps? One reason is evolution.

We all have basic instincts related to food, sex, popularity and competition, because these were the things that helped our ancestors survive and pass their genes onto the next generation. Each one of them triggers a strong response in the pleasure center of our brain, which releases the neurochemical _dopamine_ every time we taste something sweet or win a competition.

The problem is, our anticipation of pleasurable rewards like sweet flavors and winning can outweigh the rewards themselves.

In one study, monkeys were given some sweet juice, which was observed to trigger the release of dopamine in their brains. Researchers then started turning on a light right before giving the monkeys more juice. The first few times, the juice triggered the release of the same amount of dopamine. But, soon enough, more dopamine was being released when the monkeys saw the light go on than when they actually had the treat. Anticipation of a reward, the researchers concluded, is more pleasurable than the reward itself.

It's believed that we're wired this way so that we'll keep searching for and discovering new and better things, but this wiring has also led to a lot of destructive-compulsive behavior.

But thankfully, we have Buddhism to restore a healthy balance to our lives.

### 3. To follow Buddhist teachings, we should avoid focusing on things we have no control over. 

If you're like most people, you've probably spent a good amount of time worrying about two things: how you look and how smart you are. But here's the thing, these are rather pointless concerns.

Buddhism teaches us that matters of the mind and body are not true concerns of the _Self_.

In one of Buddhism's foundational texts, the Buddha asks his monks to search within and find the qualities that can truly be considered part of the "Self" — that is, the qualities that we have total control over.

You might think body and mind would fall under this category, but the body can get sick and deteriorate against our will — just as the mind can deteriorate or give us thoughts and emotions we'd rather not have.

We normally consider the body, mind and spirit to be the very core of our Self, but since none of these things are really under our control, the Buddha concludes that none of these things should be considered the Self.

This doesn't mean that there is no Self, but that the Self is mere consciousness. Although this consciousness inhabits a body, it isn't limited to any single body, and it's free of all attachments. It isn't comprised of the ego-laden attributes we add to it that make us obsessed with appearance, material things and intellectual achievements.

The things you seem to own — whether a nice house, a handsome face or a sharp mind — are all illusory. However, we _can_ attain the aforementioned state of pure consciousness.

It might be uncomfortable to so drastically change the way you think about your Self. But it's a theory of reality that's backed up by science, which is what we'll explore in the next blink.

### 4. Science supports the Buddhist view that reality is subjective and our senses are unreliable. 

We tend to think of science in terms of absolute truths. But relatively recently, with developments like quantum theory, science is beginning to view reality as subjective.

This is how science has come to support the Buddhist point of view that what the brain perceives isn't necessarily a reality.

The most famous scientific experiment in this regard is known as the "split-brain" experiment, and it calls into question the reliability of our brain's sensory input.

The name comes from the participants of the experiment: people who either had brain damage or had undergone surgery that separated the hemispheres of their brain. When such people look at something with one eye, only one hemisphere of their brain is engaged — the hemisphere associated with the perceiving eye. Interestingly, however, the unengaged hemisphere will still fabricate and believe a story about what's going on, though it has no idea what the eye is seeing.

For example, participants had their right hemisphere visually engaged with a unique prompt that told them to stand up and start walking. When they did, the participant was asked to explain why they were walking. Such explanations are the domain of the left hemisphere, which, in this case, had no recollection of the prompt. Nevertheless, the participants always concocted some explanation, often a complex narrative that had no relation to what actually occurred.

Then there was the pantyhose experiment, where psychologists Richard Nisbett and Timothy Wilson offered shoppers a selection of pantyhose to choose from. Participants almost always picked whichever pantyhose were positioned on their far right-hand side.

However, when asked to explain the reason for their choice, the participants would say it was due to the superior quality, the color or the fabric. Yet, in reality, all the pantyhose were identical! Clearly, our minds have a strong capacity for self-delusion and making up their own reality in a rather convincing manner.

In the next blink, we'll look at the transformative power of emotions and how they can change your perspective.

### 5. Buddhism can help prevent our emotions from taking over. 

Like _The Matrix_, the story of Dr. Jekyll and Mr. Hyde is a sci-fi allegory about human nature. But unlike Dr. Jekyll, who concocts a potion that turns him into the young, sociopathic Mr. Hyde, we don't need a laboratory full of bubbling beakers and dark experiments in order to transform into someone else. All we need are a few strong emotions.

One of the strongest, and most transformative, emotions is jealousy.

During the 1980s and 1990s, evolutionary psychologists Leda Cosmides and John Tooby studied the effects of jealousy and revealed just how much this emotion can change people.

When under its influence, reasonable people suddenly had the potential for violence and the desire to dish out revenge and punishment. They also mistrusted people more, and would change their appearance in an attempt to seduce others. These actions can all be scientifically explained as instinctual responses, but they also reinforce the Buddhist belief that we have very little control over ourselves.

So if you'd prefer to be in control of your emotions, rather than the other way around, Buddhism can help you take appropriate action in the form of mindfulness practices.

The aim of these practices isn't to repress emotions or become an unfeeling robot. The goal is to stop acting on negative feelings — to stop believing that they accurately reflect who you really are.

After some time devoted to mindfulness practice, you can learn to observe feelings without acting upon them. So instead of immediately honoring a feeling of resentment and yelling at your partner, you can recognize the feeling as a temporary emotion and work with your partner to understand where it came from and how it can perhaps be avoided in the future.

Without mindfulness, emotions are often given far more power than they deserve. In the next blink, we'll look at what science has to say about these pesky feelings.

> _"Whenever people are throwing things and screaming, that's a tipoff that the brain is under new management."_

### 6. To control our impulses, we need to reframe our thinking. 

The eighteenth century philosopher David Hume famously said that our ability to reason is rather weak when compared to our willingness to act on our passions.

Buddhism recognized this imbalance long ago and, much later, science has come up with some explanations for it.

In 2007, a study was conducted at Stanford University to help explain why people make so many impulsive purchases, rather than buying a product after careful evaluation. The results showed that certain purchase decisions — such as whether or not to buy a _Star Wars_ DVD — are controlled by the same parts of the brain that relate to emotions.

The _nucleus accumbens_ portion of the brain is associated with pleasure. In the above experiment, the activity in this area, or lack thereof, turned out to be the best predictor of whether a shopper would purchase something or not. If the area showed high activity when a shopper saw the cover of the _Star Wars_ DVD, it was quite likely they'd buy it.

Once we recognize the power emotions have over our decisions, we can start to improve our self-control by applying our ability to reason in an emotional way.

For example, let's say you have trouble controlling yourself around ice cream.

You might have tried to use reason and thought, "Ice cream is causing me to gain weight; thus, I need to cut it from my diet." But chances are this didn't work.

So try to use reason in an emotional and meditative way instead. Take a moment to visualize yourself after having cut ice cream from your diet. You're slimmer, full of energy and feeling much better about yourself since you can wear your favorite pair of jeans again.

Using reason paired with emotion will almost always be more effective than trying to apply pure reason alone.

Both Buddhism and neuroscience offer examples of how we can benefit from reframing the way we think, and in the next blink we'll look at another powerful example of the Buddhist perspective.

### 7. We can use our ability to construct our reality in a positive way. 

If you're at all familiar with Buddhism, you're probably familiar with the concept of "mind over matter."

Most of us will spend time imagining a scenario that doesn't reflect reality. Maybe you've entertained yourself during a long wait at the airport by inventing funny backstories for the strangers around you, or maybe you've imagined yourself sitting in an igloo to get through a particularly hot day.

Since life is all about how we perceive experiences through our senses, it's entirely possible to change the way we interpret our perceptions, especially when the current experience is less than ideal.

When the author was on a meditation retreat, his experience was far from peaceful since there was a constant sound of an electric saw buzzing away outside his room. It was all rather intolerable until his mindfulness teacher reminded him that a big part of Buddhism is about accepting your current situation as it is.

But this doesn't mean you can't change how you _perceive_ a situation. So the author let go of the idea that the sound was annoying and began to perceive the sound as music. After all, his perception of the sound as an annoyance was just as much a perceptual construction or interpretation as any other.

It's common for us to construct stories about our lives, but just like they can help us, they can also be particularly unhelpful when they cause us to suffer.

Even if you're on vacation, you can make yourself miserable if you decide the trip was a huge mistake and that you'd rather be home watching the football game.

But these kinds of negative stories are unnecessary since it's impossible to know for sure how some other scenario would have turned out. First of all, you don't know if your vacation won't result in a life-changing positive experience. Furthermore, you can't be certain that staying at home wouldn't have resulted in some unforeseeable mishap.

So don't make your life harder by dreaming up pessimistic what-if scenarios! Instead, enjoy the present and don't miss out on the chance to create amazing new memories.

### 8. Meditation can change our consciousness and give us a deeper appreciation of life. 

If you've been to a Buddhist monastery, you know that these havens can feel like a world unto themselves, especially when compared to today's hectic, fast-paced lifestyle. But it's not all about the monasteries. Experienced meditators live in a different world, no matter where they are.

More precisely, brain scans reveal that experienced meditators enjoy a different consciousness than the average person.

Gary Weber is one such meditator. Scans of his brain reveal that it has much calmer activity than a "normal" brain — so much so that unless he was focused on performing a task, his brain was revealed to be in a constant state of rest.

So how does this brain see the world around? Weber describes what he sees as an "empty fullness," which takes into consideration the vast spaces, ignored by most people, that exist between objects.

A mind like Weber's, trained in Buddhist teachings, will recognize the difference between objects like a chair and a spoon, but instead of being isolated, each object is connected to the same space that everyone else is living in. To the experienced meditator, the space unites us all, which makes it just as important as the objects within it.

Through his devoted practice, Weber has also altered his consciousness so that his thoughts are devoid of self-conscious worries, such as whether or not he said the right thing. Since Weber is emotionally removed from such concerns, you might think a calm mind is not unlike a bored mind, but this is far from the case.

Think of it this way: If you're a wine expert who's tasting a sample of wine, you might not enjoy the flavor if your primary concern is to impress your fellow critics with your opinion about the wine. When a Buddhist mind has a sip of wine, there are no expectations or self-conscious worries. Skilled meditators live purely in the moment, so they'll enjoy each sip of wine like it's the very first time they've experienced it.

Enlightenment like this is far from boring, and it's not as mysterious as you might think. All it takes is disciplined practice and you'll eventually start to see things in a clearer and calmer light.

### 9. Final summary 

The key message in this book:

**Buddhism and its advice is largely supported by science. By practicing mindfulness, meditation and reframing our thinking — key tenets of Buddhism whose effectiveness are backed by psychology and neurology — we can improve our relationship to the world and enjoy the more peaceful existence so many of us crave.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Start Where You Are_** **by Pema Chödrön**

_Start Where You Are_ (1994) is an enlightening guide to opening up your heart and mind and learning to feel happier in your skin. Discover the practices that bring calm and serenity to Buddhist monks and nuns, as well as the philosophy that puts people on the path to nirvana. This isn't advice about what incense and candles to buy; it shows you how to look deep within yourself to confront your demons and find strength in your weaknesses.
---

### Robert Wright

Robert Wright is a best-selling author and esteemed journalist whose writing has appeared in numerous publications, including the _New Yorker_ and the _Atlantic._ He's taught classes at the University of Pennsylvania and Princeton University, as well as New York's Union Theological Seminary. His 2009 book, _The Evolution of God_, was shortlisted for the Pulitzer Prize.

