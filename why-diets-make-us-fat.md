---
id: 5852ce2458b1350004ed16fa
slug: why-diets-make-us-fat-en
published_date: 2017-01-04T00:00:00.000+00:00
author: Sandra Aamodt, PhD
title: Why Diets Make Us Fat
subtitle: The Unintended Consequences of Our Obsession With Weight Loss
main_color: 3FACBC
text_color: 2E7E8A
---

# Why Diets Make Us Fat

_The Unintended Consequences of Our Obsession With Weight Loss_

**Sandra Aamodt, PhD**

Why Diets Make Us Fat (2016) demolishes many popular myths about diets and the relationship between weight and health. These blinks explore the science behind claims that dieting just doesn't work and offer alternatives for people seeking to shed a few pounds and live more healthily.

---
### 1. What’s in it for me? Discover why everything you’ve heard about dieting is wrong. 

We're constantly bombarded with trendy new diets. But whether you decide to become a fruitarian, count calories obsessively or limit yourself to the spartan sustenance of our stone age forebears, you may become disappointed when you realize that few diets give you the results you were hoping for.

While diets can help you shave off a few pounds in the short term, they rarely deliver the goods in the long run. We now have a much better understanding of why this is, thanks to advances in neuroscience, genetics and psychology.

As these blinks will show you, if your goal is to live and eat healthier, then simply focusing on weight loss is not the way to go. So let's dive into some of the popular myths about diets.

In these blinks, you'll learn

  * how evolution made gaining weight easier than losing it;

  * why willpower isn't that important to eating more healthily; and

  * how exercise might one day save your life.

### 2. Why do you gain weight again after losing it? Because your brain thinks you’re starving. 

If a woman of average height who weighs 110 pounds lost 30 pounds, most people would agree she was too skinny and possibly malnourished. Yet if a woman who weighs 230 pounds lost 30 pounds, many people would congratulate her for shedding the excess weight.

To society, there's a big difference in experience between these two women on a diet. Yet to the human brain, these scenarios are interpreted as dangerous: in both cases, the body thinks it's starving.

Your body has evolved to keep your weight within a _defended range_ ; that is, your average weight plus or minus 10 or 15 pounds.

It's relatively easy to achieve weight changes within your defended range through modifications in diet and exercise. The range itself can also shift, but it's important to keep in mind that it's far easier to raise the range than it is to lower it.

So while you can lose weight, once you drop below your defended range _,_ your body will work hard to get you back to that level.

Why is this the case? Your brain maintains a highly effective _energy-balance system,_ which automatically keeps your weight in its defended range by keeping the calories you burn close or equal to the calories you consume.

This energy-balance system, however, can easily be upset by the body's _reward system_. When you do something that helps you survive — for example, eat a calorie-rich hamburger — your body sends your brain a shot of the feel-good neurotransmitter _dopamine_.

Such a system was beneficial for our hunter-gatherer ancestors. When they found a stash of honeycomb or made a big kill, for instance, the body's reward system compelled them to overeat, which made sense for survival, as the next meal might not come for a while.

Today, however, this system doesn't work in our favor. With constant access to high-fat, sugary foods, we can too easily overeat, which pushes our bodies' reward system into a feel-good loop.

As a reaction to our overeating, we often put ourselves on diets. Yet, research suggests that if we diet repeatedly, it can cause changes in the brain, making the reward response even stronger, and resulting in even more indulgence.

### 3. Willpower plays a much smaller role in healthy eating than most dieters believe. 

So is willpower the solution for keeping the body's reward system at bay amid a world full of sugary temptation?

The idea that a dieter must have a will of steel is a common dieting myth. Yet willpower isn't suited to battling the constant allure of tasty, calorie-rich treats.

Willpower is good when channeled to achieve short- or medium-term goals, but it's ineffective at meeting long-term goals.

A short-term goal might span a few hours, days or months, and involve tasks such as cooking a complicated recipe or finishing a work assignment.

Long-term goals, on the other hand, involve repetitive actions over the span of years — so using willpower to fight cravings continually is ineffective, because your willpower will no doubt wane before your dieting goals have been met.

In short, willpower is a finite resource that's needed to guide many human behaviors. Moreover, once you've depleted your store of willpower, it needs time to recharge.

Expending willpower to drag yourself to the gym, for instance, means you're going to have less of it when you get home and come face-to-face with that leftover chocolate cake.

Willpower has different levels of efficacy for different activities, too. A wide-ranging review of studies found that willpower was the least effective when making decisions about food.

On the other hand, willpower is great at regulating _automatic behaviors_, otherwise known as habits.

As we'll explore in the next blinks, it's far more effective to form good eating habits than to try and use willpower to fight food cravings.

Once you use up your willpower, you inevitably revert to old habits. Then it's a matter of whether your habits include going for a jog or finding comfort in a pint of chocolate ice cream.

### 4. Weight discrimination and unrealistic body images can lead to dire health consequences. 

Has anyone ever made a comment on your weight, insinuating that it wouldn't hurt if you lost a few pounds? If so, you've experienced a form of weight discrimination known as _fat shaming_.

Fat shaming hinders rather than helps — instead of encouraging people to lose weight, it more often leads to further weight gain and continued unhealthy eating habits.

That's because people who feel bad about themselves are more likely to binge. They're also less likely to exercise, since they don't identify themselves as fit, active individuals.

Children and teenagers are especially vulnerable to fat shaming. A study of 2,500 American girls who were fat shamed found that these girls were twice as likely to gain weight within the next five years than those who weren't criticized about their weight.

Another result of the pervasiveness of unrealistic body images in the media is mental health issues related to weight.

One study looking at the relationship between body image and media distortions was conducted in Fiji. In the 1970s, few women in this island nation suffered from an eating disorder or dieted; in fact, a full-figured Fijian woman was seen as ideal.

In 1995, researchers examined the impact of the introduction of satellite television to Fiji. They suspected that foreign commercials and TV shows depicting a Western ideal of beauty would somehow affect how local young women saw themselves.

Researchers organized focus groups in the province of Nadroga, first in 1995 with a group of 17-year-old girls, and again in 1998 with another group of girls.

While the girls' average weight remained steady between the two groups, researchers found that their relationship to food and their bodies had changed dramatically.

In just three years, the number of girls who said they suffered from an eating disorder such as anorexia or bulimia jumped from 13 to 29 percent. The modern Western ideal of slimness had overtaken Fijian culture, with a whopping 74 percent of girls reporting feeling "too large."

> _"A woman would need a rib removed to achieve the waist-to-hip ratio of the current Barbie doll."_

### 5. Genetics plays a large part in why people put on weight and how much weight one gains. 

Have you ever noticed how it's easy to put on weight but difficult to lose it? The reason for this can be traced back hundreds of thousands of years to the first modern humans.

Early humans evolved to cope with famine, which is why our bodies are much better at hanging on to weight than shedding it.

Over some 200,000 years of modern human anatomical development, it's only in the last century that food has become something that is reliably and regularly available for the majority of people.

Before that, gathering food and evading predators required activities such as hunting and gathering, which made it difficult for early humans to overeat. The constant threat of starvation also meant that the genes that encourage weight loss were essentially undesirable and were gradually filtered out through evolution.

A person's genetic makeup also plays a large part in their predisposition for obesity, and even the subtlest differences in gene expression can lead to huge variations in weight gain.

A study conducted by Claude Bouchard and Angelo Tremblay, for instance, took pairs of identical and non-identical twins and had them consume an extra 1,000 calories per day for three months.

While the non-identical twin pairs differed in weight gain by as much as 28 pounds and as little as eight, the identical twins gained the same amounts of weight.

In another study that explored weight loss rather than weight gain, a group of identical twins burned 1,000 more calories a day than they normally did for three months. Twin pairs aligned even more closely in weight loss than in weight gain shown in the previous study — again demonstrating the power of genetic makeup when it comes to body weight.

And in a further example of how resistant bodies can be to weight loss, some participants lost just two pounds at the end of three months.

So, if you want to find out how predisposed you are to gaining weight, you can either take a range of expensive genetic tests or look at the waistlines of family members at the next reunion!

### 6. By paying more attention to the food you eat, you’ll enjoy food more and eat less of it. 

Get ready to tune out all the dieticians and health gurus who claim to have found the perfect path to weight loss: the best guide to eating healthily is listening to yourself.

_Mindfulness_, the practice of staying present in the moment, can help you become a smarter eater.

When applied to food, _mindful eating_ is about being fully aware of the dietary decisions you make and the actions you take concerning food consumption.

Instead of relying on diets and external cues, such as the temptation of junk food at the local convenience store, mindful eating will help you listen to your body's internal hunger signals.

To know what your body needs, you'll have to tune into the satiety signals from your energy-balance system _._ You can trust it — this internal system has worked for us since the dawn of humanity, long before the emergence of trends such as the Atkins or South Beach diets.

Here are a few techniques to start eating mindfully, which is the natural path to becoming fitter, thinner and less susceptible to eating disorders.

First, sit down for a meal when you're only moderately hungry. If you're famished, you're much more likely to eat too much too fast, and ignore signals from your body.

Second, eat at a slower pace. Put your utensil down between bites and savor the bite in your mouth instead of focusing on the next one.

Finally, eat only when you are completely free from distractions. Instead of checking your phone or sitting at your desk, engage fully with your food to enjoy it.

### 7. Being healthy and being thin are not as closely related as you might think. 

People often feel healthier after they've started dieting and exercising, and often attribute this to the weight loss alone. In truth, weight loss is often just a temporary side effect, because physical activity is what makes you feel truly good.

Regular exercise is a much better indicator of health than weight, because no matter how much people weigh, active people are simply healthier.

Time and time again, studies show that when it comes to predicting early deaths, exercise levels are ten times more indicative than measures of obesity.

The first study to analyze the importance of weight versus exercise was carried out on London transport employees in 1949. British doctor Jeremy Morris collected data from 31,000 men who were either bus drivers or bus conductors.

Predictably, drivers didn't get much physical activity, as they sat in the driver's seat most of the time. Conductors, on the other hand, climbed nearly 500 steps a day while collecting fares on London's double-decker buses.

The studies found that conductors were _30 percent_ less likely to have a heart attack. Furthermore, the heart attacks they did suffer from were less life-threatening.

But what if being active simply made conductors slimmer and thus healthier? In a clever twist to the study, Morris also compared employees' waistlines with the potential risk of heart attack.

Indeed, conductors were thinner, but Morris also discovered that even the heaviest conductors were still less likely to suffer a heart attack than were the sedentary drivers.

The fact that a healthy body does not necessarily equal a thin body is especially true for the elderly.

Once you pass the age of 60, it becomes more dangerous to weigh too little than to weigh too much. Older people are more prone to serious illnesses, and a certain level of fat can act as a buffer against debilitating weight loss that can stem from a prolonged illness.

Therefore, to be as healthy as possible, it's better to focus on exercise over weight loss.

### 8. Eliminate bad habits and form good ones for a healthier life. 

Doing something over and over will soon make that action become second nature. Such a habit is great when it comes to looking both ways before crossing a road, but bad if it means you beeline for the freezer and your favorite ice cream whenever you feel stressed.

So instead of straining your willpower, cultivate positive habits to maintain a healthy lifestyle.

Over half the things you do on a regular basis are automatic habits. The more you do something, the harder it is _not_ to do it again. If you build positive habits, you can use your limited willpower for other things.

How long does it take to establish a habit? People often cite 21 days, but this is a minimum. In fact, the time it takes to develop a habit varies from situation to situation, but on average, it takes around two months for an action to become ingrained.

There are steps you can take to ensure good habits are formed successfully.

First, make sure your goal is realistic. Setting unrealistic goals is a surefire way to disappoint yourself and fall off the wagon. Instead of setting a goal to jog five times a week, try resolving to take a walk three times a week.

Second, be consistent. Missing one day isn't disastrous, but missing a week will throw you off course. One tip for maintaining a routine is to keep a record of the days you've completed.

Third, remember that it's easier to break bad habits by replacing them with positive ones.

All habits involve doing something, so you can't replace them by doing nothing instead. For example, if you like to snack on salty chips, it won't be enough to not eat them — instead, try replacing the chips with fresh fruit or nuts.

Remember: when you change your habits, you change your life!

### 9. Final summary 

The key message in this book:

**Your body is much better at regulating your weight than any diet. If you learn to be responsive to your body's hunger cues and exercise regularly, you'll be on the right path toward better health.**

Actionable Advice

**Select one healthy habit and cultivate it slowly.**

Don't change your diet, exercise regime and television schedule all at the same time; that's too much of a shock to your limited stores of willpower. If there's one habit that will improve your life today and for the rest of your days, it's being physically active!

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Why We Get Fat_** **by Gary Taubes**

_Why We Get Fat_ explains why certain types of carbohydrates are the main reason we get fat. The book not only shows why people gain weight, but why the topic is so controversial. It also talks about why some people get fat and others do not, the role genetic predispositions play in this process, and which foods we should all avoid.
---

### Sandra Aamodt, PhD

Sandra Aamodt is a neuroscientist and popular science writer. She is the former editor-in-chief of _Nature Neuroscience_ and coauthor of the books _Welcome to Your Brain_ and _Welcome to Your Child's Brain._

