---
id: 5746d244b49ff200039c4da1
slug: why-everyone-else-is-a-hypocrite-en
published_date: 2016-06-01T00:00:00.000+00:00
author: Robert Kurzban
title: Why Everyone (Else) Is a Hypocrite
subtitle: Evolution and the Modular Mind
main_color: B9522A
text_color: A14724
---

# Why Everyone (Else) Is a Hypocrite

_Evolution and the Modular Mind_

**Robert Kurzban**

_Why Everyone (Else) Is a Hypocrite_ (2010) pushes us to challenge our assumptions about the human brain. These blinks explain the modular structure of our mind which, rather than creating a coherent conscious self, can lead to confusion and conflict as evolutionary traits clash with the challenges of the modern world.

---
### 1. What’s in it for me? Get to know your hypocrisy-prone brain better. 

Who's a hypocrite? The neighbor who yells at you when you host a party but then plays loud music all night himself? Or the politician who pretends to espouse green values to secure more votes in an election but then completely ignores environmental values during her term?

These blinks examine the latest findings in evolutionary psychology, as groundbreaking studies now demonstrate that perhaps we're all hard-wired hypocrites in our brains, if not in our hearts.

In these blinks, you'll also discover

  * why your brain is like a smartphone overloaded with apps;

  * why sometimes a butane torch seems a better tool for toast than a toaster; and

  * why thinking you're a good driver even after a dumb accident is still a smart idea.

### 2. Our brain is essentially an information processor that has been shaped through evolution. 

There's nothing more fascinating or mysterious than the human brain.

How exactly _did_ we end up with such an organ, capable of composing symphonies, planning megacities and speculating about parallel universes?

Some of us believe the brain must have been _designed_ by one deity or another. Evolutionary psychology, however, has an alternative explanation. In this field, the human brain isn't considered a product of divine inspiration. Instead, it's simply a biological information processor.

Much like laptops and smartphones, the human brain runs programs to complete certain tasks. The difference is that instead of silicon chips, this machine is made of neurons along which electrical impulses travel at incredible speeds.

Of course, our brains weren't always as advanced as they are now. Just as computers have evolved from huge machines that filled rooms to astonishingly thin devices that fit in a handbag, the human brain has developed considerably over time.

Familiar with Darwin's theory of natural selection? Let's recap it briefly: an organism has genes, and these genes express themselves as physical traits, such as a giraffe's long neck or a human's sophisticated brain.

Organisms with traits that allow them to adapt to an environment — such as the ability to find food, create a shelter and live in communities — are more likely to survive than organisms without them. It was this process that shaped the complexities of our brains. The way our brains are structured is a result of the challenges faced by our primitive ancestors.

And just how are our brains structured? Philosophers would have you believe that a human has a rational mind, with the idea of a single, distinct "self" and a more or less coherent set of beliefs.

The truth is that human minds are _nothing like this_. Evolution has given us a different kind of mind. To understand the brain better, let's consider what it was at the beginning: a simple tool.

### 3. Specialized tools make our lives easier, but sometimes they’re too specific to be helpful. 

Do you make toast with a butane torch? Not a civilized way to start your morning, is it?

Chances are you prefer to use a toaster. This, granted, is common sense, but it's also something particularly human. When you have a specific task to perform, whether cooking breakfast or solving an equation, you often turn to a specialized tool to make the job easier.

_Specialization_ is what happens when we create devices that are suited for a given task, making our actions more efficient. A toaster is a tool designed specifically for the task of toasting bread. Its slots are just the right size for holding one slice of bread; the machine itself is thermally insulated to ensure no heat is lost during the toasting process.

A percentage calculator app is another good example of a tool that incorporates specialized programming, allowing you to calculate percentages faster. Specialized tools such as these have been created to perform a certain task as effectively as possible, with minimal effort, cost and time.

Now let's say you're on a camping trip. You need to make toast and boil water, but you've only brought your toaster along. Perhaps here that butane torch might have been a little more helpful! Or perhaps you need to solve equations for your trigonometry class — that percentage calculator won't be much help.

In such cases, you need a device suitable for handling more than one task. Unfortunately, truly efficient, multipurpose tools are hard to find; they can't be too general, but they also shouldn't be too particular.

Luckily, there is one way to combine generalization with specialization to get the best of both worlds. How? We'll find out in the next blink.

### 4. The human brain manages complex tasks by flexibly combining specialized tools or modules. 

Let's travel back in time to the earth's most recent ice age. Saber-toothed tigers prowl around, and as an early human, you're a survivalist. Right now, you're hungry—– but not as hungry as the tigers are.

In this situation, would you rather have great vision, allowing you to spot predators from a distance, or perhaps great navigational skills, enabling you to quickly figure out an escape route?

Ideally, you'd have both. For our ancestors to survive situations like this, they needed a brain that had the _flexibility_ to tackle a range of specialized tasks. They not only needed to notice a tiger from a distance but also needed to create a mental map of the surroundings so that when a predator got too close, they could run to the forest to slow the chase.

So how does the human mind work as both a specialized and flexible tool?

Consider your smartphone. You've probably downloaded plenty of specialized apps to it. But is your smartphone itself a specialized tool? Nope — with its dozens of capabilities, the smartphone is a _multipurpose tool_.

By combining specialized apps with the ability to switch between them, a smartphone is a rather neat analogy for the way the human brain works.

One way to describe this multifunctional yet flexible system is _modular_. Our brains are made up of modules that are specialized to tackle particular tasks, such as recognizing faces. By bundling modules together, the brain can perform different tasks in series or even at the same time, switching between groups of tasks all the while.

> _"Big brains get smart by having a lot of pieces."_

### 5. You are neither the manager nor the conscious observer of your brain’s modular system. 

People seem to readily believe that the human mind is centered on the concept of a coherent self, where this self is something like a manager or supervisor of your mind.

This concept could make sense if we consider the brain's modularity, right? Some "thing" has to watch over the brain's specialized apps (or modules) to ensure everything runs smoothly.

Well, not really.

The fact is, you're _not_ the manager of your mind's modules. It's clear why this is the case. A manager of just about anything needs a brain to do the job. But if your brain is the manager, then it would need a brain of its own, which has modules, which too would need a manager, with a brain, and so on.

You see the dilemma. The bottom line is that no superior module manages all the modules in your brain. All modules work together _of their own accord_.

But what about consciousness? Certainly there are modules that are conscious. Such conscious modules would seem good candidates for a module manager job — but here too, you'd be mistaken.

The brain's many unconscious modules perform tasks without our conscious modules being aware of them, let alone directing them. This is why we sometimes fear things or love things without understanding why.

For example, you might feel paralyzed with fear of falling from a building even when you're perfectly safe, seated on a chair behind a large fence. And you might love a certain pop song, even though you know it's utter garbage!

In short, there is no manager hovering over the modules in your brain. Instead, you _are_ those modules.

> _"Nobody has a soul-like agency inside them; we just find it useful to imagine the existence of this conscious inner 'I' …"_ –Daniel Dennett

### 6. Different modules in the brain create conflict, which you experience as confusion. 

Did you ever play with Matchbox cars as a child? Thinking about the action of those small cars will help you understand how the brain's modules interact.

Cyberneticist Valentino Braitenberg used the concept of small "vehicles" in the 1980s as the basis for an insightful thought experiment that reveals the functional challenges of the modular brain, even with only two modules.

Let's imagine that the "vehicle" we're talking about has a few interesting features. First, it has a little motor with a heat sensor, programmed to move away from hot objects. It also has a light sensor, programmed to move toward light sources.

Let's now call these features the "heat-avoiding module" and the "light-seeking module," respectively.

Granted, this is a simple example. But even such simple modules can create conflict. Consider the dilemma: should our vehicle head toward or away from a fire?

Such is the nature of modular systems. The range and number of specialized modules up the potential for conflict and confusion.

You've experienced this personally, for example when you couldn't decide whether to accept a second slice of chocolate cake or refuse it to watch your weight. This represents a conflict between brain modules programmed to seek instant gratification and others focused on achieving long-term benefits.

Further conflicts arise between modules that urge you to avoid confrontation while others pressure you to stand your ground and fight. Even modules that push for generous actions are in conflict with those geared toward being selfish.

In this way, our modular brains are well adapted to cope with life's complex challenges, except that from time to time, the brain can become confused and fraught with indecision.

> _"Do I contradict myself? Very well, then, I contradict myself, I am large, I contain multitudes."_ –Walt Whitman, "Song of Myself"

### 7. Your brain likes to think that you’re faster, smarter and perhaps more talented than you really are. 

Think you're a good driver? Perhaps better than most other drivers on the road? You're not the only person with that opinion.

Studies show that most people consider themselves above-average drivers. The thing is, everyone can't be above average. So why do we think we're all so special? Blame the brain's modules!

Some of the brain's modules overestimate performance when comparing performance to others, a fact that has been proven in many psychological studies.

In these studies, participants were asked how they ranked themselves in comparison to an average person in many aspects, from intelligence to sporting ability to leadership skills. Across the board, participants rated themselves as above average. Even drivers who'd injured themselves after driving into stationary objects still considered their driving ability "excellent."

As well as overestimating talents, some of our brain's modules overestimate the control we have over a given situation. Sociologist James Henslin studied a group of cab drivers who liked to gamble. He discovered that these otherwise pragmatic men were convinced that whispering to a die or throwing dice with extra force would somehow help them roll a higher number.

There are, however, also brain modules that encourage realistic thinking by drawing attention to the facts of a given situation. In contrast to the superstitious behavior of the cab drivers, behavior driven by this module is what gives people the semblance of being rational actors.

But the human tendency toward inaccurate or misguided judgment is what plays the central role in the brain. Hang on — how could this irrational trait have possibly helped our ancestors survive?

### 8. Overconfidence was an evolutionary advantage, as by displaying value we could stay in the group. 

Why on earth have some of the brain's modules adapted to make people think too highly of themselves?

Doesn't such misguided behavior potentially put lives at risk, by making people speed down a dark street or even drinking and driving? Yes, but such traits emerged for a reason, and it has to do with how you are perceived by other humans.

If you're convinced that you have a ton of positive traits, from smarts to efficiency to even loyalty, the people around you are more likely to view you that way, too.

Let's say a person believes, perhaps mistakenly, that he's an outstanding problem solver. This attitude will be reflected in the things he says and does. He might be vocal about an opinion when a dilemma emerges and appear confident when someone approaches him with concerns. With such confidence, he won't procrastinate when implementing a solution, either.

The result of all this? This person will be considered a valuable member of his community.

This is why overestimation of one's traits or abilities is an evolutionary trait. Humans depend on other humans for survival, and the more valuable we appear to our peers, the more likely they are to accept us within their community.

A community, in general, has a better chance of staying safe from predators, finding food and building shelter. What's more, displaying such traits might even make us better potential mates, which increases our chances of reproduction.

The brain's overconfidence modules serve an important function in social contexts, helping to inspire confidence in others and boost our social standing so that we all can live, love and survive!

> _"Self-representation is … the result of a trade-off between favorability and plausibility."_ –Roy Baumeister

### 9. Hypocrisy stems from failed connections between modules and evolutionary anxieties over mating. 

Aside from overconfidence, there's another human trait that also has evolutionary origins: hypocrisy.

There are two ways humans are hypocrites. First, we ourselves do something that we've told someone else is a "wrong" thing to do. Second, we let others believe we're condemning an action on moral grounds when in reality, we're motivated by self-interest alone.

Although no one wants to be labeled a hypocrite, sometimes you just can't help acting like one. Why?

That's right — the brain's modular structure is to blame. The modules that are in charge when we judge other people's behavior or criticize a friend for cheating on a partner aren't always the same modules at work when we consider our behavior (or perhaps cheat on our partners).

Because the brain's network of modules is so complex, some modules aren't well connected with each other. This is why sometimes, we aren't even _aware_ of our hypocrisy.

And what about false self-righteousness, where you judge a person for doing something like cheating on their partner but really are jealous you didn't have the same opportunity? Such behavior might seem like folly, but evolutionarily speaking, it was the right way to think for our ancestors.

When it comes down to it, humans compete for mates. The more mates another person has, the fewer mates are available and the fewer chances we have to reproduce. Therefore from an evolutionary perspective, we'd prefer people of the same sex as ourselves to live monogamously, and our supposedly moral judgements about promiscuity are merely expressions of this preference.

With a greater understanding of the modular mind's weaknesses, we can better realize when apparently rational actions are just instinctive responses. This, in turn, will help our brains continue to evolve and adapt to the challenges of the twenty-first century.

### 10. Final summary 

The key message in this book:

**Rather than being based on a coherent, conscious "self," the human brain is a bundle of modules, each of which has a specific function. This modular system is what enabled humans to survive in a tough world with multiple predators. But this system is also the reason why people can be so inconsistent, confused and even hypocritical! A greater awareness of the way the brain works is the key to ensuring humans continue to adapt to new challenges.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Thinking, Fast and Slow_** **by Daniel Kahneman**

Daniel Kahneman's _Thinking,_ _Fast_ _and_ _Slow_ — a recapitulation of the decades of research that led to his winning the Nobel Prize — explains his contributions to our current understanding of psychology and behavioral economics. Over the years, Kahneman and his colleagues, whose work the book discusses at length, have significantly contributed to a new understanding of the human mind. We now have a better understanding of how decisions are made, why certain judgment errors are so common and how we can improve ourselves.
---

### Robert Kurzban

Robert Kurzban is a professor of psychology, a blogger for _Psychology Today_, the editor-in-chief of the journal _Evolution & Human Behavior_ and the director and founder of Pennsylvania's Laboratory of Experimental Evolutionary Psychology. In 2008, the Human Behavior and Evolution Society awarded him the Distinguished Scientific Award for Early Career Contribution.

