---
id: 565c80ad9906750007000060
slug: why-information-grows-en
published_date: 2015-12-03T00:00:00.000+00:00
author: César Hidalgo
title: Why Information Grows
subtitle: The Evolution of Order, from Atoms to Economies
main_color: 1C3F43
text_color: 1C3F43
---

# Why Information Grows

_The Evolution of Order, from Atoms to Economies_

**César Hidalgo**

_Why Information Grows_ (2015) takes you straight to the heart of the battle between entropy and order, examining the way that information is propagated and its impact on life, civilization and the universe. In doing so, the book offers a thought-provoking explanation for the success of human beings on earth.

---
### 1. What’s in it for me? Become an information expert and better understand its essence. 

What is information? Perhaps you might point to an Excel spreadsheet, or a banking statement on your desk, and say: yes, that is information.

But really, isn't that banking statement just a piece of paper with some markings made in ink? What really gives it meaning? And what is information, then, if it has no inherent meaning?

In these blinks you'll delve into the substance of information, how it acts in the world and how we through our knowledge give it meaning and spread it through networks of people and products across the globe. 

It all starts with an atom, and as you'll see, grows to encompass everything you think you know — or might want to know — in the known universe. 

In these blinks, you'll discover

  * why the Earth is a hot-bed of ever-growing information;

  * what a tree and a computer have in common; and 

  * how networking keeps rich countries rich and poor countries poor.

### 2. Information is physical order, and is completely meaningless; our knowledge gives it meaning. 

What comes to mind when you think of the date, September 11? Whether it's your birthday, the day a terrorist attack occurred in New York City or of an upcoming exam, your mind automatically attaches meaning to the date.

But is there anything about this date that is _inherently_ tied to any of those events? No, of course not. In reality, "September 11" itself is no more than meaningless information. 

But what do we mean when we talk about "information?"

_Information_ is physical order. While we often think of information as something immaterial, like data, in reality information is physical. Sure, virtual information does exist but it's always wrapped in a physical body, like a brain or a hard drive. 

So at its core, information is simply the _physical arrangement of atom_ s. 

DNA, for example, is pure information. It is a physical arrangement of atoms used to build new physical orders, like a human body. 

Information, and thus the physical arrangement of atoms, exists everywhere. It's not limited to the DNA in our bodies but it's also found in products — in everything from toys to smartphones — which are essentially configured arrangements of atoms. 

It's important not to confuse information with _meaning_. No matter what kind of information you're dealing with, whether a strand of DNA or your bedside lamp, the information itself is utterly meaningless. But that doesn't stop us from trying to make sense of it. 

As a matter of course, we attach meaning, derived from context and prior knowledge, to meaningless information. 

To help conceptualize this, think about the letters that appear on your computer screen when you strike a key on your keyboard. The letters themselves are simply a configuration of light on your screen — they have no meaning in and of themselves. 

However, your understanding of the alphabet gives these letters meaning, thus turning raw information into words and sentences.

### 3. There are far more ways to create disorder than to create order in the universe. 

What is the universe made of? Fundamentally, the universe is comprised of matter, energy and information. 

But there is something else going on that is inherent to the universe: a constant battle between chaos and order.

Information, or a specific ordering of atoms, is exceedingly rare. In fact, you're far more likely to find disorder in the universe than order.

To illustrate this, imagine that you own a Bugatti sports car. There is only one basic configuration of atoms that can produce the Bugatti that you purchased at your car dealership.

If you unfortunately wreck your car, then the car will become yet another version of your Bugatti, one in which the physical order of the original product has been destroyed. Perhaps you smashed the windshield; maybe the car just has a flat tire. There are many ways in which the information from your newly purchased Bugatti can be lost.

In fact, there are many more ways to create disorder than to create order. Consider the colorful Rubik's cube, for example. The cube has only one solved state, in which each of the cube's six sides is a single, solid color. But the cube has more than _43 quintillion_ possible unsolved states!

Bearing all this in mind, one could say _entropy_ — or disorder, the opposite of information — is the natural state of the universe. 

To illustrate this, let's light up a cigarette. As you smoke, the cigarette will burn, turning into smoke that will then dissolve into the air. All the information that once made up your cigarette will be lost.

This idea that the universe tends toward entropy was first put forward by nineteenth-century Austrian scientist Ludwig Boltzmann, one of the foremost scientific minds of his era and a major contributor to the study of atoms. 

However, Boltzmann was baffled by the findings of some of his own research. It appeared that information on _earth_ was growing — order, not chaos, was ascendant on our planet. But how could this be?

### 4. Solid matter can process and understand information. A tree is essentially a sun-run computer. 

So we've learned that our universe in general tends toward entropy. The planet Earth, however, is unique in that it is rich with information — order here is more dominant than entropy. 

But why exactly would this be the case?

Earth is different from other planets in the solar system because of its abundance of information; that is, the ordered, physical arrangements of atoms found here. This abundance is possibly due to the earth's mild temperatures.

Mild temperatures essentially allow for the existence of solid matter, such as flora and fauna. Solid forms are useful, in that a physical shell or body can protect the information "inside" from falling back into entropy or a disordered state.

For example, your genetic information, or DNA, is found in your cells. Being self-contained, "solid" units, cells essentially house your DNA to keep it from dissipating. Without cells, your DNA could have never made you!

Information isn't just abundant here on earth; it's actually growing in quantity. The reason why this is the case also has to do with the properties of solids.

Solids are able to _compute_ — that is, process and understand — information and take action according to the information provided. 

For example, a tree (solid matter) is basically a computer powered by sunlight. It reacts to the amount of sunshine, behaving differently in summer than in winter. It "knows" when to sprout leaves, when to let them fall, and can direct its roots toward water to ensure its survival.

Humans, unsurprisingly, can also compute. We have an understanding of our environment that is sufficient enough to act on the information we find within it. It is through this computational process that we can take something, like a tree, and turn it into something else, like a chair.

> _"Energy is needed for information to emerge, and solids are needed for information to endure."_

### 5. Humanity is unique in that we accumulate information in the form of new products. 

Humans are unique in that we accumulate information. We do this by creating products, essentially changing the arrangement of atoms to improve our knowledge of the world.

The products we create and use everyday embody information; some grant us immediate practical knowledge. For example, if you buy a smartphone, you accumulate both the practical knowledge of how to use the device to access information from the internet and make calls, and at least the partial knowledge of how the device was made and what it's made of. 

Such information-accumulating products differentiate us from our ancestors, for example. While society has made leaps and bounds, developing new ways to organize how we live and think, there is no question that the products we make — the physical order we create — is far more advanced than it used to be. 

Thus we have computers, smartphones and airplanes instead of blunt rocks and bare feet; we have skyscrapers and solid homes instead of thatch-roofed huts that can be destroyed by even the most moderate storms. 

What makes humans so special is our ability to "crystallize" information, to create something from seemingly nothing but our imagination. A computer or a robotic leg are things that don't appear in nature, but had to be imagined before they could be created. 

But crystallizing takes a lot of effort. When we want to create a new physical order, we need to push the limits of reality, and this can rarely be achieved by one lone individual.

Thus people work together to develop new products with the knowledge they've collectively acquired from older products made by other humans. A robotic leg, for example, might come about through advances made in programming and engineering by previous generations. 

This leads us to an interesting question: How can we really accumulate data, if each individual is limited in how much information he or she can gather?

### 6. One human mind can only learn so much; by sharing knowledge we build on what’s come before. 

You've probably been here before: you've a tough exam coming up, and you study for days on end until it feels as if your brain is about to explode. 

But why do we feel so overwhelmed after trying to cram our brains full of information? In essence, it's because we can only remember and understand a limited amount of knowledge.

To illustrate this, think of our economy — the sum of all our interactions as a society — as a computer; each person within the economy is a _personbyte_. 

Each personbyte can, over time and with effort, accumulate a limited amount of knowledge and know-how. Knowing everything is simply impossible.

Some of a person's capacity for learning is genetic or hard-wired, meaning some people can learn better or faster than others. But still, every personbyte can bring roughly the same amount of data volume to the "computer."

However, the information isn't disconnected. The economy essentially links the knowledge of many individuals together in networks. If every person sought to know everything on her own, development would cease or at best be excruciatingly slow. 

Just think, if the author of these blinks had to reinvent a computer just to write these summaries, you certainly wouldn't be reading this right now!

Instead, the economy provides each person with the knowledge and information that other people have already have created. When a computer is created, it is made up of the accumulated knowledge of many individual personbytes. One personbyte will know how to produce a motherboard; another will be able to assemble all the different parts, and so on.

But does this mean that the same knowledge is distributed to everyone, everywhere, equally? Unfortunately, no. Some places are more developed than others due to superior data access. Often places that have a more robust or accessible networks of knowledge tend to be more developed.

### 7. Building efficient networks is challenging, but modern technology has eased the process. 

Building a network takes a lot of effort. But in recent decades, networks have grown considerably and have established links worldwide. So why is networking easier today than it was in the past?

This is due primarily to the existence of global languages. Whereas in the past people spoke many different local languages, today global languages like English, Spanish, French or Chinese allow people to efficiently communicate, often without the need for a translator. 

This newfound ease of communication has allowed people to build better networks and make knowledge comprehensive for and available to nearly everyone. 

But it takes more than a global language to build an efficient network. There is also a social and cultural aspect to networks that shouldn't be neglected.

For instance, if you live in a culture in which the family is the primary social unit, your bonds within your family will be stronger compared to your bonds with society at large. Your networks will be dominated by family-based knowledge and products, simply because this is what you trust.

Yet in some countries, especially in Western nations, trust between the individual and society is quite strong. Here, individuals build networks based on trust outside of the nuclear family, the result of which is larger networks in general. 

Bigger networks allow for more personbytes, and consequently such societies are generally more prosperous and more technologically sophisticated. 

To show how important networks are for growth, look at the expansive networks used by large companies. The products produced by Apple or Google, for instance, are actually a collection of other products that network efficiently, produced by different firms.

If you buy an Apple computer, you'll notice that not all the computer's parts are made by Apple itself but by other companies, each specializing in improving their own knowledge. Apple then uses its knowledge, via its network, to build its products in a way that makes them its own.

### 8. Wealthy countries continue to prosper as they are good at building welcoming networks. 

Have you ever wondered why some countries become wealthy while others seem to wallow in poverty? How can we have on one side the super-rich, benefiting from the latest technology and living well, while on the other side the poor make do with substandard living conditions.

In part, this has to do with networks.

There are a few geopolitical centers, such as the United States and Europe, that maintain a massive amount of knowledge and personbytes. The products that emerge from these centers are increasingly complex; and because of the demand for such products, these regions can export their goods to other countries, accumulating wealth over time.

Meanwhile, other countries, while they may show signs of slow to moderate growth, still remain poor and underdeveloped. 

So why don't these countries simply create larger networks of personbytes to advance their knowledge and wealth?

Well, basically personbytes are attracted to existing networks. If they can, they'd rather enter an existing network than build one from scratch.

Take Silicon Valley, for example. Here you'll find a massive accumulation of personbytes, full of individual knowledge and crystals of information. And every day, more personbytes and companies move there to be part of this dynamic network.

So this is why richer countries seem to get richer while poorer countries stay poor. 

In many Western countries, you can find existing social structures and economies that allow newcomers to enter a network without too much hassle. For example, established academic institutions as well as strong banking networks make it easy for individuals and companies to join a network. 

In many countries, however, it's simply too hard to join networks and share information.

### 9. Final summary 

The key message in this book:

**Information is something that is both misunderstood and crucial for all aspects of life — from a society's economic growth to the existence of life itself. It is locked in a constant battle with its nemesis, entropy, to secure its continued existence.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### César Hidalgo

César A. Hidalgo leads the Macro Connections group at the MIT Media Lab and is an associate professor of media arts and sciences at the Massachusetts Institute of Technology. He is the author of many lauded academic publications and the co-author of _The Atlas of Economic Complexity_ (2014).

