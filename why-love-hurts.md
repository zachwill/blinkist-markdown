---
id: 58de6e7e2b329600041cc1c3
slug: why-love-hurts-en
published_date: 2017-04-05T00:00:00.000+00:00
author: Eva Illouz
title: Why Love Hurts
subtitle: A Sociological Explanation
main_color: BA252B
text_color: BA252B
---

# Why Love Hurts

_A Sociological Explanation_

**Eva Illouz**

_Why Love Hurts_ (2012) is about the history of love, romance and relationships. These blinks detail the gender differences, cultural expectations and social structures that shape our conception of love and make it one of the more difficult emotions to experience.

---
### 1. What’s in it for me? Discern the social dimension of love. 

When it comes to love, women and men often have differing aspirations — a committed relationship or sexual adventures, raising children or retaining individual freedom — and these can lead to tensions, a feeling of disappointment or pain.

But women and men aren't intrinsically different. Society has shaped the roles of women and men.

In these blinks, you will find out about the social dimension of love. You will see how, with societal changes, the field of love has changed as well. You will come to understand what creates the differences between women and men and how we can find our way to successful, loving relationships.

You'll also learn

  * why choosing a mate is like choosing a scoop of ice cream;

  * why men hold sway over the love women receive; and

  * how email, Skype and Facebook make us fall in love with someone we've never met.

### 2. Love between women and men was long characterized by inequality, but now things are improving. 

If you walk into a bookstore, you'll find shelves loaded with self-help books about love and marriage. This proliferation of literature suggests that people are having an increasingly difficult time finding and keeping a suitable mate.

But why is that?

Well, it might be psychological. Some people are too shy to approach potential partners, or may have had poor relationship experiences in the past. But there are also social reasons for the increasing difficulty of dating — and these social factors have a long history.

Love between women and men used to be marked by stark inequality. From the Middle Ages to the nineteenth century, the concept of love was overloaded with ideas of chivalry and romance.

Men were expected to defend women, who were considered weaker, and could expect a woman's devotion in return. During this time, the inferiority of women was deeply ingrained in the way people conceptualized love. 

Since women were considered more fragile, men's role was to protect them. In this way, love became a way of displaying masculinity and honor, while entrenching gender inequalities.

Thankfully, inequality between men and women has decreased with modernity, which has also transformed the nature of love.

This change was especially marked during the 1960s when heterosexual relationships were reshaped by the sexual revolution and a new wave of feminism. The result has been a dramatic movement toward gender parity and a division between sex and emotions.

At the same time, socioeconomic shifts made love more important in marriages. In the past, socioeconomic compatibility was a precondition for love because marriage was seen as a strategic decision to form alliances and reinforce two families' finances. But as forming alliances became less important, love became central to marriage.

### 3. The way people choose partners has changed with modernity. 

Imagine you've just won the lottery and you go to the mall with your winnings. Now that money is no longer an object, you can buy whatever you wish. Well, in many ways, love today is similar.

After all, previously, economic imperatives were the main factor in making marital decisions. But with modernity, new criteria for selecting mates entered the marriage market. One such factor is attractiveness.

Beauty has become so important that the socioeconomic status of a potential partner can be enhanced or diminished by his or her looks. Just take research by Catherine Hakim that suggests that girls who are good-looking in high school get married earlier, have a better chance of marrying and, 15 years down the line, tend to have higher average household incomes than their less attractive peers.

Or imagine you're at a party and there are two men, one attractive and poor, the other rich and ugly. You might choose to go home with the poor guy simply because he's handsome. His socioeconomic status is boosted by his good looks.

But looks aren't the only criteria that have changed when it comes to choosing a mate. The age at which people decide to settle down has also shifted.

The newfound freedom of choice in dating has produced entirely new problems. Today, people have to weigh the desire to retain their independence against the wish to start a family.

There's some discrepancy between the sexes on this issue, and men tend to prefer personal freedom at an age when women want to start families. In fact, women, in general, tend to want committed relationships earlier than men, as their perceived attractiveness is usually tied to their youth. They also feel pressure to have children before it's too late.

### 4. Women blame themselves for failed relationships and men hold the upper hand in love. 

Can you remember your last breakup? Did you think it was your fault that the relationship ended? Well, if you did, it might not be about what you said or did. It could be all about your gender instead.

That's because self-blame for failed relationships is more common among women. Their sense of self-worth is more closely connected to love than that of men.

So, when a relationship doesn't pan out, women take a blow to their self-regard, causing them to blame themselves. This explains why women are more likely to seek out therapy and self-help books.

Following a breakup, women replay in their minds everything they said or did that might have led to the split. This means they're more likely to think of themselves as the reason for the relationship's failure and, since their self-esteem is so closely tied to being loved, they become more insecure.

This difference between genders means that men hold more power in the romantic sphere. Here's how.

You already know that women are more eager to forge committed relationships since they feel pressure as they age. As a result, women are more inclined to show interest beyond mere sexual attraction to a man. Men, on the other hand, don't feel a great need to offer such recognition as they're less likely to desire a relationship.

That being said, mutual recognition of this type is key to relationships. It gives both people the chance to be seen and loved for who they are. That's why love is a desirable feeling; it comes with complete acceptance.

> _"Men need women's recognition less than women need men's recognition . . . , men and women both need other men's recognition."_

### 5. Love has become increasingly rational. 

Do you keep track of the calories you eat, the steps you take or the number of sexual partners you've had? Well, even if _you_ don't, many people do. Life has become increasingly rationalized, and that goes for love too.

In many ways, love isn't just a matter of emotions, but one of rational thought. Modern science can explain why.

In earlier times, love was a profoundly mystical experience. But in the twentieth century, the rise of various scientific fields offered increasingly rational explanations. Biology explained love through the lens of reproduction and the survival of the human race.

Then other fields like psychology and neuroscience began to view love in terms of neurochemistry and sex drive. These scientific theories vaporized the mystical aura that had surrounded love.

It's therefore no surprise that love and mating have grown increasingly rational. Just consider the fact that, from pre-modern times to the twentieth century, the selection of mates was only rational to the extent that dowry requirements were set. Logical thought stopped there and the first available match was often selected.

By contrast, today people don't just look for _anyone_, but for their soul mate. This long search is now being facilitated by the internet, which has led to the rapid growth of online dating. This process is exceedingly rational as it's based on similarities between people, like sexual preferences, health and education.

Online dating also lets people interact with many more prospective mates than they'd find in real life. With such a wide pool to select from, people can be more careful about the choices they make.

So, love has become more rational, but how does this affect people's daily lives? You'll learn all about it in the next blink!

### 6. When it comes to love, the internet and media set people up for disappointment. 

Imagine you're planning a vacation. You spend some time online looking at pictures of pristine, white sand beaches, and book what looks like a lovely hotel room. But when you arrive, the beach is littered with plastic bottles and the staff are grumpy.

Well, a similar problem spoils online dating. The internet supports all kinds of fantasies, which makes for serious disappointment when the real person fails to meet high expectations.

This issue is largely because Skype, Facebook and online-dating sites allow people to feel that the person they're in contact with is practically in the room with them. These platforms give people just enough information for their imaginations to fill in the blanks.

Just take a study by the author. One of the participants was a 38-year-old woman who had been chatting with a man she met online. They spoke over Skype, exchanged emails and even connected on Facebook. Then, after a while, the guy broke off all contact. Even though the woman had never met her chat partner, she felt like she knew him and had fallen in love.

If they _had_ met in real life, she might have found that he wasn't as interested in her as she thought. This reality check could have spared her the disappointment of losing him months later.

So, the internet can set people up for disappointment, but unrealistic depictions of love in the media don't help. After all, compared to the jaw-dropping romances that we see in art, movies and literature, our own romantic experiences feel boring, predictable and often set us up to be let down.

This sense of predictability creates a serious problem as many people feel that the lives of others are more exciting or adventurous than their own. Naturally, they grow dissatisfied with their daily routines as they consume endless images of beautiful homes, sculpted bodies and adventurous travel.

> _"The Internet seems to sustain relationships precisely through the ways it creates a phantasmatic presence."_

### 7. Passion and a recognition of socially shaped gender differences are essential to a successful relationship. 

You've learned that love is becoming an increasingly rationalized experience in which the obstacles to forging truly loving relationships can seem impossible to overcome and disappointment feels like a logical outcome.

So, in the face of this conundrum, how can you build successful relationships?

The first step is to foster passion and emotion, both of which are vital to a lasting relationship. This is easier said than done as passion and emotion can set people up to feel hurt when a relationship fails, but such feelings are also crucial to building a meaningful connection.

A meaningful connection can fully engage the self, making you forget about the world around you as long as you focus on your partner. Once you become fully engaged, you can derive a tremendous amount of meaning from this type of loving connection. Just remember, a one-night stand won't make you ponder the meaning of love and life, but a strong, caring relationship might.

Furthermore, for relationships to be successful, it's also key to understand the way gender differences are at least partially shaped by society. Men don't come from Mars and women don't come from Venus. Both genders are from the same planet; they're just socialized differently.

This means the psyches of men are not inherently unloving or emotionally distant. Men simply need a new model of masculine sexuality that enables them to feel and express emotions. When we can acknowledge that genders aren't as different as society has led us to believe, new possibilities will open up for mutually beneficial behavior and relationships.

### 8. Final summary 

The key message in this book:

**Finding love in the modern world can feel like a futile task. Women and men often have contradictory agendas and the search for love is becoming increasingly rationalized through the advent of internet dating. Nonetheless, all people can build successful relationships with real commitment.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Labor of Love_** **by Moira Weigel**

_Labor of Love_ (2016) is your guide to the history of dating. These blinks walk you through the social, cultural and economic shifts that have shaped modern rituals of courtship and explain the curious fads and fashions of flirtation that have come and gone through the ages.
---

### Eva Illouz

Eva Illouz is a professor of sociology and anthropology at the Hebrew University of Jerusalem. She researches the history of emotions and capitalism's impact on our private lives. Her recent titles include _Cold Intimacies: The Making of Emotional Capitalism_ and _Saving the Modern Soul. Therapy, Emotions, and the Culture of Self-Help_.

