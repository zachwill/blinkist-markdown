---
id: 568b8ae0de78c00007000042
slug: why-men-love-bitches-en
published_date: 2016-01-08T00:00:00.000+00:00
author: Sherry Argov
title: Why Men Love Bitches
subtitle: From Doormat to Dreamgirl - A Woman's Guide to Holding Her Own in a Relationship
main_color: CF295C
text_color: B52450
---

# Why Men Love Bitches

_From Doormat to Dreamgirl - A Woman's Guide to Holding Her Own in a Relationship_

**Sherry Argov**

_Why Men Love Bitches_ (2000) debunks the myths of male-female relationships and gives you real, insider tips on how to keep the man you want. These blinks reveal why it's vital that you regain your independence, and why doing this is advantageous to both you and your relationship.

---
### 1. What’s in it for me? Be a “bitch” and rule your relationship. 

It's time for a change: it's time for you to be more Joan Rivers and less Julie Andrews. So many of us women try our hardest to please our men, to go the extra mile to give them what they want. And what do we get in return? Nothing. We get neglected and walked over. Why? Because we are simply too nice.

These blinks take a lighthearted look at how women can revolutionize their relationships by refusing to give men all they want. Although these blinks shouldn't be taken too seriously, they provide hints as to how women can wrest control of the relationship from their partner, making it better for both of them.

In these blinks, you'll discover

  * why you should treat a man like a prehistoric hunter;

  * why women favored a Tupperware party over an exotic cruise; and

  * how to make your man feel like Rambo while giving him no actual power.

### 2. Giving your all to your man will only bore him. 

From a young age on, most women are told that the best way to attract a man is to please him. Taking care of your boyfriend's, partner's or husband's every need, want and desire is the best way to earn his respect. Right? 

Wrong! Times have changed. Being a doormat will only make you feel used _and_ create an unhappy, unbalanced relationship. And: _men don't even find it attractive_. Being ready to please won't seduce the man of your dreams. In fact, nothing could be less seductive!

Though we might hope that our partners will appreciate all the extra effort we're putting in for them, it simply doesn't work that way. Why? Men are all too ready to take us for granted. If you're available to give him a foot massage 24/7, he won't think of it as something special, or something that he's earned. Instead, it's just another treat he can request whenever he pleases. 

Sure, being sweet and caring will make your man fond of you. But not in the way that _you_ want! Unconditional love will make him feel like you're his mother, not his lover. 

Sue learned this the hard way in her relationship with Mike. She nurtured him, cleaned up after him, forgave his every mistake. The result? After living like a prince with Sue for a few months, he dumped her, claiming that, although Sue was wonderful, there was "just no passion." And he was probably right.

### 3. Being needy will create stress for you and your partner. 

Perhaps you've already found yourself playing the role of doormat. You may even be aware that your relationship can't continue this way. So what's stopping you from leaving? The unpleasant truth is that you're _devoted._

Sometimes love hits us so hard that we not only want to share our life with someone; we want to give up our entire life _for them_. The sweet girl-next-door is all too eager to withdraw from her own life and dedicate every moment to her new partner. 

This is a fatal mistake.

Utter devotion won't make you a better girlfriend. Rather, it'll strip you of that special quality that your partner was initially attracted to. To demonstrate this, let's examine the story of a Chicago couple. 

She was a whirlwind, a woman always on the go, with a wide circle of friends and a passion for salsa dancing. But when she met him, things began to change. She let her hobbies fall by the wayside; she neglected her friends. Why? Because she wanted to make time for him. 

Unfortunately, this didn't result in flourishing romance. Her partner had fallen in love with an independent woman, someone who put her own happiness first. She wasn't that woman anymore, and the relationship was dying. 

Being overly devoted will erode your individuality _and_ stress out your partner. Everybody needs space from time to time, and by desperately trying to bring your partner closer, you'll only push him away.

After giving up her hobbies and neglecting her friends, the needy woman feels hurt when her partner isn't willing to make similar sacrifices for her. Fair enough, but here's the kicker: her partner probably never expected her to make those sacrifices in the first place!

So while you're unnecessarily martyring yourself for the sake of the relationship, your partner will feel encumbered by your neediness. And instead of being able to rely on you for support during stressful times, he'll only feel that you're a burden and an added stress.

### 4. Men love the chase – so make them run! 

Briefly put: near everything we've been taught about what men want is a lie. For what do they truly yearn? Well, strangely enough, they want us to make things difficult for them. Men love the chase, something most women usually find frustrating. But why not use that knowledge to your advantage?

First, you've got to see things from a male perspective (no matter how illogical that perspective may seem). Even today, some men still love going hunting. For days, they endure bad weather, uncomfortable sleeping arrangements and unsavory camping food. 

And yes, they certainly find satisfaction in shooting a buck and hanging its head on the wall. Buying a quality piece of venison at the butcher just doesn't provide the same thrill. But making a kill isn't really what hunting is all about; it's the _thought_ of making one that's so tantalizing. 

We can see a similar logic at work when a man pursues someone he's romantically interested in. For him, chasing an attractive woman is quite the thrill. But it's not just the woman's beauty, intelligence or humor that excites him. It's the simple fact that he doesn't yet possess her. 

You can use this male tendency to your advantage every time your partner wants to meet you. Even if you're free all week, why not pretend you're booked out? More free time for you, and he'll be kept on his toes. Women who understand this have a remarkably easy time in their relationships. Try it for yourself!

### 5. Self-sufficient women will attract the right partners. 

Most of us are guilty of fantasizing about rich lovers who shower us with gifts and cater to our every whim. But while that might seem like the relationship you'd want, it's definitely not the one you need. Having a partner that covers all your expenses might mean you lose that most important of things: your independence. 

Sure, this doesn't mean your partner can't buy you dinner or give you a thoughtful present every now and then. But it should be clear that they have no financial control over you. After all, a self-sufficient woman is able to maintain her dignity. She can decide how she wants to be treated, and has the power to leave any relationship that's holding her back. 

Moreover, a woman who puts herself first is more likely to attract men that appreciate that. Namely, the men that can see her for what she really is: a person who will enrich their lives. 

Consider the story of a young consultant working in Manhattan. She had recently met a charming, handsome and high-flying lawyer, who, after a few dates, invited her to join him on a luxurious cruise. She declined. The reason? She'd already planned to host a Tupperware party that week, and there was no way she was postponing it!

He was stunned. And after just one day on the cruise, he returned to New York to attend her Tupperware party. By putting her plans first, she surprised the lawyer and showed him what kind of a woman she was. He couldn't help but fall for her, and was happy to spend an evening with her and her girlfriend's debating the best way to freeze chicken soup!

### 6. Keep a relationship fresh by making your man wait. 

If you've landed in the "doormat" category, don't fret! Even if you've played the giving girlfriend for years in your relationship, it's never too late to shake things up. There are a number of simple strategies that'll help you get out of there. In fact, it's as simple as living your own life. 

How many hours do you dedicate to your partner each week? Reflect on it, and then work out where you could take a few of those hours and give them back to yourself. If your partner is used to you spending the night at his place all the time, or to knowing where you are every hour of the day, create some distance. 

Leave your phone off, hang out with other people or spend time alone. Not only will you feel revitalized; he'll cherish the time he gets to spend with you a whole lot more, too. 

Of course, the best thing is to avoid falling into the doormat role before your relationship begins. It might be old-fashioned, but making your man wait before you sleep together is still the best strategy. The author interviewed hundreds of men, most of whom admitted in confidence that they're more attracted to a woman who makes them wait. 

And it makes sense. As we learned in the previous blinks, men are thrilled by what they can't have. Making him wait will excite his imagination, and the physical intimacy you share will be all the more special when the moment arrives. As a rule of thumb, you can make your partner wait at least a month.

### 7. Always put yourself first, but make your man feel needed from time to time. 

By now it's clear what you've got to do to improve your romantic life: put yourself first! Of course, this is easier said than done. Many strong women still have difficulties asserting themselves. Luckily, there are a few tricks we can use to ensure we stand our ground. 

What you _shouldn't_ do is "fake it 'til you make it." Pretending to be confident, independent and self-sufficient simply isn't enough. What you really want to do is follow your own inner voice. Yes, that nagging voice of doubt that you usually push aside!

Say, for example, your date wants to go out clubbing. But you're really full from dinner and would like nothing more than to curl up on the couch. What do you do?

Many women often make the mistake of going out with their partner to make him feel good. But what's the point? If one of you isn't having a good time, then the experience will be ruined for both of you. Do you and your partner a favor, and stay in when you feel like it! 

But remember: men are fickle creatures. Just when you think you've got the independent woman thing down, he'll start acting grumpy. Why? Well, while men instinctively love the chase, they also like to play the protector. They might feel threatened by your new independence. Even sensitive new-age males like to be the man of the house sometimes! 

So why not let them? Next time there's a bug in the kitchen, a jar that won't open or something heavy to move, ask him to help. Even if you don't need it! It takes almost no effort on your part, and it's fairly adorable how pleased guys get when proving their masculinity.

### 8. Let your man feel like he’s calling the shots – you can still run things behind the scenes! 

So men like feeling manly. This means they also like to call the shots. But this doesn't mean you should give your man total control over the relationship. It's all making him feel like he's in control while nudging him in the right direction. 

This is particularly useful when there's a conflict in your relationship. What do you do when you want your partner to act differently? Most of us criticize, complain or even attack our partner, but this probably just triggers his instinct to compete. Luckily, there's a better way to do things. Let's look at the story of an LA couple for guidance. 

Both husband and wife were working full-time jobs, but she did nearly all of the household chores. He only helped out occasionally. Of course, the woman was irritated. She had better things to do than the laundry!

But instead of getting critical, she praised him whenever he offered a helping hand. She expressed how happy she was that even after a draining day at work, he still did his part. Slowly, her husband began to do more and more chores, until the workload was balanced. These days, she never even has to ask him to help out.

That's because her husband began striving to live up to his wife's expectations of him. Just goes to show how effective praise is when you want your partner's behavior to change. 

You can also use the 'nudging' approach when facing big decisions. The trick here is to make him feel like the idea was his in the first place. He'll feel like he's in control while you're calling the shots behind the scenes. 

You can do this by outlining three options you're happy with, such as summer vacation destinations. Then, present them to your man and let him make the final decision. Tell him you love how he always makes great choices. He'll feel great, and so will you — the goal of any good relationship!

### 9. Final summary 

The key message in these blinks:

**There's no reason why any woman in a relationship should focus all her energy on pleasing her man. It's time to discard old myths about relationships. Starting by putting yourself first! Women that are independent, self-sufficient and focused on their own life will attract the men they deserve.**

Actionable advice:

**Treat yourself!**

Turn your phone off after reading this! Run yourself a bubble bath and dig out your favorite DVD to watch tonight. Don't worry about replying to your partner's texts; he won't resent you! Give your relationship a bit of distance. He won't be able to stop thinking about you, and you'll finally have some time to start thinking about yourself. 

**Suggested further reading:** ** _The Game_** **by Neil Strauss**

_The Game_ (2005) gives readers an inside look into the "pickup community" frequented by men desperate to convince women to sleep with them. These blinks share the advice of a leading seduction guru, and the less than great consequences of his successes. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Sherry Argov

Sherry Argov, a professional radio show host and writer, has written several number-one bestsellers — mostly relationship guides — and appeared on numerous TV shows, including _TMZ_, _The Today Show_ and _The View_. Her work has been published in over 100 magazines, including _People_, _Cosmopolitan_ and _Self_.

