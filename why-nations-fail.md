---
id: 528227cf333464000c020000
slug: why-nations-fail-en
published_date: 2012-11-09T16:56:59.341+00:00
author: Daron Acemoglu & James A. Robinson
title: Why Nations Fail
subtitle: The Origins of Power, Prosperity, and Poverty
main_color: A8D4DD
text_color: 1C5E6B
---

# Why Nations Fail

_The Origins of Power, Prosperity, and Poverty_

**Daron Acemoglu & James A. Robinson**

_Why Nations Fail_ revolves around the question as to why even today some nations are trapped in a cycle of poverty while others prosper, or at least while others appear to be on their way to prosperity. The book focuses largely on political and economic institutions, as the authors believe these are key to long-term prosperity.

---
### 1. What’s in it for me? Sharpen your understanding of political and economic power dynamics on a global, historical scale. 

It doesn't take more than a few minutes of flicking through the news before some pretty basic questions start kicking in. Why are some nations rich and others poor? And how do some nations end up prosperous and tolerant, while others sink into elite despotism and self-interested greed?

It's been common over the centuries to explain away such historical trends through a nation's culture or location. But in truth, it's the development of a country's institutions that matter. In the course of history, all nations are faced with forks in the road, which lead them to building and sustaining institutions that are either _inclusive_ or _exclusive_. It's the fallout and landscapes built from such institution-building that is explored here. And it's this that explains how nations can either prosper or fail.

In these blinks you'll learn

  * why it took centuries for the printing press to catch on in the Ottoman Empire;

  * how the power of unions faced down the military dictatorship in Brazil; and

  * how Western Europe benefited from the destructive Black Death.

### 2. A country's propensity to wealth or poverty isn’t simply based on its geography, culture or knowledge base. 

On the border shared by Mexico and the United States there lies a town that's divided in half between the two nations. The residents of Nogales, Arizona have a much higher standard of living than those living south of the border in Nogales, Sonora. They have better access to healthcare and education, their crime rates are lower, and the average household income is three times higher.

What causes such differences? The _geography hypothesis_ has been the most influential theory designed to explain such inequality — but that theory falls short here.

It was most famously espoused by the eighteenth-century French philosopher Montesquieu. He maintained that inhabitants of warmer, more tropical climates were lazier than the harder working, more resourceful types who lived in more temperate climes.

In modern times, the theory has morphed to emphasize the presence of diseases in warmer regions such as Africa, South Asia and Central America, as well as the supposed poor soil quality of those regions, which allegedly inhibits economic growth.

But it isn't just Nogales that disproves such ideas. Just look at differences between South and North Korea, the former countries of East and West Germany, and the massive economic leaps made by Botswana, Malaysia and Singapore.

Two other classically cited theories don't stand up either.

The first is the _cultural hypothesis_. In the early twentieth century, German sociologist Max Weber claimed that Western Europe's high rate of industrialization, in contrast to the rest of the world, had been caused by its "Protestant work ethic."

But just look at Korea, a peninsula that was culturally homogenous until the split between communist North and capitalist South. The cultural hypothesis simply cannot explain the differences in inequality between the two. It's the existence of the border that has caused such disparities, rather than deep and significant cultural differences.

The _ignorance hypothesis_ operates in a similar field as the cultural hypothesis. It suggests that poverty results from a dearth of knowledge regarding policies that might encourage economic growth.

The counter example here is obvious: foreign aid and expert advice brought to countries in Africa have largely failed to make a lasting difference.

However, there is a more compelling theory that explains international inequality. Let's look at it now.

### 3. Variations in living standards between countries are best explained by institutional differences. 

Forget grand theories that try to explain away variations in prosperity between countries. The truth is far simpler. What really matters are economic and political institutions.

A given country's prosperity is determined by its economic institutional landscape — the systems and regulations that direct economic behavior within its borders. That landscape includes property laws, the strength of public services and access to finance.

These economic institutions fall into one of two categories: _extractive_ or _inclusive_.

Inclusive economic institutions stimulate economic success and are designed to encourage participation in economic activities. They also nurture economic freedom.

In countries like South Korea and the USA, for instance, market rules derive from private property laws, as well as from developed banking sectors and strong public education systems.

These rules enable people to know they can work hard and be innovative, certain that their efforts will be remunerated and their wealth will be preserved.

In contrast, extractive institutions derive incomes from groups within society for the benefit of other groups. In colonial Latin America, for example, a system built on duress and the expropriation of indigenous populations was designed to benefit the colonizers. In North Korea, the Kim family founded a regime that repressed the populace, outlawed private property and concentrated all power within a select elite.

Much like economic institutions, political institutions can also be inclusive or extractive.

The main characteristic of inclusive political institutions is _pluralism_. This means that various groups in a given society are politically represented, therefore power is shared between them. For institutions to be truly inclusive, it's also essential that they are _centralized_. Centralization of power results in the rule of law being upheld; there is no need for these different groups to fight each other for superiority.

If political institutions lack pluralism or centralization, then they can generally be spoken of as being extractive.

The benefit of inclusive political institutions is that they result in power-sharing between groups. This leads to the erasure of extractive economic policies and, consequently, in mutual economic benefits for all members of society.

> _"As institutions influence behavior and incentives in real life, they forge the success or failure of nations."_

### 4. Single events taking place at critical junctures may result in diverging institutional paths. 

One event more than any other shaped the medieval and early modern periods in Europe. In the mid-fourteenth century, the Black Death traveled along established trading routes from the Far East to the European continent. Almost half its population died in the plague's devastation.

Alongside the human devastation, the ensuing economic fallout shaped Europe for centuries to come.

The Black Death is therefore an example of what is known as a _critical juncture_ — that is, an event capable of overturning the sociopolitical balance of a nation, a continent or even the entire world.

Prior to the arrival of the Black Death in Europe, the continent's social and economic systems were shaped by a highly extractive form of governance and control called _feudalism_.

A country's monarch owned land that he allotted among his lords, who, in turn, were obliged to provide military capabilities when needed. This land was then tended to by the lord's peasants, who had to pay most of their harvest back to their lord in taxes. Peasants were not permitted to migrate without their lord's permission. To top it off, the lord also held judicial power over them.

However, the Black Death caused massive labor shortages. In Western Europe, this meant that peasants finally felt they had the capacity to demand lower taxes and more rights.

But the same cannot be said of Eastern Europe. There, the peasants were less well-organized. Landowners managed to bring peasants under the yoke by taking advantage of their lack of organization. In contrast to their Western European counterparts, Eastern European institutions became increasingly extractive, as higher and higher taxes were extracted from the peasants.

It can therefore be said that the Black Death formed a critical juncture. It resulted in both the eventual dissolution of feudalism and, somewhat more swiftly, in less extractive institutions in Western Europe. But the opposite was true just a little further east.

There is a name for the phenomenon by which critical junctures lead to divergent paths. It's known as _institutional drift_. This means that regions that are otherwise quite similar bifurcate in different directions.

Something similar happened just a few centuries later. This time the critical juncture was the expansion of global trade and the colonization of the Americas. These advents accelerated the path of institutional drift, as not all countries in Europe profited economically from them.

It may take centuries, but all it takes is a few critical junctures and the resulting institutional drift to result in huge differences between the institutional landscapes of countries that were once alike.

### 5. The affluence of early industrializers like England stems from inclusive political institutions developed centuries ago. 

In the modern period, one country in particular was quick off the blocks to industrialize. England had begun the process of industrialization in the seventeenth century — and by the nineteenth, it was a global superpower.

This begs the question, why England? Well, it all came down to the country's already existing political institutions, which gave rise to inclusive economic institutions.

The initial foundation for success had been laid long before. The signing of Magna Carta in 1215 had established an embryonic English Parliament. More critical still, however, was the Glorious Revolution of 1688. This enabled William III, who was supported by Parliament, to oust James II. In return for its support, the now British Parliament was given more power, while that of the monarchy was reduced.

Unlike monarchs, members of Parliament were elected, albeit only by landowners. As a result, the elected Parliament served the interests of this minority and, in so doing, created inclusive economic institutions that encouraged active participation in the economy.

Consequently, legally enforceable property rights were enshrined in law, and stronger protection laws served to incentivize investment and innovation.

Parliament also reformed the banking system. The Bank of England was established in 1694. One of its primary purposes was to provide credit to enable British subjects to invest.

The tax system also underwent reform. To encourage manufacturing, taxes on manufactured goods, such as stoves, were abolished. They were replaced by land taxes. An expanding state bureaucracy also allowed for the more efficient collection of excise taxes. The idea was to reinvest taxes and thereby stimulate the economy.

So it was that during the eighteenth and nineteenth centuries, the country's infrastructure improved radically. First, the canals were built, and later railways too. Both these transport systems enabled the easy flow of goods and raw materials.

All of these factors together facilitated England's rapid industrialization. Manufacturers now had the means and the methods to mass produce goods. These were shipped around the globe, and the resulting profits were taxed and fed back into England's economy.

Capitalism is all good and well, but how did this economic boom and its supporting infrastructure allow for England's institutions to become increasingly inclusive? Let's look at that in the next blink.

### 6. Inclusive institutions create virtuous cycles. 

Let's continue with England as our case study. It's no fluke that when fundamental inclusive institutions are put in place, the result is inclusive economic reform.

That's because inclusive institutions not only stimulate economic growth; they also effectively reinforce themselves over time.

As England's political institutions became more pluralistic, it was in the interest of each powerful faction to ensure that every other faction's power was circumscribed by law.

Step by step, during the nineteenth and twentieth centuries, these institutions became increasingly inclusive. The right to vote was extended beyond the landed elite, until suffrage was eventually universal for men and women, regardless of wealth.

It was thanks to the concerted efforts of the disenfranchised that universal suffrage was achieved. Workers' strikes, social unrest, petitioning and campaigning all played their part.

However, the success of the disenfranchised did not exist in a vacuum. In part, institutions already established in England were geared toward compromise. It was in the interests of the elite that stability and governance be maintained and orderly. There was no reason to allow the collapse of a system that had proved so financially successful. Better to heed demands, rather than let revolution stalk the land.

This increasing suffrage marked a shift toward more pluralistic political institutions which were now equipped to represent the economic interests of a greater share of people. Economic institutions, in turn, also became more inclusive.

While the mills of justice and suffrage turned slowly, the oil that kept things turning was the media.

It monitored the actions of the powerful and kept voters informed of political events. In short, it ensured the self-fulfilling process of institutional political inclusivity kept running.

We need only look across the pond for an example.

In the US, at the turn of the twentieth century, "robber barons" created powerful monopolies, such as Standard Oil and the US Steel Company. Between 1901 and 1921, however, presidents Theodore Roosevelt, William Taft and Woodrow Wilson enacted anti-monopoly laws that shut down the barons' attempts to procure more economic power.

It was due to the efforts of the press that abuses of power by these monopolies became a national issue. Soon, people across the country were demanding reform.

### 7. Consolidation of power often adversely impacts a country's economic development. 

It would be quite natural to presume that intelligent leaders would always choose prosperity over poverty for their countries.

Unfortunately, political elites are actually a self-interested bunch, and this has adverse effects on development.

The printing press is a prime example. Invented in Mainz in 1445, it had spread to Strasbourg, Rome, Florence, London, Budapest and Krakow by the end of the fifteenth century.

The rulers of the Ottoman Empire were having none of it though. For them, the printing press represented a threat to their power, and so Muslims were forbidden to print in Arabic. It was only in 1727 that printing was permitted, but even then, religious and legal scholars were there on hand as part of the vetting process. The impact on education was significant. It is estimated that only about 2 to 3 percent of citizens in the Ottoman Empire were literate, compared to 40 to 60 percent in England.

Another factor in the inhibition of economic growth is the fear of _creative destruction_ among political elites.

Creative destruction is the process that results from innovations improving efficiency and obliterating certain economic sectors. For instance, the development of the sewing machine led to the downfall of the traditional textile industry.

In the early nineteenth century, Emperor Francis I of Austria famously resisted industrialization. Until 1811, all new machinery was prohibited, and even railways were opposed. His great fear was that new technologies would enable revolution, practically speaking. Additionally, there was the likelihood that industries controlled by elites that favored the emperor would be compromised, resulting in the elites' political collapse.

Due to this fear of the industrial revolution and associated creative destruction, Austria's development floundered.

In 1883, when 90 percent of the world's iron production relied on coal, Austria was still dependent on far less efficient charcoal. It was as if the Industrial Revolution had never happened. In fact, when the Austro-Hungarian Empire collapsed after the First World War, its textile and weaving industries were still not fully mechanized.

### 8. Extractive institutions leave an enduring legacy. 

We've seen how inclusive institutions develop over time. Extractive institutions the world over are similar; historical forces not only fashion them, but effectively prolong and perpetuate them.

This can be most clearly seen in the institution of slavery and its persistent historical influence.

Slavery had existed in Africa prior to the arrival of European colonizers in the seventeenth century. They were on the hunt for forced labor to toil in sugar plantations in the New World.

Once slavers started arriving in Africa, local rulers realized they could make a fortune selling slaves to them. Consequently, enslavement massively increased. War captives and criminals found themselves enslaved en masse. In some communities, slavery also became the only form of punishment.

In return for these slaves, as well for valued items such as cotton, traders imported weapons to Africa from Europe. Of course, all this did was further incite violent tendencies among African tribes.

Even though the global slave trade technically ended in 1807, slavery continued in Africa. It's just that slaves were now commodities forced to work in the continent of Africa, producing for both internal and export markets.

Nor was that the end. Even though African independence movements were met with great success in the second half of the twentieth century, extractive institutions established by colonizers persisted.

Take Sierra Leone. It was a British colony from the early nineteenth century to 1961. The British appointed local Paramount Chiefs to rule on the British monarchy's behalf.

Nowadays, Paramount Chiefs are elected for life by the Tribal Authority, a tiny unelected political body. Only members of a few aristocratic families — as originally prescribed by the British — are themselves eligible to become Paramount Chiefs.

That is to say, the political system is as highly extractive as it ever was.

The same is true of the economic system. In 1949, the British established the Sierra Leone Produce Marketing Board. This promised to protect farmers from price fluctuations. The catch? Just a "small" fee. Of course, this ballooned to around half a farmer's income by the mid-1960s.

Independence failed to bring this practice to an end. In fact, under Siaka Stevens, who became the prime minister in 1967, the farmers were forced to hand over 90 percent of their income in tax!

The logical question is: Why didn't these institutions just collapse after independence? Let's investigate in the next blink.

### 9. Extractive institutions create vicious cycles of poverty. 

Generally speaking, extractive institutions emerge when leaders resist development and attempt to consolidate power instead.

But that's just the start, since extractive political institutions are self-perpetuating.

The purpose of extractive structures is to maintain an elite's grip on power, so it's fairly understandable that this elite will want to perpetuate these structures.

Just look at the slave states of the US in the nineteenth century. There, a white landowning elite profited from the labor of black slaves who had no political or economic rights.

After the American Civil War and the North's victory in 1865, slavery was abolished, and black men gained the right to vote.

But the Southern landholding elite was still there, ready to extract and exploit ex-slaves as a source of cheap labor.

In an effort to consolidate power, they introduced the poll tax and literacy tests for potential voters. Of course, the intention was to disenfranchise new black voters who had been prevented from receiving the requisite education.

The dynamics of this power imbalance were formalized in the Jim Crow laws of the late nineteenth and early twentieth centuries. Segregation was officially sanctioned.

The continued existence of such extractive institutions, even after regime change, has been well studied.

The early twentieth-century German sociologist Robert Michels labeled this tendency "the iron law of oligarchy." This refers to the penchant for oligarchic institutions to persist, irrespective of whether the same elite maintains its grip on power.

That's exactly what happened in post-independence Africa. The extractive institutions established by Europeans effectively remain there today.

Needless to say, those gifted power by such institutions are almost duty-bound to consolidate their own power even further.

Take Siaka Stevens, Sierra Leone's first president. He set about actively discriminating against the Mende, an ethnic group that supported his political opponents. He debilitated economic growth in the region where the Mende lived by destroying the railway used for exports — all to crush his opponents.

Consequently, he assumed more power, but the nation's institutions could hardly be said to have represented its people anymore.

### 10. Growth under extractive institutions isn't impossible, but it’s hardly sustainable. 

Whichever way you square it, the Soviet Union can by no definition said to have been a country that fostered inclusive political or economic institutions.

That said, from its genesis right through to the 1970s, its success in certain spheres was unquestionable. Its society was innovative, and it sent the first cosmonaut into space. Its economy also boomed; the annual average growth rate was 6 percent between 1928 and 1960.

One reason behind such growth was that the Soviets had taken over countries that had remained massively underdeveloped for centuries. In the Soviet Republics, the feudal order had only recently been jettisoned. Consequently, the re-allocation of resources from the agricultural sector to the more productive industrial sector made a lot of sense.

The result was massive economic growth — surprising on closer inspection, as you might not expect such growth to happen within extractive economic institutions. Property rights were few, and workers risked incarceration if found slacking. These conditions were coupled with an extractive political institution, namely a ruthless and murderous single-party dictatorship.

Needless to say, economic success built on such extractive institutions isn't sustainable.

Once resources had been allocated to more efficient use, opportunities for growth were few. Additionally, the economic system wasn't geared toward genuinely engendering innovation and, with it, growth.

The reasons for this are clear: extractive economic systems don't incentivize work in the right way. Governing elites find themselves attempting to continuously "correct" accompanying forces in their nation's economy. And along the way, mistakes are sure to be made.

In 1956, for instance, the Soviets introduced innovation bonuses, which were linked to a given invention's productivity. However, they calculated productivity against a firm's total wage bill. That meant that labor-saving innovations could actually lose you money, since innovation reduced the wage bill!

Another feature of extractive systems is that leaders discourage creative destruction. That's because innovation — of whatever sort, and no matter how much it fosters growth — is a direct threat to an elite's position.

Finally, countries with extractive political systems are prone to elite infighting, which causes instability and limited growth. That's because everyone can see the massive rewards and riches that can be harvested once absolute power is achieved. Everyone wants a bite at that cherry.

### 11. Breaking the vicious cycles of poverty is difficult but not impossible. 

So far we've seen that sustainable growth in a society's living standard is possible. It just needs economic and political institutions that are inclusive and pluralistic in nature.

But what does that mean for future prosperity? What can countries do if they have extractive political and economic institutions today but want to buck the trends of history?

First off, it's important to realize that history isn't deterministic. That's just a fancy way of saying that the future isn't always shaped by the past.

As we've observed, extractive and inclusive institutions blossom and grow thanks to shifts in institutional landscapes after critical junctures. But it's not a predetermined route; virtuous cycles can be broken, as can vicious ones.

Just look at Britain and the rest of Western Europe. Truth be told, right up to until very recently, their institutions were highly extractive. However, critical junctures slowly guided these countries to more inclusive institutions — even if it took the Black Death and an awful lot of capitalism to get there!

More recently, the US South's exclusive institutions have been slowly becoming more inclusive after centuries of unequal rights for whites and blacks. There's still much work to be done but the civil rights movement of the 1950s and 1960s signaled that a change was "gonna come."

So what now? Well, we need to ensure that inclusive institutions are encouraged so that economic prosperity the world over can be fostered.

For instance, foreign aid has very little effect in challenging extractive institutions that extort communities across Africa and central Asia.

If positive change is to be promoted, then foreign aid needs to be directed more meaningfully. Groups currently excluded from decision-making processes need to be equipped so they can defy their countries' extractive institutions.

Brazil is a prime example. There it was an enfranchised and empowered people, rather than economists or politicians, who instigated change. It was due to a mobilized grassroots movement that the country's military dictatorship was expelled in 1985. Social movements such as those led by trade unions had laid the foundations for a strong anti-dictatorship coalition.

And with that breaking of the cycle, Brazil prospered. Between 2000 and 2012, its economy was one of fastest growing in the world.

The chain can always be shattered.

> _"The solution to the economic and political failure of nations today is to transform their extractive institutions toward inclusive ones."_

### 12. Final summary 

The key message in these blinks:

**Prosperity and poverty among nations aren't preordained fates, stemming from culture or geography. Instead, the main reason why some countries do better than others is their institutional landscape. This is shaped over the course of history, often over many centuries. The nature of a nation's institutions — namely, whether they are inclusive or extractive — is what determines prosperity. These trends can be bucked by targeting troubled countries' institutions. It will take effort, but vicious cycles of poverty the world over can be reversed.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _The Great Degeneration_** **, by** **Niall Ferguson**

In these blinks, we've seen how a society's power and prosperity lies in the strength of its institutions. Its message is essentially a positive one: nations can change, and cycles of poverty can be broken if institutions are reassessed and overhauled.

However, it may seem to sidestep what many readers think is a self-evident truth. To many, the West's century-long domination is waning, and its institutions are declining. The West has huge levels of public and private debt, while the economies in the rest of the world are quickly catching up.

_The Great Degeneration_ aims to explain what's going on and how this issue might be resolved. It too looks to institutions and suggests that the only way the West can recover is by radically reforming its wheezing institutions.
---

### Daron Acemoglu & James A. Robinson

Daron Acemoglu (b. 1967) is a professor of economics at MIT and ranks among the most highly respected economists in the world. He received the John Bates Clark Medal, which is regarded as the precursor to the Nobel Prize. 

James A. Robinson (b. 1932)is a political scientist, an economist and a professor at Harvard. He has done research in Latin America and Africa, and is widely considered an expert in foreign aid.

