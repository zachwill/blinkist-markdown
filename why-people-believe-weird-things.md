---
id: 548c5df03334320009090000
slug: why-people-believe-weird-things-en
published_date: 2014-12-17T00:00:00.000+00:00
author: Michael Shermer
title: Why People Believe Weird Things
subtitle: Pseudoscience, Superstition and Other Confusions of Our Time
main_color: 359A30
text_color: 277324
---

# Why People Believe Weird Things

_Pseudoscience, Superstition and Other Confusions of Our Time_

**Michael Shermer**

_Why People Believe Weird Things_ provides an overview of the most common pseudoscientific and supernatural theories. It'll teach you why so many people believe in them, why they're wrong, and what methods proponents of pseudosciences use to assert their incorrect theories. It also offers both rational arguments _for_ science, and rational arguments _against_ pseudoscience.

---
### 1. What’s in it for me? Learn about the strange things people believe in, and why. 

Superstition and belief in supernatural phenomena are by no means left in the past. Even today, people believe in a wide range of absurd things, such as divination, creationism or alien abductions. Some of these things may induce nothing worse than weary head shakes, and some can actually be dangerous. Why do these pseudosciences persist? Why do people believe in them?

_Why People Believe Weird Things_ attempts to answer these puzzling questions. In these blinks, you'll learn about some of the most commonly held pseudoscientific "theories," and the irrational arguments their proponents use to back them up. You'll learn what makes pseudosciences so different from actual sciences, and how to tell the difference between them. You'll also learn

  * how you can become a psychic in just a few minutes;

  * why the human wish for immortality will likely remain just a dream;

  * why the "tunnel" seen by people who have near death experiences is biological.

### 2. Science is based on facts – pseudoscience is based on belief. 

What separates real science from pseudoscience? Pseudosciences like astrology or creationism claim to be "scientific," but they actually reject scientific laws and methods.

Science is based on _laws_ that can be measured. A scientific law describes an action in nature that can be proved by tests. Once we've determined a certain law, we can use it as a basis for further theories and research.

Consider the law of gravity. We can test it again and again, and confirm that it's always true. Scientists can research the physical world further, knowing that the law of gravity will always be a part of it.

A _scientific theory_ can also be tested, but it's different from a law, because it's possible to prove a theory wrong. The fact that theories don't have to be correct is actually one of science's greatest strengths. Anyone can research a theory, and correct and improve it if necessary.

Pseudoscientific theories, however, aren't based on facts. They're based on belief, which also means they can't be proven wrong, because you can't disprove someone's subjective belief.

Unlike science, pseudosciences don't involve any sort of laws or evidence. Instead, they're based on intangible ideas or assumptions.

_Divination_, for instance, is based on the assumption that certain people have innate psychic powers. Yet throughout human history, no one has ever been able to prove that psychic powers exist. Because divination isn't based on evidence, it can't be tested.

Ultimately, pseudosciences can never be proven wrong — or right — which is where they differ from science. A scientist would say, "x is true because I can prove that x is true," whereas a pseudoscientist would say, "x is true because you can't prove that x is wrong."

### 3. Scientific knowledge always grows and improves, whereas pseudosciences are static. 

Since the dawn of humanity, scientific knowledge has been growing — and growing rapidly. Over the millennia, people have researched, tested and improved scientific ideas, so that our understanding of science is always increasing.

Each new scientific theory is based on the current body of knowledge. If a theory gets replaced by a newer one, we can still retain the useful information from it.

If a scientist can prove a theory wrong, they'll offer a better one, and they certainly don't have to abandon the rest of scientific knowledge. This means that science is always correcting itself, as people filter out the good and bad ideas, and replace the bad ones.

This happened when Darwin created his theory of evolution, which replaced the theory of special creation — the belief that the universe was made by God. Darwin's theory included a great deal of earlier scientific knowledge, such as anatomy and geology.

Unlike science, pseudosciences don't progress. Pseudoscientific theories are static because they're based on belief — if you doubt one element it means you have less than total faith, which invalidates the whole thing. These kinds of beliefs can't be tested or improved.

Creationism is a poignant example of this. Creationists try to put a scientific gloss on their beliefs, but if creationism were a true science, creationists would either have to successfully dismiss Darwin's theory of evolution with empirical evidence (which they've failed to do), or accept it. And of course, they can't accept it, because if they did they'd have to admit the Bible is wrong. So creationism can't claim any scientific rigor for itself.

Pseudosciences have little in common with actual science. Nonetheless, many people continue to believe in them. In the following blinks, we'll examine some common supernatural beliefs, and why they're wrong.

### 4. Divination isn’t magical – it’s simply guess work, close observation and research. 

If you watch a mind reader or psychic at work you might be surprised at the accuracy of their information. Accurate readings, however, don't mean that divination is real. Psychics use tricks to fool their audience into believing them, just like magicians.

There are two methods that psychics or other mystical people use to fool people into believing them.

The first is to give a _cold reading_, in which a psychic tries to guess details about the person's life, which is actually fairly easy if the guesses are vague enough.

The psychic will first ask some general questions until they find a topic the subject responds to positively, at which point the psychic goes into more detail, probing further. For example, the psychic might start out with, "Do you have problems in your relationship?" If they keenly observe how their client reacts verbally and physically to leading questions, they can make further educated guesses about their personal life and situation.

These mind readers also understand it's alright if they get a few things wrong, because the client will remember the hits much better than the misses. Fortune tellers also usually try to keep it positive, by saying things like "You'll find your true love soon."

In a _hot reading_, a psychic researches their client's life ahead of time. They're especially likely to do this before a show, because they want to make a good impression on viewers.

Psychics will often arrange informal meetings with certain spectators before a performance. It may seem harmless, but it's actually a good opportunity for the psychic to get personal information about people through small talk, then use it to wow the audience later.

Nowadays, hot readings are even easier because so many people share their personal information online. In many cases, all it takes to be a "psychic" is access to someone's Facebook or Instagram.

### 5. Near death experiences are purely biological, and can be explained. 

It's a marvel of modern medicine that many people survive severe accidents or life-threatening comas where they technically "die," but there's still some mystery: many of these survivors report the same experience. They say they felt they were leaving their body, and moving through a tunnel toward a light.

The similarities between these testimonies are one of the driving forces behind religions, mysticism and spiritualism. But while believers view the accounts as proof of life after death, scientists have found rational explanations for them.

Some hallucinations caused by chemicals are remarkably similar to reports of near death experiences. "Out of body" sensations can be induced by dissociative drugs such as ketamine or LSD. People feel they're leaving their body, and they might even watch themselves from across the room.

The fact that we have receptors in our brains for these artificially produced chemicals means that our bodies must produce them as well. So we can deduce that near-death out-of-body experiences are probably similar to those that people experience on drugs.

The common vision of the tunnel can also be explained. Many people who've experienced this tunnel (and some of those who haven't) believe it's a transition between life and death. However, the tunnel isn't a gate to heaven. We can actually explain it biologically. It's caused by abnormal cell activity in the visual cortex — the area of the brain where information from the retina is processed.

Hallucinogenic drugs or a lack of oxygen can interfere with the nerve cells in the visual cortex by making them fire at different rates. This produces lines and patterns that move across the cortex. The brain "sees" these patterns as spirals, and then connects this information with the bank of familiar images we store in our memories, to try to understand what they are. The nearest thing we have to these moving spirals is a tunnel — hence all those tunnels to the afterlife.

### 6. “Alien abductions” are really the result of psychological and physical stress. 

Stories of alien abductions are very widespread. How can so many witnesses be lying? Actually, they aren't, it's just that certain experiences can cause hallucinations that are so real that the affected person confuses reality and fantasy.

Many alien encounters are actually caused by not getting enough sleep. _"White-line fever"_ is a sleep disorder that's often experienced by truck drivers. Lack of sleep combined with the monotony of driving (and seeing the endless white lines on the road) leads to hallucinations, which can be quite extreme.

People who've experienced white-line fever report seeing bushes that turn into animals, or mailboxes that look like people. Of course, this sort of sleep deprivation can also lead to hallucinations about aliens.

Moreover, when people don't know they're hallucinating, these sightings get stored in their memory. Even after they recover, they remember the hallucinations as real experiences.

This is especially true when people report that they "remember" their alien encounter many years after experiencing it. This often happens under hypnosis, or after waking up at night.

These are simple explanations for this. During sleep or hypnosis, memory and fantasy can sometimes mix so much that it's impossible to sort them out afterward.

Alvin Lawson, a psychologist, once put some students into a hypnotic state and told them they had been kidnapped by aliens. After the students woke up and were asked about their alien kidnapping, they could provide very detailed stories — all untrue.

Rare mental sleep states known as _hypnagogic_ and _hypnopompic_ hallucinations also account for many alien reports. In these states, sleepers believe that part of their dreams are real in the first minutes after they wake, which is very disorienting. Psychologist Robert A. Baker, who analyzed these hallucinations, encountered many subjects who were convinced they had been carried away by martians.

### 7. Modern witch hunts and cults are just like those in the Dark Ages. 

In the Dark Ages, "witches" were hunted down and various cults were founded in the name of religion. We don't call people witches any more, but people still go on witch hunts today for no rational reason.

In fact, medieval witch hunts have the same characteristics as certain modern crazes. From time to time, some kind of mass hysteria is triggered. People get falsely accused of a crime, the media goes wild and people become convinced that an innocent person is guilty.

This happened in 1944, in the small town of Mattoon, Illinois. A woman claimed that a stranger anesthetized her legs by spraying some sort of gas on them. After a newspaper reported her story, more cases popped up and were heavily publicized. Newspapers ran headlines like "Anesthetic Prowler on the Loose," and the offender became known as the "Phantom Gasser of Mattoon."

Investigations soon revealed that the entire story had been made up. The police spoke of people's "wild imaginations" and the newspapers characterized the story as "mass hysteria."

This is exactly how witch hunts in the Dark Ages started. Someone made a false accusation, others became scared, the truth became irrelevant, and hysteria spread.

The flipside of the witch hunt is the cult — when one unusually charismatic person is elevated above others, and their political or philosophical views accepted without criticism. Cults have no basis in science or rational judgment either, as their followers try to justify every command their leader issues.

These characteristics are prevalent in modern cults like Scientology — a litigious new age religion with millions of followers. It emphasizes belief and unquestioning obedience, and functions as a religion, focusing on faith rather than any sort of actual science. Followers of the atheist novelist Ayn Rand have a similarly hysterical devotion to their figurehead: they think they are rational "Objectivists," but won't tolerate any questioning of Rand.

### 8. Immortality is scientifically unlikely. 

In 1994, Frank J. Tipler released a book called _The Physics of Immortality_, which initially received a lot of attention. It might be tempting to believe we have proof that immortality is real, but Tipler's pseudoscientific theory is untenable.

Tipler believed that by the far distant time when the universe collapsed on itself, humans would have invented a computer that could use the energy thrown up by that catastrophe to virtually recreate every human being from data stored in their memories. Mankind would be saved in a new virtual universe by a godlike computer.

Tipler's book includes a scientific appendix where he tried to explain his theory with mathematical formulas, and suggested how sufficient scientific progress could be made. However, even if it is _possible_ that his theory could come true, many rational arguments can be ranged against it.

The most important thing is that no one knows if all the prerequisites for building an all-knowing computer will ever be met. Science enables steady progress, but today we're nowhere near building a computer with the power to store an almost infinite amount of information, and we don't know if we'll ever be.

Tipler's theory also relies on many hypotheticals. The construction of his super computer lies far in the future, meaning many things need to occur in just the right way for it to happen. If one of these hypotheticals doesn't occur just as Tipler predicts, the computer might not be constructed at all.

So, although it's technically possible that the godlike computer will be built one day and we'll all be reborn in eternity, at the moment Tipler's theory is not coherent enough to be taken seriously.

### 9. Creationism goes against the very concept of science. 

Creationists believe that the Earth and all its living creatures were created by God. They reject Darwin, and view his theory of evolution as an attack on religion.

In reality, creationism is an attack on science. If creationists wanted to prove that God created everything, they'd have to first _disprove_ much of our knowledge about biology, geology or paleontology. Needless to say, they haven't had any success with this.

If Earth is not 4.6 billion years old, but rather 10,000 years old (as most creationists believe), then all theories about geological eras must be wrong, and dinosaurs couldn't have existed 100 million years ago. It is simply impossible to argue this given the physical evidence.

The opposite is true: a wealth of scientific evidence speaks against creationism. The fossil record clearly illustrates the evolution of living beings. Most scientists also agree on the big bang theory, and the age of Earth can be calculated almost exactly.

With so much evidence piled against them, creationists have the burden of proving their theory _right._ Naturally, they can't, so they focus instead on proving existing theories wrong. They fail at this, too.

For instance, creationists claim that we haven't found any fossils that show the transition between an ancient and a modern creature, which is untrue. They also try to show mathematically that our current population is descended from just Adam and Eve, thus "proving" the Earth must be under 10,000 years old. But mankind hasn't been steadily increasing in number each year at the rate they claim — population growth is more erratic.

Ultimately, creationists fail _both_ at proving their own theory, _and_ disproving the theory of evolution. Clearly, creationism has nothing to do with scientific method or scientific research. In fact, it's a complete rejection of the laws and conduct of science.

### 10. Holocaust deniers pretend to be sincerely interested in history, but they actually ignore historical records. 

Holocaust deniers acknowledge there was anti-Semitism in Nazi Germany, but insist that there was no planned extermination of Jews in Europe during World War II. They're emphatic: they just want to know "the truth."

According to them, Hitler's final solution for the Jews was actually deportation out of the Reich, not extermination. They believe the main causes of death at the time were disease and starvation. Moreover, they assert that between 300,000 and two million Jews died, rather than five or six million as most historians believe.

In this way, Holocaust deniers can pretend to be open to historical fact, because they do accept some things. But how do they argue their case in the face of overwhelming proof?

Holocaust deniers know that there must be some inaccuracies in that mountain of evidence, so they concentrate on their opponent's few weak points, and if they find an incorrect statement they'll conclude that _all_ their opponent's facts must be wrong.

A commonly used example is the story that Nazis manufactured soap from Jewish corpses, which is a myth. Holocaust deniers argue that if the soap story is historically wrong, the gas chambers must be a myth as well.

But there is a tremendous amount of evidence for the Holocaust, including official records, photographs and millions of testimonies. It's very clear to scientists and historians that the Holocaust happened, whether or not the Nazis made that soap. It is absurd to discount the entire Holocaust because one false story has arisen.

### 11. Final summary 

The key message in this book:

**Pseudoscience isn't comparable with real science. It isn't based on facts, but rather on superstitious belief or manipulation. Yet many people still believe in pseudoscientific or supernatural theories, some of which can be harmful, such as creationism or Holocaust denial. We need to stand up against pseudoscience by using rational, scientific arguments against them.**

Actionable advice:

**Don't go to a psychic.**

They don't have magical powers — they're just good at reading people's body language and making educated guesses about their personal lives. They might've even researched you in advance, which is especially easy if you post information about yourself online. An accurate reading doesn't mean they have psychic powers, it only means they're good at faking them.

**Suggested further reading:** ** _Going Clear_** **by Lawrence Wright**

_Going Clear_ offers a rare glimpse into the secret history and beliefs of Scientology as well as the conflicted biography of its founder L. Ron Hubbard. It also details some of the Church's darker qualities: a tooth and nail method of managing criticism and systematic approach to celebrity recruitment.
---

### Michael Shermer

Michael Shermer is a science journalist, historian and founder of The Skeptics Society — a non-profit organization of over 55,000 members that promotes scientific skepticism and fights pseudoscience. He's the editor in chief of _Skeptic,_ and writes monthly for _Scientific American_. He's also published many other books, such as _Why Darwin Matters:_ _The Case Against Intelligent Design_ and _The Science of Good and Evil: Why People Cheat, Gossip, Care, Share and Follow the Golden Rule_.

