---
id: 5c1a4e756cee070007a2ed79
slug: why-religion-en
published_date: 2018-12-21T00:00:00.000+00:00
author: Elaine Pagels
title: Why Religion?
subtitle: A Personal Story
main_color: 447A90
text_color: 376375
---

# Why Religion?

_A Personal Story_

**Elaine Pagels**

_Why Religion?_ (2018) is a personal answer to the question its title poses. Rather than explaining why anyone should adopt or eschew religious belief, Elaine Pagels's moving memoir shows how her life experiences led her to the study of religion, and how that study has helped her cope with the difficult events of her life.

---
### 1. What’s in it for me? Learn about life and loss from one of America’s foremost religious scholars. 

Why religion?

It's a question the author gets all the time. After all, she's dedicated her life to religious study, writing multiple groundbreaking books on subjects ranging from the controversial "gnostic gospels" to the origins of Satan. And yet she's not what she terms a "Bible-believing Christian." Indeed, she doesn't subscribe to any single religious creed.

So why religion?

These blinks offer a roundabout answer to that question. After embarking on a career as a religious scholar, the author suffered a series of personal losses that threatened to shatter her life. But instead of succumbing to despair and depression, she persevered. During the most difficult years of her life, her academic work was guided by the questions forced on her by loss. And how she coped with that loss was, to a degree, shaped by the stories of deprivation and survival that abound in religious texts.

In these blinks, you'll learn

  * how one woman survived the seemingly unsurvivable;

  * why grief often turns into guilt; and

  * that a happy ending isn't always a comfort.

### 2. Religion offered the author a means of expressing feelings that her family suppressed. 

When the author, Elaine Pagels, was 15 years old, she was offered a new life. Not that this offer was extended to her alone. Indeed, it happened in a stadium in San Francisco, where over 18,000 people had gathered to hear the evangelical preacher Billy Graham deliver one of his sermons.

Unlike most of the attendees, Elaine was there out of mere curiosity. She'd been invited along by a friend, whose father drove them from their hometown of Palo Alto, California, to listen to this charismatic Christian. She had no idea what to expect.

But despite her initial ambivalence, Elaine was touched and thrilled by Graham's invitation to religious rebirth. In the end, she came forward, along with thousands of others who'd decided to accept Jesus into their hearts. The crowd cheered, and the choir sang. Now they were "born again."

Elaine found it impossible to resist this invitation for one reason: her family.

She'd been brought up in a household that enshrined rationalism and suppressed emotion. Her father, a research biologist raised Presbyterian, became a staunch evolutionist after reading Darwin in college, dismissing Biblical stories as a bundle of nonsense that could appeal only to people unfamiliar with science.

But beneath her father's calm, rational exterior, rage simmered, always ready to erupt. Elaine's mother endured his outbursts in silence and was extremely sparing with expressions of maternal tenderness; to petitions from her daughter for motherly understanding, she'd invariably reply, "You shouldn't feel that way."

Neither parent encouraged physical closeness or expressions of emotion.

So, for Elaine, Billy Graham's words were an invitation to a new way of living. For her, becoming a born-again Christian meant freeing herself of her earthly father, who knew little about her, and adopting a heavenly father, who knew everything about her and loved her unconditionally.

To the 15-year-old Elaine, the right decision seemed clear enough, and she accepted Jesus into her heart.

Unsurprisingly, her parents were appalled when they found out — her father because he hated religion in general, and her mother because Elaine's choice had upset her father. These reactions frightened Elaine, but also pleased her, because they seemed to confirm that she'd discovered a new, better world, a place where emotions could be expressed freely and love reigned supreme.

What she couldn't have known then is how little time she'd spend there.

> _"I longed to turn to my mother for comfort, but she didn't have enough for herself, let alone any to share."_

### 3. Elaine abandoned Christianity after the death of her friend Paul. 

Roughly a year and a half after Elaine accepted Jesus into her heart, she received some terrible news. Her close friend Paul was dead, killed in a car accident the night before.

In the year leading up to this tragedy, Elaine had begun spending time with a group of artistic individuals. Paul was one of them. Together, they encouraged each other's artistic pursuits and interests. All of them were actors at the local community theater, and while driving around together at night, they would sing songs and recite poetry.

After Paul's death, Elaine grew even closer with these friends, each of whom was grappling with similar questions: What happens after death? How do you go on living without ignoring the inevitability of death?

Wrestling with these questions was difficult, but Elaine and her friends were engaged in that difficulty together. Though providing no clear answers, these friends offered support and sympathy — and that's much more than could be said of Elaine's family.

When Elaine's mother learned of Paul's death, she barely seemed dismayed. With typical aloofness, she said to her grieving daughter that Paul, who'd dropped out of high school to pursue painting, had been "no good" for her.

More devastating still, Elaine's Christian friends were unsympathetic. This hurt because for more than a year and a half she'd sung hymns and prayed fervently with them at their weekly meetings. When her fellow believers learned of Paul's death, however, they bluntly asked whether he'd been born again. Elaine replied that he hadn't and that he was Jewish to boot, and her friends drew the only possible evangelical conclusion: he was now in hell.

Deeply wounded, Elaine left the church, never to return. After this, she was never again a "Bible-believing Christian." Paul's death had, in this sense, killed God for Elaine. However, she remained religious in a broader sense.

That is, the music, poetry and imaginative metaphors of religious rituals remained a central — and key — part of her life. As the years passed, religion became a matter not of belief in any particular creed, but of how people "engage the imagination" to deal with life.

Of course, she hadn't yet adopted such an academic position when she abandoned her evangelical pals and their beliefs. But she was about to set out on the path that would take her there.

### 4. In graduate school, Elaine studied the gnostic gospels. 

In 1942, near the Egyptian village of Nag Hammadi, an Arab farmer dug up a man-sized jar that contained an ancient collection of books. These books eventually found their way to Cairo, where a French scholar, Jean Doresse, stumbled upon them. He was astonished when he deciphered the first words: "These are the secret words of the living Jesus, and the twin, Judas Thomas, wrote them down."

The Nag Hammadi library, as it came to be known, would end up shaping Elaine's entire life, both as a scholar and as a person.

Since Paul's death, Elaine had graduated from high school, finished her bachelor's degree at Stanford and been accepted to Harvard's graduate program in religious studies. She hadn't known it when applying, but Harvard was one of two US universities that would be granted access to facsimiles of the texts contained in the Nag Hammadi library.

These texts were exciting for a number of reasons. For starters, few scholars, besides Doresse and a handful of others, had ever seen them before. For example, the Gospel of Thomas, the text that initially caught Doresse's eye, was almost completely unfamiliar, with just a few fragments appearing in the New Testament.

Of course, as a student of religion, Elaine had heard of secret gospels. They were often referred to by scholars as "gnostic," because, rather than prescribing a set of beliefs, they urge readers to pursue _gnosis_ — a Greek word that means something like "insight" or "understanding."

This gnostic approach intrigued Elaine; she liked that, rather than comprising a strict belief system that insisted on unquestioning adherence, the texts offered a grab bag of sayings, poems and other texts that might lead to "knowledge of the heart."

However, there was a long history of orthodox — which literally means "straight-thinking" — Christians denouncing any Christian offering _gnosis_ as a heretic. In the second century, the bishop of Alexandria, a man named Athanasius, decreed that no Christian should accept heretical "secret books." But it seems that some monks went against Athanasius's wishes, removing 50 sacred texts from their library and hiding them in a large jar, which they buried near Nag Hammadi.

Elaine was elated and enchanted by the work of reading and translating these books. Driven and engaged, she finished her graduate program with distinction. Shortly thereafter, she wrote the book that launched her career: _The Gnostic Gospels_. This book not only introduced the general public to these fascinating texts; it established Elaine's reputation as an incisive and brilliant religious scholar.

### 5. Elaine’s son Mark was born with a life-threatening condition. 

Elaine was surprised, and not a little embarrassed, when her friend, the artist Mary Beth Edelson, proposed a ritual. Edelson reassured the doubtful Elaine that, though she'd only done it once, it had worked.

A ritual for what?

Well, Elaine was trying to get pregnant. She'd married her husband, the theoretical physicist Heinz Pagels, seven years earlier in 1969, while studying at Harvard. But so far, though they both desperately wanted one, they'd been unable to have a child.

So Elaine and five other women, including Edelson, met and conducted the ritual. By candlelight, with Elaine seated inside a big sculpture shaped somewhat like a birth canal, each woman spoke of her experiences of giving birth. As they spoke, a question took shape inside Elaine's head: "Are you willing to be a channel?"

This question helped Elaine realize that, without really knowing it, she'd been afraid of dying during childbirth. Then, more words appeared in her mind — but this time they formed a statement: "You don't have to do this; it does itself."

Whether the ritual is to thank or not, Elaine discovered she was pregnant less than a month later. And nine months after that, she gave birth to her son Mark.

But it wasn't all smooth sailing from there. The day after Mark's birth, doctors noticed that something was wrong with his heartbeat, and after doing an echocardiogram, they discovered the problem: there was a hole in one of the walls of his heart.

Luckily, there was a newly devised surgical method for fixing this condition, which would have been fatal a mere five years before. However, the doctors said it would be wise to wait a year, since operating on newborns is tricky and dangerous.

So the first year of Mark's life, though blissful, had a shadow hanging over it. At the back of Heinz and Elaine's minds lurked the same question: Would Mark survive the operation that awaited him?

### 6. Uncanny events surrounded Mark’s surgery. 

On the night before Mark's operation, Elaine refused to leave his room. With the surgery scheduled for early the next morning, Mark had already been admitted to the hospital, and Elaine defied a nurse's orders by spending the night on the floor, next to his bed. But she got little rest.

After a few anxious, sleepless hours, Elaine suddenly sensed that she wasn't alone. It seemed to her that a group of women, holding hands and seated in a circle, was there with her. In particular, she recognized a cherished colleague who'd retired from Duke University the year before. The group soon vanished, but Elaine felt comforted.

Then, near dawn, something more sinister occured. Though seemingly awake, she had a strange, dreamlike vision. A nightmarish figure, dark and distinctly male, though not human, approached her and silently threatened Mark's death.

Elaine stood it down, planting her feet, but it approached again, more threatening still. Again, she stood her ground — but when it approached a third time, she cried out "Jesus Christ!" This drove the figure away completely, and Elaine was suddenly sure the surgery would be a success.

And it was. Over the next few days, Mark began to show signs of recovery, and Elaine started to relax. On the fourth day, she drove home to change her clothes and found a note from the colleague she'd recognized in that circle of women the night before Mark's operation. It said that, on that same night, she and her "sister circle" had sat and prayed for Mark, Elaine and Heinz.

Another uncanny thing happened the next day. An older friend, seeing that Elaine was utterly exhausted, asked her whether, if she had a choice, she would continue her teaching work. Slightly taken aback, Elaine replied emphatically: _of course_ she'd rather not work — at least, not right now!

Oddly enough, Elaine learned later that day that a Mr. MacArthur had been trying to reach her. When she called back, a man named Rod MacArthur answered, telling her that she'd been awarded a MacArthur Fellowship, which would support her for the next five years. Unbeknownst to her, she'd been nominated; work would no longer be a necessity.

An incredible gift in and of itself, this fellowship proved invaluable to Elaine, since it allowed her to spend precious time with Mark — whose life would be cut tragically short.

### 7. Mark developed a rare lung disease and died at the age of six. 

No cure. No treatment. Fatal in all cases.

Less than a year had passed since Mark's open-heart surgery, and Elaine had taken him to the hospital for one of his regular checkups. Now she listened in disbelief as the doctors told her what they themselves could hardly believe: tests had revealed that Mark had pulmonary hypertension, a rare and terminal lung disease.

Elaine and Heinz were devastated, hardly able to comprehend the new, perpetual shadow that now hung over Mark's life. But, despite this horrible darkness, Elaine and Heinz were determined to fill Mark's remaining years with hope and brightness.

An inseparable family, they never spent more than a day apart. Every year, they'd go to Santa Cruz, California, living in a cabin surrounded by redwood forest, feeding the horses that were pastured nearby. But just because they enfolded him in love didn't mean they coddled him.

For instance, when Mark began preschool and then kindergarten, Elaine and Heinz decided not to tell his teachers of his condition, fearing that this knowledge might cause them to handle him like a delicate, breakable thing.

Yet Mark seemed somehow aware that his body was fighting for its life, though Elaine and Heinz had decided early on not to bring up the subject of death. Once, standing atop a large tree stump, he assumed a warrior's posture and announced that he was "here to fight." For Elaine, this was truer than Mark could have known.

And two days before his death, when Elaine went to his room to sing him to sleep, he embraced her and said that he'd love her "all his life, and all his death." Heinz, after hearing this, held Elaine close, and they both wept.

Then, one day, Mark barely ate anything. Elaine, alarmed, took him to the hospital. Upon arrival, a nurse drew some of Mark's blood — and then everything happened fast: Mark passed out, his body stiffening, his eyes rolling up. Elaine, looking for help, lost consciousness.

When she came to, Heinz was there, telling her that it was all over. But Elaine protested — why couldn't they ask Mark to come back? At that moment, she sensed that Mark was there, above them in the room.

As it turned out, his heart, which had stopped, had started beating again. Heinz and Elaine rushed to his side, but he remained unconscious, and the heartbeats soon ceased.

### 8. Elaine was racked by guilt after Mark’s death, and she realized the Bible had conditioned her to feel that way. 

After Mark's death, Elaine wasn't only cast into the deepest despair; she was also racked by guilt. It had been her job, as a mother, to protect his life, to keep him out of harm's way — and she'd been unable to do those things. Had she therefore failed as a parent?

Such questions weighed on her unbearably. Grasping for answers and hoping for relief, she recalled stories from the Bible, noting that grief is often entangled with guilt in such tales.

For example, consider the early death of King David and Bathsheba's firstborn son. The Bible explains that, to punish David for having sex with Bathsheba, who was married to another man, "the Lord struck the child…and it died."

Or consider the story of Sodom. In that tale, the Lord destroys the cities of Sodom and Gomorrah, and everyone in them, simply because "the men of Sodom" were evil.

Such tales moralize history, clearly asserting bad things happen only to people who deserve it. And Elaine realized that, partially because of such stories, she still longed to believe that the universe worked this way, that someone or something was keeping track of life's injustices. Furthermore, she began to see that her feelings of guilt were a way of clinging to this comforting vision of a just universe.

She realized that, by heaping guilt and blame upon herself, she was making an assumption: _someone_ must be to blame for Mark's death. She reflexively blamed herself, because that's what Christianity has conditioned people to do. In a world governed by a God that is benevolent and almighty, the fault is always yours.

Elaine, however, could no longer stand the weight of guilt. So she began to adopt a vision of the world that resembled her husband's. Heinz had been working on chaos theory, and he often spoke of the randomness of events, so Elaine deliberately shifted her perspective. She would try living in a world where things simply happen, without explanation or warning.

But her decision to occupy such an unforgiving world would soon be challenged.

> _"Now I was living in a world where volcanoes erupt because that's what volcanoes do… and in which children often die young, for no reason we can find."_

### 9. Just as Heinz and Elaine were beginning to move forward, Heinz fell to his death. 

Elaine had heard that parents who lose a child often separate. But this didn't happen to her and Heinz. In fact, they grew even closer, welded together by the strength of their love and the strain of their loss.

What's more, they were still parents. About a year before Mark's death, after a miscarriage, Elaine and Heinz decided to adopt a baby girl, Sarah. And about a year after his death, they adopted a baby boy, David. Though still grieving Mark's death — for Elaine, that grief would never really fade — it began to feel possible to move forward with life.

But then the unthinkable happened.

Roughly two years had passed since Mark's death, and Elaine, Heinz, Sarah and David were spending the summer in Colorado. A group of friends were also there, among them one of Heinz's doctoral students, Seth Lloyd, who, like Heinz, enjoyed hiking.

One morning, Seth and Heinz set out to trek up nearby Pyramid Peak. But the hikers didn't return in the early afternoon, as they usually did. Instead, Elaine got a phone call: Heinz had fallen.

After asking a friend to watch the children, Elaine rushed outside. What if Heinz had really hurt himself? What if he was unable to walk or to help care for Sarah and David? Worries swirled in her mind.

She didn't get any information until Seth returned, accompanied by two policemen. Elaine doesn't remember what was said, but the message was clear: Heinz was dead. The path, Seth explained, had crumbled beneath his feet, and he'd fallen some three hundred feet.

Unable to speak, Elaine also stopped herself from weeping. "If I start now," she thought, "I'll never stop." It felt as though her life had been swallowed by an incomprehensible darkness.

She got another call just before nightfall. Heinz's body had been found. Friends drove her to the hospital, and she was led to a back room, where there was a body bag on a table.

The doctor said that, usually, he'd encourage her to see the body, but that, in this case, he wouldn't allow it. The body had been found in pieces.

In an oblivion of grief, Elaine left the hospital. That night, a vision began to repeat itself in her head: Heinz falling, over and over and over again.

### 10. After Heinz’s death, Elaine found some comfort in the Gospel of Mark. 

Shortly after Heinz's death, Elaine sat silently in a chapel. In that deep quiet, she felt that she could talk to Heinz. And so she asked him how he felt about what had happened. Though she hadn't expected a reply, an inner voice immediately responded.

Unexpectedly, the answer she heard was, "This is fine with me." "How," Elaine asked, internally, "could it be fine?" Heinz had abandoned her and their two babies. Was that _fine_?

Elaine certainly wasn't fine with it. Grappling yet again with unbearable grief, she turned back to Biblical stories — particularly Mark's gospel.

The original story told by Mark opens with an optimistic announcement: "good news of Jesus, Messiah, Son of God." But then it goes on to relate a series of tragic events. Jesus, after being captured, mocked and tortured, is sentenced to public execution, though he's innocent. At the apex of his agony, crucified, he cries out, "My God, my God, why have you abandoned me?"

The story ends with an unsettling scene: Jesus's female followers go to his grave and find his body missing.

Decades earlier, when Elaine first read this version, she'd wondered how such a story could possibly qualify as "good news." She wasn't the first to have such a reaction; in fact, one of Mark's early readers, convinced that a more upbeat ending was intended, is thought to have written a new one.

In this ending, after dying, Jesus appears three times, first to Mary Magdalene, then to two of his disciples and, finally, to all his disciples. After this third appearance, Jesus is "taken up into heaven, and sat down at the right hand of God." Now sure that Jesus had indeed returned from the dead, his joyous disciples take to the street, proclaiming "the good news everywhere."

In the case of Heinz's death, there was no possibility of an alternative, happy ending. And so Elaine looked for what Mark might have originally meant by "good news."

On her reading, there is hope embedded in Mark's original gospel. For the story, though ending in defeat, suggests that God's victory _is_ coming, just as Jesus said it was. It just might be coming at a point in time beyond the story's narrative frame.

Though Mark and Heinz were dead, Elaine took comfort in the thought that something as yet invisible, but hopeful, might be afoot in the universe.

> _"For even while telling how Jesus was tortured, mocked, and killed, Mark suggests that what looks like total defeat may end in hope."_

### 11. Decades later, Elaine received an honorary degree from Harvard and felt that her heart had healed. 

Nearly 25 years after Heinz's death, Elaine received an honorary degree from Harvard. At first, she didn't want to attend the ceremony. Heinz had been at her first Harvard graduation; how could she bear to go to the second one without him? In the end, however, accompanied by Sarah and David, now both in their mid-twenties, she did go.

And sitting in that crowd of graduates, she realized that she'd graduated in a deeper sense. She'd passed life's true test: surviving despite unbearable loss.

At that moment, she was filled with gratitude and overcome by tears. Her two children were alive — as was she, though it barely seemed possible.

In the end, she thought, her engagement with religious texts had helped give her precisely what she'd needed: medicine for the soul.

The Nag Hammadi library even seems to mention such medicine. Book VI of the Nag Hammadi begins with a tale of the twelve apostles. Afraid and downcast, grieving for the dead Jesus, they encounter a man who calls himself a "physician of souls." This professed physician later turns out to be Jesus himself. He gives them two items: a pouch of medicine and a box of ointments. He tells them to "first heal [people's] bodies," and then to "heal the heart."

Six texts follow this one, and Elaine speculates that _they_ might be the "medicines" — the means to healing the heart. The subject matter of these texts varies greatly, from a poem titled _Thunder_, which rejoices in the presence of the divine, to a brief writing on "the mystery of sexual intercourse," to excerpts from Plato's _Republic_. If interpreted as "medicines," the variety of subject matter suggests that whoever compiled them knew that different people would need different "prescriptions."

During Mark's illness and death, and then after Heinz's death, Elaine sought healing with an urgency she couldn't previously have conceived of. The Nag Hammadi not only helped her find relief by providing heart-healing texts; it also made obvious that she wasn't alone — that, for millennia, people have been seeking these same medicines for similar ailments.

On that graduation day, surrounded by family and fellow graduates, Elaine knew that a heart sometimes _can_ heal.

### 12. Final summary 

The key message in these blinks:

**Elaine Pagels, after early encounters with religion and death, decided to pursue a career in religious studies. After the death of her son Mark, and then the death of her husband, Heinz, she found a sort of comfort in the religious texts she'd dedicated her life to studying. Though not a God-believing Christian, she remains deeply religious.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**What to read next:** ** _Religion for Atheists_** **by Alain de Botton**

Elaine Pagels, though not a "Bible-believing Christian," dedicated her life to the study of religion. And she's grateful she did. When her life was shattered by loss, she was able to draw on Biblical stories, which helped her to see how the unbearable might be survived.

Intrigued by an approach to religion that won't shackle you to God?

Well, that's exactly the angle Alain de Botton takes in his book _Religion for Atheists._ In de Botton's view, one needn't be religious to benefit from religion. Whether you're looking for a God-free version of life's deeper meaning or are simply longing for some undogmatic philosophical insight, we recommend the blinks to _Religion for Atheists_.
---

### Elaine Pagels

Elaine Pagels is a renowned religious scholar. Currently a professor at Princeton, she received a MacArthur Fellowship in 1981 and was awarded a National Humanities Medal in 2016. Her previous books include _The Gnostic Gospels_, _The Origin of Satan_ and _Adam, Eve, and the Serpent_.

