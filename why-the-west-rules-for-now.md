---
id: 5a23a298b238e10007f56702
slug: why-the-west-rules-for-now-en
published_date: 2017-12-04T00:00:00.000+00:00
author: Ian Morris
title: Why The West Rules – For Now
subtitle: The Patterns of History, and What They Reveal About the Future
main_color: CEA434
text_color: 826821
---

# Why The West Rules – For Now

_The Patterns of History, and What They Reveal About the Future_

**Ian Morris**

_Why The West Rules — For Now_ (2010) is a treatise on Western rule. It examines what "the West" is and how its current dominance came about. Starting with the earliest development of humankind, it rules out racist genetic beliefs and theories of cultural superiority. It describes how East and West have been locked neck and neck in a race for advancement up to the present day. And, of course, it goes on to address the question: will the West's dominance last?

---
### 1. What’s in it for me? Learn how the West became the West. 

Without doubt, the modern world is dominated by the West. The West, particularly England, was the crucible of the Industrial Revolution that began in 1760. It boosted Western economies, and since then the dominance of the West has been unquestioned.

But there's more to history and understanding the world than just contemporary politics. There are other ways to explain current Western rule.

To uncover this, we have to go back thousands of years to the birth of two civilizations: one in Mesopotamia and one in China. The birth of these two civilizations was the beginning of the West-East divide. Since then, each has had its own respective golden age. The West is currently outpacing the East, but for how long will it last?

In these blinks, you'll learn:

  * why the earliest artistic cave paintings were found in Europe and nowhere else;

  * how one eighteenth-century Scottish inventor helped the West leap ahead of the East; and

  * when the Chinese-dominated East will take over from the West in the future.

### 2. Common theories for today’s Western dominance are easily debunked. 

There's no escaping it. The West still dominates global politics and development. The explanations for how this came about are varied, but they broadly fall into two schools of thought.

There are what have been termed "short-term accident" theories. They argue that today's Western rule results from mere historical luck. In contrast, "long-term lock-in" theories advocate that some sort of critical factor exists in the foundations of the West, consequently "locking in" the certainty of Western dominance millennia ago. Unfortunately, many proponents of lock-in theories favor arguments for Western genetic or cultural superiority.

It's not just that lock-in theories are problematic; they also don't hold water.

Let's take long-term lock-in theories based on biological reasoning as an example. They argue that the genetic lock-in occurred 600,000 years ago when two distinct species of _Homo_ developed, the Eastern _Homo erectus_ and the Western _Homo antecessor_.

But that means nothing, as _Homo sapiens –_ modern humans — superseded both of these species around 300,000 years ago. Racial theories based on genetic superiority can, therefore, be disproved.

But how do cultural lock-in theories stack up?

In 1879, archaeologists discovered incredible cave paintings of animals in Altamira, dating back 30,000 years. No other art this old has been found. This information is used by some lock-in theorists to suggest that Western culture is uniquely creative.

But it's just coincidence. At the time, Europe was confronted with the Ice Age. Consequently, early Western humans spent more time in caves keeping warm. That's where they refined their drawing skills. The argument's strengthened by the fact that after the Ice Age ended, we have no more evidence of similar cave drawings.

We're going to have to delve a good deal deeper to explain today's Western dominance. We'll find that neither long-term lock-in nor short-term accident theories can explain it.

As we'll see, it's all about a dynamic interplay between biology, sociology and geography.

### 3. Eastern and Western historical development has barely differed. 

West and East are amorphous terms, so the author is precise. For him, the West began in the Fertile Crescent in the Middle East and Egypt and expanded westward from there. The East encompasses civilizations that developed between the Yellow and Yangtze rivers in China.

If you want to compare East and West, you must start by evaluating social development scientifically and rigorously.

Social development is a measure of a community's ability to get things done. Developments include a society's technological, organizational or cultural accomplishments. These, in turn, allow people to feed, clothe, house or reproduce themselves.

The author created a social development index for 14,000 BCE onward. It is based on four fundamental characteristics.

The first is _energy capture_, a measure of consumption per person in kilocalories per day. Populations have to extract energy from plants and animals to feed themselves, and for some community members, it's particularly important. Soldiers and sailors need energy for waging war.

_Urbanism_ is the second trait. Think of it as representing organizational capacity. Population figures are the best guide for measuring it.

The third trait is _information processing_. This is society's ability to communicate and transfer knowledge. For example, access to reliable maps impacts on mariners' ability to travel between Asia and Europe.

The last trait is a society's _capacity to wage war_. It's one thing to extract energy, organize and communicate knowledge. But it's quite another to harness these three traits for destruction.

The social development index shows there's little difference between the East and West. Similar patterns are traced in both, though the West slightly outscores the East.

The scores both resemble an exponential curve. They rise slowly for thousands of years, then skyrocket at the start of the eighteenth century, once the Industrial Revolution begins operating at full steam.

> _"Scientists are often criticized for taking the wonder out of the world, but they generally do so in the hope of putting truth in its place."_

### 4. The West got a running start after the last Ice Age thanks to geography. 

About 100,000 years ago the world slid into a long glacial period. Ice covered large portions of the Northern hemisphere.

To survive this harsh environment, humans developed a strategy. They lived as small groups, set up camps, killed animals and scavenged for edible plants before moving on to the next location.

But, by 11,700 BCE, the world was warming, and the Ice Age came to an end. Change was in the air. It is from about this time that we can distinguish between Eastern and Western geographical "cores." Agriculture was the distinction between what became the essential territories of East and West; it developed some 1,500 to 2,000 years earlier in the West.

The archaeological record shows that by the end of the Ice Age humans had begun to work the land and settle in larger groups.

This can be seen most clearly from a site in the Western core, the Hilly Flanks, a crescent-shaped region in southwest Asia that stretches from the mouth of the Tigris and Euphrates in southern Iraq to the eastern seaboard of the Mediterranean.

By 7000 BCE, farming was pervasive in the region, and the agricultural societies there spearheaded civilization. Otherwise, beyond the Hilly Flanks, the earliest evidence for farming is found in China, where rice cultivation had begun in the Yangzi valley between approximately 8000 and 7500 BCE.

Geography also played a part in giving the West its head start. Most modern cereals, like wheat, corn, rice and barley, evolved from grasses that were concentrated in the Hilly Flanks. That's equally true for domesticable species like sheep, goats, cows, and pigs. These too were species native to the Hilly Flanks.

Eastern agriculture, primarily centered in China, didn't take longer to begin because it was inferior, but because it just didn't have the geographic conditions required to trigger its development.

### 5. By the first millennium BCE, Eastern and Western social development were almost level. 

After ten thousand years of coursing ahead, the West found that its lead over the East had been reduced to a whisker.

In 1200 BCE, Eastern development was a thousand years behind. However, the West's sudden crisis in the Mediterranean meant that Eastern social development could begin to catch up. This crisis is known as the Late Bronze Age collapse.

Western civilizations, like that of Mycenaean Greece, faced difficulties in keeping their empires together and stopping them from fragmenting. Even Ramses II's Egyptian empire crumbled into smaller pieces. People went hungry, cities depopulated and armies were reduced in scale.

Archaeologists still aren't sure how the Western crisis and collapse came about. Most likely it occurred because of the destructive interaction of climate change, famine, state structure disintegration, migration and maybe even disease between 1200 and 1000 BCE.

The West's implosion effectively reduced its lead over the East by six centuries. By 1000 BCE, the East's social development score stood only a few hundred years behind the West's.

At about this time, in the first millennium BCE, both East and West began to restructure themselves in similar ways. Specifically, societies began to shift from being _low-end_ _states_ to _high-end states_.

In the original low-end states, rulers had little expenditure and so were not dependent on large tax revenues. They relied, for example, on local elites to muster armies, rather than paying for them themselves. In contrast, high-end states centralized power and built bureaucratic apparatuses for collecting taxes.

Both the East and West laid the foundations of high-end states during the tenth century BCE. However, it was in the West that the first extensive high-end states emerged. Just think of the Assyrian empire, which reached its zenith around 660 BCE.

### 6. The start of the first millennium saw the rise and fall of great empires. 

The arrival of centrally organized high-end states meant the age of empires had arrived. In the West, the Assyrian and Persian empires were the first fully-fledged high-end states. In the East, it was the Zhou dynasty (1046–256 BCE) that blazed the trail.

But these forerunners paled in comparison with what came next. In the East, the most prominent high-end state was the Chinese Han Empire. In the West, the Romans were dominant.

The Roman Republic had been founded in 509 BCE, but it was not until about 200 BCE that it achieved superpower status.

By 201 BCE, the Romans had essentially defeated their great rival in the Mediterranean, the Carthaginian Empire, which was based on the North African coast. The Romans now controlled huge portions of the Mediterranean coastline, a quite remarkable achievement.

In the Far East, the Han dynasty dominated China. Their empire existed between 206 BCE and 220 CE and was one of the largest ever to have existed.

These empires in East and West actually had a lot in common. Each had its own literate, philosophically trained elite based in large cities. And these cities were, in turn, supported by highly productive agriculture and expansive trade networks.

Nonetheless, Eastern and Western empires disintegrated in the first centuries of the Common Era. In 285 CE, the Roman Empire divided itself into Eastern and Western provinces. The Western provinces only managed to stumble on until 476 CE. The great Mediterranean-wide empire was no more.

In China, the empire also split. By around 400 CE, the Jin dynasty was ruling the southern part of the former Han empire, while northern China was further divided into five smaller kingdoms.

The reasons for their downfalls were similar in both East and West. The outer frontiers were under constant attack from nomadic barbarians, while the central administrations broke apart. They simply couldn't hold themselves together.

### 7. In 1100 CE Eastern social development peaked, but the West split. 

Western social development continued to outpace Eastern at the start of the first millennium. The Roman Empire at its peak had double the coinage in circulation as the Han dynasty.

It was only when the western half of the Roman Empire began its decline that the Chinese Eastern empire hit its stride. For starters, the East recovered more quickly from the downfall of its early empire. By 1100 CE it had reached a new peak in social development and overtaken the West.

It was the Sui dynasty that reunified China's north and south. Wendi, the first Sui emperor, conquered southern China and did so without devastating its economy. Once united, the conditions were in place for a China-wide economic boom. It was a golden age. China's farmers were also greatly assisted by the Medieval Warm Period and the resulting increase in rainfall in the semi-arid north. Greater yields from the fields meant that China's population could grow to 100 million in 1100 CE.

It was then that Eastern social development finally reached the heights previously achieved by the Roman Empire a thousand years earlier.

From about 700 CE, the West too began to slowly recover. However, the region was further subdivided into two spheres: Muslim and Christian.

Much of the West, including Spain, Northern Africa and the Middle East, was conquered and largely united by Muslim Arabs, but Christendom retained a peripheral influence in northern Europe.

In the West, too, the Medieval Warm Period was transformative. There it resulted in the devastation of dry Arab heartlands in southwest Asia. The Western center, therefore, gravitated toward the Mediterranean. Trade became concentrated in cities like Muslim Palermo and Cairo, and Christian Venice and Genoa.

Increased cultural exchange followed hot on the heels of trade expansion. The foundations of the Renaissance in Christian Europe were ultimately laid thanks to transmission of scholarship and knowledge from the Muslim empires. Geography had once more played its part.

### 8. Between 1000 and 1500 Western social development leaped forward due to new trading routes. 

You'll no doubt know the Venetian merchant Marco Polo who composed his celebrated travelogue at the start of the fourteenth century. It was a marvelous account of his journeys in the East. The Chinese palaces and the wealth of rulers he described knew no equal in the West. China's social development was simply eons ahead of his world's.

But things would soon change. By the late thirteenth century, Eastern social development had plummeted. China was fighting interminable wars against the Mongols on their northern frontiers. In the wake of the Mongol advance, China's complex infrastructure collapsed. Instead of an expected industrial advance, China was faced with destruction, famine and disease. By the time Polo put pen to paper the world he portrayed had already ceased to exist.

For all their military might, the Mongols never made it to Western Europe. The West was therefore free to undergo its own resurrection: the Renaissance began in Italy around 1300. In this period, a largely Italian cultural elite was inspired by Greek and Roman knowledge, which Arab scholars had thankfully helped preserve. The resulting atmosphere of innovation and inquiry also inspired Western adventurers to sail the ocean blue. The contrast with the Mongol-ravaged East could not have been greater.

Columbus's 1492 journey to America was an early indicator that the West would go on to turn the oceans into commercial highways. The West was out in front again. And moreover, it was geography that had once more been the deciding factor. Most likely, fifteenth-century Chinese vessels could _theoretically_ have reached America. But geography always favored the West. After all, the Atlantic route from Europe stood at 3,000 miles, while the passage over the Pacific was more than double that.

Nevertheless, despite Western progress, China's social development lead remained just about secure.

### 9. The Industrial Revolution signaled the start of Western rule. 

Between 1500 and 1800 the social development scores of both the East and West continued to rise. But the pace of change was uneven. Western social development was advancing at twice the rate of the East.

By the late eighteenth century, the West had finally managed to overtake the East.

Thanks to the discovery of the New World, Western trade was booming. This was due to the fact that colonists were able to exploit the resources of the American continent. They produced commodities that were not native to Europe or else grew better abroad, like cane sugar. These products were then shipped back home.

Furthermore, the new modern sciences of Europe began to make an impact. Simply consider how Newton's description of classical physics and mechanics enabled technological advancement to stride forward.

Nothing represented this progress better than Scotland's James Watt developing the first practical design for the steam engine.

It was this steam engine that powered the Industrial Revolution in the West.

In about 1750, East and West were still strikingly similar. But, by 1850, steam power had blown the East out of the water. For centuries, man had been dependent on animals, wind and water power. But the steam engine made power production both portable and geographically independent.

Soon after, steamships and trains came chugging into view. The world was becoming smaller, and global trade only continued to fill coffers in the West. In this febrile atmosphere of invention, communication also improved: the telegraph was revolutionary in its time.

It was clear what would come next. The Industrial Revolution had set Western rule in motion.

### 10. Despite major wars, the twentieth century was a high point for the West. 

The twentieth century was grim. It saw the largest, most abominable and disastrous wars known to humankind: the First World War and the Second World War. The Cold War that followed stretched over almost half a century, between 1947 and 1991.

Among them, the three wars resulted in a hundred million deaths and threatened human survival itself.

But despite these wars, Western dominance reached its peak.

World War One was destructive, but it reduced the power of Europe's archaic dynasties and allowed democracy to spread across the continent.

Even after the intercontinental devastation of World War Two, the West still did not collapse and was in reasonably good shape compared to the East. For instance, the Soviets were able to quickly rebuild their industries, while the American mainland never suffered from enemy bombs. In contrast, the Japanese devastation of China and the US bombings of Japan both gutted the East.

Finally, despite the atomic standoff between the United States and the Soviet Union and the threat of nuclear war, the United States won the Cold War in the West in 1991. So Western prosperity could increase again.

But the East was closing in fast. In the 1990s, China opened its markets to economic reforms, including large-scale privatization. Consequently, China's economy skyrocketed.

In 1970, 22 percent of the world's goods were produced in the United States, while China made only 5 percent. American workers were 20 times more productive than the Chinese.

But by 2000, Americans were just seven times more productive, and China's share of global production had risen to 14 percent. America's had stagnated at 21 percent.

China had become the workshop of the world.

Western rule reached a high point in the twentieth century. But was the end looming?

### 11. The East is expected to regain its lead by 2103, but there are many unknowns. 

So the West rules — for now. But how long will it last?

The author has extrapolated Eastern and Western social development indices and calculated that the East will regain its lead by 2103. Or rather, that will be the point when Western rule comes to an end.

The narrowing of the difference between the economic outputs of the United States and China is responsible. Bankers at Goldman Sachs predict that China's output will reach US levels by 2027. The accountants at PricewaterhouseCoopers, on the other hand, estimate 2025 is the more likely date. It's no wonder: China's incredible 7.5 percent annual growth rates are well above those in Western economies.

In the military sphere, information technology and per capita energy capture, it may take the East a little longer to overtake the West. The author suggests it will take until 2103 for the East to reach parity, assuming that it improves in these areas after 2050.

But, of course, the future is unpredictable, and there are many unknowns.

Some observers suggest prosperity will westernize the East. That's to say, cultural, political and economic systems increasingly resemble one another in a globalized world. And if the whole world is "Western," why continue to distinguish between East and West?

Additionally, the significant majority of recent scientific and technological advancements — such as genetic research or modern computing technology — have been made in the West. This too might be indicative of its continued rule.

And as for the unknowns of climate change, migration, pandemics and war, who can say?

We can't be certain that Eastern development will overtake the West. But events are trending decidedly in that direction.

### 12. Final summary 

The key message in this book:

**Today's Western rule is neither due to long-term lock-in nor is it a short-term accident. Racist genetic theories or ideas of cultural superiority don't hold weight and certainly can't explain history. The East has been more advanced than the West in the past, and it is likely that East will one day dominate again.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Lessons of History_** **by Will Durant and Ariel Durant**

_The Lessons of History_ (1968) gives an overview of more than 5,000 years of human history. It covers changes in morality, religion and governmental systems like socialism and capitalism, and traces the historical trends of war. Along the way, it offers a variety of lessons on what history means for the present.
---

### Ian Morris

Ian Morris is the Jean and Rebecca Willard Professor of Classics and a fellow of the Stanford Archaeology Center. He has written and edited a number of academic books, including _The Greeks_ and _The Dynamics of Ancient Empires_. Morris is a popular guest on television shows.

