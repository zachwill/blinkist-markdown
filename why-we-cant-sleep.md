---
id: 5e5f970e6cee0700069aea34
slug: why-we-cant-sleep-en
published_date: 2020-03-06T00:00:00.000+00:00
author: Ada Calhoun
title: Why We Can't Sleep
subtitle: Women's New Midlife Crisis
main_color: None
text_color: None
---

# Why We Can't Sleep

_Women's New Midlife Crisis_

**Ada Calhoun**

_Why We Can't Sleep_ (2020) explores the question of why American women from Generation X are experiencing a midlife crisis. These women were sold the story that nothing was stopping them from achieving their wildest dreams, when in reality they've faced enormous challenges to get ahead. Not only did they graduate into a disastrous job market, they also face gender and age discrimination and all too often end up having to care for young children and elderly parents simultaneously. Everything they've achieved has been against enormous odds.

---
### 1. What’s in it for me? Discover the untold story of women’s midlife crises. 

By now, we've all become very used to the trope of the male midlife crisis. The stereotypical man will wake up one day deeply unsatisfied with his life. He'll buy a yellow Ferrari, grow a beard, or take up surfing in Hawaii as a hobby. Or, of course, he'll leave his wife for a much younger woman.

But what about women? Women are depicted as reacting to these crises, but of course, they have their own. This is perhaps truest for women of Generation X, who were born between 1965 and 1980 — a time of great economic and political instability. These women were promised that they could achieve it all: a successful career as well as a thriving family and social life.

Reeling under the weight of these expectations, Gen X women are in the throes of a serious existential crisis as they reach middle-age. They are anxious and furious, and they lie awake at night. However, this is usually hard to see from the outside, as they seldom destroy their marriages or buy vehicles in lurid colors.

In these blinks, you'll get a picture of what midlife crisis actually looks like for women, what causes it, and what kind of support women really need. 

In these blinks, you'll discover 

  * why debt is destabilizing Gen X's plans for success;

  * the extra responsibilities that fall on Gen X women; and

  * how accepting that life is hard can actually be empowering.

### 2. Generation X women were told they could achieve anything – but the dream didn’t measure up to reality. 

Every girl who grew up in America in the 1980s is familiar with the Enjoli perfume ad. It showed a voraciously energetic blonde woman exchanging a smart business suit for some homey clothes while she cooks dinner for her family, then transitioning into a femme fatale in a cocktail dress poised to seduce her husband. And she seemed to retain her buoyant energy throughout!

This ad encapsulates the dream of "having it all" that American women born into Generation X grew up with, a promise that women can do and _be_ anything. They didn't have to be nurses; they could be doctors instead. They didn't have to settle for being a secretary when they could be the CEO.

This promise was tremendously exciting, and showed the progress of second-wave feminists. These women achieved wins like passing the federal law Title IX in 1972, which proclaimed that girls could no longer be discriminated against in federally-funded educational programs.

But these new possibilities also came with tremendous pressure. The promise that girls _could_ do everything morphed into an imperative. Girls _must_ do everything. Now that women had these freedoms, there was theoretically nothing stopping them achieving their dreams except a lack of imagination or unwillingness to put in the work.

However, women born into Generation X have faced structural barriers to achieving their ambitions that have nothing to do with their individual qualities or drive. For one thing, they grew up through successive recessions which caused economic precarity and job insecurity, meaning that women who grew up in this generation are often saddled with enormous amounts of debt. For another thing, gender roles haven't changed to mirror new possibilities that women have in the workplace. 

Saying that women are free to do anything while ignoring the enormous challenges they face creates unrealistic expectations. It also creates a culture of shame. Many women raised in this era have internalized the narrative that their lives are full of endless possibilities. They blame themselves when they aren't able to achieve everything they'd hoped for, much less live up to the Enjoli perfume role model they grew up with.

### 3. Gen X women face both gender and age discrimination in the workplace. 

If Facebook COO Sheryl Sandberg is to be believed, every woman can be successful in the workplace if they just lean in __ and demand that they're fairly compensated and promoted. So why do so many Gen X women find themselves in precarious or low-paying jobs in their middle age? 

Women who were raised believing that the CEO's office was theirs for the taking have been very disappointed to face enormous amounts of gender discrimination. This is despite all the hard-won feminist gains of the Boomer generation. In fact, by mid-career, men are 70% more likely than women to have senior executive positions. By late career that difference widens to an enormous 142%. 

Even when women do have the same job title, they often earn a lot less than their male counterparts. While the pay gap between men and women seems to be decreasing, with women earning 82 cents to every male dollar in 2017, the picture becomes much starker if you look at middle-aged women. By 40 years old, women only earn 73 cents to the male dollar. And if you take a look over the span of a 15-year career, taking into account periods where women are forced to take time off work, their pay drops to only 49 cents to the dollar.

Gen X women face a double whammy of not only gender, but also age discrimination. They're forced to compete with Millennials who are perceived by employers as being more savvy at jobs involving the internet and social media. An investigation by the _New York Times_ revealed that age discrimination is even built into how some Facebook job ads are distributed; telecommunications company Verizon, for example, recently specified that only 25-36 year olds should have their job ads showing up in their feeds.

The trials of the job market lead many Gen X women to turn to freelancing. This labor is often presented as empowering, a chance to "get paid doing what you love." But it comes with many stressors, like an inconsistent income, erratic work hours, and all the administration and hassle of running your own business.

Rampant discrimination has made it very difficult for Gen X women to get ahead. In spite of good degrees, perseverance, and every willingness to "lean in,"__ stable and well-paying jobs continue to elude them. As we'll see in the next blink, growing up in very precarious economic times has only made it harder.

### 4. The ambitions of Gen X women are thwarted by mountains of debt. 

The Boomer generation lived in a world of almost unbelievable financial prosperity and stability by today's standards. A middle-class family in America could live comfortably on one income, own their own house, and work at the same company for their whole lives.

That picture now seems as rosily idealistic as a fairytale. Gen X is considerably poorer than their parents, with no hope of making up lost ground. The 1987 stock market crash prompted a recession just as older Gen Xers were preparing to enter the workplace. In 1993, the hourly wage hit a new low, meaning that people started their careers at an economic disadvantage. 

Then, just as Gen X women were trying to gain ground in their careers, the dotcom bubble burst in 2001, prompting another stock market crash and recession. The Bureau of Labor Statistics reports that 2.7 million jobs were lost between 2001 and 2002 alone! 

One thing Gen X women _could_ get was a home loan, as mortgages were very easy to acquire in the early 2000s. While finally owning property seemed to them like a dream come true, the dream turned into a nightmare in 2007, with the subprime mortgage crisis and collapse of the housing bubble. The houses they had just bought lost up to 30% of their value almost overnight.

Unsurprisingly, today Gen X has a staggering amount of debt — 82 percent more than Boomers and $37,000 more than the national average. But this debt is not all due to the recessions. Gen Xers also carry crippling student loans, thanks to the ballooning cost of higher education, and are often in debt for costs related to healthcare. A lack of good jobs results in a lack of access to health insurance. That means that one health scare can plunge a family into financial crisis. 

While all Gen Xers face these circumstances, there is also a gendered dimension to debt: not only do men usually get the good jobs, women are usually also burdened with the lion's share of costs related to childcare, in cases of divorce, and caring for ailing parents.

Coping with this enormous debt means that women have often had to put career plans on hold, or abandon their creative ambitions to accept any job they can find that helps pay their bills.

### 5. Even though Gen X women are more present in the workplace, they now devote much more time to childcare. 

It's astonishing how much common wisdom about parenting can shift in just one generation. The author remembers long afternoons spent watching TV in an empty house while her parents were at work, and roaming the streets freely with her friends. Her parents fed and clothed her and gave her shelter, but not much else.

Today, parenting seems to have become so much more complicated. Middle-class parents are expected to actively stimulate their children's imaginations, engage them in creative play, make them home-cooked meals, and give them undivided attention. 

In an increasingly competitive education system, children's homework now has to be actively supervised to ensure they are successful and can submit competitive applications to universities.

And, amid concerns about safety, children are no longer allowed to freely roam the streets. Instead, they're shuttled from one organized sporting or cultural activity to another.

All of these activities are tremendously time-consuming, and the bulk of this labor is still undertaken by mothers — on top of longer hours at work. 

In fact, a Pew Research Center study shows that mothers now devote 25 hours per week to paid work, compared to their 1965 predecessors' 9 hours per week. They also do an average of 14 weekly hours of childcare, in comparison to the previous generation's 10. In contrast, men do an average of 8 hours of childcare every week. This is 3 times what their 1965 predecessors did, but still way below what women do.

But these figures don't even cover the work women do in the house that can't be quantified, which is called _invisible labor_. Usually, it's the women of the household who will remember to buy a birthday card for a grandparent, get a teacher's end-of-term present, schedule doctor's appointments, answer school e-mails, and remember to buy new socks. Essentially, women are tasked with mentally managing the household in addition to all of their work responsibilities. They carry an enormous, invisible _mental load_.

And Gen X womens' caretaking responsibilities don't end with their nuclear families. As we'll hear in the next blink, that's only the beginning.

### 6. Gen X women have to simultaneously care for young children and elderly parents – with little support. 

Imagine you're grappling with the enormous demands of caring for two small children at the same time as balancing a job and trying to keep the house in order. Just as your youngest child starts school and you're finding a bit more time for yourself, one of your parents has a health crisis. Suddenly, you need to start supporting them too. 

This is the experience of many Gen X women. As they're having children later, in their 30s and even 40s, they often find themselves dealing with the enormous demands of childcare at the same time as their parents are growing frail and coming to depend on them. As with childcare, women are lumped with most of the work of taking care of elderly parents. Those parents are often divorced and don't have support from one another, exacerbating the burden. 

This enormous responsibility of being the main carer for young children and parents simultaneously is only made harder by the fact that support networks have dwindled. Today, it's become more unusual for neighbors and childless friends to help with childcare. And because the Boomer generation had fewer children, Gen Xers in turn often have fewer siblings to help lighten the load.

While religious communities used to be a mainstay of support in troubling times, as the Gen X generation has become less religious, they are often less involved in those communities and all the resources that they used to provide.

If you live in the United States, you won't be able to depend on the government for much help either. There is a law that employees can take up to twelve weeks off work for maternity leave or other family concerns, but those weeks are unpaid, and the law only covers 60% of American workers anyway. The rest falls outside of it, and can be summarily fired for taking time off for family or health reasons.

It doesn't take a lot of imagination to understand that these caregiving demands, coupled with a lack of societal support, create enormous amounts of stress for the women of Gen X, the "sandwich generation," who are stretched extremely thin.

### 7. Gen X women can struggle to find that perfect partner or have children when they’re ready. 

All this talk about the hardship of being a mother might make you think about ditching the whole child-rearing thing all together. 

Some Gen X women have indeed made the choice not to have children, instead prioritizing other relationships, work, travel, and all the things that parents don't have much time for. This movement is called Childfree by Choice. But it is very different to being someone who wanted to have children but wasn't able to.

Unfortunately, some Gen X women fit into this latter camp of women. Why?

Sometimes it's because they couldn't find a partner they wanted to start a family with. While the romantic comedies we grew up with gave us the impression that love is always just around the corner, it can be very elusive indeed.

In fact, if you're a heterosexual woman looking for a man in New York City, you're already at a disadvantage, as there are 400,000 more women than men. You'd think that modern dating apps like Tinder would help, but some believe that technology makes things harder, facilitating a culture where people are constantly swiping and looking for something better.

Some women have finally found the perfect partner or decided to try and raise a child alone, but find they aren't able to conceive or carry a baby to term. This is partly because women are now trying to have children later than any previous generation. This is enabled in part by technologies like IVF, pioneered in 1978, or egg freezing, which first led to a successful birth in 1999. While these technologies have extended fertility for some women, they don't always work. In 2016, only 22% of IVF procedures were successful.

The pressure to navigate the dating world and make a decision on when to start trying to have a baby can be incredibly stressful for Gen X women. On paper, we have so many choices. But in practice, there are unassailable stumbling blocks, like never meeting the co-parent of your dreams or finding out that you're not able to conceive a child.

### 8. Hormonal changes can have an enormous influence on women in middle age, but medical support is lacking. 

As recently as 1970, a member of the Democratic Party's Committee on National Priorities argued that fighting for gender equality wasn't a priority. His reason? Women's hormones were so dangerous to their decision-making abilities they could never be entrusted with high-ranking positions. He argued that women couldn't be pilots or presidents or CEOs because their hormones would "endanger the world." 

In response to blatant sexism like this, feminists fighting for equality asserted that women could function exactly as well as men can. That, of course, is true — women can be just as effective at any job. 

But cis women _do_ differ physiologically from cis men. We have unique hormones, which affect our lives in specific ways. Being forced to deny that for so long in the fight for equality has had a negative effect on middle-aged women's health and well-being.

Everyone acknowledges that teenagers go through an enormous physiological adjustment during puberty. Perimenopause is a similarly enormous hormonal upheaval that affects women in middle age. While most women go through menopause at age 51, the hormonal shifts start much earlier — around ten years before for African-American women and seven years before for white women.

So already in their forties women can experience debilitating hot flashes, as well as sleeplessness, painful sex, a low libido, breast pain, and decreased appetite. They can also experience disorienting mood swings and intense feelings of anxiety and rage.

There are effective treatments, like hormone replacement therapy, which can drastically reduce these symptoms. But many women don't even know what's going on with them, never mind how to treat it. In fact, 42% of women have never even discussed menopause with their health providers. And more often than not, doctors themselves are none the wiser: according to a 2013 survey at Johns Hopkins, only one in five obstetrics and gynecology residents are given formal training about menopause.

Gen X women, as we've seen, are already under an enormous amount of stress. And going through the profound physiological transformations caused by menopause without good medical or social support only makes it worse.

### 9. Come to peace with your midlife crisis by getting real. 

We've all become accustomed to broadcasting airbrushed images of our best selves on social media. Seeing these can lead us to assume that other people lead much better, more financially solvent, or more interesting lives than we do.

But the truth is, the vast majority of middle-aged women are under an enormous amount of stress. Acknowledging that, to ourselves and to each other, can break the isolation of the crisis.

Realizing that many of the expectations you grew up with are unrealistic will help even more. Gen X women were raised to believe they could do and have everything. But there are substantial impediments to a successful career, savings in the bank, and an easy home life that are completely out of our control. 

Admitting that might sound defeatist, but in fact, it can be very empowering. It means that everything we have achieved has been won against enormous odds. And it also means that we're not crazy for feeling stressed. Our lives can be really hard. Feeling tense, anxious, or depressed in response is a very appropriate reaction. 

Seeking support is the next important step. This can be from friends or like-minded women who are in the same boat; a good therapist; or a doctor who actually _does_ know about how to support perimenopausal women. It can take the form of a dog-walker, an accountant, or a family member who doesn't mind doing some babysitting. Asking for help can be hard when you've been raised to try and be completely self-sufficient. But it's so important. 

It may feel like this phase of your life will carry on forever — that you'll always be stretched thin between all your responsibilities, that you'll always feel so wretched about your career. But the beauty of a phase is that it ends. You won't be middle-aged forever. Your kids will grow up, your hormones will calm down, and you'll get to experience a phase of life that many women find much more fulfilling and interesting. 

So beyond adjusting your expectations and asking for help, one of the best ways to get through a midlife crisis is simply to wait and remember that it will pass.

### 10. Final summary 

The key message in these blinks:

**Gen X women grew up being told that the world was their oyster and they could fulfill their feminist forebears' every fantasy: a fantastic job, an equal division of domestic labor, and a chance to have an impact on the world. But the reality is quite different. Gen X women have grown up in precarious financial times, with crippling debt and the lion's share of caregiving responsibilities. This creates a pressure cooker of stress and unfulfilled expectations, leading to a midlife crisis. But help is available, and ultimately the crisis will pass.**

Actionable advice: ****

**Create your own society or club.**

One of the best ways to connect with like-minded people — whatever your current life situation — is to set up a monthly meeting around a shared interest. For example, you could have a margarita-drinking club, a writers' society, or a monthly seedling exchange. Having that regular contact will be a great way to get support and break your sense of isolation. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts

**What to read next:** ** _The Defining Decade_** **by Meg Jay**

**Reason:** By now you understand the issues that women are wrangling with in their forties, and how to deal with them. But what about the decades that preceded middle age? How do the decisions women make in their twenties contribute to the way their lives turn out?

In _The Defining Decade_, clinical psychologist Meg Jay argues that the first decade of adulthood is vital for setting the course of our lives, from building our careers to learning how to have good relationships. While Ada Calhoun believes that 40-somethings expect too much of themselves, Jay believes that 20-somethings expect too little. They treat their twenties as a continued adolescence, when actually it's the time to establish your adult life. To learn more about the excitement and tribulations of being in your twenties, read our blinks to _The Defining Decade_.
---

### Ada Calhoun

Ada Calhoun is the _New York Times_ bestselling author of _Wedding Toasts I'll Never Give_ and _St Marks is Dead_. Calhoun's national news reporting has won multiple awards, and she is a regular contributor to prestigious publications like the _New Yorker_ and the _Atlantic_. She also teaches journalism and creative writing.

