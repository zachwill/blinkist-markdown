---
id: 52cab29c6435330012000000
slug: why-we-get-fat-en
published_date: 2014-01-08T19:25:51.126+00:00
author: Gary Taubes
title: Why We Get Fat
subtitle: And What to Do About It
main_color: EE3733
text_color: D4312D
---

# Why We Get Fat

_And What to Do About It_

**Gary Taubes**

_Why We Get Fat_ (2010) explains why certain types of carbohydrates are the main reason we get fat. The book not only shows why people gain weight, but why the topic is so controversial. It also talks about why some people get fat and others do not, the role genetic predispositions play in this process, and which foods we should all avoid.

---
### 1. People who consume more calories than they burn will get fat: a common misconception. 

The first three blinks deal with the flawed logic that dominates our current view of the causes of obesity.

The vast majority of nutrition experts believe that there is a simple formula to explain why people get overweight: if you consume more calories than you burn, you will get fat.

They explain that the reason so many people are overweight today is because they consume too many unhealthy, high-calorie foods while using hardly any energy, for example because we spend too much time sitting in the car, at a desk, or in front of the television set.

Based on this theory, one might conclude that fat people are simply lazy and gluttonous, and that our body is like a balloon that stretches when we put energy into it and shrinks when we take energy out.

However, this oversimplification doesn't do justice to the complex processes that take place within the human body — and it fails to address why, precisely, some people consume more calories than they actually need.

Also, many cases have shown that even impoverished and underfed people can get fat. For example, around the turn of the 20th century, the indigenous peoples of America lived in abject poverty and were forced to subsist on sparse amounts of food. While their children showed symptoms of deficiency, many mothers were extremely overweight — and it certainly was not because they ate more and starved their children.

Are we caught up in a dogma that sounds convincing but does not actually hold water?

At any rate, the prevailing explanatory models have not yet been able to stop the obesity epidemic or explain why, among people who share similar lifestyle, some are fat and others not at all.

In light of these contradictions, anybody who has seriously grappled with the question of how to stop being overweight should also question the established views on the subject.

**People who consume more calories than they burn will get fat: a common misconception.**

### 2. The usual explanation for being overweight is based on the flawed logic that fat makes us fat. 

We knew enough to be able to stop the spread of obesity as early as the 1960s, but health experts decided to ignore the available findings.

Until the 1950s, hardly a doubt existed that being overweight was caused by a hormonal imbalance. But after World War II, this idea gradually disappeared, only to be replaced by the notion that being overweight was caused by an eating disorder. What was it that brought about this change?

The paradigmatic shift was largely due to the publicity surrounding the drastic rise in the number of cases of heart disease. In the 1970s, this problem dominated the public health interest and in turn the economic and political scene.

Obesity, clogged arteries and heart diseases could at last be simply and plausibly explained by the intake of fatty foods. Nobody doubted the main (albeit false) logic that fat first makes us fat — and then makes us sick.

Doctors and public-health authorities vehemently warned people of the consequences of a fatty diet. This not only became a fixed thought in people's heads but also influenced an entire generation of medical students.

The fact that today's medical opinion is dominated by a flawed explanatory model does not necessarily mean that doctors and nutrition experts act carelessly. Rather, they are caught in a paradigm so convincing and appealing that it is difficult to question, let alone to dismiss as false. It is not easy to uproot a firm belief.

And so science spread the doctrine that if we want to lose weight, we should eat less fat. But people did not lose weight. On the contrary, over the years they have gotten fatter and cases of heart disease have been on the rise.

**The usual explanation for being overweight is based on the flawed logic that fat makes us fat.**

### 3. Exercising more and eating less will not necessarily prevent us from being overweight. 

Unhealthy eating habits and a lack of exercise are the two usual suspects when it comes to laying the blame on what makes us fat.

But what is it that turns us into couch potatoes? Have we really fallen under the spell of potato chips? Definitely not. We become lazy _because_ we don't feel like being active. And we must treat this lethargy just as we treat an excessive appetite — as a symptom of a hormonal imbalance.

Children experiencing growth spurts are serious couch potatoes — not because they are lazy, but because they are using a ton of energy to grow. They don't want to participate in other activities because they simply don't have enough energy left for them.

The same thing happens when people get fat. Their excess fatty tissue, screaming for more energy, makes them hungrier, and the growing fat cells make them sluggish. Accusing them of being lazy and weak willed is unfair. In fact, it is physically much more difficult for fat people to overcome their weight issue.

By the same token, the claim that to lose weight we should exercise more while consuming fewer calories is problematic, because the more we exercise, the more energy our bodies need to function, which also boosts our appetite.

Moreover, there are countless cases that contradict the idea that anyone can lose weight simply by exerting more physical energy and eating less. A 1983 study found that, among a group of Mexican immigrants in southern Texas who were living in poverty and performing hard labor, about 40% were overweight despite their intense physical exertion. If exercise and low caloric intake make people thin, why was obesity so common among these people?

It is neither laziness nor overeating that makes us fat: it is _what_ we eat.

**Exercising more and eating less will not necessarily prevent us from being overweight.**

In the next six blinks, you'll learn exactly how and why carbs make us fat.

### 4. Insulin makes us fat, and carbohydrates control insulin production. 

Simply put, the hormone known as insulin is our body's reaction to carbohydrates.

It plays an important role in our metabolism because it is responsible for the recycling of carbohydrates, fats and proteins — i.e. the nutrients that provide our bodies with energy.

Insulin makes it easier for energy to be transferred into the muscle tissue, where it is burned. At the same time, it stimulates our fatty tissue to store superfluous energy.

When we eat a meal, our bodies immediately want to burn the energy from carbohydrates in our muscle tissue. Because as soon as carbohydrates reach our bloodstream in the form of glucose, our blood sugar level skyrockets. High blood sugar levels are like poison for many cells, which is why our bodies need to keep our blood sugar levels in check. For a start, this entails burning the carbohydrates that can be easily turned into energy to more quickly provide us with fuel, and this process begins when insulin floods the body. The amount of insulin distributed depends on the amount of carbohydrates that have been absorbed.

Excess energy, whether in the form of carbohydrates, proteins or fats, is saved for potential "tough times" to come, when our bodies may have a lower external energy supply.

Food rich in carbohydrates, like sweets, bread, potatoes, noodles and rice, flood our bloodstream with sugar, forcing our body to distribute insulin. But the higher the insulin level, the more strongly the body is stimulated to form fatty deposits.

Therefore, carbohydrates have the biggest influence on the mechanism that regulates our accumulation of fat. In the end, they are what make us fat.

**Insulin makes us fat, and carbohydrates control insulin production.**

### 5. Being overweight is not the consequence but the cause of a nutritional disorder. 

When tissues expand, whether it is a tumor forming or a child growing taller, we call this "growth," and we know the changes are based on complex processes controlled by hormones.

Nobody would ever dream of saying that a child had grown because he or she ate too much. Children grow constantly and need energy to do that. They do not get bigger because they eat, but because their hormonally regulated growth requires a lot of energy and makes them hungry.

The rampant growth of a tumor is similar: it does not grow because it is being fed energy, but because of problems within the cells, which cause its growth.

So why should we think differently about being overweight and isolate it from other growth processes?

It is scientifically indisputable that the hormone insulin has a substantial influence on the formation of fat deposits, and yet when we talk about being overweight we tend to confuse cause and effect.

Just as in the case of a tumor, the growth of fat cells can be impaired because our bodies respond to a high level of blood sugar by producing excessive insulin.

When we grow fatter, our cells are using up energy and so we feel hungrier and more lethargic — just like a child going through a growth spurt. Growing fat cells consume more energy; therefore, fat people are hungrier.

When we recognize that being overweight is the consequence of a biological growth process, it becomes clear that gluttony or laziness cannot be the reasons why we get fat; instead, we should try to understand them as symptoms of a growth disorder.

**Being overweight is not the consequence but the cause of a nutritional disorder.**

### 6. Our bodies cannot process the quantity or type of carbohydrates that have become ubiquitous. 

Human beings are creatures of habit — especially when it comes to food. The longer certain nutrients have been a part of our diet over the course of evolution, the better our bodies have adapted to them and the healthier they are for us.

Carbohydrates, which make up a significant part of our diet today, are a relatively new addition to the human diet. Human beings have been inhabiting Earth for about 2.5 million years, but people only began eating flour around 12,000 years ago with the spread of agriculture, and the Western world did not even know what potatoes were before the discovery of America. White flour and white sugar only became a significant part of the human diet at the end of the 19th century.

The era of carbohydrate-rich food is but a split second in the history of humanity; therefore, we can rule out the possibility that we formed a genetic adaptation to these quickly digestible carbohydrates in such a short period of time.

Before the spread of agriculture, our ancestors lived as hunters and gatherers, preferring fatty meats to quell their hunger.

A recently published study analyzed the nutrition of present-day hunting and gathering cultures that had been completely isolated from the civilization of the 20th century. Their diet also consisted largely of fish and meat. Some of them even survived without eating any vegetables, fruits or grains at all. In other words, their diets were the polar opposite of our diet, of which carbohydrates make up two-thirds.

When health experts blame our Western lifestyle for the spread of obesity, they disregard the fact that until very recently carbohydrate-heavy foods had hardly played a role, if any, in people's lives.

**Our bodies cannot process the quantity or type of carbohydrates that have become ubiquitous.**

### 7. Carbohydrates not only make us fat: they also make us sick. 

If we regularly expose our bodies to large amounts of carbohydrates over a long period of time, we may disturb the signaling function of insulin.

What we eat greatly influences the incredibly sensitive mechanism that controls our metabolism. By eating foods rich in carbohydrates, which make up a very large part of our daily caloric intake, this mechanism can be thrown out of balance and result in an oversupply of insulin.

Since insulin largely regulates the formation of fat deposits, we not only get fat, we also become more susceptible to diseases like diabetes or high blood pressure, which frequently occur in conjunction with obesity.

This is why these diseases suddenly break out in almost every culture when it first comes into contact with Western foods.

This phenomenon can also be seen in its effect on immigrants. Breast cancer, for instance, is not very common among Japanese women. However, the breast cancer rates of the descendants of Japanese women who immigrated to the US perfectly mirror those of other women in the US.

A similar increase in the probability of contracting these diseases is always seen among immigrant groups, completely independent of their ethnicity and the disease in question.

However, demonizing our entire Western lifestyle is not the answer; instead we must try to target specific possible causes. And when we do, we quickly arrive at carbohydrates.

One clear indicator is the fact that our ancestors, like today's isolated indigenous peoples, rarely ate carbohydrates — and neither group has suffered from diabetes or similar "Western diseases." From this, we can conclude that it is very likely indeed that carbohydrates are causing the problem.

**Carbohydrates not only make us fat: they also make us sick.**

### 8. Low-calorie diets almost never work – and can even damage our health. 

Diets aimed at lowering our daily caloric intake, which are a dime a dozen, can help us get thinner in the short term, but there is always a catch in the long term: namely, we have to continue putting in an effort to maintain our lower weight.

For most of us, this path is doomed to failure. Diets limiting our caloric intake deprive us of the energy and nutrients that keep our bodies fit and help them regenerate. Such diets literally starve our bodies.

Let's say that John Doe decreases his caloric intake by 20% simply by eating 20% less of everything. In doing so, he would not only be going without 20% of the calories but also without 20% of the vitamins and minerals that come with the food.

As soon as John Doe returned to his old eating habits, his body would simply regain the mass it lost. In other words, he would experience the so-called yo-yo effect.

An experiment carried out in the US in the early 1990s proves just how counterproductive this starvation program is. Over the course of eight years, all 20,000 female participants ate 360 calories less than they had been eating every day. The foods they ate contained everything that was considered healthy and necessary: fiber, fruits and vegetables, and not much fat.

In the end, everyone was disappointed to realize that the women had only lost an average of two pounds. Most of them even put on weight in their bellies — they had lost not fat but muscle mass.

All overweight people have probably already tried simply eating less — and most of them end up staying fat. If that does not raise serious doubts about the starvation method's chances of success, what does?

**Low-calorie diets almost never work — and can even damage our health.**

### 9. Genetic disposition, age and nutrition all influence our likelihood of being overweight. 

Since carbohydrates stimulate insulin production, and insulin has a significant role in the growth of fat deposits, we can indirectly combat fat storage by consuming fewer carbohydrates. But it is also important to factor in how sensitive we are to carbohydrates and insulin.

Our genes decide first of all whether we put on weight and, if so, when and where we put it on. They not only determine how effectively our muscles burn energy but also how quickly we save energy for emergencies.

Some people are simply more prone to fat rolls. While some people seem to gain weight just by thinking about sweets, others can always take a second slice of the cream pie guilt-free.

Another factor is that the fine-tuning of our bodies changes over the course of our lives. The longer, more regularly and intensely we generate high levels of insulin from carbohydrate-rich foods in our bloodstream, the more resistant our musculature becomes towards insulin, which inhibits our bodies from burning energy. In turn, the body reacts to this resistance by raising the insulin dosage necessary to lower our blood sugar. This strengthens the resistance and advances the growth of fat cells — a catch-22 situation.

Such resistances appear first in muscle cells and only later, if at all, in fat tissues. Insulin resistance usually increases with age, which is why older people are generally more prone to being overweight, even if they never had weight issues in the past.

It is surely no secret that we tend to get fatter as we age. However, popular wisdom describes the catalyst as a slowing down of our metabolism.

When we stuff ourselves with high doses of carbohydrates throughout our lives, we reinforce this process because we boost our insulin resistance.

**Genetic disposition, age and nutrition all influence our likelihood of being overweight.**

In the last two blinks, you'll discover what you can do to get a handle on your weight.

### 10. Low-carbohydrate diets get a bad rap – and wrongfully so. 

Doctors and nutritionists generally advise against a diet without carbohydrates. This recommendation is based on three basic assumptions: first, that it is impossible to lose weight without burning more energy than you take in; second, that carbohydrates are modifiable for a healthy diet; and, third, that when you replace carbohydrates with fats, you get fat and thus become more susceptible to heart diseases.

However, these assumptions are flawed. These experts disregard the fact that meat is rich in essential vitamins. Furthermore, you can replace carbohydrates with vegetables, which, unlike sugar, also contain a lot of vitamins and minerals. While an unlimited consumption of meat, eggs and green vegetables secures a good nutrient supply, a reduced-calorie diet forces us to forego important nutrients.

Similarly, it is often ignored that diabetes and obesity were completely unknown to peoples who were not influenced by Western culture — and who nourished themselves almost entirely on fatty meats and fish. We must therefore discard a diet rich in fat as the cause of these diseases. Remarkably, the first time these peoples experienced being overweight was when flour and sugar were first introduced into their diets.

Since carbohydrates are what make us fat and not fat itself, then it is not the culprit in heart disease risk, either. If it were, then we'd have a paradox where the risk of disease got higher as we lost weight — obviously an inverted relationship.

Finally, the simple reason for the widespread skepticism of a low-carbohydrate diet is the same one that excludes carbohydrates as a key cause for being overweight in the first place: the firm belief that foods high in fat are the root of all evil.

**Low-carbohydrate diets get a bad rap — and wrongfully so.**

### 11. In order to lose weight and keep it off, or to avoid getting fat in the first place, we have to give up carbs. 

The short answer to the question about what we can do against getting fat is this: keep away from foods rich in carbohydrates!

In the long term, diets aimed at losing weight are only successful when they constantly stop fat deposits from forming and stimulate fat tissue to release excess energy.

Since this can only happen when we have moderate insulin levels, and since carbs fuel insulin production, we have to reduce our carbohydrate intake.

However, different types of carbohydrates influence our blood sugar and insulin levels in very different ways. Simply put, we can safely assume that the sweeter the temptation, the likelier it is that it will make us fat.

Some carbohydrates make it into our bloodstream very quickly: sweets, for instance, literally flood our bloodstream with glucose, which causes our blood sugar levels, and then our insulin levels to skyrocket.

But cereal products, starchy foods, and alcoholic and sugar-containing drinks are a highly concentrated sources of energy, too.

By contrast, the vast majority of carbohydrates contained in vegetables are linked to indigestible fibers, whereby they have a smaller impact on blood sugar and the release of insulin.

If we get rid of the _quick_ carbohydrates in our everyday diet, we are not only preventing the formation of fat deposits; we are simultaneously forcing the release of fat that has already been stored and transforming it into energy.

Of course, radical abstinence from carbohydrates is not necessary for everyone because not everyone is that sensitive to carbohydrates. But for anybody who is overweight or gains weight easily, it is the easiest way to shed pounds.

**In order to lose weight and keep it off, or to avoid getting fat in the first place, we have to give up carbs.**

### 12. Final summary 

The key message of this book is:

**In order to understand why we get fat, we have to free ourselves from the widespread understanding that our weight is determined by our daily caloric intake and energy consumption. Instead, we should be thinking about the amount of carbohydrates we consume and the way our bodies handle them.**

The questions this book answered:

**What sort of flawed logic dominates our view of being overweight?**

  * People who consume more calories than they burn will get fat: a common misconception.

  * The usual explanation for being overweight is based on the flawed logic that fat makes us fat.

  * Exercising more and eating less will not necessarily prevent us from being overweight.

**Why and how do carbs make us fat?**

  * Insulin makes us fat, and carbohydrates control insulin production.

  * Being overweight is not the consequence but the cause of a nutritional disorder.

  * Our bodies cannot process the quantity or type of carbohydrates that have become ubiquitous today.

  * Carbohydrates not only make us fat: they also make us sick.

  * Low-calorie diets almost never work — and can even damage our health.

  * Genetic disposition, age and nutrition all influence our likelihood of being overweight.

**How can we get a handle on our weight?**

  * Low-carbohydrate diets get a bad rap — and wrongfully so.

  * In order to lose weight and keep it off, or to avoid getting fat in the first place, we have to give up carbs.

**Suggested further reading: _Eat_ _to_ _Live_ by Joel Fuhrman**

_Eat_ _to_ _Live_ gives readers a comprehensive overview of human nutrition, a re-evaluation of conventional nutritional wisdom, personal case studies and a practical dietary program with lots of recommendations. The reader can expect to learn about a number of different nutritional studies as well as the health benefits and repercussions of basic foods such as meat, milk, fish, vegetables and fruits.
---

### Gary Taubes

Gary Taubes (b. 1956) is an American science journalist. In his books, he discusses scientific controversies and offers his readers clear insights into complex subject areas. Most recently he has attracted attention for his critical view of the nutrition science establishment.

