---
id: 5344475f6432320007000000
slug: why-we-make-mistakes-en
published_date: 2014-04-08T09:50:10.000+00:00
author: Joseph T. Hallinan
title: Why We Make Mistakes
subtitle: How We Look Without Seeing, Forget Things in Seconds, and Are All Pretty Sure We Are Way Above Average
main_color: F68231
text_color: A85922
---

# Why We Make Mistakes

_How We Look Without Seeing, Forget Things in Seconds, and Are All Pretty Sure We Are Way Above Average_

**Joseph T. Hallinan**

_Why We Make Mistakes_ is about the kinds of mistakes we commonly make, and the reasons behind them. With a broad focus encompassing neuroscience, psychology and economics, the book provides convincing explanations for our often fallible perception, our inability to recall simple data and the many biases that direct our decision making without us being aware.

---
### 1. What’s in it for me? Learn how to recognize your mistakes and avoid making them in the future. 

Have you ever found yourself struggling to remember your own phone number? Or made a snap judgment about someone based entirely on the color of their shirt?

It's humbling to realize how susceptible we are to error. 

While we're prone to seeing our mistakes as individual exceptions to the rule, our tendency to make mistakes is actually something we're born with — a result of the way that we evolved. 

So, instead of slapping our foreheads and blaming our personal shortcomings, we're better off taking a closer look at the mechanisms that lead us to blunder and reckon with them!

This is exactly what Pulitzer Prize-winning author Joseph Hallinan does in his bestseller _Why_ _We_ _Make_ _Mistakes_. Brimming with valuable insights into why we make mistakes, the book is also a trove of good advice on dealing with and offsetting our susceptibility for error.

In these blinks, you'll find out why baggage screeners at airport security don't notice 25 percent of all firearms that are smuggled through, why one in four people can't recall their own phone number, why multitasking isn't the timesaver it's cracked up to be and, finally, why Burt Reynolds once punched a guy who had no legs.

### 2. Our view of the world is limited by our eyes and our minds. 

Before he became famous, Burt Reynolds entered a bar and noticed a broad-shouldered man harassing the patrons. Reynolds told the man to stop, but to no avail. Eventually Reynolds punched him, sending him flying through the air.

That's when Reynolds noticed that the man had no legs. How could he have not seen that?

The world is incredibly complex: we're constantly confronted with much more information than our brains can handle; plus, there are many different ways to look at every situation. 

Part of the problem is that our field of vision is literally limited to 180 degrees, so at any given moment we see only half of what there is to see.

But there's also a psychological aspect to the problem: a scene will appear differently to one person than it will to another.

For instance, when we observe an event, sometimes what we notice is determined by the person we identify with. When watching a male thief steal a woman's purse, for example, men tend to focus on the thief while, in general, women focus on the female victim.

Another way our view of the world is limited is our tendency to see only what we expect to see _where_ we expect to see it. Which means we often fail to notice many, sometimes key, details.

Even seasoned professionals are susceptible to this phenomenon. The "miss" rates are extremely high in professions that require people to look out for certain unusual objects — like tumors or bombs.

For example, the number of flight passengers who attempt to smuggle firearms through security is only one in a million. As a result, baggage screeners simply don't expect to find guns in passengers' luggage and their screening process becomes lax, causing them to miss a quarter of all firearms.

And _that's_ how it was possible for Burt Reynolds to miss the fact that the broad-shouldered bully had no legs: he simply couldn't see what was right in front of his eyes.

### 3. It’s much easier to remember meaningful information than random data. 

A recent survey revealed that a quarter of its respondents couldn't remember their own phone numbers.

Why is it so difficult for us to recall such information?

Because we're more adept at remembering meaningful information than recalling abstract data. For instance, we can usually only memorize up to seven random items at a time — for example, seven numbers in sequence. However, if we make the items meaningful for ourselves, we can memorize them flawlessly.

For example, try to memorize this random sequence of numbers:

1 4 9 2 1 7 7 6 1 9 4 5

Difficult? Try breaking the sequence up into three groups:

1492 — 1776 — 1945

You'll notice that these are important historical dates: Columbus' voyage to America, the Declaration of Independence and the end of World War II. Such meaningful associations will enable you to recall the superficially random sequence without error.

The same is true of any information you want to remember accurately. One study tested people's ability to memorize biographical information about fictional people, and found that the items that were recalled accurately were those the subjects found meaningful: for instance, they found it easier to recall the fictional person's job than their name.

Why is meaningful information so much easier to remember?

Because learning involves making connections in the brain, and, in comparison with "naked" data, e.g., a string of random numbers, meaningful information is processed in more areas of the brain.

For example, we find it easier to recall a person's occupation because, as we learn it, vivid associations leap to our minds. If you learn that someone is a baker, you effortlessly conjure images of, say, a rotund man dressed all in white, slicing a loaf of fresh bread. Or perhaps you recall a recent report about the number of local bakeries that went out of business in your area.

As the occupation "baker" triggers such associations, it connects to more parts of the brain, making it much easier to recall.

### 4. We make judgments quickly based on criteria we’re not even conscious of. 

Our ability to make snap judgments based on appearances has been shaped by our evolution. For instance, we might decide whether to eat a banana based only on its color: if it's yellow, we'll eat it, but if it's green or dark brown, we won't.

While this ability to make very quick judgments using only superficial information has helped us in our survival, unfortunately it also leads us to make mistakes.

Even apparently arbitrary factors, like color, can shape our judgments and expectations. Indeed, sports teams whose uniform is black are perceived as far more aggressive than those who wear other colors. For example, when two hockey teams — namely the Pittsburgh Penguins and the Vancouver Canucks — started wearing black uniforms, the referees began to treat them differently, giving them many more penalty minutes than before.

The human face is another powerful influence over our judgments. Indeed, we tend to make snap judgments of people based solely on subtle facial features. In a study in which people were asked to judge the political competence of candidates they were unfamiliar with, based only on a brief glance at black and white photographs of those nominees' faces, the participants made their judgments in one second.

Another study revealed that its participants perceived people with broad noses and prominent chins as more dominant and people with softer faces with high foreheads as more trustworthy. Of course, both of these traits can, for example, contribute to a person being elected for office.

Moreover, we're often completely unaware of how our behavior is determined by our perceptions. Take, for instance, our sense of smell. In one experiment set in a retail store, men spent an average of $55 when the store was scented with the masculine fragrance "rose maroc," but less than half this amount when the more feminine vanilla scent was used. Our behavior in such circumstances is often greatly influenced by such perceptual effects without our awareness.

### 5. We make mistakes because we systematically simplify what we learn about the world. 

When Parisians were asked to draw a map of their home town from memory, nearly every participant straightened the River Seine.

Why?

The density and complexity of information in maps make them hard to memorize accurately. To remedy this problem, and to help us to remember routes and locations, we simplify them in our minds by altering certain details — like straightening the twists and turns of rivers and streets, and rendering uneven and ragged shapes as symmetrical and smooth.

While simplifying maps is pragmatic, it actually results in the map becoming distorted in our memories. In one experiment, for example, students were shown two maps of an area they knew: a simplified but faulty map and the actual, accurate one. When they were subsequently instructed to select the map they believed to be accurate, most of the students chose the distorted map because it corresponded to their own mental map.

But maps aren't the only thing we simplify: whenever we recount a complex story that might have some contradictions in it, we tend to simplify it to make it more coherent.

For example, imagine a friend tells you a hilarious story about him falling into a river. He tells you that, while in the river, he lost the expensive pair of pants he'd bought while in Prague, but that he also found an oyster with a large pearl inside it. The punchline to the story is that he couldn't leave the water half-naked but also didn't want to appear as if he'd fallen in accidentally, so he pretended that he'd jumped into the river on purpose.

If you were to retell this story to others, it's likely you'll make a few changes: You'd probably cut out the details that seem unnecessary, like the cost and origin of your friend's pants. You'd probably also simplify the narrative, by removing information that doesn't seem to fit: perhaps you'd choose to leave out the bit about the oyster since finding an oyster in fresh water is very unlikely.

### 6. We make mistakes because we tend to see patterns instead of single bits of information. 

Have you ever wondered how interpreters are able to translate rapidfire conversations between people in a split second?

The answer is that interpreters don't wait until each sentence has been completed but instead recognize patterns the moment the first words are spoken. This enables them to complete any given phrase very quickly.

However, this ability is not limited to interpreters. When anyone is proficient in a particular skill, they process information in the exact same way: they look for cues to familiar patterns.

Such predictable patterns are found in many fields — such as music and languages — usually in the form of conventions, like rules and common phrases.

Once you acquire or learn these rules and phrases, you'll start to notice patterns rather than individual details.

For instance, when you read a text, you scan the beginnings of words and sentences rather than focusing on each letter of every word. That's because beginnings often contain cues to the patterns that follow. For example, the words "Once upon a time..." at the beginning of a story indicate that a fairy tale is about to be told.

Looking out for cues in this way can, of course, save a lot of time and effort. But there's also a downside: being reliant on cues often leads to mistakes.

For example, for decades, most editions of Brahms' sheet music contained one particular misprint. Over the years, no proficient musician ever played or even noticed the misprinted note — until one unskilled piano student played through the piece, note by note. Yet, even after the misprint was discovered, skilled pianists had trouble finding the mistake because they'd become so accustomed to looking for musical patterns and phrases rather than individual notes. 

So while looking for patterns rather than attending to details can be very efficient, it's also one of the reasons we make mistakes.

### 7. We tend to see our past thoughts and actions in a favorable light. 

Is there someone in your life who always insists they knew exactly how things would turn out?

The tendency of people to embellish and distort the past to show themselves in a more positive light is manifested in various erroneous ways.

One is that we systematically misremember our original predictions. After learning how an event actually turned out, we tend to convince ourselves that we knew it all along, unconsciously altering our original predictions to align with the actual outcome.

For instance, in the 1970s, a group of students were asked to predict the consequences of President Nixon's visits to the Soviet Union and China. Following Nixon's visits, the same students were periodically asked to recall their original predictions and whether they thought what they'd predicted had actually occurred.

The result?

The students misremembered their original predictions in a way that rendered them more accurate.

Exaggerating our past successes is another way in which we distort the past. In one study where students were asked to recall their high school grades, the researchers found that all of the students had greatly exaggerated their past accomplishments.

One reason we alter the facts of our history is because we judge our past in line with social conventions and expectations.

For example, when men and women were asked to estimate the number of sexual partners they'd had during their lives, they all reported figures appropriate to their gender stereotypes. In other words, while men reported having had many sexual partners, women reported having had just a few. The interviewees' responses were clearly aligned with those stereotypes: they didn't recall their actual past, but a largely exaggerated version of it.

It's important to point out, however, that their exaggerations probably weren't intentional, as they were asked other equally delicate, intimate questions which they answered truthfully.

### 8. We overestimate the benefits of multitasking. 

Whenever we have a lot of tasks to do in very little time, we often turn to multitasking. And why not? On paper, multitasking seems like the only way to get everything done in time.

But is it actually possible to perform different tasks at the same time?

Whenever we think we're multitasking, we're really just switching our attention from one task to another. Our brains simply don't allow us to make more than one conscious decision at a time.

The only way we're able to perform more than one complex task simultaneously is if just _one_ of them requires our conscious attention. Then again, there aren't many complex tasks we can perform automatically.

So we flit back and forth from one task to another, switching our attention between them: from using a smartphone, to reading an article, to planning the evening's dinner.

The main problem with attention switching is that it comes at a cost: the brain slows down when we attempt to perform many different tasks at once. That's because it not only has to deal with the tasks themselves, but must also prioritize and manage competing tasks and potential distractions.

Furthermore, every time we shift our focus from one task to another, the brain takes some time and effort to adjust. Think, for example, of coming back to a complex task — like doing your accounting — after checking your email.

But this kind of multitasking is not only taxing for the brain. It's also potentially dangerous.

Some tasks demand so much of our conscious attention — like maneuvering a car safely through heavy traffic — that trying to perform other tasks simultaneously uses precious mental resources. Even performing another, apparently simple task at the same time as driving — for instance, entering an address into your GPS system — can be very dangerous. Indeed, 78 percent of all car accidents are caused by the momentary inattention of the driver.

### 9. We all tend to think we’re better than we are, and men are especially prone to overconfidence. 

Imagine you're planning to join a gym, where you're presented with various membership options: the yearly contract, the monthly membership and the pay-as-you-go option. Which one would you choose?

If you're like most people, you'd choose one of the longer-term contracts — and end up overpaying. Like almost everyone else, you'd overestimate how disciplined you'd be about sticking to your exercise regimen.

That's because we tend to believe we're much better than the average Joe in any skill or field.

For instance, weight-loss diets are known for their low success rate, yet almost all dieters will tell you that, against all the odds, they're going to triumph.

This tendency has been observed in many different studies. In one study, subjects were asked to estimate their susceptibility to bias in comparison with the general public. As it turned out, most of the subjects believed that they were especially resistant to such prejudices.

Another experiment showed that, sometimes, people believe they can predict the outcome of a series of coin tosses. A few of them were told, falsely, that their first guesses were correct, and eventually these participants believed that they had the ability to accurately predict when the toss would result in heads or in tails.

In general, however, men are more likely to overestimate their abilities than women. They believe they're more intelligent and physically attractive than women do. They're also more prone to conveniently forget a setback or a mistake before it has a chance to damage their self-esteem.

And, when men have to take a test, they're much more self-assured that they'll perform well in comparison to women — despite the fact that female students generally get better grades.

### 10. Small tweaks in product design, professional practices and data presentation can help prevent mistakes. 

In several American hospitals, newborn babies had repeatedly been given near-fatal doses of a blood thinner.

How could this happen? The cause of the almost deadly mistake was simple: the vials containing the standard dose looked very similar to those containing a much higher concentration.

Such mistakes are obviously preventable: by incorporating cues or signposts into our surroundings. In fact, we do this already with certain products and artifacts to ensure they're used properly.

For instance, bottles containing cosmetics are designed to be easily distinguishable from beverage bottles so as to prevent children from accidentally swallowing, say, shampoo or perfume.

So if different medicines are colored differently and given easily distinguishable labels, it will prevent someone from administering the wrong drug. Also, if we mark the areas of the body that will be the focus of surgery in the operating room, this will keep doctors from making incisions in the wrong place — or even removing the wrong leg.

Another way to reduce the likelihood of mistakes is to limit the amount of information people have to process. Because being bombarded with information makes us susceptible to error. Sometimes, when we experience information overload, we stop distinguishing between relevant and irrelevant facts.

Furthermore, feeling totally overwhelmed by an onslaught of information causes us to seek a way out of the problem, which is clearly a bad starting point for making an urgent decision.

For example, when newcomers to the financial world are overwhelmed by the plethora of various investment strategies, they often tend to choose the easiest option and put their money in a deposit account, which is clearly not the most profitable option. Less complex information is advantageous to novices who want to make informed and wise decisions.

### 11. Taking into account the factors that make us fail can help us avoid failure. 

So, now that you're aware that many of the mistakes you make stem from common and predictable problems in your thinking rather than random, individual "glitches," you can use this information to help you avoid making mistakes.

How?

First, by keeping an eye out for those mundane details that tend to distort your judgment.

Sometimes it's crucial that your decision making is completely rational — like when you have to vote for the right political candidate. In such cases, it's important to look out for what might cause you to be biased.

For example, you might notice that you prefer a certain candidate because he looks like a Hollywood star, which you know is no indication of his political competence.

By noticing your bias, you might be spurred to research both candidates properly and examine their policies and ideologies. This would help you make the most informed and rational decision possible.

Second, you can avoid making mistakes by being aware that you have a tendency to overestimate your ability to predict future events. This knowledge can prompt you to find out exactly how accurate your predictions are (your success rate is probably far worse than you initially thought).

An easy way of getting such feedback is to make a note of your predictions (e.g., when you choose which stocks to invest in) and to note the factors these predictions are based on. Later, when you compare your predictions to the actual outcome, you will obtain feedback that can improve your forecasts in the long run.

And, of course, learning about the inaccuracy of your predictions may help you prevent making risky decisions prematurely.

Thus, being aware of our tendency to make impulsive decisions and knowing the factors that can distort our judgment enable us to notice the situations in which we typically make mistakes.

### 12. Final Summary 

The key message in this book:

**Many of the mistakes we make don't spring from individual malfunction, but are directly related to the way we human beings think and perceive the world. By learning to recognize our own susceptibility to error, you can offset it and avoid making mistakes in the future.**

Actionable advice:

**Make a checklist.**

The next time you're planning a big trip, make a checklist. We all tend to be overconfident about our memories but sometimes just writing down all the vital points can ensure that you don't reach the border without your passport.

**Limit your research.**

The next time you find yourself over-researching, remind yourself that too much information may keep you from making good decision. When your brain is flooded with data, it slows down and stops differentiating between important and trivial facts.
---

### Joseph T. Hallinan

Joseph Hallinan is a journalist and writer who wrote for the _Wall Street Journal_ and won a Pulitzer Prize for his investigative reporting. Besides _Why We Make Mistakes_, Hallinan has also written the award-winning _Going Up the River: Travels in Prison Nation_ and, most recently, _Kidding Ourselves: The Hidden Powers of Self-Deception._

