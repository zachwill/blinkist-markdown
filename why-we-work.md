---
id: 5694b1d72f6827000700001e
slug: why-we-work-en
published_date: 2016-01-14T00:00:00.000+00:00
author: Barry Schwartz
title: Why We Work
subtitle: None
main_color: 228BAA
text_color: 156178
---

# Why We Work

_None_

**Barry Schwartz**

_Why We Work_ (2015) exposes the flawed assumptions that govern the modern working world. These blinks walk you through the reasons why current management strategies backfire, and show you some far more effective alternatives. In addition, case studies based on company success stories illustrate just how powerful engaged and fulfilled employees can be.

---
### 1. What’s in it for me? Get key insights into what motivates people to do great work. 

Why do people work? It could simply be to earn money to pay bills and buy groceries, it might be a way to maintain social interactions or perhaps it's something that we feel deeply engaged in. Well, chances are it's a mix of all these factors and more — so how do you create a workplace that fulfills all of these needs?

Work today has become badly imbalanced, tilted toward money as the main, and sometimes only, incentive to perform. For some, money might indeed be the most important incentive, but for others it can actually be demotivating; the end result is that we become less effective and less creative. 

Read on to take a closer look at the psychology of why we work and the ways we can make our everyday employment feel more important and fulfilling. 

In these blinks, you'll find out

  * how monetary incentives for parents to pick up their kids early had the opposite effect;

  * how the dangers of vicious work cycles are best avoided; and

  * why Adam Smith's ideas about incentivizing efficiency are wrong.

### 2. The most fulfilling work allows us to make a difference in the lives of others. 

Do you know what your work means to you? Whether you know exactly how and why your work matters, or have never really thought about it, your answer is a key indicator of your experience of work. 

People tend to see work from three different standpoints. For some of us, work might be nothing more than a _job_. We do it to pay the bills, buy groceries and have some spending money. Work is necessary, but going beyond a job's required tasks is pushing it. 

Others perceive their work as a _career_ ; in other words, a period that is hopefully characterized by progress and growth. Career-minded people work for promotions, raises and increased responsibilities. They certainly expect more fulfillment from their positions than those for whom work is merely a job. 

Still others believe their work to be a _calling_. For these people, the work they do creates positive change in the world. Their work becomes not only a vital part of life, but a source of happiness too. 

You probably know people that fit into each of these categories. Perhaps you've found yourself in all three of them at different stages in your life. But why do these differing attitudes exist in the first place?

Well, that's a question that one American psychologist was determined to answer. Amy Wrzesniewski and her team conducted interviews and studies with individuals across different fields and industries in order to find out what made their work a calling and not just a job. 

Hospital custodians cited interactions with patients as the most fulfilling aspect of their work, while administrative assistants said they felt most satisfied when supporting their faculty in providing education to tomorrow's leaders. In both cases, employees felt their work was a calling when witnessing the positive changes they made in the lives of others. 

How can you make your employees feel like their work is a calling? Find out in the following blinks.

> _"The difference between the good and the bad had less to do with actual duties than it did with the context in which my duties were embedded."_

### 3. Without autonomy, investment and a mission, employees feel disengaged and demotivated. 

What makes a business run well? Today, there are countless formulas, strategies and mantras out there, all designed to keep companies running effectively. In reality, it's quite simple: there are three factors that make a company come out on top, and they're all tied to how employees are treated. 

The first of these is _autonomy._ When given more independence and responsibility, everyone from entry-level workers to leaders in project teams will take more pride in their work. Granting employees the power to make key decisions in their field boosts levels of trust, commitment and respect within the workplace.

The next factor is _investment_. Some of the most successful companies today have reached great heights by dedicating time, money and effort into the skill development of their employees. Through challenging training programs, employees feel that their time at work is valuable, and that their roles themselves are also valuable. 

The third crucial factor is _mission_. A company's mission isn't just something that should halfheartedly come up in the CEO's speeches. Instead, it should be continually present in the everyday affairs of the workplace, across each level of management, within every team endeavour and, of course, in the finished products. 

During periods of poor performance, companies tend to reduce levels of autonomy, investment and mission to compensate. This, however, should be avoided, as it only serves to exacerbate problems by creating _vicious work cycles._

Take, for example, a company facing pressure from a rival in the market. They may axe jobs or introduce strict monitoring of employee performance in an effort to lift performance. Unfortunately, the opposite will occur:

Reduced job security and responsibility will make employees more demotivated than ever, leaving the company in a less competitive position. Management then responds by introducing even tighter control, employee morale shrinks again and the cycle continues. 

Take a closer look at exactly why strict management leaves employees demotivated in the next blink.

### 4. Attempts to boost productivity through overstructuring and financial incentives will backfire. 

Have you ever worked a job where it felt like your supervisor was breathing down your neck? Though some of us claim to work well under pressure, this is usually not the case. Treating employees as if they're incapable of performing well alone simply won't motivate them to perform at all. 

Overstructuring is known to decrease engagement and productivity. Even so, it's still a key characteristic of many professions and institutions today. In fact, one of the worst perpetrators of perennial overstructuring is the US education system. 

Teachers in the United States are provided with incredibly detailed curricula, designed so that even bad teachers are able to provide students with an adequate education. With no room to alter lesson plans to suit the individual needs of their students or to experiment with more effective teaching styles, it's no surprise that many teachers become unhappy with their work. 

They may have gone into teaching hoping that it was their calling, but their current work feels like nothing more than a job. This doesn't make for a positive classroom environment for students, either! 

Unfortunately, overstructuring isn't the only factor depleting employee motivation. Reliance on financial incentives is another strategy that often backfires. But what could be more motivating than a pay raise? Let's dig deeper with a case study from an Israeli day care. 

The daycare center in question had been struggling with the high number of parents picking up their kids after regular hours. So, they introduced a fine that late parents would be required to pay. Hoping that this would encourage parents to be punctual, the daycare was rudely surprised by the results. 

Before the fine was introduced, a quarter of parents tended to arrive late. But after the fine, this number shot up to 40 percent. While parents had considered tardiness as poor behavior before the fine was implemented, late pick-ups simply became an option that parents could pay a little extra for. 

Financial incentives seem like a good idea at first, but ultimately run the risk of turning social or moral contracts of integrity into flexible financial relations, thus missing their target entirely.

> _"The offer of money tells people implicitly that they are operating in the financial/commercial domain, not the social domain."_

### 5. Theories about human nature are inventions that we must question. 

Today, the word discovery is often used interchangeably with _invention._ Yet the two are entirely different things. 

A discovery is something that simply exists. Though we may have found it, we had no role in bringing it into existence. An invention is something humans have brought into existence, not by finding it, but by _building it_. In the case of inventions, humans are responsible for the decision to create something. That means considering whether the invention is helpful, necessary, harmful or useless. 

Inventions aren't just objects or products, either; they can be ideas and theories too. This is crucial to remember, as theories about human nature are mistakenly seen as _discoveries_ all too often.

This was the case with Adam Smith's influential economic theory as presented in his book _The Wealth of Nations._ This famous late-eighteenth-century work had a lot of ideas about how workplaces should be configured to maximize efficiency. 

One of Smith's key arguments was that people only work if they are provided with _incentives._ This is an idea that workplaces around the globe today still hold on to. Think about it: the most obvious reward for hard work is more pay. 

But hang on — in the previous blink, we learned that financial incentives usually backfire. So why do we still believe we or our employees will be motivated by money?

Although many theories attempt to describe human nature, some of them end up dictating it instead. If nobody had ever heard of Adam Smith's ideas, we might approach work very differently — Smith's theory may very well have become a self-fulfilling prophecy. 

In the same way that we make the very mistakes our overbearing supervisors expect us to make, we've become motivated by money because that's what we expect of ourselves. Should things stay this way? Nope! Find out more in the final blink.

> _"We live with such ideas about human nature that are so pervasive that we don't even realize there is another way to look at ourselves."_

### 6. Companies must redefine efficiency and consider three key questions to improve their workplace. 

Purchasing a shirt that was manufactured using child labor is certainly a questionable decision from an ethical point of view. And while it might seem a strange thing to ask in the context, a typical economic measure would examine whether the decision to buy the shirt was _efficient_. Most of us aren't sure how to answer that one, which indicates just how limited the concept of _efficiency_ really is. 

Economists today define efficiency purely in terms of profit. But if economists want to take a truly modern approach, efficiency should be measured in light of other factors, too. Output is more than how much money is generated — customer satisfaction should also be evaluated. Similarly, input shouldn't just be the costs of physical labor, but should include the emotional labor supplied by workers as well. 

This revised definition already provides a solid framework for more sustainable business endeavors. By showing interest in the psychological costs of work, employers are better positioned to create new working processes that increase employee happiness and productivity. 

This all starts with asking three simple questions: _what, how,_ and _when_. 

What is the purpose of your employees' tasks? What positive impacts do they create? 

How do your workers complete their tasks? Are they strictly monitored, or do they call the shots? 

And finally, _when_ can you introduce change if things aren't looking so great at your workplace? 

Employers often answer the third question with avoidance. It's true that the modern economy is in constant flux; however, waiting for stability to change your company will only allow vicious cycles to deepen. The answer to the third question is always "_now"_!

> _"Each of us will have had a hand in creating a human nature that is worth living up to."_

### 7. Final summary 

The key message in this book:

**Heavy reliance on financial incentives and strict performance monitoring leaves workers feeling demotivated and disengaged. If companies want to make their workplace positive and productive, outdated theories must be left behind. By making employees feel trusted and valued, employers can make their jobs feel like a calling.**

Actionable advice:

**Create social incentives.**

The next time you want to motivate your employees, don't dangle a bonus in front of their noses. Instead, highlight the positive impact that their work has on the lives of others by getting employees in touch with customers. An automotive company, for example, might invite car owners to share their positive experiences with the employees that sold them the vehicle!

**Suggested further reading:** ** _The Paradox of Choice_** **by Barry Schwartz**

The abundance of choice that modern society presents us with is commonly believed to result in better options and greater satisfaction. However, author Barry Schwartz argues that too many choices can be detrimental to our psychological and emotional well-being. Through arguments based on current research in the social sciences, he demonstrates how more might actually be less.

**Got** **feedback**?

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Barry Schwartz

Barry Schwartz is an American psychologist and Dorwin Cartwright Professor of Social Theory and Social Action at Swarthmore College. He has authored more than ten books and published well over 100 papers in scientific journals. He also writes for leading newspapers, is a regular guest on radio programs and has even given two TED talks.

