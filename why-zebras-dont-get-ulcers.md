---
id: 565c32359906750007000000
slug: why-zebras-dont-get-ulcers-en
published_date: 2015-11-30T00:00:00.000+00:00
author: Robert M. Sapolsky
title: Why Zebras Don't Get Ulcers
subtitle: The Acclaimed Guide to Stress, Stress-Related Diseases, and Coping
main_color: 426815
text_color: 426815
---

# Why Zebras Don't Get Ulcers

_The Acclaimed Guide to Stress, Stress-Related Diseases, and Coping_

**Robert M. Sapolsky**

_Why Zebras Don't Get Ulcers_ (1994) vividly explains the biology behind stress and its impact on our lives, functioning as an effective way to deal with immediate problems, while also posing serious health risks in the long run. The author also offers plenty of practical tips on how to keep stress under control.

---
### 1. What’s in it for me? Understand how stress works and what you can do to tackle it. 

Stress — in many ways, it's a symbol of our times. Books and articles promise to help us tackle it, either by developing more efficient routines to avoid it, or by practicing mindfulness or meditation to cope with it. But what exactly is stress and how does it affect our bodies, minds and societies?

Unlike, say, zebras on the African savannah, humans can feel and create stress with the help of our evolved and complex brains. This makes humans susceptible to all kinds of stressors, such as problems envisioned in the future, that other mammals do not experience. This stress is not only bad in itself, but it also has effects on our cardiovascular system, insulin production, reproduction and, in the end, our overall health.

To explain all these processes, these blinks go on an in-depth exploration of how exactly they work and what we can do to tackle stress.

In these blinks, you'll discover

  * the role of stress in making depression a leading cause of medical disability by 2020;

  * why poor people are more likely to suffer from stress-related diseases; and

  * how stress can lead to diabetes.

### 2. Stress originates as a response to acute physical crises, but humans stress over imaginary things, too. 

It's two in the morning and you can't sleep, despite the fact that you have to deliver a career-defining presentation tomorrow morning. You're just too stressed out! Situations like this one are central to the human experience. But why do we feel stress at all?

Looking at the rest of the animal kingdom, we find that stress responses are activated by physical danger and risk. Imagine being a zebra on the savannah: the most stressful thing you could experience is fleeing the jaws of a lion while half your leg is ripped open. Or, if you're the lion, the stress would come from chasing a zebra while close to starvation.

Both these situations, while different, are crises that need to be dealt with immediately to ensure survival. 

Some animals also deal with chronic physical stress. Imagine, for example, having to walk dozens of miles every day in order to find food or water. 

For humans, however, the biggest source of stress is often psychological — stress that we simply conjure up in our own heads.

Consider situations like traffic jams, upcoming deadlines, not finding a parking spot or tense arguments with family or loved ones. None of these situations require extraordinary physical activity, as they're rarely settled with fistfights or narrow escapes.

Nonetheless, they generate stress in our minds.

Humans also get stressed out about things that _might_ happen in the future. For example, people worry about their mortgage, upcoming job interviews, their retirement funds — you name it.

This makes sense when we have an opportunity to mobilize a plan to deal with these stressors, but it's pointless when we're unable to affect the situation that causes our worrying.

So, from an evolutionary perspective, sustained psychological stress is a very recent phenomenon.

### 3. Our brain’s autonomic nervous system manages the way we respond to and recover from stress. 

Remember the last time someone scared you silly by jumping out from behind a door? In that moment, you were suddenly wide awake, focused and felt every fiber in your body. But what exactly causes us to feel that way?

This reaction is caused by the autonomic nervous system, which ensures that our bodies function without our conscious control. It automatically directs all our involuntary actions, such as blushing, breathing, getting goosebumps or having orgasms.

The autonomic nervous system is actually two systems that work in opposition to each other. The way these systems interact is crucial to how we respond to stress.

The _sympathetic nervous system_ kicks in during real or perceived emergencies, mediating vigilance, arousal, activation and mobilization. Or, as medical students sometimes jokingly put it, this system regulates the four Fs: flight, fight, fright and fuck.

Starting in the brain, the system projects into every organ, blood vessel and sweat gland in your body, all the way to the tiny little muscles at the root of each of your hairs. This is why you get goosebumps when someone startles you.

The sympathetic nervous system works in opposition to the _parasympathetic nervous system_, which mediates calm and vegetative activities. This system promotes growth, energy storage, digestion and similar processes.

So, while the _sympathetic_ system speeds up your heart, the _parasympathetic_ system slows it down.

These systems are dynamic; they can be activated at different speeds and for different periods. Your nerves, like electrical cables, can be activated with impressive precision, such that the activity of one particular organ can be immediately stimulated or inhibited. And it's a good thing too, because quick stress responses are crucial for survival: if you need to escape a predator, you need to crank up your heart rate _now_, not in five minutes.

In addition, the brain releases hormones into the bloodstream, which, while slower than stress reactions from the nervous system, have long-lasting effects throughout the body. However, having chronically high levels of these hormones makes it difficult to have normal stress responses and recovery.

### 4. When under stress, the body prioritizes short-term, high-cost actions over long-term projects. 

Imagine an exciting vacation is just days away, but you have a stressful few days at work and you get sick. Bummer! As it turns out, this rain on your parade is characteristic of how the human body handles stress. But to figure out how, we'll need a lesson in biology.

For all mammals, stress is about maximizing the energy that muscles have available to them. 

While bacteria can become dormant to survive with limited food and plants can develop poisonous leaves to avoid being eaten, mammals have to work to both find food and flee danger. And that means using our muscles.

To boost our energy levels, glucose and fat are moved through cells into the bloodstream. In addition, our heart rate, blood pressure and breathing all increase to transport nutrients and oxygen more quickly.

While this is happening, bodily functions that don't contribute to solving the immediate danger are all put on hold as a way to save energy. Digestion and tissue repair, for example, grind to a halt, while sex drive decreases and the immune system is inhibited.

Think about it: if you're a deer fleeing a hungry wolf, your energy is better spent helping you run than growing antlers, producing sperm or managing infections that aren't immediately life-threatening.

Furthermore, stress causes certain cognitive and sensory skills to become enhanced and your senses to become sharper. This is why we're easily scared by the tiniest noises when watching a horror movie.

These protective measures, while helpful, are also taxing on the body in the long run. If you constantly turn off important long-term maintenance functions, nothing in the body will ever get repaired. As a result, you'll have less surplus energy, meaning greater fatigue, an increased risk for ulcers and greater vulnerability to infectious diseases.

By this point you should have a good understanding of your body's natural stress reactions. The following blinks will look at how prolonged stress affects our lives in the long term.

> _"If there is a tornado bearing down on your house, this isn't the day to repaint the garage."_

### 5. Stress speeds up your blood flow, making you more likely to develop arterial and heart diseases. 

If you've ever turned on the water in your garden without holding onto the hose, then you probably noticed how stiff the hose gets as the water flows through at high speed. When stressed, something similar happens inside your body that can be quite dangerous over time.

When you're under stress, the muscles around the walls of your veins tighten, pushing the blood through at a higher speed. Once the blood reaches the heart, it slams into the walls of the heart, which, like a rubber band, snaps back, causing the heart rate to increase.

In addition, your arteries dilate so that blood can more easily cross over into the tissue and deliver much-needed energy. Your small blood vessels, in turn, have to work harder in order to regulate the distribution of blood. 

The result of these processes is a vicious cycle: your body reacts by building more muscles to control the blood flow, causing these small blood vessels to become more rigid, which in turn increases your blood pressure, which means your body needs to produce more muscles to regulate blood flow and so on.

Moreover, the rapid flow of blood creates inflammation in the blood vessels' _branch points_, which then leads to the formation of _blood clots_, little agglomerations of cells that hinder the flow of blood.

These branch points, or _bifurcations_, permeate throughout your body. In fact, no cell in your body is more than five cells away from a blood vessel.

Once blood clots tear loose, they tumble through your bloodstream and clog up smaller blood vessels, causing major damage. When this happens in one of your coronary arteries, you can suffer a _heart attack_ ; when it happens in the brain, you can have a _stroke_.

The damage that stress causes on the cardiovascular system can't be understated. In fact, heart disease is the number one cause of death in the United States.

### 6. The stress of transferring energy in the body can increase the risk of diabetes, which can lead to other illnesses. 

In the previous blink, we talked about the vital role that the cardiovascular system plays in distributing much-needed energy to your muscles during times of stress. But where does that energy come from?

After a meal, you have more nutrients in your body than you need at that moment. So, the body breaks down the food into its smallest components — amino acids, glucose and fatty acids — which are then stored in various places, like your liver and fat cells.

During stressful situations this process is reversed and the nutrients are released into the bloodstream so that your muscles have ample energy to deal with the stressful situation.

However, if the situation doesn't actually demand muscular activity, the nutrients are again reabsorbed into storage. But even this type of "false alarm" is taxing for your body, because the process of releasing and reabsorbing nutrients demands energy in itself. So if you constantly stress out over minor nuisances, your body will waste lots of energy on moving nutrients in and out of storage, and as a result you'll get tired more easily.

This continual stress can lead to onset of diabetes, of which there are two types.

Type 1 diabetes occurs when the immune system destroys insulin-secreting cells in the pancreas, meaning the body can no longer produce enough insulin. Glucose reuptake is then inhibited, energy levels plummet, organs start to malfunction, and the person must inject insulin.

Type 2 diabetes happens when cells fail to respond to insulin, caused by an increase of fat in the body. When fat cells are full, insulin tries to make them store _more_ fat, but instead the cells become resistant. Less glucose is taken up, resulting in an increase of glucose in the bloodstream. In turn, the pancreas starts making more insulin, but the body resists it. This leads to cell destruction in the pancreas.

In both types, an excess of fat and glucose circulates in the bloodstream, which increases _atherosclerotic glomming_ — the thickening and hardening of artery walls. When people with either type are chronically stressed, the glomming worsens, which increases their risk of heart disease, stroke and other illnesses.

> Diabetes afflicts about 300 million people worldwide.

### 7. Stress produces changes in the brain that resemble depression and make recovery from trauma more difficult. 

Most people know someone who suffers from depression. In fact, studies estimate that somewhere between five and 20 percent of all people will suffer through a major depression at one point or another. But what effect does depression have on the body?

_Depression_, the loss of an ability to feel pleasure accompanied by incapacitating grief and guilt, is a profoundly destructive condition that can have huge consequences for an individual's life. Indeed, studies project that depression will be the second-leading cause of medical disability globally by 2020.

Interestingly, the changes that take place in the brain and behavior of a depressed person are actually very similar to those experienced by a stressed person.

For example, stress saps the neurotransmitter _dopamine_ from your pleasure pathways, making you less likely to experience pleasure. This was demonstrated in a study, in which an electrode was implanted in the specific area in rats' brains which, when activated, causes them to feel immense pleasure. 

The rats had access to a lever that stimulated the electrode, and would pleasure themselves to death by pulling the lever, ignoring food, sex and everything else.

When those same rats were severely stressed through painful electrical shocks, they would need a higher intensity of electrical current in that area of the brain to feel pleasure afterward. In essence, the high level of stress they endured made them less sensitive to pleasurable experiences.

Another similarity between stress and depression is that they can both lead to _learned helplessness_. In another experiment, a rat cage was electrified one half at a time, with a signal given beforehand indicating which half is affected. Very quickly, the rats learned to avoid the side of the cage that would be electrified. However, after some of the rats were exposed to a series of unpredictable shocks, they seemed to completely lose confidence in their problem-solving skills, and no longer bothered to avoid even the predictable shocks. This is known as learned helplessness, and is also very common in depressed individuals, who feel helpless to improve their situation.

These and other findings indicate that depression can also be a stress-related disease, induced not by severely harmful experiences, but rather by an inability to recover from them.

### 8. Our intricate reproductive system is easily affected by stress, leading to problems for both men and women. 

If you know anything about marketing, then you know that "sex sells." Unfortunately, with all the media coverage, half-truths and titillating stories about how great it can be, sex is one of our greatest sources of stress, and is itself profoundly influenced by stress.

For men, stress leads to premature ejaculation and difficulty achieving an erection. The process of getting an erection is managed by the parasympathetic autonomic nervous system, which, as you recall, is responsible for slowing the body down. One has to be calm and relaxed to get an erection — that is, the opposite of stressed.

Orgasms, on the other hand, occur only after the heart and breathing rate increase through the sympathetic system. As such, sexual therapists recommend taking deep breaths to avoid climaxing early, as expanding the chest muscles produces a signal that promotes parasympathetic activity.

Erectile dysfunction and premature ejaculation are also major causes of stress, and since they typically occur during times of stress, this can cause men to become trapped in a vicious cycle of performance anxiety.

In women, stress decreases the production and secretion of the hormone estrogen, leading to irregular menstruation cycles and a loss of libido.

Estrogen plays a crucial rule in women's sexuality by increasing the sensitivity of the genitals and other parts of the body. What's more, the hormone also gets picked up by receptors in regions of the brain that are active during salacious thoughts.

Usually, estrogen is produced by converting another hormone called _androgen_. Under chronic stress, however, this conversion process comes to a halt, and androgen concentrations build up, inhibiting numerous steps in the reproductive system.

As you've seen, long-term stress can be very hard on the body. The last few blinks will look at ways we can reduce and better cope with our stress.

### 9. Stress is unavoidable, so understanding and balancing your stress response systems is key. 

So far you've seen some of the many negative consequences of stress. But as you'll soon discover, these are just the tip of the iceberg.

Over time, scientific perspectives on how the body manages stress have changed. Scientists once believed that the body functions according to the _homeostatic_ principle, which asserts that problems in the body can be corrected with a single local adjustment.

If you imagine your body to be the city of San Francisco during a water shortage, the homeostatic solution to this problem would be ordering people to use smaller toilet tanks.

In reality, since everything in your body is connected, any part of your body can be regulated in numerous different ways. 

This principle is called _allostasis_, and describes how the body is managed through many small adjustments in various locations. So, to solve San Francisco's water problem, you would use smaller toilet tanks, but would also try to convince people to save water and import rice from China, instead of continuing with water-intensive rice farming locally.

Adjustments that occur during an allostatic stress response are complex and affect numerous other functions in the body.

Achieving allostatic balance at low stress hormone levels is like balancing two children on a seesaw — it's pretty easy. But when high stress causes a deluge of these hormones, it's more like balancing two elephants.

It's not impossible, but it takes a lot of energy that could be spent more productively on long-term building projects within the body, such as cell repair and producing antibodies against diseases.

And no matter how delicately you adjust the elephants on the seesaw, there will be some damage in the garden, just because elephants are huge and clumsy. 

In this way, it's hard to fix one major problem in the body without knocking something else out of balance. And that's why stress affects so many aspects of life: sleep, memory, eating, growing, immunity, pregnancy, aging, addiction and so on.

### 10. Taking responsibility for the things you can control and providing social support have a strong stress-reducing effect. 

What do you do when you're feeling stressed? Do you try to grab the problem by the horns yourself, or turn to a friend for help? As you'll see, both of these strategies work just fine.

Taking responsibility for stressful situations can bring relief as you regain a bit of control over your life.

For example, studies in nursing homes have shown that giving the elderly responsibility for everyday decision making, like choosing meals or activities, had numerous positive effects on their lives. Specifically, it increased activity, happiness and health while cutting mortality over the course of the study in half.

Interestingly, when staff _encouraged_ them to solve certain tasks, health indices increased. But when the staff _helped_ solve tasks, they decreased. Without actually taking responsibility for their tasks, nursing home residents wouldn't get the benefit of stress reduction.

It's also important to recognize what stress you can solve, and when you simply need to alter your perception. To illustrate the difference, imagine that you've got a big test coming up that you're stressing out about.

Before the test, you can relieve some stress by taking action, namely by studying. If you flunk the test, however, there's nothing you can do but alter your perception by reframing the meaning of your bad grade. You can, for example, remind yourself that the most important thing was what you learned, not the grade you received.

In addition, both giving and receiving social support is an extremely effective preventative measure against stressors. 

Having a shoulder to cry on can offer a lot of stress relief. But offering social support can be similarly stress-reducing. Consider that, on average, married people are generally healthier than single people, as couples both give and receive emotional support.

Similarly, judges and court practitioners, professionals who provide a valuable social service and also receive a great deal of respect for it, tend to be much healthier, both mentally and physically, in old age.

> _"God grant me the serenity to accept the things I cannot change, courage to change the things I can, and wisdom to know the difference." — Prayer by Reinhold Niebuhr, adopted by Alcoholics Anonymous_

### 11. Your place in society affects stress levels and has a great impact on resistance to illness and mortality rates. 

Up to this point, we've mentioned numerous examples of isolated incidents that contribute to stress, like fleeing predators or pre-test anxiety. But some stressors, like poverty, go beyond isolated incidents and can cause chronic stress.

In fact, being poor is associated with many physical and mental stressors. Poor people often work jobs with high physical demands and low job security, leaving them with little control over their work situation and high levels of stress.

Moreover, poor people have fewer opportunities to relieve stress with vacations or hobbies — there's just not enough money to spare. 

As a result, poverty makes you more likely to experience stress-related diseases, and this effect endures even after you're out of poverty. This was illustrated by a study of elderly nuns who entered the nunnery at a young age, which found that the conditions they grew up in influenced patterns of disease in old age — this despite having lived in the exact same conditions for 50 years.

But you don't have to _be_ poor to feel the stress-related effects of poverty. In fact, just _feeling_ poor is almost as bad. Whether or not you feel poor depends on your _subjective socioeconomic status_, which measures your standard of living and financial security against those of your peers.

Studies in the richest quarter of the world's countries show that after having reached a certain standard of living that secures your well-being, the amount of money in your bank account is actually less critical to your stress levels than how you compare your financial well-being to those around you.

> _"Medicine is a social science, and politics nothing but medicine on a large scale." — Rudolph Virchow, 19th century_

### 12. Income inequality promotes lack of trust and social cohesion, which translates into worse health for both the wealthy and the poor. 

The last blink demonstrated how poor socioeconomic status can have a negative impact on our health. But what socioeconomic factors can contribute to creating a healthier society?

For one, communities with high _social capital_ enjoy higher equity and better health. Similar to financial capital, social capital refers to the resources you can draw upon in times of trouble, like having friendly neighbors who can babysit for you when you need to work overtime. In communities with high social capital, people are less socially isolated. 

Furthermore, communities with high social capital have a better diffusion of health information and less psychological stress, since people feel safer.

One way to measure social capital is by examining voter turnout. When people feel that they're part of a community and that they can change something in their region, they're more likely to think they can change something on a national level too.

In contrast, income inequality causes the overall health of community members to decline. The higher the degree of income inequality, the higher the mortality rate will be for that community; this effect spans across all ages and races at local and regional levels.

For example, while the United States is a very wealthy nation, it has gaping income inequality. Thus, its mortality rate is much higher than its next-door neighbor, Canada, which is poorer but more egalitarian.

But it's not just the poor who benefit from greater equality. Studies show that in societies with better income equality, both the rich and poor benefit from improved health. The rich are less stressed because they don't feel like they have to wall themselves off in gated communities and private schools, while the poor are healthier because they aren't left behind.

Poverty and stress are about more than simply not having enough money to make ends meet. They are about living in a society that tolerates leaving so many people behind, and that promotes hostility, distrust and crime. All this translates into physical and psychological stressors for the rich and poor alike.

### 13. Final summary 

The key message in this book:

**Stress occurs as a means to keep us alive in life-or-death situations. But in the modern world, we stress out about everything, from missed deadlines to imaginary arguments. All this stress is terrible for the body, and for the sake of our health we all need to learn how to better cope with stress.**

Actionable advice:

**Find your personal outlet for stress and do it regularly.**

Plenty of people claim physical exercise is a great stress reliever; but that's only true if you actually _want_ to exercise. The truth is, there's no one-size-fits-all stress reliever. All that matters is that you find an activity that pulls you out of a stressful state of mind. Whatever it is, take some time to do your chosen activity every day. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Robert M. Sapolsky

Robert Sapolsky is a professor of biology and neurology at Stanford University, a leading stress researcher and a regular contributor to the magazines _Discover_ and _The Sciences_. He is also a recipient of the MacArthur Foundation Genius Grant, and is the author of _A Primate's Memoir_ and _The Trouble With Testosterone_.

