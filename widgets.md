---
id: 56d42893bb82ae000700003b
slug: widgets-en
published_date: 2016-03-01T00:00:00.000+00:00
author: Rodd Wagner
title: Widgets
subtitle: The 12 New Rules for Managing Your Employees As If They're Real People.
main_color: EF5934
text_color: D6502F
---

# Widgets

_The 12 New Rules for Managing Your Employees As If They're Real People._

**Rodd Wagner**

_Widgets_ (2015) is a guide to improving employees' performance, regardless of the field they work in. These blinks outline the fundamental principles, based on scientific research, that will help any company improve its efficiency, profitability and worker satisfaction.

---
### 1. What’s in it for me? Find the secret to boosting your employees’ performance. 

We've all read stories about how Steve Jobs, Mark Zuckerberg, Elon Musk and other business giants changed the world with their ideas and vision. What we often forget is that behind the success of all these people are legions of employees — employees who work hard every day to make the company's vision a reality. 

If you have employees who love to come to work and are passionate about learning more and taking on more responsibility, there's no limit to what your company can achieve. If you don't, your company will probably sink like a stone. So how do you energize your employees and keep them motivated? Simple — just follow the 12 essential steps.

In these blinks, you'll learn

  * why _homo economicus_ is out and _homo reciprocans_ is in;

  * how Hewlett-Packard survived a recession; and

  * what happens when you get students to build the same LEGO figure over and over again.

### 2. The way employees are incentivized affects their energy levels. 

Are you a morning person who tends to wake up energized and excited at the thought of going to work? Or do you dread hearing your alarm clock in the morning? 

Either way, you're certainly not alone. When it comes to energy levels at work, most of us fall under one of the four different types of employees. 

The first group, comprising 19 percent of the US workforce, is called the _demoralized group_. People like this don't like their jobs whatsoever and tend to feel that their bosses never give them positive feedback. 

The second type is called the _frustrated group_. Accounting for 23 percent of the working population, they sometimes receive positive reinforcement from their bosses, but often have a neutral or negative opinion of their work. 

Third is the _encouraged group_, which makes up 29 percent of all US workers. These employees see their jobs as good, but not great. 

And finally, the _energized group_ is the 29 percent of the US working population with the highest level of energy on the job. They tend to report extremely high levels of job satisfaction. 

But why are so many workers unmotivated and tired on the job?

It's in part due to the widespread view of humans as _homo economicus_ rather than _homo reciprocans_. 

Homo economicus is believed to be rational and motivated by money. So, if you want homo economicus to do something, you simply threaten him — with dismissal, for instance — and he'll do as he's told. This method sees humans as cogs in a machine that can be controlled with proverbial carrots and sticks.

The homo reciprocans, however, is defined by his tendency toward reciprocity. So, if you scratch his back, he'll do the same for you — but will also reciprocate poor treatment. This style of management is more in line with true human behavior.

So, when employers wonder about why their employees aren't engaging with their work, it often has to do with the way a boss treats his workers. If you don't treat employees like individuals, they're bound to become frustrated.

> _The choice of what kinds of attitudes your employees bring to work is yours._

### 3. Being treated like individuals and given a secure position helps employees excel. 

If a manager could do one thing to make his employees happy, what would it be?

Well, since employees want to be seen as individuals, it's essential that managers meet with their employees on a regular basis and promote individuality. For instance, the Make-a-Wish foundation encourages its employees to come up with personalized titles for themselves to be used within the organization. 

Some of the amazing choices include "Goddess of Greeting" and "Heralder of Happy News". But it wasn't just a creative exercise. Expressing their individuality actually helped employees tackle the emotional difficulties of assisting terminally ill children in satisfying their dying wishes. 

So, personality is key — but plenty of companies continue to ignore the individuality of their employees. In fact, just seven out of ten employees in the United States are given the opportunity to meet regularly with their managers, which is an important factor in addressing individuals' needs. In addition, only 21 percent of American employees agree with the statement "My manager understands me."

Beyond simply being seen as individuals, employees also require job security to ensure consistent and strong performance. Research has found that among people who are concerned about losing their jobs, 56 percent want to work elsewhere; that's compared to just 22 percent among people who feel a high level of job security. 

By way of example, in 1970, Hewlett-Packard was struggling due to a stagnating US economy. But instead of letting people go, the board of management agreed that everyone in the company, except for a few people, would be required to take off every second Friday, accepting a 10 percent pay cut in the process. A few months after their decision, the crisis was over and the company came out strong, all because they offered their employees the job security they needed to thrive.

And that's just the tip of the iceberg. In the next blinks, you'll learn about many more creative solutions for boosting employee performance!

### 4. Companies must care about their employees’ long-term financial goals, as well as their work-life balance. 

Which would you prefer: a boss who gives you the money you need to reach your goals or one who takes an interest in your personal financial ambitions, helping you reach them over time?

Well, while many people would prefer a cash payout, it's actually more beneficial for companies to invest in their employees' long-term financial goals. In fact, money doesn't actually incentivize employees in the long run — all it does is make them want more money. 

Why?

This is in part due to an effect known as _hedonic adaptation_, the human tendency to get used to material goods. For instance, when you first buy a new car, driving it can be an absolute thrill. But just a few months later it may not feel like anything special. 

So, a better strategy than giving employees money in hand is for companies to help their workers reach their long-term financial goals, which in turn helps the company succeed and breeds loyalty. In fact, among US employees who feel that their company is helping them meet their financial goals, just four percent want to leave their jobs. 

But money is useless if you can't enjoy it, which is why companies who want thriving employees need to help them strike a healthy work-life balance. For instance, in Canada, 16 percent of employees find it difficult to balance the demands of their jobs with their private lives. This can be a dangerous situation, resulting in burnout. 

Therefore, it's imperative that companies understand that people have limits, and that performing beyond them for too long is bound to backfire. Overworked employees become exhausted and their work suffers as a result. 

As such, it's in the best interests of companies to help their employees achieve a balanced routine. For example, the software provider SAS implemented a 35-hour workweek to improve its employees' well-being.

> _"The most basic fact about money is that most of us want more."_

### 5. Being cool and transparent can go a long way. 

Anyone who went to high school knows that cool people are the most popular. The same goes for businesses: attractive companies are cool companies. 

For instance, according to the author's research, just six out of ten employees think their workplace is "cool," which is unfortunate because people who work for companies that they consider cool are 13 times more creative than those who don't. In addition, employees who maintain a positive view of their companies take pride in their work and recommend the company to people on the job hunt. 

So, while it's difficult to define what a cool company is, certain factors carry a lot of weight. For example, when it comes to a person, coolness has a lot to do with being yourself and not someone else. For a company, it's similar. 

Companies shouldn't look to their peers for guidance or constantly talk about how different companies are succeeding; instead, they should be all about themselves, pointing out how they're unique and what's good about them. 

Just consider Apple. Even though everyone has heard stories about how difficult it could be to work underneath the visionary perfectionist Steve Jobs, the company is undeniably cool. 

But just as important as coolness is transparency, because maintaining transparency will help build trust with employees. For example, in 2014, the Pew Research Center reported that just 19 percent of Generation Y, referring to people born between 1980 and 1995, think other people are trustworthy. 

This is a huge drop from Generation X, born between 1965 and 1980, for which the same statistic was 30 percent — and it looks even worse when compared to the 40 percent reported by Baby Boomers, born between 1950 and 1965.

Luckily, these startling numbers can be reversed, and doing so will depend on companies becoming more transparent. Transparency promotes trust as employees no longer feel that their company is hiding important information from them. This might be as simple as making employee salaries public or talking about the strengths and weaknesses of upper management.

### 6. Productive employees are those who find their work meaningful and see a promising future for themselves at a company. 

When you're working, do you find some tasks more fulfilling and enjoyable than others?

Most people do, and making all tasks more meaningful to employees is a sure-fire way to get them to work more efficiently. For instance, an experiment conducted at Harvard University found that nobody enjoys working on tasks they consider meaningless. 

In the study, students were asked to build a LEGO figure in exchange for $3. Then they were asked if they would like to build a second one for $2.70, then a third for $2.40 and so on. 

One group of participants were given materials to assemble a new figure each time and another used the same materials over and over after they were disassembled by researchers. Unsurprisingly, the first group built 50 percent more figures on average than the second, as they could see the varying results of their work and therefore believed it to have meaning. 

If employees see their work as meaningful, they'll be more productive. But there's one thing to keep in mind: this strategy only works if the employer can communicate how significant the work is to _her_. Because if she can't, employees might feel like they're being manipulated into working more. 

But it's also essential for companies to take into account the expectations employees have for the future of the business. Among employees who feel the least optimism about a company's future, 62 percent plan to quit in the next few months. On the other hand, those who are most optimistic about a firm's future are also optimistic about their future at the company. 

So, it's crucial for companies to brighten employee expectations of what's to come, such as by investing in employees' personal development.

### 7. Employees need to be recognized and feel like they’re part of a team. 

For most people, having a boss recognize your contributions is a pretty great feeling. Being acknowledged for doing a good job is a great motivator and this recognition can be used to stimulate employee success. 

When a person accomplishes something, the neurotransmitter dopamine is released in their brain, producing a pleasurable sensation. But dopamine isn't just released when you succeed — you also get a dose of the chemical when your success is recognized by others. 

Humans are social creatures and approval from others demonstrates our importance within a group. However, despite the obvious importance of approval, research has found that just 25 percent of the US workforce feels that they receive positive feedback for good work. 

But recognizing employee contributions doesn't necessarily mean turning a blind eye to failures. Managers should still tell employees when they mess up, while keeping in mind that people need recognition to succeed. 

It's also essential for employees to feel like they're part of a team, as they are more likely to focus on customers and feel a greater sense of obligation to their company. It also makes for a more positive working environment. 

For instance, in the United States, 70 percent of workers say they feel a strong sense of teamwork on the job, 81 percent report being good friends with co-workers and 77 percent are of the opinion that they work with talented people. 

Nonetheless, plenty of companies still focus on business results at the expense of human relationships, even though it's not such a difficult balance to find.

Building meaningful human connections could be as simple as, say, allowing flexible working spaces that enable employees to work close to the people they like.

> _"In a neurobiological sense, dopamine is the fuel of motivation and learning."_

### 8. Strong companies let their employees lead and challenge them with difficult situations. 

Most parents know that giving their kids responsibilities is essential to helping them grow and feel good about themselves. Similarly, in the working world, good management is all about letting employees lead.

When employees have a voice in how their work is accomplished, they tend to be more motivated. For instance, the American online retailer Zappos runs on a _holacracy_, a company structure that swaps a top-to-bottom power division for a broadly distributed one. As a result, Zappos is renowned for its highly engaged employees. 

But this also has major implications for the role of management. For example, instead of telling employees what to do, managers become more like coaches that bring out the best in their teams. Good managers know when to give an employee more or less responsibility. So, at its core, management is becoming more about enabling employees to lead an organization. 

Putting employees in extreme situations can also help them improve, since it's only in extreme situations that people are pushed to their absolute best. Consider the crew of a US Navy submarine: they often work in extremely harsh conditions, confined to the submarine for long periods and with little personal space. They also rarely see their loved ones.

Nonetheless, many of these people have very high levels of psychological resilience and can easily bounce back from mistakes or bad news. They have adapted to the extreme circumstances of their jobs and have grown as a result. 

Applying this insight to the world of business means there's something to be gained from dire situations like a high-stress project. So, the next time you're faced with such a scenario, just keep in mind that it's also an opportunity for personal growth!

### 9. Final Summary 

The key message in this book:

**Employers have more influence over their workers than they might realize. If managers start seeing employees as individuals with personal needs, they can substantially boost the energy, motivation, productivity and creativity of their workforce, thereby making their companies more profitable.**

Actionable advice:

**Organize a company event.**

The psychological well-being of employees is essential to company success, since employees who are taken care of by their company will return the favor. Treating your employees right is hugely important and organizing events is an easy way to show them you care. Even a small gesture can pay off big time!

Got feedback?

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested further reading:** ** _Triggers_** **by Marshall Goldsmith and Mark Reiter**

_Triggers_ (2015) exposes the things in your life that you didn't know were affecting you — and what you can do to stop these things from preventing you making positive change. Backed up with insightful research and filled with the experiences of the author and his clients, these blinks will help you eliminate unwanted behaviors and put you on the path to achieving your personal goals.
---

### Rodd Wagner

Rodd Wagner has worked for a wide range of companies across the globe and is currently vice president of employee engagement strategy for BI Worldwide. His books have been featured in _The Wall Street Journal,_ ABC's _News Now_ and _CNBC.com._

