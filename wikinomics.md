---
id: 537ca8c53465300007d30000
slug: wikinomics-en
published_date: 2014-05-20T13:29:31.000+00:00
author: Don Tapscott & Anthony D. Williams
title: Wikinomics
subtitle: How Mass Collaboration Changes Everything
main_color: 55A4DB
text_color: 2070A8
---

# Wikinomics

_How Mass Collaboration Changes Everything_

**Don Tapscott & Anthony D. Williams**

_Wikinomics_ shows how Wikipedia-like mass collaboration of individuals is revolutionizing society and business, and why this is actually good for companies and the public.

---
### 1. What‘s in it for me: learn how to tap into the powers of mass collaboration. 

The term "Wikinomics" describes the mix of technology, demographics and global economics that makes mass collaboration possible. Although the complexity of mass collaboration might seem overwhelming to some, it is actually a beneficial business and social strategy if implemented correctly.

Not only can mass collaboration drive the innovation that helps businesses grow, but it can also serve as a catalyst to change the very way that society functions.

In fact, mass collaboration is _the_ engine of the new economy, and can therefore be a powerful weapon for those who can understand and wield it, and a nail in the coffin for those who can't.

Truth is, the future of business as a whole depends on how companies adapt to the "Wikinomics." As consumers continue to demand higher quality products, they also want to be more and more involved in the _creation_ of those products. Employees too expect to have a certain freedom and autonomy.

These changes demand the attention of business and a reaction that generates new strategies to manage their goods, workforce and property.

In these blinks we learn how to effectively harness the power of mass collaborations by looking at positive examples of the way in which open collaboration can increase a product's value and thus turn any company into a successful, innovative one.

In addition, these blinks will show you:

  * How to turn your competitors into collaborators by sharing your information.

  * How one business made heaps of money from the dust collecting on their shelves.

  * Why one company pays their employees to do whatever they want.

And why centralized production has gone the way of the dinosaur.

### 2. In the digital age, mass collaboration affects almost every sphere of our lives. 

Within the last 25 years, the internet has become a significant part of the lives of more than one third of the global population, and enabled us to communicate and cooperate with people all around the world on a massive scale.

In fact, this easy access to information technologies is one of the primary tools for active participation in numerous areas of life traditionally reserved for certain elites, such as scientists or academics.

Wikipedia is the perfect example of this concept in action: In the past, only scientists were responsible for assembling the world's knowledge into tomes and volumes, whereas today _anyone_ can go online and co-create the content of the world's largest encyclopedia.

In fact, even traditionally secretive endeavors, such as gold mining, can be turned into an open, collective activity.

For example, in 2000 Goldcorp Inc. published all of its data for a certain property online and ran a contest to produce the closest estimation for the location of the gold seam. As geologists, consultants and students from around the world started submitting their estimations, the company gained valuable information that enabled them to locate _twice_ as many mining targets as they had previously found on their own.

As the internet continues to evolve, mass collaboration becomes an even more key feature. Indeed, when the internet was first conceptualized, it was thought of as a "digital newspaper," but today it functions more like a "shared canvas" where each user can leave their own mark.

Even a simple act like sharing a photo on Facebook has an impact on the online environment by adding to the content of the web. Consequently, most companies try to make their websites interactive, and treat their customers like co-creators instead of mere observers.

As we can see, collaboration today involves the self-organized participation of many individuals, much like a swarm of bees. The interplay between these individual actors sounds complicated, so in the next blink we'll derive some sense from this seeming chaos.

### 3. The core principles of mass collaboration are openness, peering, sharing and acting globally. 

So what actually makes collaborative efforts successful?

For starters, both sides of a collaboration must be _open_ to each other. This means having a willingness to let external talents contribute, thereby making collaborations possible. One great example of this is Linden Labs' game _Second Life_, which is totally unscripted and allows users to create every aspect of the game from scratch.

Secondly, there's _peering_ — a method of production that utilizes flat hierarchies and self-organization where individuals voluntarily contribute to the work as a whole. Peering aids mass collaboration by increasing a product's value and motivating interested individuals to volunteer their labor and join in the effort.

The leading example of peer production in action is Wikipedia, aka the "encyclopedia that anyone can edit." Wikipedia relies completely on the passion of its contributors. In addition, there is no traditional business hierarchy; instead, the quality of the submissions is regulated by the self-correction of the community.

Next, _sharing_, or providing others with access to your information and resources, allows individual members of mass collaborations to further their own inventions and allow for thriving innovations.

One example of this is the way that people on a Skype-call or gtalk share computing power as a resource, thereby making face-to-face communication over the internet possible. Needless to say, both parties benefit from sharing.

Finally, today's open markets make collaboration possible on a massive scale never before seen. This requires all participants in a collaboration to _act globally_, meaning that they coordinate their staff and strategies in a way that promotes a unified whole instead of slicing the production into many local branches that are essentially duplicates of one another.

An example of a company failing to act globally was General Motors, which at one point was employing the same staff positions in each local division of their corporation. This turned out to be an unnecessary expense, as a single person could have performed the same duties across the globe.

Now that you have a firm understanding of what constitutes mass collaboration, the following blinks will explore what these principles will mean for the future of already-established companies.

### 4. The growing demands of a globalized economy mean that companies must massively collaborate or die. 

Companies that don't adequately adopt the principles of mass collaboration will ultimately be left in the dust; they simply won't be able to produce enough value quickly enough, and would also lose the interest of the consumer.

In fact, today's open markets offer more opportunities to customers and — the flip side — more competition for companies. This is because mass collaboration helps companies not only to better develop their products, but also to better market them to their customers.

In addition, the low production costs of emerging forces like China and India, who offer quality services and products for only a fraction of prices found in the West, makes it unlikely that companies that don't incorporate global collaboration could survive the competition.

Furthermore, by collaborating actively and on a massive scale, a company can better hope to produce enough novel innovations to remain competitive.

However, it's not the large pool of participants, and therefore the larger number of ideas, that lead mass collaborations to produce innovations; rather, they produce innovations because their contributors themselves are motivated and diverse, which makes it easier to match talents with certain tasks.

Moreover, mass collaborations, especially those that employ peer production, often use "general public licenses," which give others the opportunity to contribute to the project and ultimately results in faster progress.

Even the software giant IBM employed this strategy and adopted the open source software approach when their company faced a crisis, and learned to involve external programmers in their collaborations rather than offering ready-made but conventional products.

With all these benefits in mind, mass collaborations readily become a "must" for companies who want to be market leaders, but mass collaboration also carries an element of risk — if you're not careful.

### 5. Although mass collaboration offers many benefits, it also involves risks. 

Although mass collaboration is an overwhelmingly beneficial strategy, it also involves certain risks.

For example, finding the right ideas and implementing them consumes a lot of time, energy, and resources, and in some cases turn out to be very expensive. This is especially true in underdeveloped idea markets, where finding the right partners for a collaboration can be tough. Even worse, if you are especially unlucky, competitors can swoop in and take advantage of the same ideas you want to exploit before you have a chance to get them off the ground.

In addition, if you want to use someone _else's_ idea that's not already part of your collaboration, then you must often offer them a "bargaining chip," i.e., monetary compensation and/or other incentives. This is especially difficult for start-ups or small businesses, who might lack the resources to "ante up."

Mass collaboration also comes with the constant, hidden risk of losing control over your own product. When you give others the right to co-develop, then value moves from the component products to the new invention made from those products. This means that the companies must constantly be aware of how their product is being used and developed in order to stay innovative.

You can see this protective attitude in the music industry, where music labels still treat mash-up artists like criminals rather than collaborators, and refuse to allow them to freely remix copyrighted music.

Finally, when collaborating with external partners, you simply can't assume that they'll have the same amount of credibility and work ethics as you do. Since mass collaborations are often peer productions, a company can't count on the same work ethics as they'd enjoy in-house; their contributors simply can't and won't deliver the same kind of work as an employee.

The risks associated with mass collaboration clearly make it something that companies can't dive into head first. They must first develop a strategy for implementing these new possibilities within their existing business model.

The following blinks will explore exactly how companies can reap the benefits of mass collaboration.

### 6. Companies should find the balance between protecting their intellectual property and opening up to mass collaboration. 

While the protection of intellectual property is part of the foundation of monetary success, companies must be prepared to share at least a certain amount of their intelligence in order to collaborate effectively.

Intellectual property is important because it is necessary for commercial success. In a world where money matters, "intellectual communism" — where one offers innovation and value without receiving monetary compensation — simply doesn't provide incentives.

Therefore it makes sense to avert risk by withholding a certain amount of information; after all, collaborators today can easily turn into competitors tomorrow.

Despite acknowledging the inherent value of intellectual property, companies must balance their safeguards with a more open approach in order to collaborate effectively, and thus reach their full potential.

A great example of this kind of collaboration is the Human Genome Project: A few pharmaceutical companies were all working independently on their own human genome projects, but abandoned them and their property rights in order to collaborate with one another. Although some of them were competitors, they were still able work together on this specific project so that everyone could benefit from the results.

However, companies can also make use of mass collaboration without ever endangering their products by using external resources and inviting the help of active consumers, dubbed "prosumers."

For example, the internet platform InnoCentive, which functions like an "eBay for innovation," allows anonymous companies to take advantage of external resources by revealing the exact nature of a specific R&D problem, which the contributors then try to solve.

Famous shoemaker John Fluevog is another great example of the way in which companies can use external resources. By creating "open source footwear," he offered prosumers the chance to submit shoe designs and in doing so received many valuable ideas. Even though he alone chooses whether a design goes into production, his consumer-friendly strategy improved his reputation within his customer community.

### 7. The production and work process itself must be adequate to these strategies. Decentralization and encouragement of self-initiative are essential. 

An effective collaboration starts in-house — indeed, the implementation of the principles of mass collaboration within one's own organization is as important as finding good global partners.

In fact, many companies already adopt this mindset: As the cost of finding partners continues to sink, it becomes more lucrative for businesses to focus on a core activity and then to outsource other functions whenever it is cheaper to do so. Since performing all the necessary tasks to produce a product in-house can be very expensive, it is only logical to delegate certain tasks to suppliers.

For example, multinational firms like BMW make use of this logic in their business structure: while BMW itself focuses on managing, controlling and distributing its products, the "handwork" is increasingly handed over to specialist suppliers.

And as the complexity of work and the expectations placed on employees continues to grow, giving autonomy to the workforce and welcoming enthusiasm in all its forms becomes an increasingly valuable in-house strategy. Indeed, vertical hierarchies prove counter-productive to the creative types of work, for example, which is often executed in teams.

By forgoing these hierarchies and encouraging self-initiative and enthusiasm, companies can access the full potential of their employees. Google, for example, employs just this principle: their policy is to have their employees devote 20 percent of their work time to personal projects with the hope that this will prove profitable for Google in the long term.

In addition, a company can simultaneously win their employees' trust and motivate them to perform when they share important information with them. In fact, transparency within the organization guarantees that employees better understand their products, and can therefore better coordinate their work.

Sun Microsystems CEO Jonathan Schwartz is also an advocate of this strategy, and regularly blogs about his job to ensure that his employees understand the decision-making process.

### 8. This results in lower transaction costs and increased profits, as well as innovations and a generally greater product value. 

Ultimately, mass collaboration is greatly beneficial to companies: it helps them to expand their operations, produce novel and exciting innovations, and create value in general.

But perhaps most importantly, mass collaboration can bolster your bottom line: by collaborating with others on a massive scale, companies can squeeze more from less. For instance, mass collaborations can help reduce a company's expenditures by splitting the cost burden between themselves and the cooperating companies, or they can boost their profits by sharing "useless" inventions.

For example, Procter & Gamble did just that when they realized one day that they were using less than 10 percent of their patents! Rather than simply let this perfectly good intellectual property collect dust on the shelves, they decided to license the patents out in order to recoup the huge R&D costs of those languishing inventions.

In addition, mass collaborations drive innovation and value forward by optimally allocating talent where it belongs, and by doing so can actually increase the speed at which innovations occur. For example, companies that partner with universities can take advantage of differing perspectives, thus helping them to not only focus on a single solution to a problem, but also remain open to many solutions.

Mass collaborations also drive innovation by making it easier to find the very best person for the job: because people make willing, voluntary contributions to their collaboration, they are therefore more creative and successful.

An example of this can be seen in Lego's Mindstorm project, which served as an answer to consumers' desire to participate in the development of their toys. Now each new idea contributed by the consumer makes the product, and the Lego company, more valuable.

By now we've seen how mass collaboration can be a winning strategy for businesses; but it turns out that the implications of mass collaboration are much further reaching . . .

### 9. Mass collaboration changes the way society as a whole functions. 

Technology and the opportunities that it creates are an engine for social development. As a result, global collaboration is no longer limited to creating intellectual property, but has evolved into a culture in itself.

The digital era has changed the way people think and therefore the way society as a whole functions. Indeed, the "Net Generation," which has grown up amid ever-developing technology, has its own unique values, such as an emphasis on personal rights, co-creation of media content and skeptical views of authority, while valuing qualities like speed, transparency and playfulness.

In addition, young people use technology in a way that might be foreign to older people: rather than simply being incredibly technically proficient, young people also use internet platforms as private spaces for self expression to overcome a lack of control and choice in the physical world.

Furthermore, mass collaboration, specifically peer production, is transitioning from the immaterial world of the internet to the physical world.

The classroom, which is generally thought of as a physical space, has begun to lose its tangibility: for instance, the TakingItGlobal's Virtual Classroom project has revolutionized the learning process by connecting students and teachers from around the world, thus freeing them from the confines of the classroom.

Global enterprises also use mass collaboration to produce goods by involving specialist partners for every step of the production process. In this way the end product is a sum of the many independent parts at work.

A prime example of this is the creation of Boeing's 787 Dreamliner: its production involved 100 suppliers from six different countries who were granted almost complete freedom in implementing the part specifications. When they've completed their designs, they then send their work to Washington, where each part is fitted together like Lego building blocks.

Clearly, the possibilities for development and creation are limitless with mass collaboration. Having moved from industry to society, we have to wonder where mass collaboration will lead next!

### 10. Final summary 

The key message in this book:

**"Mass collaboration" isn't a term that is often said aloud, and yet this phenomenon has a drastic impact on every part of our lives. Businesses face the choice of either collaborating or going the way of the dinosaurs, but it's not all gloom: mass collaboration benefits us all through valuable innovations.**

Actionable advice:

**Collaborate to draw attention to your company.**

If you ever feel that your company is not being innovative enough or doesn't get enough attention from consumers and the media, then you should consider finding ways to collaborate with other companies. By harnessing the power of the global community you're more likely to find innovative solutions to your specific problems and rekindle your company's relationship with consumers.

**Team up with other companies and share in the profits.**

If you have a great idea that you think will be groundbreaking, don't hoard it for yourself! Go online and find people with similar ideas and collaborate with them. Doing so will help you to further develop your great idea, as well as share the cost burden of R&D, thus ultimately making your new product even more profitable.
---

### Don Tapscott & Anthony D. Williams

Don Tapscott is a Canadian business executive and consultant who has written more than 15 books, including the best-selling _Paradigm Shift_.

Anthony D. Williams is a consultant and researcher as well as the vice president and executive editor at New Paradigm.

