---
id: 57f8b322dbad80000386b329
slug: will-it-fly-en
published_date: 2016-10-11T00:00:00.000+00:00
author: Pat Flynn
title: Will It Fly?
subtitle: How to Test Your Next Business Idea so You Don't Waste Your Time and Money
main_color: 2EA0E9
text_color: 1F6B9C
---

# Will It Fly?

_How to Test Your Next Business Idea so You Don't Waste Your Time and Money_

**Pat Flynn**

_Will It Fly_ (2016) is a guide to testing your business ideas and achieving the smoothest possible flight to success. These blinks will help you critically examine the validity of your ideas, research your market and get to know your future customers.

---
### 1. What’s in it for me? Get your entrepreneurial idea ready for take-off. 

Say you're thinking about starting your own business. You have an innovative idea and want to build a business around that idea. But, more importantly, you want it to succeed; you want it to defy gravity and soar toward the stars.

But before an idea can pick up speed and really take flight, you first need to get it off the ground — and that's where these blinks come into the picture.

From aligning your business brainchild with your personal goals to making your plans financially viable, these blinks will make sure that your brilliant idea is ready for take-off.

In these blinks, you'll learn

  * how to identify your strengths;

  * why you probably shouldn't create a website for food trucks if you don't own a food truck; and

  * how to speak the language of your customers.

### 2. Becoming a successful entrepreneur means creating a business that’s in line with your lifestyle goals. 

What's the first order of business for any young entrepreneur before starting her business? Is it getting funding? Or maybe printing business cards? Actually, the first step involves going much deeper; the entrepreneur needs to investigate how her idea will support her desired lifestyle.

Before turning any idea into a successful business, it's essential that you make sure it's aligned with your values and goals. After all, you're going to put your time, energy and money into this project and the closer it comes to matching your personal goals and values, the more satisfied you'll be.

So, if you're a people person, make sure that's reflected by your business's social environment, and be sure to create a space that will let you nurture this aspect.

But how can you see to it that you'll make decisions about your future business that will leave you satisfied? Well, you can simply try interviewing your future self.

Self-examination will help you decide what type of business best suits you — and it's easy to do:

Grab a piece of paper and divide it into four sections. Then, identify the four most important parts of your life. Your categories might be, for example, Family, Career, Well-being and Money.

Next, assign each of these categories to a different quadrant on the paper. Once you've done that, imagine yourself five years down the road and write down all the things that come to mind.

For example, under the Money category, you might write, "in five years my mortgage is paid off, I've invested in property, have a steady income from rentals and my kids' college funds are squared away." If, during this process, you start to think that your idea won't generate enough money to make this scenario realistic, set aside time right now to improve it. This way, you'll avoid making this disappointing discovery in five years.

All this talk of the future might have you itching to get to work on your business. But first, you'll need to identify a few more personal things to ensure your idea is right for you.

### 3. Successful entrepreneurs spot their advantages by looking to the past. 

Is there one thing that you're exceptionally good at? If so, you should learn to use it to your advantage! Your unique skills and characteristics give you a competitive edge, a niche from which to operate — in other words, an _unfair advantage_.

Such an advantage is kind of like your superpower because it makes you stand out from the competition; it should thus be an integral aspect of any new idea. In fact, the mere knowledge that you're good at something will help motivate you.

For instance, imagine that you've been teaching yoga for 15 years and just had the idea to open your own online yoga school. Over the past decade and a half, you've amassed a huge amount of hands-on knowledge on different schools of yoga and a variety of techniques. Naturally, you'd want that unique knowledge to serve as the foundation for your online school.

But perhaps your strengths aren't so obvious, in which case you'll need to identify them. One way to do so is to look at your past.

By looking at your previous jobs and volunteer experience, and considering what you liked about them or didn't, you can uncover interesting patterns about yourself. The author, for example, used to work for an architecture firm. He liked the job because he learned something new every day and got to work on something physical and tangible; after working on a project, he could actually walk through it.

However, he didn't feel that he was paid enough or acknowledged for the hard work he did. He also felt that his ideas were not heard. Looking at his past, it was clear that the ability to influence outcomes was, and continues to be, very important to him. Therefore, becoming an entrepreneur was a natural fit, as it would put him fully in charge and so play to his strengths.

### 4. Map your idea and distill it to its essence. 

So, you've traveled into the future and back to the past. You know what you want from life and have some ideas about what you want to do; now it's time to work on your business idea.

For starters, jot down any thoughts that relate to your idea in a _mind map_, a useful tool for structuring your thoughts in a visual representation. You can design yours either on paper or electronically.

If you prefer writing by hand, post-its make the perfect medium because you can easily move them and use different colors. Or, if you'd rather use a computer, opt for a web-based mapping tool like MindMeister that allows you to drop and move items easily.

And remember, don't overthink it! Just be sure to get all of your thoughts out in the open. After all, our brains can't be creative while editing. For now, focus on getting your ideas flowing.

The next step is to distill your idea into a single cohesive sentence that helps you pare it down to its essence. You kept your analytical brain on silent mode during the creative phase, but now it's time to switch it back on. Start editing by organizing your mind map into different categories. Then, once you have categories, write 400 to 500 words that cover them all.

This will get you into the flow of writing, and from there you can cut your first draft down to a three- to five-sentence paragraph. Be sure to cap it at that and make sure it's easy for a potential customer or investor to understand.

Finally, edit your idea into one sentence and don't worry if it takes a few versions to find the one that's perfect. For example, a food retailer might describe his site by saying it offers "a wide selection of organic, raw vegan foods, while providing excellent customer service and support. The site is also home to a recipe blog containing short, entertaining instructional videos on how to cook with the items available on the site."

### 5. Share your idea, ask for feedback and research your market. 

OK, now your idea is in a shareable format, but before you launch your business, it's essential to research how the world will respond to it.

Your idea will be improved by sharing it and learning from the feedback. In fact, even casual chats at work or at home can lead to valuable feedback that can help you refine your concept. But you must also be sure to speak with potential clients.

For instance, the author had the idea to build a website for people who want to run food trucks and started contacting people who already had food truck businesses. The food truck owners immediately asked him if he owned a food truck — and he would reply that he didn't.

This experience forced him to think more closely about his idea and why somebody would take his advice if he wasn't in the business. Eventually, this reflection led him to reshape the idea entirely, branding himself as a curator of a community for food truck owners.

The next step is to determine how your market currently operates and attempt to add a new dimension to it. A good tool for doing so is a _market map_.

To make one, just create a spreadsheet with different pages for _places_, _people_ and _products_. In the case of an online yoga school, you would start your market map by adding places like yoga studios and existing online schools.

From there, write down the names of people you intend to contact from the different schools. These personal conversations can spark unique human contacts that become the basis for future cooperation. You might end up wanting to feature a certain school's instructors in your online offer, and having a useful contact will make it a whole lot easier.

Finally, when it comes to your products sheet, you should focus on the individual items you'll offer — in this case, different styles of yoga.

### 6. Understand your audience’s issues and how they describe them. 

Conducting research can teach you a lot about your potential market, but it's also crucial to know how to care for your clients. After all, they're the ones who will be buying your product.

To do so, you should develop an actionable customer profile, also known as a _Customer P.L.A.N_. This tool will help you identify how your business idea matches up with your audience; in the end, your business will do much better if you understand your customers in all of their complexity.

Here's how it works:

Your customer PLAN stands for your customers' _problems_, _language_, _anecdotes_ and _needs_. To get started on yours, add a page to your spreadsheet called P.L.A.N. and make four columns, one for each of the above categories.

Now, turn your attention to the first letter in your P.L.A.N., your customers' _problems_ — remember, if you can solve a problem, people will buy your products. Luckily, learning about your customers' problems is easy: just get in touch with them.

Start with the names you jotted down in the people section of your spreadsheet and ask them open-ended questions that will help identify potential issues. For example, you might ask questions like "what's the most difficult thing about . . . ?" or "which part of that task is the most time consuming?"

If you find people are hard to contact personally and your perseverance doesn't pay off, try doing an online survey. In this case, a great question to ask is "what do you find most challenging about . . . ?"

Once you've identified your customers' problems, it's time to move on their language. After all, understanding the way your customers speak will open up new doors to your target audience.

A solid strategy here is to analyze your customers' frustrations through the lens of language. For instance, make note of certain words they use to describe their struggles, goals and aspirations. And remember, speaking like your customers will build trust faster and make them more likely to respond positively to future advertising.

You've now got half your PLAN ready to go. Next up, you'll learn about the two final ingredients.

> "If you can define the problem better than your target customer, they will automatically assume you have the solution." - Jay Abraham

### 7. Strengthen your idea by collecting customer anecdotes and tuning into their needs. 

Now that you know how to identify the problems your audience faces and the language they use, it's time to consider the _anecdotes_ that will help you stay connected to real people. Here's how:

Anecdotes are stories, usually short, funny or interesting ones, and you should always encourage your customers to share them. These little bits of information will help you tailor your idea to your customers' experience.

Take this anecdote: For the first time in his life, Nick went fly fishing and shared his awesome adventure online. After traveling for a couple of hours, he finally reached the fishing area and, upon arriving, he stopped at the bait shop to ask for fishing spot recommendations.

Someone at the shop offered to guide him and led him to a good spot. When they started fishing, Nick was really enjoying the scenery but was having a hard time casting. A more experienced fly fisher noticed Nick's difficulty and offered him a few pointers, after which Nick drastically improved.

So, if you're designing a product for beginner fishermen, listening to such a story would be a great way to relate to a potential customer. You can learn about their struggles, understand them better and make a product that helps solve these issues.

And finally, identifying your customers' _needs_ will help you fully satisfy them with your future product. But what exactly is a need?

Essentially, a need is an aspect of a customer's life that doesn't work — and your job is to offer a solution. For example, while researching fly fishing, the author found that fishermen often complain of not being able to tie flies quickly. By talking to customers, he discovered that they absolutely hated losing opportunities to catch other fish.

In the process, he identified two needs: improved fly tying techniques and simplified access to flies. Once he'd done so, he was ready to come up with ideas to fulfill both those needs.

### 8. Smart entrepreneurs test the commitment of their potential customers and investors. 

You've done your market research and created a customer P.L.A.N. You're almost there! But before moving forward, remember to validate your idea — financially, that is.

You can start by performing initial testing of the commitment your product or service triggers among potential investors and customers. There are a several ways to do this.

For one, you can try advertising platforms like Google AdWords, Facebook, Twitter or any other website that your target audience visits. These sites will compound real statistics, including the number of clicks your ads generate, that will give you a sense of the initial interest in your idea.

Another way to validate your concept is to write guest blog posts on an open forum or group related to your idea. This works well because if you offer interesting content without asking anything in return, you'll add value to the platform and grab people's attention. Over time, you can begin presenting your product or service on one of these platforms and ask people if they're interested in investing.

A final validation strategy is called _hyper-targeting_, which involves identifying the percentage of the market that's potentially interested in your solution. In other words, it works by asking your audience to identify as wanting or needing your product or service.

So, if you've been active on an online forum or group, just pose a question like "who would be interested in . . . ?" Or, if you've got a website, you might ask viewers to click on a link or subscribe to a newsletter. By doing so, you'll get a sense of how many people could be interested in your idea.

And of course, you can also do this through public speaking. In this context, you can simply ask people to raise their hands if your product would solve a problem of theirs.

### 9. Final validation comes through interacting with potential customers and inviting them to invest. 

So, measuring the commitment people have to your idea is essential — but people often won't buy a product even though they showed interest in the idea initially. As a result, it's necessary to do a final round of validation.

First, interact with your potential customers and share your solution with them to make sure they'll actually buy your product in the future. Here, it's best to shoot for one-on-one interactions whether they're in person, over the phone, through private messages or by e-mail.

Regardless of the communication method you use, begin by showing interest in your customer. Then, try to focus the conversation on the product you want them to buy into. Share information about yourself and use an anecdote to help them understand why you want to serve them.

But you should also remember to be honest and let the customer know that you're interested in their feedback. In the end, receiving such feedback will let you know if a potential client likes your product and also how likely they are to buy it once it's on the market.

Then, your final validation comes when you ask for payment as a pre-order. This could be done through a link on your website or sent out through an e-mail that asks customers to buy your product in advance. When using e-mail with this strategy, wait 24 hours and, if you haven't heard anything, send a follow-up e-mail to see if the person indeed received your message.

But how many pre-orders do you need for your idea to be validated?

Well, according to the communications scholar Everett Rogers, just 2.5 percent of people will invest in a new idea right away. Naturally, you want more people to buy your product as time goes by and curiosity builds, but Rogers suggests that getting 10 percent positive responses is a good indicator of success.

In other words, if you contact 50 people and five of them commit financially, you're good to go. If, however, you don't get the 10 percent you're looking for, ask for more feedback and keep improving your idea.

### 10. Final summary 

The key message in this book:

**Plenty of people are full of good ideas, but not many of them are turned into successful businesses. The keys to doing so are market research, an understanding of your clients' problems and a rigorous financial validation of the final idea.**

**Actionable Advice:**

**Call up friends and colleagues.**

If you're having a hard time identifying your personal advantages, it can help to get in touch with some friends or colleagues. Just send out some emails asking people what they find special or interesting about you; their answers might end up revealing a set of core life values you want your business to align with.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Sprint_** **by Jake Knapp, John Zeratsky, Braden Kowitz**

_Sprint_ (2016) is a guide to start-up success, broken down into a five-day plan that lets you test new ideas and solve complex business problems. These blinks give you everything you need to move quickly from an idea to a prototype and, ultimately, make a decision on whether or not to launch.
---

### Pat Flynn

In his professional career, Pat Flynn made the journey from a 9-to-5 job to successful entrepreneurship. Now he advises others on how to build their own successful and ethical online businesses through his website and podcast, SmartPassiveIncome.com.

