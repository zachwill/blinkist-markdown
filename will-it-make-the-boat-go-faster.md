---
id: 5694b2652f68270007000024
slug: will-it-make-the-boat-go-faster-en
published_date: 2016-01-14T00:00:00.000+00:00
author: Ben Hunt-Davis and Harriet Beveridge
title: Will It Make the Boat Go Faster?
subtitle: Olympic-Winning Strategies for Everyday Success
main_color: A7535A
text_color: A7323C
---

# Will It Make the Boat Go Faster?

_Olympic-Winning Strategies for Everyday Success_

**Ben Hunt-Davis and Harriet Beveridge**

_Will It Make the Boat Go Faster?_ (2011) shares with you the inspiration and strategies you need to succeed every day, as told through the emotionally charged experiences of a member of the gold medal-winning British rowing team at the 2000 Summer Olympics in Sydney.

---
### 1. What’s in it for me? Follow in the footsteps of Olympians and become a champion. 

Becoming an Olympic champion is the highest achievement in the world of sports. But how many of us actually can achieve this goal? Just a few, but this doesn't mean that it's impossible.

So how do Olympians reach their goals? Well, you're about to find out, as these blinks share the inspiring strategies of a gold medal-winning rower from the British team at the 2000 Summer Olympics in Sydney. 

Whether you're an athlete or entrepreneur, the lessons the team learned on its path toward gold can help you realize your own goals. In these blinks, you'll discover how to break down your goals to make them more attainable and find the motivation to make it to the very end.

In these blinks, you'll learn

  * how one lofty goal can be separated into four different layers; 

  * why daydreaming is never a waste of time; and

  * which three steps to follow when the going gets tough.

### 2. Olympians divide goals into four layers: the crazy, the concrete, the control and the everyday. 

If you want to be a winner, in life as well as in sports, setting clear goals is essential. A clear goal can focus your energy and help you learn and evolve as an individual. 

So how do the best of the best — Olympic champions — set goals for themselves?

Olympians split goals into four layers: the _crazy layer_, the _concrete laye_ r, the _control layer_ and the _everyday layer_. 

The crazy layer is where you can be bold, even excessive. These are the goals that light up your imagination, that don't belong on a mundane to-do list. The author's crazy layer goal, for example, was to win the gold medal in rowing at the Olympics!

Yet to support your crazy layer, you need to provide a structure and a way to measure progress.

The concrete layer helps you do this. For example, you can turn your crazy layer goal of winning Olympic gold into a concrete layer goal with specifics: aiming to row 2000 meters in five minutes and 18 seconds. Considering this is a world record pace, your dreams of gold may just come true!

While the concrete layer should be precise and clear, not every element in this layer is under your control. The control layer thus helps you identify exactly what you can control when working toward success. For example, you certainly can't control the weather nor the speed of your competitors, but you _can_ control how much you practice. 

Finally, the everyday layer outlines the steps you need to take on a daily basis that are vital to achieving your crazy layer goal. For example, on Mondays you could complete 20 bench presses in five minutes. On Tuesdays, you could make sure to row with teammates for at least one hour, and so on throughout the week. 

This layered approach is the best way to take practical steps toward your highest goals.

> _"Being clear on what we wanted to achieve meant we could always figure out the way ahead by asking, 'Will it make the boat go faster?'"_

### 3. To stay on track you need to make the path to your goal entertaining; don’t forget to daydream! 

Ever tried sticking to a restrictive diet? Too often dieters find it easy to throw in the towel and raid the refrigerator, hungry and frustrated. 

Why is this the case? Giving up the things you enjoy eating can make you feel miserable. Therefore you need to find a path toward your goal that _entertains_ you, one that you genuinely enjoy.

Because if your goal doesn't excite you, your chances of reaching it are low. You might find your motivation plummeting when you're only halfway there!

Some of the author's friends wanted to get fit, but knew they'd have to make that goal more exciting if they really wanted to stick to it. So instead of going to a regular gym, they signed up for pole dancing classes. It worked — they got in shape and learned a new skill to boot — all because it was fun.

After you've found a fun path toward your goal, start _daydreaming_. Daydreaming can help you strengthen your desire for and belief in your goal, and it also allows your mind to form an emotional and rational attachment to the goal.

The author's repeating daydream was seeing himself standing on an Olympic podium, receiving a gold medal. This vision kept him training hard — and although it took ten years, he was eventually able to live his dream!

You should also set measurable, transparent _milestones,_ which will stoke your motivation along the way. Knowing how far you've come and how much is left can give you a sense of achievement and forward motion. This, in turn, will help you continue your training. 

When the author had to row 20 kilometers on a rowing machine, for example, he knew that it would take a while and he might lose motivation. To keep his mind on his goal, he would remark to himself each significant milestone as he rowed. "Okay, that's 10 kilometers done, only 10 more to go…"

### 4. Great teams set common goals based on mutual desires, measurable milestones and fair rules. 

Have you ever worked in a team in which each person was pulling in a different direction or chasing a different goal?

A group of people is not a team unless all members share a common goal, grounded in mutual desires and measurable milestones. 

The author's rowing team, for example, wanted to win an Olympic gold medal. While doing so might have meant different things for each team member, that didn't matter — because they all wanted that medal.

The team's collective goal also included measurable milestones. Specifically, the team set out to win regional regattas, pass required ergometer tests and clinch the world championship by rowing 2000 meters in five minutes and 18 seconds.

To be a well-functioning team, every person in the team must know how to behave with each other and agree to a clear set of rules for behavior in general. Any unspoken or vague rules can be toxic.

These rules should be discussed openly, and all members should have a say in how they are developed.

One senior manager at multinational bank HSBC was known for turning around the worst-performing regional offices because he was so good at setting clear team rules.

First, he would gather managers together and get them to agree on what they wanted to achieve and how exactly they would do it. Even if there were 200 managers in an office, he would sit them all together to figure out a common goal. Once the goal was established, the group would come up with plans on how to get there.

For a team to work well together, each member must have a deep knowledge of all the other members — of their strengths, weaknesses, habits and so on. This way, team members will better understand how to communicate, how to adjust any approach toward collaboration and bring out the best in each individual.

### 5. To achieve your goals, turn your attention to your process, not just your end results. 

You've no doubt had to deal with an irritating salesperson, someone so focused on getting you to buy something that he completely disregards what you've said you actually want. 

Why do such salespeople talk so much and listen so little? Because their focus is on selling and not on the _process_, which is where it should be.

You can start to focus on your own process by being _curious._ Ask yourself what exactly will help you achieve your goal and what will keep you from it. 

The author's coach, for example, started by observing other teams. He drew a comparison between competing teams' and his own team's performance. He then talked to his team to identify precisely what they needed to be winners. 

You also need to sharpen your _attention_. By doing so, you'll know what exactly needs to be done to achieve your goal, which will improve your performance and bring about positive results. 

One management trainer wanted better client feedback. He discovered that the key to achieving this was through improving the quality of questions he asked the group during training sessions. By sharpening his attention on this element of his practice, his questions improved and so did his feedback scores.

Lastly, you should be sure to analyze the outcomes of your process. Success should be measured by how well your process works, and not just by your end results. This will encourage improvement, even when the going gets tough. 

For instance, while some rowing teams calculated success by the numbers of regattas they won, the author's team measured their success in terms of how good their process was — that is, did their performance match up with what they had planned to do? A focus on process so obsessed the team that when they finally won gold, they barely even registered the victory!

### 6. Don’t let setbacks or problems kill your motivation or momentum. Take a breath and move forward! 

Sometimes no matter how hard we try, things still fall to pieces. So how can you plan for the unexpected? Olympic champions follow three important steps.

The first step in preparing for potential problems is to identify your weaknesses. Take this discussion to your group as well, so together you can identify areas that you might have missed on your own.

Before the Olympics, the author's team had a lot of "what if?" conversations. They considered potential setbacks and came up with the necessary preventive actions.

After you've identified your weaknesses and problem areas, you still need to remember that when something goes wrong, you should accept it and understand that it too shall pass. 

Not every problem can be predicted or controlled. When things do go sideways, just take a deep breath and figure out an alternative path.

Say you're on your way to an important meeting but are stuck in a traffic jam. Can you personally fix this problem? No, you can't! So instead of flipping out, accept the fact that you're now running late.

Your goal, however, is still in reach. You can call the office to postpone the meeting, and discuss things with your colleagues tomorrow.

The third step is to reflect on and learn from setbacks — and then let them go. Even a short period of reflection can help you avoid bigger errors later on. Think about what you can do to prevent the same thing happening again!

If you allow temporary setbacks to ruin your motivation and momentum, you end up not only losing the battle but also the war.

### 7. To take the right kind of risk, unskew your scale and clear your mind of other people’s opinions. 

Facing a tough opponent is challenge enough. So it's essential that, at the starting line, you're not also fighting against yourself!

Get your thinking straight. To do this, you need to _unskew your scale._

We all like to think that we're rational people, carefully weighing options before committing to any decision. But the reality is that we're skewed toward not taking _any_ action whatsoever when faced with risk. 

The author's team entered every regatta with the knowledge that they could lose. If they had let this risk overcome them, they would never have even participated!

To succeed you need to unskew this thinking. You need to rid yourself of irrational thoughts by asking yourself what the downside of _not_ doing something is, as opposed to the risks of doing it.

To take the right kind of risk, it's also important to rid your mind of other people's opinions.

It's natural to look to others to guide your own behavior. However, we often mistakenly think that doing what everyone else is doing is the same as taking less risk.

For instance, the predominant strategy at the Yingling Sailing race is for teams to try to win every race. So teams push on at any cost, including suffering penalties or disqualification for infringing the rules.

During the Olympics, however, the British team went against the grain. The team tried out a different strategy, one that focused on receiving zero penalties — and this is what helped them secure the gold!

### 8. Final summary 

The key message of the book:

**Whether you want to win a gold medal at the Olympics or succeed in business, you have to attack your goals in the correct fashion. This means planning carefully and strategically, working your hardest, anticipating problems and, if you're in a team, understanding one another well.**

Actionable advice:

**Think about what's riskier: going for it or** ** _not_** **going for it?**

Not all risks are the same. And the types of risk you should always seize are those with such a huge payoff, like winning Olympic gold, that you can't afford to let them pass. So rather than stewing about everything that could go wrong, think about what it would cost you to _not_ go for it.

**Suggested further reading:** ** _The Sports Gene_** **by David Epstein**

_The Sports Gene_ takes a look at the physiological traits that are beneficial in various sports, and at their hereditary background. It also examines why people in certain parts of the world have evolved in their particular way, and how this is beneficial in the realm of certain sports.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ben Hunt-Davis and Harriet Beveridge

Former British rower and Olympic champion Ben Hunt-Davis is a keynote speaker, performance coach and a director at Will It Make the Boat Go Faster, a business development company.

Harriet Beveridge is an executive coach who has worked with companies and organizations such as multinational bank HSBC, cable television channel MTV, the National Health Service (NHS) and American conglomerate 3M. Beveridge is also a comedian and runs the site HelpfulHumour.com.

