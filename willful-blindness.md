---
id: 59dce465b238e10007fea0a5
slug: willful-blindness-en
published_date: 2017-10-11T00:00:00.000+00:00
author: Margaret Heffernan
title: Willful Blindness
subtitle: Why We Ignore the Obvious at Our Peril
main_color: C54045
text_color: C54045
---

# Willful Blindness

_Why We Ignore the Obvious at Our Peril_

**Margaret Heffernan**

_Willful Blindness_ (2011) is about a common phenomenon through which humans block out the uncomfortable realities of the world. These blinks explain how and why people so often fail to see what's right in front of their noses and outlines the steps we can all take to open our eyes to the truth.

---
### 1. What’s in it for me? Overcome your willful blindness. 

If you've ever worked in an office, you've probably experienced how easily communal areas, like the kitchen, can get out of hand. At the end of the day, the sink is full of dirty dishes and the fridge is overflowing with leftovers that no one will ever touch again. Why does this happen?

The culprit is _willful blindness_, a state in which we choose not to see what's right in front of us. While a messy kitchen at work might amount to a minor hassle, willful blindness, both on a personal and a collective level, can lead to large-scale disasters.

So how does willful blindness work and how do you overcome it? You'll soon find out.

In these blinks, you'll learn

  * why no one saw the housing crisis coming in 2007;

  * why you only see naked people at the beach if you want to see them; and

  * why one of India's biggest employers speaks to his driver every day.

### 2. People tend to ignore uncomfortable facts and divergent views. 

In law, _willful blindness_ refers to the principle that a person is responsible for an action if he could and should have known something was problematic, but decided not to see it.

For instance, imagine a drug trafficker whose stash gets discovered at the airport. To avoid jail time, he might say that he was never told what was in the packages he was carrying and didn't have any intention of breaking the law. He'd be out of luck, since the law isn't interested in the _reasons_ you were blind to a reality, only that you chose to be, in this case by not watching your luggage.

That being said, willful blindness applies to much more than crime. In fact, it's common for humans to adopt such a position in a variety of situations, often with disastrous consequences. Just consider the housing crash of 2007. Virtually nobody saw it coming and willful blindness is to blame.

In the years leading up to the crash, the US housing market was expanding at a shocking pace; people with little to no income were buying lavish homes with small or nonexistent down payments. Since everybody was doing it, nobody imagined that anything could be going wrong.

Meanwhile, the financial sector was producing ever more complex financial instruments, like the credit default swaps and derivatives that eventually brought down the market. However, all of these investment vehicles were premised on the illusion of endless market expansion. People really should have known better. So, why didn't they?

It all comes back to a single human impulse: the unconscious urge to surround ourselves with people who think the same way that we do. After all, challenging our views is uncomfortable. It's much easier to reduce our exposure to people with different ideas and values.

In the case of the housing crash, the people who were pointing out the dangers had a difficult time making themselves heard or backing up their case because nobody wanted to hear a dissenting view.

### 3. We’re not only blinded by love, but by ideas too. 

Love is one of the most powerful emotions a human can experience, but it also bears inherent dangers. After all, to love someone is to view them through a rose-tinted lens that necessarily hides their flaws. As a result, love can obscure the true nature of another person, leading to potentially disastrous consequences.

For instance, some women will fail to notice when their partners are abusing their children. Only after such facts become undeniable, say through a police investigation, do the small signs of abuse become impossible to ignore. The willful blindness that obscures such signs is often fueled by a variety of fears, like losing the breadwinner of a household or challenging assumptions about a partner.

In fact, we'll often fight to maintain the view we hold of a partner even when such views are clearly delusional. Our own identity can depend so much on what the other person thinks of us, that once we find out something identity-shattering about the other person, our own identity is shattered along with his or hers.

To put it differently, love can blind us — and so can ideas. This all boils down to the fact that the human brain doesn't like conflict. As a result, we actively disregard any facts that might disprove our previously held assumptions.

Alice Stewart offers a good example. In the 1950s, she discovered that x-ray exposure during pregnancy is linked to higher rates of leukemia. However, at the time, it was unthinkable to medics that low doses of x-rays could have any harmful consequences. Because of this assumption, people assumed Stewart's findings were incorrect. They did everything they could to maintain their beliefs and disregard the evidence. Nonetheless, after many years of dispute, the facts could no longer be avoided.

Behind this refusal to believe something patently obvious was what the psychologist Anthony Greenwald called the _totalitarian ego_. It looks away from threatening or incompatible ideas, even going so far as to rewrite history to preserve one's self-image. This psychological impulse functions like an internal police state that leads us toward willful blindness.

### 4. It’s easy for people to ignore dangers, especially when in a group. 

Have you ever seen an ostrich burying its head in the sand, or at least a picture of one doing so? While this phenomenon is often characterized as something ostriches do to feel safe or hide, it is actually a simple optical illusion. Since ostriches' heads are quite small for their body size, it can sometimes look like they are burying their heads when they are merely poking around in the sand for food.

But while these big birds might not hide from reality, humans do all the time.

For instance, in Nazi Germany, people who lived in close proximity to concentration camps claimed ignorance of the horrific crimes taking place in their own backyard. While the carnage was obvious, these people refused to see it; reality was simply too horrific and they couldn't stand to acknowledge what was happening.

Or, consider an everyday example. Many people are oblivious to the dangers of working too much and sleeping too little. However, research has found that a single night without sleep has equivalent physiological effects to reaching a blood alcohol level of 0.1 percent, high enough to make it illegal to drive in many places.

Research done by Charles Czeisler, a professor of sleep medicine at Harvard Medical School, even found that the chances of hospital interns stabbing themselves with a needle or scalpel increased by 61 percent following a 24-hour shift. Nonetheless, most people don't complain. Unwilling to see the dangers they face, they just keep working.

In other words, it's easy for us to bury our heads from the realities of life. But this willingness to blind ourselves increases even more when in a group setting. For example, experiments have shown that, if a person notices signs of a fire while alone in a room, she will react within seconds.

However, when a group of people is in the same situation, their collective reaction time can be much slower. As a result, a person in a group might see smoke and ignore it since nobody else is reacting.

Why?

Well, when we find ourselves in the company of others, it becomes important to us how we're perceived. This causes problems because, if nobody else is reacting to a stimulus, any reaction might cause conflict in the group. As a result, instead of saying something, most people simply stand by.

### 5. Obedience and conformity drive humans to ignore the truth. 

Imagine you're a soldier who has been given an order that, if carried out, would take thousands of innocent lives. Would you blindly obey?

Even if you're a highly moral person, you'd have to fight the natural human impulse toward obedience, a fact that regularly blinds people to their moral responsibility.

For instance, during the Iraq war, US soldiers stationed in the Iraqi prison known as Abu Ghraib were ordered to "soften up" the prisoners. A massive scandal of abuse and torture followed, since in response to the command, the soldiers, along with CIA personnel, executed a series of horrific human rights violations, including rape, torture and murder.

In this situation, obedience to a vague order resulted in willful blindness; the soldiers were incapable of seeing the moral implications of their actions and simply followed orders.

Related to such a proclivity for obedience is the human drive toward conformity, or the adoption of the views, habits and routines of our peers. For instance, during a famous 1950s experiment, the psychologist Solomon Asch gave groups of eight college students, each of which contained only one real test subject, two cards.

The first card showed a vertical line of a certain length, while the second one had three vertical lines, of which one was the same length as that on the first card, one was visibly shorter and one was visibly longer.

The subjects were then asked which of the lines on the second card matched that on the first. The real subject was always made to answer last, while the seven "actors" in the experiment had been instructed to give the same incorrect answer.

In the end, around 35 percent of the answers given by real subjects conformed to the incorrect answers given by the actors. Even though the subjects must clearly have known the correct answer, their drive to conform to the group was so strong that they turned a blind eye to the truth.

> _"You had to keep dancing. Why wouldn't you? It was fine as long as everyone else kept doing it too."_

### 6. Distance and the division of labor can lead to willful blindness. 

As companies expand from fledgling start-ups to major corporations, the resultant growing pains almost inevitably include a sense that things are beyond their grasp. After all, suddenly having new teams and offices around the world, each of which specializes in specific areas of business, is a recipe for losing control that can be bad news for businesses.

In fact, businesses that are spread out across the globe but also work to maintain central control can be blind to the dangers inherent in their various operations.

Just take the Deepwater Horizon oil rig, which operated in the Gulf of Mexico. On this deep-sea oil-drilling platform, serious risks were routinely overlooked, eventually resulting in a major catastrophe. After a fire erupted on the platform, the whole rig sank, leading to an oil spill of titanic proportions.

But how was this allowed to happen?

Well, the platform was under the management of the offshore rig operator Transocean, but leased by the oil giant British Petroleum, or BP. Overseeing the project from London, BP was oblivious to all kinds of problems. There was the potential for gas to leak into the drilling area, a serious fire risk, as well as the inherent danger posed by their lack of an adequate fire plan.

Moreover, siloed businesses can also result in willful blindness. So, while lots of companies claim to have a bird's-eye view of every aspect of their operation, many pieces of the puzzle are routinely ignored.

Take the trendy aluminum water bottle company SIGG as an example. This Swiss firm promoted its products as free of bisphenol A, or BPA, a synthetic compound used in plastic products that can lead to diabetes and erectile dysfunction.

But despite the company's claims, the subcontractor that produces the linings for SIGG's bottles was indeed using the toxic substance. They hadn't been honest with SIGG about this fact and the company had never asked, choosing instead to remain willfully blind.

Eventually, when it came out that SIGG bottles contained a small amount of BPAs, the company was publicly tarnished, and the CEO lost his job.

> "Once you outsource critical functions, you may be blind to how the work gets done."

### 7. You can battle willful blindness in your life and throughout the world. 

Have you ever heard the story of Cassandra?

Her father was an ancient king, gifted in the art of prophecy. But unfortunately for Cassandra, her fate was such that nobody believed a word she said. Because of this tragic combination, when she warned the Trojans about the dangers of the infamous wooden horse, nobody listened. While Cassandra's story may be an ancient Greek myth, there's plenty to be learned from it — especially for whistleblowers.

These brave souls fight an uphill battle against willful blindness, pushing others to see the truth. Just take Chelsea Manning. She leaked top secret US Army documents regarding operations in Iraq and Afghanistan to Wikileaks, sparking a massive scandal about the ethical dimensions of these wars.

The most powerful information she leaked was a video depicting a 2007 US Apache helicopter strike in Baghdad that killed several people, including a Reuters journalist. Following the video's release, a global discussion emerged about the ethics of this strike and the war in general.

By making information about the conflict publicly available, Manning forced people out of their willful blindness surrounding the terror of the Iraq War.

So, while not everyone is ready to be a whistleblower, you can challenge your own willful blindness in other ways. It may sound hard, but it really just involves overcoming the drive toward homogeneity.

In other words, if you find yourself agreeing with almost everything and everyone around you, you should work to open yourself up to new ideas and perspectives. To do so, you can take a tip from one of the biggest employers in India, Ratan Tata.

When his driver takes him to work in the morning, Tata doesn't hide behind a newspaper like many CEOs do. Rather, he sits in the seat next to his driver and speaks with him, listening to his problems and thoughts.

Through this process, Tata receives valuable insight and a different perspective that help him actively combat his willful blindness. You can be like Tata and find ways to challenge yourself simply by seeking out as many views as possible.

### 8. Final summary 

The key message in this book:

**Intentionally blinding yourself to reality isn't always bad, but as a general approach, it can be dangerous. In fact, many of the challenges humans face today could be resolved much more easily if people were only willing to open their eyes and see them. By seeking out other perspectives and questioning the world around us, we can avoid falling into complacency and conformity.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Beyond Measure_** **by Margaret Heffernan**

Beyond Measure (2015) shows that transforming a struggling company into a thriving one is a simple matter of making small systemic changes that empower people to speak up, collaborate and share. Discover you can stop your company from being controlled by one overworked CEO and make it into an innovative powerhouse where ideas can flourish.
---

### Margaret Heffernan

Margaret Heffernan is a CEO, bestselling author, TED speaker and lecturer. Her other publications include _Naked Truth_ and _Women on Top._

