---
id: 544d63383630630008200000
slug: willpower-en
published_date: 2014-10-28T00:00:00.000+00:00
author: Roy F. Baumeister and John Tierney
title: Willpower
subtitle: Why Self-Control is the Secret of Success
main_color: CFC065
text_color: 827526
---

# Willpower

_Why Self-Control is the Secret of Success_

**Roy F. Baumeister and John Tierney**

_Willpower_ brings the concept of self-control back into mainstream discussions on achievement. It takes contemporary scientific studies and breaks them down, demonstrating that, despite the previously held views of many experts, willpower can indeed be harnessed and strengthened to make the changes in your life you've always wanted to make.

---
### 1. What’s in it for me? Harness your willpower and achieve your greatest goals. 

The concept of willpower has undergone a number of fundamental transformations throughout history. From an austere personal commitment in Victorian times to a quasi-scientific, almost mystical force in contemporary thought, people have conceived the mental fortitude to pursue their goals and its source in numerous ways.

However, modern scientific experiments have finally given us a more complete understanding of what willpower is, how it works, and where it comes from: willpower isn't the mere ability to control your actions — in fact, it's crucial to the mind's functionality, and intimately tied to our brain chemistry.

And with our growing scientific knowledge of willpower, we're learning not only how it works, but also how it fails. Our willpower is imperfect and won't solve all of our problems, but these blinks will offer insights into the ways in which we can use this knowledge to change our lives for the better.

After reading these blinks, you'll know

  * why students who get self-esteem boosts score lower grades;

  * why Asian-Americans with low IQs land better jobs than their European-American counterparts; and

  * why Oprah Winfrey struggles with dieting.

### 2. Willpower is like a muscle that tires with use. 

Imagine that you've just left the gym after a hard workout. Your legs wobble on the way to your car, and your arms are so exhausted that even tugging the door handle feels like a feat of strength. Your willpower works much the same way.

The more willpower you exercise without interruption, the less you'll have available immediately after.

Essentially, we have a limited "supply" of willpower to use in our day-to-day — from choosing broccoli over cookies to being nice to our in-laws. Any of these tasks can contribute to _willpower depletion_.

This was verified in one of the author's own studies, in which participants were placed in a room that smelled of delicious cookies and then divided into groups: one group got to eat cookies, while the other was asked to eat radishes instead.

In the second half of the experiment, participants had to solve geometry puzzles. Surprisingly, those who forwent the delicious cookies and instead ate radishes spent significantly less time trying to complete these puzzles than those who simply ate the cookies: 12 minutes less, in fact.

In other words, using their willpower to resist the cookies left them with less stamina available to tackle difficult tasks later.

In addition, our willpower and decision making are intimately tied together — each has drastic effects on the other. For example, many "_deciders_ " — e.g., prime ministers, mayors and other people in positions of power — seem to more easily succumb to carnal temptations and get involved in sex scandals.

This is likely due to decision fatigue. Having to make so many important decisions on a regular basis exhausts their available willpower, and thus makes it difficult to resist temptations.

In the same vein, we tend to make worse decisions when we suffer from mental fatigue. For many this plays out daily, when people come home exhausted from work and find themselves bickering with their spouse instead of finding compromises.

> _"Decision fatigue leaves us vulnerable to marketers who know how to time their sales."_

### 3. You can train your willpower to be stronger, just like a muscle. 

Some people believe that willpower is something we are born with, with a strength and quality that cannot be changed or honed. Not so!

Using your willpower to make positive changes in your behavior can strengthen self-control in other areas of your life. Even minor changes, such as making the decision to say "yes" and "no" rather than "yeah" and nope," can strengthen your willpower and add to your supply.

We can see this principle in action in one study where people who weren't habitual exercisers (but who wanted to improve their fitness) were given a plan for regular workouts.

By following this simple plan in the gym, their willpower improved elsewhere, in both lab, settings, where they performed tasks without becoming distracted by a nearby television, and in real life, where they ate more healthily, did chores, and so on.

When they exercised their self-control by following their workout plan, these study participants also strengthened their willpower in general.

When you exercise — and thus strengthen your self-control — your available willpower becomes less readily depleted, just like muscle.

Take this study, in which students twice squeezed a hand-grip for as long as they could. The experiment was then repeated with the same students after a two-week break, during which one group was additionally tasked with improving their posture.

Surprisingly, this posture-conscious group improved at the hand-grip test, but in an interesting way: they only improved on the _second_ hand-grip of the new test, and not the first. The posture improvement exercises helped to develop their self-control, which meant that they became less mentally fatigued after the _first_ hand-grip than they did during the first iteration of the experiment two weeks earlier.

Think about the implications of this: sticking to a budget might actually help you quit smoking!

> _"Exercising self-control in one area seemed to improve all areas of life."_

### 4. Glucose, not relaxation, is what gives us the strength to control ourselves. 

Psychologists once hypothesized that our willpower could improve after a period of relaxation. This thinking was based on the existence of celebrations such as Mardi Gras, in which people are allowed to indulge themselves before abstaining from their favorite things during Lent.

The idea is that if let rip and indulge, you'll have more willpower available to resist other temptations, such as chocolate or alcohol.

However, this hypothesis proved untrue: willpower isn't linked to pleasure, but to the amount of sugar in the food we consume.

For instance, people who've ingested a tasteless but sugary concoction will perform just as well in subsequent willpower tasks as those who've eaten delicious ice cream, and far better than those who performed leisure activities first, like reading the newspaper.

As we can see, it isn't the pleasure of eating a tasty sugary treat, but rather the sugar itself, which enables your self-control.

Further evidence for this is the fact that people with hypoglycemia, i.e., low blood sugar, are more likely to have difficulties controlling negative emotions or concentrating. Hypoglycemia is also more common among criminals and those with histories of violence.

In 2011, psychology professor Todd Heatherton announced that brain scans and experiments demonstrate that the brain doesn't _stop_ working during willpower depletion. Rather, it simply works _differently_, in fact, much like the brain of a hypoglycemic.

Brain scans show activity shifts between parts of the brain depending on whether your willpower is depleted. Some experts believe that we experience more intense emotions during willpower depletion — the parts of the brain responsible for self-control are less active, while emotional areas are busy.

In essence, no matter whether our genes or our diet are responsible, the less blood sugar we have in our system, the worse we are at exhibiting self-control.

Now that you know where your willpower comes from, the following blinks will teach you how to improve your self-control.

### 5. Self-control starts with setting the right goals. 

We generally direct our willpower toward achieving certain goals. For example, if you want to lose weight (goal), then you'll have to refrain from eating chocolate cake every night (willpower).

However, your success at focusing your willpower on reaching your goals hinges completely on the quality and composition of those goals.

All too often, we don't have enough willpower to achieve our visions for the future because our specific goals conflict with one another, relying on the same resources or motivation.

For example, while many people want to focus their efforts on their families and jobs, these goals are often incompatible, as it's impossible to spend more time with family while simultaneously logging extra hours in the office. There just aren't enough hours in a day.

In contrast, goals like sticking to a budget and quitting smoking are not conflicting, and are, in fact, quite harmonious. You save money by not buying cigarettes, thus keeping your expenses down.

Conflicting goals aren't just harder to achieve, but they also lead directly to _unhappiness_ instead of _action_. Surveys show that people spend a lot of time worrying about how they can rectify their conflicting goals rather than actually pursuing them, and this kind of inaction is an unnecessary source of stress and anxiety.

In order to have enough willpower available to reach your goals, you'll have to make sure they are clearly formulated without being overly specific.

You can gain some inspiration from a group of students who were instructed to make monthly plans about when, where and what they would study. These students earned better grades than those who meticulously planned on a daily basis, and, if they chose to abandon planning altogether, did so much later than the daily planners.

The reason for this is that monthly plans — in contrast to precise, daily schedules — are both more effective and less demoralizing. Planning your day to the second is time-consuming and inflexible. Monthly plans, however, allow leeway.

However, willpower alone is not always enough to get what you want.

### 6. Your willpower will fail, and when this happens, you’ll need alternative strategies. 

Not everyone has an iron will, and even those who do are subject to willpower depletion after strenuous exercises in self-control. Luckily, you don't always need willpower to achieve your goals: alternative strategies, such as developing habits or avoiding temptation, will get the job done just as well.

For instance, rather than using your limited willpower to resist temptation, you can anticipate that temptation and take measures against it. If you know that you won't be able to resist the cool satisfaction and warm buzz from a beer, don't go to the bar.

Another way to keep yourself on the straight and narrow is to make your goals and your progress public knowledge. Take weight loss, for example: by weighing yourself every day or every week and publishing the results on your social networks, you can draw on the strength and motivation from your friends and family if you find your own will failing.

Furthermore, you can avoid constantly draining your willpower by developing positive habits. While it certainly takes willpower to establish a habit, once it becomes part of your routine then you'll no longer need to make an effort to continue doing it.

We can again turn to studious students for guidance: those who perform better are _not_ the ones who have the willpower to pull themselves through all-nighters and all-day study sessions, but rather those who have formed a habit of studying every day.

If you know there is something that you could do every day that would help you to achieve your loftier goals, then do yourself a favor and make a habit of it. Exercising your willpower a little bit now in order to form a positive habit will save you a lot of stress and anxiety later on.

### 7. Religion and other forms of external authority help us develop self-control mechanisms. 

It sounds strange, but you need willpower to develop willpower. But what if you lack the baseline willpower necessary to train it? In these cases, it seems as though believing that a "higher power" is watching over you can have a great influence.

Indeed, religious people seem to have greater willpower than nonreligious people. For instance, religious people are more likely to do things that require self-control, like wearing seat belts, taking vitamins every day and avoiding unprotected sex.

This is because they're influenced by _external control_, the idea that someone — or something — is monitoring their behavior. For example, religious Catholics don't want to be shamed by their community and by their god for having a child out of wedlock, and are therefore less prone to engaging in behaviors that could have those consequences.

External control, however, has less to do with religion itself and more with the fact that someone is scrutinizing you and your behavior. In most religions, this is done by gods and the congregation.

But it's not _just_ religion. In fact, increased willpower is also clearly observable in communities outside religion where there is still external authority. For instance, one explanation for the success of the program Alcoholics Anonymous is that recovering alcoholics face pressure to abstain from alcohol from their new community of recovering alcoholics, all of whom yearn for acceptance.

In essence, appealing to the good graces of your community or a "higher power" by conforming to their wishes is little more than _peer-pressure_. The desire for acceptance leads to changes in your own behavior — hopefully for the better!

### 8. Parents should enforce self-control over self-esteem by being authoritative. 

How many times have you heard that the key to a child's success is his self-esteem? This attitude has been popular for quite a while, but does it hold water?

Even though most Western parents make a concerted effort to consistently bolster their children's self-esteem, there is little reason to believe that this parenting style is justified. In fact, there is no clear scientific evidence that supports the idea that this creates better social or intellectual outcomes for children.

For instance, while self-esteem and good grades are correlated — a fact that is often lauded by self-esteem advocates — self-esteem actually follows from good grades, and not the other way around.

This was shown in a study in which students who had received Cs on their midterm received self-esteem-boosting messages every week. When they eventually took their finals, they scored lower than the control group, and _even worse_ than they had during their midterms!

This self-esteem-oriented child development only creates narcissists, thoroughly convinced that they are better than the rest.

On the other hand, children with more self-control see more success in life. We can find examples of this in Asian-American families, where even those with lower IQs wind up with better jobs than European-Americans with higher IQs because of what psychologist Ruth Chao describes as their culture of strict rules and goals.

While some of the authoritative parenting methods, such as the "Tiger Mother," in which parents hover over their children and scrutinize their every move, are _too_ extreme to be considered healthy, Tiger children are nonetheless notably high achievers.

We can gain some valuable insight from the authoritative approach: parents should set clear goals, enforce rules, punish failures and reward excellence.

The most important aspect of boosting your children's self-control is imposing consistent punishments for their misbehavior. Consistency connects misbehavior to the punishment, and eventually children will learn to better control their general conduct.

> _"Forget about self-esteem. Work on self-control."_

### 9. People with great willpower are not necessarily good at dieting. 

You would think that people who have strong willpower would more easily lose weight than those whose willpower is easily broken. However, it turns out that this simply isn't the case.

Willpower alone doesn't lead to successful dieting. In fact, this was exemplified in one study in which overweight students with high self-control were shown to have only a slight advantage over their peers in a weight-loss program.

Even those people with strong willpower and massive success, such as Oprah Winfrey, relapse into bad eating habits after a diet, gaining weight again.

But why? Shouldn't our willpower give us the extra boost necessary to achieve our weight-loss goals? Well, some things are more powerful than willpower, namely biology:

Human beings are made for durability during times when food is scarce, such as winter or famine. To compensate for a lack of food, our bodies try and keep fat to burn as a means of survival.

What's more, our dieting regimens themselves are sometimes self-defeating. Many advise ingesting less glucose in order to shave off pounds, but as you learned in our previous blink, glucose is necessary for willpower. Therefore, no matter what diet you're on, you have to consume enough glucose to keep your willpower levels up; fail to do so, and you'll have no mental strength to do anything, let alone diet.

What's more, you also have to face the basic fact: while willpower is a strong and underrated force in our daily lives, it can't fix everything!

### 10. Final summary 

The key message in this book:

**Willpower isn't an abstract concept. In fact, it's tied to our biology. Knowing this, scientists can now study the strengths and limitations of willpower so that you can understand more about how to wield it most effectively.**

Actionable advice:

**Use the available technology to help your willpower grow.**

As you've learned in our blinks, planning and social pressure are great tools in strengthening your willpower. Find apps that measure your progress — be it weight loss, exercise, or study sessions — and post your results on social media. Not only will you have a better idea of where you stand, but your friends and family can give you the support you need to stay on track.

**Suggested** **further** **reading:** ** _The Willpower Instinct_** **by Kelly McGonigal**

_The Willpower Instinct_ introduces the latest insights into willpower from different scientific fields, such as psychology, neuroscience, economics and medicine. While considering the limits of self-control, it also gives practical advice on how we can overcome bad habits, avoid procrastination, stay focused and become more resilient to stress.
---

### Roy F. Baumeister and John Tierney

Roy F. Baumeister, PhD, is a prolific and world-renowned psychologist who has written 450 scientific publications and 28 books. He is currently Francis Eppes Professor of Psychology at Florida State University.

John Tierney is a science columnist for the _New York Times_ who has been honored with awards from the American Association for the Advancement of Science and the American Institute of Physics.

