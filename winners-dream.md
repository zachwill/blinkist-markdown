---
id: 54f442e6333035000a040000
slug: winners-dream-en
published_date: 2015-03-02T00:00:00.000+00:00
author: Bill McDermott
title: Winners Dream
subtitle: A Journey From Corner Store to Corner Office
main_color: 2EB3E6
text_color: 2181A6
---

# Winners Dream

_A Journey From Corner Store to Corner Office_

**Bill McDermott**

_Winners Dream_ offers a guide to achieving your goals by following the life and example of Bill McDermott, current CEO of SAP, the biggest software company in the world. By identifying your winning dream, you can find the motivation to overcome all obstacles in your path and help others along the way.

---
### 1. What’s in it for me? Learn how to follow your own dream, from someone who followed their own. 

Few moments are as thrilling as the one where you discover exactly what you want to pursue in your life. Some people realize they want to spend their days helping people, for others the dream is to lead a successful team.

Whatever your epiphany may be, it's rarely simple to achieve. There will be pitfalls on the way, many setbacks and long moments of indecision and doubt.

These blinks show how Bill McDermott, CEO of software giant SAP, followed his own dream from humble beginnings to financial superstardom. Reading them will help you plot — and stay on — your own course to success and fulfilment.

In these blinks you'll discover

  * why you should send your best workers to Hawaii; and

  * which four questions you need to constantly ask to keep up your game.

### 2. There’s only one thing that all winners have in common: a big dream for which they’d sacrifice almost anything. 

On what foundation do the world's highest-achieving individuals build their success? Is it a good upbringing, full of the luxuries provided by a moneyed family? A ruthless desire to win at costs?

Actually, it's neither of these. As you'll see in the story of the author and hugely successful CEO Bill McDermott, success derives from something else entirely.

The author's spectacular business career certainly wasn't written on the wall during his early childhood in lower middle-class New Jersey. But it's from this childhood that he draws the outstanding quality that guarantees success: the relentless pursuit of a big dream despite all obstacles.

Among the first steps on the road to achieving this dream is an early exposure to work and a willingness to take risks.

Seeing his dad getting up early every morning to work multiple difficult jobs to support the family, the author developed an unflinching work ethic at a very young age. It was then that he decided his dream was to become a successful businessman.

So, from the early age of eleven, the author began working several hard jobs himself — often more than one at a time — ranging from newspaper delivery to waiting tables and taking the nightshift at gas stations.

At the age of 17, the deli where he was working was robbed and stripped of all its wares. Confident in his ability to rebuild the deli, the author decided to take a chance, and bought it from its frustrated owner. In the end, his determination and steadfast work ethic made the deli more successful than before.

> _"Never let the circumstances of a moment supersede the size of your dream." — Bill McDermott._

### 3. Passion and planning trump age and experience in getting your dream job. 

If you want to land your dream job, you'll need to perform better than your rivals in order to impress your prospective bosses in interviews. However, that's easier said than done, especially if you are young and relatively inexperienced. All too often, you'll find yourself competing against candidates who are far more knowledgeable and qualified than you.

But is this cause for worry?

Regardless of what is written on your résumé, you stand the best chance of success if you can demonstrate your passion for the job and your determination to land it.

Quite simply: you have to want it more than everyone else.

When the author was interviewed for his first job as a sales representative at Xerox, he managed to impress the sales manager with his personal story of surviving and thriving during hardship, having told them how hard he had strived to earn the mere opportunity to get the job.

It worked! While the other candidates had gone in with the attitude of "testing their chances," he had shown his determination to succeed, and was hired in the spot.

But passion alone is often not enough to get you the job. You also have to do your homework.

You'll need to carefully research the job and use this newfound knowledge to create a plan for tackling the role and making improvements.

Eventually, a sales manager position became vacant at Xerox — the natural next step for the author. Although according to the company guidelines he was too young and inexperienced for the job, the author nonetheless requested a meeting with the hiring manager.

He came prepared. He presented a detailed action plan of the ways in which he would manage the sales department, and demonstrated great passion for the job. As a result, Xerox ignored older and more experienced candidates and the author earned his promotion.

> _"No one cares how much you know, until they know how much you care." — Theodore Roosevelt._

### 4. The key to sales is empathizing with customers and answering their direct needs. 

In the modern world, many people work in sales. But not all salespeople are equal; some are better than others. The author would prove himself to be a sales genius, always outselling his coworkers during his time at Xerox. But how? Was it his intricate knowledge of the products? No, it was his ability to _empathize_ with the customer.

Indeed, a successful sales career depends on your ability to put yourself in your customers' shoes and "feel" their pain. Customers will appreciate and trust someone who demonstrates that he understands what they're going through. The best salespeople exploit this.

Working at Xerox, the author developed his own technique for maximizing this empathy: the "Feel-Felt-Found" technique. He would open his sales pitch with a story of "I know how you feel, others have felt the same way, and they have found . . . "

The author earned the trust necessary to close the deal by demonstrating that he had lived his customers' difficulties and that there was a proven solution for them.

Moreover, a true understanding of the customer and his environment helps when designing products and sales pitches that are just too good to refuse.

Indeed, the best sales technique is simply to provide the customer with something they really need. To do this, you'll need to identify that need by researching their practices and finding the weak spots.

When the author got a job with the software company SAP America, he initiated a new sales strategy called _value engineering._ The company sent industry experts to their clients to learn their operations and business models in detail. With this knowledge, SAP could create products and sales strategies based on their clients' actual business practices, and their sales skyrocketed.

Don't bother telling customers a "technology story" about all the features of your newest product. Instead, tell them about business case studies and how your customer can use your product to be more successful.

Up to this point we've been discussing how to get your foot in the door at your dream job. The following blinks will show you how to lead and inspire people in order to finally realize your dream.

> _"Give them what they want and how they want it."_

### 5. A leader’s primary objective is to keep her team happy, motivated and performing well. 

Think back to all the jobs you've had that you truly hated. What was it about these jobs that you most despised? It's a fair guess that in many of these roles, it was a bad boss who fueled your discontent. Employees don't often quit companies. They quit _managers_.

A good manager cares for her subordinates and ensures they are happy and efficient. But how?

First, you need to get to know your employees and discover what's important to them. This could be things like their home, family or even dreams for the future. You do this so that you can communicate and make decisions based on their individual situations, and thus make them feel valued.

For example, if you know that one person often needs to work unusual hours because of their kids, you can make sure they have flexibility. Or if their dream is to become the best salesperson in the United States, you can bring them closer to it by sending them on a sales course.

Second, celebrate success! Your top performers will need incentives if they are to maintain or exceed their performance.

Since his days as a sales manager at Xerox, the author has done this by giving his best performers top-notch celebrations and holidays, including trips to five-star hotels in Hawaii.

However, there is one important caveat: while you should treat your employees as well as possible, you shouldn't tolerate poor performance.

If you allow a poor or lazy performer to produce shoddy work, then you'll send the wrong message to the rest of the team. Your best performers will see no point giving their all if they can reap similar rewards by doing very little.

> _"People don't quit companies, they quit managers." — McDermott_

### 6. Money isn’t everything: in addition to rewards, you need to appeal to people’s intrinsic goals to motivate them. 

We've just seen how powerful rewards like lavish holidays can be in motivating your employees to push themselves to the limit. However, if you want to motivate your staff over the long term, fancy rewards won't be enough.

These _extrinsic_ motivators may be important, but they don't have the same staying power as rewards based on _intrinsic_ factors, e.g., the personal satisfaction that comes with a job well done.

The most common questions the author asks potential employees are: "What do you want?" and "What is your dream?" These questions allow him to discern whether a candidate's intrinsic goals align with those of the team.

Most people will answer that they dream of being part of a winning team in some capacity. For example, they might want to be the most successful sales rep team in the company, or even simply learn how to be a better salesperson.

People with these kinds of goals can be motivated to help _themselves_ to get closer to achieving them, while also helping _the team_ in general.

Another way to provide your team with long-term motivation is by continually raising the bar.

People who dream of greatness realize that lofty goals and obstacles are simply steps on their journey toward their dream. Hence they see them as challenges, not burdens, and even draw motivation from them.

For example, when the author became the manager of the Xerox office on the Virgin Islands — the worst performing region in the Xerox Corporation — he immediately set a goal to become "the best performing region" by the following year.

At first his employees where paralyzed by his ambition, but soon they began drawing motivation and energy from this lofty goal. In the end, they managed to outperform every other region within the Xerox corporation!

### 7. The most successful companies never stop transforming themselves to meet the changing market. 

Nothing is more important for a business than its ability to constantly evolve in the face of innovations and fresh competition in the market. But how do you establish this in your work culture?

Start by constantly asking yourself four basic questions about your business model and your market.

1) Is your core business relevant today, and 2) will it continue to be relevant tomorrow? These questions help you evaluate whether you should continue on your current path, or whether you need to find a new direction. After all, even if your strategy works now, will it still work tomorrow?

When the author became co-CEO of SAP, he asked himself these same questions. SAP was selling software products worth billions of dollars to thousands of companies in dozens of industries. In other words, they were absolutely relevant. He concluded that with their current trajectory, their position was unlikely to change for at least ten years. So far so good!

However, before declaring victory, you still have two more questions to ask yourself: 3) What do your customers want? And 4) what will those customers want tomorrow?

Answering these questions honestly can help you discover whether there are any new opportunities you should be exploring, or whether any threats await you in the future.

By asking these questions, the author discovered that SAP's customers wanted the simplicity and elegance of applications like Facebook and Gmail in SAP's software. He also hypothesized that emergent technologies like smartphones, tablets and cloud services would quickly transform the way people used software.

So he decided to act, buying up numerous companies that already demonstrated success in mobile app and cloud design, thus adding their "DNA" to SAP.

Innovating your business model and creating a long-term strategy is one thing, but it's harder to achieve the careful, everyday implementation of that strategy. This requires discipline and, above all, a culture of trust.

> _"The secret to change is to focus all of your energy, not on fighting the old, but on building the new."_

### 8. The only way you can make effective changes in strategy is if your staff trust you to make the right decisions. 

Many companies fail to successfully implement changes in their organization and often waste millions of dollars in the process. Often, these failures aren't due to poor strategy or planning, but rather a fundamental lack of trust.

Indeed, any change within an organization requires a culture of trust. This is partly due to the nature of proactive change.

The best time to switch strategies is _before_ a change in the market, when the business is still doing well. Unfortunately, when a company is successful, people often can't wrap their heads around _why_ they should change, and thus don't trust the executives' plan.

When the author presented his bold new strategy involving new, simplified mobile software solutions alongside lofty new profit targets, people weren't happy. SAP was doing fine: they were earning a healthy profit, and change seemed unnecessary. Plus, the proposed changes to their business model seemed unrealistic, and too divergent from the well-established way SAP did business.

Getting people behind his plan would take some work.

The best way for leaders to overcome this kind of resistance is by instilling in staff the big dream that drives the change, and vanquishing doubts with a carefully planned agenda, facts and certainty.

So the author gathered hundreds of managers from across the corporation and held a strategy presentation, carefully outlining the necessary changes and the reasoning behind them. He and his co-CEO then personally trained a few of the higher managers, ensuring that they understood all details of their new strategic direction toward simplicity, mobility and cloud services, and sent them back to their departments to spread their insight and enthusiasm.

In doing so, they were able to reach out to everyone in the organization, convincing them to work together to successfully implement the plan.

> _"The most powerful thing a leader can do is change minds."_

### 9. Winning isn’t about the goal; it’s about the journey. 

At the end of the day, what does it mean to win? Is it the money, the luxurious corner office or the fancy car? No. These are just goals, specific outcomes that are part of a larger journey. A true dream is something much grander. In the end, winning is the _journey_, not the goal.

And it is the people that accompany you on this journey that make it all worthwhile.

The author knew from an early age that he wanted to be the CEO of a big company. This was his _winning dream_. But the people with whom he could share his accomplishments — his family and co-workers — were the ones who made his career accomplishments such an amazing and remarkable story.

Toward the end of his career, this realization helped the author to understand that his _true_ winning dream lay in his aspiration to always try his best and to help those around him to succeed alongside him.

Given that the dream is all about the journey and not the end goal, corporations should strive to contribute to society and help others on their way.

We can see this in SAP, which supports many organizations involved in social projects such as building playgrounds and providing software to schools and libraries. Moreover, they encourage their employees to take a "social sabbatical" to share their know-how with small business owners and NGOs.

In the end, it's not the dream itself which is rewarding, but rather overcoming all the obstacles that stand in your path. Winning is about waking up with a smile every day and chasing your dream, knowing that you've helped others on the way.

> _"Focusing on your dream will turn your working life into your life's work."_

### 10. Final summary 

The key message in this book:

**Your personal winning dream is the engine that drives your everyday performance. Without it you can't fuel your ambition and find true success. But in the end, it's not the ultimate goal that is the most important part of your winning dream, but the journey to achieve it.**

**Suggested further reading:** ** _Winning_** **by Jack Welch with Suzy Welch**

_Winning_ is a collection of no-nonsense advice and original thinking on successfully running a company, managing people and building a career. It answers the toughest questions people face both in and outside their professional lives.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Bill McDermott

Bill McDermott is the CEO of SAP, a global leader in software with branches in over 130 countries. He is on the supervisory boards of numerous companies, such as Under Armour and ANSYS, and is also on the advisory board of the Knowledge is Power Program.

