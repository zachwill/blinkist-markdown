---
id: 581749829907fc0003a26009
slug: winners-en
published_date: 2016-11-03T00:00:00.000+00:00
author: Alastair Campbell
title: Winners
subtitle: And How They Succeed
main_color: A5844A
text_color: 8C6623
---

# Winners

_And How They Succeed_

**Alastair Campbell**

_Winners_ (2015) is your guide to winning in all aspects of life. These blinks explain what makes a winner — the things that enable people to defy the odds and succeed, no matter what — as shown through real-life stories of winners at the top of the world in politics, sports and business.

---
### 1. What’s in it for me? Learn how to become a winner by acting like a winner. 

****Many people are smart, driven and talented. The chances are that you too are one of them! And yet only a lucky few "make it," becoming powerful leaders, sports champions or rock stars.

Who doesn't want to be a winner? The question is, what's the secret recipe? What do winners know that no one else does?

These blinks take a close look at the success stories of winners, and in the process, give you the inside scoop on what it takes to make it to the top.

In these blinks, you'll learn

  * how an entrepreneur launched an airline on a whim and won;

  * why losing big once can turn you into a winner for life; and

  * how you can benefit from living a life under pressure.

### 2. Winning depends first and foremost on a solid strategy that will lead you to your ultimate objective. 

If you're reading this, you've decided you want to be a winner. Whether you're shooting for the corner office or dreaming of rock 'n' roll fame, there are a few essential things to know.

First is _strategy_. Strategy is the _how_ of winning. A strategy paves the way toward your goal; thus it is the most important aspect of becoming a winner.

Former World Chess Champion Garry Kasparov says that if you don't commit to a strategy, you're doomed to react to your opponents, allowing them to control the game.

The _what_ of winning is your _objective_. You need to determine what you want to win before you can lock down a successful strategy.

For instance, if you want to lose weight, your objective might be to lose 10 pounds. Once you've defined your objective, then you can create a strategy to help you reach it.

Yet a strategy is useless if your objective is essentially unattainable. Remember: winners set goals _they can reach_.

In November 2013, Tony Pulis started managing the British soccer team Crystal Palace, a club struggling to stay in the Premier League. Pulis didn't shoot for the stars and try to make the team Premier League champions in his first year. Instead, he set a realistic objective of securing the team's position in the league — and he succeeded.

However, don't confuse your strategy with _tactics_, or the little steps in between that help you realize your strategy and ultimately, your objective.

If your strategy to lose 10 pounds is to start dieting and perform exercises daily, your tactics will include things such as monitoring your caloric intake, taking the stairs instead of the elevator and joining a diet group.

In the holy trinity of winning, strategy is just one aspect. The other two components are leadership and "teamship," or the elements that make a winning team — let's explore those next.

> _"By failing to prepare, you are preparing to fail."_ — Benjamin Franklin

### 3. Leadership comes in many forms; there’s no one trait that unites all the great leaders of history. 

So what does it take to become a _great_ leader? There's no one specific trait; each leader is outstanding in a different way.

German Chancellor Angela Merkel possesses the ability to see what works in a given situation and to stick with it. Instead of offering grand gestures, she acts pragmatically. Because of these skills, Merkel is a winner, leading one of the European Union's most powerful nations.

During the recent eurozone crisis, for example, Merkel was steadfast even when politicians such as British Prime Minister David Cameron or US President Barack Obama pushed her to act faster or more boldly. Despite pressures all around her, Merkel would not be rushed, staying true to a time-proven strategy of being cautious and sticking to long-term goals.

Winston Churchill became a great leader by recognizing Nazi Germany as a major threat early on, and he addressed this issue with a laser-like focus. In fact, his record as a winner wasn't very impressive until World War II rolled around.

Here's his story:

In 1899, Churchill as a young man tried his hand at politics but lost an election. After working as a war correspondent in South Africa, he returned to Britain and finally became a member of parliament (MP).

Yet still he lost subsequent elections, eventually fighting his way back into government. By the 1920s, he was Britain's Chancellor of the Exchequer, leading the country's return to the gold standard — a disastrous move that resulted in deflation and unemployment.

Despite all this, Churchill is still one of history's great leaders. Early on he saw the danger of Adolf Hitler and the aspirations of Nazi Germany. He channeled all his energies into finding ways to defeat the Nazis.

It was this insight and determination that resonated deeply with the British people and made Churchill the great leader the world now remembers him to be.

Next, we'll explore the building blocks of successful teams, the third element in the holy trinity of winning.

### 4. Winning means creating a team in which people share talents where you fall short. 

Do you think a soccer club composed of 11 athletic clones would be a winning team?

Definitely not. A team with members who all have the same skills just won't win. If you want to be a winner — in sports or business — you need to find a team with talents that complement your own.

A team brings together people with different strengths — a winning strategy. As a winner, you need to find people for your team who excel in areas in which you struggle.

For instance, you might be a great striker but a terrible defender. Naturally, you'd bring someone on your team who can defend the goal!

Remember that a leader's job is to define a team's goals, strategy and tactics, ensuring that everyone on the team knows what to do.

For example, a cycling team comprises athletes, mechanics, sports therapists, and so on. But in the follow car, the person with the walkie-talkie who is directing team tactics is the leader.

After a leader comes the _warriors_. They're not the "stars" of the show, but they make the engine run.

For instance, consider the role of a bus driver during an election campaign. This person drives the bus, carrying a politician from meeting to rally. If a driver were unreliable, the whole campaign would collapse!

The _talent_ refers to people who make all the difference, the rare personalities who have something extra, who are more capable or knowledgeable than the pack.

In politics, the talent is often the leaders themselves. Consider Hillary Clinton, who challenged Barack Obama in the Democratic presidential primary in the United States, but lost. She then was named secretary of state, a preeminent "talent" on the president's team.

But being a winner isn't just about strategy, solid teams and leadership. It's also about what's on your mind.

> _"If we are together, nothing is impossible. If we are divided, all will fail."_ \- Winston Churchill

### 5. Winners hate losing and put pressure on themselves, leaving their comfort zone, to avoid it. 

It's often said that winning is all about mind-set. But what exactly does this mean? Are winners merely people who _really_ want to win? Doesn't everybody want to win?

Of course, most people want to win, but not everyone fears losing. Many people are winners simply because they're too scared to lose.

Many winners have experienced the pain of defeat and will go to great lengths to avoid repeating the experience.

Olympic gold medal swimmer Michael Phelps has won more gold medals than any Olympian in the history of the games. His motivation to win goes back to the 2000 Sydney Summer Olympics.

That year, he was ready to compete but didn't win a single meet. It was this failure that pushed him to work harder, and he never lost a race again!

Winning isn't just about motivation, however. It's also about getting out of your comfort zone.

While things that are familiar and easy might make you feel good, staying in a place where you're comfortable will prevent you from winning big.

For years, Garry Kasparov won chess tournament after chess tournament. His successes kept him firmly in his comfort zone and, certain that he'd win, he stopped testing new strategies or making bold moves.

He thus stopped growing as a player, his skills stagnating. Finally, Vladimir Kramnik, an opponent, beat him and claimed Kasparov's world title.

To win, you've got to push yourself and get out of your comfort zone. In other words, you need to put yourself _under pressure_. Pressure influences both your body and mind, improving your ability to focus and think, and in turn, unlocking new heights of performance.

### 6. Winners set themselves apart from the pack by innovating and acting boldly. 

Want to stand out from the crowd? You could always dress up like Superman. But for those who aren't into costumes, there are other ways to get noticed — a key aspect of being a winner.

To get noticed, you first have to be bold. When you act boldly, you'll stand out because you'll accept challenges that those people who are more reserved just wouldn't.

Being bold also helps you push past the fact that in many areas, you might be a novice — but still have the drive to achieve. Entrepreneur Richard Branson, for example, was just 17 years old when he launched a student newspaper. A few years later, he founded Virgin, a record business.

Branson went on to found several other companies, including Virgin Airlines. All these projects had one thing in common — Branson committed despite a lack of experience in each field!

Branson is dyslexic, a potential impediment when running a news organization. He also knew nothing about the music business but was, as a consumer, frustrated with the lack of labels for great music. What's more, Branson also knew next to nothing about the airline business.

His foray into aviation was launched when he was bumped off a flight to the Virgin Islands. Annoyed with the delay and poor service, he decided to launch his own airline. A bold move that made him a winner!

Boldness will set you apart, and similarly, a hunger for innovation will help you win. But remember, innovation doesn't necessarily mean invention. Instead, to innovate, you take a product, service or process and make it better.

Thus a winner pays attention to the details and finds opportunities. Apple, for example, took an existing product like the mobile phone and improved it — a classic innovative, winning strategy.

### 7. A crisis can quickly overwhelm you if you don’t keep your mind on what matters. 

Have you ever noticed that so many events today are called a "crisis"? From minor disagreements between heads of state to a blogger's bad hair day, "crises" seem to be everywhere and define everything.

But none of these examples represent a _real_ crisis — a situation that could overwhelm if you don't make the right decisions.

A soccer team losing a few matches isn't a crisis. On the other hand, issues involving people potentially abusing power, such as US President Bill Clinton's affair with intern Monica Lewinsky, could be considered a crisis in politics.

Let's examine this a bit more deeply.

When a true crisis strikes, you need to stay focused. If you find yourself in a crisis, remember to keep your mind on what truly matters — the things over which you have influence. This is a key point.

When Clinton's relationship with Lewinsky was revealed, Clinton kept himself focused on the political issues at hand. When the media published the revelations, Clinton was on the phone with British Prime Minister Tony Blair, talking over concerns regarding Russian nuclear arms.

Clinton knew the prosecutor's report would be published that same day, but he didn't know what it would say. He also wasn't sure how friends, family or colleagues would react to the report.

He did know, however, that he couldn't control these things. What he _could_ do was focus on doing his job. In the end, it was Clinton's constant focus that earned him the respect he needed to make it through the crisis.

### 8. Final summary 

The key message in this book:

**Winners come in all shapes and sizes. While winners share certain traits, such as boldness or innovation, their ability to beat the competition is also a result of the teams they build and their focus amid crises.**

Actionable advice:

**When dealing with a crisis, be honest.**

If you find yourself in the middle of a crisis, you might feel like closing your eyes and wishing it to disappear — or worse, pointing the finger at someone else. These are not the best solutions and certainly not the easiest. Instead, do something radical: be honest.

If you did something wrong, fess up or at least initiate an honest, transparent inquiry into the issue. If you don't, someone else will do it for you — potentially bringing issues to light that could irreparably damage your reputation.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Leading_** **by Alex Ferguson, Michael Moritz**

In _Leading_ (2015), one of the world's most celebrated soccer coaches shares lessons he learned about teamwork, leadership and incredible athletic performance. His observations and experiences provide timeless insights into success in the sporting world and beyond.
---

### Alastair Campbell

Alastair Campbell was press secretary for former British Prime Minister Tony Blair, later serving as director of communications for Blair's administration for almost a decade. He is the author of several books, including _The Blair Years_.

