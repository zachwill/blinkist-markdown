---
id: 548623c36331620009b90000
slug: wired-for-story-en
published_date: 2014-12-10T00:00:00.000+00:00
author: Lisa Cron
title: Wired for Story
subtitle: The Writer's Guide to Using Brain Science to Hook Readers from the Very First Sentence
main_color: C84D68
text_color: AD314C
---

# Wired for Story

_The Writer's Guide to Using Brain Science to Hook Readers from the Very First Sentence_

**Lisa Cron**

_Wired for Story_ (2012) takes findings from modern brain science to explain why exactly certain stories suck us in, while others leave us bored and disengaged. By using some fundamental techniques drawn from understanding what makes us tick, writers can craft more compelling stories.

---
### 1. What’s in it for me? Use the findings of neuroscience to write better stories. 

We all love a good story and for generations writers have attempted to craft stories which intrigue us, shock us and excite us. Yet the writers of today have an advantage over those who have lived before: they have the power of brain science.

Studies into the human brain can show us why we like a good story and what type of narratives grab our attention the most. These blinks provide an outline to these findings. They show budding writers what they need to do to become successful.

In these blinks you will learn:

  * why Hamlet "works" as a story;

  * just how many pieces of information the human brain needs to remember every second; and

  * why your first draft is always terrible.

### 2. Humanity’s love for stories evolved out of our need to survive. 

Our TV stations are programmed full of dramas and soap operas and our bestselling book lists overflow with fiction. What is it about stories that we love so much?

Stories appeal to us so much because they are hardwired into our brains. They actually allow us to picture the future and prepare for it.

When a good story captivates us, the neurotransmitter _dopamine_ is released in the brain, which causes our concentration and interest to heighten.

We can actually thank our evolution for this process. Exchanging stories was the most effective way for our ancestors to transfer lifesaving information to one another, so we learned to pay attention to them.

Picture a Stone Age man sitting in his the cave one night, listening to his friend's story about how his daughter ate some red berries and nearly died. From that story, our ancestor would have learned crucial information about how to keep his own children out of danger.

Modern neuroscience can help us strip this evolutionary practice down further for a deeper understanding. We've now discovered that when we listen to a story, our brains process the information just like they process real life. This tells us that stories developed as a simulated learning experience, giving us a safe way of understanding how to deal with danger, without having to actually face it.

For example, if your ancient self wanted to know why you shouldn't approach a saber-tooth tiger, it was a better idea to listen to a friend's story, rather than go and find out what might happen for yourself. Both methods would teach the lesson, but the story is unlikely to result in your death.

In our modern world, our chances of facing a saber-tooth tiger are pretty low. Yet, the power of stories to engage and educate us endures.

The wonderful thing is, writers can take advantage of this. But a story alone is not enough. Engaging stories need to contain particular characteristics, which we will discover in the following blinks.

### 3. A good story needs a focus which filters out unnecessary information. 

Ever read a great story without a decent plot? Probably not! When we listen to a narrative which meanders aimlessly, we are likely to lose interest in it rapidly. This is because all good stories need an explicit _focus_ consisting of three factors in order to create an engaging story.

The first factor is the protagonist's _issue_, meaning the desire of your main character. If we look at Hamlet, the issue would be his father's murder and his investigation to find out what happened.

The second factor is the _theme_. The theme is what your story communicates to the audience about what it means to be human. In Hamlet, the themes would be sanity, madness and depression.

The last factor is the _plot_, which is the protagonist's quest to realize his goal. In Hamlet, this would be all the unexpected events leading up to Hamlet's death.

Once your focus is clear, all information in the story needs to adhere to one of these three factors. There shouldn't be any superfluous information!

So why is focus so vital? The answer is obvious when we see how our brain processes information.

Every second, our brains are flooded with around 11 million pieces of information coming from our senses, yet we can only process between five and seven — note, _not_ five to seven million — of them in total.

Precise focus assists the brain in selecting relevant and important information. Without it, we struggle to filter out what is important and our dopamine levels fall, which limits our interest in the subject matter at hand.

Without a focus, Hamlet becomes a rather boring collection of random facts about Medieval Denmark.

> A reader is a bit like a potential business investor. If you want to impress them, you need to cut through the clutter and get to the point.

### 4. Empathizing with the protagonist’s emotions engages the reader with the story. 

We tend to think there are two separate sides to our personalities. One side is responsible for reason, rationality and decision-making and the other rules our emotions, our knee-jerk reactions and judgments.

We believe that in order to make smart decisions, we can simply ignore our emotional side and concentrate on the rational one. But this is incorrect.

Neurological science has discovered that we cannot function on reason alone. Our emotions are crucial.

For example, neuroscientist Antonio Damasio studied a man with a brain tumor, rendering him incapable of experiencing emotions. He scored high on IQ tests, yet left without his emotions, he was completely unable to make decisions. Even a task as simple as choosing a pen was too difficult for him.

A writer shouldn't underestimate the importance of emotion. If you want your readers to gain anything from your story, you need to satisfy their emotional side.

One method is to put the audience in the protagonist's shoes. The audience wants to relate to the emotions that the protagonist is experiencing. They want to know exactly what it would feel like to be him.

This can be achieved in a number of ways.

You can paint a picture of how the character reacts to things emotionally such as anxiously pacing back and forth, her face turning gravely pale and so forth.

You could also reveal something to the audience that the protagonist doesn't yet know, for example say his girlfriend is returning home and the protagonist is in bed with another woman. This enables the audience to guess ahead of time how the characters will feel.

You could also describe the protagonist's thoughts by using a narrator to tell us how she feels.

### 5. Only a protagonist with a clear internal goal can make the reader truly engaged in the story. 

Can you recall a book you loved in which the main character sort of stumbled around, with no defined purpose or desire? Most likely you can't.

That's because a great writer knows that giving the protagonist a clear goal is vital to captivating an audience.

It's compelling to us because of _mirror neurons_ in our brains. When we know what the protagonist is doing, like fumbling his way through a scary house, the same areas of the brain are activated as if it were we who were doing the action.

But how is this connected to the protagonists' goal? Well, if we didn't understand the protagonist's aim, then we wouldn't be able to understand how his behavior would make him (and us) feel.

If we're unaware that our hero is searching the scary house in order to find her kidnapped partner, we're unable to sense how she's feeling when she's doing so.

There are two kinds of goals: _Internal goals_ are the goals the protagonist needs to realize so that he can evolve as a person, and _external goals_,which must be completed in the outside world (such as finding the kidnap victim).

Let's look at John McClane's goal in the movie _Die Hard_. McClane wants to stop a gang of insane terrorists from killing everyone at Nakatomi Plaza. However, his real inner goal is to get back together with Holly, his ex-wife.

Internal goals are actually the most vital, as they're what the audience can most identify with; we can't really relate to what it's like to fight a gang of terrorists, but most of us know how we'd feel if we were trying to win back a loved one.

Therefore internal goals should be crystal clear. Although the external ones can add excitement and intrigue to a plot, they mustn't dominate.

### 6. To engage with a story, we need specifics to draw us in and help us imagine. 

Stories would be dull without central concepts and abstract ideas. The problem is that you can't actually _see_ them. Ever tried visualizing the duality of man with no imagery to help you?

Images are everything, owing to the fact that our brains have evolved with an extraordinary ability to create mental pictures.

With these images we create a mental model of the world where we can imagine our actions without having to deal with the consequences in the real world.

But how crucial is imagery? Well, as neuroscientist Antonio Damasio asserts, our _entire_ consciousness is formed with images.

Even scientists use images and metaphors to envision abstract concepts. Einstein was able to bring his theory of relativity to life by recalling how, in his childhood, he sometimes visualized himself riding on a beam of light.

But how exactly does all this importance placed on imagery affect writers?

For writers, using imagery is a powerful advantage. If a story contains too many generalities, the audience's brain starts to drift off. This is because generalities don't create specific images in the brain, which makes them conceptually slippery to grasp. Consequently, the audience starts to lose interest and their dopamine levels decrease.

Take a look at the following examples on house fires;

Approximately 2,500 people die in house fires every year in the USA.

David awoke to the sound of his mother screaming as smoke filled the room. He ran to her, all the while being beaten back by the flames, only to see her trapped under the collapsed roof. "I love you'" she called out, as he fought to try to save her.

Which example sticks with you more? The second one, of course. The first is simply a general fact, so we struggle to picture it, but the second contains specifics that we can see in our minds, which immediately sucks us in.

### 7. A good story exploits our constant desire to look for patterns. 

One thing our brains really hate is randomness. What it wants is to find patterns, even where none exist.

Our tendency to notice patterns evolved as a useful tool to simplify the complex world that surrounds us and to be able to rapidly predict subsequent actions.

For instance, a caveman who observed a mammoth lower its head before a charge, would have expected to witness another charge if he saw the same action again.

It's no surprise, then, that this proclivity towards patterns has had a huge impact on story.

One fundamental assumption about stories that is ingrained into us is that everything we regard as the start of a pattern, in other words a _setup_, must follow with a _payoff_.

Anything that signifies future action in a story is known as a setup. When James Bond is shown an array of novel gadgets by Q, we know he's not just checking them out for fun. The _payoff_ is when Bond later uses one of these gadgets against a bad guy.

In order for it to be effective, the path between the setup and the payoff must be clear.

If something is set up in the beginning of a book, for example, and only crops up again towards the very end, the reader may barely remember it and therefore the payoff isn't very satisfying.

Having said that, there is one powerful tip you can use to get the audience's attention: break the expected pattern. When something doesn't unfold as we thought it would, we are shocked and intrigued.

Consider a famous scene from the first Indiana Jones film in which Indy is confronted by a sword-wielding madman. You expect the payoff to involve an impressive and skillful sword fight, but Indy nonchalantly takes out his gun and shoots the guy. It leaves an impression on us because the pattern is broken.

### 8. With long term practice, knowledge of what makes a good story becomes intuitive. 

Say someone asks you how many of the letter "e" are in the word "entrepreneur." You quickly try to visualize it, but you can't come up with the right answer. We've all been there, that disconcerting feeling of forgetting how to spell a certain word, and the more you try to figure it out, the more unsure you become.

This happens because our brains function at their best when we act using our intuition; thinking too hard about something often decreases our performance.

Nobel laureate Herbert Simon says that it takes around ten years to become an expert in a subject. After ten years of practice, around 50,000 chunks of knowledge become internalized in the brain, meaning that we can process them automatically. This knowledge becomes readily accessible to the brain without us having to really think about it.

An expert table tennis player, for example, will have practiced so many times that she can estimate the bounce of the ball or the direction of the shot automatically; she needn't think about it.

But what has this got to do with writing?

Rewriting over and over is the optimal way to develop the necessary skills for writing a compelling story. It transforms your ability to create incredibly memorable stories into something that happens intuitively.

So when you feel that your script just isn't up to scratch, remember that many great novels have most likely been rewritten innumerable times before they were published.

When Michael Arndt finished the screenplay for the critically acclaimed movie _Little Miss Sunshine,_ for instance, he had already reworked it over a hundred times.

When you need a little boost to keep going, remember what Hemingway once said: "All first drafts are shit."

### 9. Final summary 

The key message in this book:

**The key to writing a great story is understanding how the human brain processes information.** **Once you understand that and can utilize the writing techniques that exploit it, you'll be well on your way to creating outstanding stories.**

Actionable advice:

**Don't bombard your audience.**

Although sensory details can help a story come to life, too much can be overwhelming. After all, your brain can only process a tiny part of all the information coming from your senses. So think carefully about whether the sensory details actually contribute to the story or not.

**Create a relatable goal for your protagonist.**

'Outside world' goals are entertaining, but does your main character have a relatable goal? It might be exciting to read about saving the world from zombies, but it's tricky for the audience to grasp exactly how this might feel for the protagonist. So, in addition, think of an internal goal or drive that relates all humans can relate to, such as finding love or dealing with the death of a loved one.

**Suggested further reading:** ** _Lead with a Story_** **by Paul Smith**

_Lead with a Story_ teaches you how to enhance your skills as a great leader by harnessing the power of storytelling. By taking examples from one of the most successful companies in the world, you'll learn how to craft a great story that motivates people and modifies their behavior.
---

### Lisa Cron

Lisa Cron is a writer, literary consultant and an instructor in the UCLA Extension Writers' Program. She has previously worked as a publisher at W. W. Norton, a literary agent at Angela Rinaldi Literary Agency and as a story consultant for Warner Bros.

