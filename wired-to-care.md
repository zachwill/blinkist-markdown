---
id: 54c64e9a3732390009280000
slug: wired-to-care-en
published_date: 2015-01-27T00:00:00.000+00:00
author: Dev Patnaik and Peter Mortensen
title: Wired to Care
subtitle: How Companies Prosper When They Create Widespread Empathy
main_color: F09530
text_color: A36521
---

# Wired to Care

_How Companies Prosper When They Create Widespread Empathy_

**Dev Patnaik and Peter Mortensen**

Modern businesses seem to rely more on mounds of data and trend reports than on the needs and desires of actual customers. _Wired to Care_ shows how businesses can use empathy to make connections with customers and discover what they really want. At a company-wide level, this kind of empathy leads to growth, and it can be a game changer for individual employees.

---
### 1. What’s in it for me? Discover how to unleash the power of empathy and make your business thrive. 

How does your business find out what customers want? Hire a customer evaluation expert to do some research and send you a report, perhaps? Then you and your management team delve into the findings, crunch the numbers, evaluate the Venn diagrams and hope a strategy emerges.

That's what most people do, but this practice of diving into the numbers, Matrix style, will not bring you closer to your customers. Why? Because it lacks an empathetic connection. What your customers really desire cannot be found in a report; it can only be found by getting out there and interacting with them.

These blinks show how to make the right connection with your customers, and how you can never just rely on reports.

In these blinks you'll discover

  * why new Netflix employees get a free DVD player;

  * why bringing a live tiger into a meeting can help you make better decisions; and

  * why working at Nike requires fitness.

### 2. Companies need an empathetic connection to their customers in order to do well. 

Ever attended a meeting where you're trying to make an important product decision based purely on statistics and forecasts? A lot of us have been there. In the business world it's common to reduce all information into manageable chunks of data.

The danger of this is that oversimplified, compiled information neglects a crucial aspect: the connection to customers.

To make sound decisions, you need a real life link to the people it will affect, and you need to be able to feel how they will feel. That is, you need _empathy._

Empathy allows you to know what your customers really want.

For example, when the idea of an animal kingdom theme park was first proposed at Disney, the company seemed uninterested, as the data showed they were unlikely to profit from it. So executive Joe Rohde introduced some empathy to the situation: he brought a Bengal tiger into the room. The Disney's executives immediately saw the appeal of wild animals and the idea got the green light. When the park opened, it became one of the most popular in the country, drawing in over 8.9 million visitors a year. 

But if empathy is so vital for businesses, why do so many consciously remove the connections?

As a company expands and becomes more successful, it loses touch with the everyday customers it once represented. Where staff once cared about their product and customers, they now get bogged down in strategy meetings and trend reports.

For instance, when Jell-O recorded a drop in sales, executives pored over the matter for hours using data from quantitative research. Yet, when asked, nobody in the room had actually eaten Jell-O in the past six months. So how could they possibly put themselves in their customer's shoes?

So now you know that empathy works for business, in the following blinks you will find out how to use it.

### 3. It’s easier to empathize with people like yourself, so “hire” your customers when you can’t step into their shoes. 

We usually hang out with people who share similar mind-sets to us. As the saying goes, "Birds of a feather flock together." But why?

Because it's easier to connect with people who are similar to us and the way we see the world. We know about their interests because they're similar to our own, and we can guess how they would react to most situations, because it's how we would likely react. Feeling and showing empathy toward these people is therefore a no-brainer.

So if a company wants to make an empathetic connection with its consumers, it should hire people just like them.

Which is exactly what Harley Davidson does. The company's headquarters is a haven for bike lovers. Gigantic sections of the parking lot are set aside for bikes, and pictures of bikes and banners from biker gatherings line the walls. This passion for bikes gives Harley Davidson a connection to its customers. And it's hugely effective: between 1986 and 2006, the company experienced double-digit growth while the rest of the US motor industry languished.

But what if you can't hire your customers? Then put yourself in their shoes.

For enterprises like childrens' toy companies, it is impossible to hire your customers. In these cases, try to see the world from their perspective and find out what's important to them.

Take American Girl, the doll manufacturer and book publisher whose target audience is young girls. As they obviously can't hire a team of elementary schoolgirls, they try their best to see what makes them tick. For example, they read the fan mail little girls send to them and they observe how the kids play with their products. Doing this allows them to understand what the girls want, enabling them to stay near the top of their market long term.

> _"We don't spend a lot of time talking about 'what consumers want.' So far as we're concerned, we are them and they are us." — Lara Lee, Harvey Davidsons former head of service_

### 4. In order to make the most from empathy, you need to spread it through your entire organization. 

Picture your company working in a vast, air-conditioned building where only the windows on the top floor can be opened. When anyone wants to know what the weather is like outside, they have to contact people in the office with the open windows and wait for a report.

This sounds a bit stupid, but if the only way your company can inform you about customer needs is through a customer evaluation department, you're doing pretty much the same thing — only letting one group test what's outside.

But there is another way.

_Open Empathy Organizations_ are enterprises where _every_ employee is encouraged and expected to form connections with customers.

One example is Nike. Rather than relying on a small department to tell them what the customer wants, Nike provide basketball courts, soccer pitches and running tracks so that staff have a chance to act like customers and get inside their mind.

To make your enterprise an Open Empathy Organization, give the following a go:

Make it easy for employees to empathize with customers. Netflix does this well by giving employees a free DVD player and subscription to the service when they join the company. So they not only become employees, they also become customers as they use the product, look for potential improvements and notice what is great about it.

It's not always easy to connect with customers though, and this is where you need to get creative. When Nike entered the Japanese market, for example, they visited Japan first. When they returned, they recreated Japanese teenagers' bedrooms, decorating them with posters and a TV playing Japanese programs. Each employee could then empathize with the Japanese market. This approach worked and Nike were one of the few US companies to become successful in Japan.

> _"When the CEO walks out of the room, the rest still need to be able to make good decisions that serve the best interest of their customers."_

### 5. Finding your customers’ perspective can enable you to find new opportunities for growth. 

Present different people with the same object and chances are they'll come to different conclusions about it. We all hold different perspectives. We notice things that others don't, and overlook things they see.

It's no different in business. If a business can reframe its point of view and take another perspective, there's huge potential for growth.

In particular, reframe the way you see your customers' problems and you'll see new possibilities.

This is what the retail company Target did. Traditionally, the back-to-school period is a rich opportunity to sell more student supplies such as exercise books, pens, uniforms and textbooks. But Target wanted to stand out, so they changed their perspective. They viewed this period from the perspective of college students moving out of home for the first time. With this in mind, they created a back-to-school campaign around "big life changes," with new products like "kitchen-in-a-box." It was a success and Target increased its revenue by 12 percent.

As well as using customer perspectives to improve your products or market them in a different way, you can revamp your business model by taking into account what they think.

For example, Kodak always saw themselves as primarily a film manufacturer, yet they seemed to lag behind the competition. Japanese companies could provide cheaper film. Everything changed when Kodak discovered how their customers perceived them. To customers, they weren't a film company, they were a photography company. Using this discovery, Kodak began to market themselves as a firm that could help you capture memories, and subsequently launched the disposable camera. It was a huge success. This reframing pushed them to the front of the market in disposable cameras.

### 6. Empathy can give you the reason to come in to work every day. 

Our world has been influenced by many good souls: Mother Teresa, Gandhi and Mandela, to name only a handful. In comparison to these heros' and heroines' lives, you may find sitting in your office from week to week a bit low impact. But you're wrong: you just need to change your perspective!

If you want to see your work as a positive influence on the world, you need to feel like you are serving a purpose.

Many people don't think they make a difference to the world through their company or as an individual. Unsurprisingly, this causes them to work below their full potential.

Yet all work is significant. Whether you manufacture underpants or sell garden gnomes, everything is important for someone. We should also encourage others to realize that their work holds meaning and importance.

One example is the Clorox cleaning company, which makes cheap and hygienic products. These products are certainly useful, but hardly groundbreaking, and it would be a stretch to claim they made a massive difference to our lives.

So Clorox decided to change their image. They backed the message that moms are cleaning heroes who look after their family (which went down well with mothers), and then launched a new product line called Green Works, which contained no noxious chemicals, so they could take a stance on caring for the environment.

Soon, even staff in this ordinary cleaning company began to feel the importance of their job. It was this simple connection to real, everyday people that made their roles significant.

> _"I've never come across a product or service that didn't have the potential to make someone's life significantly better."_

### 7. Final summary 

The key message in this book:

**Putting yourself in somebody else's shoes not only lets you know how they view themselves and what their needs are, it also opens up a whole new approach for creating, producing and marketing your products more effectively and successfully.**

**Suggested** **further** **reading:** ** _Profit from the Positive_** **by Margaret Greenberg and Senia Maymin**

_Profit from the Positive_ explains how leaders can increase productivity, collaboration and profitability by using the tools of positive psychology to boost their employees' performance. It gives clear examples of how small changes can make big differences.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Dev Patnaik and Peter Mortensen

Dev Patnaik is the founder and CEO of the growth strategy firm Jump Associates, which has worked with clients like Nike, Samsung and Chrysler. Patnaik is also a speaker and an adjunct professor at Stanford University.

Peter Mortensen is the communications lead for Jump Associates and has written and edited for _Spin_ and _Wired_ News.

