---
id: 546bc42234353000080c0000
slug: with-charity-for-all-en
published_date: 2014-11-19T00:00:00.000+00:00
author: Ken Stern
title: With Charity for All
subtitle: Why Charities Are Failing and a Better Way to Give
main_color: BA7E85
text_color: 874149
---

# With Charity for All

_Why Charities Are Failing and a Better Way to Give_

**Ken Stern**

_With Charity for All_ offers an in-depth view of the inner workings of a sector which dominates ten percent of the US economy and employs 13 million people: the nonprofit industry. Subject to few controls, some huge nonprofit organizations are all too often afflicted with incompetence or even fraud.

---
### 1. What’s in it for me? Find out if it’s time to stop writing those monthly checks to your favorite charity. 

We all want to help out our neighbors in times of crisis — and with 1.1 million charities in the United States, this should be no problem. In fact, the nonprofit sector has never been as powerful as today, and continues to grow year by year. But is making a difference as simple as donating to your favorite charity?

In these blinks, you'll discover how charitable organizations aren't always effective — or even well-intentioned. While the purported aim of nonprofits is to work "for the benefit of society," the "charity" label covers an array of dubious organizations, from predatory hospitals to fraudulent associations.

Even when these organizations aren't plagued by downright fraud, poor management and short-sightedness lead many relief associations and charitable groups to fail drastically at achieving their stated goals.

In addition to uncovering the shortcomings of the charity sector, these blinks offer the reader valuable advice on how to find the right charity, and go on to propose simple measures the government can adopt to improve the integrity of the nonprofit sector.

After reading these blinks, you'll know

  * why some for-profit hospitals are registered as charitable organizations, even as they mercilessly chase down their debtors;

  * how drilling wells around the globe won't actually solve water shortages; and

  * why the Salvation Army pondered for weeks over accepting a $1.5 billion donation.

### 2. The growth and popularity of charities in the United States is a relatively recent phenomenon. 

To the average American, surrounded by messaging from nonprofit organizations, it comes as no surprise that charities both constitute a significant portion of US economy and have a large presence in education and health care systems. In fact, today's charities are so successful that their combined annual revenues have reached about $1.5 trillion.

But the nonprofit sector didn't always have this kind of power. As you'll see, the rise of nonprofits is a fairly recent development in US history.

Charities emerged in the United States during the second half of the nineteenth century, becoming the phenomenon they are now only after World War II.

One reason for their sudden and immense growth was the introduction of financial incentives in the 1950s, in the form of tax write-offs. As a consequence, thousands of charities were founded in the years that followed, rising from 12,000 in 1940 to 367,000 in 1967.

Some of these new charities were created by rich families as a means to pass on their wealth to posterity without paying estate tax.

This explosion was further fueled by the government policies of the 1960s and 1970s. This period saw the creation of numerous government-funded nonprofits, such as Medicare, the National Science Foundation and the National Institutes of Health.

But it's not simply the existence of charities that is relatively new, but also the public support they enjoy.

Traditionally, until the end of the nineteenth century, Americans were quite skeptical of charitable organizations. For instance, in the early years of the American Republic, some states, such as Virginia, made it discouragingly difficult for institutions like universities to operate on private donations or benefit from tax exemptions.

However, the Great Depression and World War II changed all that: the expansion of the welfare state in the aftermath of the Depression and the economic advantages of establishing charitable organizations made the concept of nonprofit initiatives more popular.

But isn't this growth in charity a good thing? As you'll see in the following blink, charities aren't always as good as we imagine them to be.

### 3. The ineffectiveness of charities is concealed by emotionally impactful photo ops. 

Charities gladly depict their projects as success stories: everyone has seen the stock photographs of smiling children in a newly built school, or a poor African village celebrating the construction of a well. However, these inspiring images often conceal a depressing reality: the ineffectiveness of charities.

Charities are ineffective because, all too often, they don't focus their energy on the right problems. In most cases, charities provide short-term solutions to long-term problems, and the effectiveness of their projects isn't backed by careful testing.

Consider, for instance, that many charities devoted to supplying water to crisis zones drill new wells, but don't maintain them. As a result, the wells become unusable after only a few years.

But _why_ don't they finish what they start?

Part of this is tied to their need to create compelling images that ensure that donations continue flowing in — and maintenance work simply doesn't provide these images. Who gets excited about maintenance crews performing routine tests? No one. Now contrast this with the emotional impact of seeing the happy faces of locals drawing fresh water from a well for the first time. In terms of emotional impact, the short-term project is the clear winner.

However, despite this short-sightedness, charities still receive generous funding from both the public and the government. Indeed, for most people, it doesn't seem to matter whether a charity actually demonstrates effective work. We (and our public officials) are happy to donate anyway.

For example, _Drug Abuse Resistance Education_, or DARE, a program which informs children about the dangers of drugs, has been shown to be ineffective as a drug prevention program. Nevertheless, its expenses — which include donations, federal grants and the salaries of the police officers that implement the program — amount to approximately _$200 million per year_.

> _"In this results-obsessed country, the public rarely demand measures of how effective charities are."_

### 4. The definition of “charity” is so loose that organizations don’t even have to serve the public good to carry the label. 

What are the names of some of your favorite charities? _Greenpeace_? _Red Cross_? _The World Wildlife Federation_? What about the _New York Stock Exchange_ (NYSE)? Wait, what?

That's right: technically, the NYSE, the cornerstone of the US financial system, is classified as a nonprofit organization. How is this possible?

For starters, there is little control regarding what constitutes a charity. This was exemplified by one Stanford University study, which found that the _Internal Revenue Service_ (IRS), which determines who is eligible for tax exemptions, has approved 99.5 percent of _all_ applications in recent years.

The reason for this lack of control is that the legal definition of "charity" is so nebulous. Among other things, charities are defined as any organization that is "beneficial for the community" — a wording which is subject to broad interpretation.

As a result of this blurry definition, even organizations that resemble fully fledged for-profit corporations or which don't actually do much good can be classified as charities.

For instance, many hospitals that initially provided services to the needy now implement the same practices as for-profit hospitals, yet maintain their classification as "charities." This includes hospitals which act _uncharitably_, enforcing aggressive debt collection to recover the costs of treatments.

Strangely, these "charitable" hospitals are more likely to be profitable than for-profit hospitals. In fact, according to the _Wall Street Journal_, 77 percent of nonprofit hospitals were profitable in 2008, compared to only 61 percent of for-profit hospitals.

Another example is the _Allstate Sugar Bowl_, which organizes an extremely popular annual football game. This "charity" organizes only one game per year and pays out more than $150,000 on executive meeting expenses, plus $500,000 to subsidize fans' travel expenses. That's a far cry from what most people would consider a truly charitable deed.

> _"Charitable hospitals have become some of the most aggressive debt collectors in the country."_

### 5. Without some kind of audit, there’s no way to thwart fraud. 

After the 2010 earthquake in Haiti, many charitable organizations flooded TV and social media with devastating images, but not all of them had good intentions. All the money raised by one group, the _M.E Foundation_, flowed not to the refugees but straight into the pockets of conmen.

Unfortunately, fraud is not uncommon in the charity world. In fact, the nonprofit industry is littered with fake organizations which don't really help anyone, choosing instead to hoard the money for themselves.

For instance, the former _US Navy Veterans Association_ (USNVA) supposedly provided support to some 66,000 Navy veterans, and had raised $100 million since its foundation in 2002. As it turned out, the USNVA did _not_ have the 66,000 members it advertised, and was in fact a one-man operation run by someone called Bobby Thompson. The USNVA spent only one percent of its money helping veterans; the rest presumably ended up in Thompson's wallet.

What's more, sometimes people even steal from legitimate charities. Andrew Liersch was CEO at _Goodwill Industries_, a charity that provides job training and other services to people with disabilities. During his tenure there, he diverted more than $20 million from the charity by rerouting and selling the best goods from their stores.

But how is fraud on this scale even possible?

Essentially, there are too few control mechanisms both within and outside the charities to ensure transparency.

The IRS, for instance, has suffered cutbacks since the 1970s, leaving it ill-equipped to control nonprofit organizations. In fact, _only one tenth of one percent_ of charitable returns are actually examined by the IRS.

The individual states, too, are no less guilty: only 13 states have some form of charity oversight.

As for internal control, charities rarely have finance committees scrutinizing their accounts. One Father Gray of a nonprofit parish in Waterbury, Connecticut, was the sole keeper of financial records, making it easy to steal money as long as he could eschew meetings with the church's financial hierarchy.

> _"The charitable world includes many organizations that tread the blurry line between legitimate and illegitimate."_

### 6. The involvement of donors can also be an impediment to more effective charities. 

You can probably name at least one celebrity who has donated to a "worthy cause": Madonna, Tom Cruise, Gwyneth Paltrow, etc. However, despite the apparent selflessness of their acts, it's worth asking how effective their big donations actually are.

Donors tend to respond to personal requests and immediate appeals without really considering whether their contributions will have a broader impact on the crisis in question. Think of all the charity advertisements you've surely seen with personal appeals from people affected by a natural disaster. This form of advertising is so popular because people are more likely to donate to an _individual_ whose suffering they've witnessed than an entire village, because it's harder to create an emotional bond with one of those.

Compounding this problem, people donate in order to reap personal benefits, such as prestige or the simple joy of giving. Because their charitable gifts are based on gut feelings and emotions, they are unlikely to analyze where money is needed most.

As a result of this impulsive behavior, the direct involvement of donors in managing charities isn't necessarily positive.

In fact, sometimes the stipulations they impose on donations can divert charities from their fundamental goals. For example, Joan Kroc, widow of _McDonald's_ founder Ray Kroc, once donated a staggering $1.5 billion to the _Salvation Army_. However, her donation included a plan to build high-end community centers complete with swimming pools and membership fees at $99 per month.

Clearly, these centers didn't match the mission of the Salvation Army — providing food and services to the poor.

In addition, donors, especially celebrities, often establish new charities, thus incurring extra costs for new offices and employees which can ultimately lead to failure. Madonna's charity, _Raising Malawi_, was founded in order to build a girls' school in Malawi, yet ended up spending nearly four of $7 million in donations on office space, salaries and architects.

This money would have been better spent by donating to an established charity with existing infrastructure.

### 7. Charity CEOs may earn millions per year – amounts which do not always reflect their achievements. 

Many would agree that it's important to reward a job well done. Yet in the charity sector, shoddy executive work can be handsomely rewarded.

Over the last ten years, charity CEOs' pay has increased at a rate of approximately seven percent per year. This trend in rising salaries — and high executive salaries, period — is especially common among charities that operate like for-profit organizations.

For example, the average salary for CEOs at elite US charitable hospitals is $1.4 million.

But even "real" charities sometimes grant high rewards to their CEOs. Bishop Eddie Long, the pastor of the New Birth Baptist Church, which has the largest congregation in Georgia, at one point earned $1 million per year through one of the charities associated with the church. That's the same amount that the organization actually distributed for good deeds.

But why are CEO salaries so high in charitable organizations? One reason is the lack of checks and balances.

In its entire history, the IRS has only penalized ten out of two million charities for excessive compensation. This is because the definition of "excessive salary" is very lax: any salary is considered acceptable so long as it's comparable to the salary for a similar position in a different organization — either non- or for-profit.

One charity CEO even justified his salary by comparing himself to Oprah Winfrey!

Setting aside the possible moral implications of obtaining such huge wealth from supposedly charitable organizations, the problem isn't that the salaries are high _per se_, but rather that they aren't proportional to the success of the charity. The logic behind high salaries is that they attract bright minds; if high pay means material reductions in poverty or homelessness, then high pay becomes a wise investment.

But that's obviously not the case!

Clearly, charities are knee-deep in problems caused by poor accountability and efficiency. These final blinks will look at how to solve them.

### 8. Effective charities adopt strategies from the for-profit world. 

Even though the nonprofit sector is riddled with problems, we can still find cases of good management.

Indeed, successful charitable organizations all share a common denominator: they implement strategies that resemble those of for-profit organizations, such as planning ahead, introducing innovative strategies and scrutinizing results.

Take the charitable organization _Youth Villages_, which started out as a home for troubled youth and managed to both stay afloat and develop into a major charity in the United States.

During times of financial trouble, the manager of Youth Villages was able to successfully hold off creditors, and pleaded with banks for small loans in order to get back on track.

And when Youth Villages realized that their previous methods for reducing recidivism weren't working, they opted to implement an entirely new strategy to prevent troubled teenagers from relapsing into crime, which addressed not only the kids themselves, but also their families.

Both of these measures represent the kinds of flexibility found in any for-profit organization in which the company was in dire need of saving.

One way to drive these kinds of improvements in existing charities is through something called an "_investment-and-support_ " charity. These are nonprofits that don't create their own charitable program, but instead identify "good" existing charities and provide them with training and infrastructure to achieve their goals.

For instance, _Invest in Kids_ (IIK) gave Incredible Years _,_ a program that trained teachers and parents to reduce childhood aggression, the infrastructure necessary for initial training and for research into the effectiveness of their program.

IIK specifically chose Incredible Years because it had already proven itself effective with over 20 years of carefully controlled tests. And as a result of IIK's funding, the then-unknown Incredible Years program was given the opportunity to provide an early childhood mental health program to a state that needed it: Colorado.

### 9. The key to responsible social investment lies in doing appropriate research and improving controls for charities. 

It's difficult to determine what makes a good charity. However, there are some uncontroversial measures, which, if adopted by government and individuals, will lead to better use of our social investments.

First, both individuals and governments should examine whether they are donating to the right charities. Donating to a particular charity because a friend or a relative recommended it to you isn't good enough. Instead, you should look for signs that the charity is actually doing good work.

Responsible charities are ones that announce clear goals and post results on their websites. They also demonstrate proper management by showing growth over time.

Furthermore, individuals would be better equipped to make the right choices regarding charitable donations if they first established institutions comprised of experts to monitor and evaluate the effectiveness of existing charities.

You could think of these experts as being the same as financial advisors for mutual funds or retirement plans, except that they advise you in the nonprofit sector.

In addition, the government should take an active role in improving charity control. One great place to start would be by providing a clear definition of what constitutes a charity so that _de facto_ for-profit organizations are forced to pay taxes.

This clarification would be the downfall of exploitative "charities," like hospitals which extort money from the uninsured under the pretense of doing charitable work.

Finally, the government should step up its efforts to identify fraudulent charities by applying the law more stringently and allocating more resources to the IRS.

None of these changes are in any way earth-shattering, but would nevertheless have a great impact on the effectiveness of charities, and ultimately the donations you make.

### 10. Final summary 

The key message in this book:

**Even if charities are well-intentioned, many are plagued with inefficiencies. As a result, they often fail to address the problems they set out to solve. Without proper controls to stop fraud and abuse, today's booming charity industry is in serious need of an overhaul.**

Actionable advice:

**Research the charities you donate to.**

Visit the websites of your favorite charities and make sure that they have a clearly worded mission statement. Look for specific projects that aim to achieve specific results, and make sure there's no pie-in-the-sky fluff. This way, you can ensure that the money you give is going to a real project and will help real people, and not just give you warm fuzzy feelings.

**Suggested** **further** **reading:** ** _The_** **_Blue_** **_Sweater_** **by Jacqueline Novogratz**

_The_ _Blue_ _Sweater_ is an autobiographical look at the author's travels in Africa and how they helped her understand the failures of traditional charity. These blinks also outline why a new type of philanthropic investing, called "patient capital," developed by the author, may be part of the answer.
---

### Ken Stern

Ken Stern is the president of _Palisades Media Ventures_ as well as the former CEO of the nationally acclaimed _National Public Radio_, which he helped develop into a trusted global information powerhouse.

