---
id: 538f210b36323900075f0000
slug: without-their-permission-en
published_date: 2014-06-03T13:54:07.000+00:00
author: Alexis Ohanian
title: Without Their Permission
subtitle: How the 21st Century Will Be Made, Not Managed
main_color: DC4C2C
text_color: DC4C2C
---

# Without Their Permission

_How the 21st Century Will Be Made, Not Managed_

**Alexis Ohanian**

_Without_ _Their_ _Permission_ explores the author's success with his hugely popular website, Reddit, as well as other internet-related stories, and investigates what exactly it is about the internet that makes it such a wonderful and dynamic place. Along the way, the author provides entrepreneurs with advice that helped him to form his remarkably successful business.

---
### 1. What’s in it for me? Find out how to get your start-up off the ground. 

If you've been paying attention to the news at all in recent years, then you've undoubtedly heard of the many young entrepreneurs who have sold their internet-based businesses to make millions, and sometimes billions of dollars. What is it about the internet that makes this kind of success story possible?

_Without_ _Their_ _Permission_ explores what it is exactly that makes the internet such a dynamic and revolutionary place — ripe for entrepreneurship — and why it's worth fighting to keep it that way.

These blinks focus on the author's own experiences in co-creating Reddit, one of the world's most popular websites, and along the way they reveal some of the author's hard-earned advice to entrepreneurs about the best approach to creating their start-up — from designing their product to acquiring the funding necessary to make it a reality.

In these blinks, you'll learn:

  * how the author was able to turn Reddit into such a hugely popular and lucrative website;

  * why success stories like the author's are dependent on a free and open internet;

  * how one man revolutionized the travel industry simply by trying to solve a problem for his debate team,

  * and why a certain whale earned the name "Mr. Splashy Pants."

### 2. In order to create a product that people need, you have to start with a real problem. 

If you were going to found a technology-based start-up, what would you do? You'd probably begin by developing a great product and hoping that people would buy it, right? Unfortunately, it's really difficult to persuade people to buy a product that they don't yet know they need. In fact, it's easier to start the whole process the other way around.

Rather than starting with a product, begin by identifying a problem. This doesn't necessarily have to be a well-known problem; in fact, people don't always realize they even have an issue until you show them a better alternative to the status quo.

For example, Adam Goldstein, co-founder and CEO of the successful travel search engine Hipmunk, realized in college that he had a problem booking flights. He was responsible for organizing all the travel for his debate team, which had a small budget for attending competitions. In order to find the cheapest flights, he had to check several flights at several airlines for several different time slots, which was an incredibly frustrating as well as time-consuming task.

Once you have identified your problem, you can create your product around it.

Adam Goldstein's solution was a travel search engine that searches a number of airlines simultaneously and ranks them according to price and other factors, such as number of layovers and flight duration.

In 2010 he brought his problem to Reddit co-founders Alexis Ohanian and Steve Huffman. They then turned his idea into a reality, gave it the name "Hipmunk," and revolutionized travel searches in the process by saving people time and money.

Unsurprisingly, the product quickly took to the market; people realized the Hipmunk's advantages even if they hadn't thought of travel searches as a problem, and were thankful for the simplification.

### 3. Bring your product to the market as soon as it is good enough. 

Are you a perfectionist who spends long hours trying to work out every single possible kink in his product?

You are not alone: many entrepreneurs fall into the trap of spending too much time trying to perfect their products. But this is a fool's errand!

Simply put, you will _never_ "perfect" your product, so the time spent unnecessarily trying to reach perfection is essentially time wasted. Not only that, but most users aren't bothered by tiny errors, making "perfection" even less of a priority.

Instead, you should bring your product to market as soon as it's "good enough." Indeed, the term _minimum_ _viable_ _product_ — building the simplest possible solution to a problem and then immediately launching it — has become a cliché in the start-up scene. And for good reason: it's what you're supposed to do!

For example, the first version of the author's own news and social networking website, Reddit, was "good enough": a simple web page where users could submit links and see them listed on the front page based on their popularity. And it was later sold for millions of dollars.

After you've created a "good enough" product, you can later use customer feedback to fine-tune it. In fact, observing how users interact with your product will tell you all you need to know about where and how you should improve it.

When we look back to the first version of Reddit, we see it was quite simple. As time progressed, however, the founders refined their service by introducing a voting system to add a new dimension to their link rating system, as well as adding various sub-pages for special content categories.

The lesson to draw from this is that it's best to start small and build from there. Take a step back from the idea of perfection and focus on the real work that needs to happen in order get your solution out there.

### 4. The number one reason for start-up failure is running out of cash, so you need a secure source of funding. 

The number one reason why start-ups fail is lack of cash. Of course there are many other possible reasons why start-ups could collapse, such as poor strategy, bad leadership, or products that don't connect with the market, but often it simply boils down to running out of money.

This makes sense, since you need cash to run your business; you've got to pay your employees, cover logistics, find the rent for your office at the end of the month and so on. Once the well runs dry, you're finished.

So securing funding is absolutely essential. Luckily, while you won't be finding any money trees in your backyard, there are many sources of cash available to you.

One source of funding is venture capital firms. For example, the author and Steve Huffman got their funding for Reddit from Y Combinator, a _seed-stage_ _venture_ _capital_ _firm_. This kind of company offers nascent start-ups cash and guidance in exchange for part of the start-up's equity.

Of course, they don't just give out money to everybody. Instead, they carefully consider your concept and business plan, and only start writing checks if they think they will earn back a profit later.

For example, before they had the idea for Reddit, the author and Huffman were rejected by Y Combinator for an earlier idea. In order to receive funding, they had to change their concept to what would later become Reddit.

_Angel_ _investing_ is another source of funding. This is when one affluent individual (instead of a company) funds your project in exchange for equity. Some of these individuals can be found on the social network Angellist, where start-ups can create profiles to attract the interest — and dollars — of wealthy investors.

Unfortunately, actually procuring funding can be complicated and frustrating, and rejection is common. Nevertheless, it's crucial for your start-up, so don't give up!

### 5. You can't make your company succeed on your own; you need to build connections and get the assistance of others. 

Have you ever heard of the lone wolf who made his fortune in business without any help? Probably not. Success is a group effort, and connections — both in the business or out of it — play a huge role in your success.

Indeed, every encounter in the business world can become a worthwhile connection. So it's important when you're attending events like conferences that, instead of dreading hours of boring talks, you see them as opportunities to connect with people who can help you later on.

Moreover, making connections like these can help you develop _social_ _currency_. Like any currency, social currency is something you can earn and spend: you earn it by doing people favors, and spend it by asking for them.

For example, you could earn social currency by mentioning a connection's newly founded start-up to a journalist who wants to write about a related topic. This small act doesn't cost you much time, and makes those people more likely to help you out in your own time of need — for example, when you need media attention for _your_ product.

Furthermore, these connections can influence the success of your business in ways that had never crossed your mind.

For example, when the author met Rachel Metz, a reporter for _Wired_, he hoped she would publish an article about Reddit so that it would gain some publicity. The good news: when they met over lunch, they really hit it off. The bad news: Metz decided not to write about Reddit because she felt that it would be unprofessional, as she and the author had become friends.

Luckily, however, Metz mentioned Reddit to her editor, who then referred it to her business connections at Condé Nast, a mass-media company who would later purchase Reddit and make the author _very_ rich.

> **Never miss an opportunity to make connections.**

### 6. The internet can give smaller and traditionally less powerful communities the influence to enact change. 

After reading so much about web-based start-ups, you might be wondering why anyone would place so much focus on the internet over other communication media, like print or TV. What is it that makes the internet so special?

In essence, the internet harnesses the power of the many: one person alone is fairly meek, but a large group of people with the same agenda has the power to enact meaningful change.

For example, there are lots of animal lovers and animal rights activists out there who want to make a difference, but what would happen if a single person stood in front of a slaughterhouse with a placard? Probably nothing.

But what would happen if it were 300 people instead of one? That's why people join forces in organizations like PETA and Greenpeace.

That's the great thing about the internet: it connects people with the same agenda — sometimes even by accident!

For example, Greenpeace once started a campaign to raise online awareness of their effort to stop Japanese humpback whaling expeditions by tracking a whale on its migration. In order to generate interest within their own small online community, they took a poll to see what they should name the whale.

A few of the options were deeply meaningful, such as "Kaimana," which means "divine power of the ocean" in Polynesian. Also on that fateful list, however, was the name "Mr. Splashy Pants."

Somehow this poll ended up on Reddit, drawing in a much larger crowd than the usual Greenpeace crew. Due largely to the votes by Reddit users, "Mr. Splashy Pants" won!

Greenpeace had inadvertently created a much broader community that wanted to protect a specific humpback whale named Mr. Splashy Pants. This untraditional yet large community added so much publicity to Greenpeace's cause that it had enough weight to actually alter the Japanese government's whale hunting plans. In fact, they called off their whaling expedition.

> _"Global connectivity isn't just changing the way we do business; it's changing the way we think of value."_

### 7. We can use the internet to leverage support and financial capital. 

Support doesn't always come from single firms or individuals. Communities can also combine their budgets in order to support a shared cause, such as a charity.

Sometimes the best way for charities to reach their target communities is via the internet.

For example, Charles Best, a social studies teacher in the Bronx, had to spend $500 of his _own_ money just to keep his classroom adequately stocked with school supplies, an experience that many other US school teachers shared.

To offset this burden on teachers, he started a web-based program called DonorsChoose.org, where users donate small amounts of money to school projects of their choosing. His site utilizes a strategy called _crowdfunding_, through which a project is funded with the help of small donations made by many people.

Crowdfunding has distinct advantages over other traditional donation models.

For example, Charles Best's crowdfunded program is especially transparent, as individuals themselves, rather than an organization, allocate funds where they choose. Whether they donate $10 or $10,000, they actually know precisely where their money goes and what it is used for.

In addition, organizations like DonorsChoose.org are not as bureaucratic, and can therefore forgo a large portion of the administrative overhead that plagues larger, traditional charities.

In order to get donations, teachers or students simply upload a description of their project to DonorsChoose.org at virtually no cost and for very little effort. Then, once the donations are gathered, an employee at DonorsChoose.org simply purchases the needed items, such as computers or microscopes, and gives them directly to the teacher, thus keeping overhead costs to a minimum.

Furthermore, donors have the option to allocate 100 percent of their donation to the project or put 15 percent of it toward covering the costs of DonorsChoose.org, thus offsetting their expenses.

DonorsChoose.org serves as a great lesson for internet start-ups: using their system, they continued to secure more donations every year even during the financial crisis, when most donors and school systems had to tighten their belts.

### 8. Thanks to new media distributors like YouTube, small content creators are empowered like never before. 

Wouldn't it be nice if the people with the best ideas always found their way into the spotlight? Sadly, it doesn't quite work that way.

In traditional media like television, big production firms wield all the power. A perfect example of this can be found in Zach Anner, a young comedian with cerebral palsy who uses a wheelchair.

A few years ago, he participated in a competition to win his own talk show on the Oprah Winfrey Network. His unusual appearance and unique humor quickly earned him a large fanbase, and as a result, he was one of two winners given his very own show.

Sadly, after only four episodes, his show ceased production due partly to disagreements with the production company over content. Anner and the production company had different ideas for the show, and the production company had all the bargaining power.

However, video viewing platforms like YouTube can empower small content creators by giving them both a place to launch their content and find an audience, thus emancipating them from big production companies.

Anner, for example, ended up putting his videos on YouTube, and in doing so has the freedom to create the content _he_ wants to produce.

In addition, YouTube contributors can gain a larger audience whenever people "thumbs up" their videos due to YouTube's rating system. So not only does everybody who wants to make their content available to the public retain 100 percent creative control, but those with real talent also will actually attract an audience.

These days there are many talented YouTubers who continue to attract a growing audience in a way that is only possible because of the internet and the transformation of media. People are no longer limited to TV and movies — they can also find a huge variety of unique independent and commercial media on platforms like YouTube.

> **You don't always need a producer to find an audience.  

** If you have an amazing creative idea you can make it a reality. Consider a DIY approach using open media such as YouTube.

### 9. In order to remain a positive force for change in society, the internet must remain uncensored and open for everybody. 

So what actually makes the internet so dynamic and revolutionary? It's because it's a free and completely democratic space. Now, can you imagine what would happen if that freedom were somehow constrained?

The internet has, in fact, faced the threat of censorship by law.

For example, in 2011 a bill called the Stop Online Piracy Act (SOPA) was introduced to US legislature, touted as a solution to internet piracy and as a means to protect intellectual property rights.

However, it would have significantly limited online First Amendment Rights and paved the way for unprecedented internet censorship by enabling private parties to shut down websites without prior notification or a judicial hearing.

Fortunately, this attempt at internet censorship was thwarted.

Since SOPA threatened the open, uncensored internet that made the author's livelihood possible, he joined forces with a group of internet experts in a lobbying blitz to stop the bill from being passed.

The author believed that SOPA would not only make success stories like his own — in which he started a business with less money than it takes to buy a new Ford Focus and ultimately became a millionaire — impossible, but would also harm innovation and slow economic growth.

And it wasn't just these internet experts taking up the fight against SOPA. The news spread throughout the internet, and thousands of people organized protest actions against it, such as the Internet Blackout Day on January 18, 2012, where several important websites including Wikipedia and Google made their sites unavailable.

Due to these massive protests, SOPA lost many of its supporters and ultimately didn't make it through Congress. And it's no surprise either: according to the author, a free and open internet is one of the most important political issues of our times, and well worth fighting for.

### 10. Final Summary 

The key message in this book:

**The** **internet** **has** **provided** **individuals** **and** **groups,** **from** **charities** **to** **start-ups,** **with** **unprecedented** **exposure** **to** **an** **audience,** **meaning** **that** **anyone** **with** **the** **right** **attitude** **and** **solutions-oriented** **strategy** **can** **create** **a** **successful** **organization.** **This,** **however,** **can** **only** **be** **possible** **if** **the** **internet** **remains** **free** **and** **open.**
---

### Alexis Ohanian

Alexis Ohanian is an internet entrepreneur, founder of Reddit, investor and open internet activist whose book, _Without_ _Their_ _Permission_, is a _Wall_ _Street_ _Journal_ bestseller.

