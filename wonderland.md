---
id: 596c98bcb238e100060188b3
slug: wonderland-en
published_date: 2017-07-20T00:00:00.000+00:00
author: Steven Johnson
title: Wonderland
subtitle: How Play Made the Modern World
main_color: AFC3D6
text_color: 4575A3
---

# Wonderland

_How Play Made the Modern World_

**Steven Johnson**

_Wonderland_ (2016) argues that the role of play and fun in human history is undervalued. We have been told by history books that wars, revolutions and monarchs are the drivers of history, and we thus tend to overlook more mundane factors in favor of powerful figures and famous movements. However, the pleasure we derive from bone flutes, board games, the color purple or alcohol have likewise contributed greatly to invention and progress.

---
### 1. What’s in it for me? Learn how play and pleasure have pushed humanity forward. 

When you open a book in history class, you're quite likely to see some serious and dour faces: ancient philosophers debating democracy, Newton pondering the properties of gravity under an apple tree or poor garment workers in large factories. What you're less likely to see, however, are people just having fun.

Nevertheless, throughout history, great advances have been made because of people playing or seeking pleasure.

Just as war and conquest have affected human history, our childlike indulgence in the wonderland that is the world of games, bright clothing and delicious tastes have transported us to new horizons as well.

In these blinks, you'll find out

  * how a flute laid the foundations for future computers;

  * why sea snails drove early seafarers into the undiscovered Atlantic; and

  * how producing a single Dorito would once have been a year-long mission.

### 2. Humans’ hardwired desire for play has been an underappreciated driver of progress. 

When we think of innovation and progress in human history, the easy assumption to make would be that necessity or utilitarianism played a part at some point.

But in reality, many important inventions have been sparked in the crucible of play. A little bit of fun has gone a long way!

Consider the Banu Musa brothers. They were Islamic scholars in ninth-century Baghdad. As two of the best engineers of their time, they published groundbreaking work on mechanics and hydraulics in _The Book of Ingenious Devices._

They described their self-built machines and introduced principles that laid the groundwork for innovations such as the steam engine or the jet engine, which would not be built until centuries later.

Useful though their ideas became, the Banu Musa brothers began by just messing about. In fact, they spent most of their time trying to entertain others by constructing frivolous trinkets and toys.

They built automated dolls, self-playing instruments and even a mechanical peacock that dispensed water and soap when its feathers were pulled.

There's a powerful conclusion in this anecdote: fun and play have shaped history far more than one might assume — and for good reason; the human brain is predisposed to engineer discovery through play.

First of all, the brain loves surprises.

Whenever we encounter novelty, our brains give us a shot of the neurotransmitter _dopamine_, which provides us with a natural high. Consequently, we're wired to want to explore our surroundings and seek out new experiences. It's through this mechanism that we might be led to important discoveries or unique creations as a result of mere curiosity or happenstance.

Second, our brains just work differently when we're playing. We suspend our disbelief and our minds start to make previously unimagined associations.

It's in this freewheeling and playful mode that our minds are at their most creative.

In the next blink, we'll examine what discoveries humans have made while at play.

### 3. Explorations and playing with sound paved the way for programming and computers. 

What do you think of when you imagine the dawn of the computer age? Perhaps a large group of people in lab coats feeding punch cards into metal behemoths?

In truth, the foundations of our information age were laid in prehistory when early humans began to explore the qualities of sound.

Archeologists have uncovered mammoth bone flutes that are up to 50,000 years old.

And these instruments are still musically recognizable to the modern ear. They produce harmonically pleasing notes: fourth- and fifth-note intervals from the tonic, still the basis of many pop hits. Just hum the bass line from Summer Nights from Grease to yourself!

Most probably, these early sonic examinations were inspired by curiosity and play. Don't forget that the sounds that seem harmonious to us were at some point plucked from the cacophony of nature by our early ancestors.

What's interesting is what happened next: the desire to play with sound led to many more inventions.

Once again, the Banu Musa brothers were at the forefront of experimentation. In fact, by building what they called "the instrument which plays by itself," they essentially created the first programmable computer.

This was a machine that played the flute. At its core was a rotating cylinder covered with tiny pins. These moved levers that opened or closed the holes of a flute and produced melodies in the process. It's the same mechanism that old music boxes use.

Their true masterstroke, though, was that the cylinder could be swapped out with a new one that played a different tune. In other words, the machine was _programmable_ as the cylinders were individually coded. Think of it as the first use of hardware and software in human history.

Obviously, it's quite different from how you might imagine coding and programming. While this first basic mechanism has gone through a myriad of transformations, in a sense this is how computing began!

### 4. The pleasure we derive from colorful clothing triggered both early sea exploration and the Industrial Revolution. 

If you lived 4,000 years ago along the Mediterranean, the palate of colors you would have experienced would have been limited to earthy tones or naturally occurring colors. In contrast to the colors seen in rocks, the sky or the sparse vegetation, the bright tones found in briefly flowering plants or fruits were cherished.

It was one thing to experience a flash of yellow or red — but the rarest and most valuable color was purple. For many centuries, having clothes dyed purple was quite the fashion statement.

What you might not know is that it was this fad for all things purple led to the earliest explorations of the Atlantic Ocean.

This is because the only source of purple dye at that time was the inky secretion of the murex sea snail, native to the Mediterranean Sea.

But when local supplies became depleted, some courageous Phoenician sailors ventured into the turbulent Atlantic Ocean, which had previously been feared and avoided. Not only did the sailors find more snails, they also opened up the Atlantic for future generations to explore.

It was this same appetite for clothing dye that helped launch the Industrial Revolution centuries later in England.

In seventeenth-century London, such was the demand for colorful clothing among societal elites that imports of brightly colored cotton fabrics from India increased dramatically.

These fabrics were valued for a number of reasons, but in particular because their texture was softer than regular scratchy wool and they didn't lose their color after washing, which had until then been a common problem.

It was on the back of this cotton craze that British entrepreneurs set about creating ways to mass-produce cotton fabrics as cheaply as possible. And it was through the desire to meet this demand that the machine that would later power the Industrial Revolution was invented: the steam engine.

Despite its revolutionary capabilities, the steam engine began its life in the shadow of a foppish fad!

### 5. Our appetite for new exotic flavors sparked a global spice trade. 

Nowadays, we're accustomed to the easy availability of flavors and spices from all over the world.

Just think of the work involved in the production of Doritos chips. They are made from corn, sunflower oil, soybean, cheddar cheese, tomato, garlic and red pepper powder. To find these ingredients a thousand years ago, you would have had to travel around the world for a whole year.

That's obviously not the case now — but it was actually the pleasure derived from different spices that stimulated the growth of the first global trading routes.

Spices can't be grown just anywhere in the world, but rather only in specific locations. To move spices around, trading is essential, which is why it is a such an age-old practice. Archeologists in modern-day Syria have actually dug up cloves dating to 1700 BC.

But back then, cloves only grew on what were known as the Spice Islands, in modern-day Indonesia. This means that 3,700 years ago, global trade was already incredibly advanced. Most probably these spices had passed through the hands of Chinese, Malaysian, Indian and Arabian traders.

Cloves are pleasant enough, but the fiery taste of the peppercorn also helped shaped human history.

What is now found free of charge on many restaurant tables was, in Europe in the Middle Ages, sometimes more valuable than gold. It was a taste affordable only to aristocracy.

It was the pepper trade that made Venice, the major redistributor for pepper at the time, a rich and powerful city.

Pepper was even used as currency. People sometimes paid their rent in peppercorns, or made it part of a wedding dowry.

And it wasn't just its taste that made pepper so highly valued. Medieval aristocrats thought that it, along with other spices, had medicinal properties. In fact, spice-based remedies were used to combat everything from sexual dysfunction to insomnia, gas pains and depression.

### 6. Our love of optical illusions paved the way for cinema. 

From the time the first cave-dwellers made shadow silhouettes with their fingers from the light of a fire, optical illusions have intrigued humankind.

What began as the shape of a baying wolf on a cave wall started a chain of discovery that eventually ended in us watching the latest blockbuster at the multiplex.

But to get there, a couple of innovations had to be perfected. And in both cases, it was the joy of entertainment that led the way.

In the eighteenth century, a young German showman from Leipzig named Johann Georg Schröpfer pioneered a business based on optical illusions.

His idea was to gather a paying crowd in a darkened room and give them a horror show-like performance. He used a magic lantern to project the image of a hovering ghost against a wall of smoke; he projected an eerie and spectral voice across the room; he even used "special effects" by blowing stinky sulfur smoke into the crowd or giving spectators little electric shocks. The audience would have a fantastic time, so it was little wonder that Scröpfer soon became known as the "ghostmaker of Leipzig"!

Quite apart from this entrepreneurship, a rather more physiological discovery was critical to the invention of cinema — although it too came about as a result of the urge to entertain.

An important phenomenon, known as _persistence of vision_, was discovered by the creators of the thaumatrope, a popular toy in the nineteenth century. It refers to how our eyes trick us into seeing motion, even though we're just seeing a sequence of static images.

This is how the thaumatrope works. You spin around 12 related images, and if it's done quickly enough your eyes will perceive a moving image like, for example, a galloping horse. This is what the earliest motion pictures looked like.

To think that these fantastic inventions came about simply because we wanted to amuse and enthrall!

### 7. The games we play affect how we envisage society, though it may not seem so at first. 

One of the benefits of play is that it has limited real-life repercussions. But play can nonetheless subtly impact our relationship with the world, how we relate to one another or even how we picture society.

Let's consider one of the oldest games from this perspective: chess.

Chess pieces essentially represent different strata of society as it once existed: monarchs, bishops, knights and pawns all interact according to certain rules. This, is turn, influenced how people conceptualized the society around them.

This was indeed the case in medieval Europe; society was seen in highly stratified, almost physiological terms. The king was the "head" and in control of all other body parts, regardless of whether or not they were reluctant to obey.

Chess, however, imparted a different vision of society. Here, classes were independent. There were still rules, but whether bishop, knight or rook, each could move independently of the king. Ultimately, the way chess downplayed regal influence may have indirectly contributed to the revolutionary fervor the spread throughout Europe in the centuries that followed!

Another game that reflects a particular zeitgeist is Monopoly. Although it's now often seen as the epitome of unbridled capitalism, its inventor had quite another intention.

In 1904, Lizzie Magie created The Landlord's Game, Monopoly's predecessor. Her aim was to disseminate the radical egalitarian ideas of the economist Henry George. George was strongly committed to reducing inequality and poverty, and Magie was a keen follower.

George argued, for instance, that private property should be heavily taxed and that the collected wealth should be used for the common good. Therefore, Magie designed a version of the game in which the goal was not to amass cash and property, but instead to distribute money as equally as possible.

It's highly ironic, therefore, that a game that brimmed with socialist and anti-monopolist touches and ideals was later plagiarized to became the ultra-capitalist board game Monopoly that we know today.

### 8. Political and social movements were the results of innovative uses of space. 

So far, we've talked about historical innovation in terms of invention. But innovations that produce change in society don't have to come along in the form of steam engines or computers. Mundane innovations have profound effects too — just think of your local neighborhood bar.

The bar essentially revolutionized the role of public space for humankind. It was a completely new form of space and became the birthplace for many political and social movements.

When you think about it, a bar is an appealing mix of home comforts and public space. It's separated from the street, but still accessible. It's somewhere to forget the grind of daily existence, at least for a few hours, and is a place to exchange ideas, debate or even network.

It's with good reason, then, that the bar became the perfect spot for people to assemble and form political movements. For instance, the Boston Tea Party was planned in a tavern called the Green Dragon. And it was at the Black Cat Tavern in Los Angeles that the LGBT movement first found its political legs, since it was one of the first bars where gays and lesbians could openly meet.

Another innovative use of space might amaze you. Nature! Prior to the seventeenth century, nature was seen as wild and savage and certainly not welcoming. Mountains in particular were considered eyesores, so much so that some people who crossed the Alps even asked to be blindfolded!

It was only with the onset of Romanticism, in the late eighteenth and early nineteenth centuries, that nature was thought of as aesthetically pleasing. Poets and painters pushed the idea of _sublimity_ in nature to the fore.

And we have a lot for which to thank the people who reimagined nature as a delightful pleasure; they're ultimately responsible for the fact that our concrete cities are dotted with verdant urban parks and lush foliage.

### 9. Final summary 

The key message in this book:

**Our hardwired hunger for surprise and delight are underestimated forces in human history, because it is while playing that humans are at their most creative. Many of humankind's defining discoveries and radical innovations were originally meant for entertainment purposes — but were later found to be useful for more serious or practical ends.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Where Good Ideas Come From_** **by Steven Johnson**

_Where Good Ideas Come From_ examines the evolution of life on Earth and the history of science. This _New York Times_ bestseller highlights many parallels between the two, ranging from carbon atoms forming the very first building blocks of life to cities and the World Wide Web fostering great innovations and discoveries.

In addition to presenting this extensive analysis, replete with anecdotes and scientific evidence, Johnson also considers how individual and organizational creativity can be cultivated.

**This is a Blinkist staff pick**

_"I love these blinks because they combine so many different layers of abstraction, from carbon atoms combining to form molecules to entire cities buzzing with creative collisions."_

– Ben H, Head of Editorial at Blinkist
---

### Steven Johnson

Steven Johnson is the best-selling author of ten nonfiction books. These include _How We Got to Now_, _Where Good Ideas Come From_ and _Everything Bad Is Good for You_. He is also a regular contributor to the _New York Times,_ the _Wall Street Journal_ and the _Financial Times._

