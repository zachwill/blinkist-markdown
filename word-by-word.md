---
id: 5a80e500b238e100072f701d
slug: word-by-word-en
published_date: 2018-02-13T00:00:00.000+00:00
author: Kory Stamper
title: Word by Word
subtitle: The Secret Life of Dictionaries
main_color: 74B4AD
text_color: 318F84
---

# Word by Word

_The Secret Life of Dictionaries_

**Kory Stamper**

_Word by Word_ (2017) is about an object, and its associated profession, for which people rarely spare a thought: dictionaries and the honorable occupation of lexicography. Kory Stamper introduces the fascinating world of word classification through her own experience at Merriam-Webster, showing what dictionaries can do and, just as importantly, what they don't.

---
### 1. What’s in it for me? Uncover the dictionary’s hidden secrets. 

Since we use them constantly, every day of our lives, it's easy to take words for granted. Particularly now, in the age of the internet, the word lists, companions, lexica and dictionaries that have been around for centuries may seem hopelessly outdated.

But nothing could be further from the truth. Dictionaries and the work of lexicographers in particular represent an ongoing attempt to understand what humans mean when we communicate.

While people often have no idea how dictionaries are created, the process of compiling a dictionary — also known as lexicography — is much more complex than you might expect. Once you understand what lexicography involves, your assumptions will be challenged not only about language and grammar, but about how we engage with and understand the world around us.

In these blinks, you'll learn

  * what the dictionary can and can't tell you about love;

  * what a snollygoster is, and how it created controversy; and

  * why lexicography isn't for chatterboxes.

### 2. For people whose job it is to research words and communication, lexicographers sure love silence. 

Choosing a career path is never easy, and it's something a lot of people struggle with. But how's this for an idea: the discipline of lexicography, or dictionary writing.

Lexicographers are a rare breed. After all, dictionary writing is not an expanding industry; just think of the last time you bought a dictionary, or even used one. It was probably years ago, if you can even remember.

But then again, lexicography was hardly a mainstream profession even in its heyday. Those who became lexicographers often didn't originally plan to do so — mostly because not many people know about lexicography in the first place!

However, if a job does open up, and you're one of the rare few who finds this kind of work appealing, then there's a good chance of getting it. The only things lexicographers need are a good command of English and a university degree of some kind — any kind.

It really doesn't matter what you studied, as there's no limit to the diversity of the English language. If you're about to define a load of mathematical terms, then a mathematician rather than an English major is much more likely to cut the mustard.

All this means that lexicographers are a rather mixed bunch, from sports enthusiasts to queer studies specialists.

That being said, lexicography is not an ideal profession for social people, as it often involves sitting alone in absolute silence for hours at a time.

In fact, when the author applied to work at Merriam-Webster, she was specifically told not to expect social interaction. She was brought to the editorial floor where everyone was busy beavering away in individual cubicles. The silence was deafening.

And this is exactly what makes it the perfect job for a linguistic nerd. You'll get to sit in silence thinking about words all day long.

But it's not all about words. In the next blink, let's look at the role of grammar in lexicography.

> _"Office chitchat of the sort you're used to is not conducive to good lexicography."_ — Fred Mish, former editor-in-chief of Merriam-Webster

### 3. Grammar is much more complex than it appears at first glance. 

You might think that because your sentences come festooned with bells and whistles, and because you can delicately balance clauses like some sort of Jenga master, that you're a grammar whiz through and through.

If that's the case, you're mistaken as to what grammar actually involves.

Grammar gets complex quickly, and for good reason: there are no simple answers and no absolute rules.

Lexicographers realize this early on in their careers, since they have to categorize words according to the eight recognized parts of speech: noun, verb, adjective, adverb, conjunction, interjection, pronoun and preposition.

As it turns out, it's the smallest words that are the trickiest and most finicky to classify.

As an example, consider the word "but" in the following sentence: "What can they do but try?"

After much consideration, the author decided that the sentence had a clause — "they try" — with an implied subject, "they." Consequently, "but" was a conjunction joining two clauses.

But a colleague of hers suggested an alternative. "Try" could be a hidden infinitive, as in "What can they do but [to] try?". In that case "but" would be a preposition.

There's no easy answer to a conundrum like this, and speculation can only get you so far.

As such, lexicographers have to fudge things a lot of the time.

The author noticed one day that the word "the" had been categorized as an adjective in the abridged dictionary she was working on.

She was sure this was a mistake, so she asked her boss about it. He argued that "the" did, of course, modify nouns, however slightly. More importantly, it was tradition; "the" had been classified as an adjective since the nineteenth century, presumably because no one had been able to work out how else to fit it into the rubric.

Let's look at a few more common misconceptions about grammar.

### 4. There’s no right or wrong when it comes to grammar usage. 

The word "grammar" alone is enough to instantly conjure up images of stern school mistresses chastising their pupils for using "who" instead of "whom."

In reality, grammar isn't about stylistic rights and wrongs — and it's certainly not about judging others.

People who aren't familiar with linguistics and lexicography often tend to assume that there's correct grammar and incorrect grammar, and that it can thus be used to judge and criticize others.

These kinds of people will leap with blue pencil in hand at signs in a supermarket saying "ten items or less." They scoff, noting that it should say "ten items or fewer."

But this isn't what grammar is about. Grammar should actually help open your mind to language and its use. In the end, getting to grips with grammar is about facing your own linguistic prejudices.

The author had to do just that when taking part in a tutorial on style and definitions at Merriam-Webster. She was asked whether "good" was an adjective or an adverb.

Memories of her school teacher insisting that "I don't feel well" should be used instead of "I don't feel good" came flooding back. Of course, the author answered that "good" must be an adjective.

But the tutor countered. Had the author ever used the phrase "I'm doing good"? Indeed she had, just a few minutes earlier. How had that slipped her mind?

Of course, in that sentence, "good" is used as an adverb.

In fact, the evidence shows that "good" has also been used as an adverb for at least 1,000 years. It is both an adverb and an adjective.

And that is precisely what lexicographers do. They record a language as it exists, not as people think it should be or would look in an imaginary rulebook.

However, lexicographers still sometimes have strong views about correct usage, and even the dictionary itself can challenge their most fundamental beliefs as to what defines correct language.

### 5. Words considered to be “wrong” can turn out to have forgotten meanings. 

Ever since the late nineteenth century, Merriam-Webster has welcomed correspondence from its readers. Sometimes readers' quibbles about linguistic matters lead to some very unexpected debates.

What's particularly fun are those occasions when words that are often thought of as "wrong" turn out to be absolutely fine.

One day the author received an email that claimed there was a mistake in the Merriam-Webster dictionary. Supposedly, the entry for "irregardless" didn't measure up.

The correspondent had a point; after all, at first glance the word is clearly nonsense. The negating "ir-" prefix serves no function, and the word means exactly the same thing as "regardless." When they write it or say it, people are most likely just blending together "irrespective" and "regardless" in their heads. Yet most dictionaries allow its use and include an entry for it. Merriam-Webster itself states that the word is nonstandard and that "regardless" is preferred — but it's still in the dictionary all the same.

The debate over irregardless was reignited by a Mississippian who wrote to the author and claimed that "irregardless" was used as the superlative, heightened version of the adjective "regardless" in his state, similarly to how "nicest" is the superlative of "nice," or "best" is the superlative of "good." The author scratched her head and asked herself if this could really work the same way with "regardless" too.

She couldn't be sure one way or another, but it prompted her to hunt for possible evidence, as far back as she could. Amazingly, there were plenty of citations in Merriam-Webster's collection dating back to the nineteenth century.

While in some instances, the use of "irregardless" could be interpreted as an intensifier, the exact meaning of the word remains open to debate. The most common explanation for the word's meaning is that it is a mixture of irrespective and regardless as in the following Merriam-Webster dictionary citation:

"I told them that irregardless of what you read in books, they're some members of the theatrical professions that occasionally visit the place where they sleep." (Ring Lardner, 1921)

### 6. There are two major kinds of defining, which can confuse dictionary readers. 

You might think a dictionary definition would aim at completeness, so that readers could really get to the heart of the matter when they look up a word. But that isn't the case.

There are actually two types of defining: _lexical defining_ and _real defining_, and lexicographers limit themselves, naturally, to the former.

Real defining is the task of philosophers, theologians and other experts. They attempt to understand and describe the essence of a given object or concept. They might, for example, ruminate on the nature of love or truth. Alternatively, they might devise philosophical riddles, such as, "Do sounds exist when no one hears them?"

This was the kind of work the author initially thought she would be doing — but it wasn't to be. Her task was limited to lexical defining, namely describing word usage and accepted meanings. Nothing else.

So instead of getting to the bottom of understanding what love is, lexicographers deal with more mundane affairs. For instance, what does the word "love" mean in the specific context of someone claiming to love pizza?

For the lexicographer, the difference between lexical defining and real defining is clear, but dictionary readers can get confused, since people often consult dictionaries looking for real definitions.

They might look up "love," expecting to learn what love really is. They're unlikely to care that "love" is often used as a synonym of "like," or as an expression of "strong affection."

You can sense readers' frustration in the comments under "love" in the online Merriam-Webster dictionary, where people vent that God is love, or that love is so much more than the dictionary definition.

But, lexicographers aren't making their definitions with such high aims in mind. They're not teasing out the exact relationship between love and affection, they're just saying that the word "love" is often used to describe feelings of affection.

### 7. To be listed in the dictionary, words have to meet three criteria. 

It might seem counterintuitive, but dictionaries don't aim to be comprehensive; the fact of the matter is that not all words make it in.

For a word to be listed in the dictionary, it has to have both wide currency and must also have been used for a long time.

The first criterion is that a word has to have appeared regularly in print before it stands a chance of being considered. For instance, if a specialized magazine like _Wine Spectator_ throws out a few choice enological terms every now and then, this won't be enough for those words to be included in the dictionary; they are just too marginal.

To be considered for inclusion in the dictionary, the word would have to appear in several other publications as well.

The second test is a little trickier. Since the word has to have been around for a good while, there's no place for words that are short-lived fads.

But it is often hard to determine or guess a word's popularity.

Take "snollygoster" as an example, which means a person without principles. It had once been popular in the nineteenth century, and thus earned its spot in the dictionary at that time. But as it fell out of use, by the early 1980s, an editor at Merriam-Webster decided to remove it so more popular terms could be introduced.

But then, out of nowhere, a TV pundit started using "snollygoster" in the 1990s to describe a controversial politician. "Snollygoster" had once more entered the popular discourse, but when people looked for it in the dictionary, they found nothing. It was a tad embarrassing for Merriam-Webster.

Finally, a word has to be meaningful to enter the dictionary. While this may seem like a redundant criterion, consider the word "antidisestablishmentarianism." No one actually uses the word, and it's only cited when people want to make lists of long words in the English language.

That said, it does sneakily find its way into less stringent dictionaries!

A definition, however, is only one part of a word's dictionary entry. A lexicographer also needs citations, which we'll take a look at now.

### 8. It’s hard to find good citations to use in dictionaries, but making up examples can be problematic too. 

Plenty of wise, intelligent people have uttered intelligent and wise words. So you might think finding quotations for the dictionary would be a breeze.

In reality, finding decent quotations — or citations as they're known in the industry — for dictionary entries is hard work.

Citations need to fulfill three criteria for lexicography purposes. First, they should illustrate the most common usage of a given word; second, they should only contain words that are listed elsewhere in the dictionary; and, most importantly, the examples should be numbingly boring.

They have to be dull for a reason: lexicography does not aim to entertain. Examples shouldn't distract or detract from definitions, so it's best for them to be direct and to the point.

Consider the definition of "pragmatic," which is "relating to matters of fact or practical affairs."

Merriam-Webster does quite well to eclipse the tedium of that definition with its citation. It quotes K.B. Clark. "_Pragmatic_ men of power have had no time or inclination to deal with . . . social morality."

There's an alternative to this search for a word's prior usage in literature or journalism, though. Lexicographers can also create their own examples — but there are other, similar problems inherent in that exercise.

Lexicographers, like authors, love language. As such, they're often tempted to create exciting examples; in fact, some lexicographers have even succeeded in sneaking some purple passages into the dictionary.

For instance, under Merriam-Webster's entry for "portly," in the sense of "dignified," the example sentence goes right out on a limb with "[she] walked with the portly grace of the grande dame that she was."

Idiosyncratic entries of this kind are rare, however, as copy editors would normally be quick to whittle phrases like this down to something like "she walked with portly grace."

In the final blink, let's take a look at those rare occasions when lexicography gets political.

### 9. Lexicographers carry a certain authority, which means their work occasionally becomes politicized. 

If anything really excites a lexicographer, it's when the time comes for _dictionary revisions_. New words are introduced, others removed, and it's lexicographers who get to make those decisions.

It's a task imbued with great responsibility; dictionaries still carry a certain authority in the eyes of their readership, even if they don't actually indicate if words are "correct" or "moral."

Consequently, dictionary revisions can be controversial.

When Merriam-Webster published the eleventh version of its Collegiate Dictionary in 2003, it included no fewer than 10,000 new words. And some entries really ruffled a few feathers.

For instance, the slang word "phat," meaning "very enjoyable," as in "a phat music beat," was met with cries of derision — how could such a word be deemed worthy of inclusion?

Some changes were more discreet. Under "marriage," for instance, a new entry was included for same-sex unions. The editorial team expected an immediate backlash, but for a good while, no one noticed.

Then on March 18, 2009, the controversy exploded. The author found herself in the middle of a political maelstrom, as conservative media outlets had finally noticed the change to "marriage" in the dictionary and orchestrated a write-in campaign against Merriam-Webster.

The author's inbox was flooded with hundreds of emails complaining that the inclusion of same-sex marriage in the dictionary somehow legitimized it.

Of course, this is nonsense. Dictionaries are not bastions of society's moral compass, they simply reflect word usage. In this case, the entry merely reflected the fact that the phrase "same-sex marriage" was appearing more frequently in print.

Therein lies the great truth behind lexicography as revealed in these blinks. The discipline may appear to epitomize lofty research from within the ivory tower, and may even seem like a strange expert science to outsiders. But, in the end, it's really just about reporting how ordinary people use language in the most banal and typical of day-to-day situations.

### 10. Final summary 

The key message in this book:

**Dictionaries aren't something we think about that often. It can seem that those dusty tomes on the shelf are long-ossified compilations of little relevance to our changing world. In truth, these compendiums are more complex than they appear at first glance. They are living collections that challenge our preconceptions and prejudices about language, politics and grammar. And lexicography also happens to be a dream job for language nerds.**

Actionable advice:

**Turn over a new leaf and crack open those forgotten spines.**

There's a lot of joy to be had in the haptic experience of reading a book, and dictionaries are no exception. Looking up a word online is all well and good, but you can also learn a lot by seeing a word in context on a dictionary page. You'll learn what related words exist, or your eye might catch something you've never seen before. Who knows? You might even get a kick out of learning about a word's etymology.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Sense of Style_** **by Steven Pinker**

_The Sense of Style_ (2014) offers a refreshing and relevant guide to writing potent, readable texts of all kinds. Instead of extolling the same confusing and sometimes counter-intuitive rules found in traditional style guides, _The Sense of Style_ offers simple tricks and heuristics guaranteed to improve your writing.
---

### Kory Stamper

Kory Stamper works as a lexicographer for Merriam-Webster. She is also a popular blogger whose writing has appeared in the _Guardian_ and the _New York Times_.

