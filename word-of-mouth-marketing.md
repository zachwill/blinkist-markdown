---
id: 534d4abc6633610007190000
slug: word-of-mouth-marketing-en
published_date: 2014-04-15T06:35:38.000+00:00
author: Andy Sernovitz
title: Word of Mouth Marketing
subtitle: How Smart Companies Get People Talking
main_color: None
text_color: None
---

# Word of Mouth Marketing

_How Smart Companies Get People Talking_

**Andy Sernovitz**

These blinks explain how generating positive word of mouth has become such a powerful marketing tool that it's even more effective than traditional advertising placements like television. Through examples and clear guidelines, the following blinks describe exactly how you can facilitate positive word of mouth for your products.

---
### 1. What’s in it for me: get your customers to do your marketing for you. 

Everywhere you look these days you'll see advertisements: "This car is reliable," "This computer is fast," "These marshmallows are tasty," and so forth.

But if you think about it, what reason do you have to trust these adverts? After all, they've been made by companies whose sole aim is to get you to buy the products.

Thankfully, these days you can also go online and read the opinions of people just like you. And you might find out that the car breaks down after 500 miles, the computer takes ten minutes to start up and the marshmallows taste like compost.

This increased availability of information has completely changed the marketing game: nowadays companies should pay more attention to the word of mouth their products are creating than to the image they are meticulously crafting through their own adverts.

In these blinks you'll discover why word-of-mouth marketing is so crucial, and what you can do to encourage it.

You'll learn, for example, why some car companies may be better off admitting their cars are ugly than touting them as beautiful.

You'll also find out why something simple like repainting a dentist's practice can get people talking about it for its entire existence.

### 2. The internet means consumers have a powerful say in the image of a product. 

Do you remember what marketing was like before the internet?

In those days companies were able to meticulously craft an image of their brand and products through careful advertising and marketing. What an individual thought of your product was quite irrelevant, as even if they didn't like it, they had no way of telling others about it. In short, the image the company crafted would endure.

These days, however, are coming to an end, as the emergence of digital media like the internet have shifted more power toward consumers.

Nowadays consumers worldwide can voice their opinions on a company and its products on, for example, personal blogs and social media. Countless other consumers will see those reviews, and this will greatly affect the image of the company in their eyes.

One driver of this shift is that the media that people follow have changed: many now prefer blogs and online communities over traditional newspapers, which means that online reviews can attract more viewers than advertisements in traditional media.

This makes it impossible to control the image of a product. No matter how flawless the image a company has crafted, one simple internet search can show what people really think of it.

The power of individual consumers' opinions is further emphasized by the fact that customer reviews are seen as more trustworthy than traditional advertisements. This is because consumers know that they all share basically the same requirements for the product, so they know the reviews are relevant for them.

This can be seen in the fact that, for example, an advertisement for a shampoo which claims it gets rid of dandruff 100 percent of the time is still seen as less trustworthy than online reviews stating that the customer was satisfied with the product.

### 3. Bad products get found out nowadays – the only way to make sure a product is successful is to make a valuable product. 

Have you ever bought something that turned out to be a complete lemon?

In the past this was not that uncommon: companies could sell shoddy products but use marketing to build a nice image around them anyway.

Today, however, word of mouth always exposes the truth: people tell each other, usually online, about their experiences with a product.

The honest opinions they convey will undercut any image constructed through marketing.

For example, a car company might invest heavily in a stylish advert proclaiming that its new model is beautiful and elegant. But if regular consumers think it's ugly and boxy, this view will be spread across the internet and it will become the predominant one.

Honest consumer reviews online will also penalize any company pushing bad products. As an example, consider the case of a well-regarded contact management software package offered by the company Act! The firm decided to offer an expensive upgrade to the software, but, due to its poor quality, disappointed customers began complaining online. The 125 negative reviews that could be seen on Amazon totally destroyed Act!'s upgrade sales.

This effect is good news for consumers because it means that companies simply must only sell good products and treat people well. This can also be very advantageous for the companies themselves.

Consider one small conference call service in Fairfield, Iowa, that decided to use all of its advertising budget on customer service. This allowed them to provide every customer with great service, taking care of their every need.

This generated a lot of positive word of mouth through reviews and comments online, and had a much greater impact than traditional advertising ever could have.

### 4. The internet has made all complaints permanent, so you need to respond to them proactively. 

Imagine you run an online electronics store, and a customer has bought something from you that's malfunctioning.

He calls your customer service hotline only to be told that the issue is his own fault. Furious, the customer writes a negative review about the product and your company.

Now you've got a problem, because this review will stay online forever, as will its negative effect on others' perception of your company.

So what could you have done to avoid this permanent stain on your record?

Well, every company is bound to make some mistakes, but what matters is not that those mistakes occur, but rather how you deal with them.

The only solution is to display the human side of your company: when you spot a negative review online, apologize to the customer and do what you can to solve the problem that has left him so unhappy.

To make amends, you could, for example, offer to exchange his malfunctioning device for a new one or send him a small gift like a free voucher for your store.

Also, participate actively in the online discussion about your product: tell your side of the story and you might just earn their respect.

Your efforts to explain and make amends will show that you're sincerely trying to make up for the mistake. And even if the most vocal critics or the original angry customer aren't fully convinced, at least other potential customers will see that you're doing your best to be an honest and proactive company, and they will appreciate this.

> _"An unhappy customer tells five people."_

### 5. Only honest marketing works in the word-of-mouth age. 

You might think that the advent of the internet has only increased the amount of lies and fabrications in circulation, but in some respects it has had the opposite effect.

Companies can no longer lie to customers and expect to get away with it.

This is because word of mouth will always reveal the truth.

For example, if someone were to launch a new mobile phone and promote it as the fastest and most robust one on the market with no basis for those claims, the truth would quickly be exposed as an army of customers and bloggers would publish their experiences.

This means advertisers are forced to be honest.

What's more, people are increasingly distrustful of traditional marketing in general. The reasons are pretty obvious: if a company has a vested interest in people buying its products, how can consumers possibly trust them to be honest in their advertising?

In fact, this distrust deepens every time an advertisement presents fraudulent claims like, "this car is totally reliable," or "this computer is unbreakable," and those claims are subsequently proven to be false.

This growing cynicism means it's more important than ever for advertisers to be honest. This is the only way companies can acquire happy customers, and happy customers are in fact the best advertisements a company can hope for.

So how can you acquire happy customers by being honest?

You need to market your product in a way that matches the consumer's expectations and the reputation the product has acquired through word of mouth.

Take the example of the car company that launched an ugly new car model in the earlier blink. Instead of proclaiming their car was something it wasn't, they should have marketed their car as "ugly but reliable." They could even joke about its appearance. This way the customer would probably appreciate their honesty, and might be more open to examining the car's strengths.

### 6. Companies need to take into account the power of good customer service to overturn the effects of negative word of mouth. 

Everyone has at one time or another encountered dismal customer service.

This is because many companies consider providing customer service as a mere mandatory chore that adds no value. This is why they often opt to outsource customer service to minimize this distasteful cost.

But this is a bad idea today, for the companies lose control of the quality of their customer service. And as we have seen, providing good customer service can be a great way to generate positive word of mouth, and as you might guess, bad customer service generates lots of negative buzz, which could end up being very costly. Remember, negative reviews are visible online _forever_, so the impact is permanent.

Few companies take this side of the financial equation into consideration, however.

For example, a hotel can't calculate all the lost revenue that is caused by customers never coming back after receiving poor customer service, much less all the customers' acquaintances who also now shun the hotel.

If these companies were able to calculate the full losses caused by customer service, they might be in for quite a shock.

And what's the only thing that can mitigate negative word of mouth?

You guessed it: good customer service.

If the hotel from the previous example were to have an outstanding service hotline that would surprise disappointed customers calling in with kindness and potentially some kind of voucher for compensation, the customers would be far more likely to forgive them.

Just like that, negative word of mouth can be converted into neutral or even positive word of mouth, stopping the exodus of customers away from the hotel.

### 7. Instead of traditional marketing using adverts, companies should look towards more profitable word-of-mouth marketing. 

Ask yourself, what's the one thing that almost all media, from newspapers to websites to television have in common?

You guessed it: advertisements.

The wealth of advertisements in every medium arises from the fact that up until now, companies have always focused their marketing efforts on _traditional marketing._ They have invested huge amounts to have advertising agencies produce TV commercials, *design banners on the internet and erect even huge billboards by the side of the road. It has turned out, however, that there's another approach that is far more cost efficient.

In word-of-mouth marketing people promote your product to their friends and acquaintances for free. These constant public recommendations build your brand's reputation beyond the scope of what traditional marketing would have achieved.

But in fact, the benefits of word-of-mouth marketing go far beyond this.

First of all, positive word of mouth can enhance your current paid advertisements. For example, imagine you run a financial services company, and a prospective customer does a search for financial services online. In the results they see a flurry of positive reviews for your company — right next to an advertisement leading to your page. Clearly the impact of that ad will be increased.

Second, your company's sales force will also benefit, as they will find it far easier to close deals and convince new customers when they can point to all the positive reviews online.

Third, you'll need to expend less time answering customers' frequently asked questions, as they will likely find a lot of the information in the reviews already posted.

In the next blinks, you'll discover the five key building blocks — the Five T's — to generating positive word of mouth.

### 8. For effective word-of-mouth marketing, first identify talkers – the people who will talk about you. 

No doubt by now you've realized the immense value of positive word of mouth. But how would you go about building a campaign for positive word of mouth? You need to follow the _Five_ _T's_ _model._

First off, you need to find people who will talk about your business.

These people are _talkers_ — "T" number one. They are interested in your product or service, and are enthusiastic and well-connected enough to spread the word around.

Why do they do this?

Because they like your brand, service or product. It might also be that people in their network frequently come to them for advice.

Talkers are usually happy customers, eager employees or people in professions where they get to talk a lot. One such profession is that of a taxi driver — they always talk a lot, especially to tourists. You could take advantage of this fact if you were to, say, start a new hotel resort, by giving taxi drivers in your town a free two-night stay. They would then probably relate this experience to their customers.

Second, when you find potential talkers, be sure to analyze their network, its reach and their influence in it to gauge their impact.

Apple, for example, has a huge network of fans who love Apple products. This fan base is very diverse, so when they talk, they reach many different places, from technology forums to design blogs. Apple, of course, is happy to leverage its fans to spread its message.

Once you've found talkers you're happy with, you should "feed" them with information that they'll love. This could mean company news, technical data — whatever they are interested in!

Apple, for example, could provide product manuals for online tech support communities and send fans a newsletter about up-and-coming products.

To disperse this information, consider creating an official _Talker_ _Program_, like a fan club, an ambassador program or a special customer advisory board where customers can voice their opinions. This will make the talkers feel more valued and make them more appreciative.

### 9. Once you have your talkers, give them topics: reasons to talk about your message. 

So now that you have your talkers, what's the next step?

Give them something to talk about: _topic_, the second T in the Five T's model.

Think about what kind of topics would spawn positive conversations about your brand.

In the short term, this means finding a current, easily relatable topic. When in doubt, just ask yourself, "Would I tell a friend about this?"

You can create these topics by, for example, providing extraordinary customer service, partnering up with a charity or even doing something silly that gets publicity. All these actions could create an instantaneous, short-term positive effect that ends when the action ends.

For example, the backpack company JanSports doesn't just tell customers they'll repair their busted backpacks, but instead says the backpacks are going to "Backpack Camp" to rest up and enjoy a short vacation. The customers even receive postcards supposedly from the backpacks emphasizing how much fun they're having "at camp." This may sound a bit silly, but it is unique and so people talk about it.

In the midterm you should strive to create a viral effect where your message will be propagated by people even after the campaign.

For example, the office supply chain Staples achieved this by repeatedly and persistently using its motto, "That was easy," in its advertisements. Eventually customers started using it themselves, spreading the message to those in earshot as well.

Finally, in the long term you should aim to become a _buzzworthy_ _company_, meaning that people talk about the company for as long as it exists. For this, the company's mere existence has to be unique.

For example, one dentist in Chicago wanted to develop some distinctive flair, but he obviously could not change his actual dentistry methods — customers tend to want dentists to stick to tried-and-true procedures. So instead he worked on the surroundings: he transformed his practice into a "dental lounge" with funky colors and background music of the patient's choosing. This enhanced the customer experience and generated a lot of buzz.

### 10. Help your message spread faster and further by using “tools” that grab the customers’ attention. 

So now you've got your talkers and you've given them a topic to talk about. What next?

You need to build _tools_ — T number three — that help them converse about the topic. No doubt the most obvious method is to start a blog and build a following on social media like Twitter. But what else can you do?

Let's say the topic you've given them is a nifty electronic assistant for retail stores that's capable of speaking to customers.

First of all, you could just make it easier for them to have the conversations. You could, for instance, simply ask people to spread the word by programming the electronic store assistant to say to people, "Tell your friends if you were happy with me."

Or you could create a newsletter for your talkers, and put in a story about the store assistant. This is basically what Chicago-based chain Potbelly Sandwich Works did when they opened a new store in Austin. Wanting to find people who already knew them from back home, they specifically targeted people who had moved to Austin from Chicago, sending them hand-signed letters to let them know the new store would help cure their homesickness.

Second, be sure to share stuff with your talkers that can be spread easily. Never let anyone leave your store empty-handed — always give them pens, calendars, magnets, T-shirts, toys or something. One possibility would be to give away stuffed toys that look like your store mascot to children. This would be natural conversation starter, when other parents inquire about the toy.

Also, offer talkers a package of electronic _cut-and-paste_ _pass-alongs_, like logos, icons and ready-to-forward email messages that they can easily send to friends.

### 11. Once people are talking, take part in the conversation. 

So now hopefully people are talking about your product. But this does not mean you should just sit back.

After all, if the conversations are happening anyway, wouldn't it be better if you were _taking_ _part_ yourself? _Taking_ _part_ is T number four.

To do this, the first thing you need to do is find the conversations that are occurring and respond to them.

Use search engines to find conversations occurring on websites, message boards, blogs and on social media. Then start answering the people talking about you: participate in discussions, reply to their emails and do your best to remedy their complaints. In all replies but especially in those to complaints, focus on turning unhappy customers into happy ones. This is an extremely powerful tool for generating positive word of mouth.

Whomever you find in your company to take part in these conversations, ensure they are _true_ _believers_ in your products. This often does not mean your public relations experts or marketing managers, but employees who really love the brand and the product, and are happy to advocate them. Be sure they do this in an honest and plain way though, by immediately coming clean about working for the company. Otherwise the talkers will feel deceived when they eventually find out anyway.

Of course, the most important conversations to participate in are the negative ones, as you need to deal with them quickly to turn them around.

The computer company Dell learned this the hard way in 2005, when renowned blogger Jeff Jarvis criticized them for bad customer service, and they failed to respond. This one criticism started a cascade of negative comments from other people as well, causing a stinging public relations defeat for Dell. This could have been avoided if Dell had responded to Jarvis' comments sooner and made a sincere effort to make up for its mistake.

### 12. Monitor and measure what people are saying about you to understand its effect on your bottom line. 

Marketing is often plagued by the fact that its effectiveness can be difficult to measure. However, with word-of-mouth campaigns, this part is actually very easy: You don't need to collect feedback forms or conduct extensive surveys to get information on what customers think of you when it is readily available online. The last T in the Five T's model is _tracking_, and it is crucial.

This means you need to monitor blogs, communities and incoming customer emails to know what consumers think of your company and products. Also emphasize feedback on your home page: link to an easy-to-use feedback form and consider posting a brief survey on the site every now and then for customers to fill out. You can also ask for feedback offline by, for example, shipping all your products with postage-paid feedback cards.

What you need to pay attention to is whether or not people are talking about you in a positive light, and whether these opinions are being shared. Also, are the actions you've taken to facilitate this working?

This will not only help you to improve on your five T's, but will also show you the importance of word-of-mouth marketing.

In the surveys for example, you can ask customers to explain exactly where they heard about you — e.g., "From an online review," or "From a co-worker." This will allow you to calculate exactly how much money was made through word of mouth, and how big an investment would have been required to gain the same sales through traditional advertising.

### 13. Final Summary 

The key message in this book:

**Word-of-mouth marketing is becoming far more effective than traditional marketing. Therefore you need to actively encourage people to talk about your product, while participating in those discussions yourself.**

Actionable advice:

**Find conversations and try to turn them around.**

Do a quick online search for reviews of your company and some of your products. What do you see? Are there angry comments there? If so, and if you understand why the situation that generated the complaint has occurred, take the time to explain your side of the story to the person complaining. Offer to help them resolve their issue or to put them in touch with someone who can perhaps help them. See if you can't turn their frown upside down!
---

### Andy Sernovitz

Andy Sernovitz is a lecturer and serial entrepreneur specializing in word-of-mouth marketing. He started the Word of Mouth Marketing Association and runs SocialMedia.org and WordofMouth.org.

