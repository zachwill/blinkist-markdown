---
id: 572232ff5ed4340003a94a90
slug: words-like-loaded-pistols-en
published_date: 2016-05-02T00:00:00.000+00:00
author: Sam Leith
title: Words Like Loaded Pistols
subtitle: Rhetoric from Aristotle to Obama
main_color: FD434A
text_color: FD434A
---

# Words Like Loaded Pistols

_Rhetoric from Aristotle to Obama_

**Sam Leith**

_Words Like Loaded Pistols_ (2012) is a guide to identifying rhetoric and using it to your advantage. These blinks use historical, contemporary and everyday examples to show how rhetoric is a part of everything we do, which is why it's such an essential topic to examine.

---
### 1. What’s in it for me? Embrace the many purposes of rhetoric. 

We all know of the dangerous men and women — like Hitler and Lenin, to name just two — who gained power and influence not through rational arguments, but through clever and emotional speeches. But while it's true that rhetoric can be abused — and often is — it shapes everything around us from our everyday conversations to human history itself.

In these blinks, we'll follow in the footsteps of Aristotle, one of the greatest thinkers of all time, to explore how rhetoric really works. You'll learn how rhetoric can help you understand the world.

And as we go along, you'll discover

  * a major similarity between Sarah Palin and Barack Obama;

  * the connection between a toy store and political rhetoric; and

  * rhetoric's intimate link with democracy.

### 2. Rhetoric is unavoidable, and everybody uses it. 

The word "rhetoric" might remind you of your pretentious college roommate's major or the nonsensical jargon hurled around by politicians, but, in fact, we all use rhetoric every single day.

Rhetoric is all about the art of influencing people through words, whether they're written or spoken. So, as much as we might try to dismiss it as outdated, stuck up or even deceptive, rhetoric determines the tone of this very sentence, and it's for rhetorical reasons that you speak one way to your best friend and another to a job interviewer.

After all, how often do you use language _without_ the intention of influencing someone?

It's hard to avoid because words are a tool for communication, which is the mutual exchange of information. And every piece of information, whether it's emotional, scientific or factual, will affect your feelings, opinions or actions.

However, in this day and age, the term "political rhetoric" is often used disparagingly even though it's impossible to avoid rhetorical strategies in politics. For instance, Obama's critics have ridiculed him for being too wordy and full of air. They don't think he's got much substance beyond speaking well. Compare that view with that of Republican Phyllis Schlafly, who praised Sarah Palin for being a "woman who worked with her hands."

But naturally, in every speech she gives, Palin works to influence her audience. Regardless of what people might think, she uses rhetoric. In fact, even people who are critical of rhetoric use it all the time. That's because accusing someone of being a smooth-talking swindler requires some smooth talking — get it?

That "get it?" is itself a common rhetorical strategy known as a _rhetorical question_.

### 3. Rhetoric is central to understanding history and humanity. 

Language sparked humanity's development and, because where there's language, there's rhetoric, it's only logical that wherever you find humans, you'll find rhetoric as well.

In fact, rhetoric has been fundamental to Western civilization and its effects have been both positive and negative. That's because democratic governments rely on speech and debate to function properly — the laws that society lives by are just legally-empowered words.

But words have also been employed as a peaceful means of resistance. Jesus was crucified for speaking peacefully with others, a fact that saw him accused of spreading dangerous ideas even though he never physically threatened the emperor. And the same goes for the peaceful activism of Martin Luther King, Jr.

On the other hand, totalitarian regimes use words as propaganda to exercise control over their people. Hitler didn't come into power through sheer force; he talked his way to becoming leader of Germany by effectively persuading a dissatisfied nation through language.

So, rhetoric can be used to many ends, but it can also teach you about what people want and how they're trying to get it. That's because people use rhetoric to attain their desires. For instance, a politician who wants the military to have more power might speak in impassioned terms about the need to protect loved ones and defend against the bad people who threaten them.

This means that the better you understand rhetoric, the better you'll be able to see what people want and how they're trying to use you to achieve their goals. Being able to recognize these intentions allows you to turn the tables on people in power.

Historically, we realized the power of rhetoric. During Shakespeare's time, for example, it was an essential facet of a liberal arts education. But it was Aristotle, generations earlier, who first deconstructed and analyzed the concept in his seminal work _Rhetoric_.

It's his theory of rhetoric that we'll explore next.

> _"All I have is a voice/To undo the folded lie."_ \- W.H. Auden, poet

### 4. Effective rhetoric begins by identifying your argument and how you’ll prove it. 

Aristotle broke down rhetoric into an orderly five-part structure, the first aspect of which is _invention_, or thinking about what you can say on a given subject. Invention can also be described as the stage during which you define the point you intend to prove and sort out which arguments and claims will help you do so.

There are often multiple ways to approach this crowning "proof" and the trick is to choose the one that fits your audience. To do so, you'll need to consider their life philosophies, prejudices and demographic makeup.

For instance, say you're trying to convince different audiences to adopt your new method of conflict resolution. When speaking to a group of managers you'd be wise to emphasize how effective and time-efficient your method is. But when speaking to people from HR it might make more sense to focus on how the method ensures that the parties treat each other justly and with respect.

OK, now that you know what you're arguing and how best to prove it, you can use the three modes of persuasion to make it happen. The first is _ethos_, based on the authority and self-presentation of the speaker. For instance, when John F. Kennedy said "_Ich bin ein Berliner_ " or "I am a Berliner," he was using ethos to appeal to his German audience.

The second mode is called _logos_, which appeals to reason and established truths. But remember, these truths are specific to a culture and location, so making a compelling argument depends on logic that's familiar to your audience. While the notion that men and women should be equal might seem self-evident for many modern people, such talk would have fallen on deaf ears when presented to many ancient cultures.

Finally, _pathos_ is an appeal to emotion: a photograph depicting an adorable, sad, sick puppy might solicit more donations for an animal shelter than statistics about animal abuse.

Now, back to Aristotle's five-part structure.

### 5. Aristotle’s second step to sound rhetoric is building an ordered structure for the ideas you’ve laid out. 

The key to good rhetoric is effective structure. That's because your argument will be most persuasive if you can guide your audience along a well-laid rhetorical path that emphasizes the strongest aspects of your argument while minimizing its weaknesses.

After all, it's no coincidence that professors prone to irrelevant and boring digressions don't do so well in their course evaluations.

So, to make your argument compelling you can use a variation on the classic "beginning, middle and end" story structure that divides your speech into six parts. First comes the _introduction_, the section where you establish your ethos, grab your audience's attention and earn their trust.

Second comes _narration_, where you lay out a relatively objective and generalized overview of the subject. After that is _division_. Here's where you point out the similarities and differences between your argument and that of your opponents.

Fourth is the _proof_ of your argument. This section relies on your use of logos and reason. Fifth is _refutation_, a time to anticipate and shoot down any objections your opponent might have to your argument.

And finally, the last section is the _conclusion_. Here you can use pathos to drive your point home with a bang and leave your audience with a lasting impression.

But remember, these are general rules, not absolutes and it's essential to use your judgment to determine when certain aspects should be added or omitted depending on the occasion. For instance, if you're the best man at a friend's wedding, your speech probably doesn't need to include a refutation of opposing arguments.

That doesn't mean you can play it fast and loose with the structure. After all, structure is essential to a well-flowing argument, whether it's delivered by pen or mouth.

> _"Though all the limbs of a statue be cast, it is not a statue until they are united." -_ Quintilian, Roman rhetorician

### 6. Style, memorization and delivery are the final pieces of Aristotle’s five-part structure. 

So, what you say is important, but it's not everything. The _style_ you use to frame an argument plays an essential role as well. In fact, the style largely determines how persuasive you'll be.

And as always, effective style is about a strong understanding of your audience. That's because it's essential to know what they'll respond to. Maybe it's a lofty, fluid style like that of Shakespeare, but the odds are it's not.

That's because modern society has clearly cast its vote against the overtly rhetorical; people no longer trust those who use flowery and inaccessible words. It's a better bet that you'll be persuasive using plain, straightforward language that's earnest, honest and not showy. Just remember, this stripped-down style is still a form of rhetoric and mastering it is about practice, not natural skill.

For instance, highly skilled orators and writers like President Obama are masters of both high and low styles, and can combine them in their rhetoric. Switching between short, snappy sentences and ones that are longer and more elaborate will add enticing color to your speech without making it sound overly rhetorical.

Finally, your strategy would be incomplete without _memorization_ and _delivery_. Two actors playing the same role with the same lines can have entirely different effects on an audience. That's because one might falter, forgetting and tripping over her words while the other smoothly delivers her lines as if they were her own thoughts.

_That_ is what memorization and delivery can do for your rhetoric.

Unfortunately, these final stages are the most stressful and scary aspects of rhetoric for many people. For some, the stress of memorization and delivery can even lead to stage fright and a suddenly blank mind.

But there's no way around it. All you can do is practice until you've got everything you want to say engraved in your mind. That's the best way to speak persuasively and compellingly.

> _"A speech is like a love-affair — any fool can start it, but to end it requires considerable skill."_ \- Lord Mancroft.

### 7. The two main uses of rhetorical oratory are political and judicial. 

The scene: a toy store. Center stage: a child, pleading with his parents to buy him a grossly overpriced piece of junk. This scenario is a prime example of _political rhetoric._

Here's how it works.

Political rhetoric is _forward-facing_, which means its goal is to push people to a particular action. Usually, political rhetoric attempts to do this by arguing that the desired course of action will work to the audience's advantage, is morally correct, or — even better — both.

For example, the child might promise his parents that he'll be very well behaved if they buy him the $100 plastic lightsaber. Or, he might base his argument on the need to restore fairness in the world, saying that all his friends already have one. In much the same way, political candidates try to earn people's support and ultimately their votes.

But there's another popular form of rhetoric beyond the political. It's called _judicial_ or _forensic rhetoric_ and it's the rhetoric of the courtroom, a type of argumentation that deals with justice. It digs into the past and works to establish a certain version of events as the truth. It then leverages this story to prove someone's innocence or guilt.

Oftentimes both oratory forms are at work simultaneously. Look at Bill Clinton's famous public statements regarding his extramarital affair with Monica Lewinsky. He employed judicial rhetoric by looking to the past and trying to establish his version of events as fact — claiming that he'd never had sexual relations with the young woman — but his arguments were political because he set out to convince the public not to impeach him.

So, next time you're faced with a complex speech or article, try understanding what the author is attempting to argue. If you can identify that you'll be well on your way to being a discerning consumer and effective communicator of news and information.

### 8. Final summary 

The key message in this book:

**Rhetoric is all around us and you must understand it if you want to be an effective communicator. Following Aristotle, you need to be able to master every aspect of your written and verbal speech including tone, structure, word choice and delivery.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with _Words Like Loaded Pistols_ as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Thank You for Arguing_** **by Jay Heinrichs**

_Thank You for Arguing_ (2013) is a guide to the art of rhetoric. These blinks explain what rhetoric really is, how persuasion works and how to win a debate by drawing on in-depth research, anecdotes and theories from the great orators of history.
---

### Sam Leith

Sam Leith is the literary editor of the _Spectator_ and has contributed to _The_ _Wall Street Journal_, _Evening Standard_ and _Guardian_, among other publications.

