---
id: 5b588d66b238e10007a4bb55
slug: words-on-the-move-en
published_date: 2018-07-27T00:00:00.000+00:00
author: John McWhorter
title: Words on the Move
subtitle: Why English Won't – and Can't – Sit Still (Like, Literally)
main_color: F03930
text_color: F03930
---

# Words on the Move

_Why English Won't – and Can't – Sit Still (Like, Literally)_

**John McWhorter**

_Words on the Move_ (2016) is a whistle-stop tour through the history of the English language, from its Anglo-Saxon roots to global lingua franca. Packed with illuminating insights into the evolution of words and meaning, John McWhorter's entertaining look at language dispels plenty of myths along the way. He argues that emoticons and the new use of "like" aren't a threat to our language, but quite the opposite — they're the latest chapters in a story of endless evolution.

---
### 1. What’s in it for me? A lucid look at linguistics with an English expert. 

As much as pedants, rulebook writers and would-be Canutes would like to turn the tide of change, the way we speak changes all the time — languages just can't sit still.

Few languages are more fidgety than English. Over its 1,400 years of evolution, it's developed sponge-like qualities of absorption and an incredible knack for innovation. From pronunciation to punctuation, today's English is light-years removed from the language in which Shakespeare wrote his dramas.

Understanding that history of change helps us put some myths to bed. From emoticons to millennials' frequent use of "like," the most recent additions to English don't undermine the language but continue its long-standing tradition of adaptation.

And that's the real constant in English — change!

In the following blinks, you'll learn

  * how pronunciation can alter the meaning of words;

  * why English turns verbs into nouns and Spanish doesn't; and

  * what the word "cafetorium" means.

### 2. Emotional self-expression might be new in art, but it’s been central to language since the Dark Ages. 

Sometimes it feels like every second person wants to be an artist these days. That's partly because it's one of the few jobs which really encourages emotional self-expression.

But art wasn't always like that. In fact, emotions only took center stage fairly recently.

Medieval artists, like thirteenth-century Florentine painter Giotto di Bondone, weren't all that interested in how individuals felt. What occupied them were the great questions of human life. Above all, that meant religion.

Things started changing around the time Leonardo da Vinci painted the _Mona Lisa_ in 1505. The work is famous for its subject's coy smile. The individual is front and center, making the painting pretty atypical, and for that reason, it's often regarded as marking a new, more individualistic era in the arts.

After that, there was no going back. From the Renaissance to Tolstoy's intimate 1877 work _Anna Karenina_, individuals and their feelings have had pride of place in our art.

While individuality and expressing emotions are relatively recent phenomena in art, they've been central to the way we speak for centuries.

Take the word "well." Speakers of Old English were using it in the early medieval period, though they spelled it "wel." So what does it mean?

Think of the sentence "Well, horses run fast." Imagine trying to explain to a toddler what the word "well" is doing here. Pretty tricky, right? That's because, unlike "horse," it's hard to pin it down to a single meaning.

It only really makes sense in the context of a previous remark or question, like "Why don't horses get eaten by wolves?"

What it suggests is an attitude. By using "well," the speaker is being gracious about another person's ignorance of a given subject. That means this short four-letter word does a lot of heavy emotional lifting. It lets us correct someone without offending him.

That makes it a unique expression of the way our feelings and emotions are embedded in the language we use!

In the following blinks, we'll delve a little deeper into the subjective world of feeling in language.

### 3. Emoticons are just the most recent addition to the emotional arsenal of our language. 

There's a certain type of linguistic traditionalist who worries that emoticons will come to replace written English entirely.

But that's a far-fetched idea. Emoticons are a way of injecting feeling _into_ texts, not replacing them!

The way we speak is usually more expressive than the way we write, but there's been a massive shift in written language. Texting makes writing much more like speaking: it's casual, quick and more emotional.

Compare that to faxing: That might seem like an earlier version of the same thing, but there's an important difference. Faxes were mainly used to convey technical content, like instructions or other information.

Faxes, in short, were usually pretty dry. Texting is a different ball game altogether. Today, a new generation of technology users is making communication a much warmer and more personal affair.

And that's where emoticons come in: they serve a particular purpose — expressing emotion. But that doesn't mean they'll replace writing. If we only used emoticons, we wouldn't be able to say very much. Sentences would contain little more than, "Well, yeah, I guess, that was totally, you get me."

So why all the fuss?

One reason is that emoticons _are_ different. For one, they're drawings. But if we put that aside, we can see that they often function in much the same way as words.

"Well," as we saw in the previous blink, is one example. But there are plenty of others. Just think of recent additions to the English language such as "totally" and "like." Then there's the new use of "ass." Added to an adjective, it intensifies the meaning. "Meanass," for example, means "extremely mean."

Other languages have similar words.

The Germans pepper their sentences with "mal" to express easy-going casualness. Japanese speakers add the "ne" particle to the end of sentences. Ask them what it means, though, and you'll get a lot of umming and ahhing. It's an emotional world — its precise meaning is difficult to pin down.

### 4. The meaning of virtually every word changes gradually over time. 

If you traveled back in time to the eighteenth century and offered someone a chocolate eclair, he might well decline by saying, "No thank you, I'm reducing." As you've probably already guessed, that would have been his way of saying he was on a diet.

That's just one example out of thousands. The meaning of virtually every word changes over time.

In fact, the real outliers are the words that retain their original meaning down the centuries.

Take the noun "brother." It's denoted the same thing for an astonishing _seven thousand years_!

It was first used by a tribe living in today's Ukraine. They spoke what linguists refer to as Proto-Indo-European — the ancient language from which most modern European languages are descended.

"I" is another case in point. Since its first use, it's always meant "I," even as its pronunciation changed. Proto-Indo-European speakers, for example, would have probably pronounced it as "eg."

But most words _do_ change. In Shakespeare's age, "science" meant knowledge in general. The dramatist's contemporaries would have been baffled by our use of the word to refer to the systematic knowledge of the natural world.

Or take the nouns "dog" and "hound." In the medieval period, the former referred to particularly large and menacing dogs, while "hound" was the generic term for all things canine. Today, that's been reversed.

As that example suggests, linguistic change is often a slow-burning process. Words take on new meanings over centuries.

You can get a sense of this by looking at the history of "innumerable." In contemporary usage, it basically means "a lot," or a very large number. But originally it referred to something that couldn't be counted. That might've been items of which there were very many, or something more abstract which couldn't be divided into countable units.

So if you lived in the late medieval world, you might have said something like, "My love is innumerable to express." In other words, "My love can't be counted."

Over time, however, the first sense won out. "Innumerable" started being used to refer to things which were so plentiful you couldn't tally them. In the end, that was the word's only meaning. It no longer meant "uncountable" but "a lot."

### 5. English is full of examples of verbs becoming nouns and taking on entirely different meanings. 

Reading Shakespeare can be a strange experience. Half of it is clearly English and readily understandable, but the rest might as well be Greek.

That shows that linguistic evolution can also be a fairly rapid affair.

One way English has been changing in recent years is by adding new nouns derived from verbs.

If you're in a business meeting, for example, you might hear someone enquire what "the ask" is, or if there's "a solve" for a certain problem.

That's pretty unique to English. Unlike a lot of other languages, word endings don't provide much of a clue as to whether they're verbs or nouns.

Take "scratch" or "walk." We're so used to sentences like "She had a clumsy walk" or "He has a scratch on his arm" that we don't pay any attention to the fact that both nouns started out as verbs.

But French or Spanish speakers would find a transformation like that jarring.

That's because verbs in their languages are clearly marked. In French, you can spot an action word by its "er" ending, as in "manger." In Spanish, it's "ar," as in "hablar," that's the giveaway. Verbs are just too "verby" to double as nouns.

But English doesn't just invent new nouns out of verbs. Over time, these new words take on different meanings.

Let's go back to our earlier example. Some people might argue that the sentence "What's the solve?" could just as easily make use of an existing noun — "solution."

But look more closely. The two words aren't really synonyms at all. "Solution" smacks of the schoolroom. It's the answer to a problem in a piece of math homework, not what hard-nosed businesspeople are looking for in the boardroom.

What they need isn't a solution, it's a "solve" — the answer to a concrete and practical problem in the fast-paced, dog-eat-dog world of business!

### 6. The meaning of exclamation marks has changed over time and in response to contact with other cultures. 

Fashion is famously fickle. It wasn't that long ago that donning dungarees marked someone out as a member of the trendy avant-garde. Today, jeans — as we now call them — are about as humdrum as it gets.

Language also has its fads. Linguistic styles come and go. Not even punctuation is exempt from changing tastes. And there's one punctuation mark that's changed more than most: the exclamation mark.

Emails and text messages are full of them. Even restaurant receipts can't resist their allure, with examples like "Tell us what you thought of our service!!"

They've become standard fare. Commonplace abbreviations like "OMG" just wouldn't be complete the same without multiple exclamation points.

So what's going on? Are people simply much more enthusiastic about life these days?

A better answer is inflation. The once rarely-sighted exclamation mark has lost some of its power since it started being used so frequently. It no longer conveys great surprise or emphasizes a strong point, but has become more like a sign indicating that we're paying attention.

In other words, it's about politeness. Simply texting "see you there" feels like a snub. Adding an exclamation point is de rigueur if you don't want the recipient to feel like she's been given the cold shoulder.

That's a change in everyday usage that's been driven by contact with different cultures.

Consider comic strips like Bob Montana and Vic Bloom's _Archie_ : They pioneered the use of multiple exclamation marks in everyday conversations. A typical dialogue would look something like this:

"You can watch me play!"

"But I don't want to watch! I want to play too!"

What the exclamation points underscore is a basic level of engagement, and that's exactly what it does when we send text messages today.

Then there are other linguistic cultures: Scandinavians were using exclamation marks in a strikingly contemporary way, even before _Archie_ first appeared in 1941. In countries like Sweden, for example, it's long been common to open a letter with "Sandra!" rather than the more subdued "Dear Sandra."

Again, that doesn't signal great excitement so much as attentiveness.

### 7. Knowing that words often meld together and create new offspring helps us understand Old English. 

Words are a bit like people. Sometimes they're magnetically attracted to each other. Once they get close, they end up mating and creating new offspring.

In linguistics, these hybrids are called _blends_.

Everyday English is littered with examples of them: Take "sitcom" — a mix of "situation" and "comedy." Then there's "motel," the child of "motor hotel," and "camcorder," a cross between "camera" and "recorder."

Blends enter a language with ever-greater frequency in the modern world. Technological networks like the global online community allow them to spread further and faster. What starts off as an internet in-joke can soon enter the speech of millions.

One good example is "staycation" — a stay-at-home vacation. It was coined by a journalist in the _Cincinnati Enquirer_ in 1944 but went unused for more than half a century. Once the 2008 financial crisis started putting pressure on people's disposable income, it reappeared. More and more people were doing what the word described and it enjoyed a resurgence.

But just because a new word gets a toehold in a language, it doesn't guarantee that it'll have staying power. Some entrants disappear as quickly as they appear, while others strike deep roots in the linguistic earth.

"Chortle," a blend of "chuckle" and "snort," — coined by Lewis Carroll in his 1856 story _Alice in Wonderland_ — is one of those hardy varieties. But "cafetorium," a coinage briefly used in the 1950s to describe large halls that could be used as both auditoriums and cafeterias, barely hung on for a decade.

Understanding the way English blends words isn't just of antiquarian interest though — it can also help us understand its past.

That's because Old English was a prodigious blender.

For example, negation was a fairly straightforward affair in Old English. You just put "ne" in front of the word you were negating. So "I have" was "Ic haebbe" and "I don't have" was "Ic ne haebbe."

That led to endless blending. To say something like "I don't have the pillow," you needed to create a sentence like "I nave the pillow." In this case, "nave" is a blend of "ne" and "have." Similarly, if we still used the logic of Old English, we'd be saying things like "nam" for "I'm not," and "nis" instead of "it isn't!"

### 8. The way we stress the pronunciation of words can also lead to linguistic change. 

If you watch the classic 1934 crime thriller _The Thin Man_, you'll notice something odd about the way inspector Nick Charles pronounces one word. In his hands — or rather, mouth — the emphasis in "suspects" shifts. Whereas we say "_sus_ pects," he says "sus _pects_."

So what's going on here?

Well, part of the evolution of verbs into nouns involves a shift of _accentuation_. The accent or stressed part of a word moves.

You can see this if you treat "suspect" as a verb. It's "to sus _pect_," right? That changes once it's become a noun — thus "the _sus_ pect."

Like many linguistic changes, the emergence of this new noun took a while. When _The Thin Man_ was released in the mid-1930s, the word was halfway through its evolution. That's why Charles pronounced it the way he did.

This is an example of the _backward shift of accentuation_ that accompanies the formation of new nouns. It's a general principle in English. Think of "_re_ bels" as a noun versus "re _bel_ " as a verb. Or the fact that crimes are "re _cord_ ed" in order to create a "_rec_ ord."

Changes in pronunciation also come into play when new words are formed out of two older words: a "black board," for example, is a board that's been painted black. A "blackboard," on the other hand, is a slate attached to schoolroom walls.

If you say the words out loud, you can see that something interesting happens to the accentuation. A "black _board_ " is the original term. But when it comes to the new term, the emphasis has shifted — hence "_black_ board." The backward shift reflects the fact that something more specific than the sum of its parts is being referred to.

The same goes for "_black_ bird" as opposed to a "black _bird_." The former is the name of a specific type of bird, while the latter can be used to refer to any type of bird that happens to have black feathers, such as a crow or a raven.

### 9. The word “like” has evolved throughout its history, and contemporary usage is just the latest change. 

As we've seen in these blinks, languages change — a lot! They evolve and adapt over time rather than remaining fixed in place. Think of it as the difference between a film and a photograph.

One recent change in the way people speak English is fairly notorious: the way they use the word "like."

But despite all the hullabaloo, "like" is a word that's never sat still for long. In fact, English speakers have been adapting its use since it first appeared in their vocabularies.

The word's origins can be traced back to Old English, where it was spelled "lic." Derived from "gelic" — meaning "with the body of" — it was a long-winded way of saying that something resembled another thing. In other words, that it was "like" something else.

Once it had been pared down to a single syllable, it started being absorbed by other words. Sometimes it was simply swallowed whole, making it hard to spot today.

Think of the adverb "slowly:" What on earth does _that_ have to do with "like?"

Initially, Old English speakers said something was "slow-like" when they wanted to describe the pace at which it was moving. Eventually, "like" was reduced to an even shorter suffix — "ly." And once that had happened, it started attaching itself to all sorts of adjectives. That's from where we get words like "angrily," "portly" and "saintly."

So it's a word that's always been taking on new roles — the way it's used today is just the latest.

But what exactly does that entail?

Think back to emoticons and words such as "well" and "totally." As we saw, they add emotion to texts and spoken language. "Like" is a bit, well, like that. It helps speakers convey their sense of something.

For example, a teenager describes an outing to a friend: "So," she says, "there we were and there were, like, grandmothers and grandfathers, and, like, grandkids and cousins all milling about."

"Like" isn't the mere filler that it's sometimes made out to be. What it allows this speaker to do is express her surprise that there were lots of people in a place she didn't expect them to be. It draws attention to the fact that she thinks it's out of the ordinary to find a whole family in the location she's talking about.

And that makes it a useful addition to English!

### 10. Final summary 

The key message in these blinks:

**The one constant in the history of the English language is change. From pronunciation to punctuation, English has changed dramatically over the centuries. But if you look closely, you'll soon notice that some contemporary tics have deep roots. Newcomers to the language like emoticons and hard-to-pin-down words such as "like" reflect the fact that language has been the primary medium of expressing emotions for much longer than art.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _I Can Hear You Whisper_** **by Lydia Denworth**

_I Can Hear You Whisper_ (2014) is about human communication and the phenomenon and culture of deafness. Hearing is a complex process that doesn't function the same way for everyone, and those who are deaf or hard of hearing have developed alternative methods of communication, around which a special culture has grown. These blinks give an overview of that culture and show that it's just as rich as any other.
---

### John McWhorter

John McWhorter is a professor of English literature at Columbia University. He's best-known for his writing on the English language and its history. His previous books include _The Language Hoax_ (2014) and _Our Magnificent Bastard Tongue_ (2009). McWhorter is also a regular contributor to major newspapers including the _New York Times_ and the _Los Angeles Times_.

