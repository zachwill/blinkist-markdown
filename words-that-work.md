---
id: 5717a6973f35690007554e42
slug: words-that-work-en
published_date: 2016-04-27T00:00:00.000+00:00
author: Dr. Frank Luntz
title: Words That Work
subtitle: It's Not What You Say, It's What People Hear
main_color: 52C3FD
text_color: 3988B0
---

# Words That Work

_It's Not What You Say, It's What People Hear_

**Dr. Frank Luntz**

_Words That Work_ (2007) is a guide to getting your point across more efficiently and effectively. These blinks explain the power of language and how it can help you in any number of situations, from business to political discussions to getting out of a traffic ticket.

---
### 1. What’s in it for me? Learn how to get your point across by using effective language. 

In _Faust I_, a masterpiece of German literature, Johann Wolfgang von Goethe wrote, "Words are mere sound and smoke." But is this really the case?

Hardly. Words carry meaning and ideas, and choosing the right words is vital to winning people over, be it in politics, advertising or your personal life. Anytime we talk to our colleagues, our boss, our children or friends, we have to weigh our words carefully in order to get our message across. 

But how can we find words that convey our message as intended? These blinks explain exactly how people will understand your words and why their interpretations might differ from what you actually mean. Find out how to make your message stick in your audience's mind and how to use language most effectively.

You'll also learn

  * why John Lennon's song _Imagine_ was such a great success;

  * how effective language helped Ronald Reagan and Bill Clinton become presidents; and

  * how to talk your way out of trouble with a local police officer.

### 2. Similar words can prompt different reactions among different people – effective communication is about considering your audience. 

Have you ever found yourself in a situation where your words were totally misinterpreted? Have you thought you were saying one thing only to find the other person heard something entirely different?

If this has happened to you, there are likely some flaws in how you communicate; that is to say, your words have failed you. But you're not alone. In fact, flawed language habits are so widespread that we encounter misunderstandings in everything from politics to business to everyday life.

This is the case because everyone has a different understanding of words. As such, two different words that technically denote the same thing can elicit entirely different reactions.

For instance, "welfare" and "assistance to the poor" essentially mean the same thing. But if you ask Americans, only 23 percent will say that the country is spending too little on welfare, while 68 percent think there is too little "assistance to the poor."

Clearly, different ways of communicating the same idea hold contrasting connotations. So, while "welfare" conjures up images of "welfare queens" and wasteful government spending, "assistance to the poor" reminds people of charity and Christian compassion.

Effective communication isn't about your message or what your words objectively mean, but rather how people _understand_ them. It's essential to consider your audience's preconceptions, especially their beliefs and fears.

Take the English novelist George Orwell, who knew this well and whose famous book _1984_ played on the deep personal fears of his readers.

For instance, one passage describes "Room 101" as a place where one is confronted with her greatest fears. Since the fears of every reader are different, Room 101 became associated with the personal nightmares of any given reader.

But how can you make sure your message is understood as you intended? In the next blinks, you'll learn more about the main pillars of exceptional communication.

> _"It's not what you say, it's what people hear."_

### 3. Effective language is clear, simple and well organized. 

How often do you pick up a dictionary and look up a word you don't know? For many people, the answer would be seldom — or not at all. And that's fine! After all, if you get used to using words so sophisticated that they are barely understood, your message is unlikely to be received. Thus, it's often best to stick with clear and direct wording.

Effective language is easy to understand. But how can you make yours as effective as possible?

First of all, it's important to use simple words and brief sentences; the more simply you present ideas, the more likely they are to be received. In the end, shorter words always have a bigger impact. Just take Apple's Mac computer, which was originally named Macintosh.

Shorter sentences are also more easily remembered. For instance, many Americans remember Dwight Eisenhower's 1952 campaign slogan "I like Ike," which used the then-presidential candidate's nickname.

By the same token, ignoring the rule of simplicity can mean big trouble. One reason John Kerry lost the 2004 presidential election was because the average American could hardly understand him. He tended to use overly complicated words and sentences that were far too long.

For example, he spoke of his preference for a "progressive internationalism" over the "too often belligerent and myopic unilateralism of the Bush Administration." In the end, many Americans had no clue what he was talking about.

But it's also important to carefully explain the relevance of your message, which means giving context. This is easy to do by arranging your message in the _right order_.

For instance, if you want to offer a solution, your audience first needs to know why there's a problem; without the initial context, your message is worthless.

Take 1920 presidential candidate Warren G. Harding's famous "Back to normalcy" campaign. His effort was successful because he began by explaining how the political climate of the post-World War I United States was one of chaos and disorientation.

Once he had given this context he offered the solution that could restore stability: himself.

### 4. Effective language appeals to the audience's imagination and senses. 

It might not seem like it, but language is extraordinarily powerful. Just think about how putting together certain words, like _a giraffe on a bike_, can paint a vivid mental image in the minds of your audience. Such a powerful mental picture forms because people struggle to resist imagining a long-necked creature, helplessly pedaling on a bicycle that's far too small for it.

In fact, the strength of the human imagination makes appealing to your audience's imaginative senses a powerful tool for conveying your message. To do so, it's essential to create a strong image in your audience's mind — in other words, to _sensualize._

For example, good advertising slogans often employ language that grabs your imagination and senses. Just take M&M's famous slogan, "melts in your mouth. . ." When you hear it, you can practically feel the chocolate on your tongue.

And there's an easy way to trigger this sensualization with one simple word: "imagine." When you ask someone to imagine something, you're asking them to generate their own personalized vision based on their deepest emotions and desires. Naturally, this makes for a powerful image and also explains why John Lennon's song _Imagine_ is among his most beloved and famous.

Alongside its visual components, the sonic quality of language also plays a central role. As such, you can achieve even greater success by utilizing the musical qualities of words. For instance, you can use words that sound similar together to make them more memorable. Returning to M&M's famous slogan, the repetition of the letter M in "_M_ elts in your _M_ outh. . ." makes the slogan stick, and the same goes for "_I_ ntel _I_ nside."

Another sonic strategy is to use words that sound like what you're describing. For instance, when you hear "Snap, Crackle and Pop," the slogan of Kellog's Rice Krispies, you get a perfect sense of what it sounds like to be eating the cereal.

> _"... people will forget what you say, but they may never forget how you made them feel."_

### 5. Effective language directly addresses people’s emotions. 

Hollywood writers live by the rule that their words should stir up emotions in viewers. They know that when language touches a person's feelings, it leaves a lasting impression in her memory.

The key to accomplishing this is to find words that either apply to a situation everyone is familiar with, a strategy called _humanization_, or even better, to their personal life experience, a technique called _personalization_.

Martin Luther King Jr.'s "I Have a Dream" speech is a great example of humanization. The message of the speech is that people shouldn't be judged by their appearance but by their character. This applied not only to the black members of his audience, but to the guiding principles of all Americans and, for that matter, human beings.

In everyday contexts, advertisers use humanization and personalization all the time to appeal to our individual life experiences and demonstrate that consuming their product will make our lives better. For instance, the skin-care line Olay uses the slogan "Love the skin you're in." This phrase plays on a sense of self-worth that anyone can relate to.

Another excellent tool for making an emotional impact is to ask questions. Addressing your audience with a question that begs a direct response will trigger a thought process and lead them to a conclusion.

So, if you reveal the conclusion to your audience right off the bat, they'll be less invested and interested in what you have to say. But if they reach an opinion on their own, it'll come with a profound emotional impact.

For example, during the 1980 US presidential debate, Ronald Reagan posed a very simple question to voters: "Are you better off today than you were four years ago?" By asking this question, he prompted a thought process that led the audience to realize that the country had indeed gotten worse under then-president Jimmy Carter, and famously resulted in Reagan sweeping to a huge victory in the presidential election.

### 6. Strong language combines the well known with surprises – and does so with credibility. 

So, you've learned some important skills for getting your point across; but it's also important to know about the major pitfalls that should be avoided. When it comes to communicating, two stand out in particular: boring your audience with old information and overwhelming them with new ideas.

After all, the key to effective language is striking the perfect balance between consistency and novelty. For instance, many companies make the mistake of changing their slogans too frequently.

Take Coca-Cola: can you think of the company's current tagline? Probably not, since they change it all the time, a practice that has wreaked havoc for their image. For example, from 2009 to 2016, their slogan was "Open happiness," but in 2016, it switched to "Taste the feeling."

Compare this to Wheaties, a company that still uses a tagline it created in 1935 with great success: "The breakfast of champions."

On the other hand, people are also easily bored. So, along with your consistency, you'll need something novel that surprises people and grabs their attention. In the 1950s, when cars were getting bigger every day, Volkswagen shocked car buyers with its successful campaign, "Think Small."

But it's also critical to communicate your credibility effectively. To do so, make sure your words don't contradict common perceptions and facts. For instance, during his 2000 presidential campaign, Al Gore became the butt of endless jokes and lost credibility when he claimed that he "invented" the internet.

Finally, it's important to remain authentic. The easiest way to accomplish this is by turning your words into actions. During the 1992 presidential election campaign, George Bush Sr. famously said, "Message: I Care."

While he obviously wasn't supposed to read out loud this note written on his cue cards, he also failed to communicate that his politics were caring in any way. Meanwhile, his opponent Bill Clinton said he would "Put people first" and explained how he'd do so by providing healthcare and high-quality education. Of course, Clinton went on to win the election.

> _"As Lincoln once said, you can't fool all of the people all of the time."_

### 7. Knowing your audience is essential to effective communication. 

So far you've learned how effective language can get your message across in the way you intended. But for language to be truly effective you need to understand something else: your audience. More specifically, you need to know your audience's hopes, beliefs and preconceptions.

For instance, if your audience is American, you should be aware of common misconceptions about average Americans, such as the assumption that many of them are highly educated. This is important because when you look at Americans older than 45, only 29 percent hold a bachelor's degree or higher. Barely one in four Americans over 25 are college educated.

Another common misconception about Americans is that they vote based on a candidate's political agenda. In reality, the majority don't know or care about political opinions; instead, they are focused on the type of person a candidate is — on their character, image and trustworthiness.

Consider the actions of George W. Bush during the national turmoil that followed 9/11. In retrospect, it's clear that his policy wasn't very effective — but Bush knew his audience extremely well. He knew that Americans wanted a commander-in-chief who was strong and determined to secure freedom for the United States and people across the world.

He also managed to convey this image successfully, which resulted in increased popularity and his reelection in 2004.

Another aspect of your audience that's important to keep in mind is how they perceive specific, frequently used words. Again, if your audience is American, it's crucial to know how words like "freedom", "fairness" and "opportunity" are perceived in the local culture.

After all, you might think that the word "freedom" carries a positive connotation, but it was so overused during the George W. Bush administration that it has become closely associated with the Republican Party.

Similarly, "fairness" has become associated with the Democrats, as it is a word they use very frequently. On the other hand, a middle-of-the-road term that most Americans like is "opportunity" — a word with no associations with either political party.

### 8. Effective communication can help you every day. 

Now that you know the ins and outs of effective language, how can it help you in your day-to-day life?

Well, imagine you're running late for a flight and the plane's door has already closed. How can you use effective communication to get on the plane?

Start by understanding the situation of your audience, in this case, the airport employees. In their eyes, opening the door again is a major hassle. Therefore, you're completely at their mercy and you will need to beg — but be sure to start and end your plea with the word "please." 

You can then tell them a story that makes it clear how catching this plane will change your life and why they should help you. For instance, you could say it's a family emergency or a life-changing job interview, something that anyone could relate to. At the same time, you should assure them you'll be eternally grateful. 

But effective communication can also bail you out when you get pulled over for speeding. In this situation, you again need to understand the situation of your audience: the police officer.

In contrast to the case of the airline employees, the cop could do you a favor without causing himself any additional work or hassle; after all, writing a ticket will produce extra paperwork that he certainly wouldn't be excited to fill out. Next, you should show that you're not a threat by turning off your engine, rolling down your window, placing both hands on the wheel and having your license and registration ready to go. 

Remember, cops have a dangerous job and never know if the person they've pulled over is a total lunatic!

Finally, you'll want to show how grateful you are, respect the officer's authority and be honest about violating the law. So, when the cop reaches your window, make eye contact and be sure the first words out of your mouth are, "I'm sorry, officer."

While this approach can't guarantee you'll dodge a ticket, it _will_ boost your chances. After all, language isn't magic, but it is a powerful tool to help you get what you want.

### 9. Final summary 

The key message in this book:

**Everybody interprets language differently, and this important fact makes the dictionary definition of your words far less important than the way they are received. Effective language is all about taking the views of your audience into account and choosing the words that will have the greatest impact upon them.**

Actionable advice:

**Make sure your letters are understood by putting essential information upfront.**

It's clear that we live in a time where attention spans are shrinking rapidly, which makes it important to grab your reader's attention in the first line of any e-mail or letter. To do so, your first paragraph should consist of a single sentence that outlines your most pressing demand. After that you should keep your remaining paragraphs short to make sure they don't end up in the trash folder or a wastebasket.   

  

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _The Secret Life of Pronouns_** **by James W. Pennebaker**

The Secret Life of Pronouns (2011) shines a light on the everyday language that we seldom pay attention to, revealing the ways in which it serves as a window into our personality and our social connections.
---

### Dr. Frank Luntz

Dr. Frank Luntz is a renowned pollster and public opinion guru who regularly works as a communications consultant for American politicians. He has led over 1,500 surveys and focus groups, and has contributed to numerous political and corporate campaigns.

