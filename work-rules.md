---
id: 55a38d4337386100076d0000
slug: work-rules-en
published_date: 2015-07-15T00:00:00.000+00:00
author: Laszlo Bock
title: Work Rules!
subtitle: Insights from Inside Google That Will Transform How You Live and Lead
main_color: FFD833
text_color: AD9323
---

# Work Rules!

_Insights from Inside Google That Will Transform How You Live and Lead_

**Laszlo Bock**

_Work Rules!_ (2015) takes us through the inner workings of Google, one of the most powerful and successful companies in the world. Bock tells us precisely how Google pulls off this feat while consistently being ranked as the best employer in the world.

---
### 1. What’s in it for me? Get an inside look at what makes working for Google so special. 

Everyone knows that working at Google is a dream job. That's not only because of the lure of fat stock options or because it's been voted the best place to work so many times. It's more than that. Working at Google somehow brings to mind the idea of personal freedom, or working at your best while being surrounded by the world's brightest minds.

How did that come to be?

Google's famous workplace culture didn't just emerge out of thin air. It's the work of Google's People Operations, one of the most innovative HR departments in the world. These blinks take a look at what makes Google special from the perspective of Laszlo Bock, the head of People Operations and a champion of Google's in-house culture.

In these blinks, you'll find out

  * why you should pay people unfairly;

  * how rewarding employees with cash makes them unhappy; and

  * the best way to hire and retain the very best candidates.

### 2. The secret to Google’s culture is its mission, transparency and voice. 

When you hear about working at Google, you probably think about beanbags, free food and other delightful perks. But it isn't the fun and the freebies that really makes Google so successful; it's a manifesto of mission, transparency and voice.

Google's mission is simple and powerful: "to organize the world's information and make it universally accessible and useful."

It's a profound mission because firstly, it gives moral, rather than commercial meaning to employees' work. That is, the information workers sift and sort through actually helps everyone who uses the search engine.

Second, the mission has no ceiling. A typical mission might be to become the market leader, but once this is accomplished, it's no longer inspiring. At Google, however, employees are constantly motivated to explore new ways of organizing information.

Another key tenet of Google's successful culture is its transparency.

In a traditional software company, a new engineer only sees the code from whatever projects they're working on. At Google, however, a newcomer can access everything from product launch initiatives to another employee's weekly status report.

Furthermore, each week the CEO updates the whole company on the past week and carves out time afterward for a 30 minute Q&A session. Thanks to this update, everyone knows who's working on what, which ensures they don't double up and work on it too. It also makes it clear who the go-to person is regarding a particular project.

Finally, Google gives every employee a voice. It values the input of everyone and gives them a say in how the company is run.

In fact, most practices within the company originated from employees.

In 2009, an annual Bureaucracy Busters program was launched so employees could identify their biggest frustrations.

### 3. Hire the best people by looking beyond their degrees and focusing on the right kind of training. 

How did Google create such a thriving culture? They employed outstanding people. Here's how they find them.

There are two ways to have an exceptional employee: hire the best or train the average. As you may already have guessed, Google does the former.

According to the Corporate Executive Board, the average company pays around $600 for training and $450 for hiring. But they're doing it all wrong.

The worst case scenario after recruiting a great candidate is that they end up performing at a mediocre level. But the worst case scenario after hiring an average candidate is that they drain a considerable amount of training resources _and_ wind up performing _below_ average!

Therefore, Google invests a great deal of time and resources in finding the right person for the job. They are incredibly scrupulous; hiring only 5,000 people a year out of one to three million applicants. That's an admission rate of 0.25 percent. Compare that with Harvard University's admission rate, which is a rather more lenient 6.1 percent.

But what should you look for in a candidate? When Google first started out, they only hired 100 people a year and the right candidate was an Ivy League graduate. However, over time, Google learned that many of their best employees weren't the ones who attended the most illustrious schools.

Instead of hiring only those with prestigious degrees, Google started looking for candidates who showed resilience and the ability to overcome obstacles.

They realized that the best approach was to hire someone who was better than you, and to seek out those who can make everyone around them more successful.

The Google hiring process involves slowly and carefully sifting through only the best performers in the field.

Karen May, the current VP of People Development at Google, previously owned an HR consulting firm and actually turned down repeated offers to work for Google for four years. But Google were convinced of her value and patiently pursued her until she finally agreed to join the company.

### 4. Let your people – with the help of data – run the show. 

Traditionally, it's the manager who controls employees' workload, salary, promotions, sick days and so on. Yet at the same time, companies expect their employees to show initiative and autonomy in their job. See the contradiction?

If you really want employees to own their jobs, do as Google does, liquidate status symbols and reduce bureaucratic hierarchy.

Google's most senior executives receive the same support, like resources and funding, as new employees and there are only four levels in the hierarchy: individual contributor, manager, director and vice president.

Googlers are also trained to lead and influence through inspiration. Want a VP title? Then show how well you can lead your project and people first.

But, as with any project, even if employees demonstrate ownership and influence, they still need a final decision maker. So how do you make the best decisions? Use data, not managerial opinion. That way decision-making is transparent and less biased. Thus, one of Google's core principles is "Don't politick. Use data."

This motto also helps address sensitive topics, especially when potentially damaging rumors are involved.

Promotion is a touchy subject in every company, and this often exacerbated by gossip. But the Google VP responsible for people used her data to prove there were no biases surrounding promotions at Google. For example, although some people believed that working at Google's HQ would guarantee a swift promotion, her data proved the promotion rate there was the same as at any other Google office.

When properly managed with data and open discussion, a power handover to employees is incredibly effective, resulting in the implementation of the best ideas. Even when someone disagrees with a final decision, the reasoning behind it is clear to everyone.

> _"If you have facts, present them and we'll use them. But if you have opinions, we're gonna use mine." — Jim Barksdale, Netscape CEO_

### 5. Both your best and worst employees represent opportunities for your company – seize them! 

If you've worked in an office before, you're probably familiar with the classic pattern of employee performance: a small number of top performers are responsible for most of the successes, and everyone else trails behind them with gradually decreasing performance.

The best and worst performers make up the _two tails_ of the performance curve. Both are the minority, while most employees are average performers, sitting in the middle of the curve.

Most companies fire poor performers, then hire new employees who require extra training and can't guarantee excellent performance. Even worse, companies also tend not to utilize their top performers.

So how does Google use these two tails to its advantage? They place outstanding performers under the microscope and help out those who need to make improvements.

Most companies also don't think to study their best performers. This is a missed opportunity, as these are the people most familiar with best practices.

How, then, do you study the best performers? Harvard professor Boris Groysberg's research shows high performance is dependent on context. That means studying other companies' best practices won't help; you must study your own.

Google do exactly this and study their best employees using an internal research team, PiLab or People and Innovation Lab.

PiLab's produced Project Oxygen. This showed how a great manager is critical for top engineer performance. In fact, Googlers working for the best managers performed five to 18 percent better than those managed by weaker managers. PiLab also determined the main traits of best manager practices so the company could teach them to the under-performing managers.

Google also knows that in most cases, below average performance is the result of lack of skill or motivation, which may stem from personal issues or indicate a bigger problem in the team.

Finally, to really reach those who need a boost, Google regularly identifies the bottom five percent of employees and offers them training, or tries to fit them into a more appropriate role within the company.

### 6. Stop wasting resources on bad training, and use the best teachers within your own company. 

$156.2 billion. That's the annual dollar amount that American companies spent on training programs in 2011. Shockingly, though, most company expenditure on training goes down the drain. Why? Because the training is run by the wrong people, is sloppily designed, too general, or doesn't get analyzed in a way that measures effectiveness.

Training should deliver specific information that people will retain. A commonly held belief is that it takes 10,000 hours to become an expert in any particular skill. But research by Anders Ericsson shows that the best way to master a skill is to split the work into smaller tasks and aim for a specific improvement in one of these small tasks through repetition, feedback and correction.

McKinsey, a global consultancy firm, does this particularly well. McKinsey sends all second year consultants to an Engagement Leadership Workshop where they're trained to deal with irate clients. First, the basic principles are taught, then consultants roleplay a scenario, and observe and discuss a video of their training. This process is repeated until the desired consultant behavior is achieved.

The training is intensive and expensive, but it ensures that every consultant leaves the training with excellent standards and the ability to cope in the best possible way with angry customers. McKinsey understands that the best way to measure training isn't by time or money spent, but through an improvement in behavior.

Who, then, should you hire to train your employees? The great news is, they're already in your company.

When Google needs a trainer for sales representatives, it seeks out the best sales manager with the maximum amount of total sales and asks them to instruct lower performing sales representatives.

When employees train other employees, not only does it save money, but it also creates a more close-knit community. Who can understand the problems of a Googler better than another Googler?

### 7. Sometimes Google rewards failure and pays people unfairly. Why? 

Compensation can be a contentious issue. So how does Google go about it?

Most companies are trapped in pay ceilings for each position and "fair" payment, with regulations specifying that salary differences for the same position may not exceed 20 percent. Unfortunately, this practice encourages top performers to look for better compensation elsewhere.

Bill Gates reportedly said that a fantastic coder is worth 10,000 times more than an average coder. Google shares this mind-set, which is why you may think it pays "unfairly." For instance, one worker may get a stock award of $10,000, while another worker in the same position might receive $1 million. You may also witness a top performer in a junior role getting paid far above an average performer in a senior role.

But Google also learned there are often more effective ways to retain employees: offer experience rather than money.

In 2004, Google established a Founders Award for performance. That year, Google awarded $12 million to two teams and the next year $45 million was divided among eleven teams. But internal surveys showed that this didn't make Googlers happy. Quite the opposite. It led people to look for other jobs where the chances of getting a generous award were higher.

Their mistake was rewarding with money instead of experiences, like a dinner for two or a team trip to Hawaii. It turned out these special occasions created more memories and brought teams together far better than cash could.

But Google didn't just give rewards when they were expected.

In 2009, Google announced the real-time collaboration tool Google Wave. One team worked on it for two years and agreed to forgo their bonuses in favor of higher compensation via stocks if Wave succeeded. Sadly, it failed, but Google rewarded the team anyway. Why? Because innovation means exploring the unknown, so there should be rewards for people who take calculated risks, even if they fail.

> _"If your goals are ambitious and crazy enough, even failure will be a pretty good achievement." — Google CEO Larry Page._

### 8. Google confronts the dark side of its culture head on. 

We've seen how empowering employees, transparency, and giving workers a voice have made Google so successful. Unfortunately, sometimes these practices backfire. But when they do, Google knows how to handle it.

Google suffers one major leak per year. When this happens, it's investigated and the guilty party, who leaked it either by accident or design, is found and fired. Not only that, but the company announces to everyone what was leaked and what happened to the employee who did it. The cost of a leak is small relative to the openness and transparency that the company values and upholds.

What else can sometimes go awry? Well, Google's culture fosters innovative thinking and this inevitably results in huge influxes of ideas, which have to be adjusted or culled periodically in order to keep the company running smoothly.

In fact, between 2006 and 2009, more than 250 products were launched and then discontinued by Google.

CEO Larry Page is responsible for the annual spring clean where he discontinues some products that are waning, don't have great market prospects, or are being outperformed by others.

By being open and explaining the reasoning behind each cull, Google manages to maintain its focus and direction, and retains its managers instead of angering them.

Finally, sometimes even company perks can go sour. Google is famous for its benefits and when it provides them, at first everyone is delighted. But after some time, some employees can start to feel entitled.

One employee, who became grumpy when the cafeteria used smaller plates, started throwing forks in the trash in protest, and some Googlers even threw food at the staff.

The final straw was Meatless Monday, which was launched to benefit employee health. Yet after a month, only one employee threatened in an anonymous survey to move to Facebook, Twitter or Microsoft. So Google shared the survey with its employees. Consequently, many staff were embarrassed by the person's actions and the level of entitlement and abuse fell.

### 9. Final summary 

The key message in this book:

**There's a reason why everyone wants to work at Google. They understand how to hire and retain the best staff, utilize the expertise already present in the company, give power to their workers, and keep them happy in their job. By studying these strategies, you too can learn how to lead and maintain a company that enjoys great success.**

Actionable advice:

**Hire in teams.**

_Confirmation bias_ can lead an individual to favoritize information or individuals that agree with their own beliefs. In job interviews it can cause employers to only take on people who echo their opinions or who resemble them. Avoid it when you're hiring by assembling a team to sort through and interview applicants. By having more than one opinion on prospective employees, you'll end up with a more diverse team.

**Take your time.**

Sometimes the hiring process can be frustrating, especially when time is of the essence. But holding out for the best new employee will most likely save money in the long run, as company funds won't be wasted on training up someone average.

**Suggested further reading:** ** _How Google Works_** **by Eric Schmidt and Jonathan Rosenberg**

_How Google Works_ shares business insights from one of the most successful technology start-ups in history. Written by the former top executives at the company, the book lays out, step by step, Google's path to success; a roadmap that your company can follow, too.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Laszlo Bock

Laszlo Bock is the senior vice president of People Operations at Google. He is responsible for attracting, developing and retaining more than 50,000 "Googlers" based around the world. During his time there, Google has been recognized as an outstanding employer over 100 times, holding a number one spot in rankings in the United States and 16 other countries.

