---
id: 553ffbf16233390007000000
slug: work-simply-en
published_date: 2015-04-30T00:00:00.000+00:00
author: Carson Tate
title: Work Simply
subtitle: Embracing the Power of Your Personal Productivity Style
main_color: 83C8E9
text_color: 57869C
---

# Work Simply

_Embracing the Power of Your Personal Productivity Style_

**Carson Tate**

In _Work Simply_ (2015), author Carson Tate draws from her own career experience to show you how to become more productive. By understanding your own productivity style, you can make lighter work of the ever-growing pile of tasks and achieve your life goals.

---
### 1. What’s in it for me? Work more productively by being yourself. 

Who doesn't want to manage their time better? Every busy executive at one point or another has been seduced by time-management tricks or tools that promise huge productivity gains.

The problem with these approaches is they're often one-size-fits-all solutions — which means they're not much of a solution at all.

Such tools don't take into account that each person uses her time differently, and therefore needs an individual solution to manage time more effectively.

In the following blinks, you'll see you can't _manage_ time as if it was an unruly employee. Once you realize that, you'll find a better path toward working more effectively; a way that better matches your personality, with the result that you'll be able to get more done.

After reading these blinks, you'll learn

  * which productivity personality you are;

  * why you just can't budget time; and

  * how to pay better attention to how your brain works.

### 2. Find your productivity style. Figuring out what type of person you really are will help. 

Have you ever been told you're a "left-brained" or "right-brained" thinker?

If so, you already know what's wrong with so many time management and productivity theories. These tools think that "one size fits all." So just make more lists, they say, and you'll improve.

Yet everyone has their own way of working, and what helps one person be more productive could be counterproductive to another. That's why the most effective way to start working better is to examine yourself and approach your work in a fashion that fits your personality.

This is where the _productivity style assessment_ can help.

A productivity style assessment is a test that is based on the findings of General Electric's long-time manager of management education, Ned Herrmann. It examines how the brain perceives, processes, comprehends, manages and communicates information.

In other words, a productivity style assessment explains how people work!

While there are many different personalities, Hermann's model offers four particular styles as a guide to start your assessment. Which style are you?

The first is the _prioritizer_. She's efficient with her time and uses data, thoughtful analysis and logic to approach and solve problems. At work, she prioritizes the most important tasks and is annoyed when colleagues chatter while there's work to be done.

The second is the _planner_. He loves making lists, organizing data and doing things in a particular order. When he gets to work, he reviews his schedule and shakes his head at some last-minute project his more unorganized colleagues are scrambling to finish.

The third is the a _rranger_, who relies on his instinct to make decisions. He works well with others, and enjoys presenting information visually, as in a colorful flowchart. His main concern: "How will this decision make people feel?"

And last, there's the _visualizer,_ who can walk into a heated discussion and find a way to reorganize all the key points that solves everyone's problems. When making decisions, she hates being dragged down by data, but will gather everyone's input and synthesize it into something new — even if it's contrary to "the way it's always been done."

### 3. You can earn money, but you can’t earn time. Make a master task list to organize your time wisely. 

Once you understand that you can control your present and your future, it's time to act! The best way to do this is to think differently about how you approach time management.

The thing is, time is not actually manageable. We cannot make time bend to our rules. It is, however, our most important resource.

This is what professor Randy Pausch emphasized in his famous 2007 talk and book, _The Last Lecture_. Pausch was suffering from terminal cancer and as part of his talk, told his students that while earning more money is often achievable, earning more _time_ is not.

Consider, then, what your time budget might look like.

To deal with the challenge of time, you should plan your activities daily, weekly and monthly, so you can spend your time in the most efficient way possible. It also helps to write a _master task list_.

A master task list contains all the things you need to do to accomplish your goals. When you have done this, organize these things into two categories: _project actions_ and _next actions_.

_Project actions_ are overarching tasks that require smaller action steps, and can take days or months to finish, such as reorganizing your kitchen or planning an off-site workshop.

A _next action_ is a single step, something that moves you forward. These should be listed starting with an action verb, for example: _Call Adam_. Or perhaps: _Revise speech_.

The point of a master task list is to release you from the burden of keeping the innumerable amount of tasks you want to complete swimming in your head all at the same time.

Noting everything you need to do — socially and professionally — gives you a chance to act and enables you to rethink and revise any future actions.

### 4. You need willpower to control your own attention. Know your weaknesses and find a balance. 

Ever feel as if you can't make yourself pay proper attention? It's vital that you harness this skill, as what you pay attention to has a significant impact on your productivity and your happiness.  

  

Attention is a precious resource, and you need willpower to direct and control it.

Each day, you can devote your attention to just a limited number of things, as attention is not infinite. Yet, the better you are able to get a grip on your focus, the more you can achieve. This is because focusing clearly on one task will prevent you from being distracted by other goals.

Focusing your attention holds different challenges for different personality types. Recall your personality type and see if any of these details pertain to you.

A _visualizer_ can get tangled up in irrelevant details and lose track of the main goal. _Arrangers_ have a tendency to forget their real goals and become submissive, due to their need to be appreciated. _Prioritizers_ can push their first priority way too hard, leaving little space for any other work; while _planners_ often miss out on great opportunities when they skirt around ideas that are more abstract.

While different personality quirks certainly make managing attention challenging, the reality is that we all need to fight for our attention daily.

In 2005, Basex, an information technology research firm, reported that almost a third of working hours are squandered on interruptions and distractions. Since smartphones have since become commonplace, this statistic is probably overly optimistic.

Attention to work must be balanced with attention to other things in life. Your daily routines should complement your attention span. Some people are more focused after a walk, while others concentrate better after working out.

Eating and sleeping habits should work with your attention patterns in the same way listening to music or alternating alone time or socializing should.

The key is to find the activities that support your attention and stick to them!

### 5. How you think and how you work is what’s unique to you – embrace it, don’t fight it! 

Pause for a moment and consider how your mind works for you, or against you. The truth is, our minds can actually sabotage us! But we have the power to change this.

First, you need to realize that you can only process a certain amount of information at once. This usually means taking in around two to three chunks of information at a time. This is why most of us struggle to recall more than a few names of people we've just met.

We process input subconsciously and unsystematically, but patterns do exist in accordance with our productivity style.

Remember how we can't manage time? Trying to do so is like trying to force square pegs into round holes. That's why time management approaches are ineffective and often lean on senseless schedules, not taking into account an individual's personality or productivity style.

If you are a _prioritizer_ and web design is the most important thing to you at the moment, it might be helpful to spend as much time on it as you can, even if it seems too much at first. In the long run, you'll get the best results, because it's a task you _had_ to do.

What wouldn't make sense, is if you tackled your productivity problem in the exact same fashion as every other web designer. You are your own person; you have your own personality and productivity style. Find what works for you and don't accept a cookie-cutter approach as the rule!

Because time is linear, we often feel we have to get things done in a linear fashion. Yet unless you're already a _planner_ or _prioritizer_, this approach can work against your productivity style.

For example, as a _visualizer,_ you could gather information from many different places, which might give you inspiration for your next project. If you force yourself to think in a more linear fashion, though, you'll end up stunting your creativity.

In short, finding and sticking to your own style of thinking and working will help you live up to your full potential!

### 6. Final summary 

The key message in this book:

**To remember the key lessons from these blinks, just keep in mind the phrase "Know the TWIST": You need _Know_ yourself, learn to manage your _Time_, have the _WIllpower_ to focus on the tasks at hand and find your own productivity _STyle_.**

Actionable advice:

**Know yourself and how you work.**

The next time you start a project, take notes on the task, particularly how it fits your goal and how much you liked doing it. Do this a few days in a row and you'll gain practical insight on how you can tweak your working style to best fit your personality and make lighter work of your daily tasks.

**What to read next:** ** _The Best Place to Work_** **,** **by Ron Friedman**

Now that you know how to find your own unique TWIST to becoming more productive, it's time to look at your work from another perspective and ask a simple question: Do you like going to work? Sadly, studies show that, if you do, you're in the minority. 

So what makes for a great workplace? In _The Best Place to Work_, award-winning psychologist Ron Friedman explores this topic and shows what _you_ can do to improve your work life.

To learn more — including why napping employees are happy employees — head over to the blinks to _The Best Place to Work_, by Ron Friedman.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Carson Tate

Carson Tate is a coach, consultant and founder of _Working Simply_, a management consultancy that has worked with high-profile clients such as Coca-Cola and John Deere. An expert on workplace productivity, Tate's ideas have been featured in the _The New York Times_, _Fast Company_ and _Forbes_.

