---
id: 55c7c8006664360007580000
slug: working-together-en
published_date: 2015-08-12T00:00:00.000+00:00
author: Michael D. Eisner with Aaron Cohen
title: Working Together
subtitle: Why Great Partnerships Succeed
main_color: CB2B2D
text_color: CB2B2D
---

# Working Together

_Why Great Partnerships Succeed_

**Michael D. Eisner with Aaron Cohen**

_Working Together_ (2010) is all about the benefits of strong business partnerships. Some people are most productive when they pair up with someone else, and these blinks use several high-profile examples to outline how different kinds of partnerships work.

---
### 1. What’s in it for me? Discover what makes a really good partnership. 

When successful business leaders are portrayed in the media, there is often a sense that their journey to the top was a one-man struggle against society, a David versus Goliath story. This narrow focus on the individual hides an important truth about success.

Just like the old saying that behind every great man there is a woman, so it is that behind every successful business there is a partnership. The simple truth is that, without a reliable partner who can complement and support you, building a great business can be very difficult.

So, what makes a great partnership? As these blinks will show you, all partnerships are unique — but successful ones have a lot in common. Without mutual respect, integrity and trust, no partnership can truly take off.

In these blinks, you'll learn

  * why constant support is so important in a partnership;

  * why humility is the ultimate goal; and

  * why lovers make great business partners.

### 2. Frank Wells and Michael Eisner: the partner as cheerleader. 

Frank Wells and Michael Eisner had a great partnership at the Walt Disney Company: Eisner was the wild and creative CEO, while Wells, the president, kept the company grounded. Their secret was that nobody could get between them.

They had an unspoken pact that they'd always trust each other and stick together. This, combined with the fact that they genuinely liked each other, made their relationship very strong.

Wells and Eisner shared all their news. They always compared notes from their phone calls and visited each other's offices at least 20 times a day. They also shared their frustrations, which made them easier to bear.

Wells also provided Eisner with a lot of support, demonstrating an important skill that's often overlooked: he was always Eisner's greatest cheerleader, enthusiastically promoting his ideas.

It was exactly this kind of passionate drive that became a crucial factor in their success. Wells was ready to do anything to help Eisner secure a deal. Once, when Wells was negotiating about the film _Dirty Harry_ with Clint Eastwood at Warner Brothers, he agreed to play tennis with Eastwood to settle a dispute.

Wells was concerned with success, not the spotlight, and because he had enjoyed a lot of success in his life, he was very confident. He didn't feel threatened by Eisner and was happy to accept the number two position in the company. In fact, when Disney wanted Wells and Eisner to be joint CEOs, Wells willingly gave way to Eisner when the latter said he'd prefer to be the sole CEO.

This kind of humility is vital in good partnerships, because it eliminates any competition. Wells even carried a note from a fortune cookie in his wallet for 30 years that read, "Humility is the final achievement." His son found it after his death.

A partner who's supportive, passionate and humble is a great asset. When partners can engage with each other without any underlying competitiveness, they can focus on creating success in their company.

### 3. Warren Buffett and Charlie Munger: the partner as a skeptic. 

Warren Buffett and Charlie Munger have had a strong, intellectual bond for almost their entire lives.

Munger is the greatest skeptic to Buffet's ambitious ideas, responding to them in one of three ways: "That's a dumb idea," "That's one of the dumbest ideas I've ever heard" or "You're out of your mind and I'm going to have you committed."

Berkshire Hathaway, their company, bases its decisions on Buffett's reactions to Munger's criticism. They apply these criticisms to decide if they should move forward with an idea or let it go.

Sometimes Buffet will go ahead with an idea even if Munger disagrees with it, but in those cases Munger will also commit to the idea, because he's committed to Buffett as a partner.

Munger has also taught Buffett a lot of important lessons: he's the one who introduced Buffett and the rest of the company to the _buy strong and hold_ strategy, whereby Berkshire Hathaway buys well-run companies and holds onto their stocks in the long run.

There isn't any competition between Buffett and Munger, either. Despite Munger's exuberant personality and the fact that he is a dominant presence in most of his roles outside of Berkshire Hathaway, he is happy working in the number two position behind Buffett at the company.

This isn't only because of humility; Munger truly believes that Buffett is better at what he does than Munger would be. According to his children, Munger is a "book with legs" because of his wealth of knowledge, but Buffett is the one who can turn this knowledge into actions.

The pair complement each other, and they have a lot of fun together as well. They're best friends with deep respect for each other, and although they think in similar ways, they each bring their own talents and skills to the partnership.

Munger openly states that Buffett is a more capable boss; he knows that envy is counterproductive.

> _"If you want to get a good partner, the way to do it is to be a good partner." - Warren Buffett_

### 4. Bill and Melinda Gates: a partnership between equals. 

Bill Gates jokes that he's never done anything alone in life except take exams. Over the years, he's had a number of important partnerships, and none more significant or meaningful than the one with his wife, Melinda.

The couple head their own philanthropic foundation that's committed to solving problems related to global health and American education.

It probably constitutes his most equal partnership too, and not just because he's married to his partner in this case; the success of their partnership also has to do with experience and maturity. Bill Gates now understands that he doesn't need to be a dominant force in order for a partnership to thrive.

Bill and Melinda Gates manage the organization equally and discuss issues openly. They even read the same books when they're on holiday so they can compare their thoughts on them. If they read different books, they discuss them so much that it feels as if they've actually read them together.

When Bill goes on trips alone, Melinda reads up on whatever he's working on so she can ask him about it when he gets back.

Equal partners can also share their fears and weaknesses, and can give each other advice. When one partner always has to be the leader, it can be tough not having anyone to talk to on an equal footing.

When two great minds tackle a project equally, a lot of things become easier. The partners' roles are more flexible, allowing them to shift and protect each other at difficult times. Moreover, if one partner is especially enthusiastic about a project, the other might take a more cautious position, ensuring that they get a more balanced perspective on their work.

Over time, this equality strengthens a bond, as it has between Bill and Melinda Gates — by now, they know each other so well that they constantly finish each other's sentences. One person doesn't necessarily have to be in a position of power for a partnership to work.

> _"Partnership helps you brainstorm...it helps you be smarter faster." - Bill Gates_

### 5. Brian Grazer and Ron Howard: outward opposites with inward similarities. 

Brian Grazer and Ron Howard are completely different on the surface, but they're similar in some very important ways.

Grazer is your typical smooth-talking Hollywood producer; he wears suits and knows how to charm people. Howard, on the other hand, is the child star turned award-winning director who's usually seen wearing jeans and a baseball cap.

Grazer and Howard don't always see eye-to-eye, but they share the same principles and stand by each other. In fact, they agreed early on that they didn't always need to agree.

It's beneficial for partners to play different roles. When they deal with lawyers and business managers, for instance, Grazer often takes charge in order to protect Howard from the harsher sides of Hollywood.

Despite their differences, Grazer and Howard know what they want to accomplish together. Their partnership is also strengthened by the fact that they genuinely like each other.

The pair has a long history together. When Howard switched from acting to directing, he struggled to have people take him seriously because he was only associated with his work on the TV show _Happy Days_. But Grazer was an ambitious young producer and recognized Howard's talent.

The first films Grazer and Howard worked on together were _Night Shift_ and _Splash_. Afterwards, they decided to part ways and pursue their own projects, partly because Grazer felt he wasn't getting enough recognition within the film industry.

However, they eventually realized that they were more successful together, so they teamed up again and started their own production company, Imagine Films Entertainment.

They decided to split the company and its earnings 50/50, even with films that they didn't work on together, or when one of them would contribute more to a film than the other. Sometimes, Grazer even produces other directors' films, like Ridley Scott's _American Gangster_.

Their financial equality illustrates how much they truly share their accomplishments. It also prevents any tension from arising since they aren't keeping score of their successes or failures.

### 6. Valentino Garavani and Giancarlo Giammetti: the lovers. 

Valentino Garavani, the world-famous fashion designer, developed an interest in fashion at an early age, but he owes much of his success to his partner, Giancarlo Giammetti.

Their partnership was founded on love. Garavani and Giammetti met in 1960 and had a romantic relationship for 12 years. They stayed in love even after their romance ended and both began dating other men.

Garavani and Giammetti spent nearly all their time together and knew each other extremely well. In order to help Garavani's fashion designing career get off the ground, Giammetti made several sacrifices; he even stopped studying architecture to save money and prevent Garavani's first fashion design house from going bankrupt.

Giammetti has served as Garavani's organizer since then. He helps plan shows and pick models so Garavani can focus solely on creating glamorous clothes. Garavani doesn't always acknowledge Giammetti's help, but he appreciates it much more than he lets on.

The power dynamic between them also strengthens their partnership. Giammetti doesn't care about fame; he thinks Garavani deserves the glory because he's the real fashion genius. Giammetti is happy to let Garavani enjoy the spotlight, while still knowing the importance of his own contributions to their success.

Giammetti also gives Garavani 51 percent of their earnings, even though he does most of the hard work. He also functions as Garavani's gatekeeper, which entails its own power dynamic: anyone who wants to get to Garavani has to go through Giammetti first.

Garavani has a sizable ego, so Giammetti's dedication to their partnership is a true testament to their love. They're both dedicated to the same goal — Garavani's success — and their strong partnership based on respect and understanding has brought them that success for over half a century.

### 7. Steve Rubell and Ian Schrager: the friends. 

Steve Rubell and Ian Schrager, the founders of Studio 54 dance club in New York, were best friends well before they went into business together. They were both from Brooklyn and met at university.

Rubell and Schrager had very different personalities, so they contributed to the partnership in different ways. Rubell was charismatic and charming, so he effortlessly attracted attention. He was a people person who took naturally to mingling with the stars at Studio 54. Schrager, on the other hand, took care of the business side.

Rubell often pushed his own ideas, while Schrager tended to be more cautious. Despite their differences, they also decided to divide everything down the middle.

Rubell and Schrager had similar principles and approaches, however, and they both learned the value of integrity after doing a stint in prison for embezzling money. They were charged with tax evasion, obstruction of justice and conspiracy for failing to report $2.5 million in income from the club's receipts.

During the trial, the US government offered Schrager a lighter sentence in exchange for testifying against Rubell. Schrager refused and the two went to prison together, which further strengthened their relationship.

When they got out, they vowed to be honest in their business practices and started a hotel business. But just as they were beginning to enjoy success once again, Rubell died of AIDS.

Schrager continued the business and it has since enjoyed great success, but he still feels that it would have been even more successful if Rubell had been working at his side. When partners have as much faith in each other as Schrager and Rubell did, they can play off each other's strengths and build something truly great.

> _"The momentum of two is much greater than the momentum of one."_

### 8. Final summary 

The key message in this book:

**Partnerships between the right pair of people can be hugely beneficial — some of us just work better when we collaborate with someone else. A good partnership won't just bring your company greater success, it can make you much happier too. After all, humans are social creatures.**

Actionable advice:

**Don't keep score.**

Don't compare yourself to your partner and don't stress about the details of who's doing what work. This kind of competitive behavior can be counterproductive. If you think of your partner as an equal, you can both share in all of your successes.

**Suggested** **further** **reading:** ** _The Partnership Charter_** **by David Gage**

_The Partnership Charter_ (2004) describes how business partners can avoid lasting damage to a business relationship by making sure to talk openly about everything. Using a tested process, business partners can put aside differences and successfully build a strong company together.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Michael D. Eisner with Aaron Cohen

Michael D. Eisner has been a leader in the American entertainment industry for over 40 years. After a stint as president and CEO of _Paramount Pictures_, he was the CEO of the _Walt Disney Company_ for over 20 years, from 1984 to 2005.

