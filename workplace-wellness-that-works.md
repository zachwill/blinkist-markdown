---
id: 565c8232990675000700006a
slug: workplace-wellness-that-works-en
published_date: 2015-12-03T00:00:00.000+00:00
author: Laura Putnam
title: Workplace Wellness that Works
subtitle: 10 Steps to Infuse Well-Being and Vitality into Any Organization
main_color: 42C2CF
text_color: 1C909C
---

# Workplace Wellness that Works

_10 Steps to Infuse Well-Being and Vitality into Any Organization_

**Laura Putnam**

_Workplace Wellness That Works_ (2015) provides a refreshing take on how to create more well-being in any organization. It offers a 10-step guide packed with practical examples from the business world on how to initiate, expand and sustain your well-being movement.

---
### 1. What’s in it for me? Get inspired to make your workplace a place to feel well! 

We've all heard about health initiatives in the workplace. Some companies put out fruit baskets, others incentivize working out or using pedometers. These might seem better than companies that don't offer any health initiatives at all, but _workplace wellness_ isn't just about fruits and pedometers. 

Workplace wellness refers to any approach that allows employees to be more human at work. It's about getting the opportunity to move more, to have fun and laugh on the job, to eat more natural foods, to build meaningful connections and to work towards a higher goal. It has more layers than just "being healthy" — it's about feeling engaged, recognized and well at work.

In these blinks, you'll discover

  * how putting family pictures on workers' hard hats can increase security; 

  * why Leonardo da Vinci should inspire your workplace wellness program; and

  * how OzForex's global vacation program creates a sense of fairness.

### 2. To start heading towards workplace wellness you need to become an agent of change. 

What do you associate with a healthy workplace? Picking up the occasional apple from the break room? David from Accounts trying to enlighten you on the perils of cigarette smoke? 

Workplace wellness is much more than that: it's about enjoying your work environment, moving around more and building meaningful connections with other people.

That sounds great, but how do you get there? By instigating a change process. When you take this initiative, you can become an _agent of change_ for your workplace. This means understanding the reason for change and communicating it to others.

Agents of change know their _inner why_ — the reason underpinning the change — and this helps make them authentic, persuasive leaders of the process.

Take Shane Valentine, who teaches for Kids Cook with Heart, a program offering cooking lessons to school children. Valentine's inner why is that almost no children in the United States meet the American Heart Association's Life's Simple 7 criteria for a healthy heart. In response to this, his program specifically addresses the criteria for a healthy diet.

Once you have a why, you'll need people to listen. Using metaphors and emotions is an effective way of communicating your message. To gain followers, you should focus on relating stories rather than regurgitating statistics. Start by building your own _story bank_, a collection of meaningful stories in your head.

The key is to make sure you embody your story. If Valentine were to star in a commercial for McDonald's, his kids cooking program would suddenly lose credibility!

As a next step, you'll need to build a strong case to convince your coworkers of the need to improve workplace wellness. 

One approach could be to highlight any monetary gains that could be achieved by implementing workplace wellness. Take _presenteeism_, a money-draining phenomenon that occurs when your employees are at work but aren't fully present, i.e., they're either not working effectively or not working at all. According to a study at Bank One, 63 percent of costs connected with poor employee health are caused by presenteeism.

> _"Your task is to courageously move forward with sparking a movement of well-being within your organization."_

### 3. Create a vision of the desired final outcome of your well-being movement. 

Do you know what Mikaela Shiffrin said before she won the gold medal for the slalom race at the 2014 Winter Olympics? "To everybody, this is my first Olympics. But to me it's my thousandth." She was referring to her vision; she had visualized the race over and over to build faith in her success, which enabled her to win. So how does this apply to workplace well-being?

Creating a vision allows you to believe in and realize a healthier workplace.

You can start creating this vision by making a collage with images that answer the question: "What's possible for your organization?" Posting your collage on a wall will guide and inspire you as you move through the process.

That's what Teresa Snyder, a team leader at a leading financial institution, did when she introduced her Get Vitality program. Her aim was to convince technically oriented employees of the value of soft skills and self-care, and how the two were related. When team members visualized people being kind to themselves and interacting positively with one another, it deepened the team's understanding of the initiative.

When forming your vision, make sure it's multidimensional. Because well-being is, too. 

Here are two key tenets to address:

First, _physical well-being._ This involves eating healthily, not smoking, getting enough sleep and moving your body. The average American sits for a whopping 9.3 hours per day even though more than _three_ hours of it can be bad for our health! 

So what can you do about this at work? All it takes are simple things like standing up every 30 minutes or having stand-up or walking meetings.

Then there is _emotional well-being_, also called _resilience_. This is your ability to bounce back from changes and it can be fostered by practicing _mindfulness_. Mindfulness entails paying attention to the present moment, deliberately and without judgment. One way to get started with this is to focus on one thing at a time at work, taking a break between two tasks and being observant of that break.

> _"Encouraging others to imagine (…) what could be is the kind of open-minded thinking needed to catalyze your wellness movement."_

### 4. Getting to know the culture of your organization is crucial for your well-being movement. 

Ever felt disengaged at the office? You're not the only one! According to Gallup, 70 percent of the US workforce is disengaged at work. This is toxic for any work environment and costs around $450 billion per year. This isn't good for any of us, but how can we tackle it?

Let's look at workplace culture, the basis for your organization's well-being movement.

Culture can encompass the shared values, social norms, rituals and philosophy of an organization, and it influences everything. Companies like online shoe retailer Zappos understand this and invest heavily in culture, resulting in them outperforming the competition. They believe people perform better if they have fun at work, so they provide free popcorn and costume parades for their staff.

IDEO, a California-based design consultancy firm is another example. They created _The Little Book of IDEO_ which lays out their values such as "Be optimistic" and "Collaborate" to clarify what it means to work there.

Among the many things culture influences is engagement, which is crucial for your movement. In addition, everyone should feel supported by the organization. This is called _perceived organizational support_. If people feel encouraged and valued through regular feedback talks, for example, they're more likely to engage in your program.

But before you begin your program, you need to understand your organization's current culture.

One way of doing this is by using marbles _._ It's almost a no-brainer, but it does give you a pretty clear picture of the company. Simply ask employees to indicate how their day went by placing a green (good), yellow (okay) or red (bad) marble into a jar. If you find a jar brimming with red marbles at the end of the day, you know that the culture needs some serious attention!

Once you've understood your organization's culture, you're now in a good position to create positive change.

### 5. To start your movement, look on the bright side. 

If given the choice, which activity would you do first: something you're bad at or something you're good at? Most likely the latter! And that's exactly where your wellness programs should start, too.

To get your movement off to a flying start: focus on _strengths_, both on the individual and organizational levels.

For business book author Marcus Buckingham, strengths are activities that make you feel strong after you've done them; contrary to weaknesses, which make you feel depleted. 

According to Gallup, strengths correlate with engagement: people whose work is centered on their strengths are six times more likely to be highly engaged in it.

This is similar to the perspective of Patty de Vries, Wellness Manager of HealthySteps, a wellness program run between Stanford Health Care and Lucile Packard Children's Hospital. According to de Vries, there are two ways to approach any new situation: with fear or love. She favors love, since this builds on strength. Think of it this way: Would you rather go to a military-like gym where a trainer screams orders at you telling you how pathetic you are or a gym where someone cheers for your success?

So how can you use peoples' strengths, or _bright spots_, to know where to focus in your program?

To pinpoint strengths, have everyone individually stick green or red dots on a _vitality wheel_, where the green dots represent things they're good at and the red dots things they're not good at. The vitality wheel is split into six dimensions of well-being: career, physical, emotional, community, financial and social. Just by acknowledging what they're good at will give employees the necessary energy to handle the red spots.

To find your organization's bright spots, aim to answer the following questions: 

"How is your company supporting the employees?" — this might be health services, yoga classes or a credit union — and "What are successes of your company's already established workplace wellness?". 

Answers to these questions should give you your starting points.

### 6. Use an interdisciplinary approach to start building your well-being movement. 

Does your company offer healthy perks? In Berlin startups, for example, it's normal to have free fruit in a communal eating area. But just offering healthy snacks doesn't necessarily increase the overall well-being of your employees.

So how can you take your well-being movement to the next level? Take some advice from Leonardo da Vinci. Having been a painter, inventor, engineer and botanist (among other things), da Vinci is a symbol of interdisciplinarity and a fantastic inspiration for your movement.

Use this interdisciplinary approach and consider all the connected departments when you select team members for your movement. You might need people from Facility Management, IT, Marketing and so on. Here's an example of how they can all contribute:

Facility Management could be responsible for sites you need, such as a fitness center; IT could help with a mobile app for services you wish to offer; marketing could assist with motivating and selling your idea to your staff.

And don't forget about people outside the company: community resources like the American Heart Association can give nutritional advice or information on heart disease. A great example is the Kids Cook with Heart program that we saw in the first blink. Health insurance providers could also provide free biometric screenings.

You can broaden your _da Vinci team_ even further with external departments such as safety or organizational development departments. Dow Chemical, the second-biggest chemical manufacturer in the world, has brought together health and safety departments for over 20 years and is heading towards zero injuries and zero adverse events.

In addition, executives and managers can give voice to the movement through the company. Senior positions could help with their knowledge, and managers make excellent contributors if they take part in the wellness activities themselves. 

Getting a variety of people on board is the first step toward getting everyone on board.

> _"Integrated action is equal to more than the sum of its parts."_

### 7. Sneak well-being into your organization by renaming it or faking a withdrawal of your wellness idea. 

Some people will roll their eyes at the mere mention of wellness initiatives: they just don't buy into it or they mistake it for esoteric nonsense. So how can we convince them otherwise? Well, there are two ways to win over skeptics.

The first is _renaming_, and it's the perfect way to sneak well-being into the workplace.

Expressions connected to wellness can be easily replaced by words that have less of a stigma to them. _Health_ can be replaced with _energy_, for instance, and _mental health_ with _emotional intelligence_. At Goldman Sachs, Head of Wellness Laura Young wanted to avoid the term _stress management_ due to its negative connotation and instead opted for _mindfulness and resilience_. This worked well: the company offers one mindfulness session each quarter, with over 500 people signing up for it every time.

Another way to persuade the skeptics is by pretending to withdraw the idea of a wellness program. This means appearing to give up the fight, but instead blending it cleverly into other programs that are already valued and embraced by your organization.

For example, Solano County in California integrated some well-being training into their usual manager's meeting under the guise of sustainable management. These managers learned how to increase their own well-being, as well as improve the work environment of their employees.

You can also use your da Vinci team to open up more entry points into your company. One way could be to hold community outreach activities as a means to give back to your community. This is directly linked to at least two facets of well-being: physical activity and the sense of purpose you feel when you volunteer to help others.

Salesforce, a sales and marketing software provider, has a 1-1-1 model for philanthropy: 1 percent of Salesforce's equity, 1 percent of its product and 1 percent of its employees' time are all given to a good cause.

> _"In a sense, the key to going stealth is to 'give up the fight' in having to prove the value of wellness to either leaders or employees."_

### 8. For lasting behavioral change, connect your well-being movement to basic human needs. 

So you started with the easy things and slipped in your wellness program. Now, how can you maintain motivation? By instilling feelings of _competency_, _autonomy_ and _relatedness_.

To make people feel competent, ask them to share their knowledge and experience. We all have insights and stories about what we do to feel good. Even a simple weight-loss story could be incredibly helpful to staff.

To satisfy the need for autonomy, support employees while they take control of their health and well-being. Leaving room to allow people to find solutions for themselves, or enabling staff go back to college to finish a degree, will encourage fulfillment and independence.

Regarding relatedness, research shows that people like being in teams and perform better when they're part of a group. Schindler Canada, an elevator producer, did an excellent job of relating safety to employees' families by placing pictures of family members on the safety helmets of their employees. As a result, safety regulations were followed far more diligently.

In addition to the above, feelings of _purpose_ and the joy of _play_ strengthen motivation.

Feeling that your life has meaning and purpose gives you energy, so find time to reflect on this. For instance, staff at fashion company Eileen Fisher begin every meeting with a moment of reflection.

Play is vital, too — we love playing! According to Stuart Brown, Professor at Berkeley, play is the basis for social relations, happiness and fulfillment. Volkswagen integrated play into their work environment by transforming the stairs next to an escalator into piano key stairs, complete with sound! This resulted in 66 percent more employees choosing the musical stairs that got them moving over the less healthy escalator.

> _"If we want to intrinsically motivate people, we need to tap into […] universal human needs — with an emphasis on emotions over logic!"_

### 9. Use nudges and cues to make behavioral change last even longer. 

Ever wondered about the fly stuck in the center of male urinals? This is what's known as a _nudge_. This unassuming fly increases accuracy, as men will naturally aim for it when they use the facilities! What does that have to do with workplace wellness, you ask?

_Nudges_ and _cues_ are highly effective approaches to behavioral change. By making small adjustments to the environment according to your wellness initiative, nudges and cues coax us into behaving differently.

A _nudge_ could be an invitation to do something, for example, by making the healthy option the easiest option. If there were apples and bananas on display at the cafeteria cash register instead of candy bars, people would probably grab them as a quick snack.

A _cue_ is more cultural and concerns which activities are considered normal and which aren't. Take a look at the success of social cues of smoking in the U.S.: In the 1960s, it was normal to smoke and almost half of the population smoked. Nowadays, only 18 percent of Americans smoke. It's become abnormal, and actually _feels_ abnormal, to smoke in the U.S. In fact, it wouldn't be out of place for someone to confront you about the ill effects of smoking if you light up around others.

The great thing is, nudges and cues have as many applications as ideas for increasing well-being.

Here are a few tips you can try at your workplace: to improve the physical aspect of well-being, try nudges like signposting stairwells or outlining indoor walkways to make walking meetings more appealing. Some cues could be policies that every meeting under 30 minutes should be a stand-up meeting or taking part in rituals like morning stretches.

To support the emotional aspect of well-being, you could integrate mindfulness practices to kick off meetings, and to enhance the social part of your wellness initiative, you could provide on-site childcare or a central sunny atrium area.

> _"Nudges and cues create an oasis of vitality that make it easy and normal for individuals to make the healthy choices."_

### 10. Some tips and tricks on launching and iterating your well-being movement. 

You've recognized the downfalls of your workplace and shored up some fantastic health-boosting strategies. Now it's time to launch your program. Take a look at the following tips and insights for the best way to jump-start your movement.

First, _learning through doing_ and encouraging a _growth mindset_ in your organization will ease your staff into your wellness program.

Learning through doing means not delaying your program until it's perfect. Just launch it, and then fine-tune it as you go. Take design company IDEO, which actually urges people to have this trial-and-error attitude. They even dedicate a program to it — called Fail Faster!

A _growth mindset_ means believing in the power of change for all people at all times. It's never too late to get onboard with this way of thinking and you can foster it by posting questions like "What do you stand for?" on social media platforms and in employee bathrooms.

Once your program is running, monitor it, and assess staff engagement and its organizational impact. This will make refining and improving it far easier.

In addition to counting participants, you can use the following methods to get a better feeling for employee engagement: 

Start by observing how people look when taking part in the program. Speak with them, ask them how they like the program, and observe what happens after a big event. Do you notice any lasting changes?

To see your movement from another perspective, measure its impact on an organizational level. For instance, are there changes in absenteeism, presenteeism (remember, when you're physically present but mentally disengaged) or productivity? Have medical costs decreased? Has the rate of injuries dropped? Has the quality of your employees' lives changed?

If you monitor your program closely and stay up to date with your da Vinci team, you shouldn't have anything to worry about during the launch. If trouble does crop up somewhere, though, don't fret — just tweak it in the next iteration.

> Thomas Edison needed 1,000 iterations to invent the bulb, so no worries if the beginning is not easy.

### 11. Going global gives your well-being movement another kick. 

If you're part of an international company, getting local staff to participate in your program is great, but what comes next? Starting to think big. Now it's time to start spreading your movement throughout company branches all over the world. This can be best achieved by cultivating a _global mindset_ in your organization.

The first part of this mindset involves _unitizing standards_ inside your company. This can also increase well-being by encouraging a culture of fairness. OzForex is a great example of this. OzForex is a multinational foreign exchange company with headquarters in Australia and many international offices all over the globe. They decided, in accordance with the normal and guaranteed four weeks of holidays in Australia, that they would start offering these holidays to all offices throughout the organization so that all employees could benefit from it.

To further inspire consideration for the company at large, you could also incorporate the following questions into your next workshop: 

Stress, anxiety and depression are growing exponentially all over the world. Why do you think this is? How can you address these taboo topics at work, and what can you do to counteract these statistics?

Another thing you can do to spread your message is learn from partners inside and outside your company via _sharing best practices._

Each department in your company is different and will therefore experience your wellness program differently. Sharing these experiences will be helpful for everyone.

For example, the India office of network provider Brocade tailored the name of their _WellFit program_ to speakers of different languages. In Kannada, the local language in the Indian region of Bangalore, it's called _Kursi biDu, Stretches maaDu_. 

In the realm of teaching, EdVillage is a small nonprofit and highlights outstanding education all over the world. Taking it one step further, EdVillage also facilitates the sharing of best practices between teachers internationally to improve education worldwide.

### 12. Final summary 

The key message in this book:

**Workplace well-being is crucial for every organization. By being an agent of change with the help of a diverse team, you can create, instigate and develop healthy programs and habits that everyone in your company can benefit from.**

Actionable advice:

**Cut the candy.**

There are simple nudges, or changes to the environment, which can improve eating and drinking habits at your company. For example, you could offer healthier options in vending machines and cafeterias. Rather than stocking up with sugar-filled candy bars, you could replace these with nut-, fruit- or vegetable-based snacks. You could also switch out candy and cookies in meetings for fruit or carrot and celery sticks with a healthy dip to make an unhealthy choice simply not an option! You may even install an on-site garden that enables staff to grow their own food and take it back home.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Laura Putnam

Laura Putnam is the founder and CEO of Motion Infusion, a well-being consultation firm. As well as being heavily involved in the American Heart Association, Putnam is a trainer, dancer and consultant, and works with nonprofit organizations, academic institutions and Fortune 500 companies.

© Laura Putnam: Workplace Wellness That Works copyright 2015, John Wiley & Sons Inc. Used by permission of John Wiley & Sons Inc. and shall not be made available to any unauthorized third parties.

