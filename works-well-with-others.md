---
id: 5677bac2fba8510007000000
slug: works-well-with-others-en
published_date: 2015-12-21T00:00:00.000+00:00
author: Ross McCammon
title: Works Well With Others
subtitle: An Outsider's Guide to Shaking Hands, Shutting Up, Handling Jerks, and Other Crucial Skills in Business That No one Ever Teaches You
main_color: EFE5C2
text_color: 9C8B4F
---

# Works Well With Others

_An Outsider's Guide to Shaking Hands, Shutting Up, Handling Jerks, and Other Crucial Skills in Business That No one Ever Teaches You_

**Ross McCammon**

_Works Well With Others_ (2015) is a guide for professionals — both new and old — seeking tips and tricks for handling themselves in the modern workplace. From mastering the interview process to fitting in on your first day, these blinks teach you those crucial social rules that no one ever talks about.

---
### 1. What’s in it for me? Take control of the things that used to scare you. 

Most of us have at least a few irrational fears. Maybe speaking in front of strangers terrifies you. Maybe you break out in cold sweats during job interviews. Or maybe just choosing an outfit for the day is a daunting task.

Well, it doesn't have to be that way.

All those potentially awkward, scary or embarrassing situations are actually pretty easy to handle — if you master a few basic tactics and techniques. These blinks will lay them all out.

In these blinks, you'll learn

  * why wearing a bright tie can make you feel more confident;

  * why you shouldn't BS your way through a job interview; and

  * how to give a toast that all the guests will remember.

### 2. When talking to a recruiter, learn to put yourself in her shoes. 

If you've applied for a job lately, you'll know it's common practice these days for companies to use an external recruiter to determine who gets an interview with the hiring manager. But sitting down for a meeting with a recruiter doesn't have to be a stressful situation. With the right mindset, you can ace the meeting and get that call back.

First off, it's important to understand what a recruiter is looking for in an interview. 

Contrary to what most people think, recruiters are more interested in building a relationship with you than in helping an employer fill a vacant position. If they don't find something for you today, they might still come across the perfect job down the line. So think of the meeting as an informative conversation, an opportunity for the recruiter to learn more about you and your career goals.

A good technique is to put yourself in the recruiter's shoes. Imagine _you_ are the one who has to find a new employee for a company. You'll meet a lot of well-educated and motivated people who may not be the right fit for this position, but might be perfect candidates for the next business you start recruiting for.

So, with this in mind, how should you act in front of a recruiter?

First of all: Don't be late! Tardiness makes a bad first impression that is always difficult to overcome. 

Second: Don't lie! If you really are right for the job, being truthful about yourself will prove it.

And third: Don't be afraid to ask questions. Find out as much as you can about the position and what the company is looking for. By learning what qualities they're seeking, you can emphasize those traits in yourself during your interview with the hiring manager.

Finally, after the meeting, send a thank you note to the recruiter. It's a simple gesture that leaves a good impression.

> _"Nothing can be found out about a person less than a month into a job. Nothing. Because you're not seeing the real person."_

### 3. Making eye contact will help you succeed in meetings and interviews. 

Everyone knows the importance of making a good first impression, and nowhere is this more important than in a job interview. Right when you walk into the room, the outcome of the entire interview is at stake. That first impression is made within the first few seconds. 

So what's the secret to making a positive first impression? Eye contact.

People form judgments within milliseconds of meeting someone. And when you make eye contact you are instantly sharing a lot of information: you are communicating your social skills, friendliness and emotional stability.

Avoid looking at the floor, the ceiling or off to the side when you walk into the interview room. If there are multiple people in the room, be sure to make eye contact with each of them.

Studies have even shown that candidates who maintained a good level of eye contact were considered far more favorably than those who didn't. So keep your eyes on the prize and stop staring at the interviewer's tie!

If you're skeptical about this, try this experiment: find a mirror and make eye contact with yourself for just a second before glancing away. Okay, reset. This time, hold your gaze for at least five seconds. Doesn't that second, steadier version of yourself seem more trustworthy?

And that's the magic of eye contact: if it's consistent, you'll come across as both more trustworthy and more competent. Conversely, the more you shift your gaze and look away when asked a question, the less confident you'll seem.

That said, don't go overboard! It's not a staring contest and you're not trying to plumb the depths of the interviewer's soul.

### 4. Avoiding certain words and phrases adds to the professionalism of your self-presentation. 

So now that you've aced the interview and landed your dream job, how do you maintain your professionalism? Well, part of being a professional is talking like one. That means ditching some of the phrases and words that you might use outside the office and sticking to what is appropriate in a professional environment.

One quick and easy way to improve your professional vocabulary is to drop the habit of apologizing. 

If you're confronted with a mistake at the office, avoid saying, "I'm sorry." The more appropriate response would be, "I understand what went wrong and I'll make sure it won't happen again."

Apologies carry an emotional weight that can be uncomfortable in a professional environment. By acknowledging the problem and offering your willingness to take corrective action, you're speaking in a helpful and professional manner. 

Another phrase to remove from your vocabulary is, "Does this make sense?" Saying this suggests that you yourself are unsure of the intelligibility of your statements and that you're hoping someone will endorse your nonsense. Obviously, this is not a good professional impression to make.

Keeping things professional means keeping colloquial phrases like "grab" out of the workplace. 

How many times have you heard someone say, "Let's grab a coffee" or "Can I grab you for a quick meeting?" Using "grab" can imply that you're not valuing the other person's time in a professional manner. Instead, show respect by asking, "Can we _have_ a meeting?" or "Let's _have_ a cup of coffee and discuss this."

As we move through our day, talking to friends, family and coworkers, it's easy to forget to adjust our vocabulary to our environment. But if you want to maintain a high level of professionalism, it's important to use professional language.

### 5. A toast can be a great opportunity, as long as you know how to seize it. 

Whether you're celebrating a business breakthrough or a friend's wedding, a thoughtful toast can be the perfect gesture. But a successful toast isn't a ramble or an unending recitation of names and acknowledgements. A successful toast is a powerful moment — a taut, punchy speech that people will remember for a long time. So how do make your next toast a successful one?

Ask yourself, what do you expect when someone gives a toast? 

There are a lot of options, right? It might be funny and light or touching and poignant. Use this flexibility to your advantage the next time you raise a glass at a party or a business lunch.

But don't make the mistake of forgetting an important name.

Everyone expects someone to raise a toast to the bride and groom at a wedding or to the guest of honor at a birthday party. But when you're giving a toast at a Christmas party or a business celebration, it can be easy, and embarrassing, to accidentally leave out an important name.

A simple and easy way to ensure that no one gets left out is to avoid extemporizing. When you know the event is coming up, take the time to write some notes and make sure every deserving individual gets honored.

Here's another smart way to avoid a bad toast and to keep yourself from merely listing off name after name: incorporate broad phrases like, "Here's to all of you" and "It's your work that made all of this possible." Such sweeping statements make everyone feel included and ensure that there are no hurt feelings after your toast.

### 6. Don’t underestimate the importance and power of your clothes. 

Everyone thinks about what they wear for important occasions. But you might be surprised to learn how much of an impact the right outfit can have.

Even if you usually don't pay much attention to your outfit, you probably know that clothes are a powerful tool.

What you wear instantly tells people a lot about who you are. The right attire can convey assurance and provide a sense of individuality. For example, a bright colored tie or a loud sweater can signal self-confidence to the people you meet.

That's why clothes are crucial to making a good first impression. 

Your choice of clothing can even make you stand out as the right candidate at a job interview. By wearing your favorite Star Wars shirt to an interview with a start-up, you can communicate your style and that touch of geekiness that means you'll fit in perfectly.

On the other hand, say you have a big interview at a bank. Wearing a nicely fitted new suit will give you a feeling of confidence _and_ show the bank that you're a serious and trustworthy individual.

And of course, if you're applying to be a drummer in a heavy metal band, you should probably wear something else entirely.

So remember, consider all the options and give yourself the advantage by dressing accordingly.

To complicate matters, your choice of attire may actually affect your psychology, too.

Researchers at Columbia University refer to the effect clothing may have on the wearer's mood and behavior as _enclothed cognition_. The data shows that people who wore a lab coat showed a higher attention to detail compared to people in street clothes!

This is the power of clothing: bad outfits can make you feel uncomfortable or self-conscious; quality clothes, in contrast, can make you feel smart, sexy and confident. Just think about your favorite shirt or dress. It's not really about how it looks; it's about how it makes you feel!

### 7. Final summary 

The key message in this book:

**The business world is full of social landmines. Whether you're being interviewed by a recruiter, trying to dress appropriately or giving a toast, you can give yourself the advantage and avoid embarrassing yourself by keeping some simple tips in mind. Remember: You're in control of your professional success!**

Actionable advice:

**Never be late for job interviews**

You've heard it many times, but it can't be stressed enough: When it comes to job interviews, be on time! Sure, events out of your control may slow you down. There might be heavy traffic or a public transportation breakdown. So always give yourself enough time! That way, even if something unexpected happens, you'll still be punctual. 

**Suggested** **further** **reading:** ** _Crucial Conversations_** **by Kerry Patterson, Joseph Grenny, Ron McMillan and Al Switzler**

We've all been in situations where rational conversations get quickly out of hand, and _Crucial_ _Conversations_ investigates the root causes of this problem. You'll learn techniques to handle such conversations and shape them into becoming positive and solutions-oriented, while preventing your high-stakes conversations from turning into shouting matches.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Ross McCammon

Ross McCammon, senior editor of _Esquire_ magazine since 2005 and a longtime columnist for _Entrepreneur_ magazine, is an expert on business etiquette. He is also the author of _The Impostor's Handbook_.

