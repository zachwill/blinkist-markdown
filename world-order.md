---
id: 56661237cb2fee000700002d
slug: world-order-en
published_date: 2015-12-10T00:00:00.000+00:00
author: Henry Kissinger
title: World Order
subtitle: Reflections on the Character of Nations and the Course of History
main_color: 0D7AC0
text_color: 0B69A6
---

# World Order

_Reflections on the Character of Nations and the Course of History_

**Henry Kissinger**

_World Order_ (2014) is a guide to the complex mechanisms that have governed international relations throughout history. These blinks explain how different countries conceive of different world orders and how they are held in balance or brought into conflict.

---
### 1. What’s in it for me? Understand the logic behind international conflicts and cooperation. 

Trying to grasp the complexities of politics can be a frustrating exercise. Have you ever wondered why conflicts between nations go on and on despite exhaustive negotiations, mediation efforts from major international actors and hundreds of hours of meetings and peace talks?

These blinks will offer plenty of fascinating insights into the sphere of international politics, all of which center on one explanation: simply put, many conflicts are rooted in nations' irreconcilable expectations as to how the world itself should be divided and organized. Through the observations and experiences of former US Secretary of State Henry Kissinger, you'll grasp why these expectations differ so much from nation to nation, how they influence states' foreign policy and how they have shaped history over time. 

In these blinks, you'll find out

  * how a brutal war actually improved diplomatic relations within Europe;

  * why the childhood of a single politician can influence the fate of nations; and

  * how a bygone Mongolian invasion is connected to present-day Russian policies.

### 2. A world order is a set of ideas about how the world should be, and a roadmap for any country’s foreign policy. 

Do you ever hear discussions about historical conflicts like the Cold War or current events like the many crises in the Middle East and wonder which historical circumstances actually caused them? Well, a key starting point for most such conflicts is the history of _world order_.

World order essentially refers to a combination of thoughts and beliefs shared by a greater community. Large communities such as regions or civilizations develop ideas and beliefs about how the world should work, such as who should have power in a civilization. 

Take European countries as an example. Most of them agree that all governments should be chosen through free, democratic elections. But this belief doesn't just apply to their countries — it's something they desire for countries around the world. 

Now that you know what a world order is, what makes it so important?

Well, the grand ideas and firm beliefs that define a nation's world order will naturally have a major impact on how it acts toward the outside world; indeed, many wars have been waged over differing views on world order. 

In addition, world orders can determine both the foreign policy platform of a nation and its long-term strategies. World orders are applied through foreign policy decisions that advance the specific agenda of any given nation; these big ideas provide a secure foundation for forward-looking strategies and strong planning. 

For example, the talented strategist Prince Klemens von Metternich was guided by the motto that to maintain stable international relations, you needed to acknowledge the true interests of all actors and not just those of your own nation.

### 3. The balance of power can be essential to maintaining world order. 

In the aftermath of World War II, the capitalist-run West, particularly the United States, and the communist Soviet Union began vying for control of global politics. This marked the birth of the conflict known as the _Cold War_, an ideological battle that would span decades, keeping the world's citizens in constant suspense.

But during all those fraught and intense years, no actual war broke out between the United States and the Soviet Union. Why didn't the conflict go further?

It was likely because the two nations were very equally matched; both felt just as threatened by their enemy as their enemy did by them. As this shows, a balance of power can actually result in a stable political situation. 

This may come as a surprise, since balanced forces might appear to cause a total standstill that precludes development and growth. Indeed, a perfect balance of power prevents people from making drastic moves, as all parties are aware that the others' strength would quickly bring strong resistance. As a result, nobody can gain a definitive advantage. 

However, this situation might actually be desirable because it allows the different actors time to plan and attend to important issues. Therefore, many politicians welcome a balance of power if clear supremacy is not possible.

During the Cold War, neither nation could gain any real ground because the other would always keep it in check. Neither side dared to initiate direct military action because the likely retaliation would have been devastating. 

In fact, the stalemate didn't cease until the Soviet Union collapsed from the inside out, disrupting the balance of power. Each side's huge nuclear stockpiles had made the situation extremely delicate, and humanity dodged a bullet. 

So, while the Cold War was a terrible period in history for many reasons, the balance of power thankfully deterred either side from using nuclear weaponry. They simply had too much respect for each other's capabilities, and this fact alone prevented a full-scale nuclear war.

> _"...in 15 cases in history where a rising and an established power interacted, ten ended in war."_

### 4. One of the most influential world orders originated in Europe. 

While there has never been a completely global world order — that is, one that spanned the entire planet — one in particular came very close. It first took shape in Europe when the Peace of Westphalia laid the foundation for a new way of conducting politics. 

Here's what happened:

Between 1618 and 1648, Europe was decimated by the Thirty Years' War. The conflict began as a dispute between Catholic and Protestant nations but rapidly escalated into an endless series of bloody battles that claimed millions of lives. Eventually, diplomats from the countries and alliances involved met in the German region of Westphalia to agree on peace treaties and make arrangements for future interactions. 

They decided that every nation, regardless of its size, had a right to its own sovereignty and decision making, and that other nations were required to respect these rights. The diplomats agreed that no single truth prevailed amongst all nations; thus, the treaty they adopted acknowledged that many different beliefs existed in Europe, all of which were equal. 

As a result of the principles outlined in the Peace of Westphalia, a new world order formed that stabilized Europe. So, while the main points of the treaty might not sound so remarkable today, at the time a basic respect for sovereignty was a novel and absolutely essential concept. 

In fact, the treaty was so successful that it provided for a relatively stable system for European nations until the twentieth century. Small wars still occurred, but never any as large and devastating as the Thirty Years' War. 

Because of its success, the central principles of the treaty were often replicated and have remained part of the European world order to this day. 

But why exactly was the European world order so successful?

> _"We can gain no lasting peace if we approach it with suspicion and mistrust or with fear."_

### 5. Flexibility is a key ingredient of the European world order. 

So, balancing powers can stabilize world order. But in the case of Europe, another essential component came into play: flexibility.

During the Thirty Years' War, any nation in Europe could ally with any other, which functioned as a defence against one nation becoming too powerful. After the war, Europe's system of alliances had become very flexible — coalitions formed easily as long as they seemed strategically beneficial, even across religious lines.

Therefore, no single nation could dominate the others. If any country attempted to do so, all the other European nations would stand together to defeat them. 

Consider the downfall of Napoleon Bonaparte, Emperor of France, in the early nineteenth century. Napoleon set out to conquer all of Europe — and he almost did, until the rest of Europe teamed up to defeat him at the Battle of Nations near Leipzig. 

But flexibility in Europe came about as a result of many factors. During the Thirty Years' War, several nations changed sides, leaving their former allies behind. It was therefore illogical for countries to rely entirely on any one ally, which in turn promoted flexibility.

However, as soon as this flexibility broke down, Europe descended into chaos and war. 

Following the _Franco-German War_, fought during 1870 and '71, the victorious German leaders founded a new German state at the French castle of Versailles. This was a major insult to the French and took an alliance between the two nations off the table. Instead of flexibility, Europe now had rigid fronts. 

While new European alliances formed, they tended to be extremely stiff, with both parties pledging steadfast, enduring support. 

So, when the Austro-Hungarian Empire waged war on Serbia following the assassination of Archduke Franz Ferdinand, they sparked a chain reaction that activated all the alliances in Europe. The result was World War I.

### 6. Germany played an important role in maintaining the European balance of power. 

You might be surprised to learn that Europe's world order was made especially possible by a country that most people don't associate with the historical stability of Europe: Germany. 

Until 1871, Germany was not a unified state, but rather a collection of small states and principalities all united by the German language. This was essential to the prevailing order of Europe, because this collection of small states was not considered a threat; in contrast, a united Germany would have been too large and powerful. Such a big and populous nation would have surely disrupted the delicate balance of power between the European nations of the time. 

On the other hand, the small German states were also allied through many common interests. In the event of an outside threat, they could readily mobilize a formidable army. As a result, other European countries were careful not to provoke the German-speaking states. 

Furthermore, Germany's location at the heart of Europe also helped the balance of power. Between the lot of them, the German states could reach the borders of nearly every other European country in an instant. This fact discouraged most countries from attacking the German states and from provoking other European countries by vying for power. 

However, Germany's essential role in balancing power meant that as soon as Germany changed, Europe's entire order was thrown into question. 

So, when the small German states unified under Chancellor Otto von Bismarck in 1871, a foundation was laid toward the world wars that would decimate Europe in the following century. The newly unified Germany was so large and militarily powerful that it destroyed the continent's delicate balance of power.

After joining together, the new Germany went on a quest for power, and by invading Belgium eventually provoked World War I. In fact, given what the continent has been through, it's incredible that Europe has any semblance of order today!

### 7. When it comes to politics, people and nations are strongly influenced by their origins and history. 

Why should we care about the roots and upbringings of leaders like US President Barack Obama and German Chancellor Angela Merkel?

People and their political actions are of course deeply influenced by how they are raised and the context in which they come of age. Take Klemens von Metternich and Otto von Bismarck, two of the greatest strategic diplomats of their time:

Klemens von Metternich was raised in Germany near the French border, and went on to become foreign minister of the Austrian Empire. Having been raised in a multicultural atmosphere, he took into account the interests of every nationality living within Austria's borders. For him, the final objective of politics was peaceful coexistence. 

Otto von Bismarck grew up in a Prussian house of nobility. In contrast to von Metternich, von Bismarck saw politics as a means to an end, and was firmly of the opinion that diplomacy was meant only to serve the interests of one's own state. 

However, it's not just the actions of people that are influenced by history — the same is true of entire countries. 

Take Russia as an example. Over the course of its history, the country has suffered invasions every time it has let its guard down; the devastating Mongol invasion in the 13th century and other invasions that led to the "Times of troubles" around 1600 all happened at times when Russia had curtailed its military defences. As a result, Russians today are deeply mistrustful of anyone that encourages them to reduce their armed forces. 

In contrast, consider the states of Europe that spent centuries suffering through bloody wars. This painful history has taught the continent that peaceful coexistence is essential to their mutual prosperity. As a result, they've worked hard to form alliances and resolve conflicts through diplomacy rather than military action. 

So, every human and every nation is deeply influenced by its past experiences; this fact is especially relevant in the Middle East, where various different civilizations and nationalities butt heads.

### 8. Many of history’s great conflicts are the result of opposing world orders. 

It might seem obvious, but when many nations meet, the risk of conflict increases: different world orders tend to be incompatible. 

This could have something to do with the fact that, by definition, every world order purports to apply universally. As such, each world order fails to acknowledge the existence of alternative, highly divergent world orders, and therefore is not designed to be applied alongside them. 

For instance, as we'll see in greater detail later on, a world order based on an orthodox interpretation of the Quran is at odds with most others. 

But even the Cold War is a great example of different world orders coming into conflict. The main antagonists of the conflict represented fundamentally divergent world orders: the United States upheld a system of free market capitalism and the USSR remained steadfast in its commitment to communism. 

As a result, the armed conflicts that took place as offshoots of the Cold War, such as the Korean War, were always fought as proxies to defend the world order of the two major powers. Conflicts would break out in less powerful countries over the issue of whether a state should join the democratic or socialist nations, or change its political leadership. 

Many of the conflicts that have occurred, and continue to occur, in the Middle East can likewise be chalked up to contradicting world orders. The Arab States are highly diverse, full of different kinds of people with entirely different world orders. This fact has triggered numerous wars over global visions based on diverging interpretations of Islam.

### 9. World orders based on radical Islam are dangerously incompatible with all others. 

As we now know, world orders tend to contain far-reaching concepts with many applications. As a result, it's often difficult to resolve discrepancies between them. While the idea that certain world orders are entirely incompatible with all others may seem extreme, such world orders do exist. 

Specifically, world orders that are based on strict adherence to the Quran leave no room for others. 

According to an orthodox interpretation of the Quran, the world breaks down into two parts:

First come the states that are under Muslim leadership and ruled by Islamic law. According to extremists, these nations should exist as one large unit ruled by a single Muslim leader. These states are referred to as _dar al-islam_, or the _House of Islam_. 

Second are all states that aren't under Islamic rule; in other words, all remaining states. Orthodox interpretations consider it to be the holy obligation of all Muslims to ensure that, one day, these states will also be ruled by the laws of Islam; Muslims are also permitted to deploy any means necessary to make this a reality. Therefore, these remaining nations are called _dar al-harb_, or the _realm of war_. 

So, it's easy to see why these strict dictates and their claim to the entire world would be incompatible with other world orders. But the Westphalian concept of world order in particular is at odds with those based on strict interpretations of the Quran. 

According to the Westphalian concept of order, every nation has a right to make its own decisions as long as they are in line with a basic set of rules and are respectful of state sovereignty. Obviously, this is totally divergent from the crusade to build a global, Islamic-ruled state. 

For instance, in the Westphalian concept, every nation is allowed to abide by the terms of its own religion, an important result of the Thirty Years' War. A world order defined by radical Islamic beliefs would find such a choice unthinkable.

> _"A peaceful world order depended on the ability to forge and expand a unitary Islamic entity..."_

### 10. The United States promotes and instrumentalizes a world order based on the Westphalian balance of power. 

Now that you've seen how world order works, let's take a look at a clear and tangible implementation of a world order: that of the United States. 

First off, the United States often used the concepts detailed in the European world order to its advantage. For a long time, the three greatest rivals of the United States were Russia, China and Japan. Using a method typical of the Westphalian world order, American leaders worked to pit their enemies against one another. 

The goal was to ensure that the rivals would always balance each other out, and none would have the wherewithal to directly attack the United States. 

For instance, when Japan was rapidly ascending to power in the early twentieth century, Theodore Roosevelt, the US President at the time, made the decision to display America's naval power. He deployed an armada of 16 ships known as the White Fleet on a world tour that made many stops in Japan. 

The tour's official motto was to take a "practice cruise around the world," but it was also clear that the United States was demonstrating the strength and size of its naval fleet. Such a demonstration of force, without resorting to any explicit attacks or aggression, is paradigmatic of the European world order. 

But the United States didn't just use Europe's concepts of world order; they also made a commitment to them and to viewing themselves as the defenders of that world order. In fact, parts of the Westphalian world order have been used time and time again by the United States to justify wars and armed conflicts. 

Just consider how often the country has waged war against powerful nations that disrupt the global balance of power and state sovereignty, like when they attacked fascist Nazi Germany during World War II.

> _"[The] United States would become the indispensable defender of the order Europe designed."_

### 11. Final summary 

The key message in this book:

**Different countries' world orders — the series of beliefs that represent and govern their outlook on global affairs — are the building blocks of international relations. While some world orders, like the one developed in 17th century Europe after decades of war, are based on mutual respect and state sovereignty, not all visions of the world are as well equipped to deter conflicts.**

Actionable advice:

**Show respect to the people you are debating with.**

The next time you find yourself in a heated discussion, keep in mind that showing your conversation partners respect, even through small gestures like greeting everyone in the same manner, can make a big impact. For instance, during the conference that resulted in the Peace of Westphalia, every diplomat was treated the same regardless of how powerful their nation was. As a result, tensions eased and a broadly agreeable result was achieved. 

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Henry Kissinger

Henry Kissinger is a German-born former American diplomat. He served as US Secretary of State for four years and was awarded the Nobel Peace Prize in 1973. He played a key role in shaping twentieth-century US foreign policy and has written over a dozen books on the subject.

