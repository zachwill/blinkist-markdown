---
id: 596b2528b238e1000601885f
slug: write-it-down-make-it-happen-en
published_date: 2017-07-17T00:00:00.000+00:00
author: Henriette Anne Klauser
title: Write It Down, Make It Happen
subtitle: Knowing What You Want – and Getting It!
main_color: 76563F
text_color: 76563F
---

# Write It Down, Make It Happen

_Knowing What You Want – and Getting It!_

**Henriette Anne Klauser**

_Write it Down, Make it Happen_ (2001) offers useful and practical advice for your personal, professional or romantic life. The advice is centered around writing down your goals and visualizing both your fears and aspirations as a way to overcome your perceived limitations and make your dreams reality.

---
### 1. What’s in it for me? Zero in on your goals and make them happen. 

If the story of your life was a novel and you were the novelist, how would you start writing? After all, you _are_ the author of your own story. Imagine if you took that idea literally, and started listing your deepest wishes and aspirations, and started getting them on paper.

These blinks will explain why that's not such a crazy idea. In fact, pinpointing your dreams can help bring them closer to reality.

You'll be amazed at what happens once you start writing down your dreams.

In these blinks, you'll learn

  * how a self-written check earned Jim Carrey his first $10 million;

  * why almost all cars are silver Toyota Yarises; and

  * how learning to ride a unicycle can help you with your French classes.

### 2. To reach your goals, you must write down precisely what you want. 

A lot of people only have vague ideas about what they really want. They desire more money, a better job, someone to love or the chance to travel somewhere exotic.

You may have similar desires of your own. But if you want to make them a reality, you need to be precise.

So rather than saying "I want money," say, "I want exactly enough money to open a restaurant." And when it comes to love, figure out exactly what you're looking for. Maybe it's someone who is funny, kind and doesn't mind that you don't want to have kids.

Being so specific might sound extreme, but if you want to reach your goals and not veer off course, you need to know exactly what it is you want.

Once you're clear on your goals, write them down on paper.

There are a number of success stories that show the power of writing down our dreams.

Jim Carrey has a famous story of writing himself a check for $10 million, when he was just an aspiring actor. Sure enough, years later, he was earning $10 million per film!

Then there's the football player Lou Holtz, who had not one but 107 different goals for his life — and he wrote down each and every one of them. He did this in 1966, when he was unemployed and just 28 years old. Among his goals were to meet the president of the United States, coach a football team and hit a hole-in-one golf shot.

Amazingly, now 50 years later, Holtz has fulfilled nearly every goal. On his website, you can see photographs of him shaking hands with both the president and the pope; as the coach of the University of Notre Dame football team, he won the 1988 national championship; and as for his luck at golf, he's managed to hit not one, but two hole-in-one shots — so far.

When you set concrete goals for yourself, there is no limit to what you can accomplish. But you may harbor some doubts, so let's take a closer look at the power of pen and paper.

### 3. You can prime your brain to recognize opportunities that will help you reach your goals. 

See if this sounds familiar: your mother calls to say she's bought a new car — let's say it's a silver Toyota Yaris — which surprises you since you've never heard of this car before.

The next day, however, you start to see this car everywhere you go, and many of them are silver, too! How is this possible?

This is the magic of your brain's filtering system, known as the _Reticular Activating System_, or _RAS_.

The RAS is what allows us to sharpen our attention, and it can be primed to notice, or not notice, certain things.

Parents of newborns are familiar with the RAS, as it is what allows them to sleep through just about anything, like traffic noise or a blaring television, yet wake up at the slightest peep from their baby. This is due to their RAS being biologically primed to hear when a child is in need.

This is the same mechanism that will allow you to hear when your name is mentioned across the room at a loud party. Despite the music and dozens of simultaneous conversations, when this happens your ears will perk up and you'll find yourself suddenly being able to overhear a conversation that was mere background noise just seconds earlier.

What's more, the RAS can also help you achieve your goals. When you write those clearly defined goals on paper, you'll prime your brain to begin its unconscious work on the project.

After you write down your goals, your RAS filter will constantly be on the lookout for messages or information that will help your quest. So, after writing down that you want enough money to open a restaurant, you'll be primed to overhear someone at a party who says they have a history of investing in restaurants. Otherwise you never would have noticed.

In the next blink, we'll learn how you can deal with the fears that prevent you from realizing your dreams.

### 4. Writing down your fears is a way of acknowledging them and breaking the spell they have over you. 

The RAS is a great example of how unconscious activities in our brain help us realize our dreams.

But we all also have unconscious fears, which are often tied to our goals, like big obstacles. However, by doing the same thing and writing these fears down on paper, you can help yourself recognize and conquer what stands in your way.

For aspiring writers, the biggest obstacle is often the fear of being rejected by a publisher. Because of this fear, many writers will send their work to a small number of publishers and give up hope when those two or three rejection letters come back. Immediately, they start to believe they aren't good enough and should let go of their dream of being a writer.

This wouldn't happen if they had acknowledged their fear of rejection ahead of time by writing it down. If they had, they'd know that a few letters would only be a temporary setback. They would also remember that countless brilliant and best-selling authors, like Frederick Forsythe and J.K. Rowling, received dozens of rejection letters before becoming wildly successful.

Of course, the fear of rejection and criticism isn't isolated to writers. Many of us share those fears, and they're a primary reason we end up pushing our dreams aside. When fear goes unacknowledged, it sits in the gut, where it festers and acts like a spell that keeps you from being creative or having confidence in your power to follow through with your ideas.

Writing down your fears can break this spell, but again, you have to be as precise as possible so that you become conscious of these fears and they gradually lose their power over you. Once you do this, you'll see how well you'll be able to recognize a fear for what it is and begin to unlock your true potential.

In the next blink, we'll look at other techniques to help identify and work through these worries.

### 5. Use rapidwriting to power through mental blocks. 

Once you've identified your fears, you can move on to dealing with the worries and concerns that might accompany them.

One of the best ways to get rid of worries is to express them through a technique called _rapidwriting_. This technique is outlined in detail in another of the author's books, _Writing on Both Sides of the Brain_, but it's essentially a method to access the deep recesses of your mind.

As the name suggests, rapidwriting involves quick, nonstop writing as way to power through a problem and work through worries, anger or issues that might be causing a mental block. In this technique, you never stop to look back at what you've written and just keep going.

Your first sentence might be something like, "I don't know what to write and think this is stupid . . ." But a few sentences later you'll likely be pouring your heart onto the page.

Rapidwriting is a lot like the Jewish tradition of _kvetching_, which is a way of talking through your feelings and getting rid of anger or resentment. It might start as a bitter diatribe, but by the end you'll have rid yourself of your negativity, will likely feel much better and will often have found a solution to your problems.

The same thing happens with rapidwriting. After writing a few pages and releasing all the bottled-up negativity, the solution will start to present itself clearly.

One of the author's friends, Nan, was worried about having to do a "desk audit" at work, which is when your boss visits your desk and listens as you talk about your work and justify its importance.

Naturally, this can be stressful, so Nan prepared by rapidwriting. She started out with the words, "I can't believe this is happening, after all my time here . . ." But by the end, she had three pages of examples that clearly showed how vital she was to the company and how her work contributed to its financial success.

So, now that we have tools for getting past our mental roadblocks, let's look at how we can stay focused.

### 6. No matter what life throws at you, you can reach your goals if you learn to be flexible. 

It might sound counterintuitive, but staying on top of your goals also means being flexible.

Think of reaching a goal as flying a plane to a destination. Statistics show that 90 percent of the time, an airplane will have to fly slightly off course in order to avoid bad weather or to get out of the way of another plane's flight path.

But these detours don't change the fact that pilots are continuously focused on getting to their destination. The goal remains the same, but in order to reach it safely, they have to be flexible.

This is the same philosophy you need to apply to achieve your goals, so that when life inevitably throws something unexpected your way, you can remain focused and stay on track.

For instance, let's say your goal is to find a new apartment. First of all, set your budget and write down precisely what you're looking for, such as two bedrooms, a garden and a location that's walking distance to a shopping district. In this case, being as precise as possible will allow you to quickly filter out all the apartments that don't have two bedrooms, a garden and nearby shops.

Now, let's say you find an apartment that meets all your requirements — but there's something you didn't expect: it's near a railroad track.

While the proximity to the train track is what gives the apartment such an attractive price, and the train only goes by twice a day, you might start ignoring all the good things and focus on this one negative. And even though you don't mind the train tracks, you might start imagining how other people will react.

This is exactly how you can get off track, lose sight of your goal and end up worrying about someone else's dream.

So don't get lost in hypothetical fears about what other people might think. You'll miss your opportunity to enjoy something that would have been perfect for _you_.

### 7. Starting off small and aligning your goals with a greater good are excellent motivators. 

It's nearly impossible to eat just one french fry. Even when they aren't that great, after you eat one you'll be tempted to eat another — and this is exactly how conquering your goals works as well.

One of the best motivators to help you achieve your goals is to successfully reach one of them, and then use the resulting psychological boost to your advantage.

The most obvious strategy is to start out with small goals and work your way up to the biggest one. So, if you want to swim across the English Channel, start with a series of smaller lakes that gradually get bigger, building your strength and confidence in the process.

The best part is, the goals don't have to be related at all. The momentum from completing one goal will still help you conquer another, even if it is completely unrelated.

Edna is another friend of the author, who saw someone riding a unicycle on television and decided she would teach herself how to ride one. Sure enough, she applied herself to this goal and was eventually unicycling to school. This accomplishment was a clear sign of what she could do when she became determined, and it motivated her to successfully accomplish more difficult tasks, such as learning French.

Another way to boost the likelihood of reaching a goal is to align your desires with a greater good, such that they're not purely about personal gain.

This doesn't have to mean changing your goals, but rather thinking of ways that meeting a personal goal can put you in a position to benefit your community or society as a whole. For example, say your goal is to publish a novel. Instead of making that the final goal, add to it by promising to donate a portion of your profits to your favorite charity.

By adding this sense of altruism, you're adding a big motivator, because now you're no longer the only one who will miss out on the rewards if you don't follow through with your plans.

### 8. Fire and water have strong primal powers that can be used to your advantage. 

Humans were using fire and water in their rituals long before pen and paper were invented, and these basic elements still have a primal power to influence us and help us succeed.

Water is an especially powerful source of creative power. Julian Jaynes was an influential psychologist who pointed out that we often make our most groundbreaking discoveries while we're in _bed_, in the _bath_ or on the _bus_.

Of these three Bs, the author is especially impressed with how powerful the bath can be. This is due in part to how many sacred religious stories, poems, myths and paintings depict the powerful forces of water.

It's no coincidence that it was while stepping into the bath that Archimedes had his famous "eureka!" moment that led to Archimedes' principle. When he entered the bath, he noticed that the water level rose, and suddenly realized that you could measure this rise to determine the precise volume of an object.

And just as water can cleanse the body, being in or around a body of water can also cleanse the mind, which is one of the best ways to promote creativity.

Fire, on the other hand, can be used in rituals to give power to your intention. Indeed, Native American tribes have been using fire rituals for this very purpose for generations. At the start of a new year, certain tribes build arrows from sticks and feathers, and each of these arrows is made to represent a specific hope for the year ahead. They then burn the arrows as a gesture of offering this hope to the universe.

This is a wonderful ritual because it helps them be clear and precise about their hopes, thereby helping these hopes become a reality.

You can do the same thing with paper by writing down three goals you hope to achieve and three unwanted things that you hope to get rid of. Then, find a safe place and set fire to the paper to give a powerful intention to your desires.

With all these tools at hand, it will soon be clear that you have what it takes to turn dreams into reality.

### 9. Final summary 

The key message in this book:

**No dream is too big for you to achieve, as long as you have the right attitude. Too often, our fears get in the way of what we hope to accomplish. But this can be remedied by writing down all that we desire and fear, thereby unlocking the power of the subconscious mind and putting us on the right path. The simple act of writing is a powerful one that is not unlike a prayer, and it can turn us into a magnet for the things we want most in our lives.**

Actionable advice:

**Start your day off by writing down your intent.**

Take some quiet time at the beginning of your day to write down what you'd like to accomplish in the hours ahead. This is not for practical stuff like doing the laundry or going shopping. It's to provide emotional and spiritual guidance for the day, such as "being the best mother I can be." Just writing your intent down will keep you feeling grateful and positive throughout the day.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Making Ideas Happen_** **by Scott Belsky**

_Making Ideas Happen_ deals with the obstacles that lie between your ideas and their implementation. It offers insight into the ways in which successful individuals and creative departments overcome these obstacles, by offering real-life examples from some of the world's leading brands and creative minds.
---

### Henriette Anne Klauser

Henriette Anne Klauser, PhD, is the president of Writing Resources, an organization that has offered popular and groundbreaking workshops for writers around the world. Klauser has taught writing at many academic institutions, including the University of Washington and California State University. Her other books include _Writing on Both Sides of the Brain_ and _Put Your Heart on Paper_.

