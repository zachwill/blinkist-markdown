---
id: 5550bb386332630007a60000
slug: write-your-business-plan-en
published_date: 2015-05-14T00:00:00.000+00:00
author: The Staff of Entrepreneur Media, Inc.
title: Write Your Business Plan
subtitle: Get Your Plan in Place and Your Business Off the Ground
main_color: FFF233
text_color: 9C941F
---

# Write Your Business Plan

_Get Your Plan in Place and Your Business Off the Ground_

**The Staff of Entrepreneur Media, Inc.**

In _Write Your Business Plan_ (2015), you'll get a comprehensive overview of how to start putting your ideas to words and getting your dream of starting a business off the ground. A well-crafted business plan is a blueprint for future success, so it's vital that you craft a plan that sells your ideas well.

---
### 1. What’s in it for me? Craft a business plan that will help establish and support your great idea. 

No matter whether you want to sell fusion tacos from a truck or develop the next killer app, if you are starting your own business, you need to write a business plan.

A thoughtful, well-crafted business plan is the critical first step that sets out why your business should exist in the first place. What exactly are you making and selling? Why do you think you'll succeed? What is the competition doing? And importantly, how much cash do you need to start?

And although it may seem a straightforward task, the process of crafting a great business plan has been confounding entrepreneurs and founders for generations.

Your business plan is so important, and you'll be shopping it around to so many people, that you have to get it right. These blinks will show you how.

In these blinks, you'll also learn

  * why no investor cares whether your accountant can sing arias;

  * how loose lips can potentially sink your business ship; and

  * why your business plan's cover is almost as important as what's inside.

### 2. Outline your goals, thinking about what you want to achieve and how you want to achieve it. 

So you've got a great idea for a business. That's a good start, but if you want to turn your idea into a successful business, you'll need a _business plan_.

A business plan acts as a blueprint, spelling out the important elements of your future company, like its purpose and its products, a marketing strategy and overall financial goals. A business plan is also an essential tool in raising funds and gaining support for your venture.

You need to clearly state your objectives and your financial situation, however, before you sit down to write your business plan.

Take some time to think about the future. What do you want to achieve? Identify clear goals so you can start working toward them.

Know that if you are unclear in your goals and your strategy to achieve them, your business plan won't be worth the paper you printed it on. You can't outline your idea for a booming business but then articulate a strategy of slow, stable growth — it just wouldn't make sense.

Your team benefits from a clear business plan too, as it explains clearly what they're all working toward. What's more, your employees will support your company more avidly when goals are set.

Financial matters too are a crucial part of any good business plan. How will you secure funding? And what sort of funding are you willing to accept?

A new business has a few options when it comes to funding, such as securing bank loans, looking for money from friends and family, or seeking venture capital. It's important that you determine which option is right for your business.

Venture capitalists, for instance, often insist on making management decisions. Sometimes VCs will even put one of their members on your board of directors, with an eye to protecting their investments. Think about it: how much control are you willing to give up?

You also have to consider your costs. If you raise money through a stock offer, for example, you might get a lot of cash but also you'll deal with high commissions and fees as well as complicated paperwork.

Take some time to think about the level of complexity you'd be comfortable with, before you dive in!

### 3. Consider crafting versions of your business plan to speak directly to a particular audience. 

You might think that one business plan is the same as the next, a bunch of statistics and financial forecasts.

The truth, however, is that while all business plans do have common elements (like an executive summary or product description) they differ quite a bit depending on their purpose.

Business plans differ because industries differ. Starting a retail business requires a different strategy than starting a management consultancy, for example.

Even businesses that are similar still require different business plans. Imagine two restaurants, one which is an upscale French establishment, the other a deli focusing on business clientele.

The French restaurant will spend a lot of time writing about its chefs, outlining their skills and experience. The deli, in contrast, will focus on how to maintain quality in a high-volume, high-speed business.

Another important tip is to tailor your business plan to the needs of your audience, or the different people who will read it. You may have to craft several versions to do so.

Bankers, for example, are more interested in financial issues than company culture. So make sure your cash-flow statements and balance sheets are professional and detailed. Bankers don't care if you've got a foosball table in your company's chill-out room!

Angel investors, in contrast, tend to care more about why you're starting your business and the emotions involved in your pitch and purpose. Your plan for this audience can be less formal yet more concise, focusing on a tight message that packs an emotional punch.

And if you're looking to attract employees, you might want to craft a version that includes details on stock options and compensation.

### 4. Sharing your ideas is also risky. Make sure your readers keep mum and your analysis is sound. 

Writing a business plan isn't like putting together a shopping list. There are risks involved, and you need to be aware of them.

Confidentiality issues are a key issue. When you write your business plan, you might divulge confidential material which could potentially harm you and your business down the line.

Imagine that in your business plan, you describe an innovative method for manufacturing vacuum cleaners. The people who read your plan could then disclose your secrets, even by accident. Your innovative ideas could be stolen and used by others.

Avoid this by having your readers sign a nondisclosure agreement, which is a contract outlining what your reader can and cannot say about what they've read. Make sure your audience contractually agrees to keep your plan and ideas safe.

Another common risk is a plan that could damage your business reputation. If you craft a plan that is too optimistic, and then you go on to not meet your projected goals, you might face a lack of trust in the future.

Let's say you want to start a microbrewery, and in your business plan you predict sales growth of 600 percent in your first year. An experienced investor will scoff at this, thinking you are either unprofessional or simply delusional. You certainly won't win investors this way.

_Analysis paralysis_ is another risk in crafting a business plan. This occurs when you spend so much time analyzing and planning your business that you don't actually get anything done.

Some people spend years in the planning stages, only to have a competitor swoop in and take control of the market in the meantime.

So after you've thought about your goals, your audience and your risks, it's time to start writing. But how do you start, exactly?

### 5. The first, crucial part of writing a great business plan is crafting an engaging executive summary. 

You're at your laptop and ready to write. What do the first few pages of your business plan look like?

First, you need a cover page that states your name and contact details.

The next page should contain your _executive summary_. These few paragraphs lay out the main points of your plan. It should give your readers the gist of your whole idea, in case they don't have time to read the plan in its entirety.

Essentially, if your business plan is a movie, the executive summary is the two-minute trailer. So keep your summary short, snappy and emotionally exciting — make it encourage your readers to read more.

Your executive summary shouldn't be longer than two pages, as most people who read your plan won't spent more than five minutes on it anyway. So use the summary to convey urgency!

As Jimmy Treybig from _Tandem Computers_ once said, an executive summary should make a reader think, "It's going to explode, and I'd better invest now or I'm going to miss out!"

You also need to be clear about financial issues from the start. As a business founder, you need cash, after all. If you're upfront about the interest rates and amount of loans you need, your potential investors will appreciate your honesty.

Also, tell your future investors how you intend to use their money, as no one will give you a dime if they don't know what you're going to do with it. So give figures, showing how much money you'll allocate to marketing, how much toward recruitment, and so on.

After you've outlined your main points and energized your readers with your executive summary, you can move on to the next section: explaining your industry.

### 6. Know your industry and your place in it; know your customers and why they’ll buy from you. 

Investment banker Peter Worrell once said, "It doesn't matter how hard you row. What matters is what boat you're in."

In other words, it doesn't matter how hard you work. If you aren't in the right industry at the right time, it will be challenging to grow your new business.

This is why in your business plan, you need to describe the state of your industry for potential investors. Show them that your market is expanding, and that you have the chance to take advantage of this growth.

In the early 1980s, for example, tech entrepreneurs often included the word "energy" in company names, as the energy industry was attractive to investors at the time. Today "mobile technology" or "3D printing" might have the same effect.

You also need to describe your competition as well as your company's unique position in your growing industry. Differentiate yourself and show that your company will have an advantage.

Let's say you're starting a 3D printing business. If one of your competitors prints in plastic and another in metal, you'll offer something unique if you print in organic material. Your readers need to know this, so highlight this fact prominently in your business plan.

Identifying trends in your industry is also crucial. Use trustworthy market research to show the potential of your business idea.

Describing your potential customer base too is important. How old are they and what do they do for a living? How much money do they make, and what is their education level?

You should know the purchasing habits of your potential customers. Are they impulsive buyers or repeat customers? What other companies do they purchase from?

Finally, explain why your customers will want to buy specifically from you. Make it clear that your product is suited to your potential customers' needs and lifestyles.

Mothers looking to buy prepared baby food, for example, are often motivated to find products that are not only nutritious but also safe. So in your plan, explain that your baby food is doctor-approved.

### 7. Put faces to your business idea by describing the skills of your management team in detail. 

Have you ever heard an investor say, "I invest in people, not in ideas?"

While your products and suppliers are important, don't forget that your people are your company's most valuable asset. Your management team brings your ideas to life.

Who would you trust more, a person with a great idea and great credentials or a person with a great idea and lousy credentials? Your investors need to see that you and your staff are qualified.

So don't just write about your team's education levels and employment experiences. Outline their skills, too. List only the skills that are applicable to your business, however. While the fact that your accountant can sing opera is interesting, it's not terribly relevant to your business.

You should also list your accomplishments, such as patents or awards. If you've supervised the opening of 37 stores in three years, for instance, include details like this in your plan. If your sales manager has increased sales on average 25 percent year on year, tout this fact prominently.

Clearly outline the job descriptions and roles of each of your team members, to help prevent future misunderstandings. A start-up, for example, might not be able to fill every position at the outset. A CEO might be in charge of marketing as well, at least in the early days.

And if you're writing an abbreviated plan, just include a short, catchy line about each important team member. A blurb such as, "Highly skilled backend software developer with eight years of experience," gives a potential investor all the information she needs to know.

So now you've got your plan on paper. Let's look at a few ways to make your work even better.

### 8. A business plan’s appendix holds the extra material: photos, resumes, store blueprints. 

While you're writing, you might struggle in deciding which information to include or leave out of your business plan.

It's vital that your plan be detailed, but remember too that it needs to be readable.

You should include documents such as advertising samples and team member resumes — important information, granted, but perhaps not relevant to all of your readers. Thus you can stick information like this in your business plan's _appendix_.

Too many documents can break up the flow of your plan. Remember that you're trying to tell a clear story in just a few pages. If your plan includes too many details or distractions, it'll put people off.

The appendix is there for readers who want to know more but don't mind flipping to the back of a plan or even going online. In contrast, readers who only have a few minutes to skim through your plan don't need to bother with it.

It's a good idea to keep your appendix materials either on a different website or a secure webpage of your own. Some people might be annoyed opening heavy attachments or flipping through lengthy hardcopies.

Your appendix should also include several images, such as photos of your team and products.

Depending on your business, it could also be advantageous to include advertising samples, factory layouts or store plans. Such files are usually too large for emails, so keep these on a website that you can then share with readers.

Have you ever opened an attachment that took ten minutes to download? Lots of people (particularly potential investors) might not have the time to spare. Keep file size in mind when you craft your business plan, keeping digital copies lightweight and easily downloadable!

### 9. Make sure your business plan looks professional, with a well-designed and accurate cover page. 

Even if your idea is genius and your words sublime, if your report looks like it was put together by a first-grader, your readers will wonder whether you're really a worthwhile investment.

Make sure your business plan looks professional and is well-designed. Your cover page is the first thing a reader will see, so be sure it's attractive and all information is accurate and correctly spelled. If your cover is awful, your audience might not get past the first page!

If you have a compelling logo, put it on your cover page. Do you use Twitter or Instagram? Include all your contact details, such as email addresses, phone numbers and social media handles.

Don't clutter your cover, however. Only include key information and make sure all the elements are balanced and attractive so your cover grabs your reader's attention and invites them to read.

It's also a good idea to keep some hard copies of your business plan. Professionally printed hard copies are useful if a potential investor requests one; you can send one off immediately, if needed.

Be sure to use high-quality white paper for printing, avoiding thinner paper, as it wrinkles or tears too easily. You should also bind your business plan, so it's easier to hold, read and pass around.

Place a transparent cover sheet over your cover page, too. This will prevent it from getting dirty or damaged.

Presentation is really important. Considering how much work you put into the contents of your plan, to fall short on presentation would be a shame. Make a good impression and be professional!

### 10. Final summary 

The key message in this book:

**Even the most brilliant business dream won't become a reality without a well-conceived, strategic business plan. Your business plan gives you both a route to success and a way of attracting investment. So outline your goals, analyze your industry, write an engaging executive summary and assemble a stellar management team. Write a plan that'll attract the kind of investment you need!**

Actionable advice:

**Keep your business plan online.**

Don't waste people's time with attachments, and remember that most readers don't want to wrestle with hard copies. Keeping your business plan and appendix online allows your readers to be better able to access your information, and importantly, have a positive first experience with your future company.

**Suggested** **further** **reading:** ** _Running Lean_** **by Ash Maurya**

_Running Lean_ presents you with a fail-safe strategy to bringing your new product successfully into the market. By promoting a methodology of clever testing and planning ahead, the book gives you a step-by-step guide in building a business model that works while saving time, money and effort.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### The Staff of Entrepreneur Media, Inc.

This book was written by the staff of Entrepreneur Media, Inc., a media company that publishes _Entrepreneur_ magazine in addition to a series of business books.

