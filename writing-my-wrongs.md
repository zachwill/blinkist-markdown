---
id: 58f49c2e1507cf00046b9938
slug: writing-my-wrongs-en
published_date: 2017-04-20T00:00:00.000+00:00
author: Shaka Senghor
title: Writing My Wrongs
subtitle: Life, Death, and Redemption in an American Prison
main_color: FFF233
text_color: 9C941F
---

# Writing My Wrongs

_Life, Death, and Redemption in an American Prison_

**Shaka Senghor**

_Writing My Wrongs_ (2013) tells the story of a man growing up during Detroit's 1980s crack epidemic. These blinks take you on a journey from his happy childhood to a life of drugs, gangs, murder and a 19-year prison sentence — and how he came to find hope and redemption through writing.

---
### 1. What’s in it for me? A remarkable story of drugs, violence, prison and redemption. 

Do you remember your teenage years? For many of us, this was when we experienced our first taste of independence from our parents. They were exciting times when we could experiment with drinking, smoking and dating for the first time. But they were also times of emotional turmoil when we struggled to find boundaries and came into conflict with our parents — and maybe even the law.

Most of us make our way through these tricky years largely unscathed. However, others, including the author, Shaka Senghor, go off the rails. This is the story of how, at the age of 14, Senghor entered a world of drugs and violence at the height of the 1980s crack epidemic in Detroit.

Despite growing up in a safe home with a loving family, he slipped into highly destructive habits, selling drugs, committing murder and eventually finding himself in prison. These blinks will take you through Senghor's lifelong journey, showing you what made him go astray and how he found redemption.

You'll also learn

  * about the dark world of Detroit's 1980s crack epidemic;

  * why calling 911 in 1980s Detroit was a waste of time; and

  * why prison life is so dangerous.

### 2. Shaka Senghor’s childhood took an unexpected turn when his parents’ divorce scarred him emotionally. 

Our lives can often take unpredictable twists and turns, leaving us in places far from where we thought we'd end up, or situations we'd never dreamed of. The author, Shaka Senghor, experienced one such twist at a young age, and it had a telling influence on the rest of his life.

As a child in 1980s Detroit, Shaka grew up in a warm, loving environment with a big family. Shaka's parents would often host family get-togethers, which would come alive with singing, eating and dancing.

During the holidays, the family would come together to hang Christmas decorations, and Shaka recalls with particular fondness how his father would give him and his three sisters money to go to the local ice skating rink.

Shaka had a promising future ahead of him. He remembers how, when his mother asked him what he wanted to be when he grew up, he'd proudly say that he wanted to become a doctor. This way, Shaka said, he could give lollipops to kids getting their shots, and help sick people to get better.

But things took an unexpected turn for the worse when his parents' marriage ran into trouble. Shaka's parents first split up when he was eleven, and it devastated him. The close-knit family life he'd grown to depend on had been taken away from him.

After a year of separation, Shaka's parents got back together again, much to his delight; unfortunately, this only lasted for a few months. Shaka's parents separated again, and this time, Shaka was told that he had to move to another part of Detroit with his father. Feeling confused and rejected, Shaka blamed the situation on himself. What had he done to make his mother want to get rid of him?

### 3. Trying to find his own way, Shaka fell in with the wrong crowd, leading him into a world of drugs and violence. 

Did you ever think about running away as a kid? Well, Shaka was one of the few who ended up actually doing it. Shaka turned 14 in 1986, at the high point of Detroit's crack epidemic. He became preoccupied with smoking cigarettes, dating girls and staying out late.

Shaka's mother was dismayed. During his weekend visits to her home, she had no idea how to manage his rebellious behavior other than by physically punishing him, or reminding him that he was free to pack up and walk right out the door. It wasn't long before Shaka did just that.

With no income of his own, Shaka went to live with friends, asking them to give him food and shelter. He began living in the squalor of a friend's basement, knowing that if he wanted to live somewhere decent, he'd need a job.

An offer for work came along when Shaka was introduced to a man named Miko. Miko was looking for someone to "roll" with him — that is, sell drugs for him. He proposed a weekly salary of $350 to Shaka, plus an extra daily allowance of $10 for food. In return, Shaka would have to hang out at the spot where Miko's customers would find him, day and night, seven days a week.

On the drug-dealing ladder, this was the lowest job you could get. But the money was good and Shaka was desperate, so he took it. Miko started him out with a bag containing one hundred small crack rocks, also known as "nickels." Shaka's job was to sell these for $5 each. At just 14 years old, Shaka was even equipped with a loaded shotgun to make sure customers didn't try to jump him or rip him off.

### 4. Shaka soon discovered the social validation that came with dealing, as well as the grim lives of the addicted. 

Most 14-year-olds have different priorities than adults; when you're young, it's easy to get carried away by your desire for social acceptance and money. This was especially true for Shaka, and without parental guidance, there was nothing holding him back.

Shaka soon became enthralled by the money he was earning. Armed with his new weekly salary, he began to frequent local malls, shopping for the freshest clothes and sneakers. Between the Jordans, Filas and Ballys, Shaka spent more money building his wardrobe in a short time than most adults do in years.

Along with Shaka's new lifestyle came newfound attention from his peers. Girls flocked to him, which made his male friends respect him more, making this newfound lifestyle increasingly intoxicating.

But this didn't mean Shaka was blind to darker aspects of the drug world. As a dealer, he came to meet many seemingly normal people whose lives had been torn apart by drug addiction.

For instance, one man named John was the embodiment of a typical middle-class life before his addiction to crack led him to lose his career and family. Shaka and his crew spent time in John's former family home, which he let them use for business. Here, Shaka was surrounded by vestiges of John's old life, like pictures of his wife and kids.

Shaka soon became aware of a creeping feeling that money couldn't buy everything. At the end of the day, the stacks of cash in his pocket couldn't buy him what he really needed: to be loved and accepted. As Shaka spent more time associating with his high-strung, drug-addicted customers, he grew increasingly lonely — though he'd never have admitted it to himself.

### 5. Shaka continued down a dark path, and things came to a head when he was convicted of murder. 

When you think of drug addicts, who do you think of? Loving mothers? Schoolteachers? Established professionals? As Shaka would soon learn through experience, the crack epidemic that gripped Detroit in the 1980s affected all sectors of society and all areas of the community.

The drug world brought Shaka in direct contact with despair and suffering. On one occasion, Shaka was visited by a woman known as the "head doctor." With unwashed clothes, tangled hair and red eyes, she displayed all the obvious signs of crack addiction. People called her the "head doctor" because she offered to exchange oral sex for drugs with anyone who had them, even if they were just a kid, like Shaka.

Experiences like these took a toll on Shaka's sense of morality and compassion. Using crack himself, it wasn't long before Shaka found himself in dangerous situations. At just 15, Shaka attempted suicide through overdose.

He survived, only to be shot in the leg multiple times by a rival dealer when he was 18. Although his desperate friends called for an ambulance, no help arrived. Situations like these were all too common at the height of Detroit's crack epidemic since emergency services considered Shaka's neighborhood too dangerous to visit. This experience left Shaka feeling tense and vulnerable, and to cope with this, he began carrying a gun at all times.

Shaka's downward spiral hit bottom when another assault happened — but this time, it was Shaka doing the shooting. Aged 19, Shaka had an altercation with two of his male customers. Having become paranoid and nervous, he suspected the men to be undercover police officers, and when the tension rose, Shaka grabbed his gun and fatally shot one of the men.

This shooting would change his life permanently, as he wound up with a 19-year prison sentence.

> _"The young Black male has perfected the art of being the best at being the worst."_ — Noted black psychologist Amos Wilson

### 6. In prison, Shaka got no respite from the violence that had plagued his life on the streets. 

American prisons are very dangerous places. Theft and violent crime are commonplace, and guards can do little to protect inmates from each other.

Take the story of a prisoner on Shaka's block named Seven. Once, Seven gave away some of his breakfast to a newbie inmate. Later, Seven asked the newbie how he was going to repay him for the food. The newbie inmate, confused, said that he thought Seven had just not been hungry.

Suddenly, Seven grabbed the inmate by his throat, choking him until he had nearly passed out. Seven then raped the inmate in full view of guards and fellow inmates; shockingly, nothing was done to stop him. Shaka witnessed all this before even making it to prison — it occurred while he was awaiting sentencing at Wayne County Jail.

In the sentencing, Shaka was convicted to 17 to 40 years in prison for the murder he committed. One of the first prisons he found himself in was called the Michigan Reformatory.

Do you remember reading about the gladiators of the Roman Colosseum who would fight to the death for entertainment? Well, this prison was known by inmates as the "Gladiator School."

Shaka quickly found out that, in prison, those who show weakness are preyed upon. On Shaka's first day at the Michigan Reformatory, he met another newbie inmate named Kevin. Kevin seemed like a nice guy to Shaka, but when seasoned inmates scoured the ranks for newbies to pick on, they picked Kevin out as a weak link.

Shaka later saw some inmates take Kevin away, and he could only assume that the inmates were going to rape him. Shaka would learn later on that Kevin had taken his own life.

### 7. For years, Shaka rebelled against the system – but through reading and writing, Shaka’s perspective on life was transformed. 

When you think of the best places to get an education, prison likely wouldn't be one of the first to come to mind — but this was indeed the case for Shaka. With all the time he suddenly had on his hands, Shaka began to borrow novels from the prison library.

Before long, Shaka was led to discover the works of authors such as Malcolm X. These authors discussed issues such as the history of African people in America, and their books helped Shaka make sense of issues of race in America. He could now understand better why prisons were disproportionately filled with black people.

But even though he was learning so much, Shaka held onto a "kill or be killed" prison mentality. He found himself in frequent altercations with guards and other inmates, and this often meant physically harming fellow black inmates so that he wouldn't appear weak.

Shaka sensed the contradiction in studying the oppression of his people while simultaneously harming other black inmates, and it left him confused and frustrated. Eventually, he was punished with seven years of solitary confinement for his violent behavior.

During these seven years, Shaka started to keep a journal, and it was in the pages of this journal that he truly began to reflect on the events in his life that had led him to that point. For the first time, Shaka was able to come to terms with his actions.

This was a transformative experience for Shaka. He started to get involved in community activities in prison. For his fellow black inmates, Shaka organized events for Black History Month and Kwanzaa, while he also mentored younger inmates and fostered literacy, exploration and self-reflection.

Shaka's community activity led him to discover an organization named Helping Our Prisoners Elevate (HOPE). Through HOPE, Shaka met Ebony, a woman who worked for the organization. During their developing relationship, Ebony offered Shaka the crucial support needed to get ready for his release from prison. Finally, at the age of 38, Shaka was released from prison on the 22nd of June, 2010.

> _"One hundred percent dissatisfaction brings about one hundred percent change."_ — Honorable Elijah Muhammad

### 8. Final summary 

The key message in this book:

**The world of crime, drugs and violence is an all-too-common reality for many people, particularly those who have been systematically oppressed by our current society. It is nevertheless possible for individuals with troubled pasts to be rehabilitated, and find hope and meaning through reading, writing and community involvement.**

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Just Mercy_** **by Bryan Stevenson**

_Just Mercy_ (2014) is a walk through the American criminal justice system of the 1980s. These blinks explain how a system that is supposed to safeguard the rights of the nation's citizens became an unjust tool to mistreat and abuse the most vulnerable members of society through mass incarceration and excessive sentencing.
---

### Shaka Senghor

Shaka Senghor is a mentor, writer and TED speaker who spent 19 years in prison for murder. He currently works as the Director of Strategy and Innovation with #cut50, an initiative to reduce the US prison population by half by the year 2025.

