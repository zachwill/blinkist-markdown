---
id: 5a74fe7eb238e100070eb3d5
slug: writing-that-works-en
published_date: 2018-02-06T00:00:00.000+00:00
author: Kenneth Roman and Joel Raphaelson
title: Writing That Works
subtitle: How To Communicate Effectively In Business
main_color: CC2929
text_color: CC2929
---

# Writing That Works

_How To Communicate Effectively In Business_

**Kenneth Roman and Joel Raphaelson**

_Writing That Works_ (1981) is the definitive guide to business writing. These blinks are full of advice on how to write clear, compelling and succinct business communications, covering everything from quarterly reports to presentations, emails and even resumes.

---
### 1. What’s in it for me? Write your way to the top of the market. 

Success in the business world requires a wide range of skills and the ability to adapt to different situations — but there's one skill that's central to all manner of business tasks: writing. While some people might be good at writing casual personal emails, chances are they don't have the slightest idea about how to draft a compelling marketing pitch.

If you're going to succeed in the world of business, you'll need to have a solid grasp of how to write for business in every possible context — and there's no better way to learn these skills than from two experts in the world of American business marketing. With a wealth of experience under their belts, the authors will walk you through all the forms your business writing might take.

In these blinks, you'll learn

  * the best way to write a resume;

  * the key to effective presentations and speeches; and

  * why junk mail might not be so bad after all.

### 2. The foundations of effective business writing are simplicity and accuracy. 

When it comes to writing, it's easy to let your prose get overly complicated. But this will make you lose your reader right off the bat. A better approach — and an easy one to implement — is to focus on simplicity and a natural feel.

Short paragraphs, sentences and words are the name of the game, but that doesn't mean sacrificing meaning.

For instance, the _Wall Street Journal_ is famous for its readability, and it never publishes an opening paragraph that exceeds three sentences. So, be like the _Wall Street Journal_ ; keep your paragraphs short and replace longer words, like _utilize_, with shorter ones, like _use_.

A natural feel is also essential, which is why you should write like you speak and avoid potentially confusing terms. For example, rather than writing, "the arguments are fivefold," write "there are five arguments."

Natural writing means avoiding jargon and technical language. In fact, the more specialized your language, the greater your chances of creating a misunderstanding. According to Harvard University paleontologist and author, Dr. Gould, young scholars only use convoluted, confusing language in the first place because they're scared of not being taken seriously.

That being said, if the reader and writer share a common technical language, some jargon may be appropriate.

A good rule of thumb for keeping things concise and natural is to replace or remove unnecessary words as you go. As an example, instead of writing, "at this current point in time," you could just write "now."

Doing all of the above will dramatically improve your writing. The next step is to make it as specific and accurate as possible.

This means avoiding generalizations that require readers to believe you, like "our program drew more new students than ever before." Instead, use specific claims that eliminate all uncertainty, like "student enrollment tripled to 210."

Specifics also decrease exaggeration, which will help you maintain the trust of readers. To stay specific and add energy to your writing, use an active, personal voice whenever possible. For instance, you should write "we recommend," as opposed to "it is recommended."

### 3. Write excellent letters, for both email and snail mail, with a few simple tricks. 

Most people's inboxes are overloaded, which is why everyone appreciates succinct emails. So, how can you write emails that grab your reader's attention from the first word?

It all starts with your subject line, which is your only chance to set yourself apart from the rest of the inbox. Regardless of whether your approach here is formal or casual, it has to get the person's attention, as in the subject line, "Are your customers happy?"

Next comes your tone, which is another key aspect because the tone of digital communications can easily be misinterpreted. The subject that starts an email and the signoff that ends it are both great places to set a positive tone for the whole message. As an example, your subject could read, "Thanks so much, everyone!" and your signoff could be "Cheers."

The third trick is to get to the point quickly and write only what's relevant. To achieve this, you should try to cut half of what you initially write.

Finally, etiquette is key. If you want someone to take action, don't list more than one recipient, as doing so will blur who's responsible.

Of course, some situations call for a more personal touch, which is where paper letters come in. To really nail them, keep a few things in mind:

First, your salutation should always be handwritten, and second, you should begin with a direct and succinct first sentence that acts like a title to an article. For example, you could write, "James Brown has informed us of issues concerning the HR department."

If you're asking for something specific, you should start with what you want, explain why you want it and end by expressing your appreciation. In your last paragraph, you should explicitly state what action should be taken. So, rather than ending with, "looking forward to hearing from you," end with "please notify us of your decision by June 8th."

The final step is the signoff, which should remain conventional and match your tone. Good formal options are "sincerely" and "cordially," while "regards" and "best wishes" are better in a more casual context.

> HBO's email motto is "Keep it short and sweet."

### 4. Convey your point through well-structured and focused presentations and speeches. 

Can you name one reason why people should listen to you?

It's a harsh question, but one you should be able to answer when putting together a presentation or speech. If you can't, don't sweat it; you can ensure that your message is worth listening to by organizing it for clarity.

First, lay out a structure that includes a simple, memorable theme, like "triple your revenue." This theme should be visible on every slide, keeping your audience focused.

From there, you can grab your audience's attention by framing your message in a way that demonstrates value. For instance, when talking to businesspeople about advertising, you could lead with a study on your audience's market.

However, this approach should be distinguished from the popular practice of giving slides a _label_, like "pricing trends." Instead, all graphs and charts you present should have a _headline_ that fosters understanding. A good example is "price competition is increasing."

That being said, having a clear presentation won't matter if you can't engage your audience. To do so, ask lots of questions and, before resolving them, have the audience guess the answers. You can also keep people on their toes by incorporating unexpected content, like audio recordings of customer testimonials.

Finally, it's important to finish on a memorable note that brings your message full circle. This could be a powerful image or, for big presentations, a small gift that symbolizes your message.

If you're giving a speech and not just a presentation, there are a few other things to remember. First, the speech itself should have a captivating title that builds anticipation. For instance, "The Tree That Grows to the Sky (on Wall Street)."

When beginning the speech, you should engage with both the topic and audience, but immediately after, move to the single most important point. Don't waste any time on tangents.

And remember, the more natural you can appear, the better. To ensure you come off as such, read your draft out loud. Edit it until it sounds like ordinary speech and you only need to glance at it to stay on track.

### 5. Drive people to action with clear plans and reports. 

Love them or hate them, plans and reports are essential to business communication.

For the most part, they're important because of the action they compel. However, when a plan or report is written in an unconvincing manner, that action is rarely achieved. So, how can you craft a plan that moves people to act?

The key is a pyramid-like argument that successively builds toward a recommendation. At the top of the pyramid is the overall purpose statement. One example is, "to acquire a growing franchise." From there, the purpose is broken into a number of packages, each with its own individual goal. For the example above, one of these subgoals might be, "outpace the competition."

One step down from there, those packages are supported by facts, like "competition has stagnated" and "the market is growing." When presenting such statements, always put them in the context of your goal and never as isolated points. In short, they should be relevant and support your argument.

And finally, your plan needs to have clear recommendations for next steps. Just consider the franchise plan described above; one recommendation drawn from this plan could be to "research six potentially profitable locations to build new stores." This works because the next action, "research six locations" and the reason, "to build new stores," are both crystal clear.

When it comes to reports, the approach is slightly different — and the structure can be as well. Specifically, the purpose of a report should be laid out in a clear, interesting fashion, right from the beginning. You could say, for instance, "this report analyzes the potential profitability for a franchise store in downtown Tokyo."

The structure that follows this initial sentence can be flexible; it might begin with recommendations, followed by supporting evidence, or proceed in the opposite order. As long as the structure is logical, you have a lot of license here.

Just remember, graphs and tables should be saved for the end, included in an appendix.

> _"Making things happen depends on clear communication."_

### 6. Speak to your reader’s desires and concerns in order to sell ideas and get funding. 

No matter what your industry, at some point, you'll have to use writing to sell your ideas. Your recommendations will need to be persuasive. But how?

Well, a good tip to start with is to cut to the chase; state what you want and offer your reasoning after the fact.

It's as simple as introducing the topic and the nature of your recommendation in the first paragraph; doing so will give the reader a sense of where you're headed.

Next, you should demonstrate your competence with background information. For instance, a consulting firm once convinced the board of the New York Botanical Garden to adopt a new environmental program by showcasing its past work. The firm outlined its prior visits to other botanical gardens and offered an astute financial analysis of the costs involved in the proposed project.

You should also be sure to highlight the benefits for the reader. You can do so by incorporating the benefits into a list of goals that correspond to the recommended action. As an example, you might lay out the goal of "achieving a suitable return on investment."

If you follow the above steps, you should be able to convince people of your recommendation. But if you're asking for funding, your approach will need to be a bit more specific.

In the first two paragraphs, you need to describe exactly how much money you're asking for and how it will be spent, thereby demonstrating that you meet the funder's criteria. From there, you can proceed with a logically structured argument, in which each point is backed up by evidence and transitions seamlessly into the next.

Build a sense of urgency and illustrate why the benefits of the project will be lost without funding.

Last, you'll want to grab your reader's attention. An effective option here is to employ a similar strategy as when adding a flourish to a presentation. For instance, you could open with a compelling description of a person whose life would be completely changed by the funding.

### 7. Make snail mail work for you. 

Old-fashioned, paper junk mail may drive you nuts, but the truth is, direct-mail marketing works.

Let's explore how to make it work for you.

The golden rule is to begin with a small sample of customers and avoid making assumptions about what works. Remember, factors like increasing the price of your product or service or which day of the week the recipient receives your mail can dramatically change the success of a mail marketing campaign.

Here are a few tips to keep in mind when composing the marketing materials:

First, write your offer in bold text, on the outside of the envelope, hinting that the letter contains something valuable. Then, follow through with this hint by providing something special and including an interesting offer. This is a great place for a promotion, like a free trial or discount.

Beyond that, your direct mail advertising will likely be read while the recipient hovers over a recycling bin, which makes it vital to lead with an interesting tidbit of information.

American Express offers a great example. The credit card company began its direct mail advertisements with the statement, "quite frankly, the American Express card is not for everyone."

Finally, and perhaps unexpectedly, the P.S. section of a letter is often the most read. This makes it a great place to hook your reader with the mention of a limited-time offer that compels him to act immediately.

But what if you're using direct mail for fundraising?

Consider these tips:

For the letter itself, show passion and appeal to the reader's emotions. A good example is the outside of a pro-gun control letter that read, "ENCLOSED: Your first chance to tell the National Rifle Association to go to hell!" The letter then began with the line, "Dear Potential Handgun Victim. . . "

Second, securing new donors is always worth the effort, since most people who donate once will donate again, which is why maintaining relationships is so important. You can keep your business relationships healthy by sending project updates and small tokens of appreciation, like pins.

When finally making the ask, you should suggest a fixed amount or range for the donation upfront. And lastly, keep in mind that donating is personal; your appeal should always be framed as if it's tailored to your reader.

### 8. Land your dream job with a well-crafted resume and cover letter. 

There are few pieces of writing that carry as much potential influence as a job application. How can you make yours as powerful as possible?

To start, write a summary in bold font. This should integrate all you have to offer, whether it's skills, experience or results. For instance, you might write, "12 years of sales management experience, proven ability in leading sales teams."

The next step is to draft a chronological list of your previous jobs, including dates, locations and the scope of your prior positions. Emphasize the specific results you produced and accomplishments you attained. Skip anything that's outdated, irrelevant or insignificant, such as your travel experience, your age or a photo.

Once you've finished the resume part of the application, you're ready to pen a killer cover letter.

First off, address your letter to a specific person by name. Write "Dear Ms. Jones" instead of "Dear Creative Director." Next, lead with a direct statement about why you're writing, avoiding any roundabout introductions. For instance, you could write, "I am writing to submit my candidacy for the research analyst position."

After introducing the topic, cover your primary qualifications. Only write things you would feel comfortable saying to the employer face-to-face.

It also pays to avoid flattery. As with all good writing, get to the point and keep it concise; you should be able to communicate everything you need to in about half a page.

Once you have done so, end with your next step, providing the days and times that you're free to speak and a time when you'll call to follow up.

And yes, you really should follow up; it'll make you stand out from the pack and it takes no effort at all!

After speaking to an employer, send her a letter that goes one step beyond a simple thank-you; write something meaningful, relevant or relatable. You might comment on something the prospective employer mentioned that's relevant to the time you spent at a tech start-up.

### 9. Edit and format your final product for an inviting reading experience. 

You now know how to write your way through all kinds of situations. But before you send out your final draft, you'll also want to know the basics of editing and formatting. These final touches are indispensable, and during your first revision you should cut out anything you think is not essential.

If you need a little nudge, just consider the famous writer, Mark Twain. He was a firm believer that cutting words adds energy to writing and, because of this, he believed in cutting one in every three words.

Once you've trimmed the fat, you should consider if the order of your content is correct; if not, cut and paste as required.

From there, you're ready to do a final fact check and consider the strength of your argument. Remember, a single statistical error can cause serious damage to your credibility.

If you've been working on a piece for too long, your mistakes might start to fade into the background. To catch them, give yourself enough time between drafts, and have another person review your writing. Both of these steps will help reveal mistakes that previously went unnoticed.

Following all of the above will have your final product edited to perfection. The last step is to format it for a smooth and appealing reading experience.

Start with a centered, all-caps header to grab your reader's focus. Then, break your text into short paragraphs, which will appear neater and more manageable. To create even more room in your piece, separate paragraphs with line spaces, rather than indents.

When it comes to adding emphasis, italics are a better choice than underlining or colored text, and you should save capitalization for words that demand some serious oomph.

Finally, to keep your reader following your train of thought, guide her with a few formatting tools. One is to use bullets, numbering or lettering to break things down, while another option is to introduce subheadings. Or, if you really want to be effective, use numbered subheadings, written in bold and with ample space above and below them.

### 10. Final summary 

The key message in this book:

**Business writing is about clarity, brevity and attention-grabbing rhetoric. Regardless of whether you're writing a work email or marketing copy for an advertising campaign, your goals should be to communicate your point succinctly, take your reader's perspective into consideration and adhere to a tight structure.**

Actionable advice:

**Handle gendered pronouns appropriately.**

In English, we sometimes use the collective pronoun, "he" as a default to refer to both men and women. When used all the time, this results in problematic, old-fashioned sentences which exclude women like, "every writer hopes that _he_ will leave a legacy." That said, replacing the "he" with "he or she" can be awkward, and using the plural "they" is grammatically incorrect.

So, what's a gender-enlightened writer to do?

In most cases, you can simply change the subject from the singular to the plural as in, "_all_ writers hope that _they_ will leave a legacy." If that doesn't work, be inclusive by alternating between "he" and "she" throughout your document.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _Pitch Perfect_** **by Bill McGowan and Alisa Bowman**

_Pitch_ _Perfect_ presents advice and principles that can help anyone to communicate more effectively. It teaches you how to present an argument or story with confidence, in a memorable way, and how to make your points with more precision. The author introduces seven principles that will help you to use exactly the right tone in both your professional and private life.
---

### Kenneth Roman and Joel Raphaelson

Kenneth Roman and Joel Raphaelson are former executives of the advertising and PR agency Ogilvy & Mather. Roman served as the company's CEO, while Raphaelson was Executive Creative Director.

