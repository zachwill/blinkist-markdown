---
id: 55914c9030383300070a0000
slug: yes-and-en
published_date: 2015-06-29T00:00:00.000+00:00
author: Kelly Leonard & Tom Yorton
title: Yes, And
subtitle: How Improvisation Reverses "No, But" Thinking and Improves Creativity and Collaboration
main_color: E7C32E
text_color: 826E1A
---

# Yes, And

_How Improvisation Reverses "No, But" Thinking and Improves Creativity and Collaboration_

**Kelly Leonard & Tom Yorton**

_Yes_, _And_ (2015) shows you how by incorporating the techniques of improvisational comedy to the business world, you can generate better ideas and foster more effective communication, with the ultimate goal of building a team ready to meet any challenge. The authors draw on personal experience from working with leading talents such as Tina Fey, Stephen Colbert and Amy Poehler.

---
### 1. What’s in it for me? Learn helpful business tips from some of the world’s most talented comedians. 

If you're looking for business advice, where do you turn? Choosing to pick up the latest tome from a business leader or academic makes sense. But what if you want to really shake things up?

How about exploring what some of today's leading comedians have to say about thinking outside the box?

It might not seem obvious, but the skills and techniques of improvisational comedy can be readily applied to the business world. On stage without a script, a comedian has to be innovative at a moment's notice and to think on her feet to solve urgent problems. Sound like the daily challenges of a business leader? You bet.

Based on the experiences of the team at The Second City, the ensemble behind today's top comedic talents, these blinks will give you unique insight into how improv skills can help you succeed.

In these blinks, you'll discover

  * whether laughter really is the best medicine;

  * why you need to grow an ensemble and not a team; and

  * why you should answer every question with "yes, and…"

### 2. A strategy used to keep comedians loquacious on stage can help generate business ideas, too. 

Have you ever seen an improv comedy show? Seemingly on the spot, actors share hilariously creative and detailed stories. It's not magic, however; comedians all use a particular, essential tool that gives them a platform to explore a multitude of ideas.

This strategy is called the "yes, and" mind-set. But how exactly does it work?

When an actor uses "yes, and," she is agreeing to follow through on or add to _any_ narrative started by a collaborator on stage.

Here's an example. Actor A opens by looking skyward and saying, "What a beautiful night. I can see all the stars." If Actor B ignored the "yes, and" strategy, she might say, "What are you talking about? It's daytime." This deviation from the starting theme could kill the conversation.

But if Actor B was in tune and playing by "yes, and," she'd further the narrative by saying, "Yes, and the earth looks so small. It's great we finally took this trip to the moon."

The genius of "yes, and" is that this strategy opens up unlimited possibilities, by giving every idea the chance to show its potential. And this technique applies to much more than just improv.

You don't have to look hard to see applications of "yes, and" in the business world, for example.

The online collaborative encyclopedia Wikipedia is a prime example of what can be accomplished when a large group embraces the "yes, and" attitude.

On the site, a user starts an article and others build upon it. In this way, the very nature of the site requires that users add to the work of others.

While there are drawbacks of such a collaborative approach (such as inaccuracies), this free, universally used website would've never been possible without the creative synergy of "yes, and."

### 3. To improve collaboration and foster creativity, don’t build a hierarchical team but an ensemble. 

Many of us take for granted that a strong business requires strong teams. Yet the idea of a team implies that its members are competing with an outside force, usually another team.

This mentality, of team against team, damages the open, free-thinking potential of "yes, and." Seeing business as a battle can hinder, not encourage, a group's creativity.

Instead, build _ensembles_. An ensemble is a group without a hierarchy, free from competition and thus capable of nurturing unlimited creativity.

In a team, roles and positions tend to be clearly defined and members learn early to stick to them. In an ensemble, everyone can chip in whenever and wherever they want, creating something that's greater than the sum of its parts.

Two of the most inspirational modern choreographers, Alvin Ailey and Twyla Tharp, created legendary routines by letting dancers add their own ideas, rather than making them just follow instructions.

So how do you build _your_ ensemble? Don't just search for the _best_ performers, but rather look for people who are the _best fit_ for your group.

An ensemble is only as strong as its ability to compensate for its weakest link. It's therefore crucial to choose members who can offer what an ensemble needs.

For example, in the 1970s and 1980s, The Second City improv comedy ensemble was composed almost entirely of white men. As a result, the ensemble's creativity stagnated, given the relatively limited breadth of experience of its members.

Instead of trying to solve its problems with star performers, however, Second City hired instead people of different races and sexual orientations. This diversity stimulated creativity, allowing the ensemble to not only come up with innovative material but also poke fun at contemporary taboo topics.

While seeking diversity may have caused the ensemble to miss top performers, the strategy actually put together the _strongest_ ensemble.

### 4. Build a safe environment for people to share ideas and build on those of others. 

When you have a great idea, are you usually open to letting other people tweak or change it altogether? Most people certainly aren't.

While it's understandable to want to retain ownership of your idea, controlling your ideas too tightly can deter co-creation and stifle creativity.

When working collaboratively, it's essential to be open to allowing others to change or build upon your ideas. This might mean your idea gets entirely transformed, but this is not a crisis. What's ultimately important is not finding _your_ idea, but _the_ idea. The best idea!

But what happens when people don't respect this mantra?

Canada's Second City Television (SCTV) was a comedy sketch program similar to the U.S. program, Saturday Night Live. The program was a rare example of horizontal collaboration, in that creatives used ideas from everyone, including the wardrobe, makeup and hair departments, when developing sketches. It was hugely popular.

But when U.S. network NBC picked up the show in 1981, it brought in new management more familiar with a top-down, traditional creative approach. The SCTV cast and crew, who had earned some 15 Emmy nominations before the merger, were turned off by this stifling of their creativity, and many top performers left.

The key to creativity lies in a collaborative approach, one which is best described as "everyone should bring a brick so the group can build a cathedral."

But collaboration doesn't come easy. One major roadblock to co-creation is fear.

Fear comes in many forms: fear of failure, of appearing foolish or fear of the unknown. Yet you have to eradicate fear wherever and however it rears its head.

To do so, there are a couple of easy steps to take. One is to ban yelling in your ensemble, as raised voices can intimidate people.

Would you feel more comfortable sharing unpolished ideas or thoughts in a hostile environment or in one that made you feel safe and welcome? The answer is obvious.

### 5. Use comedy to diffuse tension and facilitate creativity by respecting – not revering – ideas. 

Your boss is making everyone work overtime, again. You're stressed, the atmosphere is tense and everyone is quiet. Then someone cracks a joke and everyone laughs.

Suddenly, the environment transforms: people start talking freely and, as a result, loosen up and start thinking more creatively.

How did such a dismal situation become a fun, light-hearted and productive one?

The answer is comedy. Comedy can be a powerful tool for dispelling tension. Unfortunately, there's no recipe for comedy.

Or is there?

One potential recipe for comedy combines three things: a situation everyone is familiar with, a pain we can identify with and share, and critically, a safe mental distance from topics that cause offense.

Consider a skit The Second City team performed in the late 1990s during the Monica Lewinsky scandal in the United States.

A married couple stands in their kitchen. Every time the husband approaches, the wife turns her head, effectively giving him the silent treatment. Finally he pleads, "Hillary…"

The audience laughs, as they recognize the reference to the current public scandal, share the pain of marital infidelity, and aren't necessarily offended, as few audience members are likely intimates with the U.S. president or his wife.

Another way to facilitate a creative environment is to adopt a stance that _respects_ but doesn't _revere_ other people and their ideas.

What's the difference? Respect is holding a healthy esteem for something, while reverence can become an immobilizing awe.

When you respect something rather than revere it, you can give your honest opinion of it, point out imperfections and share ideas on how it could improve.

Kodak for decades was a household name, but quickly fell apart with the advent of digital cameras, even though the company had actually invented the technology itself. So what happened? 

The company had too much _reverence_ for its star product, analog film. As a result, it didn't jump on the digital film bandwagon until it was too late, and eventually had to file for bankruptcy.

### 6. Fail fast! And fail together. Only then can your ensemble learn, improve and thrive together. 

Have you ever been paralyzed by a fear of failure? You're certainly not alone if so.

Fear of failure is common and a major limitation to company innovation. Yet how does one build a culture of fearlessness? It takes a lot more than just slapping a couple of inspirational posters on the wall.

One key to overcoming the fear of failure is to foster a low-risk environment that allows for a quick recovery when failure does occur. Yet what exactly does this look like?

By lowering the stakes of failure, you create an environment that lets failure, and in turn learning from mistakes, occur as quickly as possible.

The Chicago-based software developer Basecamp routinely holds "product roasts," a meeting in which employees criticize their product and talk about all its failures collectively.

The event creates a safe environment that encourages employees to admit to and acknowledge failures, in order to improve by learning from them.

In another example, advertising agency Ogilvy & Mather holds an annual mock awards ceremony for "career-endangering stunts." During this silly event, everyone has the chance to acknowledge imperfections and mistakes common to all employees.

But what's the point of these goofy evenings and roasts?

Disarming the scary face of failure quickly and with a joke lets people move on to solving problems, while facilitating a healthy attitude toward risk that results in better overall morale. Better morale leads to more creativity, too.

Another key is building an atmosphere of mutual trust that motivates people to take risks without making them feel individually responsible for goofs. In short, people should feel like if they fail, the entourage has their back; that is, they all fail together.

Before a performance, The Second City actors exchange hugs and words of encouragement. This simple gesture reminds each member that they're supported and reinforces the idea that the group both fails together and succeeds together.

### 7. Inspire creativity by rotating leaders and giving your creatives space during the creation process. 

What does an effective leader look like? Many of us might say that a leader needs to take charge, make decisions and give orders. Correct?

Well, the entourage at The Second City would beg to differ.

Being a good leader is about _understanding_ status instead of _maintaining_ it.

This means a good leader knows that sometimes the most powerful thing she can do as a leader is to empower others to lead.

To get the most out of each employee's expertise, it's important to let people in key positions be the spark that ignites new, potentially game-changing ideas.

Viola Spolin, a pioneer of improvisational theater, promoted the idea of _following the follower_.

This methodology lets any member of a group temporarily become a leader when the individual's expertise best applies to the new situation. When a situation changes, presenting a new set of needs, so does the leader, offering a new set of skills.

Having a leader best-suited for the current needs of a group is important, but good leadership is also about setting the right parameters.

As a leader, you need to set reasonable timelines and provide the necessary resources to leave your creatives free to do their thing.

While it's _good_ to touch base with your ensemble, it's _essential_ to avoid intruding on their work before they are ready to share it.

At The Second City, once rehearsals start, no one but the actors and directors are allowed in the studio without an invitation. Even producers need a director's permission to preview a show.

The Second City enforces this as the possibility of a snap judgment, especially early in the process, can have a negative effect on the creative process overall.

When people know they are being judged, inhibitions set in and can prevent them from taking risks, which severely limits creativity.

### 8. Master the essential art of listening by focusing on the words and gestures of the speaker. 

How well we _listen_ can be the difference between running a successful business and a failing one.

Everyday we experience situations that require us to hear and respond to others, an impossible task for an individual lacking strong listening skills.

But why is listening such an important skill? Simple: most people don't do it.

According to a _Forbes_ survey, although we get 85 percent of our knowledge from listening, we only comprehend 25 percent of what we hear. Furthermore, only 2 percent of professionals have any formal training in how to listen better.

These statistics are even more shocking when we consider the costs of poor listening.

Companies that don't listen to customers develop products that nobody asked for and for which there is no demand — like New Coke, Coca-Cola's infamous misadventure in revising the recipe of its flagship soda.

Fortunately, there's good news for those of us who have problems with listening.

Listening is like a muscle that can be strengthened, if you change your habits and do exercises.

A common bad habit when trying to listen is to focus on a response while totally missing out on what the other person is saying. One way to overcome this is to _listen to understand_.

This means focusing solely on the other person's words instead of on your response. Doing so keeps you in the present moment, drastically increasing your comprehension.

The Second City once consulted an ad agency on the issue of client retention. To teach listening skills, the group had employees perform an exercise that required them to make eye contact, establishing a connection, before speaking to a person. This helped ensure the other person was listening.

In another exercise, an account executive spoke gibberish while her colleague was tasked with translating. By taking into account all possible information, from her intonation to the way she was standing, her colleague was able to translate the nonsense into comprehensible speech. This showed the power of truly listening!

And by applying these new listening skills, the agency acquired new clients and had no trouble keeping them!

### 9. Final summary 

The key message in this book:

**Taking lessons from the world of improvisational theater can help you build a creative, collaborative and fearless environment in which your business ensemble can succeed. It's as simple as applying the techniques of the "yes, and" strategy, empowering those in key positions and ensuring that team members know that if they fail, they all fail together.**

Actionable advice:

**Build your listening skills by not using the word "I."**

Here's a great improv lesson that will help hone listening skills and partnership skills. Have your team form pairs and start a conversation about any topic. The only rule: no one can use the word "I." in doing so, each participant will better focus on the other, and in the process, both will improve their listening skills!

**Suggested further reading:** ** _To Sell Is Human_** **by Daniel Pink**

_To Sell Is Human_ explains how selling has become an important part of almost every job, and equips the reader with tools and techniques to be more effective at persuading others.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!
---

### Kelly Leonard & Tom Yorton

Kelly Leonard is the executive vice president of The Second City. Since 1988, he has fostered collaborations with Lyric Opera Chicago, the Norwegian Cruise Line and _The Chicago Tribune,_ among other organizations.

Since becoming CEO of The Second City in 2002, Tom Yorton has applied his knowledge of improvisational comedy in his consulting work with businesses and professionals.

