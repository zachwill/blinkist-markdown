---
id: 59d5ebddb238e10005435c02
slug: you-are-a-badass-at-making-money-en
published_date: 2017-10-06T00:00:00.000+00:00
author: Jen Sincero
title: You Are a Badass at Making Money
subtitle: Master the Mindset of Wealth
main_color: 25BA4C
text_color: 1B8738
---

# You Are a Badass at Making Money

_Master the Mindset of Wealth_

**Jen Sincero**

_You Are a Badass at Making Money_ (2017) offers a fresh and exciting perspective on what it takes to bring home the big bucks. Yes, you too can be a money-making maestro once you improve your mind-set and understand the energy of money. So stop making excuses and fooling yourself that only evil people are rich. Unlock your inner badass and open the door to success!

---
### 1. What’s in it for me? Overcome your financial inhibitions and get rich. 

Do you sometimes wish you had more money? Of course you do! Who doesn't? Even so, most of us harbor negative emotions about money and the people who make it. All this negativity is keeping you from realizing your economic potential. That's a shame because, as you'll see in these blinks, you have the power to change your situation.

All you need to do is get into the right mind-set and overcome the notions that are holding you back. Let's get to work on making you a badass at making money!

In these blinks, you'll learn

  * that wanting money doesn't make you greedy;

  * why you have to get out of your comfort zone if you want to become rich; and

  * why Jim Carrey wrote himself a $10 million check.

### 2. Many of us have a conflicted relationship with money that keeps us from reaching our potential. 

Money is a complicated subject. Some people love and obsess over it while others have come to despise the stuff. And while there are those who desire money, there are also those who feel ashamed over having so much of it.

Our culture has many negative beliefs about money, like it being the reason people turn into greedy and selfish monsters. But this is a myth.

Making money is what allows people to break free from financial constraints and use their time and energy for the things they care about. This is how philanthropists and activists are able to start charities and spread the word about important issues.

Often, our attitude to wealth will fluctuate between good and bad.

If you visit a friend who has an impressive widescreen TV, you might feel the desire to buy a similar TV. But then you might see a person you don't like bragging about owning the exact same TV. In an instant, you feel disgusted with yourself for ever wanting such a pointless luxury item.

Another misguided belief about money is that if one person gets rich, it means someone else stays poor.

This idea stems from what's known as a _lack mind-set_, which leads us to believe that there just aren't enough dollars or resources to go around.

But think of it this way: If you eat a sandwich, do you think someone in another country is having their sandwich taken away?

No, the real scenario is that you eat your lunch to have the energy to work and create opportunities for others to do their job, thereby making it possible for them to afford their own sandwiches.

Sincero used to be plagued with negative ideas about money, and as a result, she wasted much of her youth with low-paying jobs such as catering, babysitting and knitting. But now she's here to help you avoid wasting another minute and begin a healthy and lucrative relationship with money.

### 3. Money isn’t inherently evil; it’s simply an effective way of conducting business. 

On its own, money isn't good or bad — it's a blank slate. It's up to us to decide how money will be put to use — for good or evil.

Certainly, there weren't any evil intentions when the concept of money was invented back in ancient times. People just wanted a better way of doing business than bartering, which had become a royal pain in the ass.

Back in the day, if you wanted a nice fur coat, you might have to build a stone wall. Or a nobleman could be asked to trade his daughters in exchange for a seaside castle.

Negotiations like these could take a lot of time and involve a lot of yelling; whereas with currency, if you wanted a camel, all you had to do was open up your change purse and pay for it.

So rather than being evil, it's more accurate to recognize money as an efficient way to get things done.

Think of it this way: Are all cars evil because some people are filled with road rage and scream obscenities when they get behind the wheel? Are all computers inherently evil because some people use them to commit crimes?

It's true that people around the world have done horrible things for money, but the blame should be placed with the individual perpetrators of these acts and not with the cash.

Here's an exercise to help you disassociate money from evil, greedy characteristics.

Since these negative qualities are usually reserved for people, try writing two "Dear Money" letters, as if you were addressing a person — one that explains your negative feelings and one that explains at least a few positive feelings.

Once you've finished, read the letters out loud. At this point, you'll realize that your negative feelings don't hold up to logic, while the positive things all make sense.

To continue nurturing a positive attitude toward money, express your gratitude when you get paid and treat these occasions as the blessings they are.

### 4. You can improve your situation by putting out positivity to encourage solutions to emerge. 

Even though we're taught to accept the things we see and hear as reality, the truth is far more complex.

Since our five senses are being processed by our brain, we're creating an illusion — our own unique universe in which we constantly live. So it's really important that our brains are processing things in a positive way and creating a prosperous universe.

At any given moment, we can interpret things as being either good or bad, and it's easy to only view things negatively and get stuck in a bleak worldview.

So you might think to yourself, "I'll never be able to afford a vacation, so why even bother trying to save up money. Screw it, let's just waste it on junk food, at least that'll make me happy for a little while."

This kind of thinking can last a lifetime. Alternatively, you can start putting positive thoughts into your universe and experience opportunity and good fortune.

People who accomplish great things, like ascending a mountain in a wheelchair or escaping poverty, don't do so by thinking negative thoughts. They change their situation by putting out positivity and grabbing opportunities when they happen.

This kind of attitude could change your financial situation, just as it did for the author.

Sincero once found herself in desperate need of money, so she thought about people she might reach out to. One person who came to mind was a client who'd once taken a coaching class she offered.

Over a year earlier he'd signed up for a basic offer at $25 per hour. Now, she thought, maybe he'd be interested in upgrading to the premium package at $300 per hour.

But when she opened up her email, there was already a message in her inbox from the very client she was thinking of, asking her how much she was charging these days!

It worked out because the positivity she was putting out allowed the universe to respond with the perfect solution.

### 5. Money can bring either positive or negative energy into your life, so learn to control it. 

When you exchange money with someone, it's not just a physical transaction; you're also putting either good or bad energy into the universe.

That's why it's important to make the right kinds of transactions to bring good fortune back your way.

For example, if you made $50 raking up leaves on your neighbor's lawn, this puts out different energy than stealing $50 out of someone's pocket on a crowded subway train. Naturally, offering a helpful service is positive, while stealing is negative.

If you're an employer, you're also putting out energy when you pay your workers. So, if you're ripping people off and underpaying them, you're giving out bad energy and creating a negative relationship. But if you're being honest and paying your employees the right amount, the relationship and energy will be positive.

You should always be aware of the kind of energy being created in these money situations and strive to keep all your dealings positive. You should also think of how you can channel this energy so it can continue to generate more good in the world.

For this to happen, you shouldn't focus your energy on one single thing, but rather be open to all good possibilities.

By focusing on just one person or possibility, you're blinding yourself to the infinite number of other opportunities that await and can provide more positive money.

It's a lot like finding the man of your dreams. If you're focusing all your attention on one guy, your happiness is therefore completely under his control, whether or not he decides to return your feelings. So instead, focus on recognizing the qualities of the person you're looking for, and you'll soon find there's more than one guy out there who can bring you the positive love you're looking for.

### 6. There are ways to break free from your fears and worries about making money. 

If you're feeling like your anti-money beliefs are still too deep-seated to shake, here are some more ways you can break free.

One of the best ways to start thinking differently is to dive right in and take a bold move toward making your dreams come true.

Sincero's dream was to be a successful life coach. To help her make that transition she had a choice of paying $15,000 for a group class or $85,000 to be taught by an experienced coach in one-on-one sessions.

She knew that one-on-one sessions would be far more rewarding, even though the cost was more than she made in a year. But she committed herself to finding a way to come up with the money. It was a big step, but it forced her to stop thinking "no way" and start thinking "there must be a way."

Once you leap into the great unknown, there are bound to be lingering doubts and worries. But instead of ignoring them or letting fear take over, write them down as a way to acknowledge their power and move on.

Even when Sincero was standing in line, ready to sign up for the coaching lessons, she was bombarded with fears that she was making the wrong choice. These wobbles are usually irrational.

She worried that if she succeeded, her father would feel useless because he no longer had to help her out financially. You might have similar thoughts or believe it's too late to change, but it's all nonsense!

Any subconscious worries can be altered to create a new narrative for your success story.

Sincero imagined a new image of her dad being proud of her success and realized that in reality, he'd be happy that she was finally able to support herself.

You don't necessarily have to pay for a therapist to change the narrative in your head. In fact, it's best when you can prove to yourself that you have the power to think positively and control your own destiny.

### 7. Create specific financial goals to help increase your chances of monetary success. 

Generally speaking, people don't become rich unless they want to. So naturally, it's important to tap into your desire for money if you want to start banking the Benjamins.

The key to unlocking that desire is to get specific about why you want money and how much of it you need. Then you can set solid goals for yourself.

A famous example of how well this can work comes from the actor Jim Carrey, who wrote a $10 million check to himself when he was broke and desperate for a job. Carrey dated the check three years in the future, and sure enough, this act sparked his motivation and his career.

By the time the date on the check arrived, he'd landed his biggest role yet in the movie _Dumb and Dumber_, which earned him enough money to cash that check.

A goal like this is effective because it's specific. When you're precise about the goal and how meaningful it is to you, it increases the likelihood of the goal becoming a reality.

Think of it as placing an order with the universe. Now, you wouldn't place an order at a sandwich shop by saying, "One sandwich, please." You have to tell them what kind of bread, whether you want mayo or mustard, and so on. This way, you'll get what you want — and the same goes for getting what you want out of life.

Being specific also provides a great boost for your determination. A sandwich is great, but a sandwich with all your favorite ingredients in just the right proportions is what gets your mouth watering.

But being specific doesn't mean limiting yourself. Remember to stay open to the surprises the universe has to offer. This concept is known as _surrendering to universal intelligence_.

Let's say you're a writer and you've set a goal for the month to find ten new publications that you've never sent samples to before. This is great, as long as it doesn't blind you to what your old contacts may have to offer.

The world is full of opportunities and a true badass doesn't let them slip by.

### 8. Final summary 

The key message in this book:

**Many people have negative associations with money that keep them from their full earning potential. By adopting a positive outlook on life and opening yourself up to the universe, you'll find that good things can happen, and wealth can be part of a harmonious, happy life. The universe wants to provide you with money, so take control and become a badass at making bank!**

Actionable advice:

**Create a positive environment for yourself.**

If you decide to transform your financial reality, make sure you avoid all the naysayers and doubters and surround yourself with people who believe in your potential to realize your dreams. You should also spend time in places and doing activities that form your ideal life. If there's a car you hope to buy in the future, take it for a test drive. If there's a neighborhood you want to move to, spend time in its cafe's and parks. Being in these environments is a great way to help keep you inspired and motivated.

**Got feedback?**

We'd sure love to hear what you think about our content! Just drop an email to remember@blinkist.com with the title of this book as the subject line and share your thoughts!

**Suggested** **further** **reading:** ** _You Are a Badass at Making Money_** **by Jen Sincero**

_You Are a Badass_ (2013) is your guide to living life to the fullest. These blinks combine an analysis of behaviors that can hold you back with down-to-earth strategies geared to help you break bad habits, so you can truly live your dreams.
---

### Jen Sincero

Jen Sincero is a success coach and sought-after motivational speaker who loves to improve lives and spread positive vibes. She is also a _New York Times_ bestselling author whose books include _The Straight Girl's Guide_ _to_ _Sleeping with Chicks_, _Don't Sleep With Your Drummer_ and _You Are a Badass_.

