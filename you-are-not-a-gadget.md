---
id: 534e9a5865316600073a0000
slug: you-are-not-a-gadget-en
published_date: 2014-04-23T13:13:34.643+00:00
author: Jaron Lanier
title: You Are Not a Gadget
subtitle: A Manifesto
main_color: DADA80
text_color: 737343
---

# You Are Not a Gadget

_A Manifesto_

**Jaron Lanier**

_You Are Not a Gadget_ (2010) examines why the internet tends to glorify the hive-mind and devalue the individual. Serving as both a history lesson of the web's origins and a warning of the future consequences of its current path, this book illuminates the hidden design of the web.

---
### 1. What’s in it for me? Learn how the internet is undermining human creativity and individuality. 

Many of us believe that technology is always a force for good, driving our species to ever higher heights. And we may think that the internet is the greatest of all inventions in this tradition: a technology that allows us to share and spread information and creativity across borders — and all this for almost no cost. No longer do we have to save up to buy that album or that new book: we can simply source it online for free.

But has anyone ever thought of the potential downsides?

Has anyone thought of the producers of content? How exactly do they survive in an environment where they don't get paid?

And how do they feel about their work being constantly cut up into fragments and mixed together in mash-ups and viral videos? Do you think they appreciate their hard work being transformed in this way?

These blinks outline

  * the downsides of our technology and internet worship, explaining why Londoners still have to go to work in cramped, overheated subway cars;

  * why the new trend of crowd-sourced material could lead to a totalitarian state like North Korea; and

  * why the development of robots and the internet could send us all back to a state of serfdom — except for the technology lords, of course.

### 2. Technology can get stuck in its initial design and hamper future innovation. 

In the early 1980s, inventors created technology for electronically expressing musical notes. It was called _MIDI_. Very quickly this development became incredibly popular, and was used to create the sound interfaces in a wide range of computers and electronic musical instruments.

But as technology moved on and people tried to update the MIDI system, they ran into a major problem. MIDI's initial popularity meant it had been included in so many technologies that modifying it would require changing the way all those technologies worked — including the music systems used in computers across the world.

In other words, MIDI had become _locked in_ — so enduringly so, in fact, that we still use it today!

This locked-in problem is common: the inflexible initial design of a technology becomes widespread, which limits future developments.

And the initial design is more often than not imperfect, as it is often the product of chance, the technological limits of the time or whatever solution the inventor found easiest.

The downsides of locked-in products are greater when the initial design involves a large and complex system.

This is because the more complex a system is, the more interconnected parts it will have — which will all need altering as technology improves.

For example, the London Underground was constructed with the limited technology of the nineteenth and early twentieth centuries. One result of these technological limits is the Underground's narrow railroad tunnels.

These narrow tunnels now cause many problems: for example, they are too slim to allow air conditioning units to be placed in trains.

And because of the sheer size and complexity of the system, its multiple stations, lines and miles of track, it is logistically impossible to widen the tunnels — so Londoners' daily journeys remain uncomfortable to this day.

### 3. Our tendency to worship the power of technology causes us to devalue human individuality. 

As you know, the internet contains billions of pieces of information, scattered all over the chaos of the web. But some believe it won't be long before we create technology capable of collecting all these pieces together — into a single form of ultimate wisdom!

And if this _singularity_ happens, it will signpost the moment when human knowledge is surpassed by computer intelligence.

Those who believe this often point to instances where computers have already surpassed a human.

For example, in 1997, the computer program Deep Blue defeated the chess champion Garry Kasparov.

And, the argument goes, if a computer can beat a grandmaster at chess, it can't be long before computer knowledge completely outstrips ours.

Many people who believe in this singularity take these examples as evidence that computers are superior to our brains.

But when we worship the power of computers, we forget their limitations. And one limitation is that computers seriously reduce our individuality. This is because most computer systems limit life to a binary series of either/or categories.

For example, on Facebook your life is boiled down to a series of information boxes: where you went to school, what films you like, what your relationship status is.

This makes everyone a variation on the same theme, and leaves little scope to show our uniqueness.

Another example of the limits of computers is their requirement for human input to give them purpose.

Because without us to program them and give them orders, what could a computer do with all the knowledge on the web? Nothing.

Computers need a human to transform this information into meaning, and to decide what is valuable — and what is not.

So we shouldn't deify the severely limited intelligence of computers. Instead, we should continue to explore the birthplace of our creativity and individuality: our own brains.

### 4. The internet’s open culture turns original works into interchangeable segments – with no regard for the original author. 

These days, almost everyone has a favorite online _mash-up_ — a blending together of parts of separate songs or videos. But did you ever stop to think about the original works from which they were created? Probably not.

This disregard for originality is a consequence of the internet's design, which treats unique content created by individuals as interchangeable bits of a greater whole. This phenomenon is known as _free culture,_ where all information and media is free to be edited and repackaged by others — and it changes the way content is consumed.

Under the banner of free culture, people increasingly look at only snippets of original content, whether it be a shortened news report on a blog or a mash-up of two songs. This way of consuming content has two consequences: it shortens people's attention spans and diverts attention away from the authors of the original content.

For example, reports and articles from the _New York Times_ are rarely read on their website. Instead, they are read on news aggregation sites like Yahoo News, or condensed into shorter forms on blogs.

This means many reports are divorced from their original context, which can lead to misunderstandings. And it means people now rarely have the attention span necessary to read through a whole newspaper.

By treating content as fragments in this way, we reduce the originality and quality of future works.

This is because creators know their content won't be consumed as a fully developed whole, so they create less extensive, lower quality works better suited to digital platforms.

For example, the quality of journalism has declined as bloggers just rehash news items from established news websites — instead of doing any original research themselves.

And as the quality of content declines, we are left with repeatedly recycled and stale works.

This is the tragedy: instead of being inspired by a groundbreaking film like _Blade Runner_ to create our own movie, we'd rather just mash up a scene with our favourite hit.

### 5. Today the merits of collective intelligence are erroneously valued above those of individuals. 

We've all seen it before: someone has taken a scene from our favorite movie, and spliced it together with a video of a cute cat. Pretty cool, right?

Maybe not.

This mash-up culture reduces information to mere bits to be swapped around and played with by millions of different people.

Treating information in this way implicitly promotes the idea that _crowd-sourced_ content is superior to content created by individuals. Together all these people form a _hive mind_ that's supposedly infinitely more intelligent and creative than that of the original author.

The basic idea behind this is that many people's _collective knowledge_ is greater than one person's _individual knowledge_.

For example, websites like Wikipedia diminish the value of individual authorship in favor of crowd-authored and crowd-reviewed content. For each article there is not one, but many different authors, each contributing his own bit of knowledge.

But dismissing the individual's creativity and letting the collective decide what is created could one day lead to a totalitarian society like North Korea.

Because if we bow down to the collective will and suppress our individuality, only what the collective deems good will remain. As an extreme example, imagine if, instead of having millions of different books to suit individual preferences, there was only one book in existence, compiled and approved by the hive mind.

To avoid such a situation we should never forget that individuals are superior sources of knowledge to collective minds.

What's better? One person devoting his entire life to an idea, or millions of people devoting four minutes each?

Well, quantity really doesn't equal quality, and no matter how much garbage you add to a pile, it's still garbage.

And it's been proven that one intelligent individual giving their undivided attention to a subject is far more valuable than piecemeal contributions, no matter how many people are contributing.

Einstein is a perfect example: he singlehandedly contributed more to human thought than all Wikipedia contributors combined.

### 6. Online anonymity creates an unconstructive and unfriendly environment that encourages mob behavior. 

Nearly everyone familiar with YouTube comments has encountered the unlimited negativity of trolls — people who deliberately start arguments or upset people — and the often mob-like mentality that governs the "conversation."

But why exactly does this type of behavior happen online? Could it be the result of natural human tendencies?

Actually, it's a consequence of people's ability to be anonymous online.

This anonymity is facilitated by the way many websites are designed, which encourages commenters or contributors to create short-term, anonymous identities. This allows them to participate in discussions or forums without taking responsibility for their actions.

For example, people often create "flamer" accounts to make offensive comments in complete anonymity, while avoiding any connection to a real online identity, like a Facebook profile.

This _drive-by anonymity_ encourages bad behavior on the web.

This bad behavior is exemplified by hive-like attacks online or individual trolls, and the consequences can be deadly.

For example, after being relentlessly harassed by trolls, Korean actress and star Choi Jin-Sil committed suicide in 2008.

And we've all seen this bad behavior wherever debate is possible on the internet. No forum or comment section is safe from people who would rather provoke others' emotions behind the cloak of anonymity than contribute to constructive discussions.

But there are design solutions to avoid these problems.

One way is to encourage people to invest in their online identity while still remaining anonymous.

This will encourage users to preserve their identities' reputations, while also retaining some of the benefits of anonymity.

For example, on eBay, buyer and seller ratings encourage users to engage in good behavior — even though their identities are hidden behind anonymous usernames. If they behave badly, they get bad ratings, lose their online credibility — and their ability to buy and sell at bargain rates.

### 7. The internet’s design only benefits consumers and companies like Facebook and Google – not the content creators. 

So we have seen who loses out because of the almost limitless availability of content on the internet, but who benefits?

The main beneficiaries are those who own websites that direct users to content, like Google or Facebook.

These sites use advertising to make huge amounts of money — and by collecting search data about what people actually want, they make their advertising highly efficient.

Facebook is a perfect example: it earns its enormous revenues by analyzing profiles, personalizing advertising for each user and showing each individual links they will want to click on.

The other group to benefit from the internet's design are the consumers, who can access limitless cheap or free content.

This is because digital content is easily, freely — and mostly illegally — copied over the internet, be it a book, an album or a film.

And even if users can't find an illegal copy, or don't want to take the risk, they can turn to providers like Spotify or Netflix that provide legal content for a small fee.

And all of this comes at the expense of individual creators of content.

The sheer amount of available content on the internet means creators have to fight hard to get noticed.

For example, there are millions of singers and bands on YouTube, so if you want to be the next big star, you'll need to devote most of your efforts to promoting yourself.

And, unfortunately, content creators also stand very little chance of getting paid for their efforts, because consumers demand that content should be cheap or free — or they'll go elsewhere.

This forces upcoming YouTube bands to offer their songs for free, because if they attempted to charge, they would struggle to find buyers.

In the next few blinks, we'll look at the ramifications of these technological trends on the world as a whole.

### 8. Crowd-sourcing information and content can be risky, and devalues creativity. 

As we have the seen, in the modern world the collective is more important than the individual.

Many businesses, from content providers to financial traders have learned this, and try to use it to their advantage.

They do this by extracting the collective knowledge of the crowd through statistical models.

For example, financial businesses — like hedge funds that sometimes manage billions of dollars — collect financial information from a huge number of sources using computers, which they then use like "financial search engines" to search for the best places to invest.

Similarly, content providers like YouTube also use the crowd to their advantage. Instead of paying people to create their own material to attract viewers, they let the crowd create their own material.

But this crowd-based approach creates a divide between the companies and what they do.

When hedge funds let a computer decide what to invest in, they distance themselves from the actual markets and products in which they invest.

And when content providers like YouTube rely on a member of the crowd to provide them with the next big viral video — for free — it means they don't have control over their own content.

This distance between a business and its investments or content can have dangerous consequences.

For example, the economic crash of 2008 that plunged the global economy into recession, was caused in part by traders using computer models to invest in products about which they had little or no idea.

And the YouTube model has its own dangers, too: instead of investing in and encouraging truly creative content, the site is crammed full of repetitive and mindless video junk.

Good examples are the endless mash-ups, memes and home videos that are designed to generate a quick giggle rather than long-term conversation.

### 9. As new technology replaces more jobs, society could split into a wealthy, technology-owning minority, and a poor majority. 

From the invention of the wheel to the advent of smartphones, technology has been harnessed to make people's lives easier.

For example, before the Industrial Revolution, slaves and near-slave manual laborers handled the brunt of the work.

But industrialization produced machines capable of doing the work, which meant humans no longer had to, whether it was sowing or harvesting crops, or digging the foundation of a house.

And as technology continued to advance in this way, those who had previously been shackled to manual labor were free to find work in other roles.

For example, those who had worked in the fields had more opportunities to adopt traditionally middle class roles, like being a secretary or travel agent.

But we are approaching a point where technology could make nearly all human labor totally unnecessary: a point where human workers are replaced by robots.

Seems far-fetched?

Well, robots can already fight battles and perform complicated surgery, and having everything done for us by robots does sound sort of great. Be that as it may, a review of recent history reveals a disturbing trend.

Throughout the last two decades, technology like the internet has spread all over the globe but has represented little financial benefit for the vast majority of the population.

Wealth has actually been increasingly concentrated in the pockets of the few — like technology specialists and financial traders with their computer models — while middle class prosperity has declined.

And if this trend continues, technology could become a tool that only enriches those who already own it.

This could ultimately lead to a situation where the vast majority of people are impoverished as their jobs are replaced by robots, while those who control the technology, control the wealth.

### 10. It’s not too late to promote better technology that will protect human intelligence and individuality. 

Many of us were born before the advent of the internet. This means that the web and other technologies are still young and malleable — which allows us to change technology to maintain and promote the values of human intelligence and individuality.

The best way to do this is to make sure people are able to create unique and original works by ensuring that their intellectual property is protected.

One way to do this would be to create a single protected copy of everybody's work which people pay to access online. This would ensure that content producers were recognized and compensated for their work.

Another way content creators could be compensated is through an innovation calledthe _songle_, a USB stick that allows you to listen to protected music on your computer — but only when it's plugged in.

Yet another option is _telegigging_, where people pay to watch content that is only available online at particular times.

A different approach to ensure compensation for content creators would be to institute a small tax on all internet usage.

This could be done by taxing all online users for the amount of _bits_ — meaning morsels of information — they access, and transferring the money to those whose bits, in the form of content, were consumed.

Say someone looks at your online tutorial — they would then pay a small fee for the amount of bits they used — the more bits, the higher the price.

And rather than going into the pockets of an internet provider or a search engine, this tax would go straight to you, the content creator.

By taking steps in this direction and ensuring content creators are paid for their labor, we could encourage high-quality, original online authorship.

### 11. Final summary 

The key message in this book:

**We need to make sure that the technology we design rewards the right people and fosters the right values. At the moment, the internet doesn't achieve this: content creators struggle to get compensation, while websites like Facebook and Youtube make millions — and we place far too much trust in collective intelligence over individual specialists.**

**Actionable advice:**

**Next time you think about downloading something for free from the internet, consider the producer of the original content.**

Think about the effort and time they put into the creation of what you would enjoy for free. Before downloading, try to find a link that allows you to make a donation to the creator — or at least send them an email of appreciation.

**Invest in an online identity to promote online responsibility.**

Instead of hiding behind an anonymous username like "truetimer79," post on the web under your real name. This will force you to pay attention to what you express and how you express it — and will set an example for other web users.
---

### Jaron Lanier

Jaron Lanier is a computer scientist, musician and writer. He was an early innovator in the field of virtual reality and has taught at Columbia University and New York University. His other books include _Information Is an Alienated Experience_ and _Who Owns The Future?_

