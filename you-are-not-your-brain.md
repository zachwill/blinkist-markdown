---
id: 54aa4bd96233320009920000
slug: you-are-not-your-brain-en
published_date: 2015-01-09T00:00:00.000+00:00
author: Jeffrey M. Schwartz and Rebecca Gladding
title: You Are Not Your Brain
subtitle: The Four-Step Solution for Changing Bad Habits, Ending Unhealthy Thinking, and Taking Control of Your Life
main_color: FDFD33
text_color: 7D7D19
---

# You Are Not Your Brain

_The Four-Step Solution for Changing Bad Habits, Ending Unhealthy Thinking, and Taking Control of Your Life_

**Jeffrey M. Schwartz and Rebecca Gladding**

_You Are Not Your Brain_ explores our deceptive brain messages which program us to have harmful thoughts such as "I'm not good enough." And it tells us how we can change this detrimental wiring by challenging these brain messages and focusing our attention elsewhere. In doing so, we can rewire our brain to make it work _for_ us, not against us.

---
### 1. What’s in it for me? Learn how your brain tricks you and how you can stop it. 

Have you ever felt unhappy, stressed out or terrified for no reason whatsoever? Probably: almost all of us, from time to time, experience dark thoughts and worries. As well as being unpleasant, these emotions can cripple us mentally.

Many people spend large parts of their lives constantly reacting to unhappy thoughts in their mind. For example, some people experience a crushing terror when they speak in front of an audience; others reject social interaction as their mind tells them they are unworthy of love and affection.

If such scenarios resonate with you, then these blinks can help. They show that the dark thoughts in the mind are not "you," but are false messages from the brain. And because you are not your brain, you don't have to listen to them.

Read on to discover how you can stop yourself from blindly following your brain and discover your true self. 

In these blinks you'll discover

  * the four steps to overpowering your brain;

  * how your brain is wired to make you unhappy; and

  * how you can stop your wandering mind from taking you with it.

### 2. Your brain sends you false messages, but you aren’t defined by them. 

Do you ever find yourself caught up in things you don't want to be doing, or feel helpless when you "go" somewhere, emotionally and mentally, that you don't want to be?

We all experience these slips and it results from _deceptive brain messages_ that breed false thoughts or harmful impulses, which lead us astray from our true goals and intentions. Not only that, but deceptive brain messages cause problems such as overthinking and anxiety.

Consider one of the authors' clients. He was a talented Broadway performer when his deceptive brain messages started to coerce him into believing he was unworthy of any good coming to him. Eventually, this led to him suffering from intense stage fright as well as fear of rejection for years.

Aside from stemming from his childhood experiences, he was impacted by one specific experience when he was twenty years old: he froze in front of a famous Broadway producer. And since then, his brain had been ignoring his positive qualities and honing in on his imperfections.

He paid attention to these discomforting false messages and, as a result, he started avoiding auditions completely — an unhealthy chronic response which only reinforced the messages in his brain that he was no good!

But he isn't defined by these deceitful messages, and you aren't by yours either.

Even though it may appear that we have no choice but to follow what our brain tells us, we must remember our biology isn't our destiny.

You aren't fated to live a life based on your genes. You actually have the power to break through many of the obstacles you inherit by changing the way your brain works.

> _"Clearly, the brain can exert a powerful grip on one's life — but only if you let it."_

### 3. Brain wiring makes habits hard to change and causes intense, uncomfortable sensations. 

Why is it that when we know we shouldn't be doing something, like smoking or binging on ice cream, we keep doing it over and over again? Why do bad habits feel so impossible to quit?

It's because when we act out an unhealthy behavior, it provides us with a sense of relief from distressing sensations, in turn wiring our brain to connect this behavior to a temporary "high," which strengthens the habit.

Note, for example, the authors' client, an executive who was chronically feeling stressed that everyone around him was always seeking his help and advice.

To relieve his stress, he turned to drinking a glass of wine. The trouble was, this resulted in him often downing two or more glasses so that he could continue to feel the relaxing effect. This relief made him crave alcohol each time he felt stressed and then the craving became constant.

This very thing happens whenever we pay attention to our brain's deceptive messages. For example, if the brain sends a message like "I'm not good enough," and we try to forcefully get rid of the thought by acting out a stress habit — like smoking a cigarette or looking for other people's reassurance — then we will be hooked on those temporary fixes.

That is to say, we "feed the monster" when we respond to false brain messages. We seek temporary alleviation when we train our brain to connect the unhealthy behavior to a feeling of relief.

This unhealthy wiring in turn causes distressing sensations. The most disconcerting thing about our brain's deceptive messages are the physical and emotional sensations that they elicit, which feel very real. For instance, the message "I'm unlovable and disgusting" for some people might result in a potent physical sensation to excessively diet or purge, thereby putting a real strain on their physical health.

Every time we nurture the vicious cycle of deceptive brain messages, we make the uncomfortable feelings even more real in the long term.

### 4. The mind’s ability to change the brain, called self-directed neuroplasticity, enables you to rewire your brain to work for you. 

So is there any way of conquering these destructive brain messages? Yes, there is.

The good news is we can arm ourselves with _self-directed neuroplasticity,_ meaning we can change how we think by constructively focusing our attention.

Neuroplasticity is the ability of our brain regions and connections to adopt new roles and functions. If you take control of this ability, you will be able to have a say in how your brain is wired. Self-directed neuroplasticity involves using focused attention, combined with dedication to follow through on your choices. But how do you do this?

By constructively focusing your attention, you are enabling yourself to expose the deceptive messages, rather than believing they represent your real self. When you know how to do this, you are actually reprogramming your brain to work for you, not against you.

One woman utilized this phenomenon when she was recovering from a stroke that rendered half of her muscles useless.

The people at the rehabilitation facility helped her focus her attention toward identifying the frustrating sensations that came up whenever she wasn't progressing as quickly as she wanted to. She learned to reign in the deceptive messages that told her to give up by regarding them as false. This empowered her to opt for more positive responses, which pushed her to continue on.

Amazingly, brain scans revealed that by focusing on things she valued and disregarding the deceptive brain messages, she rewired her brain so that new brain regions became devoted to moving her body. That is to say, self-directed neuroplasticity changed her brain to enable her to recover from the stroke.

This self-directed neuroplasticity is the basis for the author's Fours Steps Program, as we'll see in the next blink.

### 5. There are Four Steps to dismantle the associations between unhealthy thoughts and habits. 

Now that you know there's a way to change your brain's unhelpful wiring, you're probably wondering how to get started. Fortunately, there's a systematic method to help you, called the _Four Steps._ The Four Steps are a guide to applying self-directed neuroplasticity to your situation, so that you can break the brain circuits linked with unhealthy habits and reinforce those that support healthy actions.

Specifically, the Four Steps focus your attention to _relabel_, _reframe_, _refocus_, and _revalue_ the deceptive messages in your brain.

It's important to note that, in the program, you shouldn't attempt to _stop_ the brain messages from arising as doing so is almost impossible. Instead, the aim of the steps is to teach you how to discount the false brain messages, in order to "focus your attention on things that are genuinely important to you."

When you commit to enlisting the Four Steps, you rewire your brain in line with _Hebb's law_ and the _quantum Zeno effect_.

The reason we create both good and bad habits — such as learning to walk again after a stroke or learning to drink alcohol when under stress — can be explained by _Hebb's law_ and the _quantum Zeno effect_. Hebb's law posits that when particular brain areas are repeatedly activated by a certain behavior, like breathing deeply when we are upset, a circuit is formed, which we reinforce each time we repeat a similar behavior.

The quantum Zeno effect occurs when we, by focusing our attention, hold the activated brain areas in place long enough so that the rewiring can happen.

So by using these Four Steps and chronically concentrating on something healthy and helpful, you will eventually create and strengthen new brain circuits.

Now, let's go through the Four Steps one by one.

> _"You can focus your attention on what is important to you, not solely the messages coming from your brain."_

### 6. Step 1: Relabel by identifying your deceptive brain messages and call them what they really are. 

It may seem obvious, but before you can change anything, you first have to know what you want. So the first step in combatting your deceptive brain messages is to identify and relabel them.

This step involves developing an awareness of what your brain is doing as it's doing it, often referred to as cultivating mindfulness. Mindfulness is an experiential process, so you need to put some effort into it and practice it before it can be useful to you.

A helpful exercise for developing mindfulness is to find a private place just for yourself and then, while concentrating on your breath, notice when your thoughts drift in and out of your mind, without placing judgment on them. Bring your attention only to the fact that they arise, and as they do, return to your breathing. This exercise will help increase your awareness of thoughts as they come about during your day.

Remember, the aim at this stage is to gain an awareness of the _process_, not the _content_ of the deceptive thoughts — just watch what comes up, without attaching meaning to it. Then, when you've pinpointed your deceptive messages, you should label them for what they actually are.

Consider an example of someone who often got stuck continually thinking about her future, such as ending up a lonely old woman living in a nursing home. As she dwelled on these thoughts, she'd get all caught up in her overthinking. However, when she started relabeling these moments as "spinning" or "ruminating," it helped her separate herself from her thoughts for a moment, and she was then able to move on.

The most effective way to see your false messages for what they really are is to take "mental notes" of them in one- or two-word statements, such as "worrying" or "mind wandering."

This will help you externalize the messages and understand they are just part of your brain, not youself.

> _"At its core, mindfulness is about awareness — being fully knowledgeable that something is happening right now, in this very moment."_

### 7. Step 2: Reframe your deceptive thoughts by changing your perspective and seeing them as false, foreign invaders. 

Now that you know how to identify and label your deceptive thoughts, how can you change your relationship to them? You can use the method of _reframing_, which involves altering how important the deceptive thoughts are to you.

Take, for example, someone the authors helped. He had recurring thoughts that if he didn't check certain things at a certain time, something bad would happen to someone important to him. Although he could admit that the thoughts were false, he felt urged to attend to them, as it appeared to be the only way to rid himself of the awful sensations they generated.

Fortunately, he learned to resist the impulses to check things when he started relabelling the thoughts as "false foreign invaders." He also was able to _reframe_ them in order to perceive them as part of his unhelpful brain wiring.

The aim of reframing is to understand that false brain messages are separate from the real _you_. Knowing this places a buffer between the messages and automatically engaging in detrimental behavior.

You can also reframe by using phrases such as "it's not me; it's just my brain" or by picking up on your thinking errors.

One common thinking error is _all-or-nothing (black-or-white) thinking_, which involves classifying information in extremes, such as calling a situation "perfect" or "ruined." One of the authors' clients, used this thinking error to inform her actions, which meant that if she wasn't able to do something perfectly, she was unlikely to try doing it at all.

Another common error is _catastrophizing_, which means blowing your current experiences out of proportion or predicting that something bad is going to happen. People who over-analyze and engage in "what-if" thinking fall prey to catastrophizing a lot.

Reframing will help you recognizing these errors and you'll find solace that they aren't real; they are merely a little part your brain.

### 8. Step 3: Refocus by placing your attention on a healthy, constructive activity while the deceptive brain messages are present. 

Like most of us, your attention is probably easily caught by a variety of deceptive thoughts and impulses which cause you to become trapped in repetitive cycles of unhealthy behavior, such as overeating and over-thinking.

The third step — refocusing — can be used to alleviate these destructive attention grabbers and it entails focusing your attention on a productive activity.

The purpose of refocusing is to build up your assurance that, through experience, you can continue with your day despite the presence of deceptive thoughts and impulses.

To refocus, you'll need to choose a constructive, helpful behavior that maintains your attention. However, since it's pretty challenging to think up ways of refocusing when you're right in the middle of dealing with deceptive brain messages, it's best to jot down a list of refocus activities before the false messages crop up.

Some examples of engaging and helpful activities might be to go for a "mindful" walk by concentrating on your footsteps, the scenery and environment; doing a short workout, reading, or calling a friend. Make sure you write down plenty of activities so that you have a go-to list when you need it.

You should refocus even while the deceptive thoughts are happening. Many people falsely think that to refocus means to distract yourself from the deceptive thoughts to escape them or remove them. Instead, it's about managing your responses to the distressing thoughts and feelings and allowing them to be present. If you try to force the thoughts and sensations to go away, you're feeding them attention, strengthening that response into your brain; whereas, if you let them be and choose healthy activities, your mind will naturally refocus.

> _"When dealing with deceptive brain messages, it's not what you think or feel that matters; it's what you do that counts!"_

### 9. Step 4: Revaluing encourages you to cultivate your friendly and healthy side and see that your uncomfortable sensations are caused by false brain messages. 

Although your deceptive messages will try to convince you otherwise, you need to change your _beliefs_ about yourself — _not yourself_ — in order to rewire your brain for healthy, loving thinking.

_Revaluing_ is the fourth and final step in reprogramming your brain, and it involves looking at situations from the wider perspective of your healthy and loving side, instead of from the limited, negative perspective of your deceptive brain messages.

Revaluing requires you to understand that you've been experiencing your life from the narrow point of view of the deceptive brain messages.

Take another of the authors' clients, for example, whose life was controlled by her perfectionism until she learned how to revalue what she experienced. While challenging her flawed thoughts and refusing to give in to her impulses toward perfectionistic behavior, she began to notice how restricted her life had become. She noticed that constantly checking to see if she'd slipped up or upset someone had actually become a real obstacle in her life, as it often prevented her from taking action.

Essentially, revaluing is deciding to view your life with a positive, compassionate mindset.

Another important tenet of revaluing is to learn how to care for yourself so you can make decisions from a stable, loving place, rather than from fear, anger, sadness or other deceptive emotions.

The client above, for instance, learned to deal with her inhibiting perfectionism by asking herself what she would say to comfort a friend. Doing that helped her look at the situation from another person's caring and compassionate perspective and to revalue the situation from that point of view.

### 10. Final summary 

The key message in this book:

**Your brain gives false, destructive messages, that trigger detrimental patterns and habits. But you have the ability to reshape this brain wiring. The Four Steps of relabeling, reframing, refocusing and revaluing allow you to break down the associations between unhealthy thoughts and habits.**

Actionable advice:

**The next time you feel the urge to procrastinate, go through the Four Steps.**

(1) Relabel by saying what's happening: e.g., "I'm having the urge to go on Facebook."

(2) Reframe by reminding yourself why it's troubling you: "Checking Facebook reduces my anxiety that I might not be able to complete the work I should be doing."

(3) Refocus by doing something productive like beginning the easiest work task.

(4) Revalue by recognizing that this impulse to procrastinate is just a deceptive brain message, and needn't be taken seriously.

**Keep a notebook of healthy activities.**

In order to work through the Four Steps effectively, why not keep a little book of healthy, productive or fun refocusing activities? This way, you'll never be short of activities to distract you when a deceptive brain message pops up.

**Suggested further reading:** ** _Brain Bugs_** **by Dean Buonomano**

_Brain_ _Bugs_ explores the inner workings of the human brain — both its incredible capabilities and its major flaws. Drawing on a wealth of examples and current research, the author illustrates how the brain's many blind spots and weaknesses lead us to make foolish decisions, recall false memories, and fear the wrong things. He also offers a number of suggestions for managing our brain bugs.
---

### Jeffrey M. Schwartz and Rebecca Gladding

Jeffrey M. Schwartz is a medical doctor and research psychiatrist at the UCLA School of Medicine. His research focuses on the field of self-directed neuroplasticity and its application to obsessive-compulsive disorder.

Rebecca Gladding is a medical doctor and was a clinical instructor and attending psychiatrist at UCLA. Her expertise lies in mindfulness, anxiety and depression.

