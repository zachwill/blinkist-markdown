---
id: 5417e9666362610008100000
slug: you-are-now-less-dumb-en
published_date: 2014-09-15T00:00:00.000+00:00
author: David McRaney
title: You are Now Less Dumb
subtitle: How to Conquer Mob Mentality, How to Buy Happiness, and All the Other Ways to Outsmart Yourself
main_color: A2D0F1
text_color: 34678C
---

# You are Now Less Dumb

_How to Conquer Mob Mentality, How to Buy Happiness, and All the Other Ways to Outsmart Yourself_

**David McRaney**

_You_ _Are_ _Now_ _Less_ _Dumb_ provides thought-provoking studies and examples on how our brains often misguide us. It also enlightens readers with tips on how to uncover these unfounded beliefs and find out what's actually happening in and around us.

---
### 1. What’s in it for me? Discover why you shouldn’t always trust your brain! 

You always know why you feel the way you do, right? Well… maybe you really don't. _You_ _Are_ _Now_ _Less_ _Dumb_ explains how our brains often deceive us into believing things that have no basis in reality. Our own behavior and feelings in crowds or towards people we find attractive, for example, seem to be fairly well understood by us. But in many cases, we don't know our real reasons for saying and doing the things we say and do.

This book provides entertaining examples of how our brains play tricks on us and what we can do to avoid being fooled by these illusions.

In these blinks, you'll find out

  * why home remedies don't always work;

  * why you're seldom alone in your opinions; and

  * how wearing a doctor's coat can make you smarter.

### 2. Your brain fools you everyday – without you even realizing it! 

Every day, your brain tricks you into believing the strangest things. Similar to an optical illusion, our brains often deceive us — even when we're pretty sure what we're observing is the truth.

And though we like to think our thoughts and decisions are grounded in logic and rationality — often they're not.

Our perception of the world is unique to each of us. The way we experience the sight and smell of a flower, for example, is not the way a bee experiences it. Therefore, we can't always trust that what we see or sense is the only truth.

For example, in 1951, after an important football match between Dartmouth College and Princeton University, during which both teams struggled and Princeton ended up victorious, both schools published articles about it in their college newsletters.

But it seemed, based on the newsletters, as though the two schools had witnessed entirely different matches. Princeton students accused their rivals of being uncivilized, while Dartmouth students reported that both teams were to blame and that the game had been altogether fair.

Psychologists decided to look into this discrepancy and had students fill out questionnaires about the game. They concluded that the students had alternate versions of reality depending on their attitude and the school they supported.

Although all the students had watched the same game, not unlike the above difference between the perception of a bee and a human, they interpreted what they witnessed in entirely different ways.

Now, most of us know that what we see is not always the absolute truth. Yet, under certain circumstances, such as watching an important sports game, we often fail to remember this. In order to become less naive, we need to be aware of this — at least some of the time.

### 3. Our attitudes influence our behavior and vice versa. 

Do you feel like a respected manager because you wear a suit to work or do you wear a suit to work because you feel like a respected manager? Actually both are true. When we have an attitude, we act in accordance with it, but we also form attitudes that stem from how we act.

It's logical that, when we do a favor for someone, they're likely to appreciate us more than before. But did you know that it also makes _us_ like _them_ more? This phenomenon is known as the _Benjamin_ _Franklin_ _effect_ because Franklin once turned a rival into a friend by asking him to borrow a special book.

When Franklin's opponent lent him the book, he at first felt good only because he was being generous. However, when they met afterwards, his attitude towards Franklin was more positive. His brain simply had to find a reason why he did Franklin a favor. The most obvious explanation was that he wouldn't have lent him the book if he had disliked him, so he mustn't have disliked him after all!

Our behavior also influences how we feel about other things. This phenomenon isn't restricted to human interactions, but extends to how we feel about objects or situations.

In one study, researchers had participants sit at a desk, presented them with pictures of unfamiliar Chinese ideographs, and asked them to judge whether they were positive or negative. During this task, the participants were asked to either pull the table up or push against it. Remarkably, participants assessed the ideographs more positively when associated with the pulling motion than the pushing motion.

That's because pulling is associated with positivity and pushing with negativity. Even as babies, we pull ourselves towards things we desire and push ourselves away from unwanted objects. Amazingly, just these simple actions swayed the participants' perception.

### 4. When one event follows another, we tend to think they belong together. 

Say you ate a chicken sandwich a couple of hours ago and now you're starting to feel sick. What do you think could be the reason? The sandwich, right?

Not so fast.

When two events happen one after another, we tend to infer that the earlier event influenced the later one. Our brains automatically seek out reasons for why things happen and arrange our thoughts within certain frameworks.

For example, if you touch a hot stove and feel pain in your hand, you're not going to repeat that action. And that makes sense.

The problem is: our brain also follows this logic even when it doesn't make sense. This is known as the _post_ _hoc_ _fallacy_.

Imagine you're sick with a cold. You decide to use your grandma's old home remedy and, after a few days, your cold is gone. In this case, you're likely to conclude that your grandma's remedy has worked, even though your cold might've likely have disappeared by then anyway.

That's because our brain often falls into the "because of that, this happened" trap.

Interestingly, this also explains the _placebo_ _effect_.

In medical studies, placebo groups receive "fake" medication, i.e., medication that contains no curative ingredients. When participants communicate a benefit from this, it's called the placebo effect. It's similar to the post hoc fallacy in the sense that the placebo group associate their "medicine" with their improved state.

To illustrate this point, a research team applied an anesthetic cream to participants' arms. The cream didn't contain any effective ingredients, yet when the researchers applied heat to the arm, brain scans of the participants revealed that they experienced less pain in that area. Participants were convinced the cream had worked.

The participants' brain expected an effect from the cream; therefore, they employed the structure of "because of that, this happened."

> _"Post hoc rationalization is the fairy godmother of all things inaccurate, non-scientific, mystical, mythological and superstitious."_

### 5. Knowing just one trait of another person will influence the way we appraise the rest of their character. 

Would you agree that sometimes you're a bit shallow and form an opinion about another person based on just one characteristic? If so, don't worry — you're not alone!

When a person displays a certain trait, it influences how we perceive their personality in general. This is called the _Halo_ _Effect_.

When we get acquainted with someone, often, there's one trait that stands out. We usually notice this trait first and it then goes on to influence our behavior towards and perception of that person.

A single positive trait can make other characteristics appear more positive. Even if the person seems to act completely differently after a while, it's still hard for us to change our opinion of them.

In one US study, army officers were asked to judge particular qualities of the pilots' characters.

Interestingly, better pilots received higher ratings in leadership skills despite there being no evidence to indicate that. The officers simply presumed that the pilots' remarkable ability to fly an airplane was a reliable indicator of other traits. However, one doesn't actually have any bearing on the others.

Physical appearance also influences our attitude towards others.

Although we try to not judge people on how they look, we do so frequently. That's because it was important for our ancestors to assess potential mates based on their physical characteristics: better-looking people were often healthier and, therefore, we chose them in the hopes of producing healthier offspring.

Seeing a good-looking person causes us to ignore their negative traits and see only the desirable ones. We also perceive them to be smarter than people who are less attractive and are more likely to forgive them if they fall short of our expectations.

### 6. We’re unaware of the reasons behind our thoughts and feelings. 

Whether you're in a good or bad mood, you probably think you understand why you feel that way. But, actually, most of the time you really don't know. Sometimes the most obvious reason isn't the real one.

By now we know that our brains always need a rational explanation for events, but we're not aware of the true reasons behind our emotions, which often leads to situations where we're misguided.

In one study, a woman from a research team stood in the middle of the intimidating Capilano Suspension Bridge. While she stood there, she asked the men that walked past her to fill out a questionnaire. She also gave them her phone number in case they wanted to discuss the study with her later. The same experiment was also conducted on a regular bridge as a control for the study.

The purpose of this study was to find out whether the men would call the woman, thereby indicating whether they were attracted to her. The results showed that 50 percent of the men who crossed the scary suspension bridge called the women and a mere 12.5 percent of the control group did the same. But why?

The sensation of fear is similar to the feeling of having a crush on someone: both cause a certain nervousness in our stomachs. Well, the fear the men felt when crossing the bridge was actually misread by their brains. When the men traversed the bridge and saw the woman, their brains associated the feeling in their stomach with flirtatiousness or attraction instead of fear of the bridge.

So the next time you feel a certain emotion, try to dig a little deeper for the real reason behind your feelings — it might be different from what you think.

### 7. Counterarguments and negative comments influence your behavior more than positive ones. 

Imagine you're a huge fan of an author and when her new book comes out, it gets panned by critics. Do you think that that'd influence your feelings towards it?

When we hold strong views, we often dissect and refute arguments in order to confirm our beliefs.

We're inclined to defend our stance, so when somebody attempts to persuade us into believing something different, rather than shifting our opinion, it makes us stick to our beliefs even more. This is referred to as the _backfire_ _effect_.

For example, some people believe that Barack Obama wasn't born in the United States. Yet even after his birth certificate revealed that he was, those people persisted in their belief and went so far as to create theories about the certificate being bogus.

But although we stand by our beliefs, we're attracted to things that contradict them.

Did you ever realize that you're far more sensitive to someone giving you a negative comment than a positive one? It probably rattles you and you analyze it until you find evidence to disprove it and confirm your own belief.

In one study, researchers gave participants a strip of paper to test their saliva for risk of pancreatic disorder. One group was informed that the paper would turn green after 20 seconds if they were at risk, while the other group was told the strip would turn green if they _weren't_ at risk.

In reality, the strip was just normal paper and never changed. The group that believed the strip would turn green if they were at risk waited 20 seconds and, when nothing changed, they were relieved and stopped looking at it: their healthy self-image was confirmed.

However, the group that thought the paper would tell them if they had no risk waited longer and even re-tested. They were more invested in making sure the strip was wrong and that their health was indeed intact.

> _"Like most people, you still pick and choose what to accept even when it comes out of a lab and is based on a hundred years of research."_

### 8. We’re bad at assessing whether a group shares our opinions and norms or not. 

It turns out that most college students don't actually enjoy drinking alcohol. So why do colleges report problems with excessive drinking?

Because we tend to go along with what we perceive as the norm for those around us. Problem is, this norm is often an inaccurate perception of other people's opinions.

Remember the last time you sat through a confusing presentation and at the end the presenter asked if there were any questions? You probably checked to see if anyone raised their hand before you asked anything. If no one raised their hand, you likely didn't either as you didn't want to feel alone in your confusion.

We all think we know the majority's thoughts and thus conclude that we're in the minority, i.e., that we have an alternative opinion. But this is false.

So what can we do? Well, the best way to stop our minds from getting the best of us is to ask others for their opinions.

We like to adhere to socially accepted rules, yet often fail to ascertain whether the majority of those around us supports these rules. We try to adapt and go with the flow but, unfortunately, the norms and regulations we like to be guided by are sometimes unclear and we can't figure out if it's really necessary to follow them.

One study at Princeton University asked students how much they liked the idea of consuming alcohol in college and what they thought an average student opinion was on it. Most students said they weren't very keen on drinking, but believed most other students really enjoyed it.

The researchers concluded that, although most students weren't actually very keen on drinking alcohol, they did it regardless because they believed it was the behavior of the majority.

> _"One of the great lessons you can take … is the near certainty that everything you think and feel is … shared with millions of other people."_

### 9. Forming and maintaining groups has a massive impact on our behavior. 

Within groups such as families, circles of friends and office teams, we feel we know everybody pretty well and this brings us a sense of comfort. But how does living in groups impact our behavior and thinking?

We establish groups in which we feel we're right and those outside the group are wrong.

Following one survey that studied liberal and conservative parties, researchers found that the members of one party believed they were well-versed in the ideas of the other party and, based on that knowledge, concluded that the other party's ideas were incorrect and senseless. And the members of the other party concluded exactly the same thing.

We tend to believe that different opinions must be the result of a lack of knowledge. Yet we think we don't have to take in other peoples' opinions because we already have all the information we need. Other people should listen to us because then they could see the world as accurately as we do, and see how right we are!

Unsurprisingly, this is what creates arguments, because both parties are stubborn in thinking that their opinion is the only truth.

Aside from arguing with other groups, we also exclude members of our groups if they behave inappropriately.

When we create groups, we protect their values. If there's one person who behaves contrary to these values, we deduce that they were never a true member of the group: because if they had been, they would've adhered to its values.

In 2011, for instance, a Norwegian man named Christian Anders Behring Breivik killed 27 people, saying this was in accordance with his religion. Afterwards, many Christians denied that Breivik was a real Christian, as no true Christian would have committed such a heinous act.

Living together in groups causes us to believe there is a homogeneity without any digression from the norm. When an exception arises, we conclude that this person isn't a member of the group rather than adapting our definition of it.

### 10. Our outfit choice influences who we are and what we do. 

Are you a fashionista? Or do you consider clothes mere pieces of fabric that are basic daily necessities? However you feel about them, clothes influence our behavior and the impressions we make on others.

Clothes aren't just a means of covering up our bodies — they communicate attitudes and ideas and over time have become symbols of character traits or behavior. This means that we often unconsciously make judgements about other people based on what they're wearing.

For example, no matter where you are in the world, it's a fairly safe bet that when it comes to business professionals, most people will wear a suit. Being outfitted in this type of clothing makes us look more professional and reliable. It's even been shown that when women wear less stereotypically feminine business clothes, such as trouser suits, they're more likely to be hired than women who dress more femininely.

Astonishingly, wearing a certain type of clothing also impacts our mental capability. And since we have a tendency to link certain clothes with certain characteristics, we assess others based on their clothes — and change our behavior.

In one experiment, researchers split participants into groups and asked them to wear white coats. One group was told it was a doctor's coat, and the other group was told it was a painter's coat. Then the groups were given some mental tasks to complete. The group that believed they were wearing a doctor's coat completed far more of the tasks correctly than the group who thought they were wearing painter's coats.

The mere thought of wearing the clothing of a doctor, a profession considered to require high intelligence and accuracy, caused the participants to behave more intelligently and accurately themselves.

> _"The brain under a wizard's hat is different from one under a cowboy hat … because the architecture of your memory won't allow any object to be neutral."_

### 11. Money can buy happiness – but only to a certain extent. 

If someone offered you the choice of $140,000 a year but you could only sleep six hours every night or $80,000 a year and you could sleep eight hours every night, which would you choose? Chances are you'd take the first option.

But, in reality, there's a limit to how much money can actually improve our lives.

In order to be happy, we all need a certain amount of money to make sure our basic needs are being met. Although many studies show that more money makes us happier, a significant improvement in happiness actually maxes out when we reach an average income of about $75,000 a year. It was shown that earning more than this amount had no effect on increasing our happiness.

However, our brains are not aware of this and so we still believe more money will bring us more joy.

For example, in 2011, American participants were asked if they would like to earn more money or get more sleep. Most participants said they would prefer the money, as they believed this would improve their lives. But we now see this notion is only an illusion.

Additionally, getting paid for doing something we love often decreases the pleasure we receive from doing it. Wouldn't you have thought that being financially rewarded for your hobby would be absolutely ideal? Surprisingly, it's not! We enjoy doing the things we love because they're intrinsically motivating.

Let's say you love doing yoga because it makes you feel relaxed. But if you start getting paid for it, your brain tricks you into thinking your motivation stems from an external source. So now you're not doing yoga because you enjoy it but because it earns you money!

Then, once you stop getting paid for it, you'll probably feel like there's no point in doing it anymore because you no longer receive the initial pleasure you associated with it.

### 12. Final summary 

The key message in this book:

**Our** **brains** **cannot** **be** **totally** **trusted!** **Every** **day** **they** **select** **thoughts** **and** **reinforce** **beliefs** **based** **on** **"evidence"** **that** **may** **be** **contrary** **to** **reality.** **However,** **being** **aware** **of** **certain** **tricks** **our** **brains** **play** **on** **us** **in** **certain** **situations** **can** **help** **us** **better** **understand** **ourselves** **and** **become** **less** **naive** **about** **how** **we** **think** **and** **behave.**

Actionable advice:

**Be** **aware** **of** **what** **you** **wear!**

Fashionista or not, the importance and impact of what you wear _does_ make a difference to other peoples' perception of your characteristics. So, the next time you go for a job interview, spend a little extra time selecting your outfit.

**Think** **carefully** **before** **you** **get** **paid** **for** **your** **hobby.**

Sure, it seems ideal to get paid for doing what you love. But be conscious that being rewarded for something you already enjoy might just suck all your intrinsic motivation out of it.

**Suggested** **further** **reading:** **_You_** **_Are_** **_Not_** **_So_** **_Smart_** **by** **David** **McRaney**

_You_ _Are_ _Not_ _So_ _Smart_ explores the many different ways we have of deluding ourselves. By delving into a wide range of psychological research, the author challenges the notion that we are logical, rational beings who see the world as it really is and makes a case that we mislead ourselves every single day, for better and for worse.
---

### David McRaney

David McRaney is the author of the popular blog _You_ _Are_ _Not_ _So_ _Smart_, which became a best-selling book of the same name. McRaney has also worked as a reporter, editor, TV host and public speaker. McRaney lives in Mississippi with his wife.

